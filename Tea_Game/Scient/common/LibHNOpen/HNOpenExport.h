﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNOpenExport_H__
#define __HNOpenExport_H__

#include "UMeng/Cocos2dx/Common/CCUMSocialSDK.h"
#include "UMeng/UMengSocial.h"

USING_NS_UM_SOCIAL;

#endif	//__HNOpenExport_H__