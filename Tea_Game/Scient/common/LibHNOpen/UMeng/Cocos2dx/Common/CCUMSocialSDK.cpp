/**
 * FileName : CCUMScoialSDK.cpp
 * Author   : hehonghui
 * Company  : umeng.com
 * 该类为友盟社会化组件的Cocos2d-x版本，在友盟社会化组件Android、iOS原生SDK上进行适配，支持打开分享面板分享、直接分享、授权某平台、删除某平台授权、判断某平台是否授权这几个核心功能。
 *
 * Android 平台实现的代码参考UmengSocial/Android目录下的实现, IOS平台的参考UmengSocial/IOS目录下的实现。
 *
 */

#include <UMeng/Cocos2dx/Common/CCUMSocialSDK.h>
#include <platform/CCCommon.h>
#include <stddef.h>

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "UmSocialControllerIOS.h"

#elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "UMeng/Cocos2dx/Android/CCUMSocialController.h"

#endif

USING_NS_CC;
using namespace std;
// 使用友盟命令空间 
USING_NS_UM_SOCIAL;

CCUMSocialSDK* CCUMSocialSDK::_instance = NULL;

/*
 * 构造函数, 必须传递友盟appKey
 * @param appKey 友盟appKey
 */
CCUMSocialSDK::CCUMSocialSDK() :
		mPlatforms(new vector<int>()),
        _wrapperType("Cocos2d-x"),
        _wrapperVersion("2.0") {


	initSDK();
}

/*
 * 初始化sdk
 */
void CCUMSocialSDK::initSDK() {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	// 初始化cocos2d-x平台
	initCocos2dxSDKFromJni(_wrapperType, _wrapperVersion);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	UmSocialControllerIOS::initCocos2dxSDK(_wrapperType, _wrapperVersion);

#endif
}

/*
 * 创建CCUMSocialSDK对象, 单例模式
 * @param appKey 友盟app key
 */

CCUMSocialSDK* CCUMSocialSDK::create() {

	if (_instance == NULL) {
		_instance = new CCUMSocialSDK();
	}
	return _instance;
}

/*
 * 返回SDK中设置的所有平台
 */
vector<int>* CCUMSocialSDK::getPlatforms() {
	return mPlatforms;
}

/*
 * 对某平台进行授权
 * @param platfrom 要授权的平台, 参考CCUMTypeDef.h中的Platform枚举定义
 * @param  callback 授权回调, 具体参考CCUMTypeDef.h中的定义
 */
void CCUMSocialSDK::authorize(int platform, AuthEventHandler callback) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	// 授权某平台
	doAuthorize(platform, callback);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	UmSocialControllerIOS::authorize(platform, callback);

#endif
}

/*
 * 删除某平台的授权信息
 * @param platform 要删除授权的平台
 * @param   callback 删除授权的回调, 具体参考CCUMTypeDef.h中的定义
 */
void CCUMSocialSDK::deleteAuthorization(int platform,
		AuthEventHandler callback) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

	deletePlatformAuthorization(platform, callback);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	UmSocialControllerIOS::deleteAuthorization(platform, callback);

#endif
}

/*
 * 判断某平台是否已经授权
 * @param platform 要判定的平台, 参考CCUMTypeDef.h中的Platform枚举定义
 */
bool CCUMSocialSDK::isAuthorized(int platform) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

	return isPlatformAuthorized(platform);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	return UmSocialControllerIOS::isAuthorized(platform);

#endif
    return false;
}
//登录SDK
void CCUMSocialSDK::getPlatformInfo(int platforms, AuthEventHandler callback) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	if (platforms == XIANLIAO)
	{
		getPlatformXLInfos(platforms, callback);
	}
	else
	{
		getPlatformInfos(platforms, callback);
	}
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	UmSocialControllerIOS::getinfo(platforms, callback);
#endif
}
/*
 * 打开分享面板
 * @param text 要分享的文字内容
 * @param imgName
 * 	 // *******************************************
 要分享的图片支持url图片、assets目录下的图片、资源图片和存放在sd卡目录下的图片。这四种图片对于前缀都有一定的要求，要求如下:
 //
 1、url图片必须以"http://"或者"https://"开头,例如 : http://www.umeng.com/images/pic/home/feedback/banner.png；
 2、assets目录下的图片必须以"assets/"开头,cocos2d-x的资源图片默认会添加到该目录只, 例如 : assets/CloseNornal.png;
 3、资源图片即放在工程中的res/drawable中的图片, 必须以"res/"开头，例如 : res/myimage.png;
 4、sd卡目录下的图片即存放在本地目录的图片，此时传递绝对路径即可，例如 : /sdcard/myimage.jpg;
 5、iOS平台直接传递图片名路径或引用路径即可，例如：myimage.png;
 // *******************************************
 * @param callback 分享回调,具体参考CCUMTypeDef.h中的定义
 */
void CCUMSocialSDK::openShare(vector<int>* platforms,const char* text, const char* title,const char* imgName,const char* targeturl,ShareEventHandler callback) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

	doOpenShare(platforms,text,title,imgName,targeturl,callback);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	UmSocialControllerIOS::openShareWithImagePath(platforms,text,title,imgName,targeturl,callback);
#endif
}
void CCUMSocialSDK::setBoardDismissCallback(BoardDismissEventHandler callback){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    
    setDismissCallback(callback);
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
   	UmSocialControllerIOS::setDismissCallback(callback);
#endif
}
void CCUMSocialSDK::openShareBoard(vector<int>* platforms,const char* text, const char* title,const char* imgName,const char* targeturl) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    // 设置分享内容
    //	setShareTextContent(text);
    //	// 设置图片内容
    //	setShareImagePath(imgName);
    // 打开分享面板
    doOpenShare(platforms,text,title,imgName,targeturl,NULL);
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    //	UmSocialControllerIOS::openShareWithImagePath(mPlatforms, text, imgName, callback);
#endif
}


void CCUMSocialSDK::openCustomShare(vector<int>* platforms,BoardEventHandler callback) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
   
    doCutomOpenShare(platforms,callback);
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
   	UmSocialControllerIOS::openCustomShareBoard(platforms, callback);
#endif
}

/*
 * 直接分享到某个平台，不打开分享面板和内容编辑页面
 * @param platform 要分享到的目标平台， 参考CCUMTypeDef.h中的Platform枚举定义
 * @param text 要分享的文字内容
 * @param imgName
 * 	 // *******************************************
 要分享的图片支持url图片、assets目录下的图片、资源图片和存放在sd卡目录下的图片。这四种图片对于前缀都有一定的要求，要求如下:
 //
 1、url图片必须以"http://"或者"https://"开头,例如 : http://www.umeng.com/images/pic/home/feedback/banner.png；
 2、assets目录下的图片必须以"assets/"开头,cocos2d-x的资源图片默认会添加到该目录只, 例如 : assets/CloseNornal.png;
 3、资源图片即放在工程中的res/drawable中的图片, 必须以"res/"开头，例如 : res/myimage.png;
 4、sd卡目录下的图片即存放在本地目录的图片，此时传递绝对路径即可，例如 : /sdcard/myimage.jpg;
 5、iOS平台直接传递图片名路径或引用路径即可，例如：myimage.png;
 // *******************************************
 * @param callback 分享回调，具体参考CCUMTypeDef.h中的定义
 */
void CCUMSocialSDK::directShare(int platform, const char* text,const char* title,const char* targeturl,
		const char* imgName, ShareEventHandler callback) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID


	doDirectShare(text,title,targeturl,imgName,platform, callback);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	UmSocialControllerIOS::directShare(text,title,targeturl,imgName,platform, callback);

#endif
}
void CCUMSocialSDK::directShareAndroid(int platform, const char* text,const char* title,const char* targeturl,
                        const char* imgName, ShareEventHandler callback){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    
    
    doDirectShare(text,title,targeturl,imgName,platform, callback);
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
 
    
#endif
}
void CCUMSocialSDK::directShareIos(int platform, const char* text,const char* title,const char* targeturl,
                    const char* imgName, ShareEventHandler callback){
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    
    
      
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    UmSocialControllerIOS::directShare(text,title,targeturl,imgName,platform, callback);
    
#endif
}


/*
 *  是否开启log
 * @param flag 如果为true则开启log, 否则关闭.
 */
void CCUMSocialSDK::setLogEnable(bool flag) {
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

	setAndroidLogEnable(flag);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	UmSocialControllerIOS::openLog(flag);

#endif
}

void CCUMSocialSDK::directShareToXL(int platform, const char* text, const char* title, const char* targeturl,
	const char* imgName, ShareEventHandler callback)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

	doDirectShareToXL(text, title, targeturl, imgName, platform, callback);

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	UmSocialControllerIOS::directShareToXL(text,title,targeturl,imgName,platform, callback);
#endif
}

bool CCUMSocialSDK::isXLAppInstalled()
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

	return isAndroidXLAppInstalled();

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	return UmSocialControllerIOS::isIOSXLAppInstalled();

#endif
	return false;
}
