/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __UMeng_Social_h__
#define __UMeng_Social_h__

#include <vector>
#include "HNHttp/HNHttp.h"
#include "Cocos2dx/Common/CCUMSocialSDK.h"

USING_NS_CC;

namespace HN
{
#define WECHAT_OPEN_ID			"weChat_open_id"
#define WECHAT_ACCESS_TOKEN		"weChat_access_token"
#define WECHAT_REFRESH_TOKEN	"weChat_refresh_token"
#define QQ_OPEN_ID				"qq_open_id"
#define QQ_ACCESS_TOKEN			"qq_access_token"
    
    
static const std::string umAppKey         = "555bfe6767e58e7dcc0051d5";
static const std::string qqAppKey         = "1105632101";
static const std::string qqAppSecret      = "Ipv3a6AjZuYoVjV5";
static const std::string weChatAppKey     = "wxd78eef84d7282bfa";
static const std::string weChatAppSecret  = "edec60bb26b0cf6ca5ba9b7c525362ab";

	struct ThirdPartyLoginInfo
	{
		std::string unionid;
		std::string nickName;
		std::string headUrl;
		bool bBoy;

		ThirdPartyLoginInfo()
		{
			unionid = "";
			nickName = "";
			headUrl = "";
			bBoy = false;
		}
	};

	class UMengSocial : public HNHttpDelegate
	{		
	public:
		typedef std::function<void(bool success, ThirdPartyLoginInfo* info, const std::string& errorMsg)> AuthorizeCallBack;

		AuthorizeCallBack onAuthorizeCallBack = nullptr;

		typedef std::function<void(bool success, int platform, const std::string& errorMsg)> ShareCallBack;

		ShareCallBack onShareCallBack = nullptr;

	public:
		// 获取单例
		static UMengSocial* getInstance();

		// 销毁单例
		static void destroyInstance();

	public:
		UMengSocial();

		~UMengSocial();

    public:
		
		/*	邀请 / 炫耀
		*	如果想要分享纯图片，则bShow必须为true
		*	platform可以指定分享到某平台，-1为打开默认分享面板
		*	text为游戏需要分享的玩法，底分等信息
		*/
		//void inviteFriends(const std::string& gameName, const std::string& deskPassword, bool bShow = false, int platform = -1, const std::string& text = "");

		/*	执行分享
		*	如果想要分享纯图片，则text和url都必须为空
		*	platform可以指定分享到某平台，-1为打开默认分享面板
		*	node为想要截屏的节点，如整个结算列表，nullptr为全屏截屏
		*/
		void doShare(const std::string& text, const std::string& titile, const std::string& url, const std::string& imgName = "", 
			int platform = -1, Node* node = nullptr);

		inline void setShareLayer(Node* shareLayer){ _shareLayer = shareLayer; }
	public:
		// 执行第三方登录
		void doThirdLogin(int platform);

		// 删除授权
		void doDedeteAuthorize(int platform);

	private:
		// 设置分享平台
		vector<int>* setSharePlatforms(bool bWeixin = true, bool bQQ = false, bool bWeixinCircle = true, bool bQzone = false, bool bSina = true);

		// 执行分享
		void openUMengShare(int platform = -1);

		// 分享回调
		static void UMengShareCallback(int platform, int stCode, std::string& errorMsg);

		// 闲聊分享回调
		static void XLShareCallback(int platform, int stCode, std::string& errorMsg);

		// 授权并获取用户信息回调
		static void doAuthAndGetInfoCallback(int platform, int stCode, std::map<std::string, std::string>& data);

		void closeShareLayer();//关闭分享界面
	private:
		// 分享失败
		void shareFaild(const std::string& errorMsg);

		// 登陆失败，需要重新授权
		void loginFaild(const std::string& errorMsg);

	private:
		// 刷新微信授权凭证
		void refreshToken();

		// 刷新结果
		void refreshResult(const std::string& data);

		// 调用webApi获取用户信息
		void getUserInfo();

		// 获取微信用户信息结果
		void getWeChatInfoResult(const std::string& data);

		// 获取QQ用户信息结果
		void getQQInfoResult(const std::string& data);

	private:
		virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	protected:
		// text
		CC_SYNTHESIZE_PASS_BY_REF(std::string, _shareText, ShareText);

		// titile
		CC_SYNTHESIZE_PASS_BY_REF(std::string, _shareTitile, ShareTitile);

		// image
		CC_SYNTHESIZE_PASS_BY_REF(std::string, _shareImg, ShareImg);

		// ShareUrl
		CC_SYNTHESIZE_PASS_BY_REF(std::string, _shareUrl, ShareUrl);

		bool _bShare = true;

		int _authPlatform;

		Node* _shareLayer;
#define HNUMengSocial() UMengSocial::getInstance()
	};
}

#endif // __UMeng_Social_h__
