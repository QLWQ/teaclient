/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "UMengSocial.h"
#include "HNUIExport.h"
#include "HNNetExport.h"
#include "../LibHNLobby/HNLobby/GameChildLayer/GameShareLayer.h"
#include "../LibHNMarket/HNOperator.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

USING_NS_UM_SOCIAL;

using namespace network;

#define JSON_RESOVE(obj, key) (obj.HasMember(key) && !obj[key].IsNull())

namespace HN
{
	static UMengSocial* social = nullptr;

	UMengSocial* UMengSocial::getInstance()
	{
		if (nullptr == social)
		{
			social = new (std::nothrow) UMengSocial();
		}
		return social;
	}

	void UMengSocial::destroyInstance()
	{
		CC_SAFE_DELETE(social);
	}

	UMengSocial::UMengSocial()
	{
		_shareLayer = nullptr;
	}

	UMengSocial::~UMengSocial()
	{
		HNHttpRequest::getInstance()->removeObserver(social);
	}

	// 设置分享平台
	vector<int>* UMengSocial::setSharePlatforms(bool bWeixin, bool bQQ, bool bWeixinCircle, bool bQzone, bool bSina)
	{
		// 设置友盟分享面板上显示的平台
		vector<int>* platforms = new vector<int>();
		if (Operator::requestChannel("sysmodule", "isAppInstalled", StringUtils::format("%d", QQ)).compare("true") == 0)
		{
			if (bQQ)			platforms->push_back(QQ);
			if (bQzone && "" != _shareText)			platforms->push_back(QZONE); // QQ空间只能分享图文形式，不能分享纯图片
		}
		
		if (Operator::requestChannel("sysmodule", "isAppInstalled", StringUtils::format("%d", WEIXIN)).compare("true") == 0)
		{
			if (bWeixin)		platforms->push_back(WEIXIN);
			if (bWeixinCircle)	platforms->push_back(WEIXIN_CIRCLE);
		}
		//if (bSina)			platforms->push_back(SINA);

		return platforms;
	}

	//void UMengSocial::inviteFriends(const std::string& gameName, const std::string& deskPassword,
	//	bool bShow/* = false*/, int platform/* = -1*/, const std::string& text)
	//{
	//	//设置分享链接
	//	std::string shareUrl = PlatformConfig::getInstance()->getShareUrl();
	//	shareUrl.append("?GameID=0");
	//	shareUrl.append("&RoomID=");
	//	shareUrl.append(deskPassword);
	//	shareUrl.append("&scheme=redbirddzb");
	//	if (CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
	//	{
	//		shareUrl.append("&open=openwith");
	//		shareUrl.append("&host=com.redbird.ChessRoom-Lite");
	//	}

	//	//设置分享标题	
	//	std::string shareTitile = StringUtils::format(GBKToUtf8("【房号：%s】欢赢娱乐-%s"),
	//		deskPassword.c_str(), GBKToUtf8(gameName));

	//	//设置分享内容
	//	std::string shareContent = StringUtils::format(GBKToUtf8("我在欢赢娱乐<%s>，%d局，%s快来加入！"),
	//		GBKToUtf8(gameName), HNPlatformConfig()->getAllCount(), text.c_str());

	//	shareUrl = HNPlatformConfig()->getShareUrl();
	//	if (bShow)
	//	{
	//		//如果要分享单张图片，文字和链接需要为空
	//		shareContent = "";
	//		shareUrl = "";
	//	}

	//	doShare(shareContent, shareTitile, shareUrl, "", platform);
	//}

	// 执行分享
	void UMengSocial::doShare(const std::string& text, const std::string& titile, const std::string& url, const std::string& imgName,
		int platform, Node* node)
	{
		if (_shareLayer)  //分享前隐藏
		{
			GameShareLayer* pLayer = (GameShareLayer*)UMengSocial::getInstance()->_shareLayer;
			pLayer->removeShade(false);
			pLayer->setVisible(false);
		}

		_shareText = text;
		_shareTitile = titile.empty() ? "Welcome!" : titile;
		_shareImg = imgName;
		_shareUrl = "";
		/* url无http前缀，则添加 */
		if (url != "")
		{
			int a = url.compare(0, 7, "http://");
			if (0 != url.compare(0, 7, "http://") && 0 != url.compare(0, 8, "https://")) {
				_shareUrl.append("https://");
			}
			_shareUrl.append(url);
		}
	
		if (imgName.empty())
		{
			Node* captureNode = (node == nullptr) ? Director::getInstance()->getRunningScene() : node;
			float scale = (node == nullptr) ? 0.5f : 0.8f;
			auto img = utils::captureNode(captureNode, scale);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			_shareImg = "/sdcard/umeng_share.jpg";
#else
			_shareImg = FileUtils::getInstance()->getWritablePath() + "umeng_share.jpg";
#endif
			img->saveToFile(_shareImg);
		}
		openUMengShare(platform);
	}

	// 执行分享
	void UMengSocial::openUMengShare(int platform)
	{
#if (CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID && CC_TARGET_PLATFORM != CC_PLATFORM_IOS) 
		MessageBox("Only supports Android and IOS!", "error");
		return;
#endif
		CCUMSocialSDK* sdk = CCUMSocialSDK::create();
		if (XIANLIAO == platform)
		{
			if (!sdk->isXLAppInstalled())
				return;
			sdk->directShareToXL(platform, _shareText.c_str(), _shareTitile.c_str(), _shareUrl.c_str(), _shareImg.c_str(), share_selector(XLShareCallback));
		}
		else
		{
			// 获取分享目标平台
			vector<int>* platforms = setSharePlatforms();

			if (platform == -1)
			{
				if (platforms->empty())
				{
					platforms->push_back(SMS);
				}
				// 打开分享面板分享
				sdk->openShare(platforms, _shareText.c_str(), _shareTitile.c_str(), _shareImg.c_str(), _shareUrl.c_str(), share_selector(UMengShareCallback));

			}
			else
			{
				if (WEIXIN == platform || WEIXIN_CIRCLE == platform)
				{
					if (Operator::requestChannel("sysmodule", "isAppInstalled", StringUtils::format("%d", WEIXIN)).compare("false") == 0)
					{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) 
						platform = SMS;
#else
						MessageBox("Application not installed!", "error");
						return;
#endif

					}
				}

				if (QQ == platform || QZONE == platform)
				{
					if (Operator::requestChannel("sysmodule", "isAppInstalled", StringUtils::format("%d", QQ)).compare("false") == 0)
					{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) 
						platform = SMS;
#else
						MessageBox("Application not installed!", "error");
						return;
#endif
					}
				}

				//直接分享到对应平台
				sdk->directShare(platform, _shareText.c_str(), _shareTitile.c_str(), _shareUrl.c_str(), _shareImg.c_str(), share_selector(UMengShareCallback));
			}
		}
	}

	void UMengSocial::closeShareLayer()
	{
		if (UMengSocial::getInstance()->_shareLayer)
		{//不需要特效 直接移除节点
			GameShareLayer* pLayer = (GameShareLayer*)UMengSocial::getInstance()->_shareLayer;
			pLayer->removeFromParent();
			UMengSocial::getInstance()->_shareLayer = nullptr;
		}
	}

	//友盟分享回调
	void UMengSocial::UMengShareCallback(int platform, int stCode, std::string& errorMsg)
	{
		UMengSocial::getInstance()->closeShareLayer();

		if (200 == stCode)
		{
			if (social->onShareCallBack)
			{
				social->onShareCallBack(true, platform, errorMsg);
				social->onShareCallBack = nullptr;
			}
		}
		else if (0 == stCode || -1 == stCode)
		{
			if (social->onShareCallBack)
			{
				social->onShareCallBack(false, platform, errorMsg);
				social->onShareCallBack = nullptr;
			}
		}
	}
	//闲聊分享回调
	void UMengSocial::XLShareCallback(int platform, int stCode, std::string& errorMsg)
	{
		UMengSocial::getInstance()->closeShareLayer();

		if (0 == stCode)
		{//成功
			if (social->onShareCallBack)
			{
				social->onShareCallBack(true, platform, errorMsg);
				social->onShareCallBack = nullptr;
			}
		}
		else //if (-2 == stCode || -4 == stCode) android下0成功，-2取消，-4失败，ios下0成功，1取消，2失败
		{//分享取消或者失败
			if (social->onShareCallBack)
			{
				social->onShareCallBack(false, platform, errorMsg);
				social->onShareCallBack = nullptr;
			}
		}
	}
	// 分享失败
	void UMengSocial::shareFaild(const std::string& errorMsg)
	{
		if (!social->onShareCallBack) return;

		social->onShareCallBack(false, -1, errorMsg);
		social->onShareCallBack = nullptr;
	}

	// 执行第三方登录授权
	void UMengSocial::doThirdLogin(int platform)
	{
		_authPlatform = platform;
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
		CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
		switch (_authPlatform)
		{
		case QQ:
		{
				std::string access = UserDefault::getInstance()->getStringForKey(QQ_ACCESS_TOKEN);
				if (!access.empty())
				{
					getUserInfo();
				}
				else
				{
					CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
				}
		}
			break;
		case WEIXIN:
		{
				std::string refre = UserDefault::getInstance()->getStringForKey(WECHAT_REFRESH_TOKEN);
				if (!refre.empty())
				{
					refreshToken();
				}
				else
				{
					CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
				}
		}
			break;
		case XIANLIAO:
		{
			CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
		}
			break;
		default:
			break;
		}
#endif
		
	}

	// 删除授权
	void UMengSocial::doDedeteAuthorize(int platform)
	{
		CCUMSocialSDK::create()->deleteAuthorization(platform, nullptr);
	}

	/*
	 *	授权回调
	 *	注意安卓包名和微信回调路径包名必须一致，否则无回调信息
	 *	例：安卓包名：com.redbird.project，微信回调路径包名必须为：com.redbird.project.wxapi
	 */
	void UMengSocial::doAuthAndGetInfoCallback(int platform, int stCode, std::map<std::string, std::string>& data)
	{
		if (stCode == 100)
		{
			log("#### getInfoBegin");
		}
		else if (stCode == 200)
		{
			log("#### getInfoFinish");

			// 输出授权数据, 如果授权失败,则会输出错误信息
			auto it = data.begin();
			for (; it != data.end(); ++it) {
				log("#### data  %s -> %s.", it->first.c_str(), it->second.c_str());
			}

			if (data.empty())
			{
				social->loginFaild("授权出错");
				return;
			}		

			switch (platform)
			{
			case QQ:
			case WEIXIN:
			{
				std::string openId;
				std::string access_token;
				std::string refresh_token;
				if (data.find("uid") != data.end()) openId = data.find("uid")->second;
				if (data.find("accessToken") != data.end()) access_token = data.find("accessToken")->second;

				if (platform == QQ)
				{
					UserDefault::getInstance()->setStringForKey(QQ_OPEN_ID, openId);
					UserDefault::getInstance()->setStringForKey(QQ_ACCESS_TOKEN, access_token);
				}

				if (platform == WEIXIN)
				{
					if (data.find("access_token") != data.end()) access_token = data.find("access_token")->second;
					if (data.find("refreshToken") != data.end()) refresh_token = data.find("refreshToken")->second;
					if (data.find("refresh_token") != data.end()) refresh_token = data.find("refresh_token")->second;

					log("#### openId:%s, access_token:%s, refresh_token:%s", openId.c_str(), access_token.c_str(), refresh_token.c_str());

					UserDefault::getInstance()->setStringForKey(WECHAT_OPEN_ID, openId);
					UserDefault::getInstance()->setStringForKey(WECHAT_ACCESS_TOKEN, access_token);
					UserDefault::getInstance()->setStringForKey(WECHAT_REFRESH_TOKEN, refresh_token);
				}
				UserDefault::getInstance()->flush();

				std::string uid = data.find("uid") != data.end() ? data.find("uid")->second : "";
				std::string name = data.find("name") != data.end() ? data.find("name")->second : "";
				std::string sex = data.find("gender") != data.end() ? data.find("gender")->second : "";
				std::string headurl = data.find("iconurl") != data.end() ? data.find("iconurl")->second : "";

				if (!uid.empty())
				{
					ThirdPartyLoginInfo userInfo;
					userInfo.unionid = uid;
					userInfo.nickName = name;
					userInfo.headUrl = headurl;
					userInfo.bBoy = (sex.compare(GBKToUtf8("男")) == 0) || (sex.compare("m") == 0);

					if (social && social->onAuthorizeCallBack)
					{
						social->onAuthorizeCallBack(true, &userInfo, "授权成功");
						HNHttpRequest::getInstance()->removeObserver(social);
					}
				}
				else
				{
					if (!UserDefault::getInstance()->getStringForKey(WECHAT_REFRESH_TOKEN).empty())
					{
						social->refreshToken();
					}
					else
					{
						social->loginFaild("授权失败");
					}
				}
			}break;
			case XIANLIAO:
			{
				log("xianliao====%d", stCode);
				social->loginFaild(StringUtils::format("授权码%d", stCode));
			}
				break;
			default:
				break;
			}
		}
		else if (stCode == 0)
		{
			social->loginFaild("授权出错");
		}
		else if (stCode == -1)
		{
			social->loginFaild("取消授权");
		}
		else
		{
			auto iter = data.find("message");
			if (iter != data.end())
			{
				std::string str = Utf8ToGBK(iter->second);
				social->loginFaild(str);
			}
			else
			{
				social->loginFaild("授权失败");
			}	
		}
	}

	// 登陆失败，需要重新授权
	void UMengSocial::loginFaild(const std::string& errorMsg)
	{
		if (onAuthorizeCallBack)
		{
			onAuthorizeCallBack(false, nullptr, errorMsg);
			HNHttpRequest::getInstance()->removeObserver(social);
		}
	}

	void UMengSocial::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		if (requestName.compare("getWeChatLoginInfo") != 0
			&& requestName.compare("getQQLoginInfo") != 0
			&& requestName.compare("refreshToken") != 0)
		{
			return;
		}

		if (!isSucceed)
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

			CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
			return;
		}

		if (requestName.compare("getWeChatLoginInfo") == 0)
		{
			getWeChatInfoResult(responseData);
		}
		else if (requestName.compare("getQQLoginInfo") == 0)
		{
			getQQInfoResult(responseData);
		}
		else if (requestName.compare("refreshToken") == 0)
		{
			refreshResult(responseData);
		}
		else
		{

		}
	}

	// 刷新授权凭证
	void UMengSocial::refreshToken()
	{
		std::string refre = UserDefault::getInstance()->getStringForKey(WECHAT_REFRESH_TOKEN);
		std::string url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + weChatAppKey + "&grant_type=refresh_token&refresh_token=" + refre;

		HNHttpRequest::getInstance()->addObserver(social);
		HNHttpRequest::getInstance()->request("refreshToken", cocos2d::network::HttpRequest::Type::GET, url);

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在获取授权信息"), 25);
	}

	// 刷新授权凭证结果
	void UMengSocial::refreshResult(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (JSON_RESOVE(doc, "errcode") || !doc.IsObject())
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

			CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
			return;
		}

		auto userDefault = UserDefault::getInstance();

		if (JSON_RESOVE(doc, "openid"))
		{
			std::string openid = doc["openid"].GetString();
			userDefault->setStringForKey(WECHAT_OPEN_ID, openid);
		}

		if (JSON_RESOVE(doc, "refresh_token"))
		{
			std::string refresh_token = doc["refresh_token"].GetString();
			userDefault->setStringForKey(WECHAT_REFRESH_TOKEN, refresh_token);
		}

		if (JSON_RESOVE(doc, "access_token"))
		{
			std::string access_token = doc["access_token"].GetString();
			userDefault->setStringForKey(WECHAT_ACCESS_TOKEN, access_token);
		}

		userDefault->flush();

		getUserInfo();
	}

	void UMengSocial::getUserInfo()
	{
		std::string requestName;
		std::string requestUrl;

		switch (_authPlatform)
		{
		case WEIXIN:
		{
			std::string openid = UserDefault::getInstance()->getStringForKey(WECHAT_OPEN_ID);
			std::string access = UserDefault::getInstance()->getStringForKey(WECHAT_ACCESS_TOKEN);

			requestUrl = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access + "&openid=" + openid;
			requestName = "getWeChatLoginInfo";
		}
			break;
		case QQ:
		{
			std::string openid = UserDefault::getInstance()->getStringForKey(QQ_OPEN_ID);
			std::string access = UserDefault::getInstance()->getStringForKey(QQ_ACCESS_TOKEN);

			requestUrl = "https://graph.qq.com/user/get_user_info?access_token=" + access + "&openid=" + openid
				+ "&oauth_consumer_key=" + qqAppKey;
			requestName = "getQQLoginInfo";
		}
			break;
		default:
			break;
		}

		HNHttpRequest::getInstance()->addObserver(social);
		HNHttpRequest::getInstance()->request(requestName, cocos2d::network::HttpRequest::Type::GET, requestUrl);

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在获取用户信息"), 25);
	}

	// 获取信息结果
	void UMengSocial::getWeChatInfoResult(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (JSON_RESOVE(doc, "errcode") || !doc.IsObject())
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

			CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
			return;
		}

		ThirdPartyLoginInfo userInfo;

		if (JSON_RESOVE(doc, "unionid"))
		{
			userInfo.unionid = doc["unionid"].GetString();
		}

		if (JSON_RESOVE(doc, "nickname"))
		{
			userInfo.nickName = doc["nickname"].GetString();
		}

		if (JSON_RESOVE(doc, "headimgurl"))
		{
			userInfo.headUrl = doc["headimgurl"].GetString();
		}

		if (JSON_RESOVE(doc, "sex"))
		{
			userInfo.bBoy = doc["sex"].GetInt() == 1;
		}

		if (onAuthorizeCallBack)
		{
			onAuthorizeCallBack(true, &userInfo, "授权成功");
			HNHttpRequest::getInstance()->removeObserver(social);
		}
	}

	// 获取QQ信息结果
	void UMengSocial::getQQInfoResult(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (JSON_RESOVE(doc, "errcode") || !doc.IsObject())
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

			CCUMSocialSDK::create()->getPlatformInfo(_authPlatform, auth_selector(doAuthAndGetInfoCallback));
			return;
		}

		ThirdPartyLoginInfo userInfo;

		//QQ登陆拉取的用户信息里没有这个字段，但在授权成功后回调的信息里有，该字段保存在本地
		userInfo.unionid = UserDefault::getInstance()->getStringForKey(QQ_OPEN_ID);

		if (JSON_RESOVE(doc, "nickname"))
		{
			userInfo.nickName = doc["nickname"].GetString();
		}

		if (JSON_RESOVE(doc, "figureurl_qq_2"))
		{
			userInfo.headUrl = doc["figureurl_qq_2"].GetString();
		}
		else if (JSON_RESOVE(doc, "figureurl_qq_1"))
		{
			userInfo.headUrl = doc["figureurl_qq_1"].GetString();
		}

		if (JSON_RESOVE(doc, "sex"))
		{
			userInfo.bBoy = doc["sex"].GetInt() == 1;
		}

		if (onAuthorizeCallBack)
		{
			onAuthorizeCallBack(true, &userInfo, "授权成功");
			HNHttpRequest::getInstance()->removeObserver(social);
		}
	}
}
