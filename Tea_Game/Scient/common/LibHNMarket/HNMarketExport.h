﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNMARKETEXPORT_H__
#define __HNMARKETEXPORT_H__

#include "HNOperator.h"
#include "HNPayCallBack.h"

#endif	//__HNMARKETEXPORT_H__
