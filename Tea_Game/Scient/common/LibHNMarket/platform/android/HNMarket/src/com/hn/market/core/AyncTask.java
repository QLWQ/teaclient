/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd
 
http://www.hotniao.com

All of the content of the software, including code, pictures, 
resources, are original. For unauthorized users, the company 
reserves the right to pursue its legal liability.
****************************************************************************/

package com.hn.market.core;

import com.hn.market.base.Method;


public class AyncTask {
	private Method method;
	private String args;
	private String callBack;

	public AyncTask(Method method, String args, String callBack) {
		this.method = method;
		this.args = args;
		this.callBack = callBack;
	}
	
	public void executeMethod () {
		method.Execute(args, callBack);
	}
}