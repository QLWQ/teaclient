/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd
 
http://www.hotniao.com

All of the content of the software, including code, pictures, 
resources, are original. For unauthorized users, the company 
reserves the right to pursue its legal liability.
****************************************************************************/

package com.hn.market.core;

import android.util.Log;

import com.hn.market.base.Method;
import com.hn.market.base.Module;

public class HNPayModule extends Module {
	private static final String TAG = HNPayModule.class.getSimpleName();
	
	private static HNPayModule payModule = null;

	public static HNPayModule getInstance() {
		if (payModule == null) {
			synchronized (HNPayModule.class) {
				if (payModule == null) {
					payModule = new HNPayModule();
				}
			}
		}
		return payModule;
	}

	public HNPayModule() {
		super("PayModule");
		Register("Pay", new Pay());
		Log.d(TAG, "init systmModule");
	}
	
	public class Pay implements Method {

		@Override
		public String Execute(final String args, String callBack) {
		
			return "";
		}
	}
}
