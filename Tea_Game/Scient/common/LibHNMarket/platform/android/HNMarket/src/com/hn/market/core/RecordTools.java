/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd
 
http://www.hotniao.com

All of the content of the software, including code, pictures, 
resources, are original. For unauthorized users, the company 
reserves the right to pursue its legal liability.
****************************************************************************/

package com.hn.market.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONObject;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.util.Base64;
import android.util.Log;

import com.hn.market.export.GameChannel;

public class RecordTools {
	private MediaRecorder mMediaRecorder;
	private MediaPlayer mMediaPlayer;
	private Context mContext;	

	public static String result = "";
    
    // the path to store record file
	private String mRecordPath;
	
	// the path to store audio from player send
	private String mAudioPath;
	
	private static final String TAG = HNSysModule.class.getSimpleName();
	
	private static RecordTools recordTools;
	
	private int _curVolume = 0;
	
	public static RecordTools getInstance(Context mContext) {
		if (recordTools == null) {
			recordTools = new RecordTools(mContext);
		}
        
		recordTools.mRecordPath = mContext.getApplicationContext().getFilesDir().getAbsolutePath() + "/recordtmp.amr";
		recordTools.mAudioPath = mContext.getApplicationContext().getFilesDir().getAbsolutePath() + "/audiotmp.amr";

		return recordTools;
	}

	public RecordTools(Context mContext) {
		this.mContext = mContext;

		AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		_curVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		
	}
	
	public void startRecoderd() {
		if (mMediaPlayer != null) {
			if(mMediaPlayer.isPlaying()){
				mMediaPlayer.stop();
			}
			
			mMediaPlayer.reset();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		
		if(mMediaRecorder != null){
			mMediaRecorder.reset();
			mMediaRecorder.release();
			mMediaRecorder = null;
		}

		AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		_curVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);

		mMediaRecorder = new MediaRecorder();	
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
		mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		mMediaRecorder.setOutputFile(mRecordPath);
		try {
			mMediaRecorder.prepare();
			mMediaRecorder.start();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void playRecoderd(String data, final String callback) {
		
		try {
			
			JSONObject json = new JSONObject(data);
			
			// audio data
			String audio = json.getString("data");
			
			// audio volume
			double volume = json.getDouble("volume");
			
			// stop and release old media
			if (mMediaPlayer != null) {
				if(mMediaPlayer.isPlaying()){
					mMediaPlayer.stop();

				}
				
				mMediaPlayer.reset();
				mMediaPlayer.release();
				mMediaPlayer = null;
			}

			AudioManager amReset = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);							
			amReset.setStreamVolume(AudioManager.STREAM_MUSIC, _curVolume, 2);
			
			if(volume > 0)
			{
				byte[] buffer = Base64.decode(audio, Base64.DEFAULT);
				onWriteFileByByte(mAudioPath, buffer);
				
				AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
				_curVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
				int playVolume = (int)(_curVolume * volume);
				
				am.setStreamVolume(AudioManager.STREAM_MUSIC, playVolume, 2);
				// apply new media
				mMediaPlayer = new MediaPlayer();
				mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mMediaPlayer.setDataSource(mAudioPath);
				mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
							@Override
							public void onPrepared(MediaPlayer mp) {
								mp.start();
							}
						});
				
				mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

							@Override
							public void onCompletion(MediaPlayer mp) {
								// TODO Auto-generated method stub

								AudioManager amReset = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);							
								amReset.setStreamVolume(AudioManager.STREAM_MUSIC, _curVolume, 2);
								
								// call after play audio end.
								if(!callback.isEmpty()){															
									GameChannel.sendResponseMsg(callback);	
								}
							}
						});
				mMediaPlayer.prepareAsync();
			}
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Stop record audio and return record data.
	 */
	public String stopRecordAndReturn() {
		String data = "";
		File file = new File(mRecordPath);
		try {
			if (mMediaRecorder != null) {
				mMediaRecorder.reset();
			}
			
			byte[] buffer = new byte[(int) file.length()];
			FileInputStream in = new FileInputStream(file);
			in.read(buffer);
			in.close();
			data = Base64.encodeToString(buffer, Base64.DEFAULT);
			file.delete();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			if (mMediaRecorder != null) {
				mMediaRecorder.release();
				mMediaRecorder = null;
			}
			result = data;
			
		}		
		return data;
	}

	public void cancleRecord() {
		try
		{
			result = "";
			if (mMediaRecorder != null) {
				mMediaRecorder.release();
				mMediaRecorder = null;
			}
			File file = new File(mRecordPath);
			if (file.exists()) {
				file.delete();                               
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write buffer to File in path.
	 * 
	 * @param path
	 * @param buffer
	 */
	public static boolean onWriteFileByByte(String path, byte[] buffer) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(path);
			fos.write(buffer);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				fos = null;
			}
		}
		return false;
	}
}
