/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd
 
http://www.hotniao.com

All of the content of the software, including code, pictures, 
resources, are original. For unauthorized users, the company 
reserves the right to pursue its legal liability.
****************************************************************************/

package com.hn.market.base;


public class ResponseArgs
{
	private String callback;
	private String args;

	public ResponseArgs(String callback, String args) {
		this.callback = callback;
		this.args = args;
	}

	public String getCallBack() {
		return this.callback;
	}

	public String getArgs() {
		return this.args;
	}
}
