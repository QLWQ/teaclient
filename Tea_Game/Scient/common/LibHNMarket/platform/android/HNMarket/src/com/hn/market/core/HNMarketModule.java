/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd
 
http://www.hotniao.com

All of the content of the software, including code, pictures, 
resources, are original. For unauthorized users, the company 
reserves the right to pursue its legal liability.
****************************************************************************/

package com.hn.market.core;

import com.hn.framework.HNCommonUtils;
import com.hn.market.base.Method;
import com.hn.market.base.Module;

import android.util.Log;

public class HNMarketModule extends Module {
	private static final String TAG = HNMarketModule.class.getSimpleName();

	public HNMarketModule() {
		super("MarketModule");
		Register("GetISP", new GetISP());
		Log.i(TAG, "Init");
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////////////////

	public class GetISP implements Method {

		@Override
		public String Execute(final String args, String callBack) {
			return HNCommonUtils.getISP(activity);
		}
	}

}
