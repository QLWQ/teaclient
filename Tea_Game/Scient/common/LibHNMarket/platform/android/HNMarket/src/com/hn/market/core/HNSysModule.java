/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd
 
http://www.hotniao.com

All of the content of the software, including code, pictures, 
resources, are original. For unauthorized users, the company 
reserves the right to pursue its legal liability.
****************************************************************************/

package com.hn.market.core;

import java.io.File;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.hn.framework.HNCommonUtils;
import com.hn.market.base.Method;
import com.hn.market.base.Module;
import com.hn.market.export.ChannelExport;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;


import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

public class HNSysModule extends Module {
	private static final String TAG = HNSysModule.class.getSimpleName();
	
	private static HNSysModule sysModule = null;
	
	private String _callback = "";
	
	String shareRoomID = "-1";
	String shareGameID = "-1";

    /** Current network is GPRS */  
    public static final int NETWORK_TYPE_GPRS = 1;  
    /** Current network is EDGE */  
    public static final int NETWORK_TYPE_EDGE = 2;  
    /** Current network is UMTS */  
    public static final int NETWORK_TYPE_UMTS = 3;  
    /** Current network is CDMA: Either IS95A or IS95B */  
    public static final int NETWORK_TYPE_CDMA = 4;  
    /** Current network is EVDO revision 0 */  
    public static final int NETWORK_TYPE_EVDO_0 = 5;  
    /** Current network is EVDO revision A */  
    public static final int NETWORK_TYPE_EVDO_A = 6;  
    /** Current network is 1xRTT */  
    public static final int NETWORK_TYPE_1xRTT = 7;  
    /** Current network is HSDPA */  
    public static final int NETWORK_TYPE_HSDPA = 8;  
    /** Current network is HSUPA */  
    public static final int NETWORK_TYPE_HSUPA = 9;  
    /** Current network is HSPA */  
    public static final int NETWORK_TYPE_HSPA = 10;  
    /** Current network is iDen */  
    public static final int NETWORK_TYPE_IDEN = 11;  
    /** Current network is EVDO revision B */  
    public static final int NETWORK_TYPE_EVDO_B = 12;  
    /** Current network is LTE */  
    public static final int NETWORK_TYPE_LTE = 13;  
    /** Current network is eHRPD */  
    public static final int NETWORK_TYPE_EHRPD = 14;  
    /** Current network is HSPA+ */  
    public static final int NETWORK_TYPE_HSPAP = 15;
    
    
	public static HNSysModule getInstance() {
		if (sysModule == null) {
			synchronized (HNSysModule.class) {
				if (sysModule == null) {
					sysModule = new HNSysModule();
				}
			}
		}
		return sysModule;
	}

	public HNSysModule() {
		super("sysmodule");
		Register("getIMSI", new getIMSI());
		Register("getIMEI", new getIMEI());
		Register("getMNO", new getMNO());
		Register("getVersion", new getVersion());
		Register("getSerialNumber", new getSerialNumber());
		Register("dealPhone", new dealPhone());
		Register("dealMsg", new dealMsg());
		Register("sendEmail", new sendEmail());
		Register("installApp", new installApp());
		Register("getNetIp", new getNetIp());
		Register("isAppInstalled", new isAppInstalled());
        Register("getSdcardPath", new getSdcardPath());
		Register("getBatteryLeave", new getBatteryLeave());//获取电量
		Register("getNetType", new getNetType());//获取网络类型
		Register("getNetLevel", new getNetLevel());//获取信号强度（只能获取wifi）
		Register("setClipboard", new setClipboard());//粘贴
		Register("getClipboard", new getClipboard());//复制
		Register("getShareGameID", new getShareGameID());//获取分享信息中的游戏GameID
		Register("getShareRoomID", new getShareRoomID());//获取分享信息中的游戏RoomID
		Register("clearShareInfo", new clearShareInfo());//清理分享信息
		Log.d(TAG, "init systmModule");
	}
	
	public class getSerialNumber implements Method {
		@Override
		public String Execute(final String args, String callBack) {
			return HNCommonUtils.getSerialNumber(activity);
		}
	}

	public class getVersion implements Method {

		@Override
		public String Execute(final String args, String callBack) {
			return HNCommonUtils.getGameVersion(activity);
		}
	}

	public class getIMSI implements Method {

		@Override
		public String Execute(final String args, String callBack) {
			return HNCommonUtils.getIMSI(activity);
		}
	}

	public class getIMEI implements Method {

		@Override
		public String Execute(final String args, String callBack) {
			return HNCommonUtils.getIMEI(activity);
		}
	}

	public class getMNO implements Method {

		@Override
		public String Execute(final String args, String callBack) {
			return HNCommonUtils.getISP(activity);
		}
	}
	
	public class dealPhone implements Method {

		@Override
		public String Execute(final String args, String callBack) {
			JSONObject json;
			try {
				json = new JSONObject(args);
				String number = json.getString("number");
				HNCommonUtils.dealPhone(activity, number);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "1";
		}
	}
	
	public class dealMsg implements Method {

		@Override
		public String Execute(final String args, String callBack) {
			
			JSONObject json;
			try {
				json = new JSONObject(args);
				String number = json.getString("number");
				HNCommonUtils.dealMsg(activity, number);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return "1";
		}
	}
	
	public class sendEmail implements Method {
		
		@Override
		public String Execute(final String args, String callBack) {
			
			JSONObject json;
			try {
				json = new JSONObject(args);
				String to = json.getString("to");
				String text = json.getString("text");
				HNCommonUtils.sendEmail(activity, to, text);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			return "1";
		}
	}
	
	public class installApp implements Method{

		@Override
		public String Execute(String args, String callBack) {
			// TODO Auto-generated method stub
			HNCommonUtils.installApp(activity, args);
			return "";
		}
		
	}
	
	public class getNetIp implements Method {
		public String Execute(final String args, String callBack) {
			return HNCommonUtils.getNetIp(activity);
		}
	}
	
	public class isAppInstalled implements Method {
		public String Execute(final String args, String callBack) {
			
			final PackageManager packageManager = activity.getPackageManager();// 获取packagemanager
	        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
	        if (pinfo != null) {
	            for (int i = 0; i < pinfo.size(); i++) {
	                String pn = pinfo.get(i).packageName;
	                //Log.d(TAG, String.format("packageName:%s", pn));
	                if (args.equals("1") && pn.equals("com.tencent.mm")) {
	                	return "true";
	                }
	                if (args.equals("3") && pn.equals("com.tencent.mobileqq")) {
	                    return "true"; 
	                }
	            }
	        }

	        return "false";
		}
	}
    
    /*
     * get sd card path.
     */
    public class getSdcardPath implements Method {
		public String Execute(final String args, String callBack) {
			
			String directoryPath="";
			if (Environment.getExternalStorageState().equals(
	                Environment.MEDIA_MOUNTED)) {// 优先保存到SD卡中  
				directoryPath = activity.getExternalFilesDir("HNLog").getAbsolutePath()
						+ File.separator; 
	        } else {// 如果SD卡不存在，就保存到本应用的目录下  
	        	directoryPath = activity.getFilesDir().getAbsolutePath()
	                    + File.separator; 
	        	}

			return directoryPath;
		}
	}
    
    public class getBatteryLeave implements Method {
    	@Override
		public String Execute(final String args, String callBack) {
			_callback = callBack;
			
			Intent intent = activity.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
			
			int rawlevel = intent.getIntExtra("level", -1);	//获得当前电量
			int scale = intent.getIntExtra("scale", -1);	//获得总电量
			int level = -1;
			if(rawlevel>=0 && scale>0){
				level = (rawlevel*100)/scale;
			}
			
			return String.valueOf(level);
		}
	}
	
	public class getNetType implements Method {
		@Override
		public String Execute(final String args, String callBack) {  
			_callback = callBack;
			
			String strNetworkType = "";
		    
		    NetworkInfo networkInfo = ((ConnectivityManager) activity.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();  
		    if (networkInfo != null && networkInfo.isConnected())
		    {
		        if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
		        {
		            strNetworkType = "WIFI";
		        }
		        else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
		        {
		            String _strSubTypeName = networkInfo.getSubtypeName();
		            
		            Log.d("cocos2d-x", "Network getSubtypeName : " + _strSubTypeName);
		            
		            // TD-SCDMA   networkType is 17
		            int networkType = networkInfo.getSubtype();
		            switch (networkType) {
		                case TelephonyManager.NETWORK_TYPE_GPRS:
		                case TelephonyManager.NETWORK_TYPE_EDGE:
		                case TelephonyManager.NETWORK_TYPE_CDMA:
		                case TelephonyManager.NETWORK_TYPE_1xRTT:
		                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
		                    strNetworkType = "2G";
		                    break;
		                case TelephonyManager.NETWORK_TYPE_UMTS:
		                case TelephonyManager.NETWORK_TYPE_EVDO_0:
		                case TelephonyManager.NETWORK_TYPE_EVDO_A:
		                case TelephonyManager.NETWORK_TYPE_HSDPA:
		                case TelephonyManager.NETWORK_TYPE_HSUPA:
		                case TelephonyManager.NETWORK_TYPE_HSPA:
		                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
		                case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
		                case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
		                    strNetworkType = "3G";
		                    break;
		                case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
		                    strNetworkType = "4G";
		                    break;
		                default:
		                    // 中国移动 联通 电信 三种3G制式
		                    if (_strSubTypeName.equalsIgnoreCase("TD-SCDMA") 
		                    		|| _strSubTypeName.equalsIgnoreCase("WCDMA") 
		                    		|| _strSubTypeName.equalsIgnoreCase("CDMA2000")) 
		                    {
		                        strNetworkType = "3G";
		                    }
		                    else
		                    {
		                        strNetworkType = "NONE";
		                    }
		                    
		                    break;
		             }
		        }
		        else
		        	strNetworkType = "NONE";
		    }
		    
		    Log.d("cocos2d-x", "Network Type : " + strNetworkType);
		    
		    return strNetworkType;
		}
	}
	
	public class getNetLevel implements Method {
		@Override
		public String Execute(String args, String callBack) {
			
			try {
		        WifiManager wifiMg = (WifiManager) activity.getSystemService(activity.WIFI_SERVICE);
		            WifiInfo wifiInfo = wifiMg.getConnectionInfo();
		            //int nWSig = wifiInfo.getRssi();
		            int nWSig = wifiMg.calculateSignalLevel(wifiInfo.getRssi(), 4);
		            Log.d(TAG, "...liuniannian New WifiLevel");
		            Log.d(TAG, String.valueOf(nWSig));
		            return String.valueOf(nWSig);
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		  return "";
		}
	}
	
	public class setClipboard implements Method{
		public String Execute(final String args, String callBack)
		{
			if(callBack != "") {
				ChannelExport.getInstance().executeAyncMethod(callBack,"ok");
			}
			HNCommonUtils.SetStringToClipboard(activity, args);
			
			return "1";
		}
	}
	
	public class getClipboard implements Method{
		public String Execute(final String args, String callBack)
		{
			return HNCommonUtils.GetStringToClipboard(activity);
		}
	}
	
	public class getShareGameID implements Method {
		public String Execute(final String args, String callBack) {
			return shareGameID;
		}
	}
	
	public class getShareRoomID implements Method {
		public String Execute(final String args, String callBack) {  
			return shareRoomID;
		}
	}
	
	public class clearShareInfo implements Method {
		public String Execute(final String args, String callBack) {
			shareRoomID = "-1";
			shareGameID = "-1";
			 return "-1";
		}
	}
	public void getShareInfo(String gameID, String roomID) {
		shareRoomID = roomID;
		shareGameID = gameID;
	}
}
