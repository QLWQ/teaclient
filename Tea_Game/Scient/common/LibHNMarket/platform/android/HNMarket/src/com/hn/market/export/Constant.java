/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd
 
http://www.hotniao.com

All of the content of the software, including code, pictures, 
resources, are original. For unauthorized users, the company 
reserves the right to pursue its legal liability.
****************************************************************************/

package com.hn.market.export;

public class Constant {
	public static final int COMMOAND_PAY = 1000;
	public static final int COMMOAND_PAY_RESULT = 1001;
	public static final int COMMAND_RESPONSECHANNEL = 10000;
	
	public static final int SDK_TYPE_WECHAT = 0;
	public static final int SDK_TYPE_ALIPAY = 1;
	public static final int SDK_TYPE_ZHIFU = 2;
	public static final int SDK_TYPE_JUBAOYUN = 3;
	public static final int SDK_TYPE_YINLIAN = 4;

	public class PAY_RESULT {
		int status;
	}
}
