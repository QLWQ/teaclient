﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/
#include "HNMarketModule.h"

#define MODULE_NAME     "marketmodule"

namespace HN
{
    create_impl(MarketModule);
    
    MarketModule::MarketModule() : Module(MODULE_NAME)
    {
		resisterMethod("evaluate", HN_SELECTOR(MarketModule::evaluate));
		resisterMethod("webpageRedirect", HN_SELECTOR(MarketModule::webpageRedirect));
    }
    
    MarketModule::~MarketModule()
    {
    }
    
    std::string MarketModule::evaluate(const std::string& args)
    {
        return "";
    }

	std::string MarketModule::webpageRedirect(const std::string& args)
	{
		return "";
	}
}