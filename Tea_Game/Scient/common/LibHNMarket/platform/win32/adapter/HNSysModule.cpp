﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNSysModule.h"
#include <windows.h> 
#include <conio.h>
#include <direct.h>

#define MODULE_NAME     "sysmodule"

////////////////////////////////////////////////////////////////////////////////////////////////////
namespace HN
{
	create_impl(SysModule)

    SysModule::SysModule() : Module(MODULE_NAME)
    {
		resisterMethod("getBuildVersion", HN_SELECTOR(SysModule::getBuildVersion));
		resisterMethod("getVersion",      HN_SELECTOR(SysModule::getVersion));
		resisterMethod("getOSVersion",    HN_SELECTOR(SysModule::getOSVersion));
		resisterMethod("getDevice",       HN_SELECTOR(SysModule::getDevice));
		resisterMethod("getIMEI",         HN_SELECTOR(SysModule::getIMEI));
		resisterMethod("getSerialNumber", HN_SELECTOR(SysModule::getSerialNumber));
		resisterMethod("installApp",      HN_SELECTOR(SysModule::installApp));
		resisterMethod("getNetIp",        HN_SELECTOR(SysModule::getNetIp));
		resisterMethod("isAppInstalled",  HN_SELECTOR(SysModule::isAppInstalled));
        resisterMethod("getSdcardPath",   HN_SELECTOR(SysModule::getSdcardPath));
		resisterMethod("SetClipboard", HN_SELECTOR(SysModule::SetClipboard));
		resisterMethod("GetClipboard", HN_SELECTOR(SysModule::GetClipboard));
    }
    
    SysModule::~SysModule()
    {
    }
    
    std::string SysModule::getVersion(const std::string& args)
    {
        return "3.1.8R01101L";
    }
    
    std::string SysModule::getBuildVersion(const std::string& args)
    {
        return "3.1.8";
    }
    
    std::string SysModule::getOSVersion(const std::string& args)
    {
         return "win32";
    }
    
    std::string SysModule::getDevice(const std::string& args)
    {
        return "win32";
    }

    std::string SysModule::getIMEI(const std::string& args)
    {
		GUID uuid; 
		CoCreateGuid(&uuid); 
		// Spit the address out 
		char mac_addr[18]; 
		sprintf(mac_addr,"%02X:%02X:%02X:%02X:%02X:%02X", 
			uuid.Data4[2],uuid.Data4[3],uuid.Data4[4], 
			uuid.Data4[5],uuid.Data4[6],uuid.Data4[7]);
        return mac_addr;
    }

	std::string SysModule::getSerialNumber(const std::string& args)
	{
		char	volumeInfo[100];
		DWORD   volSerialNumber;
		DWORD   volMaxComponentLength;
		DWORD   volFileSystemFlags;
		char    fileSystemNameBuffer[100];
		GetVolumeInformationA("c:\\", volumeInfo, sizeof(volumeInfo), &volSerialNumber,
			&volMaxComponentLength, &volFileSystemFlags, fileSystemNameBuffer, sizeof(fileSystemNameBuffer));
	
		char str[64];
		sprintf(str, "%u", volSerialNumber);
		return str;

	}
    
    std::string SysModule::getISP(const std::string& args)
    {
        return "win32";
    }

	std::string SysModule::installApp(const std::string& args)
	{
		return "win32-installApp";
	}

	std::string SysModule::getNetIp(const std::string& args)
	{ 
		WSADATA wsaData;  
		int ret = WSAStartup(MAKEWORD(2,2),&wsaData);  
		if (0 != ret)  
		{  
			return "";  
		}  

		char hostname[256] = {0};  
		ret = gethostname(hostname,sizeof(hostname));  
		if (SOCKET_ERROR == ret)  
		{  
			return "";  
		}  

		HOSTENT* host = gethostbyname(hostname);  
		if (nullptr == host)  
		{  
			return "";  
		}  

		char ip[50] = {0};
		strcpy(ip, inet_ntoa(*(in_addr*)*host->h_addr_list));  
		return std::string(ip);  
	}
	
	std::string SysModule::isAppInstalled(const std::string& args)
	{
		return "false";
	}

	std::string SysModule::getSdcardPath(const std::string& args)
	{
		char buffer[MAX_PATH] = { 0 };
		_getcwd(buffer, MAX_PATH);
		std::string ret(buffer);
		int len = ret.length();
		for (int i = 0; i < len; ++i)
		{
			if (ret[i] == '\\')
			{
				ret[i] = '/';
			}
		}
		char ch = ret.at(ret.length() - 1);
		if (ch != '\\' && ch != '/')
		{
			ret.append("/");
		}
		return ret;
	}
	
	std::string SysModule::GetClipboard(const std::string& args)
	{
		return std::string("copy");
	}

	std::string SysModule::SetClipboard(const std::string& args)
	{
		return std::string("copy");
	}
}

