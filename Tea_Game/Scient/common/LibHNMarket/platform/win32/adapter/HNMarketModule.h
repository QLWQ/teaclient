﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNMARKETMODULE_H__
#define __HNMARKETMODULE_H__

#include "../../../HNModule.h"
#include "../../../HNPayCallBack.h"

namespace HN
{
    class MarketModule : public Module
    {
		create_declare(MarketModule)

    private:
        // 评价接口
        std::string evaluate(const std::string& args);
		// 评价接口
		std::string webpageRedirect(const std::string& args);
    private:
        MarketModule();
        
    public:
        ~MarketModule();
       
    };
}

#endif	//__HNMARKETMODULE_H__