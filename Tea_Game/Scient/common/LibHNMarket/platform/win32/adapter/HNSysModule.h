﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNSYSMODULE_H__
#define __HNSYSMODULE_H__

#include "../../../HNModule.h"
#include "../../../HNPayCallBack.h"

namespace HN
{
    class SysModule : public Module
    {
		create_declare(SysModule)

    private:
        SysModule();
        
    public:
        virtual ~SysModule();

    private:
        std::string getBuildVersion(const std::string& args);
        std::string getVersion(const std::string& args);
        std::string getOSVersion(const std::string& args);
        std::string getDevice(const std::string& args);
        std::string getIMEI(const std::string& args);
        std::string getISP(const std::string& args);
        
        // 获取设备序列号
		std::string getSerialNumber(const std::string& args);
        
        // 安装应用
		std::string installApp(const std::string& args);
		
        // 应用是否安装
        std::string isAppInstalled(const std::string& args);

		// 获取SD卡路劲
		std::string getSdcardPath(const std::string& args);
		
		// 获取本机ip地址
		std::string getNetIp(const std::string& args);
		
		//没有支持Win32的剪切板功能
		std::string SetClipboard(const std::string& args);

		std::string GetClipboard(const std::string& args);
    };
}

#endif	//__HNSYSMODULE_H__