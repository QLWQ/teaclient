﻿#ifndef __HNSDK__SysModule__
#define __HNSDK__SysModule__

#include "HNModule.h"
#include "HNPayCallBack.h"

namespace HN
{
    class SysModule : public Module
    {
		create_declare(SysModule)
        
    private:
        SysModule();
        
    public:
        virtual ~SysModule();
      
    private:
        std::string getBuildVersion(const std::string& args = "");
        std::string getVersion(const std::string& args = "");
        std::string getOSVersion(const std::string& args = "");
        std::string getDevice(const std::string& args = "");
        std::string getIMEI(const std::string& args = "");
        
        // 获取设备序列号
        std::string getSerialNumber(const std::string& args = "");
        std::string dealPhone(const std::string& args);
        std::string dealMsg(const std::string& args);
        
        // 安装应用
		std::string installApp(const std::string& args);
        
        // 应用是否安装
		std::string isAppInstalled(const std::string& args);
		
		// 获取本机ip地址
		std::string getNetIp(const std::string& args);
        
        // 发送邮件
        std::string sendEmail(const std::string& args);
        
        // 获取SD路径
        std::string getSdcardPath(const std::string& args);
		
        // 获取电池电量
        std::string getBatteryLevel(const std::string& args);
        
        //获取网络信号
        std::string getNetLevel(const std::string& args);
		
		//获取网络类型
        std::string getNetType(const std::string& args);
		
		//设置剪切板（复制）
		std::string SetClipboard(const std::string& args, CALLBACK_PRAGMA* callback);
		
		//获取剪切板（粘贴）
		std::string GetClipboard(const std::string& args);
    };
}

#endif      //__HNSDK__SysModule__
