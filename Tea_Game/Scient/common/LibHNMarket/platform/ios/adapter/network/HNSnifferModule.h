//Copyright (c) 2012—2015 Beijing TianRuiDiAn Network Technology Co,Ltd. All rights reserved

#ifndef __HNSnifferModule__
#define __HNSnifferModule__

#include "HNModule.h"
#import "oc/HNSnifferAdapter_IMPL.h"

namespace HN
{
    class HNSnifferModule
    {
        create_declare(HNSnifferModule)
        
    public:
        HNSnifferModule();
        
        void Start();
        
    private:
        ~HNSnifferModule();
        
    private:
        HNSnifferAdapter_IMPL *_impl;
    };
}

#endif //__HNLocationModule__
