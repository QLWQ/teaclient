//Copyright (c) 2012—2015 Beijing TianRuiDiAn Network Technology Co,Ltd. All rights reserved



#include "HNSnifferModule.h"


namespace HN
{
    
    create_impl(HNSnifferModule)
    
    HNSnifferModule::HNSnifferModule()
    {
        _impl = [[HNSnifferAdapter_IMPL alloc] init];
    }

    HNSnifferModule::~HNSnifferModule()
    {
        [_impl release];
        _impl = nil;
    }
    
    void HNSnifferModule::Start()
    {
        [_impl networkChange];
    }
}
