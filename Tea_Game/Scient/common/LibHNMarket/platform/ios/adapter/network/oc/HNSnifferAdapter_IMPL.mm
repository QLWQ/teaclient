//Copyright (c) 2012—2015 Beijing TianRuiDiAn Network Technology Co,Ltd. All rights reserved



#import "HNSnifferAdapter_IMPL.h"
#import "Reachability.h"
#include "HNNetExport.h"

@interface HNSnifferAdapter_IMPL ()

@property (nonatomic, strong) Reachability *reachability;

@end

@interface HNSnifferAdapter_IMPL()
{
   
}
@end

@implementation HNSnifferAdapter_IMPL

- (void)dealloc
{
    [super dealloc];
}

-(id)init
{
    if (self = [super init]) {
        return self;
    }
    return nil;
}


// 监听网络变化
- (void)networkChange
{
    // 开启网络状况的监听通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    // 监听的链接
    self.reachability = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    // 开始监听, 会启动一个Runloop
    [self.reachability startNotifier];
}

// 网络链接改变时会调用的方法
- (void)reachabilityChanged:(NSNotification *)note
{
    if (PlatformConfig::getInstance()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER) return;
    
    // 通过通知对象获取被监听的Reachability对象
    Reachability *currReach = [note object];
    NSAssert([currReach isKindOfClass:[Reachability class]], @"非Reachability类");
    
    //对连接改变做出响应处理动作
    NetworkStatus status = [currReach currentReachabilityStatus];
    
    //如果没有连接到网络就提醒实况
    if(status == NotReachable) {
        
        log("网络连接异常");
        
        Director::getInstance()->getRunningScene()->stopActionByTag(777);
        
        auto action = Sequence::create(DelayTime::create(2.0f), CallFunc::create([=](){
            
            EventCustom event(DISCONNECT);
            Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
        }),  nullptr);
        
        action->setTag(777);
        
        Director::getInstance()->getRunningScene()->runAction(action);
        
    } else {
        
        log("网络连接正常");
        
        Director::getInstance()->getRunningScene()->stopActionByTag(777);
    }
}

@end
