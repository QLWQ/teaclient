//Copyright (c) 2012—2015 Beijing TianRuiDiAn Network Technology Co,Ltd. All rights reserved

#include "HNModule.h"
#import <UIKit/UIKit.h>

@interface HNSnifferAdapter_IMPL : NSObject
{
    
}

- (id)init;

// 监听网络变化
- (void)networkChange;

// 网络链接改变时会调用的方法
- (void)reachabilityChanged:(NSNotification *)note;

@end
