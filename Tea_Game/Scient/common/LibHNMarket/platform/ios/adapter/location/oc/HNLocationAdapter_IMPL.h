//Copyright (c) 2012—2015 Beijing TianRuiDiAn Network Technology Co,Ltd. All rights reserved

#include "HNModule.h"
#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Base/BMKBaseComponent.h>

@interface HNLocationAdapter_IMPL : NSObject<BMKLocationServiceDelegate, BMKGeneralDelegate>
{
    CALLBACK_PRAGMA _callback;
    BMKLocationService* _locService;
    std::string _key;
}

- (id)init : (std::string) key;

- (void) locationAddress : (CALLBACK_PRAGMA*) callback;
@end
