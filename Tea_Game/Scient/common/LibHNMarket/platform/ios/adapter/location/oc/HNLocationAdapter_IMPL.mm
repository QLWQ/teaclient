//Copyright (c) 2012—2015 Beijing TianRuiDiAn Network Technology Co,Ltd. All rights reserved



#import "HNLocationAdapter_IMPL.h"
#import "platform/ios/common/JSONKit/JSONKit.h"
#import "platform/ios/HNMarket.h"
#import "platform/external/json/json.h"
#import "platform/ios/oc-marco.h"

#import<CoreLocation/CoreLocation.h>
@interface HNLocationAdapter_IMPL ()<CLLocationManagerDelegate>

@property (nonatomic,strong) CLLocationManager* locationManager;
@property (nonatomic,assign) CLLocationCoordinate2D  coor;
@end

@interface HNLocationAdapter_IMPL()
{
   
}
@end

@implementation HNLocationAdapter_IMPL
- (void)dealloc
{
    _locService = nil;
    _locService.delegate = nil;
    [super dealloc];
}

-(id)init : (std::string) key
{
    if (self = [super init]) {
        _key = key;
    }
    return self;
}

-  (void)locationAddress : (CALLBACK_PRAGMA*) callback
{
    _callback = *callback;
    
    [self startLocation];
}

//普通态定位
-(IBAction)startLocation
{
    NSLog(@"进入普通定位态");
    
    if (nil == _locService) {
        
        BOOL ret = [[[BMKMapManager alloc]init] start:[NSString stringWithUTF8String:_key.c_str()] generalDelegate:self];
        if (ret) {
            _locService = [[BMKLocationService alloc]init];
            _locService.delegate = self;
        }
        else
        {
            NSLog(@"BMKMapManager start failed!");
            
            std::string resp = "{\"success\":false}";
            HN::Market::sharedMarket()->responseChannel(&_callback, resp);
            return;
        }
    }
    
    // 检查有没有定位权限
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        
        std::string resp = "{\"success\":false}";
        
        HN::Market::sharedMarket()->responseChannel(&_callback, resp);
    }
    else
    {
        [_locService startUserLocationService];
    }
}

//停止定位
-(IBAction)stopLocation
{
    [_locService stopUserLocationService];
}

/**
 *在地图View将要启动定位时，会调用此函数
 *@param mapView 地图View
 */
- (void)willStartLocatingUser
{
    NSLog(@"start locate");
}

/**
 *用户方向更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation
{
    NSLog(@"heading is %@",userLocation.heading);
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    NSLog(@"didUpdateUserLocation lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
    
    NSString* latitude = [NSString stringWithFormat:@"%f", userLocation.location.coordinate.latitude];
    NSString* longitude = [NSString stringWithFormat:@"%f", userLocation.location.coordinate.longitude];
    
    std::string weidu = [latitude UTF8String];
    std::string jingdu = [longitude UTF8String];
    
    std::string resp = "{\"success\":true,\"latitude\":\""+ weidu + "\"," +
    "\"lontitude\":\"" + jingdu + "\"}";
    
    HN::Market::sharedMarket()->responseChannel(&_callback, resp);
    // 停止定位
    [self stopLocation];
}

/**
 *在地图View停止定位后，会调用此函数
 *@param mapView 地图View
 */
- (void)didStopLocatingUser
{
    NSLog(@"stop locate");
}

/**
 *定位失败后，会调用此函数
 *@param mapView 地图View
 *@param error 错误号，参考CLError.h中定义的错误号
 */
- (void)didFailToLocateUserWithError:(NSError *)error
{
    NSLog(@"location error");
}



///////////////////////////////////////////以下代码是调用系统原生api定位，精度不够//////////////////////////////////////////////////
/*
 -(id)init
 {
 if (self = [super init]) {
 
 // 初始化定位管理
 self.locationManager=[[CLLocationManager alloc]init];
 
 // 设置代理
 self.locationManager.delegate=self;
 
 // 设置定位的精准度
 self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
 
 // 设置设备移动更新位置信息的最小距离，单位是米
 self.locationManager.distanceFilter=kCLDistanceFilterNone;
 
 // 请求授权
 if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
 // 请求总是允许使用定位功能
 [self.locationManager requestAlwaysAuthorization];
 
 // 请求应用使用时才允许使用定位功能
 //            [self.locationManager requestWhenInUseAuthorization];
 }
}
return self;
}
 
-  (void)locationAddress : (CALLBACK_PRAGMA*) callback
{
    _callback = *callback;
    
    if ([CLLocationManager locationServicesEnabled] &&
        ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways
         || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)) {
            
            // 开始定位
            [self.locationManager startUpdatingLocation];
            
        }else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
            
            UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"提示" message:@"定位失败，请在[设置]中打开定位服务或者检查网络!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
            [al show];
        }
    else
    {
        NSLog(@"定位失败");
        
        std::string resp = "{\"success\":false,\"}";
        
        HN::Market::sharedMarket()->responseChannel(&_callback, resp);
    }
}

-  (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *locationXY=[locations lastObject];
    self.coor =locationXY.coordinate;
    NSString   *latitude   =  [NSString stringWithFormat:@"%f",self.coor.latitude];
    NSString   *longitude = [NSString stringWithFormat:@"%f",self.coor.longitude];
    NSLog(@"定位获取坐标:%@,%@",latitude,longitude);
    
    std::string weidu = [latitude UTF8String];
    std::string jingdu = [longitude UTF8String];
    
    std::string resp = "{\"success\":true,\"latitude\":\""+ weidu + "\"," +
    "\"lontitude\":\"" + jingdu + "\"}";

    HN::Market::sharedMarket()->responseChannel(&_callback, resp);
    // 停止定位
    [self.locationManager stopUpdatingLocation];
    
}

-  (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"定位失败");
    
    std::string resp = "{\"success\":false,\"}";
    
    HN::Market::sharedMarket()->responseChannel(&_callback, resp);
}
 
 */

@end
