#import "HNSysModule.h"
#import "HNUtility.h"
#import "../../external/json/json.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include "network/oc/Reachability.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#include "../HNMarket.h"

#define MODULE_NAME     "sysmodule"

////////////////////////////////////////////////////////////////////////////////////////////////////
namespace HN
{
    create_impl(SysModule)
    
    SysModule::SysModule() : Module(MODULE_NAME)
    {
        resisterMethod("getbuildversion",   HN_SELECTOR(SysModule::getBuildVersion));
        resisterMethod("getversion",        HN_SELECTOR(SysModule::getVersion));
        resisterMethod("getosversion",      HN_SELECTOR(SysModule::getOSVersion));
        resisterMethod("getdevice",         HN_SELECTOR(SysModule::getDevice));
        resisterMethod("getimei",           HN_SELECTOR(SysModule::getIMEI));
        resisterMethod("dealPhone",         HN_SELECTOR(SysModule::dealPhone));
        resisterMethod("dealMsg",           HN_SELECTOR(SysModule::dealMsg));
        resisterMethod("getSerialNumber",   HN_SELECTOR(SysModule::getSerialNumber));
		resisterMethod("installApp",        HN_SELECTOR(SysModule::installApp));
		resisterMethod("getNetIp",          HN_SELECTOR(SysModule::getNetIp));
        resisterMethod("sendEmail",         HN_SELECTOR(SysModule::sendEmail));
		resisterMethod("isAppInstalled",    HN_SELECTOR(SysModule::isAppInstalled));
        resisterMethod("getSdcardPath",     HN_SELECTOR(SysModule::getSdcardPath));
        resisterMethod("getBatteryLeave",   HN_SELECTOR(SysModule::getBatteryLevel));
        resisterMethod("getNetLevel",       HN_SELECTOR(SysModule::getNetLevel));
		resisterMethod("getNetType",        HN_SELECTOR(SysModule::getNetType));
		registerAyncMethod("SetClipboard",  HN_CALLFUNC_SELECTOR(SysModule::SetClipboard));
        resisterMethod("GetClipboard",      HN_SELECTOR(SysModule::GetClipboard));
    }
    
    SysModule::~SysModule()
    {
    }

    std::string SysModule::getVersion(const std::string& args)
    {
        NSString* gameVersion = [HNUtility getVersion];
        return [gameVersion UTF8String];
    }
    
    std::string SysModule::getBuildVersion(const std::string& args)
    {
        NSString* gameVersion = [HNUtility getBuildVersion];
        return [gameVersion UTF8String];
    }
    
    std::string SysModule::getOSVersion(const std::string& args)
    {
        NSString* osVersion = [HNUtility getOSVersion];
        return [osVersion UTF8String];
    }
    
    std::string SysModule::getDevice(const std::string& args)
    {
        NSString* device = [HNUtility getDevice];
        return [device UTF8String];
    }

    std::string SysModule::getIMEI(const std::string& args)
    {
        NSString* imei = [HNUtility getIMEI];
        return [imei UTF8String];
    }
    
    std::string SysModule::getSerialNumber(const std::string& args)
    {
        return getIMEI(args);
    }
    
    std::string SysModule::dealPhone(const std::string& args)
    {
        Json::Reader jsReader;
        Json::Value JsonRoot;
        if (jsReader.parse(args, JsonRoot))
        {
            std::string number = JsonRoot["number"].asString();
            [HNUtility dealPhone:[NSString stringWithUTF8String:number.c_str()]];
        }
        return "1";
    }
    
    std::string SysModule::dealMsg(const std::string& args)
    {
        Json::Reader jsReader;
        Json::Value JsonRoot;
        if (jsReader.parse(args, JsonRoot))
        {
            std::string number = JsonRoot["number"].asString();
            [HNUtility dealMsg:[NSString stringWithUTF8String:number.c_str()]];
        }
        return "1";
    }
	
	std::string SysModule::installApp(const std::string& args)
	{
		//Application::getInstance()->openURL(args);
		return "ios-installApp";
	}
	
	std::string SysModule::getNetIp(const std::string& args)
	{
        NSString *address = @"error";
        struct ifaddrs *interfaces = nullptr;
        struct ifaddrs *temp_addr  = nullptr;
        int success = 0;
        
        // retrieve the current interfaces - returns 0 on success
        success = getifaddrs(&interfaces);
        if (success == 0) {
            // Loop through linked list of interfaces
            temp_addr = interfaces;
            while (temp_addr != nullptr) {
                if( temp_addr->ifa_addr->sa_family == AF_INET) {
                    // Check if interface is en0 which is the wifi connection on the iPhone
                    if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                        // Get NSString from C String
                        address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    }
                }
                
                temp_addr = temp_addr->ifa_next;
            }
        }
        
        // Free memory
        freeifaddrs(interfaces);
        
        return [address UTF8String];
	}
    
    std::string SysModule::sendEmail(const std::string& args)
    {
        Json::Reader jsReader;
        Json::Value JsonRoot;
        if (jsReader.parse(args, JsonRoot))
        {
            std::string to   = JsonRoot["to"].asString();
            std::string text = JsonRoot["text"].asString();
            
            NSString* strTo   = [NSString stringWithUTF8String:to.c_str()];
            NSString* strText = [NSString stringWithUTF8String:text.c_str()];

           [HNUtility sendEmail:strTo text:strText];
        }
        
        return "";
    }
	
	std::string SysModule::isAppInstalled(const std::string& args)
    {		
		if (args.compare("1") == 0 && [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]])
		{
			return "true";
		}
        
		if (args.compare("3") == 0 && [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]])
		{
			return "true";
		}
		
        return "false";
    }
    
    std::string SysModule::getSdcardPath(const std::string& args)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDir = [paths objectAtIndex:0];
         return [docDir UTF8String];
    }
	 // 获取电池电量
    std::string SysModule::getBatteryLevel(const std::string& args)
    {
        [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
        int level = [[UIDevice currentDevice] batteryLevel] * 100;
        
        return std::to_string(level);
    }
    
    //获取网络信号
    std::string SysModule::getNetLevel(const std::string& args)
    {
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *subviews  =[[[[app valueForKey:@"_statusBar"]subviews]lastObject]subviews];
        id dataNetworkItemView = nil;
        for (id subview in subviews) {
            if ([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
                dataNetworkItemView = subview;
                break;
            }
        }
        
        //网络类型
        //int _dataNetworkType = [[dataNetworkItemView valueForKey:@"_dataNetworkType"] intValue];
        //信号强度
        //int _wifiStrengthRaw = [[dataNetworkItemView valueForKey:@"_wifiStrengthRaw"] intValue];
        //信号线数
        int _wifiStrengthBars = [[dataNetworkItemView valueForKey:@"_wifiStrengthBars"] intValue];
       
        return std::to_string(_wifiStrengthBars);
    }
    //获取网络类型
    std::string SysModule::getNetType(const std::string& args)
    {
		/*
        UIApplication *app = [UIApplication sharedApplication];
        NSArray* subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
        id dataNetworkItemView = nil;
        for (id subview in subviews) {
            if ([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
                dataNetworkItemView = subview;
                break;
            }
        }
        
        //网络类型
        int networkType = [[dataNetworkItemView valueForKey:@"_dataNetworkType"] intValue];
        
        std::string netType = "";
        switch (networkType) {
            case 0:
                netType = "NONE";
                break;
            case 1:
                netType = "2G";
                break;
            case 2:
                netType = "3G";
                break;
            case 3:
                netType = "4G";
                break;
            case 5:
                netType = "WIFI";
                break;
            default:
                break;
        }
        return netType;
		*/
		
		std::string netType = "";
        
        Reachability *reach = [Reachability reachabilityWithHostName:@"www.baidu.com"];
        switch ([reach currentReachabilityStatus]) {
            case NotReachable:// 没有网络
            {
                
                netType = "NONE";
            }break;            
            case ReachableViaWiFi:// Wifi
            {
                netType = "WIFI";
            }break;               
            case ReachableViaWWAN:// 手机自带网络
            {
                // 获取手机网络类型
                CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
                
                NSString *currentStatus = info.currentRadioAccessTechnology;
                
                if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyGPRS"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMA1x"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyEdge"]) {
                    
                    netType = "2G";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyWCDMA"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyHSDPA"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyHSUPA"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORev0"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevA"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevB"]
				|| [currentStatus isEqualToString:@"CTRadioAccessTechnologyeHRPD"]){
                    
                    netType = "3G";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyLTE"]){
                    
                    netType = "4G";
                }
            }break;   
            default:
                break;
        }
		
        return netType;
    }
	
	// 设置剪切板
	std::string SysModule::SetClipboard(const std::string& args, CALLBACK_PRAGMA* callback)
	{
		
        
        std::string text = args;
			
        [HNUtility setPasteboard:[NSString stringWithUTF8String:text.c_str()]];
        
        HN::Market::sharedMarket()->responseChannel(callback, "yes");
		
        return "1";
	}
		
	//获取剪切板
	std::string SysModule::GetClipboard(const std::string& args)
	{
		NSString* str = [HNUtility getPasteboard];
        return [str UTF8String];
	}
}

