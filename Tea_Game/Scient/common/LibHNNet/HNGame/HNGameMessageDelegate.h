﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_GameMessageDelegate_H__
#define __HN_GameMessageDelegate_H__

#include "HNBaseType.h"
#include "HNNetProtocol/HNProtocolExport.h"
#include <vector>

namespace HN
{
	
	class IGameMessageDelegate	: public IGameChartMessageDelegate
								, public IUserActionMessageDelegate
	{
	public:
		// 用户同意
		virtual void I_R_M_UserAgree(MSG_GR_R_UserAgree* agree){}

		// 玩家信息变化消息
		virtual void I_R_M_UserInfoChange(const UserInfoStruct* data){}

		// 玩家金币不足消息
		virtual void I_R_M_UserNotEnoughMoney() {}

		// 游戏开始消息
		virtual void I_R_M_GameBegin(BYTE bDeskNO){}

		// 清理桌面
		virtual void I_R_M_GameClean(){}

		// 更新比赛场进度（层级关系放游戏内处理）
		// 更新比赛轮数
		virtual void I_R_M_GameRoundNum(MSG_GR_ConTestRoundNum* contestRoundNum){}

		// 更新比赛局数
		virtual void I_R_M_GameInnings(MSG_GR_ConTestRoundNumInOneRound* contestInnings){}

		// 更新当前比赛进度
		virtual void I_R_M_GameInfoupdate(MSG_GM_S_ConTestGameInfo* contestInfodata){}

		// 排队准备
		virtual void I_R_M_QueueReady(){}
		
		// 游戏结束消息
		virtual void I_R_M_GameEnd(BYTE bDeskNO){}

		// 游戏信息消息
		virtual void I_R_M_GameInfo(MSG_GM_S_GameInfo* pGameInfo){}

		// 游戏状态消息
		virtual void I_R_M_GameStation(void* object, INT objectSize){}

		// 设置游戏回放模式
		virtual void I_R_M_GamePlayBack(bool isBack){}

		// 游戏消息
		virtual bool onGameMessage(NetMessageHead* messageHead, void* object, INT objectSize){ return true; }

		// 语音消息
		virtual bool onVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize){ return true; }

		// 游戏内红包活动
		virtual void I_R_M_GameActicity(void* object, INT objectSize){}

		// 游戏内领取红包
		virtual void I_R_M_GameReceive(void* object, INT objectSize){}

		// 比赛场更新轮数
		virtual void I_R_M_GameNumRound(void* object, INT objectSize){}

		// 比赛场更新局数
		virtual void I_R_M_GameInnings(void* object, INT objectSize){}

		// PC专用
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		// 房间信息
		virtual void I_R_M_GameBaseInfo(GameInfoEx* data){}

		// 玩家信息
		virtual void I_R_M_UserInfoList(UserInfoStruct* data){}
#endif
	};
}

#endif	//__HN_GameMessageDelegate_H__

