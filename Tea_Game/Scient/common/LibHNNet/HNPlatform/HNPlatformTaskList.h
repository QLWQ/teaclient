/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNPlatformTaskList_H__
#define __HN_HNPlatformTaskList_H__

#include "cocos2d.h"
#include <vector>
#include "network/HttpClient.h"
#include "HNNetProtocol/HNProtocolExport.h"
#include "HNNetExport.h"
#include "HNHttp/HNHttp.h"

USING_NS_CC;

struct DATE_TASKLIST
{
	string			TitleStr;					//任务内容
	int			TaskID;						//任务ID
	int			CurrentNum;					//当前进度
	int			Total;							//总进度
	int			Status;						//任务状态（0为未完成,1为未领取,2为已领取）
	int			TypeID;						//任务类型(关注->1 分享->2 部落->3 比赛->4 游戏->5 定时->6 购买->7,新手限定->8,签到(时间未到)->9,代理->10)
	//任务奖励
	int			AwardNum;						//奖励分类（几种奖励）
	int			Jewel;						//房卡
	int			WalletMoney;					//金币
	float			Money;						//现金红包
	int			Lotteries;						//礼券
	int			ShoppingTicket;					//购物券
	int			PhoneBill;						//话费
	int			Object;				//是否奖励手机或者mac

	DATE_TASKLIST()
	{
		TitleStr = "";
		TaskID = 0;
		CurrentNum = 0;
		Total = 0;
		Status = 0;
		TypeID = 1;
		AwardNum = 0;
		Jewel = 0;
		WalletMoney = 0;
		Money = 0;
		Lotteries = 0;
		ShoppingTicket = 0;
		PhoneBill = 0;
		Object = 0;
	}
};

struct DATE_RECORD
{
	string			TitleStr;					//记录内容
	int			Money;					//现金红包（分为单位）
};

namespace HN
{
	class HNPlatformTaskList : public HNHttpDelegate
	{
	public:
		typedef std::function<void()> SendCallBack;
		SendCallBack		SendReceiveCallBack = nullptr;

		typedef std::function<void()> RequestTaskCallBack;
		RequestTaskCallBack	onRequestTaskCallBack = nullptr;

		typedef std::function<void()> RequestCallBack;
		RequestCallBack		onRequestCallBack = nullptr;

		typedef std::function<void()> RequestRecordCallBack;
		RequestRecordCallBack	 onRequestRecordCallBack = nullptr;

		typedef std::function<void()> ExitCallBack;
		ExitCallBack		onExitCallBack = nullptr;

		//大厅红点更新
		typedef std::function<void()> RequestPlatformCallBack;
		RequestPlatformCallBack   onRequestPlatformCallBack = nullptr;

		// 获取单例
		static HNPlatformTaskList* getInstance();

		// 初始化
		bool init();
	private:
		// 构造函数
		HNPlatformTaskList();

		// 析构函数
		virtual ~HNPlatformTaskList();

		//HTTP请求结果
		void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData);

		//处理新手任务数据
		void dealNewbieData(const std::string& data);

		//处理每周任务数据
		void dealWeeklyData(const std::string& data);

		//处理每日任务数据
		void dealDailyData(const std::string& data);

		//处理定时任务数据
		void dealTimingData(const std::string& data);

		//处理新手红包任务
		void dealSubsidiesData(const std::string& data);

		//处理新手签到数据
		void dealSignInData(const std::string& data);

		//处理领取结果
		void dealReceiveResponse(const std::string& data);

		//查询红包零钱
		void dealLooseChangeData(const std::string& data);

		//查询红包记录
		void dealPocketRecordData(const std::string& data);

		//读取任务说明
		void dealTaskExplain(const std::string& data);
		
		//处理活动数据
		void dealHuodongData(const std::string& data, vector<DATE_TASKLIST>& datas, int title_index = -1);

		//单个任务信息
		//TaskListInfo*		_TaskListDate;

		//新手任务列表
		vector<DATE_TASKLIST> _NewbieTaskList;

		//每周任务
		vector<DATE_TASKLIST> _WeeklyTaskList;

		//定时任务
		vector<DATE_TASKLIST> _TimingTaskList;

		//每日任务
		vector<DATE_TASKLIST> _DailyTaskList;

		//新手红包
		vector<DATE_TASKLIST> _SubsidiesList;

		//新手签到
		vector<DATE_TASKLIST> _SignInList;

		//红包记录
		vector<DATE_RECORD> _RecordList;

		//单笔充值
		vector<DATE_TASKLIST> _SingleRechargeList;

		//累计充值
		vector<DATE_TASKLIST> _DoubleRechargeList;

		//是否新手任务未领取
		bool		_bIsNewbie = false;
		bool		_bIsWeekly = false;
		bool		_bIsTiming = false;
		bool		_bIsDaily = false;
		bool		_bIsSubsidies = false;
		bool		_bIsSignIn = false;

		//筛选任务状态
		void	screenNewbieTaskList();

		void	screenWeeklyTaskList();

		void	screenTimingTaskList();

		void	screenDailyTaskList();

		void	screenSubsidiesList();

		void	screenSignInList();

		//零钱
		float		_LooseChange = 0.0f;
		float		_MinStandard  = 20.0f;
		float		_MaxStandard  = 200.0f;

		bool		_isFirst;
		bool		_isRefresh;
		bool		_isShowNewbieR = false;			//是否显示新手红包
		bool		_isShowSignInR = false;			//是否显示新手签到

		//各个任务大标题
		string arr_TaskName[6];

		//任务备注内容
		string TaskTitleStr;

		string _string_Note;
	public:
		void RequestAllTaskList();

		void RequestNewbieTaskList();

		void RequestWeeklyTaskList();

		void RequestTimingTaskList();

		void RequestDailyTaskList();

		void RequestSubsidiesTaskList();

		void RequestSignInTaskList();  //请求新手签到数据

		void RequestLooseChange();

		//查询红包记录
		void RequestPocketRecord();

		//查询任务说明
		void RequestTaskExplain();

		//请求单笔充值
		void RequestSingleRechargeRecord();

		//请求累计充值
		void RequestDoubleRechargeRecord();

		//发送领取奖励
		void SendReceiveNReward(int TaskID);

		void SendReceiveWReward(int TaskID);

		void SendReceiveTReward(int TaskID);

		void SendReceiveDReward(int TaskID);

		void SendReceiveSReward(int TaskID);

		void SendReceiveSIReward(int TaskID);  //新手签到

		void SendWithdrawalRequest(float Num);

		void SendReceiveSingleRechargeRequest(int TaskID);

		void SendReceiveDoubleRechargeRequest(int TaskID);

		//返回数据
		vector<DATE_TASKLIST>& getNewbieTaskList();

		vector<DATE_TASKLIST>& getWeeklyTaskList();

		vector<DATE_TASKLIST>& getTimingTaskList();

		vector<DATE_TASKLIST>& getDailyTaskList();

		vector<DATE_TASKLIST>& getSubsidiesTaskList();

		vector<DATE_TASKLIST>& getSignInTaskList();

		vector<DATE_RECORD>& getRecordList();

		vector<DATE_TASKLIST>& getSingleRechargeList();  //获取单笔充值数据

		vector<DATE_TASKLIST>& getDoubleRechargeList();  //获取累计充值数据

		bool getNewbieState();

		bool getWeeklyState();

		bool getTimingState();

		bool getDailyState();

		bool getSubsidiesState();

		bool getSignInState();

		bool GetNewbieAllState();

		bool GetTaskListAllState();

		float getLooseChange();

		float getMinStandard();

		float getMaxStandard();

		bool getShowNewbieR();

		bool getShowSignInR();

		string getTaskNameByIndex(int index);

		string getTaskNote();

		string getTaskTitleStr();
	};

	#define  PlatformTask()	HNPlatformTaskList::getInstance()
}

#endif