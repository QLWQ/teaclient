//俱乐部用户数据头像(bluce_lickey  2020.2.24)

#include "HNPlatformClubData.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "HNPlatform/HNPlatformLogic.h"
#include "HNData/HNRoomInfoModule.h"
#include "HNUIExport.h"
#include "HNCommon/HNConverCode.h"

#define		ClubFlags_PauseAllTeahouse_C  0x00000001;

namespace HN
{
	static HNPlatformClubData* sHNPlatformClubData = nullptr;
	HNPlatformClubData* HNPlatformClubData::getInstance()
	{
		if (nullptr == sHNPlatformClubData)
		{
			sHNPlatformClubData = new HNPlatformClubData();
			sHNPlatformClubData->init();
		}
		return sHNPlatformClubData;
	}

	HNPlatformClubData::HNPlatformClubData()
	{
		Vec_UserListInfo.clear();
		map_UserID.clear();
		map_UserHeadUrl.clear();
		Vec_TeaHouseInfo.clear();
		Vec_TeaHouseTable.clear();
		Index_HeadCount = 0;

		//监听俱乐部错误消息返回
		PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_TipMsg, HN_SOCKET_CALLBACK(HNPlatformClubData::getTeaHouseErrorUpdate, this));
	}

	HNPlatformClubData::~HNPlatformClubData()
	{
		PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_TipMsg);//移除俱乐部错误信息返回
		HNHttpRequest::getInstance()->removeObserver(this);
	}

	bool HNPlatformClubData::init()
	{
		return true;
	}

	void HNPlatformClubData::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		if (!isSucceed) return;
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}
		int UserID = std::atoi(requestName.c_str());
		if (UserID == NULL)
		{
			return;
		}
		int code = doc["IsWX"].GetInt();
		if (code == 1)
		{
			std::string url = doc["HeadUrl"].GetString();
			auto itMap = map_UserHeadUrl.find(UserID);
			if (itMap == map_UserHeadUrl.end())
			{
				map_UserHeadUrl.insert(std::pair<int, std::string>(UserID, url));
			}
		}
		else
		{
			std::string url = doc["HeadUrl"].GetString();
			auto itMap = map_UserHeadUrl.find(UserID);
			if (itMap == map_UserHeadUrl.end())
			{
				map_UserHeadUrl.insert(std::pair<int, std::string>(UserID, url));
			}
		}
		//需要保证递增之后不能超过容器
		if (Index_HeadCount + 1 < Vec_UserListInfo.size())
		{
			Index_HeadCount = Index_HeadCount + 1;
			onResponseHeadUrl(Vec_UserListInfo[Index_HeadCount]);
		}
		else
		{
			if (Index_HeadCount + 1 == Vec_UserListInfo.size())
			{
				Index_HeadCount = Index_HeadCount + 1;
			}
		}
	}

	void HNPlatformClubData::onQueryClubUserList(int ClubID)
	{
		MSG_GP_I_Club_UserList ID;
		ID.iClubID = ClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_USERLIST, &ID, sizeof(MSG_GP_I_Club_UserList), HN_SOCKET_CALLBACK(HNPlatformClubData::getClubMemberInfoResult, this));
	}

	bool HNPlatformClubData::getClubMemberInfoResult(HNSocketMessage* SocketMessage)
	{
		if (SocketMessage->messageHead.bHandleCode == 0)
		{
			auto head = (MSG_GP_O_Club_UserList_Head*)SocketMessage->object;
			int num = (SocketMessage->objectSize - sizeof(MSG_GP_O_Club_UserList_Head)) / (sizeof(MSG_GP_O_Club_UserList_Data));
			auto  Data = (MSG_GP_O_Club_UserList_Data*)(SocketMessage->object + sizeof(MSG_GP_O_Club_UserList_Head));
			
			if (num)
			{
				while (num--)
				{
					auto itMap = map_UserID.find(Data->iUserID);
					if (itMap == map_UserID.end())
					{
						map_UserID.insert(std::pair<int, int>(Data->iUserID, Data->iUserID));
						Vec_UserListInfo.push_back(Data->iUserID);
						isHaveNew = true;
					}
					Data++;
				}
				//数据处理完成进行第三方头像查询
				if (isHaveNew)
				{
					isHaveNew = false;
					onResponseHeadUrl(Vec_UserListInfo[Index_HeadCount]);
				}
			}
		}
		else
		{
			//第三方头像Url查询???

		}
		return true;
	}

	void HNPlatformClubData::onResponseHeadUrl(int UserID)
	{
		std::string path = StringUtils::format("/hyyl/get_head_url.php?userid=%d", UserID);
		std::string url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		std::string str_tag = std::to_string(UserID);
		HNHttpRequest::getInstance()->request(str_tag, cocos2d::network::HttpRequest::Type::GET, url);
	}

	std::string HNPlatformClubData::getUserHeadUrlByUserID(int UserID)
	{
		std::string str = "";
		auto itMap = map_UserHeadUrl.find(UserID);
		if (itMap == map_UserHeadUrl.end())
		{
			return str;
		}
		else
		{
			str = map_UserHeadUrl[UserID];
		}
		return str;
	}

	void HNPlatformClubData::AddTeaHouseNetWorkNewsEventListener()
	{
		//初始化楼层桌子信息
		PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_InitAllTeaHouseData, HN_SOCKET_CALLBACK(HNPlatformClubData::getTeaHouseInfoResult, this));

		//更新桌子信息（楼层信息）
		PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_UpdateTeaHouseDesk, HN_SOCKET_CALLBACK(HNPlatformClubData::getTeaHouseInfoUpdate, this));

		//更新茶楼楼层信息(楼层新建和楼层更新)
		PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OnTeaHouseCreate, HN_SOCKET_CALLBACK(HNPlatformClubData::getTeaHouseFloorUpdate, this));

		//刷新俱乐部桌子
		PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_ReplenishTeahouseDesk, HN_SOCKET_CALLBACK(HNPlatformClubData::getTeaHouseSupplementUpdate, this));

		isNetEventListener = true;
	}

	void HNPlatformClubData::RemoveTeaHouseNetWorkNewsEventListener()
	{
		PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_InitAllTeaHouseData);//移除监听初始化信息
		PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_UpdateTeaHouseDesk);//移除监听更新信息

		PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OnTeaHouseCreate);//移除茶楼楼层更新信息
		PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_ReplenishTeahouseDesk);//移除刷新俱乐部桌子

		//退出俱乐部的时候移除数据
		Vec_TeaHouseInfo.clear();
		Vec_TeaHouseTable.clear();

		isNetEventListener = false;
	}

	bool HNPlatformClubData::getTeaHouseInfoResult(HNSocketMessage* SocketMessage)
	{
		Vec_TeaHouseInfo.clear();
		Vec_TeaHouseTable.clear();
		//初始化茶楼数据，桌子数据（参考结构体MSG_GP_O_Club_InitAllTeaHouseData）
		if (SocketMessage->messageHead.bHandleCode == 0)
		{
			int* ClubID = (int*)SocketMessage->object;
			int size = 0;
			size += sizeof(int);
			UINT* clubFlags = (UINT*)(SocketMessage->object + size);
			size += sizeof(UINT);
			int* teahouseCount = (int*)(SocketMessage->object + size);
			size += sizeof(int);
			if (*teahouseCount > 0)
			{
				for (int i = 0; i < *teahouseCount; i++)
				{
					auto Data = (SClubTeahouseInfo*)(SocketMessage->object + size);
					size += sizeof(SClubTeahouseInfo);
					//楼层信息存储
					STeahouseInfo_Club vec_pushData;
					vec_pushData.info = *Data;
					vec_pushData.clubId = *ClubID;
					vec_pushData.clubFlag = *clubFlags;
					Vec_TeaHouseInfo.push_back(vec_pushData);
				}
			}

			int* deskCount = (int*)(SocketMessage->object + size);
			size += sizeof(int);
			if (*deskCount > 0)
			{
				for (int i = 0; i < *deskCount; i++)
				{
					auto Data = (SClubTeahouseDeskInfo*)(SocketMessage->object + size);
					size += sizeof(SClubTeahouseDeskInfo);
					//桌子信息存储
					STeahouseDeskInfo_Club vec_pushData;
					vec_pushData.info = *Data;
					vec_pushData.clubId = *ClubID;
					Vec_TeaHouseTable.push_back(vec_pushData);
				}
			}

			//楼层房间信息
			int* roomCount = (int*)(SocketMessage->object + size);
			size += sizeof(int);
			if (*roomCount > 0)
			{
				for (int i = 0; i < *roomCount; i++)
				{
					auto Data = (ComRoomInfo*)(SocketMessage->object + size);
					size += sizeof(ComRoomInfo);
					//房间信息存储
					HNRoomInfoModule::getInstance()->addRoom(Data);
				}
			}

			//整理桌子数据（处理不同楼层桌子数据问题）
			//sortTeaHouseTableInfo();

			if (onRequestCallBack != nullptr)
			{
				onRequestCallBack();
			}

		}
		return true;
	}

	bool HNPlatformClubData::getTeaHouseInfoUpdate(HNSocketMessage* SocketMessage)
	{
		if (SocketMessage->messageHead.bHandleCode == 0)
		{
			//更新数据
			auto Data = (MSG_GP_O_Club_UpdateTeahouseDeskInfo*)SocketMessage->object;
			int teahouseId = Data->teahouseId;
			int ClubID = Data->clubId;
			WORD deskIndex = Data->deskIndex;
			bool isShowUpdate = true;
			for (int i = 0; i < 2; i++)
			{
				BYTE updateList = Data->updateList[i];
				if (updateList == ClubTeahouseDeskUpdate_State)
				{
					//更新桌子状态
					for (int j = 0; j < Vec_TeaHouseTable.size(); j++)
					{
						if (Vec_TeaHouseTable[j].info.deskIndex == deskIndex)
						{
							Vec_TeaHouseTable[j].info.deskState = Data->updateValues[i];
						}
					}
				}
				else if (updateList == ClubTeahouseDeskUpdate_CurGames)
				{
					//更新局数状态
					for (int j = 0; j < Vec_TeaHouseTable.size(); j++)
					{
						if (Vec_TeaHouseTable[j].info.deskIndex == deskIndex)
						{
							Vec_TeaHouseTable[j].info.curGames = Data->updateValues[i];
						}
					}
				}
				else if (updateList == ClubTeahouseDeskUpdate_PlayerEnter)
				{
					//更新玩家进入
					//更新桌子状态
					for (int j = 0; j < Vec_TeaHouseTable.size(); j++)
					{
						if (Vec_TeaHouseTable[j].info.deskIndex == deskIndex)
						{
							for (int z = 0; z < 6; z++)
							{
								if (Vec_TeaHouseTable[j].info.userIDList[z] == 0)
								{
									Vec_TeaHouseTable[j].info.userIDList[z] = Data->updateValues[i];
									break;
								}
							}
						}
					}
				}
				else if (updateList == ClubTeahouseDeskUpdate_PlayerLeave)
				{
					//更新玩家离开
					//更新桌子状态
					for (int j = 0; j < Vec_TeaHouseTable.size(); j++)
					{
						if (Vec_TeaHouseTable[j].info.deskIndex == deskIndex)
						{
							for (int z = 0; z < 6; z++)
							{
								if (Vec_TeaHouseTable[j].info.userIDList[z] == Data->updateValues[i])
								{
									Vec_TeaHouseTable[j].info.userIDList[z] = 0;
									break;
								}
							}
						}
					}
				}

				else if (updateList == ClubTeahouseDeskUpdate_ClearDesk)
				{
					for (int j = 0; j < Vec_TeaHouseTable.size(); j++)
					{
						if (Vec_TeaHouseTable[j].info.deskIndex == deskIndex)
						{
							Vec_TeaHouseTable[j].info.curGames = 0;
							Vec_TeaHouseTable[j].info.deskState = 1;
							for (int z = 0; z < 6; z++)
							{
								Vec_TeaHouseTable[j].info.userIDList[z] = 0;
							}
						}
					}
				}
				else if (updateList == ClubTeahouseDeskUpdate_DeleteDesk)
				{
					for (int j = 0; j < Vec_TeaHouseTable.size(); j++)
					{
						if (Vec_TeaHouseTable[j].info.deskIndex == deskIndex)
						{
							Vec_TeaHouseTable.erase(Vec_TeaHouseTable.begin() + j);
							break;
						}
					}
				}
				else if (updateList == ClubTeahouseDeskUpdate_DeleteTeahouse)
				{
					for (int j = 0; j < Vec_TeaHouseInfo.size(); j++)
					{
						if (Vec_TeaHouseInfo[j].info.teahouseId == teahouseId)
						{
							Vec_TeaHouseInfo.erase(Vec_TeaHouseInfo.begin() + j);
							break;
						}
					}
					//查询茶楼桌子数据
					std::vector<SClubTeahouseDeskInfo> Vec_Table = getTeaHouseTable(ClubID, teahouseId);
					//移除茶楼对应桌子
					for (auto item : Vec_Table)
					{
						for (int j = 0; j < Vec_TeaHouseTable.size(); j++)
						{
							if (Vec_TeaHouseTable[j].info.deskIndex == item.deskIndex && item.teahouseId == Vec_TeaHouseTable[j].info.teahouseId)
							{
								Vec_TeaHouseTable.erase(Vec_TeaHouseTable.begin() + j);
								break;
							}
						}
					}
					//通知解散
					if (Data->updateValues[0] == 1)
					{
						if (onDissolveCallBack != nullptr)
						{
							onDissolveCallBack(teahouseId);
						}
						isShowUpdate = false;
					}
				}
				else if (updateList == ClubTeahouseDeskUpdate_UpdateFlags)
				{
					//更新楼层标记
					for (int j = 0; j < Vec_TeaHouseInfo.size(); j++)
					{
						if (Vec_TeaHouseInfo[j].clubId == ClubID)
						{
							Vec_TeaHouseInfo[j].clubFlag = Data->updateValues[i];
						}
					}
				}
			}

			if (isShowUpdate)
			{
				//刷新回调
				if (onRequestCallBack != nullptr)
				{
					onRequestCallBack();
				}
			}

		}
		return true;
	}

	//有人新建了楼层信息
	bool HNPlatformClubData::getTeaHouseFloorUpdate(HNSocketMessage* SocketMessage)
	{
		if (SocketMessage->messageHead.bHandleCode == 0)
		{
			MSG_GP_O_Club_OnTeaHouseCreate* pData = (MSG_GP_O_Club_OnTeaHouseCreate*)SocketMessage->object;
			bool is_Resh = false;
			int Teahouseid = 0;
			for (int i = 0; i < Vec_TeaHouseInfo.size(); i++)
			{
				if (Vec_TeaHouseInfo[i].clubId == pData->clubId && Vec_TeaHouseInfo[i].info.teahouseId == pData->pTeahouseInfo.teahouseId)
				{
					memcpy(&Vec_TeaHouseInfo[i].info, &pData->pTeahouseInfo, sizeof(SClubTeahouseInfo));
					/*Vec_TeaHouseInfo[i].info.gameNameId = pData->pTeahouseInfo.gameNameId;
					Vec_TeaHouseInfo[i].info.roomId = pData->pTeahouseInfo.roomId;
					Vec_TeaHouseInfo[i].info.maxPlayerCount = pData->pTeahouseInfo.maxPlayerCount;
					Vec_TeaHouseInfo[i].info.maxGames = pData->pTeahouseInfo.maxGames;
					Vec_TeaHouseInfo[i].info.maxDeskCount = pData->pTeahouseInfo.maxDeskCount;
					memcpy(Vec_TeaHouseInfo[i].info.szDeskConfig, pData->pTeahouseInfo.szDeskConfig, sizeof(pData->pTeahouseInfo.szDeskConfig));
					memcpy(Vec_TeaHouseInfo[i].info.szName, pData->pTeahouseInfo.szName, sizeof(pData->pTeahouseInfo.szName));*/

					is_Resh = true;
					break;
				}
			}
			if (!is_Resh)
			{
				//楼层信息存储
				STeahouseInfo_Club vec_pushData;
				vec_pushData.info = pData->pTeahouseInfo;
				vec_pushData.clubId = pData->clubId;
				Vec_TeaHouseInfo.push_back(vec_pushData);

				for (int i = 0; i<pData->deskCount; i++)
				{
					SClubTeahouseDeskInfo& curDeskInfo = pData->pDeskInfo[i];
					STeahouseDeskInfo_Club vec_pushData;
					vec_pushData.info = curDeskInfo;
					vec_pushData.clubId = pData->clubId;
					Vec_TeaHouseTable.push_back(vec_pushData);
				}
			}
			else
			{
				//查询茶楼桌子数据
				std::vector<SClubTeahouseDeskInfo> Vec_Table = getTeaHouseTable(pData->clubId, pData->pTeahouseInfo.teahouseId);
				//如果是更新桌子判断桌子数是否对应
				if (Vec_Table.size() == pData->deskCount)
				{
					for (int i = 0; i < Vec_TeaHouseTable.size(); i++)
					{
						for (auto item : Vec_Table)
						{
							if (Vec_TeaHouseTable[i].info.deskIndex == item.deskIndex && item.teahouseId == Vec_TeaHouseTable[i].info.teahouseId)
							{
								Vec_TeaHouseTable[i].info.curGames = pData->pDeskInfo[i].curGames;
								Vec_TeaHouseTable[i].info.teahouseId = pData->pDeskInfo[i].teahouseId;
								Vec_TeaHouseTable[i].info.deskIndex = pData->pDeskInfo[i].deskIndex;
								Vec_TeaHouseTable[i].info.deskState = pData->pDeskInfo[i].deskState;
								memcpy(Vec_TeaHouseTable[i].info.userIDList, pData->pDeskInfo[i].userIDList, sizeof(pData->pDeskInfo[i].userIDList));
								break;
							}
						}
					}
				}
				else
				{
					//特殊情况（原来创建桌子6桌修改玩法之后没有达到6桌或者修改了桌子数量）
					for (auto item : Vec_Table)
					{
						for (int i = 0; i < Vec_TeaHouseTable.size(); i++)
						{
							if (Vec_TeaHouseTable[i].info.deskIndex == item.deskIndex && item.teahouseId == Vec_TeaHouseTable[i].info.teahouseId)
							{
								Vec_TeaHouseTable.erase(Vec_TeaHouseTable.begin() + i);
								break;
							}
						}
					}
					//压入新桌子数据
					for (int i = 0; i<pData->deskCount; i++)
					{
						SClubTeahouseDeskInfo& curDeskInfo = pData->pDeskInfo[i];
						STeahouseDeskInfo_Club vec_pushData;
						vec_pushData.info = curDeskInfo;
						vec_pushData.clubId = pData->clubId;
						Vec_TeaHouseTable.push_back(vec_pushData);
					}
				}
			}

			//整理桌子数据（处理不同楼层桌子数据问题）
			//sortTeaHouseTableInfo();

			if (onRequestCallBack != nullptr)
			{
				onRequestCallBack();
			}
		}
		return true;
	}

	bool HNPlatformClubData::getTeaHouseSupplementUpdate(HNSocketMessage* SocketMessage)
	{
		if (SocketMessage->messageHead.bHandleCode == 0)
		{
			//处理收到的数据处理
			MSG_GP_O_Club_ReplenishTeahouseDesk* Data = (MSG_GP_O_Club_ReplenishTeahouseDesk*)SocketMessage->object;
			int ClubID = Data->clubId;
			int TeaHouseID = Data->teahouseId;
			Vec_deskList.clear();
			for (int i = 0; i < Data->deskCount; i++)
			{
				Vec_deskList.push_back(Data->deskList[i]);
			}

			//查询茶楼桌子数据
			std::vector<SClubTeahouseDeskInfo> Vec_Table = getTeaHouseTable(ClubID, TeaHouseID);
			for (int i = 0; i < Vec_deskList.size(); i++)
			{
				if (Vec_Table.size() > 0)
				{
					bool IsHaveDesk = false;
					for (auto vec_item : Vec_Table)
					{
						if (vec_item.deskIndex == Vec_deskList[i])
						{
							IsHaveDesk = true;
							break;
						}
					}
					if (!IsHaveDesk)
					{
						STeahouseDeskInfo_Club Data;
						Data.info.curGames = 0;
						Data.info.deskIndex = Vec_deskList[i];
						Data.info.deskState = 1;
						Data.info.teahouseId = TeaHouseID;
						Data.clubId = ClubID;
						for (int j = 0; j < 6; j++)
						{
							Data.info.userIDList[j] = 0;
						}
						Vec_TeaHouseTable.push_back(Data);
					}
				}
				//没有桌子的情况下模拟桌子数据
				else
				{
					STeahouseDeskInfo_Club Data;
					Data.info.curGames = 0;
					Data.info.deskIndex = Vec_deskList[i];
					Data.info.deskState = 1;
					Data.info.teahouseId = TeaHouseID;
					Data.clubId = ClubID;
					for (int j = 0; j < 6; j++)
					{
						Data.info.userIDList[j] = 0;
					}
					Vec_TeaHouseTable.push_back(Data);
				}
			}

			if (onRequestCallBack != nullptr)
			{
				onRequestCallBack();
			}
		}
		return true;
	}

	bool HNPlatformClubData::getTeaHouseErrorUpdate(HNSocketMessage* SocketMessage)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
		switch (SocketMessage->messageHead.bHandleCode)
		{
		case(ERR_GP_USER_NOT_FOUND):
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("用户信息错误"));
		}break;
		case(ERR_GP_NAME_LIMITE) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("俱乐部名称包含屏蔽字"));
		}break;
		case(ERR_GP_CLUB_DeskPlaying) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("有正在游戏的桌子"));
		}break;
		case(ERR_GP_CLUB_LockedTeahouse) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("茶楼已暂停开房"));
		}break;
		case(ERR_GP_CLUB_NeedRedFlowers) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("红花不足"));
		}break;
		case(ERR_GP_CLUB_NeedAccess) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("权限不足"));
		}break;
		case(ERR_GP_CLUB_NotClubOwner) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("不是群主"));
		}break;
		case(ERR_GP_CLUB_InvalidUser) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("无效用户"));
		}break;
		case(ERR_GP_CLUB_SetBlackList) :
		{
			int* ClubID = (int*)SocketMessage->object;
			if (onBlackListUserCallBack != nullptr)
			{
				onBlackListUserCallBack(*ClubID);
			}
			GamePromptLayer::create()->showPrompt(GBKToUtf8("您已被加入黑名单"));
		}break;
		case(ERR_GP_CLUB_NotPartner) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("操作对象不是合伙人！"));
		}break;
		//同桌限制
		case(ERR_GP_CLUB_DeskmateLimit) :
		{
			/*int ClubID = (int)SocketMessage->object;
			if (onBlackListUserCallBack != nullptr)
			{
				onBlackListUserCallBack(ClubID);
			}*/
			GamePromptLayer::create()->showPrompt(GBKToUtf8("无法与该房间玩家同桌，请联系管理员"));
		}break;
		case(ERR_GP_CLUB_UsersLimit) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("俱乐部人数已达上限"));
		}break;
		default:
		{

		}
		}
		return true;
	}

	void HNPlatformClubData::sortTeaHouseTableInfo()
	{
		std::vector<SClubTeahouseDeskInfo> Vec_TableData;;
		if (Vec_TeaHouseTable.size() > 0)
		{

		}
	}

	void HNPlatformClubData::StartNetEventListener()
	{
		if (isNetEventListener == false)
		{
			AddTeaHouseNetWorkNewsEventListener();
		}
	}

	void HNPlatformClubData::RemoveNetEventListener()
	{
		if (isNetEventListener)
		{
			RemoveTeaHouseNetWorkNewsEventListener();
		}
	}

	std::vector<SClubTeahouseInfo> HNPlatformClubData::getTeaHouseInfo(int ClubID)
	{
		std::vector<SClubTeahouseInfo> Vec_Result;
		if (Vec_TeaHouseInfo.size() > 0)
		{
			for (auto vec_item: Vec_TeaHouseInfo)
			{
				if (vec_item.clubId == ClubID)
				{
					Vec_Result.push_back(vec_item.info);
				}
			}
		}
		return Vec_Result;
	}

	SClubTeahouseInfo HNPlatformClubData::getTeaHouseInfoByTeaHouseID(int ClubID, int TeaHouseID)
	{
		SClubTeahouseInfo Vec_Result;
		/*Vec_Result.gameNameId = 0;
		Vec_Result.maxDeskCount = 0;
		Vec_Result.maxGames = 0;
		Vec_Result.maxPlayerCount = 0;
		Vec_Result.roomId = 0;
		Vec_Result.teahouseId = 0;
		memset(Vec_Result.szDeskConfig, 0, sizeof(Vec_Result.szDeskConfig));
		memset(Vec_Result.szName, 0, sizeof(Vec_Result.szName));
		memset(&Vec_Result.option, 0, sizeof(Vec_Result.option));*/
		memset(&Vec_Result, 0, sizeof(Vec_Result));
		if (Vec_TeaHouseInfo.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseInfo)
			{
				if (vec_item.clubId == ClubID && vec_item.info.teahouseId == TeaHouseID)
				{
					/*Vec_Result.gameNameId = vec_item.info.gameNameId;
					Vec_Result.maxDeskCount = vec_item.info.maxDeskCount;
					Vec_Result.maxGames = vec_item.info.maxGames;
					Vec_Result.maxPlayerCount = vec_item.info.maxPlayerCount;
					Vec_Result.roomId = vec_item.info.roomId;
					memcpy(Vec_Result.szDeskConfig, vec_item.info.szDeskConfig, sizeof(vec_item.info.szDeskConfig));
					memcpy(Vec_Result.szName, vec_item.info.szName, sizeof(vec_item.info.szName));
					Vec_Result.teahouseId = vec_item.info.teahouseId;*/
					Vec_Result = vec_item.info;
				}
			}
		}
		return Vec_Result;
	}

	std::vector<SClubTeahouseDeskInfo> HNPlatformClubData::getTeaHouseTable(int ClubID)
	{
		std::vector<SClubTeahouseDeskInfo> Vec_Table;
		if (Vec_TeaHouseTable.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseTable)
			{
				if (vec_item.clubId == ClubID)
				{
					Vec_Table.push_back(vec_item.info);
				}
			}
		}
		return Vec_Table;
	}

	std::vector<SClubTeahouseDeskInfo> HNPlatformClubData::getTeaHouseStardTable(int ClubID)
	{
		std::vector<SClubTeahouseDeskInfo> Vec_Table;
		if (Vec_TeaHouseTable.size() <= 0)
			return Vec_Table;
		
		for (auto vec_item : Vec_TeaHouseTable)
		{
			if (vec_item.clubId != ClubID)
				continue;		
			if (vec_item.info.deskState == ClubTeahouseDeskState_Playing)
			{
				Vec_Table.push_back(vec_item.info);
			}
			else
			{
				for (int i = 0; i < 6; i++)
				{
					if (vec_item.info.userIDList[i] != 0)
					{
						Vec_Table.push_back(vec_item.info);
						break;
					}
				}
			}
		}
		return Vec_Table;
	}

	//获取当前茶楼最大人数
	int HNPlatformClubData::getTeaHouseInfoPlayer(int ClubID, int TeaHouseID)
	{
		if (Vec_TeaHouseInfo.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseInfo)
			{
				if (vec_item.clubId == ClubID && vec_item.info.teahouseId == TeaHouseID)
				{
					return vec_item.info.maxPlayerCount;
				}
			}
		}
		return 0;
	}

	//获取当前茶楼最大局数
	int HNPlatformClubData::getTeaHouseInfoMaxCount(int ClubID, int TeaHouseID)
	{
		if (Vec_TeaHouseInfo.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseInfo)
			{
				if (vec_item.clubId == ClubID && vec_item.info.teahouseId == TeaHouseID)
				{
					return vec_item.info.maxGames;
				}
			}
		}
		return 0;
	}

	//获取当前茶楼房间ID
	int HNPlatformClubData::getTeaHouseInfoRoomID(int ClubID, int TeaHouseID)
	{
		if (Vec_TeaHouseInfo.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseInfo)
			{
				if (vec_item.clubId == ClubID && vec_item.info.teahouseId == TeaHouseID)
				{
					return vec_item.info.roomId;
				}
			}
		}
		return 0;
	}

	//获取当前茶楼游戏ID
	int HNPlatformClubData::getTeaHouseInfoGameID(int ClubID, int TeaHouseID)
	{
		if (Vec_TeaHouseInfo.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseInfo)
			{
				if (vec_item.clubId == ClubID && vec_item.info.teahouseId == TeaHouseID)
				{
					return vec_item.info.gameNameId;
				}
			}
		}
		return 0;
	}

	std::vector<SClubTeahouseDeskInfo> HNPlatformClubData::getTeaHouseTable(int ClubID, int TeaHouseID)
	{
		std::vector<SClubTeahouseDeskInfo> Vec_Table;
		if (Vec_TeaHouseTable.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseTable)
			{
				if (vec_item.clubId == ClubID && vec_item.info.teahouseId == TeaHouseID)
				{
					Vec_Table.push_back(vec_item.info);
				}
			}
		}
		return Vec_Table;
	}

	UINT HNPlatformClubData::getTeaHouseInfoByState(int ClubID)
	{
		UINT Rest = 0;
		if (Vec_TeaHouseInfo.size() > 0)
		{
			for (auto vec_item : Vec_TeaHouseInfo)
			{
				if (vec_item.clubId == ClubID)
				{
					Rest = vec_item.clubFlag & ClubFlags_PauseAllTeahouse;
					break;
				}
			}
		}
		return Rest;
	}

	void HNPlatformClubData::TeaHouseUpdate()
	{
		if (onRequestCallBack != nullptr)
		{
			onRequestCallBack();
		}
	}

	void HNPlatformClubData::setTeaHouseNameByTeaHouseID(int ClubID, int TeaHouseID, const std::string& szName)
	{
		for (int i = 0; i < Vec_TeaHouseInfo.size(); i++)
		{
			if (Vec_TeaHouseInfo[i].clubId == ClubID && Vec_TeaHouseInfo[i].info.teahouseId == TeaHouseID)
			{
				memset(Vec_TeaHouseInfo[i].info.szName, 0, sizeof(Vec_TeaHouseInfo[i].info.szName));
				memcpy(Vec_TeaHouseInfo[i].info.szName, szName.c_str(), szName.length());
			}
		}

		if (onRequestCallBack != nullptr)
		{
			onRequestCallBack();
		}
	}

	int HNPlatformClubData::getTeaHousTableCount()
	{
		return Vec_TeaHouseTable.size();
	}

};