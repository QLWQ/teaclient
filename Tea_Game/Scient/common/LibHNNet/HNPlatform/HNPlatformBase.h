﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNPlatformBase_H__
#define __HN_HNPlatformBase_H__

#include "HNPlatform/HNPlatformNotifyDelegate.h"
#include "HNSocket/HNSocketMessage.h"
#include "HNUIExport.h"

namespace HN {

	class HNPlatformBase : public HNScene, IPlatformMessageDelegate
	{
	public:
		HNPlatformBase(void);
		virtual ~HNPlatformBase(void);

	public:
		virtual void onEnter() override;

		virtual void onExit() override;

	};
};

#endif	//__HN_HNPlatformScene_H__
