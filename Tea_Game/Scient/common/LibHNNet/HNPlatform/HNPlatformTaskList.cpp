/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformTaskList.h"
#include "json/rapidjson.h"
#include "json/document.h"

//static const std::string HTTP_URL[] = {"http://shuju.qp888m.com/hyyl","http://192.168.0.33:8060/hyyl"};
//static const int APP_URL = 0;				//0为线上   1为本地

namespace HN
{
	static HNPlatformTaskList* sHNPlatformTask = nullptr;
	HNPlatformTaskList* HNPlatformTaskList::getInstance()
	{
		if (nullptr == sHNPlatformTask)
		{
			sHNPlatformTask = new HNPlatformTaskList();
			sHNPlatformTask->init();
		}
		return sHNPlatformTask;
	}

	HNPlatformTaskList::HNPlatformTaskList()
	: _isFirst (false)
	{
		
	}

	HNPlatformTaskList::~HNPlatformTaskList()
	{
		HNHttpRequest::getInstance()->removeObserver(this);
	}

	bool HNPlatformTaskList::init()
	{
		//RequestAllTaskList();
		return true;
	}

	void HNPlatformTaskList::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		if (!isSucceed) return;
		if (requestName.compare("requestNewbie") == 0)
		{
			dealNewbieData(responseData);
			/*if (_isFirst)
			{
				RequestWeeklyTaskList();
			}*/
		}
		else if (requestName.compare("requestWeekly") == 0)
		{
			dealWeeklyData(responseData);
			/*if (_isFirst)
			{
				RequestTimingTaskList();
			}*/
		}
		else if (requestName.compare("requestTiming") == 0)
		{
			dealTimingData(responseData);
			/*if (_isFirst)
			{
				RequestDailyTaskList();
			}*/
		}
		else if (requestName.compare("requestDaily") == 0)
		{
			dealDailyData(responseData);
			/*if (_isFirst)
			{
				RequestSubsidiesTaskList();
			}*/
		}
		else if (requestName.compare("requestSubsidies") == 0)
		{
			dealSubsidiesData(responseData);
			/*if (_isFirst)
			{
				RequestSignInTaskList(); 
			}*/
		}
		else if (requestName.compare("requestSignIn") == 0)
		{
			dealSignInData(responseData);
			/*if (_isFirst)
			{
				RequestLooseChange();
			}*/
		}
		else if (requestName.compare("requestLooseChange") == 0)
		{
			dealLooseChangeData(responseData);
			if (_isFirst)
			{
				if (onRequestPlatformCallBack != nullptr)
				{
					onRequestPlatformCallBack();
				}
				_isFirst = false;
			}
		}
		else if (requestName.compare("AccountCashSource") == 0)
		{
			_isRefresh = true;
			dealPocketRecordData(responseData);
		}
		else if (requestName.compare("GetSinglePresentInfo") == 0)
		{
			dealHuodongData(responseData, _SingleRechargeList);
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				SendReceiveCallBack = nullptr;
			}
		}
		else if (requestName.compare("GetDoublePresentInfo") == 0)
		{
			dealHuodongData(responseData, _DoubleRechargeList);
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				_isRefresh = false;
			}
		}
		else if (requestName.compare("TaskExplain") == 0)
		{
			dealTaskExplain(responseData);
		}
		else if (requestName.compare("SendReceiveN") == 0)
		{
			//查询的领取结果
			dealReceiveResponse(responseData);
			RequestNewbieTaskList();
		}
		else if (requestName.compare("SendReceiveW") == 0)
		{
			//查询的领取结果
			dealReceiveResponse(responseData);
			RequestWeeklyTaskList();
		}
		else if (requestName.compare("SendReceiveT") == 0)
		{
			//查询的领取结果
			dealReceiveResponse(responseData);
			RequestTimingTaskList();
		}
		else if (requestName.compare("SendReceiveD") == 0)
		{
			//查询的领取结果
			dealReceiveResponse(responseData);
			RequestDailyTaskList();
		}
		else if (requestName.compare("SendReceiveS") == 0)
		{
			//查询的领取结果
			dealReceiveResponse(responseData);
			RequestSubsidiesTaskList();
		}
		else if (requestName.compare("SendReceiveSI") == 0)
		{
			//查询的领取结果
			dealReceiveResponse(responseData);
			RequestSignInTaskList();
		}
		else if (requestName.compare("SendWithdrawal") == 0)
		{
			dealReceiveResponse(responseData);
			RequestLooseChange();
		}
		else if (requestName.compare("SendReceiveSingleRecharge") == 0)
		{
			dealReceiveResponse(responseData);
			RequestSingleRechargeRecord();
		}
		else if (requestName.compare("SendReceiveSingleRecharge") == 0)
		{
			dealReceiveResponse(responseData);
			RequestDoubleRechargeRecord();
		}
	}

	void HNPlatformTaskList::RequestAllTaskList()
	{
		_isFirst = true;
		RequestNewbieTaskList();
		RequestWeeklyTaskList();
		RequestTimingTaskList();
		RequestDailyTaskList();
		RequestSubsidiesTaskList();
		RequestSignInTaskList();
		RequestLooseChange();
		RequestTaskExplain();
	}

	void HNPlatformTaskList::RequestNewbieTaskList()
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetRookieTaskInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestNewbie", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestWeeklyTaskList()
	{
		std::string path =StringUtils::format("/hyyl/huodong/GetWeekTaskInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestWeekly", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestTimingTaskList()
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetCronJobsInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestTiming", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestDailyTaskList()
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetDailyTaskInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestDaily", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestSubsidiesTaskList()
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetRookieRedBagTaskInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestSubsidies", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestSignInTaskList()
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetRookieSignTaskInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestSignIn", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestLooseChange()
	{
		std::string path = StringUtils::format("/hyyl/AccountsCashInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestLooseChange", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestPocketRecord()
	{
		std::string path = StringUtils::format("/hyyl/AccountCashSource.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("AccountCashSource", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestTaskExplain()
	{
		std::string path = "/hyyl/TNotice.php";
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("TaskExplain", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestSingleRechargeRecord()
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetSinglePresentInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("GetSinglePresentInfo", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::RequestDoubleRechargeRecord()
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetDoublePresentInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("GetDoublePresentInfo", cocos2d::network::HttpRequest::Type::GET, url);
	}

	//领取奖励
	////////////////////////////////////////////////////////////////////////////////////////
	void HNPlatformTaskList::SendReceiveNReward(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetRookieTaskAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveN", cocos2d::network::HttpRequest::Type::GET, url);
		_isRefresh = true;
	}

	void HNPlatformTaskList::SendReceiveWReward(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetWeekTaskAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveW", cocos2d::network::HttpRequest::Type::GET, url);
		_isRefresh = true;
	}

	void HNPlatformTaskList::SendReceiveTReward(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetCronJobsAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveT", cocos2d::network::HttpRequest::Type::GET, url);
		_isRefresh = true;
	}

	void HNPlatformTaskList::SendReceiveDReward(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetDailyTaskAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveD", cocos2d::network::HttpRequest::Type::GET, url);
		_isRefresh = true;
	}

	void HNPlatformTaskList::SendReceiveSReward(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetRookieTaskAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveS", cocos2d::network::HttpRequest::Type::GET, url);
		_isRefresh = true;
	}

	void HNPlatformTaskList::SendReceiveSIReward(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetRookieSignTaskAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveSI", cocos2d::network::HttpRequest::Type::GET, url);
		_isRefresh = true;
	}

	void HNPlatformTaskList::SendReceiveSingleRechargeRequest(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetSinglePresentAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveSingleRecharge", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::SendReceiveDoubleRechargeRequest(int TaskID)
	{
		std::string path = StringUtils::format("/hyyl/huodong/GetDoublePresentAward.php?UserID=%d&TaskID=%d", PlatformLogic()->loginResult.dwUserID, TaskID);
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendReceiveDoubleRecharge", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void HNPlatformTaskList::SendWithdrawalRequest(float Num)
	{
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/AwardDrawCash.php");
		std::string requestData = "";
		requestData.append(StringUtils::format("&UserID=%d", PlatformLogic()->loginResult.dwUserID));
		requestData.append(StringUtils::format("&CashNum=%f", Num));

		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("SendWithdrawal", cocos2d::network::HttpRequest::Type::POST, url, requestData);
	}
	////////////////////////////////////////////////////////////////////////////////////////

	void HNPlatformTaskList::dealNewbieData(const std::string& data)
	{
		dealHuodongData(data, _NewbieTaskList, 3);

		screenNewbieTaskList();
		//刷新任务列表之后进行界面刷新
		if (_isRefresh)
		{
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				SendReceiveCallBack = nullptr;
				_isRefresh = false;
			}
		}
	}

	void HNPlatformTaskList::dealWeeklyData(const std::string& data)
	{
		dealHuodongData(data, _WeeklyTaskList, 2);

		screenWeeklyTaskList();
		//刷新任务列表之后进行界面刷新
		if (_isRefresh)
		{
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				SendReceiveCallBack = nullptr;
				_isRefresh = false;
			}
		}
	}

	void HNPlatformTaskList::dealDailyData(const std::string& data)
	{
		dealHuodongData(data, _DailyTaskList, 0);

		screenDailyTaskList();
		//刷新任务列表之后进行界面刷新
		if (_isRefresh)
		{
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				SendReceiveCallBack = nullptr;
				_isRefresh = false;
			}
		}

		if (nullptr != onRequestTaskCallBack)
		{
			onRequestTaskCallBack();
			onRequestTaskCallBack = nullptr;
		}
	}

	void HNPlatformTaskList::dealSubsidiesData(const std::string& data)
	{
		dealHuodongData(data, _SubsidiesList, 4);

		screenSubsidiesList();
		//刷新任务列表之后进行界面刷新
		if (_isRefresh)
		{
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				SendReceiveCallBack = nullptr;
				_isRefresh = false;
			}
		}
	}

	void HNPlatformTaskList::dealSignInData(const std::string& data)
	{
		dealHuodongData(data, _SignInList, 5);

		screenSignInList();
		//刷新任务列表之后进行界面刷新
		if (_isRefresh)
		{
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				SendReceiveCallBack = nullptr;
				_isRefresh = false;
			}
		}
	}

	void HNPlatformTaskList::dealLooseChangeData(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}

		int code = doc["code"].GetInt();
		if (code != 0)
		{
			string str = doc["msg"].GetString();
			GamePromptLayer::create()->showPrompt(str);
			return;
		}

		int Money = doc["Cash"].GetInt();
		_LooseChange = (float)Money / 100.0f;

		int MinStandard = doc["MinStandard"].GetInt();
		_MinStandard = (float)MinStandard / 100.0f;

		int MaxStandard = doc["MaxStandard"].GetInt();
		_MaxStandard = (float)MaxStandard / 100.0f;

		if (onRequestCallBack != nullptr)
		{
			onRequestCallBack();
			onRequestCallBack = nullptr;
		}
	}

	void HNPlatformTaskList::dealPocketRecordData(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}

		int code = doc["code"].GetInt();
		if (code != 0)
		{
			return;
		}

		if (!doc.HasMember("Content"))
		{
			return;
		}

		_RecordList.clear();

		for (size_t i = 0; i < doc["Content"].Size(); i++)
		{
			DATE_RECORD  info;
			rapidjson::Value& Content = doc["Content"][i];
			//记录
			if (Content.HasMember("Title"))
			{
				info.TitleStr = Content["Title"].GetString();
			}
			if (Content.HasMember("AwardMoney"))
			{
				info.Money = Content["AwardMoney"].GetInt();
			}
			_RecordList.push_back(info);
		}

		//刷新任务列表之后进行界面刷新
		if (_isRefresh)
		{
			if (nullptr != onRequestRecordCallBack)
			{
				onRequestRecordCallBack();
				onRequestRecordCallBack = nullptr;
				_isRefresh = false;
			}
		}
	}

	void HNPlatformTaskList::dealTaskExplain(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError())
		{
			return;
		}

		int code = doc["Code"].GetInt();
		if (code != 1)
		{
			return;
		}

		if (!doc.HasMember("List"))
		{
			return;
		}

		rapidjson::Value& Content = doc["List"];
		if (Content.IsArray())
		{
			TaskTitleStr = Content[0]["MContent"].GetString();
		}
	}

	void HNPlatformTaskList::dealHuodongData(const std::string& data, vector<DATE_TASKLIST>& datas, int title_index/* = -1*/)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}

		int code = doc["code"].GetInt();
		if (code != 0)
		{
			return;
		}

		if (!doc.HasMember("Content"))
		{
			return;
		}

		if (doc.HasMember("Subject"))
		{//0-2每日，定时，每周，3-4新手，对局,5新手签到
			if (title_index != -1)
				arr_TaskName[title_index] = doc["Subject"].GetString();
		}

		if (title_index == 5 || title_index == 4)
		{
			if (doc.HasMember("IsShow"))
			{
				int isshow = doc["IsShow"].GetInt();
				if (title_index == 5)
					_isShowSignInR = isshow;
				else if (title_index == 4)
					_isShowNewbieR = isshow;
			}
			else
			{//默认值
				if (title_index == 5)
					_isShowSignInR = true;
				else if (title_index == 4)
					_isShowNewbieR = false;
			}
		}

		//新手对局任务备注
		if (title_index == 4 && doc.HasMember("Memo"))
		{
			_string_Note = doc["Memo"].GetString();
		}

		datas.clear();

		for (size_t i = 0; i < doc["Content"].Size(); i++)
		{
			DATE_TASKLIST info;
			rapidjson::Value& Content = doc["Content"][i];
			//任务信息
			if (Content.HasMember("Title"))
			{
				info.TitleStr = Content["Title"].GetString();
			}
			else
				info.TitleStr = "";
			if (Content.HasMember("TaskID"))
				info.TaskID = Content["TaskID"].GetInt();
			else
				info.TaskID = 0;
			info.Total = Content["Total"].GetInt();
			info.CurrentNum = Content["CurrentNum"].GetInt();
			info.Status = Content["Status"].GetInt();
			if (Content.HasMember("TypeID"))
				info.TypeID = Content["TypeID"].GetInt();
			else
				info.TypeID = 0;
			int Index = 0;
			//判断奖励类型
			if (Content.HasMember("Award"))
			{
				//房卡
				if (Content["Award"].HasMember("Jewel"))
				{
					info.Jewel = Content["Award"]["Jewel"].GetInt();
					if (info.Jewel != 0)
					{
						Index++;
					}
				}
				else
				{
					info.Jewel = 0;
				}
				//金币奖励
				if (Content["Award"].HasMember("WalletMoney"))
				{
					info.WalletMoney = Content["Award"]["WalletMoney"].GetInt();
					if (info.WalletMoney != 0)
					{
						Index++;
					}
				}
				else
				{
					info.WalletMoney = 0;
				}
				if (Content["Award"].HasMember("Money"))
				{
					info.Money = Content["Award"]["Money"].GetDouble();
					if (info.Money != 0)
					{
						Index++;
					}
				}
				else
				{
					info.Money = 0;
				}
				if (Content["Award"].HasMember("Lotteries"))
				{
					info.Lotteries = Content["Award"]["Lotteries"].GetInt();
					if (info.Lotteries != 0)
					{
						Index++;
					}
				}
				else
				{
					info.Lotteries = 0;
				}
				if (Content["Award"].HasMember("ShoppingTicket"))
				{
					info.ShoppingTicket = Content["Award"]["ShoppingTicket"].GetInt();
					if (info.ShoppingTicket != 0)
					{
						Index++;
					}
				}
				else
				{
					info.ShoppingTicket = 0;
				}
				if (Content["Award"].HasMember("PhoneBill"))
				{
					info.PhoneBill = Content["Award"]["PhoneBill"].GetInt();
					if (info.PhoneBill != 0)
					{
						Index++;
					}
				}
				else
				{
					info.PhoneBill = 0;
				}
				if (Content["Award"].HasMember("Object") && Content["Award"]["Object"].IsInt())
				{
					info.Object = Content["Award"]["Object"].GetInt();
					if (info.Object != 0)
					{
						Index++;
					}
				}
				else
				{
					info.Object = 0;
				}
			}
			info.AwardNum = Index;
			datas.push_back(info);
		}
	}

	void HNPlatformTaskList::dealTimingData(const std::string& data)
	{
		dealHuodongData(data, _TimingTaskList, 1);

		screenTimingTaskList();

		//刷新任务列表之后进行界面刷新
		if (_isRefresh)
		{
			if (nullptr != SendReceiveCallBack)
			{
				SendReceiveCallBack();
				SendReceiveCallBack = nullptr;
				_isRefresh = false;
			}
		}
	}

	void HNPlatformTaskList::screenNewbieTaskList()
	{
		if (_NewbieTaskList.size() <= 0)
		{
			return;
		}
		_bIsNewbie = false;
		for (int i = 0; i < _NewbieTaskList.size(); i++)
		{
			if (_NewbieTaskList[i].Status == 1)
			{
				_bIsNewbie = true;
				break;
			}
		}
	}

	void HNPlatformTaskList::screenWeeklyTaskList()
	{
		if (_WeeklyTaskList.size() <= 0)
		{
			return;
		}
		_bIsWeekly = false;
		for (int i = 0; i < _WeeklyTaskList.size(); i++)
		{
			if (_WeeklyTaskList[i].Status == 1)
			{
				_bIsWeekly = true;
				break;
			}
		}
	}

	void HNPlatformTaskList::screenTimingTaskList()
	{
		if (_TimingTaskList.size() <= 0)
		{
			return;
		}
		_bIsTiming = false;
		for (int i = 0; i < _TimingTaskList.size(); i++)
		{
			if (_TimingTaskList[i].Status == 1)
			{
				_bIsTiming = true;
				break;
			}
		}
	}

	void HNPlatformTaskList::screenDailyTaskList()
	{
		if (_DailyTaskList.size() <= 0)
		{
			return;
		}
		_bIsDaily = false;
		for (int i = 0; i < _DailyTaskList.size(); i++)
		{
			if (_DailyTaskList[i].Status == 1)
			{
				_bIsDaily = true;
				break;
			}
		}
	}

	void HNPlatformTaskList::screenSubsidiesList()
	{
		if (_SubsidiesList.size() <= 0)
		{
			return;
		}
		_bIsSubsidies = false;
		for (int i = 0; i < _SubsidiesList.size(); i++)
		{
			if (_SubsidiesList[i].Status == 1)
			{
				_bIsSubsidies = true;
				break;
			}
		}
	}

	void HNPlatformTaskList::screenSignInList()
	{
		if (_SignInList.size() <= 0)
		{
			return;
		}
		_bIsSignIn = false;
		for (int i = 0; i < _SignInList.size(); i++)
		{
			if (_SignInList[i].Status == 1)
			{
				_bIsSignIn = true;
				break;
			}
		}
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getNewbieTaskList()
	{
		return _NewbieTaskList;
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getWeeklyTaskList()
	{
		return _WeeklyTaskList;
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getTimingTaskList()
	{
		return _TimingTaskList;
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getDailyTaskList()
	{
		return _DailyTaskList;
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getSubsidiesTaskList()
	{
		return _SubsidiesList;
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getSignInTaskList()
	{
		return _SignInList;
	}

	vector<DATE_RECORD>& HNPlatformTaskList::getRecordList()
	{
		return _RecordList;
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getSingleRechargeList()
	{
		return _SingleRechargeList;
	}

	vector<DATE_TASKLIST>& HNPlatformTaskList::getDoubleRechargeList()
	{
		return _DoubleRechargeList;
	}

	bool HNPlatformTaskList::getNewbieState()
	{
		return _bIsNewbie;
	}

	bool HNPlatformTaskList::getWeeklyState()
	{
		return _bIsWeekly;
	}

	bool HNPlatformTaskList::getTimingState()
	{
		return _bIsTiming;
	}

	bool HNPlatformTaskList::getDailyState()
	{
		return _bIsDaily;
	}

	bool HNPlatformTaskList::getSubsidiesState()
	{
		return _bIsSubsidies;
	}

	bool HNPlatformTaskList::getSignInState()
	{
		return _bIsSignIn;
	}

	//获取大厅新手红包可领取状态
	bool HNPlatformTaskList::GetNewbieAllState()
	{
		if (_bIsNewbie || _bIsSubsidies || _bIsSignIn)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//获取大厅任务可领取状态
	bool HNPlatformTaskList::GetTaskListAllState()
	{
		if (_bIsDaily || _bIsTiming || _bIsWeekly)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	float HNPlatformTaskList::getLooseChange()
	{
		return _LooseChange;
	}

	float HNPlatformTaskList::getMinStandard()
	{
		return _MinStandard;
	}

	float HNPlatformTaskList::getMaxStandard()
	{
		return _MaxStandard;
	}

	bool HNPlatformTaskList::getShowNewbieR()
	{
		return _isShowNewbieR;
	}

	bool HNPlatformTaskList::getShowSignInR()
	{
		return _isShowSignInR;
	}

	string HNPlatformTaskList::getTaskNameByIndex(int index)
	{
		if (arr_TaskName[index].empty())
		{
			string str = "";
			return str;
		}
		return arr_TaskName[index];
	}

	string HNPlatformTaskList::getTaskNote()
	{
		return _string_Note;
	}

	string HNPlatformTaskList::getTaskTitleStr()
	{
		return TaskTitleStr;
	}

	void HNPlatformTaskList::dealReceiveResponse(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}

		int code = doc["code"].GetInt();
		string str = doc["msg"].GetString();
		GamePromptLayer::create()->showPrompt(str);
	}
};

