//俱乐部成员第三方头像存储（bluce_lickey  2020.2.24）

#ifndef __HN_HNPlatformClubData_H__
#define __HN_HNPlatformClubData_H__

#include "cocos2d.h"
#include <vector>
#include "network/HttpClient.h"
#include "HNHttp/HNHttp.h"
#include "HNNetProtocol/HNPlatformMessage.h"
#include "HNSocket/HNSocketMessage.h"

USING_NS_CC;

namespace HN
{
	struct STeahouseInfo_Club
	{
		SClubTeahouseInfo info;
		int			clubId;
		UINT			clubFlag;
	};

	struct STeahouseDeskInfo_Club
	{
		SClubTeahouseDeskInfo info;
		int			clubId;
	};

	class HNPlatformClubData : public HNHttpDelegate
	{
	public:
		// 获取单例
		static HNPlatformClubData* getInstance();

		// 初始化
		bool init();

		//更新回调
		typedef std::function<void()> RequestCallBack;
		RequestCallBack		onRequestCallBack = nullptr;

		//解散楼层回调
		typedef std::function<void(int TeaHouseID)> DissolveCallBack;
		DissolveCallBack		onDissolveCallBack = nullptr;

		//黑名单回调
		typedef std::function<void(int ClubID)> BlackListUserCallBack;
		BlackListUserCallBack	onBlackListUserCallBack = nullptr;

	private:

		// 构造函数
		HNPlatformClubData();

		// 析构函数
		virtual ~HNPlatformClubData();

		//HTTP请求结果
		void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData);

		//获取俱乐部成员数据
		bool getClubMemberInfoResult(HNSocketMessage* SocketMessage);

		//查询用户头像数据
		void onResponseHeadUrl(int iUserID);

		//添加茶楼楼层以及桌子网络消息监听事件
		void AddTeaHouseNetWorkNewsEventListener();

		//移除茶楼楼层以及桌子网络消息监听事件
		void RemoveTeaHouseNetWorkNewsEventListener();

		//获取楼层，桌子数据
		bool getTeaHouseInfoResult(HNSocketMessage* SocketMessage);

		//更新楼层，桌子数据
		bool getTeaHouseInfoUpdate(HNSocketMessage* SocketMessage);

		//新增茶楼楼层创建（解散）
		bool getTeaHouseFloorUpdate(HNSocketMessage* SocketMessage);

		//刷新俱乐部桌子（补桌使用）
		bool getTeaHouseSupplementUpdate(HNSocketMessage* SocketMessage);

		//监听俱乐部错误消息返回
		bool getTeaHouseErrorUpdate(HNSocketMessage* SocketMessage);

		//筛选桌子数据处理
		void sortTeaHouseTableInfo();

	private:
		//俱乐部用户列表
		std::vector<int> Vec_UserListInfo;
		std::map<int, int> map_UserID;
		std::map<int, std::string> map_UserHeadUrl;
		//std::map<int, vector<SClubTeahouseInfo*>> map_TeahouseInfo;
		std::vector<STeahouseInfo_Club> Vec_TeaHouseInfo;
		std::vector<STeahouseDeskInfo_Club> Vec_TeaHouseTable;
		std::vector<int> Vec_deskList;

		int Index_HeadCount;
		bool isHaveNew;

		bool isNetEventListener = false;
	public:
		//根据俱乐部ID查询用户列表头像数据
		void onQueryClubUserList(int ClubID);

		//根据用户ID查找用户头像数据
		std::string getUserHeadUrlByUserID(int UserID);

		//初始化网络监听
		void StartNetEventListener();

		//移除网络监听
		void RemoveNetEventListener();

		//获取当前楼层数据
		std::vector<SClubTeahouseInfo> getTeaHouseInfo(int ClubID);

		//获取当前茶楼最大人数
		int getTeaHouseInfoPlayer(int ClubID, int TeaHouseID);

		//获取当前茶楼最大局数
		int getTeaHouseInfoMaxCount(int ClubID, int TeaHouseID);

		//获取当前茶楼房间ID
		int getTeaHouseInfoRoomID(int ClubID, int TeaHouseID);

		//获取当前茶楼游戏ID
		int getTeaHouseInfoGameID(int ClubID, int TeaHouseID);

		//获取当前茶楼楼层数据
		SClubTeahouseInfo getTeaHouseInfoByTeaHouseID(int ClubID, int TeaHouseID);

		//获取桌子数据
		std::vector<SClubTeahouseDeskInfo> getTeaHouseTable(int ClubID);

		//获取当前茶楼游戏中，等待中的桌子信息
		std::vector<SClubTeahouseDeskInfo> getTeaHouseStardTable(int ClubID);

		//获取桌子信息（ClubID以及TeaHouseID）
		std::vector<SClubTeahouseDeskInfo> getTeaHouseTable(int ClubID, int TeaHouseID);

		//获取当前茶楼房间状态（暂停或者需要恢复）
		UINT getTeaHouseInfoByState(int ClubID);

		//通知刷新茶楼信息
		void TeaHouseUpdate();

		//修改楼层名字
		void setTeaHouseNameByTeaHouseID(int ClubID, int TeaHouseID, const std::string& requestName);

		//获取桌子总数
		int getTeaHousTableCount();
	};

#define  PlatformClubData()	HNPlatformClubData::getInstance()
//#define  RoomInfoModule()	HNRoomInfoModule::getInstance()
}


#endif