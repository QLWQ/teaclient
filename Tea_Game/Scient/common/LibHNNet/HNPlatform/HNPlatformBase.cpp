﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformBase.h"
#include "HNPlatform/HNPlatformLogic.h"

namespace HN 
{

	HNPlatformBase::HNPlatformBase(void)
	{
	}

	HNPlatformBase::~HNPlatformBase(void)
	{
	}

	void HNPlatformBase::onEnter()
	{
		HNScene::onEnter();
		PlatformLogic()->addObserver(this);
	}

	void HNPlatformBase::onExit()
	{
		HNScene::onExit();
		PlatformLogic()->removeObserver(this);
	}
}