﻿/****************************************************************************
Copyright (c) 2014-2016 Beijing TianRuiDiAn Network Technology Co.,Ltd.
Copyright (c) 2014-2016 ShenZhen Redbird Network Polytron Technologies Inc.

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_MatchMessageDelegate_H__
#define __HN_MatchMessageDelegate_H__

#include "HNBaseType.h"
#include "HNNetProtocol/HNProtocolExport.h"
#include <vector>

namespace HN
{	
	class IMatchMessageDelegate : public IGameMatchMessageDelegate
	{		
	public:
		// 网络断开
		virtual void I_R_M_DisConnect() {}
	};
}

#endif	//__HN_MatchMessageDelegate_H__

