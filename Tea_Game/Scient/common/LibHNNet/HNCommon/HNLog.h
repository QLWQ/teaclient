﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNLOG_H__
#define __HN_HNLOG_H__

#include "cocos2d.h"
#include "log4z.h"
#include "bugly/CrashReport.h"

USING_NS_CC;

// cocos2d debug
#if !defined(COCOS2D_DEBUG) || COCOS2D_DEBUG == 0
#define HNLOG(...) do {} while (0)
#define HNLOG_DEBUG(...)       do {} while (0)
#define HNLOG_ERROR(...)   do {} while (0)
#define HNLOG_INFO(...)  do {} while (0)
#define HNLOG_WARNING(...)   do {} while (0)

#elif (COCOS2D_DEBUG == 1)
#define HNLOG(format,...)			HNLog::logDebug("%s (%s:%d) " format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_DEBUG(format,...)		HNLog::logDebug("Text >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_ERROR(format,...)		HNLog::logError("Error >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_INFO(format,...)		HNLog::logInfo("Information >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_WARNING(format,...)	HNLog::logWarning("Warning >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)

#elif COCOS2D_DEBUG > 1
#define HNLOG(format,...)			HNLog::logDebug("%s (%s:%d) " format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_DEBUG(format,...)		HNLog::logDebug("Text >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_ERROR(format,...)		HNLog::logError("Error >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_INFO(format,...)		HNLog::logInfo("Information >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_WARNING(format,...)	HNLog::logWarning("Warning >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define HNLOG(...) 
#define HNLOG_DEBUG(...)	
#define HNLOG_ERROR(...)		
#define HNLOG_INFO(...)		
#define HNLOG_WARNING(...)	
#endif // COCOS2D_DEBUG

/*
 *	@brief	bugly相关宏定义
 *	@HNLOG_EXCEPTION	主动上传错误信息到bugly后台
 *	@HNLOG_BUGLY_LOG	应用崩溃时上传日志信息到bugly后台
 */
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define HNLOG_EXCEPTION(format,...) CrashReport::reportException(1, "report", StringUtils::format(format, ##__VA_ARGS__).c_str(), StringUtils::format("%s (%s:%d) ", __FILE__, __FUNCTION__, __LINE__).c_str())
#define HNLOG_BUGLY_LOG(format,...) CrashReport::log(CrashReport::CRLogLevel::Error, "log", format, ##__VA_ARGS__)
#else
#define HNLOG_EXCEPTION(format,...) HNLog::logDebug("%s (%s:%d) " format, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define HNLOG_BUGLY_LOG(format,...) HNLog::logDebug("Text >> (%s:%d) " format, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#endif

// 启动日志
#define HNLOGEX_START(path) { \
	zsummer::log4z::ILog4zManager::getInstance()->setLoggerPath(LOG4Z_MAIN_LOGGER_ID, path.c_str()); \
	zsummer::log4z::ILog4zManager::getInstance()->start(); \
	path = "************log store path:" + path; \
	LOGE(path.c_str()); \
}

// Trace
#define HNLOGEX_TRACE(format, ...) { \
	char buf[MAX_LOG_LENGTH] = { 0 }; \
	sprintf(buf, format, ##__VA_ARGS__); \
	strcat(buf, "\n"); \
	LOGT(buf); \
}


// Debug
#define HNLOGEX_DEBUG(format, ...) { \
	char buf[MAX_LOG_LENGTH] = { 0 }; \
	sprintf(buf, format, ##__VA_ARGS__); \
	strcat(buf, "\n"); \
	LOGD(buf); \
}


// Info
#define HNLOGEX_INFO(format, ...) { \
	char buf[MAX_LOG_LENGTH] = { 0 }; \
	sprintf(buf, format, ##__VA_ARGS__); \
	strcat(buf, "\n"); \
	LOGI(buf); \
}

// Warn
#define HNLOGEX_WARN(format, ...) { \
	char buf[MAX_LOG_LENGTH] = { 0 }; \
	sprintf(buf, format, ##__VA_ARGS__); \
	strcat(buf, "\n"); \
	LOGW(buf); \
}

// Error
#define HNLOGEX_ERROR(format, ...) { \
	char buf[MAX_LOG_LENGTH] = { 0 }; \
	sprintf(buf, format, ##__VA_ARGS__); \
	strcat(buf, "\n"); \
	LOGE(buf); \
}

// Alarm
#define HNLOGEX_ALARM(format, ...) { \
	char buf[MAX_LOG_LENGTH] = { 0 }; \
	sprintf(buf, format, ##__VA_ARGS__); \
	strcat(buf, "\n"); \
	LOGA(buf); \
}

// Fatal
#define HNLOGEX_FATAL(format, ...) { \
	char buf[MAX_LOG_LENGTH] = { 0 }; \
	sprintf(buf, format, ##__VA_ARGS__); \
	strcat(buf, "\n"); \
	LOGF(buf); \
}

namespace HN 
{
	enum HNLOG_LEVEL
	{
		LOG_DEBUG = 0,
		LOG_INFO,
		LOG_WARN,
		LOG_ERROR
	};

	class HNLog
	{
	public:
		static void logDebug(const char* format, ...);
		static void logError(const char* format, ...);
		static void logInfo(const char* format, ...);
		static void logWarning(const char* format, ...);
		static void logFile(const char* format, ...);

		/**
		*    @brief 初始化
		*
		*    @param appId 注册应用时，Bugly分配的AppID
		*    @param debug 是否开启Debug信息打印，默认关闭，开启则会打印SDK的调试信息
		*    @param level 是否开启崩溃时自定义日志的上报，默认值为 {@link CRLogLevel:Off}，即关闭。设置为其他的值，即会在崩溃时上报自定义日志。如设置为CRLogLevel:Warning，则会上报CRLogLevel:Warning、CRLogLevel:Error的日志
		*/
		static void initBugly(const char* appId, bool debug, CrashReport::CRLogLevel level);
	};
};

#endif		//__HN_HNLOG_H__
