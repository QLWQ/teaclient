/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_REFVALUE_H__
#define __HN_REFVALUE_H__
#include "cocos/base/CCRef.h"
namespace HN
{
	template<typename T>
	class RefValue :public cocos2d::Ref
	{
	public:
		typedef T value_type;

	public:
		RefValue(typename std::remove_const<value_type>::type &vParam) :_value(vParam)
		{}

		inline value_type getValue()const{ return _value; }
		void setValue(value_type &vParam){ _value = vParam; }
		value_type& getValueRef(){ return _value; }
		void setValueRef(value_type &vParam)
		{
			_value = vParam;
		}
	private:
		value_type _value;
	};

	template<typename _Ty>
	struct get_type
	{
		typedef _Ty type;
	};
#define CAST_REFVALUE(Type,Value) dynamic_cast<RefValue<typename get_type<Type >::type >*>((Value))
}
#endif