﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNSocketMessage.h"
#include "HNCommon/HNLog.h"
#include "HNCommon/HNMemoryPool.h"
#include "recast/fastlz/fastlz.h"

namespace HN
{
	//////////////////////////////////////////////////////////////////////////
	//HNMemPool gMemPool(sizeof(HNSocketMessage), 32, 1024);

	HNSocketMessage* HNSocketMessage::getMessage()
	{
		return new HNSocketMessage();
	}

	void HNSocketMessage::releaseMessage(HNSocketMessage* message)
	{
		HN_SAFE_DELETE(message);
	}

	HNSocketMessage::HNSocketMessage() : objectSize(0), socketStatus(SocketStatus_UNKNOW)
	{
		::memset(object, 0x0, sizeof(object));
		::memset(&messageHead, 0x0, sizeof(object));
	}

	HNSocketMessage::~HNSocketMessage()
	{

	}

	/*void *HNSocketMessage::operator new(std::size_t ObjectSize)
	{
	return gMemPool.get();
	}

	void HNSocketMessage::operator delete(void *ptrObject)
	{
	gMemPool.release(ptrObject);
	}*/

	void HNSocketMessage::setMessage(const NetMessageHead* head, CHAR* obj, INT Size)
	{
		messageHead = *head;

		// 压缩处理
		if ((head->bDataRule & 0x01) != 0x00)
		{
			objectSize = fastlz_decompress(obj, Size, object, MAX_BUFF);
			messageHead.uMessageSize = sizeof(NetMessageHead) + objectSize;
		}
		else
		{
			objectSize = Size;
			memcpy(object, obj, objectSize);
		}

		CHAR buf[16] = { 0 };
		sprintf(buf, "%u_%u", messageHead.bMainID, messageHead.bAssistantID);
		strKey.assign(buf);
	}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	void HNSocketMessage::print(wchar_t* outBuf, const wchar_t* typeName, UDT_MembersDataMap& memberDataMap, const char* object, wchar_t* name /*= L""*/, int depth /*= 0*/, long index/* = -1*/)
	{
		auto itMap = memberDataMap.find(typeName);
		if (itMap == memberDataMap.end())
			return;

		wchar_t buf[100] = { 0 };
		for (int i = 0; i < depth + 1; i++)
		{
			wcscat_s(buf, L"    ");
		}

		wchar_t temp[256] = { 0 };
		if (index >= 0)  //数组索引
			wsprintf(temp, L"%s%s[%d] ( %s )   =\n", buf, name, index, typeName);
		else
			wsprintf(temp, L"%s%s ( %s )   =\n", buf, name, typeName);
		wcscat(outBuf, temp);

		memset(temp, 0, sizeof(temp));
		wsprintf(temp, L"%s{\n", buf, name, typeName);
		wcscat(outBuf, temp);

		memset(buf, 0, sizeof(buf));
		for (int i = 0; i < depth + 2; i++)
		{
			wcscat_s(buf, L"    ");
		}
		auto parent = itMap->first;
		for (auto iter = itMap->second->begin(); iter != itMap->second->end(); ++iter)
		{
			auto data = *iter;
			if (data->is_static || data->is_const)  //静态变量或者常量跳过
				continue;
			if (data->data_kind != 7)  //仅打印成员变量信息
				continue;

			if (data->is_array)
			{//数组		
				if ((data->udt_kind == eUDT_KIND::kind_class || data->udt_kind == eUDT_KIND::kind_struct)
					&& wcslen(data->type) > 0)
				{//结构数组
					auto itTargetMap = memberDataMap.find(data->type);
					if (itTargetMap != memberDataMap.end())
					{
						int size = data->size;
						int count = 1;
						std::vector<unsigned long> vecs;
						if (data->array_elems[0] > 0)
						{//数组元素数量计算
							for (int i = 0; i < sizeof(data->array_elems) / sizeof(unsigned long); i++)
							{
								if (data->array_elems[i] == 0)
									break;
								count *= data->array_elems[i];
								vecs.push_back(data->array_elems[i]);
							}
						}
						wchar_t buf2[100] = { 0 };
						for (auto ir_resv = vecs.rbegin(); ir_resv != vecs.rend(); ir_resv++)
						{
							wsprintf(buf2 + wcslen(buf2), L"[%d]", *ir_resv);
						}

						memset(temp, 0, sizeof(temp));
						wsprintf(temp, L"%s%s%s ( %s )  =  \n", buf, data->name, buf2, data->type);
						wcscat(outBuf, temp);

						memset(temp, 0, sizeof(temp));
						wsprintf(temp, L"%s{\n", buf);
						wcscat(outBuf, temp);

						for (size_t i = 0; i < count; i++)
						{
							print(outBuf, data->type, memberDataMap, object + data->offset, data->name, depth + 2, i);
						}

						memset(temp, 0, sizeof(temp));
						wsprintf(temp, L"%s}\n", buf);
						wcscat(outBuf, temp);
					}

					continue;
				}
				printBaseArrayType(data, outBuf, buf, object);
			}
			else
			{//非数组
				if ((data->udt_kind == eUDT_KIND::kind_class || data->udt_kind == eUDT_KIND::kind_struct)
					&& wcslen(data->type) > 0)
				{//结构体或者类,并且含有类型信息
					auto itTargetMap = memberDataMap.find(data->type);
					if (itTargetMap != memberDataMap.end())
						print(outBuf, data->type, memberDataMap, object + data->offset, data->name, depth + 1);

					continue;
				}
				printBaseType(data, outBuf, buf, object);
			}
		}

		memset(buf, 0, sizeof(buf));
		for (int i = 0; i < depth + 1; i++)
		{
			wcscat(buf, L"    ");
		}

		memset(temp, 0, sizeof(temp));
		wsprintf(temp, L"%s}\n", buf);
		wcscat(outBuf, temp);
	}


	//基本类型数组
	void HNSocketMessage::printBaseArrayType(const st_PDB_MemberData* data, wchar_t* outBuf, const wchar_t* preBuf, const char* object)
	{
		int size = data->size;
		int count = 1;
		std::vector<unsigned long> vecs;
		if (data->array_elems[0] > 0)
		{//数组元素数量计算
			for (int i = 0; i < sizeof(data->array_elems) / sizeof(unsigned long); i++)
			{
				if (data->array_elems[i] == 0)
					break;
				count *= data->array_elems[i];

				vecs.push_back(data->array_elems[i]);
			}
		}
		wchar_t buf[100] = { 0 };
		for (auto ir_resv = vecs.rbegin(); ir_resv != vecs.rend(); ir_resv++)
		{
			wsprintf(buf + wcslen(buf), L"[%d]", *ir_resv);
		}
		wchar_t temp[1024 * 5] = { 0 };
		memset(temp, 0, sizeof(temp));
		wsprintf(temp, L"%s%s%s=  \n", preBuf, data->name, buf);
		wcscat(outBuf, temp);

		memset(temp, 0, sizeof(temp));
		wsprintf(temp, L"%s{\n", preBuf);
		wcscat(outBuf, temp);

		wchar_t tempBuf[1024 * 5] = { 0 };
		if (wcscmp(data->type, L"bool") == 0)
		{
			for (size_t i = 0; i < count; i++)
			{
				bool value = *(bool*)(object + data->offset + i * size);
				if (i != 0 && i % 10 == 0)
				{
					wchar_t tmp[256] = { 0 };
					wsprintf(tmp, L"\n    %s", preBuf);
					wcscat(tempBuf, tmp);
				}
				wcscat(tempBuf, value ? L"true  " : L"false ");
			}
		}
		else if (wcscmp(data->type, L"unsigned char") == 0)
		{
			//_snwprintf(tempBuf, size * count, L"%hs", (unsigned char*)(object + data->offset));
			formatBaseTypePrint<unsigned char>(data, L"%-3d ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"char") == 0
			|| wcscmp(data->type, L"signed char") == 0)
		{
			//_snwprintf(tempBuf, size * count, L"%hs", (char*)(object + data->offset));
			formatBaseTypePrint<char>(data, L"%-3d ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"short") == 0)
		{
			formatBaseTypePrint<short>(data, L"%-5d ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"unsigned short") == 0)
		{
			formatBaseTypePrint<unsigned short>(data, L"%-5d ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"int") == 0)
		{
			formatBaseTypePrint<int>(data, L"%-5d ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"unsigned int") == 0)
		{
			formatBaseTypePrint<unsigned int>(data, L"%-5u ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"__int64") == 0)
		{
			formatBaseTypePrint<__int64>(data, L"%-5ld ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"unsigned __int64") == 0)
		{
			formatBaseTypePrint<unsigned __int64>(data, L"%-5lld ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"float") == 0)
		{
			formatBaseTypePrint<float>(data, L"%f ", preBuf, tempBuf, object);
		}
		else if (wcscmp(data->type, L"double") == 0)
		{
			formatBaseTypePrint<double>(data, L"%lf ", preBuf, tempBuf, object);
		}
		else
		{
			//输出未知的类型信息
			wprintf(L"type:%s,size:%d,name:%s", data->type, data->size, data->name);
		}

		memset(temp, 0, sizeof(temp));
		wsprintf(temp, L"    %s%s", preBuf, tempBuf);
		wcscat(outBuf, temp);

		memset(temp, 0, sizeof(temp));
		wsprintf(temp, L"\n%s}\n", preBuf);
		wcscat(outBuf, temp);
	}

	//基本类型
	void HNSocketMessage::printBaseType(const st_PDB_MemberData* data, wchar_t* outBuf, const wchar_t* preBuf, const char* object)
	{
		wchar_t temp[256] = { 0 };
		memset(temp, 0, sizeof(temp));
		if (data->is_enum)
		{//枚举
			wsprintf(temp, L"%s%-15s=  %d\n", preBuf, data->name, *(int*)(object + data->offset));
		}
		else
		{//基本类型
			if (wcscmp(data->type, L"bool") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %s\n", preBuf, data->name, (*(bool*)(object + data->offset)) ? L"true" : L"false");
			}
			else if (wcscmp(data->type, L"char") == 0
				|| wcscmp(data->type, L"signed char") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %d\n", preBuf, data->name, *(char*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"unsigned char") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %d\n", preBuf, data->name, *(unsigned char*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"short") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %d\n", preBuf, data->name, *(short*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"unsigned short") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %d\n", preBuf, data->name, *(unsigned short*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"int") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %d\n", preBuf, data->name, *(int*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"unsigned int") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %u\n", preBuf, data->name, *(unsigned int*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"__int64") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %ld\n", preBuf, data->name, *(__int64*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"unsigned __int64") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %lld\n", preBuf, data->name, *(unsigned __int64*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"float") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %f\n", preBuf, data->name, *(float*)(object + data->offset));
			}
			else if (wcscmp(data->type, L"double") == 0)
			{
				wsprintf(temp, L"%s%-15s=  %lf\n", preBuf, data->name, *(double*)(object + data->offset));
			}
			else
			{
				//输出未知的类型信息
				wprintf(L"type:%s,size:%d,name:%s", data->type, data->size, data->name);
			}
		}
		wcscat(outBuf, temp);
	}
#endif
}