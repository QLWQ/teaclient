﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_SocketExport_H__
#define __HN_SocketExport_H__

// 网络通信接口
#include "HNSocket/HNSocket.h"
#include "HNSocket/HNSocketEventDelegate.h"
#include "HNSocket/HNSocketLogic.h"
#include "HNSocket/HNSocketMessage.h"
#include "HNSocket/HNSocketMessageDelegate.h"
#include "HNSocket/HNSocketProtocolData.h"
#include "HNSocket/HNSocketThread.h"

#endif	//__HN_SocketExport_H__
