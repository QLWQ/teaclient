﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNSocketProtocolData.h"

namespace HN {
	UINT HNSocketProtocolData::PlatformCheckCode = INVALID_VALUE;
	UINT HNSocketProtocolData::GameCheckCode = INVALID_VALUE;
}
