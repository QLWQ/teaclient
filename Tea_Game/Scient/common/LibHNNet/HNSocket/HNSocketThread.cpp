﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNSocketThread.h"
#include "HNSocket.h"
#include "HNSocketMessage.h"
#include "HNSocketProtocolData.h"
#include "HNSocketEventDelegate.h"
#include "HNCommon/HNLog.h"
#include "HNCommon/HNCommonMarco.h"
#include "recast/fastlz/fastlz.h"
#include "HNExternal/MD5/MD5.h"

namespace HN
{
	static const INT TCP_BUFSIZE_READ = 8192;
	static const INT TCP_RECV_BUFFER_SIZE = 100*1024;
	static const INT TCP_SEND_BUFFER_SIZE = 16400;

	HNSocketThread::HNSocketThread(ISocketEventDelegate* delegate)
		: _recvThreadExit(true)
		, _heartBeatThreadExit(true)
		, _connected(false)
		, _delegate(delegate)
	{
		// RC4数据加密秘钥初始化
		std::string key = MD5_CTX::MD5String(std::to_string(getSecrectKey()));
		_rc4.init((unsigned char*)(key.c_str()), key.size());

		_socket = new HNSocket();
		_socketDataCacheQueue = new HNSocketDataCacheQueue();	
		_socketMessageQueue = new HNSocketMessageQueue();
	}

	HNSocketThread::~HNSocketThread(void)
	{
		close();

		clearMessageCache();

		HN_SAFE_DELETE(_socketMessageQueue);
		HN_SAFE_DELETE(_socketDataCacheQueue);
		HN_SAFE_DELETE(_socket);
	}

	bool HNSocketThread::openWithIp(const CHAR* ip, INT port)
	{
		assert(ip != nullptr);
		if (!_connected)
		{
			_socket->setIp(ip);
			_socket->setPort(port);
			std::thread recvThread(std::bind(&HNSocketThread::onSocketReadThread, this));
			recvThread.detach();
		}
		return true;
	}

	bool HNSocketThread::openWithHost(const CHAR* host, INT port)
	{
		assert(host != nullptr);
		if (!_connected)
		{
			_socket->setHost(host);
			_socket->setPort(port);
			std::thread recvThread(std::bind(&HNSocketThread::onSocketReadThread, this));
			recvThread.detach();
		}
		return true;
	}

	bool HNSocketThread::initSocket()
	{
		bool ret = false;
		do
		{
			// create socket
			if (!_socket->create())
			{
				socketErrMsgNotify();
				break;
			}

			// set recv buffer
			_socket->setSoRcvbuf(TCP_RECV_BUFFER_SIZE);

			// set send buffer
			_socket->setSoSendbuf(TCP_SEND_BUFFER_SIZE);

			INT err = _socket->connect();

			if (HNSocket::TCP_CONNECT_SUCCESS == err)
			{
				_connected = true;
				_recvThreadExit = false;
				ret = true;

				HNSocketProtocolData data;
				data.createPackage(MDM_CONNECT, ASS_NET_TEST_1, 0);
				send(data.getPackage(), data.getPackageSize());
			}
			else
			{
				socketErrMsgNotify();
			}
		} while (0);

		return ret;
	}

	void HNSocketThread::clear()
	{
		_connected = false;
		_recvThreadExit = true;
		_heartBeatThreadExit = true;
		_socketDataCacheQueue->clear();
	}

	bool HNSocketThread::close()
	{
		if (_connected)
		{
			HNLOGEX_DEBUG("%s >> onSocketReadThread >> close.", _tag.c_str());
			_connected = false;
			_recvThreadExit = true;
			_heartBeatThreadExit = true;
			_socket->close();
			return true;
		}
		return false;
	}

	/*
	 * 说明：压缩和加密，都只针对数据部分，包头不变，同时存在压缩和加密是，先压缩，再加密
	 */
	INT HNSocketThread::send(char* object, INT objectSize)
	{
		size_t headSize = sizeof(NetMessageHead);

		int cacheSize = (objectSize - headSize) * 1.05 + headSize;
		char* pBuff = (char*)malloc(sizeof(char) * cacheSize);
		memset(pBuff, 0x00, cacheSize);
		int buffSize = 0;

		// 加密数据部分-仅仅有数据时才需要加密
		if (objectSize > headSize)
		{
			// 超过200字节压缩
			if (objectSize - headSize > 200)
			{
				// 包头
				memcpy(pBuff, object, headSize);
				NetMessageHead* pHead = (NetMessageHead*)pBuff;
				pHead->bDataRule = 1;

				buffSize = fastlz_compress(object + headSize, objectSize - headSize, pBuff + headSize);
				if (buffSize <= 0)
				{
					HNLOGEX_ERROR("compress error.");
					return -1;
				}
				else
				{
					buffSize += headSize;
				}
				pHead->uMessageSize = buffSize;
			}
			else
			{
				memcpy(pBuff, object, objectSize);
				buffSize = objectSize;
			}
			
			_rc4.encrpyt((unsigned char*)pBuff + headSize, buffSize - headSize);
		}
		else
		{
			memcpy(pBuff, object, objectSize);
			buffSize = objectSize;
		}

		if (_connected)
		{
			int code = _socket->send(pBuff, buffSize);

			if (-1 == code) {
				HNLOGEX_DEBUG("send error : %d", code);
				disconnectMsgNotify();
			}
			else
			{
				//HNLOGEX_DEBUG("send error : %d", code);
			}
			return code;
		}
		HNLOGEX_DEBUG("error: network not connected");
		return -1;
	}

	/*
	INT HNSocketThread::send(char* object, INT objectSize)
	{
		// 去包头数据
		size_t headSize = sizeof(NetMessageHead);

		// 加密数据部分-仅仅有数据时才需要加密
		if (objectSize > headSize)
		{
			// RC4
			_rc4.encrpyt((unsigned char*)object + headSize, objectSize - headSize);
		}

		if (_connected)
		{
			int code = _socket->send(object, objectSize);
			if (-1 == code) {
				HNLOGEX_DEBUG("send error : %d", code);
				disconnectMsgNotify();
			}
			else
			{
				//HNLOGEX_DEBUG("send error : %d", code);
			}
			return code;
		}
		HNLOGEX_DEBUG("error: network not connected");
		return -1;
	}
	*/

	void HNSocketThread::onSocketReadThread()
	{
		if (!initSocket()) return;

		HNLOGEX_DEBUG("%s >> onSocketReadThread >> begin.", _tag.c_str());
		
		while (true)
		{
			if (_recvThreadExit) break;

			timeval timeOut = { 1, 0 };
			INT nready = _socket->select(&timeOut);

			//HNLOGEX_DEBUG("===========onSocketReadThread==============");

			// outtime
			if (0 == nready) continue;

			// network error
			if (SOCKET_ERROR == nready)
			{
				HNLOGEX_DEBUG("%s >> network error >> %s >> (nready:%d)", _tag.c_str(), strerror(errno), nready);
				disconnectMsgNotify();
				break;
			}
        
			CHAR readBuffer[TCP_BUFSIZE_READ];
			INT recvSize = _socket->recv(readBuffer, sizeof(readBuffer));
        
			if (recvSize > 0)
			{
				onRead(readBuffer, recvSize);
			}

			// 0: server close
			if (0 == recvSize)
			{
				HNLOGEX_DEBUG("%s >> server close >> %s >> (recvSize:%d)", _tag.c_str(), strerror(errno), recvSize);
				disconnectMsgNotify();
				break;
			}
			
			// -1: network error
			if (SOCKET_ERROR == recvSize)
			{
				if (errno == EWOULDBLOCK || errno == EINTR || errno == EAGAIN) continue;

				HNLOGEX_DEBUG("%s >> network error >> %s >> (recvSize:%d)", _tag.c_str(), strerror(errno), recvSize);
				disconnectMsgNotify();
				break;
			}
		}
	
		clear();
		HNLOGEX_DEBUG("%s >> onSocketReadThread >> end.", _tag.c_str());
	}

	void HNSocketThread::onRead(CHAR* buffer, INT recvSize)
	{
		// 参数校验
		if (!buffer || 0 >= recvSize) return;

		// cache network data
		_socketDataCacheQueue->push(buffer, recvSize);

		const UINT NetMessageHeadSize = sizeof(NetMessageHead);

		NetMessageHead* pMessageHead = nullptr;
	
		UINT messageSize = (UINT)_socketDataCacheQueue->size();
		if (messageSize >= NetMessageHeadSize)
		{
			do
			{
				pMessageHead = (NetMessageHead*) _socketDataCacheQueue->front();
				if (nullptr != pMessageHead && messageSize >= pMessageHead->uMessageSize)
				{
					size_t headSize = sizeof(NetMessageHead);

					// 解密数据部分-有数据时解密
					if (pMessageHead->uMessageSize > headSize)
					{
						// RC4解密
						_rc4.decrypt((unsigned char*)_socketDataCacheQueue->front() + headSize, pMessageHead->uMessageSize - headSize);
					}
					CHAR* pData = _socketDataCacheQueue->front() + NetMessageHeadSize;
					HNSocketMessage* SocketMessage = HNSocketMessage::getMessage();
					if (SocketMessage)
					{
						//if (ASS_GP_CLUB_OperationRequest == pMessageHead->bAssistantID)
						//{
							//printf("%lu pMessageHead->uMessageSize", pMessageHead->uMessageSize);
						//}
						//HNLOGEX_DEBUG("%s >> not a complete data packet [NetMessageHeadSize = %lu, pMessageHead->uMessageSize = %lu, pMessageHead->uMessageSize - NetMessageHeadSize = %d]",
							//_tag.c_str(), NetMessageHeadSize, pMessageHead->uMessageSize, pMessageHead->uMessageSize - NetMessageHeadSize);
						//HNLOGEX_DEBUG("%s >>bMainID ==%d  and   bAssistantID  == %d  DEBUG[NetMessageHeadSize = %lu , pMessageHead->uMessageSize = %lu]", _tag.c_str(), pMessageHead->bMainID, pMessageHead->bAssistantID, NetMessageHeadSize, pMessageHead->uMessageSize);
						SocketMessage->setMessage(pMessageHead, pData, pMessageHead->uMessageSize - NetMessageHeadSize);
						SocketMessage->socketStatus = SocketStatus_RECV;
						_socketDataCacheQueue->pop(pMessageHead->uMessageSize);

						if(MDM_CONNECT == SocketMessage->messageHead.bMainID && ASS_NET_TEST_1 == SocketMessage->messageHead.bAssistantID)
						{
							HNSocketMessage::releaseMessage(SocketMessage);
						}
						else
						{
							bool bBackground = Configuration::getInstance()->getValue("bBackground", Value(false)).asBool();
							bool bControl = Configuration::getInstance()->getValue("bControl", Value(false)).asBool();

							std::lock_guard <std::mutex> autoLock(_dataMutex);
							if (bControl && bBackground)
							{
								if (SocketMessage->messageHead.bMainID != MDM_GM_GAME_NOTIFY)
								{
									_socketMessageQueue->push_back(SocketMessage);
									_delegate->onReadComplete();
								}
								else
								{
									HNSocketMessage::releaseMessage(SocketMessage);
								}
							}
							else
							{
								_socketMessageQueue->push_back(SocketMessage);
								_delegate->onReadComplete();
							}
						}						
					}
					else
					{
						clearMessageCache();

						disconnectMsgNotify();
						break;
					}
				}
				else
				{
					HNLOG_ERROR("%s >> not a complete data packet [messageSize = %lu, pMessageHead->uMessageSize = %lu]",
						_tag.c_str(), messageSize, pMessageHead->uMessageSize);
				}
				messageSize = (UINT)_socketDataCacheQueue->size();
			} while(messageSize >= pMessageHead->uMessageSize);
		}
	}

	void HNSocketThread::transform(std::function<void(HNSocketMessage* socketMessage)> func)
	{
		int queueSize = 0;
		do 
		{	
			HNSocketMessage* socketMessage = nullptr;
			{
				std::lock_guard < std::mutex > autoLock(_dataMutex);
				queueSize = _socketMessageQueue->size();
				if (queueSize > 0)
				{
					socketMessage = _socketMessageQueue->front();
					_socketMessageQueue->pop_front();
				}
			}
			if (nullptr != socketMessage)
			{
				func(socketMessage);
				HNSocketMessage::releaseMessage(socketMessage);
			}
		} while (queueSize > 0);

		Director::getInstance()->getScheduler()->pauseTarget(this);
	}

	void HNSocketThread::startHeartBeatThread()
	{
		if (_heartBeatThreadExit)
		{
			_heartBeatThreadExit = false;
			// create heartBeat Thread
			std::thread heartBeatThread(std::bind(&HNSocketThread::onHeartBeatThread, this));
			heartBeatThread.detach();
		}
	}

	void HNSocketThread::onHeartBeatThread()
	{
		while (true)
		{
			if (_heartBeatThreadExit) break;
			heartBeat();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			Sleep(15000);
#else
			sleep(15);
#endif
		}
	}

	void HNSocketThread::heartBeat()
	{
		if (_tag.compare("platform") == 0 && INVALID_VALUE != HNSocketProtocolData::PlatformCheckCode)
		{
			HNSocketProtocolData data;
			data.createPackage(MDM_CONNECT, ASS_NET_TEST_1, HNSocketProtocolData::PlatformCheckCode);
			send(data.getPackage(), data.getPackageSize());
		}
		else if (_tag.compare("game") == 0 && INVALID_VALUE != HNSocketProtocolData::GameCheckCode)
		{
			HNLOGEX_DEBUG("%s >> is a heartBeat == game heartBeat!!!", _tag.c_str());
			HNSocketProtocolData data;
			data.createPackage(MDM_CONNECT, ASS_NET_TEST_1, HNSocketProtocolData::GameCheckCode);
			send(data.getPackage(), data.getPackageSize());
		}
	}

	void HNSocketThread::disconnectMsgNotify()
	{
		if (_recvThreadExit) return;

		//心跳未启动，断网直接走网络错误通知
		if (_heartBeatThreadExit)
		{
			socketErrMsgNotify();
			return;
		}

		HNSocketMessage* SocketMessage = HNSocketMessage::getMessage();
		SocketMessage->socketStatus = SocketStatus_DISCONNECT;
		{
			std::lock_guard <std::mutex> autoLock(_dataMutex);
			_socketMessageQueue->push_back(SocketMessage);
		}
		_delegate->onReadComplete();
		close();
	}

	void HNSocketThread::socketErrMsgNotify()
	{
		HNSocketMessage* SocketMessage = HNSocketMessage::getMessage();
		SocketMessage->socketStatus = SocketStatus_ERROR;
		{
			std::lock_guard <std::mutex> autoLock(_dataMutex);
			_socketMessageQueue->push_back(SocketMessage);
		}
		_delegate->onReadComplete();
		close();
	}

	void HNSocketThread::clearMessageCache()
	{
		std::lock_guard <std::mutex> autoLock(_dataMutex);
		for (auto iter = _socketMessageQueue->begin(); iter != _socketMessageQueue->end();)
		{
			HNSocketMessage* message = *iter;
			HNSocketMessage::releaseMessage(message);
			iter = _socketMessageQueue->erase(iter);
		}
		_socketMessageQueue->clear();
	}
}
