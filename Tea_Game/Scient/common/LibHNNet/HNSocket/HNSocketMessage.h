﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __NH_HNSocketMessage_H__
#define __NH_HNSocketMessage_H__

#include "HNBaseType.h"
#include "HNNetProtocol/HNProtocolExport.h"
#include <functional>
#include <string>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "dia2dump/dia2.h"
#include "dia2dump/dia2dump.h"
#include "dia2dump/PrintSymbol.h"
#include <stdio.h>
using namespace PDB;
#include "HNCommon/HNLog.h"
#endif

namespace HN
{
	class HNSocketMessage;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define DUMP_ALLMSG(T,socketMessage){\
		if (message != nullptr){\
			CHAR buf[1024 * 12] = { 0 };\
			strcat(buf, "HNSocketMessage = {\n");\
			strcat(buf, "\tNetMessageHead = {\n");\
			sprintf(buf + strlen(buf), "\t\tuMessageSize  = %u\n", message->messageHead.uMessageSize);\
			sprintf(buf + strlen(buf), "\t\tbMainID       = %u\n", message->messageHead.bMainID);\
			sprintf(buf + strlen(buf), "\t\tbAssistantID  = %u\n", message->messageHead.bAssistantID);\
			sprintf(buf + strlen(buf), "\t\tbHandleCode   = %u\n", message->messageHead.bHandleCode);\
			sprintf(buf + strlen(buf), "\t\tbReserve      = %u\n", message->messageHead.bReserve);\
			sprintf(buf + strlen(buf), "\t\tbDataRule     = %u\n", message->messageHead.bDataRule);\
			strcat(buf, "\t}\n");\
			sprintf(buf + strlen(buf), "\t\tsocketStatus  = %d\n", message->socketStatus);\
			sprintf(buf + strlen(buf), "\t\tstrKey        = %s\n", message->strKey.c_str());\
			sprintf(buf + strlen(buf), "\t\tobjectSize    = %d\n", message->objectSize);\
			int size = sizeof(T);\
			cout << "目标结构体大小:" << size << endl;\
			cout << "消息结构体大小:" << message->objectSize << endl;\
			strcat(buf, "\tobject  = {\n");\
			if (size != message->objectSize){\
				for (size_t i = 0; i < message->objectSize; i++){\
					if (i % 16 == 0){\
						if (i != 0){\
							strcat(buf, "\n");\
						}\
						strcat(buf, "\t");\
					}\
					sprintf(buf + strlen(buf), "%02X ", (unsigned char)message->object[i]);\
				}\
			}else{\
				wchar_t outBuf[1024 * 10] = { 0 };\
				HNSocketMessage::print<T>(message->object, outBuf);\
				char outstr[1024 * 10] = { 0 };\
				sprintf(outstr, "%ws", outBuf);\
				strcat(buf, outstr);\
			}\
			strcat(buf, "\n\t}\n");\
			strcat(buf, "}\n");\
			string msg; \
			msg.assign(buf); \
			HNLOGEX_INFO("%s", msg.c_str()); \
		}\
	}
#define DUMP_MSG(T,object){\
		if (object != nullptr){\
			wchar_t outBuf[1024 * 10] = { 0 };\
			HNSocketMessage::print<T>(object, outBuf);\
			char outstr[1024 * 10] = { 0 };\
			sprintf(outstr, "%ws", outBuf);\
			string msg;\
			msg.assign(outstr);\
			HNLOGEX_INFO("%s", msg.c_str());\
		}\
	}
#else 
	#define DUMP_ALLMSG(T,socketMessage) {}
	#define DUMP_MSG(T,object)	{}
#endif
	

	#define SELECTER_KEY_FORMAT	"%u_%u"

	typedef std::function<bool(HNSocketMessage* socketMessage)> SEL_SocketMessage;

	#define HN_SOCKET_CALLBACK(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1, ##__VA_ARGS__)


	class HNSocketMessage 
	{
		public:
			static HNSocketMessage* getMessage();
			static void releaseMessage(HNSocketMessage* message);

		public:
			void setMessage(const NetMessageHead* head, CHAR* obj, INT Size);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			template <typename T>
			static void  print(const char* object, wchar_t* outBuf)
			{
				/*
				仅适用于简单结构体,基本类型或者数组或者自定义结构体，指针、函数、union、接口等类型（除外） 包含类或者结构体嵌套
				*/
				//获取对象类型名称
				std::string cType = typeid(T).name();
				//去除class和struct前缀
				string::size_type npos = cType.find("class ");
				if (npos != string::npos)
					cType.replace(npos, strlen("class "), "");
				npos = cType.find("struct ");
				if (npos != string::npos)
					cType.replace(npos, strlen("struct "), "");

				wchar_t wsType[256] = { 0 };
				wsprintf(wsType, L"%hs", cType.c_str());
				UDT_MembersDataMap memberDatas;
				int ret = GetAllMember(memberDatas, wsType);

				print(outBuf, wsType, memberDatas, object);
				wprintf(outBuf);

				//释放内存
				for (auto it = memberDatas.begin(); it != memberDatas.end(); ++it)
				{
					if (it->second->size() > 0)
					{
						for (auto iter = it->second->begin(); iter != it->second->end(); ++iter)
						{
							HN_SAFE_DELETE(*iter);
						}
					}
					HN_SAFE_DELETE(it->second);
				}
				memberDatas.clear();
			}
#endif
	private:
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		static void print(wchar_t* outBuf, const wchar_t* typeName, UDT_MembersDataMap& memberDataMap, const char* object, wchar_t* name = L"", int depth = 0, long index = -1);
		static void printBaseArrayType(const st_PDB_MemberData* data, wchar_t* outBuf, const wchar_t* preBuf, const char* object);
		static void printBaseType(const st_PDB_MemberData* data, wchar_t* outBuf, const wchar_t* preBuf, const char* object);


		template<typename T>
		static void formatBaseTypePrint(const st_PDB_MemberData* data, const wchar_t* format, const wchar_t* preBuf, wchar_t* outBuf, const char* object)
		{
			int size = data->size;
			int count = 1;
			if (data->is_array && data->array_elems[0] > 0)
			{//数组元素数量计算
				for (int i = 0; i < sizeof(data->array_elems) / sizeof(unsigned long); i++)
				{
					if (data->array_elems[i] == 0)
						break;
					count *= data->array_elems[i];
				}
			}

			wchar_t temp[512] = { 0 };
			for (size_t i = 0; i < count; i++)
			{
				T value = *(T*)(object + data->offset + i * size);
				if (i != 0 && i % 10 == 0)
				{
					memset(temp, 0, sizeof(temp));
					wsprintf(temp, L"\n    %s", preBuf);
					wcscat(outBuf, temp);
				}
				memset(temp, 0, sizeof(temp));
				wsprintf(temp, format, value);
				wcscat(outBuf, temp);
			}
		}
#endif
		private:
			HNSocketMessage();
			~HNSocketMessage();

			/*void *operator new(std::size_t ObjectSize);
			void operator delete(void *ptrObject);*/
			static const int MAX_BUFF = 4096;

		public:
			NetMessageHead messageHead;
			UINT objectSize = 0;
			CHAR object[MAX_BUFF];
			emSocketStatus socketStatus;
			std::string strKey;
			
	};

	class HNSocketSelectorItem
	{
	public:
		HNSocketSelectorItem(SEL_SocketMessage selector) : _selector(selector)
		{
		}

	public:
		bool doCallSelector(HNSocketMessage* socketMessage)
		{ 
			return (_selector)(socketMessage);
		}

	private:
		SEL_SocketMessage	_selector;
	};
};

#endif	//__NH_HNSocketMessage_H__
