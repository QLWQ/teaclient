﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_Export_H__
#define __HN_Export_H__

#include "HNData/HNRoomInfoModule.h"
#include "HNData/HNUserInfoModule.h"
#include "HNData/HNGamesInfoModule.h"
#include "HNData/HNUpdateInfoModule.h"

#include "HNCommon/HNUIDelegateQueue.h"
#include "HNCommon/HNLog.h"
#include "HNCommon/HNConverCode.h"
#include "HNCommon/HNCommonMarco.h"
#include "HNCommon/HNRefValue.h"

#include "HNGame/HNGameTableUsersData.h"
#include "HNGame/HNGameMessageDelegate.h"

#include "HNPlatform/HNPlatformExport.h"

#include "HNRoom/HNRoomExport.h"

#include "HNSocket/HNSocketExport.h"

#include "HNNetProtocol/HNProtocolExport.h"

#include "HNExternal/MD5/MD5.h"

#include "HNBaseType.h"

#include "HNHttp/HNHttp.h"

#include "HNHttp/HNUserHeadHttp.h"

using namespace HN;

#endif	//__HN_Export_H__
