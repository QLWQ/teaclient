/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_Http_H__
#define __HN_Http_H__

#include <string>
#include "cocos2d.h"
#include "network/HttpClient.h"

namespace HN
{
	class HNHttpDelegate
	{
	public:
		virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) = 0;
	};

	class HNHttpRequest
	{
	public:
		static HNHttpRequest* getInstance();
		static void destroyInstance();
		void request(const std::string& name, cocos2d::network::HttpRequest::Type type, const std::string& url, const std::string& param = "", const std::string pathAndFile = "");
		void addObserver(HNHttpDelegate* delegate);
		void removeObserver(HNHttpDelegate* delegate);
		size_t saveResponseData(const std::string pathAndFile, void *responseBuffer, ssize_t m_responseDataLength);

	public:
		HNHttpRequest();
		virtual ~HNHttpRequest();

	protected:
		void onHttpResponse(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response, const std::string pathAndFile);

	private:
		std::vector<HNHttpDelegate*> _addCache;
		std::vector<HNHttpDelegate*> _removeCache;
		std::vector<HNHttpDelegate*> _observers;

	};
}

#endif