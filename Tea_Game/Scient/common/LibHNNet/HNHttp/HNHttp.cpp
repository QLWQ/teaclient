/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNHttp.h"

USING_NS_CC;
using namespace network;

namespace HN
{
	static HNHttpRequest* s_hn_http_request = nullptr;

	HNHttpRequest* HNHttpRequest::getInstance()
	{
		if (s_hn_http_request == nullptr)
		{
			s_hn_http_request = new (std::nothrow) HNHttpRequest();
		}
		return s_hn_http_request;
	}

	void HNHttpRequest::destroyInstance()
	{
		delete s_hn_http_request;
		s_hn_http_request = nullptr;
	}

	void HNHttpRequest::request(const std::string& name, cocos2d::network::HttpRequest::Type type,
		const std::string& url, const std::string& param, const std::string pathAndFile)
	{
		HttpRequest* request = new (std::nothrow) HttpRequest();
		request->setUrl(url.c_str());
		request->setRequestType(type);
		request->setResponseCallback(CC_CALLBACK_2(HNHttpRequest::onHttpResponse, this, pathAndFile));
		request->setTag(name.c_str());

		if (type == HttpRequest::Type::POST && !param.empty())
		{
			request->setRequestData(param.c_str(), param.size());
		}

		HttpClient::getInstance()->sendImmediate(request);
		request->release();
	}

	void HNHttpRequest::addObserver(HNHttpDelegate* delegate)
	{
		auto observer = std::find(_addCache.begin(), _addCache.end(), delegate);
		if (observer == _addCache.end())
		{
			_addCache.push_back(delegate);
		}

		observer = std::find(_removeCache.begin(), _removeCache.end(), delegate);
		if (observer != _removeCache.end())
		{
			_removeCache.erase(observer);
		}		
	}

	void HNHttpRequest::removeObserver(HNHttpDelegate* delegate)
	{
		auto observer = std::find(_removeCache.begin(), _removeCache.end(), delegate);
		if (observer == _removeCache.end())
		{
			_removeCache.push_back(delegate);
		}

		observer = std::find(_addCache.begin(), _addCache.end(), delegate);
		if (observer != _addCache.end())
		{
			_addCache.erase(observer);
		}
	}

	HNHttpRequest::HNHttpRequest()
	{

	}

	HNHttpRequest::~HNHttpRequest()
	{

	}

	void HNHttpRequest::onHttpResponse(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response, const std::string pathAndFile)
	{
		// add cache
		for (auto observer : _addCache)
		{
			if (std::find(_observers.begin(), _observers.end(), observer) == _observers.end())
			{
				_observers.push_back(observer);
			}
		}
		_addCache.clear();

		// remove cache
		for (auto observer : _removeCache)
		{
			auto iter = std::find(_observers.begin(), _observers.end(), observer);
			if (iter != _observers.end())
			{
				_observers.erase(iter);
			}
		}
		_removeCache.clear();

		bool bRet = response->isSucceed() && response->getResponseCode() == 200;
		std::string data;

		if (bRet)
		{
			std::vector<char>* recv = response->getResponseData();
			data.assign(recv->begin(), recv->end());

			if (!pathAndFile.empty() && !data.empty())
			{
				char* responseString = new char[data.size() + 1];
				std::copy(recv->begin(), recv->end(), responseString);
				saveResponseData(pathAndFile, responseString, data.size());
				delete[]responseString;
			}
		}
		else
		{
			data = response->getErrorBuffer();
		}

		// dispatch
		for (auto observer : _observers)
		{
			observer->onHttpResponse(response->getHttpRequest()->getTag(), bRet, data);
		}
	}

	size_t HNHttpRequest::saveResponseData(const std::string pathAndFile, void *responseBuffer, ssize_t m_responseDataLength)
	{
		std::string fileAbsoluteName = FileUtils::getInstance()->getWritablePath() + pathAndFile;

		auto fileUtils = FileUtils::getInstance();
		std::string dirPath = fileUtils->getWritablePath();
		size_t found = fileAbsoluteName.find_last_of("/\\");
		if (found != std::string::npos)
		{
			dirPath = fileAbsoluteName.substr(0, found + 1);
		}

		if (!fileUtils->isDirectoryExist(dirPath))
		{
			if (!fileUtils->createDirectory(dirPath))
				return 0;
		}

		FILE *fp = fopen(fileAbsoluteName.c_str(), "wb");
		CCAssert(fp, "HNHttpRequest::saveResponseData() - open file failure");
		if (fp == NULL) return 0;

		size_t writedBytes = m_responseDataLength;
		if (m_responseDataLength > 0)
		{
			fwrite(responseBuffer, m_responseDataLength, 1, fp);
		}

		fclose(fp);
		return writedBytes;
	}
}

