#ifndef __REGS_H__
#define __REGS_H__
#include <vector>
#include <map>

namespace PDB
{

	enum eUDT_KIND
	{
		kind_base = 0,
		kind_struct = 1,
		kind_class,
		kind_union,
		kind_interface,
	};


	typedef struct _st_PDB_MemberData
	{
		wchar_t type[100];	//类型
		wchar_t name[100];  //变量名
		wchar_t access[20]; //权限级别private,public，protected
		unsigned long  array_elems[10];   //数组特有,依次存储高级到低级数组的大小 int[20][12] ,存储是12,20
		unsigned long  size;				//数据类型大小
		unsigned long  data_kind;  //种类,静态成员、成员等
		unsigned long  offset; //位置偏移
		eUDT_KIND  udt_kind; //自定义数据类型
		unsigned char  is_static;   //是否静态 0不是,1是
		unsigned char  is_const;   //是否const 0不是,1是
		unsigned char  is_enum;   //是否enum 0不是,1是
		unsigned char  is_array;   //是否数组 0不是,1是
		unsigned short tag;	//Tags returned by Dia  SymTagEnum枚举
		_st_PDB_MemberData()
		{
			memset(this, 0, sizeof(_st_PDB_MemberData));
		}
	}st_PDB_MemberData;

	typedef std::vector<st_PDB_MemberData*>	UDT_MembersDataVec;
	typedef std::map<std::wstring, UDT_MembersDataVec*> UDT_MembersDataMap;


	extern const wchar_t * const rgRegX86[];
	extern const wchar_t * const rgRegAMD64[];
	extern const wchar_t * const rgRegMips[];
	extern const wchar_t * const rgReg68k[];
	extern const wchar_t * const rgRegAlpha[];
	extern const wchar_t * const rgRegPpc[];
	extern const wchar_t * const rgRegSh[];
	extern const wchar_t * const rgRegArm[];

	typedef struct MapIa64Reg{
		CV_HREG_e  iCvReg;
		const wchar_t* wszRegName;
	}MapIa64Reg;
	extern const MapIa64Reg mpIa64regSz[];
	int cmpIa64regSz(const void*, const void*);

	extern unsigned long g_dwMachineType;
	const wchar_t* SzNameC7Reg(unsigned short, unsigned long);
	const wchar_t* SzNameC7Reg(unsigned short);

}

#endif