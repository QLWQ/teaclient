﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNUpdateInfoModule.h"
#include "HNCommon/HNCommonMarco.h"

namespace HN
{
	HNUpdateInfoModule* HNUpdateInfoModule::getInstance()
	{
		static HNUpdateInfoModule sUpdateModule;
		return &sUpdateModule;
	}

	HNUpdateInfoModule::HNUpdateInfoModule()
	{
		_updateList = new UPDATELIST();
	}

	HNUpdateInfoModule::~HNUpdateInfoModule()
	{
		clear();
		HN_SAFE_DELETE(_updateList);
	}

	// 清空数据
	void HNUpdateInfoModule::clear()
	{
		for (auto iter = _updateList->begin(); iter != _updateList->end(); ++iter)
		{
			(*iter)->am->release();
			HN_SAFE_DELETE(*iter);
		}
		_updateList->clear();
	}

	// 添加需要更新的游戏
	void HNUpdateInfoModule::addGame(int gameID, std::string resPath)
	{
		std::string storagePath = FileUtils::getInstance()->getWritablePath() + resPath;
		std::string manifestPath = resPath + "project.manifest";
		auto am = AssetsManagerEx::create(manifestPath, storagePath);
		am->retain();

		// 初始化更新信息
		updateStruct *update = new updateStruct;
		update->eventCode = EventAssetsManagerEx::EventCode::NEW_VERSION_FOUND;
		update->am = am;
		update->GameID = gameID;
		update->resPath = resPath;
		update->isUpdated = false;
		update->isAlreadyCheck = false;
		_updateList->push_back(update);
	}

	void HNUpdateInfoModule::deleteGame(int gameID)
	{
		for (auto iter = _updateList->begin(); iter != _updateList->end(); ++iter)
		{
			if ((*iter)->GameID == gameID)
			{
				(*iter)->am->release();
				HN_SAFE_DELETE(*iter);
				_updateList->erase(iter);
				break;
			}
		}
	}

	// 通过游戏ID查找更新信息
	updateStruct* HNUpdateInfoModule::findGame(INT gameID)
	{
		updateStruct* update = nullptr;
		for (auto iter = _updateList->begin(); iter != _updateList->end(); ++iter)
		{
			if (gameID == (*iter)->GameID)
			{
				update = *iter;
				break;
			}
		}
		return update;
	}

	// 遍历更新数据
	void HNUpdateInfoModule::transform(const TransformUpdateFunc& func)
	{
		INT index = 0;
		for (auto iter = _updateList->begin(); iter != _updateList->end(); ++iter, ++index)
		{
			func(*iter, index);
		}
	}

	// 重置更新信息
	void HNUpdateInfoModule::resetGame(INT gameID)
	{
		updateStruct temp = *findGame(gameID);
		deleteGame(gameID);
		addGame(gameID, temp.resPath);
	}
}
