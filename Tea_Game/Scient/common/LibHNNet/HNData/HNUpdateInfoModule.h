﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_HNUpdateInfoModule_h__
#define _HN_HNUpdateInfoModule_h__

#include "HNNetProtocol/HNProtocolExport.h"
#include <vector>
#include <functional>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include <string>

USING_NS_CC_EXT;

using namespace std;

namespace HN
{
	struct updateStruct
	{
		AssetsManagerEx* am;
		EventAssetsManagerEx::EventCode		eventCode;
		int GameID;
		std::string resPath;
		bool isAlreadyCheck;
		bool isUpdated;
	};

	typedef std::vector<updateStruct *> UPDATELIST;

	typedef std::function<void(updateStruct* info, INT index)> TransformUpdateFunc;

	class HNUpdateInfoModule
	{
	public:
		// 获取更新信息单例
		static HNUpdateInfoModule* getInstance();

	public:
		// 添加需要更新的游戏
		void addGame(int gameID, std::string resPath);

		// 删除需要更新的游戏
		void deleteGame(int gameID);

		// 清空数据
		void clear();

		INT getUpdateCount() const
		{
			return (INT)_updateList->size();
		}

		// 通过游戏ID查找更新信息
		updateStruct* findGame(INT gameID);

		// 重置更新信息
		void resetGame(INT gameID);

		// 遍历更新数据
		void transform(const TransformUpdateFunc& func);

		// 获取所有更新信息
		UPDATELIST getUpdateList() const
		{
			return *_updateList;
		}

	private:
		// 更新信息列表
		UPDATELIST*	_updateList;

	private:
		HNUpdateInfoModule();

		~HNUpdateInfoModule();
	};
}

#define UpdateInfoModule()	HNUpdateInfoModule::getInstance()

#endif // _HN_HNUpdateInfoModule_h__
