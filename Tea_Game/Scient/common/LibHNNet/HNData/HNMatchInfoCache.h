﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_HNMatchInfoCache_h__
#define _HN_HNMatchInfoCache_h__

#include "HNNetProtocol/HNProtocolExport.h"
#include <vector>
#include <functional>

namespace HN
{
	typedef std::vector<MSG_GP_ContestApplyInfo *> MATCHLIST;

	typedef std::function<void(MSG_GP_ContestApplyInfo* info, INT index)> TransformMatchFunc;

	class HNMatchInfoCache
	{
	public:
		// 获取比赛信息单例
		static HNMatchInfoCache* getInstance();

	public:
		// 删除比赛信息
		bool removeMatch(INT matchID);

		// 更新比赛信息
		void updateMatch(MSG_GP_ContestApplyInfo* info);

		// 添加比赛信息
		const MSG_GP_ContestApplyInfo* addMatch(MSG_GP_ContestApplyInfo* info);

		// 清空数据
		void clear();

		// 当前已选择比赛场
		void setSelectMatch(MSG_GP_ContestApplyInfo* info);

		// 获取当前已选择比赛场
		MSG_GP_ContestApplyInfo* getSelectMatch();

		INT getMatchCount() const
		{
			return (INT)_matchList->size();
		}

		// 通过比赛ID查找比赛信息
		MSG_GP_ContestApplyInfo* findMatch(INT matchID);

		// 遍历比赛数据
		void transform(const TransformMatchFunc& func);

		// 获取所有比赛信息
		MATCHLIST getMatchList() const
		{
			return *_matchList;
		}

	private:
		// 比赛信息列表
		MATCHLIST*	_matchList;

		INT			_iMatchID = -1; // 当前选择的比赛场ID

	private:
		HNMatchInfoCache();

		~HNMatchInfoCache();
	};
}

#define MatchInfoCache()	HNMatchInfoCache::getInstance()

#endif // _HN_HNMatchInfoModule_h__
