﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNMatchInfoCache.h"
#include "HNCommon/HNCommonMarco.h"

namespace HN
{
	HNMatchInfoCache* HNMatchInfoCache::getInstance()
	{
		static HNMatchInfoCache sMatchModule;
		return &sMatchModule;
	}

	HNMatchInfoCache::HNMatchInfoCache()
	{
		_matchList = new MATCHLIST();
	}

	HNMatchInfoCache::~HNMatchInfoCache()
	{
		clear();
		HN_SAFE_DELETE(_matchList);
	}

	bool HNMatchInfoCache::removeMatch(INT matchID)
	{
		for (auto iter = _matchList->begin(); iter != _matchList->end(); ++iter)
		{
			if (matchID == (*iter)->iContestID)
			{
				HN_SAFE_DELETE(*iter);
				_matchList->erase(iter);
				return true;
			}
		}
		return false;
	}

	// 更新比赛信息
	void HNMatchInfoCache::updateMatch(MSG_GP_ContestApplyInfo* info)
	{
		MSG_GP_ContestApplyInfo* oldUser = findMatch(info->iContestID);
		if (nullptr != oldUser)
		{
			*oldUser = *info;
		}
		else
		{
			addMatch(info);
		}
	}

	// 添加比赛信息
	const MSG_GP_ContestApplyInfo* HNMatchInfoCache::addMatch(MSG_GP_ContestApplyInfo* info)
	{
		if (info == nullptr || info->iContestID == INVALID_MATCH_ID)
		{
			return nullptr;
		}

		const MSG_GP_ContestApplyInfo* pMatchInfo = findMatch(info->iContestID);
		if (pMatchInfo != nullptr)
		{
			return nullptr;
		}

		MSG_GP_ContestApplyInfo * newMatch = new MSG_GP_ContestApplyInfo;
		*newMatch = *info;
		_matchList->push_back(newMatch);
		return newMatch;
	}

	// 清空数据
	void HNMatchInfoCache::clear()
	{
		for (auto iter = _matchList->begin(); iter != _matchList->end(); ++iter)
		{
			HN_SAFE_DELETE(*iter);
		}
		_matchList->clear();
	}

	void HNMatchInfoCache::setSelectMatch(MSG_GP_ContestApplyInfo* info)
	{
		_iMatchID = info->iContestID;
	}

	// 获取当前已选择比赛场
	MSG_GP_ContestApplyInfo* HNMatchInfoCache::getSelectMatch()
	{
		auto ptr = std::find_if(_matchList->begin(), _matchList->end(), 
			[=](MSG_GP_ContestApplyInfo* info) {
			return _iMatchID == info->iContestID;
		});

		if (ptr != _matchList->end())
		{
			return *ptr;
		}
		return nullptr;
	}

	// 通过比赛ID查找比赛信息
	MSG_GP_ContestApplyInfo* HNMatchInfoCache::findMatch(INT matchID)
	{
		MSG_GP_ContestApplyInfo* match = nullptr;
		for (auto iter = _matchList->begin(); iter != _matchList->end(); ++iter)
		{
			if (matchID == (*iter)->iContestID)
			{
				match = *iter;
				break;
			}
		}
		return match;
	}

	// 遍历比赛数据
	void HNMatchInfoCache::transform(const TransformMatchFunc& func)
	{
		INT index = 0;
		for (auto iter = _matchList->begin(); iter != _matchList->end(); ++iter, ++index)
		{
			func(*iter, index);
		}
	}
}
