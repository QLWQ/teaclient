﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_ProtocolExport_h__
#define __HN_ProtocolExport_h__

#include "HNNetProtocol/HNBaseCommand.h"
#include "HNNetProtocol/HNComStruct.h"
#include "HNNetProtocol/HNNetMessageHead.h"
#include "HNNetProtocol/HNPlatformMessage.h"
#include "HNNetProtocol/HNRoomMessage.h"
#include "HNNetProtocol/HNBankMessage.h"
#include "HNNetProtocol/HNVIPRoomMessage.h"
#include "HNNetProtocol/HNPlatformConfig.h"
#include "HNNetProtocol/HNConstDefine.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "HNNetProtocol/HNRoomForPCMessage.h"
#endif

#include <vector>

// 比赛接口
class IGameMatchMessageDelegate
{
public:	
	// 更新比赛信息
	virtual void I_R_M_UserContest(MSG_GR_ContestChange* contestChange){}

	//通知用户晋级
	virtual void I_R_M_UserPromotion(){}

	// 比赛结束
	virtual void I_R_M_ContestOver(MSG_GR_ContestAward* contestAward){}

	// 比赛淘汰
	virtual void I_R_M_ContestKick(MSG_GR_R_ContestMingCi* contestRank){}

	// 等待比赛结束
	virtual void I_R_M_ContestWaitOver(){}

	// 个人参赛纪录
	virtual void I_R_M_ContestRecord(MSG_GR_ContestRecord_Result* contestRecord){}

	// 更新比赛轮数
	virtual void I_R_M_ContestRoundNum(MSG_GR_ConTestRoundNum* contestRoundNum){}

	// 更新比赛局数
	virtual void I_R_M_ContestInnings(MSG_GR_ConTestRoundNumInOneRound* contestInnings){}

	// 更新当前比赛进度
	virtual void I_R_M_ContestInfoupdate(MSG_GM_S_ConTestGameInfo* contestInfodata){}

	// 更新等待比赛状态
	virtual void I_R_M_ContestInfoMatchWait(MSG_GR_SContestWaitRiseInfo* contestinfowait){}

	// 游戏结算消息转接
	virtual void I_R_M_ContestResult(void* object, INT objectSize){}
};

// 聊天接口
class IGameChartMessageDelegate
{
public:
	// 聊天消息
	virtual void I_R_M_NormalTalk(void* object, INT objectSize){}

	// 道具特效
	virtual void I_R_M_NormalAction(void* object, INT objectSize){}
};


// 用户行为接口
class IUserActionMessageDelegate
{
public:
	// 用户坐下消息
	virtual void I_R_M_UserSit(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user){}

	// 用户加入排队机
	virtual void I_R_M_UserJoinQueue(const std::string& message) {}

	// 排队用户坐下
	virtual void I_R_M_QueueUserSit(BYTE deskNo, const std::vector<MSG_GR_Queue_UserSit*>& users){}

	// 用户站起消息
	virtual void I_R_M_UserUp(MSG_GR_R_UserUp * userUp, const UserInfoStruct* user){}

	// 用户断线
	virtual void I_R_M_UserCut(INT dwUserID, BYTE bDeskNO, BYTE	bDeskStation){}

	// 用户坐下失败
	virtual void I_R_M_SitError(const std::string& message){}
};



#endif // __HN_ProtocolExport_h__
