﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_MessageHead_H_
#define _HN_MessageHead_H_

#include "HNBaseType.h"
#include "HNCommon/HNCommonMarco.h"
#include "HNComStruct.h"

namespace HN 
{
#pragma  pack(1)
	// network packet header
	typedef struct tagNetMessageHead
	{		
		UINT						uMessageSize = 0;			// 数据包大小
		UINT						bMainID      = 0;			// 处理主类型
		UINT						bAssistantID = 0;			// 辅助处理类型 ID
		UINT						bHandleCode  = 0;			// 数据包处理代码
		UINT						bReserve     = 0;			// 预留字段
		UINT                        bDataRule    = 0;           // 数据信息 俱乐部做特殊处理，1代表第一条有数据有数据，随机长整型代表第二条无数据， 0代表第一条无数据
	}  NetMessageHead;

#pragma pack()
}

#endif // !_HN_NetMessageHead_H_
