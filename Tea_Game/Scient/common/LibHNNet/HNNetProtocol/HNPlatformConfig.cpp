/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformConfig.h"
#include "HNBaseCommand.h"

static PlatformConfig* sPlatformConfig = nullptr;

PlatformConfig* PlatformConfig::getInstance()
{
	if (nullptr == sPlatformConfig)
	{
		sPlatformConfig = new (std::nothrow) PlatformConfig();
	}
	return sPlatformConfig;
}

void PlatformConfig::destroyInstance()
{
	CC_SAFE_DELETE(sPlatformConfig);
}

std::string PlatformConfig::getVoiceUrl()
{
	return buildHttp(getAPIServerUrl(), "/Public/BaseInfo.ashx");
}

std::string PlatformConfig::getAppInfoUrl()
{
	return buildHttp(getWebServerUrl(), "/Public/AppInfo.ashx?");
}

std::string PlatformConfig::getPromoterUrl()
{
	return buildHttp(getAPIServerUrl(), "/api/tuiguang/Tuigung.ashx");
}

std::string PlatformConfig::getDailiUrl()
{
	return "";
	//return "http://admin.qp888m.com:8081/Admin/Index/applyAgency?";
	//return  "http://192.168.0.33:8061/Admin/Index/applyAgency?";  //buildHttp(getAPIServerUrl(), "/Home/Index/applyAgency?");
}
std::string PlatformConfig::getOnlineInstallUrl_iOS(const std::string& url)
{
	std::string plist = "itms-services://?action=download-manifest&url=" + url;
	return plist;
}

std::string PlatformConfig::getNoticeUrl()
{
	return buildHttp(getAPIServerUrl(), "/Public/XmlHttpUser.aspx?");
}

std::string PlatformConfig::getGoodsListUrl(const std::string& platForm_Type)
{
	auto url_prefix = StringUtils::format("/public/xmlhttpuser.aspx?type=GetMobilePayConfig&GameType=%s", platForm_Type.c_str());
	return buildHttp(getAPIServerUrl(), url_prefix.c_str());
}

std::string PlatformConfig::getOrderUrl()
{
	return buildHttp(getAPIServerUrl(), "/public/xmlhttpuser.aspx?");
}

std::string PlatformConfig::getBindDiliUrl()
{
	return "";
	//return "http://admin.qp888m.com:8081/Admin/Index/bindAgency?";
	//return  "http://192.168.0.33:8061/Admin/Index/bindAgency?";   //buildHttp(getAPIServerUrl(), "/Home/Index/bindAgency?");
}

std::string PlatformConfig::getPrizeUrl()
{
	return buildHttp(getAPIServerUrl(), "/public/prizedata.ashx?");
}

std::string PlatformConfig::getTurnImgUrl()
{
	return buildHttp(getAPIServerUrl(), "/Turn/turnBg.png");
}

std::string PlatformConfig::getPayCallbackUrl_iOS()
{
	return buildHttp(getAPIServerUrl(), "/public/xmlhttpuser.aspx");
}

std::string PlatformConfig::getProtocolUrl()
{
	std::string url = buildHttp(getWebServerUrl(), "/xy/Protocol.aspx");
	return url;
}

std::string PlatformConfig::getEditUrl()
{
	std::string url = buildHttp(getAPIServerUrl(), "/public/auditconfig.ashx");
	return url;
}

std::string PlatformConfig::buildHttp(const std::string& url, const std::string& path)
{
	std::string http("http://");
	http.append(url);
	http.append(path);
	return http;
}

std::string PlatformConfig::buildHttps(const std::string& url, const std::string& path)
{
	std::string https("https://");
	https.append(url);
	https.append(path);
	return https;
}

PlatformConfig::PlatformConfig()
	: _gameDesignSize(1280, 720)
	, _platformDesignSize(1280, 720)
	, _accountType(UNKNOWN)
	, _shareUrl("")
	, _shareContent("")
	, _shareImage("")
	, _appId(1)
	, _isReconnect(false)
	, _nowCount(0)
	, _allCount(0)
	, _playCount(0)
	, _playMode(0)
{
	
}

PlatformConfig::~PlatformConfig()
{
}



