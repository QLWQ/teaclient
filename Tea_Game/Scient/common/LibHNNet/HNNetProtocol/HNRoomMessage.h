﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_RoomMessage_H__
#define _HN_RoomMessage_H__

#include "HNComStruct.h"
#pragma pack(1)

/*********************************************************************************/
#define MAX_TALK_LEN				   500						//最大聊天数据长度
	

///	通信标识定义 

/*********************************************************************************/
//连接消息
#define MDM_GR_CONNECT					1						//连接消息类型
#define	ASS_GR_CONNECT_SUCCESS			3						


//登录游戏房间的相关定义
/*--------------------------------------------------------------------------------*/
#define	MDM_GR_LOGON					100						//登陆游戏房间

#define MDM_GR_QUEUE_MSG				114						///排队消息

//登录返回结果
#define ASS_GR_LOGON_SUCCESS			2						//登陆成功
#define ASS_GR_LOGON_ERROR				3						//登陆失败
#define ASS_GR_SEND_FINISH				4						//登陆完成
#define ASS_GR_LOGON_BY_ID				5						//通过用户ID登陆
#define ASS_GR_IS_VIPROOM               16                      //是否VIP房间
#define ASS_GR_VIP_PW                   17                      //VIP房间需要密码
#define ASS_GR_VIP_NO_PW                18                      //VIP房间不需要密码(第一个进入不需要密码)
#define ASS_GR_NO_VIP                   19                      //不是VIP房间
#define ASS_GR_VIPROOM_PW               20                      //VIP房间密码
#define ASS_GR_VIPROOM_PW_RIGHT         21                      //VIP房间密码正确

/*----------------------------------------------------------------------------------*/
//服务端主动推送
//用户列表主ID
#define MDM_GR_USER_LIST				101						//登录成功后返回个人信息以及该房间玩家信息
//用户列表辅助ID
#define ASS_GR_ONLINE_USER				1						//在线用户
#define ASS_GR_NETCUT_USER				2						//断线用户
#define ASS_GR_DESK_STATION				3						//桌子状态
/*-----------------------------------------------------------------------------------*/

//客户端发送

//玩家动作
#define MDM_GR_USER_ACTION				102						//用户动作，玩家坐桌，起身，断线等都是以该值为主标志
//玩家动作辅助ID
#define ASS_GR_USER_UP					1						//用户起来
#define ASS_GR_USER_SIT					2						//用户坐下
#define ASS_GR_WATCH_UP					3						//旁观起来
#define ASS_GR_WATCH_SIT				4						//旁观坐下
#define ASS_GR_USER_COME				5						//用户进入
#define ASS_GR_USER_LEFT				6						//用户离开
#define ASS_GR_USER_CUT					7						//用户断线
#define ASS_GR_SIT_ERROR				8						//坐下错误
#define ASS_GR_SET_TABLE_BASEPOINT		9						//改变桌子倍数
#define ASS_GR_USER_HIT_BEGIN			10						//用户同意开始
#define ASS_GR_JOIN_QUEUE				11						//加入排队机
#define ASS_GR_QUIT_QUEUE				12						//退出排队机
#define ASS_GR_QUEUE_USER_SIT			13						//排队用户坐下
#define ASS_GR_LEASE_TIMEOVER			14						//排队用户坐下
#define ASS_GR_CHANGE_DESK				15						//快速换桌
#define ASS_GR_SIT_REPEAT				16						//重复坐下（此id暂时不处理）
#define ASS_GR_FORCE_QUITE				17						//强制退出
#define ASS_GR_MASTER_LEAVE				18						//房主主动退出
#define ASS_GR_GET_USER_INFO			19		                //获取坐下用户信息
#define ASS_GR_CONTEST_APPLY			24						//比赛场 报名/退赛
#define ASS_GR_CONTEST_APPLYINFO		25						//比赛场报名信息
#define ASS_GR_FAST_JOIN_IN			    27		                //快速坐桌
#define ASS_GR_ClubTeahouse_FastJoin		28						//俱乐部茶楼快速加入游戏（传入结构体：MSG_GR_ClubTeahouse_FastJoin）



//玩家动作部分操作结果：（专用于比赛房间）
#define ERR_GR_APPLY_SUCCEEDED			1						//参赛或退赛成功
#define ERR_GR_APPLY_ALREADYSIGN		2						//已经报名
#define ERR_GR_APPLY_BEGIN				3						//比赛已经开始
#define ERR_GR_APPLY_NOTENOUPH_MONEY	4						//由于钱包金币不够，导致报名失败

enum E_GR_ERROR_MASTER_LEAVE	//房主离开错误码
{
	ERR_GR_LEAVE_SUCCESS	= 1, //房主成功离开
	ERR_GR_NOT_BUY_ROOM		= 2, //不是可购买房间
	ERR_GR_IN_GAME			= 3, //游戏已经开始
	ERR_GR_NOT_IN_DESK		= 4, //用户未坐下
	ERR_GR_NOT_LEAVE		= 5, //房主不允许离开
	ERR_GR_NOT_MASTER		= 6, //不是房主
	ERR_GR_IS_CLUBROOM		= 7, //俱乐部房主不允许离开
};

enum E_GR_ERROR_GET_DESK_USERINFO	//获取坐桌玩家信息错误码
{
	ERR_GR_GET_DESKUSER_SUCCESS		= 0, //获取玩家信息成功
	ERR_GR_USER_ONT_ONLINE			= 1, //玩家不在线，返回空数据
	ERR_GR_USER_ONT_SITTING			= 2, //玩家没有坐下，有效的玩家信息，无效的坐下信息
	ERR_GR_USER_IN_DIFF_DESK		= 3, //对方与自己不在同一张桌子，有效的玩家信息，无效的坐下信息
	ERR_GR_MY_NOT_SITTING_			= 4, //自己没有坐下，返回空数据
};


//房间信息
#define MDM_GR_ROOM						103		
//房间信息辅助ID
#define	ASS_GR_NORMAL_TALK				1					//普通聊天
#define	ASS_GR_HIGH_TALK				2					//高级聊天
#define	ASS_GR_USER_AGREE				3					//用户同意
#define	ASS_GR_GAME_BEGIN				4					//游戏开始
#define	ASS_GR_GAME_END					5					//游戏结束
#define	ASS_GR_USER_POINT				6					//用户经验值
#define	ASS_GR_SHORT_MSG				7					//聊短信息
#define	ASS_GR_ROOM_SET					8					//房间设置
#define	ASS_GR_INVITEUSER				9					//邀请用户
#define	ASS_GR_INSTANT_UPDATE			10					//即时更新分数金币
#define	ASS_GR_UPDATE_CHARM				11					//即时更新魅力
#define	ASS_GR_ROOM_PASSWORD_SET		12					//房间设置
#define	ASS_GR_ROOM_QUEUE_READY			13					//排队机准备
#define	ASS_GR_GET_NICKNAME_ONID        14					//根据ID获取昵称
#define	ASS_GR_OWNER_T_ONE_LEFT_ROOM    15					//房主踢玩家离开房间
#define	ASS_GR_GET_NICKNAME_ONID_INGAME 16					//根据ID获取昵称 游戏中
#define	ASS_GR_USER_CONTEST				17					//用户比赛信息，发送NET_ROOM_CONTEST_CHANGE_RESULT，排名有更改
#define	ASS_GR_AVATAR_LOGO_CHANGE		18					//用户形象更改信息
#define	ASS_GR_CAHNGE_ROOM				19					//比赛开始，未报名用户切换房间
#define	ASS_GR_CONTEST_GAMEOVER			20					//比赛结束
#define	ASS_GR_CONTEST_KICK				21					//用户被淘汰
#define	ASS_GR_CONTEST_WAIT_GAMEOVER	22					//比赛结束，但是有用户还在打最后一局，通知已打完的用户等待排名
#define	ASS_GR_INIT_CONTEST				23					//比赛开始，初始化比赛信息
#define	ASS_GR_CONTEST_PEOPLE			24					//获取报名数量，登陆完成后服务端主动发送NET_ROOM_CONTEST_PEOPLE_RESULT
#define	ASS_GR_CONTEST_RECORD			25					//个人参赛纪录，登陆完成后服务端主动发送NET_ROOM_CONTEST_RECORD_RESULT
#define	ASS_GR_CONTEST_ABANDON			27					//比赛取消（报名人数不足）
#define	ASS_GR_MONEY_NOTENOUGH			28					//金币不足提示
#define	ASS_GR_CONTEST_EXIT				29					//退赛
#define	ASS_GR_CONTEST_CLEARDESK			30					//继续比赛(当前局数)
#define	ASS_GR_CONTEST_ROUNDNUM			31					//比赛轮数更新
#define	ASS_GR_CONTEST_PAIHANG			32					//排行榜数据
#define	ASS_GR_CONTEST_FINISHTOWAIT		33					//等待其他桌游戏结束
#define	ASS_GR_CONTEST_FINISHTONEXT		34					//其他桌比赛结束当前晋级
#define	ASS_GR_CONTEST_WAIT_RISE_INFO     35					// 等待晋级信息

//最大阶段轮数
enum
{
	_ContestRoundInfo_MaxRounds = 10,
};
//比赛轮数
struct MSG_GR_ConTestRoundNum
{
	int RoundNum;					//当前多少人数
	int RoundNumTwo;				//晋级多少人
	int curRound;					// 当前轮数
	int totalRounds;					//总共多少轮数
	int	riseList[_ContestRoundInfo_MaxRounds];	// 晋级人数表，长度等于totalRounds（如：32 16 8，那么就是32进16 16进8 8进冠军）
};

//当前比赛局数
struct MSG_GR_ConTestRoundNumInOneRound
{
	int ContestRunGameCount;
	int ContestRunGameCountTwo;
};

//排行榜数据
struct MSG_GR_ContestPaiMing
{
	int					        dwUserID;							///用户 ID	
	LLONG						i64ContestScore;					///比赛分数
	int							iRankNum;							///比赛排名
	int                         iUpPeople;//达到此数才开始比赛
	int                         iRunNum;
	int                         iAllRunNum;
	int                         iIsKick;
	int                         pAwards;  //奖多少
	int                         pAwardTypes;//奖什么0 金币 1 红包 2 房卡
	char		                szUserName[50];
};

// 比赛等待状态数据更新
typedef struct SContestWaitRiseInfo
{
	int	remainTables;	// 剩余等待对局完成的桌子数
	int	userRank;		// 用户最新排名
} MSG_GR_SContestWaitRiseInfo;

/*------------------------------------------------------------------------------------------------------------------------*/

//框架消息
#define MDM_GM_GAME_FRAME				150						//框架消息
//框架消息辅助ID
#define ASS_GM_GAME_INFO				1						//游戏信息
#define ASS_GM_GAME_STATION				2						//游戏状态
#define ASS_GM_FORCE_QUIT				3						//强行退出
#define ASS_GM_NORMAL_TALK				4						//普通聊天
#define ASS_GM_HIGH_TALK				5						//高级聊天
#define ASS_GM_WATCH_SET				6						//旁观设置	
#define ASS_GM_CLEAN_USER               9                       //比赛场清理用户信息
#define ASS_GM_USE_KICK_PROP            7						//使用踢人卡
#define ASS_GM_USE_ANTI_KICK_PROP       8						//使用防踢卡
#define ASS_GM_SET_VIDEOADDR			10						//设置视频服务器地址
#define ASS_GM_GET_GAMENUM			11						//请求游戏活动进度
#define ASS_GM_GET_RANDNUM			12						//请求红包奖励
#define ASS_GM_CONTEST_GAME_INFO		13						//比赛游戏信息

//断线重连比赛场进度更新
struct MSG_GM_S_ConTestGameInfo
{
	int  iCurrentRound;			//当前轮
	int  iCurrentRoundToNext;		//下一轮
	int  iCurrentJu;				//当前局
	int  iTotalJu;				//总局
	char szGameRoomName[61];	//比赛场名称
	// 新加轮数总轮数以及晋级人数表
	int curRound;		// 当前轮数
	int totalRounds;	// 总轮数（如16进8，8进4，4进冠军，则是3轮）
	int	riseList[_ContestRoundInfo_MaxRounds];	// 晋级人数表（如：32 16 8，那么就是32进16 16进8 8进冠军）
};

/*************************************************************************/
//游戏中通讯（道具特效）
#define MDM_GR_PROP				160								// 房间和游戏中道具相关的消息

//框架消息辅助ID
#define ASS_PROP_NEW_TEXIAO			8							// 使用特效道具

// 使用特效道具
struct DoNewProp
{
	int  dwUserID;						//发送用户
	int  dwDestID;						//目标用户
	int  iPropNum;						//特效计数
};
/*************************************************************************/
/*********************************************************************************/
//通知消息
#define MDM_GM_GAME_NOTIFY				180						//游戏消息
#define ASS_GM_AGREE_GAME				1						//同意游戏

/*********************************************************************************/
///错误代码
#define ERR_GR_ERROR_UNKNOW				0						//未知错误
#define ERR_GR_LOGON_SUCCESS			1						//登陆成功
#define ERR_GR_USER_NO_FIND				2						//用户不存在
#define ERR_GR_USER_PASS_ERROR			3						//用户密码错误
#define ERR_GR_USER_VALIDATA			4						//用户帐号禁用
#define ERR_GR_USER_IP_LIMITED			5						//登陆 IP 禁止
#define ERR_GR_IP_NO_ORDER				6						//不是指定地址
#define ERR_GR_ONLY_MEMBER				7						//会员游戏房间
#define ERR_GR_IN_OTHER_ROOM			8						//正在其他房间
#define ERR_GR_ACCOUNTS_IN_USE			9						//帐号正在使用
#define ERR_GR_PEOPLE_FULL				10						//人数已经满
#define ERR_GR_LIST_PART				11						//部分用户列表
#define ERR_GR_LIST_FINISH				12						//全部用户列表
#define ERR_GR_STOP_LOGON				13						//暂停登陆服务

#define ERR_GR_CONTEST_NOSIGNUP			14
#define ERR_GR_CONTEST_TIMEOUT			15						//比赛房重连超时
#define ERR_GR_CONTEST_KICK				16
#define ERR_GR_CONTEST_NOTSTRAT			17
#define ERR_GR_CONTEST_OVER				18
#define ERR_GR_CONTEST_BEGUN			19

#define ERR_GR_MATCH_LOGON				160						//游戏房间
#define ERR_GR_TIME_OVER				161						//时间到期

///wushuqun 2009.6.5
///不在混战场活动时间内
#define ERR_GR_BATTLEROOM_OUTTIME       162

///用户坐下错误码
#define ERR_GR_SIT_SUCCESS				50						//成功坐下
#define ERR_GR_BEGIN_GAME				51						//游戏已经开始
#define ERR_GR_ALREAD_USER				52						//已经有人存在
#define ERR_GR_PASS_ERROR				53						//密码错误
#define ERR_GR_IP_SAME					54						//IP 相同
#define ERR_GR_CUT_HIGH					55						//断线率太高
#define ERR_GR_POINT_LOW				56						//经验值太低
#define ERR_GR_POINT_HIGH				57						//经验值太高
#define ERR_GR_NO_FRIEND				58						//不受欢迎
#define ERR_GR_POINT_LIMIT				59						//经验值不够
#define ERR_GR_CAN_NOT_LEFT				60						//不能离开
#define ERR_GR_NOT_FIX_STATION			61						//游戏已经开始
#define ERR_GR_MATCH_FINISH				62						//比赛结束
#define ERR_GR_MONEY_LIMIT				63						//金币太低
#define ERR_GR_MATCH_WAIT				64						//比赛场排队提示
#define ERR_GR_IP_SAME_3				65						//IP前3 相同
#define ERR_GR_IP_SAME_4				66						//IP前4 相同
#define ERR_GR_UNENABLE_WATCH			67						//不允许旁观
#define ERR_GR_DESK_FULL				68						//人数已满
#define ERR_GR_FAST_SIT                 69                      //快速坐下失败
#define ERR_GR_POSITION_FAIL            70                      //未能获取位置信息
#define ERR_GR_VIP_PASS_ERROR           71                      //VIP房间密码错误
#define ERR_GR_JEWEL_LOMIL              72                      //钻石不足

/*********************************************************************************/

//封桌
#define MDM_GR_MANAGE        115 
#define ASS_GR_ALONE_DESK    15  //封桌
#define	ASS_GR_DEALONE_DESK  16  //解封


//游戏房间登陆
typedef struct tagMSG_GR_S_RoomLogon 
{
	UINT								uNameID;				//游戏 ID
	INT									dwUserID;				//用户 ID
	CHAR								szMD5Pass[50];			//加密密码
	float								fLat;					//用户经度
	float								fLnt;					//用户维度
	char								szLocation[128];		//用户地址
	bool								bLogonByPhone;			//是否手机登陆

}  MSG_GR_S_RoomLogon;


// 游戏房间登陆返回
typedef struct tagMSG_GR_R_LogonResult 
{
	UserInfoStruct						pUserInfoStruct;		//用户信息
	SysGiveMoney                        sGiveMoney;				//非比赛场玩家金币不足自动赠送
	INT									nVirtualUser;			//虚拟玩家个数
	INT									iRemainPeople;			//比赛中还剩下的人数

}  MSG_GR_R_LogonResult;

///游戏房间登陆
typedef struct tagMSG_GR_R_OtherRoom 
{
	INT									iRoomID;
	CHAR								szGameRoomName[61];		//房间名字
}  MSG_GR_R_OtherRoom;

///游戏桌子状态
typedef struct tagMSG_GR_DeskStation 
{
	bool								isBuy[100];							//是否用来购买的VIP桌(0: 普通桌子， 1, 购买的桌子）
	BYTE								bDeskStation[100];					//桌子状态
	BYTE								bDeskLock[100];						//锁定状态
	BYTE                                bVirtualDesk[100];                  //虚拟状态
	BYTE                                bUserCount[100];					//桌子人数
}  MSG_GR_DeskStation;

///用户进入房间
typedef struct tagMSG_GR_R_UserCome 
{
	UserInfoStruct						pUserInfoStruct;					//用户信息
}  MSG_GR_R_UserCome;

///用户离开房间
typedef struct tagMSG_GR_R_UserLeft 
{
	INT									dwUserID;							//用户 ID
}  MSG_GR_R_UserLeft;

///用户坐下
typedef struct tagMSG_GR_S_UserSit 
{
	BYTE								bDeskIndex;							//桌子索引
	BYTE								bDeskStation;						//桌子位置
	CHAR								szPassword[61];						//桌子密码
}  MSG_GR_S_UserSit;

// 用户快速加入俱乐部茶楼
typedef struct MSG_GR_ClubTeahouse_FastJoin
{
	int		clubId;
	int		teahouseId;
	int		deskIndex;			
} MSG_GR_S_TeaHouseSit;

///用户坐下
typedef struct tagMSG_GR_R_UserSit 
{
	INT									dwUserID;							//用户 ID
	BYTE								bLock;								//是否密码
	BYTE								bDeskIndex;							//桌子索引
	BYTE								bDeskStation;						//桌子位置
	BYTE								bUserState;							//用户状态
	BYTE								bIsDeskOwner;						//台主离开（锁桌）
	bool								bLeave;								//是否需要离开游戏（仅仅换桌）
	bool								bDeskMaster;						//是否是房主（开房间）

}  MSG_GR_R_UserSit;

///用户站起
typedef MSG_GR_R_UserSit MSG_GR_R_UserUp;

///获取坐下用户信息
typedef struct tagMSG_GR_I_GetDeskUserInfo
{
	INT iUserID;

}  MSG_GR_I_GetDeskUserInfo;

//请求红包奖励
typedef struct tagMSG_GR_Get_RandNum
{
	INT iUserID;
} MSG_GR_Get_RandNum;

///获取坐下用户信息结果
typedef struct tagMSG_GR_O_GetDeskUserInfo
{
	MSG_GR_R_UserSit	userSit;
	UserInfoStruct		userInfo;

}  MSG_GR_O_GetDeskUserInfo;

//排队玩家坐桌成功结构体
typedef struct MSG_GR_Queue_UserSit
{
	BYTE	bDeskStation;	// 玩家座位
	UINT	dwUserID;		// 玩家ID
}  MSG_GR_Queue_UserSit;

///用户断线
typedef struct tagMSG_GR_R_UserCut 
{
	INT									dwUserID;							//用户 ID
	BYTE								bDeskNO;							//游戏桌号
	BYTE								bDeskStation;						//位置号码
}  MSG_GR_R_UserCut;

///用户同意结构
typedef struct tagMSG_GR_R_UserAgree 
{
	BYTE								bDeskNO;							//游戏桌号
	BYTE								bDeskStation;						//位置号码
	BYTE								bAgreeGame;							//同意标志
}  MSG_GR_R_UserAgree;

///用户分数
typedef struct tagMSG_GR_R_InstantUpdate 
{
	INT									dwUserID;							//用户 ID
	INT									dwPoint;							//用户分数
	INT									dwMoney;							//用户金币
}  MSG_GR_R_InstantUpdate;

///用户经验值
typedef struct tagMSG_GR_R_UserPoint 
{	//广播消息
	INT									dwUserID;							//用户 ID
	LLONG								dwPoint;							//用户经验值
	LLONG								dwMoney;							//用户金币
	LLONG								dwSend;								//赠送
	BYTE								bWinCount;							//胜局
	BYTE								bLostCount;							//输局
	BYTE								bMidCount;							//平局
	BYTE								bCutCount;							//逃局
	SysGiveMoney                        sGiveMoney;							//自动赠送
}  MSG_GR_R_UserPoint;

/********************************************************************************************/

///游戏信息
typedef struct tagMSG_GM_S_GameInfo 
{
	BYTE								bGameStation;						//游戏状态
	BYTE								bWatchOther;						//允许旁观
	BYTE								bWaitTime;							//等待时间
	BYTE								bReserve;							//保留字段
	CHAR								szMessage[1000];					//系统消息
}  MSG_GM_S_GameInfo;

//获取红包活动当前状态
typedef struct tagMSG_GR_GameNum
{
	INT		 iUserID;
	INT		 iGameNum;					//当前游戏局数
	INT		 iCanTakeNum;					//当前可以领取红包次数
	INT		 idangqianjieduan;				//任务达标局数
}	MSG_GR_GameNum;

//获取红包活动奖励
typedef struct tagMSG_GR_Set_JiangLi
{
	INT         iUserID;
	INT         iRandnum;
	INT         iJieDuan;
	INT         iAwardNum;
} MSG_GR_Set_JiangLi;

///修改用户经验值
typedef struct tagMSG_GR_S_RefalshMoney 
{
	INT									dwUserID;							//用户 ID
	LLONG								i64Money;							//用户金币
}  MSG_GR_S_RefalshMoney;

///旁观设置
typedef struct tagMSG_GM_WatchSet 
{
	INT									dwUserID;							//设置对象
}  MSG_GM_WatchSet;

///消息数据包
typedef struct tagMSG_GA_S_Message
{
	BYTE								bFontSize;							//字体大小
	BYTE								bCloseFace;							//关闭界面
	BYTE								bShowStation;						//显示位置
	CHAR								szMessage[1000];					//消息内容
}  MSG_GA_S_Message;

//游戏公告消息ATT
typedef struct tagGameNoticeMessage 
{
	BYTE			bDeskIndex;				//桌子号
	BYTE			bAwardCardShape;		//牌型奖励
	CHAR			szMessage[255];			//未进入游戏 在房间 右边框显示公告内容
}  GameNoticeMessage;

///////////////////////////////////////////////比赛相关/////////////////////////////////////////////
//比赛开始
//Out:   给比赛用发送初始化信息
//ASS_GR_INIT_CONTEST	

typedef struct tagMSG_GR_ContestChange
{
	int									dwUserID;							// 用户 ID	
	int									iContestCount;						// 比赛局数
	LLONG							    i64ContestScore;					// 比赛分数
	int									iRankNum;							// 比赛排名
	int									iRemainPeople;						// 比赛中还剩多少人
	int									iUpPeople;							//达到此数才开始比赛
	char									szUserName[50];						//用户名称
	int									iRunNum;
	int									iAllRunNum;
}  MSG_GR_ContestChange;

typedef struct tagMSG_GR_ContestAward
{
	int									dwUserID;
	int									iAward;
	int									iAwardType;
	int									iRank;

}  MSG_GR_ContestAward;

typedef struct tagMSG_GR_ContestAbandon
{
	int									iUserID;
	LLONG								i64WalletMoney;
	int									iJewels;
}  MSG_GR_ContestAbandon;

// 参赛纪录，用于房间中显示个人记录
typedef struct tagMSG_GR_ContestRecord_Result
{
	INT		iChanpionTimes;	  // 夺冠次数
	INT		iContestTimes;	  // 参赛次数
	BYTE	bBestRank;		  // 最佳排名
}  MSG_GR_ContestRecord_Result;

typedef struct tagMSG_GR_R_ContestMingCi
{
	INT		Rank;
}  MSG_GR_R_ContestMingCi;

//////////////////////////////////////////语音功能//////////////////////////////////////
#define MDM_GR_VOICE				183									///语音发送消息
#define ASS_GR_VOIEC				0								    ///语音发送成功

typedef struct VoiceInfo
{
	UINT uUserID;  //用户id
	UINT uVoiceID; //音频ID
    INT  iVoiceTime;//录音时长

	VoiceInfo()
	{
		::memset(this, 0, sizeof(VoiceInfo));
	}
}MSG_GP_S_VoiceInfo;


/////////////////////////////////////////////获取桌子信息和解散桌子协议/////////////////////////////////

#define MDM_GR_DESKRUNOUT						181		//获取桌子信息接口
#define ASS_GR_GETDESKMASTER					0		//获取创建房间信息
#define ASS_GR_DESKRUNOUT						1		//桌子解散广播消息（解散成功或者失败广播）
#define ASS_GR_DISMISS							2		//玩家申请解散（服务端广播其他所有人此消息）
#define ASS_GR_DISMISS_AGREE					3		//用户回复解散申请
#define ASS_GR_DISMISS_CANCEL					4		//取消已经同意的解散操作
#define ASS_GR_DISMISS_NETCUT					5		//断线重连回来重新通知当前解散状态
#define ASS_GR_GETDISTANCEINFO					6		//距离过近提示
#define ASS_GR_DISTANCE_AGREE					7		//是否同意过近继续游戏
#define ASS_GR_DISTANCE_ALLAGREE				8		//所有玩家同意继续游戏
#define ASS_GR_JEWES_CHANGE						9		//玩家钻石变化


struct VipDeskInfo
{
	BYTE byDeskIndex;//桌号
};

// 接收（获取当前桌子信息，用于客户端显示）
struct VipDeskInfoResult
{
	BYTE byDeskIndex;		//桌号
	int  iMasterID;			//桌主ID	
	int  iPlayCount;		//游戏总局数
	int  iNowCount;			//当前游戏局数
	int	 iType;				//结算类型（按局数，按总分， 按圈数,3按时效）
	int  iSeconds;          //剩余秒数 iType为3时有效
	BYTE bTimeKeeper;       //是否开始计时（iType为3时有效）
	char szPassWord[20];	//桌子密码
	char szGameName[64];	//游戏名称
	BYTE DeskConfig[512];//特殊规则
};

// 桌子是否解散成功通知
struct VIPDeskRunOut
{
	bool bSuccess;			//解散是否成功
	BYTE bDeskIndex;		//桌子号码
};

//接收(广播)：
struct VipDeskDismissNotify
{
	int iUserID;	//申请解散桌子ID
	int iTimeCount;	//解散倒计时
};

struct VipDeskDismissAgree
{
	bool bAgree;//是否同意
};

struct VipDeskDismissAgreeRes
{
	int iUserID;
	bool bAgree;//是否同意
};

struct NetCutDismissNotify
{
	int iUserID;			//申请解散玩家ID
	int iUserAgreeID[20];	//同意解散玩家ID
	int iAgreeNum;
	int iTimeCount;			//解散倒计时
};

// 玩家距离检测通知，用于防作弊
typedef struct tagMSG_GR_S_Position_Notice
{
	INT		iUserIDA;		// 距离过近玩家A
	INT		iUserIDB;		// 距离过近玩家B
	INT		iDistance;		// 玩家之间距离
	bool	bDistance;		// 距离过近
	bool	bSameIP;		// IP相同
}  MSG_GR_S_Position_Notice;

/////////////////////////////////////////////服务端房间被释放重新激活获取玩家信息协议/////////////////////////////////
#define MDM_GR_RETURNDESK						182		//还原桌子信息
#define ASS_GR_RETURNDESK						0		//还原桌子信息

// 玩家距离检测通知，用于防作弊
typedef struct tagMSG_GR_S_ReturnGameInfo
{
	BYTE	iDeskNo;			// 玩家桌子号
	INT		iDeskStation;		// 玩家座位号
	INT		iUserID;			// 玩家ID
	LLONG	i64Score;			// 玩家分数
	char	nickname[61];		// 玩家昵称
	char	headUrl[256];		// 头像地址
	INT		iLogoID;			// 头像ID
	INT		iUserState;			// 用户状态（1：在线，2：断线，3：离线）
}  ReturnGameInfo;

#pragma pack()



/********************************************************************************************/

#endif	//_HN_RoomMessage_H__
