/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef PlatformConfig_h__
#define PlatformConfig_h__

#include "cocos2d.h"
#include <string>
#include "HNBaseType.h"

USING_NS_CC;

class PlatformConfig
{
public:
	enum ACCOUNT_TYPE
	{
		UNKNOWN = 0,
		GUEST,
		NORMAL
	};

	enum SCENE_STATE
	{
		INPLATFORM,		//当前场景在大厅中
		INROOM,			//当前场景在房间中
		INGAME,			//当前场景在游戏中
		INMATCH,		//当前场景在比赛备战区
		OTHER
	};
public:
	// 获取配置单例
	static PlatformConfig* getInstance();

	// 销毁配置单例
	static void destroyInstance();

	// 获取更新检测地址
	std::string getAppInfoUrl();

	// 获取推广员地址
	std::string getPromoterUrl();

	//获取推广信息地址
	std::string getDailiUrl();

	// 获取iOS在线安装地址
	std::string getOnlineInstallUrl_iOS(const std::string& url);

	// 获取苹果内购后台验证地址
	std::string getPayCallbackUrl_iOS();

	// 获取公告地址
	std::string getNoticeUrl();

	// 获取商品列表地址
	std::string getGoodsListUrl(const std::string& platForm_Type);

	// 获取订单信息接口地址
	std::string getOrderUrl();

	//获取绑定接口地址
	std::string getBindDiliUrl();

	// 获取兑换奖品地址
	std::string getPrizeUrl();

	//获取转盘奖品图片
	std::string getTurnImgUrl();

	// 获取用户协议地址
	std::string getProtocolUrl();

	// 获取客服信息
	std::string getEditUrl();

	// 获取语音地址
	std::string getVoiceUrl();

	// 获取支付回到地址
	//std::string getPayCallbackUrl_iOS();

	// 生成Http完整路径
	std::string buildHttp(const std::string& url, const std::string& path);

	// 生成Https完整路径
	std::string buildHttps(const std::string& url, const std::string& path);

	// APP授权码
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _appKey, AppKey);

	// APP类型id
	CC_SYNTHESIZE_PASS_BY_REF(int, _appId, AppId);

	// 游戏logo
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _gameLogo, GameLogo);

	// 过程动画图片
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _splashLogo, SplashLogo);

	// 游戏设计尺寸
	CC_SYNTHESIZE_PASS_BY_REF(cocos2d::Size, _gameDesignSize, GameDesignSize);

	// 平台设计尺寸
	CC_SYNTHESIZE_PASS_BY_REF(cocos2d::Size, _platformDesignSize, PlatformDesignSize);

	// 获取登陆玩家类型（正常的还是游客）
	CC_SYNTHESIZE_PASS_BY_REF(ACCOUNT_TYPE, _accountType, AccountType);

	// 玩家当前场景状态（平台，房间，游戏）
	CC_SYNTHESIZE_PASS_BY_REF(SCENE_STATE, _sceneState, SceneState);

	// 房主id
	CC_SYNTHESIZE_PASS_BY_REF(INT, _masterID, MasterID);

	// 剩余局数
	CC_SYNTHESIZE_PASS_BY_REF(INT, _playCount, PlayCount);

	// 当前局数
	CC_SYNTHESIZE_PASS_BY_REF(INT, _nowCount, NowCount);

	// 总局数
	CC_SYNTHESIZE_PASS_BY_REF(INT, _allCount, AllCount);

	// 剩余时间（时效房间用）
	CC_SYNTHESIZE_PASS_BY_REF(INT, _restTime, RestTime);

	// vip房号
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _vipRoomNum, VipRoomNum);

	// 游戏玩法
	CC_SYNTHESIZE_PASS_BY_REF(INT, _playMode, playMode);

	// 是否开启比赛场
	CC_SYNTHESIZE_PASS_BY_REF(bool, _isOpenMatch, OpenMatch);

	// plist下载地址
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _downloadPlistUrl, DownloadPlistUrl);

	// 分享地址
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _shareUrl, ShareUrl);

	// 分享内容
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _shareContent, ShareContent);

	// 分享图片
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _shareImage, ShareImage);

	// 是否IAP支付(苹果平台才有效）
	CC_SYNTHESIZE_PASS_BY_REF(bool, _isIAP, IsIAP);

	// 是否pc启动
	CC_SYNTHESIZE_PASS_BY_REF(bool, _isStartFromPC, IsStartFromPC);

	// 是否正在执行重连
	CC_SYNTHESIZE_PASS_BY_REF(bool, _isReconnect, IsReconnect);

protected:


	// 构造函数
	PlatformConfig();

	// 析构函数
	~PlatformConfig();

#define HNPlatformConfig() PlatformConfig::getInstance()
};

#endif // PlatformConfig_h__
