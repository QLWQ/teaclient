/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNBaseCommand.h"
#include "HNSocket/HNSocket.h"
#include "HNExternal/MD5/MD5.h"
#include "cocos2d.h"

#include "json/rapidjson.h"
#include "json/document.h"

USING_NS_CC;
//39.98.185.201
// 正式地址
static const std::string PLATFORM_SERVER_ADDRESS("103.248.139.107");  // 服务器地址120.79.136.27   192.168.1.5		//本地服务器 192.168.0.33
//static const std::string API_SERVER_URL("120.79.136.27");			//Web接口地址47.104.220.115

// app更新专用地址（iOS需要SSL证书）
static std::string WEB_SERVER_URL("www.1018000.com:8888");

static std::string API_SERVER_URL("www.1018000.com:8888");			//Web接口地址47.104.220.115
static std::string PHP_SERVER_URL("www.1018000.com:8888");


// 为防止被破解，key值更改为9位随机数
LLONG getSecrectKey()
{
	return 457878412;
}

std::string getPlatformServerAddress()
{
	std::string address(PLATFORM_SERVER_ADDRESS);

//#ifdef _DEBUG
    address = getPlatformAddressFromConfig();
    if (address.empty())
    {
        address = PLATFORM_SERVER_ADDRESS;
    }
//#endif

	if(HN::HNSocket::isValidIP(address))
	{
		std::vector<std::string> ips = HN::HNSocket::getIpAddress(address.c_str());
		return ips.empty() ? address : ips.at(0);
	}
	else
	{
		return address;
	}
}

INT getPlatformServerPort()
{
	return 3015;
}

std::string getWebServerUrl()
{
	return WEB_SERVER_URL;
}

std::string getAPIServerUrl()
{
	return API_SERVER_URL;
}

std::string getXXTEA_KEY()
{
	// 防止进入静态内存区域
	char key[] = { '9', 'O', 'O', 'D', 'e', 'u', 'C', 'v', 'T', 'W', 'w', 'a', 'D', 'R', 'J', 'R', 'u', 'K', 'Y' };
	return std::string(key);
}

std::string getPlatformAddressFromConfig()
{
    std::string ip;
    do
    {
        std::string configfile("config.json");
        if (cocos2d::FileUtils::getInstance()->isFileExist(configfile))
        {
            std::string json = cocos2d::FileUtils::getInstance()->getStringFromFile(configfile);
            rapidjson::Document doc;
            doc.Parse<rapidjson::kParseDefaultFlags>(json.c_str());
            if (doc.HasParseError() || !doc.IsObject())
            {
                break;
            }

			std::string select_server = "server";
			if (doc.HasMember("select_server"))
			{
				select_server = doc["select_server"].GetString();
			}
			if (doc.HasMember(select_server.c_str()))
			{
				if (doc[select_server.c_str()].HasMember("ip"))
				{
					ip = doc[select_server.c_str()]["ip"].GetString();
				}
			}
        }
    } while (0);
	return ip;
}

void initConfigUrl()
{
	do
	{
		std::string configfile("config.json");
		if (cocos2d::FileUtils::getInstance()->isFileExist(configfile))
		{
			std::string json = cocos2d::FileUtils::getInstance()->getStringFromFile(configfile);
			rapidjson::Document doc;
			doc.Parse<rapidjson::kParseDefaultFlags>(json.c_str());
			if (doc.HasParseError() || !doc.IsObject())
			{
				break;
			}

			std::string select_server = "server";
			if (doc.HasMember("select_server"))
			{
				select_server = doc["select_server"].GetString();
			}
			if (doc.HasMember(select_server.c_str()))
			{
				if (doc[select_server.c_str()].HasMember("api_server"))
				{
					API_SERVER_URL = doc[select_server.c_str()]["api_server"].GetString();
				}
				if (doc[select_server.c_str()].HasMember("php_server"))
				{
					PHP_SERVER_URL = doc[select_server.c_str()]["php_server"].GetString();
				}
				if (doc[select_server.c_str()].HasMember("web_server"))
				{
					WEB_SERVER_URL = doc[select_server.c_str()]["web_server"].GetString();
				}
			}
		}
	} while (0);
}

std::string getPHPServerUrl()
{
	return PHP_SERVER_URL;
}