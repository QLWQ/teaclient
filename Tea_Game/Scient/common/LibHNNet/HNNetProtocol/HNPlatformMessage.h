/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_NetMessageHall_H_
#define _HN_NetMessageHall_H_

#include "HNBaseType.h"
#include "HNComStruct.h"

//////////////////////////////////////////////////////////////////////////
///常量定义
#define		MAX_PEOPLE				180				//最大游戏人数 百家乐
#define		MAX_SEND_SIZE			2044			//最大消息包
#define		MAX_TALK_LEN			500				//最大聊天数据长度
#define		NORMAL_TALK_LEN			200				//普通聊天数据长度

//////////////////////////////////////////////////////////////////////////
//游戏通讯指令宏定义

//////////////////////////////////////////////////////////////////////////
// 客户端到服务端心跳指令
#define		MDM_CONNECT			1
#define		ASS_NET_TEST_1		1
#define		ASS_NET_TEST_2		2

//////////////////////////////////////////////////////////////////////////

// 大厅主标识
#define		MDM_GP_CONNECT					1			// 连接消息类型
#define		MDM_GP_LOGONUSERS	 			119			// 统计登录人数

//////////////////////////////////////////////////////////////////////////
// 快速登录主ID和，辅助ID标志
#define		MDM_GP_REGISTER			       99			// 快速登录
#define		ASS_GP_REGISTER					0			//
#define		ERR_REGISTER_ERROR				0			// 注册失败
#define		ERR_REGISTER_SUCCESS			1			// 注册成功
#define		ERR_REGISTER_NAME_EXIST			2			// 用户名存在


//////////////////////////////////////////////////////////////////////////

//游戏主标识
#define		MDM_GM_GAME_FRAME				150			//框架消息
#define		MDM_GM_MESSAGE					151			//信息消息


//大厅辅助处理消息标志
#define		ASS_GP_NET_TEST					1			//网络测试
#define		ASS_GP_CONNECT_SUCCESS 			3			//连接成功

//////////////////////////////////////////////////////////////////////////
//大厅登陆
#define		MDM_GP_LOGON					100			
#define		ASS_GP_LOGON_BY_NAME			1			//通过用户名字登陆
#define		ASS_GP_LOGON_BY_ACC				2			//通过用户ACC 登陆
#define		ASS_GP_LOGON_BY_MOBILE			3			//通过用户手机登陆
#define		ASS_GP_LOGON_REG				4			//用户注册
#define		ASS_GP_LOGON_SUCCESS			5			//登陆成功
#define		ASS_GP_LOGON_ERROR				6			//登陆失败
#define		ASS_GP_LOGON_ALLO_PART			7			//异地登陆
#define		ASS_GP_LOGON_LOCK_VALID			8			//锁机验证
#define		ASS_GP_LOGON_BY_SOFTWARE        10
#define		ASS_GP_LOGON_MOBILE_VALID		11			//手机验证

/// 错误代码
#define		ERR_GP_ERROR_UNKNOW				0		    //未知错误
#define		ERR_GP_LOGON_SUCCESS			1		    //登陆成功
#define		ERR_GP_USER_NO_FIND				2		    //登陆名字错误
#define		ERR_GP_USER_PASS_ERROR			3		    //用户密码错误
#define		ERR_GP_USER_VALIDATA			4		    //用户帐号禁用
#define		ERR_GP_USER_IP_LIMITED			5		    //登陆 IP 禁止
#define		ERR_GP_USER_EXIST				6		    //用户昵称已经存在
#define		ERR_GP_PASS_LIMITED				7		    //密码禁止效验
#define		ERR_GP_IP_NO_ORDER				8		    //不是指定地址 
#define		ERR_GP_LIST_PART				9		    //部分游戏列表
#define		ERR_GP_LIST_FINISH				10		    //全部游戏列表
#define		ERR_GP_USER_LOGON				11		    //此帐号已经登录
#define		ERR_GP_USERNICK_EXIST			12		    //此昵称已经存在
#define		ERR_GP_USER_BAD					13		    //未法字符
#define		ERR_GP_IP_FULL					14		    //IP已满
#define		ERR_GP_LOCK_SUCCESS				15		    //锁定机器成功	
#define		ERR_GP_ACCOUNT_HAS_LOCK			16		    //机器已经处于锁定状态	
#define		ERR_GP_UNLOCK_SUCCESS			17		    //解除锁定成功 
#define		ERR_GP_NO_LOCK					18		    //机器根本就没有锁定，所以解锁失败 
#define		ERR_GP_CODE_DISMATCH			19		    //机器码不匹配，解锁失败。
#define		ERR_GP_ACCOUNT_LOCKED			20		    //本账号锁定了某台机器，登录失败
#define		ERR_GP_MATHINE_LOCKED			21
#define		ERR_GP_VISITER_LOCKED			23			//游客登录锁定机器
#define		ERR_GP_LOGINTYPE_ERROR			24			//登陆类型错误
#define		ERR_GP_LOGINTYPE_MAINTAIN	        25			//维护中无法登陆

// 第三方认证返回的错误码
#define		ERR_GP_USER_NOT_EXIST           30			// 用户不存在
#define		ERR_GP_USER_OVERDATE            31			// 用户已过期

#define		EXP_GP_ALLO_PARTY				50			//本账号异地登陆


//#define       ASS_GP_CONTEST_APPLY				157			//金币兑换
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

#define		MDM_GP_LIST						101			// 游戏列表

///通信辅助标识										
#define		ASS_GP_LIST_KIND				1			//获取游戏类型列表
#define		ASS_GP_LIST_NAME				2			//获取游戏名字列表
#define		ASS_GP_LIST_ROOM				3			//获取游戏房间列表
#define		ASS_GP_LIST_COUNT				4			//获取游戏人数列表
#define		ASS_GP_ROOM_LIST_PEOCOUNT		5			//获取游戏人数列表
#define		ASS_GP_ROOM_PASSWORD			6			//发送房间密码,试图进入密码房间时发送此消息
#define		ASS_GP_GET_SELLGAMELIST			7			//获取游戏销售列表
#define		ASS_GP_LIST_CONTEST				8			//获取比赛列表

//////////////////////////////////////////////////////////////////////////
//添加用户资料管理通讯协议
#define		MDM_GP_USERINFO	 				115
#define		ASS_GP_USERINFO_UPDATE_BASE		1		//用户更新基本信息
#define		ASS_GP_USERINFO_UPDATE_DETAIL	2		//用户更新详细信息
#define		ASS_GP_USERINFO_UPDATE_PWD		3		//用户修改密码
#define		ASS_GP_USERINFO_FORGET_PWD		4		//用户忘记密码
#define		ASS_GP_USERINFO_ACCEPT			5		//服务端已接受
#define		ASS_GP_USERINFO_NOTACCEPT		6		//服务端未能接受
#define		ASS_GP_USERINFO_NICKNAMEID		10		//根据玩家昵称找ID或ID找昵称

//////////////////////////////////////////////////////////////////////////

#define		ASS_GP_LOGONUSERS_COUNT			1

//////////////////////////////////////////////////////////////////////////
//抽奖
#define MDM_GP_LUCKDRAW           144  //转盘抽奖
#define ASS_GP_LUCK_DRAW_CONFIG   0    //获取抽奖信息
#define ASS_GP_LUCK_DRAW_Do		  1    //抽奖操作
#define ASS_GP_LUCK_DRAW_RECOD	  2		//抽奖记录
//////////////////////////////////////////////////////////////////////////

///	网络通信数据包定义 

#pragma pack(1)
struct MSG_GP_LuckDraw_Config_Request
{
	int iUserID;
	MSG_GP_LuckDraw_Config_Request()
	{
		memset(this, 0, sizeof(MSG_GP_LuckDraw_Config_Request));
	}
};

struct MSG_GP_LuckDraw_Config_Result
{
	BYTE    iFreeTotalCount; // 总免费次数
	BYTE    iLeaveFreeCount; // 剩余免费抽奖次数
	int     i64Money;        // 积分抽奖消耗
	BYTE    type[10];        // 类别 0-金币,1-房卡,2-积分   3-不属于游戏里面的东西
	int     value[10];       // 值
	char	name[10][64];	// 名字
	MSG_GP_LuckDraw_Config_Result()
	{
		memset(this, 0, sizeof(MSG_GP_LuckDraw_Config_Result));
	}
};

struct MSG_GP_LuckDraw_Do_Request
{
	int iUserID;
	MSG_GP_LuckDraw_Do_Request()
	{
		memset(this, 0, sizeof(MSG_GP_LuckDraw_Do_Request));
	}
};

struct MSG_GP_LuckDraw_Do_Result
{
	int index;
	MSG_GP_LuckDraw_Do_Result()
	{
		memset(this, 0, sizeof(MSG_GP_LuckDraw_Do_Result));
	}
};
//////////////////////////////
//请求抽奖记录结构体
struct MSG_GP_I_LuckDraw_Recod
{
	int     iUserID;
};
//返回抽奖记录结构体
struct MSG_GP_O_LuckDraw_Recod
{

	int     value[10];       // 值
	char   name[10][64];	// 名字
	LLONG	 Time[10];
};
// 连接成功消息 
typedef struct tagMSG_S_ConnectSuccess 
{
	BYTE							bMaxVer;						//最新版本号码
	BYTE							bLessVer;						//最低版本号码
	BYTE							bReserve[2];					//保留字段
	UINT							i64CheckCode;					//加密后的校验码，由客户端解密在包头中返回
} MSG_S_ConnectSuccess;

// 用户登陆（帐号）结构
typedef struct tagMSG_GP_S_LogonByNameStruct 
{
	bool								bForced;					//是否强登
	BYTE								bLogonType;					//登陆类型(0：游客，1：账号，2：微信，3：QQ)
	CHAR								szName[32];					//登陆名字
	CHAR								TML_SN[128];				//数字签名	
	CHAR								szMD5Pass[33];				//登陆密码
	CHAR								szMathineCode[64];			//本机机器码
	INT									iUserID;					//用户ID登录，如果ID>0用ID登录

} MSG_GP_S_LogonByNameStruct;

// 大厅登录返回结果
typedef struct tagMSG_GP_R_LogonResult 
{
	INT									dwUserID;					//用户 ID 
	UINT								dwNowLogonIP;				//现在登陆 IP
	BYTE								bLogonType;					//登录类型
	INT									bLogoID;					//用户头像
	bool								bBoy;						//性别
	CHAR								szName[32];					//用户登录名
	CHAR								TML_SN[128];				//数字签名
	CHAR								szMD5Pass[33];				//用户密码
	CHAR								nickName[50];				//用户昵称
	LLONG								i64Money;					//钱包金币
	LLONG								i64Bank;					//银行金币
	INT									iJewels;					//钻石
	INT									iClubJewels;						///俱乐部钻石
	INT									iLockJewels;				//冻结钻石
	INT                                 iLotteries;                 //奖券
	CHAR								szSignDescr[128];			//个性签名
	CHAR								szMobileNo[20];				//移动电话
	CHAR								headUrl[256];				//用户头像URL
	INT									iVipTime;					//VIP时间
	INT                                  iVipLevel; 
	bool								bLoginBulletin;				//是否有登录公告
	INT									iLockMathine;				//当前帐号是否锁定了某台机器，1为锁定，0为未锁定
	INT									iBindMobile;				//当前帐号是否绑定手机号码，1为绑定，0为未绑定
	char								WrongMessage[256];
	INT									iAddFriendType;				//是否允许任何人加为好友
	BYTE								iCutRoomID;					//断线重连房间号
} MSG_GP_R_LogonResult;

typedef MSG_GP_R_LogonResult MSG_GP_UserInfo;

// 登陆服务器登陆信息
typedef struct tagDL_GP_RoomListPeoCountStruct 
{
	UINT							uID;				//房间ID 
	UINT							uOnLineCount;		//在线人数
	INT								uVirtualUser;		//扩展机器人人数
} DL_GP_RoomListPeoCountStruct;

// 进入密码房间时向服务器提交房间密码
typedef struct MSG_GP_CheckRoomPasswd 
{
	UINT uRoomID;			 // 房间ID
	char szMD5PassWord[50];  // 房间MD5密码
} MSG_GP_CheckRoomPasswd;

// 进入密码房间返回信息
typedef struct MSG_GP_S_C_CheckRoomPasswd
{
	bool bRet;		// 成功与否
	UINT uRoomID;	// 房间号
} MSG_GP_S_C_CheckRoomPasswd;

//////////////////////////////////////////////////////////////////////////
// 统计登录人数
typedef struct tagONLINEUSERSCOUNT 
{
	UINT							uiLogonPeopCount;	//登录人数
}  ONLINEUSERSCOUNT;

//////////////////////////////////////////////////////////////////////////
// 获取游戏房间数据包
typedef struct tagMSG_GP_SR_GetRoomStruct 
{
	UINT								uKindID;		//类型 ID
	UINT								uNameID;		//名字 ID
}  MSG_GP_SR_GetRoomStruct;

//////////////////////////////////////////////////////////////////////////
// 玩家列表操作
typedef struct tagMSG_GP_User_Opt_Struct 
{
	INT									dwTargetID;		//操作对像
	INT									dwUserID;		//操作id
	UINT								uType;			//操作类型
}  MSG_GP_User_Opt_Struct;

//////////////////////////////////////////////////////////////////////////
// 获取游戏房间数据包
typedef struct tagMSG_GP_SR_OnLineStruct 
{
	UINT								uKindID;		//类型 ID
	UINT								uNameID;		//名字 ID
	UINT								uOnLineCount;	//在线人数
}  MSG_GP_SR_OnLineStruct;

//////////////////////////////////////////////////////////////////////////
// 游戏信息
typedef struct tagMSG_GM_S_ClientInfo 
{
	BYTE								bEnableWatch;	//允许旁观
}  MSG_GM_S_ClientInfo;


// 用户注册信息
typedef struct tagMSG_GP_SR_Register
{
	BYTE								byRegisteType;		// 0-快速注册，1-普通注册，2-微信登陆，3-QQ登陆
	CHAR								szMathineCode[64];	// 硬件ID
	CHAR								szName[32];			// 用户名
	CHAR								szPswd[33];			// 密码
	//CHAR								szPhoneNum[20];		// 手机号
	CHAR								szNickName[50];		// 微信昵称
	bool								bBoy;				// 性别
	CHAR								headUrl[256];		// 头像url
	CHAR								szUionID[64];		// 第三方登陆唯一标识
}  MSG_GP_S_Register;


struct MSG_GP_JEWELTOMONEY
{
	int		iUserID;			//用户ID
	int		iJewels;			//用于转换的房卡数量
	LLONG i64WalletMoney;		//需要兑换成的金币数量
};
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// 用户修改密码
typedef struct MSG_GP_S_ChPassword
{
	UINT dwUserID;			//用户ID 
	CHAR szHardID[64];		//硬盘ID
	CHAR szMD5OldPass[80];  //用户老密码
	CHAR szMD5NewPass[80];  //用户新密码

	MSG_GP_S_ChPassword()
	{
		::memset(this, 0x0, sizeof(MSG_GP_S_ChPassword));
	}
}  MSG_GP_S_ChPassword;

// 用户忘记密码
struct MSG_GP_I_ForgetPWD
{
	BYTE	bUserID;			//找回方式：0，使用用户名;1，使用用户ID
	INT		iUserID;			//用户ID
	CHAR	szUserName[64];		//用户账号
	CHAR	szPhoneNum[20];		//用户手机号
	CHAR	szNewPWD[64];		//用户新密码

	MSG_GP_I_ForgetPWD()
	{
		::memset(this, 0x0, sizeof(MSG_GP_I_ForgetPWD));
	}
};

//////////////////////////////////////////////////////////////////////////
//手机短信验证码获取
#define		MDM_GP_SMS				117
#define		ASS_GP_SMS_VCODE		1
typedef struct MSG_GP_SmsVCode
{
	char szName[61];		//用户名
	char szMobileNo[50];	//手机号码
	char szVCode[36];		//产生的验证码，使用MD5加密
	/*
	 * 短信验证码类型
	 * 0为登陆验证
	 * 1为锁机验证
	 * 2为解锁机验证
	 * 3为修改手机验证
	 * 4为绑定手机验证
	 * 5为解除绑定手机验证
	 */
	UINT nType;				//短信验证码类型
								
}  MSG_GP_SmsVCode;;

//////////////////////////////////////////////////////////////////////////
///绑定手机号码

#define		MDM_GP_BIND_MOBILE	112
#define		ASS_GP_BIND_MOBILE	1

typedef struct MSG_GP_S_BindMobile
{
	UINT								dwUserID;				//用户ID
	UINT								dwCommanType;			//命令请求类型,1表示要求绑定，0表示要求解除绑定
	char								szMobileNo[50];			//手机号码
}  MSG_GP_S_BindMobile;

///绑定/解除绑定手机返回数据包
typedef struct MSG_GP_R_BindMobile
{
	UINT	dwUserID;
	UINT	dwCommanType;				//请求命令类型（0请求解除绑定手机结果，1请求绑定手机结果）
	UINT	dwCommanResult;				//请求的结果
}  MSG_GP_R_BindMobile;

//////////////////////////////////////////////////////////////////////////
///通信主标识
#define	MDM_GP_MESSAGE					103								///系统消息
///通信辅助标识
#define ASS_GP_NEWS_SYSMSG				1								///新闻和系统消息
#define ASS_GP_DUDU						2								///小喇叭
#define ASS_GP_TALK_MSG					3								//聊天消息

///聊天结构 
struct MSG_GR_RS_NormalTalk
{
	UINT								crColor;							///字体颜色
	WORD								iLength;							///信息长度
	INT									dwSendID;							///用户 ID
	INT									dwTargetID;							///目标 ID
	INT									nDefaultIndex;						///=0，输入的内容，>0，选择的内容
	CHAR								szMessage[MAX_TALK_LEN+1];			///聊天内容
};
#define MDM_GP_SHARESTR		156
#define ASS_GP_SHARE_STR				1								//请求大厅分享的语句
struct MSG_GR_RS_SHARESTR
{
	char		shareStr[256];
};
//移动端喇叭
#define MDM_GP_SPEAKER                 137
#define ASS_GP_SPEAKER_REQUEST         0     //查询
#define ASS_GP_SPEAKER_RESULT          1     //查询结果
#define ASS_GP_SPEAKER_SEND            2     //广播消息
#define ASS_GP_SPEAKER_SEND_RESULT     3     //广播消息效果

//查询喇叭消耗金币
struct MSG_GP_S_SPEAKER_MONEY
{
	INT iUserID;
	MSG_GP_S_SPEAKER_MONEY()
	{
		memset(this, 0, sizeof(MSG_GP_S_SPEAKER_MONEY));
	}
};

//查询喇叭消耗金币结果
struct MSG_GP_S_SPEAKER_MONEY_RES
{
	INT iUserID;
	INT iMoney; //消耗的金币
	MSG_GP_S_SPEAKER_MONEY_RES()
	{
		memset(this, 0, sizeof(MSG_GP_S_SPEAKER_MONEY_RES));
	}
};

//发送广播消息
struct MSG_GP_S_SPEAKER_SEND
{
	INT iUserID;
	INT iMessageLen;//信息长度
	char szMessage[500];
	MSG_GP_S_SPEAKER_SEND()
	{
		memset(this, 0, sizeof(MSG_GP_S_SPEAKER_SEND));
	}
};

struct MSG_GP_S_SPEAKER_SEND_RES
{
	INT iSuccess;//操作成功（0：未知错误， 1：发送成功， 2、用户不存在，3、钱包金币不足）
	INT iMessageLen;//信息长度
	char szNickname[50];
	char szMessage[500];
	MSG_GP_S_SPEAKER_SEND_RES()
	{
		memset(this, 0, sizeof(MSG_GP_S_SPEAKER_SEND_RES));
	}
};
////////////////////////////////////////////////////////////////////////
//获取背包信息
#define        MDM_GP_GETBUG              157

//获取任务列表状态
///////////////////////////////////////////////////////////////////////
#define		 MDM_GP_GETTASKINFO			158

struct MSG_GP_O_TaskInfo_Data
{
	int  firstinjoinmatch;
	int  firstcreateclub;
	int  firstplaymatch;
};

//////////////////////////////////////////////////////////////////////////
//分享送积分
#define		MDM_GP_SHARE				155
#define		ASS_GP_USE_SHARE_SUCCESS	1
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// 道具获取以及兑换消息。

#define		MDM_GP_PROP							140
#define		ASS_PROP_BUY_NEW					0x0b
#define		ASS_PROP_GETUSERPROP				0x01
#define		DTK_GP_PROP_BUY_ERROR				10
#define		DTK_GP_PROP_BUY_NOMONEY				20
#define		DTK_GP_PROP_BUY_SUCCEED				80		//购买成功
#define		DTK_GP_PROP_BUYANDUSE_SUCCEED		81		//即买即用成功

typedef struct _TAG_PROP_BUY
{
	INT		dwUserID;					//购买者ID
	INT		nPropID;					//道具ID
	INT		iPropPayMoney;				//总共的金币
	INT		nPropBuyCount;				//道具数量
	/*
	 * 这个字段意思是即买即用（1）还是买了等待被用（0），如果像等下管理员换成话费的应该填0，否则不会存在用户道具表里面
	 */
	bool	bUse;						//手游特别注意

	_TAG_PROP_BUY()
	{
		memset(this, 0x0, sizeof(_TAG_PROP_BUY));
		bUse = false;
	}
}  _TAG_PROP_BUY;

typedef struct MSG_PROP_C_GETSAVED
{
	INT		dwUserID;						//用户ID
}  MSG_PROP_C_GETSAVED;


// 服务端发送的数据。
// 从服务器上取得的个人道具，每一个道具一条消息。
typedef struct MSG_PROP_S_GETPROP
{
	INT			dwUserID;						//用户ID号
	INT			nPropID;						//道具ID号
	INT			nHoldCount;						//拥有道具的数量
	CHAR		szPropName[50];					//道具名称
	INT			attribAction;					//操作属性
	INT			attribValue;					//取值属性
	INT			dwCost;							//当前该笔交易花了多少钱，服务器须提供此数据
	INT			iPrice;							//道具价格
	INT			iVipPrice;						//VIP道具价格
}  MSG_PROP_S_GETPROP, _TAG_USERPROP;


//主ID=140，辅助ID=21
#define		MDM_GP_NET_PROP_BUY_LOG		140
#define		ASS_GP_NET_PROP_BUY_LOG		21

typedef struct NET_PROP_BUY_LOG_RESULT
{
	CHAR	szPropName[50];						//道具名称 
	BYTE	bPropID;							//道具ID
	UINT	iPorpCount;							//购买数量
	BYTE	bCostType;							//花费的类型（1-金币，2-奖券）
	UINT	iCost;								//花费的数量
	LLONG	llTime;								//购买的时间
}  NET_PROP_BUY_LOG_RESULT;


//////////////////////////////////////////////////////////////////////////////////////////
//
// 排行榜消息
//////////////////////////////////////////////////////////////////////////////////////////
#define MDM_GP_PAIHANGBANG					133

typedef struct MSG_GP_PaiHangBang_In
{
	INT		count;		//前几名
	INT     type;		// 0:金币总合来排 1: 钱包的来 2: 银行的来 其它值，直接失败 3:魅力
}  MSG_GP_PaiHangBang_In;

typedef struct MSG_GP_MoneyPaiHangBang_Item
{
	CHAR							nickName[64];						// 用户昵称
	int                             iUserID;                            // 用户ID
	int                             iUserLogoID;                        // 用户头像ID
	LLONG							i64Money;							// 用户金币
}  MSG_GP_MoneyPaiHangBang_Item;

typedef struct MSG_GP_MoneyPaiHangBang_Result
{
	MSG_GP_MoneyPaiHangBang_Item items[20];
}  MSG_GP_MoneyPaiHangBang_Result;

//////////////////////////////////////////////////////////////////////////////////////////
//
// 签到消息
//////////////////////////////////////////////////////////////////////////////////////////
#define MDM_GP_SIGN							134
#define ASS_GP_SIGN_CHECK					1
#define ASS_GP_SIGN_DO						2

typedef struct MSG_GP_S_SIGN_CHECK_RESULT
{
	UINT				dwUserID;           // 用户ID
	BYTE				iRs;			    // 结果: 0:未签到 1-已经签到	
	BYTE				byCountDay;		    // 连续签到天数
	INT					iAwardMoney[7];		// 签到奖励金币
	INT                 iAwardJewels[7];    // 签到奖励钻石
	INT                 iAwardLotteries[7]; // 签到奖励奖券
} MSG_GP_S_SIGN_CHECK_RESULT;

// 结果使用通过Handlecode 0-失败 1-签到成功，
typedef struct MSG_GP_S_SIGN_DO_RESULT
{
	UINT				dwUserID;       // 用户ID
	INT					iGetMoney;		// 当天签到获取的金币
	INT                 iGetJewels;     // 当天签到获取的钻石
	INT                 iGetLotteries;  // 当前签到奖励的奖券
}  MSG_GP_S_SIGN_DO_RESULT;

//////////////////////////////////////////////////////////////////////////////////////////
//
// 在线奖励消息
//////////////////////////////////////////////////////////////////////////////////////////

//在线奖励金币
#define MDM_GP_ONLINE_AWARD					135
#define ASS_GP_ONLINE_AWARD_CHECK			1
#define ASS_GP_ONLINE_AWARD_DO				2

//0失败  1 成功
typedef struct MSG_GP_S_ONLINE_AWARD_CHECK_RESULT
{
	UINT	dwUserID;	
	UINT	iLeftTime;				//还差多少时间可以领取,0表示可以领取
	UINT	iOnLineTimeMoney;		//能领取的金币
}  MSG_GP_S_ONLINE_AWARD_CHECK_RESULT;

typedef struct MSG_GP_S_ONLINE_AWARD_DO_RESULT
{
	UINT	dwUserID;	
	UINT    iCurrentGetMoney;		//此次领取的金币
	UINT	iNextTime;				//下次时间
	UINT	iNextMoney;				//下次金币
} MSG_GP_S_ONLINE_AWARD_DO_RESULT;

////////////////////////////////////////////////////////////////////////////////////////////
//
//俱乐部消息
//
/////////////////////////////////////////////////////////////////////////////////////////////

//俱乐部消息
#define	MDM_GP_CLUB  143
#define	ASS_GP_CREATE_CLUB			0				// 创建俱乐部
#define	ASS_GP_DISSMISS_CLUB			1				// 解散俱乐部
#define	ASS_GP_JOIN_CLUB				2				// 申请加入俱乐部
#define	ASS_GP_CLUB_USERLIST			3				// 俱乐部玩家列表
#define	ASS_GP_CLUB_TALK				4				// 俱乐部聊天
#define	ASS_GP_CLUB_ROOMLIST			5				// 俱乐部房间列表
#define	ASS_GP_CLUB_CREATEROOM		6				// 俱乐部创建房间
#define	ASS_GP_CLUB_CHANGENAME		7				// 俱乐部更名
#define	ASS_GP_CLUB_KICKUSER			8				// 会长踢人
#define	ASS_GP_CLUB_STATISTICS			9				// 俱乐部统计信息
#define	ASS_GP_CLUB_LIST				10				// 俱乐部列表
#define	ASS_GP_REVIEW_LIST			11				// 会长审核列表
#define	ASS_GP_MASTER_OPTION			12				// 会长审核操作
#define	ASS_GP_CLUB_NOTICE			13				// 会长更改俱乐部公告
#define	ASS_GP_CLUB_USERCHANGE		14				// 退出/加入俱乐部(监听消息)
#define	ASS_GP_ENTER_CLUB			15				// 进入俱乐部（开始接受消息）
#define	ASS_GP_CLUB_ROOMCHANGE		16			    // 俱乐部创建/解散房间
#define	ASS_GP_CLUB_JOINRESULT		17				// 俱乐部进入成功/失败，成功返回俱乐部信息，失败返回空
#define	ASS_GP_JOIN_CLUB_TOMASTER	    18				// 申请加入俱乐部通知创建者
#define	ASS_GP_LEAVE_CLUB			19				// 申请离开俱乐部
#define	ASS_GP_CLUB_KICKUSER_TAR		20				// 会长踢人通知被踢者
#define	ASS_GP_CLUB_NOTICE_UPDATE		21				// 更改俱乐部公告后更新消息
#define	ASS_GP_CLUB_NAME_UPDATE		22				// 更改俱乐部名字后更新消息
#define	ASS_GP_DISSMISS_CLUB_NOTIFY	23			    // 解散俱乐部后更新消息
#define	ASS_GP_GET_BUYDESKRECORD     24				// 会长查询开房记录
#define	ASS_GP_SET_CLUBJEWELS		25				//添加俱乐部钻石
#define	ASS_GP_CLUBJEWELSCHANGE		26				// 俱乐部钻石更新广播通知
#define	ASS_GP_GET_CLUBJEWELS		27				//俱乐部钻石
#define ASS_GP_JJ_CLUB_ROOMLIST     28				//晋江俱乐部房间列表
#define ASS_GP_ON_USER_LOGIN_ROOM		32				//用户登录房间
#define ASS_GP_ON_USER_QUIT_ROOM		33				// 用户退出房间

//茶楼对接
#define ASS_GP_CLUB_GetCreateAccess		34				// 获取创建俱乐部的权限
#define ASS_GP_CLUB_GetCreateTeaHouseAccess		35				// 获取创建俱乐部茶楼的权限
#define ASS_GP_CLUB_CreateTeaHouse		36				// 创建俱乐部茶楼
#define ASS_GP_CLUB_InitAllTeaHouseData		37				// 初始化所有俱乐部茶楼和桌子数据
#define ASS_GP_CLUB_UpdateTeaHouseDesk		38				// 更新俱乐部茶楼桌子信息
#define ASS_GP_CLUB_LeaveHall				39				// 离开俱乐部大厅
#define ASS_GP_CLUB_OnTeaHouseCreate		40				// 某人创建了一个俱乐部茶楼(修改俱乐部茶楼)
#define ASS_GP_CLUB_ReplenishTeahouseDesk	41				// 刷新俱乐部茶楼桌子
#define ASS_GP_CLUB_DeleteTeaHouse		42				// 删除俱乐部茶楼，传入结构：MSG_GP_I_Club_DeleteTeahouse  返回消息：ASS_GP_CLUB_UpdateTeaHouseDesk
#define ASS_GP_CLUB_OperationRequest		43				// 通用的操作请求消息（传入的基础结构：SClubRequestBase，实际结构跟请求的消息类型有关，返回也如此）
#define ASS_GP_CLUB_TipMsg				44					// 俱乐部返回给客户端的通用提示消息（错误码如：ERR_GP_CLUB_DeskPlaying）

//数据队列信息头
struct DataLineHead
{
	UINT								uSize;					//数据大小
	UINT								uDataKind;					//数据类型
};

struct DataBaseResultLine
{
	DataLineHead						LineHead;					///队列头
	UINT								uHandleRusult;				///结果结果
	UINT								uHandleKind;				///处理类型
	UINT								uIndex;					///对象索引
	unsigned long						dwHandleID;				///对象标识
	//DWORD						dwHandleID	;


};

//茶楼错误类型
enum E_HALL_ERROR_CLUB							// MDM_GP_CLUB
{
	ERR_GP_CLUB_REQUEST_SUCCESS = 0,				// 操作成功
	ERR_GP_USER_NOT_FOUND = 1,					// 用户信息错误
	ERR_GP_NAME_LIMITE = 2,						// 俱乐部名称包含屏蔽字
	ERR_GP_CLUB_DeskPlaying = 10,					// 有正在游戏的桌子
	ERR_GP_CLUB_LockedTeahouse = 11,					// 茶楼已被锁定（暂停）
	ERR_GP_CLUB_NeedRedFlowers = 12,					//红花不足
	ERR_GP_CLUB_NeedAccess = 13,					// 权限不足
	ERR_GP_CLUB_NotClubOwner = 14,					// 不是俱乐部拥有者
	ERR_GP_CLUB_InvalidUser = 15,						// 无效的用户
	ERR_GP_CLUB_SetBlackList = 16,				// 被设为黑名单（返回数据：俱乐部ID）
	ERR_GP_CLUB_NotPartner = 17,				// 不是一个合伙人
	ERR_GP_CLUB_DeskmateLimit = 18,				// 同桌限制
	ERR_GP_CLUB_UsersLimit = 19,				// 俱乐部人数已达上限或该用户加入的俱乐部数量已达上限
};

// 俱乐部标记
enum EClubFlags
{
	ClubFlags_None = 0x0,

	ClubFlags_PauseAllTeahouse = 0x00000001,	// 暂停所有茶楼
};

// 俱乐部用户权限
enum EClubAccess
{
	ClubAccess_None = 0x0,
	ClubAccess_Admin = 0x00000001,	// 管理员
	ClubAccess_Partner = 0x00000002,	// 合伙人
	ClubAccess_BlackList = 0x00000004,	// 黑名单用户
	ClubAccess_AllowAdminTakeRedFlower = 0x00000008,	// 允许管理员向合伙人的下线成员送花/摘花
};

//动作类型处理
enum
{
	ClubRequest_None = 0,
	ClubRequest_PauseAllTeahouse = 1,		// 暂停所有楼层（传入结构：SClubRequest_PauseAllTeahouse	返回"SClubTeahouseDeskUpdate_SetPauseState"消息通知）
	ClubRequest_DismissTeahouseDesk = 2,		// 解散茶楼桌子（传入结构：SClubRequest_DismissTeahouseDesk	返回结构：无）
	ClubRequest_KickDeskUser = 3,		// 踢出桌子上的用户（传入结构：SClubRequest_KickDeskUser		返回结构：无）
	ClubRequest_SetAdminAccess = 4,		// 设置管理员权限账号（传入结构：SClubRequest_SetAdminAccess	返回消息：SClubResponseBase）
	ClubRequest_GetAdminUsers = 5,		// 获取管理员权限账号（传入结构：SClubRequestBase				返回结构：SClubRequest_GetAccessUsers_Ret）
	ClubRequest_GetDeskUserInfos = 6,	// 获取桌子用户信息（传入结构：SClubRequest_GetDeskUserInfos	返回结构：SClubOperationResponse_GetDeskUserInfos）
	ClubRequest_QueryUserInfo = 7,		// 查询用户信息（传入结构：SClubRequestBase						返回结构：SClubOperationResponse_QueryUserInfo）
	ClubRequest_QueryClubInfo = 8,		// 查询俱乐部信息（传入结构：SClubRequestBase					返回结构：SClubOperationResponse_QueryClubInfo）
	ClubRequest_ModifyClubInfo = 9,		// 修改俱乐部信息（传入结构：SClubRequest_ModifyClubInfo		返回结构：SClubResponseBase）
	ClubRequest_GiveRedFlowers = 10,		// 给用户送红花（传入结构：SClubRequest_GiveRedFlowers			返回结构：SClubOperationResponse_GiveRedFlowers）
	ClubRequest_TakeOutRedFlowers = 11,		// 摘取用户红花（传入结构：SClubRequest_TakeOutRedFlowers		返回结构：SClubOperationResponse_TakeOutRedFlowers）
	ClubRequest_QueryRedFlowersLog = 12,		// 查询红花日志（传入结构：SClubRequest_QueryRedFlowersLog		返回结构：SClubOperationResponse_QueryRedFlowersLog）
	ClubRequest_QueryPendingLog = 13,		// 查询待处理日志（传入结构：SClubRequest_QueryPendingLog		返回结构：SClubOperationResponse_QueryPendingLog）
	ClubRequest_ProcessPendingLog = 14,		// 处理待处理日志（传入结构：SClubRequest_ProcessPendingLog		返回结构：SClubOperationResponse_ProcessPendingLog）
	ClubRequest_QueryMemberList = 15,		// 查询成员列表（传入结构：SClubRequest_QueryMemberList			返回结构：SClubOperationResponse_QueryMemberList）
	ClubRequest_QueryUserGameLog = 16,		// 查询用户战绩（传入结构：SClubRequest_QueryUserGameLog		返回结构：SClubOperationResponse_QueryUserGameLog）
	ClubRequest_SetBlackListUser = 17,		// 将用户加入/移出黑名单（传入结构：SClubRequest_SetBlackListUser	返回结构：SClubOperationResponse_SetBlackListUser）
	ClubRequest_SetDeskmateLimit = 18,		// 设置同桌限制			（传入结构：SClubRequest_SetDeskmateLimit	返回结构：SClubOperationResponse_SetDeskmateLimit）
	ClubRequest_QueryDeskmateLimit = 19,		// 查询同桌限制			（传入结构：SClubRequestBase				返回结构：SClubOperationResponse_QueryDeskmateLimit）
	ClubRequest_SetPartner = 20,		// 设置/移除合伙人		（传入结构：SClubRequest_SetPartner		返回结构：SClubOperationResponse_SetPartner）
	ClubRequest_SearchMember = 21,		// 搜索成员				（传入结构：SClubRequest_SearchMember		返回结构：SClubOperationResponse_SearchMember）
	ClubRequest_SetMemberOfPartner = 22,		// 加入/移除合作群		（传入结构：SClubRequest_SetMemberOfPartner	返回结构：SClubOperationResponse_SetMemberOfPartner）
	ClubRequest_QueryPartnerList = 23,		// 查询合伙人列表		（传入结构：SClubRequest_QueryPartnerList				返回结构：SClubOperationResponse_QueryPartnerList）
	ClubRequest_SetPartnerRedFlowersRate = 24,		// 设置合伙人的红花比例	（传入结构：SClubRequest_SetPartnerRedFlowersRate	返回结构：SClubResponseBase）
	ClubRequest_QueryOnlineUserCount = 25,		// 查询俱乐部在线成员数量（传入结构：SClubRequestBase	返回结构：SClubOperationResponse_QueryOnlineUserCount）
	ClubRequest_ModifyTeahouseName = 26,		// 修改楼层名字			（传入结构：SClubRequest_ModifyTeahouseName	返回结构：SClubOperationResponse_ModifyTeahouseName）
	ClubRequest_AllowAdminTakeRedFlower = 27,		// 设置是否允许管理员向合伙人的下线成员送花/摘花（传入结构：SClubRequest_AllowAdminTakeRedFlower	返回结构：SClubOperationResponse_AllowAdminTakeRedFlower）
	ClubRequest_QueryPartnerInfo = 28,		// 查询合伙人信息		（传入结构：SClubRequestBase				返回结构：SClubOperationResponse_QueryPartnerInfo）
	ClubRequest_QueryQunGameLog = 29,		// 查询本群战绩			（传入结构：SClubRequest_QueryQunGameLog	返回结构：SClubOperationResponse_QueryQunGameLog）
	ClubRequest_ModifyMemberNote = 30,		// 修改成员备注			（传入结构：SClubRequest_ModifyMemberNote	返回结构：SClubOperationResponse_ModifyMemberNote））
	ClubRequest_AddMember = 31,		// 添加成员到俱乐部		（传入结构：SClubRequest_AddMember			返回结构：SClubResponseBase）
};

enum
{
	Club_MaxAccessUsers = 5,	// 最大5个管理员权限账号
};

enum
{
	ClubTeahouseDeskMaxUsers = 6,
};

// 俱乐部茶楼通用的操作返回消息
enum EClubOperationResponse
{
	ClubOperationResponse_None = 0,
};

enum EClubRedFlowersLogType
{
	ClubRedFlowersLogType_None = 0,

	ClubRedFlowersLogType_Give = 1,	// 送花
	ClubRedFlowersLogType_TakeOut = 2,	// 摘花
	ClubRedFlowersLogType_TakeOut_Game = 3,	// 从游戏结算中摘花
	ClubRedFlowersLogType_Score_Game = 4,	// 从游戏结算中获得/扣除红花
	ClubRedFlowersLogType_Rebate = 5,	// 俱乐部拥有者或合伙人获得的返利
};

// 客户端向MServer发出的和种操作请求，然后MServer会将此消息转发给GServer处理
struct SClubRequestBase
{
	int		clubId;
	int		gameNameId;
	BYTE		msg;	// 如：ClubRequest_PauseAllTeahouse
};

// 暂停茶楼桌子请求
struct SClubRequest_PauseAllTeahouse : public SClubRequestBase
{
	bool	bPause;
};

// 俱乐部解散茶楼桌子请求
struct SClubRequest_DismissTeahouseDesk : public SClubRequestBase
{
	int	teahouseId;
	int	deskIndex;
};

// 俱乐部茶楼 - 踢出桌子用户请求
struct SClubRequest_KickDeskUser : public SClubRequestBase
{
	int	teahouseId;
	int	deskIndex;
	int	userId;

	SClubRequest_KickDeskUser()
	{
		memset(this, 0, sizeof(SClubRequest_KickDeskUser));
	}
};

// 俱乐部茶楼 - 设置用户权限
struct SClubRequest_SetAccessUsers : public SClubRequestBase
{
	int	userIdList[Club_MaxAccessUsers];	// 保存要设置的每个用户id
	int	accessList[Club_MaxAccessUsers];	// 保存对应用户的权限

	SClubRequest_SetAccessUsers()
	{
		memset(this, 0, sizeof(SClubRequest_SetAccessUsers));
	}
};

// 俱乐部茶楼 - 请求获取桌子用户信息
struct SClubRequest_GetDeskUserInfos : public SClubRequestBase
{
	int   teahouseId;
	int   deskIndex;

	SClubRequest_GetDeskUserInfos()
	{
		memset(this, 0, sizeof(SClubRequest_GetDeskUserInfos));
	}
};

// 俱乐部茶楼 - 修改俱乐部信息
struct SClubRequest_ModifyClubInfo : public SClubRequestBase
{
	char	szClubName[40];
	UINT	bigWinnerMaxScore;	// 大赢家最低分数设置

	SClubRequest_ModifyClubInfo()
	{
		memset(this, 0, sizeof(SClubRequest_ModifyClubInfo));
	}
};

// 俱乐部茶楼 - 给用户送红花
struct SClubRequest_GiveRedFlowers : public SClubRequestBase
{
	int	targetUserId;	// 要赠送的用户id
	int	redFlowers;		// 送出的红花数

	SClubRequest_GiveRedFlowers()
	{
		memset(this, 0, sizeof(SClubRequest_GiveRedFlowers));
	}
};

// 俱乐部茶楼 - 向用户摘取红花
struct SClubRequest_TakeOutRedFlowers : public SClubRequestBase
{
	int	targetUserId;	// 要摘取的用户id
	int	redFlowers;		// 摘取的红花数

	SClubRequest_TakeOutRedFlowers()
	{
		memset(this, 0, sizeof(SClubRequest_TakeOutRedFlowers));
	}
};

// 俱乐部茶楼 - 查询红花日志
struct SClubRequest_QueryRedFlowersLog : public SClubRequestBase
{
	int	targetUserId;	// 要查询的用户id（若查询所有，则填0）
	int	partnerUserId;	// 合伙人用户id（若要查询合伙人及其下属成员的所有红花日志，则填此值）

	SClubRequest_QueryRedFlowersLog()
	{
		memset(this, 0, sizeof(SClubRequest_QueryRedFlowersLog));
	}
};

// 俱乐部茶楼 - 查询待处理日志
struct SClubRequest_QueryPendingLog : public SClubRequestBase
{
	int	targetUserId;	// 要查询的用户id（若查询所有，则填0）

	SClubRequest_QueryPendingLog()
	{
		memset(this, 0, sizeof(SClubRequest_QueryPendingLog));
	}
};

// 俱乐部茶楼 - 处理待处理日志
struct SClubRequest_ProcessPendingLog : public SClubRequestBase
{
	int	targetUserId;	// 要处理的用户id
	int	logId;			// 要处理的日志id

	SClubRequest_ProcessPendingLog()
	{
		memset(this, 0, sizeof(SClubRequest_ProcessPendingLog));
	}
};

// 俱乐部茶楼 - 查询成员列表
struct SClubRequest_QueryMemberList : public SClubRequestBase
{
	int		partnerUserId;	// 合伙人用户id（非合作群填0）
	int		query_day;		// 0=今日 1=昨日 2=前日

	SClubRequest_QueryMemberList()
	{
		memset(this, 0, sizeof(SClubRequest_QueryMemberList));
	}
};

// 俱乐部茶楼 - 查询用户战绩
struct SClubRequest_QueryUserGameLog : public SClubRequestBase
{
	int		findUserId;
	int		query_day;		// 0=今日 1=昨日 2=前日

	SClubRequest_QueryUserGameLog()
	{
		memset(this, 0, sizeof(SClubRequest_QueryUserGameLog));
	}
};

// 俱乐部茶楼 - 添加/移除黑名单
struct SClubRequest_SetBlackListUser : public SClubRequestBase
{
	int		userId;
	bool		bAddToBlackList;	// 是否添加到黑名单

	SClubRequest_SetBlackListUser()
	{
		memset(this, 0, sizeof(SClubRequest_SetBlackListUser));
	}
};

// 俱乐部茶楼 - 设置同桌限制
struct SClubRequest_SetDeskmateLimit : public SClubRequestBase
{
	int		userId;
	int		userId2;
	bool		bSet;		//true设置限制，false解除限制

	SClubRequest_SetDeskmateLimit()
	{
		memset(this, 0, sizeof(SClubRequest_SetDeskmateLimit));
	}
};

// 俱乐部茶楼 - 添加/移除合伙人
struct SClubRequest_SetPartner : public SClubRequestBase
{
	int		userId;
	bool		bSetPartner;	// 是否添加为合伙人

	SClubRequest_SetPartner()
	{
		memset(this, 0, sizeof(SClubRequest_SetPartner));
	}
};

// 俱乐部茶楼 - 搜索成员
struct SClubRequest_SearchMember : public SClubRequestBase
{
	int		searchUserId;		// 要搜索的成员用户id
	int		partnerUserId;		// 所属的合伙人用户id（若不填，则返回一般成员）

	SClubRequest_SearchMember()
	{
		memset(this, 0, sizeof(SClubRequest_SearchMember));
	}
};

// 俱乐部茶楼 - 加入/移除合作群
struct SClubRequest_SetMemberOfPartner : public SClubRequestBase
{
	int		partnerUserId;	// 合伙人用户id
	int		inviteUserId;	// 邀请的成员用户id
	bool		bAdd;			// 是否加入到合作群

	SClubRequest_SetMemberOfPartner()
	{
		memset(this, 0, sizeof(SClubRequest_SetMemberOfPartner));
	}
};

// 俱乐部茶楼 - 设置合伙人的红花比例
struct SClubRequest_SetPartnerRedFlowersRate : public SClubRequestBase
{
	int		partnerUserId;
	float		rate;

	SClubRequest_SetPartnerRedFlowersRate()
	{
		memset(this, 0, sizeof(SClubRequest_SetPartnerRedFlowersRate));
	}
};

// 俱乐部茶楼 - 修改楼层名字
struct SClubRequest_ModifyTeahouseName : public SClubRequestBase
{
	int		teahouseId;
	char		szName[32];

	SClubRequest_ModifyTeahouseName()
	{
		memset(this, 0, sizeof(SClubRequest_ModifyTeahouseName));
	}
};

// 俱乐部茶楼 - 查询合伙人列表
struct SClubRequest_QueryPartnerList : public SClubRequestBase
{
	int query_day;		// 0=今日 1=昨日 2=前日

	SClubRequest_QueryPartnerList()
	{
		memset(this, 0, sizeof(SClubRequest_QueryPartnerList));
	}
};

// 俱乐部茶楼 - 设置是否允许管理员向合伙人的下线成员送花/摘花
struct SClubRequest_AllowAdminTakeRedFlower : public SClubRequestBase
{
	bool	bAllow;

	SClubRequest_AllowAdminTakeRedFlower()
	{
		memset(this, 0, sizeof(SClubRequest_AllowAdminTakeRedFlower));
	}
};

// 俱乐部茶楼 - 查询本群战绩
struct SClubRequest_QueryQunGameLog : public SClubRequestBase
{
	int query_day;		// 0=今日 1=昨日 2=前日

	SClubRequest_QueryQunGameLog()
	{
		memset(this, 0, sizeof(SClubRequest_QueryQunGameLog));
	}
};

// 俱乐部茶楼 - 设置成员备注信息
struct SClubRequest_ModifyMemberNote : public SClubRequestBase
{
	int		targetUserId;	// 要修改的用户id
	char	szNote[20];		// 备注

	SClubRequest_ModifyMemberNote()
	{
		memset(this, 0, sizeof(SClubRequest_ModifyMemberNote));
	}
};

// 俱乐部茶楼 - 添加成员到俱乐部
struct SClubRequest_AddMember : public SClubRequestBase
{
	int		targetUserId;	// 要添加的用户id

	SClubRequest_AddMember()
	{
		memset(this, 0, sizeof(SClubRequest_AddMember));
	}
};

//////////////////////////////////////////////////////////////

// 客户端发送的操作响应数据
struct SClubResponseBase
{
	int		clubId;
	BYTE		msg;	// 如：ClubRequest_PauseAllTeahouse
};

// 俱乐部茶楼 - 获取有权限的用户 - 返回给客户端的结构
struct SClubRequest_GetAccessUsers_Ret : public SClubResponseBase
{
	int	userIdList[Club_MaxAccessUsers];	// 保存每个用户id
	int	accessList[Club_MaxAccessUsers];	// 保存对应用户的权限

	SClubRequest_GetAccessUsers_Ret()
	{
		memset(this, 0, sizeof(SClubRequest_GetAccessUsers_Ret));
	}
};

// 俱乐部茶楼 - 获取桌子用户信息 - 返回给客户端的结构
struct SClubOperationResponse_GetDeskUserInfos : public SClubResponseBase
{
	int		teahouseId;
	int			userIdList[ClubTeahouseDeskMaxUsers];
	char		szUserNames[ClubTeahouseDeskMaxUsers][32];
	LLONG		userScoreList[ClubTeahouseDeskMaxUsers];	// -1表示等待中暂无

	SClubOperationResponse_GetDeskUserInfos()
	{
		memset(this, 0, sizeof(SClubOperationResponse_GetDeskUserInfos));
	}
};

// 俱乐部茶楼 - 获取用户信息 - 返回给客户端的结构
struct SClubOperationResponse_QueryUserInfo : public SClubResponseBase
{
	UINT		access;		// 权限（如：ClubAccess_Admin）
	float		redFlowers;	// 红花数量

	SClubOperationResponse_QueryUserInfo()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryUserInfo));
	}
};

// 俱乐部茶楼 - 查询俱乐部信息 - 返回给客户端的结构
struct SClubOperationResponse_QueryClubInfo : public SClubResponseBase
{
	char		szClubName[40];
	UINT		bigWinnerMaxScore;	// 大赢家最低分数设置

	SClubOperationResponse_QueryClubInfo()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryClubInfo));
	}
};

// 俱乐部茶楼 - 给用户送红花 - 返回给客户端的结构
struct SClubOperationResponse_GiveRedFlowers : public SClubResponseBase
{
	int		targetUserId;	// 要赠送的用户id
	float		srcRedFlowers;	// 送花者/摘花者的最新红花数
	float		dstRedFlowers;	// 得花者/被摘花者的最新红花数

	SClubOperationResponse_GiveRedFlowers()
	{
		memset(this, 0, sizeof(SClubOperationResponse_GiveRedFlowers));
	}
};

// 俱乐部茶楼 - 向用户摘取红花 - 返回给客户端的结构
struct SClubOperationResponse_TakeOutRedFlowers : public SClubResponseBase
{
	int		targetUserId;	// 要赠送的用户id
	float		dstRedFlowers;	// 接收红花者的最新红花数
	float		selfRedFlowers;	// 赠送者的最新红花数

	SClubOperationResponse_TakeOutRedFlowers()
	{
		memset(this, 0, sizeof(SClubOperationResponse_TakeOutRedFlowers));
	}
};

//红花日志
struct SClubFlowersLog
{
	EClubRedFlowersLogType	type;
	int						srcUserId;			// 送花者或摘花者用户id
	char					szSrcUserName[20];	// 送花者或摘花者用户名
	int						dstUserId;			// 得花者或被摘花者用户id
	char					szDstUserName[20];	// 得花者或被摘花者用户名
	float					srcOldRedFlowers;	// 送花者或摘花者原有红花数
	float					dstOldRedFlowers;	// 得花者或被摘花者原有红花数
	float					redFlowers;			// 赠送或摘取的红花数
	UINT					date;				// 操作日期时间戳（同time()所返回的时间戳，单位为秒）
};

// 俱乐部茶楼 - 查询红花日志 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_QueryRedFlowersLog : public SClubResponseBase
{
	int				logCount;
	SClubFlowersLog		flowersLogList[0];

	SClubOperationResponse_QueryRedFlowersLog()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryRedFlowersLog));
	}
};

//待处理日志
struct SClubPendingLog
{
	int		logId;
	int		userId;
	char		szName[20];
	float		amount;
	bool		bProcessed;
	UINT		date;		// 日期时间戳（同time()所返回的时间戳，单位为秒）
};

// 俱乐部茶楼 - 查询待处理日志 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_QueryPendingLog : public SClubResponseBase
{
	int				logCount;
	SClubPendingLog		pendingLogList[0];

	SClubOperationResponse_QueryPendingLog()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryPendingLog));
	}
};

// 俱乐部茶楼 - 处理待处理日志 - 返回给客户端的结构
struct SClubOperationResponse_ProcessPendingLog : public SClubResponseBase
{
	int		logId;		// 要处理的日志id
	bool		bOK;		// 是否处理成功
	bool		bProcessed;	// 最新的处理状态

	SClubOperationResponse_ProcessPendingLog()
	{
		memset(this, 0, sizeof(SClubOperationResponse_ProcessPendingLog));
	}
};

// 俱乐部成员信息
struct SClubMemberInfo
{
	int		userId;
	char		szUserName[20];
	char		szNote[20];		// 备注
	char		szBossName[20];	// 上级名字
	UINT		access;			// 用户权限（参见枚举类型：EClubAccess）
	float		redFlowers;		// 当前红花数
	int		winCount;		// 当日赢的次数
	int		bigWinnerCount;	// 当日的大赢家次数
	float		winRedFlowers;	// 当日赢得的红花数
	int		roomCount;		// 当日参与的房间总数

	SClubMemberInfo()
	{
		memset(this, 0, sizeof(SClubMemberInfo));
	}
};

// 俱乐部茶楼 - 查询成员列表 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_QueryMemberList : public SClubResponseBase
{
	//int		partnerUserId;	// 合伙人用户id（非合作群填0）
	int		iCreaterID;
	int		count;
	SClubMemberInfo	memberInfos[0];

	SClubOperationResponse_QueryMemberList()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryMemberList));
	}
};

// 俱乐部用户游戏日志
struct SClubUserGameLog
{
	SClubUserGameLog()
	{
		memset(this, 0, sizeof(SClubUserGameLog));
	}

	int		userId;
	char	szUserName[20];
	char	szDeskPass[10];	// 房间号
	char	szRoomName[32];
	int		deskIndex;
	int	winScore;		// 赢分
	float	winRedFlowers;	// 赢得的红花数（负值为输掉的）
	BYTE	isBigWinner;	// 是否是大赢家(1=是 0=否)
	UINT	date;			// 日期时间戳（同time()所返回的时间戳，单位为秒）
	UINT	gameSN;			// 回放码
};

// 俱乐部茶楼 - 查询用户战绩 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_QueryUserGameLog : public SClubResponseBase
{
	int					zhanji;			// 总战绩（当日赢取的红花数总和）
	int					roomCount;		// 房间数（当日参与的房间数)
	int					bigWinnerCount;	// 大赢家次数

	int					count;
	SClubUserGameLog	logList[0];

	SClubOperationResponse_QueryUserGameLog()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryUserGameLog));
	}
};

// 俱乐部茶楼 - 添加/移除黑名单 - 返回给客户端的结构
struct SClubOperationResponse_SetBlackListUser : public SClubResponseBase
{
	int		userId;
	bool	bAddToBlackList;	// 是否已添加到黑名单（黑名单标志：ClubAccess_BlackList）

	SClubOperationResponse_SetBlackListUser()
	{
		memset(this, 0, sizeof(SClubOperationResponse_SetBlackListUser));
	}
};

// 俱乐部茶楼 - 设置同桌限制 - 返回给客户端的结构
struct SClubOperationResponse_SetDeskmateLimit : public SClubResponseBase
{
	int		userId;
	int		userId2;
	bool		bSet;		//true设置，false移除
	bool		bOK;		// 是否设置成功

	SClubOperationResponse_SetDeskmateLimit()
	{
		memset(this, 0, sizeof(SClubOperationResponse_SetDeskmateLimit));
	}
};

//同桌限制
struct SClubDeskmateLimit
{
	int		userIds[2];
	char	szUserNames[2][20];
};

// 俱乐部茶楼 - 查询同桌限制 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_QueryDeskmateLimit : public SClubResponseBase
{
	int					count;
	SClubDeskmateLimit	deskmateList[0];

	SClubOperationResponse_QueryDeskmateLimit()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryDeskmateLimit));
	}
};

// 俱乐部茶楼 - 添加/移除合伙人 - 返回给客户端的结构
struct SClubOperationResponse_SetPartner : public SClubResponseBase
{
	int		userId;
	bool	bSetPartner;	// 是否添加为合伙人（合伙标志：ClubAccess_Partner）

	SClubOperationResponse_SetPartner()
	{
		memset(this, 0, sizeof(SClubOperationResponse_SetPartner));
	}
};

struct SClubSearchMemberInfo
{
	int		userId;
	char	szNickName[20];
	UINT	clubAccess;		// 俱乐部权限
	int		partnerUserId;	// 所属的合伙人用户id
	bool	bInClub;		// 是否已经在俱乐部中
};

// 俱乐部茶楼 - 搜索成员 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_SearchMember : public SClubResponseBase
{
	int						count;
	SClubSearchMemberInfo	members[0];

	SClubOperationResponse_SearchMember()
	{
		memset(this, 0, sizeof(SClubOperationResponse_SearchMember));
	}
};

// 俱乐部茶楼 - 加入/移除合作群 - 返回给客户端的结构
struct SClubOperationResponse_SetMemberOfPartner : public SClubResponseBase
{
	int		partnerUserId;	// 合伙人用户id
	int		inviteUserId;	// 邀请的成员用户id

	SClubOperationResponse_SetMemberOfPartner()
	{
		memset(this, 0, sizeof(SClubOperationResponse_SetMemberOfPartner));
	}
};

// 合伙人的统计信息
struct SClubPartnerFilterInfo
{
	int		partnerUserId;
	UINT		clubAccess;
	char		szNickName[20];
	int		totalRoomCount;			// 总房间数
	int		totalUserCount;			// 总人数
	int		totalBigWinnerCount;		// 总大赢家数
	int		memberCount;			// 成员数
	int		giveCount;				// 赠送数
	float		redFlowersRate;			// 红花比例
};

// 俱乐部茶楼 - 查询合伙人列表 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_QueryPartnerList : public SClubResponseBase
{
	int						count;
	SClubPartnerFilterInfo	infos[0];

	SClubOperationResponse_QueryPartnerList()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryPartnerList));
	}
};

// 俱乐部茶楼 - 查询在线用户数量 - 返回给客户端的结构
struct SClubOperationResponse_QueryOnlineUserCount : public SClubResponseBase
{
	int	count;

	SClubOperationResponse_QueryOnlineUserCount()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryOnlineUserCount));
	}
};

// 俱乐部茶楼 - 修改楼层名字 - 返回给客户端的结构
struct SClubOperationResponse_ModifyTeahouseName : public SClubResponseBase
{
	int		teahouseId;
	char	szName[32];

	SClubOperationResponse_ModifyTeahouseName()
	{
		memset(this, 0, sizeof(SClubOperationResponse_ModifyTeahouseName));
	}
};

// 俱乐部茶楼 - 查询合伙人信息 - 返回给客户端的结构
struct SClubOperationResponse_QueryPartnerInfo : public SClubResponseBase
{
	float	redFlowersRate;
	float	rebate_today;	// 今日总返利
	UINT	clubAccess;		// 合伙人权限

	SClubOperationResponse_QueryPartnerInfo()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryPartnerInfo));
	}
};

// 俱乐部茶楼 - 设置是否允许管理员向合伙人的下线成员送花/摘花 - 返回给客户端的结构
struct SClubOperationResponse_AllowAdminTakeRedFlower : public SClubResponseBase
{
	bool	bAllow;

	SClubOperationResponse_AllowAdminTakeRedFlower()
	{
		memset(this, 0, sizeof(SClubOperationResponse_AllowAdminTakeRedFlower));
	}
};

// 俱乐部茶楼 - 修改成员备注 - 返回给客户端的结构
struct SClubOperationResponse_ModifyMemberNote : public SClubResponseBase
{
	int		targetUserId;
	char	szNote[20];

	SClubOperationResponse_ModifyMemberNote()
	{
		memset(this, 0, sizeof(SClubOperationResponse_ModifyMemberNote));
	}
};

// 俱乐部群房间日志
struct SClubQunRoomLog
{
	SClubQunRoomLog()
	{
		memset(this, 0, sizeof(SClubQunRoomLog));
	}

	enum
	{
		_MaxRoomLogRounds = 20,
	};

	char		szRoomName[32];
	char		szDeskPass[10];	// 房间号
	UINT		date;			// 日期时间戳（同time()所返回的时间戳，单位为秒）
	int		winScore;		// 分数
	char		szUserNames[ClubTeahouseDeskMaxUsers][20];
	BYTE		roundCount;		// 游戏局数
	int		userScore[ClubTeahouseDeskMaxUsers][_MaxRoomLogRounds];
	float		redflowersList[ClubTeahouseDeskMaxUsers][_MaxRoomLogRounds];
	char		szGameSN[_MaxRoomLogRounds][10];	// 每一局的回放码
};

// 俱乐部茶楼 - 查询本群战绩 - 返回给客户端的结构（变长结构）
struct SClubOperationResponse_QueryQunGameLog : public SClubResponseBase
{
	int				roomCount;			// 开房总数
	int				todayRoomCount;		// 今日开房数
	int				yesterdayRoomCount;	// 昨日开房数
	int				weekdayRoomCount;	// 7日开房数
	int				monthRoomCount;		// 30日开房数
	int				bigWinnerCount;		// 大赢家次数

	int				count;
	SClubQunRoomLog	logList[0];

	SClubOperationResponse_QueryQunGameLog()
	{
		memset(this, 0, sizeof(SClubOperationResponse_QueryQunGameLog));
	}
};

///////////////////////////////////////////////////////////////////////////

// 创建俱乐部
struct MSG_GP_I_CreateClub
{
	char	szClubName[32];

	MSG_GP_I_CreateClub()
	{
		memset(this, 0, sizeof(MSG_GP_I_CreateClub));
	};

};
//
struct MSG_GP_O_CreateClub
{
	int		iClubID;
	char	szClubName[32];
};

//消息
struct DL_O_HALL_CreateClub
{
	//DataBaseResultLine ResultHead;

	MSG_GP_O_CreateClub _data;

	DL_O_HALL_CreateClub()
	{
		memset(this, 0, sizeof(DL_O_HALL_CreateClub));
	}
};

// 申请加入俱乐部
struct MSG_GP_I_JoinClub
{
	int		iClubID;

	MSG_GP_I_JoinClub()
	{
		memset(this, 0, sizeof(MSG_GP_I_JoinClub));
	}

};

//俱乐部成员数据
struct MSG_GP_O_Club_UserList_Data
{
	int		iUserID;	
	int		iLogoID;
	bool	bSex;
	char	szUserNickName[30];
	//char	szHeadURL[256];
	int     iOnlineFlag;
	UINT		accessFlags;		// 参见类型：EClubAccess
	float		redflowers;			// 红花数
};

// 申请加入俱乐部通知创建者
struct MSG_GP_O_Club_UserJoin
{
	bool							bJoin;					//true-加入，false-退出
	int							iClub;
	MSG_GP_O_Club_UserList_Data		_data;
};

// 退出/加入俱乐部
struct MSG_GP_O_Club_UserChange
{
	bool							bJoin;					//true-加入，false-退出
	int							iClub;
	MSG_GP_O_Club_UserList_Data		_data;
};


//俱乐部玩家列表头部消息
struct MSG_GP_O_Club_UserList_Head
{
	int		iClubID;
	int		iUserNum;
	int		iCreaterID;
};


//俱乐部列表
struct MSG_GP_O_Club_List
{
	bool		bCreater;
	bool		bSex;
	int		iClubID;
	int		iClubUserNum;
	int		iRoomNum;
	int		iLogoID;
	char		szClubName[32];
	char		szHeadURL[256];
	char		szClubOwnerName[32];	// 俱乐部群主名字
	int		readyDeskCount;			// 等待中的桌子数量
	int		playingDeskCount;		// 游戏中的桌子数量
};

// 会长审核列表
struct MSG_GP_I_Club_ReviewList
{
	int		iClubID;
	MSG_GP_I_Club_ReviewList()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_ReviewList));
	}

};
//申请加入俱乐部消息列表头部
struct MSG_GP_O_Club_ReviewList_Head
{
	int		iClubID;
	int		iUserNum;
};
//申请加入俱乐部消息列表内容
struct MSG_GP_O_Club_ReviewList_Data
{
	BYTE		bUserType;				//0-申请名单，1-屏蔽名单
	int		iUserID;
	int		iLogoID;
	bool		bSex;
	char		szUserNickName[30];
	char		szHeadURL[256];
};

//会长对申请消息列表审核操作
struct MSG_GP_I_Club_MasterOpt
{
	int		iClubID;			//俱乐部ID
	int		iTargetID;			//操作对象ID,
	BYTE		bOptType;			//0-同意，1-拒绝，2-移入黑名单，3-移出黑名单

	MSG_GP_I_Club_MasterOpt()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_MasterOpt));
	}
};

// 俱乐部创建房间
struct MSG_GP_I_Club_BuyDesk
{
	int		iUserID;						// 用户ID
	int		iClubID;						// 俱乐部ID
	int		iPlayeCount;					// 购买局数
	int		iGameID;						// 游戏ID
	BYTE		bIsNeedPassword;				// 俱乐部开设房间是否需要密码 0 不要 1 要
	BYTE		bFinishCondition;					// 结束条件，0局数，1胡息
	BYTE		bPayType;						// 0开局之后扣除房费，1房主开房时付费，2AA制扣除房费
	BYTE		bPositionLimit;					// 0不开启定位，1开启定位
	BYTE		bPlayerNum;					// 游戏限制人数,0与游戏人数相同
	BYTE		bMidEnter;						// 0中途不可以进入，1中途可进入
	BYTE		szDeskConfig[512];

	MSG_GP_I_Club_BuyDesk()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_BuyDesk));
	}
};
//创建俱乐部房间的信息
struct MSG_GP_O_Club_RoomList_Data   //
{
	BYTE		bPayType;					//付费方式
	int			iCount;						//创建局数
	//int			iLogoID;	
	int			isPlay;						//房间是否已开始
	int			iRoomID;
	int     iDeskID;
	BYTE		bIsNeedPassword;			// 俱乐部开设房间是否需要密码 0 不要 1 要
	//bool		bSex;						//性别
	char		szDeskPass[20];				//桌子密码
	char		szGameName[30];				//游戏名称
	//char		szUserNickName[30];			//房主昵称
	//char		szHeadURL[256];				//房主头像
	//BYTE		szDeskConfig[512];			//各个游戏房间配置
};

//创建晋江俱乐部房间的信息
enum
{
	_MaxClubRoomUsers = 10,
};
struct MSG_GP_O_JJClub_RoomList_Data
{
	BYTE		bPayType;					//付费方式
	int			iCount;						//创建局数
	//int			iLogoID;
	int			isPlay;						//房间是否已开始(0 等待中 1 游戏中)
	int			iRoomID;						//房号
	int     iDeskID;
	BYTE		bIsNeedPassword;			//俱乐部开设房间是否需要密码 0 不要 1 要
	//bool		bSex;						//性别
	char		szDeskPass[20];				//桌子密码
	char		szGameName[30];				//游戏名称
	//char		szUserNickName[30];			//房主昵称
	//char		szHeadURL[256];				//房主头像
	BYTE		szDeskConfig[20];			//各个游戏房间配置
	int			iPlayCount;					//房间人数限制
	int		userIdList[_MaxClubRoomUsers];	// 房间里的每个用户id <Add By Bluce_Lickey 2020-2-22>
};




//创建房间结果
struct MSG_GP_O_Club_BuyDesk
{
	int									iUserID;						// 用户ID
	int									iClubID;						// 俱乐部ID
	BYTE								bIsNeedPassword;				// 俱乐部开设房间是否需要密码 0 不要 1 要
	MSG_GP_O_Club_RoomList_Data			_RoomData;
};

// 俱乐部创建/解散房间
struct MSG_GP_O_Club_RoomChange
{
	bool							bCreate;						// true-创建，false-解散
	int								iClub;
	MSG_GP_O_Club_RoomList_Data		_data;
};

// 俱乐部房间列表		 
struct MSG_GP_I_Club_RoomList
{
	int		iClubID;
	
	MSG_GP_I_Club_RoomList()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_RoomList));
	}
};

//获取俱乐部房间列表结果
struct MSG_GP_O_Club_RoomList_Head
{
	int		iClubID;
	int		iRoomNum;
};

// 进入俱乐部（开始接受消息）
struct MSG_GP_I_Club_EnterClub
{
	int		iClubID;

	MSG_GP_I_Club_EnterClub()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_EnterClub));
	}
};

//接收的消息内容
struct MSG_GP_O_Club_EnterClub
{
	int		iClubID;
	int		iMasterID;
	int		iClubJewels;
	char	szClubNotice[100];
};

// 俱乐部更名
struct MSG_GP_I_ChangeName
{
	int		iClubID;
	char		szNewClubName[32];
	MSG_GP_I_ChangeName()
	{
		memset(this, 0, sizeof(MSG_GP_I_ChangeName));
	}
};
//更名结果
struct MSG_GP_O_ChangeName
{
	int		iClubID;
	char		szNewClubName[32];
};


// 俱乐部玩家列表
struct MSG_GP_I_Club_UserList
{
	int		iClubID;
	MSG_GP_I_Club_UserList()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_UserList));
	}
};

// 申请退出俱乐部
struct MSG_GP_I_LeaveClub
{
	int		iClubID;
	MSG_GP_I_LeaveClub()
	{
		memset(this, 0, sizeof(MSG_GP_I_LeaveClub));
	}
};
// 申请退出俱乐部消息回复
struct MSG_GP_O_LeaveClub
{
	int		iClubID;
};

// 会长踢人
struct MSG_GP_I_Club_KickUser
{
	int		iClubID;
	int		iTargetID;
	MSG_GP_I_Club_KickUser()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_KickUser));
	}
};
// 会长踢人返回结果
struct MSG_GP_O_Club_KickUser
{
	int		iClubID;
	int		iTargetID;
};

// 俱乐部公告
struct MSG_GP_I_Club_Notice
{
	int		iClubID;
	char	szClubNotice[100];
	MSG_GP_I_Club_Notice()
	{
		memset(this, 0, sizeof(MSG_GP_I_Club_Notice));
	}
};

//俱乐部公告回复消息
struct MSG_GP_O_Club_Notice
{
	int		iClubID;
	char	szClubNotice[100];
};

// 添加俱乐部钻石
struct MSG_GP_I_SetClubJewels
{
	int		iClubID;
	int		iJewels;
	MSG_GP_I_SetClubJewels()
	{
		memset(this, 0, sizeof(MSG_GP_I_SetClubJewels));
	}
};
//添加俱乐部钻石回调
struct MSG_GP_O_ClubJewels
{
	int		iJewels;
};

// 解散俱乐部
struct MSG_GP_I_DissmissClub
{
	int		iClubID;
	MSG_GP_I_DissmissClub()
	{
		memset(this, 0, sizeof(MSG_GP_I_DissmissClub));
	}
};

// 解散俱乐部消息回复
struct MSG_GP_O_DissmissClub
{
	int		iClubID;
};

// 获取俱乐部钻石
struct MSG_GP_I_GetClubJewels
{
	int		iClubID;
	MSG_GP_I_GetClubJewels()
	{
		memset(this, 0, sizeof(MSG_GP_I_GetClubJewels));
	}
};
//俱乐部钻石
struct MSG_GP_O_ClubGetJewels
{
	int		iJewels;
};

// 返回获取俱乐部创建权限的查询结果
struct MSG_GP_O_Club_GetCreateAccess
{
	int userId;
	int clubAccess; // 1=有权限 0=无权限
};

// 发送俱乐部获取创建茶楼权限
struct MSG_GP_I_Club_GetCreateTeahouseAccess
{
	int clubId;
};

// 返回创建茶楼楼层权限
struct MSG_GP_O_Club_GetCreateTeahouseAccess
{
	int		userId;
	int		clubId;
	int		clubAccess;	// 1=有权限 0=无权限
};

enum _ETakeType
{
	_TakeType_None = 0,
	_TakeType_AA = 1,	// AA摘花
	_TakeType_Step = 2,	// 阶梯摘花
};

enum
{
	_MaxStepCount = 5,
};

// 俱乐部茶楼选项
struct SClubTeahouseOption
{
	bool		m_bOpenRedFlowersMode;				// 是否开启红花结算
	_ETakeType	m_Type;
	float		m_ScoreToRedFlowers;				// 1分等于多少朵红花
	int			m_MinRedFlowersOnJoinDesk;			// 进房最低红花数
	int			m_AATakeCount;						// AA摘花数量
	int			m_TakeScoreRanges[_MaxStepCount];	// 阶梯摘花分数范围表
	int			m_TakeNumRanges[_MaxStepCount];		// 阶梯摘花数量范围表（0为空）
};

// 发送俱乐部创建茶楼
struct MSG_GP_I_Club_CreateTeahouse
{
	int	iClubID;          // 俱乐部ID
	int	teahouseId;       // 茶楼id（创建填0，修改填茶楼id）
	char	szName[32];    // 茶楼名称
	int	gameNameId;	    // GameNameID
	int	newGameNameId;			// 要修改的新的GameNameID
	int	iPlayeCount;     // 购买局数
	int	maxDeskCount;   // 最大桌子数
	BYTE		bPlayerNum;  // 游戏限制人数,0与游戏人数相同
	BYTE		szDeskConfig[512];
	SClubTeahouseOption	option;					// 茶楼红花选项
};

//返回茶楼楼层创建结果
// 俱乐部创建茶楼 - 返回给客户端的结构体
struct MSG_GP_O_Club_CreateTeahouse
{
	int		userId;
	int		clubId;
	int		teahouseId;
	BYTE		isOK; // 1=创建成功 0=创建失败
	char		szErrorInfo[64]; // 创建失败时的描述
};

// 俱乐部茶楼信息
struct SClubTeahouseInfo
{
	int		teahouseId;
	char		szName[32];
	int		roomId;
	int		gameNameId;
	int		maxPlayerCount;	// 最大人数
	int		maxGames;		// 最大局数
	int		maxDeskCount;	// 最大桌子数（查看最大桌子数与当前桌子数不相同的情况下请求ASS_GP_CLUB_ReplenishTeahouseDesk对应结构体MSG_GP_I_Club_ReplenishTeahouseDesk）
	BYTE		szDeskConfig[20];
	SClubTeahouseOption	option;					// 茶楼红花选项
};

// 俱乐部茶楼桌子信息
struct SClubTeahouseDeskInfo
{
	int		teahouseId;			// 所属的俱乐部茶楼ID
	int		deskIndex;			// 桌子索引
	BYTE		deskState;			// 桌子状态
	BYTE		curGames;			// 当前局数
	int		userIDList[ClubTeahouseDeskMaxUsers];		// 所有加入桌子的用户ID
};

// 初始化俱乐部所有茶楼数据 - 返回给客户端的结构体（可变长度，仅用于参考）
struct MSG_GP_O_Club_InitAllTeaHouseData
{
	int						clubId;
	UINT						clubFlags;
	//int						teahouseCount;
	//SClubTeahouseInfo		pTeahouseInfo[teahouseCount];
	//int						deskCount;
	//SClubTeahouseDeskInfo	pDeskInfo[deskCount];
	//int						roomCount;
	//	ComRoomInfo				pRoomInfos[roomCount];
};

//更新茶楼信息
enum EClubTeahouseDeskState
{
	ClubTeahouseDeskState_None = 0,	// 未使用

	ClubTeahouseDeskState_Free = 1,	// 空闲
	ClubTeahouseDeskState_Waiting = 2,	// 等待中
	ClubTeahouseDeskState_Playing = 3,	// 游戏中
};

enum
{
	ClubTeahouseDeskUpdate_None = 0,
	ClubTeahouseDeskUpdate_State = 1,	// 更新桌子状态	（更新数值 = 桌子状态，即：EClubTeahouseDeskState枚举类型）
	ClubTeahouseDeskUpdate_CurGames = 2,	// 更新当前局数	（更新数值 = 当前局数）
	ClubTeahouseDeskUpdate_PlayerEnter = 3,	// 有玩家进入	（更新数值 = 用户ID）
	ClubTeahouseDeskUpdate_PlayerLeave = 4,	// 有玩家离开	（更新数值 = 用户ID）
	ClubTeahouseDeskUpdate_ClearDesk  = 5,	// 清除桌子
	ClubTeahouseDeskUpdate_DeleteDesk = 6,	// 删除桌子
	ClubTeahouseDeskUpdate_DeleteTeahouse = 7,	//删除楼层	(更新数值 = 1==显示解散提示框 0==不显示解散提示框)
	ClubTeahouseDeskUpdate_UpdateFlags = 8,		//更新所有茶楼是否是暂停的状态（更新数值：俱乐部标记）
};

// 通知客户端更新俱乐部茶楼桌子信息
struct MSG_GP_O_Club_UpdateTeahouseDeskInfo
{
	int		clubId;
	int		teahouseId;
	WORD	deskIndex;
	BYTE		updateList[2];		// 如：ClubTeahouseDeskUpdate_State
	int		updateValues[2];	// 具体更新的数值
};

// 离开俱乐部大厅
struct MSG_GP_I_Club_LeaveHall
{
	int		clubId;
};

struct MSG_GP_O_Club_OnTeaHouseCreate
{
	int						clubId;
	SClubTeahouseInfo		pTeahouseInfo;
	int						deskCount;
	SClubTeahouseDeskInfo		pDeskInfo[0];
};

// 请求刷新俱乐部茶楼桌子（以自动续桌）
struct MSG_GP_I_Club_ReplenishTeahouseDesk
{
	int		clubId;
	int		teahouseId;
	int		gameNameId;
};

// 请求刷新俱乐部茶楼桌子 - 返回给客户端的结构（变长结构）
struct MSG_GP_O_Club_ReplenishTeahouseDesk
{
	int		clubId;
	int		teahouseId;
	BYTE		deskCount;	// 增加的桌子数量
	int		deskList[0];	// 增加的每个桌子索引
};

// 俱乐部删除茶楼
struct MSG_GP_I_Club_DeleteTeahouse
{
	int		clubId;		// 俱乐部id
	int		teahouseId;	// 茶楼id
	int		gameNameId;	// GameNameID
};

/////////////////////////////////////////////VIP房间创建，进入/////////////////////////////////
//VIP桌子
#define MDM_GP_DESK_VIP					    141
#define ASS_GP_BUY_DESK						0				//购买桌子
#define ASS_GP_ENTER_DESK					1				//进入vip桌子
#define ASS_GP_GETTOTALRECORD				3				//获取总战绩
#define ASS_GP_GETSINGLERECORD				4				//获取单局战绩
#define ASS_GP_GET_CUTROOM					5				//获取断线重连房间信息
#define ASS_GP_GET_DESKCONFIG				6				//获取创建桌子配置信息
#define ASS_GP_GET_RECORDURL				7				//获取回放文件地址
#define ASS_GP_GET_DESKLIST					8				//获取已创建桌子列表
#define ASS_GP_GET_DESKUSER					9				//获取已进入桌子玩家列表
#define ASS_GP_DISMISS_DESK					10				//通过桌子号解散桌子
#define ASS_GP_DELETE_RECORD				11				//删除房间管理记录


//发送时钻石字段填-1表示查询，结果表示6或7。
struct MSG_GP_S_BUY_DESK//购买桌子
{
	int iUserID;				// 用户ID
	int iPlayCount;				// 购买局数/总分数
	int iGameID;				// 游戏id
	BYTE bFinishCondition;		// 结算条件（0:局数，1:总分数，2:圈数，3:时效）
	BYTE bPayType;				// 0:房主开局后付费，1:房主开房时付费， 2:AA制扣费 
	BYTE bPositionLimit;		// 0:不开启定位，1:开启定位
	BYTE bPlayerNum;			// 游戏限制人数（0:与游戏人数相同，其它人数填写对应值）
	BYTE bMidEnter;             // 0:中途不可以进入, 1中途可以进入
	int iBuyMinute;             // 购买时长
	char szDeskConfig[512];		// 开房玩法设置(与游戏服务端协商定义结构体，然后转成字符串)

	MSG_GP_S_BUY_DESK()
	{
		memset(this, 0, sizeof(MSG_GP_S_BUY_DESK));
	}
};

//HandleCode:(1:购买成功;2:用户不存在;3:钱包金币不足;4:购买金币不合法;5:桌子已购买完;6:已购买过;7:未购买)
struct MSG_GP_S_BUY_DESK_RES//购买桌子结果
{
	int iUserID;		//用户ID
	int iRoomID;		//房间号
	int iDeskID;		//桌子号
	int iJewels;		//消耗钻石
	int iGameNameID;	//游戏ID
	char szPassWord[20];//桌子密码
	//char szGameWord[20];//回放码
	MSG_GP_S_BUY_DESK_RES()
	{
		memset(this, 0, sizeof(MSG_GP_S_BUY_DESK_RES));
	}
};
struct MSG_GP_S_ENTER_VIPDESK//进入VIP桌子
{
	int iUserID;//用户ID
	char szInputPassWord[20];//桌子密码
	MSG_GP_S_ENTER_VIPDESK()
	{
		memset(this, 0, sizeof(MSG_GP_S_ENTER_VIPDESK));
	}
};

//HandleCode:0:没有这个桌子;1：进入桌子成功;2：用户不存在;3:重新分配桌子;4：无空闲桌子
struct MSG_GP_S_ENTER_VIPDESK_RES//进入VIP桌子结果
{
	int iUserID;				//用户ID
	int iRoomID;				//房间号
	int iDeskID;				//桌子号
	BYTE bType;					//0：普通房间，1：比赛场，2：VIP房间
	BYTE bPositionLimit;		//0:不开启定位，1:开启定位
	char szPass[20];			//桌子密码
	ComRoomInfo tCutNetRoomInfo;	//断线房间信息

	MSG_GP_S_ENTER_VIPDESK_RES()
	{
		memset(this, 0, sizeof(MSG_GP_S_ENTER_VIPDESK_RES));
	}
};

//请求断线重连房间信息
struct MSG_GP_S_GET_CUTNETROONINFO
{
	int iUserID;
	BYTE iType;		//0：获取所有房间断线信息，1：获取比赛场断线信息

	MSG_GP_S_GET_CUTNETROONINFO()
	{
		memset(this, 0, sizeof(MSG_GP_S_GET_CUTNETROONINFO));
	}
};

//HandleCode: 0未找到断线信息，1，2可购买房间获取信息成功，3可购买房间获取信息失败，4普通房间获取信息成功

//断线房间信息
struct  MSG_GP_S_GET_CUTNETROOMINFO_RES
{
	int iUserID;				//用户ID
	int iRoomID;				//房间号
	int iDeskID;				//桌子号
	BYTE bType;					//0：普通房间，1：比赛场，2：VIP房间
	BYTE bPositionLimit;		// 0:不开启定位，1:开启定位
	char szPass[20];			//桌子密码
	ComRoomInfo tCutNetRoomInfo;	//断线房间信息

	MSG_GP_S_GET_CUTNETROOMINFO_RES()
	{
		memset(this, 0, sizeof(MSG_GP_S_GET_CUTNETROOMINFO_RES));
	}
};

//请求创建桌子配置信息
struct MSG_GP_I_BuyDeskConfig
{
	int		iGameID;		//游戏id

	MSG_GP_I_BuyDeskConfig()
	{
		memset(this, 0, sizeof(MSG_GP_I_BuyDeskConfig));
	}
};

////请求创建桌子配置信息结果
//struct MSG_GP_O_BuyDeskConfig
//{
//	int		iGameID;			//游戏id
//	int		iBuyCount[5];		//购买房间局数选项
//	int		iJewels[5];			//购买局数对应的价格
//	int		iAAJewels[5];		//AA制房卡价格
//	BYTE	bInvoice;			//是否允许代开（0：无，1：有）
//	BYTE	bFinishCondition;	//结算条件（0:局数，1:总分数，2:圈数）
//};

//购买桌子之前的配置
struct MSG_GP_DeskBuyCountItem
{
	int iBuyCount;
	int iJewels;
	int iAAJewels;
};

struct MSG_GP_O_BuyDeskConfig
{
	int iGameID;
	MSG_GP_DeskBuyCountItem buyCounts[5];
	BYTE szGameDynamicInfo1[128];  //对应游戏支持的玩法（具体玩法与游戏对接）
	BYTE bInvoice;//是否有代开功能， 0-无，1-有
	BYTE bMinute; //多少分钟一个钻石
	BYTE bFinishCondition;	//结算条件（0:局数，1:总分数，2:圈数,3:时效）
	MSG_GP_O_BuyDeskConfig()
	{
		memset(this, 0, sizeof(MSG_GP_O_BuyDeskConfig));
	}
};

///////////////////////////////////////////////////////////////////////////////////////////
//房间管理相关
///////////////////////////////////////////////////////////////////////////////////////////
//获取已创建房间列表信息
struct MSG_GP_I_GetBuyDeskList
{
	int		iUserID;		//玩家id
	BYTE	bType;			//获取类型（0：开启房间，1：关闭房间）
};

//获取已创建房间列表信息结果
struct MSG_GP_O_GetBuyDeskList
{
	BYTE	bType;			//获取类型（0：开启房间，1：关闭房间）
	int		iUserID;		//玩家id
	int		iGameStart;		//游戏是否开始（是否已经扣除钻石）
	LLONG	i64BuyTime;		//创建时间
	char	szDeskPass[20];	//房间号
	char	szGameName[32];	//游戏名称
};

//获取已加入房间内玩家信息
struct MSG_GP_I_GetDeskUser
{
	char	szDeskPass[20];	//房间号

	MSG_GP_I_GetDeskUser()
	{
		memset(this, 0, sizeof(MSG_GP_I_GetDeskUser));
	}
};

//获取已加入房间内玩家信息结果
struct MSG_GP_O_GetDeskUser
{
	BYTE	bBoy;				//玩家性别
	int		iLogoID;			//头像ID
	int		iUserID;			//玩家ID
	char	szNickName[50];		//玩家昵称
	char	szUserHeadUrl[256];	//头像地址
};

////////////////////////////////////////////////////////////////////////////
enum E_HALL_ERROR_DISMISSDESK
{
	ERR_GP_DISMISSDESK_SUCCESS = 0,	// 解散成功
	ERR_GP_NOT_FIND_USER_CLR,		// 未找到此用户
	ERR_GP_NOT_DESK_MASTER,			// 不是当前房间房主
	ERR_GP_DESKPASSS_ERROR,			// 房号错误
	ERR_GP_DESK_INGAME				// 游戏已经开始
};

//通过房间号解散对应房间
struct MSG_GP_I_DismissDesk
{
	char	szDeskPass[20];	//房间号

	MSG_GP_I_DismissDesk()
	{
		memset(this, 0, sizeof(MSG_GP_I_DismissDesk));
	}
};


enum E_HALL_ERROR_DELETE_RECORD
{
	ERR_GP_DELETE_RECORD_SUCCESS = 0,	// 删除成功
	ERR_GP_NOT_FIND_USER_DEL,			// 未找到此用户
	ERR_GP_NOT_DESK_MASTER_DEL,			// 不是当前房间房主
	ERR_GP_DESKPASSS_ERROR_DEL			// 房号错误
};

//通过房间号删除对应房间记录
struct MSG_GP_I_DeleteRecord
{
	char	szDeskPass[20];	//房间号

	MSG_GP_I_DeleteRecord()
	{
		memset(this, 0, sizeof(MSG_GP_I_DeleteRecord));
	}
};

///////////////////////////////////////////////////////////////////////////////////////////
//战绩回放相关
///////////////////////////////////////////////////////////////////////////////////////////

//请求总战绩
struct MSG_GP_I_GetTotalRecord
{
	BYTE	iType;			//0：vip战绩，1：普通战绩
	int		iUserID;		//用户ID
	int		iClubID;		//俱乐部ID，0为普通战绩
	int		iStartCount;	//开始请求条数

	MSG_GP_I_GetTotalRecord()
	{
		memset(this, 0, sizeof(MSG_GP_I_GetTotalRecord));
	}
};

//请求总战绩结果（每次返回一条战绩，handleCode判断是否发完）
struct MSG_GP_O_GetTotalRecord
{
	BYTE	iType;				//0：vip战绩，1：普通战绩
	int		iRecordID;			//战绩ID	
	int		iClubID;			//俱乐部ID，0为普通战绩
	int		iPlayerCount;		//玩家人数
	int		iTotalCount;		//战绩总条数
	int		iScore[10];			//玩家成绩
	LLONG	iPlayTime;			//时间
	char	szGameName[32];		//游戏名称
	char	szPassWord[10];		//桌子密码（房号）
	char	szNickName[10][50];	//游戏玩家昵称

	MSG_GP_O_GetTotalRecord()
	{
		memset(this, 0, sizeof(MSG_GP_O_GetTotalRecord));
	}
};

//请求单局战绩
struct MSG_GP_I_GetSingleRecord
{
	int		iRecordID;		//战绩ID	
	int		iUserID;		//用户ID
	int		iStartCount;	//开始请求条数

	MSG_GP_I_GetSingleRecord()
	{
		memset(this, 0, sizeof(MSG_GP_I_GetSingleRecord));
	}
};

//请求单局战绩结果（每次返回一条战绩，handleCode判断是否发完）
struct MSG_GP_O_GetSingleRecord
{
	bool	bPlayBack;			//是否开启回放
	int		iPlayerCount;		//玩家人数
	int		iTotalCount;		//战绩总条数
	int		iGameID;			//游戏ID
	int		iScore[10];			//玩家成绩
	LLONG	iPlayTime;			//时间
	int     szUserID[10];
	char	szRecordCode[10][10];	//回放码
	char	szPassWord[10];		//桌子密码（房号）
	char	szNickName[10][50];	//游戏玩家昵称

	MSG_GP_O_GetSingleRecord()
	{
		memset(this, 0, sizeof(MSG_GP_O_GetSingleRecord));
	}
};

//请求回放文件下载地址
struct MSG_GP_I_GetRecordUrl
{
	char	szRecordCode[10];		//回放码

	MSG_GP_I_GetRecordUrl()
	{
		memset(this, 0, sizeof(MSG_GP_I_GetRecordUrl));
	}
};

//回放文件下载地址
struct MSG_GP_O_GetRecordUrl
{
	char	szRecordCode[10];		//回放码
	int		iGameNameID;			//回放游戏ID
};


///////////////////////////////////////////////////////////////////////////////////////////
//获取大厅功能开关信息
///////////////////////////////////////////////////////////////////////////////////////////
#define MDM_GP_GET_CONFIG					    142
#define ASS_GP_GET_CONFIG						0		//获取大厅功能开关信息

struct TFuncConfig
{
	char name[15];
	bool bShowAndroid;
	bool bShowIos;
};

struct MSG_GP_S_GET_CONFIG_RES
{
	int iCount;	//配置的功能个数
	TFuncConfig _data[20];

	MSG_GP_S_GET_CONFIG_RES()
	{
		memset(this, 0, sizeof(MSG_GP_S_GET_CONFIG_RES));
	}
};

/********************************************************************************************/
//比赛消息
/********************************************************************************************/

#define MDM_GP_CONTEST					51				// 大厅比赛信息主id
#define ASS_GP_GET_CONTEST_ROOMID		0				// 获取比赛房间信息
#define ASS_GP_GET_APPLY_NUM			4				// 获取比赛已报名人数
#define ASS_GP_CONTEST_APPLY			5				// 大厅报名/退赛
#define ASS_GP_CONTEST_NOTICE			6				// 赛前通知
#define ASS_GP_CONTEST_AWARDINFO		7				// 比赛奖励
#define ASS_GP_CONTEST_HAVEPLAYNUM		8				// 获取剩余免费次数
#define ASS_GP_CONTEST_GET_CONTESTAWARDLIST 9			// 请求获奖记录

//比赛列表信息
struct  MSG_GP_ContestApplyInfo
{
	BYTE			iContestType;						//比赛类型(0：人满赛，1：定时赛，2：循环赛)
	BYTE			iAwardType;							//奖励类型（0：金币，1：红包，2：房卡）
	BYTE			iEnterType;							//报名费类型（0：金币，1：钻石/房卡）
	int				iGameID;							//游戏ID
	int				iContestID;							//比赛ID
	int				iContestKind;						//比赛分类
	int				iContestAward;						//第一名奖励数量
	int				iOnlinePeople;						//此项比赛总人数
	int				iNeedPeople;						//此项比赛开赛人数
	int				iEnterFee;							//报名所需费用
	LLONG			beginTime;							//比赛开始时间
	LLONG			ContestBeginTime;					//报名开始时间
	LLONG			ContestEndTime;						//报名结束时间
	int			ContestBeginRemainSecTime;					// 距离比赛开始剩余的秒时间
	char			szRoomName[50];						//比赛名称
	bool			isInvalid;							//赛场是否失效（定时赛过期，满人赛房间不足）
	BYTE			contestState;							//0：比赛未开始，1：比赛进行中，2：比赛已结束
	int           iCanPlayNum;						//免费次数（如果免费次数-已使用免费次数=0且报名消费为0则免费场不能再报名了）
	char			szResume[64];						// 赛事玩法
	char			szDesc[128];						// 赛制描述
	int           iHavePlayNum;						//已使用的免费次数
	//int           iIsCanToApply;						//0可报，1不可报
	BYTE			isApply;							//0：未报名，1：已报名，2：正在比赛中
};

// 报名或者退赛请求,大厅报名和房间报名共用结构体
//（注意，报名分为大厅报名和房间报名，满人赛必须在备战区房间中报名，定时赛循环赛在大厅报名）
struct MSG_GP_ContestApply
{
	int				iContestID;							//比赛ID	
	int				iUserID;							//玩家ID
	int				iType;								//操作类型: 0-报名, 1-退赛
};

/*
0满人赛不能在大厅报名, 1参赛成功, 2退赛成功, 3报名失败(已报名了), 4报名失败（比赛已开赛）,
5未报名退赛失败, 6比赛已开赛无法退赛, 7已达到最大人数, 8钱不足, 9房间未开启, 10用户不存在, 11比赛ID错误
*/
struct  MSG_GP_ContestApply_Result
{
	int				iContestID;							//比赛ID	
	int				iUserID;							//玩家ID
	LLONG 			i64WalletMoney;						//玩家钱包总金币
	LLONG 			i64Jewels;							//玩家钱包总房卡/钻石
	int     		iApplyNum;							//报名数量
	BYTE			bResult;							//报名结果
};

//获取已报名人数结果
struct MSG_GP_UpdateApplyNum
{
	int				iContestID;							//比赛ID
	int				iApplyNum;							//已报名人数
};

//获取比赛房间信息请求
struct MSG_GP_GetContestRoomID
{
	int				iUserID;							//用户ID
	int				iContestID;							//比赛ID
};

//获取比赛房间免费次数
struct MSG_GP_ContestHavePlayNum
{
	int				iUserID;							//用户ID
	int				iContestID;							//比赛ID
};

//发送获奖记录查询
struct MSG_GP_I_GetContestAwardlist
{
	int		UserID;
};

//比赛房间信息
struct ContestInfo
{
	int				iContestType;						//比赛类型(0：人满赛，1：定时赛，2：循环赛)
	int				iContestNum;						//已报名人数
	int				iChampionCount;						//夺冠次数
	int				iBestRank;							//最佳名次
	int				iJoinCount;							//参赛次数
	LLONG			iContestTime;						//开赛时间
	int				iRankAward[3];						//前三名奖励信息
	int				iAwardType[3];						//前三名奖励类型
};

//获取比赛房间信息请求结果
struct MSG_GP_GetContestRoomID_Result
{
	int				iUserID;							//用户ID
	ContestInfo		  tContestInfo;						//比赛信息
	ComRoomInfo		tRoomInfo;							//房间信息
};

//获取比赛房间剩余次数结果
struct MSG_GP_ContestHavePlayNumResult
{
	int			iContestID;						//比赛场信息标识
	int			iHavePlayNum;					//比赛场剩余次数
};

//获取比赛获奖记录结果
struct MSG_GP_O_GetContestAwardlist
{
	int		iUserID;
	int     iRankNum;              //比赛排名
	LLONG	FinishTime;   //结束时间
	char		szRoomName[30];//房间名字
	int     iConTestAward;        //奖励多少
	int     iConTestAwardType;    //奖励什么
	char		OrderID[50];  //订单号
	int     IsTake;               //是否已领取
	int     iGameID;
};


//定时赛和循环赛的赛前广播通知
struct MSG_GP_ContestNotice
{
	int				ContestID;							//比赛ID	
	int				iRoomID;							//比赛房间ID
	LLONG			beginTime;							//比赛开始时间
	char			szRoomName[50];						//赛场名称
};

//获取比赛奖励信息
struct MSG_GP_I_GetContestAward
{
	int				ContestID;							//比赛ID	

	MSG_GP_I_GetContestAward()
	{
		memset(this, 0, sizeof(MSG_GP_I_GetContestAward));
	}
};

//获取比赛奖励信息
struct MSG_GP_O_GetContestAward
{
	int				iAward[10];							//奖励数量
	BYTE			iAwardType[10];						//奖励类型（0:金币,1:奖券,2:钻石）
};
struct MSG_GP_O_BugList_Data
{
	int reward;
	char danhao[50];
};

/********************************************************************************************/
//兑换功能
/********************************************************************************************/
#define MDM_GP_MONEY_CHANGE					108			// 大厅比赛信息主id
#define ASS_GP_CHANGE_CONFIG				0			// 获取兑换比例
#define ASS_GP_COINTOJEWELS					1			// 金币兑换钻石
#define ASS_GP_JEWELSTOCOIN					2			// 钻石兑换金币

//获取兑换比例请求
struct MSG_GP_O_ChangeConfig
{
	int				iCoinToJewels;						//多少金币兑换1钻石，0为关闭
	int				iJewelsToCoin;						//1钻石兑换多少金币，0为关闭
};

//兑换金币或房卡
struct MSG_GP_I_ChangeRequest
{
	int				iUserID;							//用户ID
	int				iChangeJewels;						//消耗或兑换的钻石数量
};

//兑换结果
struct MSG_GP_O_ChangeRespones
{
	int				iUserID;							//用户ID
	int				iJewels;							//用户钻石
	LLONG			i64Money;							//用户钱包金币
};

#pragma pack()

#endif // !_HN_HNNetMessageHall_H_
