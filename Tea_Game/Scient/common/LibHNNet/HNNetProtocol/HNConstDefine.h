/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNBaseType.h"

#ifndef __HN_ConstDefine_H__
#define __HN_ConstDefine_H__

// 本地数据保存配置
//////////////////////////////////////////////////////////////////////////
static const char* GAME_SOUND_BUTTON     = "platform/sound/sound_button.mp3";       //点击弹出按钮音效
static const char* GAME_SOUND_CLOSE      = "platform/sound/sound_close.mp3";        //点击关闭按钮音效
static const char* GAME_BACKGROUND_MUSIC = "platform/sound/background.mp3";         //背景音乐

// 本地数据保存配置
//////////////////////////////////////////////////////////////////////////
static const char* SOUND_VALUE_TEXT		= "sound";
static const char* MUSIC_VALUE_TEXT		= "music";
static const char* USERNAME_TEXT		= "username";
static const char* PASSWORD_TEXT		= "password";
static const char* SAVE_TEXT			= "save";
static const char* GUEST_LOGIN_TEXT		= "guest";
static const char* GUEST_USERNAME_TEXT	= "guest_username";
static const char* GUEST_PASSWORD_TEXT	= "guest_password";	
static const char* AUTO_LOGIN = "auto_login";
//////////////////////////////////////////////////////////////////////////

static const char* DISCONNECT			= "isDisConnect";			//网络断开消息
static const char* RECONNECTION			= "reconnection";			//重连成功消息
static const char* FOCEGROUND			= "foreground";				//切回前台消息


#define INVALID_SOUND_VALUE		-1
#define MAX_SOUND_VALUE			100
#define MIN_SOUND_VALUE			0

#define INVALID_MUSIC_VALUE		-1
#define MAX__MUSIC_VALUE		100
#define MIN_MUSIC_VALUE			0
//////////////////////////////////////////////////////////////////////////
static const char* LOADING				= "platform/loading.png";

//////////////////////////////////////////////////////////////////////////

class MoneyChangeNotify
{
public:
	virtual ~MoneyChangeNotify() {}
	virtual void walletChanged() = 0;
	virtual void bankChanged(LLONG money) {}
	virtual void updateUserInfo() = 0;
};

#endif	//__HN_ConstDefine_H__
                                                  
