/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_VIPRoomMessage_H__
#define _HN_VIPRoomMessage_H__

#include "HNBaseType.h"
#include "HNCommon/HNCommonMarco.h"

#pragma pack(1)

/************************************************************************************************
各vip房间游戏玩法规则定义在此处，和游戏服务端协商定义，最终创建房间的时候转成字符串发送给平台
************************************************************************************************/

/*
//例如：斗地主玩法定义
struct  MSG_GR_LANDLORD
{
	bool bjiaofen;		//是否叫分
	bool iRoomID;		//房间号
	int iDeskID;		//桌子号

	MSG_GR_LANDLORD()
	{
		memset(this, 0, sizeof(MSG_GR_LANDLORD));
	}
};
*/
struct  MSG_GR_CaiShenThirteenCard
{
	bool bShuangHua;		//是否双王
	bool bSiHua;		    //是否4花
	int  iPeopleNum;		//桌子人数
	int  iPlayMode;         //玩法  1温州 2福建
	INT			iJushuType;			//局数类型
	INT			iFangfeiType;		//付费类型
	bool bTongSha;         //通杀
	bool bJiaLiangMen;     //加两门
	MSG_GR_CaiShenThirteenCard()
	{
		memset(this, 0, sizeof(MSG_GR_CaiShenThirteenCard));
	}
};

#pragma pack()

#endif	//_HN_VIPRoomMessage_H__
