﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_ComStruct_H__
#define _HN_ComStruct_H__

/********************************************************************************************/
#include "HNBaseType.h"
#include "HNCommon/HNCommonMarco.h"
#include <string>

/// 房间规则设置
#define		GRR_MEMBER_ROOM			0x00000001L							//会员房间
#define		GRR_IP_LIMITED			0x00000002L							//地址限制
#define		GRR_ENABLE_WATCH		0x00000004L							//允许旁观
#define		GRR_UNENABLE_WATCH		0x00000008L							//不许旁观
#define		GRR_AUTO_SIT_DESK		0x00000010L							//自动坐下，现在用于防作弊场使用
#define		GRR_LIMIT_DESK			0x00000020L							//限制座位
#define		GRR_LIMIT_SAME_IP		0x00000040L							//限制同IP
#define		GRR_RECORD_GAME			0x00000080L							//记录游戏
#define		GRR_STOP_TIME_CONTROL	0x00000100L							//停止时间控制
#define		GRR_ALL_ON_DESK			0x00000200L							//是否所有人坐下才开始启动
#define		GRR_FORBID_ROOM_TALK	0x00000400L							//禁止房间聊天
#define		GRR_FORBID_GAME_TALK	0x00000800L							//禁止游戏聊天
#define		GRR_MATCH_REG			0x00001000L							//比赛报名
#define		GRR_EXPERCISE_ROOM		0x00002000L							//训练场
#define		GRR_VIDEO_ROOM			0x00004000L							//视频房间

/// 修改防作弊场可看见其他玩家姓名和头像问题！
#define		GRR_NOTFUFENG			0x00008000L							//不允许负积分
#define		GRR_NOTCHEAT			0x00010000L							//防作弊

/// 台费场
#define		GRR_ALL_NEED_TAX		0x00020000L							//收台费场,所有人都需缴纳一定数额台费
#define		GRR_QUEUE_GAME			0x00040000L							//排队机

#define		GRR_NOT_COST_POINT		0x00080000L							//金币场不扣积分

#define		GRR_CONTEST				0x00100000L							//定时淘汰比赛场

#define		GRR_TIMINGCONTEST		0x00800000L							//定时赛		--RoomRule == 8388608

#define		GRR_GAME_BUY			0x02000000L							//可购买的房间



/********************************************************************************************/
///用户状态定义
//#define	USER_NO_STATE		 	0									//没有状态，不可以访问
#define		USER_LOOK_STATE		 	1									//进入了大厅没有坐下
#define		USER_SITTING		 	2									//坐在游戏桌
#define		USER_ARGEE				3									//同意状态
#define		USER_WATCH_GAME		 	4									//旁观游戏
//#define	USER_DESK_AGREE			5								///大厅同意
#define		USER_CUT_GAME		 	20									//断线状态		（游戏中状态）
#define		USER_PLAY_GAME		 	21									//游戏进行中状态	（游戏中状态）
/********************************************************************************************/

#pragma pack(1)

/// 游戏列表辅助结构
typedef struct tagAssistantHead 
{
	UINT						uSize;								// 数据大小
	UINT						bDataType;							// 类型标识
} AssistantHead;

/// 游戏名称结构
typedef struct tagComNameInfo 
{
	AssistantHead				Head;
	UINT						uCanBuy;							// 1，只有普通房间，2，只有创建房间，3，两种都有
	UINT						uKindID;							// 游戏类型 ID 号码
	UINT						uNameID;							// 游戏名称 ID 号码
	UINT						m_uOnLineCount;						// 在线人数
	UINT						uVer;								// 版本（pc端用来更新游戏）
	CHAR						szGameName[61];						// 游戏名称

}  ComNameInfo;


///游戏类型de结构
typedef struct tagComKindInfo
{
	AssistantHead				Head;
	UINT						uKindID;							// 游戏类型 ID 号码
	char						szKindName[61];						// 游戏类型名字

}  ComKindInfo;

/// 游戏房间列表结构
typedef struct tagComRoomInfo 
{
	AssistantHead				Head;
	UINT						uComType;							// 游戏类型（金币场，积分场之类，暂时只用到金币场）
	UINT						uKindID;							// 游戏类型 ID 号码
	UINT						uNameID;							// 游戏名称 ID 号码
	UINT						uRoomID;							// 游戏房间 ID 号码
	UINT						uPeopleCount;						// 房间在线人数
	UINT						iUpPeople;							// 比赛房间用户达到值后才开赛
	UINT						uDeskPeople;						// 每桌游戏人数
	UINT						uDeskCount;							// 房间桌子数目
	UINT						uServicePort;						// 房间服务端口
	CHAR						szServiceIP[25];					// 房间服务器IP地址
	CHAR						szGameRoomName[61];					// 游戏房间名称
	INT							uVirtualUser;						// 虚拟玩家数
	INT							uVirtualGameTime;					// 封桌持续时间
	UINT						uVer;								// 版本（pc端用来更新游戏）
	UINT						dwRoomRule;							// 游戏房间规则
	bool                        bVIPRoom;                           // 是否VIP房间（只有vip才能进）

	INT							iBasePoint;							// 基础倍数
	UINT						iLessPoint;							// 金币下限
	UINT						iMaxPoint;							// 金币上限

	INT							iContestID;							// 比赛ID
	LLONG						i64TimeStart;						// 定时赛开赛时间

	bool						bHasPassword;						// 有无密码	（输入密码才能进）
	UINT						dwTax;								// 房间税率

}  ComRoomInfo;

/// 用户信息结构
typedef struct tagUserInfoStruct 
{
	INT							dwUserID;							//ID号码
	LLONG						i64Money;							//钱包金币
	LLONG						i64Bank;							//银行金币
	INT							iJewels;							//可用钻石
	INT							iLockJewels;						//冻结钻石
	UINT						uWinCount;							//胜利数目
	UINT						uLostCount;							//输数目
	UINT						uCutCount;							//强退数目
	UINT						uMidCount;							//和局数目
	CHAR						szName[32];							//登录名
	UINT						bLogoID;							//头像ID
	BYTE						bDeskNO;							//游戏桌号
	BYTE						bDeskStation;						//游戏座位号
	BYTE						bUserState;							//用户状态
	UINT						dwUserIP;							//登录IP地址
	bool						bBoy;								//性别
	CHAR						nickName[50];						//用户昵称
	INT							iVipTime;							//会员时间
	INT                          iVipLevel;
	bool						isVirtual;							//是否是机器人 
	CHAR						szSignDescr[128];			        //个性签名
	CHAR						headUrl[256];						//用户头像URL
	FLOAT						fLat;								//用户经度
	FLOAT						fLnt;								//用户维度
	CHAR						szLocation[128];					//用户地址
	///比赛专用
	LLONG						i64ContestScore;					//比赛分数
	INT                          iIsHaveTaoTai;					//是否淘汰
	INT							iRankNum;							//排行名次
	INT							iContestCount;						//比赛次数
	bool						bLogonByPhone;						//是否手机登陆

}  UserInfoStruct;

// 自动赠送结构体
typedef struct tagSysGiveMoney 
{	
	LLONG						i64Money;		//赠送数量
	LLONG						i64MinMoney;	//赠送条件（小于xx金币则赠送）
	INT							iCount;			//本日已经赠送次数
	INT							iTotal;			//本日最大赠送次数
	INT							iResultCode;	//赠送结果（1：成功，2：身上的钱大于最小领取限制，3：已经领完）

}  SysGiveMoney;

#pragma pack()

#endif	//_HN_ComStruct_H__
