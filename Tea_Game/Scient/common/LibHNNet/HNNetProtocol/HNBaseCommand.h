﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HNBaseCommand_H__
#define _HNBaseCommand_H__

#include "HNBaseType.h"
#include <string>

// 此处为密钥由客户自己管理，设置在整数范围
LLONG getSecrectKey();

// 平台的登录ID地址
std::string getPlatformServerAddress();

// 平台的端口
INT getPlatformServerPort();

// 获取域名Url
std::string getWebServerUrl();

// 获取API接口地址
std::string getAPIServerUrl();

// 获取加密秘钥
std::string getXXTEA_KEY();

// 从本地config获取ip
std::string getPlatformAddressFromConfig();

//初始化配置Url信息
void initConfigUrl();

// 获取php域名Url
std::string getPHPServerUrl();
#endif	//_HNBaseCommand_H__