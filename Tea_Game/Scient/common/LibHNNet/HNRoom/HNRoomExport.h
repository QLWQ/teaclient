﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNRoom_Export_H__
#define __HN_HNRoom_Export_H__

#include "HNRoom/HNRoomMessageDelegate.h"
#include "HNRoom/HNRoomLogic.h"

#endif	//__HN_HNRoom_Export_H__

