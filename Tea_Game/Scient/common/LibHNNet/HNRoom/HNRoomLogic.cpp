/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNRoomLogic.h"
#include "HNRoomMessageDelegate.h"
#include "HNData/HNRoomInfoModule.h"
#include "HNData/HNUserInfoModule.h"
#include "HNPlatform/HNPlatformExport.h"
#include "HNSocket/HNSocketProtocolData.h"
#include "HNSocket/HNSocketLogic.h"
#include "HNSocket/HNSocket.h"
#include "HNNetProtocol/HNProtocolExport.h"
#include "HNGame/HNGameMessageDelegate.h"
#include "HNCommon/HNConverCode.h"
#include "HNSocket/HNSocketThread.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "HNNetProtocol/HNRoomForPCMessage.h"
#endif


namespace HN
{
	static HNRoomLogic* sHNRoomLogic = nullptr;

	HNRoomLogic* HNRoomLogic::getInstance()
	{
		if (nullptr == sHNRoomLogic)
		{
			sHNRoomLogic = new HNRoomLogic();
			sHNRoomLogic->init();
		}
		return sHNRoomLogic;
	}
	
	HNRoomLogic::HNRoomLogic(void)
		: _gameRoomRule(0)
		, _serverPort(-1)
		, _connected(false)
		, _logined(false)
		, _selectedRoom(nullptr)
	{
        memset(&loginResult, 0x00, sizeof(loginResult));
		_socketLogic = new HNSocketLogic(this, "game");
		
		_gameNotifyQueue = new HNUIDelegateQueue<IGameMessageDelegate*>();
		_addGameNotifyQueue = new HNUIDelegateQueue<IGameMessageDelegate*>();
		_removeGameNotifyQueue = new HNUIDelegateQueue<IGameMessageDelegate*>();

		_roomNotifyQueue = new HNUIDelegateQueue<IRoomMessageDelegate*>();
		_addRoomNotifyQueue = new HNUIDelegateQueue<IRoomMessageDelegate*>();
		_removeRoomNotifyQueue = new HNUIDelegateQueue<IRoomMessageDelegate*>();
	}

	HNRoomLogic::~HNRoomLogic(void)
	{
		HN_SAFE_DELETE(_addGameNotifyQueue);
		HN_SAFE_DELETE(_gameNotifyQueue);
		HN_SAFE_DELETE(_removeGameNotifyQueue);
		HN_SAFE_DELETE(_roomNotifyQueue);
		HN_SAFE_DELETE(_addRoomNotifyQueue);
		HN_SAFE_DELETE(_removeRoomNotifyQueue);
		HN_SAFE_DELETE(_socketLogic);
	}

	bool HNRoomLogic::init() 
	{
		return true;
	}

	bool HNRoomLogic::connect(const CHAR* ipOrHost, INT port)
	{
		std::string ip(ipOrHost);
		if(!HNSocket::isValidIP(ip))
		{
			ip = HNSocket::getIpAddress(ip.c_str()).at(0);
		}
		_serverAddress = ip;
		_serverPort    = port;
		return _socketLogic->openWithIp(_serverAddress.c_str(), _serverPort);
	}

	bool HNRoomLogic::close()
	{
		_connected =  false;
		//setRoomRule(0);
		return _socketLogic->close();
	}

	bool HNRoomLogic::isConnect() const 
	{ 
		return _connected && _socketLogic->connected(); 
	} 

	void HNRoomLogic::setConnect(bool isConnect)
	{
		_connected = isConnect;
	}

	bool HNRoomLogic::isLogin() const
	{
		return _logined;
	}

	INT HNRoomLogic::sendData(UINT MainID, UINT AssistantID, void* object, INT objectSize)
	{	
		return _socketLogic->send(MainID, AssistantID, HNSocketProtocolData::GameCheckCode, object, objectSize);
	}

	void HNRoomLogic::sendData(UINT MainID, UINT AssistantID, void* object, INT objectSize, SEL_SocketMessage selector)
	{
		_socketLogic->send(MainID, AssistantID, HNSocketProtocolData::GameCheckCode, object, objectSize);
		_socketLogic->addEventSelector(MainID, AssistantID, selector);
	}

	void HNRoomLogic::addEventSelector(UINT MainID, UINT AssistantID, SEL_SocketMessage selector)
	{
		_socketLogic->addEventSelector(MainID, AssistantID, selector);
	}

	void HNRoomLogic::removeEventSelector(UINT MainID, UINT AssistantID)
	{
		_socketLogic->removeEventSelector(MainID, AssistantID);
	}

	void HNRoomLogic::addGameObserver(IGameMessageDelegate* delegate)
	{
		_addGameNotifyQueue->addObserver(delegate);
		_removeGameNotifyQueue->removeObserver(delegate);
	}

	void HNRoomLogic::removeGameObserver(IGameMessageDelegate* delegate)
	{
		_removeGameNotifyQueue->addObserver(delegate);
		_addGameNotifyQueue->removeObserver(delegate);
	}

	void HNRoomLogic::addRoomObserver(IRoomMessageDelegate* delegate)
	{
		_addRoomNotifyQueue->addObserver(delegate);
		_removeRoomNotifyQueue->removeObserver(delegate);
	}

	void HNRoomLogic::removeRoomObserver(IRoomMessageDelegate* delegate)
	{
		_removeRoomNotifyQueue->addObserver(delegate);
		_addRoomNotifyQueue->removeObserver(delegate);
	}

	void HNRoomLogic::login(UINT uGameID, float latitude, float longtitude, const std::string& addr)
	{
		MSG_GR_S_RoomLogon data = {0};

		data.uNameID = uGameID;
		data.dwUserID = PlatformLogic()->loginResult.dwUserID;
		strcpy(data.szMD5Pass, PlatformLogic()->loginResult.szMD5Pass);
		data.fLat = latitude;
		data.fLnt = longtitude;
		strcpy(data.szLocation, addr.c_str());
		data.bLogonByPhone = true;
		sendData(MDM_GR_LOGON, ASS_GR_LOGON_BY_ID, &data, sizeof(data));
	}

	void HNRoomLogic::onConnected(bool connect)
	{
		_connected = connect;
		// 分发事件
		dispatchFrameMessage([connect](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_Connect(connect);
			return false;
		});
	}

	void HNRoomLogic::onSocketMessage(HNSocketMessage* socketMessage)
	{
		std::string msgInfo = StringUtils::format("==========mainid:%d, assistid:%d==========", socketMessage->messageHead.bMainID, socketMessage->messageHead.bAssistantID);
		log("%s", msgInfo.c_str());

		switch (socketMessage->messageHead.bMainID)
		{
		// 连接
		case MDM_GR_CONNECT: 
			H_R_M_Connect(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 登录消息
		case MDM_GR_LOGON: 
			H_R_M_Login(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 用户列表
		case MDM_GR_USER_LIST:
			H_R_M_UserList(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 恢复释放桌子信息
		case MDM_GR_RETURNDESK:
			H_R_M_ReturnDeskInfo(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 用户动作 
		case MDM_GR_USER_ACTION:
			H_R_M_UserAction(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 房间消息
		case MDM_GR_ROOM:
			H_R_M_Room(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 框架消息
		case MDM_GM_GAME_FRAME:	
			H_R_M_GameFrame(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 特效道具
		case MDM_GR_PROP:
			H_R_M_GameProp(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 游戏消息
		case MDM_GM_GAME_NOTIFY:	
			H_R_M_GameNotify(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			
			//游戏消息转比赛场结算
			H_R_M_GameResult(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;
		// 语音消息
		case MDM_GR_VOICE:
			H_R_M_VoiceNotify(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;
		// 封桌
		case MDM_GR_MANAGE:
			H_R_M_Manage(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// PC专用消息
#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		// 房间信息
		case MDM_GR_GAMEBASEINFO:
			H_R_M_GameBaseInfo(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 玩家列表
		case MDM_GR_USERINFO:
			H_R_M_UserInfo(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 用户状态
		case MDM_GR_USERSTATUS:
			H_R_M_UserStats(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;

		// 关闭窗体
		case MDM_GR_CONTROL:
			H_R_M_Control(&socketMessage->messageHead, socketMessage->object, socketMessage->objectSize);
			break;
#endif

		default:
			break;
		}
	}

	void HNRoomLogic::H_R_M_GameNotify(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dispatchGameMessage([&messageHead, &object, & objectSize](IGameMessageDelegate* delegate) -> bool 
		{
			delegate->onGameMessage((NetMessageHead*)messageHead, object, objectSize);
			return true;
		});
	}

	void HNRoomLogic::H_R_M_GameResult(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (messageHead->bAssistantID == 205)			//泉州麻将结算消息
		{
			if (_matchDelegate) _matchDelegate->I_R_M_ContestResult(object, objectSize);
		}
	}

	void HNRoomLogic::H_R_M_VoiceNotify(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dispatchGameMessage([&messageHead, &object, &objectSize](IGameMessageDelegate* delegate) -> bool
		{
			delegate->onVoiceMessage((NetMessageHead*)messageHead, object, objectSize);
			return true;
		});
	}

	void HNRoomLogic::H_R_M_GameFrame(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		// 游戏信息
		if(ASS_GM_GAME_INFO == messageHead->bAssistantID)
		{
			H_R_M_GameInfo(messageHead, object, objectSize);
		}

		// 游戏状态
		else if(ASS_GM_GAME_STATION == messageHead->bAssistantID)
		{
			H_R_M_GameStation(messageHead, object, objectSize);
		}

		// 普通聊天
		else if(ASS_GM_NORMAL_TALK == messageHead->bAssistantID)
		{
			H_R_M_NormalTalk(messageHead, object, objectSize);
		}

		else if (ASS_GM_GET_GAMENUM == messageHead->bAssistantID)
		{
			H_R_M_GameActivity(messageHead, object, objectSize);
		}

		else if (ASS_GM_GET_RANDNUM == messageHead->bAssistantID)
		{
			H_R_M_GameReceive(messageHead, object, objectSize);
		}

		else if (ASS_GM_CONTEST_GAME_INFO == messageHead->bAssistantID)
		{
			H_R_M_GameInfoUP(messageHead, object, objectSize);
		}
	}

	void HNRoomLogic::H_R_M_GameProp(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		//道具特效
		if (ASS_PROP_NEW_TEXIAO == messageHead->bAssistantID)
		{
			H_R_M_NormalAction(messageHead, object, objectSize);
		}
	}

	void HNRoomLogic::H_R_M_GameInfo(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		MSG_GM_S_GameInfo* pGameInfo = (MSG_GM_S_GameInfo*)object;

		// 显示信息
		if (messageHead->uMessageSize > (sizeof(MSG_GM_S_GameInfo) - sizeof(pGameInfo->szMessage)))
		{
			dispatchGameMessage([&pGameInfo](IGameMessageDelegate* delegate) -> bool 
			{
				delegate->I_R_M_GameInfo(pGameInfo);
				return true;
			});
		}
	}

	void HNRoomLogic::H_R_M_GameStation(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool 
		{
			delegate->I_R_M_GameStation(object, objectSize);
			return true;
		});
	}

	void HNRoomLogic::H_R_M_NormalTalk(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool 
		{
			(dynamic_cast<IGameChartMessageDelegate*>(delegate))->I_R_M_NormalTalk(object, objectSize);
			return true;
		});
	}

	void HNRoomLogic::H_R_M_NormalAction(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (messageHead->bHandleCode == 1)
		{
			//金币不足
			dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserNotEnoughMoney();
				return true;
			});
			return;
		}
		dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool
		{
			(dynamic_cast<IGameChartMessageDelegate*>(delegate))->I_R_M_NormalAction(object, objectSize);
			return true;
		});
	}

	void HNRoomLogic::H_R_M_GameActivity(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CCAssert(ASS_GM_GET_GAMENUM == messageHead->bAssistantID, "assist id error.");
		CCAssert(objectSize == sizeof(MSG_GR_GameNum), "size error.");

		dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameActicity(object, objectSize);
			return true;
		});
	}

	void HNRoomLogic::H_R_M_GameReceive(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CCAssert(ASS_GM_GET_RANDNUM == messageHead->bAssistantID, "assist id error.");
		CCAssert(objectSize == sizeof(MSG_GR_Set_JiangLi), "size error.");

		dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameReceive(object, objectSize);
			return true;
		});
	}

	void HNRoomLogic::H_R_M_GameInfoUP(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		//log("Sizeof(MSG_GM_S_ConTestGameInfo)==%d,%d", objectSize, sizeof(MSG_GM_S_ConTestGameInfo));
		CHECK_SOCKET_DATA(MSG_GM_S_ConTestGameInfo, objectSize, "MSG_GM_S_ConTestGameInfo size of error!");
		MSG_GM_S_ConTestGameInfo* contestInfodata = (MSG_GM_S_ConTestGameInfo*)object;
		if (_matchDelegate) _matchDelegate->I_R_M_ContestInfoupdate(contestInfodata);

		dispatchGameMessage([&messageHead, contestInfodata](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameInfoupdate(contestInfodata);
			return false;
		});
	}

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	void HNRoomLogic::H_R_M_GameBaseInfo(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CCAssert(ASS_GR_GAMEBASEINFO == messageHead->bAssistantID, "assist id error.");
		CCAssert(objectSize == sizeof(GameInfoEx), "size error.");

		dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameBaseInfo((GameInfoEx*)object);
			return true;
		});
	}
#endif

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	void HNRoomLogic::H_R_M_UserInfo(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		
		CCAssert(ASS_GR_USERINFOLIST == messageHead->bAssistantID, "assist id error.");
		CHECK_SOCKET_DATA(UserInfoStruct, objectSize, "size error.");

		UserInfoStruct* pData = (UserInfoStruct*)object;

		// 更新
		UserInfoModule()->updateUser(pData);

		MSG_GR_R_UserSit userSit;
		userSit.bDeskIndex   = pData->bDeskNO;
		userSit.bDeskStation = pData->bDeskStation;
		userSit.dwUserID     = pData->dwUserID;

		// 分发游戏消息(当做坐下消息处理）
		dispatchGameMessage([&userSit, &pData](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserSit(&userSit, pData);
			return false;
		});
	}
#endif

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	void HNRoomLogic::H_R_M_UserStats(const NetMessageHead* messageHead, void* object, INT objectSize)
	{

	}
#endif

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	void HNRoomLogic::H_R_M_Control(const NetMessageHead* messageHead, void* object, INT objectSize)
	{

	}
#endif

	void HNRoomLogic::H_R_M_Room(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		// 游戏开始
		if(ASS_GR_GAME_BEGIN == messageHead->bAssistantID)
		{
			H_R_M_GameBegin(messageHead, object, objectSize);
		}

		// 比赛场继续
		else if (ASS_GR_CONTEST_CLEARDESK == messageHead->bAssistantID)
		{
			H_R_M_GameClean(messageHead, object, objectSize);
		}

		// 比赛场轮数
		else if (ASS_GR_CONTEST_ROUNDNUM == messageHead->bAssistantID)
		{
			H_R_M_GameRound(messageHead, object, objectSize);
		}

		// 更新当前排名，剩余等待桌数
		else if (ASS_GR_CONTEST_WAIT_RISE_INFO == messageHead->bAssistantID)
		{
			H_R_M_GameWaitInfo(messageHead, object, objectSize);
		}

		// 排队准备
		else if (ASS_GR_ROOM_QUEUE_READY == messageHead->bAssistantID)
		{
			H_R_M_QueueReady(messageHead, object, objectSize);
		}

		// 游戏结算
		else if(ASS_GR_USER_POINT == messageHead->bAssistantID)
		{
			H_R_M_UserPoint(messageHead, object, objectSize);
		}

		// 游戏结束
		else if(ASS_GR_GAME_END == messageHead->bAssistantID)
		{
			H_R_M_GameEnd(messageHead, object, objectSize);
		}

		// 用户同意
		else if(ASS_GR_USER_AGREE == messageHead->bAssistantID)
		{
			H_R_M_UserAgree(messageHead, object, objectSize);
		}

		// 及时更新金币和积分
		else if(ASS_GR_INSTANT_UPDATE == messageHead->bAssistantID)
		{
			H_R_M_InstantUpdate(messageHead, object, objectSize);
		}

		// 金币不足提示
		else if (ASS_GR_MONEY_NOTENOUGH == messageHead->bAssistantID)
		{
			H_R_M_NotEnoughMoney(messageHead, object, objectSize);
		}

		// 用户比赛信息
		else if(ASS_GR_USER_CONTEST == messageHead->bAssistantID)
		{
			H_R_M_UserContest(messageHead, object, objectSize);
		}

		//其他桌比赛结束晋级
		else if (ASS_GR_CONTEST_FINISHTONEXT == messageHead->bAssistantID)
		{
			H_R_M_UserPromotion(messageHead, object, objectSize);
		}

		// 比赛结束
		else if(ASS_GR_CONTEST_GAMEOVER == messageHead->bAssistantID)
		{
			H_R_M_ContestOver(messageHead, object, objectSize);
		}

		// 用户被淘汰
		else if(ASS_GR_CONTEST_KICK == messageHead->bAssistantID)
		{
			H_R_M_ContestKick(messageHead, object, objectSize);
		}

		// 比赛等待结束
		else if(ASS_GR_CONTEST_WAIT_GAMEOVER == messageHead->bAssistantID)
		{
			H_R_M_ContestWaitOver(messageHead, object, objectSize);
		}

		// 等待其他桌比赛结束
		else if (ASS_GR_CONTEST_FINISHTOWAIT == messageHead->bAssistantID)
		{
			H_R_M_ContestWaitOver(messageHead, object, objectSize);
		}
	}

	void HNRoomLogic::H_R_M_GameBegin(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		BYTE deskNo = (BYTE)messageHead->bHandleCode;
		// 更新用户状态
		UserInfoModule()->transform(deskNo, [](UserInfoStruct* user, INT index) 
		{
			user->bUserState = USER_PLAY_GAME;
		});

		dispatchFrameMessage([deskNo](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UpdateDeskState(deskNo, true);
			return false;
		});

		dispatchGameMessage([&messageHead](IGameMessageDelegate* delegate) -> bool 
		{
			delegate->I_R_M_GameBegin((BYTE)messageHead->bHandleCode);
			return false;
		});
	}

	void HNRoomLogic::H_R_M_GameClean(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dispatchGameMessage([&messageHead](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameClean();
			return false;
		});

		CHECK_SOCKET_DATA(MSG_GR_ConTestRoundNumInOneRound, objectSize, "MSG_GR_ConTestRoundNumInOneRound size of error!");
		MSG_GR_ConTestRoundNumInOneRound* contestInnings = (MSG_GR_ConTestRoundNumInOneRound*)object;
		//CHECK_SOCKET_DATA(MSG_GR_ConTestRoundNumInOneRound + contestInnings->, objectSize, "MSG_GR_ConTestRoundNumInOneRound size of error!");
		if (_matchDelegate) _matchDelegate->I_R_M_ContestInnings(contestInnings);

		dispatchGameMessage([&messageHead, contestInnings](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameInnings(contestInnings);
			return false;
		});

		/*dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameInnings(object, objectSize);
		});*/
	}

	void HNRoomLogic::H_R_M_GameRound(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_ConTestRoundNum, objectSize, "MSG_GR_ConTestRoundNum size of error!");
		MSG_GR_ConTestRoundNum* contestRoundNum = (MSG_GR_ConTestRoundNum*)object;
		//DUMP_MSG(MSG_GR_ConTestRoundNum, (char*)object);
		if (_matchDelegate) _matchDelegate->I_R_M_ContestRoundNum(contestRoundNum);

		dispatchGameMessage([&messageHead, contestRoundNum](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameRoundNum(contestRoundNum);
			return false;
		});
		
		/*dispatchGameMessage([&object, objectSize](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameNumRound(object, objectSize);
		});*/
	}

	void HNRoomLogic::H_R_M_GameWaitInfo(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_SContestWaitRiseInfo, objectSize, "MSG_GR_SContestWaitRiseInfo size of error!");
		MSG_GR_SContestWaitRiseInfo* SContestWaitRiseInfo = (MSG_GR_SContestWaitRiseInfo*)object;

		if (_matchDelegate) _matchDelegate->I_R_M_ContestInfoMatchWait(SContestWaitRiseInfo);
	}

	void HNRoomLogic::H_R_M_QueueReady(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dispatchGameMessage([=](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_QueueReady();
			return false;
		});
	}

	void HNRoomLogic::H_R_M_UserPoint(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		// 判断是否是入库类型
		auto isNormal = []()->bool
		{
			bool special = (RoomLogic()->getRoomRule() & GRR_EXPERCISE_ROOM)
				|| (RoomLogic()->getRoomRule() & GRR_CONTEST) 
				|| (RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST);

			return !special;
		};

		UserInfoStruct userInfo = {0};

		if (10 == messageHead->bHandleCode)			// 同步金币
		{
			// 效验数据
			CHECK_SOCKET_DATA(MSG_GR_S_RefalshMoney, objectSize, "MSG_GR_S_RefalshMoney size is error.");

			MSG_GR_S_RefalshMoney * pReflashMoney = (MSG_GR_S_RefalshMoney*)object;

			// 处理数据
			const UserInfoStruct * pUserInfo = UserInfoModule()->findUser(pReflashMoney->dwUserID);
			if (pUserInfo == nullptr) return;

			userInfo = *pUserInfo;
			userInfo.i64Money = pReflashMoney->i64Money;
			UserInfoModule()->updateUser(&userInfo);

			// 更新自己数据
			if (pReflashMoney->dwUserID == PlatformLogic()->loginResult.dwUserID)
			{
				RoomLogic()->loginResult.pUserInfoStruct = userInfo;
				if (isNormal())
				{
					PlatformLogic()->loginResult.i64Money = pReflashMoney->i64Money;
				}				
			}
		}
		else if (11 == messageHead->bHandleCode)	// 同步经验值
		{
			// 效验数据
			CHECK_SOCKET_DATA(MSG_GR_S_RefalshMoney, objectSize, "MSG_GR_S_RefalshMoney size is error.");

			MSG_GR_S_RefalshMoney * pReflashMoney = (MSG_GR_S_RefalshMoney*)object;

			// 处理数据
			const UserInfoStruct * pUserInfo = UserInfoModule()->findUser(pReflashMoney->dwUserID);
			if (pUserInfo == nullptr) return;

			userInfo = *pUserInfo;
			userInfo.i64Money = pReflashMoney->i64Money;
			UserInfoModule()->updateUser(&userInfo);

			// 更新自己数据
			if (pUserInfo->dwUserID == PlatformLogic()->loginResult.dwUserID)
			{
				RoomLogic()->loginResult.pUserInfoStruct = userInfo;
			}

		}
		else if (0 == messageHead->bHandleCode)		// 同步经验值
		{
			// 效验数据
			CHECK_SOCKET_DATA(MSG_GR_R_UserPoint, objectSize, "MSG_GR_R_UserPoint size is error.");

			MSG_GR_R_UserPoint * pUserPoint = (MSG_GR_R_UserPoint*)object;

			const UserInfoStruct * pUserInfo = UserInfoModule()->findUser(pUserPoint->dwUserID);
			if (pUserInfo == nullptr) return;

			userInfo = *pUserInfo;

			// 更新用户信息
			userInfo.i64Money += pUserPoint->dwMoney;				// 用户金币
			userInfo.uWinCount += pUserPoint->bWinCount;			// 胜局
			userInfo.uLostCount += pUserPoint->bLostCount;			// 输局
			userInfo.uMidCount += pUserPoint->bMidCount;			// 平局
			userInfo.uCutCount += pUserPoint->bCutCount;			// 逃局

			UserInfoModule()->updateUser(&userInfo);

			// 更新自己数据
			if (pUserInfo->dwUserID == PlatformLogic()->loginResult.dwUserID)
			{
				RoomLogic()->loginResult.pUserInfoStruct = userInfo;
				if (isNormal())
				{
					PlatformLogic()->loginResult.i64Money += pUserPoint->dwMoney;
				}				
			}
		}

		if (0 == userInfo.dwUserID) return;
		
		dispatchFrameMessage([&userInfo](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserInfoChange(&userInfo);
			return false;
		});

		dispatchGameMessage([&userInfo](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserInfoChange(&userInfo);
			return false;
		});
	}

	void HNRoomLogic::H_R_M_GameEnd(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		BYTE deskNo = (BYTE)messageHead->bHandleCode;

		dispatchFrameMessage([deskNo](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UpdateDeskState(deskNo, false);
			return false;
		});

		dispatchGameMessage([deskNo](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_GameEnd(deskNo);
			return false;
		});

		if ((_gameRoomRule & GRR_CONTEST) || (_gameRoomRule & GRR_TIMINGCONTEST) || (_gameRoomRule & GRR_QUEUE_GAME))
		{
			std::vector<UserInfoStruct*> users;
			UserInfoModule()->findDeskUsers(deskNo, users);
			std::for_each(users.begin(), users.end(), [](UserInfoStruct* user)
			{
				user->bDeskStation = INVALID_DESKSTATION;
				user->bDeskNO = INVALID_DESKNO;
				user->bUserState = USER_LOOK_STATE;
			});
		}
	}


	void HNRoomLogic::H_R_M_UserAgree(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_R_UserAgree, objectSize, "MSG_GR_R_UserAgree size is error.");

		MSG_GR_R_UserAgree * pUserAgree = (MSG_GR_R_UserAgree*)object;

		const UserInfoStruct * pUserInfo = UserInfoModule()->findUser(pUserAgree->bDeskNO, pUserAgree->bDeskStation);
		if (nullptr != pUserInfo)
		{
			UserInfoStruct userInfo = *pUserInfo;
			userInfo.bUserState = USER_ARGEE;
			UserInfoModule()->updateUser(&userInfo);
		}

		dispatchGameMessage([&pUserAgree](IGameMessageDelegate* delegate) -> bool 
		{
			delegate->I_R_M_UserAgree(pUserAgree);
			return false;
		});
	}


	void HNRoomLogic::H_R_M_InstantUpdate(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_R_InstantUpdate, objectSize, "MSG_GR_R_InstantUpdate size is error.");

		MSG_GR_R_InstantUpdate * pInstantUpdate = (MSG_GR_R_InstantUpdate*)object;

		// 更新数据
		const UserInfoStruct * pUserInfo = UserInfoModule()->findUser(pInstantUpdate->dwUserID);
		if (nullptr != pUserInfo)
		{
			UserInfoStruct userInfo = *pUserInfo;
			userInfo.i64Money += pInstantUpdate->dwMoney;
			UserInfoModule()->updateUser(&userInfo);
		}

		// 更新自己数据
		if (pUserInfo->dwUserID == PlatformLogic()->loginResult.dwUserID)
		{
			RoomLogic()->loginResult.pUserInfoStruct = *pUserInfo;
			PlatformLogic()->loginResult.i64Money = pInstantUpdate->dwMoney;
		}
	}

	// 金币不足提示
	void HNRoomLogic::H_R_M_NotEnoughMoney(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dispatchGameMessage([](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserNotEnoughMoney();
			return false;
		});
	}

	void HNRoomLogic::H_R_M_UserContest(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_ContestChange, objectSize, "MSG_GR_ContestChange size of error!");
		MSG_GR_ContestChange * contestChange = (MSG_GR_ContestChange*)object;

		const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(contestChange->dwUserID);
		if (nullptr != pUserInfo)
		{
			UserInfoStruct userInfo = *pUserInfo;
			userInfo.i64ContestScore = contestChange->i64ContestScore;	// 当前比赛分数
			//屏蔽原先更新排名信息
			//userInfo.iRankNum = contestChange->iRankNum;				// 排行名次
			userInfo.iContestCount = contestChange->iContestCount;		// 比赛局数
			UserInfoModule()->updateUser(&userInfo);

			// 游戏中不再继承比赛代理，所以比赛分数变化需要游戏显示则通过结算消息通知
			dispatchGameMessage([&userInfo](IGameMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserInfoChange(&userInfo);
				return false;
			});
		}

		RoomLogic()->loginResult.iRemainPeople = contestChange->iRemainPeople;	// 比赛中还剩下的人数

		if (contestChange->dwUserID != PlatformLogic()->loginResult.dwUserID) return;

		if (_matchDelegate) _matchDelegate->I_R_M_UserContest(contestChange);
	}

	void HNRoomLogic::H_R_M_UserPromotion(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		//MSG_GR_ContestChange* contestChange;
		const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(PlatformLogic()->loginResult.dwUserID);
		if (nullptr != pUserInfo)
		{
			/*contestChange->i64ContestScore = pUserInfo->i64ContestScore;
			contestChange->iRankNum = pUserInfo->iRankNum;
			contestChange->dwUserID = pUserInfo->dwUserID;
			contestChange->iContestCount = pUserInfo->iContestCount;
			contestChange->iRemainPeople = RoomLogic()->loginResult.iRemainPeople;*/
			//strcpy(contestChange->szUserName, pUserInfo->
			if (_matchDelegate) _matchDelegate->I_R_M_UserPromotion();
		}
	}

	void HNRoomLogic::H_R_M_ContestOver(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_ContestAward, objectSize, "MSG_GR_ContestAward size of error!");
		MSG_GR_ContestAward* contestAward = (MSG_GR_ContestAward*)object;

		if (_matchDelegate) _matchDelegate->I_R_M_ContestOver(contestAward);
	}

	void HNRoomLogic::H_R_M_ContestKick(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_R_ContestMingCi, objectSize, "MSG_GR_R_ContestMingCi size of error!");
		MSG_GR_R_ContestMingCi* contestRank = (MSG_GR_R_ContestMingCi*)object;
		if (_matchDelegate) _matchDelegate->I_R_M_ContestKick(contestRank);
	}

	void HNRoomLogic::H_R_M_ContestWaitOver(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (_matchDelegate) _matchDelegate->I_R_M_ContestWaitOver();
	}

	void HNRoomLogic::H_R_M_ContestRecord(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_ContestRecord_Result, objectSize, "MSG_GR_ContestRecord_Result size of error!");
		MSG_GR_ContestRecord_Result* contestRecord = (MSG_GR_ContestRecord_Result*)object;

		if (_matchDelegate) _matchDelegate->I_R_M_ContestRecord(contestRecord);
	}
	
	void HNRoomLogic::H_R_M_Manage(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		//封桌
		if(messageHead->bHandleCode == 1)
		{
			RoomLogic()->deskStation.bVirtualDesk[messageHead->bAssistantID] = 1;

			dispatchFrameMessage([&messageHead](IRoomMessageDelegate* delegate) -> bool 
			{
				delegate->I_R_M_UpdateVirtualDesk(messageHead->bAssistantID, true);
				return true;
			});
		}

		// 解封桌子
		else
		{
			RoomLogic()->deskStation.bVirtualDesk[messageHead->bAssistantID] = 0;

			dispatchFrameMessage([&messageHead](IRoomMessageDelegate* delegate) -> bool 
			{
				delegate->I_R_M_UpdateVirtualDesk(messageHead->bAssistantID, false);
				return true;
			});
		}
	}

	void HNRoomLogic::H_R_M_Connect(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CCAssert(ASS_GR_CONNECT_SUCCESS == messageHead->bAssistantID, "assistId error.");

		CHECK_SOCKET_DATA(MSG_S_ConnectSuccess, objectSize, "size error.");

		int n = sizeof(MSG_S_ConnectSuccess);
		MSG_S_ConnectSuccess* pData = (MSG_S_ConnectSuccess*)object;

		_connected = true;
		HNSocketProtocolData::GameCheckCode = (UINT)(pData->i64CheckCode - getSecrectKey()) / 23;

		_socketLogic->startHeartBeatThread();

		// 分发事件
		dispatchFrameMessage([](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_Connect(true);
			return false;
		});
	}	

	void HNRoomLogic::H_R_M_Login(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		char chs[20] = {0};
		std::string message;

		// 异地登录
		if(ASS_GP_LOGON_ALLO_PART == messageHead->bAssistantID)
		{
			message = GBKToUtf8("异地登录");

			dispatchFrameMessage([&messageHead, &message](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_Login(false, messageHead->bHandleCode, message);
				return false;
			});
		}

		// 设备锁定
		else if(ASS_GP_LOGON_LOCK_VALID == messageHead->bAssistantID)
		{
			message = GBKToUtf8("设备锁定");

			dispatchFrameMessage([&messageHead, &message](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_Login(false, messageHead->bHandleCode, message);
				return false;
			});
		}

		// 登录成功
		else if(ASS_GR_LOGON_SUCCESS == messageHead->bAssistantID)
		{
			message = GBKToUtf8("房间登录成功");

			CHECK_SOCKET_DATA(MSG_GR_R_LogonResult, objectSize, "size error.");

			MSG_GR_R_LogonResult * pData = (MSG_GR_R_LogonResult*)object;

			loginResult = *pData;
			_logined    = (ERR_GP_LOGON_SUCCESS == messageHead->bHandleCode);

			// 有系统赠送
			if (pData->sGiveMoney.iResultCode == 1)
			{
				PlatformLogic()->loginResult.i64Money = pData->pUserInfoStruct.i64Money;
			}

			dispatchFrameMessage([&](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_SysGiveMoney(&loginResult.sGiveMoney);

				delegate->I_R_M_Login(true, messageHead->bHandleCode, message);

				return false;
			});
		}

		// 登录错误
		else if(ASS_GR_LOGON_ERROR == messageHead->bAssistantID)
		{
			switch (messageHead->bHandleCode)
			{
			case ERR_GR_USER_PASS_ERROR:
				message = GBKToUtf8("用户密码错误");
				break;
			case ERR_GR_CONTEST_TIMEOUT:
				message = GBKToUtf8("比赛场连接超时");
				break;
			case ERR_GR_IN_OTHER_ROOM:
				message = GBKToUtf8("正在其他房间");
				break;
			case ERR_GR_ACCOUNTS_IN_USE:
				message = GBKToUtf8("帐号正在使用");
				break;
			case ERR_GR_STOP_LOGON:
				message = GBKToUtf8("暂停登陆服务");
				break;
			case ERR_GR_PEOPLE_FULL:
				message = GBKToUtf8("房间人数已经满");
				break;
			default:
				message = StringUtils::format(GBKToUtf8("未知登录错误%d"), messageHead->bHandleCode);
				break;
			}

			dispatchFrameMessage([&messageHead, &message](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_Login(false, messageHead->bHandleCode, message);
				return false;
			});
		}

		// 登录完成
		else if(ASS_GR_SEND_FINISH == messageHead->bAssistantID)
		{
			dispatchFrameMessage([](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_LoginFinish();
				return false;
			});	
		}
	}	

	void HNRoomLogic::H_R_M_UserList(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if(ASS_GR_ONLINE_USER == messageHead->bAssistantID || ASS_GR_NETCUT_USER == messageHead->bAssistantID)
		{
			UINT userCount = objectSize / sizeof(UserInfoStruct);
			UserInfoStruct * pUserInfo = (UserInfoStruct *)object;
			while (userCount-- > 0)
			{
				UserInfoModule()->updateUser(pUserInfo++);
			}
		}
		else if(ASS_GR_DESK_STATION == messageHead->bAssistantID)
		{
			CHECK_SOCKET_DATA(MSG_GR_DeskStation, objectSize, "size error.");

			MSG_GR_DeskStation * pDeskStation = (MSG_GR_DeskStation *)object;
			deskStation = *pDeskStation;
		}
		else
		{

		}
	}

	void HNRoomLogic::H_R_M_UserAction(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		// 加入排队机
		if (ASS_GR_JOIN_QUEUE == messageHead->bAssistantID)
		{
			H_R_M_UserJoinQueue(messageHead, object, objectSize);
		}

		// 排队坐下
		if(ASS_GR_QUEUE_USER_SIT == messageHead->bAssistantID)
		{
			H_R_M_QueueUserSit(messageHead, object, objectSize);
		}

		// 用户进入房间
		else if(ASS_GR_USER_COME == messageHead->bAssistantID)
		{
			H_R_M_UserCome(messageHead, object, objectSize);
		}

		// 用户离开房间
		else if(ASS_GR_USER_LEFT == messageHead->bAssistantID)
		{
			H_R_M_UserLeft(messageHead, object, objectSize);
		}

		// 用户断线
		else if(ASS_GR_USER_CUT == messageHead->bAssistantID)
		{
			H_R_M_UserCut(messageHead, object, objectSize);
		}

		// 用户站起（包含旁观站起）
		else if(ASS_GR_USER_UP == messageHead->bAssistantID || ASS_GR_WATCH_UP == messageHead->bAssistantID)
		{
			H_R_M_UserUp(messageHead, object, objectSize);
		}

		// 坐下错误
		else if(ASS_GR_SIT_ERROR == messageHead->bAssistantID)
		{
			H_R_M_SitError(messageHead, object, objectSize);
		}

		// 用户坐下
		else if(ASS_GR_USER_SIT == messageHead->bAssistantID
			|| ASS_GR_WATCH_SIT == messageHead->bAssistantID
			|| ASS_GR_FAST_JOIN_IN == messageHead->bAssistantID)
		{
			H_R_M_UserSit(messageHead, object, objectSize);
		}

		// 获取坐下用户信息
		else if (ASS_GR_GET_USER_INFO == messageHead->bAssistantID)
		{
			H_R_M_SitUserInfo(messageHead, object, objectSize);
		}
	}

	void HNRoomLogic::H_R_M_UserJoinQueue(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (1 == messageHead->bHandleCode)
		{
			// 分发游戏消息
			dispatchGameMessage([=](IGameMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserJoinQueue(GBKToUtf8("用户金币不足"));
				return false;
			});

			// 分发房间消息
			dispatchFrameMessage([=](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserJoinQueue(GBKToUtf8("用户金币不足"));
				return false;
			});
		}		
	}

	void HNRoomLogic::H_R_M_QueueUserSit(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		// 桌子号
		BYTE deskNo = messageHead->bHandleCode;

		std::vector<MSG_GR_Queue_UserSit *> queueUsers;

		UINT count = objectSize / sizeof(MSG_GR_Queue_UserSit);
		MSG_GR_Queue_UserSit * tmp = (MSG_GR_Queue_UserSit *)object;

		while (count-- > 0)
		{
			const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(tmp->dwUserID);

			if (nullptr != pUserInfo)
			{
				UserInfoStruct userInfo = *pUserInfo;
				userInfo.bDeskNO = deskNo;
				userInfo.bDeskStation = tmp->bDeskStation;
				userInfo.bUserState = USER_ARGEE;
				UserInfoModule()->updateUser(&userInfo);
			}
			queueUsers.push_back(tmp++);
		}

		bool isFind = false;

		for (auto user : queueUsers)
		{
			if (user->dwUserID == PlatformLogic()->loginResult.dwUserID)
			{
				const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(user->dwUserID);
				isFind = true;
				RoomLogic()->loginResult.pUserInfoStruct.bDeskNO = deskNo;
				RoomLogic()->loginResult.pUserInfoStruct.bDeskStation = pUserInfo->bDeskStation;
				break;
			}
		}

		// 此轮坐下的玩家中没有自己则不分发消息
		if (!isFind) return;

		// 分发游戏消息
		dispatchGameMessage([&deskNo, &queueUsers](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_QueueUserSit(deskNo, queueUsers);
			return false;
		});

		// 分发房间消息
		dispatchFrameMessage([&deskNo, &queueUsers](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_QueueUserSit(deskNo, queueUsers);
			return false;
		});
	}

	void HNRoomLogic::H_R_M_UserCome(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_R_UserCome, objectSize, "size error.");

		MSG_GR_R_UserCome* pUserCome = (MSG_GR_R_UserCome*)object;

		UserInfoModule()->updateUser(&pUserCome->pUserInfoStruct);
		
		// 分发房间消息
		dispatchFrameMessage([&pUserCome](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserCome(&pUserCome->pUserInfoStruct);
			return false;
		});	
	}


	void HNRoomLogic::H_R_M_UserLeft(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_R_UserLeft, objectSize, "size error.");

		MSG_GR_R_UserLeft * pUserLeft = (MSG_GR_R_UserLeft*)object;

		const UserInfoStruct* userInfo = UserInfoModule()->findUser(pUserLeft->dwUserID);

		if (userInfo == nullptr)
		{
			return;
		}

		// 分发房间消息
		dispatchFrameMessage([&userInfo](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserLeft(userInfo);
			return false;
		});
		UserInfoModule()->removeUser(pUserLeft->dwUserID);
	}

	void HNRoomLogic::H_R_M_UserCut(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CHECK_SOCKET_DATA(MSG_GR_R_UserCut, objectSize, "size error.");

		MSG_GR_R_UserCut* pUserCut = (MSG_GR_R_UserCut*)object;

		const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(pUserCut->dwUserID);
		if (nullptr != pUserInfo)
		{
			UserInfoStruct userInfo = *pUserInfo;
			//设置数据
			userInfo.bUserState = USER_CUT_GAME;
			UserInfoModule()->updateUser(&userInfo);
		}

		// 分发游戏消息
		dispatchGameMessage([&pUserCut, &pUserInfo](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserCut(pUserCut->dwUserID, pUserCut->bDeskNO, pUserCut->bDeskStation);
			return false;
		});

		// 分发房间消息
		dispatchFrameMessage([&pUserCut](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_UserCut(pUserCut->dwUserID, pUserCut->bDeskNO, pUserCut->bDeskStation);
			return false;
		});
	}


	void HNRoomLogic::H_R_M_UserUp(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (ERR_GR_SIT_SUCCESS == messageHead->bHandleCode)
		{
			// 数据校验
			CHECK_SOCKET_DATA(MSG_GR_R_UserUp, objectSize, "size error.");

			MSG_GR_R_UserUp* pUserUp = (MSG_GR_R_UserUp*)object;

			// 锁桌更新
			if(pUserUp->bDeskIndex != INVALID_DESKNO)
			{
				RoomLogic()->deskStation.bDeskLock[pUserUp->bDeskIndex] = pUserUp->bLock;
				RoomLogic()->deskStation.bUserCount[pUserUp->bDeskIndex]--;
			}
			
			// 更新自己信息
			if (pUserUp->dwUserID == loginResult.pUserInfoStruct.dwUserID)
			{
				loginResult.pUserInfoStruct.bDeskNO = INVALID_DESKNO;
				loginResult.pUserInfoStruct.bDeskStation = INVALID_DESKSTATION;
				loginResult.pUserInfoStruct.bUserState = pUserUp->bUserState;
			}

			const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(pUserUp->dwUserID);
			// 更新用户数据
			if (nullptr != pUserInfo)
			{
				UserInfoStruct userInfo = *pUserInfo;
				userInfo.bDeskNO        = INVALID_DESKNO;
				userInfo.bDeskStation   = INVALID_DESKSTATION;
				userInfo.bUserState     = USER_LOOK_STATE;
				UserInfoModule()->updateUser(&userInfo);
			}
			// 分发游戏消息
			dispatchGameMessage([&pUserUp, &pUserInfo](IGameMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserUp(pUserUp, pUserInfo);
				return false;
			});

			// 分发房间消息
			dispatchFrameMessage([&pUserUp, &pUserInfo](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserUp(pUserUp, pUserInfo);
				return false;
			});
		}
		else
		{
			std::string message = getSitErrorReason(messageHead->bHandleCode);
			// 分发游戏消息
			dispatchGameMessage([&message](IGameMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_SitError(message);
				return false;
			});

			// 分发房间消息
			dispatchFrameMessage([&message](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_SitError(message);
				return false;
			});
		}
	}

	void HNRoomLogic::H_R_M_SitError(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		std::string message = getSitErrorReason(messageHead->bHandleCode);
		// 分发游戏消息
		dispatchGameMessage([&message](IGameMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_SitError(message);
			return false;
		});	

		// 分发房间消息
		dispatchFrameMessage([&message](IRoomMessageDelegate* delegate) -> bool
		{
			delegate->I_R_M_SitError(message);
			return false;
		});
	}


	void HNRoomLogic::H_R_M_UserSit(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (ERR_GR_SIT_SUCCESS == messageHead->bHandleCode)
		{
			// 数据校验
			CHECK_SOCKET_DATA(MSG_GR_R_UserSit, objectSize, "MSG_GR_R_UserSit size error.");

			MSG_GR_R_UserSit * pUserSit = (MSG_GR_R_UserSit*)object;

			const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(pUserSit->dwUserID); //6.3获取玩家IP
			int a = pUserInfo->dwUserIP;

			// 锁桌更新
			if (pUserSit->bDeskIndex != INVALID_DESKNO)
			{
				if (pUserSit->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID
					&& pUserSit->bDeskIndex != RoomLogic()->loginResult.pUserInfoStruct.bDeskNO)
				{
					RoomLogic()->deskStation.bUserCount[pUserSit->bDeskIndex]++;
				}
				else
				{
					// 不是掉线重新坐下的就把桌子人数加1				
					if (!pUserInfo || pUserInfo->bDeskNO != pUserSit->bDeskIndex)
					{
						RoomLogic()->deskStation.bUserCount[pUserSit->bDeskIndex]++;
					}
				}
				
				RoomLogic()->deskStation.bDeskLock[pUserSit->bDeskIndex] = pUserSit->bLock;
			}

			// 更新自己信息
			if (pUserSit->dwUserID == loginResult.pUserInfoStruct.dwUserID)
			{
				loginResult.pUserInfoStruct.bDeskNO      = pUserSit->bDeskIndex;
				loginResult.pUserInfoStruct.bDeskStation = pUserSit->bDeskStation;
				loginResult.pUserInfoStruct.bUserState   = pUserSit->bUserState;
			}
			else
			{
				// 分发桌子人数改变消息
				dispatchFrameMessage([&pUserSit](IRoomMessageDelegate* delegate) -> bool
				{
					delegate->I_R_M_UpdateDeskUserCount(pUserSit->bDeskIndex);
					return false;
				});
			}

			// 更新用户数据
			if (nullptr != pUserInfo)
			{
				UserInfoStruct userInfo = *pUserInfo;
				userInfo.bDeskNO      = pUserSit->bDeskIndex;
				userInfo.bDeskStation = pUserSit->bDeskStation;
				userInfo.bUserState   = pUserSit->bUserState;
				UserInfoModule()->updateUser(&userInfo);
			}
			else
			{
				if (pUserSit->bDeskIndex == loginResult.pUserInfoStruct.bDeskNO)
				{
					MSG_GR_I_GetDeskUserInfo info;
					info.iUserID = pUserSit->dwUserID;

					// 玩家比自己先进房间，但是没有坐下，所以自己坐下之后派发的同桌玩家中没有此玩家
					sendData(MDM_GR_USER_ACTION, ASS_GR_GET_USER_INFO, &info, sizeof(MSG_GR_I_GetDeskUserInfo));
				}
			
				return;
			}

			// 分发游戏消息
			dispatchGameMessage([&pUserSit, &pUserInfo](IGameMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserSit(pUserSit, pUserInfo);
				return false;
			});

			// 分发房间消息
			dispatchFrameMessage([&pUserSit, &pUserInfo](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserSit(pUserSit, pUserInfo);
				return false;
			});
		}
	}

	// 更新坐下用户信息
	void HNRoomLogic::H_R_M_SitUserInfo(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (ERR_GR_GET_DESKUSER_SUCCESS == messageHead->bHandleCode)
		{
			// 数据校验
			CHECK_SOCKET_DATA(MSG_GR_O_GetDeskUserInfo, objectSize, "MSG_GR_O_GetDeskUserInfo size error.");

			MSG_GR_O_GetDeskUserInfo* pSitUser = (MSG_GR_O_GetDeskUserInfo*)object;

			UserInfoModule()->updateUser(&pSitUser->userInfo);

			if (pSitUser->userInfo.bDeskNO != loginResult.pUserInfoStruct.bDeskNO) return;

			// 分发游戏消息
			dispatchGameMessage([pSitUser](IGameMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserSit(&pSitUser->userSit, &pSitUser->userInfo);
				return false;
			});

			// 分发房间消息
			dispatchFrameMessage([pSitUser](IRoomMessageDelegate* delegate) -> bool
			{
				delegate->I_R_M_UserSit(&pSitUser->userSit, &pSitUser->userInfo);
				return false;
			});
		}

		if (ERR_GR_USER_ONT_SITTING == messageHead->bHandleCode
			|| ERR_GR_USER_IN_DIFF_DESK == messageHead->bHandleCode)
		{
			// 数据校验
			CHECK_SOCKET_DATA(MSG_GR_O_GetDeskUserInfo, objectSize, "MSG_GR_O_GetDeskUserInfo size error.");

			MSG_GR_O_GetDeskUserInfo* pSitUser = (MSG_GR_O_GetDeskUserInfo*)object;

			UserInfoModule()->updateUser(&pSitUser->userInfo);
		}
	}

	void HNRoomLogic::H_R_M_ReturnDeskInfo(const NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (ASS_GR_RETURNDESK == messageHead->bAssistantID)
		{
			// 数据校验
			CHECK_SOCKET_DATA(ReturnGameInfo, objectSize, "ReturnGameInfo size error.");
			ReturnGameInfo * pInfo = (ReturnGameInfo*)object;

			UserInfoStruct user;
			memset(&user, 0x0, sizeof(UserInfoStruct));
			user.bBoy = true;
			user.bDeskNO = pInfo->iDeskNo;
			user.bDeskStation = pInfo->iDeskStation;
			user.bLogoID = pInfo->iLogoID;
			user.bUserState = pInfo->iUserState == 1 ? USER_SITTING : USER_CUT_GAME;
			user.dwUserID = pInfo->iUserID;
			user.i64Money = pInfo->i64Score;
			strcpy(user.nickName, pInfo->nickname);
			strcpy(user.headUrl, pInfo->headUrl);
			UserInfoModule()->addUser(&user);
		}
	}

	std::string HNRoomLogic::getSitErrorReason(int handleCode)
	{
		std::string message;
		switch (handleCode)
		{
		case ERR_GR_BEGIN_GAME:
			message = "坐下此位置失败,游戏已经开始了!";
			break;
		case ERR_GR_ALREAD_USER:
			message = "坐下此位置失败,下次动作快一点喔!";
			break;
		case ERR_GR_PASS_ERROR:
			message = "游戏桌密码错误,请在游戏设置中重新设置您的携带密码!";
			break;
		case ERR_GR_IP_SAME:
			message = "同桌玩家不允许有相同 IP 地址的玩家一起进行游戏!";
			break;
		case ERR_GR_IP_SAME_3:
			message = "同桌玩家不允许有 IP 地址前３位相同的玩家一起进行游戏!";
			break;
		case ERR_GR_IP_SAME_4:
			message = "同桌玩家不允许有IP 地址前４位相同的玩家一起进行游戏!";
			break;
		case ERR_GR_CUT_HIGH:
			message = "同桌玩家认为您的逃跑率太高,不愿意和您游戏!";
			break;
		case ERR_GR_POINT_LOW:
			message = "本桌玩家设置的进入条件，您不符合本桌进入条件!";
			break;
		case ERR_GR_POINT_HIGH:	
			message = "本桌玩家设置的进入条件，您不符合本桌进入条件!";
			break;
		case ERR_GR_NO_FRIEND:
			message = "此桌有您不欢迎的玩家!";
			break;
		case ERR_GR_POINT_LIMIT:
			message = "金币不足！"; //StringUtils::format("此游戏桌需要至少 %u 的游戏积分, 您的积分不够, 不能游戏!", loginResult.uLessPoint);
			break;
		case ERR_GR_CAN_NOT_LEFT:
			message = "游戏中，不允许离开";
			break;
		case ERR_GR_NOT_FIX_STATION:
			message = "游戏已经开始，无法中途加入!";
			break;
		case ERR_GR_MATCH_FINISH:
			message = "您的比赛已经结束了,不能继续参加比赛!";
			break;
		case ERR_GR_MATCH_WAIT://比赛场排队中
			message = "比赛排队中";
			break;
		case ERR_GR_UNENABLE_WATCH:
			message = "不允许旁观游戏!";
			break;
		case ERR_GR_FAST_SIT:
			message = "快速坐下失败！";
			break;
		case ERR_GR_POSITION_FAIL:
			message = "未能获取位置信息！";
			break;
		case ERR_GR_MONEY_LIMIT:
			message = "金币不足！";
			break;
		case ERR_GR_DESK_FULL:
			message = "桌上人数已满！";
			break;
		case ERR_GR_VIP_PASS_ERROR:
			message = "当前房间已经解散！";
			break;
		case ERR_GR_JEWEL_LOMIL:
			message = "钻石不足！";
			break;
		default:
			message = "未知错误";
			break;
		}

		return GBKToUtf8(message.c_str());
	}

	bool HNRoomLogic::dispatchFrameMessage(const RoomFrameMessageFun& func)
	{
		auto iterA = _addRoomNotifyQueue->begin();
		while(iterA != _addRoomNotifyQueue->end())
		{
			auto next = iterA;
			next++;
			_roomNotifyQueue->addObserver(iterA->second);
			_addRoomNotifyQueue->removeObserver(iterA->second);
			iterA = next;
		}

		auto iterR = _removeRoomNotifyQueue->begin();
		while(iterR != _removeRoomNotifyQueue->end())
		{
			auto next = iterR;
			next++;
			_roomNotifyQueue->removeObserver(iterR->second);
			_removeRoomNotifyQueue->removeObserver(iterR->second);
			iterR = next;
		}

		for (auto iter = _roomNotifyQueue->begin(); iter != _roomNotifyQueue->end(); ++iter)
		{
			bool bRemoved = false;
			for(auto iterR = _removeRoomNotifyQueue->begin(); iterR != _removeRoomNotifyQueue->end(); iterR++)
			{
				if(iterR->second == iter->second)
				{
					bRemoved = true;
					break;
				}
			}
			if(!bRemoved)
			{
				func(iter->second);
			}				
		}
		return true;
	}

	bool HNRoomLogic::dispatchGameMessage(const GameMessageFun& func)
	{
		auto iterA = _addGameNotifyQueue->begin();
		while(iterA != _addGameNotifyQueue->end())
		{
			auto next = iterA;
			next++;
			_gameNotifyQueue->addObserver(iterA->second);
			_addGameNotifyQueue->removeObserver(iterA->second);
			iterA = next;
		}
		
		auto iterR = _removeGameNotifyQueue->begin();
		while(iterR != _removeGameNotifyQueue->end())
		{
			auto next = iterR;
			next++;
			_gameNotifyQueue->removeObserver(iterR->second);
			_removeGameNotifyQueue->removeObserver(iterR->second);
			iterR = next;
		}		

		for (auto iter = _gameNotifyQueue->begin(); iter != _gameNotifyQueue->end(); ++iter)
		{
			bool bRemoved = false;
			for(auto iterR = _removeGameNotifyQueue->begin(); iterR != _removeGameNotifyQueue->end(); iterR++)
			{
				if(iterR->second == iter->second)
				{
					bRemoved = true;
					break;
				}
			}
			if(!bRemoved)
			{
				func(iter->second);
			}				
		}
		return true;
	}

}
