﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_RoomNotifyDelegate_H__
#define __HN_RoomNotifyDelegate_H__

#include "HNBaseType.h"
#include "HNNetProtocol/HNProtocolExport.h"
#include <vector>

namespace HN
{
	class IRoomMessageDelegate : public IGameChartMessageDelegate
		, public IUserActionMessageDelegate
	{
	public:
		// 房间连接成功
		virtual void I_R_M_Connect(bool result) {}

		// 房间登录
		virtual void I_R_M_Login(bool success, UINT handleCode, const std::string& message) {}

		// 系统自动赠送
		virtual void I_R_M_SysGiveMoney(SysGiveMoney* give) {}

		// 房间登录完成
		virtual void I_R_M_LoginFinish() {}

		// 用户进入房间
		virtual void I_R_M_UserCome(UserInfoStruct* user) {}

		// 用户离开房间
		virtual void I_R_M_UserLeft(const UserInfoStruct* user) {}

		// 玩家信息变化消息
		virtual void I_R_M_UserInfoChange(const UserInfoStruct* data) {}

		// 结算消息
		virtual void I_R_M_GamePoint(void * object, INT objectSize) {}

		// 更新是否封桌状态
		virtual void I_R_M_UpdateVirtualDesk(BYTE deskNo, bool isVirtual) {}

		// 更新是否游戏中状态
		virtual void I_R_M_UpdateDeskState(BYTE deskNo, bool isPlaying) {}

		// 更新桌上人数
		virtual void I_R_M_UpdateDeskUserCount(BYTE deskNo) {}
	};
}

#endif	//__HN_RoomNotifyDelegate_H__

