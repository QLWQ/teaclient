/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNPlatformRegist_H__
#define __HNPlatformRegist_H__

#include "HNPlatformLogic/HNPlatformLogicBase.h"

namespace HN
{
	/*
	 * 注册接口
	 */
	class IHNPlatformRegist: public IHNPlatformLogicBase
	{
	public:
		virtual void onPlatformRegistCallback(bool success, ThirdLoginType registType, const std::string& message,
			const std::string&name, const std::string& pwd){}

		// 获取验证码结果
		virtual void onPlatformGetVerifyCode(bool success, const std::string& phone,
			const std::string& code, const std::string& message) {}
	};

	/*
	 * 注册逻辑
	 */
	class HNPlatformRegist: public HNPlatformLogicBase
	{
	public:
		// 注册请求
		bool requestRegist(ThirdLoginType type, const std::string &name = "", const std::string pwd = "",
			const std::string& phoneNum = "", const std::string& nickName = "", const std::string& uionID = "", const std::string& headUrl = "", bool bBoy = true);

		// 获取验证码
		void getVerifyCode(std::string phoneNum);

		// 平台连接
		virtual void I_P_M_Connect(bool result) override;
		
		// 平台注册
		virtual void I_P_M_Regist(MSG_GP_S_Register* registerStruct, UINT ErrorCode) override;
		
		// 构造函数
		HNPlatformRegist(IHNPlatformRegist* callback);

		// 析构函数
		virtual ~HNPlatformRegist();

	private:
		bool verifyCodeSelector(HNSocketMessage* socketMessage);
		void creatHttprequest();
	protected:
		// 注册类型
		ThirdLoginType _registType = Guest;

		// 是否获取验证码
		bool _getVerifyCode = false;

		// 注册用户名
		std::string _name = "";

		// 注册密码
		std::string _pwd = "";

		// 电话号码
		std::string _phone = "";

		// 头像url
		std::string _headUrl = "";

		// 昵称
		std::string _nickName = "";

		// 微信，QQ登陆验证码
		std::string _uionID = "";

		// 性别
		bool		_bBoy = true;

		// 回调
		IHNPlatformRegist* _callback = nullptr;

		// 注册逻辑
		void platformRegist();

		// 注册返回码
		enum REGIST_CODE
		{
			eFAILE			= 0, // 注册失败
			eSUCCESS		= 1, // 注册成功
			eEXIST			= 2, // 相同名称
			eSENSITIVE		= 3, // 敏感词汇
			eALLNUMLIMIT	= 4, // 注册总限制
			eDAYNUMLIMIT	= 5, // 注册单日限制
		};
	};
}


#endif // !__HNPlatformRegist_H__
