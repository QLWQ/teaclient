/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNPlatformLogicBase_H__
#define __HNPlatformLogicBase_H__

#include "HNLogic/HNLogicBase.h"
#include "HNNetExport.h"

namespace HN
{
	enum ThirdLoginType
	{
		Guest = 0,	//游客
		Account,	//账号
		WeChat,		//微信
		QQLogin,	//QQ
	};

	/*
	 * common interface for platform logic.
	 */
	class IHNPlatformLogicBase 
	{
	public:
		virtual void onPlatformDisConnectCallback(const std::string& message){}
		virtual void onPlatformNewsCallback(const std::string& message){}
		virtual void onPlatformHornCallback(MSG_GP_S_SPEAKER_SEND_RES* pData){}

	//游戏列表
	public:
		virtual void onPlatformGameListCallback(bool success, const std::string& message){}
		virtual void onPlatformGameUserCountCallback(UINT Id, UINT count){}

	//房间列表
	public:
		virtual void onPlatformRoomListCallback(bool success, const std::string& message){}
		//virtual void onPlatformRoomUserCountCallback(UINT roomID, UINT userCount){}
		virtual void onRoomSitCallback(bool success, const std::string& message, INT userID, BYTE deskNo, BYTE seatNo){}
	};

	/*
	 * platform logic base.
	 */
	class HNPlatformLogicBase: public HNLogicBase, public IPlatformMessageDelegate
	{
	public:
		virtual void start() override;
		virtual void stop() override;
		HNPlatformLogicBase(IHNPlatformLogicBase* callback);
		virtual ~HNPlatformLogicBase();
	public:
		// 网络断开
		virtual void I_P_M_DisConnect() override;

		// 新闻系统消息
		virtual void I_P_M_NewsSys(MSG_GR_RS_NormalTalk* pData) override;

		// 大厅喇叭消息
		virtual void I_P_M_PlatformHorn(MSG_GP_S_SPEAKER_SEND_RES* pData) override;

	public:
		void requestRoomList();
		// 房间列表
		virtual void I_P_M_RoomList() override;
		// 房间人数
		virtual void I_P_M_RoomUserCount(UINT roomID, UINT peopleCount, UINT virtualCount) override;

	public:
		void requestGameList();
		// 游戏列表
		virtual void I_P_M_GameList() override;
		// 游戏列表人数更新
		virtual void I_P_M_GameUserCount(DL_GP_RoomListPeoCountStruct* userCount) override;

	protected:
		IHNPlatformLogicBase* _callback;
	};
}


#endif