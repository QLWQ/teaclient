/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformLogicBase.h"
#include "HNGameCreator.h"

namespace HN
{
	void HNPlatformLogicBase::start()
	{
		PlatformLogic()->addObserver(this);
	}

	void HNPlatformLogicBase::stop()
	{
		PlatformLogic()->removeObserver(this);
	}

	HNPlatformLogicBase::HNPlatformLogicBase(IHNPlatformLogicBase* callback)
	{
		CCAssert(callback != nullptr, "callback is null.");
		_callback = callback;
	}

	HNPlatformLogicBase::~HNPlatformLogicBase()
	{

	}

	void HNPlatformLogicBase::I_P_M_DisConnect()
	{
		if (PlatformConfig::getInstance()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER) return;
		__NotificationCenter::getInstance()->postNotification(DISCONNECT);
	}

	void HNPlatformLogicBase::I_P_M_NewsSys(MSG_GR_RS_NormalTalk* pData)
	{
		_callback->onPlatformNewsCallback(pData->szMessage);
	}

	void HNPlatformLogicBase::I_P_M_PlatformHorn(MSG_GP_S_SPEAKER_SEND_RES* pData)
	{
		_callback->onPlatformHornCallback(pData);
	}

	void HNPlatformLogicBase::requestRoomList()
	{
		UINT gameNameID = GameCreator()->getCurrentGameNameID();
		UINT gameKindID = GameCreator()->getCurrentGameKindID();

		if (0 == gameNameID || -1 == gameKindID)
		{
			_callback->onPlatformRoomListCallback(false, GBKToUtf8("游戏id或kindid错误"));
			return;
		}

		PlatformLogic()->getRoomList(gameKindID, gameNameID);
	}
	void HNPlatformLogicBase::I_P_M_RoomList()
	{
		_callback->onPlatformRoomListCallback(true, GBKToUtf8("获取成功"));
	}

	void HNPlatformLogicBase::I_P_M_RoomUserCount(UINT roomID, UINT peopleCount, UINT virtualCount)
	{
		//_callback->onPlatformRoomUserCountCallback(roomID, peopleCount + virtualCount);
	}

	void HNPlatformLogicBase::requestGameList()
	{
		PlatformLogic()->sendData(MDM_GP_LIST, ASS_GP_LIST_KIND);
	}
	void HNPlatformLogicBase::I_P_M_GameList()
	{
		_callback->onPlatformGameListCallback(true, GBKToUtf8("获取游戏列表成功"));
	}
	void HNPlatformLogicBase::I_P_M_GameUserCount(DL_GP_RoomListPeoCountStruct* userCount)
	{
		UINT count = userCount->uOnLineCount + userCount->uVirtualUser;
		_callback->onPlatformGameUserCountCallback(userCount->uID, count);
	}
	
}