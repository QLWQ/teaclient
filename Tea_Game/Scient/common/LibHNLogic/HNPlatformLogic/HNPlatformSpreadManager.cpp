/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformSpreadManager.h"


namespace HN
{
	HNSpreadManager* HNSpreadManager::_spreadManager = nullptr;

	HNSpreadManager* HNSpreadManager::getInstance()
	{
		if (nullptr == _spreadManager)
		{
			_spreadManager = new HNSpreadManager();
		}
		return _spreadManager;
	}

	HNSpreadManager::HNSpreadManager()
	{
	}

	HNSpreadManager::~HNSpreadManager()
	{
	}
}
