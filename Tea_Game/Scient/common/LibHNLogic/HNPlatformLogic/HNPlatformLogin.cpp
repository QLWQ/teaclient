/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformLogin.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "HNOperator.h"

USING_NS_CC;

namespace HN
{
	static const std::string Word_Wrong_Name	= GBKToUtf8("登陆名字错误"); 
	static const std::string Word_Wrong_Pass	= GBKToUtf8("用户密码错误"); 
	static const std::string Word_User_Validata   = GBKToUtf8("账户被禁用，请联系客服");
	static const std::string Word_IP_Limited = GBKToUtf8("该登录 IP 已被禁用");
	static const std::string Word_User_Exist = GBKToUtf8("用户昵称已经存在");
	static const std::string Word_Logined		= GBKToUtf8("帐号已经登录");
	static const std::string Word_Visiter_Lock	= GBKToUtf8("游客账号设备错误");
	static const std::string Word_LoginTypeErr  = GBKToUtf8("登录类型错误");
	static const std::string Word_Failed		= GBKToUtf8("登录失败");
	static const std::string Word_Maintain		= GBKToUtf8("系统维护中,暂时无法登陆");

	bool HNPlatformLogin::requestLogin(const std::string& name, const std::string& pwd, ThirdLoginType loginType, bool forced/* = false*/)
	{
		if(name.empty() || pwd.empty())
		{
			_callback->onPlatformLoginCallback(false, GBKToUtf8("名称或者密码为空"), name, pwd);
			return false;
		}
		_name = name;
		_pwd  = pwd;
		_forced = forced;
		_loginType = loginType;

		platformLogin();

		return true;
	}

	void HNPlatformLogin::I_P_M_Connect(bool result)
	{
		if(!result)
		{
			_callback->onPlatformLoginCallback(false, GBKToUtf8("平台网络连接失败"), _name, _pwd);
			return;
		}
		platformLogin();
	}

	void HNPlatformLogin::I_P_M_Login(bool result, UINT dwErrorCode, std::string& Errormessage)
	{
		std::string Errormessagetemp = GBKToUtf8(Errormessage);
		if(!result)
		{
			char message[256] = {0};
			switch (dwErrorCode)
			{
			case ERR_GP_USER_NO_FIND:
				sprintf(message, "%s(%u)", Word_Wrong_Name.c_str(), dwErrorCode);
				break;
			case ERR_GP_USER_PASS_ERROR:
				sprintf(message, "%s(%u)", Word_Wrong_Pass.c_str(), dwErrorCode);
				break;
			case ERR_GP_USER_VALIDATA:
				sprintf(message, "%s(%u)", Word_User_Validata.c_str(), dwErrorCode);
				break;
			case ERR_GP_USER_IP_LIMITED:
				sprintf(message, "%s(%u)", Word_IP_Limited.c_str(), dwErrorCode);
				break;
			case ERR_GP_USER_EXIST:
				sprintf(message, "%s(%u)", Word_User_Exist.c_str(), dwErrorCode);
				break;
			case ERR_GP_USER_LOGON:
				sprintf(message, "%s(%u)", Word_Logined.c_str(), dwErrorCode);
				break;
			case ERR_GP_LOGINTYPE_ERROR:
				sprintf(message, "%s(%u)", Word_LoginTypeErr.c_str(), dwErrorCode);
				break;
			case ERR_GP_VISITER_LOCKED:
				sprintf(message, "%s(%u)", Word_Visiter_Lock.c_str(), dwErrorCode);
				break;
			case ERR_GP_LOGINTYPE_MAINTAIN:
				sprintf(message, "%s", Errormessagetemp.c_str());
				break;
			default:
				sprintf(message, "%s(%u)", Word_Failed.c_str(), dwErrorCode);
				break;
			}
			_callback->onPlatformLoginCallback(false, message, _name, _pwd);
			return;
		}
		_callback->onPlatformLoginCallback(true, GBKToUtf8("登录成功"), _name, _pwd);
	}

	HNPlatformLogin::HNPlatformLogin(IHNPlatformLogin* callback)
		: HNPlatformLogicBase(callback)
	{
		CCAssert(callback != nullptr, "callback is null.");
		_callback = callback;
	}

	HNPlatformLogin::~HNPlatformLogin()
	{
		_callback = nullptr;
	}

	void HNPlatformLogin::platformLogin()
	{
		if(PlatformLogic()->isConnect())
		{
			std::string onlyString = Operator::requestChannel("sysmodule", "getSerialNumber");
			PlatformLogic()->login(_name, _pwd, onlyString, _loginType, _forced);
		}
		else
		{
			PlatformLogic()->connect();
		}
	}
}

