/****************************************************************************
Copyright (c) 2014-2016 Beijing TianRuiDiAn Network Technology Co.,Ltd.
Copyright (c) 2014-2016 ShenZhen Redbird Network Polytron Technologies Inc.

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformLogic/HNPlatformGameList.h"

namespace HN
{
	void HNPlatformGameList::requestGameList()
	{
		platformRequestGameList();
	}

	void HNPlatformGameList::I_P_M_GameList()
	{
		_callback->onPlatformGameListCallback(true, GBKToUtf8("获取游戏列表成功"));
	}

	void HNPlatformGameList::I_P_M_GameUserCount(DL_GP_RoomListPeoCountStruct* userCount)
	{
		UINT count = userCount->uOnLineCount + userCount->uVirtualUser;
		_callback->onPlatformGameUserCountCallback(userCount->uID, count);
	}

	HNPlatformGameList::HNPlatformGameList(IHNPlatformGameList* callback)
		: HNPlatformLogicBase(callback)
	{
		CCAssert(callback != nullptr, "callback is null.");
		_callback = callback;
	}

	HNPlatformGameList::~HNPlatformGameList()
	{

	}

	void HNPlatformGameList::platformRequestGameList()
	{
		if (GamesInfoModule()->getGameNameCount() <= 0)
			PlatformLogic()->sendData(MDM_GP_LIST, ASS_GP_LIST_KIND);
		else
			_callback->onPlatformGameListCallback(true, GBKToUtf8("获取游戏列表成功"));
	}
}