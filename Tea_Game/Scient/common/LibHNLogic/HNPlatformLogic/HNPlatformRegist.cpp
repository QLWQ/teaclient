/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPlatformRegist.h"
#include "HNMarketExport.h"

namespace HN
{	
	bool HNPlatformRegist::requestRegist(ThirdLoginType type, const std::string &name /*= ""*/,
		const std::string pwd /*= ""*/,const std::string& phoneNum /*= ""*/, const std::string& nickName /*= ""*/, 
		const std::string& uionID /*= ""*/, const std::string& headUrl /*= ""*/, bool bBoy /*= true*/)
	{
		if (type == ThirdLoginType::Account)
		{
			if (name.empty() || pwd.empty())
			{
				_callback->onPlatformRegistCallback(false, type, GBKToUtf8("名称或密码为空"), name, pwd);
				return false;
			}
		}

		_registType = type;
		_name = name;
		_pwd = pwd;
		_phone = phoneNum;
		_nickName = nickName;
		_uionID = uionID;
		_bBoy = bBoy;
		_headUrl = headUrl;
		platformRegist();
		return true;
	}
	
	void HNPlatformRegist::I_P_M_Connect(bool result)
	{
		if(!result)
		{
			if (_getVerifyCode)
			{

				_callback->onPlatformGetVerifyCode(false, "", "", GBKToUtf8("平台网络连接失败"));
			}
			else
			{
				_callback->onPlatformRegistCallback(false, _registType, GBKToUtf8("平台网络连接失败"), _name, _pwd);
			}
			
			return;
		}

		if (_getVerifyCode)
		{
			getVerifyCode(_phone);
		}
		else
		{
			platformRegist();
		}
	}
	
	void HNPlatformRegist::I_P_M_Regist(MSG_GP_S_Register* registerStruct, UINT ErrorCode)
	{	
		_name = std::string(registerStruct->szName);
		_pwd = std::string(registerStruct->szPswd);

		std::string msg;

		switch (ErrorCode)
		{
		case eSUCCESS:
			msg = GBKToUtf8("注册成功");
			if (_uionID != "")  creatHttprequest();
			break;
		case eFAILE:
			msg = StringUtils::format(GBKToUtf8("注册失败(%u)"), ErrorCode);
			break;
		case eEXIST:
			msg = StringUtils::format(GBKToUtf8("用户名已存在(%u)"), ErrorCode);
			break;
		case eSENSITIVE:
			msg = StringUtils::format(GBKToUtf8("包含敏感词汇(%u)"), ErrorCode);
			break;
		case eALLNUMLIMIT:
			msg = StringUtils::format(GBKToUtf8("超出注册总限制(%u)"), ErrorCode);
			break;
		case eDAYNUMLIMIT:
			msg = StringUtils::format(GBKToUtf8("超出注册单日限制(%u)"), ErrorCode);
			break;
		case 6 :
			msg = StringUtils::format(GBKToUtf8("微信登录老用户(%u)"), ErrorCode);
			break;
		default:
			msg = StringUtils::format(GBKToUtf8("注册失败(%u)"), ErrorCode);
			break;
		}

		_callback->onPlatformRegistCallback((ErrorCode == eSUCCESS || ErrorCode == 6), _registType, msg, _name, _pwd);
	}

	HNPlatformRegist::HNPlatformRegist(IHNPlatformRegist* callback)
		: HNPlatformLogicBase(callback)
	{
		CCAssert(callback != nullptr, "callback is null.");
		_callback = callback;
	}

	HNPlatformRegist::~HNPlatformRegist()
	{
		_callback = nullptr;
		PlatformLogic()->removeEventSelector(MDM_GP_SMS, ASS_GP_SMS_VCODE);
	}

	void HNPlatformRegist::platformRegist()
	{
		if (PlatformLogic()->isConnect())
		{
			// 0-快速注册，1-普通注册，2-微信登陆，3-QQ登陆
			std::string onlyString = Operator::requestChannel("sysmodule", "getSerialNumber");
			if (_registType == 0)
			{
				PlatformLogic()->regist(_registType, onlyString);
			}
			else if (_registType == 1)
			{
				PlatformLogic()->regist(_registType, onlyString, _bBoy, _uionID, _nickName, _name, _pwd);
			}
			else if (_registType == 2 || _registType == 3)
			{
				PlatformLogic()->regist(_registType, onlyString, _bBoy, _uionID, _nickName, _name, _pwd, _phone, _headUrl);
			}
			else
			{
			}
		}
		else
		{
			PlatformLogic()->connect();
		}
	}

	void HNPlatformRegist::getVerifyCode(std::string phoneNum)
	{
		_phone = phoneNum;

		_getVerifyCode = true;

		if (PlatformLogic()->isConnect())
		{
			MSG_GP_SmsVCode SmsVCode;
			SmsVCode.nType = 4;
			strcpy(SmsVCode.szName, PlatformLogic()->loginResult.szName);
			strcpy(SmsVCode.szMobileNo, phoneNum.c_str());
			PlatformLogic()->sendData(MDM_GP_SMS, ASS_GP_SMS_VCODE, &SmsVCode, sizeof(SmsVCode),
				HN_SOCKET_CALLBACK(HNPlatformRegist::verifyCodeSelector, this));
		}
		else
		{
			PlatformLogic()->connect();
		}
	}

	bool HNPlatformRegist::verifyCodeSelector(HNSocketMessage* socketMessage)
	{
		CCAssert(sizeof (MSG_GP_SmsVCode) == socketMessage->objectSize, "MSG_GP_SmsVCode is error.");
		if (socketMessage->objectSize != sizeof(MSG_GP_SmsVCode)) return true;

		MSG_GP_SmsVCode *smsVCode = (MSG_GP_SmsVCode *)socketMessage->object;

		_callback->onPlatformGetVerifyCode(true, smsVCode->szMobileNo, smsVCode->szVCode, GBKToUtf8("获取验证码成功"));
		return true;
	}
	void HNPlatformRegist::creatHttprequest()
	{
//		std::string url = StringUtils::format("http://alipay.qp888m.com/app/set_branch.php?unionid=%s&pfid=%s", _uionID.c_str(), "20002");
// 		network::HttpRequest* request = new (std::nothrow) network::HttpRequest();
// 		request->setUrl(url.c_str());
// 		request->setRequestType(network::HttpRequest::Type::POST);
// 		network::HttpClient::getInstance()->send(request);
// 		request->release();
	}
}