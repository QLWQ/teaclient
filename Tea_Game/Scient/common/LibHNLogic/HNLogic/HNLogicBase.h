/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNLogicBase_H__
#define __HNLogicBase_H__

#include "HNNetExport.h"

namespace HN
{
	/*
	 * redbird logic base.
	 */
	class HNLogicBase
	{
	public:
		virtual void start();
		virtual void stop();
	protected:
		HNLogicBase();
		virtual ~HNLogicBase();
	};
}

#endif