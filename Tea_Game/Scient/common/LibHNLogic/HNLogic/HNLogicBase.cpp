/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNLogicBase.h"

namespace HN
{
	void HNLogicBase::start()
	{

	}

	void HNLogicBase::stop()
	{

	}

	HNLogicBase::HNLogicBase()
	{

	}

	HNLogicBase::~HNLogicBase()
	{
		stop();
	}

}