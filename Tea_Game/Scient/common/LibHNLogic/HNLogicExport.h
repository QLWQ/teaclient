﻿/****************************************************************************
Copyright (c) 2014-2016 Beijing TianRuiDiAn Network Technology Co.,Ltd.
Copyright (c) 2014-2016 ShenZhen Redbird Network Polytron Technologies Inc.

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNLogicExport_H__
#define __HN_HNLogicExport_H__

#include "HNPlatformLogic/HNPlatformLogin.h"
#include "HNPlatformLogic/HNPlatformRegist.h"
#include "HNPlatformLogic/HNGameCreator.h"
#include "HNRoomLogic/HNRoomLogicBase.h"
#include "HNRoomLogic/HNGameLogicBase.h"
#include "HNRoomLogic/HNRoomDeskLogic.h"
#include "HNRoomLogic/HNVipRoomLogic.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "HNPCStartLogic/HNPCStartLogic.h"
#endif

using namespace HN;

#endif	//__HN_HNLogicExport_H__