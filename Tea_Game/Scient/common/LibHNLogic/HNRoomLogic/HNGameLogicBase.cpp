/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNGameLogicBase.h"
#include "HNNetExport.h"

namespace HN
{
	void HNGameLogicBase::start()
	{
		RoomLogic()->addGameObserver(this);
	}

	void HNGameLogicBase::stop()
	{
		RoomLogic()->removeGameObserver(this);
	}

	HNGameLogicBase::HNGameLogicBase(BYTE deskNo, INT maxPlayers, bool autoCreate, IHNGameLogicBase* callback)
		: _deskNo(deskNo)
		, _maxPlayers(maxPlayers)
		, _autoCreate(autoCreate)
		, _mySeatNo(INVALID_DESKSTATION)
		, _callback(callback)
	{
		CCAssert(callback != nullptr, "callback is null.");

		initParams();

		_deskUserList = new HNGameTableUsersData(deskNo);
		_lookerList = new HNGameTableUsersData(deskNo);
		start();

		HNPlatformConfig()->setMasterID(INVALID_USER_ID);

		HNPlatformConfig()->setSceneState(PlatformConfig::SCENE_STATE::INGAME);

		loadDeskUsers();
	}

	HNGameLogicBase::~HNGameLogicBase()
	{
		stop();
		_user_id.clear();
		_user_seatNo.clear();
		_deskUserList->clear();
		_lookerList->clear();
		HN_SAFE_DELETE(_deskUserList);
		HN_SAFE_DELETE(_lookerList);
	}

	//////////////////////////////////////////////////////////////////////////
	void HNGameLogicBase::I_R_M_UserAgree(MSG_GR_R_UserAgree* agree)
	{
		if (agree->bDeskNO != _deskNo) return;

		if (_isPlaying) return;

		dealUserAgreeResp(agree);		
	}

	void HNGameLogicBase::I_R_M_GameBegin(BYTE bDeskNO)
	{
		if (bDeskNO != _deskNo) return;

		_isPlaying = true;

		dealGameStartResp(bDeskNO);	
	}

	void HNGameLogicBase::I_R_M_GameClean()
	{
		//if (_isPlaying) return;

		dealGameClean();
	}

	void HNGameLogicBase::I_R_M_GameRoundNum(MSG_GR_ConTestRoundNum* contestRoundNum)
	{
		dealMatchRoundNum(contestRoundNum);
	}

	void HNGameLogicBase::I_R_M_GameInnings(MSG_GR_ConTestRoundNumInOneRound* contestInnings)
	{
		dealMatchInnings(contestInnings);
	}

	void HNGameLogicBase::I_R_M_GameInfoupdate(MSG_GM_S_ConTestGameInfo* contestInfodata)
	{
		dealMatchInfoupdate(contestInfodata);
	}

	void HNGameLogicBase::I_R_M_GameEnd(BYTE bDeskNO)
	{
		if (bDeskNO != _deskNo) return;

		_isPlaying = false;

		dealGameEndResp(bDeskNO);

		if ((RoomLogic()->getRoomRule() & GRR_CONTEST) || (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME))
		{
			_deskUserList->clear();
			_lookerList->clear();
		}
	}

	void HNGameLogicBase::I_R_M_SitError(const std::string& message)
	{
		dealUserSitOrUpError(message);
	}

	void HNGameLogicBase::I_R_M_UserSit(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		if (!userSit || !user) return;

		if (userSit->dwUserID == PlatformLogic()->loginResult.dwUserID)
		{
			refreshParams();
			
			_deskUserList->update(userSit->bDeskIndex);
			_lookerList->update(userSit->bDeskIndex, true);
			_deskNo = userSit->bDeskIndex;
			_isPlaying = false;
		}

		// 只派发同桌玩家坐下消息
		if ((userSit->bDeskIndex != _deskNo) || (user->bDeskNO != _deskNo)) return;

		if (userSit->bUserState != USER_WATCH_GAME && _maxPlayers > 0)
		{
			_existPlayer[userSit->bDeskStation] = true;
		}

		// 不管是正常坐下还是旁观坐下，都需要加载桌上玩家，获取gamestation恢复游戏场景
		if (userSit->dwUserID == PlatformLogic()->loginResult.dwUserID)
		{
			_mySeatNo = userSit->bDeskStation;

			// 游戏中收到自己坐下消息，表示是换桌或者断线重连，有可能更换座位号，所以需要重新更新桌子玩家信息
			loadDeskUsers();

			// 断线重连回来，会收到自己坐下的消息，这时候重新申请恢复场景
			sendGameInfo();
		}

		dealUserSitResp(userSit, user);
	}

	void HNGameLogicBase::I_R_M_UserJoinQueue(const std::string& message)
	{
		dealQueueUserSitMessage(false, message);
	}

	void HNGameLogicBase::I_R_M_QueueReady()
	{
		dealUserQueueReady();
	}

	// 排队用户坐下
	void HNGameLogicBase::I_R_M_QueueUserSit(BYTE deskNo, const std::vector<MSG_GR_Queue_UserSit*>& users)
	{
		BYTE mySeatNo = INVALID_DESKNO;

		for (auto iter = users.begin(); iter != users.end(); ++iter)
		{
			if((*iter)->dwUserID == PlatformLogic()->loginResult.dwUserID)
			{
				mySeatNo = (*iter)->bDeskStation;
				break;
			}
		}

		if (INVALID_DESKNO == mySeatNo) return;

		refreshParams();

		_deskNo = deskNo;
		_deskUserList->update(_deskNo);
		_lookerList->update(_deskNo, true);

		loadDeskUsers();
		dealQueueUserSitMessage(true, GBKToUtf8("排队坐下成功"));

		// 比赛开赛消息使用消息中心通知出去，比赛控制中心接收显示开赛动画
		EventCustom event(RECONNECTION);
		Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
	}

	void HNGameLogicBase::I_R_M_UserUp(MSG_GR_R_UserUp * userUp, const UserInfoStruct* user)
	{
		if (!userUp || !user) return;
		if (userUp->bDeskIndex != _deskNo) return;

		if (userUp->dwUserID == PlatformLogic()->loginResult.dwUserID)
		{
			// 如果是处于重连状态不需要自己离开当前游戏
			if (HNPlatformConfig()->getIsReconnect()) return;

			_mySeatNo = INVALID_DESKSTATION;
			_deskNo = INVALID_DESKNO;
		}

		_deskUserList->update();
		_lookerList->update(_deskNo, true);

		if (userUp->bUserState != USER_WATCH_GAME && _maxPlayers > 0)
		{
			_existPlayer[userUp->bDeskStation] = false;
		}

		// 逻辑站起
		if (!userUp->bLeave)
		{
			// 如果逻辑站起的是自己，虽然自己不用离开桌子，但是需要把同桌的其他玩家都移除掉
			if (user->dwUserID == PlatformLogic()->loginResult.dwUserID)
			{
				_deskUserList->transform([=](UserInfoStruct* upUser, INT index) {

					if (user->dwUserID != PlatformLogic()->loginResult.dwUserID)
					{
						if (_maxPlayers > 0)
						{
							_existPlayer[upUser->bDeskStation] = false;
						}

						MSG_GR_R_UserSit sit;
						sit.dwUserID = upUser->dwUserID;
						sit.bDeskStation = upUser->bDeskStation;
						sit.bDeskIndex = upUser->bDeskNO;

						dealUserUpResp(&sit, upUser);

						// 有玩家站起，则移除掉界面可能存在的聊天气泡
						BYTE viewSeatNo = logicToViewSeatNo(upUser->bDeskStation);
						EventCustom event("dealUserUpResp");
						event.setUserData(&viewSeatNo);
						Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
					}
				});

				_lookerList->transform([=](UserInfoStruct* upUser, INT index) {

					if (user->dwUserID != PlatformLogic()->loginResult.dwUserID)
					{
						MSG_GR_R_UserSit sit;
						sit.dwUserID = upUser->dwUserID;
						sit.bDeskStation = upUser->bDeskStation;
						sit.bDeskIndex = upUser->bDeskNO;

						dealUserUpResp(&sit, upUser);
					}
				});

				return;
			}
		}

		dealUserUpResp(userUp, user);

		// 有玩家站起，则移除掉界面可能存在的聊天气泡
		BYTE viewSeatNo = logicToViewSeatNo(userUp->bDeskStation);
		EventCustom event("dealUserUpResp");
		event.setUserData(&viewSeatNo);
		Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
	}

	void HNGameLogicBase::I_R_M_GameInfo(MSG_GM_S_GameInfo* pGameInfo)
	{
		_gameStatus = pGameInfo->bGameStation;
		_waitTime   = pGameInfo->bWaitTime;
		_watchOther = pGameInfo->bWatchOther;

		dealGameInfoResp(pGameInfo);
	}

	void HNGameLogicBase::I_R_M_GameStation(void* object, INT objectSize)
	{
		dealGameStationResp(object, objectSize);
	}

	//设置回放模式
	void HNGameLogicBase::I_R_M_GamePlayBack(bool isback)
	{
		dealGamePlayBack(isback);
	}

	bool HNGameLogicBase::onGameMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		dealGameMessage(messageHead, object, objectSize);
		return true;
	}

	bool HNGameLogicBase::onVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		CCAssert(sizeof (VoiceInfo) == objectSize, "VoiceInfo size is error.");
		VoiceInfo* voiceInfo = (VoiceInfo*)object;
		auto userInfo = getUserByUserID(voiceInfo->uUserID);
		if (!userInfo || userInfo->bDeskNO != _deskNo) return true;
		dealVoiceMessage(messageHead, object, objectSize);
		return true;
	}

	void HNGameLogicBase::I_R_M_UserCut(INT dwUserID, BYTE bDeskNO, BYTE	bDeskStation)
	{
		if (bDeskNO != _deskNo) return;

		dealUserCutMessageResp(dwUserID, bDeskStation);
	}

	// 玩家信息变化消息
	void HNGameLogicBase::I_R_M_UserInfoChange(const UserInfoStruct* user)
	{
		if (user->bDeskNO != _deskNo) return;

		dealUserInfoChangeResp(user);
	}

	void HNGameLogicBase::I_R_M_UserNotEnoughMoney()
	{
		dealUserNotEnoughMoney();
	}

	//////////////////////////////////////////////////////////////////////////
	// 聊天消息
	void HNGameLogicBase::I_R_M_NormalTalk(void* object, INT objectSize)
	{
		MSG_GR_RS_NormalTalk* pData = (MSG_GR_RS_NormalTalk*)object;
		dealUserChatMessage(pData);
	}

	void HNGameLogicBase::I_R_M_NormalAction(void* object, INT objectSize)
	{
		DoNewProp* pData = (DoNewProp*)object;
		dealUserActionMessage(pData);
	}

	void HNGameLogicBase::I_R_M_GameActicity(void* object, INT objectSize)
	{
		MSG_GR_GameNum* pData = (MSG_GR_GameNum*)object;
		dealGameActivityResp(pData);
	}

	void HNGameLogicBase::I_R_M_GameReceive(void* object, INT objectSize)
	{
		MSG_GR_Set_JiangLi* pData = (MSG_GR_Set_JiangLi*)object;
		dealGameActivityReceive(pData);
	}

	void HNGameLogicBase::I_R_M_GameNumRound(void* object, INT objectSize)
	{
		MSG_GR_ConTestRoundNum* pData = (MSG_GR_ConTestRoundNum*)object;
		//dealGameNumRound(pData);
	}

	void HNGameLogicBase::I_R_M_GameInnings(void* object, INT objectSize)
	{
		MSG_GR_ConTestRoundNumInOneRound* pData = (MSG_GR_ConTestRoundNumInOneRound*)object;
		//dealGameInnings(pData);
	}

	//////////////////////////////////////////////////////////////////////////
	void HNGameLogicBase::dealUserAgreeResp(MSG_GR_R_UserAgree* agree)
	{
		HNLOG("dealUserAgreeResp");	
	}

	void HNGameLogicBase::dealGameStartResp(BYTE bDeskNO)
	{
		HNLOG("dealGameStartResp");
	}

	void HNGameLogicBase::dealGameClean()
	{
		HNLOG("dealGameClean");
	}

	void HNGameLogicBase::dealMatchRoundNum(MSG_GR_ConTestRoundNum* contestRoundNum)
	{
		HNLOG("dealMatchRoundNum");
	}

	void HNGameLogicBase::dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound* contestRoundNum)
	{
		HNLOG("dealMatchInnings");
	}

	void HNGameLogicBase::dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo* contestRoundNum)
	{
		HNLOG("dealMatchInfoupdate");
	}

	void HNGameLogicBase::dealGameEndResp(BYTE bDeskNO)
	{
		HNLOG("dealGameEndResp");
	}

	void HNGameLogicBase::dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		HNLOG("dealUserSitResp");
	}

	void HNGameLogicBase::dealQueueUserSitMessage(bool success, const std::string& message)
	{
		HNLOG("dealQueueUserSitMessage");
	}

	void HNGameLogicBase::dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		HNLOG("dealUserUpResp");
	}

	void HNGameLogicBase::dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo)
	{
		HNLOG("dealGameInfoResp");
	}

	void HNGameLogicBase::dealUserCutMessageResp(INT userId, BYTE seatNo)
	{
		HNLOG("dealUserCutMessageResp");
	}

	void HNGameLogicBase::dealUserInfoChangeResp(const UserInfoStruct* user)
	{
		HNLOG("dealUserInfoChangeResp");
	}

	void HNGameLogicBase::dealUserNotEnoughMoney()
	{
		HNLOG("dealUserNotEnoughMoney");
	}

	void HNGameLogicBase::dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk)
	{
		HNLOG("dealUserChatMessage");
	}

	void HNGameLogicBase::dealUserActionMessage(DoNewProp* normalAction)
	{
		HNLOG("dealUserActionMessage");
	}

	void HNGameLogicBase::dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		HNLOG("dealVoiceMessage");
	}

	void HNGameLogicBase::dealUserSitOrUpError(const std::string& message)
	{
		HNLOG("dealUserSitOrUpError");
	}

	void HNGameLogicBase::dealUserQueueReady()
	{
		HNLOG("dealUserQueueReady");
	}

	void HNGameLogicBase::dealGameActivityResp(MSG_GR_GameNum* pData)
	{
		HNLOG("dealGameActivityResp");
	}

	void HNGameLogicBase::dealGameActivityReceive(MSG_GR_Set_JiangLi* pData)
	{
		HNLOG("dealGameActivityReceive");
	}

	/*void HNGameLogicBase::dealGameNumRound(MSG_GR_ConTestRoundNum* pData)
	{
		HNLOG("dealGameNumRound");
	}

	void HNGameLogicBase::dealGameInnings(MSG_GR_ConTestRoundNumInOneRound* pData)
	{
		HNLOG("dealGameInnings");
	}*/

	void HNGameLogicBase::sendGameInfo()
	{
		HNLOG("sendGameInfo");
		MSG_GM_S_ClientInfo ClientInfo;
		ClientInfo.bEnableWatch = 0;
		RoomLogic()->sendData(MDM_GM_GAME_FRAME, ASS_GM_GAME_INFO, &ClientInfo, sizeof(ClientInfo));
	}

	void HNGameLogicBase::sendUserUp()
	{
		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_USER_UP);
	}

	void HNGameLogicBase::sendUserSit(BYTE lSeatNo)
	{
		if(INVALID_DESKSTATION == lSeatNo)
		{
			return;
		}

		UserInfoStruct* userInfo = _deskUserList->getUserByDeskStation(lSeatNo);
		if (nullptr != userInfo)
		{
			BYTE tmpSeatNo = lSeatNo;
			do 
			{
				tmpSeatNo = (tmpSeatNo + 1) % _maxPlayers;
				if (_deskUserList->getUserByDeskStation(tmpSeatNo) == nullptr)
				{
					break;
				}
			} while (tmpSeatNo != lSeatNo);
			if(tmpSeatNo == lSeatNo)
			{
				HNLOG("no empty sit.");
				return;
			}
			else
			{
				lSeatNo = tmpSeatNo;
			}
		}

		MSG_GR_S_UserSit data;
		data.bDeskIndex   = _deskNo;
		data.bDeskStation = lSeatNo;
		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_USER_SIT, &data, sizeof(data));
	}

	void HNGameLogicBase::sendWatchSit(BYTE lSeatNo)
	{

	}

	void HNGameLogicBase::sendFastWatch()
	{

	}

	void HNGameLogicBase::sendJoinQueue()
	{
		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_JOIN_QUEUE);
	}

	void HNGameLogicBase::sendForceQuit()
	{
		RoomLogic()->sendData(MDM_GM_GAME_FRAME, ASS_GM_FORCE_QUIT);
	}

	void HNGameLogicBase::sendAgreeGame()
	{
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_GM_AGREE_GAME);
	}

	void HNGameLogicBase::sendChatMsg(const std::string& msg)
	{
		MSG_GR_RS_NormalTalk data;
		data.dwTargetID = RoomLogic()->loginResult.pUserInfoStruct.dwUserID;
		data.dwSendID = RoomLogic()->loginResult.pUserInfoStruct.dwUserID;
		data.iLength = msg.size();
		sprintf(data.szMessage,"%s", Utf8ToGB(msg.c_str()));
		RoomLogic()->sendData(MDM_GM_GAME_FRAME, ASS_GM_NORMAL_TALK, (void*)&data, sizeof(MSG_GR_RS_NormalTalk)); //语音聊天6.3

	}

	void HNGameLogicBase::sendActionChatMsg(INT TargetID, int index)
	{
		DoNewProp data;
		data.dwDestID = TargetID;
		data.dwUserID = RoomLogic()->loginResult.pUserInfoStruct.dwUserID;
		data.iPropNum = index;
		RoomLogic()->sendData(MDM_GR_PROP, ASS_PROP_NEW_TEXIAO, (void*)&data, sizeof(DoNewProp)); //表情动作聊天6.3
	}

	void HNGameLogicBase::sendChangeDesk()
	{
		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_CHANGE_DESK);
	}

	//////////////////////////////////////////////////////////////////////////

	void HNGameLogicBase::loadDeskUsers()
	{
		BYTE seatNo = INVALID_DESKNO;
		for(int i = 0; i < _maxPlayers; i++)
		{
			UserInfoStruct* pUser = _deskUserList->getUserByDeskStation(i);
			if(nullptr != pUser)
			{
				if(pUser->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID)
				{
					_mySeatNo = i;
					_seatOffset = -_mySeatNo;
				}
				if(_maxPlayers > 0)
				{
					_existPlayer[i] = true;
				}

			}
		}
		if(-1 == _maxPlayers)
		{
			UserInfoStruct* pUser = _deskUserList->getUserByUserID(RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
			if(nullptr != pUser)
			{
				_mySeatNo = pUser->bDeskStation;
			}
		}
	}

	void HNGameLogicBase::clearDesk()
	{

	}

	void HNGameLogicBase::clearDeskUsers()
	{

	}


	//////////////////////////////////////////////////////////////////////////
	INT HNGameLogicBase::getUserId(BYTE lSeatNo)
	{
		if(INVALID_DESKSTATION == lSeatNo)
		{
			return INVALID_USER_ID;
		}

		UserInfoStruct* userInfo = _deskUserList->getUserByDeskStation(lSeatNo);
		return (nullptr != userInfo) ? userInfo->dwUserID : INVALID_USER_ID;
	}

	UserInfoStruct* HNGameLogicBase::getUserByUserID(INT userID)
	{
		auto pUser = _deskUserList->getUserByUserID(userID);
		if (pUser == nullptr)
			return nullptr;

		UserInfoStruct user = *pUser;
		if (userID != PlatformLogic()->loginResult.dwUserID)
		{
			// 防作弊不显示用户名称
			if (RoomLogic()->getSelectedRoom()->dwRoomRule & GRR_NOTCHEAT)
			{
				sprintf(user.szName, "******");
				sprintf(user.nickName, "玩家");
			}
		}

		// 比赛场显示比赛积分
		if (RoomLogic()->getSelectedRoom()->dwRoomRule & GRR_CONTEST
			|| RoomLogic()->getSelectedRoom()->dwRoomRule & GRR_TIMINGCONTEST)
		{
			user.i64Money = user.i64ContestScore;
		}

		auto it = _user_id.find(userID);

		if (it != _user_id.end())
		{
			it->second = user;
		}
		else
		{
			_user_id.insert(std::pair<UINT, UserInfoStruct>(userID, user));
		}

		return &_user_id.find(userID)->second;
	}

	UserInfoStruct* HNGameLogicBase::getUserBySeatNo(BYTE lSeatNo)
	{
		auto pUser = _deskUserList->getUserByDeskStation(lSeatNo);
		if (pUser == nullptr)
			return nullptr;

		UserInfoStruct user = *pUser;

		if (pUser->dwUserID != PlatformLogic()->loginResult.dwUserID)
		{
			// 防作弊不显示用户名称
			if (RoomLogic()->getSelectedRoom()->dwRoomRule & GRR_NOTCHEAT)
			{
				sprintf(user.szName, "******");
				sprintf(user.nickName, "玩家");
			}
		}

		// 比赛场显示比赛积分
		if (RoomLogic()->getSelectedRoom()->dwRoomRule & GRR_CONTEST
			|| RoomLogic()->getSelectedRoom()->dwRoomRule & GRR_TIMINGCONTEST)
		{
			user.i64Money = user.i64ContestScore;
		}

		auto it = _user_seatNo.find(lSeatNo);

		if (it != _user_seatNo.end())
		{
			it->second = user;
		}
		else
		{
			_user_seatNo.insert(std::pair<UINT, UserInfoStruct>(lSeatNo, user));
		}

		return &_user_seatNo.find(lSeatNo)->second;
	}

	UserInfoStruct* HNGameLogicBase::getLookerByUserID(INT userID)
	{
		return _lookerList->getUserByUserID(userID);
	}

	bool HNGameLogicBase::isLooker()
	{
		return RoomLogic()->loginResult.pUserInfoStruct.bUserState == USER_WATCH_GAME;
	}

	bool HNGameLogicBase::getUserIsBoy(BYTE lSeatNo)
	{
		UserInfoStruct* userInfo = _deskUserList->getUserByDeskStation(lSeatNo);
		return (nullptr != userInfo) ? userInfo->bBoy : true;
	}

	BYTE HNGameLogicBase::getMySeatNo() const
	{
		return _mySeatNo;
	}
	//////////////////////////////////////////////////////////////////////////

	void HNGameLogicBase::initParams()
	{
		_gameStatus = 0;
		_waitTime = 0;
		_seatOffset = 0;

		if(_maxPlayers > 0)
		{
			_existPlayer.resize(_maxPlayers, false);
			_playing.resize(_maxPlayers, false);
		}

		// 发送当前逻辑指针给回放控制器
		handlerVideo();
	}

	void HNGameLogicBase::refreshParams()
	{
		_gameStatus = 0;
		_waitTime = 0;
		_seatOffset = 0;

		if (_maxPlayers > 0)
		{
			_existPlayer.resize(_maxPlayers, false);
			_playing.resize(_maxPlayers, false);
		}
	}

	BYTE HNGameLogicBase::getNextUserSeatNo(BYTE lSeatNo)
	{
		if(_maxPlayers <= 0)
		{
			return INVALID_DESKSTATION;
		}

		BYTE nextSeatNo = lSeatNo;
		int count = 0;
		while(count < _maxPlayers)
		{
			nextSeatNo = (nextSeatNo + 1 + _maxPlayers) % _maxPlayers;
			UserInfoStruct* pUser = _deskUserList->getUserByDeskStation(nextSeatNo);
			if(pUser != nullptr && _playing[nextSeatNo])
			{
				return nextSeatNo;
			}
			count++;
		}
		return INVALID_DESKSTATION;
	}

	BYTE HNGameLogicBase::getLastUserSeatNo(BYTE lSeatNo)
	{
		if(_maxPlayers <= 0)
		{
			return INVALID_DESKSTATION;
		}

		BYTE lastSeatNo = lSeatNo;
		int count = 0;
		while(count < _maxPlayers)
		{
			lastSeatNo = (lastSeatNo - 1 + _maxPlayers) % _maxPlayers;
			UserInfoStruct* pUser = _deskUserList->getUserByDeskStation(lastSeatNo);
			if(pUser != nullptr && _playing[lastSeatNo])
			{
				return lastSeatNo;
			}
			count++;
		}
		return INVALID_DESKSTATION;
	}


	BYTE HNGameLogicBase::viewToLogicSeatNo(BYTE vSeatNO)
	{
		return (vSeatNO - _seatOffset + _maxPlayers) % _maxPlayers;
	}

	BYTE HNGameLogicBase::logicToViewSeatNo(BYTE lSeatNO)
	{
		return (lSeatNO + _seatOffset + _maxPlayers) % _maxPlayers;
	}

	void HNGameLogicBase::handlerVideo()
	{
		IGameMessageDelegate *d = this;
		RefValue<IGameMessageDelegate*> rDelegate(d);

		EventCustom event("RecordVideoController-start");
		event.setUserData(&rDelegate);
		Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
	}
}