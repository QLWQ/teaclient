/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_VipRoomLogic_h__
#define HN_VipRoomLogic_h__

#include "cocos2d.h"
#include "HNSocket/HNSocketMessage.h"

using namespace HN;

class HNVipRoomDelegate
{
public:
    // 房卡局数更新
    virtual void onRoomCountChange(int totalCount, int count) {}

    // 确定房主
    virtual void onRoomHostChange(UINT userid){}

    // 房间密码确定
    virtual void onRoomPwdChange(const std::string& pwd){}
};


class IHNVIPRoomLogicBase
{
public:
	// 获取房间信息通知
	virtual void onRoomGetRoomInfoCallback(bool success, VipDeskInfoResult* info) {}

	// 申请解散通知
	virtual void onRoomDismissNotifyCallback(VipDeskDismissNotify* notify) {}

	// 解散结果通知
	virtual void onRoomDissmissCallback(bool success) {}

	// 解散消息玩家操作通知
	virtual void onRoomDissmissActionCallback(UINT userID, bool isAgree) {}

	// 断线重连消息通知
	virtual void onRoomNetCutCallback(NetCutDismissNotify* notify) {}

	// IP/距离过近通知
	virtual void onRoomDistanceCallback(std::vector<MSG_GR_S_Position_Notice *> queueNotice) {}

	// IP/距离过近玩家操作通知
	virtual void onRoomDistanceActionCallback(UINT userID, bool isAgree) {}

	// 所有玩家同意游戏开始通知
	virtual void onRoomAllUserAgreeCallback(bool isAgree) {}

	// 返回大厅通知
	virtual void onRoomReturnPlatformCallback(bool success, std::string message) {}
};

class VipRoomLogic : public Ref
{
public:
	static VipRoomLogic* create(IHNVIPRoomLogicBase* base);

	// 设置桌号
	void setDeskNo(int deskNo);

public:
	// 申请解散房间
	void requestDismissDesk(std::string password = "");

	// 申请回到大厅
	void requestReturnPlatform();

	// 解散房间 同意/拒绝 操作
	void userDismissAction(bool bAgree);

	// 距离过近 同意/拒绝 操作
	void userDistanceAction(bool bAgree);

private:
	// 初始化
	bool init();

	// 添加各种监听事件
	void addEventSelectors();

	// 获取当前房间规则信息
	void getDeskInfo(int deskNo);

private:
	// 获取当前房间规则回调
	bool getDeskInfoMessagesEventSelector(HNSocketMessage* socketMessage);

	// 申请解散回调
	bool requestDismissMessagesEventSelector(HNSocketMessage* socketMessage);

	// 玩家 同意/拒绝 操作回调
	bool userActionMessagesEventSelector(HNSocketMessage* socketMessage);

	// 解散广播通知回调
	bool dismissNotifyMessagesEventSelector(HNSocketMessage* socketMessage);

	// 掉线解散广播通知回调
	bool netCutDismissNotifyMessagesEventSelector(HNSocketMessage* socketMessage);

	// 监听距离过近消息提示
	bool distanceEventSelector(HNSocketMessage* socketMessage);

	// 距离过近玩家 同意/拒绝 操作回调
	bool distanceActionEventSelector(HNSocketMessage* socketMessage);

	// 所有玩家同意游戏开始回调
	bool allUserAgreeEventSelector(HNSocketMessage* socketMessage);

protected:
	VipRoomLogic(IHNVIPRoomLogicBase* base);

	virtual ~VipRoomLogic();

private:
	IHNVIPRoomLogicBase* _callback	= nullptr;
	bool	_isDismiss				= false;	// 房间是否已解散
	EventListenerCustom* _listener	= nullptr;
};

#endif // HN_VipRoomLogic_h__
