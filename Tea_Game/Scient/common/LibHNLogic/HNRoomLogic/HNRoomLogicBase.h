/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNRoomLogicBase_H__
#define __HNRoomLogicBase_H__

#include "HNLogic/HNLogicBase.h"

namespace HN
{
	class IHNRoomLogicBase
	{
	public:
		virtual void onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode) {}
		virtual void onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo) {}
		virtual void onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo) {}
		virtual void onRoomSitUpCallback(bool success, const std::string& message, BYTE destNo, BYTE seatNo) {}
		virtual void onRoomUserCome(INT userID) {}
		virtual void onRoomUserLeft(INT userID) {}
		virtual void onRoomDisConnect(const std::string &message) {}
		// 房间密码
		virtual void onPlatformRoomPassEnter(bool success, UINT roomId) {}

		// 系统自动赠送
		virtual void onSysGiveMoney(SysGiveMoney* give) {}
	};

	class IRoomMessageDelegate;
	/*
	 * redbird room logic base.
	 */
	class HNRoomLogicBase: public HNLogicBase, public IRoomMessageDelegate, public IPlatformMessageDelegate
	{
	public:
		virtual void start() override;
		virtual void stop() override;
		void requestLogin(UINT roomID, float latitude = 0,
			float longtitude = 0, const std::string& addr = "");
		void requestSit(BYTE deskNo);
		void requestSit(BYTE deskNo, BYTE seatNo, std::string password = "");
		void requestJoinQueue();
		void requestWatchSit(BYTE deskNo, BYTE seatNo, std::string password = "");

		// 快速坐下
		void requestQuickSit(BYTE deskNo = 255, std::string password = "");

		// 茶楼快速坐下
		void requestTeaHouseQuickSit(int	clubId, int	teahouseId, int deskNo = 255);

		// 登录密码房间
		void requestRoomPasword(UINT roomId, const std::string& password);

	public:
		// 普通聊天
		virtual void I_R_M_NormalTalk(void* object, INT objectSize){}

		// 道具特效
		virtual void I_R_M_NormalAction(void* object, INT objectSize){}

		// 用户坐下
		virtual void I_R_M_UserSit(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user);

		// 用户坐下失败
		virtual void I_R_M_SitError(const std::string& message) override;

		// 用户加入排队机
		virtual void I_R_M_UserJoinQueue(const std::string& message) override;

		// 用户排队坐下
		virtual void I_R_M_QueueUserSit(BYTE deskNo, const std::vector<MSG_GR_Queue_UserSit*>& users);

		// 用户站起
		virtual void I_R_M_UserUp(MSG_GR_R_UserUp * userUp, const UserInfoStruct* user);

		// 用户断线
		virtual void I_R_M_UserCut(INT dwUserID, BYTE bDeskNO, BYTE	bDeskStation) {}

		// 网络连接
		virtual void I_R_M_Connect(bool result) override;

		// 房间登录
		virtual void I_R_M_Login(bool success, UINT handleCode, const std::string& message) override;

		// 登录完成
		virtual void I_R_M_LoginFinish() override;

		// 系统自动赠送
		virtual void I_R_M_SysGiveMoney(SysGiveMoney* give) override;

		// 用户进入
		virtual void I_R_M_UserCome(UserInfoStruct* user) override;

		// 用户离开
		virtual void I_R_M_UserLeft(const UserInfoStruct* user) override;

		// 用户信息变化消息
		virtual void I_R_M_UserInfoChange(const UserInfoStruct* user) override;

		// 房间密码
		virtual void I_P_M_RoomPassword(MSG_GP_S_C_CheckRoomPasswd* pData) override;

	public:
		HNRoomLogicBase(IHNRoomLogicBase* callback);
		virtual ~HNRoomLogicBase();
	protected:
		IHNRoomLogicBase* _callback;
		UINT _roomID;
		float	_latitude;		//维度
		float	_longtitude;	//经度
		std::string	_addr;		//玩家物理地址
	};
}

#endif