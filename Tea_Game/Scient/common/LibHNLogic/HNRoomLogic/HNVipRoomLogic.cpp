/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNVipRoomLogic.h"
#include "HNNetExport.h"
#include <string>

VipRoomLogic::VipRoomLogic(IHNVIPRoomLogicBase* base)
: _callback (base)
{
}

VipRoomLogic::~VipRoomLogic()
{
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);

	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS);
	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS_AGREE);
	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DESKRUNOUT);
	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_GETDESKMASTER);
	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_GETDISTANCEINFO);
	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISTANCE_AGREE);
	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISTANCE_ALLAGREE);
	RoomLogic()->removeEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS_NETCUT);
	RoomLogic()->removeEventSelector(MDM_GR_USER_ACTION, ASS_GR_MASTER_LEAVE);
}

VipRoomLogic* VipRoomLogic::create(IHNVIPRoomLogicBase* base)
{
	VipRoomLogic *pRet = new VipRoomLogic(base);
	if (pRet && pRet->init()) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool VipRoomLogic::init()
{
	addEventSelectors();

	return true;
}

void VipRoomLogic::setDeskNo(int deskNo)
{
	Director::getInstance()->getScheduler()->schedule([=](float fDelta)
	{
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			// 延迟获取当前桌子信息
			getDeskInfo(deskNo);
		}
	}, this, 0.0f, 0, 0.0f, false, "delayGetDeskInfo");

	// 监听重连之后重新获取桌子信息
	_listener = EventListenerCustom::create(RECONNECTION, [=](EventCustom* event) {
		
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			// 延迟获取当前桌子信息
			getDeskInfo(deskNo);
		}
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);
}

void VipRoomLogic::addEventSelectors()
{
	// 监听玩家申请解散消息
	RoomLogic()->addEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS,
		HN_SOCKET_CALLBACK(VipRoomLogic::requestDismissMessagesEventSelector, this));

	// 监听玩家同意或拒绝解散消息
	RoomLogic()->addEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS_AGREE,
		HN_SOCKET_CALLBACK(VipRoomLogic::userActionMessagesEventSelector, this));

	// 监听房间解散广播消息
	RoomLogic()->addEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DESKRUNOUT,
		HN_SOCKET_CALLBACK(VipRoomLogic::dismissNotifyMessagesEventSelector, this));

	// 监听掉线解散广播消息
	RoomLogic()->addEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS_NETCUT,
		HN_SOCKET_CALLBACK(VipRoomLogic::netCutDismissNotifyMessagesEventSelector, this));

	// 监听距离过近消息提示
	RoomLogic()->addEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_GETDISTANCEINFO,
		HN_SOCKET_CALLBACK(VipRoomLogic::distanceEventSelector, this));

	// 监听距离过近玩家操作消息
	RoomLogic()->addEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISTANCE_AGREE,
		HN_SOCKET_CALLBACK(VipRoomLogic::distanceActionEventSelector, this));

	// 监听距离过近所有玩家同意消息
	RoomLogic()->addEventSelector(MDM_GR_DESKRUNOUT, ASS_GR_DISTANCE_ALLAGREE,
		HN_SOCKET_CALLBACK(VipRoomLogic::allUserAgreeEventSelector, this));
}

void VipRoomLogic::getDeskInfo(int deskNo)
{
	VipDeskInfo getInfo;
	getInfo.byDeskIndex = deskNo;

	RoomLogic()->sendData(MDM_GR_DESKRUNOUT, ASS_GR_GETDESKMASTER, &getInfo, sizeof(VipDeskInfo),
		HN_SOCKET_CALLBACK(VipRoomLogic::getDeskInfoMessagesEventSelector, this));
}

bool VipRoomLogic::getDeskInfoMessagesEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(VipDeskInfoResult, socketMessage->objectSize, true, "VipDeskInfoResult size of error!");

	auto info = (VipDeskInfoResult*)socketMessage->object;

	/*if (info->iMasterID == 0)
	{
		_callback->onRoomGetRoomInfoCallback(false, nullptr);
		return true;
	}*/

	if (0 == info->iType)
	{
		HNPlatformConfig()->setPlayCount(info->iPlayCount - info->iNowCount);
	}
	else if (3 == info->iType)
	{
		HNPlatformConfig()->setRestTime(info->iSeconds);
	}
	else
	{
		HNPlatformConfig()->setPlayCount(-1);
	}

	HNPlatformConfig()->setNowCount(info->iNowCount);
	HNPlatformConfig()->setAllCount(info->iPlayCount);
	HNPlatformConfig()->setMasterID(info->iMasterID);
	HNPlatformConfig()->setVipRoomNum(info->szPassWord);

	_callback->onRoomGetRoomInfoCallback(true, info);

	return true;
}

void VipRoomLogic::requestDismissDesk(std::string password/* = ""*/)
{
	if (password.empty())
	{
		VipDeskDismissNotify dismiss;
		dismiss.iUserID = PlatformLogic()->loginResult.dwUserID;

		RoomLogic()->sendData(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS, &dismiss, sizeof(VipDeskDismissNotify));
	}
	else
	{
		// 房间管理中直接用桌子密码解散对应房间

	}
}
bool VipRoomLogic::requestDismissMessagesEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(VipDeskDismissNotify, socketMessage->objectSize, true, "VipDeskDismissNotify size of error!");
	VipDeskDismissNotify* dismiss = (VipDeskDismissNotify*)socketMessage->object;

	_callback->onRoomDismissNotifyCallback(dismiss);
	return true;
}

void VipRoomLogic::userDismissAction(bool bAgree)
{
	VipDeskDismissAgree dismiss_agree;
	dismiss_agree.bAgree = bAgree;

	RoomLogic()->sendData(MDM_GR_DESKRUNOUT, ASS_GR_DISMISS_AGREE, &dismiss_agree, sizeof(VipDeskDismissAgree));
}

bool VipRoomLogic::userActionMessagesEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(VipDeskDismissAgreeRes, socketMessage->objectSize, true, "VipDeskDismissAgreeRes size of error!");
	VipDeskDismissAgreeRes* res = (VipDeskDismissAgreeRes*)socketMessage->object;

	_callback->onRoomDissmissActionCallback(res->iUserID, res->bAgree);
	return true;
}

bool VipRoomLogic::netCutDismissNotifyMessagesEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(NetCutDismissNotify, socketMessage->objectSize, true, "NetCutDismissNotify size of error!");
	NetCutDismissNotify* notify = (NetCutDismissNotify*)socketMessage->object;

	_callback->onRoomNetCutCallback(notify);
	return true;
}

bool VipRoomLogic::dismissNotifyMessagesEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(VIPDeskRunOut, socketMessage->objectSize, true, "VIPDeskRunOut size of error!");
	VIPDeskRunOut* res = (VIPDeskRunOut*)socketMessage->object;

	if (res->bSuccess)
	{
		_isDismiss = true;

		_callback->onRoomDissmissCallback(true);
	}
	else
	{
		_callback->onRoomDissmissCallback(false);
	}
	return true;
}

bool VipRoomLogic::distanceEventSelector(HNSocketMessage* socketMessage)
{
	std::vector<MSG_GR_S_Position_Notice *> queueNotice;

	UINT count = socketMessage->objectSize / sizeof(MSG_GR_S_Position_Notice);
	MSG_GR_S_Position_Notice * temp = (MSG_GR_S_Position_Notice *)socketMessage->object;

	while (count-- > 0)	{
		queueNotice.push_back(temp++);
	}

	_callback->onRoomDistanceCallback(queueNotice);
	return true;
}

bool VipRoomLogic::distanceActionEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(VipDeskDismissAgreeRes, socketMessage->objectSize, true, "VipDeskDismissAgreeRes size of error!");
	VipDeskDismissAgreeRes* res = (VipDeskDismissAgreeRes*)socketMessage->object;

	_callback->onRoomDistanceActionCallback(res->iUserID, res->bAgree);
	return true;
}

void VipRoomLogic::userDistanceAction(bool bAgree)
{
	if (bAgree)
	{

		VipDeskDismissAgree dismiss_agree;
		dismiss_agree.bAgree = bAgree;

		RoomLogic()->sendData(MDM_GR_DESKRUNOUT, ASS_GR_DISTANCE_AGREE, &dismiss_agree, sizeof(VipDeskDismissAgree));
	}
	else
	{
		if (PlatformLogic()->loginResult.dwUserID == HNPlatformConfig()->getMasterID())
		{
			// 如果是房主拒绝继续游戏，则解散当前房间
			requestDismissDesk();
		}
		else
		{
			// 玩家拒绝继续游戏，则拒绝的玩家退出当前房间，其他玩家弹出开局失败提示
			RoomLogic()->close();
			_callback->onRoomReturnPlatformCallback(true, "success");
		}
	}
}

// 所有玩家同意游戏开始回调
bool VipRoomLogic::allUserAgreeEventSelector(HNSocketMessage* socketMessage)
{
	_callback->onRoomAllUserAgreeCallback(true);
	return true;
}

void VipRoomLogic::requestReturnPlatform()
{
	// 如果房间已经解散，点击返回按钮直接回到大厅
	if (_isDismiss)
	{
		RoomLogic()->close();
		_callback->onRoomReturnPlatformCallback(true, "success");
	}
	else
	{
		if (PlatformLogic()->loginResult.dwUserID == HNPlatformConfig()->getMasterID())
		{
			RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_MASTER_LEAVE, nullptr, 0, [=](HNSocketMessage* socketMessage) {

				std::string str("");

				switch (socketMessage->messageHead.bHandleCode)
				{
				case ERR_GR_LEAVE_SUCCESS:
				{
					RoomLogic()->close();
					_callback->onRoomReturnPlatformCallback(true, "success");
					return true;
				}
				break;
				case ERR_GR_NOT_BUY_ROOM:
					str = GBKToUtf8("不是可购买房间");
					break;
				case ERR_GR_IN_GAME:
					str = GBKToUtf8("游戏已经开始");
					break;
				case ERR_GR_NOT_IN_DESK:
					str = GBKToUtf8("用户未坐下");
					break;
				case ERR_GR_NOT_LEAVE:
					str = GBKToUtf8("房主不允许离开");
					break;
				case ERR_GR_NOT_MASTER:
					str = GBKToUtf8("不是房主");
					break;
				case ERR_GR_IS_CLUBROOM:{
					RoomLogic()->close();
					_callback->onRoomReturnPlatformCallback(true, "success");
					return true;
				}
					break;
				default:
					break;
				}

				_callback->onRoomReturnPlatformCallback(false, str);

				RoomLogic()->removeEventSelector(MDM_GR_USER_ACTION, ASS_GR_MASTER_LEAVE);
				return true;
			});
		}
		else
		{
			if ((HNPlatformConfig()->getNowCount() > 0) && (RoomLogic()->getRoomRule() & GRR_GAME_BUY))
			{
				_callback->onRoomReturnPlatformCallback(false, GBKToUtf8("游戏已经开始"));
			}
			else
			{
				
				RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_USER_UP);
				RoomLogic()->close();
				_callback->onRoomReturnPlatformCallback(true, "success");
				
			}
		}
	}
}
