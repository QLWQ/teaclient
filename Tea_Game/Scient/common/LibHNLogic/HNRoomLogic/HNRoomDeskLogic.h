/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNRoomDeskLogic_H__
#define __HNRoomDeskLogic_H__

#include "HNLogic/HNLogicBase.h"
#include "HNNetExport.h"

namespace HN
{
	class IHNRoomDeskLogic
	{
	public:
		virtual void onDeskSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo){}
		virtual void onRoomUserCome(INT userID){}
		virtual void onRoomUserLeft(INT userID){}
		virtual void onRoomDeskUserCountChanged(BYTE deskNo){}
		virtual void onUpdateVirtualDesk(BYTE deskNo, bool isVirtual) {}
		virtual void onUpdateDeskState(BYTE deskNo, bool isPlaying) {}
	};


	class HNRoomDeskLogic:public HNLogicBase, public IRoomMessageDelegate
	{
	public:
		virtual void start() override;
		virtual void stop() override;
		void requestSit(BYTE deskNo, const std::string& password = "");
		void requestSit(BYTE deskNo, BYTE seatNo, const std::string& password = "");

		// 快速坐下
		void requestQuickSit(BYTE deskNo = 255, std::string password = "");

		// 茶楼快速坐下
		void requestTeaHouseQuickSit(int	clubId, int	teahouseId, int deskNo = 255);

		// 构造函数
		HNRoomDeskLogic(IHNRoomDeskLogic* callback);

		// 析构函数
		virtual ~HNRoomDeskLogic();
	
		// 用户坐下
		virtual void I_R_M_UserSit(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;

		// 用户坐下失败
		virtual void I_R_M_SitError(const std::string& message) override;

		// 用户站起
		virtual void I_R_M_UserUp(MSG_GR_R_UserUp * userUp, const UserInfoStruct* user) override;

		// 用户进入
		virtual void I_R_M_UserCome(UserInfoStruct* user) override;

		// 用户离开
		virtual void I_R_M_UserLeft(const UserInfoStruct* user) override;

		// 更新是否封桌状态
		virtual void I_R_M_UpdateVirtualDesk(BYTE deskNo, bool isVirtual) override;

		// 更新桌子状态
		virtual void I_R_M_UpdateDeskState(BYTE deskNo, bool isPlaying) override;

		// 更新桌上人数
		virtual void I_R_M_UpdateDeskUserCount(BYTE deskNo) override;

	protected:
		IHNRoomDeskLogic* _callback;
		UINT _roomID;
	};
}

#endif