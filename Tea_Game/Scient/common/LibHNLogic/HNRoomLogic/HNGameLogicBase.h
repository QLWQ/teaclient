/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNGameLogicBase_H__
#define __HNGameLogicBase_H__

#include "HNLogic/HNLogicBase.h"
#include "HNNetExport.h"

namespace HN
{
	/*
	 * @brief: redbird game logic interface.
	 */
	class IHNGameLogicBase
	{
	public:
		virtual void onGameDisconnect(){}
	};

	/*
	 * @brief: redbird game logic.
	 */
	class HNGameLogicBase: public HNLogicBase, public IGameMessageDelegate, public Ref
	{
	public:
		/*
		 * @brief: will start receive message after called.
		 */
		virtual void start() override;
		/*
		 * @brief: will stop receive message after called.
		 */
		virtual void stop() override;
		/*
		 * @brief: send message to room server from client. 
		 */
		virtual void sendGameInfo();
		virtual void sendUserUp();
		virtual void sendUserSit(BYTE lSeatNo);
		virtual void sendWatchSit(BYTE lSeatNo);
		virtual void sendFastWatch();
		virtual void sendJoinQueue();
		virtual void sendForceQuit();
		virtual void sendAgreeGame();
		virtual void sendChatMsg(const std::string& msg);
		virtual void sendActionChatMsg(INT TargetID, int index);
	
		//换桌
		virtual void sendChangeDesk();
        /*
		 * @brief: translate between view seat no and logic seat no.
		 */
		virtual BYTE viewToLogicSeatNo(BYTE vSeatNO);
		virtual BYTE logicToViewSeatNo(BYTE lSeatNO);

		/*
		 * @brief: get user id from logic seatNo.
		 */
		INT getUserId(BYTE lSeatNo);
		/*
		 * @brief: get my seatNo.
		 */
		BYTE getMySeatNo() const;
		/*
		 * @brief:  get user whether is boy.
		 * @param:  lSeatNo is user logic seatNo.
		 * @return: true if it is boy, or false.
		 */
		bool getUserIsBoy(BYTE lSeatNo);
		/*
		 * @brief:  get user's info from userId.
		 * @param:  userID is user's id.
		 * @return: return user's info.
		 */
		UserInfoStruct* getUserByUserID(INT userID);
		/*
		 * @brief:  get user's info from logic seatNo.
		 * @param:  lSeatNo is user's logic seatNo.
		 * @return: return user's info.
		 */
		UserInfoStruct* getUserBySeatNo(BYTE lSeatNo);
		/*
		* @brief:  get watch user's info from userId.
		* @param:  userID is user's id.
		* @return: return user's info.
		*/
		UserInfoStruct* getLookerByUserID(INT userID);

		BYTE getGameStatus() const { return _gameStatus; }
		
		void setGameStatus(BYTE status) { _gameStatus = status; }

		// 是否旁观状态
		bool isLooker();

	public:
		HNGameLogicBase(BYTE deskNo, INT maxPlayers, bool autoCreate, IHNGameLogicBase* callback);
		virtual ~HNGameLogicBase();

	private:
		virtual void loadDeskUsers() final;

	protected:		
		virtual void clearDesk() = 0;

		virtual void clearDeskUsers() = 0;

	protected:
		/*
		 * framework message deal function.
		 */
		virtual void dealUserAgreeResp(MSG_GR_R_UserAgree* agree) = 0;
		virtual void dealGameStartResp(BYTE bDeskNO);
		virtual void dealGameClean() = 0;
		//更新比赛场进度
		virtual void dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum) = 0;
		virtual void dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum) = 0;
		virtual void dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum) = 0;
		////////////////////////////////////////////////////////////////////
		virtual void dealGameEndResp(BYTE bDeskNO);
		virtual void dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) = 0;
		virtual void dealQueueUserSitMessage(bool success, const std::string& message) = 0;
		virtual void dealUserUpResp(MSG_GR_R_UserUp * userSit, const UserInfoStruct* user) = 0;
		virtual void dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo);
		virtual void dealGameStationResp(void* object, INT objectSize) = 0;
		virtual void dealGamePlayBack(bool isback) = 0;
		virtual void dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize) = 0;
		virtual void dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize);
		virtual void dealUserCutMessageResp(INT userId, BYTE seatNo) = 0;
		virtual void dealUserInfoChangeResp(const UserInfoStruct* user) = 0;
		virtual void dealUserNotEnoughMoney();
		virtual void dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk);
		virtual void dealUserActionMessage(DoNewProp* normalAction);
		virtual void dealUserSitOrUpError(const std::string& message);
		virtual void dealUserQueueReady();
		virtual void dealGameActivityResp(MSG_GR_GameNum* pData);
		virtual void dealGameActivityReceive(MSG_GR_Set_JiangLi* pData);
		//virtual void dealGameNumRound(MSG_GR_ConTestRoundNum* pData);
		//virtual void dealGameInnings(MSG_GR_ConTestRoundNumInOneRound* pData);

	protected:
		/*
		 * get next and last user seat no.
		 * @param:  seatNo find seat no.
		 * @return: return seat no if success, or return INVALID_DESKSTATION
		 */
		/*
		 * @brief:  get next user's logic seatNo.
		 */
		virtual BYTE getNextUserSeatNo(BYTE lSeatNo);
		/*
		 * @brief:  get last user's logic seatNo.
		 */
		virtual BYTE getLastUserSeatNo(BYTE lSeatNo);

	private:
		/*
		* init member only once.
		*/
		virtual void initParams();
		/*
		* refresh member before every game start.
		*/
		virtual void refreshParams();

	private:
		/*
		 * @brief:  platfrom message.
		 */
		// 用户同意
		virtual void I_R_M_UserAgree(MSG_GR_R_UserAgree* agree) override;

		// 游戏开始
		virtual void I_R_M_GameBegin(BYTE bDeskNO) override;

		// 游戏清理
		virtual void I_R_M_GameClean() override;

		// 更新比赛轮数
		virtual void I_R_M_GameRoundNum(MSG_GR_ConTestRoundNum* contestRoundNum) override;

		// 更新比赛局数
		virtual void I_R_M_GameInnings(MSG_GR_ConTestRoundNumInOneRound* contestInnings) override;

		// 更新当前比赛进度
		virtual void I_R_M_GameInfoupdate(MSG_GM_S_ConTestGameInfo* contestInfodata) override;

		// 游戏结束
		virtual void I_R_M_GameEnd(BYTE bDeskNO) override;

		// 游戏信息
		virtual void I_R_M_GameInfo(MSG_GM_S_GameInfo* pGameInfo) override;

		// 游戏状态
		virtual void I_R_M_GameStation(void* object, INT objectSize) override;

		// 设置游戏回放模式
		virtual void I_R_M_GamePlayBack(bool isBack) override;

		// 游戏消息
		virtual bool onGameMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;

		// 语音消息
		virtual bool onVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;

		// 玩家信息变化消息
		virtual void I_R_M_UserInfoChange(const UserInfoStruct* user) override;

		// 玩家金币不足消息
		virtual void I_R_M_UserNotEnoughMoney() override;

		// 用户加入排队机
		virtual void I_R_M_UserJoinQueue(const std::string& message) override;

		// 排队准备
		virtual void I_R_M_QueueReady() override;
	
		// 排队用户坐下
		virtual void I_R_M_QueueUserSit(BYTE deskNo, const std::vector<MSG_GR_Queue_UserSit*>& users) override;

		// 用户坐下
		virtual void I_R_M_UserSit(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;

		// 用户坐下失败
		virtual void I_R_M_SitError(const std::string& message) override;
		
		// 用户站起
		virtual void I_R_M_UserUp(MSG_GR_R_UserUp * userUp, const UserInfoStruct* user) override;

		// 用户断线
		virtual void I_R_M_UserCut(INT dwUserID, BYTE bDeskNO, BYTE	bDeskStation) override;

		// 聊天消息
		virtual void I_R_M_NormalTalk(void* object, INT objectSize) override;

		// 道具特效
		virtual void I_R_M_NormalAction(void* object, INT objectSize) override;

		// 游戏内红包活动
		virtual void I_R_M_GameActicity(void* object, INT objectSize) override;

		// 游戏内领取红包
		virtual void I_R_M_GameReceive(void* object, INT objectSize) override;

		// 比赛场更新轮数
		virtual void I_R_M_GameNumRound(void* object, INT objectSize) override;

		// 比赛场更新局数
		virtual void I_R_M_GameInnings(void* object, INT objectSize) override;

		// 处理回放
		void handlerVideo();

	protected:
		IHNGameLogicBase* _callback;
		HNGameTableUsersData*		_deskUserList;	//坐桌玩家列表
		HNGameTableUsersData*		_lookerList;	//旁观玩家列表
		BYTE						_deskNo;
		BYTE						_mySeatNo;		
		BYTE						_watchOther;	// 允许旁观
		BYTE						_gameStatus;	// 游戏状态
		// + is clockwise, - is opposite.
		int							_seatOffset;	// 座位偏移量
		// before auto action, player's action time.
		BYTE						_waitTime;		// 等待时间
		// max player count in the table.
		INT							_maxPlayers;
		// if the table is auto created
		bool						_autoCreate;
		std::vector<bool>			_existPlayer;
		std::vector<bool>			_playing;

		std::map<UINT, UserInfoStruct>	_user_id;		//通过id查找玩家
		std::map<UINT, UserInfoStruct>	_user_seatNo;	//通过座位号查找玩家

		bool						_isPlaying = false;		// 游戏进行中

		CC_SYNTHESIZE_PASS_BY_REF(bool, _isRecordMode, recordMode);//是否回放模式

	};
}

#endif