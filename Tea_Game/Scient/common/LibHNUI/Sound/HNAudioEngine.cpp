﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNAudioEngine.h"
#include <SimpleAudioEngine.h>
#include "AudioEngine.h"
#include "HNCommon/HNLog.h"

using namespace experimental;

namespace HN
{
	static HNAudioEngine* instant = nullptr;

	static const char* SOUND_VALUE_TEXT = "sound";
	static const char* MUSIC_VALUE_TEXT = "music";

	HNAudioEngine::HNAudioEngine()
	{
		_switchOfMusic = true;
		_switchOfEffect = true;
	}

	HNAudioEngine::~HNAudioEngine()
	{
		_audioCaches.clear();
	}

	HNAudioEngine* HNAudioEngine::getInstance()
	{
		if (nullptr == instant)
		{
			instant = new HNAudioEngine();
		}
		return instant;
	}

	void HNAudioEngine::destroyInstance()
	{
		CC_SAFE_DELETE(instant);
	}

	void HNAudioEngine::setSwitchOfMusic(bool isOpen)
	{
		_switchOfMusic = isOpen;
	}

	bool HNAudioEngine::getSwitchOfMusic() const
	{
		return _switchOfMusic;
	}

	void HNAudioEngine::setSwitcjofEffect(bool isOpen)
	{
		_switchOfEffect = isOpen;
	}

	bool HNAudioEngine::getSwitcjofEffect() const
	{
		return _switchOfEffect;
	}

	void HNAudioEngine::playBackgroundMusic(const char* pszFilePath, bool bLoop)
	{
		//if (_switchOfMusic)
		{
			if (nullptr != pszFilePath)
			{
				if (0 != _currentBackgroundName.compare(pszFilePath))
				{
					//AudioEngine::stop(_bkMusicID);
					_currentBackgroundName = pszFilePath;
					CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(pszFilePath, bLoop);
					//float value = Configuration::getInstance()->getValue(MUSIC_VALUE_TEXT, Value(1.f)).asFloat();
					//_bkMusicID = AudioEngine::play2d(pszFilePath, bLoop, value);

					//log("############value:%f, path:%s, id:%d", value, pszFilePath, _bkMusicID);
				}
			}
		}
	}

	void HNAudioEngine::stopBackgroundMusic(bool bReleaseData)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic(bReleaseData);
		//AudioEngine::stop(_bkMusicID);
	}

	void HNAudioEngine::pauseBackgroundMusic()
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		//AudioEngine::pause(_bkMusicID);
	}

	void HNAudioEngine::resumeBackgroundMusic()
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		//AudioEngine::resume(_bkMusicID);
	}

	//暂停音效
	void HNAudioEngine::pauseEffect()
	{
		AudioEngine::pauseAll();
	}

	//继续音效
	void HNAudioEngine::resumeEffect()
	{
		AudioEngine::resumeAll();
	}

	void HNAudioEngine::stopAllEffect()
	{
		AudioEngine::stopAll();
	}
	
	void HNAudioEngine::preloadEffect(const std::string& filePath, std::function<void(bool isSuccess)> callback)
	{
		std::string path(filePath);
		_audioCaches.insert(std::pair<std::string, bool>(path, false));

		AudioEngine::preload(path, [=](bool isSuccess) {
			if (isSuccess)
			{
				_audioCaches.find(path)->second = true;

				if (callback)
				{
					callback(isSuccess);
				}
			}
		});
	}

	int HNAudioEngine::playEffect(const char* pszFilePath, bool bLoop, float pitch, float pan, float gain)
	{
		if (_switchOfEffect)
		{
			//return CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(pszFilePath, bLoop, pitch, pan, gain);
			float value = Configuration::getInstance()->getValue(SOUND_VALUE_TEXT, Value(1.f)).asFloat();

			auto it = _audioCaches.find(pszFilePath);

			if (it != _audioCaches.end())
			{
				if (it->second)
				{
					 return AudioEngine::play2d(pszFilePath, bLoop, value);
				}
			}
			else
			{
				std::string filePath(pszFilePath);
				_audioCaches.insert(std::pair<std::string, bool>(filePath, false));

				AudioEngine::preload(filePath, [=](bool isSuccess) {
					if (isSuccess)
					{
						_audioCaches.find(filePath)->second = true;

						AudioEngine::play2d(filePath, bLoop, value);
					}
				});
			}
		}
		return -1;
	}

	void HNAudioEngine::setEffectsVolume(float volume)
	{
		//CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(volume);
		Configuration::getInstance()->setValue(SOUND_VALUE_TEXT, Value(volume));
	}

	float HNAudioEngine::getEffectsVolume() const
	{
		//return CocosDenshion::SimpleAudioEngine::getInstance()->getEffectsVolume();
		return Configuration::getInstance()->getValue(SOUND_VALUE_TEXT, Value(1.f)).asFloat();
	}

	void HNAudioEngine::setBackgroundMusicVolume(float volume)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(volume);
		//AudioEngine::setVolume(_bkMusicID, volume);
		//Configuration::getInstance()->setValue(MUSIC_VALUE_TEXT, Value(volume));
	}

	float HNAudioEngine::getBackgroundMusicVolume() const
	{
		return CocosDenshion::SimpleAudioEngine::getInstance()->getBackgroundMusicVolume();
		//return AudioEngine::getVolume(_bkMusicID);
	}

	bool HNAudioEngine::initAudio()
	{
		return AudioEngine::lazyInit();
	}
}
