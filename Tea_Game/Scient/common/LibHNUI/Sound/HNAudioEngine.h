﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_AUDIO_ENGINE_
#define _HN_AUDIO_ENGINE_

#include "cocos2d.h"
#include <vector>

USING_NS_CC;

namespace HN
{

	class HNAudioEngine
	{
	public:
		// 获取单例
		static HNAudioEngine* getInstance();

		// 销毁单例
		static void destroyInstance();

	public:
		HNAudioEngine();
		~HNAudioEngine();

		//播放音乐
		void playBackgroundMusic(const char* pszFilePath, bool bLoop = true);

		//停止播放音乐
		void stopBackgroundMusic(bool bReleaseData = false);

		//暂停播放
		void pauseBackgroundMusic();

		//继续播放
		void resumeBackgroundMusic();

		//暂停音效
		void pauseEffect();

		//继续音效
		void resumeEffect();

		// 停止音效
		void stopAllEffect();
		
		// 预加载音效
		void preloadEffect(const std::string& filePath, std::function<void(bool isSuccess)> callback);

		//播放音效
		int playEffect(const char* pszFilePath, bool bLoop = false,float pitch = 1.0f, float pan = 0.0f, float gain = 1.0f);

		//设置音量
		void setEffectsVolume(float volume);
		void setBackgroundMusicVolume(float volume);

		//获得音量大小
		float getEffectsVolume() const;
		float getBackgroundMusicVolume() const;

		//设置声音开关
		void setSwitchOfMusic(bool isOpen);
		void setSwitcjofEffect(bool isOpen);

		//获取声音开发与否
		bool getSwitchOfMusic() const;
		bool getSwitcjofEffect() const;

		static bool initAudio();
	private:
		bool			_switchOfMusic;						//true设置音乐为开

		bool			_switchOfEffect;                    //ture设置音效为开

		std::string		_currentBackgroundName;				//当前背景音乐名称

		int				_bkMusicID = -1;

		std::map<std::string, bool> _audioCaches;
	};

}
#endif 		//_HN_AUDIO_ENGINE_


