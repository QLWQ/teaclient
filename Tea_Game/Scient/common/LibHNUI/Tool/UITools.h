/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __UITOOLS_H__
#define __UITOOLS_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;

namespace HN
{
	class UITools
	{
	public:
		~UITools();

		static void switchUI(Node *pSrc, Node *pDst, bool bRemove = true, bool bUseNature=true);
		static void scaleUI(Node *parent, const Vec2 &vScale, bool bChildScale);

		static Sprite *CirCleSprite(Sprite *avatar, Sprite *stencilBg);
		static Sprite *CirCleSprite(Sprite *avatar, std::string stencilBgFile);

		static void showToast(std::string content);

		// �ü��ı�
		static void ClippingText(Layout* parent, Text* text);

	private:
		static void scaleUI(Node *target, const Vec2 &vScale);

	private:
		UITools();
		CC_DISALLOW_COPY_AND_ASSIGN(UITools)
	};
}

#endif