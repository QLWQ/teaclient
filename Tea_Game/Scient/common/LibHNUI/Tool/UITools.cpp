/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "UITools.h"


namespace HN
{
	UITools::UITools()
	{

	}

	UITools::~UITools()
	{

	}

	void UITools::switchUI(Node *pSrc, Node *pDst, bool bRemove, bool bUseNature)
	{
		if (!pSrc || !pDst)
			return;

		if (bUseNature)
		{
			pDst->setPosition(pSrc->getPosition());
			pDst->setRotation(pSrc->getRotation());
			pDst->setScaleX(pSrc->getContentSize().width / pDst->getContentSize().width);
			pDst->setScaleY(pSrc->getContentSize().height / pDst->getContentSize().height);
			pDst->setAnchorPoint(pSrc->getAnchorPoint());
			pDst->setLocalZOrder(pSrc->getLocalZOrder());
		}

		if (pSrc->getParent())
		{
			pSrc->getParent()->addChild(pDst);

			if (bRemove)
				pSrc->removeFromParent();
		}
	}

	void UITools::scaleUI(Node *parent, const Vec2 &vScale, bool bChildScale)
	{
		if (!parent)
			return;

		if (!bChildScale)
		{
			auto vecChildrens = parent->getChildren();
			for (int i = 0; i < (int)vecChildrens.size(); ++i)
				scaleUI(vecChildrens.at(i), Vec2(1.0f / vScale.x, 1.0f / vScale.y));
		}

		parent->setScaleX(vScale.x);
		parent->setScaleY(vScale.y);
	}

	void UITools::scaleUI(Node *target, const Vec2 &vScale)
	{
		if (!target)
			return;

		target->setScaleX(vScale.x);
		target->setScaleY(vScale.y);

		auto vecChildrens = target->getChildren();
		if (vecChildrens.empty())
			return;

		for (int i = 0; i < (int)vecChildrens.size(); ++i)
			scaleUI(vecChildrens.at(i), vScale);
	}

	Sprite * UITools::CirCleSprite(Sprite *avatar, std::string stencilBgFile)
	{
		//
		if (!avatar) {
			return NULL;
		}
		if (stencilBgFile == "")
		{
			return NULL;
		}
		Sprite * Stencil = Sprite::create(stencilBgFile);
		if (!Stencil) {
			return NULL;
		}
		return CirCleSprite(avatar, Stencil);
	}

	Sprite * UITools::CirCleSprite(Sprite *avatar, Sprite *Stencil)
	{
		//参数解释 avatar 要显示头像 StencilBg 底板 必须透明
		//得到的头像必须使用  Sprite->getTexture()->setAntiAliasTexParameters();来抗锯齿，
		/*
		 例子 Sprite * photo = CirCleSprite("","");
		 photo->getTexture()->setAntiAliasTexParameters();
		 */
		if (!avatar) {
			return NULL;
		}
		if (!Stencil) {
			return NULL;
		}

		Rect rect = avatar->getBoundingBox();
		if (Stencil->getContentSize().width < rect.size.width)
		{
			float scaleX = (Stencil->getContentSize().width - 2) / rect.size.width;
			float scaleY = (Stencil->getContentSize().height - 2) / rect.size.height;
			avatar->setScaleX(scaleX);
			avatar->setScaleY(scaleY);
		}

		int x = Stencil->getContentSize().width - avatar->getContentSize().width;
		int y = Stencil->getContentSize().height - avatar->getContentSize().height;
		RenderTexture * renderTexture = RenderTexture::create(Stencil->getContentSize().width, Stencil->getContentSize().height);

		Stencil->setPosition(Vec2(Stencil->getContentSize().width / 2, Stencil->getContentSize().height / 2));
		avatar->setPosition(Vec2(avatar->getContentSize().width / 2 + x / 2, avatar->getContentSize().height / 2 + y / 2));

		BlendFunc bf1 = { GL_ONE, GL_ZERO };
		BlendFunc bf2 = { GL_DST_ALPHA, GL_ZERO };
		Stencil->setBlendFunc(bf1);
		avatar->setBlendFunc(bf2);

		renderTexture->begin();
		Stencil->visit();
		avatar->visit();
		renderTexture->end();

		Sprite * retval = Sprite::createWithTexture(renderTexture->getSprite()->getTexture());
		retval->setFlippedY(true);
		return retval;
	}

	void UITools::showToast(std::string content)
	{
		Sprite *sp = Sprite::create();
		sp->setName("show_toast");

		ui::Text *text = ui::Text::create(content, "", 30);
		Size size = text->getContentSize();
		sp->setContentSize(size * 2);
		text->setPosition(Vec2(size.width, size.height));
		sp->addChild(text, 10);

		LayerColor *bg = LayerColor::create(Color4B(0, 0, 0, 200));
		bg->setContentSize(size * 2);
		bg->setPosition(Vec2(0, 0));
		sp->addChild(bg, -1);

		Size winSize = Director::getInstance()->getWinSize();

		sp->setScale(winSize.width / 1334.0);
		sp->setPosition(Vec2(winSize.width / 2, winSize.height * 0.15));
		sp->runAction(Sequence::create(DelayTime::create(2.0), RemoveSelf::create(true), nullptr));

		Director::getInstance()->getRunningScene()->removeChildByName("show_toast");
		Director::getInstance()->getRunningScene()->addChild(sp, 999);
	}

	void UITools::ClippingText(Layout* parent, Text* text)
	{
		// 若昵称过长，点击自动滚动
		parent->addClickEventListener([=](Ref*) {

			float width = text->getContentSize().width - (parent->getContentSize().width - text->getPositionX()) + 5;
			if (width > 0)
			{
				parent->setTouchEnabled(false);
				auto action = MoveBy::create(0.01f * width, Vec2(-width, 0));
				text->runAction(Sequence::create(action, action->reverse(), CallFunc::create([=]() {
					parent->setTouchEnabled(true);
				}), nullptr));
			}
		});
	}

}
