/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __Tools_h__
#define __Tools_h__

#include <string>
#include <stack>

namespace HN
{
	// Get individual number
#define FIRST_IPADDRESS(x)  (((x) >> 24) & 0xff)
#define SECOND_IPADDRESS(x) (((x) >> 16) & 0xff)
#define THIRD_IPADDRESS(x)  (((x) >> 8) & 0xff)
#define FOURTH_IPADDRESS(x) ((x) & 0xff)
#define FLOAT_ZERO    0.00001f			//浮点数用来判断是否等于0的值

	enum class BUBBLE_ITEM_TYPE
	{
		unknow,	//未知
		string,//字符串
		expression//表情
	};

	typedef struct _BUBBLE_ITEM
	{
		BUBBLE_ITEM_TYPE t;
		std::string name;

		_BUBBLE_ITEM() : t(BUBBLE_ITEM_TYPE::unknow) {}
	} BUBBLE_ITEM;

	class Tools
	{		
	public:

		// 验证是否是邮箱地址
		static bool verifyEmailAddress(const std::string& email);

		// 验证是否是手机号
		static bool verifyPhone(const std::string& phone);

		// 验证是否是中文
		static bool verifyChinese(const std::string& word);

		// 验证是否只有数字
		static bool verifyNumber(const std::string& word);

		// 验证是否只有数字和英文
		static bool verifyNumberAndEnglish(const std::string& word);

		// 验证是否为机器人ID
		static int verifyUserIDByAmdin(const int& dwUserID);

		// url编码
		static std::string base64urlencode(const std::string &str);
		
		// ip地址转成字符串
		static std::string parseIPAdddress(unsigned int address);

		// 过滤空格回车
		static void TrimSpace(char* str);

		// 解析聊天内容
		static std::stack<BUBBLE_ITEM> parseChatMsg(const std::string &msg);
		
		// 过滤emoji表情
		static std::string filterEmoji(const std::string& source);
		static bool isEmojiCharacter(wchar_t codePoint);

		//替换一个子字符串以后，重新从开始处替换
		static std::string& replace_all(std::string& str, const std::string& old_value, const std::string& new_value);

		// 从字符串开始处依次替换子字符串
		static std::string& replace_all_distinct(std::string& str, const std::string& old_value, const std::string& new_value);
	};
}


#endif // __Tools_h__