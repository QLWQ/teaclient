/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "Tools.h"
#include <regex>
#include "HNCommon/HNConverCode.h"

namespace HN
{
	std::stack<BUBBLE_ITEM> Tools::parseChatMsg(const std::string &msg)
	{
		int length = msg.size();
		std::stack<char> tmp;
		std::stack<_BUBBLE_ITEM> stackMsg;
		while (1 < length--)
		{
			char c0 = msg[length];
			char c1 = msg[length - 1];
			if (c0 == ':' && c1 == '/')
			{
				length -= 1;

				//解析表情
				_BUBBLE_ITEM exp;
				if (tmp.size() > 0)
				{
					exp.t = BUBBLE_ITEM_TYPE::expression;
					for (size_t i = 0; i < 2; i++)
					{
						exp.name.push_back(tmp.top());
						tmp.pop();
					}
				}

				//解析字符串
				if (tmp.size() > 0)
				{
					_BUBBLE_ITEM str;
					str.t = BUBBLE_ITEM_TYPE::string;
					while (!tmp.empty())
					{
						str.name.push_back(tmp.top());
						tmp.pop();
					}

					if (!str.name.empty()) stackMsg.push(str);
				}
				if (!exp.name.empty()) stackMsg.push(exp);
			}
			else
			{
				tmp.push(c0);
			}
		}

		//处理第0个字符
		if (length == 0)
		{
			tmp.push(msg[length]);
		}

		//解析字符串
		if (tmp.size() > 0)
		{
			_BUBBLE_ITEM str;
			str.t = BUBBLE_ITEM_TYPE::string;
			while (!tmp.empty())
			{
				str.name.push_back(tmp.top());
				tmp.pop();
			}
			stackMsg.push(str);
		}

		return stackMsg;
	}

	bool Tools::verifyEmailAddress(const std::string& email)
	{
		std::regex pattern("([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)");
		return std::regex_match(email, pattern);
	}

	bool Tools::verifyPhone(const std::string& phone)
	{
		return true;
	}

	bool Tools::verifyChinese(const std::string& word)
	{
		std::regex pattern("^[\u4E00-\u9FA5]+$");
		return std::regex_match(word, pattern);
	}

	bool Tools::verifyNumber(const std::string& word)
	{
		std::regex pattern("^[0-9]*$");
		return std::regex_match(word, pattern);
	}

	bool Tools::verifyNumberAndEnglish(const std::string& word)
	{
		std::regex pattern("^[A-Za-z0-9]+$");
		return std::regex_match(word, pattern);
	}

	int Tools::verifyUserIDByAmdin(const int& dwUserID)
	{
		if (dwUserID / 10000 <= 0)
		{
			int newID = (dwUserID * dwUserID) % 49999 + 50000;
			return newID;
		}
		else
		{
			return dwUserID;
		}
	}

	std::string Tools::base64urlencode(const std::string &str)
	{
		std::string encode = str;
		std::string::size_type pos(0);
		while(( pos = encode.find("+") ) != std::string::npos)
		{
			encode.replace(pos,1,"%2B");
		}
		pos = 0;
		while(( pos = encode.find("/") ) != std::string::npos)
		{
			encode.replace(pos,1,"%2F");
		}
		return encode;
	}

	std::string Tools::parseIPAdddress(unsigned int address)
	{
		char ipAddress[32];

		
		sprintf(ipAddress, "%d.%d.%d.%d", FOURTH_IPADDRESS(address), THIRD_IPADDRESS(address),
			SECOND_IPADDRESS(address), FIRST_IPADDRESS(address));

		return std::string(ipAddress);
	}

	void Tools::TrimSpace(char* str)
	{
		char *start = str - 1;
		char *end = str;
		char *p = str;
		while (*p)
		{
			switch (*p)
			{
			case ' ':
			case '\r':
			case '\n':
			{
						 if (start + 1 == p)
							 start = p;
			}
				break;
			default:
				break;
			}
			++p;
		}
		//现在来到了字符串的尾部 反向向前
		--p;
		++start;
		if (*start == 0)
		{
			//已经到字符串的末尾了
			*str = 0;
			return;
		}
		end = p + 1;
		while (p > start)
		{
			switch (*p)
			{
			case ' ':
			case '\r':
			case '\n':
			{
						 if (end - 1 == p)
							 end = p;
			}
				break;
			default:
				break;
			}
			--p;
		}
		memmove(str, start, end - start);
		//*(str + (int)end - (int)start) = 0;
	}

	bool Tools::isEmojiCharacter(wchar_t codePoint) {
		return 
			((codePoint >= 0x0) && (codePoint <= 0x2F)) ||
			((codePoint >= 0x5B) && (codePoint <= 0x60)) ||
			((codePoint >= 0x7B) && (codePoint <= 0xFF)) ||
			((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||
			((codePoint >= 0x1F600) && (codePoint <= 0x1F64F)) ||	//表情符号
			((codePoint >= 0x1F680) && (codePoint <= 0x1F6FF)) ||	//交通和地图符号
			((codePoint >= 0x2600) && (codePoint <= 0x26FF)) ||		//杂项符号
			((codePoint >= 0x2300) && (codePoint <= 0x23FF)) ||		//各种技术符号
			((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}

	/**
	* 过滤emoji 或者 其他非文字类型的字符
	* @param source
	* @return
	*/
	std::string Tools::filterEmoji(const std::string& source) {

		std::wstring temp = utf8_to_unicode(source.c_str());

		std::wstring buf;

		for (int i = 0; i < temp.length(); i++) {
			wchar_t codePoint = temp[i];

			if (!isEmojiCharacter(codePoint)) {
				buf.push_back(codePoint);
			}
		}

		if (buf.empty())
		{
			return "匿名";
		}
		else
		{
			return unicode_to_utf8(buf.c_str());
		}
	}

	//替换一个子字符串以后，重新从开始处替换
	std::string& Tools::replace_all(std::string& str, const std::string& old_value, const std::string& new_value)
	{
		while (true) {
			std::string::size_type   pos(0);
			if ((pos = str.find(old_value)) != std::string::npos)
				str.replace(pos, old_value.length(), new_value);
			else   break;
		}
		return str;
	}


	//从字符串开始处依次替换子字符串
	std::string& Tools::replace_all_distinct(std::string& str, const std::string& old_value, const std::string& new_value)
	{
		for (std::string::size_type pos(0); pos != std::string::npos; pos += new_value.length()) {
			if ((pos = str.find(old_value, pos)) != std::string::npos)
				str.replace(pos, old_value.length(), new_value);
			else   break;
		}
		return str;
	}
}