/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_RichTextView_h__
#define __HN_RichTextView_h__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace std;
using namespace ui;

namespace HN
{
	enum RichType
	{
		TEXT,	// 文本
		IMG,	// 图片
		XML,	// xml
	};

	enum RichFlags {
		ITALICS_FLAG = 1 << 0,          // 斜体文本
		BOLD_FLAG = 1 << 1,             // 粗体文本
		UNDERLINE_FLAG = 1 << 2,        // 下划线
		STRIKETHROUGH_FLAG = 1 << 3,    // 删除线
		URL_FLAG = 1 << 4,              // 超链接
		OUTLINE_FLAG = 1 << 5,          // 描边
		SHADOW_FLAG = 1 << 6,           // 阴影
		GLOW_FLAG = 1 << 7,             // 发光

		NONE_FLAG = 0,
	};

	struct RichTextStruct
	{
		RichType type;			// 富文本类型
		int flags;				// 文本标签
		std::string text;		// 文本内容
		std::string fileName;	// 图片/xml 文件路径名称
		std::string fontName;	// 字体名称
		Color3B color;			// 字体颜色
		float fontSize;			// 字体大小
		std::string url;		// 链接

		RichTextStruct()
		{
			type = TEXT;
			flags = NONE_FLAG;
			text = "";
			fileName = "";
			fontName = "Helvetica";
			color = Color3B::BLACK;
			fontSize = 20;
			url = "";
		}
	};

	class RichTextView : public Layout
	{
	public:
		// 参数为想创建的矩形大小
		static RichTextView* createWithSize(const cocos2d::Size size);

		// 设置内容
		void setString(std::vector<RichTextStruct>& alltext);

	private:
		// 重新调整文本和滑动区大小
		void resize();

	protected:
		bool init(const cocos2d::Size size);

	protected:
		RichTextView();

		virtual ~RichTextView();

	protected:
		ui::ListView* _listView = nullptr;
	};
}


#endif // __HN_RichTextView_h__
