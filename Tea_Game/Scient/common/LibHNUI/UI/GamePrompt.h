/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef GamePrompt_h__
#define GamePrompt_h__

#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "Base/HNLayer.h"
#include "HNTextView.h"

using namespace HN;
using namespace ui;
using namespace cocostudio;

class GamePromptLayer : public HNLayer
{
private:
	//Text*			promptText         = nullptr;
	TextView*		promptText			= nullptr;
	Button*         promptButtonSure1	= nullptr;
	Button*         promptButtonSure2	= nullptr;
	Button*         promptButtonCancel	= nullptr;
public:
	static GamePromptLayer* create(bool bCanSelect = false);

	GamePromptLayer();
	virtual ~GamePromptLayer();

public:
	virtual bool init() override;  

	void showPrompt(const std::string& prompt);

	void setCallBack(std::function<void ()> sure);

	void setCancelCallBack(std::function<void ()> cancel);

	void closeView();
private:
	void closeFunc() override;

	void menuClickCallBack(Ref* pSender);

	void setPrompt(const std::string& prompt);

	void setPrompt(const std::string& prompt, const std::string& font, int size);

	void setPromptCanSelect();

private:
	std::function<void ()> _sure = nullptr;

	std::function<void ()> _cancel = nullptr;

};

#endif // GamePrompt_h__
