/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNRichTextView.h"
#include "HNNetExport.h"
#include <string>

namespace HN
{
	RichTextView* RichTextView::createWithSize(const Size size)
	{
		RichTextView *pRet = new (std::nothrow)RichTextView();
		if (pRet && pRet->init(size))
		{
			pRet->autorelease();
		}
		else
		{
			delete pRet;
			pRet = nullptr;
		}

		return pRet;
	}

	RichTextView::RichTextView()
	{
	}

	RichTextView::~RichTextView()
	{

	}

	bool RichTextView::init(const Size size)
	{
		if (!Layout::init()) return false;

		setCascadeOpacityEnabled(true);
		setIgnoreAnchorPointForPosition(false);
		setContentSize(size);
		setAnchorPoint(Vec2::ANCHOR_MIDDLE);

		_listView = ui::ListView::create();
		_listView->setContentSize(size);
		_listView->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		_listView->setBounceEnabled(true);
		_listView->setDirection(ui::ScrollView::Direction::VERTICAL);
		_listView->setItemsMargin(10.0f);
		_listView->setPosition(size / 2);
		addChild(_listView);

		return true;
	}

	void RichTextView::setString(std::vector<RichTextStruct>& alltext)
	{
		int tag = 0;
		for (auto rich : alltext)
		{
			switch (rich.type)
			{
			case RichType::TEXT:
			{
				RichText* richText = RichText::create();
				_listView->pushBackCustomItem(richText);
				
				// 创建文本Element
				RichElementText* re = RichElementText::create(tag, rich.color, 255, rich.text, rich.fontName, rich.fontSize, rich.flags, rich.url);
				richText->pushBackElement(re);
			} break;
			case RichType::IMG:
			{
				RichText* richText = RichText::create();
				_listView->pushBackCustomItem(richText);

				// 创建图片Element
				auto func = [=](Size imgSize) {
				
					RichElementImage* reimg = RichElementImage::create(tag, Color3B::WHITE, 255, rich.fileName, rich.url, Widget::TextureResType::PLIST);
					richText->pushBackElement(reimg);

					Size size = getContentSize();
					// 宽度比当前控件小25像素，给滑动条留位置
					size.width -= 25;

					if (imgSize.width > size.width)
					{
						float scale = size.width / imgSize.width;
						size.height = imgSize.height * scale;
						reimg->setWidth(size.width);
						reimg->setHeight(size.height);
					}
				};

				// 检查是否网络图片
				if (rich.fileName.find("http://") != rich.fileName.npos)
				{
					// 使用ImageUrl控件下载网络图片，完成后加载RichElementImage
					ImageUrl* imgurl = ImageUrl::createWithRect(getBoundingBox());
					imgurl->loadTextureWithUrl(rich.fileName);
					imgurl->retain();
					imgurl->onLoaded = [=](bool success) {
					
						if (success)
						{
							Texture2D* texture = SpriteFrameCache::getInstance()->getSpriteFrameByName(rich.fileName)->getTexture();

							func(texture->getContentSize());
						}

						resize();
						imgurl->release();
					};
				}
				else
				{
					Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(rich.fileName);
					auto sp = SpriteFrame::create(rich.fileName, Rect(Vec2::ZERO, texture->getContentSize()));
					SpriteFrameCache::getInstance()->addSpriteFrame(sp, rich.fileName);

					func(texture->getContentSize());
				}
			} break;
			case RichType::XML:
			{
				RichText* richText = RichText::createWithXML(rich.fileName.empty() ? rich.text : rich.fileName);
				_listView->pushBackCustomItem(richText);
			} break;
			default:
				break;
			}
			tag++;
		}

		// 重新计算富文本大小和位置
		resize();
	}

	void RichTextView::resize()
	{
		for (auto child : _listView->getItems())
		{
			RichText* richtext = (RichText*)child;
			richtext->ignoreContentAdaptWithSize(false);
			richtext->setContentSize(Size(getContentSize().width - 20, 0));
			richtext->setWrapMode(RichText::WRAP_PER_CHAR);
			richtext->formatText();
		}

		_listView->refreshView();
	}
}