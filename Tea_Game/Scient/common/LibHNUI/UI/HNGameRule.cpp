/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNGameRule.h"
#include "HNNetExport.h"

HNGameRule::HNGameRule()
{
	
}

HNGameRule::~HNGameRule()
{
	
}


bool HNGameRule::init()
{
	if (!HNLayer::init()) return false;

	return true;
}

//std::string HNGameRule::getGameRule()
//{
//	return _gameRule;
//}

void HNGameRule::setRoomConfig(const MSG_GP_O_BuyDeskConfig config)
{

}

void HNGameRule::setGameRule(void* ptr, int size)
{
	if (size <= 0){
		return;
	}

	memset(_gameRule, 0x00, sizeof(char)* MAX_LENGTH);
	memcpy(_gameRule, ptr, size);
}

void* HNGameRule::getGameRule()
{
	return _gameRule;
}

void HNGameRule::getGameRule(char szDeskConfig[])
{

}

BYTE HNGameRule::getMidEnter()
{
	return 0;
}

int HNGameRule::getGameTime()
{
	return 0;
}

//局数
int HNGameRule::getSelectCount()
{
	return 0;
}

//是否定位（0:不定位，1:定位）
int HNGameRule::getSelectLocation()
{
	return 0;
}

//支付类型（0:房主，1:AA）
int HNGameRule::getSelectPay()
{
	return 0;
}