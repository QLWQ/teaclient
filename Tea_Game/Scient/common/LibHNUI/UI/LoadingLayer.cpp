﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "LoadingLayer.h"
#include "Base/HNLayerColor.h"

namespace HN 
{
	static const char* LOADING = "platform/loading_black.png";
	static const char* CANCEL = "platform/common/huangseanniu1.png";
	static const char* TITLE = "platform/common/quxiaoxiao.png";

	static const int LOADING_LAYER_TAG		= 99999999;
	static const int LOADING_LAYER_ZORDER	= 99999999;

	LoadingLayer* LoadingLayer::createLoading(Node* parent, const std::string& text, float fontSize, float timeOut)
	{
		auto loading = (LoadingLayer*)parent->getChildByTag(LOADING_LAYER_TAG);;
		if (nullptr == loading)
		{
			loading = LoadingLayer::create();
			loading->setPosition(Size::ZERO);
			loading->createDialog(parent, text, fontSize, LOADING, timeOut, true);
			parent->addChild(loading, LOADING_LAYER_ZORDER, LOADING_LAYER_TAG);
		}
		loading->setText(text);
		return loading;
	}

	LoadingLayer*  LoadingLayer::createLoading(Node* parent, const std::string& text, float fontSize, const std::string& image, float timeOut)
	{
		auto loading = (LoadingLayer*)parent->getChildByTag(LOADING_LAYER_TAG);;
		if (nullptr == loading)
		{
			loading = LoadingLayer::create();
			loading->setPosition(Size::ZERO);
			loading->createDialog(parent, text, fontSize, image, timeOut);
			parent->addChild(loading, LOADING_LAYER_ZORDER, LOADING_LAYER_TAG);
		}
		loading->setText(text);
		return loading;
	}

	void LoadingLayer::removeLoading(Node* parent)
	{
		CCAssert(nullptr != parent, "parent is null");

		auto loading = (HNLayer*)parent->getChildByTag(LOADING_LAYER_TAG);
		if (loading)
		{
			loading->stopAllActions();
			loading->removeFromParent();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	LoadingLayer::LoadingLayer()
		: _label(nullptr)
	{

	}

	LoadingLayer::~LoadingLayer()
	{

	}

	void LoadingLayer::createDialog(Node* parent, const std::string& text, float fontSize, const std::string& image, float timeOut, bool rotate/* = false*/)
	{
		// 转圈动画	
		auto sprite_load = Sprite::create();
		sprite_load->setPosition(Vec2(_winSize.width / 2, _winSize.height / 2 + sprite_load->getContentSize().width / 2));
		this->addChild(sprite_load, 2);
		if (rotate)
		{
			sprite_load->runAction(RepeatForever::create(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=](){
				sprite_load->setRotation(sprite_load->getRotation() + 30);
			}), nullptr)));
		}
		else sprite_load->runAction(RepeatForever::create(RotateBy::create(0.4f, 90.0f)));

		if (FileUtils::getInstance()->isFileExist(image))
		{
			sprite_load->setTexture(image);
			quicklyShade(parent, LOADING_LAYER_ZORDER);
		}

		// 提示字样
		_label = Label::createWithSystemFont(text, "", fontSize);
		//_label->setColor(Color3B(255, 215, 72));
		this->addChild(_label, 2);
		_label->setPosition(Vec2(_winSize.width / 2, sprite_load->getPositionY() - sprite_load->getContentSize().height / 2 - 30));

		// 取消按钮
		_btn_cancel = Button::create(CANCEL, "", "");
		_btn_cancel->setPosition(Vec2(_label->getPositionX(), _label->getPositionY() - 100));
		addChild(_btn_cancel, 2);
		_btn_cancel->setVisible(false);
		_btn_cancel->setScale(0.8f);
		_btn_cancel->addClickEventListener([=](Ref*){
			if (_func) _func();
			this->removeFromParent();
		});

		auto img_title = ImageView::create(TITLE);
		img_title->setPosition(Vec2(_btn_cancel->getContentSize().width / 2, _btn_cancel->getContentSize().height * 0.6f));
		_btn_cancel->addChild(img_title);

		//增加超时取消
		auto action = CCSequence::create(CCDelayTime::create(timeOut), CCCallFunc::create([=](){
			auto parent = getParent();
			if (parent)
				LoadingLayer::removeLoading(parent);
		}), nullptr);

		runAction(action);
	}

	void LoadingLayer::setText(const std::string& text)
	{
		_label->setString(text);
	}

	void LoadingLayer::setCancelCallBack(std::function<void()> cancel)
	{
		_func = cancel;
		_btn_cancel->setVisible(true);
	}

	bool LoadingLayer::init()
	{
		if (!HNLayer::init())
		{
			return false;
		}

		auto listener = EventListenerTouchOneByOne::create();
		listener->setSwallowTouches(true);
		listener->onTouchBegan = [&](Touch* touch, Event* event)
		{
			auto target = static_cast<Sprite*>(event->getCurrentTarget());      
			Point locationInNode = target->convertToNodeSpace(touch->getLocation());
			Rect rect = Rect(0, 0, _winSize.width, _winSize.height);
			return rect.containsPoint(locationInNode);
		};
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

		return true;
	}
}