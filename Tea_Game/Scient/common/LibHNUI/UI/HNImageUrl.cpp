/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNImageUrl.h"
#include <string>
#include "json/rapidjson.h"
#include "json/document.h"
#include "HNNetExport.h"
using namespace network;

namespace HN
{
	ImageUrl* ImageUrl::createWithRect(const Rect rect)
	{
		ImageUrl *pRet = new (std::nothrow)ImageUrl();
		if (pRet && pRet->init(rect))
		{
			pRet->autorelease();
		}
		else
		{
			delete pRet;
			pRet = nullptr;
		}

		return pRet;
	}

	ImageUrl::ImageUrl()
	{
	}

	ImageUrl::~ImageUrl()
	{
		//HNUserHeadHttpRequest::getInstance()->removeObserver(this);
		//HNHttpRequest::getInstance()->removeObserver(this);
	}

	bool ImageUrl::init(const cocos2d::Rect rect)
	{
		if (!ImageView::init()) return false;

		_size = Size(rect.size.width, rect.size.height);

		setTouchEnabled(true);

		setCascadeOpacityEnabled(true);

		setIgnoreAnchorPointForPosition(false);
		
		return true;
	}

	bool ImageUrl::isAlreadyLoaded(const std::string& url)
	{
		return SpriteFrameCache::getInstance()->getSpriteFrameByName(url) != nullptr;
	}

	void ImageUrl::loadTexture(const std::string& fileName, Widget::TextureResType restype/* = Widget::TextureResType::LOCAL*/)
	{
		ImageView::loadTexture(fileName, restype);

		Texture2D* texture = nullptr;
		if (restype == Widget::TextureResType::LOCAL)
		{
			texture = Director::getInstance()->getTextureCache()->addImage(fileName);
		}
		else
		{
			texture = SpriteFrameCache::getInstance()->getSpriteFrameByName(fileName)->getTexture();
		}

		setScale(_size.width / texture->getContentSize().width, _size.height / texture->getContentSize().height);
	}

	void ImageUrl::onResponseHeadUrl(int UserID)
	{
		std::string url = StringUtils::format("http://alipay.qp888m.com/app/get_head_url.php?userid=%d",UserID);
		HttpRequest* request = new (std::nothrow) HttpRequest();
		request->setUrl(url);
		request->setRequestType(HttpRequest::Type::GET);
		request->setResponseCallback(CC_CALLBACK_2(ImageUrl::onResponseUrl, this));
		request->setTag("requestUserHeadUrl");

		HttpClient::getInstance()->sendImmediate(request);
		request->release();
		/*HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestUserHeadUrl", HttpRequest::Type::GET, url);*/
	}

	void ImageUrl::loadTextureWithUrl(const std::string& url)
	{
		if (url.empty()) {
			if (onLoaded) onLoaded(false);
			return;
		}

		// 纹理已经存在，则直接加载
		if (isAlreadyLoaded(url)) {
			loadTexture(url, Widget::TextureResType::PLIST);
			if (onLoaded) onLoaded(true);
			return;
		}

		_url = url;

		/*
		每次添加监听者，都会导致每次收到http回复，所有监听都会收到返回值，导致requestName和url不匹配
		HNUserHeadHttpRequest::getInstance()->addObserver(this);
		HNUserHeadHttpRequest::getInstance()->request(url, HttpRequest::Type::GET, url);
		*/

		HttpRequest* request = new (std::nothrow) HttpRequest();
		request->setUrl(url.c_str());
		request->setRequestType(HttpRequest::Type::GET);
		request->setResponseCallback(CC_CALLBACK_2(ImageUrl::onHttpResponse, this));
		request->setTag(url.c_str());
		HttpClient::getInstance()->sendImmediate(request);
		request->release();
	}

	std::string ImageUrl::getImageData()
	{
		std::string data;
		data.insert(data.begin(), _imageData.begin(), _imageData.end());
		return data;
	}

	void ImageUrl::onResponseUrl(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response)
	{
		std::string requestName = response->getHttpRequest()->getTag();
		std::vector<char> *data = response->getResponseData();
		int data_length = data->size();
		std::string responseData;
		for (int i = 0; i < data_length; ++i)
		{
			responseData += (*data)[i];
		}
		responseData += '\0';
		if (requestName.compare("requestUserHeadUrl") == 0)
		{
			rapidjson::Document doc;
			doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

			if (doc.HasParseError() || !doc.IsObject())
			{
				return;
			}
			//存在微信头像
			int code = doc["IsWX"].GetInt();
			if (code == 1)
			{
				std::string url = doc["HeadUrl"].GetString();
				loadTextureWithUrl(url);
			}
		}
	}

	void ImageUrl::onHttpResponse(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response)   //(const std::string& requestName, bool isSucceed, std::vector<char> &responseData)
	{
		std::string requestName = response->getHttpRequest()->getTag();
		bool isSucceed = response->isSucceed();
		std::vector<char> responseData;

		if (isSucceed)
		{
			responseData = *response->getResponseData();
		}
		do
		{
			CC_BREAK_IF(requestName.compare(_url) != 0);

			CC_BREAK_IF(!isSucceed);

			// 存储
			_imageData.clear();
			_imageData.assign(responseData.begin(), responseData.end());

			Image* image = new Image();
			bool isOk = image->initWithImageData((unsigned char*)responseData.data(), responseData.size());
			if (!isOk)
			{
				image->release();
				break;
			}

			Texture2D* texture = Director::getInstance()->getTextureCache()->addImage(image, _url);
			image->release();

			CC_BREAK_IF(texture == nullptr);

			auto sf = SpriteFrame::createWithTexture(texture, Rect(Vec2::ZERO, texture->getContentSize()));
			SpriteFrameCache::getInstance()->addSpriteFrame(sf, _url);

			loadTexture(_url, Widget::TextureResType::PLIST);

			setScale(_size.width / texture->getContentSize().width, _size.height / texture->getContentSize().height);

			if (onLoaded) onLoaded(true);
		} while (0);

		if (onLoaded) onLoaded(false);
	}
}
