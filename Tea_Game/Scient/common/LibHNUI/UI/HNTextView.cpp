/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNTextView.h"
#include "HNNetExport.h"
#include <string>

namespace HN
{
	TextView* TextView::createWithSize(const Size size)
	{
		TextView *pRet = new (std::nothrow)TextView();
		if (pRet && pRet->init(size))
		{
			pRet->autorelease();
		}
		else
		{
			delete pRet;
			pRet = nullptr;
		}

		return pRet;
	}

	TextView::TextView()
	{
	}

	TextView::~TextView()
	{

	}
	bool TextView::init(const Size size)
	{
		if (!Layout::init()) return false;

		setCascadeOpacityEnabled(true);
		setIgnoreAnchorPointForPosition(false);
		setContentSize(size);
		setAnchorPoint(Vec2::ANCHOR_MIDDLE);

		_label = Label::create();
		_label->setSystemFontSize(_fontSize);
		_label->setTextColor(Color4B::BLACK);
		_label->setAlignment(TextHAlignment::CENTER);
		_label->setPositionX(size.width / 2);
		_label->setDimensions(size.width - 5, 0);
		_fontName = _label->getSystemFontName();

		_scrollView = ui::ScrollView::create();
		_scrollView->setContentSize(size);
		_scrollView->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		_scrollView->setTouchEnabled(true);
		_scrollView->setBounceEnabled(true);
		_scrollView->setDirection(ui::ScrollView::Direction::VERTICAL);
		_scrollView->setScrollBarPositionFromCornerForVertical(Vec2(3, 20));
		_scrollView->setScrollBarWidth(6);
		_scrollView->setInnerContainerSize(size);
		_scrollView->setPosition(size / 2);
		addChild(_scrollView);

		_scrollView->addChild(_label);

		return true;
	}

	void TextView::setString(const std::string& text)
	{
		_label->setString(text);

		resize();
	}

	const std::string& TextView::getString()const
	{
		return _label->getString();
	}

	void TextView::setFontSize(float size)
	{
		if (_type == Text::Type::SYSTEM)
		{
			_label->setSystemFontSize(size);
		}
		else
		{
			TTFConfig config = _label->getTTFConfig();
			config.fontSize = size;
			_label->setTTFConfig(config);
		}
		_fontSize = size;
		resize();
	}

	float TextView::getFontSize()const
	{
		return _fontSize;
	}

	void TextView::setFontName(const std::string& name)
	{
		if (FileUtils::getInstance()->isFileExist(name))
		{
			TTFConfig config = _label->getTTFConfig();
			config.fontFilePath = name;
			config.fontSize = _fontSize;
			_label->setTTFConfig(config);
			_type = Text::Type::TTF;
		}
		else
		{
			_label->setSystemFontName(name);
			_type = Text::Type::SYSTEM;
		}
	}

	const std::string& TextView::getFontName()const
	{
		return _fontName;
	}

	void TextView::setTextColor(const Color4B color)
	{
		_label->setTextColor(color);
	}

	const Color4B& TextView::getTextColor() const
	{
		return _label->getTextColor();
	}

	void TextView::setAlignment(TextHAlignment hAlignment, TextVAlignment vAlignment)
	{
		_label->setAlignment(hAlignment, vAlignment);
	}

	void TextView::resize()
	{
		Size size = _label->getContentSize();
		if (size.height > _scrollView->getContentSize().height)
		{
			_scrollView->setTouchEnabled(true);
			_scrollView->setInnerContainerSize(size);
			_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			_label->setPositionY(size.height);
		}
		else if (_label->getVerticalAlignment() == TextVAlignment::TOP)
		{
			_scrollView->setTouchEnabled(false);
			_scrollView->setInnerContainerSize(getContentSize());
			_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			_label->setPositionY(getContentSize().height);
		}
		else
		{
			_scrollView->setTouchEnabled(false);
			_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			_label->setPositionY(getContentSize().height / 2);
		}
	}
}