/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_GameTextField_h__
#define _HN_GameTextField_h__

#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "Base/HNLayer.h"
#include "HNTextView.h"

using namespace HN;
using namespace ui;
using namespace cocostudio;

class GameTextField : public HNLayer
{
private:
	Text*		_text_prompt = nullptr;
	TextField*	_textfield	= nullptr;
	Button*		_btn_sure	= nullptr;
	Button*		_btn_cancel	= nullptr;

public:
	static GameTextField* create();

	GameTextField();
	virtual ~GameTextField();

public:
	virtual bool init() override;  

	void showPrompt(const std::string& prompt, int maxLength, std::function<void(const std::string& text)> func);

	void close();

private:
	void menuClickCallBack(Ref* pSender);

	void setPrompt(const std::string& prompt);

private:
	std::function<void (const std::string& text)> _func = nullptr;

};

#endif // _HN_GameTextField_h__
