/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__HNDatePickerView__
#define __HN__HNDatePickerView__

#include <iostream>
#include "HNPickerView.h"


class HNDatePickerView : public Node, public HNPickerViewDataSource, public HNPickerViewDelegate {
    
public:

    enum class Mode : int
    {
        Time = 0,       // Displays hour, minute, and optionally AM/PM designation depending on the locale setting
        Date,           // Displays month, day, and year depending on the locale setting
		YearDayAndTime, // Displays month, day, year, and hour depending on the locale setting
        DateAndTime,    // Displays date, hour, minute, and optionally AM/PM designation depending on the locale setting
        CountDownTimer, // Displays hour and minute
    };
    
public:

	CC_LISTENING_FUNCTION(void(tm*), SelectRow)
	CC_SYNTHESIZE(float, _cellHeight, CellHeight)
        
public:
    static HNDatePickerView* create(HNDatePickerView::Mode m_mode);
    
	HNDatePickerView(HNDatePickerView::Mode m_mode);
    virtual ~HNDatePickerView();
    
    virtual bool init();
	virtual void onEnterTransitionDidFinish();
	virtual void onExitTransitionDidStart();
    
	void setTextColor(const Color4B &color);
	void setTextColorForSelected(const Color4B &color);
    void setContentSize(cocos2d::Size size);
    void setBackGroundViewForSelected(Layout* view);
	void setBeginAndEnd(bool isBegin = true);
        
public:
    void setDate(int year, int month, int day, bool animated);

protected:
    virtual unsigned int numberOfComponentsInPickerView(HNPickerView* pickerView);
    virtual unsigned int numberOfRowsInComponent(HNPickerView* pickerView, unsigned int component);
    virtual float widthForComponent(HNPickerView* pickerView, unsigned int component);
    virtual float rowHeightForComponent(HNPickerView* pickerView, unsigned int component);
    std::string titleForRow( unsigned int row, unsigned int component);
    
    virtual void didSelectRow(HNPickerView* pickerView, unsigned int row, unsigned int component);
    void setMode(HNDatePickerView::Mode mode);
    
private:
        
	HNPickerView* m_pPickerView;
    tm m_tTM;
    HNDatePickerView::Mode m_eMode;
    bool isSetDate;
};

#endif /* defined(__HN__HNDatePickerView__) */
