/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNPickerView.h"
#include "HNCommon/HNConverCode.h"

using namespace HN;

HNPickerView::HNPickerView()
: m_delegate(nullptr)
, m_dataSource(nullptr)
, m_fontSizeNormal(20)
, m_fontSizeSelected(22)
, m_fontColorNormal(Color4B(0, 0, 0, 255))
, m_fontColorSelected(Color4B(0, 0, 0, 255))
, m_separateColor(Color3B(127, 127, 127))
, m_displayRow(7)
{
   // this->setDisplayRange(false);
}

HNPickerView::~HNPickerView()
{

}

HNPickerView* HNPickerView::create()
{
	HNPickerView* view = new HNPickerView();
    if (view && view->init())
    {
        view->autorelease();
    } else {
        CC_SAFE_DELETE(view);
    }
    return view;
}

bool HNPickerView::init()
{
	if (!Layout::init()) return false;

    return true;
}


void HNPickerView::onEnterTransitionDidFinish()
{
	Layout::onEnterTransitionDidFinish();
    
	Director::getInstance()->getScheduler()->schedule([=](float fDelta)
    {
        this->reloadAllComponents();
    }, this, 0.0f, 0, 0.0f, false, "first_reload_data");
}

void HNPickerView::onExitTransitionDidStart()
{
	Layout::onExitTransitionDidStart();
}

void HNPickerView::setContentSize(Size size)
{
    if (_contentSize.equals(size)) return;
	Layout::setContentSize(size);
    if (_running)
    {
        std::vector<int> selected = m_selected;
        
        for (size_t i=0; i<selected.size(); i++)
        {
            if (TableView* tableView = m_tableViews.at(i))
            {
                int maxRow = 0;
                if (m_obNumberOfRowsInComponent)
                {
                    maxRow = m_obNumberOfRowsInComponent((unsigned int)i);
                }
                else if (m_dataSource)
                {
                    maxRow = m_dataSource->numberOfRowsInComponent(this, (unsigned int)i);
                }
                unsigned int rowHeight = 0;
                if (m_obHeightForComponent)
                {
                    rowHeight = m_obHeightForComponent((unsigned int)i);
                }
                else if (m_dataSource)
                {
                    rowHeight = m_dataSource->rowHeightForComponent(this, (unsigned int)i);
                }
                Vec2 offset = Vec2::ZERO;
                int row = selected.at(i);
                if (maxRow <= m_displayRow[i])
                {
                    m_selected[i] = row ;
                    offset.y = row * rowHeight;
                    tableView->setContentOffset(-offset, false);
                }
                else
                {
                    m_selected[i] = row;
                    offset.y = m_selected[i] * rowHeight + rowHeight / 2 + tableView->getContentSize().height / 2;
                    tableView->setContentOffset(-offset, false);
                }
            }
        }
    }
}

float HNPickerView::calcTotalWidth(unsigned int component)
{
    float total = 0;
    for (int i=0; i<component; i++)
    {
        unsigned int width = 0;
        if (m_obWidthForComponent)
        {
            width = m_obWidthForComponent(i);
        }
        else if (m_dataSource)
        {
            width = m_dataSource->widthForComponent(this, i);
        }
        
        total += width;
    }
    return total;
}

void HNPickerView::reloadAllComponents()
{
    if (!this->isRunning()) return;
    
    m_tableViews.clear();
    m_selected.clear();
    m_componentsIndex.clear();
    m_displayRow.clear();
    
    this->removeAllChildren();
    
    // reload data
    unsigned int component = 1;
    if (m_obNumberOfComponents)
    {
        component = m_obNumberOfComponents();
    }
    else if (m_dataSource)
    {
        component = m_dataSource->numberOfComponentsInPickerView(this);
    }
    
    float total_width = calcTotalWidth(component);
    m_componentsIndex.resize(component);
    m_componentOffsetX.resize(component);
    m_displayRow.resize(component);
    float start_x = _contentSize.width / 2 - total_width / 2;
    for (int i=0; i<component; i++)
    {
        m_selected.push_back(0);
        m_componentsIndex[i] = std::vector<int>();
        m_componentOffsetX[i] = start_x;
    
        unsigned int rowHeight = 0;
        if (m_obHeightForComponent)
        {
            rowHeight = m_obHeightForComponent(i);
        }
        else if (m_dataSource)
        {
            rowHeight = m_dataSource->rowHeightForComponent(this, i);
        }
        
        m_displayRow[i] = _contentSize.height / rowHeight;
        unsigned int tableHeight = MAX(rowHeight, rowHeight * m_displayRow[i]) ;
        unsigned int tableWidth = 0;
        if (m_obWidthForComponent)
        {
            tableWidth = m_obWidthForComponent(i);
        }
        else if (m_dataSource)
        {
            tableWidth = m_dataSource->widthForComponent(this, i);
        }
        
        if (m_displayRow[i] % 2 == 0)
        {
            m_displayRow[i] += 1;
        }
        
        // create tableview       
        float start_y = _contentSize.height/2 - tableHeight/2;
        
        TableView* tableView = TableView::create(this, Size(tableWidth, tableHeight));
		tableView->setPosition(Vec2(start_x, start_y));
		tableView->setTouchEnabled(true);
		tableView->setDelegate(this);
		tableView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		this->addChild(tableView, 1, i);
        m_tableViews.pushBack(tableView);
        
        // create highlight
        Size selectSize = Size(tableWidth, rowHeight);
        
        Layout* selectedView = nullptr;
        if (m_obViewForSelected)
        {
            selectedView = m_obViewForSelected(i, selectSize);
        }
        else if (m_dataSource)
        {
            selectedView = m_dataSource->viewForSelect(this, i, selectSize);
        }
        
        if (selectedView == nullptr)
        {
			auto layout = Layout::create();
			layout->setContentSize(Size(selectSize.width, selectSize.height));
			layout->setPosition(Vec2(start_x, _contentSize.height / 2 - rowHeight / 2));
			layout->setTouchEnabled(false);
			this->addChild(layout, 2);
        }
        else
        {
			selectedView->setContentSize(Size(selectSize.width, selectSize.height));
			selectedView->setPosition(Vec2(start_x, _contentSize.height / 2 - rowHeight / 2));
			selectedView->setTouchEnabled(false);
			selectedView->setOpacity(100);
            this->addChild(selectedView, 2);
        }
        
        start_x += tableWidth;
    }

	for (int i = 0; i < m_tableViews.size(); ++i)
	{
		this->reloadComponent(0, i, true);
	}
}


void HNPickerView::reloadComponent(unsigned int _row,unsigned int component, bool bReloadData)
{
    // reload component
    unsigned int row = 0;
    if (m_obNumberOfRowsInComponent)
    {
        row = m_obNumberOfRowsInComponent(component);
    }
    else if (m_dataSource)
    {
        row = m_dataSource->numberOfRowsInComponent(this, component);
    }
    
    //int head = m_displayRow[component]/2;
    //int foot = m_displayRow[component]/2;
	int head = row / 2;
	int foot = row / 2;
    if (row <= m_displayRow[component])
    {
		/*row += (head + foot);
		m_componentsIndex[component].resize(row);
		for (int i=0; i < row; i++)
		{
			if (i < head)
			{
				m_componentsIndex[component][i] = -1;
			}
			else if (i >= row - foot)
			{
				m_componentsIndex[component][i] = -1;
			}
			else
			{
				m_componentsIndex[component][i] = i - head;
			}
		}*/

		m_componentsIndex[component].resize(row);
		for (int i = 0; i < row; i++)
		{
			m_componentsIndex[component][i] = i;
		}
    }
    else
    {
        int cycle = 3;
        m_componentsIndex[component].resize(row * 3);
        while (cycle--)
        {
            for (int i=0; i<row; i++)
            {
                m_componentsIndex[component][i + cycle * row] = i;
            }
        }
    }
    
    if (bReloadData)
    {
        // reload table view
		TableView* view = m_tableViews.at(component);

        view->reloadData();
    }

	// reset selected index
	this->selectRow(_row, component, false);
}

Node* HNPickerView::viewForRowInComponent(int component, int row, Size size)
{
    int index = m_componentsIndex[component][row];
    if (index == -1)
    {
        return nullptr;
    }
    
    Node* view = nullptr;
    if (m_obViewForRow)
    {
        view = m_obViewForRow(index, component);
    }
    else if (m_dataSource)
    {
        view = m_dataSource->viewForRow(this, index, component);
    }
    
    if (!view)
    {
        do
        {
            std::string title;
            if (m_obTitleForRow)
            {
                title = m_obTitleForRow(index, component);
            }
            else if (m_dataSource)
            {
                title = m_dataSource->titleForRow(this, index, component);
            }
            CC_BREAK_IF(title.empty());
            
            Label* label = Label::create();
            label->setString(title);
            label->setTextColor(m_fontColorNormal);
            label->setSystemFontSize(m_fontSizeNormal);
            label->setAlignment(TextHAlignment::CENTER);
			label->setPosition(size / 2);
            
            view = label;
            
        } while (0);
    }
    
    return view;
}

TableViewCell* HNPickerView::tableCellAtIndex(TableView *table, ssize_t idx)
{
    TableViewCell* cell = table->dequeueCell();
    if (cell == nullptr)
    {
        cell = TableViewCell::create();
    }
	else
	{
		cell->removeAllChildren();
	}

	cell->setIdx(idx);

	int component = table->getTag();
	if (component != -1)
	{
		Node* view = viewForRowInComponent((unsigned int)component, idx, tableCellSizeForIndex(table, idx));
		if (view) cell->addChild(view);
	}
    
    return cell;
}

ssize_t HNPickerView::numberOfCellsInTableView(TableView *table)
{
	int component = table->getTag();
	if (-1 == component) return 0;
	return m_componentsIndex[component].size();
}

void HNPickerView::tableCellTouched(TableView* table, TableViewCell* cell)
{

}

Size HNPickerView::tableCellSizeForIndex(TableView *table, ssize_t idx)
{
    unsigned int component = (unsigned int)m_tableViews.getIndex(table);
    unsigned int height = 0;
	unsigned int width = 0;
    if (m_obHeightForComponent)
    {
		height = m_obHeightForComponent(component);
    }
    else if (m_dataSource)
    {
		height = m_dataSource->rowHeightForComponent(this, component);
    }

	if (m_obWidthForComponent)
	{
		width = m_obWidthForComponent(component);
	}
	else if (m_dataSource)
	{
		width = m_dataSource->widthForComponent(this, component);
	}
    
    return Size(width, height);
}

void HNPickerView::scrollViewDidScroll(extension::ScrollView* view)
{
	_isScroll = true;
	stopActionByTag(100);
	auto action = Sequence::create(DelayTime::create(0.02f), CallFunc::create([&]() {

		_isScroll = false;
	}), nullptr);
	action->setTag(100);
	runAction(action);
}

void HNPickerView::selectRow(unsigned int row, unsigned int component, bool animated)
{
	TableView* tableView = m_tableViews.at(component);
    if (tableView)
    {
        unsigned int maxRow = 0;
        if (m_obNumberOfRowsInComponent)
        {
            maxRow = m_obNumberOfRowsInComponent(component);
        }
        else if (m_dataSource)
        {
            maxRow = m_dataSource->numberOfRowsInComponent(this, component);
        }

        unsigned int rowHeight = 0;
        if (m_obHeightForComponent)
        {
            rowHeight = m_obHeightForComponent(component);
        }
        else if (m_dataSource)
        {
            rowHeight = m_dataSource->rowHeightForComponent(this, component);
        }
        
        if (row < maxRow)
        {
            Vec2 offset = Vec2::ZERO;
            if (maxRow <= m_displayRow[component])
            {
                m_selected[component] = row + m_displayRow[component] / 2;
                offset.y = row * rowHeight;
               tableView->setContentOffset(-offset, false);
            }
            else
            {
                m_selected[component] = maxRow + row;
				float y = tableView->getContentSize().height - m_selected[component] * rowHeight - getContentSize().height / 2 - rowHeight / 2;
				offset.y = -y;
                tableView->setContentOffset(offset, false);
            }
        }
    }
}

int HNPickerView::selectedRowInComponent(unsigned int component)
{
    return m_selected[component];
}

void HNPickerView::visit(Renderer *renderer, const Mat4 &parentTransform, uint32_t parentFlags)
{
    for (int i = 0; i < m_tableViews.size(); i++)
    {
        // cycle data
        TableView* tableView = (TableView*)m_tableViews.at(i);
        Vec2 offset = tableView->getContentOffset();
        unsigned int component = (unsigned int)m_tableViews.getIndex(tableView);
        
        unsigned int row = 0;
        if (m_obNumberOfRowsInComponent)
        {
            row = m_obNumberOfRowsInComponent(component);
        }
        else if (m_dataSource)
        {
            row = m_dataSource->numberOfRowsInComponent(this, component);
        }
        
        unsigned int rowHeight = 0;
        if (m_obHeightForComponent)
        {
            rowHeight = m_obHeightForComponent(i);
        }
        else if (m_dataSource)
        {
            rowHeight = m_dataSource->rowHeightForComponent(this, i);
        }
        
        if (row > m_displayRow[component])
        {
			int y = row * 2 * rowHeight;
			int temp = rowHeight * m_componentsIndex[component].size() - tableView->getBoundingBox().size.height;
            if (offset.y > 0)
            {
                offset.y -= y;
                tableView->setContentOffset(offset, false);
            }
            else if (offset.y < -temp)
            {
                offset.y += y;
				tableView->setContentOffset(offset, false);
            }
        }
        
        
        // set opacity
        int offset_y = abs((int)offset.y);
        int remainder = offset_y % rowHeight;
        int index = offset_y / rowHeight;
        
        if (remainder >= rowHeight * 0.5)
        {
            index++;
        }
        
		/*for (int i = index - 1; i < index + m_displayRow[component] + 2; i++)
		{
			TableViewCell* cell = tableView->cellAtIndex(i);
			if (cell)
			{
				float y = cell->getPosition().y - offset_y + cell->getContentSize().height / 2;
				float mid = tableView->getContentSize().height / 2;
				float length = fabs(mid - y);
				float op = (fabs(length - mid)) / mid;
				op = powf(op, 2);
				op = MAX(op, 0.1f);
				cell->setOpacity(op);
			}
		}*/
        
        // fixed position in the middle
		if (!tableView->isDragging() && !_isScroll)
        {
            if (remainder > rowHeight / 2)
            {
				float y = index * rowHeight;
                offset.y = -y;
                tableView->setContentOffset(offset, true);
            }
            else if (remainder > 0)
            {
				float y = index * rowHeight;
				offset.y = -y;
                tableView->setContentOffset(offset, true);
            }
            else
            {
				int maxrow = tableView->getContentSize().height / rowHeight;
                // set selected when stop scrolling.              
				int selected = maxrow - index - m_displayRow[component] / 2;

				if (m_displayRow[component] % 2 != 0)
				{
					selected--;
				}

				if (maxrow < m_displayRow[component])
				{
					selected = index;
				}
                
                if (m_selected[component] != selected)
                {
                    m_selected[component] = selected;
                    if (m_obDidSelectRow)
                    {
                        m_obDidSelectRow(m_componentsIndex[component][m_selected[component]], component);
                    }
                    else if (m_delegate)
                    {
                        m_delegate->didSelectRow(this, m_componentsIndex[component][m_selected[component]], component);
                    }
                }
				else
				{
					if (!_isCallback)
					{
						if (m_obDidSelectRow)
						{
							m_obDidSelectRow(m_componentsIndex[component][m_selected[component]], component);
						}
						else if (m_delegate)
						{
							m_delegate->didSelectRow(this, m_componentsIndex[component][m_selected[component]], component);
						}
						_isCallback = true;
					}
				}
            }
            
        }
    }
    
    Layout::visit(renderer, parentTransform, parentFlags);
}
