/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__HNPickerView__
#define __HN__HNPickerView__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include <vector>
#include "ui/UILayout.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace ui;

#define CC_LISTENING_FUNCTION(FUNCTION, VARNAME)\
protected: std::function<FUNCTION> m_ob##VARNAME{nullptr};\
public: void on##VARNAME(const std::function<FUNCTION>& var){ m_ob##VARNAME = var; }

#define CC_LISTENING_FUNCTION_D(FUNCTION, VARNAME)\
protected: std::function<FUNCTION> m_ob##VARNAME{nullptr};\
public: void on##VARNAME(const std::function<FUNCTION>& var);

class HNPickerView;

class HNPickerViewDelegate {

public:
    virtual void didSelectRow(HNPickerView* pickerView, unsigned int row, unsigned int component) {}
};

class HNPickerViewDataSource {
    
public:
    virtual ~HNPickerViewDataSource() {};

    virtual unsigned int numberOfComponentsInPickerView(HNPickerView* pickerView) = 0;

    virtual unsigned int numberOfRowsInComponent(HNPickerView* pickerView, unsigned int component) = 0;
    
    virtual float widthForComponent(HNPickerView* pickerView, unsigned int component) {return 0;}

    virtual float rowHeightForComponent(HNPickerView* pickerView, unsigned int component) {return 0;}

    virtual const char* titleForRow(HNPickerView* pickerView, unsigned int row, unsigned int component) {return NULL;}

    virtual Layout* viewForRow(HNPickerView* pickerView, unsigned int row, unsigned int component) {return NULL;}

    virtual Layout* viewForSelect(HNPickerView* pickerView, unsigned int component, const cocos2d::Size& size) {return NULL;}
};

class HNPickerView : public Layout, public TableViewDataSource, public TableViewDelegate
{
public:
    // event listeners. If these functions are set, the corresponding function of CAPickerViewDataSource will fail.
    CC_LISTENING_FUNCTION(unsigned int(), NumberOfComponents);
    
    CC_LISTENING_FUNCTION(unsigned int(int component), NumberOfRowsInComponent);
    
    CC_LISTENING_FUNCTION(unsigned int(int component), WidthForComponent);
    
    CC_LISTENING_FUNCTION(unsigned int(unsigned int component), HeightForComponent);
    
    CC_LISTENING_FUNCTION(std::string(unsigned int row, unsigned int component), TitleForRow);
    
    CC_LISTENING_FUNCTION(Layout*(unsigned int row, unsigned int component), ViewForRow);
    
    CC_LISTENING_FUNCTION(Layout*(unsigned int component, cocos2d::Size size), ViewForSelected);
    
    // event listeners. If these functions are set, the corresponding function of CAPickerViewDelegate will fail.
    CC_LISTENING_FUNCTION(void(unsigned int row, unsigned int component), DidSelectRow);
    
public:
    
    HNPickerView();
    virtual ~HNPickerView();
    
    static HNPickerView* create();
    
    virtual bool init();

	virtual void onEnterTransitionDidFinish();

	virtual void onExitTransitionDidStart();

	virtual void visit(Renderer *renderer, const Mat4 &parentTransform, uint32_t parentFlags) override;
    

    // Reloading whole view or single component
    virtual void reloadAllComponents();
    virtual void reloadComponent(unsigned int row, unsigned int component, bool bReload = true);
    
    // selection. in this case, it means showing the appropriate row in the middle
    // animated: scrolls the specified row to center. default is false
    virtual void selectRow(unsigned int row, unsigned int component, bool animated = false);
    
    // returns selected row. -1 if nothing selected
    virtual int selectedRowInComponent(unsigned int component);

	virtual void setContentSize(cocos2d::Size size);

	CC_SYNTHESIZE(HNPickerViewDelegate*, m_delegate, PickerViewDelegate);
	CC_SYNTHESIZE(HNPickerViewDataSource*, m_dataSource, PickerViewDataSource);
    
    CC_SYNTHESIZE(float, m_fontSizeNormal, FontSizeNormal);
    CC_SYNTHESIZE(float, m_fontSizeSelected, FontSizeSelected);
    CC_SYNTHESIZE_PASS_BY_REF(Color4B, m_fontColorNormal, FontColorNormal);
    CC_SYNTHESIZE_PASS_BY_REF(Color4B, m_fontColorSelected, FontColorSelected);
    CC_SYNTHESIZE_PASS_BY_REF(Color3B, m_separateColor, SeparateColor);

private:
    float calcTotalWidth(unsigned int component);
	Node* viewForRowInComponent(int component, int row, cocos2d::Size size);
    
protected:
	// 创建cell
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx) override;

	// Cell个数
	virtual ssize_t numberOfCellsInTableView(TableView *table) override;

	// Cell大小
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, ssize_t idx) override;

	virtual void tableCellTouched(TableView* table, TableViewCell* cell) override;

	// 滚动监听
	virtual void scrollViewDidScroll(extension::ScrollView* view) override;
    
private:

	Vector<TableView*> m_tableViews;
    std::vector<int> m_selected;
    std::vector< std::vector<int> > m_componentsIndex;
    std::vector<float> m_componentOffsetX;
    std::vector<int> m_displayRow;
	bool _isScroll = false;
	bool _isCallback = false;
};

#endif /* defined(__HN__HNPickerView__) */
