/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNDatePickerView.h"
#include <sstream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include "HNCalendar.h"
#include "HNCommon/HNConverCode.h"

using namespace HN;

HNDatePickerView::HNDatePickerView(HNDatePickerView::Mode m_mode)
: m_pPickerView(nullptr)
, m_eMode(m_mode)
, isSetDate(false)
, _cellHeight(60)
{

}

HNDatePickerView::~HNDatePickerView()
{
    CC_SAFE_RELEASE(m_pPickerView);
}

HNDatePickerView* HNDatePickerView::create(HNDatePickerView::Mode m_mode)
{
	HNDatePickerView* view = new HNDatePickerView(m_mode);
    if (view && view->init()) {
        view->autorelease();
    } else {
        CC_SAFE_DELETE(view);
    }
    return view;
}

bool HNDatePickerView::init()
{
    if (!Node::init()) {
        return false;
    }
    m_pPickerView = new HNPickerView();
    m_pPickerView->setFontSizeNormal(28);
    m_pPickerView->setFontSizeSelected(28);
	m_pPickerView->setPickerViewDataSource(this);
	m_pPickerView->setPickerViewDelegate(this);
	m_pPickerView->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    this->addChild(m_pPickerView);

	m_pPickerView->onTitleForRow(CC_CALLBACK_2(HNDatePickerView::titleForRow, this));
    
	std::time_t t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    auto tm = std::localtime(&t);
	m_tTM.tm_sec = tm->tm_sec;
	m_tTM.tm_min = tm->tm_min;
	m_tTM.tm_hour = tm->tm_hour;
	m_tTM.tm_mday = tm->tm_mday;
	m_tTM.tm_mon = tm->tm_mon;
	m_tTM.tm_year = tm->tm_year;
	m_tTM.tm_wday = tm->tm_wday;
	m_tTM.tm_yday = tm->tm_yday;
	m_tTM.tm_isdst = tm->tm_isdst;

	if (m_eMode == HNDatePickerView::Mode::YearDayAndTime)
	{
		m_tTM.tm_min = 0;
	}
    return true;
}

void HNDatePickerView::onEnterTransitionDidFinish()
{
	Node::onEnterTransitionDidFinish();

	Director::getInstance()->getScheduler()->schedule([=](float fDelta)
	{
		if (!isSetDate)
		{
			setMode(m_eMode);
		}
	}, this, 0.0f, 0, 0.0f, false, "first_setMode");
}

void HNDatePickerView::onExitTransitionDidStart()
{
	Node::onExitTransitionDidStart();
}

void HNDatePickerView::setTextColor(const Color4B &color)
{
	m_pPickerView->setFontColorNormal(color);
}

void HNDatePickerView::setTextColorForSelected(const Color4B &color)
{
	m_pPickerView->setFontColorSelected(color);
}

void HNDatePickerView::setContentSize(Size size)
{
	m_pPickerView->setContentSize(size);
}

void HNDatePickerView::setBackGroundViewForSelected(Layout* view)
{ 
    m_pPickerView->onViewForSelected([&](unsigned int component, Size size){

		return view;
    });
}

void HNDatePickerView::setBeginAndEnd(bool isBegin)
{
	if (m_eMode == HNDatePickerView::Mode::YearDayAndTime)
	{
		if (isBegin)
		{
			m_tTM.tm_hour = 0;
		}
		else
		{
			if (23 == m_tTM.tm_hour)
			{
				m_tTM.tm_hour = 0;

				int tem_day = HNCalendar::create()->_dayCountOfMonth(m_tTM.tm_year, m_tTM.tm_mon);
				if (m_tTM.tm_mday + 1 > tem_day)
				{
					m_tTM.tm_mday = 1;
					if (11 == m_tTM.tm_mon)
					{
						m_tTM.tm_mon = 0;
						m_tTM.tm_year++;
					}
					else
					{
						m_tTM.tm_mon++;
					}
				}
				else
				{
					m_tTM.tm_mday++;
				}
			}
			else
			{
				m_tTM.tm_hour++;
			}
		}
	}
}

void HNDatePickerView::setDate(int year, int month, int day, bool animated)
{
    isSetDate = true;
    if (year>=1900 && year<=2100) {
        m_tTM.tm_year = year-1900;
    }else{
        isSetDate = false;
        return;
    }
    if (month>=1 && month<=12) {
        m_tTM.tm_mon = month-1;
    }else{
        isSetDate = false;
        return;
    }
    if (day>=1 && day<=31) {
        m_tTM.tm_mday = day;
    }else{
        isSetDate = false;
        return;
    }
    
    int tem_day = HNCalendar::create()->_dayCountOfMonth(year,month);
    while (m_tTM.tm_mday>tem_day) {
        m_tTM.tm_mday--;
    }

    if (m_pPickerView)
    {
        m_pPickerView->setPickerViewDelegate(this);
        m_pPickerView->setPickerViewDataSource(this);
        m_pPickerView->reloadAllComponents();
        switch (m_eMode)
        {
            case HNDatePickerView::Mode::Date:
            {
                m_pPickerView->selectRow(m_tTM.tm_year, 0, animated);
                m_pPickerView->selectRow(m_tTM.tm_mon, 1, animated);
                m_pPickerView->selectRow(m_tTM.tm_mday-1, 2, animated);
                break;
            }
			case HNDatePickerView::Mode::YearDayAndTime:
			{
				m_pPickerView->selectRow(m_tTM.tm_year, 0, animated);
				m_tTM.tm_mon--;
				m_pPickerView->selectRow(HNCalendar::create(mktime(&m_tTM))->dayOfYear(), 1, animated);
				m_pPickerView->selectRow(m_tTM.tm_hour, 2);
				break;
			}
            case HNDatePickerView::Mode::DateAndTime:
                m_tTM.tm_mon--;
                m_pPickerView->selectRow(HNCalendar::create(mktime(&m_tTM))->dayOfYear(), 0, animated);
                m_pPickerView->selectRow(m_tTM.tm_hour, 1);
                m_pPickerView->selectRow(m_tTM.tm_min, 2);
                break;
            default:
                break;
        }
    }
}

unsigned int HNDatePickerView::numberOfComponentsInPickerView(HNPickerView* pickerView)
{
    int components = 0;
    switch (m_eMode)
    {
        case HNDatePickerView::Mode::CountDownTimer:
            components = 2;
            break;
            
        case HNDatePickerView::Mode::Date:
            components = 3;
            break;

		case HNDatePickerView::Mode::YearDayAndTime:
			components = 3;
			break;
            
        case HNDatePickerView::Mode::DateAndTime:
            components = 3;
            break;
            
        case HNDatePickerView::Mode::Time:
            components = 2;
            break;
            
        default:
            break;
    }
    
    return components;
}

unsigned int HNDatePickerView::numberOfRowsInComponent(HNPickerView* pickerView, unsigned int component)
{
    int row = 0;
    switch (m_eMode)
    {
        case HNDatePickerView::Mode::CountDownTimer:
            if (component == 0) { // hour
                row = 24; // 0 ~ 23
            } else { // min
                row = 60; // 0 ~ 59
            }
            break;
            
        case HNDatePickerView::Mode::Date:
            if (component == 0) { // year
                row = 2100 - 1900+1; // 1900~2100
            } else if (component == 1) { // month
                row = 12;
            } else { // day
                row = HNCalendar::create()->_dayCountOfMonth(m_tTM.tm_year + 1900, m_tTM.tm_mon+1);
            }
            break;

		case HNDatePickerView::Mode::YearDayAndTime:
			if (component == 0) { // year
				row = 7;
			}
			else if (component == 1) { // date
				row = HNCalendar::create()->_isLeapYear(m_tTM.tm_year + 1900) ? 366 : 365;
			}
			else { // hour
				row = 24;
			}
			break;
            
        case HNDatePickerView::Mode::DateAndTime:
            if (component == 0) { // date
                row = HNCalendar::create()->_isLeapYear(m_tTM.tm_year + 1900) ? 366 : 365;
            } else if (component == 1) { // hour
                row = 24;
            } else { // minutes
                row = 60;
            }
            break;
            
        case HNDatePickerView::Mode::Time:
            if (component == 0) {
                row = 24;
            } else {
                row = 60;
            }
            break;
            
        default:
            break;
    }
    return row;
}

float HNDatePickerView::widthForComponent(HNPickerView* pickerView, unsigned int component)
{
    float width = m_pPickerView->getBoundingBox().size.width/numberOfComponentsInPickerView(pickerView);
    switch (m_eMode)
    {
        case HNDatePickerView::Mode::CountDownTimer:
            if (component == 0) { // hour                
            } else { // min
            }
            break;
            
        case HNDatePickerView::Mode::Date:
            if (component == 0) { // years
            } else if (component == 1) { // month
            } else { // day
            }
            break;

		case HNDatePickerView::Mode::YearDayAndTime:
			width = m_pPickerView->getContentSize().width / 3;
			if (component == 0) { // years
				//width *= 2;
			}
			else if (component == 1) { // date
				//width *= 2;
			}
			else { // hour
			}
			break;
            
        case HNDatePickerView::Mode::DateAndTime:
            width = m_pPickerView->getContentSize().width / 4;
            if (component == 0) { // date
                width *= 2;
            } else if (component == 1) { // hour
            } else { // minutes
            }
            break;
            
        case HNDatePickerView::Mode::Time:
            if (component == 0) { // hour
            } else { // min
            }
            break;
            
        default:
            break;
    }
    return width;
}

float HNDatePickerView::rowHeightForComponent(HNPickerView* pickerView, unsigned int component)
{
    return _cellHeight;
}

std::string HNDatePickerView::titleForRow(unsigned int row, unsigned int component)
{
	std::string str = "";
    switch (m_eMode)
    {
        case HNDatePickerView::Mode::CountDownTimer:
            if (component == 0)
            { // hour
				str = StringUtils::format(GBKToUtf8("%d小时"), row);
            } else { // min
				str = StringUtils::format(GBKToUtf8("%d分钟"), row);
            }
            break;
            
        case HNDatePickerView::Mode::Date:
            if (component == 0)
            { // years
				str = StringUtils::format(GBKToUtf8("%d年"), row + 1900);
            } else if (component == 1) { // month
				str = StringUtils::format(GBKToUtf8("%d月"), row + 1);
            } else { // day
				str = StringUtils::format(GBKToUtf8("%d日"), row + 1);
            }
            break;

		case HNDatePickerView::Mode::YearDayAndTime:
			if (component == 0)
			{ // years
				time_t tt = time(NULL);
				struct tm* Time = localtime(&tt);
				str = StringUtils::format("%d", row + 1900 + Time->tm_year - 3);
			}
			else if (component == 1)
			{ // date
				time_t t = mktime(&m_tTM);

				HNCalendar* cal = HNCalendar::create(t);
				int day = row;
				int dayOfYear = cal->dayOfYear();
				cal->addDay(day - dayOfYear);
				int week = cal->dayOfWeek();
				int month = cal->monthOfYear();
				int date = cal->dayOfMonth();

				str = StringUtils::format(GBKToUtf8("%d月%d"), month, date);
			}
			else
			{ // hour
				str = StringUtils::format("%02d:00", row);
			}
			break;
            
        case HNDatePickerView::Mode::DateAndTime:
            if (component == 0)
            { // date
				HNCalendar* cal = HNCalendar::create(m_tTM.tm_year + 1900, m_tTM.tm_mon, m_tTM.tm_mday);
                int day = row;
                int dayOfYear = cal->dayOfYear();
                cal->addDay(day - dayOfYear);
                int week = cal->dayOfWeek();
                int month = cal->monthOfYear();
                int date = cal->dayOfMonth();

				str = StringUtils::format(GBKToUtf8("%d月%d日"), month, date);
            }
            else if (component == 1)
            { // hour
				str = StringUtils::format("%02d:00", row);
            }
            else
            { // minutes
				str = StringUtils::format("%02d", row);
            }
            break;
            
        case HNDatePickerView::Mode::Time:
            if (component == 0)
            { // hour
				str = StringUtils::format("%02d", row);
            }
            else
            { // minutes
				str = StringUtils::format("%02d", row);
            }
            break;
            
        default:
            break;
    }
	//return buff;
	return str;
}

void HNDatePickerView::didSelectRow(HNPickerView* pickerView, unsigned int row, unsigned int component)
{
    switch (m_eMode)
    {
        case HNDatePickerView::Mode::CountDownTimer:
            if (component == 0)
            { // hour
                m_tTM.tm_hour = row;
            }
            else
            { // min
                m_tTM.tm_min = row;
            }
            break;
            
        case HNDatePickerView::Mode::Date:
        {
            if (component == 0)
            { // years
                m_tTM.tm_year = row;
                int tem_day = HNCalendar::create()->_dayCountOfMonth(m_tTM.tm_year + 1900, m_tTM.tm_mon + 1);
                if (m_tTM.tm_mday > tem_day) {
                    m_tTM.tm_mday = 1;
                    m_pPickerView->reloadComponent(0, 2);
                }else{
                    m_pPickerView->reloadComponent(m_tTM.tm_mday - 1, 2);
                }
            }
            else if (component == 1)
            {
                m_tTM.tm_mon = row;
                int tem_day = HNCalendar::create()->_dayCountOfMonth(m_tTM.tm_year + 1900, m_tTM.tm_mon + 1);
                if (m_tTM.tm_mday > tem_day) {
                    m_tTM.tm_mday = 1;
                    m_pPickerView->reloadComponent(0, 2);
                }else{
                    m_pPickerView->reloadComponent(m_tTM.tm_mday - 1, 2);
                }
            }
            else
            {
                m_tTM.tm_mday = row + 1;
            }
            break;
        }
		case HNDatePickerView::Mode::YearDayAndTime:
		{
			if (component == 0)
			{ // years
				time_t tt = time(NULL);
				struct tm* Time = localtime(&tt);
				m_tTM.tm_year = row + Time->tm_year - 3;

				int tem_day = HNCalendar::create()->_dayCountOfMonth(m_tTM.tm_year + 1900, m_tTM.tm_mon + 1);
				if (m_tTM.tm_mday > tem_day) {
					m_tTM.tm_mday = 1;
					m_pPickerView->reloadComponent(0, 1);
				}
				else {
					int tem_row = 0;
					for (int i = 0; i < m_tTM.tm_mon; ++i)
					{
						tem_row += HNCalendar::create()->_dayCountOfMonth(m_tTM.tm_year + 1900, i + 1);
					}

					tem_row += m_tTM.tm_mday;
					m_pPickerView->reloadComponent(tem_row - 1, 1);
				}
			}
			else if (component == 1)
			{ // date
				HNCalendar* cal = HNCalendar::create();
				int month, date;
				cal->dateByDayOfYear(m_tTM.tm_year, row, month, date);
				m_tTM.tm_mon = month;
				m_tTM.tm_mday = date;
				m_tTM.tm_mon--;
				if (m_tTM.tm_mon < 0)
				{
					m_tTM.tm_mon = 0;
				}
			}
			else
			{ // hour
				m_tTM.tm_hour = row;
				m_tTM.tm_min = 0;
			}
			break;
		}
        case HNDatePickerView::Mode::DateAndTime:
            if (component == 0)
            { // date
				HNCalendar* cal = HNCalendar::create();
                int month, date;
                cal->dateByDayOfYear(m_tTM.tm_year, row, month, date);
                m_tTM.tm_mon = month;
                m_tTM.tm_mday = date;
                m_tTM.tm_mon--;
				if (m_tTM.tm_mon < 0)
				{
					m_tTM.tm_mon = 0;
				}
            }
            else if (component == 1)
            { // hour
                m_tTM.tm_hour = row;
            }
            else
            { // minutes
                m_tTM.tm_min = row;
            }
            break;
            
        case HNDatePickerView::Mode::Time:
            if (component == 0)
            { // hour
                m_tTM.tm_hour = row;
            }
            else
            { // minutes
                m_tTM.tm_min = row;
            }
            break;
            
        default:
            break;
    }
    
    if (m_obSelectRow)
    {
        m_obSelectRow(&m_tTM);
    }
}

void HNDatePickerView::setMode(HNDatePickerView::Mode mode)
{
    m_eMode = mode;
    
    if (m_pPickerView)
    {
        m_pPickerView->setPickerViewDelegate(this);
        m_pPickerView->setPickerViewDataSource(this);
        
        switch (m_eMode)
        {
            case HNDatePickerView::Mode::Date:
                m_pPickerView->selectRow(m_tTM.tm_year, 0);
                m_pPickerView->selectRow(m_tTM.tm_mon, 1);
                m_pPickerView->selectRow(m_tTM.tm_mday, 2);
                break;
			case HNDatePickerView::Mode::YearDayAndTime:
				m_pPickerView->selectRow(3, 0);
				m_pPickerView->selectRow(HNCalendar::create(mktime(&m_tTM))->dayOfYear(), 1);
				m_pPickerView->selectRow(m_tTM.tm_hour, 2);
				break;
            case HNDatePickerView::Mode::DateAndTime:
                m_pPickerView->selectRow(HNCalendar::create(mktime(&m_tTM))->dayOfYear(), 0);
                m_pPickerView->selectRow(m_tTM.tm_hour, 1);
                m_pPickerView->selectRow(m_tTM.tm_min, 2);
                break;
            case HNDatePickerView::Mode::Time:
                m_pPickerView->selectRow(m_tTM.tm_hour, 0);
                m_pPickerView->selectRow(m_tTM.tm_min, 1);
                break;
            default:
                break;
        }
        //m_pPickerView->reloadAllComponents();
    }
}
