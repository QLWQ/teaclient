/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__HNCalendar__
#define __HN__HNCalendar__

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class HNCalendar : public Ref {
    
public:
	HNCalendar();
    virtual ~HNCalendar();
    
public:
    static HNCalendar* create();
    static HNCalendar* create(int year, int month, int day); // year: 1 ~ 10000, month: 1 ~ 12, day: 1 ~ 31
    static HNCalendar* create(time_t time); // since 1900
    
    void setCalendar(int year, int month, int day);
    void setCalendar(time_t time);
    
    // count >= 1 && count <= 10000
    void addYear(int count = 1);
    void addMonth(int count = 1);
    void addDay(int count = 1);
    
    bool isLeapYear();
    
    int dayCountOfMonth();
    int dayCountOfYear();
    
    int dayOfMonth();
    int dayOfWeek();
    int dayOfYear();
    int monthOfYear();
    int weekOfYear();
    
    void dateByDayOfYear(int year, int day, int& month, int& date);
    
public:
    bool _isLeapYear(int year);
    int _dayCountOfMonth(int year, int month);
    int _calcDayCount(tm target);
    int _compareDate(tm date1, tm date2);
    int _dayCountOfYear(int year);
private:
    tm m_tDateTime;
};

#endif /* defined(__HN__HNCalendar__) */
