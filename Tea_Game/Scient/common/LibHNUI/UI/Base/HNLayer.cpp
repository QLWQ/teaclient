﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNLayer.h"
#include "HNLayerColor.h"
#include "Sound/HNAudioEngine.h"
#include "HNNetExport.h"

namespace HN 
{
	HNLayer::HNLayer(void) 
		: _colorLayer(nullptr)
	{
	}

	HNLayer::~HNLayer(void)
	{
		
	}

	bool HNLayer::init()
	{
		if (!Layer::init()) return false;
		
		setCascadeOpacityEnabled(true);

		_winSize = Director::getInstance()->getWinSize();

		return true;
	}

	void HNLayer::setBackGroundImage(const std::string& name)
	{
		auto visibleSize = Director::getInstance()->getVisibleSize();
		//背景
		auto loadingBG = Sprite::create(name.c_str());
		loadingBG->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
		float _xScale = visibleSize.width / loadingBG->getContentSize().width;
		float _yScale = visibleSize.height / loadingBG->getContentSize().height;
		loadingBG->setScaleX(_xScale);
		loadingBG->setScaleY(_yScale);
		loadingBG->setName("BackGround");
		this->addChild(loadingBG, -1);
	}

	void HNLayer::modifyBackGroundImage(const std::string& name)
	{
		auto sp = dynamic_cast<Sprite*> (this->getChildByName("BackGround"));
		if (sp != nullptr)
		{
			sp->setTexture(name);
		}
	}

	float HNLayer::getRealScaleX(int designWidth)
	{
		auto visibleSize = Director::getInstance()->getVisibleSize();
		return visibleSize.width / designWidth;
	}

	float HNLayer::getRealScaleY(int designHeight)
	{
		auto visibleSize = Director::getInstance()->getVisibleSize();
		return visibleSize.height / designHeight;
	}

	void HNLayer::enableKeyboard()
	{
		//对手机返回键的监听
		auto listener = EventListenerKeyboard::create();
		//和回调函数绑定
		listener->onKeyReleased = CC_CALLBACK_2(HNLayer::onKeyReleased, this);
		//添加到事件分发器中
		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	}

	bool HNLayer::switchToLayoutControl(ui::Widget* root, const std::string& controlName, Node* newControl)
	{
		auto tmp = (Text*)Helper::seekWidgetByName(root, controlName);
		if (nullptr == tmp) return false;
		newControl->setPosition(tmp->getPosition());
		newControl->setTag(tmp->getTag());
		newControl->setLocalZOrder(tmp->getLocalZOrder());
		newControl->setScaleX(tmp->getScaleX());
		newControl->setScaleY(tmp->getScaleY());
		newControl->setAnchorPoint(tmp->getAnchorPoint());
		root->addChild(newControl);
		return true;
	}

	void HNLayer::startShade(Node* parent, int zOrder, int opacity/* = 150*/, std::function<void(Ref*)> touchCallback/* = nullptr*/)
	{
		if (nullptr == _colorLayer)
		{
			_colorLayer = Layout::create();
			_colorLayer->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
			_colorLayer->setBackGroundColor(Color3B::BLACK);
			_colorLayer->setContentSize(_winSize);
			_colorLayer->setOpacity(0);
			_colorLayer->setTouchEnabled(true);

			parent->addChild(_colorLayer, zOrder - 1);

			_colorLayer->runAction(FadeTo::create(0.2f, opacity));

			if (touchCallback)
			{
				_colorLayer->addClickEventListener(touchCallback);
			}
		}
	}

	void HNLayer::quicklyShade(Node* parent, int zOrder, int opacity/* = 150*/, std::function<void(Ref*)> touchCallback/* = nullptr*/)
	{
		if (nullptr == _colorLayer)
		{
			_colorLayer = Layout::create();
			_colorLayer->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
			_colorLayer->setBackGroundColor(Color3B::BLACK);
			_colorLayer->setContentSize(_winSize);
			_colorLayer->setOpacity(opacity);
			_colorLayer->setTouchEnabled(true);

			parent->addChild(_colorLayer, zOrder - 1);

			if (touchCallback)
			{
				_colorLayer->addClickEventListener(touchCallback);
			}
		}
	}

	// 移除遮挡层
	void HNLayer::removeShade(bool action/* = false*/)
	{
		if (_colorLayer)
		{
			if (action)
			{
				_colorLayer->runAction(Sequence::create(FadeOut::create(0.2f), RemoveSelf::create(true), nullptr));
			}
			else
			{
				_colorLayer->removeFromParent();
			}
			_colorLayer = nullptr;
		}
	}
    
    void HNLayer::releaseUnusedRes()
    {
        scheduleOnce(schedule_selector(HNLayer::releaseGameRes), 0.f);
    }
    
    
    void HNLayer::releaseGameRes(float delta)
    {
        AnimationCache::destroyInstance();
        SpriteFrameCache::getInstance()->removeUnusedSpriteFrames();
        Director::getInstance()->getTextureCache()->removeUnusedTextures();
    }

	void HNLayer::open(ACTION_TYPE_LAYER type, Node* parent, Vec2 pos, int zOrder/* = 2*/, int tag/* = 0*/,
		bool showShade/* = true*/, std::function<void(Ref*)> touchCallback/* = nullptr*/)
	{
		if (showShade) startShade(parent, zOrder, 150, touchCallback);

		ActionInterval* action = nullptr;
		switch (type)
		{
		case HN::HNLayer::SCALE:
		{
			setPosition(pos);
			setScale(0.1f);
			runAction(ScaleTo::create(0.2f, 1.0f));
		} break;
		case HN::HNLayer::FADE:
		{
			setPosition(pos);
			setOpacity(0);
			runAction(FadeIn::create(0.2f));
		} break;
		case HN::HNLayer::TOP:
		{
			setPosition(Vec2(pos.x, pos.y + _winSize.height));
			runAction(MoveBy::create(0.3f, Vec2(0, -_winSize.height)));
		} break;
		case HN::HNLayer::LEFT:
		{
			setPosition(Vec2(pos.x - _winSize.width, pos.y));
			runAction(MoveBy::create(0.2f, Vec2(_winSize.width, 0)));
		} break;
		case HN::HNLayer::RIGHT:
		{
			setPosition(Vec2(pos.x + _winSize.width, pos.y));
			runAction(MoveBy::create(0.2f, Vec2(-_winSize.width, 0)));
		} break;
		case	HN::HNLayer::BOTTOM:
		{
			setPosition(Vec2(pos.x, pos.y - _winSize.height));
			runAction(MoveBy::create(0.3f, Vec2(0, 0)));
		}
		case HN::HNLayer::NONE:
		default:
		{
			setPosition(pos);
		} break;
		}

		parent->addChild(this, zOrder, tag);
	}

	void HNLayer::close(ACTION_TYPE_LAYER type/* = FADE*/)
	{
		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_CLOSE);

		removeShade(type);

		ActionInterval* action = nullptr;

		switch (type)
		{
		case HN::HNLayer::SCALE:
			action = Sequence::create(ScaleTo::create(0.1f, 0.1f), CallFunc::create(this, CC_CALLFUNC_SELECTOR(HNLayer::closeFunc)), RemoveSelf::create(true), nullptr);
			break;
		case HN::HNLayer::FADE:
			action = Sequence::create(FadeOut::create(0.2f), CallFunc::create(this, CC_CALLFUNC_SELECTOR(HNLayer::closeFunc)), RemoveSelf::create(true), nullptr);
			break;
		case HN::HNLayer::TOP:
			action = Sequence::create(MoveBy::create(0.2f, Vec2(0, _winSize.height)), CallFunc::create(this, CC_CALLFUNC_SELECTOR(HNLayer::closeFunc)), RemoveSelf::create(true), nullptr);
			break;
		case HN::HNLayer::LEFT:
			action = Sequence::create(MoveBy::create(0.2f, Vec2(-_winSize.width, 0)), CallFunc::create(this, CC_CALLFUNC_SELECTOR(HNLayer::closeFunc)), RemoveSelf::create(true), nullptr);
			break;
		case HN::HNLayer::RIGHT:
			action = Sequence::create(MoveBy::create(0.2f, Vec2(_winSize.width, 0)), CallFunc::create(this, CC_CALLFUNC_SELECTOR(HNLayer::closeFunc)), RemoveSelf::create(true), nullptr);
			break;
		case HN::HNLayer::NONE:
		default:
			action = Sequence::create(CallFunc::create(this, CC_CALLFUNC_SELECTOR(HNLayer::closeFunc)), RemoveSelf::create(true), nullptr);
			break;
		}

		runAction(action);
	}

	void HNLayer::removeFromParent()
	{
		removeShade();

		Layer::removeFromParent();
	}

	void HNLayer::closeFunc()
	{
		log("closeFunc");
	}
}