﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNGameUIBase_H__
#define __HN_HNGameUIBase_H__

#include "HNBaseType.h"
#include "HNLayer.h"

namespace HN
{
    class HNGameUIBase: public HNLayer
	{
	public:
		// 构造函数
		HNGameUIBase();

		// 析构函数
		virtual ~HNGameUIBase();

		// 初始化
		virtual bool init();

		// 加载界面（初始化时）
		virtual void loadUI();

		// 卸载界面（界面移除）
		virtual void unLoadUI();

		//回放模式
		virtual void setRecordMode(){};

	public:
		// 发送消息
		void sendData(UINT MainID, UINT AssistantID, void* object = nullptr, INT objectSize = 0);
	};
}


#endif