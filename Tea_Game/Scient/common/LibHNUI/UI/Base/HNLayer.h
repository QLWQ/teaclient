﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNLayer_H__
#define __HN_HNLayer_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

namespace HN {

	class HNLayer : public Layer
	{
	public:
		enum ACTION_TYPE_LAYER
		{
			SCALE = 0,		// 缩放 动作
			FADE,			// 渐隐/渐现 动作
			TOP,			// 顶部掉下/收回 动作
			LEFT,			// 左边滑入 动作
			RIGHT,			// 右边滑入 动作
			BOTTOM,			//底部弹出
			NONE			// 无动作直接显示
		};

	public:
		HNLayer(void);
		virtual ~HNLayer(void);

		virtual bool init() override;

	public:
		void setBackGroundImage(const std::string& name);

		void modifyBackGroundImage(const std::string& name);

		// 按键监听
		void enableKeyboard();
		
		// 渐现遮挡层
		void startShade(Node* parent, int zOrder, int opacity = 150, std::function<void(Ref*)> touchCallback = nullptr);

		// 直接显示遮挡层
        void quicklyShade(Node* parent, int zOrder, int opacity = 150, std::function<void(Ref*)> touchCallback = nullptr);

		// 移除遮挡层
		void removeShade(bool action = false);
        
        // 释放无用的资源
        void releaseUnusedRes();

	public:
		float getRealScaleX(int designWidth);

		float getRealScaleY(int designHeight);

	public:
		bool switchToLayoutControl(ui::Widget* root, const std::string& controlName, Node* newControl);

	public:
		/**
		* 添加和显示此节点.
		* @param type	用于显示的特效，缩放/渐现等.
		* @param parent  添加到的父节点
		* @param showShade  是否显示遮挡屏蔽层
		* @param touchCallback  点击遮挡层的回调
		*/
		virtual void open(ACTION_TYPE_LAYER type, Node* parent, Vec2 pos, int zOrder = 2, int tag = 0,
			bool showShade = true, std::function<void(Ref*)> touchCallback = nullptr);

		/**
		* 关闭和移除此节点.
		* @param type	用于显示的特效，缩放/渐隐等.
		*/
		virtual void close(ACTION_TYPE_LAYER type = FADE);

		virtual void removeFromParent() override;
        
    protected:
        // 释放资源
        void releaseGameRes(float delta);

		// 关闭此节点前回调
		virtual void closeFunc();

	protected:
		Layout*			_colorLayer = nullptr;
        cocos2d::Size	_winSize;
	};

};

#endif	//__HN_HNLayer_H__
