﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNSprite.h"

namespace HN 
{

	HNSprite* HNSprite::create(const std::string& filename)
	{
		HNSprite *sprite = new (std::nothrow) HNSprite();
		if (sprite && sprite->initWithFile(filename))
		{
			sprite->autorelease();
			return sprite;
		}
		CC_SAFE_DELETE(sprite);
		return nullptr;
	}

	HNSprite::HNSprite(void)
	{

	}

	HNSprite::~HNSprite(void)
	{

	}

}