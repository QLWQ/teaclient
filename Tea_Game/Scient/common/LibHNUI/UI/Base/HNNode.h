﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNNode_H__
#define __HN_HNNode_H__

#include "cocos2d.h"

USING_NS_CC;

namespace HN {

	class HNNode : public Node
	{
	public:
		HNNode(void);
		virtual ~HNNode(void);
	};

};

#endif	//__HN_HNNode_H__