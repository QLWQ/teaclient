﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNGameUIBase.h"
#include "HNRoom/HNRoomLogic.h"
#include "HNCommon/HNLog.h"


namespace HN
{
	HNGameUIBase::HNGameUIBase()
	{

	}

	HNGameUIBase::~HNGameUIBase()
	{
		unLoadUI();		
	}

	bool HNGameUIBase::init()
	{
		if (!HNLayer::init())
		{
			return false;
		}

		loadUI();

		return true;
	}

	void HNGameUIBase::loadUI()
	{
		
	}

	void HNGameUIBase::unLoadUI()
	{

	}

	void HNGameUIBase::sendData(UINT MainID, UINT AssistantID, void* object, INT objectSize)
	{
		RoomLogic()->sendData(MainID, AssistantID, object, objectSize);
	}
}