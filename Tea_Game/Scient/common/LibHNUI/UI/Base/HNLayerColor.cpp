﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNLayerColor.h"

namespace HN {

	HNLayerColor::HNLayerColor(void)
	{
	}

	HNLayerColor::~HNLayerColor(void)
	{
	}

	bool HNLayerColor::init()
	{
		if (!LayerColor::init())
		{
			return false;
		}
		this->setAnchorPoint(Vec2(0.5f, 0.5f));
		return true;
	}


}