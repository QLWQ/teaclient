﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_HNSprite_H__
#define __HN_HNSprite_H__

#include "cocos2d.h"

USING_NS_CC;

namespace HN {

	class HNSprite : public Sprite
	{
	public:
		HNSprite(void);
		static HNSprite* create(const std::string& filename);
		virtual ~HNSprite(void);
	};

};

#endif	//__HN_HNSprite_H__

