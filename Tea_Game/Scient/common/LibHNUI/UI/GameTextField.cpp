/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameTextField.h"
#include <string>
#include "GamePrompt.h"
#include "HNCommon/HNConverCode.h"

static const char* PROMPT_CSB		= "platform/prompt/Node_textfield.csb";

static const int GAMEPROMPT_LAYER_ZORDER	= 100000000;		// 子节点弹出框层级

static const int GAMEPROMPT_LAYER_TAG		= 100000000;		// 子节点弹出框层级


GameTextField::GameTextField()
{

}

GameTextField::~GameTextField()
{

}

GameTextField* GameTextField::create()
{
	GameTextField *pRet = new GameTextField();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return nullptr;
	}
}

void GameTextField::showPrompt(const std::string& prompt, int maxLength, std::function<void(const std::string& text)> func)
{
	Node* root = Director::getInstance()->getRunningScene();
	CCAssert(nullptr != root, "root is null");

	_func = func;

	if (maxLength > 0)
	{
		_textfield->setMaxLength(maxLength);
		_textfield->setMaxLengthEnabled(true);
	}
	else
	{
		_textfield->setMaxLengthEnabled(false);
	}

	if (!getParent())
	{
		setPosition(Director::getInstance()->getWinSize() / 2);
		root->addChild(this, GAMEPROMPT_LAYER_ZORDER, GAMEPROMPT_LAYER_TAG);
	}
	setPrompt(prompt);
}

void GameTextField::close()
{
	this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
		this->removeFromParent();
	}), nullptr));
}

bool GameTextField::init()
{
	if (!HNLayer::init()) {
		return false;
	}

	setIgnoreAnchorPointForPosition(false);
	setCascadeOpacityEnabled(true);

	auto node = CSLoader::createNode(PROMPT_CSB);
	node->setPosition(_winSize / 2);
	addChild(node, 2);

	auto layout_bg = dynamic_cast<Layout*>(node->getChildByName("Panel_bg"));
	layout_bg->setScale(_winSize.width / 1280, _winSize.height / 720);

	auto imgbg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));

	// 显示的文本
	_text_prompt = (Text*)imgbg->getChildByName("Text_prompt");
	_text_prompt->setString("");

	_textfield = dynamic_cast<TextField*>(imgbg->getChildByName("TextField_prompt"));
	_textfield->setString("");;

	// 确定按钮
	_btn_sure = (Button*)imgbg->getChildByName("Button_sure");
	_btn_sure->addClickEventListener(CC_CALLBACK_1(GameTextField::menuClickCallBack, this));

	// 取消按钮
	_btn_cancel = (Button*)imgbg->getChildByName("Button_cancel");
	_btn_cancel->addClickEventListener(CC_CALLBACK_1(GameTextField::menuClickCallBack, this));

	return true;
}

void GameTextField::setPrompt(const std::string& prompt)
{
	if (nullptr != _text_prompt) {
		_text_prompt->setString(prompt);
	}
}

void GameTextField::menuClickCallBack(Ref* pSender)
{
	Button* btn = (Button*)pSender;
	auto name = btn->getName();

	if (name.compare("Button_sure") == 0)
	{
		auto text = _textfield->getString();
		if (text.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入内容"));
			return;
		}
		if (nullptr != _func) {

			_func(text);
		}
	}

	close();
}
