/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNCalculateBoard.h"
#include <string>
#include "cocostudio/ActionTimeline/CSLoader.h"

USING_NS_CC;
using namespace ui;
using namespace HN;
using namespace cocostudio;

HNCalculateBoard::HNCalculateBoard() 
	: _calculateBoardNode(nullptr)
	, _calculateBoardLayout(nullptr)
	, _btn_confirm(nullptr)
	, _btn_share(nullptr)
	, _btn_back(nullptr)
{
}

HNCalculateBoard::~HNCalculateBoard()
{

}

void HNCalculateBoard::showBoard(Node* parent, int zorder)
{
	CCAssert(nullptr != parent, "parent is null");

	parent->addChild(this, zorder);
	this->setVisible(true);
}

void HNCalculateBoard::close()
{
	_calculateBoardNode->runAction(Sequence::create(ScaleTo::create(0.1f, 0.3f), CCCallFunc::create([&]()
	{
		this->removeFromParent();
	}), nullptr));
}

bool HNCalculateBoard::init()
{
    if ( !HNLayer::init()) return false;
	//���κ���Ĳ�
	auto colorLayer = LayerColor::create(Color4B(0, 0, 0, 100));
	addChild(colorLayer, 1);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = [=](Touch* touch ,Event* event){

		if (!isVisible()) return false;
		return true;
	};
	touchListener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener,this);

	Size winSize = Director::getInstance()->getWinSize();
	
	return true;
}

void HNCalculateBoard::confirmBtnCallBack(Ref* pSender, Widget::TouchEventType type)
{
	//for override 
}

void HNCalculateBoard::shareBtnCallBack(Ref* pSender, Widget::TouchEventType type)
{
	//for override 
}

void HNCalculateBoard::backBtnCallBack(Ref* pSender, Widget::TouchEventType type)
{
	//for override 
}

void HNCalculateBoard::showAndUpdateBoard()
{
	// for override
}

void HNCalculateBoard::updateUserHead(int userId, int logoID, const std::string& headUrl, Sprite* headSprite)
{
	// for override 
}
