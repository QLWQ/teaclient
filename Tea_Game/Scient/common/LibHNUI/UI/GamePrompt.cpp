/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GamePrompt.h"
#include <string>

static const char* PROMPT_CSB		= "platform/prompt/promptNode.csb";

static const int GAMEPROMPT_LAYER_ZORDER	= 100000000;		// 子节点弹出框层级

static const int GAMEPROMPT_LAYER_TAG		= 100000000;		// 子节点弹出框层级


GamePromptLayer::GamePromptLayer()
{

}

GamePromptLayer::~GamePromptLayer()
{

}

GamePromptLayer* GamePromptLayer::create(bool bCanSelect/* = false*/)
{
	GamePromptLayer *pRet = new GamePromptLayer();
	if (pRet && pRet->init())
	{
		if (bCanSelect) pRet->setPromptCanSelect();
		pRet->autorelease();
		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return nullptr;
	}
}

void GamePromptLayer::showPrompt(const std::string& prompt)
{
	Node* root = Director::getInstance()->getRunningScene();
	CCAssert(nullptr != root, "root is null");

	if (!getParent())
	{
		open(ACTION_TYPE_LAYER::FADE, root, Vec2::ZERO, GAMEPROMPT_LAYER_ZORDER, GAMEPROMPT_LAYER_TAG);
		//setPosition(Director::getInstance()->getWinSize() / 2);
		//root->addChild(this, GAMEPROMPT_LAYER_ZORDER, GAMEPROMPT_LAYER_TAG);
	}
	setPrompt(prompt);
}

void GamePromptLayer::closeFunc()
{
	
}

void GamePromptLayer::setCallBack(std::function<void ()> sure)
{
	_sure = sure;
}

void GamePromptLayer::setCancelCallBack(std::function<void ()> cancel)
{
	_cancel = cancel;
}

bool GamePromptLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(PROMPT_CSB);
	node->setPosition(_winSize / 2);
	addChild(node, 2);

	auto imgbg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));

	//显示的文本
	auto text = (Text*)imgbg->getChildByName("Text_prompt");
	text->setVisible(false);

	promptText = TextView::createWithSize(text->getContentSize());
	promptText->setFontSize(text->getFontSize());
	promptText->setTextColor(text->getTextColor());
	promptText->setAlignment(text->getTextHorizontalAlignment(), text->getTextVerticalAlignment());
	promptText->setPosition(text->getPosition());
	promptText->setFontName(text->getFontName());
	text->getParent()->addChild(promptText);

	//确定按钮
	promptButtonSure1 = (Button*)imgbg->getChildByName("Button_sure");
	promptButtonSure1->addClickEventListener(CC_CALLBACK_1(GamePromptLayer::menuClickCallBack, this));

	//确定按钮
	promptButtonSure2 = (Button*)imgbg->getChildByName("Button_sure2");
	promptButtonSure2->addClickEventListener(CC_CALLBACK_1(GamePromptLayer::menuClickCallBack, this));
	promptButtonSure2->setVisible(false);

	//确定按钮
	promptButtonCancel = (Button*)imgbg->getChildByName("Button_cancel");
	promptButtonCancel->addClickEventListener(CC_CALLBACK_1(GamePromptLayer::menuClickCallBack, this));
	promptButtonCancel->setVisible(false);

	return true;
}

void GamePromptLayer::setPrompt(const std::string& prompt)
{
	if (nullptr != promptText) {
		promptText->setString(prompt);
	}
}

void GamePromptLayer::setPrompt(const std::string& prompt, const std::string& font, int size)
{
	if (nullptr != promptText)
	{
		promptText->setFontName(font);
		promptText->setFontSize(size);
		promptText->setString(prompt);
	}
}

void GamePromptLayer::setPromptCanSelect()
{
	promptButtonSure1->setVisible(false);
	promptButtonSure2->setVisible(true);
	promptButtonCancel->setVisible(true);
}

void GamePromptLayer::closeView()
{
	close();
}

void GamePromptLayer::menuClickCallBack(Ref* pSender)
{
	Button* btn = (Button*)pSender;
	auto name = btn->getName();

	btn->setEnabled(false);

	if (name.compare("Button_sure") == 0 || name.compare("Button_sure2") == 0)
	{
		if (nullptr != _sure) {
			_sure();
		}
	}
	if (name.compare("Button_cancel") == 0)
	{
		if (nullptr != _cancel)	{
			_cancel();
		}
	}

	close();
}