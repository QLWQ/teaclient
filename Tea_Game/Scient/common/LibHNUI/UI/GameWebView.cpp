/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameWebView.h"
#include <string>

static const int LAYER_ZORDER			= 100000000;		// 子节点弹出框层级
static const int LAYER_TAG				= 100000000;		// 子节点弹出框层级

static const char* CLOSE_MENU_N				= "platform/common/fanhui.png";

GameWebViewLayer::GameWebViewLayer() 
	: _titleText (nullptr)
	, _layerColor (nullptr)
{

}

GameWebViewLayer::~GameWebViewLayer()
{

}

void GameWebViewLayer::showWebView(const std::string& URL)
{
	Node* root = Director::getInstance()->getRunningScene();
	CCAssert(nullptr != root, "root is null");

	root->addChild(this, LAYER_ZORDER, LAYER_TAG);
	setURL(URL);
}

void GameWebViewLayer::setTitle(const std::string& title)
{
	if (nullptr != _titleText)
	{
		_titleText->setString(title);
	}
}

void GameWebViewLayer::setColor(Color3B color)
{
	_layerColor->setColor(color);
}

void GameWebViewLayer::closeWebView()
{
	this->runAction(Sequence::create(ScaleTo::create(0.1f, 0.3f), CCCallFunc::create([&]()
	{
		if (onCloseCallBack)
		{
			onCloseCallBack();
		}
		this->removeFromParent();
	}), nullptr));
}

bool GameWebViewLayer::init()
{
    if ( !HNLayer::init()) return false;
	
	Size winSize = Director::getInstance()->getWinSize();
	//屏蔽后面的层
	_layerColor = LayerColor::create(Color4B(0, 0, 205, 200));
	_layerColor->setContentSize(Size(winSize));
	_layerColor->setPosition(Vec2(0, 0));
	addChild(_layerColor);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = [=](Touch* touch ,Event* event){
		return true;
	};
	touchListener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener,_layerColor);

	//标题
	_titleText = Text::create("", "", 60);
	addChild(_titleText, 5);
	_titleText->setPosition(Vec2(winSize.width/2, winSize.height-40));
	_titleText->setString("");

	// 关闭按钮
	auto closeBtn = Button::create(CLOSE_MENU_N);
	closeBtn->setPosition(Vec2(40, winSize.height-40));
	addChild(closeBtn,5);
	closeBtn->addTouchEventListener(CC_CALLBACK_2(GameWebViewLayer::closeEventCallBack,this));

	return true;
}

void GameWebViewLayer::setURL(const std::string& URL)
{
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		Size winSize = Director::getInstance()->getWinSize();
		auto myWebView = cocos2d::experimental::ui::WebView::create();
		addChild(myWebView);
		myWebView->setContentSize(Size(winSize.width, winSize.height-80));
		myWebView->setPosition(Vec2(winSize.width/2, winSize.height/2 - 40));
		myWebView->loadURL(URL);
		myWebView->setScalesPageToFit(true);
		myWebView->setOnDidFailLoading([=](cocos2d::experimental::ui::WebView *sender, const std::string &url){

			Application::getInstance()->openURL(url);
		});
	#endif		
}

void GameWebViewLayer::closeEventCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (Widget::TouchEventType::ENDED != type) return;

			_layerColor->setColor(Color3B(255,255,255));
			closeWebView();
}
