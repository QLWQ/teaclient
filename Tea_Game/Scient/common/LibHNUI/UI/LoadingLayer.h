﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __libCommon_LoadingLayer_h__
#define __libCommon_LoadingLayer_h__

#include "Base/HNLayer.h"
#include <string>

namespace HN
{

	class LoadingLayer : public HNLayer
	{
	public:
		// 创建loading
		static LoadingLayer* createLoading(Node* parent, const std::string& text, float fontSize, float timeOut = 3.0);
		static LoadingLayer* createLoading(Node* parent, const std::string& text, float fontSize, const std::string& image, float timeOut = 3.0);


		static void removeLoading(Node* parent);
		virtual bool init() override;
		
		CREATE_FUNC(LoadingLayer);	

	public:
		void setCancelCallBack(std::function<void()> cancel);

		void setText(const std::string& text);

	protected:
		// 创建界面
		void createDialog(Node* parent, const std::string& text, float fontSize, const std::string& image, float timeOut, bool rotate = false);


	private:
		Label*	_label;
		Button* _btn_cancel = nullptr;
		std::function<void()> _func = nullptr;

		LoadingLayer();
		virtual ~LoadingLayer();
	};

}
#endif // __libCommon_LoadingLayer_h__
