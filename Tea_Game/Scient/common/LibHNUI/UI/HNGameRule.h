/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#pragma once

#include "UI/Base/HNLayer.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"
#include "HNNetProtocol/HNPlatformMessage.h"

using namespace cocostudio;
using namespace HN;

const int MAX_LENGTH = 513;
class HNGameRule : public HNLayer
{
public:
	HNGameRule();

	virtual ~HNGameRule();

	virtual bool init() override;

	//virtual std::string getGameRule();

	virtual void setGameRule(void* ptr, int size);
	
	virtual void* getGameRule();

	virtual void getGameRule(char szDeskConfig[]);

	virtual BYTE getMidEnter();

	virtual void setRoomConfig(const MSG_GP_O_BuyDeskConfig config);

	virtual int getGameTime(); //获取房间时效（时效房间用）
	//局数
	virtual int getSelectCount();

	//是否定位（0:不定位，1:定位）
	virtual int getSelectLocation();

	//支付类型（0:房主，1:AA）
	virtual int getSelectPay();
protected:
	char _gameRule[MAX_LENGTH];
	
};
