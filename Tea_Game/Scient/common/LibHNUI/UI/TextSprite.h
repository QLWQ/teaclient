﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef TextSprite_h__
#define TextSprite_h__

#include "Base/HNSprite.h"

namespace HN 
{

	class TextSprite : public HNSprite
	{
	public:
		enum TextLayout
		{
			LEFT = 0,
			CENTER,
			RIGHT
		};
	public:
		TextSprite();
		virtual ~TextSprite();

	public:
		static TextSprite* create(const std::string& filename);
		static TextSprite* createWithTTF(const TTFConfig& ttfConfig, const std::string& filename);

	public:
		bool initWithTTF(const TTFConfig& ttfConfig, const std::string& filename);

		void setText (const std::string& text, const Color4B &color = Color4B::WHITE);

		void setText (const TTFConfig& ttfConfig, const std::string& text, const Color4B &color = Color4B::WHITE);

		void setTextLayout(TextLayout layout);

		void runTextAction();

		void setIndent(float indent);

	private:
		void createLabel(const TTFConfig& ttfConfig, const std::string& text, const Color4B &color = Color4B::WHITE);

	private:
		Label*			_text;
		TTFConfig		_ttfConfig;
		float	_indent;
	};

}

#endif // TextSprite_h__
