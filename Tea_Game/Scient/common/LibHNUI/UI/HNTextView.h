/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_TextView_h__
#define __HN_TextView_h__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace std;
using namespace ui;

namespace HN
{
	class TextView : public Layout
	{
	public:
		// 参数为想创建的矩形大小
        static TextView* createWithSize(const cocos2d::Size size);

		// 设置文本内容
		void setString(const std::string& text);

		// 获取文本内容
		const std::string& getString()const;

		// 设置文本字号
		void setFontSize(float size);

		// 获取文本字号
		float getFontSize()const;

		// 设置文本字体
		void setFontName(const std::string& name);

		// 获取文本字体
		const std::string& getFontName()const;

		// 设置字体颜色
		void setTextColor(const Color4B color);

		// 获取字体颜色
		const Color4B& getTextColor() const;

		// 设置文本对齐
		void setAlignment(TextHAlignment hAlignment, TextVAlignment vAlignment);

	private:
		// 重新调整文本和滑动区大小
		void resize();

	protected:
		bool init(const cocos2d::Size size);

	protected:
		TextView();

		virtual ~TextView();

	protected:
		Label* _label = nullptr;
		Text::Type _type = Text::Type::SYSTEM;
		float _fontSize = 20;
		std::string _fontName;
		ui::ScrollView* _scrollView = nullptr;
	};
}


#endif // __HN_TextView_h__
