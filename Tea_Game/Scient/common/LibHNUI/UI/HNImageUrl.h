/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_ImageUrl_h__
#define __HN_ImageUrl_h__

#include "ui/CocosGUI.h"
#include "network/HttpClient.h"

USING_NS_CC;
using namespace ui;

namespace HN
{
	class ImageUrl : public ImageView//, public HNUserHeadHttpDelegate
//		, public HNHttpDelegate
	{
	public:
		typedef std::function<void(bool success)> LoadedCallback;
		LoadedCallback onLoaded = nullptr;

	public:
		// 参数为想创建的矩形大小，若想替换某图片，请传入图片的BoundingBox，此值为缩放后的大小
        static ImageUrl* createWithRect(const cocos2d::Rect rect);

		// 是否成功更新网络图片
		bool isAlreadyLoaded(const std::string& url);

		// 加载本地纹理
		void loadTexture(const std::string& fileName, Widget::TextureResType restype = Widget::TextureResType::LOCAL);

		// 加载网络纹理
		void loadTextureWithUrl(const std::string& url);

		// 网络请求
		void onResponseHeadUrl(int UserID);

		// http请求响应
		//virtual void onHttpResponse(const std::string& requestName, bool isSucceed, std::vector<char> &responseData) override;
		void onHttpResponse(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response);

		// http请求响应
		void onResponseUrl(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response);

		// 获取图片数据
		std::string getImageData();

	protected:
		bool init(const cocos2d::Rect rect);

	protected:
		ImageUrl();

		virtual ~ImageUrl();

	protected:
		std::string _url; // url地址
		std::vector<char> _imageData;
		cocos2d::Size _size = cocos2d::Size::ZERO;
	};
}


#endif // __HN_ImageUrl_h__
