﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_UIExport_H__
#define __HN_UIExport_H__

#include "UI/Base/HNLayer.h"
#include "UI/Base/HNLayerColor.h"
#include "UI/Base/HNNode.h"
#include "UI/Base/HNSprite.h"
#include "UI/Base/HNScene.h"
#include "UI/Base/HNGameUIBase.h"

#include "UI/HNDialogBase.h"
#include "UI/TextSprite.h"
#include "UI/LoadingLayer.h"
#include "UI/HNEditBox.h"
#include "UI/HNShake.h"
#include "UI/HNCalculateBoard.h"
#include "UI/GamePrompt.h"
#include "UI/GameTextField.h"
#include "UI/GameWebView.h"
#include "UI/HNGameRule.h"
#include "UI/HNImageUrl.h"
#include "UI/HNTextView.h"
#include "UI/HNRichTextView.h"

#include "UI/PickerView/HNPickerView.h"
#include "UI/PickerView/HNDatePickerView.h"

#include "UI/Wrapper/HNWrapper.h"
#include "UI/Wrapper/HNSwitchImageViewWrapper.h"

#include "Sound/HNAudioEngine.h"

#include "Tool/Tools.h"
#include "Tool/UITools.h"

using namespace HN;

#endif	//__HN_UIExport_H__