/****************************************************************************
Copyright (c) 2014-2016 Beijing TianRuiDiAn Network Technology Co.,Ltd.
Copyright (c) 2014-2016 ShenZhen Redbird Network Polytron Technologies Inc.

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef PlatformExport_h__
#define PlatformExport_h__


#include "HNLobby/GameBaseLayer/GameInitial.h"
#include "HNLobby/GameBaseLayer/GamePlatform.h"
#include "HNLobby/GameBaseLayer/GameLists.h"
#include "HNLobby/GameBaseLayer/GameRoom.h"
#include "HNLobby/GameBaseLayer/GameDesk.h"
#include "HNLobby/GameBaseLayer/LayerManger.h"

#include "HNLobby/GameUpdate/HNUpdate.h"
#include "HNLobby/GameUpdate/GameResUpdate.h"

#include "HNLobby/CreateRoom/CreateRoom/CreateRoomLayer.h"
#include "HNLobby/CreateRoom/CreateRoom/JoinRoomLayer.h"
#include "HNLobby/CreateRoom/CreateRoom/VipRoomController.h"
#include "HNLobby/CreateRoom/RoomManager/RoomManager.h"
#include "HNLobby/CreateRoom/CreateRoom/GameRecord.h"

#include "HNLobby/GameMenu/GameLandLayer.h"
#include "HNLobby/GameMenu/GameMenu.h"
#include "HNLobby/GameMenu/GameRegisterLayer.h"
#include "HNLobby/GameMenu/GameFindPsd.h"

#include "HNLobby/GameChildLayer/GameBankLayer.h"
#include "HNLobby/GameChildLayer/GameRankingList.h"
#include "HNLobby/GameChildLayer/GameSetLayer.h"
#include "HNLobby/GameChildLayer/ServiceLayer.h"
#include "HNLobby/GameChildLayer/GameExitLayer.h"
#include "HNLobby/GameChildLayer/TurnLayer.h"
#include "HNLobby/GameChildLayer/GameTask.h"
#include "HNLobby/GameChildLayer/GameShareLayer.h"

#include "HNLobby/GameNotice/GameNotice.h"
#include "HNLobby/GameNotice/HNNewsHelper.h"

#include "HNLobby/GamePopularise/GameSpread.h"

#include "HNLobby/GameShop/ProductInfo.h"
#include "HNLobby/GameShop/ShopManager.h"
#include "HNLobby/GameShop/GameStoreLayer.h"
#include "HNLobby/GameShop/GameGiftShop.h"
#include "HNLobby/GameShop/ExchangeDiamonds.h"
#include "HNLobby/GameShop/GameOrderList.h"

#include "HNLobby/GamePersionalCenter/GameUserDataLayer.h"
#include "HNLobby/GamePersionalCenter/GameUserHead.h"
#include "HNLobby/GamePersionalCenter/BindPhone.h"
#include "HNLobby/GamePersionalCenter/ModifyPassword.h"

#include "HNLobby/GameVoice/GameVoiceManager.h"
#include "HNLobby/GameVoice/GameChatLayer.h"

#include "HNLobby/GameReconnection/Reconnection.h"

#include "HNLobby/GameNotifyManger/GameNotifyCenter.h"

#include "HNLobby/GameMatch/GameMatchRegistration.h"
#include "HNLobby/GameMatch/GameMatchController.h"
#include "HNLobby/GameMatch/GameMatchWaiting.h"

#include "HNLobby/GameRecord/RecordList/GameRoomRecord.h"
#include "HNLobby/GameRecord/RecordPlay/BackPlay/BackPlayMaskLayer.h"
#include "HNLobby/GameRecord/RecordPlay/RecordParse/RecordVideoController.h"

#include "HNLobby/GameLocation/GameLocation.h"

#endif // PlatformExport_h__
                                                  
