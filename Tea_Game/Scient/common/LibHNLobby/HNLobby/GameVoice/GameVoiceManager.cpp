/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNLobby/GameVoice/GameVoiceManager.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include <algorithm>
#include <string>
#include <cstdlib>
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"
#include "spine/Json.h"
#include "HNMarketExport.h"

using namespace network;

#define JSON_RESOVE(obj, key) (obj.HasMember(key) && !obj[key].IsNull())

namespace HN
{
	static VoiceManager* pruductManager = nullptr;

	VoiceManager* VoiceManager::getInstance()
	{
		if (nullptr == pruductManager)
		{
			pruductManager = new (std::nothrow) VoiceManager();
			return pruductManager;
		}
		return pruductManager;
	}

	void VoiceManager::destroyInstance()
	{
		CC_SAFE_DELETE(pruductManager);
	}

	VoiceManager::VoiceManager()
	{

	}

	VoiceManager::~VoiceManager()
	{
		HNHttpRequest::getInstance()->removeObserver(this);
	}

	void VoiceManager::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		if (requestName.compare("uploadAudio") == 0)
		{
			onHandleUploadAudio(isSucceed, responseData);
		}
		else if (requestName.compare("downloadAudio") == 0)
		{
			onHandleDownloadAudio(isSucceed, responseData);
		}
	}

	void VoiceManager::onHandleUploadAudio(bool isSucceed, const std::string &responseData)
	{
		log("upload audio str>>>>>1");

		if (!isSucceed)
		{
			log("upload audio str>>>>>2");
			return;
		}

		log("upload audio str>>>>>3");

		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());
		if (!doc.IsObject())
		{
			return;
		}

		int status = 1;

		if (JSON_RESOVE(doc, "status")) status = doc["status"].GetInt();

		log("upload audio str>>>>>%d", status);

		if (0 == status) //获取失败
		{
			return;
		}

		int audioID = 0;
		rapidjson::Value& _array = doc["info"];
		if (JSON_RESOVE(_array[0], "id")) audioID = _array[0]["id"].GetInt();
        
        sendVoiceMessage(audioID);
	}

	void VoiceManager::onHandleDownloadAudio(bool isSucceed, const std::string &responseData)
	{
        if (!isSucceed)
        {
            return;
        }
        
        rapidjson::Document doc;
        doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());
        if (!doc.IsObject())
        {
            return;
        }
        
        int status = 1;
        
        if (JSON_RESOVE(doc, "status")) status = doc["status"].GetInt();
        
        if (0 == status) //获取失败
        {
            return;
        }
        
        std::string audioString = "";
        rapidjson::Value& _array = doc["info"];
        if (JSON_RESOVE(_array[0], "Audio")) audioString = _array[0]["Audio"].GetString();

        playAudio(audioString);
	}

	//开始录音
	void VoiceManager::startRecordAudio()
	{   
		HNAudioEngine::getInstance()->pauseBackgroundMusic();
        Operator::requestChannel("hnvoice", "startRecord");
	}
    
    void VoiceManager::stopRecordAudio(int voiceTime)
    {
        _voiceTime = voiceTime;
        std::string audio = Operator::requestChannel("hnvoice", "stopRecord");
    
		log("recorded audio str>>>>>%s", audio.c_str());
        uploadAudio(audio);
		HNAudioEngine::getInstance()->resumeBackgroundMusic();
    }

	//结束录音
	void VoiceManager::endRecordAudio(std::string audio)
	{
		return;
	}

	//上传音频(http接口)
	void VoiceManager::uploadAudio(std::string &audio)
	{
		std::string url = PlatformConfig::getInstance()->getVoiceUrl();
		std::string requestData = "action=SaveAudio";		
		requestData.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));
		requestData.append("&audio=");
        requestData.append(URLEncode(audio));
        requestData.append(StringUtils::format("&voiceTime=%d", _voiceTime));
		
		log("upload audio requestData>>>>>%s", requestData.c_str());

		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("uploadAudio", HttpRequest::Type::POST, url, requestData);
	}

	//发送TCP消息给游戏服务端, 表示玩家已经发出语音
	void VoiceManager::sendVoiceMessage(int audioID)
	{
		VoiceInfo audioInfo;
		audioInfo.uUserID = PlatformLogic()->loginResult.dwUserID;
		audioInfo.uVoiceID = audioID;
        audioInfo.iVoiceTime = _voiceTime;
		_voiceTime = 0;
		RoomLogic()->sendData(MDM_GR_VOICE, ASS_GR_VOIEC, &audioInfo, sizeof(audioInfo));
	}

	//下载音频(http接口)
	void VoiceManager::downloadAudio(int audioID)
	{
		std::string url = PlatformConfig::getInstance()->getVoiceUrl();
		std::string requestData = "action=GetAudio";
		requestData.append(StringUtils::format("&id=%d", audioID));

		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("downloadAudio", HttpRequest::Type::POST, url, requestData);
	}

	//播放音频
	void VoiceManager::playAudio(std::string &audio)
	{	
		rapidjson::Document doc;
		doc.SetObject();
		rapidjson::Document::AllocatorType &allocator = doc.GetAllocator();
		rapidjson::Value value(rapidjson::kObjectType);

		// 音效
		value.AddMember("data", rapidjson::StringRef(audio.c_str()), allocator);

		// 音量
		value.AddMember("volume", HNAudioEngine::getInstance()->getEffectsVolume(), allocator);

		rapidjson::StringBuffer buffer;
		rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
		value.Accept(writer);

		std::string data = buffer.GetString();

		HNAudioEngine::getInstance()->pauseBackgroundMusic();
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        _callback.selector = CC_CALLBACK_1(VoiceManager::onPlayFinished, this);
		Operator::requestChannel("hnvoice", "playAudio", data, &_callback);
#else
		Operator::requestChannel("hnvoice", "playAudio", data);
#endif
	}
    
    void VoiceManager::onPlayFinished(std::string data)
    {
        bool isRecord = Configuration::getInstance()->getValue("isRecord", Value(true)).asBool();
        if((data.compare("finish") == 0) && !isRecord)
        {
            HNAudioEngine::getInstance()->resumeBackgroundMusic();
        }
    }

	//播放音频
	void VoiceManager::playAudio(int audioID)
	{
		downloadAudio(audioID);
	}

	//取消录音
	void VoiceManager::cancelRecordAudio()
	{
		Operator::requestChannel("hnvoice", "cancelRecord");
	}
    
    void VoiceManager::stopPlayAudio()
    {
        Operator::requestChannel("hnvoice", "stopRlayRecord");
    }
}
