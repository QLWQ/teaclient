/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAME_CHAT_LAYER_H__
#define __GAME_CHAT_LAYER_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNNetExport.h"
#include "HNUIExport.h"

USING_NS_CC;
using namespace cocostudio;
using namespace ui;
using namespace cocostudio::timeline;

struct Chat_Record_Info
{
	INT tag;						//聊天标签
	std::string content;			//聊天内容
	std::string userName;			//聊天玩家昵称
    INT voiceTime;                  //语音时长
	Chat_Record_Info()
	{
		memset(this, 0, sizeof(Chat_Record_Info));
	}
};

class GameChatLayer : public HNLayer, public ui::EditBoxDelegate
{
public:
	GameChatLayer();
	~GameChatLayer();
	CREATE_FUNC(GameChatLayer);
	virtual bool init() override;

	virtual void editBoxReturn(ui::EditBox* editBox) {};
public:
	void showChatLayer();

	void setCustomchat();

	void closeChatLayer();

	std::function<void(const std::string& text)> onSendTextCallBack;

	//处理文本聊天消息
	void onHandleTextMessage(Node* parent, Vec2 bubblePos, std::string msg, const std::string &nickName, bool isBoy, bool isFlipped, int userNo, bool isRecord = false);
	//处理语音聊天消息
	void onHandleVocieMessage(Node* parent, Vec2 bubblePos, int voiceID, const std::string &nickName, bool isFlipped, int userNo, int voiceTime = -1);
    
    void editBoxEditingDidBegin(ui::EditBox* editBox) override;
	void editBoxEditingDidEnd(ui::EditBox* editBox) override;

	void voiceChatUiButtonCallBack(Ref* pSender, Widget::TouchEventType type);
private:
	//聊天界面按钮回调
	void chatUiButtonCallBack(Ref* pSender);
	//语音聊天按钮回调
	//点击表情回调
	void faceEventCallBack(Ref* pSender);
	//点击常用语回调
	void commonEventCallBack(Ref* pSender);
	//聊天记录点击回调
	void chatRecordEventCallBack(Ref* pSender, ui::ListView::EventType type);
	//录音动画
	void playVoiceAni(bool isPlay);
	//设置录音合法
	void setRecordVoiceValid(float dalta);
	//设置录音不合法
	void setRecordVoiceInValid(float dalta);
	//显示聊天记录
	void showChatRecords();
	//增加聊天记录并显示
	void updateRecordListView();
	//更新聊天记录容器
	void addChatRecord(Chat_Record_Info record);
	//显示聊天记录
	void showChatRecord(std::string msg, const std::string &nickName);
	//显示文本聊天气泡
	int showBubble(Node* parent, Vec2 bubblePos, std::string msg, bool isFlipped, int userNo, bool isRecord = false);
	//显示语音聊天气泡
	void showVoiceBubble(Node* parent, Vec2 bubblePos, int voiceID, bool isFlipped, int userNo, bool isRecord, int voiceTime = -1);
	//语音气泡点击回调
	void downLoadVoice(Ref* pSender);
	//判断聊天消息是否是常用语
	void dealChatContent(const std::string& msg, bool isBoy);
	//启动背景音乐和音效
	void resumeBackgroundMusic(float delta);
	//常用语音效
	void playCommonChatEffect(bool isBoy, int index);
    //记录录音时间
    void updateVoiceTime(float dt);
private:
	struct CHAT_UI
	{
		Button*			Button_common;
		Button*			Button_face;
		Button*			Button_record;
		Button*			Button_send;
		Button*			Button_voice;
		Button*			Button_message;
		Button*			Button_content;
        Button*         Button_commonText[9];
        Button*         Button_faceText[27];
		TextField*		TextField_content;
		HNEditBox*		editBox;
		ui::ScrollView* ScrollView_face;
		ui::ScrollView* ScrollView_chat;
		ui::ListView*	ListView_common;
		ui::ListView*	ListView_chatRecord;
		Layout*			layout;
	}_chatUI;
	Node* _voiceAniNode = nullptr;
	std::vector<Chat_Record_Info> _chatRecords; //聊天记录容器
	EventListenerTouchOneByOne* _touchEventListener; //触摸事件监听器
	bool _isRecordedVoiceValid; //录音是否合法
	int _voiceTime = 0;//录音时长
	bool _isNormalEnd = false;  //判断是否是正常结束录音，用来处理录音超时的问题
	bool _isAlreadySend = false; 

	EventListenerCustom*	_listener = nullptr;
	bool _isShowCustomChat = false;
};
#endif