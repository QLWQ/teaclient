/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameChatLayer.h"
#include "HNLobbyExport.h"

static const int FACE_NUM         = 21;   //表情个数
static const int COMMON_CHAT_NUM  = 9;    //聊天常用语个数
static const int TEXT_MAX_WIDTH   = 350;  //文本最大宽度
static const int TEXT_HEIGHT      = 25;   //文本一行高度

GameChatLayer::GameChatLayer() 
:_isRecordedVoiceValid(false)
{
	_isShowCustomChat = false;
}

GameChatLayer::~GameChatLayer()
{
	//HNAudioEngine::getInstance()->resumeBackgroundMusic();
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
}

void GameChatLayer::showChatLayer()
{
	//updateRecordListView();
	_chatUI.Button_face->setEnabled(true);
	_chatUI.Button_common->setEnabled(false);
	_chatUI.Button_record->setEnabled(true);
	_chatUI.ScrollView_face->setVisible(false);
	_chatUI.ListView_common->setVisible(true);
	_chatUI.ListView_chatRecord->setVisible(false);
	this->runAction(Sequence::create(FadeIn::create(0.1f), CallFunc::create([=]()
	{
		this->setVisible(true);
	}), nullptr));
}

void GameChatLayer::setCustomchat()
{
	_isShowCustomChat = true;
	_chatUI.Button_send->setVisible(true);
	//_chatUI.Button_content->setEnabled(false);
	_chatUI.Button_content->setTitleText("");
	_chatUI.Button_content->setVisible(true);
	_chatUI.editBox->setVisible(true);
}

void GameChatLayer::closeChatLayer()
{
	this->runAction(Sequence::create(FadeOut::create(0.1f), CCCallFunc::create([=]()
	{
		this->setVisible(false);
	}), nullptr));
}

bool GameChatLayer::init()
{
	if (!HNLayer::init()) return false;

	this->setVisible(false);
	auto chat_Node = CSLoader::createNode("platform/Chat/chat.csb");
	addChild(chat_Node);
	_chatUI.layout = dynamic_cast<Layout*> (chat_Node->getChildByName("Panel_Chat"));

	auto Button_close = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_close"));
	Button_close->addClickEventListener([=](Ref*){
		closeChatLayer();
	});

	//常用语按钮
	_chatUI.Button_common = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_common"));
	_chatUI.Button_common->addClickEventListener(CC_CALLBACK_1(GameChatLayer::chatUiButtonCallBack, this));

	//表情按钮
	_chatUI.Button_face = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_face"));
	_chatUI.Button_face->addClickEventListener(CC_CALLBACK_1(GameChatLayer::chatUiButtonCallBack, this));

	//聊天记录按钮
	_chatUI.Button_record = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_chat"));
	_chatUI.Button_record->addClickEventListener(CC_CALLBACK_1(GameChatLayer::chatUiButtonCallBack, this));
	_chatUI.Button_record->setVisible(false);

	//发送
	_chatUI.Button_send = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_send"));
	_chatUI.Button_send->addClickEventListener(CC_CALLBACK_1(GameChatLayer::chatUiButtonCallBack, this));
	_chatUI.Button_send->setVisible(false);

	//语音聊天按钮
	_chatUI.Button_voice = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_voice"));
	_chatUI.Button_voice->addClickEventListener(CC_CALLBACK_1(GameChatLayer::chatUiButtonCallBack, this));
	_chatUI.Button_voice->setVisible(false);

	//文本聊天按钮
	_chatUI.Button_message = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_message"));
	_chatUI.Button_message->addClickEventListener(CC_CALLBACK_1(GameChatLayer::chatUiButtonCallBack, this));
	_chatUI.Button_message->setVisible(false);

	//聊天按钮
	_chatUI.Button_content = dynamic_cast<Button*> (_chatUI.layout->getChildByName("Button_chat_content"));
	_chatUI.Button_content->addTouchEventListener(CC_CALLBACK_2(GameChatLayer::voiceChatUiButtonCallBack, this));
	_chatUI.Button_content->setVisible(false);
	//_chatUI.Button_content->setEnabled(false);

	//输入框
	_chatUI.TextField_content = dynamic_cast<TextField*> (_chatUI.layout->getChildByName("TextField_msg"));
	_chatUI.TextField_content->setVisible(false);
	_chatUI.editBox = HNEditBox::createEditBox(_chatUI.TextField_content, this);
	_chatUI.editBox->setVisible(false);

	//聊天记录列表
	_chatUI.ListView_chatRecord = dynamic_cast<ui::ListView*> (_chatUI.layout->getChildByName("ListView_chatRecord"));
	//_chatUI.ListView_chatRecord->addEventListener(ListView::ccListViewCallback(CC_CALLBACK_2(GameChatLayer::chatRecordEventCallBack, this)));
	_chatUI.ListView_chatRecord->setVisible(false);

	//表情列表
	_chatUI.ScrollView_face = dynamic_cast<ui::ScrollView*> (_chatUI.layout->getChildByName("ScrollView_face"));
	std::string str("");
	for (int i = 0; i < FACE_NUM; i++)
	{
		str = StringUtils::format("/:%02d", i);
		_chatUI.Button_faceText[i] = (Button*)_chatUI.ScrollView_face->getChildByName(str);
		_chatUI.Button_faceText[i]->addClickEventListener(CC_CALLBACK_1(GameChatLayer::faceEventCallBack, this));
	}

	//常用语列表
	_chatUI.ListView_common = dynamic_cast<ui::ListView*> (_chatUI.layout->getChildByName("ListView_common"));
	for (int i = 0; i < COMMON_CHAT_NUM; i++)
	{
		str = StringUtils::format("Button_common_%d", i);
		_chatUI.Button_commonText[i] = dynamic_cast<Button*> (_chatUI.ListView_common->getChildByName(str));
		_chatUI.Button_commonText[i]->addClickEventListener(CC_CALLBACK_1(GameChatLayer::commonEventCallBack, this));
	}

	auto bgSize = _chatUI.layout->getBoundingBox();
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = [&](Touch* touch, Event* event)
	{
		if (!this->isVisible())  return false;
		auto target = dynamic_cast<GameChatLayer*>(event->getCurrentTarget());
		Point locationInNode = touch->getLocation();
		Size size = target->getContentSize();
		Rect rect = Rect(0, 0, size.width, size.height);
		if (rect.containsPoint(locationInNode))
		{
			if (bgSize.containsPoint(locationInNode)) return true;
			closeChatLayer();
			return true;
		}
		else
		{
			return false;
		}
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);


	_listener = EventListenerCustom::create("dealUserUpResp", [=](EventCustom* event) {
		
		BYTE seatNo = *(BYTE*)event->getUserData();
		Node* bubbleNode = Director::getInstance()->getRunningScene()->getChildByName(StringUtils::format("bubbleNode%d", seatNo));
		if (bubbleNode != nullptr)
		{
			bubbleNode->removeFromParent();
		}
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);

	return true;
}

//聊天界面按钮回调
void GameChatLayer::chatUiButtonCallBack(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	if (pSender == _chatUI.Button_send)
	{
		if (_chatUI.editBox->getString().empty())
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("输入框不能为空！"));
		}
		else if (_chatUI.editBox->getString().size() >= 90)
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("字数太多！"));
		}
		else
		{
			if (nullptr != onSendTextCallBack)
			{
				if ((RoomLogic()->getRoomRule() & GRR_FORBID_ROOM_TALK) ||
					(RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK))
				{
					GamePromptLayer::create(false)->showPrompt(GBKToUtf8("此房间禁止聊天"));
				}
				else
				{
					onSendTextCallBack(_chatUI.editBox->getString());
				}

				_chatUI.editBox->setString("");
				closeChatLayer();
			}
		}
	}
	else if (pSender == _chatUI.Button_common)
	{
		_chatUI.ScrollView_face->setVisible(false);
		_chatUI.ListView_chatRecord->setVisible(false);
		_chatUI.ListView_common->setVisible(true);
		_chatUI.Button_face->setEnabled(true);
		_chatUI.Button_record->setEnabled(true);
		_chatUI.Button_common->setEnabled(false);
		//_chatUI.Button_message->setVisible(true);
		if (_isShowCustomChat)
		{
			_chatUI.Button_send->setVisible(true);
			_chatUI.editBox->setVisible(true);
			_chatUI.Button_content->setVisible(true);
		}
	}
	if (pSender == _chatUI.Button_face)
	{
		_chatUI.ScrollView_face->setVisible(true);
		_chatUI.ListView_chatRecord->setVisible(false);
		_chatUI.ListView_common->setVisible(false);
		_chatUI.Button_face->setEnabled(false);
		_chatUI.Button_record->setEnabled(true);
		_chatUI.Button_common->setEnabled(true);
		//_chatUI.Button_message->setVisible(true);

		_chatUI.Button_send->setVisible(false);
		_chatUI.editBox->setVisible(false);
		_chatUI.Button_content->setVisible(false);
	}
	if (pSender == _chatUI.Button_record)
	{
		_chatUI.ScrollView_face->setVisible(false);
		_chatUI.ListView_chatRecord->setVisible(true);
		_chatUI.ListView_common->setVisible(false);
		_chatUI.Button_face->setEnabled(true);
		_chatUI.Button_record->setEnabled(false);
		_chatUI.Button_common->setEnabled(true);
		updateRecordListView();
	}
	if (pSender == _chatUI.Button_voice) //语音聊天
	{
		_chatUI.Button_voice->setVisible(false);
		_chatUI.Button_message->setVisible(true);
		_chatUI.Button_content->setEnabled(true);
		_chatUI.Button_content->setTitleText(GBKToUtf8("按住 说话"));
		_chatUI.editBox->setVisible(false);
	}
	if (pSender == _chatUI.Button_message) //文本聊天
	{
		_chatUI.Button_voice->setVisible(true);
		_chatUI.Button_message->setVisible(false);
		_chatUI.Button_content->setEnabled(false);
		_chatUI.Button_content->setTitleText("");
		_chatUI.editBox->setVisible(true);
		_chatUI.editBox->setString("");
		_chatUI.editBox->setString("");
	}
}

void GameChatLayer::voiceChatUiButtonCallBack(Ref* pSender, Widget::TouchEventType type)
{
	Button* voice_chat_btn = (Button*)pSender;
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		{
           /* Configuration::getInstance()->setValue("isRecord", Value(true));
            auto effectValue = HNAudioEngine::getInstance()->getEffectsVolume();
            Configuration::getInstance()->setValue("gameeffectValue", Value(effectValue));*/
           // HNAudioEngine::getInstance()->setEffectsVolume(0.5);
            //HNAudioEngine::getInstance()->stopAllEffect();
            
            ///VoiceManager::getInstance()->stopPlayAudio();
			_isAlreadySend = false;
			VoiceManager::getInstance()->startRecordAudio();
			this->scheduleOnce(schedule_selector(GameChatLayer::setRecordVoiceValid), 0.5f);
			this->scheduleOnce(schedule_selector(GameChatLayer::setRecordVoiceInValid), 10.0f);
			voice_chat_btn->setTitleText(GBKToUtf8("松开 结束"));
			//播放录音动画
			playVoiceAni(true);
            this->schedule(schedule_selector(GameChatLayer::updateVoiceTime), 1.0f);
		}
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		{
			if (_isNormalEnd)
			{
				if (!_isRecordedVoiceValid) //如果录音时间小于0.5s或大于10s，则认为录音时间太短或太长，录音不合法，不会发送
				{
					VoiceManager::getInstance()->cancelRecordAudio();
				}
				else
				{
					VoiceManager::getInstance()->stopRecordAudio(_voiceTime);
					_isAlreadySend = true;
				}
			}
			HNAudioEngine::getInstance()->resumeBackgroundMusic();
			this->unschedule(schedule_selector(GameChatLayer::setRecordVoiceValid));
            this->unschedule(schedule_selector(GameChatLayer::setRecordVoiceInValid));
            this->unschedule(schedule_selector(GameChatLayer::updateVoiceTime));
			voice_chat_btn->setTitleText(GBKToUtf8(""));
			//停止录音动画
			playVoiceAni(false);
            _isRecordedVoiceValid = false;
            _voiceTime = 0;
            closeChatLayer();
            Configuration::getInstance()->setValue("isRecord", Value(false));
            
         /*   float effectValue = Configuration::getInstance()->getValue("gameeffectValue", Value(0)).asFloat();
            HNAudioEngine::getInstance()->setEffectsVolume(effectValue);*/
		}
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED: //滑出按钮，则取消录音
	{
			if (_isNormalEnd || !_isAlreadySend)
			{
				VoiceManager::getInstance()->cancelRecordAudio();
				HNAudioEngine::getInstance()->resumeBackgroundMusic();
			}
			else
			{
				VoiceManager::getInstance()->cancelRecordAudio();
			}
			this->unschedule(schedule_selector(GameChatLayer::setRecordVoiceValid));
            this->unschedule(schedule_selector(GameChatLayer::setRecordVoiceInValid));
            this->unschedule(schedule_selector(GameChatLayer::updateVoiceTime));
			voice_chat_btn->setTitleText(GBKToUtf8(""));
			//停止录音动画
			playVoiceAni(false);
            _isRecordedVoiceValid = false;
            _voiceTime = 0;
            HNAudioEngine::getInstance()->resumeBackgroundMusic();
            Configuration::getInstance()->setValue("isRecord", Value(false));
        
         /*   float effectValue = Configuration::getInstance()->getValue("gameeffectValue", Value(0)).asFloat();
            HNAudioEngine::getInstance()->setEffectsVolume(effectValue);*/
		}
		break;
	default:
		break;
	}
}

//录音动画
void GameChatLayer::playVoiceAni(bool isPlay)
{
	if (isPlay)
	{
		_voiceAniNode = CSLoader::createNode("platform/Chat/voiceChat.csb");
		ActionTimeline* animate = CSLoader::createTimeline("platform/Chat/voiceChat.csb");
		animate->play("animation", true);
		_voiceAniNode->setPosition(_winSize / 2);
		Director::getInstance()->getRunningScene()->addChild(_voiceAniNode, this->getLocalZOrder() + 1);
		auto imgBg = dynamic_cast<Sprite*> (_voiceAniNode->getChildByName("Sprite_bg"));
		auto img = dynamic_cast<Sprite*> (imgBg->getChildByName("Sprite_ani"));
		img->runAction(animate);
	}
	else
	{
		if (_voiceAniNode != nullptr)
		{
			_voiceAniNode->stopAllActions();
			_voiceAniNode->removeFromParent();
			_voiceAniNode = nullptr;
		}
	}
}

void GameChatLayer::setRecordVoiceValid(float dalta)
{
	_isRecordedVoiceValid = true;
	_isNormalEnd = true;
}

void GameChatLayer::setRecordVoiceInValid(float dalta)
{
	if (_isRecordedVoiceValid)
	{
		_isNormalEnd = false;
		_isRecordedVoiceValid = false;
		VoiceManager::getInstance()->stopRecordAudio(_voiceTime); //到了10s的时候自动停止，发送之前10s内的录音
		_isAlreadySend = true;
        //停止录音动画
        playVoiceAni(false);
        _voiceTime = 0;
        closeChatLayer();
	}
}

//点击表情回调
void GameChatLayer::faceEventCallBack(Ref* pSender)
{
	auto btn = (Button*)pSender;
	std::string faceStr = btn->getName();
	if (nullptr != onSendTextCallBack)
	{
		if ((RoomLogic()->getRoomRule() & GRR_FORBID_ROOM_TALK) ||
			(RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK))
		{
			GamePromptLayer::create(false)->showPrompt(GBKToUtf8("此房间禁止聊天"));
		}
		else
		{
			onSendTextCallBack(faceStr);
		}
		closeChatLayer();
	}
}

//点击常用语回调
void GameChatLayer::commonEventCallBack(Ref* pSender)
{
	Button* btn = (Button*)pSender;
	auto text = dynamic_cast<Text*>(btn->getChildByName("Text_context"));
	auto textContent = text->getString();
	if (nullptr != onSendTextCallBack)
	{
		if ((RoomLogic()->getRoomRule() & GRR_FORBID_ROOM_TALK) ||
			(RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK))
		{
			GamePromptLayer::create(false)->showPrompt(GBKToUtf8("此房间禁止聊天"));
		}
		else
		{
			onSendTextCallBack(textContent);
		}
		closeChatLayer();
	}
}

void GameChatLayer::showChatRecords()
{
	for (int msgIndex = 0; msgIndex < _chatRecords.size(); msgIndex++)
	{
		//聊天消息结构体
		Chat_Record_Info chatItem = _chatRecords.at(_chatRecords.size() - msgIndex - 1);
		std::string chatMsg = chatItem.content;
		int chatTag = chatItem.tag;
        std::string userName = chatItem.userName;
        int voiceChatTime = chatItem.voiceTime;

		auto recordItemNode = CSLoader::createNode("platform/Chat/recordItem.csb");
		auto layer = dynamic_cast<Layout*> (recordItemNode->getChildByName("Panel_1"));
		auto Text_name = dynamic_cast<Text*>(layer->getChildByName("Text_name"));
		Text_name->setString(GBKToUtf8(userName.c_str()));
		//获取名称占位宽
		auto nameSize = Text_name->getContentSize();
		auto namePos = Text_name->getPosition();
		int row = 0;
		if (chatTag != 0)//是语音
		{
			showVoiceBubble(layer, Vec2(nameSize.width + namePos.x, namePos.y), chatTag, false, -1, true, voiceChatTime);
		}
		else
		{
			row = showBubble(layer, Vec2(nameSize.width + namePos.x, namePos.y), chatMsg, false, -1, true);
		}
		auto msgLayout = Layout::create();
		if (row > 1)
		{
			msgLayout->setContentSize(Size(layer->getContentSize().width, layer->getContentSize().height + (row - 1) * TEXT_HEIGHT));
		}
		else
		{
			msgLayout->setContentSize(layer->getContentSize());
		}
		msgLayout->addChild(recordItemNode);
		msgLayout->setTouchEnabled(true);
		_chatUI.ListView_chatRecord->pushBackCustomItem(msgLayout);
	}
}

//更新聊天记录
void GameChatLayer::updateRecordListView()
{	
	_chatUI.ListView_chatRecord->removeAllChildrenWithCleanup(true);
	showChatRecords();
}

//更新聊天记录容器
void GameChatLayer::addChatRecord(Chat_Record_Info record)
{
	_chatRecords.push_back(record);

	if (_chatRecords.size() == 101)
	{
		for (int i = 0; i < 100; i++)
		{
			_chatRecords[i] = _chatRecords[i + 1];
		}
		//移除最后加进去的一行
		_chatRecords.pop_back();
	}
}

void GameChatLayer::onHandleTextMessage(Node* parent, Vec2 bubblePos, std::string msg, const std::string &nickName, bool isBoy, bool isFlipped, int userNo, bool isRecord)
{
	//播放聊天
	dealChatContent(msg, isBoy);
	//显示聊天记录
	showChatRecord(msg, nickName);
	//显示聊天气泡
	showBubble(parent, bubblePos, msg, isFlipped, userNo, isRecord);
}

void GameChatLayer::showChatRecord(std::string msg, const std::string &nickName)
{
	//增加聊天记录
	std::string userName = StringUtils::format("[%s说]:", nickName.c_str());
	Chat_Record_Info chat_item;
	chat_item.userName = userName;
	chat_item.content = msg;
	addChatRecord(chat_item);
}

//显示聊天气泡
int GameChatLayer::showBubble(Node* parent, Vec2 bubblePos, std::string msg, bool isFlipped, int userNo, bool isRecord)
{
	Node* bubbleNode = nullptr;
	Vec2 pos = bubblePos;
	if (!isRecord)
	{
		bubbleNode = getParent()->getChildByName(StringUtils::format("bubbleNode%d", userNo));
		if (bubbleNode != nullptr)
		{
			bubbleNode->removeFromParent();
		}
	}
	bubbleNode = CSLoader::createNode("platform/Chat/textBubble.csb");

	if (isRecord)
	{
		parent->addChild(bubbleNode, 99);
	}
	else
	{
		pos = parent->convertToWorldSpace(bubblePos);
		getParent()->addChild(bubbleNode, 99);
	}
	
	bubbleNode->setName(StringUtils::format("bubbleNode%d", userNo));
	
	auto layout = dynamic_cast<Layout*>(bubbleNode->getChildByName("Panel_1"));
	layout->setPosition(pos);
	layout->setScale(0.3f);
	layout->runAction(ScaleTo::create(isRecord ? 0.1f : 0.3f, 1.0f));

	auto Image_bubble = dynamic_cast<ImageView*> (layout->getChildByName("Image_bubble"));
	auto Text_content = dynamic_cast<Text*> (Image_bubble->getChildByName("Text_content"));
	
	//设置气泡扩展方向
	layout->setAnchorPoint(Vec2(isFlipped ? 1 : 0, 0.5f));
	Image_bubble->setAnchorPoint(Vec2(isFlipped ? 1 : 0, 0.5f));

	//判断是表情还是文本
	for (int i = 0; msg[i] != '\0'; i++)
	{
		if (msg[i] == '/' && msg[i + 1] == ':' && msg.compare("/:") != 0)
		{
			char num[3] = { 0, 0, 0 };
			num[0] = msg[i + 2];
			num[1] = msg[i + 3];
			int imageNumber = atoi(num);
			if (imageNumber >= 0 && imageNumber < FACE_NUM)
			{
				//显示表情
				auto bubblePos = Image_bubble->getPosition();
				auto str = StringUtils::format("platform/Chat/chatRes/new/bq/%d.png", imageNumber + 1);
				auto faceSp = Sprite::create(str);
				faceSp->ignoreAnchorPointForPosition(false);
				faceSp->setAnchorPoint(Vec2(isFlipped ? 1 : 0, 0.5f));
				faceSp->setPosition(bubblePos);
				layout->addChild(faceSp);
				Image_bubble->setVisible(false);
				Text_content->setVisible(false);
				break;
			}
		}
		else
		{
			break;
		}
    }
	//设置文本内容
	Text_content->setString(StringUtils::format("%s", GBKToUtf8(msg.c_str())));
	//获取文本初始宽高
	auto textSize = Text_content->getContentSize();
	//计算文本应该显示的行数
	int row = (textSize.width - 5) / TEXT_MAX_WIDTH + 1;
	//文字实际宽
	float textWidth = (float)((row > 1) ? TEXT_MAX_WIDTH : textSize.width);
	//文字实际高
	float textHeight = (float)((row > 1) ? textSize.height + (row - 1) * (TEXT_HEIGHT + 10) : textSize.height);
	//设置文本框大小
	Text_content->ignoreContentAdaptWithSize(false);
	Text_content->setTextAreaSize(Size(textWidth, textHeight));
	//获取气泡初始大小
	auto bubbleSize = Image_bubble->getContentSize();
	//设置气泡大小
	Image_bubble->setContentSize(Size(bubbleSize.width + textWidth, bubbleSize.height + textHeight - textSize.height));
	Text_content->setPositionY(Image_bubble->getContentSize().height / 2);
    
    Text_content->setString(StringUtils::format("%s", GBKToUtf8(msg.c_str())));
	if (!isRecord)
	{
		layout->runAction(Sequence::create(DelayTime::create(3.0f), ScaleTo::create(0.3f, 0.3f), CallFunc::create([=](){
			bubbleNode->removeFromParent();
		}), nullptr));
	}
	return row;
}

//显示语音聊天
void GameChatLayer::onHandleVocieMessage(Node* parent, Vec2 bubblePos, int voiceID, const std::string &nickName,
	bool isFlipped, int userNo, int voiceTime)
{
	//增加聊天记录
	Chat_Record_Info chat_info;
	chat_info.tag = voiceID;
	chat_info.userName = StringUtils::format("[%s说]:", nickName.c_str());
    chat_info.voiceTime = voiceTime;
	addChatRecord(chat_info);
	//显示气泡
	showVoiceBubble(parent, bubblePos, voiceID, isFlipped, userNo, false, voiceTime);
}

//显示语音气泡
void GameChatLayer::showVoiceBubble(Node* parent, Vec2 bubblePos, int voiceID, bool isFlipped, int userNo, bool isRecord, int voiceTime)
{
	Node* bubbleNode = nullptr;
	Vec2 pos = bubblePos;
	if (!isRecord)
	{
		bubbleNode = Director::getInstance()->getRunningScene()->getChildByName(StringUtils::format("bubbleNode%d", userNo));
		if (bubbleNode != nullptr)
		{
			bubbleNode->removeFromParent();
		}
	}

	bubbleNode = CSLoader::createNode("platform/Chat/voiceBubble.csb");
	if (isRecord)
	{
		parent->addChild(bubbleNode, 100);
	}
	else
	{
		pos = parent->convertToWorldSpace(bubblePos);
		Director::getInstance()->getRunningScene()->addChild(bubbleNode, 100);
	}

	bubbleNode->setName(StringUtils::format("bubbleNode%d", userNo));

	auto layout = dynamic_cast<Layout*>(bubbleNode->getChildByName("Panel_1"));
	layout->setPosition(pos);
	//设置气泡方向
	layout->setFlippedX(isFlipped);
	auto Sprite_ani = dynamic_cast<Sprite*> (layout->getChildByName("Sprite_ani"));

	auto Button_voice = dynamic_cast<Button*> (layout->getChildByName("Button_voice"));
	//下载音频文件并播放
	//downLoadVoice(Button_voice); //gjd
	Button_voice->setTag(voiceID);
    Button_voice->addClickEventListener([=](Ref* pSender){
        Button_voice->setTouchEnabled(false);
		//下载音频文件并播放
		downLoadVoice(Button_voice);

		//7.11播放后保存音量大小
		auto effectValue = HNAudioEngine::getInstance()->getEffectsVolume();
		Configuration::getInstance()->setValue("gameeffectValue", Value(effectValue));

		//播放动画
		auto animate = CSLoader::createTimeline("platform/Chat/voiceBubble.csb");
		animate->play("animation", true);
		Sprite_ani->runAction(animate);
		bubbleNode->runAction(Sequence::create(DelayTime::create((float)voiceTime), CallFunc::create([=](){
			if (!isRecord)
			{
				bubbleNode->removeFromParent();
			}
			else
			{
				Button_voice->setTouchEnabled(true);
				Sprite_ani->stopAllActions();
				Sprite_ani->setTexture("platform/Chat/chatRes/voiceAni0.png");
			}
			Button_voice->setTouchEnabled(true);
		}), nullptr));
	});

	//录音时长显示
	auto Text_voiceTime = dynamic_cast<Text*> (layout->getChildByName("Text_voiceTime"));
	if (voiceTime >= 0)
	{
		Text_voiceTime->setString(StringUtils::format("%ds", voiceTime + 1));
	}
	else
	{
		Text_voiceTime->setString("");
	}
    Text_voiceTime->setAnchorPoint(Vec2(isFlipped ? 1 : 0, 0.5f));
    Text_voiceTime->setFlippedX(isFlipped ? true : false);

    //自动播放
	if (!isRecord)
	{
		Button_voice->setTouchEnabled(false);
		//下载音频文件并播放
		downLoadVoice(Button_voice);
		//播放动画
		auto animate = CSLoader::createTimeline("platform/Chat/voiceBubble.csb");
		animate->play("animation", true);
		Sprite_ani->runAction(animate);
		bubbleNode->runAction(Sequence::create(DelayTime::create((float)voiceTime), CallFunc::create([=](){
			if (!isRecord)
			{
				bubbleNode->removeFromParent();
			}
			else
			{
				Button_voice->setTouchEnabled(true);
				Sprite_ani->stopAllActions();
				Sprite_ani->setTexture("platform/Chat/chatRes/voiceAni0.png");
			}
			Button_voice->setTouchEnabled(true);
		}), nullptr));
	}
	
}

void GameChatLayer::downLoadVoice(Ref* pSender)
{

	Button* bubble = dynamic_cast<Button*>(pSender);
	int voiceID = bubble->getTag();
	VoiceManager::getInstance()->downloadAudio(voiceID);

    this->unschedule(schedule_selector(GameChatLayer::resumeBackgroundMusic));
    this->scheduleOnce(schedule_selector(GameChatLayer::resumeBackgroundMusic), 10.f);
}

void GameChatLayer::resumeBackgroundMusic(float delta)
{
    bool isRecord = Configuration::getInstance()->getValue("isRecord", Value(true)).asBool();
    if(!isRecord)
    {
        HNAudioEngine::getInstance()->resumeBackgroundMusic();
    }
}

void GameChatLayer::dealChatContent(const std::string& msg, bool isBoy)
{
	char *str[COMMON_CHAT_NUM] = { "大家好！很高兴见到各位！",
		"快点吧，我等到花儿都谢了！",
		"不要走！决战到天亮！",
		"你是帅哥还是美女呀？",
		"君子报仇，十盘不算晚！",
		"哈哈！手气真好！",
		"为什么受伤的总是我？",
		"你的牌打得太好啦！",
		"怎么又断线了，网络怎么这么差呀！"
	};
	for (int i = 0; i < COMMON_CHAT_NUM; i++)
	{
		if (strcmp(str[i], msg.c_str()) == 0)
		{
			playCommonChatEffect(isBoy, i);
		}
	}
}

//常用语音效
void GameChatLayer::playCommonChatEffect(bool isBoy, int index)
{
	std::string str("platform/sound/chat/");
	isBoy ? str.append("man/") : str.append("woman/");
	std::string tmp = StringUtils::format("chat_%d.mp3", index);
	str.append(tmp);
	HNAudioEngine::getInstance()->playEffect(str.c_str());
}

//记录录音时间
void GameChatLayer::updateVoiceTime(float dt)
{
    _voiceTime++;
}

void GameChatLayer::editBoxEditingDidBegin(ui::EditBox* editBox)
{
    //设置常用语不可点击，设置表情不可点击
	for (int i = 0; i < COMMON_CHAT_NUM; i++)
    {
        _chatUI.Button_commonText[i]->setEnabled(false);
        _chatUI.Button_commonText[i]->setBright(true);
    }
	for (int i = 0; i < FACE_NUM; i++)
    {
        _chatUI.Button_faceText[i]->setEnabled(false);
        _chatUI.Button_faceText[i]->setBright(true);
    }
}

void GameChatLayer::editBoxEditingDidEnd(ui::EditBox* editBox)
{
    this->runAction(Sequence::create(DelayTime::create(0.2f), CallFunc::create([=](){
        //设置常用语不可点击，设置表情不可点击
		for (int i = 0; i < COMMON_CHAT_NUM; i++)
        {
            _chatUI.Button_commonText[i]->setEnabled(true);
        }
		for (int i = 0; i < FACE_NUM; i++)
        {
            _chatUI.Button_faceText[i]->setEnabled(true);
        }
    }) , nullptr));
}
