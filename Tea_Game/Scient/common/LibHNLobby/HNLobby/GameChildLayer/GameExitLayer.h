/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMEXIT_LAYER_H__
#define __GAMEXIT_LAYER_H__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocostudio;
using namespace HN;

class GameExitLayer : public HNLayer
{
public:
	typedef std::function<void()> ExitCallBack;
	ExitCallBack		onExitCallBack = nullptr;

public:
	static GameExitLayer* create(bool bSwitch = false);
	virtual bool init(bool bSwitch);

	void close();

	void showExitLayer(Node* parent, int zorder, int tag = -1);

private:
	void callBackQuit();
	void callBackSwitch();
};

#endif // __GAMEXIT_LAYER_H__