/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMESHARE_LAYER_H__
#define __GAMESHARE_LAYER_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class GameShareLayer : public HNLayer
{
public:
	GameShareLayer();
	virtual ~GameShareLayer();
	
	CREATE_FUNC(GameShareLayer);
	virtual bool init() override;

	//显示界面
	void show();

	//分享总结算界面回调
	typedef std::function<void()> CloseShareCallBack;
	CloseShareCallBack	onCloseShareCallBack = nullptr;

	//设置分享邀请好友信息
	void SetInviteInfo(const std::string& gameName, const std::string& deskPassword, bool bShow = false,const std::string& text = ""); 

	//设置分享信息
	inline void SetShareInfo(const std::string& text, const std::string& titile, const std::string& url, const std::string& imgName = "", Node* captureNode = nullptr)
	{
		_text = text;
		_titile = titile;
		_url = url;
		_imgName = imgName;
		_captureNode = captureNode;
	}
	inline void SetShareCallBack(std::function<void(bool success, int platform, const std::string& errorMsg)> callback){ _callback = callback; }  //设置回调函数
private:
	void closeFunc() override;

	// 点击分享
	void onShareClick(Ref* pRef);
private:
	bool _isTouch; 

	std::string _text;
	std::string _titile;
	std::string _url;
	std::string _imgName;
	Node*		_captureNode; 
	std::function<void(bool success, int platform, const std::string& errorMsg)> _callback;  //回调函数
};

#endif // __GAMESHARE_LAYER_H__
