/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GamePasswordInput.h"
#include "HNNetExport.h"

GamePasswordInput::GamePasswordInput()
	: _editbox_pass(nullptr)
	, onEnterCallback(nullptr)
{

}

GamePasswordInput::~GamePasswordInput()
{

}

GamePasswordInput* GamePasswordInput::create()
{
	GamePasswordInput* room = new GamePasswordInput();
	if(room->init())
	{
		room->autorelease();
		return room;

	}
	CC_SAFE_DELETE(room);
	return nullptr;
}

bool GamePasswordInput::init()
{
	if ( !HNLayer::init()) return false;

	auto csLoader = CSLoader::createNode("platform/passwordInput/passwordInput.csb");
	this->addChild(csLoader);
	csLoader->setPosition(_winSize / 2);

	auto button_close = dynamic_cast<ui::Button*>(csLoader->getChildByName("Button_close"));
	button_close->addClickEventListener([=](Ref*) {
		close();
	});

	auto button_enter = dynamic_cast<ui::Button*>(csLoader->getChildByName("Button_enter"));
	button_enter->addTouchEventListener(CC_CALLBACK_2(GamePasswordInput::onEnterClickCallback, this));

	auto editbox_pass = dynamic_cast<ui::TextField*>(csLoader->getChildByName("TextField_password"));
	editbox_pass->setVisible(false);
	_editbox_pass = HNEditBox::createEditBox(editbox_pass, this);
	_editbox_pass->setPasswordEnabled(true);

	return true;
}

void GamePasswordInput::onEnterClickCallback(Ref* pSender, Widget::TouchEventType type)
{
	if(type != Widget::TouchEventType::ENDED)
	{
		return;
	}

	std::string pass = _editbox_pass->getString();
	if(!pass.empty())
	{
		if(onEnterCallback != nullptr)
		{
			onEnterCallback(pass);
			close();
		}
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("�����뷿������"));
	}
}


