/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameExitLayer.h"
#include "HNOpenExport.h"
#include "SimpleAudioEngine.h"
#include "../GameUpdate/GameResUpdate.h"
#include "../GameReconnection/Reconnection.h"
#include "../GameNotifyManger/GameNotifyCenter.h"

static const char* EXIT_PATH = "platform/exitUi/ExitGameNode.csb";

GameExitLayer* GameExitLayer::create(bool bSwitch/* = false*/)
{
	GameExitLayer *pRet = new GameExitLayer();
	if (pRet && pRet->init(bSwitch))
	{
		pRet->autorelease();
	}
	else
	{
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

void GameExitLayer::close()
{
	this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
		this->removeFromParent();
	}), nullptr));
}

void GameExitLayer::showExitLayer(Node* parent, int zorder, int tag)
{
	CCAssert(nullptr != parent, "parent is nullptr");
	if (0 != tag)
	{
		parent->addChild(this, zorder, tag);
	}
	else
	{
		parent->addChild(this, zorder);
	}

	setPosition(Director::getInstance()->getWinSize() / 2);
}

bool GameExitLayer::init(bool bSwitch)
{
	if (!HNLayer::init()) return false;

	auto winSize = Director::getInstance()->getWinSize();

	auto node = CSLoader::createNode(EXIT_PATH);
	node->setPosition(winSize / 2);
	addChild(node);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_exit"));
	auto layout_BG = dynamic_cast<Layout*>(layout->getChildByName("Panel_BG"));
	layout_BG->setScale(winSize.width / 1280, winSize.height / 720);
	layout_BG->addClickEventListener([=](Ref*){
		close();
	});

	auto imgBG = dynamic_cast<ImageView*>(layout->getChildByName("Image_BG"));

	auto btn_quit = dynamic_cast<Button*>(imgBG->getChildByName("Button_quit"));
	btn_quit->addClickEventListener([=](Ref*){
		callBackQuit();
	});

	auto btn_continue = dynamic_cast<Button*>(imgBG->getChildByName("Button_continue"));
	btn_continue->setVisible(!bSwitch);
	btn_continue->addClickEventListener([=](Ref*){
		close();
	});

	auto btn_swith = dynamic_cast<Button*>(imgBG->getChildByName("Button_switch"));
	btn_swith->setVisible(bSwitch);
	btn_swith->addClickEventListener([=](Ref*){
		callBackSwitch();
	});
	
	return true;
}

void GameExitLayer::callBackQuit()
{
	HNUMengSocial()->destroyInstance();
	GameReconnection()->destroyInstance();
	HNGameNotifyCenter()->destroyInstance();
	HNPlatformConfig()->destroyInstance();
	SpriteFrameCache::getInstance()->removeUnusedSpriteFrames();
    AnimationCache::getInstance()->destroyInstance();
	HNAudioEngine::getInstance()->destroyInstance();
    CocosDenshion::SimpleAudioEngine::getInstance()->end();
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	exit(0);
#endif
}

void GameExitLayer::callBackSwitch()
{
	this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
		if (onExitCallBack) onExitCallBack();
		this->removeFromParent();
	}), nullptr));
}
