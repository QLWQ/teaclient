/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMERANKING_LAYER_H__
#define __GAMERANKING_LAYER_H__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include <vector>

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

class GameRankingList 
	: public HNLayer
	, public TableViewDataSource
	, public TableViewDelegate
{
public:
	GameRankingList();
	virtual ~GameRankingList();

public:
	virtual bool init() override;

	CREATE_FUNC(GameRankingList);

private:
	/***************************TableView**********************************/
	//触摸事件
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);

	//Cell大小
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);

	//添加Cell
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

	//设置Cell个数
	virtual ssize_t numberOfCellsInTableView(TableView *table);

private:
	void closeFunc() override;

	// 获取排行数据
	void getRankListData();

	// 申请打开排行榜回调
	bool openRankListEventSelector(HNSocketMessage* socketMessage);

	// 显示子列表
	Node* showRankItem(Layout* layout, int idx);

	// 显示我的排名
	void showMyRank(int rank);

private:
	TableView *	_tableView	= nullptr;
	vector<MSG_GP_MoneyPaiHangBang_Item> _iList;
	ImageView*	_img_myRank = nullptr;
};


#endif // __GAMERANKING_LAYER_H__