/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameSignLayer.h"
#include <sstream>

static const char* SIGNUI_PATH = "platform/Sign/signUi.csb";

GameSignLayer::GameSignLayer(void)
{
}


GameSignLayer::~GameSignLayer(void)
{	
}

void GameSignLayer::closeFunc()
{
	PlatformLogic()->removeEventSelector(MDM_GP_SIGN, ASS_GP_SIGN_DO);
	PlatformLogic()->removeEventSelector(MDM_GP_SIGN, ASS_GP_SIGN_CHECK);
	if (onCloseCallBack != nullptr)
	{
		onCloseCallBack();
	}
}

bool GameSignLayer::init()
{
	if (!HNLayer::init()) return false;

	// 布局
	auto node = CSLoader::createNode(SIGNUI_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node,101);

	_csNode = dynamic_cast<ImageView*>(node->getChildByName("Image_signBg"));
	if (_winSize.width / _winSize.height > 1.78)
	{
		_csNode->setScale(0.9f);
	}

	// 关闭
	auto btnClose = (Button*)_csNode->getChildByName("Button_close");
	btnClose->addClickEventListener([=](Ref*) {
		close();
	});

	// 领取签到
	auto btnGet = (Button*)_csNode->getChildByName("Button_get");
	if (nullptr != btnGet)
	{
		btnGet->addClickEventListener([=](Ref*) {
			PlatformLogic()->sendData(MDM_GP_SIGN, ASS_GP_SIGN_DO, 0, 0, HN_SOCKET_CALLBACK(GameSignLayer::getSignRewardEventSelector, this));
		});
		
		btnGet->setEnabled(false);
	}
	_btnGet = btnGet;

	// 奖励初始化
	for (int i = 1; i < 8; i++)	{
		updateItemTitle(i);
		updateItemLogo(i);
		updateItemReward(i, i * 1000);
	}

	checkSignData();

	return true;
}

//查询签到信息
void GameSignLayer::checkSignData()
{
	PlatformLogic()->sendData(MDM_GP_SIGN, ASS_GP_SIGN_CHECK, 0, 0, HN_SOCKET_CALLBACK(GameSignLayer::checkSignDataEventSelector, this));
}

//申请查询签到信息回调
bool GameSignLayer::checkSignDataEventSelector(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_S_SIGN_CHECK_RESULT) == socketMessage->objectSize, "MSG_GP_S_SIGN_CHECK_RESULT is error.");
	MSG_GP_S_SIGN_CHECK_RESULT* iCheck = (MSG_GP_S_SIGN_CHECK_RESULT*)socketMessage->object;

	char str[64] = {0};

	if (iCheck->byCountDay > 7)
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("查询奖励失败！"));
		prompt->setCallBack([=]() {
			close();
		});
		return true;
	}

	//// 更新奖励状态
	//for (int i = 1; i <= 7; i++)
	//{
	//	if (i < iCheck->byCountDay)
	//	{ 
	//		updateItemStatus(i, eDisable);
	//	}
	//	else if (i == iCheck->byCountDay)
	//	{
	//		if (0 == iCheck->iRs) {
	//			updateItemStatus(i, eCanGet);
	//			_btnGet->setEnabled(true);
	//			auto imgName = (ImageView*)_btnGet->getChildByName("Image_name");
	//			imgName->loadTexture("platform/Sign/res/lingquqiandao.png");
	//		}
	//		else 
	//		{
	//			updateItemStatus(i, eDisable);
	//		}
	//	}
	//	else
	//	{
	//		updateItemStatus(i, eNormal);
	//	}
	//}

	// 更新状态
	for (int i = 1; i < 8; i++)
	{
		if (i < iCheck->byCountDay)
		{
			updateItemStatus(i, iCheck->iAwardMoney[i-1], iCheck->iAwardJewels[i-1], iCheck->iAwardLotteries[i-1], eDisable);
		}
		else if (i == iCheck->byCountDay)
		{
			if (0 == iCheck->iRs) {
				updateItemStatus(i, iCheck->iAwardMoney[i-1], iCheck->iAwardJewels[i-1], iCheck->iAwardLotteries[i-1], eCanGet);
				_btnGet->setEnabled(true);
			}
			else
			{
				updateItemStatus(i, iCheck->iAwardMoney[i-1], iCheck->iAwardJewels[i-1], iCheck->iAwardLotteries[i-1], eDisable);
			}
		}
		else
		{
			updateItemStatus(i, iCheck->iAwardMoney[i-1], iCheck->iAwardJewels[i-1], iCheck->iAwardLotteries[i-1], eNormal);
		}
	}

	return true;
}

// 申请领取签到奖励回调
bool GameSignLayer::getSignRewardEventSelector(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_S_SIGN_DO_RESULT) == socketMessage->objectSize, "MSG_GP_S_SIGN_DO_RESULT is error.");
	MSG_GP_S_SIGN_DO_RESULT* iGet = (MSG_GP_S_SIGN_DO_RESULT*)socketMessage->object;

	std::string str;
	if(0 == socketMessage->messageHead.bHandleCode)//if (1 == iGet->iRs)
	{
// 		PlatformLogic()->loginResult.i64Money += iGet->iGetMoney;
// 		str.append("恭喜你领取成功，获得");
// 		str.append(StringUtils::toString(iGet->iGetMoney));
// 		str.append("金币奖励！");
// 		if (nullptr != onUpdataUserMoney)
// 		{
// 			onUpdataUserMoney(PlatformLogic()->loginResult.i64Money);
// 		}
// 		
// 		auto meteor = ParticleSystemQuad::create("platform/particle/bigwin_blowout_2.plist");
// 		meteor->setPosition(_winSize / 2);
// 		meteor->setAutoRemoveOnFinish(true);
// 		Director::getInstance()->getRunningScene()->addChild(meteor, 100000001);
		PlatformLogic()->loginResult.iJewels += iGet->iGetJewels;
		str.append("恭喜你领取成功，获得");
		str.append(StringUtils::toString(iGet->iGetJewels));
		str.append("房卡奖励！");
		if (nullptr != onUpdataUserMoney)
		{
			onUpdataUserMoney(PlatformLogic()->loginResult.iJewels);
		}

// 		auto meteor = ParticleSystemQuad::create("platform/particle/bigwin_blowout_2.plist");
// 		meteor->setPosition(_winSize / 2);
// 		meteor->setAutoRemoveOnFinish(true);
// 		Director::getInstance()->getRunningScene()->addChild(meteor, 100000001);
	}
	else
	{
		str.append("领取失败！");
	}

	auto prompt = GamePromptLayer::create();
	prompt->showPrompt(GBKToUtf8(str.c_str()));
	prompt->setCallBack([=]() {
		close();
	});

	return true;
}

// 更新标题
void GameSignLayer::updateItemTitle(int day)
{
	do 
	{
		CC_BREAK_IF(day < 1 || day > 7);
		CC_BREAK_IF(nullptr == _csNode);

		std::string str = StringUtils::format("FileNode_day%d", day);
		auto item = _csNode->getChildByName(str);
		CC_BREAK_IF(nullptr == item);

		/*auto title = (Sprite*)item->getChildByName("Sprite_tianshu");
		CC_BREAK_IF(nullptr == title);

		str = StringUtils::format("platform/Sign/res/tianziti%d.png", day);
		title->setTexture(str);*/
	} while (0);
}

// 更新图标
void GameSignLayer::updateItemLogo(int day)
{
	do
	{
		CC_BREAK_IF(day < 1 || day > 7);
		CC_BREAK_IF(nullptr == _csNode);

		std::string str = StringUtils::format("FileNode_day%d", day);
		auto item = _csNode->getChildByName(str);
		CC_BREAK_IF(nullptr == item);

		auto logo = (ImageView*)item->getChildByName("Image_bg");
		CC_BREAK_IF(nullptr == logo);

		str = StringUtils::format("platform/Sign/res/new/fangka%d.png", day);
		logo->loadTexture(str);

	} while (0);
}

// 更新奖励
void GameSignLayer::updateItemReward(int day, int reward)
{
	do
	{
		CC_BREAK_IF(day < 1 || day > 7);
		CC_BREAK_IF(nullptr == _csNode);

		std::string str = StringUtils::format("FileNode_day%d", day);
		auto item = _csNode->getChildByName(str);
		CC_BREAK_IF(nullptr == item);

	/*	auto money = (TextAtlas*)item->getChildByName("AtlasLabel_money");
		CC_BREAK_IF(nullptr == money);

		str = StringUtils::format("%d", reward);
		money->setString(str);*/

	} while (0);
}

// 设置按钮状态
void GameSignLayer::updateItemStatus(int day, int coin, int diamond, int lottery, SIGN_STATUS status)
{
	do
	{
		CC_BREAK_IF(day < 1 || day > 7);
		CC_BREAK_IF(nullptr == _csNode);

		std::string str = StringUtils::format("FileNode_day%d", day);
		auto item = _csNode->getChildByName(str);
		CC_BREAK_IF(nullptr == item);

		auto lingqu = (ImageView*)item->getChildByName("Image_lingqu");
		auto jiahei = (Layout*)item->getChildByName("Image_jiahei");

		CC_BREAK_IF(nullptr == lingqu || nullptr == jiahei);

		lingqu->setVisible(status == eCanGet);
		jiahei->setVisible(status == eDisable);

		// 奖励额度
		/*auto awardCoin = (TextAtlas*)item->getChildByName("AtlasLabel_money");
		CC_BREAK_IF(nullptr == awardCoin);
		str = StringUtils::format("%d", diamond);
		awardCoin->setString(str);*/

		// 可领取动画
		/*auto light = (ImageView*)lingqu->getChildByName("Image_light");
		CC_BREAK_IF(nullptr == light);

		if (eCanGet == status)
		{
			auto rotate = RotateBy::create(0.1f, 5);
			light->runAction(RepeatForever::create(rotate));
		}
		else {
			light->stopAllActions();
		}*/
	} while (0);
}
