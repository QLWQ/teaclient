/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameUserPopupInfoLayer.h"
#include "HNOpenExport.h"
#include "../LibHNMarket/HNOperator.h"
#include "../GameLocation/GameLocation.h"
#include "../GamePersionalCenter/GameUserHead.h"

static const char* POPUPINFO_PATH = "platform/Chat/PopUserInfoLayer.csb";
static const char* USER_MEN_HEAD = "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";

GameUserPopupInfoLayer::GameUserPopupInfoLayer()
{
	_pLogicBase = nullptr;
	_nMaxPlayerCount = 0;
	_curDir = 0;
	memset(_vecUser, 0, sizeof(_vecUser));
	memset(_actionMoney, 0, sizeof(_actionMoney));
	_node = nullptr;
}

GameUserPopupInfoLayer::~GameUserPopupInfoLayer()
{

}

void GameUserPopupInfoLayer::closeFunc()
{

}

bool GameUserPopupInfoLayer::init()
{
	if (!HNLayer::init()) return false;

	_node = CSLoader::createNode(POPUPINFO_PATH);
	CCAssert(_node != nullptr, "null");
	_node->setPosition(Vec2::ZERO);
	addChild(_node, 101);

	this->setName(GAME_POPUPINFO_LAYER_NAME);
	return true;
}

void GameUserPopupInfoLayer::onEnter()
{
	HNLayer::onEnter();

	if (_node == nullptr)
		return;
	//个人信息界面
	auto userInfo = _vecUser[_curDir];
	float X = _vecUser[_curDir]->fLat;
	float Y = _vecUser[_curDir]->fLnt;
	string strIP = Tools::parseIPAdddress(userInfo->dwUserIP);
	Tools::TrimSpace(userInfo->nickName);
	string strName = userInfo->nickName;
	if (strName.length() > 16)
	{
		strName.erase(17, strName.length());
		strName.append("...");
	}

	int dwUserID = Tools::verifyUserIDByAmdin(userInfo->dwUserID);
	string strID = StringUtils::toString(dwUserID);

	Layout* ting = (Layout*)_node->getChildByName("panel_make");
	ImageView* Bg = (ImageView*)_node->getChildByName("image_bg");
	ting->addTouchEventListener(
		[=](Ref* pSender, Widget::TouchEventType type){
		close();
	});
	Button* btn = (Button*)Bg->getChildByName("btn_close");
	btn->addTouchEventListener(
		[=](Ref* pSender, Widget::TouchEventType type){
		close();
	});
	auto pNameText = (Text*)Bg->getChildByName("userName_0");
	auto pIPText = (Text*)Bg->getChildByName("userIP_0");
	Text* ip = (Text*)Bg->getChildByName("Text_1_0_0");
	if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
	{
		pIPText->setVisible(false);
		ip->setVisible(false);
	}
	auto pIDText = (Text*)Bg->getChildByName("userID_0");
	auto pAddrText = (Text*)Bg->getChildByName("text_addr");
	pAddrText->setString(GBKToUtf8("未知区域"));

	if (userInfo->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID)
	{
		Layout* MyInfo = (Layout*)_node->getChildByName("panel_myInfo");
		MyInfo->setVisible(true);
		Text* txt_RoomCard = (Text*)MyInfo->getChildByName("userRoomCard");
		txt_RoomCard->setString(to_string(PlatformLogic()->loginResult.iJewels));
		Text* txt_userGold = (Text*)MyInfo->getChildByName("userGold");
		txt_userGold->setString(to_string(_actionMoney[userInfo->bDeskStation]));
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////地图地理位置，距离
	if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
	{
		int Index = 0;
		std::vector<UserInfoStruct *> vec;
		std::vector<Text*> vec_text;
		//bool isIPsame = false;
		Layout* panel_location = (Layout*)_node->getChildByName("panel_location");
		for (int i = 0; i < _nMaxPlayerCount; i++)
		{
			BYTE dir = logicToViewSeatNo(i);
			UserInfoStruct* userTemp = _vecUser[dir];
			if (userTemp != nullptr)
			{
				if (userTemp->dwUserID != userInfo->dwUserID)
				{
					Index++;
					if (Index > 3)
					{
						break;
					}
					string str = StringUtils::format("user_%d", Index);
					ImageView* UserBg = (ImageView*)panel_location->getChildByName(str);
					UserBg->setVisible(true);
					ImageView* img_head = dynamic_cast<ImageView*>(UserBg->getChildByName("Image_tempHead"));
					ImageView* ima_frame = dynamic_cast<ImageView*>(UserBg->getChildByName("Image_headFrame"));
					ima_frame->setVisible(false);
					auto userHead = GameUserHead::create(img_head, ima_frame);
					userHead->show();
					userHead->loadTexture(userTemp->bBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);
					userHead->setHeadByFaceID(userTemp->bLogoID);
					if (userTemp->headUrl != ""){
						userHead->loadTextureWithUrl(userTemp->headUrl);
					}
					Text* text_Distance = (Text*)UserBg->getChildByName("Text_distance");
					vec_text.push_back(text_Distance);
					if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
					{
						text_Distance->setString("");
					}
					vec.push_back(userTemp);
				}
			}
		}
		const float float_zero = 0.00001f;
		//查询距离回调
		GameLocation::getInstance()->onGetLocationDistance = [=](bool success, float Distance1, float Distance2, float Distance3)
		{
			if (success)
			{
				float array_distance[3] = { 0.0f };
				array_distance[0] = Distance1;
				array_distance[1] = Distance2;
				array_distance[2] = Distance3;
				if (vec_text.size() == 0)
				{
					return;
				}
				for (int i = 0; i < vec_text.size(); i++)
				{
					if (i < 3)
					{
						vec_text[i]->setString(StringUtils::format(GBKToUtf8("%.2f公里"), array_distance[i]));
					}
				}
				//判断是否是没获取到经纬度信息的用户
				for (int i = 1; i <= min(vec_text.size(), vec.size()); i++)
				{
					float fx = vec[i - 1]->fLat;
					float fy = vec[i - 1]->fLnt;
					if (std::abs(fx) <= float_zero && std::abs(fy) <= float_zero)
					{
						vec_text[i - 1]->setString(GBKToUtf8("未知距离"));
					}
				}
			}
		};

		if (std::abs(X) > float_zero || std::abs(Y) > float_zero)
		{
			if (vec.size() > 0)
			{
				float fx = X;
				float fy = Y;
				float tx1 = 0;
				float ty1 = 0;
				float tx2 = 0;
				float ty2 = 0;
				float tx3 = 0;
				float ty3 = 0;
				for (int i = 1; i <= vec.size(); i++)
				{
					if (i == 1)
					{
						tx1 = vec[i - 1]->fLat;
						ty1 = vec[i - 1]->fLnt;
					}
					else if (i == 2)
					{
						tx2 = vec[i - 1]->fLat;
						ty2 = vec[i - 1]->fLnt;
					}
					else if (i == 3)
					{
						tx3 = vec[i - 1]->fLat;
						ty3 = vec[i - 1]->fLnt;
					}
				}
				GameLocation::getInstance()->getDistanceByLocation(fx, fy, tx1, ty1, tx2, ty2, tx3, ty3);
			}
		}
		else
		{//未开启定位的用户
			for (int i = 1; i <= vec_text.size(); i++)
			{
				vec_text[i - 1]->setString(GBKToUtf8("未知距离"));
			}
		}
		if (pAddrText != nullptr && strlen(userInfo->szLocation) > 0)
		{
			pAddrText->setString(userInfo->szLocation);
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	auto img_head = dynamic_cast<ImageView*>(Bg->getChildByName("Image_tempHead"));
	auto ima_frame = dynamic_cast<ImageView*>(Bg->getChildByName("Image_headFrame"));
	ima_frame->setVisible(false);

	// 更新用户结算榜头像
	auto userHead = GameUserHead::create(img_head, ima_frame);
	userHead->show();
	userHead->loadTexture(userInfo->bBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);
	userHead->setHeadByFaceID(userInfo->bLogoID);
	if (userInfo->headUrl != ""){
		userHead->loadTextureWithUrl(userInfo->headUrl);
	}
	pNameText->setString(GBKToUtf8(strName));
	pIPText->setString(strIP);
	pIDText->setString((strID));

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//动作效果按钮
	for (int i = 0; i < 8; i++)
	{
		string strBtnName = StringUtils::format("btn_Animation_%d", i);
		Button* Btn_Animation = (Button*)Bg->getChildByName(strBtnName);
		Btn_Animation->setTag(i);
		Btn_Animation->setUserData(userInfo);
		Btn_Animation->addTouchEventListener(CC_CALLBACK_2(GameUserPopupInfoLayer::sendAction, this));
	}
}

void GameUserPopupInfoLayer::sendAction(Ref* pSender, Widget::TouchEventType type)
{
	if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
	{
		Button* btn_Animation = dynamic_cast<Button*>(pSender);
		auto UserInfo = (UserInfoStruct*)btn_Animation->getUserData();
		int Animation = btn_Animation->getTag();
		if (UserInfo->dwUserID != RoomLogic()->loginResult.pUserInfoStruct.dwUserID)
		{
			//自己点自己的信息栏动作表情不发消息
			if (_pLogicBase) _pLogicBase->sendActionChatMsg(UserInfo->dwUserID, Animation);
			//发送完成关闭信息弹窗
			//close();
		}
		else
		{
			close();
		}
	}
}

BYTE GameUserPopupInfoLayer::logicToViewSeatNo(BYTE lSeatNO)
{
	auto userInfo = RoomLogic()->loginResult.pUserInfoStruct;
	return (lSeatNO - userInfo.bDeskStation + _nMaxPlayerCount) % _nMaxPlayerCount;
}

void GameUserPopupInfoLayer::setUserInfo(UserInfoStruct* users[],const int& len)
{
	if (users == nullptr)
		return;
	for (int i = 0; i < len;i++)
	{
		_vecUser[i] = users[i];
	}
}

void GameUserPopupInfoLayer::setUserMoney(int* moneys, const int& len)
{
	if (moneys == nullptr)
		return;
	for (int i = 0; i < len; i++)
	{
		_actionMoney[i] = moneys[i];
	}
	auto userInfo = _vecUser[_curDir];
	if (userInfo->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID)
	{
		Layout* MyInfo = (Layout*)_node->getChildByName("panel_myInfo");
		MyInfo->setVisible(true);
		Text* txt_RoomCard = (Text*)MyInfo->getChildByName("userRoomCard");
		txt_RoomCard->setString(to_string(PlatformLogic()->loginResult.iJewels));
		Text* txt_userGold = (Text*)MyInfo->getChildByName("userGold");
		txt_userGold->setString(to_string(_actionMoney[userInfo->bDeskStation]));
	}
}

void GameUserPopupInfoLayer::doAction(BYTE sendSeatNo, BYTE TargetSeatNo, int index, Node* btn_head1, Node* btn_head2)
{
	int giftNo = index;
	auto viewSSeatNo = logicToViewSeatNo(sendSeatNo);
	auto viewTSeatNo = logicToViewSeatNo(TargetSeatNo);

	Vec2 startPos = btn_head1->getParent()->convertToWorldSpace(btn_head1->getPosition());
	Vec2 endPos = btn_head2->getParent()->convertToWorldSpace(btn_head2->getPosition());

	if (giftNo < 0 || giftNo > 7)
	{
		return;
	}
	auto giftSp = Sprite::create();
	//btn_head1->getParent()->getParent()->addChild(giftSp, 10);
	Director::getInstance()->getRunningScene()->addChild(giftSp, 10);
	giftSp->setTexture(StringUtils::format("platform/Games/Action/animation%d.png", giftNo));
	if (viewSSeatNo == 3 && viewTSeatNo == 1 && index == 0)
	{
		giftSp->setFlippedX(true);
	}
	else if (viewSSeatNo == 2 && viewTSeatNo == 1 && index == 0)
	{
		giftSp->setFlippedX(true);
	}
	giftSp->setPosition(startPos);
	giftSp->runAction(Sequence::create(MoveTo::create(0.5f, endPos), CallFunc::create([=](){
		//播放动画
		giftSp->setVisible(false);
		std::string strFile = StringUtils::format("Animation_%d", giftNo);
		createAni(strFile, giftSp);
	}), DelayTime::create(0.5f), CallFunc::create([=](){
		std::string str = StringUtils::format("platform/Games/Action/item%d.mp3", giftNo);
		HNAudioEngine::getInstance()->playEffect(str.c_str());
	}), DelayTime::create(2.5f), RemoveSelf::create(), nullptr));

	//开始执行表情动作的时候关闭
	close();
}

void GameUserPopupInfoLayer::createAni(std::string fileName, Node* playNode)
{
	//CSB场景读取
	std::string CSBName = StringUtils::format("platform/Games/Action/%s.csb", fileName.c_str());
	auto node = CSLoader::createNode(CSBName);
	node->setPosition(playNode->getPosition());

	//CSB内置动画播放
	auto ation = CSLoader::createTimeline(CSBName);
	node->runAction(ation);
	ation->gotoFrameAndPlay(0, false);
	node->runAction(Sequence::create(DelayTime::create(2.5f), RemoveSelf::create(), 0));
	//playNode->getParent()->getParent()->addChild(node, 10);
	Director::getInstance()->getRunningScene()->addChild(node, 10);
}