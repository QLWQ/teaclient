/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRankingList.h"
#include "HNUIExport.h"
#include <string>
#include "../GamePersionalCenter/GameUserHead.h"

static const char* RANKING_PATH			= "platform/rankingList/ranking_Node.csb";
static const char* ITEM_PATH			= "platform/rankingList/list_Node.csb";
static const char* USER_MEN_HEAD		= "platform/head/men_head.png";					
static const char* USER_WOMEN_HEAD		= "platform/head/women_head.png";

// 排行布局
static const char* RANK_UI_PATH = "platform/rankingList/RankUi.csb";

// 排行选项
static const char* RANK_ITEM_UI_PATH = "platform/rankingList/RankItem.csb";


GameRankingList::GameRankingList()
{	
}

GameRankingList::~GameRankingList()
{
	PlatformLogic()->removeEventSelector(MDM_GP_PAIHANGBANG, 0);
}

void GameRankingList::closeFunc()
{
	
}

bool GameRankingList::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(RANK_UI_PATH);
	node->setPosition(_winSize / 2);
	addChild(node);

	auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}

	// 关闭
	auto btnClose = (Button*)img_bg->getChildByName("Button_close");
	btnClose->addClickEventListener([=](Ref*) {
		close();
	});

	// 排行榜背景
	auto layout_rankbg = dynamic_cast<Layout*>(img_bg->getChildByName("Panel_container"));

	//排行榜列表
	_tableView = TableView::create(this, layout_rankbg->getContentSize());
	_tableView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	_tableView->setDelegate(this);
	layout_rankbg->addChild(_tableView);

	_img_myRank = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_loginUser"));

	getRankListData();

	return true;
}

//获取排行数据
void GameRankingList::getRankListData()
{
	MSG_GP_PaiHangBang_In rank;
	rank.count = 20;
	rank.type = 0;

	PlatformLogic()->sendData(MDM_GP_PAIHANGBANG, 0, &rank, sizeof(rank), 
		HN_SOCKET_CALLBACK(GameRankingList::openRankListEventSelector, this));
}

//申请打开排行榜回调
bool GameRankingList::openRankListEventSelector(HNSocketMessage* socketMessage)
{
	if (1 == socketMessage->messageHead.bHandleCode)
	{
		CCAssert(sizeof(MSG_GP_MoneyPaiHangBang_Result) == socketMessage->objectSize, "MSG_GP_MoneyPaiHangBang_Result is error.");
		MSG_GP_MoneyPaiHangBang_Result* iRank = (MSG_GP_MoneyPaiHangBang_Result*)socketMessage->object;

		int userId = PlatformLogic()->loginResult.dwUserID;
		int MyRankNum = -1;
		for (int i = 0; i < 20; i++)
		{
			if (iRank->items[i].i64Money > 0)
			{
				_iList.push_back(iRank->items[i]);
			}
			if( userId == iRank->items[i].iUserID)
			{
				MyRankNum = i + 1;
			}
		}
		showMyRank(MyRankNum);
		_tableView->reloadData();//更新数据
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("查询失败!"));
	}
	return true;
}

//添加Cell
TableViewCell* GameRankingList::tableCellAtIndex(TableView *table, ssize_t idx)
{
	TableViewCell* cell = table->dequeueCell();
	if (!cell)
	{
		cell = new TableViewCell();
		cell->autorelease();
	}

	if (idx >= _iList.size()) return cell;

	auto layout = dynamic_cast<Layout*>(cell->getChildByName("rankLayout"));
	auto layout_rank = showRankItem(layout, idx);
	if (!layout)
	{
		layout_rank->removeFromParentAndCleanup(false);
		layout_rank->setName("rankLayout");
		cell->setContentSize(Size(960, 90));
		layout_rank->setPosition(cell->getContentSize() / 2);
		cell->addChild(layout_rank);
	}

	return cell;
}

Node* GameRankingList::showRankItem(Layout* layout, int idx)
{
	Node* csNode = nullptr;
	if (!layout) {
		csNode = CSLoader::createNode(RANK_ITEM_UI_PATH);
	}
	
	auto iter = _iList.begin();
	MSG_GP_MoneyPaiHangBang_Item ite = *(iter + idx);

	auto sp_rank = dynamic_cast<Sprite*>(csNode->getChildByName("Sprite_rank"));
	auto text_rank = dynamic_cast<TextAtlas*>(csNode->getChildByName("AtlasLabel_rank"));

	if (idx < 3)
	{
		sp_rank->setVisible(true);
		text_rank->setVisible(false);

		sp_rank->setTexture(StringUtils::format("platform/rankingList/res/paiming%d.png", idx + 1));
	}
	else
	{
		sp_rank->setVisible(false);
		text_rank->setVisible(true);

		text_rank->setString(to_string(idx + 1));
	}
	
	// 昵称
	auto text_nick = (Text*)csNode->getChildByName("Text_nicheng");
	std::string nickName(ite.nickName);
	if (!nickName.empty()) {
		text_nick->setString(StringUtils::format("%s", GBKToUtf8(ite.nickName)));
	}
	else {
		text_nick->setString(GBKToUtf8("未知"));
	}

	// 金币
	auto text_gold = (Text*)csNode->getChildByName("Text_jinbi");
	text_gold->setString(StringUtils::format("%lld", ite.i64Money));

	return csNode;
}

// 设置Cell大小
Size GameRankingList::tableCellSizeForIndex(TableView *table, ssize_t idx)
{
	return Size(960, 90);
} 

// 设置Cell个数
ssize_t GameRankingList::numberOfCellsInTableView(TableView *table)
{
	return _iList.size();
}

// Cell触摸事件
void GameRankingList::tableCellTouched(TableView *table, TableViewCell *cell)
{
	log("cell touched at index %d", cell->getIdx());
}

void GameRankingList::showMyRank(int rank)
{
	do 
	{
		// +
		auto img_jia = (ImageView*)_img_myRank->getChildByName("Image_jia");
		CC_BREAK_IF(img_jia == nullptr);
		
		// 排名
		auto text_rank = (TextAtlas*)(_img_myRank->getChildByName("AtlasLabel_rank"));
		CC_BREAK_IF(text_rank == nullptr);

		// 金币
		auto text_coin = (Text*)_img_myRank->getChildByName("Text_jinbi");
		CC_BREAK_IF(text_coin == nullptr);

		// 昵称
		auto text_name = (Text*)_img_myRank->getChildByName("Text_nicheng");
		CC_BREAK_IF(text_name == nullptr);

		if (rank > 0 && rank <= 20)
		{
			text_rank->setString(StringUtils::format("%d", rank));
			img_jia->setVisible(false);
		}
		else
		{
			text_rank->setString("20");
			img_jia->setVisible(true);
		}
		
		std::string nickName(PlatformLogic()->loginResult.nickName);
		if (!nickName.empty()) {
			text_name->setString(StringUtils::format("%s", GBKToUtf8(PlatformLogic()->loginResult.nickName)));
		}
		else {
			text_name->setString(GBKToUtf8("未知"));
		}

		text_coin->setString(StringUtils::format("%lld", PlatformLogic()->loginResult.i64Money));
	} while (0);
}
