#include "GameAgentApply.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "ui/UICheckBox.h"
#include "HNPlatformLogic/HNGameCreator.h"
#include <regex>

#define Word_Empty_ID		GBKToUtf8("代理商ID不能为空!")
#define Word_Empty_Phone	GBKToUtf8("手机号码不能为空!")
#define Word_False_Phone	GBKToUtf8("请输入正确的手机号码!")
#define Sumit_Succeed		GBKToUtf8("提交申请成功，请耐心等待回复!")
static const char* GAME_AGENT_CSB = "platform/AgentApply/Agentapply.csb";
GameAgentApply::GameAgentApply()
{
}

GameAgentApply::~GameAgentApply()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

bool GameAgentApply::init()
{
	if (!HNLayer::init()) return false;
	auto node = CSLoader::createNode(GAME_AGENT_CSB);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node,101);

	auto img_BG = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));

	if (_winSize.width / _winSize.height > 1.78)
	{
		img_BG->setScale(0.9f);
	}
	auto btn_close = dynamic_cast<Button*>(node->getChildByName("Button_close"));
	btn_close->setLocalZOrder(2);
	if (btn_close) btn_close->addClickEventListener([=](Ref*){
		close();
	});
	// 账号输入框
	auto TextField_AgentID = (TextField*)node->getChildByName("TextField_ID");
	TextField_AgentID->setVisible(false);
	_editBoxID = HNEditBox::createEditBox(TextField_AgentID, this);

	_editBoxID->setCascadeOpacityEnabled(true);

	// 密码输入框
	auto textField_Phone = (TextField*)node->getChildByName("TextField_Phone");
	textField_Phone->setVisible(false);
	_editBoxPhone = HNEditBox::createEditBox(textField_Phone, this);

	_editBoxPhone->setCascadeOpacityEnabled(true);

	//提交按钮回调
	_btn_submit = (Button*)node->getChildByName("Button_submit");
	_btn_submit->addClickEventListener(CC_CALLBACK_1(GameAgentApply::submitClickCallback, this));

	return true;
}

void GameAgentApply::submitClickCallback(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	do 
	{
		std::string AgentID = _editBoxID->getString();
		std::string phone = _editBoxPhone->getString(); 
		if (AgentID.empty())
		{
			GamePromptLayer::create()->showPrompt(Word_Empty_ID);
			break;
			
		}
		if (phone.empty())
		{
			GamePromptLayer::create()->showPrompt(Word_Empty_Phone);
			break;
		}
		if (!getTruePhoneNo(phone) || _editBoxPhone->getStringLength() != 11)
		{
			GamePromptLayer::create()->showPrompt(Word_False_Phone);
			break;
		}
		if (!AgentID.empty() && !phone.empty() && getTruePhoneNo(phone))
		{
			submitSucceedCallBack();
			break;
		}
	} while (0);
	
}


void GameAgentApply::submitSucceedCallBack()
{
	std::string url = PlatformConfig::getInstance()->getDailiUrl();
	url.append(StringUtils::format("pid=%s", _editBoxID->getString().c_str()));
	url.append(StringUtils::format("&mobile=%s", _editBoxPhone->getString().c_str()));
	url.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("GetDaili", network::HttpRequest::Type::GET, url);
}

bool GameAgentApply::getTruePhoneNo(std::string str)
{
	std::regex pattern("^[0-9]*$");
	return std::regex_match(str, pattern);
}

void GameAgentApply::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed)
	{
		return;
	}
	if (requestName.compare("GetDaili") == 0)
	{
		this->removeFromParent();
		GamePromptLayer::create()->showPrompt(Sumit_Succeed);
	}
}
