/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "TurnLayer.h"
#include "HNMarketExport.h"
#include <SimpleAudioEngine.h>

using namespace CocosDenshion;

static const char* TURN_UI_NODE = "platform/turnUi/turnUiNode.csb";
static const char* TURN_UI_ORDER = "platform/turnUi/turnOrder.csb";
static const char* TURN_UI_ORDERITEM = "platform/turnUi/turnOrderItem.csb";
static const char* GIFT_TYPE[3] = {"谢谢参与", "张房卡", "积分"};
static const char* TURN_BG_MUSIC = "platform/sound/fruitroll.mp3";         //滚动音效
static const char* TURN_END_EFFECT = "platform/sound/gifteffect.mp3";       //中奖音效
//角度值
static const int turnAngle[GIFT_NUM] = {3, 42, 79, 113, 148, 183, 219, 257, 295, 330};

TurnLayer::TurnLayer()
{

}

TurnLayer::~TurnLayer()
{
}

bool TurnLayer::init()
{
	if (!HNLayer::init()) 
	{
		return false;
	}
	srand(time(NULL));

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("加载中"), 22);

	auto node = CSLoader::createNode(TURN_UI_NODE);
	addChild(node,101);
	_layer = dynamic_cast<Layout*> (node->getChildByName("Panel_1"));
	_layer->addClickEventListener(CC_CALLBACK_1(TurnLayer::btnCallBack, this));
	_closeBtn = dynamic_cast<Button*> (_layer->getChildByName("Button_close"));
	_closeBtn->addClickEventListener(CC_CALLBACK_1(TurnLayer::btnCallBack, this));

	//抽奖按钮
	_startBtn = dynamic_cast<Button*> (_layer->getChildByName("Button_start"));
	_startBtn->addClickEventListener(CC_CALLBACK_1(TurnLayer::btnCallBack, this));

	//奖品图片
	m_get_sprite = (Sprite*)_layer->getChildByName("Sprite_turnBg");
	for (int i = 0; i < GIFT_NUM; i++)
	{
		//_SpGift[i] = dynamic_cast<Sprite*> (m_get_sprite->getChildByName(StringUtils::format("Sprite_gift_%d", i)));
		_TextGift[i] = dynamic_cast<Text*> (m_get_sprite->getChildByName(StringUtils::format("Text_gift_%d", i)));
	}

	//转动指针
	_TurnPointer = dynamic_cast<ImageView*> (_layer->getChildByName("Image_turnPointer"));
	//抽奖记录按钮
	auto choujiangjiluBtn = (Button*)_layer->getChildByName("Button_cjjl");
	choujiangjiluBtn->addClickEventListener(CC_CALLBACK_1(TurnLayer::btnCallBack, this));
	//setRefreshTime(24);
	requestGift();
	requestWithParams();
	return true;
}

void TurnLayer::btnCallBack(Ref* pSender)
{
	auto btn = (Button*)pSender;
	auto btnName = btn->getName();
	if (btnName.compare("Panel_1") == 0)
	{
		//关闭界面
		closeLayer();
	}
	else if (btnName.compare("Button_start") == 0)
	{
		if (!m_isChoujiang)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("您的积分不足，暂时无法进行抽奖！"));
			return;
		}
		
		//开始抽奖
		sendStartLuckDraw();
		_startBtn->setEnabled(false);
		_closeBtn->setEnabled(false);
	}
	else if (btnName.compare("Button_cjjl") == 0)
	{
		sendGetChoujiangjilu();
	}
	
}

void TurnLayer::closeLayer()
{
	this->removeFromParent();
}

//设置消耗的金币数
void TurnLayer::setCostMoney(int money)
{
	auto textCost = dynamic_cast<Text*> (_layer->getChildByName("Text_cost"));
	textCost->setString(StringUtils::format("%d%s",money, GBKToUtf8("礼券")));
	auto textJifen = (Text*)_layer->getChildByName("Text_jifen");
	std::string str = HNToWan(PlatformLogic()->loginResult.iLotteries);
	textJifen->setString(str);
}

//设置免费次数
void TurnLayer::setAllTimes(INT times)
{
	auto textTimesPrompt = dynamic_cast<Text*> (_layer->getChildByName("Text_timesPrompt"));

	textTimesPrompt->setString(StringUtils::format("%s%d%s", GBKToUtf8("每天免费次数："), times, GBKToUtf8("次")));
}

//设置刷新时间
void TurnLayer::setRefreshTime(int time)
{
	auto textRefresh = dynamic_cast<Text*> (_layer->getChildByName("Text_refreshPrompt"));
	
	textRefresh->setString(StringUtils::format("%d%s", time, GBKToUtf8("点刷新次数")));
}

//剩余次数
void TurnLayer::setRestTimes(int restTimes, int allTimes)
{
	auto textRestTimes = dynamic_cast<Text*> (_layer->getChildByName("Text_restTimes"));

	textRestTimes->setString(StringUtils::format("%s%d/%d%s", GBKToUtf8("剩余"), restTimes, allTimes, GBKToUtf8("次")));
}

//请求奖品信息
void TurnLayer::requestGift()
{
	MSG_GP_LuckDraw_Config_Request info;
	info.iUserID = PlatformLogic()->loginResult.dwUserID;
	PlatformLogic()->sendData(MDM_GP_LUCKDRAW, ASS_GP_LUCK_DRAW_CONFIG, &info, sizeof(MSG_GP_LuckDraw_Config_Request),
		HN_SOCKET_CALLBACK(TurnLayer::requestGiftEventSelector, this));
}

//请求奖品信息回复
bool TurnLayer::requestGiftEventSelector(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_LuckDraw_Config_Result) == socketMessage->objectSize, "MSG_GP_LuckDraw_Config_Result is error.");
	MSG_GP_LuckDraw_Config_Result* data = (MSG_GP_LuckDraw_Config_Result*)socketMessage->object;
	
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		_configData = *data;

		if (PlatformLogic()->loginResult.iLotteries < _configData.i64Money)
		{
			//_startBtn->setEnabled(false);
			m_isChoujiang = false;
		}
		setCostMoney(data->i64Money);


		for (int i = 0; i < GIFT_NUM; i++)
		{
			if (data->value[i] > 0)
			{
				std::string nameStr = data->name[i];
				_TextGift[i]->setString(StringUtils::format("%s*%d", GBKToUtf8(nameStr), data->value[i]));
			}
			else
			{
				_TextGift[i]->setString(GBKToUtf8("谢谢参与！"));
			}
		}
	}
	else
	{
		if (PlatformLogic()->loginResult.iLotteries < _configData.i64Money)
		{
			//_startBtn->setEnabled(false);
			m_isChoujiang = false;
		}
		GamePromptLayer::create()->showPrompt(GBKToUtf8("出错啦,请稍后再试"));
	}
	return true;
}

//抽奖请求
void TurnLayer::sendStartLuckDraw()
{
	MSG_GP_LuckDraw_Do_Request info;
	info.iUserID = PlatformLogic()->loginResult.dwUserID;
	PlatformLogic()->sendData(MDM_GP_LUCKDRAW, ASS_GP_LUCK_DRAW_Do, &info, sizeof(MSG_GP_LuckDraw_Do_Request),
		HN_SOCKET_CALLBACK(TurnLayer::LuckDrawEventSelector, this));
}

//抽奖回复
bool TurnLayer::LuckDrawEventSelector(HNSocketMessage* socketMessage)
{
	{
		PlatformLogic()->loginResult.iLotteries -= _configData.i64Money;
		std::string str = HNToWan(PlatformLogic()->loginResult.iLotteries);
		auto textJifen = (Text*)_layer->getChildByName("Text_jifen");
		textJifen->setString(str);
		onSetUserMoney();
	}

	CCAssert(sizeof(MSG_GP_LuckDraw_Do_Result) == socketMessage->objectSize, "MSG_GP_LuckDraw_Do_Result is error.");
	MSG_GP_LuckDraw_Do_Result* data = (MSG_GP_LuckDraw_Do_Result*)socketMessage->object;
	
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		playTurn(data->index);
	}
	else
	{
		//_startBtn->setEnabled(true);
		_closeBtn->setEnabled(true);
		GamePromptLayer::create()->showPrompt(GBKToUtf8("出错啦,请稍后再试"));
	}
	return true;
}

//指针转动
void TurnLayer::playTurn(int areaIndex)
{
	auto effectIdx = SimpleAudioEngine::getInstance()->playEffect(TURN_BG_MUSIC);
	m_get_sprite->setRotation(0);
	auto a1 = EaseExponentialIn::create(RotateBy::create(1.5f, 360));
	auto a2 = RotateBy::create(1.0f, 360 * 4);
	//auto a4 = CCEaseBounceOut::create(RotateTo::create(0.5f, 360));

	int randNum = 12;
	int rotateEnd = 0;

	/*if (_lastIndex > areaIndex)
	{
		rotateEnd = turnAngle[areaIndex] + randNum + (360 - turnAngle[_lastIndex] - _lastRandNum);
	}
	else
	{
		rotateEnd = turnAngle[areaIndex] - (turnAngle[_lastIndex] + _lastRandNum) + randNum;
	}*/
	rotateEnd = (10 - areaIndex) * 36 ;
	
	auto acEnd = EaseExponentialOut::create(RotateBy::create(5.0f, rotateEnd + 1080 ));

	m_get_sprite->runAction(Sequence::create(acEnd, DelayTime::create(1.0f), CallFunc::create([=](){
		SimpleAudioEngine::getInstance()->stopEffect(effectIdx);
	}), DelayTime::create(1.0f), CallFunc::create([=](){
		
		//播放中奖动画
		if (_configData.value[areaIndex] == 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("谢谢参与"));
		}
		else
		{
			std::string str = "";
			SimpleAudioEngine::getInstance()->playEffect(TURN_END_EFFECT);
			if (_configData.type[areaIndex] == 3)
			{
				str = StringUtils::format("%s%s*%d", GBKToUtf8("恭喜你抽中"), GBKToUtf8(_configData.name[areaIndex]), _configData.value[areaIndex]);
			}
			else
			{
				str = StringUtils::format("%s%d%s", GBKToUtf8("恭喜你抽中"), _configData.value[areaIndex], GBKToUtf8(GIFT_TYPE[_configData.type[areaIndex]]));
				if (_configData.type[areaIndex] == 2)
				{
					PlatformLogic()->loginResult.iLotteries = PlatformLogic()->loginResult.iLotteries + _configData.value[areaIndex];
				}
			}
			
			
			GamePromptLayer::create()->showPrompt(str);
		}
		if (PlatformLogic()->loginResult.iLotteries >= _configData.i64Money)
		{
			_startBtn->setEnabled(true);
			m_isChoujiang = true;
		}
		_closeBtn->setEnabled(true);
		_lastIndex = areaIndex;
		_lastRandNum = randNum;
		onUpdataUserMoney();
		if (_configData.type[areaIndex] == 2)
		{
			//std::string str = HNToWan(PlatformLogic()->loginResult.iLotteries + _configData.value[areaIndex]);
			std::string str = HNToWan(PlatformLogic()->loginResult.iLotteries);
			auto textJifen = (Text*)_layer->getChildByName("Text_jifen");
			textJifen->setString(str);
		}
		
		
	}), nullptr));

	//m_get_sprite->runAction(RotateBy::create(4.3f + 0.5f * (rotateEnd / 80.0f), 360));
}

void TurnLayer::requestWithParams()
{
	HttpRequest* request = new HttpRequest();
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(CC_CALLBACK_2(TurnLayer::onHttpResponse, this));
	request->setUrl(PlatformConfig::getInstance()->getTurnImgUrl());
	HttpClient::getInstance()->send(request);
	request->release();
}

void TurnLayer::onHttpResponse(HttpClient *sender, HttpResponse *response)
{
	if (!response->isSucceed())
	{
		return;
	}
	const char* tag = response->getHttpRequest()->getTag();
	if (0 == strcmp("PicGet", tag))
	{
		vector<char> *data = response->getResponseData();
		int data_length = data->size();
		std::string res;
		for (int i = 0; i < data_length; ++i)
		{
			res += (*data)[i];
		}
		res += '\0';
	}

	// 数据转存
	unsigned char* pBuffer = NULL;
	ssize_t bufferSize = 0;
	vector<char> *buffer = response->getResponseData();
	std::string path = FileUtils::sharedFileUtils()->getWritablePath() + "TurnBg1.png";
	pBuffer = FileUtils::sharedFileUtils()->getFileData(path.c_str(), "r", &bufferSize);
	std::string buff(buffer->begin(), buffer->end());

	//保存到本地文件
	FILE *fp = fopen(path.c_str(), "wb+");
	fwrite(buff.c_str(), 1, buffer->size(), fp);
	fclose(fp);
	setTurnBg();

}

void TurnLayer::setTurnBg()
{
	
	unsigned char* pBuffer = NULL;
	ssize_t bufferSize = 0;
	std::string path = FileUtils::sharedFileUtils()->getWritablePath() + "TurnBg1.png";
	pBuffer = FileUtils::sharedFileUtils()->getFileData(path.c_str(), "r", &bufferSize);
	if (!pBuffer)
	{
		return;
	}
	Image* img = new Image();
	img->initWithImageData(pBuffer, bufferSize);
	free(pBuffer);
	Texture2D* texture = new Texture2D();
	bool isImg = texture->initWithImage(img);
	img->release();
	if (!isImg)
	{
		delete texture;
		return;
	}

	//这里建议使用成员变量来保存精灵，不然有可能导致显示白色块，出现异常！
	if (m_get_sprite)
	{
		//m_get_sprite->setTexture(texture);
	}
	texture->release();

	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

}

void TurnLayer::sendGetChoujiangjilu()
{
	MSG_GP_I_LuckDraw_Recod info;
	info.iUserID = PlatformLogic()->loginResult.dwUserID;
	PlatformLogic()->sendData(MDM_GP_LUCKDRAW, ASS_GP_LUCK_DRAW_RECOD, &info, sizeof(MSG_GP_I_LuckDraw_Recod),
		HN_SOCKET_CALLBACK(TurnLayer::getJiluDataSelector, this));
}

bool TurnLayer::getJiluDataSelector(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_O_LuckDraw_Recod) == socketMessage->objectSize, "MSG_GP_O_LuckDraw_Recod is error.");
	MSG_GP_O_LuckDraw_Recod* data = (MSG_GP_O_LuckDraw_Recod*)socketMessage->object;

	auto node = CSLoader::createNode(TURN_UI_ORDER);
	addChild(node, 102);
	auto panel = (Layout*)node->getChildByName("Panel_1");
	auto listviewOreder = (ListView*)panel->getChildByName("ListView_orderItem");
	auto closeBtn = (Button*)panel->getChildByName("Button_close");
	closeBtn->addClickEventListener([=](Ref*){
		node->removeFromParent();
	});
	for (int i = 0; i < 10;i++)
	{
		std::string name = data->name[i];
		time_t Time = data->Time[i];
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		if (p->tm_hour >= 24)
		{
			p->tm_hour -= 24;
		}
		char choujiangTime[64];
		strftime(choujiangTime, sizeof(choujiangTime), "%Y-%m-%d %H:%M", p);
		if (name == "")
		{
			break;
		}
		auto item = addNewJiluItem(choujiangTime, name, data->value[i]);
		listviewOreder->pushBackCustomItem(item);
	}
	return true;
}

Layout* TurnLayer::addNewJiluItem(std::string time, std::string name, int value)
{
	auto node = CSLoader::createNode(TURN_UI_ORDERITEM);
	auto panel = (Layout*)node->getChildByName("Panel_1");
	auto timeText = (Text*)panel->getChildByName("Text_time");
	auto nameText = (Text*)panel->getChildByName("Text_name");
	timeText->setString(time);
	if (value == 0)
	{
		nameText->setString(StringUtils::format("%s", GBKToUtf8(name)));
	}
	else
	{
		nameText->setString(StringUtils::format("%s *%d", GBKToUtf8(name), value));
	}
	
	panel->removeFromParent();
	return panel;
}

