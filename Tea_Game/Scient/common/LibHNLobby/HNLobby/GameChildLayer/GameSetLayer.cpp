/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameSetLayer.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "../../game/CaiShenThirteenCard/Classes/GameMessage/CaiShenThirteenCardMessageHead.h"
#include "../../game/QZHOUMJ/Classes/GameMessage/QZHOUMJ_MessageHead.h"
#include "../../game/ZPUMJ/Classes/GameMessage/ZPUMJ_MessageHead.h"

static const char* SETTING_PATH = "platform/setting/settingNode.csb";

///*** 十三水部分  ***/
//#define		CHEAT_SSS_BUTTON_COUNT		3			//作弊按钮数
//static const std::string _sss_cb_names[CHEAT_SSS_BUTTON_COUNT] = { "cheat_none", "cheat_last", "cheat_win" };
//static const Vec2 _sss_cb_points[CHEAT_SSS_BUTTON_COUNT] = { Vec2(130, 170), Vec2(330, 170), Vec2(630, 170) };
//static const std::string _sss_txt_titles[CHEAT_SSS_BUTTON_COUNT] = { GBKToUtf8("不作弊"), GBKToUtf8("大概率尾道好牌"), GBKToUtf8("必赢") };


GameSetLayer::GameSetLayer()
{
	_uGameID = 0;
}

GameSetLayer::~GameSetLayer()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

void GameSetLayer::closeFunc()
{
	//不根据滑动条设置音量效果音大小
	//UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, _effectSlider->getPercent());
	//UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, _musicSlider->getPercent());
	//UserDefault::getInstance()->flush();
}
bool GameSetLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(SETTING_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node,101);
	_CSBNode = node;

	auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	//img_bg->loadTexture("platform/setting/res/new/bj.png");
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}
	_pImgBg = img_bg;
	// 关闭
	auto btn_guanBi = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btn_guanBi->addClickEventListener([=](Ref*) {
		close();
	});

	
    //音效滑动条
	_effectSlider = (Slider*)img_bg->getChildByName("Slider_yinxiao");
	_effectSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT, 90));
	_effectSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(GameSetLayer::sliderCallback, this)));

    //音乐滑动条
	_musicSlider = (Slider*)img_bg->getChildByName("Slider_yinyue");
	_musicSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT, 90));
	_musicSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(GameSetLayer::sliderCallback, this)));
	
	//动态显示音效和音乐是否开启
	_spSetMusic = (Sprite*)img_bg->getChildByName("Sprite_1");
	_spSetMusic->setTexture(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT,90) ? "platform/setting/res/new/btn_yy1.png" : "platform/setting/res/new/btn_yy2.png");

	_spSetEffect = (Sprite*)img_bg->getChildByName("Sprite_2");
	_spSetEffect->setTexture(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT, 90) ? "platform/setting/res/new/btn_yx1.png" : "platform/setting/res/new/btn_yx2.png");

	////音乐
	//_music_CheckBox = (CheckBox*)img_bg->getChildByName("CheckBox_yinyue");
	//_music_CheckBox->setSelected(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0);
	//_music_CheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(GameSetLayer::yinyueCallBack, this)));
	////CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::shuangjinpinghu, this)

	////音效
	//_effect_CheckBox = (CheckBox*)img_bg->getChildByName("CheckBox_yinxiao");
	//_effect_CheckBox->setSelected(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0);
	//_effect_CheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(GameSetLayer::yinxiaoCallBack, this)));

	////静音
	//_allvolume_CheckBox = (CheckBox*)img_bg->getChildByName("CheckBox_jingyin");
	//_allvolume_CheckBox->setSelected(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0 && UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0);
	//_allvolume_CheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(GameSetLayer::jingyinCallBack, this)));

	////头像框
	//auto CheckBox_touxiang = (CheckBox*)img_bg->getChildByName("CheckBox_touxiang");
	//CheckBox_touxiang->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(GameSetLayer::touxiangCallBack, this)));


	// 切换账号
	auto btn_qieHuan = (Button*)img_bg->getChildByName("Button_qieHuan");
	btn_qieHuan->addClickEventListener(CC_CALLBACK_1(GameSetLayer::onQieHuanClick, this));

	// 离开游戏
	auto btn_liKai = (Button*)img_bg->getChildByName("Button_liKai");
	btn_liKai->addClickEventListener(CC_CALLBACK_1(GameSetLayer::onLiKaiClick, this));

	// 游戏中隐藏切换账号和关闭游戏按钮
	if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INGAME)
	{
		btn_qieHuan->setVisible(false);
		btn_liKai->setVisible(false);
	}

	if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INGAME
		&& RoomLogic() != nullptr && RoomLogic()->getSelectedRoom() != nullptr)
	{
		_uGameID = RoomLogic()->getSelectedRoom()->uNameID;
		if (_uGameID == CaiShenThirteenCard::GAME_NAME_ID ||
			_uGameID == QZHOUMJ::YJNAME_ID || _uGameID == ZPUMJ::NAME_ID)
		{
			
			////这里请求是否能有特殊权限
			//std::string url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), StringUtils::format("/hyapp/getUserInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID));
			////std::string url = StringUtils::format("http://192.168.0.33:8060/hyapp/getUserInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
			//HNHttpRequest::getInstance()->addObserver(this);
			//HNHttpRequest::getInstance()->request("requestCheat", cocos2d::network::HttpRequest::Type::GET, url);
			
			//测试代码
			createCheatSwitch();
		}
	}
	return true;
}

void GameSetLayer::onLiKaiClick(Ref* pRef)
{
	Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	exit(0);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	exit(0);
#endif
}

// 切换账号
void GameSetLayer::onQieHuanClick(Ref* pRef)
{
	if (nullptr != onExitCallBack) {
		if (HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::INPLATFORM  &&  (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST))
		{
			auto PopLayer = GamePromptLayer::create(true);
			PopLayer->showPrompt(GBKToUtf8("当前比赛未结束,若退出游戏后无法加入其他房间!"));
			PopLayer->setCallBack([=](){
				//点击退出按钮
				onExitCallBack();
			});
			PopLayer->setCancelCallBack([=](){
				//点击关闭
				close();
			});
		}
		else
		{
			onExitCallBack();
		}
	}
}

void GameSetLayer::sliderCallback(Ref* pSender, Slider::EventType type)
{
	Slider* pSlider = static_cast<Slider*>(pSender);
	auto name = pSlider->getName();
	int Volume = pSlider->getPercent();
	float l = pSlider->getPercent() / 100.f;

	if (name.compare("Slider_yinyue") == 0)
	{
		if (pSlider->getPercent() == 0)
		{
			HNAudioEngine::getInstance()->setSwitchOfMusic(false);
			HNAudioEngine::getInstance()->pauseBackgroundMusic();
			_spSetMusic->setTexture("platform/setting/res/new/btn_yy2.png");
		}
		else if (!HNAudioEngine::getInstance()->getSwitchOfMusic())
		{
			HNAudioEngine::getInstance()->setSwitchOfMusic(true);
			HNAudioEngine::getInstance()->resumeBackgroundMusic();
			_spSetMusic->setTexture("platform/setting/res/new/btn_yy1.png");
		}
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(l);
		UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, Volume);
	}

	if (name.compare("Slider_yinxiao") == 0)
	{
		if (pSlider->getPercent() == 0)
		{
			HNAudioEngine::getInstance()->setSwitcjofEffect(false);
			_spSetEffect->setTexture("platform/setting/res/new/btn_yx2.png");
		}
		else
		{
			HNAudioEngine::getInstance()->setSwitcjofEffect(true);
			_spSetEffect->setTexture("platform/setting/res/new/btn_yx1.png");
		}
		HNAudioEngine::getInstance()->setEffectsVolume(l);
		UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, Volume);
	}
	UserDefault::getInstance()->flush();
}

void  GameSetLayer::yinyueCallBack(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	bool bTouch = checkBox->isSelected();
	if (checkBox->isSelected())
	{
		HNAudioEngine::getInstance()->pauseBackgroundMusic();
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
		UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 0);
		checkBox->setSelected(true);
		//是否全部静音
		if (UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0 && UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0)
		{
			_allvolume_CheckBox->setSelected(true);
		}
	}
	else
	{
		checkBox->setSelected(false);
		HNAudioEngine::getInstance()->resumeBackgroundMusic();
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(1.0f);
		UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 100);
		if (_allvolume_CheckBox->isSelected())
		{
			_allvolume_CheckBox->setSelected(false);
		}
	}
	UserDefault::getInstance()->flush();
}

void GameSetLayer::yinxiaoCallBack(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox->isSelected())
	{
		//HNAudioEngine::getInstance()->setSwitcjofEffect(false);
		HNAudioEngine::getInstance()->setEffectsVolume(0.0f);
		UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 0);
		//是否全部静音
		if (UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0 && UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0)
		{
			_allvolume_CheckBox->setSelected(true);
		}
	}
	else
	{
		//HNAudioEngine::getInstance()->setSwitcjofEffect(true);
		HNAudioEngine::getInstance()->setEffectsVolume(1.0f);
		UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 100);
		if (_allvolume_CheckBox->isSelected())
		{
			_allvolume_CheckBox->setSelected(false);
		}
	}
	UserDefault::getInstance()->flush();
}
void GameSetLayer::jingyinCallBack(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox->isSelected())
	{
		HNAudioEngine::getInstance()->pauseBackgroundMusic();
		//HNAudioEngine::getInstance()->setSwitcjofEffect(false);
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(0.0);
		HNAudioEngine::getInstance()->setEffectsVolume(0.0);
		UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 0);
		UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 0);
		_effect_CheckBox->setSelected(true);
		_music_CheckBox->setSelected(true);
	}
	else
	{
		HNAudioEngine::getInstance()->resumeBackgroundMusic();
		//HNAudioEngine::getInstance()->setSwitcjofEffect(true);
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(1.0);
		HNAudioEngine::getInstance()->setEffectsVolume(1.0);
		UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 100);
		UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 100);
		_effect_CheckBox->setSelected(false);
		_music_CheckBox->setSelected(false);
	}
	UserDefault::getInstance()->flush();
}

//void  GameSetLayer::touxiangCallBack(Ref* pSender, CheckBox::EventType type)
//{
//	int a = 0;
//}

void GameSetLayer::setGameStateView()
{
	auto img_bg = dynamic_cast<ImageView*>(_CSBNode->getChildByName("Image_bg"));
	Size consize = img_bg->getContentSize();

	/*
	//解散房间
	auto btn_diss = Button::create("platform/setting/res/new/jsfj_an.png", "", "");
	btn_diss->setPositionPercent(Vec2(32,20));
	img_bg->addChild(btn_diss);
	btn_diss->addClickEventListener([=](Ref*) {
		if (nullptr != onDisCallBack)
		{
			onDisCallBack();
		}
	});
	//离开游戏
	auto btn_Exit = Button::create("platform/setting/res/new/tcyx_an.png", "", "");
	btn_Exit->setPositionPercent(Vec2(68,20));
	img_bg->addChild(btn_Exit);
	btn_Exit->addClickEventListener([=](Ref*) {
		if (nullptr != onExitCallBack)
		{
			onExitCallBack();
		}
	});
	*/
	// 解散房间
	auto btn_qieHuan = (Button*)img_bg->getChildByName("Button_qieHuan");
	btn_qieHuan->loadTextureNormal("platform/setting/res/new/jsfj_an.png");
	btn_qieHuan->setPositionPercent(Vec2(0.32, 0.2));
	btn_qieHuan->setVisible(true);
	btn_qieHuan->addClickEventListener([=](Ref*) {
		if (nullptr != onDisCallBack)
		{
			onDisCallBack();
		}
	});

	// 离开游戏
	auto btn_Exit = (Button*)img_bg->getChildByName("Button_liKai");
	btn_Exit->loadTextureNormal("platform/setting/res/new/tcyx_an.png");
	btn_Exit->setPositionPercent(Vec2(0.68, 0.2));
	btn_Exit->addClickEventListener(CC_CALLBACK_1(GameSetLayer::onQieHuanClick, this));
	btn_Exit->setVisible(true);
}

void GameSetLayer::setGameGlodExit()
{
	auto img_bg = dynamic_cast<ImageView*>(_CSBNode->getChildByName("Image_bg"));
	//Size consize = img_bg->getContentSize();

	// 解散房间
	auto btn_dis = (Button*)img_bg->getChildByName("Button_qieHuan");
	btn_dis->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);

	// 离开游戏
	auto btn_exit = (Button*)img_bg->getChildByName("Button_liKai");
	if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
	{
		btn_exit->setPositionPercent(Vec2(0.5,0.2));
		btn_exit->setVisible(true);
	}
	btn_exit->addClickEventListener(CC_CALLBACK_1(GameSetLayer::onQieHuanClick, this));
}


/**  游金麻将作弊部分 ***/

void GameSetLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

	if (doc.HasParseError() || !doc.IsObject())
	{
		return;
	}
	if (requestName.compare("requestCheat") == 0)
	{
		if (doc.HasMember("Cheated") && doc.HasMember("Code"))  //Code = 1标识通讯正常
		{
			int cheated = doc["Cheated"].GetInt();
			int code = doc["Code"].GetInt();
			if (cheated == 1 && code == 1)
			{
				//创建作弊按钮开关
				createCheatSwitch();
			}
		}
	}
}

void GameSetLayer::CheckBoxCheatEvent(Ref* pSender, CheckBox::EventType type)
{
	if (_uGameID == 0) return;
	CheckBox* pCheckBox = dynamic_cast<CheckBox*>(pSender);
	if (pCheckBox)
	{
		if (onShowMJCheatCallBack != nullptr)
		{
			onShowMJCheatCallBack(pCheckBox->isSelected());
		}
	}
}

void GameSetLayer::createCheatSwitch()
{
	if (_uGameID == 0) return;

	auto pCheckBox = CheckBox::create("platform/createRoomUI/createUi/res/new/fx_dt.png", "platform/createRoomUI/createUi/res/new/gou_tb.png");
	pCheckBox->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	pCheckBox->setPosition(Vec2(370, 165));
	pCheckBox->setName("mj_cheat_btn");
	pCheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(GameSetLayer::CheckBoxCheatEvent, this)));
	_pImgBg->addChild(pCheckBox);

	Text* pText = Text::create(GBKToUtf8("特殊权限"), "platform/common/RTWSYueRoudGoG0v1-Regular.ttf", 32);
	pText->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	pText->setPosition(Vec2(120, pCheckBox->getContentSize().height / 2));
	pText->setColor(Color3B(255, 0, 0));
	pCheckBox->addChild(pText);
}

void GameSetLayer::updateCheatStatus(int status)
{
	if (_uGameID == 0) return;
	auto pCheckBox = dynamic_cast<CheckBox*>(_pImgBg->getChildByName("mj_cheat_btn"));
	if (pCheckBox)
		pCheckBox->setSelected(status > 0 ? true : false);
}
