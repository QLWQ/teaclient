/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRules.h"
#include "HNNetExport.h"
#include "HNPlatformLogic/HNGameCreator.h"

static const char* GAMERULES_CSB	= "platform/gameRulesUi/rulesUi_Node.csb";
#define BTN_NORMAL_COLOR	Color3B(161,86,41)    
#define BTN_SELECT_COLOR	Color3B(242,232,210) 

//隐藏规则列表
static const std::vector<std::string> _HideRulesList =
{
	GBKToUtf8("晋江麻将"),
};

GameRules::GameRules()
{
	m_ShowTypeMap.clear();
	m_ShowContentMap.clear();
	m_pListView = nullptr;
	m_pScrollView = nullptr;
	m_pLabel = nullptr;
	m_pSelectButton = nullptr;
	m_nUrlTimes = 0;
}

GameRules::~GameRules()
{
	
}

bool GameRules::init()
{
	if (!HNLayer::init())
	{
		return false;
	}
	auto pNode = CSLoader::createNode(GAMERULES_CSB);
	pNode->setPosition(_winSize / 2);
	addChild(pNode);

	auto image_bg = dynamic_cast<ImageView*>(pNode->getChildByName("Image_bg"));

	if (_winSize.width / _winSize.height > 1.78)
	{
		image_bg->setScale(0.9f);
	}

	auto btn_close = dynamic_cast<Button*>(image_bg->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*){
		close();
	});

	m_pScrollView = dynamic_cast<ui::ScrollView*>(image_bg->getChildByName("ScrollView"));
	m_pScrollView->setScrollBarPositionFromCornerForVertical(Vec2(6, 20));

	auto pText = dynamic_cast<Text*>(m_pScrollView->getChildByName("Text"));
	pText->setVisible(false);

	m_pLabel = Label::create();
	TTFConfig config = m_pLabel->getTTFConfig();
	config.fontFilePath = pText->getFontName();
	config.fontSize = pText->getFontSize();
	m_pLabel->setTTFConfig(config);
	m_pLabel->setTextColor(pText->getTextColor());
	m_pLabel->setPositionX(pText->getParent()->getContentSize().width / 2);
	m_pLabel->setDimensions(pText->getParent()->getContentSize().width - 5, 0);
	m_pLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
	pText->getParent()->addChild(m_pLabel);

	m_pImage = dynamic_cast<ImageView*>(m_pScrollView->getChildByName("Image"));

	m_pListView = dynamic_cast<ListView*>(image_bg->getChildByName("ListView"));
	m_pListView->setScrollBarPositionFromCornerForVertical(Vec2(3, 20));
	return true;
}

void GameRules::addTextInfo(const std::string& title, const std::string content, const std::string& type)
{
	if (std::find(_HideRulesList.begin(), _HideRulesList.end(), title) != _HideRulesList.end())
		return;
	m_ShowTypeMap.insert(std::pair<std::string, std::string>(title, type));
	m_ShowContentMap.insert(std::pair<std::string, std::string>(title, content));
}

void GameRules::addListItem(const std::string& title)
{
	if (m_pListView)
	{
		auto pImg = ImageView::create("platform/common/new/anniu2.png");
		auto pBtn = Button::create();
		pBtn->loadTextureNormal("platform/common/new/anniu2.png");
		pBtn->loadTexturePressed("platform/common/new/anniu1.png");
		pBtn->loadTextureDisabled("platform/common/new/anniu1.png");
		pBtn->setTitleColor(BTN_NORMAL_COLOR); 
		pBtn->setTitleFontName("platform/common/hkljhtjw8.ttc");
		pBtn->setTitleFontSize(44);
		pBtn->setSize(pImg->getSize());
		pBtn->setName(title);
		pBtn->ignoreContentAdaptWithSize(false);
		pBtn->addClickEventListener(CC_CALLBACK_1(GameRules::onClickCallback, this));
		pBtn->setTitleText(title);
		pBtn->setTag(m_pListView->getChildrenCount());
		m_pListView->pushBackCustomItem(pBtn);
	}
}

void GameRules::onClickCallback(Ref* pSender)
{
	m_nUrlTimes = 0;
	if (m_pSelectButton)
	{
		m_pSelectButton->setTitleColor(BTN_NORMAL_COLOR); 
		m_pSelectButton->loadTextureNormal("platform/common/new/anniu2.png");
	}
	auto pBtn = dynamic_cast<Button*>(pSender);
	m_pSelectButton = pBtn;
	std::string szTitle = pBtn->getTitleText();
	pBtn->loadTextureNormal("platform/common/new/anniu1.png");
	pBtn->setTitleColor(BTN_SELECT_COLOR); 
	std::string szContent = m_ShowContentMap[szTitle];
	std::string szType = m_ShowTypeMap[szTitle];
	if (szType == "text")
		updateText(szContent);
	else if (szType == "image")
		updateImage(szContent);
}
void GameRules::updateText(const std::string& content)
{
	if (m_pLabel && m_pImage && m_pScrollView)
	{
		m_pLabel->setVisible(true);
		m_pImage->setVisible(false);
		m_pLabel->setString(content);
		auto s0 = m_pLabel->getContentSize();
		auto pText = dynamic_cast<Text*>(m_pScrollView->getChildByName("Text"));
		Size s1 = Size(m_pScrollView->getContentSize().width, s0.height + abs(pText ? pText->getPositionY() : 40));
		m_pScrollView->setInnerContainerSize(s1);	
		if (s1.height > m_pScrollView->getContentSize().height)
		{
			m_pScrollView->setTouchEnabled(true);
			m_pScrollView->setInnerContainerSize(s1);
			m_pLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			m_pLabel->setPositionY(s1.height - abs(pText ? pText->getPositionY() : 40));
		}
		else if (m_pLabel->getVerticalAlignment() == TextVAlignment::TOP)
		{
			m_pScrollView->setTouchEnabled(false);
			m_pScrollView->setInnerContainerSize(m_pScrollView->getContentSize());
			m_pLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
			m_pLabel->setPositionY(m_pScrollView->getContentSize().height - abs(pText ? pText->getPositionY() : 40));
		}
		else
		{
			m_pScrollView->setTouchEnabled(false);
			m_pLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			m_pLabel->setPositionY(m_pScrollView->getContentSize().height / 2 - abs(pText ? pText->getPositionY() : 40));
		}
	}
}

void GameRules::updateImage(const std::string& img_url)
{
	if (m_pLabel && m_pImage && m_pScrollView)
	{
		m_pLabel->setVisible(false);
		m_pImage->setVisible(true);

		auto func = [=](Size imgSize) {
			m_pImage->loadTexture(img_url, TextureResType::PLIST);
			Size size = m_pScrollView->getContentSize();
			size.width -= 25;

			float scale = size.width / imgSize.width;
			imgSize = imgSize * scale;
			m_pImage->setSize(imgSize);
			m_pScrollView->setInnerContainerSize(imgSize);
			if (imgSize.height > m_pScrollView->getContentSize().height)
			{
				m_pScrollView->setTouchEnabled(true);
				m_pImage->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
				m_pImage->setPositionX(0);
				m_pImage->setPositionY(imgSize.height);
			}
			else
			{
				m_pScrollView->setTouchEnabled(false);
				m_pImage->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
				m_pImage->setPositionX(0);
				m_pImage->setPositionY(size.height);
			}
		};

		auto failFunc = [=](const std::string& url, const int times)
		{
			if (times > 1)  //最多多请求一次
				return;
			updateImage(url);
		};

		if (img_url.find("http://") != string::npos)
		{
			ImageUrl* imgurl = ImageUrl::createWithRect(getBoundingBox());
			imgurl->retain();
			imgurl->onLoaded = [=](bool success) {

				if (success)
				{
					Texture2D* texture = SpriteFrameCache::getInstance()->getSpriteFrameByName(img_url)->getTexture();
					if (texture)
						func(texture->getContentSize());
				}
				else
				{
					failFunc(img_url, ++m_nUrlTimes);
				}		
				//imgurl->runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=](){ imgurl->release(); }), nullptr));
				imgurl->onLoaded = nullptr;
				//imgurl->release();
			};
			imgurl->loadTextureWithUrl(img_url);
		}
		else
		{
			Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(img_url);
			if (texture)
			{
				auto sp = SpriteFrame::create(img_url, Rect(Vec2::ZERO, texture->getContentSize()));
				SpriteFrameCache::getInstance()->addSpriteFrame(sp, img_url);

				func(texture->getContentSize());
			}
		}
	}
}


void GameRules::updateShow()
{
	for (auto it = m_ShowContentMap.begin(); it != m_ShowContentMap.end(); it++)
	{
		addListItem(it->first);
	}
	if (m_pListView->getChildrenCount() > 0)
	{
		auto pBtn = dynamic_cast<Button*>(m_pListView->getChildByTag(0));
		onClickCallback(pBtn);
	}
}
