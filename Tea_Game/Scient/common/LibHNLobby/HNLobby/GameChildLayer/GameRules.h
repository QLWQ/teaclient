/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_GameRules_h__
#define HN_GameRules_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;
using namespace HN;

class GameRules : public HNLayer
{
public:

	virtual bool init() override;

	GameRules();

	virtual ~GameRules();

	CREATE_FUNC(GameRules);

	void addTextInfo(const std::string& title, const std::string content, const std::string& type);

	void updateShow();
private:
	void onClickCallback(Ref* pSender);  

	void addListItem(const std::string& title);

	void updateTextHeight(); 

	void updateText(const std::string& content);

	void updateImage(const std::string& img_url);
private:
	std::unordered_map<std::string, std::string> m_ShowContentMap; 
	std::unordered_map<std::string, std::string> m_ShowTypeMap;
	ListView* m_pListView = nullptr;
	ui::ScrollView*	m_pScrollView;
	Label* m_pLabel = nullptr;
	ImageView* m_pImage;
	Button*  m_pSelectButton;
	int m_nUrlTimes;
};

#endif // HN_GameRules_h__
