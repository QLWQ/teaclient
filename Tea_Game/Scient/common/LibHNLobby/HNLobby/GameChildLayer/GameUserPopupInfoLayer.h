/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GameUserPopupInfoLayer_H__
#define __GameUserPopupInfoLayer_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNLogicExport.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
#define		MAX_PLAYER_COUNT			4
#define		GAME_POPUPINFO_LAYER_NAME   "GameUserPopupInfoLayer"

class GameUserPopupInfoLayer : public HNLayer
{
public:
	GameUserPopupInfoLayer();
	virtual ~GameUserPopupInfoLayer();
	
	CREATE_FUNC(GameUserPopupInfoLayer);
	virtual bool init() override;
	virtual void onEnter();

	//设置指定游戏的logicBase
	inline void setLogicBase(HNGameLogicBase* logic) { _pLogicBase = logic; }

	//设置人数
	inline void setMaxPlayerCount(const int& count) { _nMaxPlayerCount = count; }

	//设置当前方向
	inline void setCurDir(const BYTE& dir){ _curDir = dir; }

	//设置用户信息
	void setUserInfo(UserInfoStruct* users[], const int& len);

	//设置用户金币
	void setUserMoney(int* moneys, const int& len);

	//执行扔表情
	void doAction(BYTE sendSeatNo, BYTE TargetSeatNo, int index, Node* btn_head1, Node* btn_head2);
private:
	void closeFunc() override;

	//发送表情
	void sendAction(Ref* pSender, Widget::TouchEventType type);

	//获取玩家方向
	BYTE logicToViewSeatNo(BYTE lSeatNO);

	//创建动画
	void createAni(std::string fileName, Node* playNode);
private:
	HNGameLogicBase*				_pLogicBase;
	int								_nMaxPlayerCount;		//该游戏最大人数
	UserInfoStruct*					_vecUser[MAX_PLAYER_COUNT];
	INT								_actionMoney[MAX_PLAYER_COUNT];//用户金币
	BYTE							_curDir;
	Node*							_node;
};

#endif // __GameUserPopupInfoLayer_H__
