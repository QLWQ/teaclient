/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef ServiceLayer_h__
#define ServiceLayer_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;

class ServiceLayer : public HNLayer, public HNHttpDelegate
{
public:
	// 初始化
	virtual bool init() override; 

	// 设置位置
	void setBGPositon(Vec2 vec);
	
	//跳转微信
	void JumpWechat(float dt);

	// 创建
	CREATE_FUNC(ServiceLayer);

protected:
	void closeFunc() override;

	// 按钮回调
	void onServiceClick(Ref* pRef);

	// 客服信息回调
	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	// 构造函数
	ServiceLayer();

	// 析构函数
	virtual ~ServiceLayer();

	// 获取客服信息
	void requestServiceInfo();

private:
	// 布局文件
	Node*           _csNode = nullptr;
	
	// 客服电话
	std::string		_phone;

	// QQ
	std::string		_qq;

	// 客服邮箱
	std::string		_email;

	// 按钮
	Button*         _buttonPhone = nullptr;
	Button*         _buttonQQ = nullptr;
	Button*			_buttonWechat = nullptr;

	// 文本
	Text*			_text_phone = nullptr;
	Text*			_text_QQ = nullptr;
	Text*			_text_WeChat = nullptr;
};

#endif // ServiceLayer_h__
