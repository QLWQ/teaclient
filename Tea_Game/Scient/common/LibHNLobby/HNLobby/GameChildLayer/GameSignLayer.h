/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMESIGN_LAYER_H__
#define __GAMESIGN_LAYER_H__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class GameSignLayer : public HNLayer
{
public:
	typedef std::function<void (LLONG money)> UpdataUserMoney;
	UpdataUserMoney onUpdataUserMoney = nullptr;

	typedef std::function<void ()> CloseCallBack;
	CloseCallBack	onCloseCallBack = nullptr;

public:
	GameSignLayer();
	virtual ~GameSignLayer();

public:
	virtual bool init() override;

private:
	void closeFunc() override;

	//查询签到信息
	void checkSignData();

	//申请查询签到信息回调
	bool checkSignDataEventSelector(HNSocketMessage* socketMessage);

	// 更新标题
	void updateItemTitle(int day);

	// 更新图标
	void updateItemLogo(int day);

	// 更新奖励
	void updateItemReward(int day, int reward);

	// 设置按钮状态
	enum SIGN_STATUS
	{
		eNormal, // 正常状态
		eCanGet, // 能领取
		eDisable // 不能领取
	};

	/*
	 * 功能：更新签到奖励选项
	 * 参数：day-第几天 coin-奖励金币 diamond-奖励钻石 lottery-奖励奖券 status-按钮状态
	 */
	void updateItemStatus(int day, int coin, int diamond, int lottery, SIGN_STATUS status);

private:
	//申请领取签到奖励回调
	bool getSignRewardEventSelector(HNSocketMessage* socketMessage);

	ImageView* _csNode = nullptr;
	Button* _btnGet = nullptr;

public:
	CREATE_FUNC(GameSignLayer);
};

#endif // __GAMESIGN_LAYER_H__