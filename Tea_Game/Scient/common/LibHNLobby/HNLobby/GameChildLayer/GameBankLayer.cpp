/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameBankLayer.h"
#include "HNMarketExport.h"
#include <string> 

// 布局文件
static const char* BANK_UI_PATH = "platform/bankUi/BankUi.csb";

GameBankLayer* GameBankLayer::createGameBank()
{
	GameBankLayer *pRet = new GameBankLayer(); 
	if (pRet && pRet->init()) 
	{ 
		pRet->autorelease(); 
		return pRet; 
	}
	CC_SAFE_DELETE(pRet);
	return pRet;
}

GameBankLayer::GameBankLayer()
{
	
}

GameBankLayer::~GameBankLayer()
{
	PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_OPEN);
	PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_CHPWD);
	PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_CHECK);
	PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_TRANSFER);
}

void GameBankLayer::closeFunc()
{
	
}

bool GameBankLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(BANK_UI_PATH);
	node->setPosition(_winSize / 2);
	addChild(node);

	_csNode = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		_csNode->setScale(0.9f);
	}

	bool bShow = Configuration::getInstance()->getValue("transfer", Value(false)).asBool();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	if (HNPlatformConfig()->getIsIAP())
	{
		bShow = false;
	}
#endif
	
	// 关闭
	auto closeBtn = dynamic_cast<Button*>(_csNode->getChildByName("Button_close"));
	closeBtn->addClickEventListener([=](Ref*){
		close();
	});

	// 存储
	_btnStore = (Button*)_csNode->getChildByName("Button_store");
	_btnStore->addClickEventListener(CC_CALLBACK_1(GameBankLayer::topButtonClickCallBack, this));

	// 资料
	_btnInfo = (Button*)_csNode->getChildByName("Button_info");
	_btnInfo->addClickEventListener(CC_CALLBACK_1(GameBankLayer::topButtonClickCallBack, this));

	// 转账
	_btnTranslate = (Button*)_csNode->getChildByName("Button_translate");
	_btnTranslate->addClickEventListener(CC_CALLBACK_1(GameBankLayer::topButtonClickCallBack, this));

	// 转账是否显示
	if (!bShow)
	{
		_btnTranslate->setVisible(false);
		_btnTranslate->setEnabled(false);
	}

	// 默认使用存储
	changeSelect(SelectOption::eStore);

	// 金额显示
	_curMoney = (TextAtlas*)_csNode->getChildByName("AtlasLabel_xianjin");
	_depositMoney = (TextAtlas*)_csNode->getChildByName("AtlasLabel_cunkuan");

	// 内容面板
	_storeLayout = dynamic_cast<Layout*>(_csNode->getChildByName("Panel_store"));
	_infoLayout = dynamic_cast<Layout*>(_csNode->getChildByName("Panel_info"));
	_translateLayout = dynamic_cast<Layout*>(_csNode->getChildByName("Panel_translate"));

	// 存储
	loadOperatorUI();

	// 资料
	loadModifyUI();

	// 转账
	loadGiroUI();
	
	// 打开银行
	PlatformLogic()->sendData(MDM_GP_BANK, ASS_GP_BANK_OPEN, nullptr, 0 , HN_SOCKET_CALLBACK(GameBankLayer::bankerEventSelector, this));

	return true;

}

void GameBankLayer::topButtonClickCallBack(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
	Button* btn = (Button*)pSender;
	std::string name = ((Button*)pSender)->getName();

	SelectOption option = SelectOption::eNone;

	if (name == "Button_store") {
		option = SelectOption::eStore;
	}
	else if (name == "Button_info") {
		option = SelectOption::eInfo;
	}
	else if (name == "Button_translate") {
		option = SelectOption::eTranslate;
	}

	if (SelectOption::eNone != option) {
		changeSelect(option);
	}
}

void GameBankLayer::changeSelect(SelectOption option)
{
	do 
	{
		// 存储
		_btnStore->setEnabled(option != SelectOption::eStore);
		
		auto imgStore = (ImageView*)_btnStore->getChildByName("Image_name");
		CC_BREAK_IF(imgStore == nullptr);
		imgStore->loadTexture(option == SelectOption::eStore ? "platform/bankUi/res/cunchu1.png" : "platform/bankUi/res/cunchu2.png");

		auto layoutStore = (Layout*)_csNode->getChildByName("Panel_store");
		layoutStore->setVisible(option == SelectOption::eStore);

		// 信息
		_btnInfo->setEnabled(option != SelectOption::eInfo);

		auto imgInfo = (ImageView*)_btnInfo->getChildByName("Image_name");
		CC_BREAK_IF(imgInfo == nullptr);
		imgInfo->loadTexture(option == SelectOption::eInfo ? "platform/bankUi/res/ziliao1.png" : "platform/bankUi/res/ziliao2.png");

		auto layoutInfo = (Layout*)_csNode->getChildByName("Panel_info");
		layoutInfo->setVisible(option == SelectOption::eInfo);

		// 转账
		_btnTranslate->setEnabled(option != SelectOption::eTranslate);

		auto imgTranslate = (ImageView*)_btnTranslate->getChildByName("Image_name");
		CC_BREAK_IF(imgTranslate == nullptr);
		imgTranslate->loadTexture(option == SelectOption::eTranslate ? "platform/bankUi/res/zhuanzhang1.png" : "platform/bankUi/res/zhuanzhang2.png");

		auto layoutTranslate = (Layout*)_csNode->getChildByName("Panel_translate");
		layoutTranslate->setVisible(option == SelectOption::eTranslate);

	} while (0);
}

// 加载操作界面
void GameBankLayer::loadOperatorUI()
{
	// 金额
	auto textField_num = dynamic_cast<TextField*>(_storeLayout->getChildByName("Image_input1")->getChildByName("TextField_coin"));
	textField_num->setVisible(false);
	auto editBox_num = HNEditBox::createEditBox(textField_num, this);
	editBox_num->setInputMode(cocos2d::ui::EditBox::InputMode::PHONE_NUMBER);

	// 密码
	auto imgPsdBg = (ImageView*)_storeLayout->getChildByName("Image_input2");
	auto textField_psd = dynamic_cast<TextField*>(imgPsdBg->getChildByName("TextField_pass"));
	textField_psd->setVisible(false);
	auto editBox_psd = HNEditBox::createEditBox(textField_psd, this);
	editBox_psd->setPasswordEnabled(true);
	auto imgPsd = (ImageView*)_storeLayout->getChildByName("Image_t4");

	auto checkBox_save = dynamic_cast<CheckBox*>(_storeLayout->getChildByName("CheckBox_store"));
	auto checkBox_take = dynamic_cast<CheckBox*>(_storeLayout->getChildByName("CheckBox_get"));

	checkBox_save->addClickEventListener([=](Ref*){
		checkBox_save->setEnabled(false);
		checkBox_take->setEnabled(true);
		editBox_psd->setEnabled(false);
		checkBox_take->setSelectedState(false);
		editBox_psd->setVisible(false);
		imgPsd->setVisible(false);
		imgPsdBg->setVisible(false);
	});

	checkBox_take->addClickEventListener([=](Ref*){
		checkBox_take->setEnabled(false);
		checkBox_save->setEnabled(true);
		editBox_psd->setEnabled(true);
		checkBox_save->setSelectedState(false);
		editBox_psd->setVisible(true);
		imgPsd->setVisible(true);
		imgPsdBg->setVisible(true);
	});

	//这种写法不能在编辑器或者在使用封闭函数之前关闭点击，否则无法开启点击
	checkBox_save->setEnabled(false);
	editBox_psd->setEnabled(false);
	editBox_psd->setVisible(false);
	imgPsd->setVisible(false);
	imgPsdBg->setVisible(false);

	auto btn_sure = dynamic_cast<Button*>(_storeLayout->getChildByName("Button_sure"));
	btn_sure->addClickEventListener([=](Ref*){

		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
		// 银行打开失败，不做任何处理
		if (!_bankOpen) return;
		// 存款
		if (checkBox_save->isSelected())
		{
			std::string strMoney = editBox_num->getString();
			do
			{
				if (strMoney.empty())
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入存款金额。")); break;
				}

				TMSG_GP_BankCheck BankCheck = { 0 };
				BankCheck.operate_type = 2;
				BankCheck.user_id = PlatformLogic()->loginResult.dwUserID;
				BankCheck.game_id = 0;
				BankCheck.money = atol(strMoney.c_str());
				if (BankCheck.money <= 0)
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入合法金额。"));
					break;
				}
				PlatformLogic()->sendData(MDM_GP_BANK, ASS_GP_BANK_CHECK, &BankCheck, sizeof(BankCheck),
					HN_SOCKET_CALLBACK(GameBankLayer::bankerEventSelector, this));
			} while (0);
		}
		// 取款
		else if (checkBox_take->isSelected())
		{
			std::string strMoney = editBox_num->getString();
			std::string password = editBox_psd->getString();
			do
			{
				if (strMoney.empty())
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入取款金额。")); break;
				}

				if (password.empty())
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入银行密码。")); break;
				}
				TMSG_GP_BankCheck BankCheck = { 0 };
				BankCheck.operate_type = 1;
				BankCheck.user_id = PlatformLogic()->loginResult.dwUserID;
				BankCheck.game_id = 0;
				BankCheck.money = atol(strMoney.c_str());
				strcpy(BankCheck.szMD5Pass, MD5_CTX::MD5String(password).c_str());
				if (BankCheck.money <= 0)
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入合法金额。"));
					break;
				}
				PlatformLogic()->sendData(MDM_GP_BANK, ASS_GP_BANK_CHECK, &BankCheck, sizeof(BankCheck),
					HN_SOCKET_CALLBACK(GameBankLayer::bankerEventSelector, this));
			} while (0);
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请选择操作类型。"));
		}

		editBox_num->setString("");
		editBox_psd->setString("");
	});
}

// 加载修改密码界面
void GameBankLayer::loadModifyUI()
{
	//获取三个输入框
	auto textField_old = dynamic_cast<TextField*>(_infoLayout->getChildByName("Image_old")->getChildByName("TextField_old"));
	textField_old->setVisible(false);
	auto editBox_old = HNEditBox::createEditBox(textField_old, this);
	editBox_old->setPasswordEnabled(true);

	auto textField_new = dynamic_cast<TextField*>(_infoLayout->getChildByName("Image_new")->getChildByName("TextField_new"));
	textField_new->setVisible(false);
	auto editBox_new = HNEditBox::createEditBox(textField_new, this);
	editBox_new->setPasswordEnabled(true);

	auto textField_sure = dynamic_cast<TextField*>(_infoLayout->getChildByName("Image_queding")->getChildByName("TextField_queding"));
	textField_sure->setVisible(false);
	auto editBox_sure = HNEditBox::createEditBox(textField_sure, this);
	editBox_sure->setPasswordEnabled(true);

	auto btn_sure = dynamic_cast<Button*>(_infoLayout->getChildByName("Button_sure"));
	btn_sure->addClickEventListener([=](Ref*){

		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
		// 银行打开失败，不做任何处理
		if (!_bankOpen) return;

		bool canModify = false;
		std::string oldPassword = editBox_old->getString();
		std::string newPassword = editBox_new->getString();
		std::string surePassword = editBox_sure->getString();
		do
		{
			std::string errorText("");
			if (oldPassword.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("旧密码为空。")); break;
			}

			if (newPassword.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("新密码为空。")); break;
			}

			if (surePassword.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("确认密码为空。")); break;
			}

			if (surePassword.compare(newPassword))
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("两次输入密码不同。")); break;
			}

			TMSG_GP_BankChPwd BankChPwd = { 0 };
			BankChPwd.user_id = PlatformLogic()->loginResult.dwUserID;
			strcpy(BankChPwd.MD5Pass_old, MD5_CTX::MD5String(oldPassword).c_str());
			strcpy(BankChPwd.MD5Pass_new, MD5_CTX::MD5String(newPassword).c_str());
			PlatformLogic()->sendData(MDM_GP_BANK, ASS_GP_BANK_CHPWD, &BankChPwd, sizeof(BankChPwd),
				HN_SOCKET_CALLBACK(GameBankLayer::bankerEventSelector, this));

			editBox_old->setString("");
			editBox_new->setString("");
			editBox_sure->setString("");
		} while (0);
	});
}

// 加载转账界面
void GameBankLayer::loadGiroUI()
{
	//获取输入框
	auto textField_nameorID = dynamic_cast<TextField*>(_translateLayout->getChildByName("Image_id")->getChildByName("TextField_id"));
	textField_nameorID->setVisible(false);
	auto editBox_id = HNEditBox::createEditBox(textField_nameorID, this);

	auto textField_num = dynamic_cast<TextField*>(_translateLayout->getChildByName("Image_count")->getChildByName("TextField_count"));
	textField_num->setVisible(false);
	auto editBox_num = HNEditBox::createEditBox(textField_num, this);
	editBox_num->setInputMode(cocos2d::ui::EditBox::InputMode::PHONE_NUMBER);

	auto textField_psd = dynamic_cast<TextField*>(_translateLayout->getChildByName("Image_pass")->getChildByName("TextField_pass"));
	textField_psd->setVisible(false);
	auto editBox_psd = HNEditBox::createEditBox(textField_psd, this);
	editBox_psd->setPasswordEnabled(true);

	auto btn_sure = dynamic_cast<Button*>(_translateLayout->getChildByName("Button_sure"));
	btn_sure->addClickEventListener([=](Ref*){

		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
		std::string userText = editBox_id->getString();
		std::string forwardMoney = editBox_num->getString();
		std::string bankPassword = editBox_psd->getString();
		do
		{
			if (userText.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入对方ID。"));
				break;
			}

			if (forwardMoney.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("转账金币数为空。")); 
				break;
			}

			if (bankPassword.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入银行密码。")); 
				break;
			}

			MSG_GP_R_LogonResult& LogonResult = PlatformLogic()->loginResult;
			TMSG_GP_BankTransfer bankTransfer;

			// 只支持ID转账
			{
				bankTransfer.UserID = LogonResult.dwUserID;
				strcpy(bankTransfer.szNickName, LogonResult.nickName);

				bankTransfer.destUserID = atol(userText.c_str());
				bankTransfer.i64Money = atol(forwardMoney.c_str());
				bankTransfer.bUseDestID = true;
				strcpy(bankTransfer.szMD5Pass, MD5_CTX::MD5String(bankPassword).c_str());
			}

			if (bankTransfer.i64Money <= 0)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入合法金额。"));
				break;
			}

			PlatformLogic()->sendData(MDM_GP_BANK, ASS_GP_BANK_TRANSFER, &bankTransfer, sizeof(bankTransfer),
				HN_SOCKET_CALLBACK(GameBankLayer::giroEventSelector, this));

			editBox_id->setString("");
			editBox_num->setString("");
			editBox_psd->setString("");
		} while (0);
	});
}

bool GameBankLayer::giroEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(TMSG_GP_BankTransfer, socketMessage->objectSize, true, "TMSG_GP_BankTransfer size of error!");
	TMSG_GP_BankTransfer* pdate = (TMSG_GP_BankTransfer*)socketMessage->object;

	UINT bAssistantID = socketMessage->messageHead.bAssistantID;
	UINT bHandleCode = socketMessage->messageHead.bHandleCode;

	std::string errText("");
	if (ASS_GP_BANK_TRANSFER == bAssistantID)	// 转账
	{	
		if (HC_GP_BANK_TRANSFER_ERROR == bHandleCode)	//1		//转帐错误
		{
			errText = "转帐错误。";
		}
		else if (HC_GP_BANK_TRANSFER_NO_DEST == bHandleCode)	//3		//转帐目标不存在
		{
			errText = "转帐目标不存在。";
		}
		else if (HC_GP_BANK_TRANSFER_PASSWORD_ERROR == bHandleCode)	//4		//转帐密码错误
		{
			errText = "转帐密码错误。";
		}
		else if (HC_GP_BANK_TRANSFER_TOTAL_LESS == bHandleCode)	//5		//银行总额太小，不够资格
		{
			errText = "银行总额太小，不够资格。";
		}
		else if (HC_GP_BANK_TRANSFER_TOO_LESS == bHandleCode)//6		// 单笔转帐数目太少
		{
			errText = "单笔转帐数目太少。";
		}
		else if (HC_GP_BANK_TRANSFER_MULTIPLE == bHandleCode)//7		// 单笔转帐数目必须是某数的倍数
		{
			errText = "单笔转帐数目必须是某数的倍数。";
		}
		else if (HC_GP_BANK_TRANSFER_NOT_ENOUGH == bHandleCode)//8		// 银行金额不够本次转帐
		{
			errText = "银行金额不够本次转帐。";
		}
		else if (HC_GP_BANK_TRANSFER_TOO_MANY_TIME == bHandleCode)	//9	// 当天转账的次数太多了
		{
			errText = "当天转账的次数太多。";
		}
		else if (HC_GP_BANK_TRANSFER_SUC == bHandleCode)		//2		//转帐成功
		{
			errText = "转帐成功。";

			_bankMoney -= pdate->i64Money;
			PlatformLogic()->loginResult.i64Bank = _bankMoney;
			_delegate->bankChanged(_bankMoney);

			std::string str = StringUtils::format("%lld", _bankMoney);

			if (_depositMoney)
			{
				_depositMoney->setString(str);
			}
		}
		else
		{

		}
		GamePromptLayer::create()->showPrompt(GBKToUtf8(errText.c_str()));
	}
	else if (ASS_GP_BANK_TRANS_RECORD == bAssistantID)	// 转账查询
	{
		if (HC_GP_BANK_TRANS_RECORD_SUC == bHandleCode)
		{

		}
	}
	return true;
}

bool GameBankLayer::bankerEventSelector(HNSocketMessage* socketMessage)
{ 
	UINT bAssistantID = socketMessage->messageHead.bAssistantID;
	UINT bHandleCode = socketMessage->messageHead.bHandleCode;
	// 打开银行
	if (ASS_GP_BANK_OPEN == bAssistantID)
	{
		switch (bHandleCode)
		{
		case HC_GP_BANK_OPEN_SUC://					1		//打开钱柜成功
			{
				CHECK_SOCKET_DATA_RETURN(TMSG_GP_BankInfo, socketMessage->objectSize, true, "TMSG_GP_BankInfo size of error!");
				TMSG_GP_BankInfo * bankInfo = reinterpret_cast<TMSG_GP_BankInfo*>(socketMessage->object);

				_bankMoney = bankInfo->i64Bank;
				_walletMoney = bankInfo->i64Wallet;

				if (_curMoney) _curMoney->setString(StringUtils::format("%lld", _walletMoney));
				if (_depositMoney) _depositMoney->setString(StringUtils::format("%lld", _bankMoney));
				
				_bankOpen = true;

			} break;
		case HC_GP_BANK_OPEN_ERR://					2		//打开钱柜错误
			{
				_bankOpen = false;
			} break;
		case HC_GP_BANK_OPEN_ERR_PSW://				3		//打开钱框错误
			{
				_bankOpen = false;
			} break;
		default:
			break;
		}
	}
	// 修改密码
	else if (ASS_GP_BANK_CHPWD == bAssistantID)
	{
		switch (bHandleCode)
		{
		case HC_GP_BANK_CHPWD_SUC://					1			//修改成功
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("密码修改成功"));
			} break;
		case HC_GP_BANK_CHPWD_ERR_PWD://				2			//修改失败，密码错误
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("修改失败，密码错误。"));
			} break;
		default:
			break;
		}
	}
	// 存取钱
	else if (ASS_GP_BANK_CHECK == bAssistantID)
	{
		switch (bHandleCode)
		{
		case HC_GP_BANK_CHECK_SUC://					0			//操作成功
			{
				saveAndOpenBank(socketMessage->object, socketMessage->objectSize);
			} break;
		case HC_GP_BANK_CHECK_NO_USER://				1			//没有找到用户
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("金币不足无法存入。"));
			} break;
		case HC_GP_BANK_CHECK_ERR_PWD://				2			//错误密码
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("错误密码。"));
			} break;
		case HC_GP_BANK_CHECK_NOT_MONEY://				4			//余额不足
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("余额不足。"));
			} break;
		case HC_GP_BANK_CHECK_CAN_NOT_OPEN://			5			//房间中不能进行存取操作
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("房间中不能进行存取操作。"));
			} break;
		default:
			break;
		}
	}
	return true;
}

void GameBankLayer::saveAndOpenBank(void* pDate, int isize)
{
	CHECK_SOCKET_DATA(TMSG_GP_BankCheck, isize, "TMSG_GP_BankCheck size of error!");
	TMSG_GP_BankCheck *BankCheck = reinterpret_cast<TMSG_GP_BankCheck*>(pDate);

	std::string promptInfo;

	// 取钱
	if (1 == BankCheck->operate_type)
	{
		_bankMoney -= BankCheck->money;
		_walletMoney += BankCheck->money;
		promptInfo = GBKToUtf8("取款成功");
	}

	// 存钱 
	if (2 == BankCheck->operate_type)
	{
		_bankMoney += BankCheck->money;
		_walletMoney -= BankCheck->money;
		promptInfo = GBKToUtf8("存款成功");
	}

	PlatformLogic()->loginResult.i64Bank = _bankMoney;
	PlatformLogic()->loginResult.i64Money = _walletMoney;
	_delegate->walletChanged();
	_delegate->bankChanged(_bankMoney);

	if (_curMoney) _curMoney->setString(StringUtils::format("%lld", _walletMoney));
	if (_depositMoney) _depositMoney->setString(StringUtils::format("%lld", _bankMoney));

	GamePromptLayer::create()->showPrompt(promptInfo.c_str());
}
