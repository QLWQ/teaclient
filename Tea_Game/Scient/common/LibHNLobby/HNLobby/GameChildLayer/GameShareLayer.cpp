/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameShareLayer.h"
#include "HNOpenExport.h"
#include "../LibHNMarket/HNOperator.h"

static const char* SHARE_PATH = "platform/share/shareNode.csb";

static const int SHARE_LAYER_ZORDER  = 100000000 - 1;		// 子节点弹出框层级
static const int SHARE_LAYER_TAG     = 100000000 - 1;		// 子节点弹出框TAG

GameShareLayer::GameShareLayer()
{
	_isTouch = true;
	_text = "";
	_titile = "";
	_url = "";
	_imgName = "";
	_captureNode = nullptr;
	_callback = nullptr;
}

GameShareLayer::~GameShareLayer()
{

}

void GameShareLayer::closeFunc()
{

}

void GameShareLayer::show()
{
	Node* root = Director::getInstance()->getRunningScene();
	CCAssert(nullptr != root, "root is null");

	if (!getParent())
	{
		open(HNLayer::ACTION_TYPE_LAYER::SCALE, root, Vec2::ZERO, SHARE_LAYER_ZORDER, SHARE_LAYER_TAG);
	}
}

bool GameShareLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(SHARE_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node,101);

	auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}

	// 关闭
	auto btn_guanBi = dynamic_cast<Button*>(node->getChildByName("Button_close"));
	btn_guanBi->addClickEventListener([=](Ref*) {
		close();
	});
  
	// 微信分享
	auto btn_wechat = (Button*)img_bg->getChildByName("Button_wechat");
	btn_wechat->addClickEventListener(CC_CALLBACK_1(GameShareLayer::onShareClick, this));

	// 闲聊分享
	auto btn_xianliao = (Button*)img_bg->getChildByName("Button_xianliao");
	btn_xianliao->addClickEventListener(CC_CALLBACK_1(GameShareLayer::onShareClick, this));

	// 微信朋友圈分享
	auto btn_wechat_circle = (Button*)img_bg->getChildByName("Button_wechat_circle");
	btn_wechat_circle->addClickEventListener(CC_CALLBACK_1(GameShareLayer::onShareClick, this));

	return true;
}

// 点击分享
void GameShareLayer::onShareClick(Ref* pRef)
{
	if (!_isTouch) return;
	_isTouch = false;

	auto btn = (Button*)pRef;
	btn->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	//需要分享时候回调的时候
	if (onCloseShareCallBack != nullptr)
	{
		onCloseShareCallBack();
		onCloseShareCallBack = nullptr;
	}

#if (CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID && CC_TARGET_PLATFORM != CC_PLATFORM_IOS) 
	MessageBox("Only supports Android and IOS!", "error");
	return;
#endif

	std::string name(btn->getName());
	if (_callback)
		UMengSocial::getInstance()->onShareCallBack = _callback;

	if (0 == name.compare("Button_wechat"))
	{//微信
		if (Operator::requestChannel("sysmodule", "isAppInstalled", StringUtils::format("%d", WEIXIN)).compare("false") == 0)
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请先安装微信App!"));
		else
		{
			UMengSocial::getInstance()->setShareLayer(this);
			UMengSocial::getInstance()->doShare(_text, _titile, _url, _imgName, WEIXIN, _captureNode);
		}
	}
	else if (0 == name.compare("Button_xianliao"))
	{//闲聊
		if (CCUMSocialSDK::create()->isXLAppInstalled())
		{
			UMengSocial::getInstance()->setShareLayer(this);
			UMengSocial::getInstance()->doShare(_text, _titile, _url, _imgName, XIANLIAO, _captureNode);
		}
		else
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请先安装闲聊App!"));
	}
	else if (0 == name.compare("Button_wechat_circle"))
	{//微信朋友圈
		if (Operator::requestChannel("sysmodule", "isAppInstalled", StringUtils::format("%d", WEIXIN)).compare("false") == 0)
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请先安装微信App!"));
		else
		{
			UMengSocial::getInstance()->setShareLayer(this);
			UMengSocial::getInstance()->doShare(_text, _titile, _url, _imgName, WEIXIN_CIRCLE, _captureNode);
		}
	}
	else{
		log("share failed... function:onShareClick,node name=%s", name.c_str());
	}
}

void GameShareLayer::SetInviteInfo(const std::string& gameName, const std::string& deskPassword,
	bool bShow/* = false*/,  const std::string& text)
{
	//设置分享链接
	std::string shareUrl = PlatformConfig::getInstance()->getShareUrl();
	shareUrl.append("?GameID=0");
	shareUrl.append("&RoomID=");
	shareUrl.append(deskPassword);
	shareUrl.append("&scheme=redbirddzb");
	if (CC_PLATFORM_ANDROID == CC_TARGET_PLATFORM)
	{
		shareUrl.append("&open=openwith");
		shareUrl.append("&host=com.redbird.ChessRoom-Lite");
	}

	//设置分享标题	
	_titile = StringUtils::format(GBKToUtf8("【房号：%s】手游平台-%s"),
		deskPassword.c_str(), GBKToUtf8(gameName));

	//设置分享内容
	_text = StringUtils::format(GBKToUtf8("我在手游平台<%s>，%d局，%s快来加入！"),
		GBKToUtf8(gameName), HNPlatformConfig()->getAllCount(), text.c_str());

	_url = HNPlatformConfig()->getShareUrl();
	if (bShow)
	{
		//如果要分享单张图片，文字和链接需要为空
		_text = "";
		_url = "";
	}
}