#include "GameIntegralAct.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "HNPlatform/HNPlatformTaskList.h"
#include "../GameShop/GameStoreLayer.h"

static const char* GAME_INTEGRAL = "platform/IntegralAct/IntegralAct.csb";
static const char* GAME_INTEGRAL_ITEM = "platform/IntegralAct/IntegralItem.csb";
static const char* TASK_CONTENT = "platform/lobbytask/TaskDetailsNode.csb";

#define  BUTTON_SELECT_IMG		"platform/common/new/yxsm_an.png"    //选中按钮
#define  TEXT_FONTNAME			"platform/common/RTWSYueRoudGoG0v1-Regular.ttf"
#define  TEXT_COLOR				Color3B(191,74,5)
#define BTN_NORMAL_COLOR	ccc3(161,86,41)  
#define BTN_SELECT_COLOR	ccc3(242,232,210)

static const std::string TASK_BUTTON[] = {
	"platform/lobbytask/res/new/qgz_an.png",
	"platform/lobbytask/res/new/qfx_an.png",
	"platform/lobbytask/res/new/qwc_an.png",
	"platform/lobbytask/res/new/qcj_an.png",
	"platform/lobbytask/res/new/qwc_an.png",
	"platform/lobbytask/res/new/sjwd_an.png",
	"platform/lobbytask/res/new/qgm_an.png",
	"platform/lobbytask/res/new/ycg_an.png",
	"platform/lobbytask/res/new/sjwd_an.png",  //新手签到时间未到
	"platform/lobbytask/res/new/qwc_an.png",
};
static const std::string TASK_COMPLETE[] = { "platform/lobbytask/res/new/lq_an.png", "platform/lobbytask/res/new/dc_an.png" };


//活动列表
static const std::vector<std::string> _IntegralActList = 
{
	GBKToUtf8("推广"),
	GBKToUtf8("代理"),
	GBKToUtf8("单笔充值"),
	GBKToUtf8("累计充值"),
	
};
//隐藏活动列表
static const std::vector<std::string> _HideIntegralActList =
{
	GBKToUtf8("推广"),
	GBKToUtf8("代理"),
	GBKToUtf8("单笔充值"),
	GBKToUtf8("累计充值"),
};

GameIntegralAct::GameIntegralAct()
{
	_panel_map.clear();
}

GameIntegralAct::~GameIntegralAct()
{
	_panel_map.clear();
	HNHttpRequest::getInstance()->removeObserver(this);
}

bool GameIntegralAct::init()
{
	if (!HNLayer::init()) return false;
	auto node = CSLoader::createNode(GAME_INTEGRAL);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node,101);

	auto img_BG = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	_img_bg = img_BG;
	if (_winSize.width / _winSize.height > 1.78)
	{
		img_BG->setScale(0.9f);
	}
	auto btn_close = dynamic_cast<Button*>(node->getChildByName("Button_close"));
	btn_close->setLocalZOrder(2);
	if (btn_close) btn_close->addClickEventListener([=](Ref*){
		close();
	});

	//按钮列表
	_list_btn = dynamic_cast<ListView*>(node->getChildByName("ListView_btn"));

	//Layout列表
	for (auto i = 1; i <= _IntegralActList.size(); i++)
	{
		std::string szLayoutName = StringUtils::format("panel_list_%d", i);
		auto pLayout = dynamic_cast<Layout*>(img_BG->getChildByName(szLayoutName));
		if (pLayout)
		{
			std::string title = _IntegralActList[i - 1];
			pLayout->setVisible(false);
			_panel_map[title] = pLayout;
			if (1 == i || 2 == i)
			{//代理或者推广的按钮
				auto pBtn = dynamic_cast<Button*>(pLayout->getChildByName("Button_commit"));
				pBtn->setUserData((void*)title.c_str());
				if (pBtn)
				{
					pBtn->addClickEventListener([=](Ref* ref){
						auto btn = (Button*)ref;
						std::string name(btn->getName());
						std::string title = (char*)btn->getUserData();
						int type = (title == _IntegralActList[0]) ? 1 : 2;   //1推广，2代理
						auto pTextField = dynamic_cast<HNEditBox*>(btn->getParent()->getChildByName("TextField_HN"));
						std::string content = pTextField->getString();
						if (content == "")
						{
							GamePromptLayer::create()->showPrompt(GBKToUtf8("输入不能为空！"));
							return;
						}
						/*else if (!Tools::verifyNumber(content))
						{
							GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入手机号！"));
							return;
						}*/
						std::string requestData = "";
						requestData.append(StringUtils::format("UserID=%d", PlatformLogic()->loginResult.dwUserID));
						requestData.append(StringUtils::format("&ContactInfo=%s", content.c_str()));
						requestData.append(StringUtils::format("&Type=%d", type));
						HNHttpRequest::getInstance()->addObserver(this);
						//HNHttpRequest::getInstance()->request(StringUtils::format("AgencyApply_%d", type), network::HttpRequest::Type::POST, "http://192.168.0.33:8060/hyyl/AgencyApply.php", requestData);
						std::string url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/AgencyApply.php");
						HNHttpRequest::getInstance()->request(StringUtils::format("AgencyApply_%d", type), network::HttpRequest::Type::POST, url, requestData);
					});
				}

 				auto pTextField = dynamic_cast<TextField*>(pLayout->getChildByName("TextField"));
				if (pTextField)
				{
					pTextField->setVisible(false);
					auto pEditBoxCode = HNEditBox::createEditBox(pTextField, this);
					pEditBoxCode->setName("TextField_HN");
				}
			}
			else if ( 3 == i || 4 == i)
			{//单笔充值或者累计充值
				auto pList = dynamic_cast<ListView*>(pLayout->getChildByName("ListView"));
				if (pList)
					pList->setTag(i);
				//进行第一次刷新
				RefreshViewList(i);
			}
		}
	}

	//请求下活动信息
	HNHttpRequest::getInstance()->addObserver(this);
	std::string url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/ANotice.php");
	HNHttpRequest::getInstance()->request("IntegralActNotice", network::HttpRequest::Type::GET, url);

	HNPlatformTaskList::getInstance()->RequestSingleRechargeRecord();
	HNPlatformTaskList::getInstance()->RequestDoubleRechargeRecord();
	return true;
}

void GameIntegralAct::InitButton(const std::string& title,const std::string& content)
{
	auto pImg = ImageView::create("platform/common/new/yxsm_an.png");
	auto pBtn = Button::create();
	pBtn->loadTexturePressed("platform/common/new/yxsm_an.png");
	pBtn->loadTextureDisabled("platform/common/new/yxsm_an.png");
	pBtn->setTitleColor(BTN_NORMAL_COLOR);
	pBtn->setTitleFontName("platform/common/hkljhtjw8.ttc");
	pBtn->setTitleFontSize(44);
	pBtn->setSize(pImg->getSize());
	pBtn->setName(content);  
	pBtn->ignoreContentAdaptWithSize(false);
	pBtn->addClickEventListener(CC_CALLBACK_1(GameIntegralAct::LeftButtonCallBack, this));
	pBtn->setTitleText(title);
	_list_btn->pushBackCustomItem(pBtn);
}

void GameIntegralAct::LeftButtonCallBack(Ref* ref)
{
	auto btn = (Button*)ref;
	std::string name(btn->getName());

	for (auto it = _panel_map.begin(); it != _panel_map.end();it++)
	{
		auto pBtn = dynamic_cast<Button*>(_list_btn->getChildByName(it->first));
		if (it->first == name)
		{		
			pBtn->loadTextureNormal("platform/common/new/yxsm_an.png");
			pBtn->setTitleColor(BTN_SELECT_COLOR);
			if (it->second)
				it->second->setVisible(true);
		}
		else
		{
			pBtn->loadTextureNormal("");
			pBtn->setTitleColor(BTN_NORMAL_COLOR);
			if(it->second)
				it->second->setVisible(false);
		}
		if (_IntegralActList[2] == name)
		{
			RefreshViewList(3);
		}
		else if (_IntegralActList[3] == name)
		{
			RefreshViewList(4);
		}
	}
}

void GameIntegralAct::closeFunc()
{
	if (onCloseCallBack)
	{
		onCloseCallBack();
	}
}

void GameIntegralAct::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;

	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

	if (doc.HasParseError() || !doc.IsObject())
	{
		return;
	}

	if (requestName.compare("IntegralActNotice") == 0)
	{

		if (doc.HasMember("List") && doc["List"].IsArray() && doc["List"].Capacity() > 0)
		{
			for (int i = 0; i < doc["List"].Size(); i++)
			{
				rapidjson::Value& value = doc["List"][(rapidjson::SizeType)i];
				std::string szTitle = value["MTitle"].GetString();
				std::string szContent = value["MContent"].GetString();
				if (_panel_map.find(szContent) != _panel_map.end() && std::find(_HideIntegralActList.begin(),_HideIntegralActList.end(),szContent) == _HideIntegralActList.end())
				{
					InitButton(szTitle,szContent);
				}
			}
			if (_list_btn->getChildrenCount() > 0)
			{
				LeftButtonCallBack(_list_btn->getChildren().at(0));
			}
		}
	}
	//2代理，1推广
	else if (requestName.compare("AgencyApply_1") == 0 || requestName.compare("AgencyApply_2") == 0)
	{
		int code = doc["code"].GetInt();   //0成功,-1失败
		if(doc.HasMember("msg"))
		{
			GamePromptLayer::create()->showPrompt(doc["msg"].GetString());
		}
	}
}

void GameIntegralAct::editBoxReturn(ui::EditBox* editBox)
{
	
}

void GameIntegralAct::editBoxEditingDidEnd(ui::EditBox* editBox)
{
	
}

//刷新处理
void GameIntegralAct::refreshList(ListView* list, const vector<DATE_TASKLIST>& datas)
{
	if (list != nullptr)
	{
		list->removeAllItems();
	}

	//更新内容
	for (int i = 0; i < datas.size(); i++)
	{
		auto node = CSLoader::createNode(TASK_CONTENT);
		auto paneTask = dynamic_cast<Layout*>(node->getChildByName("panel_Task"));
		Text* text_Task = dynamic_cast<Text*>(paneTask->getChildByName("text_Task"));
		text_Task->setString(datas[i].TitleStr);

		//任务奖励
		string str = "";
		string str1 = "";
		string AwardNum = "";
		string AwardNum1 = "";
		if (datas[i].AwardNum == 1)
		{
			if (datas[i].Jewel != 0)
			{
				str = "platform/lobbytask/res/new/fk_tb.png";
				AwardNum = StringUtils::format("+%d", datas[i].Jewel);
			}
			else if (datas[i].WalletMoney != 0)
			{
				str = "platform/lobbytask/res/new/jb_tb.png";
				AwardNum = StringUtils::format("+%d", datas[i].WalletMoney);
			}
			else if (std::abs(datas[i].Money) > FLOAT_ZERO)
			{
				str = "platform/lobbytask/res/new/hb_tb.png";
				AwardNum = GBKToUtf8(StringUtils::format("+%.1f", datas[i].Money));
			}
			else if (datas[i].Lotteries != 0)
			{
				str = "platform/lobbytask/res/new/lq_tb.png";
				AwardNum = StringUtils::format("+%d", datas[i].Lotteries);
			}
			else if (datas[i].ShoppingTicket != 0)
			{
				str = "platform/lobbytask/res/new/gwq_tb.png";
				AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].ShoppingTicket));
			}
			else if (datas[i].PhoneBill != 0)
			{
				str = "platform/lobbytask/res/new/hf_tb.png";
				AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].PhoneBill));
			}
			else if (datas[i].Object != 0)  //手机或者电脑特殊处理
			{
				str = "platform/lobbytask/res/new/sjtb.png";
				AwardNum = "iPhone\nX 64G";
				str1 = "platform/lobbytask/res/new/dntb.png";
				AwardNum1 = "MacBook\n Air128G";
			}
		}
		else if (datas[i].AwardNum == 2)
		{
			const static int nMaxNum = 2;
			int nNum = 0;

			if (nNum < nMaxNum && datas[i].WalletMoney != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/jb_tb.png";
					AwardNum = StringUtils::format("+%d", datas[i].WalletMoney);
				}
				else
				{
					str1 = "platform/lobbytask/res/new/jb_tb.png";
					AwardNum1 = StringUtils::format("+%d", datas[i].WalletMoney);
				}
				nNum++;
			}
			if (datas[i].Jewel != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/fk_tb.png";
					AwardNum = StringUtils::format("+%d", datas[i].Jewel);
				}
				else
				{
					str1 = "platform/lobbytask/res/new/fk_tb.png";
					AwardNum1 = StringUtils::format("+%d", datas[i].Jewel);
				}
				nNum++;
			}
			if (nNum < nMaxNum && std::abs(datas[i].Money) > FLOAT_ZERO)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/hb_tb.png";
					AwardNum = GBKToUtf8(StringUtils::format("+%.1f", datas[i].Money));
				}
				else
				{
					str1 = "platform/lobbytask/res/new/hb_tb.png";
					AwardNum1 = GBKToUtf8(StringUtils::format("+%.1f", datas[i].Money));
				}
				nNum++;
			}
			if (nNum < nMaxNum && datas[i].Lotteries != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/lq_tb.png";
					AwardNum = StringUtils::format("+%d", datas[i].Lotteries);
				}
				else
				{
					str1 = "platform/lobbytask/res/new/lq_tb.png";
					AwardNum1 = StringUtils::format("+%d", datas[i].Lotteries);
				}
				nNum++;
			}
			if (nNum < nMaxNum && datas[i].ShoppingTicket != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/gwq_tb.png";
					AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].ShoppingTicket));
				}
				else
				{
					str1 = "platform/lobbytask/res/new/gwq_tb.png";
					AwardNum1 = GBKToUtf8(StringUtils::format("+%d", datas[i].ShoppingTicket));
				}
				nNum++;
			}
			if (nNum < nMaxNum && datas[i].PhoneBill != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/hf_tb.png";
					AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].PhoneBill));
				}
				else
				{
					str1 = "platform/lobbytask/res/new/hf_tb.png";
					AwardNum1 = GBKToUtf8(StringUtils::format("+%d", datas[i].PhoneBill));
				}
				nNum++;
			}
		}
		ImageView* image_reward = dynamic_cast<ImageView*>(paneTask->getChildByName("image_reward"));
		image_reward->loadTexture(str);

		//奖励表现
		if (datas[i].AwardNum == 1)
		{
			if (datas[i].Object > 0)
			{//奖励mac或者iphone
				//创建第二个奖励
				if (datas[i].ShoppingTicket == 0)
				{
					image_reward->setScale(0.7);
					image_reward->setPositionX(375.5);
				}
				ImageView* imag_rewardother = ImageView::create(str1);
				imag_rewardother->setPosition(Vec2(540.5, image_reward->getPositionY()));
				imag_rewardother->setScale(0.7);
				paneTask->addChild(imag_rewardother);

				Text* text = Text::create(AwardNum, TEXT_FONTNAME, 22);
				text->setAnchorPoint(Vec2(0, 0.5));
				text->setPosition(Vec2(image_reward->getContentSize().width / 2 + image_reward->getPositionX() - 30, 55.5));
				text->setColor(TEXT_COLOR);
				paneTask->addChild(text);

				Text* text_or = Text::create(GBKToUtf8("或"), TEXT_FONTNAME, 22);
				text_or->setAnchorPoint(Vec2(0, 0.5));
				text_or->setPosition(Vec2(image_reward->getContentSize().width / 2 + image_reward->getPositionX() + 40, 55.5));
				text_or->setColor(TEXT_COLOR);
				paneTask->addChild(text_or);

				Text* text1 = Text::create(AwardNum1, TEXT_FONTNAME, 22);
				text1->setAnchorPoint(Vec2(0, 0.5));
				text1->setPosition(Vec2(imag_rewardother->getContentSize().width / 2 + imag_rewardother->getPositionX() - 20, 55.5));
				text1->setColor(TEXT_COLOR);
				paneTask->addChild(text1);
			}
			else
			{
				Text* text = Text::create(AwardNum, TEXT_FONTNAME, 32);
				text->setAnchorPoint(Vec2(0, 0.5));
				text->setPosition(Vec2(image_reward->getContentSize().width / 2 + image_reward->getPositionX(), 55.5f));
				text->setColor(TEXT_COLOR);
				paneTask->addChild(text);
			}
		}
		else if (datas[i].AwardNum == 2)
		{
			//创建第二个奖励
			if (datas[i].ShoppingTicket == 0)
			{
				image_reward->setPositionX(375.5);
			}
			ImageView* imag_rewardother = ImageView::create(str1);
			imag_rewardother->setPosition(Vec2(550.5, image_reward->getPositionY()));
			paneTask->addChild(imag_rewardother);

			Text* text = Text::create(AwardNum, TEXT_FONTNAME, 32);
			text->setAnchorPoint(Vec2(0, 0.5));
			text->setPosition(Vec2(image_reward->getContentSize().width / 2 + image_reward->getPositionX(), 55.5));
			text->setColor(TEXT_COLOR);
			paneTask->addChild(text);

			Text* text1 = Text::create(AwardNum1, TEXT_FONTNAME, 32);
			text1->setAnchorPoint(Vec2(0, 0.5));
			text1->setPosition(Vec2(imag_rewardother->getContentSize().width / 2 + imag_rewardother->getPositionX(), 55.5));
			text1->setColor(TEXT_COLOR);
			paneTask->addChild(text1);
		}

		Text* text_Task_progress = dynamic_cast<Text*>(paneTask->getChildByName("text_Task_progress"));
		text_Task_progress->setString(StringUtils::format("%d / %d", datas[i].CurrentNum, datas[i].Total));
		auto pLoadingBar = dynamic_cast<LoadingBar*>(paneTask->getChildByName("LoadingBar"));
		pLoadingBar->setPercent(datas[i].Total == 0 ? 0 : ((float)datas[i].CurrentNum / (float)datas[i].Total) * 100);
		if (datas[i].Total == 1 || datas[i].Total == 0)
		{
			text_Task->setFontSize(28);
			text_Task->setPositionY(55.5);
			text_Task_progress->setVisible(false);
			dynamic_cast<ImageView*>(paneTask->getChildByName("image_loadbar"))->setVisible(false);
			pLoadingBar->setVisible(false);
		}

		Button* btn_TaskState = dynamic_cast<Button*>(paneTask->getChildByName("btn_TaskState"));
		//任务可领取
		if (datas[i].Status == 1)
		{
			btn_TaskState->setEnabled(true);
			btn_TaskState->loadTextures(TASK_COMPLETE[0], TASK_COMPLETE[0], TASK_COMPLETE[1]);
		}
		else if (datas[i].Status == 2)
		{
			btn_TaskState->loadTextures("", "", TASK_COMPLETE[1]);
			btn_TaskState->setEnabled(false);
		}
		//任务未完成的状态显示（去完成，去购买。。。）
		else
		{
			btn_TaskState->setEnabled(true);
			btn_TaskState->loadTextures(TASK_BUTTON[datas[i].TypeID - 1], TASK_BUTTON[datas[i].TypeID - 1], TASK_COMPLETE[1]);
		}
		btn_TaskState->setTag(list->getTag());					//添加任务标识（每日任务）
		btn_TaskState->setUserData((void*)&(datas[i]));	//记录任务信息
		btn_TaskState->addClickEventListener(CC_CALLBACK_1(GameIntegralAct::ClickReceiveBtn, this));

		paneTask->removeFromParentAndCleanup(true);
		list->pushBackCustomItem(paneTask);
	}
}


void GameIntegralAct::ClickReceiveBtn(Ref* pSender)
{
	Button* btn_Click = (Button*)pSender;
	int Index = btn_Click->getTag();
	DATE_TASKLIST* Data = (DATE_TASKLIST*)btn_Click->getUserData();
	if (Data->Status == 1)
	{
		//设置领取回调
		HNPlatformTaskList::getInstance()->SendReceiveCallBack = [=](){
			RefreshViewList(Index);
		};

		//领取奖励
		switch (Index)
		{
			case (3) :
			{
				//单笔充值
				HNPlatformTaskList::getInstance()->SendReceiveSingleRechargeRequest(Data->TaskID);
				break;
			}
			case (4) :
			{
				//累计充值
				HNPlatformTaskList::getInstance()->SendReceiveDoubleRechargeRequest(Data->TaskID);
				break;
			}
		}
	}
	//如果任务还没有完成的状态下（根据任务类型执行相对应的跳转）
	else
	{
		switch (Data->TypeID)
		{
			case 7:
			{	//充值,跳转商城
				if (onOpenStoreCallBack != nullptr)
				{
					onOpenStoreCallBack();
					close();
				}
			}
			break;
			default:
				break;
		}
	}
}

void GameIntegralAct::RefreshViewList(int Index)
{
	if (_img_bg == nullptr) return;
	std::string szLayoutName = StringUtils::format("panel_list_%d", Index);
	auto pLayout = dynamic_cast<Layout*>(_img_bg->getChildByName(szLayoutName));
	if (pLayout == nullptr) return;
	auto pList = dynamic_cast<ListView*>(pLayout->getChildByName("ListView"));
	if (pList == nullptr) return;

	switch (Index)
	{
	case (3) :
	{//单笔充值		
		m_datas = HNPlatformTaskList::getInstance()->getSingleRechargeList();
		break;
	}
	case(4) :
	{//累计充值
		m_datas = HNPlatformTaskList::getInstance()->getDoubleRechargeList();
		break;
	}
	}
	refreshList(pList, m_datas);
}