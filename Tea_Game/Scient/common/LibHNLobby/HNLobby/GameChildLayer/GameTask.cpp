/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameTask.h"
#include <sstream>
#include "HNPlatform/HNPlatformTaskList.h"
#include "HNOpenExport.h"
#include "../GameClub/GameClub.h"
#include "../GameChildLayer/ServiceLayer.h"

USING_NS_UM_SOCIAL;

static const char* TASK_PATH = "platform/lobbytask/TaskLayer.csb";
static const char* TASK_CONTENT = "platform/lobbytask/TaskDetailsNode.csb";
static const std::string TASK_BUTTON[] = { 
	"platform/lobbytask/res/new/qgz_an.png", 
	"platform/lobbytask/res/new/qfx_an.png",
	"platform/lobbytask/res/new/qwc_an.png", 
	"platform/lobbytask/res/new/qcj_an.png",
	"platform/lobbytask/res/new/qwc_an.png", 
	"platform/lobbytask/res/new/sjwd_an.png",
	"platform/lobbytask/res/new/qgm_an.png", 
	"platform/lobbytask/res/new/ycg_an.png",
	"platform/lobbytask/res/new/sjwd_an.png",  //新手签到时间未到
	"platform/lobbytask/res/new/qwc_an.png",	
};
static const std::string TASK_COMPLETE[] = {"platform/lobbytask/res/new/lq_an.png","platform/lobbytask/res/new/dc_an.png"};

static const std::string TASK_NAME[] = { "每日任务", "新春任务", "每周任务", "新手任务", "新春活动", "" };

#define  BUTTON_SELECT_IMG		"platform/common/new/yxsm_an.png"    //选中按钮
#define  TEXT_FONTNAME			"platform/common/RTWSYueRoudGoG0v1-Regular.ttf"
#define  TEXT_COLOR				Color3B(191,74,5)
#define  BTN_NORMAL_COLOR		Color3B(161,86,41)   
#define  BTN_SELECT_COLOR		Color3B(242,232,210) 

GameTaskLayer::GameTaskLayer()
{
}

GameTaskLayer::~GameTaskLayer()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

bool GameTaskLayer::init()
{
	if (!HNLayer::init()) return false;

	float scalex = 1280 / _winSize.width;
	float scaley = 720 / _winSize.height;

	auto node = CSLoader::createNode(TASK_PATH);
	//node->setPosition(_winSize / 2);
	addChild(node);

	//触摸屏蔽层
	Layout* panel_make = (Layout*)node->getChildByName("panel_make");

	ImageView* img_bg = (ImageView*)node->getChildByName("image_bg");

	//关闭按钮
	Button* btn_close = (Button*)img_bg->getChildByName("btn_close");
	btn_close->addClickEventListener([=](Ref*){
		if (onExitCallBack != nullptr)
		{
			onExitCallBack();
		}
		close();
	});

	//切换任务列表
	m_list_btn = (ListView*)img_bg->getChildByName("list_btn");
	m_list_btn->setScrollBarEnabled(false);

	//各类任务列表获取
	m_ViewList_bg = img_bg;

	//新手任务切换出任务系统
	for (int i = 0; i < 3; i++)
	{
		string btn_name = StringUtils::format("btn_%d", i);
		Button* btn = (Button*)m_list_btn->getChildByName(btn_name);
		btn->addClickEventListener(CC_CALLBACK_1(GameTaskLayer::ListBottomCallBack, this));
		btn->setTag(i);
		btn->setTitleColor(i == 0 ? BTN_SELECT_COLOR : BTN_NORMAL_COLOR);
		btn->loadTextureNormal(i == 0 ? BUTTON_SELECT_IMG : "");
		//string a = ;
		btn->setTitleText(HNPlatformTaskList::getInstance()->getTaskNameByIndex(i));//GBKToUtf8(TASK_NAME[i]));
		auto sp = (Sprite*)btn->getChildByName("sp_red");
		CCActionInterval *forwardIn = CCFadeIn::create(1);
		CCActionInterval *backIn = CCFadeOut::create(2);
		CCAction *actionIn = CCRepeatForever::create(dynamic_cast<CCActionInterval *>(CCSequence::create(forwardIn, DelayTime::create(1.5f), backIn, NULL)));
		sp->runAction(actionIn);
		sp->setVisible(false);
	}

	m_list_Newbie = (ListView*)img_bg->getChildByName("list_btn_newbie");
	m_list_Newbie->setScrollBarEnabled(false);

	//新手任务以及新手红包
	for (int i = 0; i < 3; i++)
	{
		string btn_name = StringUtils::format("btn_%d", i);
		Button* btn = (Button*)m_list_Newbie->getChildByName(btn_name);
		btn->addClickEventListener(CC_CALLBACK_1(GameTaskLayer::ListBtnNewbieCallBack, this));
		btn->setTag(i + 3);
		btn->setTitleColor(i == 0 ? BTN_SELECT_COLOR : BTN_NORMAL_COLOR);
		btn->loadTextureNormal(i == 0 ? BUTTON_SELECT_IMG : "");
		//string a = ;
		btn->setTitleText(HNPlatformTaskList::getInstance()->getTaskNameByIndex(i + 3));
		if (i == 1 || i == 0)
		{
			if ( (i == 1 && !HNPlatformTaskList::getInstance()->getShowNewbieR() )
				|| (i == 2 && !HNPlatformTaskList::getInstance()->getShowSignInR()))
			{//没有该任务则直接移除
				btn->removeFromParent();
			}
			auto sp = (Sprite*)btn->getChildByName("sp_red");
			CCActionInterval *forwardIn = CCFadeIn::create(1);
			CCActionInterval *backIn = CCFadeOut::create(2);
			CCAction *actionIn = CCRepeatForever::create(dynamic_cast<CCActionInterval *>(CCSequence::create(forwardIn, DelayTime::create(1.5f), backIn, NULL)));
			sp->runAction(actionIn);
			sp->setVisible(false);
		}
		else if (i == 2)
		{
			btn->setVisible(false);
		}
	}

	string str = HNPlatformTaskList::getInstance()->getTaskNote();
	Text_Bottom = Text::create(str, "platform/common/RTWSYueRoudGoG0v1-Regular.ttf", 24);
	Text_Bottom->setSize(Size(760, 80));
	Text_Bottom->ignoreContentAdaptWithSize(false);
	Text_Bottom->setPosition(Vec2(745,80));
	Text_Bottom->setColor(Color3B(132, 81, 44));
	Text_Bottom->setVisible(false);
	img_bg->addChild(Text_Bottom);

	Image_bottom = (ImageView*)img_bg->getChildByName("Image_bottom");
	Image_bottom->setPositionX(744.38);
	Image_bottom->setVisible(false);
	Image_bottom->loadTexture("platform/lobbytask/res/new/wz.png");
	Image_bottom->setContentSize(Size(727, 53));

	//默认初始化任务列表
	for (int i = 0; i < 6; i++)
	{//0-2每日、定时、周任务，3-4新手以及新手红包
		string list_name = StringUtils::format("list_Task_%d", i);
		ListView* list = (ListView*)m_ViewList_bg->getChildByName(list_name);
		list->setTag(i);
	}

	//请求任务状态
	NewbieTask = HNPlatformTaskList::getInstance()->getNewbieTaskList();
	WeeklyTask = HNPlatformTaskList::getInstance()->getWeeklyTaskList();
	TimingTask = HNPlatformTaskList::getInstance()->getTimingTaskList();
	DailyTask = HNPlatformTaskList::getInstance()->getDailyTaskList();

	//请求未领取任务状态(更新任务状态改变红点)

	//请求新手红包状态
	SubsidiesTask = HNPlatformTaskList::getInstance()->getSubsidiesTaskList();
	SignInTask = HNPlatformTaskList::getInstance()->getSignInTaskList();

	//初始化任务说明规则
	auto btn_help = dynamic_cast<Button*>(img_bg->getChildByName("btn_help"));
	btn_help->addClickEventListener(CC_CALLBACK_1(GameTaskLayer::TaskHelpBottomCallBack, this));

	m_panel_Help = dynamic_cast<Layout*>(node->getChildByName("panel_help"));
	if (m_panel_Help)
	{
		initTaskExplainView();
	}

	return true;
}

void GameTaskLayer::initTaskExplainView()
{
	auto m_pScrollView = dynamic_cast<ui::ScrollView*>(m_panel_Help->getChildByName("ScrollView_Task"));
	auto pText = dynamic_cast<Text*>(m_pScrollView->getChildByName("Text"));
	pText->setVisible(false);
	auto m_pLabel = Label::create();
	TTFConfig config = m_pLabel->getTTFConfig();
	config.fontFilePath = pText->getFontName();
	config.fontSize = pText->getFontSize();
	m_pLabel->setTTFConfig(config);
	m_pLabel->setTextColor(pText->getTextColor());
	m_pLabel->setAnchorPoint(Vec2(0, 1));
	//m_pLabel->setPositionX(pText->getParent()->getContentSize().width / 2);
	m_pLabel->setDimensions(pText->getParent()->getContentSize().width - 5, 0);
	m_pLabel->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
	pText->getParent()->addChild(m_pLabel);
	m_pLabel->setString(HNPlatformTaskList::getInstance() ->getTaskTitleStr());
	auto s0 = m_pLabel->getContentSize();
	Size s1 = Size(m_pScrollView->getContentSize().width, s0.height + 30);
	m_pScrollView->setInnerContainerSize(s1);
	if (s0.height < m_pScrollView->getContentSize().height)
	{
		m_pLabel->setPosition(Vec2(10, m_pScrollView->getContentSize().height - 10));
	}
	else
	{
		m_pLabel->setPosition(Vec2(10, s0.height + 10));
	}

	//设置关闭按钮
	auto btn_close = (Button*)m_panel_Help->getChildByName("btn_close");
	btn_close->addClickEventListener([=](Ref*){
		m_panel_Help->setVisible(false);
	});
}

void GameTaskLayer::ListBottomCallBack(Ref* pSender)
{
	Button* ClickBtn = (Button*)pSender;
	int Index = ClickBtn->getTag();
	for (int i = 0; i < 3; i++)
	{
		string btn_name = StringUtils::format("btn_%d", i);
		Button* Btn = (Button*)m_list_btn->getChildByName(btn_name);
		if (Btn == nullptr) continue;
		Btn->setEnabled(Btn->getTag() != Index);
		Btn->setTitleColor(Btn->getTag() == Index ? BTN_SELECT_COLOR : BTN_NORMAL_COLOR);
		Btn->loadTextureNormal(Btn->getTag() == Index ? BUTTON_SELECT_IMG : "");
	}
	ListBtnSwitchViewList(Index);
}

void GameTaskLayer::ListBtnNewbieCallBack(Ref* pSender)
{
	Button* ClickBtn = (Button*)pSender;
	int Index = ClickBtn->getTag();
	for (int i = 0; i < 3; i++)
	{
		string btn_name = StringUtils::format("btn_%d", i);
		Button* Btn = (Button*)m_list_Newbie->getChildByName(btn_name);
		if (Btn == nullptr) continue;
		Btn->setEnabled(Btn->getTag() != Index);
		Btn->setTitleColor(Btn->getTag() == Index ? BTN_SELECT_COLOR : BTN_NORMAL_COLOR);
		Btn->loadTextureNormal(Btn->getTag() == Index ? BUTTON_SELECT_IMG : "");
	}
	ListBtnSwitchNewbie(Index);
}

void GameTaskLayer::TaskHelpBottomCallBack(Ref* pSender)
{
	if (m_panel_Help)
	{
		m_panel_Help->setVisible(true);
	}
}

void GameTaskLayer::ButtonTiXianCallBack(Ref* pSender)
{
	ListView* list = (ListView*)m_ViewList_bg->getChildByName("list_Task_4");
	list->setVisible(true);
	//刷新数据
	HNPlatformTaskList::getInstance()->RequestLooseChange();
	HNPlatformTaskList::getInstance()->onRequestCallBack = [=](){
		_LooseChange = HNPlatformTaskList::getInstance()->getLooseChange();
		Text_Money->setString(StringUtils::format("%.2f", _LooseChange));
	};
}

void GameTaskLayer::ClickTiXianBtn(Ref* pSender)
{
	if (editBox->getString().empty())
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("输入框不能为空！"));
		return;
	}
	std::string Num = editBox->getString();
	if (!Tools::verifyNumber(Num))
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为整数！"));
		return;
	}
	if (stoi(Num) < _MinStandard)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8(StringUtils::format("输入不能小于%.1f", _MinStandard)));
		return;
	}
	if (stoi(Num) > _MaxStandard)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8(StringUtils::format("输入不能大于%.1f", _MaxStandard)));
		return;
	}
	if (_LooseChange < stoi(Num))
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("零钱不足，无法提现！"));
		return;
	}
	//发起提现
	HNPlatformTaskList::getInstance()->SendWithdrawalRequest(stoi(Num));
	//设置更新
	HNPlatformTaskList::getInstance()->onRequestCallBack = [=](){
		_LooseChange = HNPlatformTaskList::getInstance()->getLooseChange();
		Text_Money->setString(StringUtils::format("%.2f", _LooseChange));
	};
}

void GameTaskLayer::ListBtnSwitchViewList(int Index)
{
	//有效区域判断
	if (Index<0 || Index>2)
	{
		return;
	}
	for (int i = 0; i < 3; i++)
	{
		string list_name = StringUtils::format("list_Task_%d", i);
		ListView* list = (ListView*)m_ViewList_bg->getChildByName(list_name);
		int tag = list->getTag();
		list->setVisible(tag == Index);
	}
}

void GameTaskLayer::ListBtnSwitchNewbie(int Index)
{
	//有效区域判断
	if (Index < 3 || Index > 5)
	{
		return;
	}
	for (int i = 3; i < 6; i ++)
	{
		string list_name = StringUtils::format("list_Task_%d", i);
		ListView* list = (ListView*)m_ViewList_bg->getChildByName(list_name);
		int tag = list->getTag();
		list->setVisible(tag == Index);
		if (i == 4 && tag == Index)
		{
			//Image_bottom->setVisible(false);
			Text_Bottom->setVisible(true);
		}
		else if (i == 4)
		{
			//Image_bottom->setVisible(false);
			Text_Bottom->setVisible(false);
		}
	}
}

void GameTaskLayer::AllViewListInit()
{
	m_list_btn->setVisible(true);				//任务列表按钮显示
	m_list_Newbie->setVisible(false);
	for (int i = 0; i < 3;i++)
	{
		ListView* list = (ListView*)m_ViewList_bg->getChildByName(StringUtils::format("list_Task_%d", i));
		list->setVisible(i == 0 ? true : false);
		RefreshViewList(i);
	}
}

void GameTaskLayer::AllNewbieListInit()
{
	m_list_Newbie->setVisible(true);			//新手任务按钮显示
	m_list_btn->setVisible(false);
	for (int i = 3; i < 6; i++)
	{
		ListView* list = (ListView*)m_ViewList_bg->getChildByName(StringUtils::format("list_Task_%d", i));
		list->setVisible(3 == i ? true : false);
		RefreshViewList(i);
	}
}

void GameTaskLayer::RefreshViewList(int Index)
{
	switch (Index)
	{
	case (3) :
	{
		RefreshNewbieTask();
		break;
	}
	case(0) :
	{
		RefreshDailyTask();
		break;
	}
	case(1) :
	{
		RefreshTimingTask();
		break;
	}
	case(2) :
	{
		RefreshWeeklyTask();
		break;
	}
	case(4):
	{
		RefreshSubsidiesTask();
		break;
	}
	case(5) :  //新手签到
	{
		RefreshSignInTask();
		break;
	}
	}
}

//刷新处理
void GameTaskLayer::refreshTask(const int& type, const vector<DATE_TASKLIST>& datas)
{
	ListView* list = (ListView*)m_ViewList_bg->getChildByName(StringUtils::format("list_Task_%d",type));
	if (list!= nullptr && list->getCurSelectedIndex() != -1)
	{
		list->removeAllItems();
	}

	bool bShowRed = false;
	//更新每日任务内容
	for (int i = 0; i < datas.size(); i++)
	{
		auto node = CSLoader::createNode(TASK_CONTENT);
		auto paneTask = dynamic_cast<Layout*>(node->getChildByName("panel_Task"));
		Text* text_Task = dynamic_cast<Text*>(paneTask->getChildByName("text_Task"));
		text_Task->setString(datas[i].TitleStr);

		//任务奖励
		string str = "";
		string str1 = "";
		string AwardNum = "";
		string AwardNum1 = "";
		if (datas[i].AwardNum == 1)
		{
			if (datas[i].Jewel != 0)
			{
				str = "platform/lobbytask/res/new/fk_tb.png";
				AwardNum = StringUtils::format("+%d", datas[i].Jewel);
			}
			else if (datas[i].WalletMoney != 0)
			{
				str = "platform/lobbytask/res/new/jb_tb.png";
				AwardNum = StringUtils::format("+%d", datas[i].WalletMoney);
			}
			else if (std::abs(datas[i].Money) > FLOAT_ZERO)
			{
				str = "platform/lobbytask/res/new/hb_tb.png";
				AwardNum = GBKToUtf8(StringUtils::format("+%.1f", datas[i].Money));
			}
			else if (datas[i].Lotteries != 0)
			{
				str = "platform/lobbytask/res/new/lq_tb.png";
				AwardNum = StringUtils::format("+%d", datas[i].Lotteries);
			}
			else if (datas[i].ShoppingTicket != 0)
			{
				str = "platform/lobbytask/res/new/gwq_tb.png";
				AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].ShoppingTicket));
			}
			else if (datas[i].PhoneBill != 0)
			{
				str = "platform/lobbytask/res/new/hf_tb.png";
				AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].PhoneBill));
			}
		}
		else if (datas[i].AwardNum == 2)
		{
			const static int nMaxNum = 2;
			int nNum = 0;
			
			if (nNum < nMaxNum && datas[i].WalletMoney != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/jb_tb.png";
					AwardNum = StringUtils::format("+%d", datas[i].WalletMoney);
				}
				else
				{
					str1 = "platform/lobbytask/res/new/jb_tb.png";
					AwardNum1= StringUtils::format("+%d", datas[i].WalletMoney);
				}
				nNum++;
			}
			if (datas[i].Jewel != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/fk_tb.png";
					AwardNum = StringUtils::format("+%d", datas[i].Jewel);
				}
				else
				{
					str1 = "platform/lobbytask/res/new/fk_tb.png";
					AwardNum1 = StringUtils::format("+%d", datas[i].Jewel);
				}
				nNum++;
			}
			if (nNum < nMaxNum && std::abs(datas[i].Money) > FLOAT_ZERO)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/hb_tb.png";
					AwardNum = GBKToUtf8(StringUtils::format("+%.1f", datas[i].Money));
				}
				else
				{
					str1 = "platform/lobbytask/res/new/hb_tb.png";
					AwardNum1 = GBKToUtf8(StringUtils::format("+%.1f", datas[i].Money));
				}
				nNum++;
			}
			if (nNum < nMaxNum && datas[i].Lotteries != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/lq_tb.png";
					AwardNum = StringUtils::format("+%d", datas[i].Lotteries);
				}
				else
				{
					str1 = "platform/lobbytask/res/new/lq_tb.png";
					AwardNum1 = StringUtils::format("+%d", datas[i].Lotteries);
				}
				nNum++;
			}
			if (nNum < nMaxNum && datas[i].ShoppingTicket != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/gwq_tb.png";
					AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].ShoppingTicket));
				}
				else
				{
					str1 = "platform/lobbytask/res/new/gwq_tb.png";
					AwardNum1 = GBKToUtf8(StringUtils::format("+%d", datas[i].ShoppingTicket));
				}
				nNum++;
			}
			if (nNum < nMaxNum && datas[i].PhoneBill != 0)
			{
				if (nNum == 0)
				{
					str = "platform/lobbytask/res/new/hf_tb.png";
					AwardNum = GBKToUtf8(StringUtils::format("+%d", datas[i].PhoneBill));
				}
				else
				{
					str1 = "platform/lobbytask/res/new/hf_tb.png";
					AwardNum1 = GBKToUtf8(StringUtils::format("+%d", datas[i].PhoneBill));
				}
				nNum++;
			}
		}
		ImageView* image_reward = dynamic_cast<ImageView*>(paneTask->getChildByName("image_reward"));
		image_reward->loadTexture(str);

		//奖励表现
		if (datas[i].AwardNum == 1)
		{
			/*if (datas[i].ShoppingTicket == 0)
			{
			image_reward->setPositionX(59);
			}*/
			Text* text = Text::create(AwardNum, TEXT_FONTNAME, 32);
			text->setAnchorPoint(Vec2(0, 0.5));
			text->setPosition(Vec2(image_reward->getContentSize().width / 2 + image_reward->getPositionX(), 55.5f));
			text->setColor(TEXT_COLOR);
			paneTask->addChild(text);
		}
		else if (datas[i].AwardNum == 2)
		{
			//创建第二个奖励
			if (datas[i].ShoppingTicket == 0)
			{
				image_reward->setPositionX(375.5);
			}
			ImageView* imag_rewardother = ImageView::create(str1);
			imag_rewardother->setPosition(Vec2(550.5, image_reward->getPositionY()));
			paneTask->addChild(imag_rewardother);

			Text* text = Text::create(AwardNum, TEXT_FONTNAME, 32);
			text->setAnchorPoint(Vec2(0, 0.5));
			text->setPosition(Vec2(image_reward->getContentSize().width / 2 + image_reward->getPositionX(), 55.5));
			text->setColor(TEXT_COLOR);
			paneTask->addChild(text);

			Text* text1 = Text::create(AwardNum1, TEXT_FONTNAME, 32);
			text1->setAnchorPoint(Vec2(0, 0.5));
			text1->setPosition(Vec2(imag_rewardother->getContentSize().width / 2 + imag_rewardother->getPositionX(), 55.5));
			text1->setColor(TEXT_COLOR);
			paneTask->addChild(text1);
		}

		Text* text_Task_progress = dynamic_cast<Text*>(paneTask->getChildByName("text_Task_progress"));
		text_Task_progress->setString(StringUtils::format("%d / %d", datas[i].CurrentNum, datas[i].Total));
		auto pLoadingBar = dynamic_cast<LoadingBar*>(paneTask->getChildByName("LoadingBar"));
		pLoadingBar->setPercent(datas[i].Total == 0 ? 0 : ((float)datas[i].CurrentNum / (float)datas[i].Total) * 100);
		if (datas[i].Total == 1 || datas[i].Total == 0)
		{
			text_Task->setFontSize(28);
			text_Task->setPositionY(55.5);
			text_Task_progress->setVisible(false);
			dynamic_cast<ImageView*>(paneTask->getChildByName("image_loadbar"))->setVisible(false);
			pLoadingBar->setVisible(false);
		}

		Button* btn_TaskState = dynamic_cast<Button*>(paneTask->getChildByName("btn_TaskState"));
		//btn_TaskState->loadTextures("","","");
		//任务可领取
		if (datas[i].Status == 1)
		{
			bShowRed = true;
			btn_TaskState->loadTextures(TASK_COMPLETE[0], TASK_COMPLETE[0], TASK_COMPLETE[1]);
		}
		else if (datas[i].Status == 2)
		{
			btn_TaskState->loadTextures("", "", TASK_COMPLETE[1]);
			btn_TaskState->setEnabled(false);
		}
		//任务未完成的状态显示（去完成，去购买。。。）
		else
		{
			btn_TaskState->loadTextures(TASK_BUTTON[datas[i].TypeID - 1], TASK_BUTTON[datas[i].TypeID - 1], TASK_COMPLETE[1]);
		}
		btn_TaskState->setTag(type);					//添加任务标识（每日任务）
		btn_TaskState->setUserData((void*)&(datas[i]));	//记录任务信息
		btn_TaskState->addClickEventListener(CC_CALLBACK_1(GameTaskLayer::ClickReceiveBtn, this));

		paneTask->removeFromParentAndCleanup(true);
		list->pushBackCustomItem(paneTask);
	}

	if (type >= 3 && type <= 5)
	{//新手引导
		string btn_name = StringUtils::format("btn_%d", type - 3);
		Button* btn = (Button*)m_list_Newbie->getChildByName(btn_name);
		if (btn)
		{
			auto sp = (Sprite*)btn->getChildByName("sp_red");
			sp->setVisible(bShowRed);
		}

	}
	else if (type >=0 && type <= 2)
	{
		string btn_name = StringUtils::format("btn_%d", type);
		Button* btn = (Button*)m_list_btn->getChildByName(btn_name);
		if (btn)
		{
			auto sp = (Sprite*)btn->getChildByName("sp_red");
			sp->setVisible(bShowRed);
		}
	}
}

//新手任务
void GameTaskLayer::RefreshNewbieTask()
{
	if (NewbieTask.size() <= 0)
	{
		NewbieTask = HNPlatformTaskList::getInstance()->getNewbieTaskList();
	}
	refreshTask(3, NewbieTask);
}

//日常任务
void GameTaskLayer::RefreshDailyTask()
{
	if (DailyTask.size() <= 0)
	{
		DailyTask = HNPlatformTaskList::getInstance()->getDailyTaskList();
	}
	refreshTask(0, DailyTask);
}

//限时任务
void GameTaskLayer::RefreshTimingTask()
{
	if (TimingTask.size() <= 0)
	{
		TimingTask = HNPlatformTaskList::getInstance()->getTimingTaskList();
	}
	refreshTask(1, TimingTask);
}

//每周任务
void GameTaskLayer::RefreshWeeklyTask()
{
	if (WeeklyTask.size() <= 0)
	{
		WeeklyTask = HNPlatformTaskList::getInstance()->getWeeklyTaskList();
	}
	refreshTask(2, WeeklyTask);
}

//对局红包
void GameTaskLayer::RefreshSubsidiesTask()
{
	if (SubsidiesTask.size() <= 0)
	{
		SubsidiesTask = HNPlatformTaskList::getInstance()->getSubsidiesTaskList();
	}
	refreshTask(4, SubsidiesTask);
}

//新手签到
void GameTaskLayer::RefreshSignInTask()
{
	if (SignInTask.size() <= 0)
	{
		SignInTask = HNPlatformTaskList::getInstance()->getSignInTaskList();
	}
	refreshTask(5, SignInTask);
}

void GameTaskLayer::ClickReceiveBtn(Ref* pSender)
{
	Button* btn_Click = (Button*)pSender;
	int Index = btn_Click->getTag();
	DATE_TASKLIST* Data = (DATE_TASKLIST*)btn_Click->getUserData();
	if (Data->Status == 1)
	{
		//领取奖励
		switch (Index)
		{
			case (3) :
			{
				//领取新手奖励
				HNPlatformTaskList::getInstance()->SendReceiveNReward(Data->TaskID);
				break;
			}
			case (0) :
			{
				//领取每日任务奖励
				HNPlatformTaskList::getInstance()->SendReceiveDReward(Data->TaskID);
				break;
			}
			case (1) :
			{
				//领取定时任务奖励
				HNPlatformTaskList::getInstance()->SendReceiveTReward(Data->TaskID);
				break;
			}
			case (2) :
			{
				//领取每周任务奖励
				HNPlatformTaskList::getInstance()->SendReceiveWReward(Data->TaskID);
				break;
			}
			case (4) :
			{
				//领取新手红包任务奖励
				HNPlatformTaskList::getInstance()->SendReceiveSReward(Data->TaskID);
				break;
			}
			case (5) :
			{
				//领取新手签到任务奖励
				HNPlatformTaskList::getInstance()->SendReceiveSIReward(Data->TaskID);
				break;
			}
		}
		//设置领取回调
		HNPlatformTaskList::getInstance()->SendReceiveCallBack = [=](){
			if (Index == 3)
			{
				NewbieTask.clear();
			}
			else if (Index == 0)
			{
				DailyTask.clear();
			}
			else if (Index == 1)
			{
				TimingTask.clear();
			}
			else if (Index == 2)
			{
				WeeklyTask.clear();
			}
			else if (Index == 4)
			{
				SubsidiesTask.clear();
			}
			else if (Index == 5)
			{
				SignInTask.clear();
			}
			RefreshViewList(Index);
		};
	}
	//如果任务还没有完成的状态下（根据任务类型执行相对应的跳转）
	else
	{
		bool isClose = false;
		switch (Data->TypeID)
		{
			case (1) :
			{
				//关注公众号
				Application::getInstance()->copyToClipboard(GBKToUtf8("欢赢互娱"));
				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("复制成功，正在跳转微信..."), 36);
				this->scheduleOnce(schedule_selector(GameTaskLayer::JumpWechat), 1.0f);
				break;
			}
			case (2) :
			{
				std::string shareStr = HNPlatformLogic::getInstance()->getShareStr().shareStr;
				//UMengSocial::getInstance()->doShare(HNPlatformConfig()->getShareContent(), GBKToUtf8(shareStr), HNPlatformConfig()->getShareUrl());
				auto shareLayer = GameShareLayer::create();
				shareLayer->setName("shareLayer");
				shareLayer->SetShareInfo(HNPlatformConfig()->getShareContent(), GBKToUtf8(shareStr), HNPlatformConfig()->getShareUrl());
				shareLayer->SetShareCallBack([=](bool success, int platform, const std::string& errorMsg) {
					std::string str;
					if (success)
					{
						std::string path = StringUtils::format("/hyyl/TestShare.php?UserID=%d",PlatformLogic()->loginResult.dwUserID);
						std::string url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
						HNHttpRequest::getInstance()->addObserver(this);
						HNHttpRequest::getInstance()->request("requestShareCallBack", cocos2d::network::HttpRequest::Type::GET, url);
					}
					else
					{
						std::string str;
						if (str.empty()) {
							str = GBKToUtf8("分享失败");
						}
						else {
							str = errorMsg;
						}
						runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=]() {
							GamePromptLayer::create()->showPrompt(str);
						}), nullptr));
					}
				});
				shareLayer->show();
				break;
			}
			case (3) :
			{
				//俱乐部
				close();
				Club::createClubScene();
				break;
			}
			case (4) :
			{
				//比赛场
				GamePromptLayer::create()->showPrompt(GBKToUtf8("产品更新升级中。"));
				break;
			}
			case (5) :
			{
				//游戏任务
				if (onEnterGameCallBack != nullptr)
				{
					onEnterGameCallBack();
				}
				isClose = true;
				break;
			}
			case (6) :
			{
				//定时任务
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("产品更新升级中。"));
				break;
			}
			case (7) :
			{
				//商场购买房卡
				if (onOpenStoreCallBack != nullptr)
				{
					onOpenStoreCallBack();
				}
				isClose = true;
				break;
			}
			case (8) :
			{
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("任务已经错过了。"));
				break;
			}
			case (9) :
			{
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("22222任务已经错过了。"));
				break;
			}
					 
			case (10) :
			{
				auto serviceLayer = ServiceLayer::create();
				serviceLayer->setName("serviceLayer");
				serviceLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, this->getZOrder() + 1, this->getTag() + 1);
				break;
			}
		}
		if (isClose)
		{
			close();
		}
	}
}

void GameTaskLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;
	if (requestName.compare("requestShareCallBack") == 0)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}
		int code = doc["code"].GetInt();
		if (code == 0)
		{
			//任务进度更新
			HNPlatformTaskList::getInstance()->RequestDailyTaskList();

			//设置更新回调
			HNPlatformTaskList::getInstance()->onRequestTaskCallBack = [=](){
				DailyTask.clear();
				RefreshDailyTask();
			};
			//updateUserInfo();
			//close();
			if (doc.HasMember("msg"))
			{
				std::string rs = doc["msg"].GetString();
				GamePromptLayer::create()->showPrompt(rs);
			}
			//任务进度更新
			//HNPlatformTaskList::getInstance()->RequestAllTaskList();
		}
	}
}

void GameTaskLayer::JumpWechat(float dt)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	Application::getInstance()->openURL("weixin://");
	close();
}