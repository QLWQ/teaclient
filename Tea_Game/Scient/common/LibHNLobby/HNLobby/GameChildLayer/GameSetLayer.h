/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMESET_LAYER_H__
#define __GAMESET_LAYER_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class GameSetLayer : public HNLayer, public HNHttpDelegate
{
public:
	GameSetLayer();
	virtual ~GameSetLayer();

	typedef std::function<void()> ExitCallBack;
	ExitCallBack		onExitCallBack = nullptr;

	typedef std::function<void()> DisCallBack;
	DisCallBack			onDisCallBack = nullptr;

	typedef std::function<void(bool)> ShowMJCheatCallBack;
	ShowMJCheatCallBack onShowMJCheatCallBack = nullptr;
public:
    virtual bool init() override;
	//设置游戏状态按钮（离开，解散）
	void setGameStateView();

	//设置金币场模式
	void setGameGlodExit();

	void updateCheatStatus(int status);
private:
	void closeFunc() override;

	// 拖动条回调函数
	void sliderCallback(Ref* pSender, Slider::EventType type);

	// 离开游戏
	void onLiKaiClick(Ref* pRef);

	// 切换账号
	void onQieHuanClick(Ref* pRef);

	//音乐回调
	void yinyueCallBack(Ref* pSender, CheckBox::EventType type);
	//音效回调
	void yinxiaoCallBack(Ref* pSender, CheckBox::EventType type);
	//静音回调
	void jingyinCallBack(Ref* pSender, CheckBox::EventType type);
	//头像框回调
	//void touxiangCallBack(Ref* pSender, CheckBox::EventType type);

	/*** 作弊部分相关代码  ***/
	//HTTP请求结果
	void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData);

	void createCheatSwitch();  //创建作弊开关按钮

	void CheckBoxCheatEvent(Ref* pSender, CheckBox::EventType type);
private:
	Slider* _effectSlider = nullptr;
	Slider* _musicSlider = nullptr;
	Node* _CSBNode = nullptr;
	CheckBox* _effect_CheckBox = nullptr;
	CheckBox* _music_CheckBox = nullptr;
	CheckBox* _allvolume_CheckBox = nullptr;
	UINT    _uGameID;  //游戏id
	ImageView* _pImgBg = nullptr;
	Sprite* _spSetMusic = nullptr;
	Sprite* _spSetEffect = nullptr;
public:
    CREATE_FUNC(GameSetLayer);
};

#endif // __GAMESET_LAYER_H__
