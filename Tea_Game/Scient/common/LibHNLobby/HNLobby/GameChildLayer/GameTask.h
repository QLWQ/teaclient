/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMETASK_LAYER_H__
#define __GAMETASK_LAYER_H__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include <vector>
#include "cocostudio/CocoStudio.h"
//#include "HNNetProtocol/HNProtocolExport.h"
#include "ui/CocosGUI.h"
#include "HNPlatform/HNPlatformTaskList.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class GameTaskLayer : public HNLayer, public ui::EditBoxDelegate, public HNHttpDelegate
{
public:
	GameTaskLayer();
	~GameTaskLayer();

	virtual bool init() override;

	virtual void editBoxReturn(ui::EditBox* editBox) {};

	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	CREATE_FUNC(GameTaskLayer);

	//退出回调更新用户数据
	typedef std::function<void()> ExitCallBack;
	ExitCallBack		onExitCallBack = nullptr;

	//调用进入游戏
	typedef std::function<void()> EnterGameCallBack;
	EnterGameCallBack	onEnterGameCallBack = nullptr;

	//调用商场接口
	typedef std::function<void()> OpenStoreCallBack;
	OpenStoreCallBack	onOpenStoreCallBack = nullptr;

	void RefreshViewList(int Index);

	//默认初始化任务列表
	void AllViewListInit();

	//默认新手任务列表
	void AllNewbieListInit();
private:
	//初始化任务说明
	void initTaskExplainView();

	//按钮点击切换处理
	void ListBottomCallBack(Ref* pSender);

	//新手任务以及新手红包按钮处理
	void ListBtnNewbieCallBack(Ref* pSender);

	//点击提现按钮
	void ButtonTiXianCallBack(Ref* pSender);

	//确认提现按钮
	void ClickTiXianBtn(Ref* pSender);

	//点击任务按钮
	void ClickReceiveBtn(Ref* pSender);

	//点击切换任务列表
	void ListBtnSwitchViewList(int Index);

	//切换新手任务新手红包
	void ListBtnSwitchNewbie(int Index);
	
	//刷新新手任务
	void RefreshNewbieTask();

	//刷新每周任务
	void RefreshWeeklyTask();

	//刷新定时任务
	void RefreshTimingTask();

	//刷新每日任务
	void RefreshDailyTask();

	//刷新新手红包
	void RefreshSubsidiesTask();

	//刷新新手签到
	void RefreshSignInTask();

	//跳转微信
	void JumpWechat(float dt);

	//打开关于任务说明
	void TaskHelpBottomCallBack(Ref* pSender);
	
	//刷新任务
	void refreshTask(const int& type, const vector<DATE_TASKLIST>& datas);
private:
	ListView* m_list_btn = nullptr;
	ListView* m_list_Newbie = nullptr;
	//Layout* m_panel_Clone = nullptr;
	ImageView* m_ViewList_bg = nullptr;
	Layout*  m_panel_Help = nullptr;

	vector<DATE_TASKLIST> NewbieTask;
	vector<DATE_TASKLIST> WeeklyTask;
	vector<DATE_TASKLIST> TimingTask;
	vector<DATE_TASKLIST> DailyTask;
	vector<DATE_TASKLIST> SubsidiesTask;
	vector<DATE_TASKLIST> SignInTask;  //新手签到数据
	//是否新手任务未领取
	/*bool		_bIsNewbie = false;
	bool		_bIsWeekly = false;
	bool		_bIsTiming = false;
	bool		_bIsDaily = false;
	bool		_bIsSubsidies = false;*/

	float		_LooseChange;
	float		_MinStandard;
	float		_MaxStandard;

	TextField*		TextField_content;
	HNEditBox*		editBox;
	Text*			Text_Money;
	Layout*		Temp_panle_close;
	ImageView*	Image_bottom;				//新手对局提示
	Text*			Text_Bottom;
};

#endif