/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "ServiceLayer.h"
#include "HNMarketExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

static const char* SERVICE_UI_JSON = "platform/serviceUi/ServiceNode.csb";

ServiceLayer::ServiceLayer()
{

}

ServiceLayer::~ServiceLayer()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

void ServiceLayer::requestServiceInfo()
{
	std::string param = StringUtils::format("action=GetServiceInfo&key=");
	HNHttpRequest::getInstance()->request("service", network::HttpRequest::Type::POST, PlatformConfig::getInstance()->getEditUrl(), param);
	HNHttpRequest::getInstance()->addObserver(this);
}

void ServiceLayer::closeFunc()
{
	
}

bool ServiceLayer::init()
{
	if (!HNLayer::init()) return false;

	_csNode = CSLoader::createNode(SERVICE_UI_JSON);
	_csNode->setPosition(_winSize / 2);
	addChild(_csNode, 2, 3);

	// 关闭
	auto btnClose = (Button*)_csNode->getChildByName("Button_close");
	if (nullptr != btnClose)
	{
		btnClose->addClickEventListener([=](Ref*) {
			close();
		});
	}

	// 电话
	_buttonPhone = (Button*)_csNode->getChildByName("Button_phone");
	_buttonPhone->addClickEventListener(CC_CALLBACK_1(ServiceLayer::onServiceClick, this));
	_buttonPhone->setEnabled(false);
	_buttonPhone->setVisible(false);

	auto Image_phone = (ImageView*)_csNode->getChildByName("Image_phone");
	Image_phone->setVisible(false);

	// 信息
	_buttonQQ = (Button*)_csNode->getChildByName("Button_qq");
	_buttonQQ->addClickEventListener(CC_CALLBACK_1(ServiceLayer::onServiceClick, this));
	_buttonQQ->setEnabled(false);

	_buttonWechat = (Button*)_csNode->getChildByName("Button_wechat");
	_buttonWechat->addClickEventListener(CC_CALLBACK_1(ServiceLayer::onServiceClick, this));
	_buttonWechat->setEnabled(false);

	_text_phone = (Text*)_csNode->getChildByName("Text_phone");
	_text_phone->setString("");
	_text_phone->setVisible(false);

	_text_QQ = (Text*)_csNode->getChildByName("Text_qq");
	_text_QQ->setString("");

	_text_WeChat = (Text*)_csNode->getChildByName("Text_weixin");
	_text_WeChat->setString("");

	auto imageview_bg = (ImageView*)_csNode->getChildByName("Image_bg");
	auto bgRect = imageview_bg->getBoundingBox();

	// 创建一个点击事件
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = [&](Touch* touch, Event* event)
	{
		// 获取的当前触摸的目标
		auto target = dynamic_cast<ServiceLayer*>(event->getCurrentTarget());
		Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		Size size = target->getContentSize();
		Rect rect = Rect(0, 0, size.width, size.height);
		if (rect.containsPoint(locationInNode))
		{
			if (bgRect.containsPoint(locationInNode)) return true;
			close();
			return true;
		}
		else
		{
			return false;
		}
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	auto image_cpoy_wechat = dynamic_cast<ImageView*>(_text_WeChat->getChildByName("Image_copy"));
	if (image_cpoy_wechat)
	{
		image_cpoy_wechat->setPosition(Vec2(_text_WeChat->getContentSize().width + 10, image_cpoy_wechat->getPositionY()));
	}

	auto image_cpoy_qq = dynamic_cast<ImageView*>(_text_QQ->getChildByName("Image_copy"));
	if (image_cpoy_qq)
	{
		image_cpoy_qq->setPosition(Vec2(_text_QQ->getContentSize().width + 10, image_cpoy_qq->getPositionY()));
	}

	auto image_cpoy_phone = dynamic_cast<ImageView*>(_text_phone->getChildByName("Image_copy"));
	if (image_cpoy_phone)
	{
		image_cpoy_phone->setPosition(Vec2(_text_phone->getContentSize().width + 10, image_cpoy_phone->getPositionY()));
	}

	requestServiceInfo();
	return true;
}

void ServiceLayer::setBGPositon(Vec2 vec)
{
	_csNode->setPosition(Vec2(vec.x, vec.y - 100));
}

void ServiceLayer::onServiceClick(Ref* pRef)
{
	Node* pNdoe = (Node*)pRef;
	std::string name = pNdoe->getName();

	// 电话
	if (name.compare("Button_phone") == 0)
	{
		std::string param = StringUtils::format("{\"number\":\"%s\"}", _phone.c_str());
		Operator::requestChannel("sysmodule", "dealPhone", param);
	}

	// QQ
	else if (name.compare("Button_qq") == 0)
	{
		std::string param;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		param = StringUtils::format("mqqwpa://im/chat?chat_type=wpa&uin=%s", _qq.c_str());
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		param = StringUtils::format("mqq://im/chat?chat_type=wpa&uin=%s&version=1&src_type=web", _qq.c_str());
#endif
		Application::getInstance()->openURL(param);
	}
	else if (name.compare("Button_wechat")==0)
	{
		std::string  str = _text_WeChat->getString();
		Application::getInstance()->copyToClipboard(str);
		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("复制成功，正在跳转微信..."), 36);
		this->scheduleOnce(schedule_selector(ServiceLayer::JumpWechat), 1.0f);
	}
	else
	{

	}
}

void ServiceLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (requestName.compare("service") == 0)
	{
		if (!isSucceed)	{
			return;
		}

		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());
		if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember("content") || !doc["content"].IsArray()) {
			return;
		}

		for (auto i = 0; i < doc["content"].Size(); i++)
		{
			if (doc["content"][i].IsObject() && doc["content"][i].HasMember("ParaName") && doc["content"][i].HasMember("ParaValue"))
			{
				std::string name  = doc["content"][i]["ParaName"].GetString();
				std::string value = doc["content"][i]["ParaValue"].GetString();

				if (value.empty()) continue;
				ImageView* pImage = nullptr;
				Size text_size = Size::ZERO;
				if (name.compare("CustomTel") == 0)
				{
					_phone = value;
					_buttonPhone->setEnabled(true);
					_text_phone->setString(value);
					pImage = dynamic_cast<ImageView*>(_text_phone->getChildByName("Image_copy"));
					text_size = _text_phone->getContentSize();
				}
				else if (name.compare("CustomQq") == 0)
				{
					_qq = value;
					_buttonQQ->setEnabled(true);
					_text_QQ->setString(_qq);
					pImage = dynamic_cast<ImageView*>(_text_QQ->getChildByName("Image_copy"));
					text_size = _text_QQ->getContentSize();
				}
				else if (name.compare("CustomWeiXin") == 0)
				{
					_text_WeChat->setString(value);
					_buttonWechat->setEnabled(true);
					pImage = dynamic_cast<ImageView*>(_text_WeChat->getChildByName("Image_copy"));
					text_size = _text_WeChat->getContentSize();
				}
				else
				{

				}
				if (pImage)
				{
					pImage->setPosition(Vec2(text_size.width + 10, pImage->getPositionY()));
				}
			}
		}
	}
}

void ServiceLayer::JumpWechat(float dt)
{
	Application::getInstance()->openURL("weixin://");
	CCLOG("123");
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
}
