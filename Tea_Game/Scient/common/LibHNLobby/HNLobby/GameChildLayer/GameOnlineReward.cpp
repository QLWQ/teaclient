/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameOnlineReward.h"


static const char* ONLINEUI_PATH = "platform/onlineReward/Node_onlineReward.csb";

GameOnlineReward::GameOnlineReward(void)
	: onUpdataUserMoney (nullptr)
	, onCloseCallBack (nullptr)
{

}

GameOnlineReward::~GameOnlineReward(void)
{
	
}

void GameOnlineReward::close()
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_CLOSE);
	_OnlineRewardUi.ImageView_OnlineBG->runAction(Sequence::create(ScaleTo::create(0.1f, 0.3f), CCCallFunc::create([&]()
	{
		PlatformLogic()->removeEventSelector(MDM_GP_ONLINE_AWARD, ASS_GP_ONLINE_AWARD_CHECK);
		PlatformLogic()->removeEventSelector(MDM_GP_ONLINE_AWARD, ASS_GP_ONLINE_AWARD_DO);

		if (onCloseCallBack) onCloseCallBack();
		this->removeFromParent();
	}), nullptr));
}

bool GameOnlineReward::init()
{
	if (!HNLayer::init()) return false;
	Size winSize = Director::getInstance()->getWinSize();

	auto node = CSLoader::createNode(ONLINEUI_PATH);
	node->setPosition(winSize / 2);
	addChild(node);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_online"));
	layout->setScale(winSize.width / 1280, winSize.height / 720);
	layout->addClickEventListener([=](Ref*){

		close();
	});

	_OnlineRewardUi.ImageView_OnlineBG = (ImageView*)node->getChildByName("Image_RewardBG");

	_OnlineRewardUi.Layout_Wait = (Layout*)_OnlineRewardUi.ImageView_OnlineBG->getChildByName("Panel_Wait");
	_OnlineRewardUi.Layout_Get = (Layout*)_OnlineRewardUi.ImageView_OnlineBG->getChildByName("Panel_Get");

	_OnlineRewardUi.ImageView_Light = (ImageView*)_OnlineRewardUi.Layout_Get->getChildByName("Image_Light");
	_OnlineRewardUi.ImageView_Light->runAction(RepeatForever::create(RotateBy::create(2.5f, 90.0f)));

	_OnlineRewardUi.Text_Time = (Text*)_OnlineRewardUi.Layout_Wait->getChildByName("Text_Time");
	_OnlineRewardUi.Text_Money = (Text*)_OnlineRewardUi.Layout_Wait->getChildByName("Text_Money");

	_OnlineRewardUi.Button_Get = (Button*)_OnlineRewardUi.ImageView_OnlineBG->getChildByName("Button_Get");
	_OnlineRewardUi.Button_Get->addClickEventListener([=](Ref*){
		PlatformLogic()->sendData(MDM_GP_ONLINE_AWARD, ASS_GP_ONLINE_AWARD_DO, 0, 0,
			HN_SOCKET_CALLBACK(GameOnlineReward::getOnlineRewardEventSelector, this));
	});

	_OnlineRewardUi.Button_Get->setEnabled(false);
	_OnlineRewardUi.Button_Get->setBright(false);

	checkRewardData();

	return true;
}

//查询在线奖励信息
void GameOnlineReward::checkRewardData()
{
	PlatformLogic()->sendData(MDM_GP_ONLINE_AWARD, ASS_GP_ONLINE_AWARD_CHECK, 0, 0, 
		HN_SOCKET_CALLBACK(GameOnlineReward::checkRewardDataEventSelector, this));
}

//申请查询在线奖励信息回调
bool GameOnlineReward::checkRewardDataEventSelector(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_S_ONLINE_AWARD_CHECK_RESULT) == socketMessage->objectSize, "MSG_GP_S_ONLINE_AWARD_CHECK_RESULT is error.");
	MSG_GP_S_ONLINE_AWARD_CHECK_RESULT* iCheck = (MSG_GP_S_ONLINE_AWARD_CHECK_RESULT*)socketMessage->object;

	if (1 == socketMessage->messageHead.bHandleCode)
	{
		_Time = iCheck->iLeftTime;

		if (_Time > 0)
		{
			char str[12];
			sprintf(str, "%u", iCheck->iOnLineTimeMoney);
			_OnlineRewardUi.Text_Money->setString(str);
			_OnlineRewardUi.Text_Money->setVisible(true);
			_OnlineRewardUi.Text_Time->setVisible(true);
			_OnlineRewardUi.Button_Get->setEnabled(false);
			_OnlineRewardUi.Button_Get->setBright(false);
			
			updataGetRewardTimeMessage();
		}
		else if (0 == _Time && iCheck->iOnLineTimeMoney > 0)
		{
			_OnlineRewardUi.Layout_Wait->setVisible(false);
			_OnlineRewardUi.Layout_Get->setVisible(true);
			_OnlineRewardUi.Button_Get->setEnabled(true);
			_OnlineRewardUi.Button_Get->setBright(true);
		}		
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("查询失败!"));
	}
	
	return true;
}

//申请领取在线奖励回调
bool GameOnlineReward::getOnlineRewardEventSelector(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_S_ONLINE_AWARD_DO_RESULT) == socketMessage->objectSize, "MSG_GP_S_ONLINE_AWARD_DO_RESULT is error.");
	MSG_GP_S_ONLINE_AWARD_DO_RESULT* iGet = (MSG_GP_S_ONLINE_AWARD_DO_RESULT*)socketMessage->object;

	char buff[64];
	std::string str;
	if (1 == socketMessage->messageHead.bHandleCode)
	{
		_Time = iGet->iNextTime;
		sprintf(buff, "%u", iGet->iNextMoney);
		_OnlineRewardUi.Text_Money->setString(buff);
		_OnlineRewardUi.Button_Get->setEnabled(false);
		_OnlineRewardUi.Button_Get->setBright(false);
		_OnlineRewardUi.Layout_Wait->setVisible(true);
		_OnlineRewardUi.Layout_Get->setVisible(false);

		updataGetRewardTimeMessage();

		PlatformLogic()->loginResult.i64Money += iGet->iCurrentGetMoney;
		str.append("恭喜你领取成功，获得");
		str.append(StringUtils::toString(UINT(iGet->iCurrentGetMoney)));
		str.append("金币奖励！");

		if (nullptr != onUpdataUserMoney)
		{
			onUpdataUserMoney(PlatformLogic()->loginResult.i64Money);
		}

		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8(str.c_str()));

		auto meteor = ParticleSystemQuad::create("platform/particle/bigwin_blowout_2.plist");
		meteor->setPosition(this->getPosition());
		meteor->setAutoRemoveOnFinish(true);
		Director::getInstance()->getRunningScene()->addChild(meteor, 100000001);
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("领取失败!"));
	}

	return true;
}

//领奖倒计时
void GameOnlineReward::updataGetRewardTimeMessage()
{
	char str[32];
	INT h = _Time / 3600;
	INT m = (_Time % 3600) / 60;
	INT s = (_Time % 3600) % 60;
	sprintf(str, "%02d:%02d:%02d", h, m, s);

	_OnlineRewardUi.Text_Time->setString(str);

	_Time--;
	if (_Time < 0)
	{
		_OnlineRewardUi.Layout_Wait->setVisible(false);
		_OnlineRewardUi.Layout_Get->setVisible(true);
		_OnlineRewardUi.Button_Get->setEnabled(true);
		_OnlineRewardUi.Button_Get->setBright(true);

		return;
	}

	this->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=](){
		updataGetRewardTimeMessage();
	}), nullptr));
}
