/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef TurnLayer_h__
#define TurnLayer_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "network/HttpClient.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;
using namespace cocos2d::network;
static const int GIFT_NUM = 10;

class TurnLayer : public HNLayer
{
public:
	TurnLayer();

	virtual ~TurnLayer();

	virtual bool init() override; 

	CREATE_FUNC(TurnLayer);

	std::function<void()> onUpdataUserMoney = nullptr;

	std::function<void()> onSetUserMoney = nullptr;
	//请求转盘物品
	void requestWithParams();
	//请求转盘物品的回调
	void onHttpResponse(HttpClient *sender, HttpResponse *response);
	//设置转盘背景
	void setTurnBg();
protected:
	void btnCallBack(Ref* pSender);

	void closeLayer();

	//设置消耗的金币数
	void setCostMoney(int money);

	//设置免费次数
	void setAllTimes(INT times);
	
	//设置刷新时间
	void setRefreshTime(int time);

	//剩余次数
	void setRestTimes(int restTimes, int allTimes);

	//请求奖品信息
	void requestGift();

	//请求奖品信息回复
	bool requestGiftEventSelector(HNSocketMessage* socketMessage);

	//抽奖请求
	void sendStartLuckDraw();

	//抽奖回复
	bool LuckDrawEventSelector(HNSocketMessage* socketMessage);

	//指针转动
	void playTurn(int areaIndex);

	//请求抽奖记录回调
	void sendGetChoujiangjilu();
	//抽奖记录回复
	bool getJiluDataSelector(HNSocketMessage* socketMessage);
	//创建增加抽奖记录item
	Layout* addNewJiluItem(std::string time,std::string name,int value);
private:
	Layout* _layer = nullptr;
	//Sprite* _SpGift[GIFT_NUM];
	Text* _TextGift[GIFT_NUM];
	ImageView* _TurnPointer = nullptr;
	Button* _startBtn = nullptr;
	Button* _closeBtn = nullptr;
	int _lastIndex = 0;  //上一次转动的下标
	int _lastRandNum = 0;  //上一次获取的随机值
	Sprite* m_get_sprite = nullptr;//转盘背景图
	MSG_GP_LuckDraw_Config_Result _configData;
	bool	m_isChoujiang = true;
};
#endif