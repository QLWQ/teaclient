/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_GameBankerLayer_h__
#define HN_GameBankerLayer_h__

#include "HNLobby/GameShop/ShopManager.h"
#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;

class GameBankLayer : public HNLayer, public ui::EditBoxDelegate
{
private:
	Layout* _storeLayout		= nullptr;
	Layout* _infoLayout		= nullptr;
	Layout* _translateLayout		= nullptr;

	//顶部3按钮
	Button* _btnStore = nullptr;
	Button* _btnInfo = nullptr;
	Button* _btnTranslate = nullptr;

	TextAtlas* _curMoney			= nullptr;//身上金币
	TextAtlas* _depositMoney		= nullptr;//银行存款

	Node* _csNode = nullptr;

private:
	bool					_bankOpen = false;

	// 钱柜
	LLONG					_bankMoney = 0;

	// 钱包的钱
	LLONG					_walletMoney = 0;

	MoneyChangeNotify*		_delegate = nullptr;
public:
	static GameBankLayer* createGameBank();

public:
	GameBankLayer();
	virtual ~GameBankLayer();

	virtual bool init() override;

	void setChangeDelegate(MoneyChangeNotify* delegate)
	{
		_delegate = delegate;
	}

private:
	void closeFunc() override;

	// 加载操作界面
	void loadOperatorUI();

	// 加载修改密码界面
	void loadModifyUI();

	// 加载转账界面
	void loadGiroUI();

	// 界面消息回调函数
private:
	void topButtonClickCallBack(Ref* pSender);
	
	// 切换按钮
	enum SelectOption
	{
		eNone,      // 未知
		eStore,     // 存储
		eInfo,      // 资料
		eTranslate, // 转账
	};
	void changeSelect(SelectOption option);
private:
	// 银行操作处理函数
	bool bankerEventSelector(HNSocketMessage* socketMessage);

	// 银行转账处理函数
	bool giroEventSelector(HNSocketMessage* socketMessage);

	void saveAndOpenBank(void* pDate, int isize);

	virtual void editBoxReturn(ui::EditBox* editBox) {};
};


#endif // HN_GameBankerLayer_h__
