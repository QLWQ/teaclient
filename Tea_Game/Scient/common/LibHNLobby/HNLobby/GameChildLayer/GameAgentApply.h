#ifndef __GAMEAGENTAPPLY_H__
#define __GAMEAGENTAPPLY_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"
#include "ui/UICheckBox.h"
#include "network/HttpClient.h"
USING_NS_CC;
using namespace ui;
using namespace cocostudio;
using namespace HN;

class GameAgentApply : public HNLayer, public ui::EditBoxDelegate, public HNHttpDelegate
{
public:
	GameAgentApply();
	virtual ~GameAgentApply();
	// ��ʼ��
	virtual bool init() override;
	CREATE_FUNC(GameAgentApply);
private:
	
	virtual void editBoxReturn(ui::EditBox* editBox) {};
	virtual void submitClickCallback(Ref* pSender);
	void submitSucceedCallBack();
	bool getTruePhoneNo(std::string str);
private:
	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;
public:
	HNEditBox*					_editBoxID		= nullptr;
	HNEditBox*					_editBoxPhone	= nullptr;
	Button*						_btn_submit		= nullptr;
};


#endif