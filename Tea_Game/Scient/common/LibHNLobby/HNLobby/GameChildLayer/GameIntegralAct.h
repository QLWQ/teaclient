#ifndef __GAMEINTEGRALACT_H__
#define __GAMEINTEGRALACT_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;
using namespace HN;

struct DATE_TASKLIST;
class GameIntegralAct : public HNLayer, public HNHttpDelegate, public ui::EditBoxDelegate
{
public:
	GameIntegralAct();
	~GameIntegralAct();

	virtual bool init() override;
	CREATE_FUNC(GameIntegralAct);
	typedef std::function<void()> CloseCallBack;
	CloseCallBack	onCloseCallBack = nullptr;

	//调用商场接口
	typedef std::function<void()> OpenStoreCallBack;
	OpenStoreCallBack	onOpenStoreCallBack = nullptr;
private:
	void closeFunc() override;
	void LeftButtonCallBack(Ref* pSender);

	void InitButton(const std::string& title, const std::string& content);
	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;
	virtual void editBoxReturn(ui::EditBox* editBox) override;
	virtual void editBoxEditingDidEnd(ui::EditBox* editBox) override;

	void refreshList(ListView* list, const vector<DATE_TASKLIST>& datas);
	void ClickReceiveBtn(Ref* pSender);
	void RefreshViewList(int Index);
private:
	ListView* _list_btn = nullptr;
	ImageView*		_img_bg = nullptr;
	std::map<std::string, Layout*>  _panel_map;
	vector<DATE_TASKLIST> m_datas;
};



#endif // !__GAMEINTEGRALACT_H__
