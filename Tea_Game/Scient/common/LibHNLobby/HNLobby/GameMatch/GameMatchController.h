/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_GameMatchController_h__
#define HN_GameMatchController_h__

#include "HNUIExport.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "HNSocket/HNSocketMessage.h"

using namespace HN;
using namespace ui;
using namespace cocostudio;

USING_NS_CC;

class GameMatchController : public HNLayer, public IGameMatchMessageDelegate
{
	enum RankingType
	{
		Champion = 1,	//冠军
		RunnerUp,		//亚军
		Third,			//季军
		Other			//优胜奖
	};

	enum NUM_ENUM
	{
		MAX_PALYER = 10
	};

public:
	GameMatchController();
	virtual ~GameMatchController();

	virtual bool init(BYTE deskNo);

	static GameMatchController* create(BYTE deskNo);

public:
	// 设置排行显示位置
	void setRankingTipPos(Vec2 pos = Vec2::ZERO, Color4B color = Color4B::WHITE, int fontsize = 24);
	void testGame();

	// 设置结算界面
	void setContestResult(string bg_str, LLONG GameScore[MAX_PALYER]);
private:

	// 用户比赛信息
	virtual void I_R_M_UserContest(MSG_GR_ContestChange* contestChange) override;

	// 通知用户晋级
	virtual void I_R_M_UserPromotion() override;

	// 比赛结束颁奖
	virtual void I_R_M_ContestOver(MSG_GR_ContestAward* contestAward) override;

	// 比赛淘汰
	virtual void I_R_M_ContestKick(MSG_GR_R_ContestMingCi* contestRank) override;

	// 等待比赛结束
	virtual void I_R_M_ContestWaitOver() override;

	// 个人参赛纪录
	virtual void I_R_M_ContestRecord(MSG_GR_ContestRecord_Result* contestRecord) override;

	// 比赛开赛动画
	void showContestBegin();

	// 比赛等待动画
	void showContestWait();

	// 比赛排队动画
	void showContestQueue();

	// 比赛淘汰动画
	void showContestKick(int Rank);

	// 比赛晋级动画
	void showContestPromotion();

	// 比赛颁奖动画
	void showContestAwards(MSG_GR_ContestAward* contestAward);

	// 清理其他动画
	void clearAnimation();

	// 更新比赛轮数
	virtual void I_R_M_ContestRoundNum(MSG_GR_ConTestRoundNum* contestRoundNum) override;

	// 更新比赛局数
	virtual void I_R_M_ContestInnings(MSG_GR_ConTestRoundNumInOneRound* contestInnings) override;

	// 场景状态比赛进度
	virtual void I_R_M_ContestInfoupdate(MSG_GM_S_ConTestGameInfo* contestInfodata) override;

	// 游戏结算消息转接
	virtual void I_R_M_ContestResult(void* object, INT objectSize) override;

	// 比赛状态更新数据
	virtual void I_R_M_ContestInfoMatchWait(MSG_GR_SContestWaitRiseInfo* contestinfowait) override;

	// 游戏开始去掉组队中动画
	void GameStardClean();

	// 请求排行榜数据
	void sendRankQuest();

	// 接收排行榜数据
	bool ResultRankCallback(HNSocketMessage* socketMessage);

	// 查询当前自己的排名信息
	bool ResultRankNum(HNSocketMessage* socketMessage);

	// 绘画排行榜界面
	void showMatchRank();

	// 定时器更新
	void updateTimeCount(float dt);

	// 超时异常处理
	void updateTimeOut(float dt);

protected:
	Text*		_text_tip = nullptr;
	ImageView*	_img_RankList = nullptr;
	ListView*	_list_rank = nullptr;
	TextBMFont* _FNTtext_MatchGame = nullptr;
	ImageView*	_Image_Share = nullptr;
	Text*		_Text_Time = nullptr;
	Layout*	_GameBg = nullptr;
	Layout*	_panel_result = nullptr;

	//等待中的排名状态
	Text*		_Text_RankNum = nullptr;
	bool		_waitOver = false;		//是否处于等待颁奖
	bool		_isJoin = false;		//是否有参加当局比赛
	bool		_eliminate = false;		//是否已经被淘汰
	int		_RoundNum = 0;
	int		_RoundNumTwo = 0;
	int		_RunGameCount = 0;
	int		_RunGameCountTwo = 0;

	//当前比赛总阶段以及当前阶段
	int		_totalRounds = 0;
	int		_curRound = 0;
	int		_arrRiseList[10];

	EventListenerCustom*	_listener = nullptr;
	vector<MSG_GR_ContestPaiMing>	_Vec_RankData;
	int		_nowTime;
	int		_nowIndex;

	//请求当前排名冷却时间
	int		_RankTime;

	//当前排名信息获取
	int		_index_Rank;
	//当前等待桌数处理
	int		_index_DeskNum;

	//是否接收到淘汰消息
	bool		_isMatchOver = false;
	bool		_isMatchPromotion = false;

	//是否当前显示等待中动画
	bool		_isMatchWaitGame = false;

	//获胜奖励数据
	MSG_GR_ContestAward* _Data_Award;
};

#endif // HN_GameMatchController_h__
