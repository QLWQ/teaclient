/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_GameMatchWaiting_h__
#define HN_GameMatchWaiting_h__

#include "cocos2d.h"
#include "HNNetExport.h"
#include "HNRoomLogic/HNRoomLogicBase.h"

using namespace ui;
using namespace cocostudio;

USING_NS_CC;

namespace HN
{
	class GameMatchWaiting : public HNLayer, public IGameMatchMessageDelegate, public HN::IHNRoomLogicBase
	{
	public:
		static GameMatchWaiting* createMatchWaiting(int matchID, bool isFull = false);

		GameMatchWaiting();

		~GameMatchWaiting();

		virtual void onEnterTransitionDidFinish() override;

		void close();

		// 检测资源更新
		void checkResUpdate(int roomID, int gameID);

		//更新结果回调
		std::function <void(bool isSucccess, cocos2d::extension::EventAssetsManagerEx::EventCode code)> updateResult;

		bool updateMatchInfo(HNSocketMessage* socketMessage);

		void getMatchInfo(const int& iContestID);

		//void checkResUpdate(int roomID, int gameID);
	private:
		// 登陆房间回调
		virtual void onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode) override;

		static GameMatchWaiting* create(int matchID, bool isFull);

		bool init(int matchID, bool isFull);

		// 报名/退赛
		void sendRegistOrRetire(bool isRegist);

		// 刷新人数
		void updateUserCount();

	private:
		// 获取比赛信息结果
		//bool getMatchInfoCallback(HNSocketMessage* socketMessage);

		// 退赛结果
		bool retireCallback(HNSocketMessage* socketMessage);

		// 比赛取消监听
		void contestAbandonListener();

		// 比赛即将开赛监听
		void contestInitListener();		
		
	protected:
		HNRoomLogicBase*		_roomLogic = nullptr;
		int						_matchID = -1;			//比赛id
		bool					_isFull = false;		// 是否满人赛
		Button*					_btn_retire = nullptr;	//退赛按钮
		ImageView*				_img_tip = nullptr;		//开赛提示
		ImageView*				_imgBG = nullptr;
		EventListenerCustom*	_listener = nullptr;	// 断线重连成功监听
		EventListenerCustom*	_foreground = nullptr;	//切后台监听

		bool					_isFullMatch = false;
		int					_Time_H = 0;
		int					_Time_M = 0;
		
		//int					_current = 0;
		int					_NeetNum = 0;
	};
}

#endif //HN_GameMatchWaiting_h__

