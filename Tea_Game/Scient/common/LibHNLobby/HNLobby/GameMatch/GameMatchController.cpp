/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameMatchController.h"
#include "HNNetExport.h"
#include <string>
#include "UMeng/UMengSocial.h"
#include "HNCommon/HNLog.h"
#include "../GameBaseLayer/GamePlatform.h"
#include "../GameReconnection/Reconnection.h"
#include "../GameChildLayer/GameShareLayer.h"

static const char* MATCH_BEGIN_CSB	= "platform/match/match_begin_Node.csb";
static const char* MATCH_WIN_CSB	= "platform/match/match_win_Node.csb";
static const char* MATCH_LOSE_CSB	= "platform/match/match_lose_Node.csb";
static const char* MATCH_WAIT_CSB	= "platform/match/match_wait_Node.csb";
static const char* MATCH_QUEUE_CSB	= "platform/match/match_queue_Node.csb";
static const char* MATCH_AWARD_CSB	= "platform/match/match_award_Node.csb";
static const char* MATCH_CONTROLLER	= "platform/match/match_controllerLayer.csb";
static const int WAIT_GAMETIME[] = { 90, 70, 50, 30, 10 };
static const int PLAYER_COUNT = 4;
static const int ACTION_MAX_COUNT = 20;
static const int WAIT_TIME_OUT = 10;

#pragma pack(1)
//游戏结算结构体
struct GameStatusGameFinish
{
	char	allZimoArray[PLAYER_COUNT];			/*所有自摸次数*/
	char	allDianpaoArray[PLAYER_COUNT];		/*所有点炮次数*/
	char	allYouJinArray[PLAYER_COUNT];			/*所有游金次数*/
	char	allShuanYouJinArray[PLAYER_COUNT];			/*所有双游金次数*/
	char	allSanYouJinArray[PLAYER_COUNT];			/*所有三游金次数*/
	char	allSanJinDaoArray[PLAYER_COUNT];		/*所有三金倒次数*/
	char	allJiepaoArray[PLAYER_COUNT];		/*所有接炮次数*/
	char	allMinggangArray[PLAYER_COUNT];		/*所有明杠次数*/
	char	allAngangArray[PLAYER_COUNT];		/*所有暗杠次数*/
	bool	isHu[PLAYER_COUNT];					/*是否是胡*/
	bool	isZiMo[PLAYER_COUNT];				/*是否是自摸*/
	bool	isqiangjing[PLAYER_COUNT];			//是否抢金
	bool	issanjindao[PLAYER_COUNT];			//是否三金倒
	bool	istianhu[PLAYER_COUNT];				//是否天胡
	bool	isyoujin[PLAYER_COUNT][3];			//是否游金
	bool	isbahuayou[PLAYER_COUNT];			//是否八花游
	bool    isliuju;							//是否是流局
	int		minggangPan[PLAYER_COUNT];			/*明杠盘*/
	int		angangPan[PLAYER_COUNT];			/*暗杠盘*/
	int		bugangPan[PLAYER_COUNT];			/*补杠盘*/
	int		keziPan[PLAYER_COUNT];				/*刻子盘*/
	int		huaPan[PLAYER_COUNT];				/*花盘*/
	int		jinPan[PLAYER_COUNT];				/*金盘*/
	int		ziPaipengPan[PLAYER_COUNT];			/*字牌盘*/
	int		Allpan[PLAYER_COUNT];				/*总盘数*/
	int		allRoomScoreArray[PLAYER_COUNT];
	int		allPlayerUser[PLAYER_COUNT];
	int		totalScoreArray[PLAYER_COUNT];		/* 总得分 */
	char	finishCardId;						/* 最后一张牌，出的牌或者发的牌 */
	char	actCardArray[PLAYER_COUNT][ACTION_MAX_COUNT][10];	 /* 吃碰杠听，所有动作的倍数，0下标是动作类型，1下标保存点炮对方座位，2下标是卡组数量，后面是卡牌 */
	char	actCountArray[PLAYER_COUNT];		/* 动作数量 */
	char	handCardArray[PLAYER_COUNT][30];	/* 手牌 */
	char	handCountArray[PLAYER_COUNT];		/* 手牌数量*/
	char	huCardId[PLAYER_COUNT];				/* 胡的牌 */
	char	huTypeArray[PLAYER_COUNT];			/* 胡牌类型 */
	char	additionHuTypeArray[PLAYER_COUNT];	/* 附加胡*/
	int  	alltotalScoreArray[PLAYER_COUNT];	/*所有总得分*/
	int		bankUser;							/*庄家位置*/
};

GameMatchController::GameMatchController()
{
	memset(_arrRiseList, 0, sizeof(_arrRiseList));
}

GameMatchController::~GameMatchController()
{
	if (_Text_Time)
	{
		unschedule(schedule_selector(GameMatchController::updateTimeCount));
	}
	if (_nowTime)
	{
		unschedule(schedule_selector(GameMatchController::updateTimeOut));
	}
	RoomLogic()->removeEventSelector(MDM_GR_ROOM, ASS_GR_CONTEST_PAIHANG);
	RoomLogic()->setMatchDelegate(nullptr);
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
}

GameMatchController* GameMatchController::create(BYTE deskNo)
{
	GameMatchController *pRet = new GameMatchController();
	if (pRet && pRet->init(deskNo))
	{
		pRet->autorelease();
		return pRet;
	}
	CC_SAFE_DELETE(pRet);
	return pRet;
}

bool GameMatchController::init(BYTE deskNo)
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(MATCH_CONTROLLER);
	//node->setPosition(_winSize / 2);
	addChild(node,-1);

	//排行列表
	_img_RankList = dynamic_cast<ImageView*>(node->getChildByName("Image_rank_bg"));
	//_img_RankList->setLocalZOrder(99999);
	_list_rank = dynamic_cast<ListView*>(_img_RankList->getChildByName("list_rank"));
	Button* btn_close = dynamic_cast<Button*>(_img_RankList->getChildByName("btn_close"));
	btn_close->addClickEventListener([=](Ref*) {
		_img_RankList->setVisible(false);
	});

	//游戏背景
	_GameBg = dynamic_cast<Layout*>(node->getChildByName("panel_make"));

	//游戏结算
	_panel_result = dynamic_cast<Layout*>(node->getChildByName("panel_result"));

	//当前排名按钮
	Button* Button_rank = dynamic_cast<Button*>(node->getChildByName("Button_rank"));
	Button_rank->addClickEventListener([=](Ref*) {
		//请求排行榜数据更新
		_Vec_RankData.clear();
		sendRankQuest();
		//_img_RankList->setVisible(true);
	});

	//剩余张数
	TextBMFont* text_CardNum = dynamic_cast<TextBMFont*>(node->getChildByName("fnt_CardNum"));
	text_CardNum->setVisible(false);

	//比赛场当前进度信息
	_FNTtext_MatchGame = dynamic_cast<TextBMFont*>(node->getChildByName("fnt_MatchGame"));
	_FNTtext_MatchGame->setString("");

	_text_tip = dynamic_cast<Text*>(node->getChildByName("Text_rank"));/*Text::create(StringUtils::format(GBKToUtf8("当前排名：%d / %d"), RoomLogic()->loginResult.pUserInfoStruct.iRankNum, RoomLogic()->loginResult.iRemainPeople), "", 24);
	_text_tip->setPosition(Vec2(_winSize.width * 0.91f, _winSize.height * 0.93f));
	addChild(_text_tip);*/

	RoomLogic()->setMatchDelegate(this);

	// 比赛开赛前进入则显示排队状态
	if (deskNo == INVALID_DESKNO)
	{
		showContestQueue();
	}
	else
	{
		_isJoin = true;
	}

	// 监听开赛消息
	_listener = EventListenerCustom::create(RECONNECTION, [=](EventCustom* event){
		
		showContestBegin();
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);

	return true;
}

void GameMatchController::testGame()
{
	_RoundNum = 8;
	_RoundNumTwo = 4;
	I_R_M_ContestWaitOver();
}

void GameMatchController::setRankingTipPos(Vec2 pos/* = Vec2::ZERO*/, Color4B color/* = Color4B::WHITE*/, int fontsize/* = 24*/)
{
	if (pos != Vec2::ZERO)
	{
		_text_tip->setPosition(pos);
	}

	if (_text_tip->getTextColor() != Color4B::WHITE)
	{
		_text_tip->setTextColor(color);
	}

	if (_text_tip->getFontSize() != 24)
	{
		_text_tip->setFontSize(fontsize);
	}
}

void GameMatchController::I_R_M_UserContest(MSG_GR_ContestChange* contestChange)
{
	_text_tip->setString(StringUtils::format(GBKToUtf8("当前排名：%d / %d"), contestChange->iRankNum, contestChange->iRemainPeople));

	/*if (!_waitOver 
		&& _isJoin 
		&& !_eliminate 
		&& UserInfoModule()->findUser(PlatformLogic()->loginResult.dwUserID)->bUserState == USER_LOOK_STATE) */
		//showContestPromotion();
}

void GameMatchController::I_R_M_UserPromotion()
{
	if (_isJoin&& !_eliminate)
	{
		_isMatchPromotion = true;
		showContestPromotion();
	}
}

void GameMatchController::I_R_M_ContestRoundNum(MSG_GR_ConTestRoundNum* contestRoundNum)
{
	_RoundNum = contestRoundNum->RoundNum;
	_RoundNumTwo = contestRoundNum->RoundNumTwo;
	_totalRounds = contestRoundNum->totalRounds;
	_curRound = contestRoundNum->curRound;

	for (int i = 0; i < _totalRounds; i++)
	{
		_arrRiseList[i] = contestRoundNum->riseList[i];
	}
	 
	if (_RunGameCount != 0)
	{
		if (_RoundNumTwo == 2)
		{
			//_FNTtext_MatchGame->setString(StringUtils::format(GBKToUtf8("总决赛.第%d/%d局"), _RunGameCount, _RunGameCountTwo));
		}
		else
		{
			//_FNTtext_MatchGame->setString(StringUtils::format(GBKToUtf8("%d进%d.第%d/%d局"), _RoundNum, _RoundNumTwo, _RunGameCount, _RunGameCountTwo));
		}
	}
}

void GameMatchController::I_R_M_ContestInnings(MSG_GR_ConTestRoundNumInOneRound* contestInnings)
{
	_RunGameCount = contestInnings->ContestRunGameCount;
	_RunGameCountTwo = contestInnings->ContestRunGameCountTwo;
	if (_RoundNum != 0)
	{
		if (_RoundNumTwo == 2)
		{
			//_FNTtext_MatchGame->setString(StringUtils::format(GBKToUtf8("总决赛.第%d/%d局"), _RunGameCount, _RunGameCountTwo));
		}
		else
		{
			//_FNTtext_MatchGame->setString(StringUtils::format(GBKToUtf8("%d进%d.第%d/%d局"), _RoundNum, _RoundNumTwo, _RunGameCount, _RunGameCountTwo));
		}
	}
}

void GameMatchController::I_R_M_ContestInfoupdate(MSG_GM_S_ConTestGameInfo* contestInfodata)
{
	//轮数
	_RoundNum = contestInfodata->iCurrentRound;
	_RoundNumTwo = contestInfodata->iCurrentRoundToNext;
	//局数
	_RunGameCount = contestInfodata->iCurrentJu;
	_RunGameCountTwo = contestInfodata->iTotalJu;

	//总轮数
	_totalRounds = contestInfodata->totalRounds;
	_curRound = contestInfodata->curRound;
	for (int i = 0; i < _totalRounds; i++)
	{
		_arrRiseList[i] = contestInfodata->riseList[i];
	}

	if (_RoundNum != 0)
	{
		if (_RoundNumTwo == 2)
		{
			//_FNTtext_MatchGame->setString(StringUtils::format(GBKToUtf8("总决赛.第%d/%d局"), _RunGameCount, _RunGameCountTwo));
		}
		else
		{
			//_FNTtext_MatchGame->setString(StringUtils::format(GBKToUtf8("%d进%d.第%d/%d局"), _RoundNum, _RoundNumTwo, _RunGameCount, _RunGameCountTwo));
		}
	}
	GameStardClean();
}

void GameMatchController::I_R_M_ContestInfoMatchWait(MSG_GR_SContestWaitRiseInfo* contestinfowait)
{
	_index_Rank = contestinfowait->userRank;
	_index_DeskNum = contestinfowait->remainTables;
	if (getChildByName("wait"))
	{
		showContestWait();
	}
}

void GameMatchController::I_R_M_ContestResult(void* object, INT objectSize)
{
	char* pData = (char*)object;
	int size = sizeof(GameStatusGameFinish);
	//CCAssert(sizeof(GameStatusGameFinish) == datasize, "Broken message GameStatusGameFinish.");
	if (size == objectSize)
	{
		GameStatusGameFinish* pProtData = (GameStatusGameFinish*)pData;
		_GameBg->setVisible(true);
		_panel_result->setVisible(true);
		auto userInfo = RoomLogic()->loginResult.pUserInfoStruct;
		// 显示玩家
		std::vector<UserInfoStruct *> vec;
		UserInfoModule()->findDeskUsers(userInfo.bDeskNO, vec);
		int Index = 2;
		for (auto &v : vec)
		{
			if (v == nullptr)
			{

			}
			else
			{
				int allRoomScore = v->i64ContestScore;
				for (int i = 0; i<PLAYER_COUNT; i++)
				{
					if (pProtData->allPlayerUser[i] == v->dwUserID)
					{
						allRoomScore = allRoomScore + pProtData->allRoomScoreArray[i];
						string str = StringUtils::format("sp_user_%d", Index);
						if (Index < 0)
						{
							break;
						}
						if (pProtData->allPlayerUser[i] == userInfo.dwUserID)
						{
							str = StringUtils::format("sp_user_%d", 3);
						}
						else
						{
							Index--;
						}
						Sprite* Sp_node = dynamic_cast<Sprite*>(_panel_result->getChildByName(str));
						if (pProtData->allRoomScoreArray[i] > 0)
						{
							//获胜玩家背景
							Sp_node->setTexture("platform/match/res/result/bj_1.png");
						}
						else
						{
							Sp_node->setTexture("platform/match/res/result/bj_2.png");
						}
						Sp_node->setVisible(true);

						//用户名字
						Text* text_name = dynamic_cast<Text*>(Sp_node->getChildByName("text_name"));
						Tools::TrimSpace(v->nickName);
						string strName = v->nickName;
						if (strName.length() > 16)
						{
							strName.erase(17, strName.length());
							strName.append("...");
						}
						text_name->setString(GBKToUtf8(strName));

						//分数
						Text* text_score = dynamic_cast<Text*>(Sp_node->getChildByName("text_score"));
						text_score->setString(to_string(allRoomScore));

						//输赢积分
						TextAtlas* AtlasLabel_Score = dynamic_cast<TextAtlas*>(Sp_node->getChildByName("AtlasLabel_Score"));
						if (pProtData->allRoomScoreArray[i] > 0)
						{
							string str = "/";
							str = str + to_string(pProtData->allRoomScoreArray[i]);
							AtlasLabel_Score->setString(str);
						}
						else if (pProtData->allRoomScoreArray[i] < 0)
						{
							string str = ".";
							str = str + to_string(abs(pProtData->allRoomScoreArray[i]));
							AtlasLabel_Score->setString(str);
						}
						else
						{
							AtlasLabel_Score->setString("0");
						}

						ImageView* image_head = dynamic_cast<ImageView*>(Sp_node->getChildByName("image_head"));
						auto userHead = GameUserHead::create(image_head);
						userHead->show();
						userHead->loadTexture(v->bBoy ? "platform/common/boy.png" : "platform/common/girl.png");
						userHead->setHeadByFaceID(v->bLogoID);
						if (v->headUrl != ""){
							userHead->loadTextureWithUrl(v->headUrl);
						}
						break;
					}
				}
			}
		}
		_panel_result->runAction(Sequence::create(DelayTime::create(3.0f), CallFunc::create([=](){
			_GameBg->setVisible(false);
			_panel_result->setVisible(false);
			/*if (_isMatchOver)
			{
				_isMatchOver = false;
				showContestKick(_index_Rank);
			}
			else if (_isMatchPromotion)
			{
				_isMatchPromotion = false;
				showContestPromotion();
			}*/
		}), nullptr));
	}
}

void GameMatchController::GameStardClean()
{
	if (getChildByName("wait"))
	{
		if (_Text_Time)
		{
			unschedule(schedule_selector(GameMatchController::updateTimeCount));
		}
		getChildByName("wait")->removeFromParent();
		_isMatchWaitGame = false;
		_nowTime = 0;
	}
	if (getChildByName("queue"))
	{
		getChildByName("queue")->removeFromParent();
		unschedule(schedule_selector(GameMatchController::updateTimeOut));
	}
}

void GameMatchController::sendRankQuest()
{
	MSG_GP_I_GetContestAwardlist registration;
	//registration.UserID = PlatformLogic()->loginResult.dwUserID;
	HNLOGEX_DEBUG("%d RoomLogic()->sendData(MDM_GR_ROOM, ASS_GR_CONTEST_PAIHANG).", ASS_GR_CONTEST_PAIHANG);
	RoomLogic()->sendData(MDM_GR_ROOM, ASS_GR_CONTEST_PAIHANG, (void*)&registration, sizeof(registration),
		HN_SOCKET_CALLBACK(GameMatchController::ResultRankCallback, this));
}

bool GameMatchController::ResultRankNum(HNSocketMessage* socketMessage)
{
	if (socketMessage->messageHead.bHandleCode == 1)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GR_ContestPaiMing, socketMessage->objectSize, true, "MSG_GR_ContestPaiMing size is error");
		MSG_GR_ContestPaiMing* info = (MSG_GR_ContestPaiMing*)socketMessage->object;
		if (info->dwUserID == PlatformLogic()->loginResult.dwUserID)
		{
			_index_Rank = info->iRankNum;
		}
	}
	else
	{
		if (!_isMatchWaitGame)
		{
			showContestWait();
		}
		else
		{
			if (_Text_RankNum)
			{
				_Text_RankNum->setString(to_string(_index_Rank));
			}
		}
	}
	return true;
}

bool GameMatchController::ResultRankCallback(HNSocketMessage* socketMessage)
{
	HNLOGEX_DEBUG("%d ASS_GR_CONTEST_PAIHANG ==> CALLBACK.", ASS_GR_CONTEST_PAIHANG);
	//排行榜数据多条接收处理
	if (socketMessage->messageHead.bHandleCode == 1)
	{
		//int size = sizeof(MSG_GR_ContestPaiMing);
		CHECK_SOCKET_DATA_RETURN(MSG_GR_ContestPaiMing, socketMessage->objectSize, true, "MSG_GR_ContestPaiMing size is error");
		MSG_GR_ContestPaiMing* info = (MSG_GR_ContestPaiMing*)socketMessage->object;
		MSG_GR_ContestPaiMing Data;
		Data.dwUserID = info->dwUserID;
		Data.i64ContestScore = info->i64ContestScore;
		Data.iRankNum = info->iRankNum;
		Data.iUpPeople = info->iUpPeople;
		strcpy(Data.szUserName, info->szUserName);
		Data.iRunNum = info->iRunNum;
		Data.iAllRunNum = info->iAllRunNum;
		Data.iIsKick = info->iIsKick;
		Data.pAwards = info->pAwards;
		Data.pAwardTypes = info->pAwardTypes;
		_Vec_RankData.push_back(Data);
	}
	else
	{
		//接收完毕
		showMatchRank();
	}
	return true;
}

void GameMatchController::I_R_M_ContestOver(MSG_GR_ContestAward* contestAward)
{
	_Data_Award = contestAward;
	showContestAwards(contestAward);
}

void GameMatchController::I_R_M_ContestKick(MSG_GR_R_ContestMingCi* contestRank)
{
	_eliminate = true;
	_isMatchOver = true;
	_index_Rank = contestRank->Rank;
	showContestKick(_index_Rank);
}

void GameMatchController::I_R_M_ContestWaitOver()
{
	_waitOver = true;
	//MSG_GP_I_GetContestAwardlist registration;
	////registration.UserID = PlatformLogic()->loginResult.dwUserID;
	//RoomLogic()->sendData(MDM_GR_ROOM, ASS_GR_CONTEST_PAIHANG, (void*)&registration, sizeof(registration),
		//HN_SOCKET_CALLBACK(GameMatchController::ResultRankNum, this));
	_nowTime = 0;
	showContestWait();
}

void GameMatchController::I_R_M_ContestRecord(MSG_GR_ContestRecord_Result* contestRecord)
{

}

// 比赛开赛动画
void GameMatchController::showContestBegin()
{
	if (_isJoin) return;
	_isJoin = true;

	clearAnimation();
	
	HNAudioEngine::getInstance()->playEffect("platform/sound/match/resultbus/bout_start.mp3");

	auto node = CSLoader::createNode(MATCH_BEGIN_CSB);
	node->setPosition(_winSize / 2);
	addChild(node, 10);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_begin"));
	auto panelBG = dynamic_cast<Layout*>(layout->getChildByName("Panel_bg"));
	panelBG->setScale(_winSize.width / 1280, _winSize.height / 720);

	auto sprite = dynamic_cast<Sprite*>(layout->getChildByName("Sprite_begin"));

	auto timeline = CSLoader::createTimeline(MATCH_BEGIN_CSB);
	node->runAction(timeline);
	timeline->play("begin", false);

	timeline->setAnimationEndCallFunc("begin", [=](){

		HNAudioEngine::getInstance()->playEffect("platform/sound/match/resultbus/match_start.mp3");

		sprite->runAction(Sequence::create(ScaleBy::create(0.3f, 3.0f), CallFunc::create([=](){
			node->removeFromParent();
		}), nullptr));
	});
}

// 比赛等待动画
void GameMatchController::showContestWait()
{
	clearAnimation();

	HNAudioEngine::getInstance()->playEffect("platform/sound/match/playview/island_sound_win.mp3");

	auto node = CSLoader::createNode(MATCH_WAIT_CSB);
	node->setPosition(_winSize / 2);
	node->setName("wait");
	addChild(node, 10);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_wait"));
	auto panelBG = dynamic_cast<Layout*>(layout->getChildByName("Panel_bg"));
	panelBG->setScale(_winSize.width / 1280, _winSize.height / 720);

	//根据当前进度进行等待中更新
	auto Image_Rank = dynamic_cast<ImageView*>(layout->getChildByName("Image_Rank"));
	_Text_RankNum = dynamic_cast<Text*>(Image_Rank->getChildByName("text_rank"));
	_Text_RankNum->setString(to_string(_index_Rank));

	auto Text_Tip = dynamic_cast<Text*>(layout->getChildByName("text_tip"));
	//Text_Tip->setString(StringUtils::format(GBKToUtf8("请等待其他%d桌结束,确认前%d晋级名单.预计等待     秒", _index_DeskNum, _arrRiseList[_curRound])));
	int ContentSizeX = Text_Tip->getContentSize().width;
	auto text_desknum = dynamic_cast<Text*>(Text_Tip->getChildByName("text_desknum"));
	text_desknum->setPosition(Vec2(ContentSizeX, 13.5));
	text_desknum->setString(to_string(_index_DeskNum));
	ContentSizeX = ContentSizeX + text_desknum->getContentSize().width;
	auto text_tip_1 = dynamic_cast<Text*>(Text_Tip->getChildByName("text_tip_1"));
	text_tip_1->setPosition(Vec2(ContentSizeX, 13.5));
	ContentSizeX = ContentSizeX + text_tip_1->getContentSize().width;
	auto text_nextplay = dynamic_cast<Text*>(Text_Tip->getChildByName("text_nextplay"));
	text_nextplay->setPosition(Vec2(ContentSizeX, 13.5));
	text_nextplay->setString(to_string(_arrRiseList[_curRound]));
	ContentSizeX = ContentSizeX + text_nextplay->getContentSize().width;
	auto text_tip_2 = dynamic_cast<Text*>(Text_Tip->getChildByName("text_tip_2"));
	text_tip_2->setPosition(Vec2(ContentSizeX, 13.5));
	ContentSizeX = ContentSizeX + text_tip_2->getContentSize().width;
	_Text_Time = dynamic_cast<Text*>(Text_Tip->getChildByName("text_time"));
	_Text_Time->setPosition(Vec2(ContentSizeX + 14.5, 13.5));
	ContentSizeX = ContentSizeX + _Text_Time->getContentSize().width;
	auto text_tip_3 = dynamic_cast<Text*>(Text_Tip->getChildByName("text_tip_3"));
	text_tip_3->setPosition(Vec2(ContentSizeX, 13.5));

	auto panel_LastRound = dynamic_cast<Layout*>(layout->getChildByName("panel_LastRound3"));
	auto panel_MoreRound = dynamic_cast<Layout*>(layout->getChildByName("panel_MoreRound"));
	if (_totalRounds - _curRound > 2 && _totalRounds != 0)
	{
		panel_LastRound->setVisible(false);
		//剩余阶段大于3阶段处理
		panel_MoreRound->setVisible(true);
		//更新阶段晋级数据
		auto text = dynamic_cast<Text*>(panel_MoreRound->getChildByName("Text_PlayerNum"));
		text->setString(to_string(_arrRiseList[_curRound - 1]));
		auto text_next = dynamic_cast<Text*>(panel_MoreRound->getChildByName("Text_NextNum"));
		text_next->setString(to_string(_arrRiseList[_curRound]));
	}
	else if (_totalRounds - _curRound <= 2 && _totalRounds != 0)
	{
		panel_LastRound->setVisible(true);
		panel_MoreRound->setVisible(false);

		//前8晋级
		auto round_8 = dynamic_cast<Layout*>(panel_LastRound->getChildByName("panel_round_1"));
		auto round_4 = dynamic_cast<Layout*>(panel_LastRound->getChildByName("panel_round_2"));
		auto round_1 = dynamic_cast<Layout*>(panel_LastRound->getChildByName("panel_round_3"));

		//初始化晋级前几数据
		for (int i = _totalRounds - 1; i > 0; i--)
		{
			if (_totalRounds - i == 2)
			{
				auto text = dynamic_cast<Text*>(panel_LastRound->getChildByName("text_promotion_1"));
				text->setString(to_string(_arrRiseList[i]));
				auto text_Round = dynamic_cast<Text*>(round_8->getChildByName("text_promotion"));
				text_Round->setString(to_string(_arrRiseList[i]));
				auto text_index_1 = dynamic_cast<Text*>(panel_LastRound->getChildByName("text_index_1"));
				text_index_1->setString(GBKToUtf8(StringUtils::format("第%d轮", _totalRounds - 2)));
			}
			else if (_totalRounds - i == 1)
			{
				auto text = dynamic_cast<Text*>(panel_LastRound->getChildByName("text_promotion_2"));
				text->setString(to_string(_arrRiseList[i]));
				auto text_Round = dynamic_cast<Text*>(round_4->getChildByName("text_promotion"));
				text_Round->setString(to_string(_arrRiseList[i]));
				auto text_index_1 = dynamic_cast<Text*>(panel_LastRound->getChildByName("text_index_2"));
				text_index_1->setString(GBKToUtf8(StringUtils::format("第%d轮", _totalRounds - 1)));
			}
			else
			{
				auto text_index_1 = dynamic_cast<Text*>(panel_LastRound->getChildByName("text_index_3"));
				text_index_1->setString(GBKToUtf8(StringUtils::format("第%d轮", _totalRounds)));
				break;
			}
		}

		//倒数第三轮
		if (_totalRounds - _curRound == 2 && _totalRounds != 0)
		{
			round_8->setVisible(true);
		}
		//倒数第二轮
		else if (_totalRounds - _curRound == 1 && _totalRounds != 0)
		{
			round_8->setVisible(true);
			round_4->setVisible(true);
		}
		//最后一轮
		else if (_curRound == _totalRounds && _totalRounds != 0)
		{
			round_8->setVisible(true);
			round_4->setVisible(true);
			round_1->setVisible(true);
		}
	}
	else
	{
		//出现错误
		assert(false);
	}

	//开启预计倒计时(更新等待界面不重置倒计时)
	if (_nowTime == 0)
	{
		_nowIndex = 0;
		_nowTime = WAIT_GAMETIME[_nowIndex];
		_RankTime = 10;
	}
	_Text_Time->setVisible(true);
	_Text_Time->setString(to_string(_nowTime));
	schedule(schedule_selector(GameMatchController::updateTimeCount), 1.0f);

	_isMatchWaitGame = true;

	/*auto timeline = CSLoader::createTimeline(MATCH_WAIT_CSB);
	node->runAction(timeline);
	timeline->play("animation0", false);

	timeline->setAnimationEndCallFunc("animation0", [=](){

		auto img_wait = dynamic_cast<ImageView*>(layout->getChildByName("Image_wait"));
		img_wait->runAction(RepeatForever::create(Sequence::create(FadeOut::create(1.0f), FadeIn::create(1.0f), nullptr)));
	});*/
}

void GameMatchController::updateTimeCount(float dt)
{
	_nowTime--;
	_RankTime--;
	if (_Text_Time)
	{
		_Text_Time->setString(to_string(_nowTime));
		//不需要定时请求排名榜数据
		//if (_RankTime <= 0)
		//{
			//I_R_M_ContestWaitOver();
		//}
		//_Text_RankNum->setString(to_string(UserInfoModule()->findUser(PlatformLogic()->loginResult.dwUserID)->iRankNum));
	}
	if (_nowTime < 0)
	{
		_nowIndex++;
		if (_nowIndex > 4)
		{
			_nowIndex = 4;
		}
		_nowTime = WAIT_GAMETIME[_nowIndex];
		if (_Text_Time)
		{
			_Text_Time->setString(to_string(_nowTime));
			//_Text_RankNum->setString(to_string(UserInfoModule()->findUser(PlatformLogic()->loginResult.dwUserID)->iRankNum));
		}
		//schedule(schedule_selector(GameMatchController::updateTimeCount), 1.0f);
	}
}

void GameMatchController::updateTimeOut(float dt)
{
	_nowTime--;
	if (_nowTime < 0)
	{
		_isMatchWaitGame = false;
		clearAnimation();
		unschedule(schedule_selector(GameMatchController::updateTimeOut));
	}
}

// 比赛排队动画
void GameMatchController::showContestQueue()
{
	clearAnimation();

	_isJoin = false;

	auto node = CSLoader::createNode(MATCH_QUEUE_CSB);
	node->setPosition(_winSize / 2);
	node->setName("queue");
	addChild(node, 10);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_queue"));
	auto panelBG = dynamic_cast<Layout*>(layout->getChildByName("Panel_bg"));
	panelBG->setScale(_winSize.width / 1280, _winSize.height / 720);

	// 退赛按钮
	auto btn_retire = dynamic_cast<Button*>(layout->getChildByName("Button_retire"));
	btn_retire->setVisible(false);
	btn_retire->addClickEventListener([=](Ref*) {
	
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(GBKToUtf8("比赛进行中，离开将退出比赛\n是否退出？"));
		prompt->setCallBack([=]() {

			RoomLogic()->sendData(MDM_GR_ROOM, ASS_GR_CONTEST_EXIT, nullptr, 0, [=](HNSocketMessage* socketMessage) {

				if (0 == socketMessage->messageHead.bHandleCode)
				{
					RoomLogic()->close();
					GamePlatform::createPlatform();
				}
				else
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("退出比赛失败！"));
				}

				RoomLogic()->removeEventSelector(MDM_GR_ROOM, ASS_GR_CONTEST_EXIT);
				return true;
			});
		});
	});

	auto img_wait = dynamic_cast<ImageView*>(layout->getChildByName("Image_tip"));
	img_wait->runAction(RepeatForever::create(Sequence::create(FadeOut::create(1.0f), FadeIn::create(1.0f), nullptr)));

	_nowTime = WAIT_TIME_OUT;
	schedule(schedule_selector(GameMatchController::updateTimeOut), 1.0f);
}

// 比赛淘汰动画
void GameMatchController::showContestKick(int Rank)
{
	clearAnimation();

	HNAudioEngine::getInstance()->playEffect("platform/sound/match/resultbus/konckout.mp3");
	HNAudioEngine::getInstance()->playEffect("platform/sound/match/playview/promotion_sound_fail.mp3");

	auto node = CSLoader::createNode(MATCH_LOSE_CSB);
	node->setPosition(_winSize / 2);
	addChild(node, 10);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_lose"));
	auto panelBG = dynamic_cast<Layout*>(layout->getChildByName("Panel_bg"));
	panelBG->setScale(_winSize.width / 1280, _winSize.height / 720);

	auto sp_lose = dynamic_cast<Sprite*>(layout->getChildByName("sp_lose"));
	auto text_rank = dynamic_cast<Text*>(sp_lose->getChildByName("text_rank"));
	text_rank->setString(to_string(Rank));

	auto btn_close = dynamic_cast<Button*>(layout->getChildByName("Button_exit"));
	btn_close->addClickEventListener([=](Ref*){
		RoomLogic()->close();
		//GamePlatform::createPlatform();
		//更换返回大厅跳转比赛场
		GamePlatform::returnPlatformNode(MATCH);
	});

	auto timeline = CSLoader::createTimeline(MATCH_LOSE_CSB);
	node->runAction(timeline);
	timeline->play("animation0", false);
}

// 比赛晋级动画
void GameMatchController::showContestPromotion()
{
	_isMatchWaitGame = false;
	clearAnimation();

	// 颁奖界面暂时关闭重连功能
	GameReconnection()->openOrCloseReconnet(false);
	
	HNAudioEngine::getInstance()->playEffect("platform/sound/match/resultbus/promotion.mp3");
	HNAudioEngine::getInstance()->playEffect("platform/sound/match/playview/promotion_sound_win.mp3");

	auto node = CSLoader::createNode(MATCH_WIN_CSB);
	node->setPosition(_winSize / 2);
	node->setName("promotion");
	addChild(node, 10);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_win"));
	auto panelBG = dynamic_cast<Layout*>(layout->getChildByName("Panel_bg"));
	panelBG->setScale(_winSize.width / 1280, _winSize.height / 720);

	auto sprite = dynamic_cast<Sprite*>(layout->getChildByName("Sprite_win"));
	auto img_text = dynamic_cast<ImageView*>(sprite->getChildByName("Image_text"));
	auto img_anim = dynamic_cast<ImageView*>(sprite->getChildByName("Image_anim"));

	auto TextAtlas_rank = dynamic_cast<TextAtlas*>(layout->getChildByName("AtlasLabel_Ranking"));
	//TextAtlas_rank->setString(StringUtils::format("%d/%d", 
		//UserInfoModule()->findUser(PlatformLogic()->loginResult.dwUserID)->iRankNum, RoomLogic()->loginResult.iRemainPeople));

	auto sp_roundnum = dynamic_cast<Sprite*>(layout->getChildByName("sp_roundnum"));
	auto AtlasLabel_Round = dynamic_cast<TextAtlas*>(layout->getChildByName("AtlasLabel_Round"));
	//第几轮晋级赛
	if (_curRound < _totalRounds && _totalRounds != 0)
	{
		AtlasLabel_Round->setString(to_string(_curRound));
		//第二轮晋级赛
		sp_roundnum->setTexture("platform/match/res/promotion/jjs.png");
	}
	//第几轮总决赛
	else if (_curRound == _totalRounds && _totalRounds != 0)
	{
		AtlasLabel_Round->setString(to_string(_curRound));
		//第三轮总决赛
		sp_roundnum->setTexture("platform/match/res/promotion/zjs.png");
	}


	auto timeline = CSLoader::createTimeline(MATCH_WIN_CSB);
	node->runAction(timeline);
	timeline->play("animation0", false);

	timeline->setAnimationEndCallFunc("animation0", [=](){
		timeline->play("animation1", false);
		img_anim->setVisible(true);
		img_anim->runAction(Sequence::create(DelayTime::create(0.3f), ScaleBy::create(0.3f, 0.2f), Hide::create(),
			CallFunc::create([=](){
			img_text->setVisible(true);
		}), DelayTime::create(1.0f), CallFunc::create([=](){
			showContestQueue();
		}), nullptr));
	});
}

// 比赛颁奖动画
void GameMatchController::showContestAwards(MSG_GR_ContestAward* contestAward)
{
	clearAnimation();

	auto node = CSLoader::createNode(MATCH_AWARD_CSB);
	node->setPosition(_winSize / 2);
	addChild(node, 10);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_reward"));
	auto img_BG = dynamic_cast<ImageView*>(layout->getChildByName("Image_BG"));
	img_BG->setScale(_winSize.width / 1280, _winSize.height / 720);

	auto text_tip = dynamic_cast<Text*>(layout->getChildByName("Text_tip"));
	text_tip->setString(StringUtils::format(GBKToUtf8("恭喜您在【%s】比赛中获得"), GBKToUtf8(RoomLogic()->getSelectedRoom()->szGameRoomName)));
	text_tip->setVisible(contestAward->iAward != 0);

	// 名次
	auto textBMFont_rank = dynamic_cast<TextBMFont*>(layout->getChildByName("Fnt_rank"));
	textBMFont_rank->setString(StringUtils::format(GBKToUtf8("第%d名"), contestAward->iRank));

	// 名次精灵图片
	auto sp_rank = dynamic_cast<Sprite*>(layout->getChildByName("sp_rank"));
	if (contestAward->iRank < 4)
	{
		sp_rank->setTexture(StringUtils::format("platform/match/res/reward/Imag_Rank_%d.png", contestAward->iRank));
	}
	else
	{
		sp_rank->setTexture("platform/match/res/reward/Imag_Rank_4.png");
		auto text_rank = dynamic_cast<Text*>(layout->getChildByName("text_rank"));
		text_rank->setVisible(true);
		text_rank->setString(to_string(contestAward->iRank));
	}

	auto img_reward = dynamic_cast<ImageView*>(layout->getChildByName("Image_reward"));
	
	auto text_reward = dynamic_cast<Text*>(layout->getChildByName("Text_reward"));
	
	auto text_send = dynamic_cast<Text*>(layout->getChildByName("Text_send"));

	if (contestAward->iAward != 0)
	{
		img_reward->setVisible(true);
		text_reward->setVisible(true);
		text_send->setVisible(false);
	}
	else
	{
		img_reward->setVisible(false);
		text_reward->setVisible(false);
		text_send->setVisible(true);
	}
	//img_reward->setVisible(contestAward->iAward != 0);
	//img_reward->setContentSize(Size(109, 65));
	//text_reward->setVisible(contestAward->iAward != 0);
	//text_send->setVisible(contestAward->iAward == 0);

	//排名获取类型（金币）
	if (contestAward->iAwardType == 0)
	{
		img_reward->loadTexture("platform/match/res/reward/jb.png");
		text_reward->setString(StringUtils::format("X%d", contestAward->iAward));

		PlatformLogic()->loginResult.i64Money += contestAward->iAward;

		//img_reward->setVisible(true);
		//text_reward->setVisible(true);
		//text_send->setVisible(false);
	}
	//排名获取类型（红包）
	else if (contestAward->iAwardType == 1)
	{
		img_reward->loadTexture("platform/match/res/reward/hb.png");
		text_reward->setString(StringUtils::format(GBKToUtf8("X%d元"), contestAward->iAward));

		PlatformLogic()->loginResult.iLotteries += contestAward->iAward;
		//img_reward->setVisible(false);
		//text_reward->setVisible(false);
		//text_send->setVisible(false);
	}
	//排名获取类型（房卡）
	else if (contestAward->iAwardType == 2)
	{
		img_reward->loadTexture("platform/match/res/reward/fk.png");
		text_reward->setString(StringUtils::format("X%d", contestAward->iAward));

		PlatformLogic()->loginResult.iLotteries += contestAward->iAward;
		//img_reward->setVisible(false);
		//text_reward->setVisible(false);
		//text_send->setVisible(true);
	}
	else
	{
		img_reward->loadTexture("platform/match/res/reward/lq.png");
		text_reward->setString(StringUtils::format("X%d", contestAward->iAward));

		PlatformLogic()->loginResult.iLotteries += contestAward->iAward;
		img_reward->setVisible(false);
		text_reward->setVisible(false);
		//text_send->setVisible(true);
	}

	_Image_Share = dynamic_cast<ImageView*>(layout->getChildByName("Image_Share"));
	_Image_Share->setVisible(false);

	//分享时显示的头像
	auto user = UserInfoModule()->findUser(PlatformLogic()->loginResult.dwUserID);
	auto img_head = dynamic_cast<ImageView*>(_Image_Share->getChildByName("Image_head"));
	GameUserHead*	userHead = GameUserHead::create(img_head);
	userHead->show();
	userHead->loadTexture("platform/common/boy.png");
	if (user)
	{
		userHead->setHeadByFaceID(user->bLogoID);
		userHead->loadTextureWithUrl(user->headUrl);
	}

	//分享时显示的昵称
	auto Text_name = dynamic_cast<Text*>(_Image_Share->getChildByName("Text_name"));
	Tools::TrimSpace((CHAR*)user->nickName);
	string strName = user->nickName;
	if (strName.length() > 12)
	{
		strName.erase(13, strName.length());
		strName.append("...");
	}
	Text_name->setString(GBKToUtf8(strName));

	//分享时候的名次
	auto Text_rank = dynamic_cast<Text*>(_Image_Share->getChildByName("Text_rank"));
	Text_rank->setString(StringUtils::format(GBKToUtf8("第%d名"), contestAward->iRank));

	time_t tt = time(NULL);
	struct tm* Time = localtime(&tt);
	char BeginTime[64];
	strftime(BeginTime, sizeof(BeginTime), "%Y-%m-%d %H:%M", Time);

	// 时间
	auto text_time = dynamic_cast<Text*>(layout->getChildByName("Text_time"));
	text_time->setString(BeginTime);

	// 退出按钮
	auto btn_exit = dynamic_cast<Button*>(layout->getChildByName("Button_exit"));
	btn_exit->addClickEventListener([=](Ref*){
		RoomLogic()->close();
		//GamePlatform::createPlatform();
		//更换返回大厅跳转比赛场
		GamePlatform::returnPlatformNode(MATCH);
	});

	// 分享按钮
	auto btn_share = dynamic_cast<Button*>(layout->getChildByName("Button_share"));
	btn_share->addClickEventListener([=](Ref*){
		auto shareLayer = GameShareLayer::create();
		shareLayer->setName("shareLayer");
		shareLayer->SetShareInfo("", "", "");
		shareLayer->show();
		shareLayer->onCloseShareCallBack = [this](){
			_Image_Share->setVisible(true);
			_Image_Share->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=](){
				_Image_Share->setVisible(false);
			}), nullptr)); };
	});
	// 动画
	auto timeline = CSLoader::createTimeline(MATCH_AWARD_CSB);
	node->runAction(timeline);
	timeline->play("animation0", false);

	RankingType type = Other;
	if (contestAward->iRank < 4)
	{
		type = (RankingType)contestAward->iRank;
	}
	
	timeline->setAnimationEndCallFunc("animation0", [=](){

		timeline->play("animation1", false);

		HNAudioEngine::getInstance()->playEffect("platform/sound/match/playview/diploma_sound_first.mp3");

		auto sprite = dynamic_cast<Sprite*>(layout->getChildByName("Sprite_win"));
		auto img_rank = dynamic_cast<ImageView*>(sprite->getChildByName("Image_rank"));
		auto img_anim = dynamic_cast<ImageView*>(sprite->getChildByName("Image_anim"));

		img_rank->loadTexture(StringUtils::format("platform/match/res/reward/award%d_txt.png", type));
		img_anim->loadTexture(StringUtils::format("platform/match/res/reward/award%d_anim.png", type));
		img_anim->setVisible(true);
		img_anim->runAction(Sequence::create(DelayTime::create(0.3f), ScaleBy::create(0.3f, 0.2f), Hide::create(),
			CallFunc::create([=](){
			img_rank->setVisible(true);	

			auto img_seal = dynamic_cast<ImageView*>(layout->getChildByName("Image_seal"));
			img_seal->setScale(1.5f);
			//img_seal->setVisible(true);
			img_seal->runAction(Sequence::create(DelayTime::create(0.8f), ScaleTo::create(0.15f, 1.0f), nullptr));

			HNAudioEngine::getInstance()->playEffect("platform/sound/match/playview/diploma_sound_seal.mp3");
		}), nullptr));
	});
}

//更新排行榜界面处理
void GameMatchController::showMatchRank()
{
	if (_list_rank == nullptr)
	{
		return;
	}
	_img_RankList->setVisible(true);

	if (_Vec_RankData.size() == 0)
	{
		return;
	}
	else
	{
		//sort(_Vec_RankData.begin(), _Vec_RankData.end(), sortFun);
	}

	Widget* item = _list_rank->getItem(0);
	if (item != nullptr)
	{
		_list_rank->removeAllItems();
	}

	//根据排行榜数据进行压入排行榜成员更新数据
	for (int i = 0; i < _Vec_RankData.size(); i++)
	{
		auto node = CSLoader::createNode("platform/match/match_controllerNode.csb");
		Layout* panel_clone = dynamic_cast<Layout*>(node->getChildByName("panel_clone"));
		if (_Vec_RankData[i].iRankNum <= 3)
		{
			ImageView* img_rank = dynamic_cast<ImageView*>(panel_clone->getChildByName("img_rank"));
			img_rank->loadTexture(StringUtils::format("platform/match/res/matchcontroller/rank_%d.png", _Vec_RankData[i].iRankNum));
			Text* text_rank = dynamic_cast<Text*>(panel_clone->getChildByName("text_rank"));
			text_rank->setVisible(false);
		}
		else
		{
			ImageView* img_rank = dynamic_cast<ImageView*>(panel_clone->getChildByName("img_rank"));
			img_rank->setVisible(false);
			//ImageView* img_rank_other = dynamic_cast<ImageView*>(panel_clone->getChildByName("img_rank_other"));
			//img_rank_other->setVisible(true);
			Text* text_rank = dynamic_cast<Text*>(panel_clone->getChildByName("text_rank"));
			text_rank->setString(to_string(_Vec_RankData[i].iRankNum));
			text_rank->setVisible(true);
		}

		//用户名字
		Text* text_name = dynamic_cast<Text*>(panel_clone->getChildByName("text_name"));
		Tools::TrimSpace(_Vec_RankData[i].szUserName);
		string strName = _Vec_RankData[i].szUserName;
		if (strName.length() > 12)
		{
			strName.erase(13, strName.length());
			strName.append("...");
		}
		text_name->setString(GBKToUtf8(strName));

		//用户积分
		Text* text_score = dynamic_cast<Text*>(panel_clone->getChildByName("text_score"));
		text_score->setString(to_string(_Vec_RankData[i].i64ContestScore));
		text_score->setVisible(!_Vec_RankData[i].iIsKick);

		//是否淘汰
		Text* text_eliminate = dynamic_cast<Text*>(panel_clone->getChildByName("text_eliminate"));
		text_eliminate->setVisible(_Vec_RankData[i].iIsKick);

		//当前局数状态
		Text* text_progress = dynamic_cast<Text*>(panel_clone->getChildByName("text_progress"));
		text_progress->setString(StringUtils::format(GBKToUtf8("%d/%d局"), _Vec_RankData[i].iRunNum, _Vec_RankData[i].iAllRunNum));
		text_progress->setVisible(!_Vec_RankData[i].iIsKick);

		//奖励类型
		ImageView* img_reward_type = dynamic_cast<ImageView*>(panel_clone->getChildByName("img_reward_type"));
		img_reward_type->setVisible(_Vec_RankData[i].pAwards);

		//奖励数目
		Text* text_reward = dynamic_cast<Text*>(panel_clone->getChildByName("text_reward"));
		//text_reward->setString(to_string(_Vec_RankData[i].pAwards));
		text_reward->setVisible(_Vec_RankData[i].pAwards);

		//根据奖励数目进行区分类型
		if (_Vec_RankData[i].pAwardTypes == 0)
		{
			img_reward_type->loadTexture("platform/match/res/matchcontroller/jb.png");
			text_reward->setString(StringUtils::format("X%d", _Vec_RankData[i].pAwards));
		}
		else if (_Vec_RankData[i].pAwardTypes == 1)
		{
			img_reward_type->loadTexture("platform/match/res/matchcontroller/hb.png");
			text_reward->setString(GBKToUtf8(StringUtils::format("%d元",_Vec_RankData[i].pAwards)));
		}
		else if (_Vec_RankData[i].pAwardTypes == 2)
		{
			img_reward_type->loadTexture("platform/match/res/matchcontroller/fk.png");
			text_reward->setString(GBKToUtf8(StringUtils::format("%d张", _Vec_RankData[i].pAwards)));
		}

		panel_clone->removeFromParentAndCleanup(true);
		_list_rank->pushBackCustomItem(panel_clone);
	}
}

void GameMatchController::clearAnimation()
{
	if (getChildByName("wait"))
	{
		if (!_isMatchWaitGame)
		{
			_nowTime = 0;
		}
		if (_Text_Time)
		{
			unschedule(schedule_selector(GameMatchController::updateTimeCount));
		}
		getChildByName("wait")->removeFromParent();
		_isMatchWaitGame = false;
	}

	if (getChildByName("promotion"))
	{
		getChildByName("promotion")->removeFromParent();
	}

	if (getChildByName("queue"))
	{
		getChildByName("queue")->removeFromParent();
		unschedule(schedule_selector(GameMatchController::updateTimeOut));
	}
}
