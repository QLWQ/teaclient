/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameMatchRegistration.h"
#include "HNPlatformLogic/HNGameCreator.h"
#include "HNData/HNMatchInfoCache.h"
#include "GameMatchWaiting.h"
#include "../GameReconnection/Reconnection.h"
#include "../GameUpdate/GameResUpdate.h"
#include "../GameShop/ExchangeDiamonds.h"
#include "../GameShop/GameStoreLayer.h"
#include "UMeng/UMengSocial.h"
#include "../GameLocation/GameLocation.h"
#include "../GameBaseLayer/GamePlatform.h"
#include "../GameChildLayer/GameShareLayer.h"

namespace HN
{
	static const char* MATCH_PATH		= "platform/matchRegist/registration_Node.csb";
	static const char* MATCH_ITEM_PATH	= "platform/matchRegist/matchItem_Node.csb";
	static const char* REWARD_ITEM_PATH = "platform/matchRegist/rewardItem_Node.csb";
	static const char* RECORF_PATH = "platform/matchRegist/Record_Node.csb";
	static const char* RECORD_ITEM_PATH = "platform/matchRegist/RecordItem_Node.csb";
	static const char* GAME_PAGESPROMPT_PATH = "platform/common/yuandian1.png";
	static const char* FULL_BG_PATH = "platform/matchRegist/res/registerItem/zhuozidikuang2.png";
	static const char* TIME_BG_PATH = "platform/matchRegist/res/registerItem/zhuozidikuang.png";
	static const char* FULL_ICON_PATH = "platform/matchRegist/res/registerItem/jiangbei.png";
	static const char* TIME_ICON_PATH = "platform/matchRegist/res/registerItem/naozhong.png";
	static const char* STATE_PATH1 = "platform/matchRegist/res/matchItem/bm_ybm_an2.png";			//消费报名
	static const char* STATE_PATH2 = "platform/matchRegist/res/matchItem/bm_ybm_an1.png";			//已报名状态
	static const char* STATE_PATH3 = "platform/matchRegist/res/matchItem/bm_ybm_an4.png";			//无法报名状态
	static const char* STATE_PATH4 = "platform/matchRegist/res/matchItem/mfbm_an.png";				//免费报名可点击
	static const char* STATE_PATH5 = "platform/matchRegist/res/matchItem/bsjxz_an.png";				//比赛已开始
	static const char* STATE_PATH6 = "platform/matchRegist/res/matchItem/bm_ybm_an5.png";				//已报名状态（定时赛提示比赛前两分钟进入）
	static const char* STATE_PATH7 = "platform/matchRegist/res/matchItem/jr_an.png";				//进入等待比赛开始（定时赛提示比赛前两分钟进入）
	static const char* ICON_PATH1 = "platform/matchRegist/res/registerItem/renshutubiao.png";
	static const char* ICON_PATH2 = "platform/matchRegist/res/registerItem/gerenhongse.png";
	static const char* EXCHANGE_PATH = "platform/matchRegist/Exchange_Node.csb";

	static const INT IS_MATCHREGISTRATION = 99;
	static const INT IS_MATCHTIMESTRATION = 98;
	//设置定时赛开启进入时间
	static const INT IS_MATCHTIME = 2;

	//设置定时器开启倒计时（时，分）
	static const INT MATCH_TIME_H = 1;
	static const INT MATCH_TIME_M = 0;

	GameMatchRegistration::GameMatchRegistration()
	{
		m_iContestID = 0;
		m_bFull = 0;
		m_socketMessage = nullptr;
		_roomLogic = new HNRoomLogicBase(this);
	}

	GameMatchRegistration::~GameMatchRegistration()
	{
		HNHttpRequest::getInstance()->removeObserver(this); 
		stopAllActions();
		PlatformLogic()->removeEventSelector(MDM_GP_LIST, ASS_GP_LIST_CONTEST);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_CONTEST_APPLY);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_GET_APPLY_NUM);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_CONTEST_AWARDINFO);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_CONTEST_GET_CONTESTAWARDLIST);
		//PlatformLogic()->removeEventSelector(MDM_GP_GETBUG, 1);
		PlatformLogic()->removeEventSelector(MDM_GP_MONEY_CHANGE, ASS_GP_JEWELSTOCOIN);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_GET_CONTEST_ROOMID);
		_matchVec.clear();

		RoomLogic()->removeEventSelector(MDM_GR_USER_ACTION, ASS_GR_CONTEST_APPLY);
		_roomLogic->stop();
		HN_SAFE_DELETE(_roomLogic);
	}
	
	bool GameMatchRegistration::init()
	{
		if (!HNLayer::init()) return false;

		float scalex = 1280 / _winSize.width;
		float scaley = 720 / _winSize.height;
		
		auto node = CSLoader::createNode(MATCH_PATH);
		node->setPosition(_winSize / 2);
		addChild(node);

		auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_match"));
		layout->setSwallowTouches(false);
		auto imgBG = dynamic_cast<Layout*>(layout->getChildByName("Image_BG"));
		imgBG->setSwallowTouches(false);
		imgBG->setScale(_winSize.width / 1280, _winSize.height / 720);
		auto Image_matchs = dynamic_cast<ImageView*>(imgBG->getChildByName("Image_matchs"));
		_panel_di = dynamic_cast<Layout*>(Image_matchs->getChildByName("Panel_di"));

		_applyLayer = dynamic_cast<Layout*>(layout->getChildByName("Panel_regist"));
		_applyLayer->setScale(_winSize.width / 1280, _winSize.height / 720);
		_applyLayer->setVisible(false);
		_applyLayer->setZOrder(0);
		
		if (_winSize.width / _winSize.height < 1.78f)
		{
			for (auto child : _applyLayer->getChildren())
			{
				child->setScale(scalex, scaley);
			}
		}
		else
		{
			for (auto child : _applyLayer->getChildren())
			{
				child->setScale(0.9f, scaley * 0.9f);
			}
		}

		auto layout_top = dynamic_cast<Layout*>(imgBG->getChildByName("Panel_top"));
		layout_top->setScale(scalex, scaley);

		auto closeBtn = dynamic_cast<Button*>(layout_top->getChildByName("Button_close"));
		closeBtn->addClickEventListener([=](Ref*){
			closeBtn->setEnabled(false);
			close(); 
		});

		//增加房卡
		auto addRoomCard = dynamic_cast<Button*>(layout_top->getChildByName("Button_add_lottery"));
		addRoomCard->addClickEventListener([=](Ref*){
			GameStoreLayer* storeLayer = GameStoreLayer::createGameStore(_delegate);
			storeLayer->setName("storeLayer");
			storeLayer->setChangeDelegate(_delegate);
			storeLayer->onCloseCallBack = [this]() {
				updateUserInfo();
			};
			storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, 100, 1001);
			storeLayer->ClickChangeStore(1);
		});

		//获奖记录
		auto match_record = dynamic_cast<Button*>(layout_top->getChildByName("Button_match_record"));
		match_record->addClickEventListener([=](Ref*){
			_vec_MatchRecord.clear();
			MSG_GP_I_GetContestAwardlist get;
			get.UserID = PlatformLogic()->loginResult.dwUserID;
			PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_CONTEST_GET_CONTESTAWARDLIST, (void*)&get, sizeof(get),
				HN_SOCKET_CALLBACK(GameMatchRegistration::getMatchRecordCallback, this));
		});

		// 金币
		auto img_gold = dynamic_cast<ImageView*>(layout_top->getChildByName("Image_gold"));
		_text_gold = dynamic_cast<TextAtlas*>(img_gold->getChildByName("AtlasLabel_value"));

		// 奖券
		auto img_lottery = dynamic_cast<ImageView*>(layout_top->getChildByName("Image_lottery"));
		_text_lottery = dynamic_cast<TextAtlas*>(img_lottery->getChildByName("AtlasLabel_value"));

		//背包按钮
		auto btn_Bag = dynamic_cast<Button*>(Image_matchs->getChildByName("Button_Bag"));
		btn_Bag->addClickEventListener([=](Ref*){
			BagPage();
		      });
		//兑换记录按钮
		auto btn_Record = dynamic_cast<Button*>(Image_matchs->getChildByName("Button_Record"));
		btn_Record->addClickEventListener([=](Ref*){
			RecordPage();
		});
		// 房间列表
		_matchList = dynamic_cast<ui::ScrollView*>(layout->getChildByName("ScrollView_match"));
		_matchList->setSwallowTouches(true);
		//_matchList->setCustomScrollThreshold(15);//如果未指定该值，则在页面的半宽度滚动时，pageView将翻页
		//_matchList->scrollToPage(0);//滚动到具有给定索引的页面。
		//_matchList->removeAllPages();//	删除PageView的所有页面。
		//_matchList->setIndicatorEnabled(true);//切换页面指示器已启用。
		//_matchList->setIndicatorIndexNodesTexture(GAME_PAGESPROMPT_PATH);//为索引节点设置纹理。
		//_matchList->setIndicatorIndexNodesScale(0.85f);//设置页面指示符的索引节点的比例
		//_matchList->setIndicatorSelectedIndexColor(Color3B::WHITE);//设置页面指示器所选索引的颜色。
		//_matchList->setIndicatorPosition(Vec2(_matchList->getContentSize().width * 0.5f, _matchList->getContentSize().height * 0.04f));//使用锚点设置页面指示器的位置

		// 更新显示玩家信息
		updateUserInfo();

		// 获取比赛列表信息
		getMatchList();

		// 检测是否有正在进行的比赛
		Reconnection::getInstance()->getCutRoomInfo(1);

		// 间隔2秒循环查询列表信息
		this->runAction(RepeatForever::create(Sequence::create(DelayTime::create(2.f), CallFunc::create([=](){
			getMatchList();
		}), nullptr)));

		
		_numberForCell = {5,7,10,15,20,25,30,50,100,200};

		_Exchangenode = CSLoader::createNode(EXCHANGE_PATH);
		_Exchangenode->setPosition(Vec2(640, 360));
		addChild(_Exchangenode);
		_Exchangenode->setVisible(false);
		_Exchangenode->setScale(0);

		//兑换二级界面
		_TwoExchangeNode = CSLoader::createNode("platform/matchRegist/twoExchange_Node.csb");
		_TwoExchangeNode->setPosition(Vec2(640, 360));
		addChild(_TwoExchangeNode);
		_TwoExchangeNode->setVisible(false);
		_TwoExchangeNode->setScale(0);

		//背包页面
		_Bag_match = CSLoader::createNode("platform/createRoomUI/createUi/matchBag.csb");
		CCAssert(_Bag_match != nullptr, "null");
		_Bag_match->setPosition(Vec2(640, 360));
		addChild(_Bag_match);
		_Bag_match->setVisible(false);
		_Bag_match->setScale(0);
		_Bag_match->setZOrder(100000);

		auto PlaneBg = dynamic_cast<Layout*>(_Bag_match->getChildByName("Panel_Bg"));
		auto PlaneXianJin = dynamic_cast<ui::ScrollView*>(PlaneBg->getChildByName("ScrollView_XianJin"));

		PlaneXianJin->setVisible(false);
		//发送请求背包信息
		_mapBag.clear();
		//PlatformLogic()->sendData(MDM_GP_GETBUG, 1, 0, 0,
		//	HN_SOCKET_CALLBACK(GameMatchRegistration::getBagList, this));

		//获奖记录
		_RecordMatch = CSLoader::createNode("platform/matchRegist/WinRecordLayer.csb");
		CCAssert(_RecordMatch != nullptr, "null");
		addChild(_RecordMatch);
		_RecordMatch->setVisible(false);
		_RecordMatch->setZOrder(100000);

		_RecordShare = dynamic_cast<ImageView*>(_RecordMatch->getChildByName("Image_share"));
		_RecordShare->setVisible(false);

		//规则页面
		_RuleMatch = CSLoader::createNode("platform/matchRegist/MatchRule_Node.csb");
		CCAssert(_RuleMatch != nullptr, "null");
		_RuleMatch->setPosition(Vec2(640, 360));
		addChild(_RuleMatch);
		_RuleMatch->setVisible(false);
		_RuleMatch->setZOrder(100000);
		//奖励规则页面
		auto Scrollreward = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_reward"));
		Scrollreward->setVisible(false);

		//奖励规则列表
		auto ListReward = dynamic_cast<ListView*>(_RuleMatch->getChildByName("ListView_reward"));
		ListReward->setVisible(true);

		//通用规则页面
		auto Scrollcurrency = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_currency"));
		Scrollcurrency->setVisible(false);
		

		return true;
	}
	bool GameMatchRegistration::getBagList(HNSocketMessage* socketMessage)
	{
		UINT count = socketMessage->objectSize / sizeof(MSG_GP_O_BugList_Data);
		MSG_GP_O_BugList_Data * tmp = (MSG_GP_O_BugList_Data *)socketMessage->object;
		while (count-- > 0)
		{
			
			_BagDinDan.push_back(tmp->danhao);
			_mapBag.insert(pair<std::string, int>(tmp->danhao, tmp->reward));
		}

		//创建兑换列表
		if (_exchangeItems == nullptr)		createExchange();
		
		return true;
	}

	void GameMatchRegistration::createExchange()
	{
		_exchangeItems = TableView::create(this, _panel_di->getContentSize());
		_exchangeItems->setDirection(TableView::Direction::HORIZONTAL);
		_exchangeItems->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		_exchangeItems->setDelegate(this);
		_exchangeItems->reloadData();
		_panel_di->addChild(_exchangeItems);
	}
	//添加Cell
	TableViewCell* GameMatchRegistration::tableCellAtIndex(TableView *table, ssize_t idx)
	{
		TableViewCell* cell = table->dequeueCell();
		if (!cell)
		{
			cell = new TableViewCell();
		}
		else
		{
			cell->removeAllChildren();
		}
		

		auto index = idx + 1;
		string name = StringUtils::format("platform/matchRegist/res/registerItem/money_%d.png", index);

		if (_Selectedreward == Selectedreward::GOLD)
			name = StringUtils::format("platform/matchRegist/res/registerItem/gold_%d.png", index);
		
		bool isExist = FileUtils::getInstance()->isFileExist(name);
		if (isExist)
		{
			auto items = Sprite::create(name);
			items->setAnchorPoint(Vec2(0.5, 0.5));
			items->setPosition(Vec2(100, 70));
			items->setName("okey");
			cell->addChild(items);
		}
		
		return cell;
	}

	//设置cell数量
	ssize_t GameMatchRegistration::numberOfCellsInTableView(TableView* table)
	{
		int numberCell = 0;
		if (_Selectedreward == Selectedreward::GOLD)
			numberCell = 4;
		else
			numberCell = 10;
		return numberCell;
	}

	//设置cell大小
	Size GameMatchRegistration::tableCellSizeForIndex(TableView* table, ssize_t idx)
	{
		return Size(200,140);
	}

	//设置触摸函数
	void GameMatchRegistration::tableCellTouched(TableView* table, TableViewCell* cell)
	{
		int index = cell->getIdx();
		if (_Selectedreward == Selectedreward::GOLD)
		{
			if (index == 3)
			{
				auto exchangeMoneyLayer = ExchangeDiamonds::createExchangeMoneyLayer();
				exchangeMoneyLayer->open(ACTION_TYPE_LAYER::FADE, this->getParent(), Vec2::ZERO, 1000000 + 1, 10, true, [=](Ref*) {
					exchangeMoneyLayer->close();
				});
				exchangeMoneyLayer->setChangeDelegate(_delegate);

			}
			else
			{
				std::vector<int> goldnum = {5,10,20};
				MSG_GP_I_ChangeRequest change;
				change.iChangeJewels = goldnum[index];
				change.iUserID = PlatformLogic()->loginResult.dwUserID;
				PlatformLogic()->sendData(MDM_GP_MONEY_CHANGE, ASS_GP_JEWELSTOCOIN, &change, sizeof(change),
					HN_SOCKET_CALLBACK(GameMatchRegistration::exchangeDiamondsCallBack, this));
			}
			
		}
		else
		    inerFaceExchange(index);
	}
	void GameMatchRegistration::setChangeDelegate(MoneyChangeNotify* delegate)
	{
		_delegate = delegate;
	}
	//房卡兑换回调
	bool  GameMatchRegistration::exchangeDiamondsCallBack(HNSocketMessage* socketMessage)
	{
		CCAssert(sizeof(MSG_GP_O_ChangeRespones) == socketMessage->objectSize, "MSG_GP_O_ChangeRespones is error.");
		MSG_GP_O_ChangeRespones* exchangeResult = (MSG_GP_O_ChangeRespones*)socketMessage->object;

		//(0:兑换成功；1：用户不存在;2：兑换功能关闭;3：金币不足；4：钻石不足)
		switch (socketMessage->messageHead.bHandleCode)
		{
		case 0:
		{
				  PlatformLogic()->loginResult.i64Money = exchangeResult->i64Money;
				  PlatformLogic()->loginResult.iJewels = exchangeResult->iJewels;

				  _delegate->walletChanged();
				 

				  GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换成功！"));
		}break;
		case 1:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("用户不存在！"));
			break;
		case 2:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换功能关闭！"));
			break;
		case 3:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("金币不足！"));
			break;
		case 4:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换所需房卡不足！"));
			break;
		default:
			break;
		}

		return true;
	}
	//兑换界面
	void GameMatchRegistration::inerFaceExchange(int index)
	{
		auto idx = index  + 1;
		_Exchangenode->setVisible(true);
		_Exchangenode->runAction(ScaleTo::create(0.1f, 1));

		auto PlaneBg = dynamic_cast<Layout*>(_Exchangenode->getChildByName("Panel_Bg"));
		
		//退出按钮
		auto btnExit = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Exit"));
		btnExit->addClickEventListener([=](Ref*){
			_Exchangenode->runAction(Sequence::create(ScaleTo::create(0.1f, 0), CallFunc::create([=](){
				_Exchangenode->setVisible(false);
			}), nullptr));
		});
		//底框类型
		auto Dikuang = dynamic_cast<Sprite*>(PlaneBg->getChildByName("Sprite_DiKuang"));

		string nameDi = "";
		//if (idx <= 4)
			nameDi = StringUtils::format("platform/matchRegist/res/registerItem/Exchange_XianJin.png");
		//else
			//nameDi = StringUtils::format("platform/matchRegist/res/registerItem/Exchange_HuaFei.png");
		Dikuang->setTexture(nameDi);
		//确定按钮
		auto btnSure = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Sure"));
		btnSure->addClickEventListener([=](Ref*){		
			auto count = idx - 1;	
			isHaveExchange(_numberForCell.at(count));		
		});
		//红包类型
		auto spLottery = dynamic_cast<Sprite*>(PlaneBg->getChildByName("Sprite_Lottery"));
		string name = StringUtils::format("platform/matchRegist/res/registerItem/Lottry_%d.png", idx);
		bool isExist = FileUtils::getInstance()->isFileExist(name);
		if (isExist)
		{
			spLottery->setTexture(name);
		}
		auto AmountCount = idx - 1;

		//兑换金额
		auto Amount = dynamic_cast<Text*>(PlaneBg->getChildByName("Text_Monay"));
		Amount->setString(StringUtils::format("%d%s", _numberForCell[AmountCount], GBKToUtf8("元现金")));
	}
	bool GameMatchRegistration::isHaveExchange(int number)
	{
		bool b_exchange = false;
		if (sizeof(_BagDinDan) != 0)
		{

			for ( auto dingdan : _BagDinDan)
			{			
				auto iter = _mapBag.find(dingdan);
				if (iter != _mapBag.end()) 
				{
					if (_mapBag[dingdan] == number)
					{
						b_exchange = true;
						_DingDanHao = dingdan;
					}
					
				}
			}
		}
		ExchangeIng(b_exchange);

		return true;
	}
	void GameMatchRegistration::creatHttprequest(string data)
	{
		std::string url = "http://alipay.qp888m.com/wxhongbao/wxhongbao.php";
		//url.append("?szOrderid=" + data);
		//url.append(StringUtils::format("&UserID=%d", PlatformLogic()->loginResult.dwUserID));
		std::string param = StringUtils::format("{ \"szOrderid\":%s, \"UserID\" : %d }", GBKToUtf8(data), PlatformLogic()->loginResult.dwUserID);
			//"{\"szOrderid\":\"1967390747\",\"UserID\" :10515}");
		network::HttpRequest* request = new (std::nothrow) network::HttpRequest();
		request->setUrl(url.c_str());
		request->setRequestType(network::HttpRequest::Type::POST);
		request->setRequestData(param.c_str(), param.size());
		request->setResponseCallback(CC_CALLBACK_2(GameMatchRegistration::onHttpResponseBag, this));
		network::HttpClient::getInstance()->send(request);
		request->release();
	}
	void GameMatchRegistration::onHttpResponseBag(HttpClient *sender, HttpResponse *response)
	{
		if (!response->isSucceed())
		{
			return;
		}

		std::vector<char> *buffer = response->getResponseData();
	    char isSucceed = 1;
		int count = 0;

		for (auto success : *buffer)
		{
			if (count == 8)
			{
				isSucceed = success;
			}
			count += 1;
		}
	    if (isSucceed == '0')
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换成功..."));
		
			vector<string>::iterator iter = _BagDinDan.begin();
			while (iter != _BagDinDan.end())
			{
				if (*iter == _DingDanHao)
					iter = _BagDinDan.erase(iter);
				else
					iter++;
			}
			map<string, int >::iterator l_it = _mapBag.find(_DingDanHao);

			if (l_it != _mapBag.end())	_mapBag.erase(l_it);
			

		}
		else if (isSucceed == '1')
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换失败，兑换券不存在..."));
		}
		else if (isSucceed == '2')
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换失败，兑换卷已兑换过..."));
		}
		else if (isSucceed == '3')
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换失败，未绑定微信公众号..."));
		}
	}
	void GameMatchRegistration::ExchangeIng(bool condition)
	{
		_Exchangenode->runAction(Sequence::create(ScaleTo::create(0.1f, 0), CallFunc::create([=](){
			_Exchangenode->setVisible(false);
		}), nullptr));

		
		if (condition == true)
		{
			//_TwoExchangeNode->setVisible(true);
			//_TwoExchangeNode->runAction(ScaleTo::create(0.1f, 1));
			//auto PlaneBg = dynamic_cast<Layout*>(_TwoExchangeNode->getChildByName("Panel_Bg"));	
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("请稍候..."), 25);
			creatHttprequest(_DingDanHao);
			
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("您的兑换券不足..."));
		}
		
	}
	//背包页面
	void GameMatchRegistration::BagPage()
	{

		_Bag_match->setVisible(true);
		_Bag_match->runAction(ScaleTo::create(0.1f, 1));

		auto PlaneBg = dynamic_cast<Layout*>(_Bag_match->getChildByName("Panel_Bg"));
		auto btExit = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Back"));
		//退出按钮
		btExit->addClickEventListener([=](Ref*){
			_Bag_match->runAction(Sequence::create(ScaleTo::create(0.1f, 0), CallFunc::create([=](){
				_Bag_match->setVisible(false);
			}), nullptr));
		});

		auto checkBox = (CheckBox*)Helper::seekWidgetByName(PlaneBg, "CheckBox_switch");
		checkBox->addEventListener(CC_CALLBACK_2(GameMatchRegistration::checkBoxCallback, this));

		_PlaneXianJin = dynamic_cast<ui::ScrollView*>(PlaneBg->getChildByName("ScrollView_XianJin"));
		_PlaneXianJin->setSwallowTouches(true);
		_PlaneHuaFei = dynamic_cast<ui::ScrollView*>(PlaneBg->getChildByName("ScrollView_HuaFei"));
		_PlaneHuaFei->setSwallowTouches(true);
		auto arrTyprXJ = {5,7,10,15,20,25,30,50,100,200};
		int count = 0;
		//红包数量
		for (auto number : arrTyprXJ)
		{
			count += 1;
			string name = StringUtils::format("Text_%d", count);
			auto SpriteXJ = dynamic_cast<Text*>(_PlaneXianJin->getChildByName(name));
			auto BagCount = isHaveBag(number);
			SpriteXJ->setString(StringUtils::format("x%d", BagCount));
		}


	}
	int GameMatchRegistration::isHaveBag(int number)
	{
		int count = 0;
		if (sizeof(_BagDinDan) != 0)
		{

			for (auto dingdan : _BagDinDan)
			{
				auto iter = _mapBag.find(dingdan);
				if (iter != _mapBag.end())
				{
					if (_mapBag[dingdan] == number)
					{	
						count += 1;
					}

				}
			}
		}
		return count;
	}
	void GameMatchRegistration::checkBoxCallback(cocos2d::Ref * ref, CheckBox::EventType type)
	{
		
		switch (type)
		{
		case cocos2d::ui::CheckBox::EventType::SELECTED:
			_PlaneHuaFei->setVisible(true);
			_PlaneXianJin->setVisible(false);
			break;
		case cocos2d::ui::CheckBox::EventType::UNSELECTED:
			_PlaneXianJin->setVisible(true);
			_PlaneHuaFei->setVisible(false);
			break;
		default:
			break;
		}

	}
	//规则页面
	void GameMatchRegistration::RulePage(MSG_GP_ContestApplyInfo* info)
	{
		_RuleMatch->setVisible(true);

		/*MSG_GP_GetContestRoomID get;
		get.iContestID = info->iContestID;
		get.iUserID = PlatformLogic()->loginResult.dwUserID;
		PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_GET_CONTEST_ROOMID, (void*)&get, sizeof(get),
			HN_SOCKET_CALLBACK(GameMatchRegistration::getMatchInfoCallback, this));*/

		getMatchInfo(info->iContestID,true);

		auto PlaneBg = dynamic_cast<Layout*>(_RuleMatch->getChildByName("Panel_bg"));
		
		//规则页面
		auto Scrollreward = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_currency"));
		Scrollreward->setVisible(false);

		//奖励规则列表
		auto ListReward = dynamic_cast<ListView*>(_RuleMatch->getChildByName("ListView_reward"));
		ListReward->setVisible(true);

		//游戏场信息
		string str_game = "";
		if (info->iGameID == 20171121)
		{
			str_game = "游金麻将";
		}
		else if (info->iGameID == 20171123)
		{
			str_game = "晋江麻将";
		}
		else if (info->iGameID == 10100003)
		{
			str_game = "斗地主";
		}
		else if (info->iGameID == 12345678)
		{
			str_game = "跑得快";
		}
		auto Text_gameName = dynamic_cast<Text*>(Scrollreward->getChildByName("Text_name"));
		Text_gameName->setString(GBKToUtf8(StringUtils::format("%s(%s)", info->szRoomName, str_game.c_str())));

		//比赛时间(无时间限制还是限制报名时间？)
		auto Text_gameTime = dynamic_cast<Text*>(Scrollreward->getChildByName("Text_time"));
		std::string strRule;
		time_t Time = info->ContestBeginTime;
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		if (p->tm_hour >= 24)
		{
			p->tm_hour -= 24;
		}
		char BeginTime[64];
		strftime(BeginTime, sizeof(BeginTime), "%H:%M", p);

		time_t EndTime = info->ContestEndTime;
		struct tm *ep = gmtime(&EndTime);
		ep->tm_hour += 8;
		if (ep->tm_hour >= 24)
		{
			ep->tm_hour -= 24;
		}
		char EndTimes[64];
		strftime(EndTimes, sizeof(EndTimes), "%H:%M", ep);
		if (p->tm_hour == 0 && ep->tm_hour == 24)
		{
			strRule = GBKToUtf8("无时间限制");
		}
		else
		{
			strRule = StringUtils::format(GBKToUtf8("每日%s—%s报名"), BeginTime, EndTimes);
		}
		if (info->iContestType == 0)
		{
			strRule = strRule + GBKToUtf8(",人满即开.");
		}
		Text_gameTime->setString(strRule);

		    //入场消耗  
		auto Consume = dynamic_cast<Text*>(Scrollreward->getChildByName("Text_consume"));
		Consume->setString(GBKToUtf8(StringUtils::format("X%d", info->iEnterFee)));
		    //消耗类型
		auto ConsumeType = dynamic_cast<Sprite*>(Scrollreward->getChildByName("Sprite_Type"));
		if (info->iEnterType == 0)   
			ConsumeType->setTexture("platform/matchRegist/res/registerItem/bmjb.png");//金币
		else  
			ConsumeType->setTexture("platform/matchRegist/res/registerItem/card.png");//房卡
		if (info->iCanPlayNum > 0)
		{
			ConsumeType->setVisible(false);
			Consume->setPositionX(202.5);
			if (info->iCanPlayNum == 1)
			{
				Consume->setString(GBKToUtf8("每日首次免费"));
			}
			else if (info->iCanPlayNum > 1 && info->iEnterFee == 0)
			{
				Consume->setString(StringUtils::format(GBKToUtf8("每日%d次免费,满%d次后则无法报名"), info->iCanPlayNum, info->iCanPlayNum));
			}
			//是否免费次数之后要消耗		
			if (info->iCanPlayNum == 1 && info->iEnterFee != 0)
			{
				if (info->iEnterType == 0)   //金币
				{
					Consume->setString(StringUtils::format(GBKToUtf8("每日首次免费,第二次起需消耗金币%d"), info->iEnterFee));
				}
				else
				{
					Consume->setString(StringUtils::format(GBKToUtf8("每日首次免费,第二次起需消耗房卡%d张"), info->iEnterFee));
				}
			}
			else if (info->iCanPlayNum > 1 && info->iEnterFee != 0)
			{
				if (info->iEnterType == 0)   //金币
				{
					Consume->setString(StringUtils::format(GBKToUtf8("每日%d次免费,满%d次起需消耗金币%d"), info->iCanPlayNum, info->iCanPlayNum, info->iEnterFee));
				}
				else
				{
					Consume->setString(StringUtils::format(GBKToUtf8("每日%d次免费,满%d次起需消耗房卡%d张"), info->iCanPlayNum, info->iCanPlayNum, info->iEnterFee));
				}
			}
		}
		else
		{
			ConsumeType->setVisible(true);
			Consume->setPositionX(240.7);
		}

		    //比赛类型 (0：人满赛，1：定时赛，2：循环赛)
		auto MatchType = dynamic_cast<Text*>(Scrollreward->getChildByName("Text_Contion"));
		if (info->iContestType == 0)
		{
			MatchType->setString(StringUtils::format(GBKToUtf8("满%d人开赛"), info->iNeedPeople));
		}
		else if (info->iContestType == 1)
		{
			std::string strRule;
			time_t Time = info->beginTime;
			struct tm *p = gmtime(&Time);
			p->tm_hour += 8;
			if (p->tm_hour >= 24)
			{
				p->tm_hour -= 24;
			}
			char BeginTime[64];
			strftime(BeginTime, sizeof(BeginTime), "%H:%M", p);

			strRule = StringUtils::format(GBKToUtf8("%s准时开赛"), BeginTime);

			MatchType->setString(strRule);
		}

		//赛事玩法
		auto Text_play_type = dynamic_cast<Text*>(Scrollreward->getChildByName("Text_play_type"));
		Text_play_type->setString(GBKToUtf8(StringUtils::format("%s,%s", str_game.c_str(), info->szResume)));

		//比赛规则
		auto Text_rule_2 = dynamic_cast<Text*>(Scrollreward->getChildByName("Text_rule_2"));
		Text_rule_2->setString(GBKToUtf8(StringUtils::format("%s", info->szDesc)));

		//通用规则页面
		//auto Scrollcurrency = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_currency"));
		//Scrollcurrency->setVisible(false);
		//奖励规则按钮
		auto CheckBoxReward = (CheckBox*)Helper::seekWidgetByName(PlaneBg, "CheckBox_reward");
		CheckBoxReward->addEventListener(CC_CALLBACK_2(GameMatchRegistration::checkBoxCallbackReward, this));
		CheckBoxReward->setSelected(true);
		CheckBoxReward->setTag(2);
		//通用规则按钮
		auto CheckBoxCurrency = (CheckBox*)Helper::seekWidgetByName(PlaneBg, "CheckBox_currency");
		CheckBoxCurrency->addEventListener(CC_CALLBACK_2(GameMatchRegistration::checkBoxCallbackCurrency, this));
		CheckBoxCurrency->setSelected(false);
		CheckBoxCurrency->setTag(3);
		//退出按钮
		auto Exit = dynamic_cast<Button*>(_RuleMatch->getChildByName("Button_Exit"));
		Exit->addClickEventListener([=](Ref*){
			_RuleMatch->setVisible(false);
		});
	
	}
	void GameMatchRegistration::checkBoxCallbackReward(cocos2d::Ref * ref, cocos2d::ui::CheckBox::EventType type)
	{
		
		//奖励规则页面
		auto Scrollreward = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_reward"));
		//奖励规则列表
		auto ListReward = dynamic_cast<ListView*>(_RuleMatch->getChildByName("ListView_reward"));
		//通用规则页面
		auto Scrollcurrency = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_currency"));
		auto PlaneBg = dynamic_cast<Layout*>(_RuleMatch->getChildByName("Panel_bg"));
		auto CheckBoxCurrency = (CheckBox*)Helper::seekWidgetByName(PlaneBg, "CheckBox_currency");
		switch (type)
		{
		case cocos2d::ui::CheckBox::EventType::SELECTED:
			//Scrollreward->setVisible(true);
			ListReward->setVisible(true);
			Scrollcurrency->setVisible(false);
			CheckBoxCurrency->setSelected(false);
			break;
		case cocos2d::ui::CheckBox::EventType::UNSELECTED:
			
			break;
		default:
			break;
		}
	}
	void GameMatchRegistration::checkBoxCallbackCurrency(cocos2d::Ref * ref, cocos2d::ui::CheckBox::EventType type)
	{
		//奖励规则页面
		auto Scrollreward = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_reward"));
		//奖励规则列表
		auto ListReward = dynamic_cast<ListView*>(_RuleMatch->getChildByName("ListView_reward"));
		//通用规则页面
		auto Scrollcurrency = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_currency"));
		auto PlaneBg = dynamic_cast<Layout*>(_RuleMatch->getChildByName("Panel_bg"));
		auto CheckBoxReward = (CheckBox*)Helper::seekWidgetByName(PlaneBg, "CheckBox_reward");

		//奖励规则
		string str_RuleRecord = "";
		if (_RuleRecordNum == 1)
		{
			str_RuleRecord = "根据玩家积分取排名，第1名的玩家发放奖励。";
		}
		else if (_RuleRecordNum == 2)
		{
			str_RuleRecord = "根据玩家积分取排名，第1名、第2名的玩家发放奖励。";
		}
		else if (_RuleRecordNum == 3)
		{
			str_RuleRecord = "根据玩家积分取排名，第1名、第2名、第3名的玩家发放奖励。";
		}
		else
		{
			str_RuleRecord = StringUtils::format("根据玩家积分取排名，前%d名的玩家发放奖励。", _RuleRecordNum);
		}

		auto Text_rule_5 = dynamic_cast<Text*>(Scrollcurrency->getChildByName("Text_rule_5"));
		Text_rule_5->setString(GBKToUtf8(str_RuleRecord));

		switch (type)
		{
		case cocos2d::ui::CheckBox::EventType::SELECTED:
			//Scrollreward->setVisible(false);
			ListReward->setVisible(false);
			Scrollcurrency->setVisible(true);
			CheckBoxReward->setSelected(false);
			break;
		case cocos2d::ui::CheckBox::EventType::UNSELECTED:

			break;
		default:
			break;
		}
	}
	bool GameMatchRegistration::getMatchInfoCallback(HNSocketMessage* socketMessage)
	{
		m_bCanTouch = true;
		CHECK_SOCKET_DATA_RETURN(MSG_GP_GetContestRoomID_Result, socketMessage->objectSize, true, "MSG_GP_GetContestRoomID_Result size is error");
		MSG_GP_GetContestRoomID_Result* info = (MSG_GP_GetContestRoomID_Result*)socketMessage->object;
		if (m_bRulePage)
		{
			//奖励规则页面
			auto Scrollreward = dynamic_cast<ui::ScrollView*>(_RuleMatch->getChildByName("ScrollView_reward"));
			//奖励规则列表
			auto ListReward = dynamic_cast<ListView*>(_RuleMatch->getChildByName("ListView_reward"));
			//刷新前判断成员清理成员
			Widget* item = ListReward->getItem(0);
			if (item != nullptr)
			{
				ListReward->removeAllItems();
			}
			_RuleRecordNum = 0;
			for (int i = 0; i < 3; i++)
			{
				if (info->tContestInfo.iRankAward[i] == 0)
				{
					break;
				}
				_RuleRecordNum++;
				auto node = CSLoader::createNode("platform/matchRegist/MatchRewardNode.csb");
				auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
				int a = i % 2;
				if (i % 2 == 1)
				{
					img_bg->loadTexture("platform/matchRegist/res/MatchRule/txdt_1.png");
				}

				//排名图标
				auto Image_rank = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_rank"));
				if (i + 1 > 3)
				{
					//只有前三显示图标
					Image_rank->setVisible(false);
					//显示文本排名
					auto Text_top_rank = dynamic_cast<Text*>(img_bg->getChildByName("Text_top_rank"));
					Text_top_rank->setString(to_string(i + 1));
				}
				else
				{
					string str_rank = StringUtils::format("platform/matchRegist/res/MatchRule/jp_%d.png", i + 1);
					Image_rank->loadTexture(str_rank);
				}

				//排名文字
				auto Text_rank = dynamic_cast<Text*>(img_bg->getChildByName("Text_rank"));
				Text_rank->setString(StringUtils::format(GBKToUtf8("第%d名"), i + 1));

				//奖励类型
				auto Image_reward_type = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_reward_type"));

				auto matchReward = dynamic_cast<Text*>(img_bg->getChildByName("Text_reward"));
				if (info->tContestInfo.iAwardType[i] == Selectedreward::GOLD)
				{
					Image_reward_type->loadTexture("platform/matchRegist/res/MatchRule/jb.png");
					matchReward->setString(StringUtils::format(GBKToUtf8("金币X%d"), info->tContestInfo.iRankAward[i]));
				}
				else if (info->tContestInfo.iAwardType[i] == Selectedreward::MONEY)
				{
					Image_reward_type->loadTexture("platform/matchRegist/res/MatchRule/hb.png");
					matchReward->setString(StringUtils::format(GBKToUtf8("现金红包%d元"), info->tContestInfo.iRankAward[i]));
				}
				else if (info->tContestInfo.iAwardType[i] == Selectedreward::ROOMCARD)
				{
					Image_reward_type->loadTexture("platform/matchRegist/res/MatchRule/fk.png");
					matchReward->setString(StringUtils::format(GBKToUtf8("房卡X%d"), info->tContestInfo.iRankAward[i]));
				}
				else
				{
					matchReward->setString(StringUtils::format(GBKToUtf8("%d"), info->tContestInfo.iRankAward[i]));
				}
				img_bg->removeFromParentAndCleanup(true);
				ListReward->pushBackCustomItem(img_bg);
			}
		}
		else
		{//非规则界面，比赛场报名前查询信息

			if (0 == info->tRoomInfo.uRoomID)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("未能获取房间信息，可能人数已满"));
				prompt->setCallBack([=](){
					close();
				});
				return false;
			}
			if (m_socketMessage)
			{
				HNSocketMessage::releaseMessage(m_socketMessage);
				m_socketMessage = nullptr;
			}
			m_socketMessage = new HNSocketMessage(*socketMessage);
			//if (m_bFull)
			//{
			//	// 满人赛登陆备战区自动报名
			//	sendRegistOrRetireInMatch(info->tRoomInfo.iContestID, true);
			//}
			//else
			{
				//满人赛进行记录当前房间操作
				RoomInfoModule()->addRoom(&info->tRoomInfo);
				auto room = RoomInfoModule()->findRoom(info->tRoomInfo.uRoomID);
				RoomLogic()->setRoomRule(room->dwRoomRule);
				RoomLogic()->setSelectedRoom(room);
				//在登录房间前进行资源更新
				checkResUpdate(room->uRoomID, room->uNameID);
			}
		}
		return true;
	}

	bool GameMatchRegistration::getMatchInfoNumCallback(HNSocketMessage* socketMessage)
	{
		//CHECK_SOCKET_DATA_RETURN(MSG_GP_ContestHavePlayNumResult, socketMessage->objectSize, true, "MSG_GP_ContestHavePlayNumResult size is error");
		//MSG_GP_ContestHavePlayNumResult* info = (MSG_GP_ContestHavePlayNumResult*)socketMessage->object;

		//// 更新比赛信息前关闭点击，防止野指针崩溃
		//for (auto btn : _matchVec)
		//{
		//	if (btn != nullptr)
		//	{
		//		auto data = (MSG_GP_ContestApplyInfo*)btn->getUserData();
		//		if (data->iContestID == info->iContestID)
		//		{
		//			auto btnSate = dynamic_cast<Button*>(btn->getChildByName("Button_state"));
		//			if (btnSate != nullptr && info->iHavePlayNum > 0)
		//			{
		//				btnSate->loadTextures(STATE_PATH4, STATE_PATH4, STATE_PATH4);
		//				btnSate->setEnabled(true);
		//			}
		//			else if (btnSate != nullptr && info->iHavePlayNum == 0)
		//			{
		//				btnSate->loadTextures(STATE_PATH3, STATE_PATH3, STATE_PATH3);
		//				btnSate->setEnabled(false);
		//			}
		//		}
		//	}
		//}

		return true;
	}

	bool GameMatchRegistration::getMatchRecordCallback(HNSocketMessage* socketMessage)
	{
		//获奖记录可能有多条，进行的处理是bHandleCode == 0还没有接收结束
		if (socketMessage->messageHead.bHandleCode == 0)
		{
			CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetContestAwardlist, socketMessage->objectSize, true, "MSG_GP_O_GetContestAwardlist size is error");
			MSG_GP_O_GetContestAwardlist* info = (MSG_GP_O_GetContestAwardlist*)socketMessage->object;
			MSG_GP_O_GetContestAwardlist Data;
			Data.FinishTime = info->FinishTime;
			Data.iConTestAward = info->iConTestAward;
			Data.iUserID = info->iUserID;
			Data.iRankNum = info->iRankNum;
			strcpy(Data.szRoomName, info->szRoomName);
			//bool isbool = strcmp(Data.szRoomName, info->szRoomName) ? false : true;
			Data.iGameID = info->iGameID;
			Data.iConTestAwardType = info->iConTestAwardType;
			strcpy(Data.OrderID, info->OrderID);
			Data.IsTake = info->IsTake;
			_vec_MatchRecord.push_back(Data);
		}
		else
		{
			//发送结束开始更新数据
			showMatchRecord();
		}
		
		return true;
	}

	void GameMatchRegistration::RecordPage()
	{
		
		std::string url = StringUtils::format("http://alipay.qp888m.com/app/get_exchangecash_log.php?userid=10515");// , PlatformLogic()->loginResult.dwUserID);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("Record", cocos2d::network::HttpRequest::Type::POST, url);
		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在查询页面信息"), 25);

		auto node = CSLoader::createNode(RECORF_PATH);
		node->setPosition(_winSize / 2);
		addChild(node);

		auto PlaneBg = dynamic_cast<Layout*>(node->getChildByName("Panel_Bg"));
		auto Scroll = dynamic_cast<ui::ScrollView*>(PlaneBg->getChildByName("ScrollView_Item"));
		_RecordList = Scroll;

		auto btnExit = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Exit"));
		btnExit->addClickEventListener([=](Ref*){    node->removeFromParentAndCleanup(true);     });


	}
	void GameMatchRegistration::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
		if (!isSucceed) return;

		if (requestName.compare("Record") == 0)
		{
			rapidjson::Document doc;

			doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

			if (doc.HasParseError() || !doc.IsArray()) {
				return;
			}

				rapidjson::Value& array = doc;
				if (array.Size() > 0)
				{
					_RecordList->setInnerContainerSize(Size(_RecordList->getContentSize().width, array.Size() * 92));
					for (int i = 0; i < array.Size(); i++)
					{
						auto node = CSLoader::createNode(RECORD_ITEM_PATH);
						node->setPosition(_RecordList->getContentSize().width * 0.5, array.Size() * 92 - 44 - 90*i);
						_RecordList->addChild(node);

						auto SpriteBg = dynamic_cast<Sprite*>(node->getChildByName("Sprite_Item"));

						auto SpriteType = dynamic_cast<Sprite*>(SpriteBg->getChildByName("Sprite_Type"));
						std::string name = StringUtils::format("platform/matchRegist/res/registerItem/MoneyTip/xianjin%d.png", array[i]["object"].GetInt());
						if (FileUtils::getInstance()->isFileExist(name))  SpriteType->setTexture(name);

						auto TextTime = dynamic_cast<Text*>(SpriteBg->getChildByName("Text_Time"));
						std::string stime = array[i]["addtime"].GetString();
						TextTime->setString(stime.c_str());
						
						
					}
				}
			}
		else if (requestName.compare("MatchAward") == 0)
		{
			rapidjson::Document doc;

			doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

			if (doc.HasParseError() || !doc.IsObject()) {
				return;
			}

			int code = doc["code"].GetInt();
			if (code == 0)
			{
				string str = doc["msg"].GetString();
				GamePromptLayer::create()->showPrompt(str);

				//领取成功
				_vec_MatchRecord.clear();
				MSG_GP_I_GetContestAwardlist get;
				get.UserID = PlatformLogic()->loginResult.dwUserID;
				PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_CONTEST_GET_CONTESTAWARDLIST, (void*)&get, sizeof(get),
					HN_SOCKET_CALLBACK(GameMatchRegistration::getMatchRecordCallback, this));
			}
			else
			{
				string str = doc["msg"].GetString();
				GamePromptLayer::create()->showPrompt(str);
			}
		}
		
	}
/////////////////////////////////////////////////////////////////////////////////////
	void GameMatchRegistration::closeFunc()
	{
		if (onCloseCallBack) onCloseCallBack();
	}

	void GameMatchRegistration::getMatchList()
	{
		PlatformLogic()->sendData(MDM_GP_LIST, ASS_GP_LIST_CONTEST, 0, 0,
			HN_SOCKET_CALLBACK(GameMatchRegistration::getMatchListCallback, this));
	}

	bool GameMatchRegistration::getMatchListCallback(HNSocketMessage* socketMessage)
	{
		UINT count = socketMessage->objectSize / sizeof(MSG_GP_ContestApplyInfo);
		MSG_GP_ContestApplyInfo * tmp = (MSG_GP_ContestApplyInfo *)socketMessage->object;

		_vec_MatchFirstInfo.clear();
		_vec_MatchInfo.clear();
		_vec_MatchLastInfo.clear();
		// 更新比赛信息前关闭点击，防止野指针崩溃
		for (auto btn : _matchVec)
		{
			if (btn != nullptr)
			{
				auto btnSate = dynamic_cast<Button*>(btn->getChildByName("Button_state"));
				if (btnSate  != nullptr )
				{
					btnSate->setEnabled(false);
					//btnSate->setVisible(false);
				}
				/*auto matchTimeOutBtn = dynamic_cast<Button*>(btn->getChildByName("Button_TimeOut"));
				if (matchTimeOutBtn != nullptr)
				{
					matchTimeOutBtn->setVisible(false);
				}*/
			}
		}
		while (count-- > 0)
		{
			//无法报名的默认排后面
			if (tmp->isInvalid)
			{
				//定时赛正在进行中
				if (tmp->contestState == 1 && tmp->iContestType >= 1)
				{
					_vec_MatchFirstInfo.push_back(tmp);
				}
				else
				{
					_vec_MatchLastInfo.push_back(tmp);
				}
			}
			else
			{
				//已经进入一小时倒计时的优先显示
				if (tmp->ContestBeginRemainSecTime <= MATCH_TIME_H * 60 * 60 && tmp->ContestBeginRemainSecTime > 0 && tmp->iContestType >= 1)
				{
					_vec_MatchFirstInfo.push_back(tmp);
				}
				//可以报名的定时赛优先显示
				else if (tmp->contestState == 1 || tmp->iContestType >= 1)
				{
					_vec_MatchFirstInfo.push_back(tmp);
				}
				else
				{
					_vec_MatchInfo.push_back(tmp);
				}
			}
			tmp++;
		}

		// 比赛信息个数有可能会变，所以每次更新之前先清理原有数据
		MatchInfoCache()->clear();

		if (_vec_MatchInfo.size() > 0 || _vec_MatchFirstInfo.size() > 0 || _vec_MatchLastInfo.size() > 0)
		{
			if (_vec_MatchFirstInfo.size() > 0)
			{
				for (int i = 0; i < _vec_MatchFirstInfo.size(); i++)
				{
					// 过滤本地没有的游戏，不显示在比赛列表
					for (auto game : GameCreator()->getValidGames())
					{
						if (game->uNameID == _vec_MatchFirstInfo.at(i)->iGameID)
						{
							MatchInfoCache()->addMatch(_vec_MatchFirstInfo.at(i));
						}
					}
				}
			}
			if (_vec_MatchInfo.size() > 0)
			{
				for (int i = 0; i < _vec_MatchInfo.size(); i++)
				{
					// 过滤本地没有的游戏，不显示在比赛列表
					for (auto game : GameCreator()->getValidGames())
					{
						if (game->uNameID == _vec_MatchInfo.at(i)->iGameID)
						{
							MatchInfoCache()->addMatch(_vec_MatchInfo.at(i));
						}
					}
				}
			}
			if (_vec_MatchLastInfo.size() > 0)
			{
				for (int i = 0; i < _vec_MatchLastInfo.size(); i++)
				{
					// 过滤本地没有的游戏，不显示在比赛列表
					for (auto game : GameCreator()->getValidGames())
					{
						if (game->uNameID == _vec_MatchLastInfo.at(i)->iGameID)
						{
							MatchInfoCache()->addMatch(_vec_MatchLastInfo.at(i));
						}
					}
				}
			}
		}

		// 如果获取到的列表信息数量跟本地列表相同，则直接更新列表数据
		if (_matchVec.size() == MatchInfoCache()->getMatchCount())
		{
			MatchInfoCache()->transform([=](MSG_GP_ContestApplyInfo* info, INT index){
				_matchVec[index]->setUserData(info);
				updateMatchInfo(_matchVec[index]);
			});
		}
		else
		{
			// 若数量有变化则清空重新创建列表
			if (_matchVec.size() > 0)
			{
				_matchVec.clear();
				_matchList->removeAllChildren();
			}

			if (MatchInfoCache()->getMatchCount() > 0) createMatchList();
		}

		
		return true;
	}

	void GameMatchRegistration::createMatchList()
	{
		auto matchs = MatchInfoCache()->getMatchList();

		std::vector<MSG_GP_ContestApplyInfo*> pages;

		int currentIndex = 0;
		int pageCount = matchs.size();
		int remainder = (matchs.size() % 6);

		//if (pageCount > 0)
		//{
			//for (int currentPage = 0; currentPage < pageCount; currentPage++)
			//{
				pages.clear();
				for (int room = 0; room < pageCount;  room++)
				{
					pages.push_back(matchs[room]);
				}
				createMatchPage(pages);
		//	}
		//}

	/*	if (remainder > 0)
		{
			pages.clear();
			for (int room = 0; room < remainder; currentIndex++, room++)
			{
				pages.push_back(matchs[currentIndex]);
			}
			createMatchPage(pages);
		}*/
	}

	void GameMatchRegistration::createMatchPage(std::vector<MSG_GP_ContestApplyInfo*> pages)
	{
		//创建比赛列表子页面
		/*auto matchPage = Layout::create();
		matchPage->setName("page");
		matchPage->setContentSize(_matchList->getContentSize());
		matchPage->setCascadeOpacityEnabled(true);*/
		_matchList->removeAllChildren();
		int idx = 0;
		int total = pages.size();
		if (total < 3) total = 3;
		for (auto match : pages)
		{
			//if (match->iGameID != _ReturniGameId() )continue;
			//if (_Selectedreward != match->iAwardType) continue;
			idx++;

			

			auto item = createMatchItemNode(match);
			//item->removeFromParentAndCleanup(false);
			
			if (nullptr != item)
			{
				item->setPosition(Vec2(_matchList->getContentSize().width * 0.5f, total * 165 + 50 - 160 * idx));
				//if (match->szRoomName == "斗地主精英赛")
				_matchList->addChild(item);
			}
		}
		
		_matchList->setInnerContainerSize(Size(_matchList->getContentSize().width, total * 160));
	}


	Node*	GameMatchRegistration::createMatchItemNode(MSG_GP_ContestApplyInfo* info)
	{
		// 单个比赛场button
		auto node = CSLoader::createNode(MATCH_ITEM_PATH);
		auto matchBg = dynamic_cast<Sprite*>(node->getChildByName("Sprite_list"));
		auto matchBtn = dynamic_cast<Button*>(matchBg->getChildByName("Button_state"));

		//比赛场奖励
		auto matchReward = dynamic_cast<ImageView*>(matchBg->getChildByName("Image_count"));

		matchBg->setUserData(info);
		matchBtn->addClickEventListener([=](Ref*){
			if (m_bCanTouch)
			{
				m_bCanTouch = false;
				auto matchinfo = (MSG_GP_ContestApplyInfo*)matchBg->getUserData();
				showMatchRegistPopup(matchinfo);
			}
		});

		switch (info->iAwardType)
		{
			//更换奖励显示
		case Selectedreward::GOLD://iContestID
			matchReward->loadTexture("platform/matchRegist/res/matchItem/jb.png");
			/*if (info->iContestAward == 5000)
			  matchBg->setTexture("platform/matchRegist/res/matchItem/gold1.png");
			else if (info->iContestAward == 10000)
				matchBg->setTexture("platform/matchRegist/res/registerItem/gold2.png");
			else
				matchBg->setTexture("platform/matchRegist/res/registerItem/gold3.png");*/

			break;
		case Selectedreward::MONEY:
			matchReward->loadTexture("platform/matchRegist/res/matchItem/hb.png");
			/*if (info->iContestAward == 120)
			{
				matchBg->setTexture("platform/matchRegist/res/registerItem/money200.png");
			}
			else if (info->iContestAward == 65){
				matchBg->setTexture("platform/matchRegist/res/registerItem/money100.png");
			}

			else if (info->iContestAward == 18){
				matchBg->setTexture("platform/matchRegist/res/registerItem/money30.png");
			}
			else if (info->iContestAward == 13){
				matchBg->setTexture("platform/matchRegist/res/registerItem/money20.png");
			}
			else{
				matchBg->setTexture("platform/matchRegist/res/registerItem/money10.png");
			}*/
			
			break;
		case Selectedreward::ROOMCARD:
			matchReward->loadTexture("platform/matchRegist/res/matchItem/fk.png");
			break;
		case Selectedreward::TELEPHONEBILL:
			matchReward->loadTexture("platform/matchRegist/res/matchItem/fk.png");
			/*if (info->iContestAward == 100)
				matchBg->setTexture("platform/matchRegist/res/registerItem/Telephonebill100.png");
			else if (info->iContestAward == 50)
				matchBg->setTexture("platform/matchRegist/res/registerItem/Telephonebill50.png");
			else if (info->iContestAward == 30)
				matchBg->setTexture("platform/matchRegist/res/registerItem/Telephonebill30.png");
			else
				matchBg->setTexture("platform/matchRegist/res/registerItem/Telephonebill10.png");*/
			break;
		case Selectedreward::REDENVELOPES:
			matchReward->loadTexture("platform/matchRegist/res/matchItem/hb.png");
			break;
		}

		// 更新比赛场信息
		updateMatchInfo(matchBg);

		// 保存现有列表信息
		_matchVec.push_back(matchBg);

		return node;
	}

	void GameMatchRegistration::updateMatchInfo(Sprite* matchBtn)
	{
		auto info = (MSG_GP_ContestApplyInfo*)matchBtn->getUserData();

		if (info->iCanPlayNum != 0)
		{
			////更新按钮状态，是否免费次数状态
			//MSG_GP_ContestHavePlayNum get;
			//get.iContestID = info->iContestID;
			//get.iUserID = PlatformLogic()->loginResult.dwUserID;
			//PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_CONTEST_HAVEPLAYNUM, (void*)&get, sizeof(get),
			//	HN_SOCKET_CALLBACK(GameMatchRegistration::getMatchInfoNumCallback, this));
			if (info->iCanPlayNum == 1)
			{
				auto Text_MatchTip = dynamic_cast<Text*>(matchBtn->getChildByName("Text_MatchTip"));
				Text_MatchTip->setString(GBKToUtf8("每日首次免费"));
				Text_MatchTip->setVisible(true);
			}
			else if (info->iCanPlayNum > 1)
			{
				auto Text_MatchTip = dynamic_cast<Text*>(matchBtn->getChildByName("Text_MatchTip"));
				Text_MatchTip->setString(StringUtils::format(GBKToUtf8("每日%d次免费(%d/%d)"), info->iCanPlayNum, info->iHavePlayNum, info->iCanPlayNum));
				Text_MatchTip->setVisible(true);
			}
		}

		// 比赛场名称
		auto matchName = dynamic_cast<Text*>(matchBtn->getChildByName("Text_name"));
		matchName->setString(GBKToUtf8(info->szRoomName));

		// 点
		auto image_dian = dynamic_cast<ImageView*>(matchBtn->getChildByName("Image_dian"));
		image_dian->setPositionX(matchName->getPositionX() + matchName->getContentSize().width + 30);

		// 游戏名字
		auto text_game = dynamic_cast<Text*>(matchBtn->getChildByName("Text_game"));
		text_game->setPositionX(matchName->getPositionX() + matchName->getContentSize().width + 60);
		string str_game = "";
		if (info->iGameID == 20171121)
		{
			str_game = "游金麻将";
		}
		else if (info->iGameID == 20171123)
		{
			str_game = "晋江麻将";
		}
		else if (info->iGameID == 10100003)
		{
			str_game = "斗地主";
		}
		else if (info->iGameID == 12345678)
		{
			str_game = "跑得快";
		}
		else
		{

		}
		text_game->setString(GBKToUtf8(str_game.c_str()));

		// 已参赛人数
		auto matchPeople = dynamic_cast<Text*>(matchBtn->getChildByName("Text_people"));
		if (info->iContestType >= 1)
		{
			matchPeople->setString(StringUtils::format(GBKToUtf8("%d人"), info->iOnlinePeople));
		}
		else
		{
			matchPeople->setString(StringUtils::format(GBKToUtf8("%d/%d"), info->iOnlinePeople, info->iNeedPeople));
		}
		//参赛所需人数
		auto matchTotalPeople = dynamic_cast<Text*>(matchBtn->getChildByName("Text_TatalPeople"));
		matchTotalPeople->setString(StringUtils::format(GBKToUtf8("%d"), info->iNeedPeople));
		//auto xieGan = dynamic_cast<Text*>(matchBtn->getChildByName("Text_7"));

	    //参赛所需费用
		auto numberMatch = dynamic_cast<Text*>(matchBtn->getChildByName("Text_Cost"));
		numberMatch->setString(StringUtils::format(GBKToUtf8("X%d"), info->iEnterFee));

		//参赛类型
		auto TypeMatch = dynamic_cast<Sprite*>(matchBtn->getChildByName("Sprite_Cost"));
		if (info->iEnterType == 0)   //金币
			TypeMatch->setTexture("platform/matchRegist/res/matchItem/jb_1.png");
		else  //房卡
			TypeMatch->setTexture("platform/matchRegist/res/matchItem/fk_1.png");

		//参赛规则按钮
		auto MatchRullee = dynamic_cast<Button*>(matchBtn->getChildByName("Button_Rule"));
		MatchRullee->addClickEventListener([=](Ref*){
			RulePage(info);
		});

		// 调整显示位置
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
		//matchName->setTextVerticalAlignment(TextVAlignment::CENTER);
		//matchReward->setTextVerticalAlignment(TextVAlignment::CENTER);
#endif

		auto img_count = dynamic_cast<ImageView*>(matchBtn->getChildByName("Image_count"));

		//auto spr_icon = dynamic_cast<Sprite*>(matchBtn->getChildByName("Sprite_icon"));
		if (0 == info->iContestType)
		{
			string str = StringUtils::format(GBKToUtf8("满%d人开赛"), info->iNeedPeople);
			auto spr_icon = dynamic_cast<Text*>(matchBtn->getChildByName("text_play_tip"));
			spr_icon->setString(str);

			//matchBtn->loadTextures(FULL_BG_PATH, FULL_BG_PATH, FULL_BG_PATH);
			////spr_icon->setTexture(FULL_ICON_PATH);
			//img_count->loadTexture(ICON_PATH2);

			matchName->setTextColor(Color4B(105, 55, 33, 255));
			matchPeople->setTextColor(Color4B(105, 55, 33, 255));
			//matchReward->setTextColor(Color4B(87, 63, 171, 255));
		}

		// 比赛规则
		auto matchRule = dynamic_cast<Text*>(matchBtn->getChildByName("text_play_tip"));
		matchRule->setVisible(true);

		// 比赛倒计时(默认不显示)
		auto matchTime = dynamic_cast<ImageView*>(matchBtn->getChildByName("Image_playtime"));
		matchTime->setVisible(false);

		std::string strRule ;
		if (info->iContestType >= 1)
		{
			time_t Time = info->beginTime;
			struct tm *p = gmtime(&Time);
			p->tm_hour += 8;
			if (p->tm_hour >= 24)
			{
				p->tm_hour -= 24;
			}
			char BeginTime[64];
			strftime(BeginTime, sizeof(BeginTime), "%H:%M", p);
			strRule = StringUtils::format(GBKToUtf8("%s开赛"), BeginTime);
			matchRule->setVisible(true);

			matchName->setTextColor(Color4B(167, 69, 23, 255));
			matchPeople->setTextColor(Color4B(167, 69, 23, 255));
		}
		else
		{
			strRule = StringUtils::format(GBKToUtf8("满%d人开赛"), info->iNeedPeople);
		}

		
		matchRule->setString(strRule);

		// 第一名奖励提示
		string text_reward = "";
		if (info->iAwardType == 0)
		{
			text_reward = StringUtils::format(GBKToUtf8("金币X%d"), info->iContestAward);
		}
		else if (info->iAwardType == 1)
		{
			text_reward = StringUtils::format(GBKToUtf8("红包%d元"), info->iContestAward);
		}
		else if (info->iAwardType == 2)
		{
			text_reward = StringUtils::format(GBKToUtf8("房卡%d张"), info->iContestAward);
		}
		auto matchReward = dynamic_cast<Text*>(matchBtn->getChildByName("text_reward"));
		matchReward->setString(text_reward);

		// 已经报名提示
		auto img_state = dynamic_cast<Button*>(matchBtn->getChildByName("Button_state"));
		img_state->loadTextures(info->isApply ? STATE_PATH2 : STATE_PATH1,
			info->isApply ? STATE_PATH2 : STATE_PATH1,
			info->isApply ? STATE_PATH2 : STATE_PATH1);
		
		//定时赛超时或者时间未到处理
		auto matchTimeOutBtn = dynamic_cast<Button*>(matchBtn->getChildByName("Button_TimeOut"));
		matchTimeOutBtn->setUserData(info);
		matchTimeOutBtn->addClickEventListener([=](Ref*){
			if (m_bCanTouch)
			{
				m_bCanTouch = false;
				auto matchinfo = (MSG_GP_ContestApplyInfo*)matchTimeOutBtn->getUserData();
				showMatchTimeOutPopup(matchinfo);
			}
		});

		// 赛前提示
		auto Text_Tip = dynamic_cast<Text*>(matchBtn->getChildByName("Text_Tip"));
		Text_Tip->setVisible(false);
		bool isTimeOut = false;
		bool isTimeNow = false;
		bool isTimeFuture = false;
		if (info->isApply == 0 )
		{
			TypeMatch->setVisible(true);
			numberMatch->setVisible(true);
		}
		else
		{
			TypeMatch->setVisible(false);
			numberMatch->setVisible(false);	
		}

		//如果时间到了开始前2分钟，显示进入
		if (info->iContestType >= 1)
		{
			time_t Time = info->beginTime;
			tm *p = gmtime(&Time);
			p->tm_hour += 8;
			if (p->tm_hour >= 24)
			{
				p->tm_hour -= 24;
			}
			int tm_hour = p->tm_hour;
			int tm_min = p->tm_min;

			time_t tt = time(NULL);
			tm* TimeNow = gmtime(&tt);
			TimeNow->tm_hour += 8;
			if (TimeNow->tm_hour >= 24)
			{
				TimeNow->tm_hour -= 24;
			}
			if (TimeNow->tm_hour == tm_hour)
			{
				//赛前两分钟
				if (tm_min - TimeNow->tm_min <= IS_MATCHTIME)
				{
					//比赛已开始
					if (tm_min - TimeNow->tm_min <= 0)
					{
						if (info->isApply != 0)
						{
							isTimeOut = true;
						}
					}
					else
					{
						//进入等待
						isTimeNow = true;
					}
				}
				else if (tm_min - TimeNow->tm_min > IS_MATCHTIME)
				{
					//已报名提前两分钟进入
					if (info->isApply != 0)
					{
						isTimeFuture = true;
					}
				}
			}
			else if (TimeNow->tm_hour > tm_hour)
			{
				//超过时间
				if (info->isApply != 0)
				{
					isTimeOut = true;
				}
			}
			else if (TimeNow->tm_hour < tm_hour)
			{
				if (tm_hour - TimeNow->tm_hour > 1)
				{
					//时间未到
					if (info->isApply != 0)
					{
						isTimeFuture = true;
					}
				}
				else
				{
					tm_min += 60;
					if (tm_min - TimeNow->tm_min > IS_MATCHTIME)
					{
						//已报名提前两分钟进入
						if (info->isApply != 0)
						{
							isTimeFuture = true;
						}
					}
					else if (tm_min - TimeNow->tm_min <= IS_MATCHTIME)
					{
						//进入等待
						isTimeNow = true;
					}
				}
			}
			if (info->ContestBeginRemainSecTime > 0)
			{
				if (info->ContestBeginRemainSecTime > MATCH_TIME_H * 60 * 60)
				{
					matchTime->setVisible(false);
					matchRule->setVisible(true);
				}
				else
				{
					int sec = info->ContestBeginRemainSecTime % 60;
					int min = info->ContestBeginRemainSecTime / 60;
					matchTime->setVisible(true);
					matchRule->setVisible(false);
					auto Text_minute = dynamic_cast<Text*>(matchTime->getChildByName("Text_minute"));
					Text_minute->setString(StringUtils::format("%02d", min));
					auto Text_second = dynamic_cast<Text*>(matchTime->getChildByName("Text_second"));
					Text_second->setString(StringUtils::format("%02d", sec));
					matchTime->stopAllActions();
					matchTime->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([info, matchTime, matchRule](){
						info->ContestBeginRemainSecTime--;
						if (info->ContestBeginRemainSecTime != 0)
						{
							int sec = info->ContestBeginRemainSecTime % 60;
							int min = info->ContestBeginRemainSecTime / 60;
							auto Text_minute = dynamic_cast<Text*>(matchTime->getChildByName("Text_minute"));
							Text_minute->setString(StringUtils::format("%02d", min));
							auto Text_second = dynamic_cast<Text*>(matchTime->getChildByName("Text_second"));
							Text_second->setString(StringUtils::format("%02d", sec));
						}
						else
						{
							matchTime->setVisible(false);
							matchRule->setVisible(true);
						}
					}), nullptr));
				}
			}
			else
			{
				matchTime->setVisible(false);
				matchRule->setVisible(true);
			}
		}

		//时间未到无法报名
		if (info->isInvalid)
		{
			if (isTimeOut)
			{
				//超时显示为比赛开始中
				img_state->setEnabled(false);
				img_state->loadTextures(STATE_PATH5, STATE_PATH5, STATE_PATH5);
				TypeMatch->setVisible(false);
				numberMatch->setVisible(false);
			}
			//定时赛且比赛状态为正在比赛中
			else if (info->iContestType >= 1 && info->contestState == 1)
			{
				matchTimeOutBtn->setVisible(true);
				img_state->setEnabled(false);
				img_state->loadTextures(STATE_PATH5, STATE_PATH5, STATE_PATH5);
				TypeMatch->setVisible(false);
				numberMatch->setVisible(false);
			}
			else
			{
				img_state->setEnabled(false);
				img_state->loadTextures(STATE_PATH3, STATE_PATH3, STATE_PATH3);
				TypeMatch->setVisible(false);
				numberMatch->setVisible(false);
				if (info->iContestType >= 1)
				{
					matchTimeOutBtn->setVisible(true);
				}
			}
		}
		else
		{
			matchTimeOutBtn->setVisible(false);
			//免费报名（）
			if (info->iCanPlayNum != 0 && info->iCanPlayNum - info->iHavePlayNum != 0)
			{
				img_state->loadTextures(STATE_PATH4, STATE_PATH4, STATE_PATH4);
				img_state->setEnabled(true);
				TypeMatch->setVisible(false);
				numberMatch->setVisible(false);
				if (isTimeNow)
				{
					img_state->setEnabled(true);
					img_state->loadTextures(STATE_PATH7, STATE_PATH7, STATE_PATH7);
					TypeMatch->setVisible(false);
					numberMatch->setVisible(false);
					//提示文字比赛前2分钟进入等待
					Text_Tip->setVisible(true);
				}
				else if (isTimeFuture)
				{
					img_state->setEnabled(true);
					img_state->loadTextures(STATE_PATH6, STATE_PATH6, STATE_PATH6);
					TypeMatch->setVisible(false);
					numberMatch->setVisible(false);
					//提示文字比赛前2分钟进入等待
					Text_Tip->setVisible(true);
				}
			}
			//免费次数用完之后无法报名处理
			else if (info->iCanPlayNum != 0 && info->iCanPlayNum - info->iHavePlayNum == 0 && info->iEnterFee == 0)
			{
				img_state->loadTextures(STATE_PATH3, STATE_PATH3, STATE_PATH3);
				img_state->setEnabled(false);
				TypeMatch->setVisible(false);
				numberMatch->setVisible(false);
				if (isTimeNow)
				{
					img_state->setEnabled(true);
					img_state->loadTextures(STATE_PATH7, STATE_PATH7, STATE_PATH7);
					TypeMatch->setVisible(false);
					numberMatch->setVisible(false);
					//提示文字比赛前2分钟进入等待
					Text_Tip->setVisible(true);
				}
				else if (isTimeFuture)
				{
					img_state->setEnabled(true);
					img_state->loadTextures(STATE_PATH6, STATE_PATH6, STATE_PATH6);
					TypeMatch->setVisible(false);
					numberMatch->setVisible(false);
					//提示文字比赛前2分钟进入等待
					Text_Tip->setVisible(true);
				}
			}
			else
			{
				img_state->setEnabled(true);
				if (isTimeNow)
				{
					img_state->loadTextures(STATE_PATH7, STATE_PATH7, STATE_PATH7);
					TypeMatch->setVisible(false);
					numberMatch->setVisible(false);
					//提示文字比赛前2分钟进入等待
					Text_Tip->setVisible(true);
				}
				else if (isTimeFuture)
				{
					img_state->loadTextures(STATE_PATH6, STATE_PATH6, STATE_PATH6);
					TypeMatch->setVisible(false);
					numberMatch->setVisible(false);
					//提示文字比赛前2分钟进入等待
					Text_Tip->setVisible(true);
				}
			}
		}
	}

	void GameMatchRegistration::showMatchRegistPopup(MSG_GP_ContestApplyInfo* info)
	{
		MatchInfoCache()->setSelectMatch(info);

		/*_applyLayer->setOpacity(255);
		_applyLayer->setVisible(true);

		// 报名操作页面
		auto applyBG = dynamic_cast<ImageView*>(_applyLayer->getChildByName("Image_applyBG"));

		auto awardList = dynamic_cast<ui::ListView*>(applyBG->getChildByName("ListView_regist"));
		awardList->removeAllItems();

		MSG_GP_I_GetContestAward get;
		get.ContestID = info->iContestID;

		PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_CONTEST_AWARDINFO, &get, sizeof(get), [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetContestAward, socketMessage->objectSize, true,
				"MSG_GP_O_GetContestAward size of error!");

			MSG_GP_O_GetContestAward *awardInfo = (MSG_GP_O_GetContestAward*)socketMessage->object;

			for (int i = 0; i < 10; i++)
			{
				if (awardInfo->iAward[i] > 0)
				{
					auto item_node = CSLoader::createNode(REWARD_ITEM_PATH);
					auto img_award = dynamic_cast<ImageView*>(item_node->getChildByName("Image_item"));
					img_award->removeFromParentAndCleanup(false);
					img_award->setScale(0.8f);

					auto text_rank = dynamic_cast<Text*>(img_award->getChildByName("Text_rank"));
					text_rank->setString(StringUtils::format(GBKToUtf8("第%d名"), i + 1));
					//auto spr_icon = dynamic_cast<Sprite*>(img_award->getChildByName("Sprite_icon"));
					//spr_icon->setTexture(StringUtils::format("platform/matchRegist/res/mymatch/award%d.png", awardInfo->iAwardType[i]));
					auto text_value = dynamic_cast<Text*>(img_award->getChildByName("Text_value"));
					text_value->setString(to_string(awardInfo->iAward[i]));

					awardList->pushBackCustomItem(img_award);
				}
			}

			PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_CONTEST_AWARDINFO);

			return true;
		});

		// 关闭按钮
		auto btn_close = dynamic_cast<Button*>(applyBG->getChildByName("Button_close"));
		btn_close->addClickEventListener([=](Ref*) {
		
			_applyLayer->runAction(Sequence::create(FadeOut::create(0.15f), CallFunc::create([=]() {
				_applyLayer->setVisible(false);
				_applyLayer->stopAllActions();
				PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_GET_APPLY_NUM);
				PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_CONTEST_AWARDINFO);
			}), nullptr));
		});

		// 报名按钮
		_btn_apply = dynamic_cast<Button*>(applyBG->getChildByName("Button_apply"));
		_btn_apply->setVisible(!info->isApply);
		_btn_apply->addClickEventListener([this](Ref*){*/

			auto match = MatchInfoCache()->getSelectMatch();

			if (match)
			{
				// 检测是否需要更新
				// 满人赛登陆备战区再报名，防止报名人数已满但真正进入比赛人数不够
				if (0 == match->iContestType)
				{
					if (match->isApply == 0)
					{
						getMatchInfo(match->iContestID, false, match->iContestType);
					}
					else
					{
						getMatchInfo(match->iContestID, false, IS_MATCHREGISTRATION);
					}
					//GameMatchWaiting::createMatchWaiting(match->iContestID, true);
				}
				else
				{
					if (match->isApply == 0)
					{
						getMatchInfo(match->iContestID, false, match->iContestType);
						// 定时赛和循环赛直接报名，走大厅报名消息
						//sendRegistOrRetireInPlatform(match->iContestID, true);
					}
					else
					{
						getMatchInfo(match->iContestID, false, IS_MATCHTIMESTRATION);
						//GameMatchWaiting::createMatchWaiting(match->iContestID);
					}
					
				}
			}
			else
			{
				log("MatchInfo is Null");
				m_bCanTouch = true;
			}
		

		//auto img_type = dynamic_cast<ImageView*>(applyBG->getChildByName("Image_type"));

		// 退赛按钮
		//_btn_retire = dynamic_cast<Button*>(applyBG->getChildByName("Button_retire"));
		//_btn_retire->addClickEventListener([=](Ref*) {
			// 退赛
			//sendRegistOrRetire(MatchInfoCache()->getSelectMatch()->iContestID, false);
	//	});

		// 进入备战区按钮
		//_btn_enter = dynamic_cast<Button*>(applyBG->getChildByName("Button_enter"));
		//_btn_enter->addClickEventListener([=](Ref*) {
			// 进入备战区
		//	GameMatchWaiting::createMatchWaiting(MatchInfoCache()->getSelectMatch()->iContestID);
		//});

		/*std::string str;
		if (0 == info->iContestType)
		{
			str = StringUtils::format("%d", info->iNeedPeople);

			//img_type->loadTexture("platform/matchRegist/res/mymatch/kaisairenshu.png");

			_btn_retire->setVisible(false);
			_btn_enter->setVisible(false);
		}

		if (info->iContestType >= 1)
		{
			time_t Time = info->beginTime;
			struct tm *p = gmtime(&Time);
			p->tm_hour += 8;
			if (p->tm_hour >= 24)
			{
				p->tm_hour -= 24;
			}
			char BeginTime[64];
			strftime(BeginTime, sizeof(BeginTime), "%H:%M", p);

			str = StringUtils::format(GBKToUtf8("%s"), BeginTime);

			//img_type->loadTexture("platform/matchRegist/res/mymatch/kaisaishijian.png");

			_btn_retire->setVisible(info->isApply);
			_btn_enter->setVisible(info->isApply);
		}

		//auto matchRule = dynamic_cast<Text*>(applyBG->getChildByName("Text_time"));
		//matchRule->setString(str);

		//auto matchCost = dynamic_cast<Text*>(applyBG->getChildByName("Text_cost"));
		if (info->iEnterFee == 0)
		{
		//	matchCost->setString(GBKToUtf8("免费参赛"));
		}
		else
		{
			if (info->iEnterType == 0)
			{
				matchCost->setString(StringUtils::format(GBKToUtf8("%d金币"), info->iEnterFee));
			}
			else
			{
				matchCost->setString(StringUtils::format(GBKToUtf8("%d钻石"), info->iEnterFee));
			}
		}

		Text* textPeople = dynamic_cast<Text*>(applyBG->getChildByName("Text_count"));
		textPeople->setString(GBKToUtf8("0"));

		_contestID = info->iContestID;

		auto func = CallFunc::create([=](){

			PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_GET_APPLY_NUM, &_contestID, sizeof(int), [=](HNSocketMessage* socketMessage) {

				CHECK_SOCKET_DATA_RETURN(MSG_GP_UpdateApplyNum, socketMessage->objectSize, true, "MSG_GP_UpdateApplyNum size is error");
				MSG_GP_UpdateApplyNum *info = (MSG_GP_UpdateApplyNum*)socketMessage->object;

				textPeople->setString(std::to_string(info->iApplyNum));

				return true;
			});
		});

		_applyLayer->runAction(func);*/

		// 间隔3秒循环查询已报名人数信息showPrompt(GBKToUtf8("服务器未启动"));
		//_applyLayer->runAction(RepeatForever::create(Sequence::create(DelayTime::create(3.0f), func, nullptr)));
	}

	void GameMatchRegistration::showMatchTimeOutPopup(MSG_GP_ContestApplyInfo* info)
	{
		time_t Time = info->ContestBeginTime;
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		if (p->tm_hour >= 24)
		{
			p->tm_hour -= 24;
		}
		int tm_hour_B = p->tm_hour;
		int tm_min_B = p->tm_min;
		char BeginTime[64];
		strftime(BeginTime, sizeof(BeginTime), "%H:%M", p);

		time_t EndTime = info->ContestEndTime;
		struct tm *ep = gmtime(&EndTime);
		ep->tm_hour += 8;
		if (ep->tm_hour >= 24)
		{
			ep->tm_hour -= 24;
		}
		int tm_hour_E = ep->tm_hour;
		int tm_min_E = ep->tm_min;
		char EndTimes[64];
		strftime(EndTimes, sizeof(EndTimes), "%H:%M", ep);

		time_t tt = time(NULL);
		tm* TimeNow = gmtime(&tt);
		TimeNow->tm_hour += 8;
		if (TimeNow->tm_hour >= 24)
		{
			TimeNow->tm_hour -= 24;
		}
		int tm_hour_N = TimeNow->tm_hour;
		int tm_min_N = TimeNow->tm_min;

		bool IS_Time_future = false;
		bool IS_Time_out = false;

		//还没到报名时间
		if (tm_hour_N <= tm_hour_B)
		{
			if (tm_hour_B == tm_hour_N)
			{
				if (tm_min_N < tm_min_B)
				{
					IS_Time_future = true;
				}
			}
			else
			{
				IS_Time_future = true;
			}
		}

		//报名时间已过
		if (tm_hour_N >= tm_hour_E)
		{
			if (tm_hour_E == tm_hour_N)
			{
				if (tm_min_N > tm_min_E)
				{
					IS_Time_out = true;
				}
			}
			else
			{
				IS_Time_out = true;
			}
		}
		string strRule;

		//弹窗提示
		auto prompt = GamePromptLayer::create();
		if (IS_Time_out)
		{
			strRule = StringUtils::format(GBKToUtf8("比赛正进行中,请在明天%s到%s报名参赛。"), BeginTime, EndTimes);
		}
		else if (IS_Time_future)
		{
			strRule = StringUtils::format(GBKToUtf8("比赛尚未开始,请在%s到%s报名参赛。"), BeginTime, EndTimes);
		}
		prompt->showPrompt(strRule);

		m_bCanTouch = true;
	}

	void GameMatchRegistration::showMatchDetails()
	{

	}

	void GameMatchRegistration::sendRegistOrRetireInPlatform(int matchID, bool isRegist)
	{
		MSG_GP_ContestApply registration;
		registration.iContestID = matchID;
		registration.iUserID = PlatformLogic()->loginResult.dwUserID;
		registration.iType = !isRegist;

		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_CONTEST_APPLY, (void*)&registration, sizeof(registration),
			HN_SOCKET_CALLBACK(GameMatchRegistration::registOrRetireInPlatformCallback, this));

		// 防止重复点击
		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("请稍候..."), 25);
	}


	void GameMatchRegistration::sendRegistOrRetireInMatch(int matchID, bool isRegist)
	{
		MSG_GP_ContestApply registration;
		registration.iContestID = matchID;
		registration.iUserID = PlatformLogic()->loginResult.dwUserID;
		registration.iType = !isRegist;

		// 满人赛报名必须是走房间报名消息
		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_CONTEST_APPLY, (void*)&registration, sizeof(registration),
			HN_SOCKET_CALLBACK(GameMatchRegistration::registOrRetireInMatchCallback, this));

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("请稍候..."), 25);
	}

	bool GameMatchRegistration::registOrRetireInPlatformCallback(HNSocketMessage* socketMessage)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
		m_bCanTouch = true;

		CHECK_SOCKET_DATA_RETURN(MSG_GP_ContestApply_Result, socketMessage->objectSize, true, "MSG_GP_ContestApply_Result size is error");
		MSG_GP_ContestApply_Result* applyResult = (MSG_GP_ContestApply_Result*)socketMessage->object;

		auto info = MatchInfoCache()->getSelectMatch();

		std::string str("");
		switch (applyResult->bResult)
		{
		case 0:
			str = "满人赛不能在大厅中报名。"; break;
		case 1:
		{
			//GamePromptLayer::create()->showPrompt(GBKToUtf8("报名成功，等待比赛开始。"));
			updateTimingInfo(info);
			//this->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=](){
			//	// 进入备战区
			//	GameMatchWaiting::createMatchWaiting(MatchInfoCache()->getSelectMatch()->iContestID);
			//}), nullptr));

			return true;
		} break;
		case 2:
		{
			str = "退赛成功。";
			//_btn_apply->setVisible(true);
			//_btn_enter->setVisible(false);
			//_btn_retire->setVisible(false);

			//if (info) info->isApply = false;
		} break;
		case 3:
			str = "已经报名，无需再次报名。"; break;
		case 4:
			str = "比赛已经开始，无法报名。"; break;
		case 5:
			str = "未报名，退赛失败。"; break;
		case 6:
			str = "比赛已开始，无法退赛。"; break;
		case 7:
			str = "达到最大报名人数，无法报名。"; break;
		case 8:
			str = "金币不足，无法报名。"; break;
		case 9:
			str = "比赛房间未开启。"; break;
		case 10:
			str = "用户不存在。"; break;
		case 11:
			str = "比赛ID错误。"; break;
		case 12:
			str = "免费次数用完，报名失败。"; break;
		default:
			str = "未知错误。"; break;
		}

		//GamePromptLayer::create()->showPrompt(GBKToUtf8(str.c_str()));
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8(str.c_str()));
		prompt->setCallBack([=](){
			RoomLogic()->close();
			RoomLogic()->setRoomRule(0);
		});
		for (auto btn : _matchVec)
		{
			auto temp = (MSG_GP_ContestApplyInfo*)btn->getUserData();
			if (temp && info)
			{
				if (temp->iContestID == info->iContestID)
				{
					btn->setUserData(info);
					updateMatchInfo(btn);
				}
			}
		}

		return true;
	}

	bool GameMatchRegistration::registOrRetireInMatchCallback(HNSocketMessage* socketMessage)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		CHECK_SOCKET_DATA_RETURN(MSG_GP_ContestApply_Result, socketMessage->objectSize, true, "MSG_GP_ContestApply_Result size is error");
		MSG_GP_ContestApply_Result* applyResult = (MSG_GP_ContestApply_Result*)socketMessage->object;

		if (1 == applyResult->bResult)
		{
			//GamePromptLayer::create()->showPrompt(GBKToUtf8("报名成功，等待比赛开始。"));
			//_btn_retire->setVisible(true);

			//// 监听比赛取消消息
			//contestAbandonListener();

			//// 监听比赛开赛消息
			//contestInitListener();
			
			//报名成功则打开等待界面
			MSG_GP_GetContestRoomID_Result* info = (MSG_GP_GetContestRoomID_Result*)m_socketMessage->object;
			auto pMatchWaitingLayer = GameMatchWaiting::createMatchWaiting(info->tRoomInfo.iContestID,true);
			pMatchWaitingLayer->updateMatchInfo(m_socketMessage);
			if (m_socketMessage)
			{
				HNSocketMessage::releaseMessage(m_socketMessage);
				m_socketMessage = nullptr;
			}
		}
		else if (3 == applyResult->bResult)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("已经报名，无需再次报名"));
			//_btn_retire->setVisible(true);
		}
		else
		{
			std::string str("");
			switch (applyResult->bResult)
			{
			case 0:
				str = "满人赛不能在大厅中报名。"; break;
			case 2:
				str = "退赛成功，即将离开备战区。"; break;
			case 4:
				str = "比赛已经开始，无法报名。"; break;
			case 5:
				str = "未报名，退赛失败。"; break;
			case 6:
				str = "比赛已开始，无法退赛。"; break;
			case 7:
				str = "达到最大报名人数，无法报名。"; break;
			case 8:
				str = "金币不足，无法报名。"; break;
			case 9:
				str = "比赛房间未开启。"; break;
			case 10:
				str = "用户不存在。"; break;
			case 11:
				str = "比赛ID错误。"; break;
			case 12:
				str = "免费次数已用完，报名失败。"; break;
			case 13:
				str = "房卡不足，无法报名。"; break;
			default:
				str = "未知错误。"; break;
			}

			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8(str.c_str()));
			prompt->setCallBack([=](){
				RoomLogic()->close();
				RoomLogic()->setRoomRule(0);
			});
		}

		return true;
	}

	void GameMatchRegistration::updateUserInfo()
	{
		//金币
		LLONG iMoney = PlatformLogic()->loginResult.i64Money;
		_text_gold->setString(to_string(iMoney));

		//奖券
		LLONG lottery = PlatformLogic()->loginResult.iLotteries;
		_text_lottery->setString(to_string(lottery));

		//房卡
		LLONG Jewels = PlatformLogic()->loginResult.iJewels;
		_text_lottery->setString(to_string(Jewels));
	}

	void GameMatchRegistration::updateTimingInfo(MSG_GP_ContestApplyInfo* info)
	{
		auto node = CSLoader::createNode("platform/matchRegist/res/Manchu_match/Timing_matchLayer.csb");
		addChild(node);

		auto Image_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));

		//赛事名称
		string str_game = "";
		if (info->iGameID == 20171121)
		{
			str_game = "游金麻将";
		}
		else if (info->iGameID == 20171123)
		{
			str_game = "晋江麻将";
		}
		else if (info->iGameID == 10100003)
		{
			str_game = "斗地主";
		}
		else if (info->iGameID == 12345678)
		{
			str_game = "跑得快";
		}
		auto Text_name = dynamic_cast<Text*>(Image_bg->getChildByName("Text_name"));
		Text_name->setString(GBKToUtf8(StringUtils::format("%s(%s)", info->szRoomName, str_game.c_str())));


		//时间
		std::string strRule;
		time_t Time = info->beginTime;
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		if (p->tm_hour >= 24)
		{
			p->tm_hour -= 24;
		}
		char BeginTime[64];
		strftime(BeginTime, sizeof(BeginTime), "%H:%M", p);

		strRule = StringUtils::format(GBKToUtf8("%s开赛"), BeginTime);
		auto Text_time = dynamic_cast<Text*>(Image_bg->getChildByName("Text_time"));
		Text_time->setString(strRule);

		//关闭按钮
		auto Button_sure = dynamic_cast<Button*>(Image_bg->getChildByName("Button_sure"));
		Button_sure->addClickEventListener([=](Ref*){
			node->removeFromParent();
		});

		//报名成功之后关闭当前房间链接
		RoomLogic()->close();
		RoomLogic()->setRoomRule(0);
	}

	void GameMatchRegistration::showMatchRecord()
	{
		if (_RecordMatch == nullptr)
		{
			return;
		}
		_RecordMatch->setVisible(true);
		ImageView* Img_bg = dynamic_cast<ImageView*>(_RecordMatch->getChildByName("Image_bg"));
		Button* btn_close = dynamic_cast<Button*>(Img_bg->getChildByName("btn_close"));
		btn_close->addClickEventListener([=](Ref*) {
			_RecordMatch->setVisible(false);
		});
		if (_vec_MatchRecord.size() == 0)
		{
			auto noRecord = dynamic_cast<ImageView*>(Img_bg->getChildByName("Image_NoRecord"));
			noRecord->setVisible(true);
			return;
		}
		else
		{
			auto noRecord = dynamic_cast<ImageView*>(Img_bg->getChildByName("Image_NoRecord"));
			noRecord->setVisible(false);

			ListView* list = dynamic_cast<ListView*>(Img_bg->getChildByName("list_record"));
			list->setVisible(true);
			Widget* item = list->getItem(0);
			if (item != nullptr)
			{
				list->removeAllItems();
			}
			//战绩记录条
			for (int i = 0; i < _vec_MatchRecord.size(); i++)
			{
				auto node = CSLoader::createNode("platform/matchRegist/WinRecordNode.csb");
				Layout* panel_clone = dynamic_cast<Layout*>(node->getChildByName("panel_clone"));

				//排名
				if (_vec_MatchRecord[i].iRankNum <= 3)
				{
					ImageView* img_rank = dynamic_cast<ImageView*>(panel_clone->getChildByName("image_rank"));
					img_rank->loadTexture(StringUtils::format("platform/matchRegist/res/winrecord/rank_%d.png", _vec_MatchRecord[i].iRankNum));
				}
				else
				{
					ImageView* img_rank = dynamic_cast<ImageView*>(panel_clone->getChildByName("image_rank"));
					img_rank->setVisible(false);
					Text* text_rank = dynamic_cast<Text*>(panel_clone->getChildByName("text_rank"));
					text_rank->setString(StringUtils::format(GBKToUtf8("第%d名"), _vec_MatchRecord[i].iRankNum));
					text_rank->setVisible(true);
				}

				//游戏名字
				string str_game = "";
				if (_vec_MatchRecord[i].iGameID == 20171121)
				{
					str_game = "游金麻将";
				}
				else if (_vec_MatchRecord[i].iGameID == 20171123)
				{
					str_game = "晋江麻将";
				}
				else if (_vec_MatchRecord[i].iGameID == 10100003)
				{
					str_game = "斗地主";
				}
				else if (_vec_MatchRecord[i].iGameID == 12345678)
				{
					str_game = "跑得快";
				}
				str_game = StringUtils::format("%s(%s)", _vec_MatchRecord[i].szRoomName, str_game.c_str());
				Text* Text_GameName = dynamic_cast<Text*>(panel_clone->getChildByName("Text_GameName"));
				Text_GameName->setString(GBKToUtf8(str_game));
				if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
				{
					//Text_GameName->setString(Utf8ToGBK(_vec_MatchRecord[i].szRoomName));
				}

				//游戏时间
				Text* Text_Time = dynamic_cast<Text*>(panel_clone->getChildByName("Text_Time"));
				if (_vec_MatchRecord[i].FinishTime != 0)
				{
					std::string strRule;
					time_t Time = _vec_MatchRecord[i].FinishTime;
					struct tm *p = gmtime(&Time);
					p->tm_hour += 8;
					/*if (p->tm_hour >= 24)
					{
					p->tm_hour -= 24;
					}*/
					char BeginTime[64];
					strftime(BeginTime, sizeof(BeginTime), "%m-%d %H:%M", p);
					Text_Time->setString(BeginTime);
				}

				//奖励类型
				ImageView* Image_Reward_type = dynamic_cast<ImageView*>(panel_clone->getChildByName("Image_Reward_type"));
				Text* Text_Reward_num = dynamic_cast<Text*>(panel_clone->getChildByName("Text_Reward_num"));
				if (_vec_MatchRecord[i].iConTestAwardType == Selectedreward::GOLD)
				{
					Image_Reward_type->loadTexture("platform/matchRegist/res/matchItem/jb.png");
					Text_Reward_num->setString(StringUtils::format("X%d", _vec_MatchRecord[i].iConTestAward));
				}
				else if (_vec_MatchRecord[i].iConTestAwardType == Selectedreward::MONEY)
				{
					Image_Reward_type->loadTexture("platform/matchRegist/res/matchItem/hb.png");
					Text_Reward_num->setString(StringUtils::format(GBKToUtf8("%d元"), _vec_MatchRecord[i].iConTestAward));
				}
				else if (_vec_MatchRecord[i].iConTestAwardType == Selectedreward::ROOMCARD)
				{
					Image_Reward_type->loadTexture("platform/matchRegist/res/matchItem/fk.png");
					Text_Reward_num->setString(StringUtils::format("X%d", _vec_MatchRecord[i].iConTestAward));
				}
				else
				{
					//类型未知
					Image_Reward_type->setVisible(false);
				}

				//分享
				Button* btn_share = dynamic_cast<Button*>(panel_clone->getChildByName("btn_share"));
				btn_share->setUserData(&_vec_MatchRecord[i]);
				btn_share->addClickEventListener([=](Ref*ref){
					//更新分享内容
					showUpdataShare(ref);
					//分享链接
					//UMengSocial::getInstance()->doShare("", "", "");
					auto shareLayer = GameShareLayer::create();
					shareLayer->setName("shareLayer");
					shareLayer->SetShareInfo("", "", "");
					shareLayer->onCloseShareCallBack = [=](){
						_RecordShare->setVisible(true);
						_RecordShare->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=](){
							_RecordShare->setVisible(false);
						}), nullptr));
					};
					shareLayer->show();
				});
				//领取
				Button* btn_Reward = dynamic_cast<Button*>(panel_clone->getChildByName("btn_Reward"));
				if (_vec_MatchRecord[i].IsTake == 1)
				{
					btn_share->setVisible(false);
					btn_Reward->setVisible(true);
					btn_Reward->setName(_vec_MatchRecord[i].OrderID);
					btn_Reward->addClickEventListener([=](Ref* ref){
						//申请领取接口
						SendWithdrawalRequest(ref);
						//更新领取完之后的按钮显示状态

					});
				}
				else
				{
					btn_share->setVisible(true);
					btn_Reward->setVisible(false);
				}

				panel_clone->removeFromParentAndCleanup(true);
				list->pushBackCustomItem(panel_clone);
			}
		}
	}

	void GameMatchRegistration::showUpdataShare(Ref* ref)
	{
		Button* btn = (Button*)ref;
		auto Data = (MSG_GP_O_GetContestAwardlist*)btn->getUserData();
		if (_RecordShare)
		{
			auto Image_head = dynamic_cast<ImageView*>(_RecordShare->getChildByName("Image_head"));
			//auto userInfo = RoomLogic()->loginResult.pUserInfoStruct;
			MSG_GP_R_LogonResult& LogonResult = PlatformLogic()->loginResult;
			auto userHead = GameUserHead::create(Image_head);
			userHead->show();
			userHead->loadTexture(LogonResult.bBoy ? "platform/common/boy.png" : "platform/common/girl.png");
			userHead->setHeadByFaceID(LogonResult.bLogoID);
			if (LogonResult.headUrl != ""){
				userHead->loadTextureWithUrl(LogonResult.headUrl);
			}

			auto Sprite_rank = dynamic_cast<Sprite*>(_RecordShare->getChildByName("Sprite_rank"));
			Sprite_rank->setTexture(StringUtils::format("platform/match/res/reward/Imag_Rank_%d.png", Data->iRankNum <= 3 ? Data->iRankNum : 4));
			//string str = StringUtils::format("platform/match/res/reward/Imag_Rank_%d.png", Data->iRankNum <= 3 ? Data->iRankNum : 4);

			auto Text_rank_other = dynamic_cast<Text*>(_RecordShare->getChildByName("Text_rank_other"));
			if (Data->iRankNum > 3)
			{
				Text_rank_other->setString(to_string(Data->iRankNum));
				Text_rank_other->setVisible(true);
			}
			else
			{
				Text_rank_other->setVisible(false);
			}

			string awardtype = "";
			if (Data->iConTestAwardType == 0)
			{
				//金币
				awardtype = "platform/match/res/reward/jb.png";
			}
			else if (Data->iConTestAwardType == 1)
			{
				//红包
				awardtype = "platform/match/res/reward/hb.png";
			}
			else
			{
				//房卡
				awardtype = "platform/match/res/reward/fk.png";
			}
			auto Image_awardtype = dynamic_cast<ImageView*>(_RecordShare->getChildByName("Image_awardtype"));
			Image_awardtype->loadTexture(awardtype);
			Image_awardtype->setVisible(Data->iConTestAward > 0);

			auto Text_awardnum = dynamic_cast<Text*>(_RecordShare->getChildByName("Text_awardnum"));
			if (Data->iConTestAwardType == 0)
			{
				Text_awardnum->setString(StringUtils::format("X%d", Data->iConTestAward));
			}
			else if (Data->iConTestAwardType == 1)
			{
				Text_awardnum->setString(StringUtils::format(GBKToUtf8("X%d元"), Data->iConTestAward));
			}
			else
			{
				Text_awardnum->setString(StringUtils::format("X%d", Data->iConTestAward));
			}
			Text_awardnum->setVisible(Data->iConTestAward > 0);

			auto Text_rank = dynamic_cast<Text*>(_RecordShare->getChildByName("Text_rank"));
			Text_rank->setString(StringUtils::format(GBKToUtf8("第%d名"), Data->iRankNum));

			auto Text_name = dynamic_cast<Text*>(_RecordShare->getChildByName("Text_name"));
			Tools::TrimSpace(LogonResult.nickName);
			string strName = LogonResult.nickName;
			if (strName.length() > 12)
			{
				strName.erase(13, strName.length());
				strName.append("...");
			}
			Text_name->setString(GBKToUtf8(strName));
			//_RecordShare->setVisible(true);
		}
	}

	void GameMatchRegistration::SendWithdrawalRequest(Ref* ref)
	{
		std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/MatchAwardDrawCash.php");

		std::string requestData = "";
		Button* btn = (Button*)ref;
		string str = btn->getName();
		requestData.append(StringUtils::format("&UserID=%d", PlatformLogic()->loginResult.dwUserID));
		requestData.append(StringUtils::format("&OrderID=%s", str.c_str()));

		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("MatchAward", cocos2d::network::HttpRequest::Type::POST, url, requestData);
	}

	void GameMatchRegistration::onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode)
	{
		if (success)
		{
			log("loginMatchRoomSuccess!");
			auto match = MatchInfoCache()->getSelectMatch();
			if (match && m_socketMessage)
			{
				if (m_bFull == 0)
				{
					// 满人赛登陆备战区自动报名
					sendRegistOrRetireInMatch(match->iContestID, true);
				}
				else if (m_bFull == 1)
				{
					// 定时赛点击报名
					bool isTimeOut = false;
					bool isTimeNow = false;
					bool isTimeFuture = false;
					time_t Time = match->beginTime;
					tm *p = gmtime(&Time);
					p->tm_hour += 8;
					if (p->tm_hour >= 24)
					{
						p->tm_hour -= 24;
					}
					int tm_hour = p->tm_hour;
					int tm_min = p->tm_min;

					time_t tt = time(NULL);
					tm* TimeNow = gmtime(&tt);
					TimeNow->tm_hour += 8;
					if (TimeNow->tm_hour >= 24)
					{
						TimeNow->tm_hour -= 24;
					}
					if (TimeNow->tm_hour == tm_hour)
					{
						//赛前两分钟
						if (tm_min - TimeNow->tm_min <= IS_MATCHTIME)
						{
							//比赛已开始
							if (tm_min - TimeNow->tm_min <= 0)
							{
								isTimeOut = true;
							}
							else
							{
								//进入等待
								isTimeNow = true;
							}
						}
						else if (tm_min - TimeNow->tm_min > IS_MATCHTIME)
						{
							//已报名提前两分钟进入
							isTimeFuture = true;
						}
					}
					else if (TimeNow->tm_hour > tm_hour)
					{
						//超过时间
						isTimeOut = true;
					}
					else if (TimeNow->tm_hour < tm_hour)
					{
						if (tm_hour - TimeNow->tm_hour > 1)
						{
							//时间未到
							isTimeFuture = true;
						}
						else
						{
							tm_min += 60;
							if (tm_min - TimeNow->tm_min > IS_MATCHTIME)
							{
								//已报名提前两分钟进入
								isTimeFuture = true;
							}
							else if (tm_min - TimeNow->tm_min <= IS_MATCHTIME)
							{
								//进入等待
								isTimeNow = true;
							}
						}
					}
					if (isTimeNow)
					{
						RoomLogic()->close();
						RoomLogic()->setRoomRule(0);
						auto pMatchWaitingLayer = GameMatchWaiting::createMatchWaiting(match->iContestID, false);
						pMatchWaitingLayer->updateMatchInfo(m_socketMessage);
						if (m_socketMessage)
						{
							HNSocketMessage::releaseMessage(m_socketMessage);
							m_socketMessage = nullptr;
						}
					}
					else
					{
						sendRegistOrRetireInPlatform(match->iContestID, true);
						RoomLogic()->setSelectedRoom(nullptr);
					}
				}
				else if (m_bFull == IS_MATCHREGISTRATION)
				{				
					auto pMatchWaitingLayer = GameMatchWaiting::createMatchWaiting(match->iContestID, true);
					pMatchWaitingLayer->updateMatchInfo(m_socketMessage);
					if (m_socketMessage)
					{
						HNSocketMessage::releaseMessage(m_socketMessage);
						m_socketMessage = nullptr;
					}
				}
				//定时赛
				else if (m_bFull == IS_MATCHTIMESTRATION)
				{
					bool isTimeOut = false;
					bool isTimeNow = false;
					bool isTimeFuture = false;
					time_t Time = match->beginTime;
					tm *p = gmtime(&Time);
					p->tm_hour += 8;
					if (p->tm_hour >= 24)
					{
						p->tm_hour -= 24;
					}
					int tm_hour = p->tm_hour;
					int tm_min = p->tm_min;

					time_t tt = time(NULL);
					tm* TimeNow = gmtime(&tt);
					TimeNow->tm_hour += 8;
					if (TimeNow->tm_hour >= 24)
					{
						TimeNow->tm_hour -= 24;
					}
					if (TimeNow->tm_hour == tm_hour)
					{
						//赛前两分钟
						if (tm_min - TimeNow->tm_min <= IS_MATCHTIME)
						{
							//比赛已开始
							if (tm_min - TimeNow->tm_min <= 0)
							{
								isTimeOut = true;
							}
							else
							{
								//进入等待
								isTimeNow = true;
							}
						}
						else if (tm_min - TimeNow->tm_min > IS_MATCHTIME)
						{
							//已报名提前两分钟进入
							isTimeFuture = true;
						}
					}
					else if (TimeNow->tm_hour > tm_hour)
					{
						//超过时间
						isTimeOut = true;
					}
					else if (TimeNow->tm_hour < tm_hour)
					{
						if (tm_hour - TimeNow->tm_hour > 1)
						{
							//时间未到
							isTimeFuture = true;
						}
						else
						{
							tm_min += 60;
							if (tm_min - TimeNow->tm_min > IS_MATCHTIME)
							{
								//已报名提前两分钟进入
								isTimeFuture = true;
							}
							else if (tm_min - TimeNow->tm_min <= IS_MATCHTIME)
							{
								//进入等待
								isTimeNow = true;
							}
						}
					}
					if (isTimeNow)
					{
						RoomLogic()->close();
						RoomLogic()->setRoomRule(0);
						auto pMatchWaitingLayer = GameMatchWaiting::createMatchWaiting(match->iContestID, false);
						pMatchWaitingLayer->updateMatchInfo(m_socketMessage);
						if (m_socketMessage)
						{
							HNSocketMessage::releaseMessage(m_socketMessage);
							m_socketMessage = nullptr;
						}
					}
					else
					{
						updateTimingInfo(match);
					}
				}
			}
		}
		else
		{
			_roomLogic->stop();
			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(message);
			prompt->setCallBack([=](){
				_roomLogic->start();
				_roomLogic->requestLogin(roomID);
			});

			prompt->setCancelCallBack([=](){ close(); });
		}
	}

	void GameMatchRegistration::onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		_roomLogic->stop();
		if (success)
		{
			// 启动游戏
			bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
			if (!ret)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("游戏启动失败。"));
				prompt->setCallBack([=](){ close(); });
			}
		}
		else
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(message);
			prompt->setCallBack([=](){ close(); });
		}
	}
	// 检测资源更新
	void GameMatchRegistration::checkResUpdate(int roomID, int gameID)
	{
		// 检测是否需要更新
		auto update = GameResUpdate::create();
		update->retain();
		update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {

			if (success)
			{
				bool bLocation = true;  //默认开启定位
				if (bLocation)
				{
					GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr) {
						if (success)
						{
							_roomLogic->start();
							_roomLogic->requestLogin(roomID, latitude, longtitude, addr);
						}
						else
						{
							//不强制开启定位
							_roomLogic->start();
							_roomLogic->requestLogin(roomID);
							/*
							RoomLogic()->setRoomRule(0);
							RoomLogic()->setSelectedRoom(nullptr);
							RoomLogic()->close();
							//返回游戏大厅
							GamePlatform::returnPlatform(LayerType::PLATFORM);
							MessageBox(GBKToUtf8("定位失败，请在[设置]中打开定位服务或者检查网络!"), GBKToUtf8("提示"));
							*/
						}
					};
					GameLocation::getInstance()->getLocation();
				}
				else
				{
					_roomLogic->start();
					_roomLogic->requestLogin(roomID);
				}
			}
			else
			{
				RoomLogic()->setRoomRule(0);
				RoomLogic()->setSelectedRoom(nullptr);
				RoomLogic()->close();
				//返回游戏大厅
				GamePlatform::returnPlatform(LayerType::PLATFORM);
			}

			update->release();
		};
		update->checkUpdate(gameID, true);
	}

	void GameMatchRegistration::getMatchInfo(const int& iContestID,bool isRulePage /*= false*/, BYTE isFull/* = false*/)
	{
		m_bFull = isFull;
		m_bRulePage = isRulePage;

		MSG_GP_GetContestRoomID get;
		get.iContestID = iContestID;
		get.iUserID = PlatformLogic()->loginResult.dwUserID;

		//如果之前有房间链接未断开，请求房间信息后端默认恢复当前链接房间ID信息，故先做断开操作
		RoomLogic()->close();
		RoomLogic()->setRoomRule(0);

		PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_GET_CONTEST_ROOMID, (void*)&get, sizeof(get),
			HN_SOCKET_CALLBACK(GameMatchRegistration::getMatchInfoCallback, this));
	}
}

	