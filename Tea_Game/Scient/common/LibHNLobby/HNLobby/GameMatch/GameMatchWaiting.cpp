/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameMatchWaiting.h"
#include "HNPlatformLogic/HNGameCreator.h"
#include "../GameBaseLayer/GamePlatform.h"
#include "../GameUpdate/GameResUpdate.h"
#include "../GameMatch/GameMatchRegistration.h"
#include "../GameLocation/GameLocation.h"

namespace HN
{
	static const char* MANCHU_MATCH_PATH = "platform/matchRegist/res/Manchu_match/Manchu_matchLayer.csb";
	static const char* TIMING_MATCH_PATH = "platform/matchRegist/res/Manchu_match/Timing_matchLayer.csb";

	GameMatchWaiting* GameMatchWaiting::createMatchWaiting(int matchID, bool isFull/* = false*/)
	{
		Scene* scene = Scene::create();
		auto mainlayer = GameMatchWaiting::create(matchID, isFull);
		scene->addChild(mainlayer);
		//本场景变暗消失后另一场景慢慢出现
		//Director::getInstance()->replaceScene(TransitionFade::create(0.3f, scene));
		Director::getInstance()->replaceScene(scene);
		return mainlayer;
	}

	GameMatchWaiting* GameMatchWaiting::create(int matchID, bool isFull)
	{
		auto pRet = new (std::nothrow) GameMatchWaiting();
		if (pRet && pRet->init(matchID, isFull))
		{
			pRet->autorelease();
		}
		else
		{
			CC_SAFE_DELETE(pRet);
		}
		return pRet;
	}

	bool GameMatchWaiting::init(int matchID, bool isFull)
	{
		if (!HNLayer::init()) return false;

		HNPlatformConfig()->setSceneState(PlatformConfig::SCENE_STATE::INMATCH);

		_matchID = matchID;

		_isFull = isFull;

		float scalex = 1280 / _winSize.width;
		float scaley = 720 / _winSize.height;

		if (!_isFull)
		{
			getMatchInfo(matchID);
		}

		auto node = CSLoader::createNode(MANCHU_MATCH_PATH);
		addChild(node);

		_imgBG = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
		_imgBG->setScale(_winSize.width / 1280, _winSize.height / 720);

		if (_winSize.width / _winSize.height < 1.78f)
		{
			for (auto child : _imgBG->getChildren())
			{
				child->setScale(scalex, scaley);
			}
		}
		else
		{
			for (auto child : _imgBG->getChildren())
			{
				child->setScale(0.9f, scaley * 0.9f);
			}
		}

		/*if (_isFull)
		{*/
			// 退赛按钮
			_btn_retire = dynamic_cast<Button*>(_imgBG->getChildByName("Button_close"));
			//_btn_retire->setVisible(false);
			_btn_retire->addClickEventListener([=](Ref*){
				sendRegistOrRetire(false);
			});

			updateUserCount();
		//}
		//else
		//{
		//	//定时赛提示开始时间
		//	_btn_retire = dynamic_cast<Button*>(_imgBG->getChildByName("Button_sure"));
		//	//_btn_retire->setVisible(false);
		//	_btn_retire->addClickEventListener([=](Ref*){
		//		_btn_retire->setEnabled(false);
		//		close();
		//	});
		//}

		//// 关闭
		//auto closeBtn = dynamic_cast<Button*>(_imgBG->getChildByName("Button_close"));
		//closeBtn->addClickEventListener([=](Ref*){
		//	//满人赛退出房间会被退赛，所以需要有提示
		//	if (_isFull)
		//	{
		//		auto prompt = GamePromptLayer::create(true);
		//		prompt->showPrompt(GBKToUtf8("满人赛退出备战区将退赛\n确认退出？"));
		//		prompt->setCallBack([=](){
		//			close();
		//		});
		//	}
		//	else
		//	{
		//		closeBtn->setEnabled(false);
		//		close();
		//	}
		//});

		// 开赛提示
		_img_tip = dynamic_cast<ImageView*>(_imgBG->getChildByName("Image_tip"));
		_img_tip->setVisible(false);

		// 重连成功通知
		_listener = EventListenerCustom::create(RECONNECTION, [=](EventCustom* event) {

			if (_isFull)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("网络中断已退赛，请重新报名。"));
				prompt->setCallBack([=]() {
					GamePlatform::createPlatform();
				});
			}
		});
		Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);

		// 切回前台通知
		_foreground = EventListenerCustom::create(FOCEGROUND, [=](EventCustom* event) {
			//等待比赛开始界面切后台默认退赛处理
			close();
		});

		Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_foreground, 1);

		// 监听比赛取消消息
		contestAbandonListener();

		// 监听比赛开赛消息
		contestInitListener();
		return true;
	}

	void GameMatchWaiting::onEnterTransitionDidFinish()
	{
		
	}

	GameMatchWaiting::GameMatchWaiting()
	{
		_roomLogic = new HNRoomLogicBase(this);
	}

	GameMatchWaiting::~GameMatchWaiting()
	{
		stopAllActions();
		_roomLogic->stop();
		HN_SAFE_DELETE(_roomLogic);
		RoomLogic()->removeEventSelector(MDM_GR_ROOM, ASS_GR_INIT_CONTEST);
		RoomLogic()->removeEventSelector(MDM_GR_USER_ACTION, ASS_GR_CONTEST_APPLY);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_GET_CONTEST_ROOMID);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_GET_APPLY_NUM);
		Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
		Director::getInstance()->getEventDispatcher()->removeEventListener(_foreground);
	}


	void GameMatchWaiting::close()
	{
		RoomLogic()->close();
		RoomLogic()->setRoomRule(0);
		GamePlatform::createPlatform();
	}

	void GameMatchWaiting::updateUserCount()
	{
		PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_GET_APPLY_NUM, &_matchID, sizeof(int), [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(MSG_GP_UpdateApplyNum, socketMessage->objectSize, true, "MSG_GP_UpdateApplyNum size is error");
			MSG_GP_UpdateApplyNum *info = (MSG_GP_UpdateApplyNum*)socketMessage->object;

			if (_isFull)
			{
				//更新提示标题（满人==报名成功）
				auto tip_yes = dynamic_cast<Sprite*>(_imgBG->getChildByName("tip_yes"));
				tip_yes->setTexture("platform/matchRegist/res/Manchu_match/tip_yes.png");
				Layout* layout = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Player"));
				layout->setVisible(true);
				Layout* layoutTime = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Time"));
				layoutTime->setVisible(false);
				auto textPeople = dynamic_cast<Text*>(layout->getChildByName("Text_current_player"));
				if (textPeople)
				{
					//_current = info->iApplyNum;
					textPeople->setString(StringUtils::format(GBKToUtf8("%d人,"), info->iApplyNum));

					auto text_middle = dynamic_cast<Text*>(layout->getChildByName("Text_middle"));
					text_middle->setPositionX(textPeople->getPositionX() + textPeople->getContentSize().width);

					auto Text_residue_player = dynamic_cast<Text*>(layout->getChildByName("Text_residue_player"));
					if (_NeetNum != 0)
					{
						Text_residue_player->setString(to_string(_NeetNum - info->iApplyNum));
					}
					else
					{
						Text_residue_player->setString(to_string(info->iApplyNum));
					}
					Text_residue_player->setPositionX(text_middle->getPositionX() + text_middle->getContentSize().width);

					auto Text_end = dynamic_cast<Text*>(layout->getChildByName("Text_end"));
					Text_end->setPositionX(Text_residue_player->getPositionX() + Text_residue_player->getContentSize().width);
				}
			}
			else
			{
				//更新提示标题（定时==即将开赛）
				auto tip_yes = dynamic_cast<Sprite*>(_imgBG->getChildByName("tip_yes"));
				tip_yes->setTexture("platform/matchRegist/res/Manchu_match/jjks.png");
				Layout* layout = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Player"));
				layout->setVisible(false);
				Layout* layoutTime = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Time"));
				layoutTime->setVisible(true);

				auto textPeople = dynamic_cast<Text*>(layoutTime->getChildByName("Text_current_player"));
				if (textPeople)
				{
					textPeople->setString(StringUtils::format(GBKToUtf8("%d人,"), info->iApplyNum));
					auto textPlayTime = dynamic_cast<Text*>(layoutTime->getChildByName("Text_player_time"));
					textPlayTime->setString(StringUtils::format("%d:%02d", _Time_H, _Time_M));
				}
			}

			return true;
		});

		// 间隔2秒循环查询已报名人数信息
		this->runAction(Sequence::create(DelayTime::create(2.0f), CallFunc::create([=](){
			if (PlatformLogic()->isConnect()) updateUserCount();
		}), nullptr));
	}


	bool GameMatchWaiting::updateMatchInfo(HNSocketMessage* socketMessage)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_GetContestRoomID_Result, socketMessage->objectSize, true, "MSG_GP_GetContestRoomID_Result size is error");
		MSG_GP_GetContestRoomID_Result* info = (MSG_GP_GetContestRoomID_Result*)socketMessage->object;

		// 赛场名称
		auto textName = dynamic_cast<Text*>(_imgBG->getChildByName("Text_name"));
		if (textName != nullptr)
		{
			string str_game = "";
			if (info->tRoomInfo.uNameID == 20171121)
			{
				str_game = "游金麻将";
			}
			else if (info->tRoomInfo.uNameID == 20171123)
			{
				str_game = "晋江麻将";
			}
			else if (info->tRoomInfo.uNameID == 10100003)
			{
				str_game = "斗地主";
			}
			textName->setString(GBKToUtf8(StringUtils::format("%s(%s)", info->tRoomInfo.szGameRoomName, str_game.c_str())));
		}
		_NeetNum = info->tRoomInfo.iUpPeople;
		//判定是否为满人赛还是定时赛
		if (info->tContestInfo.iContestType == 0)
		{
			_isFullMatch = true;
		}
		else
		{
			_isFullMatch = false;
			//定时赛需要执行报名操作
			//sendRegistOrRetire(true);
		}

		// 开赛条件
		std::string str;
		if (info->tContestInfo.iContestType >= 1)
		{
			time_t Time = info->tContestInfo.iContestTime;
			struct tm *p = gmtime(&Time);
			p->tm_hour += 8;
			if (p->tm_hour >= 24)
			{
				p->tm_hour -= 24;
			}
			_Time_H = p->tm_hour;
			_Time_M = p->tm_min;
		}

		// 已报名人数
		if (_isFullMatch)
		{
			//更新提示标题（满人==报名成功）
			auto tip_yes = dynamic_cast<Sprite*>(_imgBG->getChildByName("tip_yes"));
			tip_yes->setTexture("platform/matchRegist/res/Manchu_match/tip_yes.png");
			Layout* layout = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Player"));
			layout->setVisible(true);
			Layout* layoutTime = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Time"));
			layoutTime->setVisible(false);
			auto textPeople = dynamic_cast<Text*>(layout->getChildByName("Text_current_player"));
			if (textPeople != nullptr)
			{
				textPeople->setString(StringUtils::format(GBKToUtf8("%d人,"), info->tContestInfo.iContestNum));
				auto text_middle = dynamic_cast<Text*>(layout->getChildByName("Text_middle"));
				text_middle->setPositionX(textPeople->getPositionX() + textPeople->getContentSize().width);

				auto Text_residue_player = dynamic_cast<Text*>(layout->getChildByName("Text_residue_player"));
				Text_residue_player->setString(to_string(info->tRoomInfo.iUpPeople - info->tContestInfo.iContestNum));
				Text_residue_player->setPositionX(text_middle->getPositionX() + text_middle->getContentSize().width);

				auto Text_end = dynamic_cast<Text*>(layout->getChildByName("Text_end"));
				Text_end->setPositionX(Text_residue_player->getPositionX() + Text_residue_player->getContentSize().width);
			}
		}
		else
		{
			//更新提示标题（定时==即将开赛）
			auto tip_yes = dynamic_cast<Sprite*>(_imgBG->getChildByName("tip_yes"));
			tip_yes->setTexture("platform/matchRegist/res/Manchu_match/jjks.png");
			Layout* layout = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Player"));
			layout->setVisible(false);
			Layout* layoutTime = dynamic_cast<Layout*>(_imgBG->getChildByName("panel_Time"));
			layoutTime->setVisible(true);

			auto textPeople = dynamic_cast<Text*>(layoutTime->getChildByName("Text_current_player"));
			if (textPeople)
			{
				textPeople->setString(StringUtils::format(GBKToUtf8("%d人,"), info->tContestInfo.iContestNum));
				auto textPlayTime = dynamic_cast<Text*>(layoutTime->getChildByName("Text_player_time"));
				textPlayTime->setString(StringUtils::format("%d:%02d", _Time_H, _Time_M));
			}
		}

		// 前三名奖励提示
		//if (_isFull)
		//{
		for (int i = 0; i < 3; i++)
		{
			auto matchReward = dynamic_cast<ImageView*>(_imgBG->getChildByName(StringUtils::format("Image_reward_%d", i + 1)));
			auto Text_reward_type = dynamic_cast<Text*>(matchReward->getChildByName("Text_reward_type"));
			auto Text_reward_num = dynamic_cast<Text*>(matchReward->getChildByName("Text_reward_num"));
			if (info->tContestInfo.iAwardType[i] == 0)
			{
				Text_reward_type->setString(GBKToUtf8("金币"));
				Text_reward_num->setString(to_string(info->tContestInfo.iRankAward[i]));
			}
			else if (info->tContestInfo.iAwardType[i] == 2)
			{
				Text_reward_type->setString(GBKToUtf8("房卡"));
				Text_reward_num->setString(StringUtils::format(GBKToUtf8("%d张"), info->tContestInfo.iRankAward[i]));
			}
			else
			{
				Text_reward_type->setString(GBKToUtf8("红包"));
				Text_reward_num->setString(StringUtils::format(GBKToUtf8("%d元"),info->tContestInfo.iRankAward[i]));
			}
			if (info->tContestInfo.iRankAward[i] == 0)
			{
				matchReward->setVisible(false);
			}
		}
		//}

		if (!_isFull)
		{
			RoomInfoModule()->addRoom(&info->tRoomInfo);
			auto room = RoomInfoModule()->findRoom(info->tRoomInfo.uRoomID);
			RoomLogic()->setRoomRule(room->dwRoomRule);
			RoomLogic()->setSelectedRoom(room);

			//在登录房间前进行资源更新
			checkResUpdate(room->uRoomID, room->uNameID);
		}

		return true;
	}
	void GameMatchWaiting::sendRegistOrRetire(bool isRegist)
	{
		MSG_GP_ContestApply registration;
		registration.iContestID = _matchID;
		registration.iUserID = PlatformLogic()->loginResult.dwUserID;
		registration.iType = !isRegist;

		// 满人赛报名必须是走房间报名消息
		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_CONTEST_APPLY, (void*)&registration, sizeof(registration),
			HN_SOCKET_CALLBACK(GameMatchWaiting::retireCallback, this));

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("请稍候..."), 25);
	}
	bool GameMatchWaiting::retireCallback(HNSocketMessage* socketMessage)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		CHECK_SOCKET_DATA_RETURN(MSG_GP_ContestApply_Result, socketMessage->objectSize, true, "MSG_GP_ContestApply_Result size is error");
		MSG_GP_ContestApply_Result* applyResult = (MSG_GP_ContestApply_Result*)socketMessage->object;

		if (1 == applyResult->bResult)
		{
			//GamePromptLayer::create()->showPrompt(GBKToUtf8("报名成功，等待比赛开始。"));
			_btn_retire->setVisible(true);

			//// 监听比赛取消消息
			//contestAbandonListener();

			//// 监听比赛开赛消息
			//contestInitListener();
		}
		else if (3 == applyResult->bResult)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("已经报名，无需再次报名"));
			_btn_retire->setVisible(true);
		}
		else
		{
			std::string str("");
			switch (applyResult->bResult)
			{
			case 0:
				str = "满人赛不能在大厅中报名。"; break;
			case 2:
				RoomLogic()->setSelectedRoom(nullptr);
				str = "退赛成功，即将离开备战区。"; break;
			case 4:
				str = "比赛已经开始，无法报名。"; break;
			case 5:
				str = "未报名，退赛失败。"; break;
			case 6:
				str = "比赛已开始，无法退赛。"; break;
			case 7:
				str = "达到最大报名人数，无法报名。"; break;
			case 8:
				str = "金币不足，无法报名。"; break;
			case 9:
				str = "比赛房间未开启。"; break;
			case 10:
				str = "用户不存在。"; break;
			case 11:
				str = "比赛ID错误。"; break;
			case 12:
				str = "免费次数已用完，报名失败。"; break;
			case 13:
				str = "房卡不足，无法报名。"; break;
			default:
				str = StringUtils::format("未知错误。(%d)",applyResult->bResult); break;
			}

			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8(str.c_str()));
			prompt->setCallBack([=](){ close(); });
		}

		return true;
	}

	// 比赛取消监听
	void GameMatchWaiting::contestAbandonListener()
	{
		RoomLogic()->addEventSelector(MDM_GR_ROOM, ASS_GR_CONTEST_ABANDON, [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(MSG_GR_ContestAbandon, socketMessage->objectSize, true, "MSG_GR_ContestAbandon size is error");
			MSG_GR_ContestAbandon *info = (MSG_GR_ContestAbandon*)socketMessage->object;

			if (info->iUserID == PlatformLogic()->loginResult.dwUserID)
			{
				PlatformLogic()->loginResult.i64Money = info->i64WalletMoney;
				PlatformLogic()->loginResult.iJewels = info->iJewels;
			}

			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("当前报名人数不足，比赛取消。"));
			prompt->setCallBack([=](){ close(); });

			RoomLogic()->removeEventSelector(MDM_GR_ROOM, ASS_GR_CONTEST_ABANDON);

			return true;
		});
	}

	// 比赛即将开赛监听
	void GameMatchWaiting::contestInitListener()
	{
		RoomLogic()->addEventSelector(MDM_GR_ROOM, ASS_GR_INIT_CONTEST, [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(MSG_GR_ContestChange, socketMessage->objectSize, true, "MSG_GR_ContestChange size is error");

			MSG_GR_ContestChange *contestChange = (MSG_GR_ContestChange*)socketMessage->object;

			const UserInfoStruct* pUserInfo = UserInfoModule()->findUser(contestChange->dwUserID);
			if (nullptr != pUserInfo)
			{
				UserInfoStruct userInfo = *pUserInfo;
				userInfo.iRankNum = contestChange->iRankNum;				// 排行名次
				userInfo.iContestCount = contestChange->iContestCount;		// 比赛局数
				UserInfoModule()->updateUser(&userInfo);
			}

			RoomLogic()->loginResult.pUserInfoStruct.iRankNum = contestChange->iRankNum;// 排行名次
			RoomLogic()->loginResult.iRemainPeople = contestChange->iRemainPeople;		// 比赛中还剩下的人数

			_img_tip->setVisible(true);
			_img_tip->runAction(CallFunc::create([=](){//Sequence::create(DelayTime::create(2.0f), CallFunc::create([=](){
			
				// 启动游戏
				bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, INVALID_DESKNO, false);
				if (!ret)
				{
					auto prompt = GamePromptLayer::create();
					prompt->showPrompt(GBKToUtf8("游戏启动失败。"));
					prompt->setCallBack([=](){ close(); });
				}
			}));

			_btn_retire->setVisible(false);

			auto closeBtn = dynamic_cast<Button*>(_imgBG->getChildByName("Button_close"));
			closeBtn->setVisible(false);

			RoomLogic()->removeEventSelector(MDM_GR_ROOM, ASS_GR_INIT_CONTEST);

			return true;
		});
	}
	
	void GameMatchWaiting::getMatchInfo(const int& iContestID)
	{
		//m_bFull = isFull;
		//m_bRulePage = isRulePage;

		MSG_GP_GetContestRoomID get;
		get.iContestID = iContestID;
		get.iUserID = PlatformLogic()->loginResult.dwUserID;

		PlatformLogic()->sendData(MDM_GP_CONTEST, ASS_GP_GET_CONTEST_ROOMID, (void*)&get, sizeof(get),
			HN_SOCKET_CALLBACK(GameMatchWaiting::updateMatchInfo, this));
	}

	// 检测资源更新
	void GameMatchWaiting::checkResUpdate(int roomID, int gameID)
	{
		// 检测是否需要更新
		auto update = GameResUpdate::create();
		update->retain();
		update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {

			if (success)
			{
				bool bLocation = true;  //默认开启定位
				if (bLocation)
				{
					GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr) {
						if (success)
						{
							_roomLogic->start();
							_roomLogic->requestLogin(roomID, latitude, longtitude, addr);
						}
						else
						{
							//不强制开启定位
							_roomLogic->start();
							_roomLogic->requestLogin(roomID);
							/*
							RoomLogic()->setRoomRule(0);
							RoomLogic()->setSelectedRoom(nullptr);
							RoomLogic()->close();
							//返回游戏大厅
							GamePlatform::returnPlatform(LayerType::PLATFORM);
							MessageBox(GBKToUtf8("定位失败，请在[设置]中打开定位服务或者检查网络!"), GBKToUtf8("提示"));
							*/
						}
					};
					GameLocation::getInstance()->getLocation();
				}
				else
				{
					_roomLogic->start();
					_roomLogic->requestLogin(roomID);
				}
			}
			else
			{
				RoomLogic()->setRoomRule(0);
				RoomLogic()->setSelectedRoom(nullptr);
				RoomLogic()->close();
				//返回游戏大厅
				GamePlatform::returnPlatform(LayerType::PLATFORM);
			}

			update->release();
		};
		update->checkUpdate(gameID, true);
	}

	void GameMatchWaiting::onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode)
	{
		if (success)
		{
			sendRegistOrRetire(true);
		}
		else
		{
			_roomLogic->stop();
			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(message);
			prompt->setCallBack([=](){
				_roomLogic->start();
				_roomLogic->requestLogin(roomID);
			});

			prompt->setCancelCallBack([=](){ close(); });
		}
	}
}
