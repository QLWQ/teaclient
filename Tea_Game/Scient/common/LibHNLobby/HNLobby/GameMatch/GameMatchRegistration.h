/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_GameMatchRegistration_h__
#define HN_GameMatchRegistration_h__

#include "cocos2d.h"
#include "HNNetExport.h"
#include "HNRoomLogic/HNRoomLogicBase.h"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"

USING_NS_CC;

using namespace ui;
using namespace cocostudio;
using namespace cocos2d::network;

namespace HN
{
	
	class GameMatchRegistration : public HNLayer, public TableViewDelegate, public TableViewDataSource, public HNHttpDelegate, public HN::IHNRoomLogicBase

	{
	public:
		typedef std::function<void()> CloseCallBack;
		CloseCallBack	onCloseCallBack;

		typedef std::function<int()> ReturniGameId;
		ReturniGameId	_ReturniGameId;

		enum Selectedreward
		{
			GOLD = 0,	//金币
			MONEY,		//现金
			ROOMCARD,	//房卡
			TELEPHONEBILL, //话费
			REDENVELOPES, //红包
		}_Selectedreward;

		void setChangeDelegate(MoneyChangeNotify* delegate);

	public:
		GameMatchRegistration();

		~GameMatchRegistration();

		virtual bool init() override;

		CREATE_FUNC(GameMatchRegistration);
		
	private:
		void closeFunc() override;

		// 报名弹出框
		void showMatchRegistPopup(MSG_GP_ContestApplyInfo* info);

		// 未在报名时间状态弹出框（定时赛）
		void showMatchTimeOutPopup(MSG_GP_ContestApplyInfo* info);

		// 比赛详细信息
		void showMatchDetails();

		// 获奖记录详情
		void showMatchRecord();

		// 根据当前分享按钮更新分享内容
		void showUpdataShare(Ref* ref);
	private:
		// 获取赛期列表
		void getMatchList();

		// 参赛/退赛
		void sendRegistOrRetireInPlatform(int matchID, bool isRegist);

		//比赛场 参赛/退赛
		void sendRegistOrRetireInMatch(int matchID, bool isRegist);
	private:
		// 获取比赛列表信息结果
		bool getMatchListCallback(HNSocketMessage* socketMessage);

		// 获取已报名玩家信息结果
		bool getApplyNumCallback(HNSocketMessage* socketMessage);

		// 参赛/退赛结果
		bool registOrRetireInPlatformCallback(HNSocketMessage* socketMessage);

		//比赛场 参赛/退赛结果
		bool registOrRetireInMatchCallback(HNSocketMessage* socketMessage);

		//获取背包信息
		bool getBagList(HNSocketMessage* socketMessage);
		//请求房卡兑换
		bool exchangeDiamondsCallBack(HNSocketMessage* socketMessage);
		//获取比赛房间信息
		bool getMatchInfoCallback(HNSocketMessage* socketMessage);

		//获取剩余免费次数
		bool getMatchInfoNumCallback(HNSocketMessage* socketMessage);

		//获取获奖记录
		bool getMatchRecordCallback(HNSocketMessage* socketMessage);

	private:
		// 登陆房间回调
		virtual void onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode) override;

		// 排队坐下回调
		virtual void onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo) override;

		// 获取比赛信息
		void getMatchInfo(const int& iContestID, bool isRulePage = false, BYTE isFull = 0);

		void checkResUpdate(int roomID, int gameID);
	private:
		void createMatchList();

		void createMatchPage(std::vector<MSG_GP_ContestApplyInfo*> pages);

		Button*	createMatchItem(MSG_GP_ContestApplyInfo* info);

		Node* createMatchItemNode(MSG_GP_ContestApplyInfo* info);
		// 更新比赛信息
		void updateMatchInfo(Sprite* matchBtn);

		// 更新玩家信息
		void updateUserInfo();

		// 更新定时赛报名成功
		void updateTimingInfo(MSG_GP_ContestApplyInfo* info);
	private:
		//创建兑换列表
		void createExchange();
		//兑换界面
		void inerFaceExchange(int index);
		//请求Http数据
		void creatHttprequest(string data);
		//http请求响应
		void onHttpResponseBag(HttpClient *sender, HttpResponse *response);
		void onHttpResponseRecord(HttpClient *sender, HttpResponse *response);

		//http请求领取
		void SendWithdrawalRequest(Ref* ref);

		//网络回调
		void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData);
		//判断是否拥有兑换券
		bool isHaveExchange(int number);
		//计算背包拥有兑换卷的数量
		int isHaveBag(int number);
		//兑换操作页面
		void ExchangeIng(bool condition);
		
		//添加Cell
		virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

		//设置cell数量
		virtual ssize_t numberOfCellsInTableView(TableView* table);

		//设置cell大小
		virtual Size tableCellSizeForIndex(TableView* table, ssize_t idx);

		//设置触摸函数
		virtual void tableCellTouched(TableView* table, TableViewCell* cell);

		//背包页面
		void BagPage();
		//规则页面
		void RulePage(MSG_GP_ContestApplyInfo* info);
		//记录页面
		void RecordPage();

		

		//复选框
		void checkBoxCallback(cocos2d::Ref * ref, cocos2d::ui::CheckBox::EventType type);

		void checkBoxCallbackReward(cocos2d::Ref * ref, cocos2d::ui::CheckBox::EventType type);
		void checkBoxCallbackCurrency(cocos2d::Ref * ref, cocos2d::ui::CheckBox::EventType type);
	protected:
		HNRoomLogicBase*		_roomLogic = nullptr;
		ui::ScrollView*				_matchList = nullptr;// 房间列表
		TableView*                _exchangeItems = nullptr; //房间列表
		Layout*					_applyLayer = nullptr;
		Layout*                   _panel_di = nullptr; //话费 现金兑换
		std::vector<Sprite*>	_matchVec;
		bool					_isJoin = false;
		TextAtlas*				_text_gold = nullptr;	//金币
		TextAtlas*				_text_lottery = nullptr;//奖券
		Button*					_btn_apply = nullptr;	//报名按钮
		Button*					_btn_retire = nullptr;	//退赛按钮
		Button*					_btn_enter = nullptr;	//进入按钮
		int						_contestID = 0;			//当前选择的比赛场id
		Vector<Button*>	      ButtonExChange;
		Node* _Exchangenode = nullptr;          //兑换界面
		Node* _TwoExchangeNode = nullptr;       //兑换二级界面
		std::vector<int>        _numberForCell ;

		std::vector<string>   _BagDinDan;                      //订单号
		std::map<string, int>   _mapBag;                //订单号和钱数
		string                   _DingDanHao;  //订单号
		Node*            _Bag_match = nullptr;             //背包页面
		ui::ScrollView* _PlaneXianJin = nullptr;       //现金页面
		ui::ScrollView* _PlaneHuaFei = nullptr;       //话费页面

		Node*            _RuleMatch = nullptr;      //规则页面
		Node*			_RecordMatch = nullptr;		//获奖记录界面
		ImageView*		_RecordShare = nullptr;		//获奖记录分享页面
		//记录当前规则页面奖励人数
		int				_RuleRecordNum = 0;

		vector<MSG_GP_O_GetContestAwardlist>		_vec_MatchRecord;
		vector<MSG_GP_ContestApplyInfo*>			_vec_MatchFirstInfo;
		vector<MSG_GP_ContestApplyInfo*>			_vec_MatchInfo;
		vector<MSG_GP_ContestApplyInfo*>			_vec_MatchLastInfo;

		MoneyChangeNotify*				_delegate;
		ui::ScrollView*                  _RecordList;
	
		int m_iContestID;
		BYTE m_bFull;	
		int m_bRulePage;
		HNSocketMessage* m_socketMessage;

		//设置点击报名的冷却状态
		bool m_bCanTouch = true;
	};
}

#endif //HN_GameMatchRegistration_h__

