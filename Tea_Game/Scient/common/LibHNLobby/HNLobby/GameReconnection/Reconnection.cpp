/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/


#include "Reconnection.h"
#include "../GameBaseLayer/GamePlatform.h"
#include "../GameMenu/GameMenu.h"
#include "../GameNotifyManger/GameNotifyCenter.h"
#include "../GameLocation/GameLocation.h"
#include "../GameUpdate/GameResUpdate.h"
#include "../GameNotice/Mail/HNMailData.h"

namespace HN
{
	static Reconnection* logic = nullptr;

	Reconnection* Reconnection::getInstance()
	{
		if (nullptr == logic)
		{
			logic = new Reconnection();
			logic->init();
		}
		return logic;
	}

	void Reconnection::destroyInstance()
	{
		HN_SAFE_DELETE(logic);
	}

	Reconnection::Reconnection()
	{
		_gameLogin = new HNPlatformLogin(this);
		_roomLogic = new HNRoomLogicBase(this);
	}

	Reconnection::~Reconnection()
	{
		_gameLogin->stop();
		_roomLogic->stop();
		HN_SAFE_DELETE(_gameLogin);
		HN_SAFE_DELETE(_roomLogic);

		PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GET_CUTROOM);
		PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_ENTER_DESK);

		Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
		Director::getInstance()->getEventDispatcher()->removeEventListener(_foreground);
	}

	void Reconnection::init()
	{
		// 断线通知
		_listener = EventListenerCustom::create(DISCONNECT, [=](EventCustom* event){
			if (_isOpen) reConnection(true);
		});

		Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);

		// 切回前台通知
		_foreground = EventListenerCustom::create(FOCEGROUND, [=](EventCustom* event) {
			
			enterForeground();

			shareDeskInfo();
		});

		Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_foreground, 1);
	}

	void Reconnection::saveInfoAndCheckReconnection(const std::string &userName, const std::string &passWord)
	{
		_uName = userName;
		_pWord = passWord;

		// 重置参数
		resetParams();

		// 检查有没有重连状态
		checkReconnection();

		// 开始全局广播消息监听
		GameNotifyCenter::getInstance()->startListening();

		// 初始化邮件数据
		MailData()->getMailsInfo();
	}

	void Reconnection::checkReconnection()
	{
		if (0 == PlatformLogic()->loginResult.iCutRoomID)//如果不是断线重连，直接进大厅
		{
			if (_isVipRoom)
			{
				if (RoomLogic()->getSelectedRoom()) loginRoom(RoomLogic()->getSelectedRoom());
			}
			else if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("当前游戏已经结束，请重新排队"));
				prompt->setCallBack([=](){
					//UserDefault::getInstance()->setBoolForKey("Reconnection", false);
					GamePlatform::createPlatform();
				});
			}
			else if (RoomLogic()->getRoomRule() & GRR_CONTEST
				|| RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("当前比赛已经结束，请重新报名参赛"));
				prompt->setCallBack([=](){
					//UserDefault::getInstance()->setBoolForKey("Reconnection", false);
					GamePlatform::createPlatform();
				});
			}
			else
			{
				//UserDefault::getInstance()->setBoolForKey("Reconnection", false);
				GamePlatform::createPlatform();
			}
		}
		else//如果是断线重连，直接请求指定房间进入指定游戏
		{
			getCutRoomInfo();
		}
	}

	// 获取断线房间信息
	void Reconnection::getCutRoomInfo(BYTE isMatch/* = 0*/)
	{
		MSG_GP_S_GET_CUTNETROONINFO info;
		info.iUserID = PlatformLogic()->loginResult.dwUserID;
		info.iType = isMatch;
		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GET_CUTROOM, &info, sizeof(MSG_GP_S_GET_CUTNETROONINFO),
			HN_SOCKET_CALLBACK(Reconnection::getRoomInfoCallback, this));
	}

	bool Reconnection::getRoomInfoCallback(HNSocketMessage* socketMessage)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_S_GET_CUTNETROOMINFO_RES, socketMessage->objectSize, true, 
			"MSG_GP_S_GET_CUTNETROOMINFO_RES size of error!");
		auto joinRoomRes = (MSG_GP_S_GET_CUTNETROOMINFO_RES*)socketMessage->object;

		if (socketMessage->messageHead.bHandleCode == 1
			|| socketMessage->messageHead.bHandleCode == 2
			|| socketMessage->messageHead.bHandleCode == 4)
		{
			if (joinRoomRes->tCutNetRoomInfo.uRoomID == 0)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("购买房间信息错误，无法进入"));

				if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
				{
					PlatformLogic()->close();
				}
				
				return true;
			}

			_deskNo = joinRoomRes->iDeskID;
			_deskPsd = joinRoomRes->szPass;
			_isLocation = joinRoomRes->bPositionLimit;
			_selectRoomID = joinRoomRes->tCutNetRoomInfo.uRoomID;
			if (onCloseCallBack)
			{
				//onCloseCallBack();
				onCloseCallBack = nullptr;
			}
			UserDefault::getInstance()->setBoolForKey("Reconnection", true);

			RoomInfoModule()->addRoom(&joinRoomRes->tCutNetRoomInfo);

			// 进入游戏之前先检测资源有没有更新
			checkResUpdate(joinRoomRes->bType);
		}
		else if (socketMessage->messageHead.bHandleCode == 5)
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("房费不足"));
			prompt->setCallBack([=](){
				GamePlatform::createPlatform();
			});
		}
		else if (socketMessage->messageHead.bHandleCode == 6)
		{
			//没有正在进行的比赛
			if (onCloseCallBack)
			{
				onCloseCallBack();
				onCloseCallBack = nullptr;
			}
			UserDefault::getInstance()->setBoolForKey("Reconnection", false);
		}
		else
		{
			UserDefault::getInstance()->setBoolForKey("Reconnection", false);
			GamePlatform::createPlatform();
		}

		return true;
	}

	void Reconnection::checkResUpdate(BYTE type)
	{
		// 检测是否需要更新
		auto update = GameResUpdate::create();
		update->retain();
		update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {

			if (success)
			{
				joinRoom(type);
			}

			update->release();
		};
		update->checkUpdate(RoomInfoModule()->findRoom(_selectRoomID)->uNameID, true);
	}

	void Reconnection::joinRoom(BYTE type)
	{
		if (1 == type)
		{
			if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
			{
				loginRoom(RoomInfoModule()->findRoom(_selectRoomID));
			}
			else
			{
				if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INGAME)
				{
					loginRoom(RoomInfoModule()->findRoom(_selectRoomID));
				}
				else
				{
					auto prompt = GamePromptLayer::create();
					prompt->showPrompt(StringUtils::format(GBKToUtf8("您正在%s比赛中，前往继续比赛"),
						GBKToUtf8(RoomInfoModule()->findRoom(_selectRoomID)->szGameRoomName)));
					prompt->setCallBack([=](){
						loginRoom(RoomInfoModule()->findRoom(_selectRoomID));
					});
				}
			}
		}
		else
		{
			loginRoom(RoomInfoModule()->findRoom(_selectRoomID));
		}
	}
	// 进入创建房间
	void Reconnection::doLoginVipRoom(const std::string &password)
	{
		HNLOGEX_DEBUG("doLoginVipRoom : %s", password.c_str());

		MSG_GP_S_ENTER_VIPDESK joinRoomStruct;
		joinRoomStruct.iUserID = PlatformLogic()->loginResult.dwUserID;
		strcpy(joinRoomStruct.szInputPassWord, password.c_str());

		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_ENTER_DESK, &joinRoomStruct, sizeof(MSG_GP_S_ENTER_VIPDESK),
			HN_SOCKET_CALLBACK(Reconnection::onJoinRoomMessageCallback, this));

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("获取房间信息"), 25);  //gjd02切换场景，创建房间进入游戏。
	}

	// 进入创建房间回调
	bool Reconnection::onJoinRoomMessageCallback(HNSocketMessage* socketMessage)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_S_ENTER_VIPDESK_RES, socketMessage->objectSize, true, "MSG_GP_S_ENTER_VIPDESK_RES size of error!");
		MSG_GP_S_ENTER_VIPDESK_RES* joinRoomRes = (MSG_GP_S_ENTER_VIPDESK_RES*)socketMessage->object;

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		switch (socketMessage->messageHead.bHandleCode)
		{
		case 0:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("房号错误或过期")); break;
		case 1:
		case 3:
		{
			if (joinRoomRes->tCutNetRoomInfo.uRoomID == 0)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("购买房间信息错误，无法进入")); break;
			}

			_deskNo = joinRoomRes->iDeskID;
			_deskPsd = joinRoomRes->szPass;
			_isLocation = joinRoomRes->bPositionLimit;
			_selectRoomID = joinRoomRes->tCutNetRoomInfo.uRoomID;

			RoomInfoModule()->addRoom(&joinRoomRes->tCutNetRoomInfo);

			// 进入游戏之前先检测资源有没有更新
			checkResUpdate(joinRoomRes->bType);
		}break;
		case 2:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("用户不存在")); break;
		case 4:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("无空闲桌子")); break;
		case 5:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("房费不足")); break;
		case 6:
		{
			_deskNo = joinRoomRes->iDeskID;
			_deskPsd = joinRoomRes->szPass;
			_isLocation = joinRoomRes->bPositionLimit;
			_selectRoomID = joinRoomRes->tCutNetRoomInfo.uRoomID;
			RoomInfoModule()->addRoom(&joinRoomRes->tCutNetRoomInfo);

			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("您有正在进行的游戏"));
			prompt->setCallBack([=]() {

				// 进入游戏之前先检测资源有没有更新
				checkResUpdate(joinRoomRes->bType);
			});
		} break;
		case 7:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("非此部落成员")); break;
		default:
			GamePromptLayer::create()->showPrompt(GBKToUtf8("未知错误")); break;
			break;
		}

		return true;
	}
	void Reconnection::loginRoom(ComRoomInfo* roomInfo)
	{
		// 如果需要定位则先获取位置信息
		if (_isLocation)
		{
			GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr){

				if (success)
				{
					// 定位成功，执行登陆
					_roomLogic->start();
					_roomLogic->requestLogin(roomInfo->uRoomID, latitude, longtitude, addr);

					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("登录房间，请稍候..."), 25);
				}
				else
				{//不强制开启定位
					_roomLogic->start();
					_roomLogic->requestLogin(roomInfo->uRoomID);

					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("登录房间，请稍候..."), 25);
					//MessageBox(GBKToUtf8("定位失败，请在[设置]中打开定位服务或者检查网络!"), GBKToUtf8("提示"));
				}
			};

            GameLocation::getInstance()->getLocation();
		}
		else
		{
			_roomLogic->start();
			_roomLogic->requestLogin(roomInfo->uRoomID);

			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("登录房间，请稍候..."), 25);
		}
	}

	void Reconnection::onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode)
	{
		HNLog::logDebug("### onRoomLoginCallback:%s", message.c_str());

		// 游戏中断线重连会主动派发坐下消息，所以不用申请坐下
		if (success && RoomLogic()->loginResult.pUserInfoStruct.bDeskStation != INVALID_DESKSTATION) return;

		_roomLogic->stop();
		auto node = Director::getInstance()->getRunningScene();

		if (success)
		{
			if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INROOM)
			{
				LoadingLayer::removeLoading(node);
				_isReconnect = false;
				// 派发重连成功消息
				EventCustom event(RECONNECTION);
				Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
				return;
			}

			if (_deskNo != 255)
			{
				_roomLogic->start();

				if (_isLooker)
				{
					_roomLogic->requestWatchSit(_deskNo, _seatNo, _deskPsd);
				}
				else
				{
					_roomLogic->requestQuickSit(_deskNo, _deskPsd);
				}
				
				LoadingLayer::createLoading(node, GBKToUtf8("游戏加载中，请稍候..."), 25);
			}
			else
			{
				// 比赛场直接进游戏等待
				if (RoomLogic()->getRoomRule() & GRR_CONTEST
					|| RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
				{
					_isReconnect = false;
					HNPlatformConfig()->setIsReconnect(false);
					if (HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::INGAME)
					{
						// 启动游戏
						bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, INVALID_DESKNO, true);
						if (!ret)
						{
							RoomLogic()->close();
							LoadingLayer::removeLoading(node);
							auto prompt = GamePromptLayer::create();
							prompt->showPrompt(GBKToUtf8("启动失败，请检查是否有此游戏！"));
							prompt->setCallBack([=]() {
								GamePlatform::createPlatform();
							});
						}
					}
					else
					{
						LoadingLayer::removeLoading(node);
					}
				}
				else
				{
					RoomLogic()->close();
					_isReconnect = false;

					if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
					{
						PlatformLogic()->close();
						LoadingLayer::removeLoading(node);

						GamePromptLayer::create()->showPrompt(StringUtils::format(GBKToUtf8("错误的桌子号：%d"), _deskNo));
					}
					else
					{
						//UserDefault::getInstance()->setBoolForKey("Reconnection", false);
						GamePlatform::createPlatform();
					}
				}
			}
		}
		else
		{
			if (_CurConnNums < _MaxConnNums)
			{
				++_CurConnNums;
				node->runAction(Sequence::create(DelayTime::create(2.0f), CallFunc::create([=](){
					if (_isReconnect)
					{
						reConnection(false);
					}
					else
					{
						loginRoom(RoomLogic()->getSelectedRoom());
					}
				}), nullptr));
			}
			else
			{
				LoadingLayer::removeLoading(node);

				auto prompt = GamePromptLayer::create(true);
				prompt->showPrompt(StringUtils::format(GBKToUtf8("%s, 请重试"), message.c_str()));
				prompt->setCallBack([=](){
					_CurConnNums = 0;
					if (_isReconnect)
					{
						reConnection(false);
					}
					else
					{
						loginRoom(RoomLogic()->getSelectedRoom());
					}
				});
				prompt->setCancelCallBack([=](){
					if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INGAME
						|| HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
					{
						GamePlatform::createPlatform();
					}
				});
			}
		}
	}

	void Reconnection::onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo)
	{
		HNLog::logDebug("### onRoomSitCallback:%s", message.c_str());

		_roomLogic->stop();
		auto node = Director::getInstance()->getRunningScene();

		if (success)
		{
			_isReconnect = false;
			HNPlatformConfig()->setIsReconnect(false);

			// 派发重连成功消息
			EventCustom event1(RECONNECTION);
			Director::getInstance()->getEventDispatcher()->dispatchEvent(&event1);

			// 如果是游戏中重连，则坐下成功之后自动发送gameInfo，不需要额外处理
			if (HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::INGAME)
			{
				// 启动游戏
				bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
				if (!ret)
				{
					RoomLogic()->close();
					LoadingLayer::removeLoading(node);
					auto prompt = GamePromptLayer::create();
					prompt->showPrompt(GBKToUtf8("启动失败，请检查是否有此游戏！"));
					prompt->setCallBack([=](){
						if (HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::OTHER)
						{
							GamePlatform::createPlatform();
						}
						else
						{
							PlatformLogic()->close();
						}
					});
				}
			}
			else
			{
				LoadingLayer::removeLoading(node);
			}
		}
		else
		{
			LoadingLayer::removeLoading(node);

			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(message);
			prompt->setCallBack([=](){
				_CurConnNums = 0;
				_isReconnect = false;
				HNPlatformConfig()->setIsReconnect(false);
				RoomLogic()->close();
				if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INGAME
					|| HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
				{
					GamePlatform::createPlatform();
				}
			});
		}
	}

	// 登陆大厅回调
	void Reconnection::onPlatformLoginCallback(bool success, const std::string& message,
		const std::string& name, const std::string& pwd)
	{
		HNLog::logDebug("### onPlatformLoginCallback:%s", message.c_str());

		_gameLogin->stop();

		if (success)
		{
			_CurConnNums = 0;
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

			switch (HNPlatformConfig()->getSceneState())
			{
			case PlatformConfig::SCENE_STATE::INPLATFORM:
			case PlatformConfig::SCENE_STATE::INMATCH:
			{
				_isReconnect = false;
				// 派发重连成功消息
				EventCustom event(RECONNECTION);
				Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
			} break;
			case PlatformConfig::SCENE_STATE::OTHER:
			case PlatformConfig::SCENE_STATE::INROOM:
				if (RoomLogic()->getSelectedRoom()) loginRoom(RoomLogic()->getSelectedRoom());
				break;
			case PlatformConfig::SCENE_STATE::INGAME:
			{
				//如果是创建房间的重连则走平台获取断线信息通道，防止重连进已经解散的房间
				if (_isVipRoom
					|| RoomLogic()->getRoomRule() & GRR_QUEUE_GAME
					|| RoomLogic()->getRoomRule() & GRR_CONTEST
					|| RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
				{
					checkReconnection();
				}
				else
				{
					auto roominfo = RoomLogic()->getSelectedRoom();
					if (!roominfo) break;

					if (GameCreator()->getCurrentGameType() == HNGameCreator::SINGLE)
					{
						_isReconnect = false;
						auto prompt = GamePromptLayer::create();
						prompt->showPrompt(GBKToUtf8("网络断开，请重新进入游戏！"));
						prompt->setCallBack([=](){
							GamePlatform::createPlatform();
						});
					}
					else
					{
						loginRoom(roominfo);
					}
				}
			}
				break;
			default:
				break;
			}
		}
		else
		{
			if (_CurConnNums < _MaxConnNums)
			{
				++_CurConnNums;
				Director::getInstance()->getRunningScene()->runAction(Sequence::create(DelayTime::create(2.0f), CallFunc::create([=](){
					reConnection(false);
				}), nullptr));
			}
			else
			{
				LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

				auto prompt = GamePromptLayer::create(true);
				prompt->showPrompt(StringUtils::format(GBKToUtf8("%s, 请重试"), message.c_str()));
				prompt->setCallBack([=](){
					_CurConnNums = 0;
					reConnection(false);
				});
				prompt->setCancelCallBack([=](){
					PlatformLogic()->close();
					GameMenu::createMenu();
				});
			}
		}
	}

	// 重连
	void Reconnection::reConnection(bool bAuto/* = true*/)
	{
		if (!_isOpen || (_isReconnect && bAuto)) return;
		if (!_isReconnect) LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
		
		_isReconnect = true;

		if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INGAME)
		{
			_deskNo = RoomLogic()->loginResult.pUserInfoStruct.bDeskNO;
			_seatNo = RoomLogic()->loginResult.pUserInfoStruct.bDeskStation;

			_isVipRoom = RoomLogic()->getRoomRule() & GRR_GAME_BUY;
			_isLooker = RoomLogic()->loginResult.pUserInfoStruct.bUserState == USER_WATCH_GAME;
			HNPlatformConfig()->setIsReconnect(true);
		}

		_roomLogic->stop();
		RoomLogic()->close();
		PlatformLogic()->close();

		_gameLogin->start();
		_gameLogin->requestLogin(_uName, _pWord, (ThirdLoginType)PlatformLogic()->loginResult.bLogonType, true);

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在重连，请稍候..."), 25);
	}

	void Reconnection::resetParams()
	{
		_isReconnect = false;
		_isOpen = true;
		_isVipRoom = false;
		_isLocation = false;
		_deskPsd = "";
		_CurConnNums = 0;
		_deskNo = 255;
		_seatNo = 255;
	}

	void Reconnection::openOrCloseReconnet(bool bOpen)
	{
		_isOpen = bOpen;

		if (_isOpen && !PlatformLogic()->isConnect())
		{
			reConnection(true);
		}
	}

	void Reconnection::enterForeground()
	{
		UINT nowTime = time(NULL);
		UINT lastTime = Configuration::getInstance()->getValue("bkTime", Value(0)).asUnsignedInt();
		if (0 == lastTime) return;

		UINT useTime = nowTime - lastTime;

		if (40 <= useTime)
		{
			log("enterBackground outTime:%d", useTime);

			Configuration::getInstance()->setValue("bkTime", Value(0));

			//下一帧再检测是否需要执行重连（如果切后台回来已经断线，则交由正常断线重连流程处理）
			Director::getInstance()->getScheduler()->schedule([=](float fDelta)
			{
				if (!_isReconnect && HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::OTHER)
				{
					reConnection(false);
				}
			}, this, 0.0f, 0, 0.0f, false, "delayReConnection");
		}
	}
    
    // 进入房间
    void Reconnection::shareDeskInfo()
    {
		if (HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::INPLATFORM &&
			HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::INROOM) return;

		Director::getInstance()->getScheduler()->schedule([=](float fDelta)
		{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			std::string roomID = Operator::requestChannel("sysmodule", "getShareRoomID");
			UserDefault::getInstance()->setStringForKey("DZBRoomID", roomID);
			UserDefault::getInstance()->flush();
#endif
			std::string strPasswordValue = UserDefault::getInstance()->getStringForKey("DZBRoomID", "null");
			if (strPasswordValue != "null" && strPasswordValue != "-1")
			{
				if (PlatformLogic()->isConnect())
				{
					doLoginVipRoom(strPasswordValue);
					std::string clearID = Operator::requestChannel("sysmodule", "clearShareInfo");
					UserDefault::getInstance()->setStringForKey("DZBRoomID", "null");
					UserDefault::getInstance()->flush();
				}
			}
		}, this, 0.0f, 0, 0.0f, false, "delayShareDeskInfo");
	}
}
