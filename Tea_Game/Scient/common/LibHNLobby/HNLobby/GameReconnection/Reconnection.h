/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNReconnection_h__
#define __HNReconnection_h__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "HNMarketExport.h"
#include "cocos2d.h"
#include "HNPlatformLogic/HNPlatformLogin.h"
#include "HNRoomLogic/HNRoomLogicBase.h"

USING_NS_CC;

namespace HN
{
	class Reconnection 
		: public Ref
		, public HN::IHNPlatformLogin
		, public HN::IHNRoomLogicBase
	{
	public:
		static Reconnection* getInstance();

		// 销毁单例
		static void destroyInstance();

		//更新结果回调
		std::function <void(bool isSucccess, cocos2d::extension::EventAssetsManagerEx::EventCode code)> updateResult;

		//比赛中的游戏检测
		typedef std::function<void()> CloseCallBack;
		CloseCallBack	onCloseCallBack;

	public:
		Reconnection();

		virtual ~Reconnection();

	public:
		virtual void init();

		// 保存登录信息并检测是否有重连状态
		void saveInfoAndCheckReconnection(const std::string &userName, const std::string &passWord);

		// 获取断线房间信息
		void getCutRoomInfo(BYTE isMatch = 0);

		// 进入创建房间
		void doLoginVipRoom(const std::string &password);

		// 开启/关闭 断线重连功能
		void openOrCloseReconnet(bool bOpen);

		// 后台切换回来
		void enterForeground();

        void shareDeskInfo();

	private:
		// 重连
		void reConnection(bool bAuto = true);
	

	private:
		// 检查重连
		void checkReconnection();

		// 获取重连房间信息
		bool getRoomInfoCallback(HNSocketMessage* socketMessage);

		// 进入创建房间回调
		bool onJoinRoomMessageCallback(HNSocketMessage* socketMessage);

		// 检测资源更新（0：普通房间，1：比赛场，2：VIP房间）
		void checkResUpdate(BYTE type);

		// 加入房间（0：普通房间，1：比赛场，2：VIP房间）
		void joinRoom(BYTE type);

		// 登录房间
		void loginRoom(ComRoomInfo* roomInfo);

	private:
		// 登陆大厅回调
		virtual void onPlatformLoginCallback(bool success, const std::string& message,
			const std::string& name, const std::string& pwd)  override;

		// 登陆房间回调
		virtual void onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode) override;

		// 坐下回调
		virtual void onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo) override;

	private:
		// 重置参数
		void resetParams();

	private:
		bool _isReconnect	= false;		// 是否已经处于重连状态
		bool _isOpen		= true;			// 是否开启重连功能
		bool _isVipRoom		= false;		// 当前重连的是vip房间
		bool _isLocation	= false;		// 是否需要定位
		bool _isLooker		= false;		// 是否旁观状态
		std::string _uName	= "";			// 玩家登录账号
		std::string _pWord	= "";			// 玩家登陆密码
		std::string _deskPsd = "";			// vip桌子密码
		int _MaxConnNums	= 5;			// 最大重连次数
		int _CurConnNums	= 0;			// 当前重连次数

		BYTE _deskNo		= 255;
		BYTE _seatNo		= 255;
		INT	_selectRoomID	= 0;

		HNPlatformLogin*		_gameLogin		= nullptr;
		HNRoomLogicBase*		_roomLogic		= nullptr;

		EventListenerCustom*	_listener		= nullptr;

		EventListenerCustom*	_foreground		= nullptr;

#define GameReconnection() Reconnection::getInstance()
	};
}

#endif // __HNReconnection_h__
