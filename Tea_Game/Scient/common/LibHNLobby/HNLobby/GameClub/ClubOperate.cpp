#include "ClubOperate.h"

static const char* OPERATE_CSB_PATH = "platform/Club/CreateClub.csb";
 
std::string TextCount = "";

int InputType = 0;

ClubOperate::ClubOperate()
{

}

ClubOperate::~ClubOperate()
{
	
}

bool ClubOperate::init()
{
	if (!Layer::init()) return false;

	auto Node = CSLoader::createNode(OPERATE_CSB_PATH);
	//
	addChild(Node);

	auto Panel = dynamic_cast<Layout*>(Node->getChildByName("Panel_1"));

	Image_bg = dynamic_cast<ImageView*>(Node->getChildByName("Image_bg"));

	Sprite_top = dynamic_cast<Sprite*>(Image_bg->getChildByName("Sprite_top"));

	auto Btn_cancel = dynamic_cast<Button*>(Image_bg->getChildByName("Button_close"));

	_ImagePassWord = dynamic_cast<ImageView*>(Node->getChildByName("Image_password"));
	_ImagePassWord->setVisible(false);
	Btn_define = dynamic_cast<Button*>(Image_bg->getChildByName("Button_define"));
	Btn_define->setEnabled(false);
	_TextField = dynamic_cast<TextField*>(Image_bg->getChildByName("TextField_"));
	_TextField->setTextHorizontalAlignment(TextHAlignment::CENTER); //设置靠左
	_TextField->setTextVerticalAlignment(TextVAlignment::CENTER); //设置垂直居中
	//监听事件
	Btn_cancel->addClickEventListener(CC_CALLBACK_1(ClubOperate::onButtonCallBack, this));
	Btn_define->addClickEventListener(CC_CALLBACK_1(ClubOperate::onButtonCallBack, this));
	_TextField->addEventListenerTextField(this, textfieldeventselector(ClubOperate::onTextFiledCallBack));
	Image_bg->setSwallowTouches(true);
	Panel->addClickEventListener([=](Ref*){
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			this->removeFromParent();
		}), nullptr));
	});

	
	return true;
}

void ClubOperate::ShowIamgeOfPassWord(bool isShow)
{
	if (_ImagePassWord != nullptr)_ImagePassWord->setVisible(isShow);

}

void ClubOperate::SetImageViewRes(int num)
{
	std::string TipsClub1 = "platform/Club/res/teahouseset/TeaHouseCreate.png";//创建俱乐部
	std::string TipsClub2 = "platform/Club/res/teahouseset/joinTeahouse.png";//加入俱乐部
	std::string TipsClub3 = "platform/Club/res/teahouseset/TeaHouseCreate.png";//充值房卡
	std::string TipsClub4 = "platform/Club/res/teahouseset/teahouseName.png";//修改俱乐部名称
	std::string TipsClub5 = "platform/Club/res/teahouseset/teahouseNotice.png";//俱乐部公告
	std::string TipsClub6 = "platform/Club/res/teahouseset/TeaHouseCreate.png";//搜索房间
	std::string TipsClub7 = "platform/Club/res/teahouseset/TeaHouseCreate.png";//加入房间

	InputType = num;

	switch (num)
	{
	case(1) : Sprite_top->setTexture(TipsClub1); break;
	case(2) : Sprite_top->setTexture(TipsClub2); break;
	case(3) : Sprite_top->setTexture(TipsClub3); break;
	case(4) : Sprite_top->setTexture(TipsClub4); break;
	case(5) : Sprite_top->setTexture(TipsClub5); break;
	case(6) : Sprite_top->setTexture(TipsClub6); break;
	case(7) : Sprite_top->setTexture(TipsClub7); break;
	}
	if (num == 1)
	{
		//auto Text_create = dynamic_cast<Text*>(Image_bg->getChildByName("Text_create"));
		//Text_create->setVisible(true);
	}
}

void ClubOperate::SetPlaceHolderCount(const std::string &count)
{
	_TextField->setPlaceHolder(GBKToUtf8(count));
}


void ClubOperate::SetCallBack(std::function<void()> CallBack)
{
	if (CallBack)
	{
		_Callback = CallBack;
	}
}

void ClubOperate::onButtonCallBack(Ref* pSender)
{
	auto Btn = (Button*)pSender;

	auto BtnName = Btn->getName();

	if (BtnName.compare("Button_close") == 0)
	{
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			this->removeFromParent();
		}), nullptr));

	}
	else if (BtnName.compare("Button_define") == 0)
	{
		if (_Callback != nullptr)
		{
			_Callback();
		}
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			this->removeFromParent();
		}), nullptr));
	}

}

void ClubOperate::onTextFiledCallBack(CCObject* pSender, TextFiledEventType type)
{
	TextCount = _TextField->getString();
	auto name_len = strlen(Utf8ToGBK(_TextField->getString()));
	auto Checkingtheformat = [=](){
		if (name_len <= 0) Btn_define->setEnabled(false);
		else if (name_len > 12 && InputType == 4) Btn_define->setEnabled(false);
		else if (name_len > 12 && InputType == 1) Btn_define->setEnabled(false);
		else if (name_len > 100) Btn_define->setEnabled(false);				//公告预留较多
		else Btn_define->setEnabled(true);
	};

	switch (type)
	{	
		//触摸开始
		case(TEXTFIELD_EVENT_ATTACH_WITH_IME) :
		{
			Checkingtheformat();
		}
		//触摸结束
		case(TEXTFIELD_EVENT_DETACH_WITH_IME) :
		{
			Checkingtheformat();
		}
		//输入文本
		case(TEXTFIELD_EVENT_INSERT_TEXT) :
		{
			Checkingtheformat();
		}
		//删除文本
		case(TEXTFIELD_EVENT_DELETE_BACKWARD) :
		{
			Checkingtheformat();

		}
	}
}