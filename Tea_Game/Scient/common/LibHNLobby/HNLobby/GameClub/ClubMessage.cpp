#include "ClubMessage.h"
#include "ClubTips.h"
static const char* CLUB_MESSAGE_PATH = "platform/Club/ClubMsg.csb";
static const char* CLUB_MSG_ITEM = "platform/Club/ClubMsgItem.csb";
static const char* USER_MEN_HEAD = "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";
static const char* USER_HEAD_MASK = "platform/common/touxiangdi.png";
int num;
int SelectedClubID;

bool ClubMsg::init()
{
	if (!Layer::init())
	{
		return false;
	}
	
	//加载csb文件
	auto Node = CSLoader::createNode(CLUB_MESSAGE_PATH);
	//
	addChild(Node);
	//背景
	Image_bg = dynamic_cast<ImageView*>(Node->getChildByName("Image_bg"));
	//设置可触摸
	Image_bg->setTouchEnabled(true);
	//文字提示
	Text_Tips = dynamic_cast<Text*>(Image_bg->getChildByName("Text_Tips"));
	Text_Tips->setVisible(true);
	//阴影层
	auto Panel_bg = dynamic_cast<Layout*>(Node->getChildByName("Panel_bg"));
	//阴影层监听事件
	Panel_bg->setSwallowTouches(true);
	Panel_bg->addClickEventListener([=](Ref*){
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			this->removeFromParent();
		}), nullptr));
	});

	//关闭按钮
	auto Button_Close = (Button*)Image_bg->getChildByName("Button_close");
	Button_Close->addClickEventListener([=](Ref*){
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			this->removeFromParent();
		}), nullptr));
	});

	Panel_2 = dynamic_cast<Layout*>(Image_bg->getChildByName("Panel_2"));
	return true;
}

void ClubMsg::RequestClubMessage(int Clubid)
{
	if (Message.size() != 0) Message.clear();
	if (!Clubid)return;
	SelectedClubID = Clubid;
	MSG_GP_I_Club_ReviewList ID;
	ID.iClubID = SelectedClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_REVIEW_LIST, &ID, sizeof(MSG_GP_I_Club_ReviewList), HN_SOCKET_CALLBACK(ClubMsg::getClubMsgResult, this));
}

bool ClubMsg::getClubMsgResult(HNSocketMessage* SocketMessage)
{
	switch (SocketMessage->messageHead.bHandleCode)
	{
	case(0):
	{
		auto head = (MSG_GP_O_Club_ReviewList_Head*)SocketMessage->object;
		int usernum = (SocketMessage->objectSize - sizeof(MSG_GP_O_Club_ReviewList_Head)) / sizeof(MSG_GP_O_Club_ReviewList_Data);
		//总cell个数
		num = usernum;
		//网络消息数据
		auto  _data = (MSG_GP_O_Club_ReviewList_Data*)(SocketMessage->object + sizeof(MSG_GP_O_Club_ReviewList_Head));
	
		if (usernum)
		{
			Text_Tips->setVisible(false);
			while (usernum-- > 0)
			{
				Message.push_back(*_data);
				_data++;
			};
			if (_tableView != nullptr) _tableView->reloadData();
			else CreateTableView();
		}
		else
		{
			Text_Tips->setVisible(true);
			if (_tableView != nullptr) _tableView->reloadData();
		}

	}break;
	case(2):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("部落ID错误"));
	}break;
	case(3):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("玩家不是会长"));

	}break;
	}
	return true;
}

void ClubMsg::CreateTableView()
{
	//创建TableView
	_tableView = TableView::create(this, Size ( 1214, 475));
	_tableView->setAnchorPoint(Vec2(0.5f,0.5f));
	//设置滚动方向
	_tableView->setDirection(extension::ScrollView::Direction::VERTICAL);
	//设置Item在视图中的排序和填充方式
	_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//
	_tableView->setDelegate(this);
	//将TableView加入到ScrollView中
	Panel_2->addChild(_tableView);
	//
}

//添加Cell
TableViewCell* ClubMsg::tableCellAtIndex(TableView* table, ssize_t index)
{
	TableViewCell* cell = table->dequeueCell();
	Node* item = nullptr;
	if (!cell)
	{
		cell = new TableViewCell();
		//cell->autorelease();
		item = CSLoader::createNode(CLUB_MSG_ITEM);
		item->setPosition(Vec2(5, 0));
		item->setTag(1);
		cell->addChild(item);
	}
	else{
		item = cell->getChildByTag(1);
	}
	
	//item背景
	auto item_bg = dynamic_cast<ImageView*>(item->getChildByName("item_bg"));
	//用户名称
	auto name = dynamic_cast<Text*>(item_bg->getChildByName("Text_name"));
	name->setString(GBKToUtf8(Message[index].szUserNickName));
	//用户ID
	auto id = dynamic_cast<Text*>(item_bg->getChildByName("Text_id"));
	id->setString(StringUtils::format("ID:%d", Message[index].iUserID));
	//头像框
	auto frame = dynamic_cast<ImageView*>(item_bg->getChildByName("Image_frame"));
	//头像
	auto head = dynamic_cast<ImageView*>(item_bg->getChildByName("Image_head"));
	// 创建头像
	auto _userHead = GameUserHead::create(head, frame);
	_userHead->show(USER_HEAD_MASK);//添加底图
	_userHead->loadTexture(Message[index].bSex? USER_MEN_HEAD : USER_WOMEN_HEAD);//判断是男还是女，没有网络头像情况下剩下
	_userHead->loadTextureWithUrl(Message[index].szHeadURL);//设置网络头像
	//_userHead->onResponseHeadUrl(Message[index].iUserID);
	//同意按钮
	auto Button_agree = dynamic_cast<Button*>(item_bg->getChildByName("Button_agree"));
	//添加按钮回调
	Button_agree->addClickEventListener([=](Ref*){
		MSG_GP_I_Club_MasterOpt Operate;
		Operate.iClubID = SelectedClubID;
		Operate.iTargetID = Message[index].iUserID;
		Operate.bOptType = 0;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_MASTER_OPTION, &Operate, sizeof(MSG_GP_I_Club_MasterOpt), HN_SOCKET_CALLBACK(ClubMsg::getOperateResult, this));
	});

	//拒绝按钮
	auto Button_refuse = dynamic_cast<Button*>(item_bg->getChildByName("Button_refuse"));
	//添加拒绝按钮回调
	Button_refuse->addClickEventListener([=](Ref*){
		MSG_GP_I_Club_MasterOpt Operate;
		Operate.iClubID = SelectedClubID;
		Operate.iTargetID = Message[index].iUserID;
		Operate.bOptType = 1;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_MASTER_OPTION, &Operate, sizeof(MSG_GP_I_Club_MasterOpt), HN_SOCKET_CALLBACK(ClubMsg::getOperateResult, this));
	});

	return cell;
}

//返回 Item 大小
Size ClubMsg::tableCellSizeForIndex(TableView* table, ssize_t index)
{
	return Size(1214, 85);
}

//触摸函数
void ClubMsg::tableCellTouched(TableView* table, TableViewCell* index)
{


}

//返回Cell总个数
ssize_t ClubMsg::numberOfCellsInTableView(TableView* table)
{
	return num;
}


bool ClubMsg::getOperateResult(HNSocketMessage* socketMessage)
{
	//如果成功那么就刷新视图
	switch (socketMessage->messageHead.bHandleCode)
	{
	case(0):
	{
		 if (Message.size() != 0) Message.clear();
		MSG_GP_I_Club_ReviewList ID;
		ID.iClubID = SelectedClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_REVIEW_LIST, &ID, sizeof(MSG_GP_I_Club_ReviewList), HN_SOCKET_CALLBACK(ClubMsg::getClubMsgResult, this));
	}break;
	case(2):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("部落ID错误"));
	}break;
	case(3) :
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("玩家不是会长"));
	}break;
	case(5):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("玩家已加入该工会"));

	}break;
	case(8):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("超过加入部落限制"));

	}break;
	case(9):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("部落人数超过限制"));

	}break;
	}
	
	return true;
}