#include "ClubPartner.h"
#include "HNPlatform/HNPlatformClubData.h"
#include "../GamePersionalCenter/GameUserHead.h"
#include "ClubUserRedFlow.h"

static const char* TEA_HOUSE_PARTNER_SCB_PATH = "platform/Club/res/ClubPartnerLayer.csb";

static const std::string TEA_HOUSE_DAYLIST[] = { "今日输赢", "昨日输赢", "前日输赢" };

ClubPartner::ClubPartner()
{
	Vec_PartnerInfo.clear();
}

ClubPartner::~ClubPartner()
{
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest);//移除监听消息
}

bool ClubPartner::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//加载csb文件（茶楼或者俱乐部）
	auto node = CSLoader::createNode(TEA_HOUSE_PARTNER_SCB_PATH);
	//断言
	CCASSERT(node != nullptr, "node is null");
	//添加csb文件
	addChild(node);

	//保存节点
	m_NodePartner = node;

	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, HN_SOCKET_CALLBACK(ClubPartner::getMessageBack, this));

	return true;
}

void ClubPartner::initPartnerList()
{
	//查询合作群列表
	SendQueryPartnerList();

	//背景
	auto Image_bg = (ImageView*)m_NodePartner->getChildByName("Image_bg");
	Image_bg->setVisible(true);

	//成员隐藏
	auto Image_Member = (ImageView*)m_NodePartner->getChildByName("Image_Member");
	Image_Member->setVisible(false);

	//标题
	auto Sprite_top = (Sprite*)Image_bg->getChildByName("Sprite_top");

	//关闭
	auto Button_close = (Button*)Image_bg->getChildByName("Button_close");
	Button_close->addClickEventListener([=](Ref*){
		this->removeFromParent();
	});

	//添加合作群
	auto Button_addPartner = (Button*)Image_bg->getChildByName("Button_addPartner");
	Button_addPartner->addClickEventListener([=](Ref*){
		if (Panel_AddPartner)
		{
			//Panel_AddPartner->setVisible(true);
			setAddPartnerVisible(true);
		}
	});

	//合作群人数
	Text_PartnerNum = (Text*)Image_bg->getChildByName("Text_PartnerNum");

	//合作群列表
	m_ListViewPartner = (ListView*)Image_bg->getChildByName("ListView_Partner");

	//点击更多选择时间
	auto Button_More = (Button*)Image_bg->getChildByName("Button_More");
	Button_More->addClickEventListener([=](Ref*){
		if (m_ImgMore)
		{
			m_MoreClose->setVisible(true);
			m_ImgMore->setVisible(true);
		}
	});

	//当前选择的时间
	m_TextTime = (Text*)Button_More->getChildByName("Text_Time");

	//关闭弹出选择页面
	m_MoreClose = (Layout*)Image_bg->getChildByName("Panel_close");
	m_MoreClose->addTouchEventListener([=](Ref* ref, Widget::TouchEventType type){
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		m_MoreClose->setVisible(false);
		m_ImgMore->setVisible(false);
	});

	//弹出页面
	m_ImgMore = (ImageView*)Image_bg->getChildByName("Image_More");

	//今日战绩
	auto Button_nowday = (Button*)m_ImgMore->getChildByName("Button_nowday");
	Button_nowday->addClickEventListener([=](Ref*){
		//发送查询今日合作群列表
		SendQueryParterList(0);
	});
	//昨日战绩
	auto Button_yesterday = (Button*)m_ImgMore->getChildByName("Button_yesterday");
	Button_yesterday->addClickEventListener([=](Ref*){
		//发送查询昨日合作群列表
		SendQueryParterList(1);
	});
	//前日战绩
	auto Button_byesterday = (Button*)m_ImgMore->getChildByName("Button_byesterday");
	Button_byesterday->addClickEventListener([=](Ref*){
		//发送查询前日合作群列表
		SendQueryParterList(2);
	});

	//添加合作群用户
	Panel_AddPartner = (Layout*)Image_bg->getChildByName("Panel_AddPartner");

	//关闭添加合作群
	auto Button_close_A = (Button*)Panel_AddPartner->getChildByName("Button_close_A");
	Button_close_A->addClickEventListener([=](Ref*){
		setAddPartnerVisible(false);
	});

	//输入搜索
	TextField_Search = (TextField*)Panel_AddPartner->getChildByName("TextField_Search");
	TextField_Search->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_Search->setTextVerticalAlignment(TextVAlignment::CENTER);

	//搜索列表容器
	ListView_SearchM = (ListView*)Panel_AddPartner->getChildByName("ListView_SearchM");

	//搜索按钮
	auto Button_search = (Button*)Panel_AddPartner->getChildByName("Button_search");
	Button_search->addClickEventListener([=](Ref*){
		//检测输入发送搜索
		SearchUserByID();
	});

	//修改红花比例
	Panel_ModifyRedFRate = (Layout*)Image_bg->getChildByName("Panel_ModifyRedFRate");

	//关闭红花修改
	auto Button_close_M = (Button*)Panel_ModifyRedFRate->getChildByName("Button_close_M");
	Button_close_M->addClickEventListener([=](Ref*){
		Panel_ModifyRedFRate->setVisible(false);
		//TextField_ModifyRedFRate->setString("");
	});

	//红花比例输入框
	TextField_ModifyRedFRate = (TextField*)Panel_ModifyRedFRate->getChildByName("TextField_ModifyRedFRate");
	TextField_ModifyRedFRate->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_ModifyRedFRate->setTextVerticalAlignment(TextVAlignment::CENTER);

	//确定修改红花比例
	auto Button_Sure_M = (Button*)Panel_ModifyRedFRate->getChildByName("Button_Sure_M");
	Button_Sure_M->addClickEventListener([=](Ref*){
		//检测输入发送修改比例
		ModifyRedFlowerRate();
	});

	//添加合作群成员
	Panel_AddMembers = (Layout*)Image_bg->getChildByName("Panel_AddMembers");

	//关闭添加合作群成员
	auto Button_close_AM = (Button*)Panel_AddMembers->getChildByName("Button_close_AM");
	Button_close_AM->addClickEventListener([=](Ref*){
		Panel_AddMembers->setVisible(false);
	});

	//搜索成员加入当前合作群
	TextField_SearchAM = (TextField*)Panel_AddMembers->getChildByName("TextField_SearchAM");
	TextField_SearchAM->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_SearchAM->setTextVerticalAlignment(TextVAlignment::CENTER);

	//搜索按钮
	auto Button_searchAM = (Button*)Panel_AddMembers->getChildByName("Button_search");
	Button_searchAM->addClickEventListener([=](Ref*){
		//检测输入发送搜索
		SearchUserInfo();
	});

	//搜索成员结果列表
	ListView_SearchAM = (ListView*)Panel_AddMembers->getChildByName("ListView_SearchAM");

	//成员列表
	Panel_MemberList = (Layout*)Image_bg->getChildByName("Panel_MemberList");

	//成员列表关闭
	auto Button_member_close = (Button*)Panel_MemberList->getChildByName("Button_member_close");
	Button_member_close->addClickEventListener([=](Ref*){
		Panel_MemberList->setVisible(false);
	});

	//成员列表
	ListView_Member = (ListView*)Panel_MemberList->getChildByName("ListView_Member");

	//成员详情
	Panel_UserInfo = (Layout*)m_NodePartner->getChildByName("Panel_UserInfo");

	//成员详情战绩
	ListView_Record = (ListView*)Panel_UserInfo->getChildByName("ListView_UserInfo");

	//关闭成员详情
	auto Button_userinfo_close = (Button*)Panel_UserInfo->getChildByName("Button_userinfo_close");
	Button_userinfo_close->addClickEventListener([=](Ref*){
		Panel_UserInfo->setVisible(false);
	});
}

void ClubPartner::initPartnerMemberList(int PartnerID)
{
	m_iPartnerID = PartnerID;

	//查询当前返利红花比例
	//发送查询成员
	SendPartnerUserList();

	//合作群隐藏
	auto Image_bg = (ImageView*)m_NodePartner->getChildByName("Image_bg");
	Image_bg->setVisible(false);

	//成员
	auto Image_Member = (ImageView*)m_NodePartner->getChildByName("Image_Member");
	Image_Member->setVisible(true);

	//关闭
	auto Button_close = (Button*)Image_Member->getChildByName("Button_return");
	Button_close->addClickEventListener([=](Ref*){
		this->removeFromParent();
	});

	//红花比例
	P_Text_redFlowerRate = (Text*)Image_Member->getChildByName("Text_redFlowerRate");

	//今日获得红花
	//P_Text_reap_redFlower = (Text*)Image_Member->getChildByName("Text_reap_redFlower");

	//刷新
	auto Button_refresh = (Button*)Image_Member->getChildByName("Button_refresh");
	Button_refresh->addClickEventListener([=](Ref*){
		Button_refresh->setEnabled(false);
		SendPartnerUserList();
		Button_refresh->runAction(Sequence::create(DelayTime::create(5.0f), CallFunc::create([=]() {
			Button_refresh->setEnabled(true);
		}), nullptr));
	});

	//详情
	auto Button_details = (Button*)Image_Member->getChildByName("Button_details");
	Button_details->addClickEventListener([=](Ref*){
		ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
		ClubRedFlow->SetClubID(m_iClubID);
		ClubRedFlow->initCSBByType(1, m_iPartnerID, false);
		ClubRedFlow->setName("ClubRedFlow");
		this->addChild(ClubRedFlow);
	});

	//红花比例
	P_Text_redFlowerRate = (Text*)Image_Member->getChildByName("Text_redFlowerRate");

	//获得红花
	P_Text_reap_redFlower = (Text*)Image_Member->getChildByName("Text_reap_redFlower");

	//参与房间数
	P_Text_RoomNum = (Text*)Image_Member->getChildByName("Text_RoomNum");

	//大赢家数
	P_Text_BigWinNum = (Text*)Image_Member->getChildByName("Text_BigWinNum");

	//成员参与数
	P_Text_PPlayerNum = (Text*)Image_Member->getChildByName("Text_PPlayerNum");
	
	//总成员数
	P_Text_PlayerNum = (Text*)Image_Member->getChildByName("Text_PlayerNum");

	//群主是否可操作按钮
	P_Button_operation = (Button*)Image_Member->getChildByName("Button_operation");
	P_Button_operation->addClickEventListener([=](Ref*){
		if (m_uiAccess & ClubAccess_AllowAdminTakeRedFlower)
		{
			auto SendDisbandClub = [=](){
				SClubRequest_AllowAdminTakeRedFlower Date;
				Date.bAllow = false;
				Date.clubId = m_iClubID;
				Date.gameNameId = 0;
				Date.msg = ClubRequest_AllowAdminTakeRedFlower;
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_AllowAdminTakeRedFlower));
			};
			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(GBKToUtf8("取消群主和副群主给自己玩家摘花/送花？"));
			prompt->setCallBack([=](){
				SendDisbandClub();
			});
		}
		else
		{
			auto SendDisbandClub = [=](){
				SClubRequest_AllowAdminTakeRedFlower Date;
				Date.bAllow = true;
				Date.clubId = m_iClubID;
				Date.gameNameId = 0;
				Date.msg = ClubRequest_AllowAdminTakeRedFlower;
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_AllowAdminTakeRedFlower));
			};
			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(GBKToUtf8("允许群主和副群主给自己玩家摘花/送花？"));
			prompt->setCallBack([=](){
				SendDisbandClub();
			});
		}
	});

	//添加合作群用户
	Panel_AddPartner = (Layout*)Image_bg->getChildByName("Panel_AddPartner");
	Panel_AddPartner->setVisible(false);

	//手动添加
	P_Button_addMember = (Button*)Image_Member->getChildByName("Button_addMember");
	P_Button_addMember->addClickEventListener([=](Ref*){
		if (Panel_AddMembers)
		{
			TextField_SearchAM->setString("");
			Panel_AddMembers->setVisible(true);
		}
	});

	//成员列表
	P_ListView_PartnerMember = (ListView*)Image_Member->getChildByName("ListView_PartnerMember");

	//添加合作群成员
	Panel_AddMembers = (Layout*)Image_Member->getChildByName("Panel_AddMembers");

	//关闭添加合作群成员
	auto Button_close_AM = (Button*)Panel_AddMembers->getChildByName("Button_close_AM");
	Button_close_AM->addClickEventListener([=](Ref*){
		Panel_AddMembers->setVisible(false);
	});

	//搜索成员加入当前合作群
	TextField_SearchAM = (TextField*)Panel_AddMembers->getChildByName("TextField_SearchAM");
	TextField_SearchAM->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_SearchAM->setTextVerticalAlignment(TextVAlignment::CENTER);

	//搜索按钮
	auto Button_searchAM = (Button*)Panel_AddMembers->getChildByName("Button_search");
	Button_searchAM->addClickEventListener([=](Ref*){
		//检测输入发送搜索
		SearchUserInfo();
	});

	//搜索成员结果列表
	ListView_SearchAM = (ListView*)Panel_AddMembers->getChildByName("ListView_SearchAM");

	//添加成员层
	P_Panel_AddMembers = (Layout*)Image_Member->getChildByName("Panel_AddMembers");

	//成员详情
	Panel_UserInfo = (Layout*)m_NodePartner->getChildByName("Panel_UserInfo");

	//成员详情战绩
	ListView_Record = (ListView*)Panel_UserInfo->getChildByName("ListView_UserInfo");

	//关闭成员详情
	auto Button_userinfo_close = (Button*)Panel_UserInfo->getChildByName("Button_userinfo_close");
	Button_userinfo_close->addClickEventListener([=](Ref*){
		Panel_UserInfo->setVisible(false);
	});
}

bool ClubPartner::getMessageBack(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{
		SClubResponseBase* Data = (SClubResponseBase*)SocketMessage->object;
		log("sure======Message===%d", Data->msg);
		if (ClubRequest_QueryPartnerList == Data->msg)
		{
			// 查询合伙人列表
			SClubOperationResponse_QueryPartnerList* Result = (SClubOperationResponse_QueryPartnerList*)SocketMessage->object;
			if (Result->count > 0)
			{
				if (m_ListViewPartner)
				{
					Widget* item = m_ListViewPartner->getItem(0);
					if (item != nullptr)
					{
						m_ListViewPartner->removeAllItems();
					}
				}
				//保存数据
				for (int i = 0; i < Result->count; i++)
				{
					Vec_PartnerInfo.push_back(Result->infos[i]);
				}
				//更新列表
				UpdatePartnerList();
			}
			else
			{
				if (m_ListViewPartner)
				{
					Widget* item = m_ListViewPartner->getItem(0);
					if (item != nullptr)
					{
						m_ListViewPartner->removeAllItems();
					}
				}
				//压入暂无数据
				auto Layout_bk = Layout::create();
				Layout_bk->setSize(Size(1205, 470));
				Layout_bk->setBackGroundImage("platform/Club/res/userInfo/word_zanwushuju.png");
				m_ListViewPartner->pushBackCustomItem(Layout_bk);
			}
		}
		else if (ClubRequest_SearchMember == Data->msg)
		{
			SClubOperationResponse_SearchMember* Result = (SClubOperationResponse_SearchMember*)SocketMessage->object;
			if (Result->count > 0)
			{
				if (Vec_SearchMember.size() > 0)Vec_SearchMember.clear();
				for (int i = 0; i < Result->count; i++)
				{
					Vec_SearchMember.push_back(Result->members[i]);
				}
				//更新搜索列表
				UpdateSearchUserList();
			}
			else
			{
				if (ListView_SearchM)
				{
					Widget* item = ListView_SearchM->getItem(0);
					if (item != nullptr)
					{
						ListView_SearchM->removeAllItems();
					}
				}
				if (ListView_SearchAM)
				{
					Widget* item = ListView_SearchAM->getItem(0);
					if (item != nullptr)
					{
						ListView_SearchAM->removeAllItems();
					}
				}
				//查无数据
				GamePromptLayer::create()->showPrompt(GBKToUtf8("未搜索到该用户ID"));
			}
		}
		else if (ClubRequest_SetPartner == Data->msg)
		{
			SClubOperationResponse_SetPartner* Result = (SClubOperationResponse_SetPartner*)SocketMessage->object;
			if (Result->bSetPartner)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("设置成功"));
			}
			else
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("移除成功"));
			}
			//关闭添加合伙人
			setAddPartnerVisible(false);
			//查询最新合伙人列表(合伙人数量变化)
			SendQueryPartnerList();
		}
		else if (ClubRequest_SetMemberOfPartner == Data->msg)
		{
			SClubOperationResponse_SetMemberOfPartner* Result = (SClubOperationResponse_SetMemberOfPartner*)SocketMessage->object;
			GamePromptLayer::create()->showPrompt(GBKToUtf8("修改成功"));
			//关闭成员管理
			Panel_AddMembers->setVisible(false);
			if (ListView_SearchAM)
			{
				Widget* item = ListView_SearchAM->getItem(0);
				if (item != nullptr)
				{
					ListView_SearchAM->removeAllItems();
				}
			}
			//更新成员列表
			if (ListView_Member)
			{
				if (Panel_MemberList->isVisible())
				{
					SendQueryMember(m_iPartnerID);
				}
				//查询最新合伙人列表(成员数变化)
				SendQueryPartnerList();
			}
			else if (P_ListView_PartnerMember)
			{
				if (P_ListView_PartnerMember->getParent()->isVisible())
				{
					SendQueryMember(m_iPartnerID);
				}
			}
		}
		else if (ClubRequest_SetPartnerRedFlowersRate == Data->msg)
		{
			SClubResponseBase* Result = (SClubResponseBase*)SocketMessage->object;
			for (auto TextFlowerRate: Vec_RedFlowerRate)
			{
				if (TextFlowerRate->getTag() == m_iPartnerID)
				{
					int Rate = stoi(TextField_ModifyRedFRate->getString());
					TextFlowerRate->setString(StringUtils::format("%d%%", Rate));
				}
			}
		}
		else if (ClubRequest_QueryMemberList == Data->msg)
		{
			SClubOperationResponse_QueryMemberList* Data = (SClubOperationResponse_QueryMemberList*)SocketMessage->object;
			//m_iCreaterID = Data->iCreaterID;

			if (Data->count > 0)
			{
				for (int i = 0; i<Data->count; i++)
				{
					MemberInfo.push_back(Data->memberInfos[i]);
				}
			}
			if (P_ListView_PartnerMember)
			{
				UpDatePartnerMember();
			}
			else if (ListView_Member)
			{
				UpdateMember();
			}
		}
		else if (ClubRequest_QueryUserGameLog == Data->msg)
		{
			SClubOperationResponse_QueryUserGameLog* Result = (SClubOperationResponse_QueryUserGameLog*)SocketMessage->object;
			if (Result->count > 0)
			{
				if (ListView_Record)
				{
					Widget* item = ListView_Record->getItem(0);
					if (item != nullptr)
					{
						ListView_Record->removeAllItems();
					}
					setRecordInfo(Result);
				}
			}
			else
			{
				//暂无战绩处理
				if (ListView_Record)
				{
					Widget* item = ListView_Record->getItem(0);
					if (item != nullptr)
					{
						ListView_Record->removeAllItems();
					}

					auto Layout_bk = Layout::create();
					Layout_bk->setSize(Size(812, 260));
					Layout_bk->setBackGroundImage("platform/Club/res/userInfo/word_zanwushuju.png");
					ListView_Record->pushBackCustomItem(Layout_bk);
				}
			}
		}
		else if (ClubRequest_GiveRedFlowers == Data->msg)
		{
			SClubOperationResponse_GiveRedFlowers* Result = (SClubOperationResponse_GiveRedFlowers*)SocketMessage->object;
			//operationID = Result->targetUserId;
			if (P_ListView_PartnerMember)
			{
				if (P_ListView_PartnerMember->getParent()->isVisible())
				{
					SendQueryMember(m_iPartnerID);
				}
			}
			Text_Redflower->setString(to_string((int)Result->dstRedFlowers));
			GamePromptLayer::create()->showPrompt(GBKToUtf8("赠送成功！"));
		}
		else if (ClubRequest_TakeOutRedFlowers == Data->msg)
		{
			SClubOperationResponse_TakeOutRedFlowers* Result = (SClubOperationResponse_TakeOutRedFlowers*)SocketMessage->object;
			//operationID = Result->targetUserId;
			if (P_ListView_PartnerMember)
			{
				if (P_ListView_PartnerMember->getParent()->isVisible())
				{
					SendQueryMember(m_iPartnerID);
				}
			}
			Text_Redflower->setString(to_string((int)Result->dstRedFlowers));
			GamePromptLayer::create()->showPrompt(GBKToUtf8("摘花成功！"));
		}
		else if (ClubRequest_QueryPartnerInfo == Data->msg)
		{
			SClubOperationResponse_QueryPartnerInfo* Result = (SClubOperationResponse_QueryPartnerInfo*)SocketMessage->object;
			if (P_Text_redFlowerRate)
			{
				P_Text_redFlowerRate->setString(StringUtils::format("%d%%", (int)(Result->redFlowersRate*100)));
				P_Text_reap_redFlower->setString(StringUtils::format("%.2f", Result->rebate_today));
				m_uiAccess = Result->clubAccess;
				if (P_Button_operation)
				{
					if (m_uiAccess & ClubAccess_AllowAdminTakeRedFlower)
					{
						P_Button_operation->loadTextures("platform/Club/res/ClubPartner/btn_jzjcz.png","platform/Club/res/ClubPartner/btn_jzjcz.png","platform/Club/res/ClubPartner/btn_jzjcz.png");
					}
					else
					{
						P_Button_operation->loadTextures("platform/Club/res/ClubPartner/btn_qzkcz.png", "platform/Club/res/ClubPartner/btn_qzkcz.png", "platform/Club/res/ClubPartner/btn_qzkcz.png");
					}
				}
			}
		}
		else if (ClubRequest_AllowAdminTakeRedFlower == Data->msg)
		{
			SClubOperationResponse_AllowAdminTakeRedFlower* Result = (SClubOperationResponse_AllowAdminTakeRedFlower*)SocketMessage->object;
			if (Result->bAllow)
			{
				P_Button_operation->loadTextures("platform/Club/res/ClubPartner/btn_jzjcz.png", "platform/Club/res/ClubPartner/btn_jzjcz.png", "platform/Club/res/ClubPartner/btn_jzjcz.png");
			}
			else
			{
				P_Button_operation->loadTextures("platform/Club/res/ClubPartner/btn_qzkcz.png", "platform/Club/res/ClubPartner/btn_qzkcz.png", "platform/Club/res/ClubPartner/btn_qzkcz.png");
			}
			//查询当前合伙人红花比例，红花收入
			SClubRequestBase Data;
			Data.msg = ClubRequest_QueryPartnerInfo;
			Data.clubId = m_iClubID;
			Data.gameNameId = 0;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequestBase));
		}
		else
		{

		}

	}
	return true;
}

//发送查询
void ClubPartner::SendQueryPartnerList()
{
	if (Vec_PartnerInfo.size() > 0)
	{
		Vec_PartnerInfo.clear();
	}
	SClubRequest_QueryPartnerList Data;
	Data.msg = ClubRequest_QueryPartnerList;
	Data.clubId = m_iClubID;
	Data.gameNameId = 0;
	Data.query_day = Query_Day;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_QueryPartnerList));
}

//发送查询成员列表
void ClubPartner::SendPartnerUserList()
{
	//查询当前合伙人红花比例，红花收入
	SClubRequestBase Data;
	Data.msg = ClubRequest_QueryPartnerInfo;
	Data.clubId = m_iClubID;
	Data.gameNameId = 0;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequestBase));
	SendQueryMember(m_iPartnerID);
}

//发送查询某天的合作群列表
void ClubPartner::SendQueryParterList(int Index)
{
	m_MoreClose->setVisible(false);
	m_ImgMore->setVisible(false);
	m_TextTime->setString(GBKToUtf8(TEA_HOUSE_DAYLIST[Index]));
	if (Query_Day == Index)
	{
		return;
	}
	Query_Day = Index;
	SendQueryPartnerList();
}

//查询成员列表
void ClubPartner::SendQueryMember(int partnerUserId)
{
	if (MemberInfo.size() > 0) MemberInfo.clear();
	SClubRequest_QueryMemberList Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_QueryMemberList;
	Date.query_day = -1;			//发送查询时间（-1=所有记录 0=今日 1=昨日 2=前日）
	Date.partnerUserId = partnerUserId;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_QueryMemberList));
}

void ClubPartner::SendMemberRecord(int UserID)
{
	SClubRequest_QueryUserGameLog Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_QueryUserGameLog;
	Date.findUserId = UserID;
	Date.query_day = -1;			//发送查询时间（-1=所有记录 0=今日 1=昨日 2=前日）
	//Date.partnerUserId = m_iPartnerID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_QueryUserGameLog));
}

//更新合作群列表
void ClubPartner::UpdatePartnerList()
{
	if (m_ListViewPartner)
	{
		if (Vec_RedFlowerRate.size() > 0)
		{
			Vec_RedFlowerRate.clear();
		}
		for (int i = 0; i < Vec_PartnerInfo.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/ClubPartner/ClubPartnerNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("Panel_Clone");

			//用户头像
			auto Image_head = (ImageView*)panel_clone->getChildByName("Image_head");
			auto _userHead = GameUserHead::create(Image_head);
			_userHead->show("platform/head/men_head.png");
			//_userHead->setHeadByFaceID(Data.iLogoID);
			_userHead->setName("Temp_Head");
			_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Vec_PartnerInfo[i].partnerUserId));

			//用户名字
			auto Text_name = (Text*)panel_clone->getChildByName("Text_name");
			Tools::TrimSpace(Vec_PartnerInfo[i].szNickName);
			string str = Vec_PartnerInfo[i].szNickName;
			if (str.length() > 12)
			{
				str.erase(13, str.length());
				str.append("...");
				Text_name->setString(GBKToUtf8(str));
			}
			else
			{
				Text_name->setString(GBKToUtf8(str));
			}

			//用户ID
			auto Text_userid = (Text*)panel_clone->getChildByName("Text_userid");
			Text_userid->setString(to_string(Vec_PartnerInfo[i].partnerUserId));

			//房间数
			auto Text_roomNum = (Text*)panel_clone->getChildByName("Text_roomNum");
			Text_roomNum->setString(to_string(Vec_PartnerInfo[i].totalRoomCount));

			//总人数
			auto Text_playerNum = (Text*)panel_clone->getChildByName("Text_playerNum");
			Text_playerNum->setString(to_string(Vec_PartnerInfo[i].totalUserCount));

			//总大赢家
			auto Text_bigwinNum = (Text*)panel_clone->getChildByName("Text_bigwinNum");
			Text_bigwinNum->setString(to_string(Vec_PartnerInfo[i].totalBigWinnerCount));

			//成员数
			auto Text_memberNum = (Text*)panel_clone->getChildByName("Text_memberNum");
			Text_memberNum->setString(to_string(Vec_PartnerInfo[i].memberCount));

			//赠送数
			auto Text_giveNum = (Text*)panel_clone->getChildByName("Text_giveNum");
			Text_giveNum->setString(to_string(Vec_PartnerInfo[i].giveCount));

			//合作详情
			auto Button_PartnerInfo = (Button*)panel_clone->getChildByName("Button_PartnerInfo");
			Button_PartnerInfo->addClickEventListener([=](Ref*){
				//当前合作者的红花变化记录
				ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
				ClubRedFlow->SetClubID(m_iClubID);
				ClubRedFlow->initCSBByType(3, Vec_PartnerInfo[i].partnerUserId, true);
				ClubRedFlow->setName("ClubRedFlow");
				this->addChild(ClubRedFlow);
			});

			//红花比例分配
			auto Button_RedFlower_P = (Button*)panel_clone->getChildByName("Button_RedFlower_P");
			Button_RedFlower_P->addClickEventListener([=](Ref*){
				//调整红花比例界面
				if (Panel_ModifyRedFRate)
				{
					TextField_ModifyRedFRate->setString("");
					m_iPartnerID = Vec_PartnerInfo[i].partnerUserId;
					Panel_ModifyRedFRate->setVisible(true);
				}
			});

			//红花比例显示
			auto Text_RedFlower_T = (Text*)Button_RedFlower_P->getChildByName("Text");
			Text_RedFlower_T->setString(StringUtils::format("%d%%", (int)(Vec_PartnerInfo[i].redFlowersRate * 100)));
			Text_RedFlower_T->setTag(Vec_PartnerInfo[i].partnerUserId);
			Vec_RedFlowerRate.push_back(Text_RedFlower_T);

			//分配
			auto Button_allot = (Button*)panel_clone->getChildByName("Button_allot");
			Button_allot->addClickEventListener([=](Ref*){
				//分配合作群成员（添加底下成员）
				if (Panel_AddMembers)
				{
					TextField_SearchAM->setString("");
					m_iPartnerID = Vec_PartnerInfo[i].partnerUserId;
					Panel_AddMembers->setVisible(true);
				}
			});

			//详情
			auto Button_details = (Button*)panel_clone->getChildByName("Button_details");
			Button_details->addClickEventListener([=](Ref*){
				//底下成员列表
				if (Panel_MemberList)
				{
					Panel_MemberList->setVisible(true);
					//查询合作群成员列表
					m_iPartnerID = Vec_PartnerInfo[i].partnerUserId;
					m_uiAccess = Vec_PartnerInfo[i].clubAccess;
					SendQueryMember(m_iPartnerID);
				}
			});

			//删除合作人
			auto Button_delete = (Button*)panel_clone->getChildByName("Button_delete");
			Button_delete->addClickEventListener([=](Ref*){
				auto SendDisbandClub = [=](){
					SClubRequest_SetPartner Date;
					Date.bSetPartner = false;
					Date.clubId = m_iClubID;
					Date.gameNameId = 0;
					Date.msg = ClubRequest_SetPartner;
					Date.userId = Vec_PartnerInfo[i].partnerUserId;
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetPartner));
				};
				auto prompt = GamePromptLayer::create(true);
				prompt->showPrompt(GBKToUtf8("您确定删除该合作群？"));
				prompt->setCallBack([=](){
					SendDisbandClub();
				});
			});

			panel_clone->removeFromParentAndCleanup(true);
			m_ListViewPartner->pushBackCustomItem(panel_clone);
		}
	}
}

void ClubPartner::UpdateMember()
{
	//初始化清空列表
	if (ListView_Member != nullptr)
	{
		Widget* item = ListView_Member->getItem(0);
		if (item != nullptr)
		{
			ListView_Member->removeAllItems();
		}
	}

	if (MemberInfo.size() > 0)
	{
		//Text_PartnerNum->setString(to_string(MemberInfo.size()));
		for (int i = 0; i < MemberInfo.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/ClubPartner/MembersNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("Panel_Clone");

			//用户ID
			auto Text_UserID = (Text*)panel_clone->getChildByName("Text_userid");
			Text_UserID->setString(to_string(MemberInfo[i].userId));

			//用户名字
			auto Text_UserName = (Text*)panel_clone->getChildByName("Text_name");
			Tools::TrimSpace(MemberInfo[i].szUserName);
			std::string nickName(MemberInfo[i].szUserName);
			if (!nickName.empty())
			{
				string str = MemberInfo[i].szUserName;
				if (str.length() > 12)
				{
					str.erase(13, str.length());
					str.append("...");
					Text_UserName->setString(GBKToUtf8(str));
				}
				else
				{
					Text_UserName->setString(GBKToUtf8(str));
				}
			}
			else
			{
				Text_UserName->setString(GBKToUtf8("未知"));
			}

			//用户头像
			auto Image_head = (ImageView*)panel_clone->getChildByName("Image_head");
			auto _userHead = GameUserHead::create(Image_head);
			_userHead->show("platform/head/men_head.png");
			//_userHead->setHeadByFaceID(Data.iLogoID);
			_userHead->setName("Temp_Head");
			_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(MemberInfo[i].userId));

			//房间次数
			auto Text_roomNum = (Text*)panel_clone->getChildByName("Text_roomNum");
			Text_roomNum->setString(to_string(MemberInfo[i].roomCount));

			//大赢家次数
			auto Text_WinPlayerNum = (Text*)panel_clone->getChildByName("Text_bigwinNum");
			Text_WinPlayerNum->setString(to_string(MemberInfo[i].bigWinnerCount));

			//用户详情
			auto Button_UserInfo = (Button*)panel_clone->getChildByName("Button_UserInfo");
			Button_UserInfo->addClickEventListener([=](Ref*){
				if (MemberInfo.size() > 0)
				{
					ShowUserInfoDump(MemberInfo[i]);
				}
			});

			//删除用户
			auto Button_Delete = (Button*)panel_clone->getChildByName("Button_Delete");
			Button_Delete->addClickEventListener([=](Ref*){
				auto SendData = [=](){
					SClubRequest_SetMemberOfPartner Date;
					Date.bAdd = false;
					Date.partnerUserId = m_iPartnerID;
					Date.clubId = m_iClubID;
					Date.gameNameId = 0;
					Date.inviteUserId = MemberInfo[i].userId;
					Date.msg = ClubRequest_SetMemberOfPartner;
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetMemberOfPartner));
				};
				auto prompt = GamePromptLayer::create(true);
				prompt->showPrompt(GBKToUtf8("您确定删除该合作人下级成员？"));
				prompt->setCallBack([=](){
					SendData();
				});
			});

			panel_clone->removeFromParentAndCleanup(true);
			ListView_Member->pushBackCustomItem(panel_clone);
		}
	}
	//更新合伙人人数
	Text_PartnerNum->setString(to_string(MemberInfo.size()));
}

void ClubPartner::UpDatePartnerMember()
{
	if (P_ListView_PartnerMember)
	{
		Widget* item = P_ListView_PartnerMember->getItem(0);
		if (item != nullptr)
		{
			P_ListView_PartnerMember->removeAllItems();
		}
	}
	int RoomNum = 0;
	int BigWinNum = 0;
	int PPlayerNum = 0;
	int PlayerNum = 0;
	if (MemberInfo.size() > 0)
	{
		for (int i = 0; i < MemberInfo.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/ClubPartner/PartnerMemberNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("Panel_Clone");

			//用户ID
			auto Text_UserID = (Text*)panel_clone->getChildByName("Text_userid");
			Text_UserID->setString(to_string(MemberInfo[i].userId));

			//用户名字
			auto Text_UserName = (Text*)panel_clone->getChildByName("Text_name");
			Tools::TrimSpace(MemberInfo[i].szUserName);
			std::string nickName(MemberInfo[i].szUserName);
			if (!nickName.empty())
			{
				string str = MemberInfo[i].szUserName;
				if (str.length() > 12)
				{
					str.erase(13, str.length());
					str.append("...");
					Text_UserName->setString(GBKToUtf8(str));
				}
				else
				{
					Text_UserName->setString(GBKToUtf8(str));
				}
			}
			else
			{
				Text_UserName->setString(GBKToUtf8("未知"));
			}

			//用户头像
			auto Image_head = (ImageView*)panel_clone->getChildByName("Image_head");
			auto _userHead = GameUserHead::create(Image_head);
			_userHead->show("platform/head/men_head.png");
			//_userHead->setHeadByFaceID(Data.iLogoID);
			_userHead->setName("Temp_Head");
			_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(MemberInfo[i].userId));

			//房间次数
			auto Text_roomNum = (Text*)panel_clone->getChildByName("Text_roomNum");
			Text_roomNum->setString(to_string(MemberInfo[i].roomCount));
			RoomNum += MemberInfo[i].roomCount;

			//大赢家次数
			auto Text_WinPlayerNum = (Text*)panel_clone->getChildByName("Text_bigwinNum");
			Text_WinPlayerNum->setString(to_string(MemberInfo[i].bigWinnerCount));
			BigWinNum += MemberInfo[i].bigWinnerCount;

			//战绩次数
			auto Text_record = (Text*)panel_clone->getChildByName("Text_record");
			Text_record->setString(to_string((int)MemberInfo[i].winRedFlowers));
			if (MemberInfo[i].winCount)
			{
				PPlayerNum++;
			}

			//红花数量
			auto Text_redFlower = (Text*)panel_clone->getChildByName("Text_redFlower");
			Text_redFlower->setString(to_string((int)MemberInfo[i].redFlowers));
			PlayerNum++;

			//用户详情
			auto Button_UserInfo = (Button*)panel_clone->getChildByName("Button_UserInfo");
			Button_UserInfo->addClickEventListener([=](Ref*){
				if (MemberInfo.size() > 0)
				{
					ShowUserInfoDump(MemberInfo[i]);
				}
			});

			//删除用户
			auto Button_Delete = (Button*)panel_clone->getChildByName("Button_Delete");
			Button_Delete->addClickEventListener([=](Ref*){
				auto SendData = [=](){
					SClubRequest_SetMemberOfPartner Date;
					Date.bAdd = false;
					Date.partnerUserId = m_iPartnerID;
					Date.clubId = m_iClubID;
					Date.gameNameId = 0;
					Date.inviteUserId = MemberInfo[i].userId;
					Date.msg = ClubRequest_SetMemberOfPartner;
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetMemberOfPartner));
				};
				auto prompt = GamePromptLayer::create(true);
				prompt->showPrompt(GBKToUtf8("您确定删除该合作人下级成员？"));
				prompt->setCallBack([=](){
					SendData();
				});
			});

			panel_clone->removeFromParentAndCleanup(true);
			P_ListView_PartnerMember->pushBackCustomItem(panel_clone);
		}
	}

	//数据统计
	P_Text_RoomNum->setString(to_string(RoomNum));
	P_Text_BigWinNum->setString(to_string(BigWinNum));
	P_Text_PPlayerNum->setString(to_string(PPlayerNum));
	P_Text_PlayerNum->setString(to_string(PlayerNum));
}

void ClubPartner::UpdateSearchUserList()
{
	//搜索成员添加到合伙人列表
	if (Panel_AddPartner->isVisible())
	{
		Widget* item = ListView_SearchM->getItem(0);
		if (item != nullptr)
		{
			ListView_SearchM->removeAllItems();
		}
		for (int i = 0; i < Vec_SearchMember.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/ClubPartner/SearchMemberNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("Panel_Clone");

			//用户头像
			auto Image_head = (ImageView*)panel_clone->getChildByName("Image_head");
			auto _userHead = GameUserHead::create(Image_head);
			_userHead->show("platform/head/men_head.png");
			//_userHead->setHeadByFaceID(Data.iLogoID);
			_userHead->setName("Temp_Head");
			_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Vec_SearchMember[i].userId));

			//用户名字
			auto Text_Name = (Text*)panel_clone->getChildByName("Text_Name");
			Tools::TrimSpace(Vec_SearchMember[i].szNickName);
			string str = Vec_SearchMember[i].szNickName;
			if (str.length() > 12)
			{
				str.erase(13, str.length());
				str.append("...");
				Text_Name->setString(GBKToUtf8(str));
			}
			else
			{
				Text_Name->setString(GBKToUtf8(str));
			}

			//用户ID
			auto Text_UserID = (Text*)panel_clone->getChildByName("Text_UserID");
			Text_UserID->setString(to_string(Vec_SearchMember[i].userId));

			//是否能添加合伙人
			auto Button_Add = (Button*)panel_clone->getChildByName("Button_Add");
			if (Vec_SearchMember[i].clubAccess & ClubAccess_Admin || ClubAccess_Partner & Vec_SearchMember[i].clubAccess)
			{
				Button_Add->setEnabled(false);
			}
			else
			{
				Button_Add->setEnabled(true);
			}
			Button_Add->addClickEventListener([=](Ref*){
				if (Vec_SearchMember[i].partnerUserId != 0)
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("该玩家当前为合伙人下级，无法设置合伙人"));
					return;
				}
				SClubRequest_SetPartner Date;
				Date.bSetPartner = true;
				Date.clubId = m_iClubID;
				Date.gameNameId = 0;
				Date.msg = ClubRequest_SetPartner;
				Date.userId = Vec_SearchMember[i].userId;
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetPartner));
			});

			panel_clone->removeFromParentAndCleanup(true);
			ListView_SearchM->pushBackCustomItem(panel_clone);
		}
	}
	//搜索成员添加到当前下级
	else if (Panel_AddMembers->isVisible())
	{
		Widget* item = ListView_SearchAM->getItem(0);
		if (item != nullptr)
		{
			ListView_SearchAM->removeAllItems();
		}
		for (int i = 0; i < Vec_SearchMember.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/ClubPartner/SearchMemberNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("Panel_Clone");

			//用户头像
			auto Image_head = (ImageView*)panel_clone->getChildByName("Image_head");
			auto _userHead = GameUserHead::create(Image_head);
			_userHead->show("platform/head/men_head.png");
			//_userHead->setHeadByFaceID(Data.iLogoID);
			_userHead->setName("Temp_Head");
			_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Vec_SearchMember[i].userId));

			//用户名字
			auto Text_Name = (Text*)panel_clone->getChildByName("Text_Name");
			Tools::TrimSpace(Vec_SearchMember[i].szNickName);
			string str = Vec_SearchMember[i].szNickName;
			if (str.length() > 12)
			{
				str.erase(13, str.length());
				str.append("...");
				Text_Name->setString(GBKToUtf8(str));
			}
			else
			{
				Text_Name->setString(GBKToUtf8(str));
			}

			//用户ID
			auto Text_UserID = (Text*)panel_clone->getChildByName("Text_UserID");
			Text_UserID->setString(to_string(Vec_SearchMember[i].userId));

			//是否能添加合伙人
			auto Button_Add = (Button*)panel_clone->getChildByName("Button_Add");
			if (Vec_SearchMember[i].clubAccess & ClubAccess_Admin || ClubAccess_Partner & Vec_SearchMember[i].clubAccess)
			{
				Button_Add->setEnabled(false);
			}
			else if (Vec_SearchMember[i].partnerUserId != 0)
			{
				Button_Add->setEnabled(false);
			}
			else
			{
				Button_Add->setEnabled(true);
			}
			Button_Add->addClickEventListener([=](Ref*){
				if (Vec_SearchMember[i].partnerUserId != 0)
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("该玩家当前已有合伙人，无法设置"));
					return;
				}
				SClubRequest_SetMemberOfPartner Date;
				Date.bAdd = true;
				Date.clubId = m_iClubID;
				Date.gameNameId = 0;
				Date.msg = ClubRequest_SetMemberOfPartner;
				Date.inviteUserId = Vec_SearchMember[i].userId;
				Date.partnerUserId = m_iPartnerID;
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetMemberOfPartner));
			});

			panel_clone->removeFromParentAndCleanup(true);
			ListView_SearchAM->pushBackCustomItem(panel_clone);
		}
	}
}

void ClubPartner::setAddPartnerVisible(bool bShow)
{
	Panel_AddPartner->setVisible(bShow);
	if (!bShow)
	{
		//清理列表
		if (ListView_SearchM)
		{
			Widget* item = ListView_SearchM->getItem(0);
			if (item != nullptr)
			{
				ListView_SearchM->removeAllItems();
			}
		}
	}
	else
	{
		if (TextField_Search)
		{
			TextField_Search->setString("");
		}
	}
}

void ClubPartner::SearchUserByID()
{
	//搜索成员添加合伙人
	string str1 = TextField_Search->getString();
	auto name_len1 = strlen(Utf8ToGBK(str1));
	bool isNum1 = Tools::verifyNumber(str1);
	SClubRequest_SearchMember Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_SearchMember;
	if (isNum1 && name_len1 > 0)
	{	
		Date.searchUserId = stoi(str1);
	}
	else if (name_len1 > 0)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为用户ID"));
		return;
	}
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SearchMember));
}

void ClubPartner::SearchUserInfo()
{
	//搜索成员添加合伙人下级
	string str1 = TextField_SearchAM->getString();
	auto name_len1 = strlen(Utf8ToGBK(str1));
	bool isNum1 = Tools::verifyNumber(str1);
	SClubRequest_SearchMember Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_SearchMember;
	if (isNum1 && name_len1 > 0)
	{
		Date.searchUserId = stoi(str1);
	}
	else if (name_len1 > 0)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为用户ID"));
		return;
	}
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SearchMember));
}

void ClubPartner::ModifyRedFlowerRate()
{
	//修改红花比例
	string str1 = TextField_ModifyRedFRate->getString();
	auto name_len1 = strlen(Utf8ToGBK(str1));
	bool isNum1 = Tools::verifyNumber(str1);
	SClubRequest_SetPartnerRedFlowersRate Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_SetPartnerRedFlowersRate;
	if (isNum1 && name_len1 > 0)
	{
		int Rate = stoi(str1);
		if (Rate < 0 || Rate > 100)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("输入范围为0-100"));
			return;
		}
		Date.rate = (float)Rate / 100;
		Date.partnerUserId = m_iPartnerID;
	}
	else if (name_len1 > 0)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为数字"));
		return;
	}
	//发送比例时隐藏输入界面（修改成功需要获取输入数据）
	Panel_ModifyRedFRate->setVisible(false);
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SearchMember));
}

void ClubPartner::ShowUserInfoDump(SClubMemberInfo Data)
{
	if (Panel_UserInfo)
	{
		Panel_UserInfo->setVisible(true);

		//头像
		auto Image_head = (ImageView*)Panel_UserInfo->getChildByName("Image_head");
		auto _userHead = GameUserHead::create(Image_head);
		_userHead->show("platform/head/men_head.png");
		//_userHead->setHeadByFaceID(Data.iLogoID);
		_userHead->setName("Temp_Head");
		_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Data.userId));

		//用户名字
		auto Text_Name = (Text*)Panel_UserInfo->getChildByName("Text_name");
		Tools::TrimSpace(Data.szUserName);
		string str = Data.szUserName;
		if (str.length() > 16)
		{
			str.erase(17, str.length());
			str.append("...");
			Text_Name->setString(GBKToUtf8(str));
		}
		else
		{
			Text_Name->setString(GBKToUtf8(str));
		}

		//用户ID
		auto Text_UserID = (Text*)Panel_UserInfo->getChildByName("Text_userid");
		Text_UserID->setString(to_string(Data.userId));

		//备注
		auto Text_Remark = (Text*)Panel_UserInfo->getChildByName("Text_remark");

		//赠送
		auto Button_give = (Button*)Panel_UserInfo->getChildByName("Button_give");
		Button_give->addClickEventListener([=](Ref*){
			if (panel_Dump && m_uiAccess & ClubAccess_AllowAdminTakeRedFlower || m_uiAccess &ClubAccess_Partner)
			{
				//红花数量
				auto Text_Redflower1 = (Text*)panel_Dump->getChildByName("Text_Redflower");
				Text_Redflower1->setString(Text_Redflower->getString());

				//头像
				auto Image_head1 = (ImageView*)panel_Dump->getChildByName("Image_head");
				auto _userHead1 = GameUserHead::create(Image_head1);
				_userHead1->show("platform/head/men_head.png");
				//_userHead1->setHeadByFaceID(Data.iLogoID);
				_userHead1->setName("Temp_Head");
				_userHead1->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Data.userId));

				//名字
				auto Text_Name1 = (Text*)panel_Dump->getChildByName("Text_Name");
				Text_Name1->setString(Text_Name->getString());

				//ID
				auto Text_UserID1 = (Text*)panel_Dump->getChildByName("Text_UserID");
				Text_UserID1->setString(Text_UserID->getString());

				RedFlowerType = 1;
				TextField_RedFlower->setString("");
				Sprite_Type->setTexture("platform/Club/res/userInfo/word_zengsong.png");
				panel_Dump->setVisible(true);
			}
			else
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("当前合伙人禁止管理员送成员红花"));
			}
		});

		//摘花
		auto Button_pick = (Button*)Panel_UserInfo->getChildByName("Button_pick");
		Button_pick->addClickEventListener([=](Ref*){
			if (panel_Dump && m_uiAccess & ClubAccess_AllowAdminTakeRedFlower || m_uiAccess &ClubAccess_Partner)
			{
				//红花数量
				auto Text_Redflower1 = (Text*)panel_Dump->getChildByName("Text_Redflower");
				Text_Redflower1->setString(Text_Redflower->getString());

				//头像
				auto Image_head1 = (ImageView*)panel_Dump->getChildByName("Image_head");
				auto _userHead1 = GameUserHead::create(Image_head1);
				_userHead1->show("platform/head/men_head.png");
				//_userHead1->setHeadByFaceID(Data.iLogoID);
				_userHead1->setName("Temp_Head");
				_userHead1->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Data.userId));

				//名字
				auto Text_Name1 = (Text*)panel_Dump->getChildByName("Text_Name");
				Text_Name1->setString(Text_Name->getString());

				//ID
				auto Text_UserID1 = (Text*)panel_Dump->getChildByName("Text_UserID");
				Text_UserID1->setString(Text_UserID->getString());

				RedFlowerType = 2;
				TextField_RedFlower->setString("");
				Sprite_Type->setTexture("platform/Club/res/userInfo/word_zhaiqu.png");
				panel_Dump->setVisible(true);
			}
			else
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("当前合伙人禁止管理员摘成员红花"));
			}
		});

		//红花变化
		auto Button_change = (Button*)Panel_UserInfo->getChildByName("Button_change");
		Button_change->addClickEventListener([=](Ref*){
			ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
			ClubRedFlow->SetClubID(m_iClubID);
			ClubRedFlow->initCSBByType(1, Data.userId, true);
			ClubRedFlow->setName("ClubRedFlow");
			this->addChild(ClubRedFlow);
		});

		//待处理
		auto Button_pending = (Button*)Panel_UserInfo->getChildByName("Button_pending");
		Button_pending->addClickEventListener([=](Ref*){
			ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
			ClubRedFlow->SetClubID(m_iClubID);
			ClubRedFlow->initCSBByType(2, Data.userId, false);
			ClubRedFlow->setName("ClubRedFlow");
			this->addChild(ClubRedFlow);
		});

		//红花
		auto Sprite_Redflower = (Sprite*)Panel_UserInfo->getChildByName("Sprite_Redflower");
		Text_Redflower = (Text*)Sprite_Redflower->getChildByName("Text_Redflower");
		Text_Redflower->setString(StringUtils::format("%d", (int)Data.redFlowers));
		/*if (Data.access & ClubAccess_Admin)
		{
			Text_Redflower->setString(StringUtils::format("%.2f", Data.redFlowers));
		}
		else if (Data.access & ClubAccess_Partner)
		{
			Text_Redflower->setString(StringUtils::format("%.2f", Data.redFlowers));
		}
		else
		{
			Text_Redflower->setString(StringUtils::format("%d", (int)Data.redFlowers));
		}*/

		//送花摘花弹窗
		panel_Dump = (Layout*)m_NodePartner->getChildByName("panel_Dump");

		//赠送或者摘花操作
		Sprite_Type = (Sprite*)panel_Dump->getChildByName("Sprite_Type");

		//赠送（摘花）数量
		TextField_RedFlower = (TextField*)panel_Dump->getChildByName("TextField_RedFlower");
		TextField_RedFlower->setString("");
		TextField_RedFlower->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextField_RedFlower->setTextVerticalAlignment(TextVAlignment::CENTER);

		//确定
		auto Button_Sure = (Button*)panel_Dump->getChildByName("Button_Sure");
		Button_Sure->addClickEventListener([=](Ref*){
			//确定发送赠花摘花
			SClubRequest_GiveRedFlowers Date;
			int NowNum = 0;
			if (RedFlowerType == 1)
			{
				Date.msg = ClubRequest_GiveRedFlowers;
			}
			else
			{
				Date.msg = ClubRequest_TakeOutRedFlowers;
			}
			Date.targetUserId = Data.userId;
			string str1 = TextField_RedFlower->getString();
			auto name_len1 = strlen(Utf8ToGBK(str1));
			bool isNum1 = Tools::verifyNumber(str1);
			if (isNum1 && name_len1 > 0)
			{
				Date.redFlowers = stoi(str1);
			}
			else if (!isNum1)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为整数数字"));
				return;
			}
			//是否可以摘花判断
			if (RedFlowerType == 2)
			{
				string str_red = Text_Redflower->getString();
				auto name_len = strlen(Utf8ToGBK(str_red));
				bool isNum = Tools::verifyNumber(str_red);
				if (isNum && name_len > 0)
				{
					NowNum = stoi(str_red);
					if (Date.redFlowers > NowNum)
					{
						GamePromptLayer::create()->showPrompt(GBKToUtf8("当前玩家红花不足，无法执行摘花操作！"));
						return;
					}
				}
			}
			Date.gameNameId = 0;
			Date.clubId = m_iClubID;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_GiveRedFlowers));
			if (panel_Dump)
			{
				panel_Dump->setVisible(false);
			}
		});

		//取消
		auto Button_close_D = (Button*)panel_Dump->getChildByName("Button_close");
		Button_close_D->addClickEventListener([=](Ref*){
			panel_Dump->setVisible(false);
		});

		//查询成员战绩
		SendMemberRecord(Data.userId);
	}
}

//更新成员战绩
void ClubPartner::setRecordInfo(SClubOperationResponse_QueryUserGameLog* Date)
{
	//更新用户弹窗战绩
	for (int i = 0; i < Date->count; i++)
	{
		auto node = CSLoader::createNode("platform/Club/res/userInfo/UserRecordNode.csb");
		auto panel_clone = (Layout*)node->getChildByName("Panel_clone");

		//时间
		time_t Time = Date->logList[i].date;
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		if (p->tm_hour >= 24)
		{
			p->tm_hour -= 24;
		}
		char BeginTime[64];
		strftime(BeginTime, sizeof(BeginTime), "%Y-%m-%d %H:%M:%S", p);
		auto Text_time = (Text*)panel_clone->getChildByName("Text_time_y");
		Text_time->setString(BeginTime);

		//房间号
		auto Text_roomID = (Text*)panel_clone->getChildByName("Text_roomID");
		//char szDeskPass[20];
		//sprintf(szDeskPass, "%03d%03d", Date->logList[i].roomId, Date->logList[i].deskIndex);
		Text_roomID->setString(Date->logList[i].szDeskPass);

		//分数
		auto Text_score = (Text*)panel_clone->getChildByName("Text_score");
		Text_score->setString(to_string(Date->logList[i].winScore));

		panel_clone->removeFromParentAndCleanup(true);
		ListView_Record->pushBackCustomItem(panel_clone);
	}
}
