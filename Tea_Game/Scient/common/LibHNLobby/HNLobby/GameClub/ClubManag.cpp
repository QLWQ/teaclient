#include "ClubManag.h"
#include "HNPlatform/HNPlatformClubData.h"
#include "../GamePersionalCenter/GameUserHead.h"
#include "ClubUserRedFlow.h"

static const char* TEA_HOUSE_MANAG_SCB_PATH = "platform/Club/GroupManageLayer.csb";

//按钮列表命名
static const std::string TEAHOUSE_MENUBUTTON[] = { "Button_RoomList", "Button_Member", "Button_Limit" };
static const int BUTTON_NUM = 3;

static const std::string TEAHOUSE_PANLELIST[] = { "panel_RoomList", "panel_Member", "panel_Limit" };
static const std::string TEAHOUSE_SELECTED[] = { "platform/Club/res/GroupManage/word_fjxq_l.png", "platform/Club/res/GroupManage/word_cylb_l.png", "platform/Club/res/GroupManage/word_tzxz_l.png" };
static const std::string TEAHOUSE_CONVEN[] = { "platform/Club/res/GroupManage/word_fjxq_a.png", "platform/Club/res/GroupManage/word_cylb_a.png", "platform/Club/res/GroupManage/word_tzxz_a.png" };
static const std::string TEA_HOUSE_DAYLIST[] = { "今日输赢", "昨日输赢", "前日输赢" };

ClubManag::ClubManag()
{
	MemberInfo.clear();
}

ClubManag::~ClubManag()
{
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest);//移除监听消息
}

bool ClubManag::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//加载csb文件（茶楼或者俱乐部）
	auto node = CSLoader::createNode(TEA_HOUSE_MANAG_SCB_PATH);
	//断言
	CCASSERT(node != nullptr, "node is null");
	//添加csb文件
	addChild(node);

	//保存节点
	Club_ManagNode = node;

	//初始化群管理
	//createManagScene();

	//优先查询成员列表数据
	//SendQueryMember();

	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, HN_SOCKET_CALLBACK(ClubManag::getMessageBack, this));

	return true;
}

void ClubManag::getTeaHouseSetInfo()
{
	//茶楼名称，大赢家最低分
	SClubRequestBase  Data;
	Data.gameNameId = 0;
	Data.msg = ClubRequest_QueryClubInfo;
	Data.clubId = m_iClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequestBase));
	//管理员账户
	SClubRequestBase  Data1;
	Data1.gameNameId = 0;
	Data1.msg = ClubRequest_GetAdminUsers;
	Data1.clubId = m_iClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data1, sizeof(SClubRequestBase));
}

//查询成员列表
void ClubManag::SendQueryMember()
{
	if (MemberInfo.size() > 0) MemberInfo.clear();
	SClubRequest_QueryMemberList Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_QueryMemberList;
	Date.query_day = Query_Day;			//发送查询时间（0=今日 1=昨日 2=前日）
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_QueryMemberList));
}

//查询同桌限制
void ClubManag::SendDeskmateLimit()
{
	SClubRequestBase Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_QueryDeskmateLimit;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequestBase));
}

void ClubManag::createManagScene()
{
	//优先查询成员列表数据
	SendQueryMember();

	//群设置背景
	Image_bg = (ImageView*)Club_ManagNode->getChildByName("Image_bg");

	//退出群管理界面
	auto Button_Return = (Button*)Image_bg->getChildByName("Button_Return");
	Button_Return->addClickEventListener([=](Ref*){
		this->removeFromParent();
	});

	//切换页面按钮
	for (int i = 0; i < BUTTON_NUM; i++)
	{
		auto Btn = (Button*)Image_bg->getChildByName(TEAHOUSE_MENUBUTTON[i]);
		if (Btn)
		{
			Btn->addClickEventListener(CC_CALLBACK_1(ClubManag::BtnCallBack, this));
		}
	}

	//切换页面容器初始化
	//房间列表
	panel_RoomList = (Layout*)Image_bg->getChildByName(TEAHOUSE_PANLELIST[0]);

	//成员列表
	panel_Member = (Layout*)Image_bg->getChildByName(TEAHOUSE_PANLELIST[1]);

	//同桌限制
	panel_Limit = (Layout*)Image_bg->getChildByName(TEAHOUSE_PANLELIST[2]);

	//设置页面
	Panel_teahouseset = (Layout*)Image_bg->getChildByName("Panel_teahouseset");
	Panel_teahouseset->addTouchEventListener([=](Ref* ref, Widget::TouchEventType type){
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		Panel_teahouseset->setVisible(false);
	});

	//老友圈昵称修改
	TextField_name = (TextField*)Panel_teahouseset->getChildByName("TextField_name");
	TextField_name->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_name->setTextVerticalAlignment(TextVAlignment::CENTER);

	//管理员账户1输入
	TextField_input1 = (TextField*)Panel_teahouseset->getChildByName("TextField_input1");
	TextField_input1->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_input1->setTextVerticalAlignment(TextVAlignment::CENTER);

	//管理员账户2输入
	TextField_input2 = (TextField*)Panel_teahouseset->getChildByName("TextField_input2");
	TextField_input2->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_input2->setTextVerticalAlignment(TextVAlignment::CENTER);

	//大赢家最低分数设置
	TextField_score = (TextField*)Panel_teahouseset->getChildByName("TextField_score");
	TextField_score->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_score->setTextVerticalAlignment(TextVAlignment::CENTER);

	//房间详情
	Panel_RoomInfo = (Layout*)Image_bg->getChildByName("Panel_RoomInfo");
	Panel_RoomInfo->addClickEventListener([=](Ref*){
		Panel_RoomInfo->setVisible(false);
	});

	//房间详情关闭
	auto Button_close_R = (Button*)Panel_RoomInfo->getChildByName("Button_sure");
	Button_close_R->addClickEventListener([=](Ref*){
		Panel_RoomInfo->setVisible(false);
	});

	//添加成员
	Panel_AddMembers = (Layout*)Image_bg->getChildByName("Panel_AddMembers");
	Panel_AddMembers->addClickEventListener([=](Ref*){
		Panel_AddMembers->setVisible(false);
	});

	//关闭
	auto Button_close_AM = (Button*)Panel_AddMembers->getChildByName("Button_close_AM");
	Button_close_AM->addClickEventListener([=](Ref*){
		Panel_AddMembers->setVisible(false);
	});

	//输入框
	TextField_SearchAM = (TextField*)Panel_AddMembers->getChildByName("TextField_SearchAM");
	TextField_SearchAM->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_SearchAM->setTextVerticalAlignment(TextVAlignment::CENTER);

	//搜索按钮
	auto Button_search = (Button*)Panel_AddMembers->getChildByName("Button_search");
	Button_search->addClickEventListener([=](Ref*){
		SearchInput();
	});

	//搜索列表
	ListView_SearchAM = (ListView*)Panel_AddMembers->getChildByName("ListView_SearchAM");

	//解散老友圈
	auto Button_dissolve = (Button*)Panel_teahouseset->getChildByName("Button_dissolve");
	Button_dissolve->addClickEventListener([=](Ref*){
		//权限判断（只有创建者可以解散）
		if (m_iCreaterID == PlatformLogic()->loginResult.dwUserID)
		{
			auto SendDisbandClub = [=](){
				if (_DissmissClubCallBack != nullptr)
				{
					_DissmissClubCallBack();
				}
				this->removeFromParent();
			};
			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(GBKToUtf8("您确定解散该老友圈？"));
			prompt->setCallBack([=](){
				SendDisbandClub();
			});
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("只有群主可以解散该老友圈。"));
		}
	});

	//大赢家最低分说明
	auto Panel_Close_Help = (Layout*)Panel_teahouseset->getChildByName("Panel_Close_Help");
	Panel_Close_Help->addTouchEventListener([=](Ref* ref, Widget::TouchEventType type){
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		Panel_Close_Help->setVisible(false);
	});

	//大赢家最低分说明关闭
	auto Button_close_Help = (Button*)Panel_Close_Help->getChildByName("Button_close_Help");
	Button_close_Help->addClickEventListener([=](Ref*){
		if (Panel_Close_Help)
		{
			Panel_Close_Help->setVisible(false);
		}
	});

	//关于大赢家最低分设置
	auto Button_Help = (Button*)Panel_teahouseset->getChildByName("Button_Help");
	Button_Help->addClickEventListener([=](Ref*){
		if (Panel_Close_Help)
		{
			Panel_Close_Help->setVisible(true);
		}
	});

	//暂停或者恢复房间
	auto Button_room_state = (Button*)Panel_teahouseset->getChildByName("Button_room_state");
	Button_room_state->addClickEventListener([=](Ref*){
		//判断当前暂停还是恢复
		auto Rest = HNPlatformClubData::getInstance()->getTeaHouseInfoByState(m_iClubID);
		if (Rest)
		{
			//恢复开房
			log("stard！！！");
			SClubRequest_PauseAllTeahouse  Data;
			Data.gameNameId = 0;
			Data.msg = ClubRequest_PauseAllTeahouse;
			Data.bPause = false;
			Data.clubId = m_iClubID;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_PauseAllTeahouse));
			Panel_teahouseset->setVisible(false);
		}
		else
		{
			//暂停开房
			log("stop！！！");
			SClubRequest_PauseAllTeahouse  Data;
			Data.gameNameId = 0;
			Data.msg = ClubRequest_PauseAllTeahouse;
			Data.bPause = true;
			Data.clubId = m_iClubID;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_PauseAllTeahouse));
			Panel_teahouseset->setVisible(false);
		}
	});

	//确定按钮
	auto Button_sure = (Button*)Panel_teahouseset->getChildByName("Button_sure");
	Button_sure->addClickEventListener([=](Ref*){
		//确定之后发送修改

		//发送修改茶楼名字，大赢家最低分
		SClubRequest_ModifyClubInfo Date;
		string str_name = Utf8ToGBK(TextField_name->getString());
		auto Tname_len = strlen(Utf8ToGBK(str_name));
		if (Tname_len == 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("茶楼名字不能为空"));
			return;
		}
		strcpy(Date.szClubName, str_name.c_str());
		string str_score = TextField_score->getString();
		auto score_len = strlen(Utf8ToGBK(str_score));
		bool isNum = Tools::verifyNumber(str_score);
		if (isNum && score_len > 0)
		{
			Date.bigWinnerMaxScore = std::stoi(str_score);
		}
		else
		{
			Date.bigWinnerMaxScore = 0;
		}
		Date.gameNameId = 0;
		Date.msg = ClubRequest_ModifyClubInfo;
		Date.clubId = m_iClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_ModifyClubInfo));

		//发送设置管理员账户
		SClubRequest_SetAccessUsers  Data;
		string str = TextField_input1->getString();
		auto name_len = strlen(Utf8ToGBK(str));
		bool bNum = Tools::verifyNumber(str);
		if (bNum && name_len > 0)
		{
			Data.userIdList[0] = std::stoi(str);
			Data.accessList[0] = ClubAccess_Admin;
		}
		else if (!bNum)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("填写管理员账户必须为数字"));
			return;
		}
		string str1 = TextField_input2->getString();
		auto name_len1 = strlen(Utf8ToGBK(str1));
		bool isNum1 = Tools::verifyNumber(str1);
		if (isNum1 && name_len1 > 0)
		{
			Data.userIdList[1] = std::stoi(str1);
			Data.accessList[1] = ClubAccess_Admin;
		}
		else if (!isNum1)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("填写管理员账户必须为数字"));
			return;
		}
		Data.gameNameId = 0;
		Data.msg = ClubRequest_SetAdminAccess;
		Data.clubId = m_iClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_SetAccessUsers));
		Panel_teahouseset->setVisible(false);
	});

	//设置
	auto Button_set = (Button*)Image_bg->getChildByName("Button_set");
	Button_set->addClickEventListener([=](Ref*){
		auto Rest = HNPlatformClubData::getInstance()->getTeaHouseInfoByState(m_iClubID);
		Button_room_state->loadTextureNormal(Rest ? "platform/Club/res/teahouseset/btn_hfkf.png" : "platform/Club/res/teahouseset/btn_ztfj.png");
		Button_room_state->loadTextureDisabled(Rest ? "platform/Club/res/teahouseset/btn_hfkf.png" : "platform/Club/res/teahouseset/btn_ztfj.png");
		Panel_teahouseset->setVisible(true);
	});

	//刷新.
	auto Button_refresh = (Button*)Image_bg->getChildByName("Button_refresh");
	Button_refresh->addClickEventListener([=](Ref*){
		Button_refresh->setEnabled(false);
		Button_refresh->runAction(Sequence::create(DelayTime::create(5.0f), CallFunc::create([=]() {
			Button_refresh->setEnabled(true);
		}), nullptr));
		//获取获取茶楼名字及大赢家最低分，设置页面管理员账号
		getTeaHouseSetInfo();
		//默认刷新列表数据
		UpdateRoomInfo();
		SendQueryMember();
		SendDeskmateLimit();
	});

	//获取获取茶楼名字及大赢家最低分，设置页面管理员账号
	getTeaHouseSetInfo();

	//默认刷新列表数据
	initDateList();
}

//点击切换按钮切换列表以及更新切换按钮
void ClubManag::BtnCallBack(Ref* pSender)
{
	auto Btn = (Button*)pSender;
	std::string name = Btn->getName();

	for (int i = 0; i < BUTTON_NUM; i++)
	{
		auto Panle_list = (Layout*)Image_bg->getChildByName(TEAHOUSE_PANLELIST[i]);
		if (name.compare(TEAHOUSE_MENUBUTTON[i]) == 0)
		{
			Panle_list->setVisible(true);
			Btn->setEnabled(false);
			auto sp = (Sprite*)Btn->getChildByName("Sprite_tip");
			sp->setTexture(TEAHOUSE_SELECTED[i]);
		}
		else
		{
			Panle_list->setVisible(false);
			auto Btn_Copy = (Button*)Image_bg->getChildByName(TEAHOUSE_MENUBUTTON[i]);
			Btn_Copy->setEnabled(true);
			auto sp = (Sprite*)Btn_Copy->getChildByName("Sprite_tip");
			sp->setTexture(TEAHOUSE_CONVEN[i]);
		}
	}
}

//初始化列表数据
void ClubManag::initDateList()
{
	//房间列表处理
	if (panel_RoomList)
	{
		//房间列表背景
		auto Imag_bg = (ImageView*)panel_RoomList->getChildByName("Image_houseinfo_bg");

		//茶楼楼主
		auto Text_teahouseM = (Text*)Imag_bg->getChildByName("Text_teahouseM");
		Text_teahouseM->setString(GBKToUtf8(m_szClubOwnerName));

		//茶楼ID
		auto Text_teahouseID = (Text*)Imag_bg->getChildByName("Text_teahouseID");
		Text_teahouseID->setString(to_string(m_iClubID));

		//房间数
		auto Text_roomNum = (Text*)Imag_bg->getChildByName("Text_roomNum");
		//Text_roomNum->setString(to_string(m_iClubID));

		//游戏人数
		auto Text_playerNum = (Text*)Imag_bg->getChildByName("Text_playerNum");
		//Text_playerNum->setString(to_string(m_iClubID));

		//清理按钮
		auto Button_clear = (Button*)Imag_bg->getChildByName("Button_clear");
		Button_clear->addClickEventListener([=](Ref*){

		});

		//分享按钮
		auto Button_share = (Button*)Imag_bg->getChildByName("Button_share");
		Button_share->addClickEventListener([=](Ref*){

		});

		//初始化房间信息列表
		UpdateRoomInfo();

	}

	if (panel_Member)
	{
		//组件初始化
		ListView_Member = (ListView*)panel_Member->getChildByName("ListView_Member");

		//点击下拉列表
		auto Button_change = (Button*)panel_Member->getChildByName("Button_change");

		//当前俱乐部ID
		Text_teahouseID = (Text*)panel_Member->getChildByName("Text_teahouseID");
		Text_teahouseID->setString(to_string(m_iClubID));

		//输入搜索
		TextField_search = (TextField*)panel_Member->getChildByName("TextField_search");

		//搜索按钮（确定）
		auto Button_search = (Button*)panel_Member->getChildByName("Button_search");
		Button_search->addClickEventListener([=](Ref*){
			//检测输入搜索框输入是否合法
			SearchMember();
		});

		//手动添加成员
		auto Button_manually_add = (Button*)panel_Member->getChildByName("Button_manually_add");
		Button_manually_add->addClickEventListener([=](Ref*){
			if (Panel_AddMembers)
			{
				TextField_SearchAM->setString("");
				if (ListView_SearchAM)
				{
					Widget* item = ListView_SearchAM->getItem(0);
					if (item != nullptr)
					{
						ListView_SearchAM->removeAllItems();
					}
				}
				Panel_AddMembers->setVisible(true);
			}
		});

		//显示当前茶楼成员，上限人数（群员共100人，上限300人）
		Text_tip = (Text*)panel_Member->getChildByName("Text_tip");

		//当前日期显示（今日，昨日，前日）
		Text_current_tip = (Text*)panel_Member->getChildByName("Text_current_tip");

		//关闭更多选项
		Panel_close = (Layout*)panel_Member->getChildByName("Panel_close");
		Panel_close->setVisible(false);
		Panel_close->addTouchEventListener([=](Ref* ref, Widget::TouchEventType type){
			if (Widget::TouchEventType::ENDED != type) {
				return;
			}
			Panel_close->setVisible(false);
			Image_switch->setVisible(false);
		});

		//更多选项
		Image_switch = (ImageView*)panel_Member->getChildByName("Image_switch");
		auto btn_switch_0 = (Button*)Image_switch->getChildByName("Button_switch_0");
		btn_switch_0->addClickEventListener([=](Ref*){
			setSwitchDayList(0);
		});
		auto btn_switch_1 = (Button*)Image_switch->getChildByName("Button_switch_1");
		btn_switch_1->addClickEventListener([=](Ref*){
			setSwitchDayList(1);
		});
		auto btn_switch_2 = (Button*)Image_switch->getChildByName("Button_switch_2");
		btn_switch_2->addClickEventListener([=](Ref*){
			setSwitchDayList(2);
		});

		//切换日期成员记录
		auto Button_switch = (Button*)panel_Member->getChildByName("Button_switch");
		Button_switch->addClickEventListener([=](Ref*){
			if (Image_switch)
			{
				Panel_close->setVisible(true);
				Image_switch->setVisible(true);
			}
		});

		//更新用户列表数据
		//UpdateMember();
	}

	if (panel_Limit)
	{
		//房间列表背景
		auto Imag_bg = (ImageView*)panel_Limit->getChildByName("Image_bg");

		//同桌限制列表
		ListView_Limit = (ListView*)panel_Limit->getChildByName("ListView_Limit");

		//初始化同桌限制列表
		SendDeskmateLimit();
	}
}

void ClubManag::UpdateRoomInfo()
{
	if (panel_RoomList)
	{
		//房间列表
		auto ListView_roominfo = (ListView*)panel_RoomList->getChildByName("ListView_roominfo");
		int Index = 0;

		//初始化清空列表
		if (ListView_roominfo != nullptr)
		{
			Widget* item = ListView_roominfo->getItem(0);
			if (item != nullptr)
			{
				ListView_roominfo->removeAllItems();
			}
		}

		//获取游戏中和等待中的桌子信息
		std::vector<SClubTeahouseDeskInfo> Vec_Table = HNPlatformClubData::getInstance()->getTeaHouseStardTable(m_iClubID);

		if (ListView_roominfo)
		{
			if (Vec_Table.size() > 0)
			{
				int Y = std::ceil((float)Vec_Table.size() / 6.0);  //每行放6张桌子

				auto Layout_bk = Layout::create();
				Layout_bk->setSize(Size(1204, 200 * Y));
				Layout_bk->setAnchorPoint(Vec2(0, 1));
				Layout_bk->setName("Table_bg");

				for (int i = 0; i < Vec_Table.size(); i++)
				{
					auto node = CSLoader::createNode("platform/Club/res/teahouseset/RoomInfoNode.csb");
					auto panel_clone = (Layout*)node->getChildByName("panel_clone");

					//获取桌子号
					int RoomID = HNPlatformClubData::getInstance()->getTeaHouseInfoRoomID(m_iClubID, Vec_Table[i].teahouseId);
					char szDeskPass[20];
					sprintf(szDeskPass, GBKToUtf8("桌子号:%03d%03d"), RoomID, Vec_Table[i].deskIndex);

					//桌子号
					auto Text_tableID = (Text*)panel_clone->getChildByName("Text_tableID");
					Text_tableID->setString(szDeskPass);

					//桌子状态（游戏中（当前局数/总局数），准备中）
					string str_state = "";
					int total_Num = HNPlatformClubData::getInstance()->getTeaHouseInfoMaxCount(m_iClubID, Vec_Table[i].teahouseId);
					if (Vec_Table[i].deskState == ClubTeahouseDeskState_Playing)
					{
						str_state = StringUtils::format(GBKToUtf8("(游戏%d/%d)"), Vec_Table[i].curGames, total_Num);
					}
					else
					{
						str_state = GBKToUtf8("(准备中)");
					}
					auto Text_tableState = (Text*)panel_clone->getChildByName("Text_tableState");
					Text_tableState->setString(str_state);

					//人数
					string str_player = "";
					int MaxPlayerNum = HNPlatformClubData::getInstance()->getTeaHouseInfoPlayer(m_iClubID, Vec_Table[i].teahouseId);
					int current_PlayerNum = 0;
					for (int j = 0; j < MaxPlayerNum; j++)
					{
						if (Vec_Table[i].userIDList[j] != 0)
						{
							current_PlayerNum++;
						}
					}
					auto Text_tablePlayer = (Text*)panel_clone->getChildByName("Text_tablePlayer");
					Text_tablePlayer->setString(StringUtils::format(GBKToUtf8("人数:%d/%d"), current_PlayerNum, MaxPlayerNum));

					//设置坐标以及压入问题
					panel_clone->setTag(Vec_Table[i].deskIndex);
					panel_clone->setUserData(&Vec_Table[i]);
					panel_clone->removeFromParentAndCleanup(true);
					panel_clone->addTouchEventListener([=](Ref* ref, Widget::TouchEventType type){
						if (Widget::TouchEventType::ENDED != type) {
							return;
						}
						Layout* Btn_Panel_Clone = (Layout*)ref;
						auto info = (SClubTeahouseDeskInfo*)Btn_Panel_Clone->getUserData();
						m_TempInfo = Vec_Table[i];
						m_iCurrentP = current_PlayerNum;
						//发送房间查询
						int GameID = HNPlatformClubData::getInstance()->getTeaHouseInfoGameID(m_iClubID, Vec_Table[i].teahouseId);
						SClubRequest_GetDeskUserInfos  Data;
						Data.gameNameId = GameID;
						Data.msg = ClubRequest_GetDeskUserInfos;
						Data.teahouseId = Vec_Table[i].teahouseId;
						Data.deskIndex = Vec_Table[i].deskIndex;
						Data.clubId = m_iClubID;
						PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_GetDeskUserInfos));
						//LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("获取房间信息中，请稍候..."), 28);
					});

					int x = Index % 6;
					int y = std::floor((float)Index / 6.0);
					panel_clone->setPosition(Vec2((panel_clone->getContentSize().width / 2) + x * 210, (Y * 210 - panel_clone->getContentSize().height / 2) - y * 210));
					Layout_bk->addChild(panel_clone);
					Index++;
				}
				ListView_roominfo->pushBackCustomItem(Layout_bk);
			}
		}
	}
}

void ClubManag::SearchMember()
{
	SearchMemberInfo.clear();
	string str1 = TextField_search->getString();
	auto name_len1 = strlen(Utf8ToGBK(str1));
	bool isNum1 = Tools::verifyNumber(str1);
	if (isNum1 && name_len1 > 0)
	{
		int UserID = stoi(str1);
		for (auto member : MemberInfo)
		{
			if (member.userId == UserID)
			{
				SearchMemberInfo.push_back(member);
			}
		}
		UpdateSearchMember();
	}
	else if (!isNum1)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为整数数字"));
		return;
	}
	else
	{
		UpdateMember();
	}
}

void ClubManag::UpdateSearchMember()
{
	//初始化清空列表
	if (ListView_Member != nullptr)
	{
		Widget* item = ListView_Member->getItem(0);
		if (item != nullptr)
		{
			ListView_Member->removeAllItems();
		}
	}

	if (SearchMemberInfo.size() > 0)
	{
		for (int i = 0; i < SearchMemberInfo.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/teahouseset/MembersNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("panel_clone");

			//用户ID
			auto Text_UserID = (Text*)panel_clone->getChildByName("Text_UserID");
			Text_UserID->setString(to_string(SearchMemberInfo[i].userId));

			//用户名字
			auto Text_UserName = (Text*)panel_clone->getChildByName("Text_UserName");
			Tools::TrimSpace(SearchMemberInfo[i].szUserName);
			std::string nickName(SearchMemberInfo[i].szUserName);
			if (!nickName.empty())
			{
				string str = SearchMemberInfo[i].szUserName;
				if (str.length() > 16)
				{
					str.erase(17, str.length());
					str.append("...");
					Text_UserName->setString(GBKToUtf8(str));
				}
				else
				{
					Text_UserName->setString(GBKToUtf8(str));
				}
			}
			else
			{
				Text_UserName->setString(GBKToUtf8("未知"));
			}

			//备注信息
			auto Text_Remark = (Text*)panel_clone->getChildByName("Text_Remark");
			string str_szNote = SearchMemberInfo[i].szNote;
			if (str_szNote.compare("") == 0)
			{
				Text_Remark->setString(GBKToUtf8("暂无备注"));
			}
			else
			{
				Text_Remark->setString(GBKToUtf8(SearchMemberInfo[i].szNote));
			}



			//赢的次数
			auto Text_WinNum = (Text*)panel_clone->getChildByName("Text_WinNum");
			Text_WinNum->setString(to_string(SearchMemberInfo[i].winCount));


			//大赢家次数
			auto Text_WinPlayerNum = (Text*)panel_clone->getChildByName("Text_WinPlayerNum");
			Text_WinPlayerNum->setString(to_string(SearchMemberInfo[i].bigWinnerCount));

			//红花
			auto Text_RedFlow = (Text*)panel_clone->getChildByName("Text_RedFlow");
			if (SearchMemberInfo[i].access & ClubAccess_Admin)
			{
				Text_RedFlow->setString(StringUtils::format("%.2f", SearchMemberInfo[i].redFlowers));
			}
			else if (SearchMemberInfo[i].access & ClubAccess_Partner)
			{
				Text_RedFlow->setString(StringUtils::format("%.2f", SearchMemberInfo[i].redFlowers));
			}
			else if (m_iCreaterID == SearchMemberInfo[i].userId)
			{
				Text_RedFlow->setString(StringUtils::format("%.2f", SearchMemberInfo[i].redFlowers));
			}
			else
			{
				Text_RedFlow->setString(StringUtils::format("%d", (int)SearchMemberInfo[i].redFlowers));
			}

			//当前用户信息弹窗存在的时候刷新红花数量
			if (Text_Redflower && operationID == SearchMemberInfo[i].userId)
			{
				Text_Redflower->setString(Text_RedFlow->getString());
				operationID = 0;
			}


			//查看详情
			auto Button_show = (Button*)panel_clone->getChildByName("Button_show");
			Button_show->addClickEventListener([=](Ref*){
				if (SearchMemberInfo.size() > 0)
				{
					ShowUserInfoDump(SearchMemberInfo[i]);
				}
			});

			panel_clone->removeFromParentAndCleanup(true);
			ListView_Member->pushBackCustomItem(panel_clone);
		}
	}
}

void ClubManag::UpdateMember()
{
	//初始化清空列表
	if (ListView_Member != nullptr)
	{
		Widget* item = ListView_Member->getItem(0);
		if (item != nullptr)
		{
			ListView_Member->removeAllItems();
		}
	}

	if (MemberInfo.size() > 0)
	{
		for (int i = 0; i < MemberInfo.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/teahouseset/MembersNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("panel_clone");

			//用户ID
			auto Text_UserID = (Text*)panel_clone->getChildByName("Text_UserID");
			Text_UserID->setString(to_string(MemberInfo[i].userId));

			//用户名字
			auto Text_UserName = (Text*)panel_clone->getChildByName("Text_UserName");
			Tools::TrimSpace(MemberInfo[i].szUserName);
			std::string nickName(MemberInfo[i].szUserName);
			if (!nickName.empty())
			{
				string str = MemberInfo[i].szUserName;
				if (str.length() > 16)
				{
					str.erase(17, str.length());
					str.append("...");
					Text_UserName->setString(GBKToUtf8(str));
				}
				else
				{
					Text_UserName->setString(GBKToUtf8(str));
				}
			}
			else
			{
				Text_UserName->setString(GBKToUtf8("未知"));
			}

			//备注信息
			auto Text_Remark = (Text*)panel_clone->getChildByName("Text_Remark");
			string str_szNote = MemberInfo[i].szNote;
			if (str_szNote.compare("") == 0)
			{
				Text_Remark->setString(GBKToUtf8("暂无备注"));
			}
			else
			{
				Text_Remark->setString(GBKToUtf8(MemberInfo[i].szNote));
			}
			


			//赢的次数
			auto Text_WinNum = (Text*)panel_clone->getChildByName("Text_WinNum");
			Text_WinNum->setString(to_string(MemberInfo[i].winCount));


			//大赢家次数
			auto Text_WinPlayerNum = (Text*)panel_clone->getChildByName("Text_WinPlayerNum");
			Text_WinPlayerNum->setString(to_string(MemberInfo[i].bigWinnerCount));

			//红花
			auto Text_RedFlow = (Text*)panel_clone->getChildByName("Text_RedFlow");
			if (MemberInfo[i].access & ClubAccess_Admin)
			{
				Text_RedFlow->setString(StringUtils::format("%.2f", MemberInfo[i].redFlowers));
			}
			else if (MemberInfo[i].access & ClubAccess_Partner)
			{
				Text_RedFlow->setString(StringUtils::format("%.2f", MemberInfo[i].redFlowers));
			}
			else if (m_iCreaterID == MemberInfo[i].userId)
			{
				Text_RedFlow->setString(StringUtils::format("%.2f", MemberInfo[i].redFlowers));
			}
			else
			{
				Text_RedFlow->setString(StringUtils::format("%d", (int)MemberInfo[i].redFlowers));
			}

			//当前用户信息弹窗存在的时候刷新红花数量
			if (Text_Redflower && operationID == MemberInfo[i].userId)
			{
				Text_Redflower->setString(Text_RedFlow->getString());
				operationID = 0;
			}


			//查看详情
			auto Button_show = (Button*)panel_clone->getChildByName("Button_show");
			Button_show->addClickEventListener([=](Ref*){
				if (MemberInfo.size() > 0)
				{
					ShowUserInfoDump(MemberInfo[i]);
				}
			});

			panel_clone->removeFromParentAndCleanup(true);
			ListView_Member->pushBackCustomItem(panel_clone);
		}
		Text_tip->setString(StringUtils::format(GBKToUtf8("群员共%d人，上限300人"), MemberInfo.size()));
	}
}

void ClubManag::setRoomUserInfo(SClubTeahouseDeskInfo info, SClubOperationResponse_GetDeskUserInfos* Data)
{
	//点开房间详情
	if (Panel_RoomInfo)
	{
		Panel_RoomInfo->setVisible(true);
		//房间ID
		auto Text_RoomID = (Text*)Panel_RoomInfo->getChildByName("Text_RoomID");
		int RoomID = HNPlatformClubData::getInstance()->getTeaHouseInfoRoomID(m_iClubID, info.teahouseId);
		Text_RoomID->setString(StringUtils::format("%03d%03d", RoomID, info.deskIndex));

		//解散房间
		auto Btn_Diss = (Button*)Panel_RoomInfo->getChildByName("Button_dissolve");
		Btn_Diss->addClickEventListener([=](Ref*){
			int GameID = HNPlatformClubData::getInstance()->getTeaHouseInfoGameID(m_iClubID, info.teahouseId);
			SClubRequest_DismissTeahouseDesk  Data;
			Data.gameNameId = GameID;
			Data.msg = ClubRequest_DismissTeahouseDesk;
			Data.teahouseId = info.teahouseId;
			Data.deskIndex = info.deskIndex;
			Data.clubId = m_iClubID;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_DismissTeahouseDesk));
			Panel_RoomInfo->setVisible(false);
		});

		//用户信息
		for (int j = 0; j < 6; j++)
		{
			string str_name = StringUtils::format("panel_Player_%d", j);
			auto Node_Player = (Layout*)Panel_RoomInfo->getChildByName(str_name);
			Node_Player->setVisible(m_iCurrentP > j ? true : false);

			if (j + 1 > m_iCurrentP)
			{
				continue;
			}

			//玩家名字
			auto Text_Name = (Text*)Node_Player->getChildByName("Text_Name");
			string str_name1 = GBKToUtf8(Data->szUserNames[j]);
			Text_Name->setString(str_name1);//GBKToUtf8(Data->szUserNames[j]));

			//玩家分数
			auto Text_Score = (Text*)Node_Player->getChildByName("Text_Score");
			Text_Score->setString(info.deskState == ClubTeahouseDeskState_Playing ? to_string(Data->userScoreList[j]) : GBKToUtf8("暂无"));

			//踢出玩家
			auto Button_Kicked = (Button*)Node_Player->getChildByName("Button_Kicked");
			Button_Kicked->addClickEventListener([=](Ref*){
				int GameID = HNPlatformClubData::getInstance()->getTeaHouseInfoGameID(m_iClubID, info.teahouseId);
				//发送踢出玩家的消息
				auto SendKicked = [=](){
					SClubRequest_KickDeskUser Request;
					Request.clubId = m_iClubID;
					Request.gameNameId = GameID;
					Request.deskIndex = info.deskIndex;
					Request.msg = ClubRequest_KickDeskUser;
					Request.teahouseId = info.teahouseId;
					Request.userId = Button_Kicked->getTag();
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Request, sizeof(SClubRequest_KickDeskUser));
				};
				auto prompt = GamePromptLayer::create(true);
				prompt->showPrompt(GBKToUtf8("您确定要踢出该玩家？"));
				prompt->setCallBack([=](){
					SendKicked();
					Panel_RoomInfo->setVisible(false);
				});
			});
			Button_Kicked->setTag(Data->userIdList[j]);
			//设置是否可见踢出玩家按钮（游戏中禁止使用）
			Button_Kicked->setVisible(info.deskState == ClubTeahouseDeskState_Playing ? false : true);
		}
	}
}

//监听所有动作执行消息返回
bool ClubManag::getMessageBack(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		SClubResponseBase* Data = (SClubResponseBase*)socketMessage->object;
		log("sure======Message===%d",Data->msg);
		if (ClubRequest_PauseAllTeahouse == Data->msg)
		{
			//暂停或恢复开房
		}
		else if (ClubRequest_DismissTeahouseDesk == Data->msg)
		{
			// 解散茶楼桌子
		}
		else if (ClubRequest_KickDeskUser == Data->msg)
		{
			// 踢出桌子上的用户
		}
		else if (ClubRequest_SetAdminAccess == Data->msg)
		{
			// 设置管理员权限账号
			SClubResponseBase* Result = (SClubResponseBase*)socketMessage->object;
			getTeaHouseSetInfo();
		}
		else if (ClubRequest_GetAdminUsers == Data->msg)
		{
			SClubRequest_GetAccessUsers_Ret* Result = (SClubRequest_GetAccessUsers_Ret*)socketMessage->object;
			if (Result->msg == ClubRequest_GetAdminUsers)
			{
				int Index = 0;
				for (int i = 0; i < Club_MaxAccessUsers; i++)
				{
					if (Result->userIdList[i] != 0)
					{
						Index++;
					}
					else
					{
						if (i == 0)
						{
							if (TextField_input1)
							{
								TextField_input1->setString("");
							}
						}
						else if (i == 1)
						{
							if (TextField_input2)
							{
								TextField_input2->setString("");
							}
						}
					}
				}
				if (Index > 0)
				{
					//设置管理员账户
					setAdminResult(Result, Index);
				}

				//默认初始化设置的俱乐部名字
				if (TextField_name)
				{
					TextField_name->setString(m_szClubName);
				}

				//默认初始化设置大赢家底分
				if (TextField_score)
				{
					TextField_score->setString(to_string(iClubBasisScore));
				}
			}
		}
		else if (ClubRequest_GetDeskUserInfos == Data->msg)
		{
			SClubOperationResponse_GetDeskUserInfos* Result = (SClubOperationResponse_GetDeskUserInfos*)socketMessage->object;
			setRoomUserInfo(m_TempInfo, Result);
		}
		else if (ClubRequest_QueryUserInfo == Data->msg)
		{
			//用户红花、权限查询（进入茶楼已查询）

		}
		else if (ClubRequest_QueryClubInfo == Data->msg)
		{
			// 查询俱乐部信息
			SClubOperationResponse_QueryClubInfo* Result = (SClubOperationResponse_QueryClubInfo*)socketMessage->object;
			m_szClubName = GBKToUtf8(Result->szClubName);
			iClubBasisScore = Result->bigWinnerMaxScore;
		}
		else if (ClubRequest_ModifyClubInfo == Data->msg)
		{
			SClubResponseBase* Result = (SClubResponseBase*)socketMessage->object;
		}
		else if (ClubRequest_GiveRedFlowers == Data->msg)
		{
			SClubOperationResponse_GiveRedFlowers* Result = (SClubOperationResponse_GiveRedFlowers*)socketMessage->object;
			operationID = Result->targetUserId;
			SendQueryMember();
			GamePromptLayer::create()->showPrompt(GBKToUtf8("赠送成功！"));
		}
		else if (ClubRequest_TakeOutRedFlowers == Data->msg)
		{
			SClubOperationResponse_TakeOutRedFlowers* Result = (SClubOperationResponse_TakeOutRedFlowers*)socketMessage->object;
			operationID = Result->targetUserId;
			SendQueryMember();
			GamePromptLayer::create()->showPrompt(GBKToUtf8("摘花成功！"));
		}
		else if (ClubRequest_QueryRedFlowersLog == Data->msg)
		{

		}
		else if (ClubRequest_QueryPendingLog == Data->msg)
		{

		}
		else if (ClubRequest_ProcessPendingLog == Data->msg)
		{

		}
		else if (ClubRequest_QueryMemberList == Data->msg)
		{
			getClubMemberInfoResult(socketMessage);
		}
		else if (ClubRequest_QueryUserGameLog == Data->msg)
		{
			SClubOperationResponse_QueryUserGameLog* Result = (SClubOperationResponse_QueryUserGameLog*)socketMessage->object;
			if (Result->count > 0)
			{
				if (ListView_Record)
				{
					Widget* item = ListView_Record->getItem(0);
					if (item != nullptr)
					{
						ListView_Record->removeAllItems();
					}
					setRecordInfo(Result);
				}
			}
			else
			{
				//暂无战绩处理
				if (ListView_Record)
				{
					Widget* item = ListView_Record->getItem(0);
					if (item != nullptr)
					{
						ListView_Record->removeAllItems();
					}

					auto Layout_bk = Layout::create();
					Layout_bk->setSize(Size(813,328));
					Layout_bk->setBackGroundImage("platform/Club/res/userInfo/word_zanwushuju.png");
					ListView_Record->pushBackCustomItem(Layout_bk);

					Text_record->setString("0");
					Text_roomNum->setString("0");
					Text_winplayerNum->setString("0");
				}
			}
		}
		else if (ClubRequest_SetBlackListUser == Data->msg)
		{
			SClubOperationResponse_SetBlackListUser* Result = (SClubOperationResponse_SetBlackListUser*)socketMessage->object;
			if (Result->bAddToBlackList)
			{
				Button_UserList_Sate->loadTextures("platform/Club/res/userInfo/btn_bmd.png", "platform/Club/res/userInfo/btn_bmd.png", "platform/Club/res/userInfo/btn_bmd.png");
			}
			else
			{
				Button_UserList_Sate->loadTextures("platform/Club/res/userInfo/btn_hmd.png", "platform/Club/res/userInfo/btn_hmd.png", "platform/Club/res/userInfo/btn_hmd.png");
			}
			if (userinfoLayer)
			{
				userinfoLayer->setVisible(false);
			}
			GamePromptLayer::create()->showPrompt(GBKToUtf8("设置成功！"));
			SendQueryMember();
		}
		else if (ClubRequest_SetDeskmateLimit == Data->msg)
		{
			SClubOperationResponse_SetDeskmateLimit* Result = (SClubOperationResponse_SetDeskmateLimit*)socketMessage->object;
			if (Result->bOK)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("设置成功！"));
				SendDeskmateLimit();
			}
		}
		else if (ClubRequest_QueryDeskmateLimit == Data->msg)
		{
			SClubOperationResponse_QueryDeskmateLimit* Result = (SClubOperationResponse_QueryDeskmateLimit*)socketMessage->object;
			Vec_LimitList.clear();
			if (Result->count > 0)
			{
				if (ListView_Limit)
				{
					Widget* item = ListView_Limit->getItem(0);
					if (item != nullptr)
					{
						ListView_Limit->removeAllItems();
					}
					for (int i = 0; i < Result->count; i++)
					{
						SClubDeskmateLimit Data = Result->deskmateList[i];
						//for (int j = 0; j < 2; j++)
						//memcpy(Data.szUserNames, Result->deskmateList[i].szUserNames, sizeof(Result->deskmateList[i].szUserNames));
						////memcpy(Data.userIds, Result->deskmateList[i].userIds, sizeof(Result->deskmateList[i].userIds));
						//Data.userIds = Result->deskmateList[i].userIds
						Vec_LimitList.push_back(Data);
					}
					setLimitInfo(Result);
				}
			}
			else
			{
				//暂无战绩处理
				if (ListView_Limit)
				{
					Widget* item = ListView_Limit->getItem(0);
					if (item != nullptr)
					{
						ListView_Limit->removeAllItems();
					}

					auto Layout_bk = Layout::create();
					Layout_bk->setSize(Size(1204, 465));
					Layout_bk->setBackGroundImage("platform/Club/res/userInfo/word_zanwushuju.png");
					ListView_Limit->pushBackCustomItem(Layout_bk);
				}
			}
		}
		else if (ClubRequest_ModifyMemberNote == Data->msg)
		{
			SendQueryMember();
		}
		else if (ClubRequest_SearchMember == Data->msg)
		{
			SClubOperationResponse_SearchMember* Result = (SClubOperationResponse_SearchMember*)socketMessage->object;
			if (Result->count > 0)
			{
				if (Vec_SearchMember.size() > 0)Vec_SearchMember.clear();
				for (int i = 0; i < Result->count; i++)
				{
					Vec_SearchMember.push_back(Result->members[i]);
				}
				//更新搜索列表
				UpdateSearchUserList();
			}
		}
		else if (ClubRequest_AddMember == Data->msg)
		{
			if (Panel_AddMembers)
			{
				Panel_AddMembers->setVisible(false);
			}
			SendQueryMember();
		}
	}
	return true;
}

bool ClubManag::getDeleteMemberResult(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{
		SendQueryMember();
		if (userinfoLayer)
		{
			userinfoLayer->setVisible(false);
			GamePromptLayer::create()->showPrompt(GBKToUtf8("当前玩家已踢出"));
		}
	}
	return true;
}

bool ClubManag::getClubMemberInfoResult(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{	
		SClubOperationResponse_QueryMemberList* Data = (SClubOperationResponse_QueryMemberList*)SocketMessage->object;
		m_iCreaterID = Data->iCreaterID;

		if (Data->count > 0)
		{
			for (int i = 0; i<Data->count; i++)
			{
				MemberInfo.push_back(Data->memberInfos[i]);
			}
		}
		UpdateMember();
	}
	return true;
}

void ClubManag::setAdminResult(SClubRequest_GetAccessUsers_Ret* result, int Index)
{
	if (Index == 1)
	{
		if (TextField_input1)
		{
			TextField_input1->setString(to_string(result->userIdList[0]));
		}
	}
	else if (Index == 2)
	{
		if (TextField_input1)
		{
			TextField_input1->setString(to_string(result->userIdList[0]));
		}
		if (TextField_input2)
		{
			TextField_input2->setString(to_string(result->userIdList[1]));
		}
	}
	else
	{

	}
}

void ClubManag::ShowUserInfoDump(SClubMemberInfo Data)
{
	if (userinfoLayer)
	{
		userinfoLayer->setVisible(true);
	}
	else
	{
		userinfoLayer = CSLoader::createNode("platform/Club/res/userInfo/userinfoLayer.csb");
		addChild(userinfoLayer);
	}

	auto panel_make = (Layout*)userinfoLayer->getChildByName("panel_make");
	//头像
	auto Image_head = (ImageView*)panel_make->getChildByName("Image_head");
	auto _userHead = GameUserHead::create(Image_head);
	_userHead->show("platform/head/men_head.png");
	//_userHead->setHeadByFaceID(Data.iLogoID);
	_userHead->setName("Temp_Head");
	_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Data.userId));

	//用户名字
	auto Text_Name = (Text*)panel_make->getChildByName("Text_Name");
	Tools::TrimSpace(Data.szUserName);
	string str = Data.szUserName;
	if (str.length() > 16)
	{
		str.erase(17, str.length());
		str.append("...");
		Text_Name->setString(GBKToUtf8(str));
	}
	else
	{
		Text_Name->setString(GBKToUtf8(str));
	}

	//用户ID
	auto Text_UserID = (Text*)panel_make->getChildByName("Text_UserID");
	Text_UserID->setString(to_string(Data.userId));
	operationID = Data.userId;

	//备注
	auto Text_Remark = (Text*)panel_make->getChildByName("Text_Remark");

	//点击备注
	TextField_Remark = (TextField*)panel_make->getChildByName("TextField_Remark");
	string str_Remark = Data.szNote;
	if (!str_Remark.empty())
	{
		TextField_Remark->setString(GBKToUtf8(Data.szNote));
	}
	else
	{
		TextField_Remark->setString("");
	}
	//SClubRequest_ModifyMemberNote UserData;
	////memset(UserData, 0, sizeof(UserData));
	//UserData.clubId = m_iClubID;
	//UserData.targetUserId = Data.userId;
	//TextField_Remark->setUserData(*UserData);
	TextField_Remark->setTextHorizontalAlignment(TextHAlignment::LEFT);
	TextField_Remark->setTextVerticalAlignment(TextVAlignment::CENTER);
	TextField_Remark->addEventListener(CC_CALLBACK_2(ClubManag::onTextFiledCallBack, this));
	
	//上级
	auto Text_superior_head = (Text*)panel_make->getChildByName("Text_superior_head");
	string str_BossName = Data.szBossName;
	Text_superior_head->setVisible(!str_BossName.empty());

	auto Text_superior = (Text*)panel_make->getChildByName("Text_superior");
	Text_superior->setString(GBKToUtf8(Data.szBossName));
	Text_superior->setVisible(!str_BossName.empty());

	//关闭
	auto Button_close = (Button*)panel_make->getChildByName("Button_close");
	Button_close->addClickEventListener([=](Ref*){
		userinfoLayer->setVisible(false);
	});

	//赠送
	auto Button_give = (Button*)panel_make->getChildByName("Button_give");
	Button_give->addClickEventListener([=](Ref*){
		if (panel_Dump)
		{
			RedFlowerType = 1;
			TextField_RedFlower->setString("");
			Sprite_Type->setTexture("platform/Club/res/userInfo/word_zengsong.png");
			panel_Dump->setVisible(true);
		}
	});

	//摘花
	auto Button_pick = (Button*)panel_make->getChildByName("Button_pick");
	Button_pick->addClickEventListener([=](Ref*){
		if (panel_Dump)
		{
			RedFlowerType = 2;
			TextField_RedFlower->setString("");
			Sprite_Type->setTexture("platform/Club/res/userInfo/word_zhaiqu.png");
			panel_Dump->setVisible(true);
		}
	});

	//红花变化
	auto Button_change = (Button*)panel_make->getChildByName("Button_change");
	Button_change->addClickEventListener([=](Ref*){
		bool isManag = false;
		if (Data.access & ClubAccess_Admin)
		{
			isManag = true;
		}
		else if (Data.access & ClubAccess_Partner)
		{
			isManag = true;
		}
		else if (m_iCreaterID == Data.userId)
		{
			isManag = true;
		}
		ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
		ClubRedFlow->SetClubID(m_iClubID);
		ClubRedFlow->initCSBByType(1, Data.userId, isManag);
		ClubRedFlow->setName("ClubRedFlow");
		this->addChild(ClubRedFlow);
	});

	//待处理
	auto Button_pending = (Button*)panel_make->getChildByName("Button_pending");
	Button_pending->addClickEventListener([=](Ref*){
		bool isManag = false;
		if (Data.access & ClubAccess_Admin)
		{
			isManag = true;
		}
		else if (Data.access & ClubAccess_Partner)
		{
			isManag = true;
		}
		else if (m_iCreaterID == Data.userId)
		{
			isManag = true;
		}
		ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
		ClubRedFlow->SetClubID(m_iClubID);
		ClubRedFlow->initCSBByType(2, Data.userId, true);
		ClubRedFlow->setName("ClubRedFlow");
		this->addChild(ClubRedFlow);
	});

	//红花
	auto Sprite_Redflower = (Sprite*)panel_make->getChildByName("Sprite_Redflower");
	Text_Redflower = (Text*)Sprite_Redflower->getChildByName("Text_Redflower");
	if (Data.access & ClubAccess_Admin)
	{
		Text_Redflower->setString(StringUtils::format("%.2f", Data.redFlowers));
	}
	else if (Data.access & ClubAccess_Partner)
	{
		Text_Redflower->setString(StringUtils::format("%.2f", Data.redFlowers));
	}
	else if (m_iCreaterID == Data.userId)
	{
		Text_Redflower->setString(StringUtils::format("%.2f", Data.redFlowers));
	}
	else
	{
		Text_Redflower->setString(StringUtils::format("%d", (int)Data.redFlowers));
	}

	//点击选择日期查看
	auto Button_More = (Button*)panel_make->getChildByName("Button_More");
	Button_More->addClickEventListener([=](Ref*){
		if (Panel_Close_U)
		{
			Panel_Close_U->setVisible(true);
		}
		if (sp_drop_down)
		{
			sp_drop_down->setVisible(true);
		}
	});
	
	//选择的日期
	text_Time = (Text*)Button_More->getChildByName("Text");

	//更多日期选择
	sp_drop_down = (Sprite*)panel_make->getChildByName("Sprite_drop_down");

	//关闭弹出
	Panel_Close_U = (Layout*)panel_make->getChildByName("Panel_close");
	Panel_Close_U->setVisible(false);
	Panel_Close_U->addTouchEventListener([=](Ref* ref, Widget::TouchEventType type){
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		Panel_Close_U->setVisible(false);
		sp_drop_down->setVisible(false);
	});

	//今日输赢
	auto Button_nowday = (Button*)sp_drop_down->getChildByName("Button_nowday");
	Button_nowday->addClickEventListener([=](Ref*){
		setSwitchDayUserInfo(0, Data.userId);
	});

	//昨日输赢
	auto Button_yesterday = (Button*)sp_drop_down->getChildByName("Button_yesterday");
	Button_yesterday->addClickEventListener([=](Ref*){
		setSwitchDayUserInfo(1, Data.userId);
	});

	//前日输赢
	auto Button_byesterday = (Button*)sp_drop_down->getChildByName("Button_byesterday");
	Button_byesterday->addClickEventListener([=](Ref*){
		setSwitchDayUserInfo(2, Data.userId);
	});

	//黑白名单按钮
	Button_UserList_Sate = (Button*)panel_make->getChildByName("Button_UserList_Sate");
	if (Data.access & ClubAccess_BlackList)
	{
		Button_UserList_Sate->loadTextures("platform/Club/res/userInfo/btn_bmd.png","platform/Club/res/userInfo/btn_bmd.png","platform/Club/res/userInfo/btn_bmd.png");
	}
	else
	{
		Button_UserList_Sate->loadTextures("platform/Club/res/userInfo/btn_hmd.png", "platform/Club/res/userInfo/btn_hmd.png", "platform/Club/res/userInfo/btn_hmd.png");
	}
	Button_UserList_Sate->addClickEventListener([=](Ref*){
		//判断用户权限是黑名单还是白名单
		if (Data.access & ClubAccess_BlackList)
		{
			SClubRequest_SetBlackListUser Date;
			Date.clubId = m_iClubID;
			Date.gameNameId = 0;
			Date.msg = ClubRequest_SetBlackListUser;
			Date.bAddToBlackList = false;
			Date.userId = Data.userId;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetBlackListUser));
		}
		else
		{
			if (Data.access & ClubAccess_Admin || Data.access & ClubAccess_Partner || m_iCreaterID == Data.userId)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("操作无效！"));
				return;
			}
			SClubRequest_SetBlackListUser Date;
			Date.clubId = m_iClubID;
			Date.gameNameId = 0;
			Date.msg = ClubRequest_SetBlackListUser;
			Date.bAddToBlackList = true;
			Date.userId = Data.userId;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetBlackListUser));
		}
	});

	//同桌限制
	auto Button_Limit = (Button*)panel_make->getChildByName("Button_Limit");
	Button_Limit->addClickEventListener([=](Ref*){
		//setOpenLimit_Input(Data.userId);
		if (Panel_Limit_Input)
		{
			TextField_Limit->setString("");
			Panel_Limit_Input->setVisible(true);
		}
	});

	//输入同桌限制
	Panel_Limit_Input = (Layout*)panel_make->getChildByName("Panel_BindLimit");
	Panel_Limit_Input->addTouchEventListener([=](Ref* ref, Widget::TouchEventType type){
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		Panel_Limit_Input->setVisible(false);
	});

	//同桌限制输入框
	TextField_Limit = (TextField*)Panel_Limit_Input->getChildByName("TextField_Limit");
	TextField_Limit->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_Limit->setTextVerticalAlignment(TextVAlignment::CENTER);

	//关闭
	auto Button_close_U = (Button*)Panel_Limit_Input->getChildByName("Button_close");
	Button_close_U->addClickEventListener([=](Ref*){
		Panel_Limit_Input->setVisible(false);
	});

	//确定
	auto Button_sure = (Button*)Panel_Limit_Input->getChildByName("Button_sure");
	Button_sure->addClickEventListener([=](Ref*){
		SClubRequest_SetDeskmateLimit Date;
		string str1 = TextField_Limit->getString();
		auto name_len1 = strlen(Utf8ToGBK(str1));
		bool isNum1 = Tools::verifyNumber(str1);
		if (isNum1 && name_len1 > 0)
		{
			Date.userId2 = stoi(str1);
		}
		else if (!isNum1)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为整数数字"));
			return;
		}
		Date.bSet = true;
		Date.gameNameId = 0;
		Date.clubId = m_iClubID;
		Date.msg = ClubRequest_SetDeskmateLimit;
		Date.userId = Data.userId;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SetDeskmateLimit));
		if (Panel_Limit_Input)
		{
			Panel_Limit_Input->setVisible(false);
		}
	});

	//踢出
	auto Button_Kickedout = (Button*)panel_make->getChildByName("Button_Kickedout");
	auto SendDelete = [=](){
		MSG_GP_I_Club_KickUser Request;
		Request.iClubID = m_iClubID;
		Request.iTargetID = Data.userId;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_KICKUSER, &Request, sizeof(MSG_GP_I_Club_KickUser), HN_SOCKET_CALLBACK(ClubManag::getDeleteMemberResult, this)); };

	Button_Kickedout->addClickEventListener([=](Ref*){
		if (Data.access & ClubAccess_Admin)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("不能踢出管理员"));
			return;
		}
		else if (Data.access & ClubAccess_Partner)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("不能踢出合作玩家"));
			return;
		}
		else if (m_iCreaterID == Data.userId)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("不能踢出群主"));
			return;
		}
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(GBKToUtf8("您确定要删除该玩家？"));
		prompt->setCallBack([=](){
			SendDelete();
		});
	});

	//清零
	auto Button_clear = (Button*)panel_make->getChildByName("Button_clear");

	//清除记录
	auto Button_empty = (Button*)panel_make->getChildByName("Button_empty");

	//战绩
	Text_record = (Text*)panel_make->getChildByName("Text_record");

	//房间数
	Text_roomNum = (Text*)panel_make->getChildByName("Text_roomNum");

	//大赢家次数
	Text_winplayerNum = (Text*)panel_make->getChildByName("Text_winplayerNum");

	//战绩列表
	ListView_Record = (ListView*)panel_make->getChildByName("ListView_Record");

	//赠送摘花弹窗
	panel_Dump = (Layout*)panel_make->getChildByName("panel_Dump");

	//红花数量
	auto Text_Redflower1 = (Text*)panel_Dump->getChildByName("Text_Redflower");
	Text_Redflower1->setString(Text_Redflower->getString());

	//auto shuxue = (Text*)panel_Dump->getChildByName("Text_Redflower");

	//赠送或者摘花操作
	Sprite_Type = (Sprite*)panel_Dump->getChildByName("Sprite_Type");

	//头像
	auto Image_head1 = (ImageView*)panel_Dump->getChildByName("Image_head");
	auto _userHead1 = GameUserHead::create(Image_head1);
	_userHead1->show("platform/head/men_head.png");
	//_userHead1->setHeadByFaceID(Data.iLogoID);
	_userHead1->setName("Temp_Head");
	_userHead1->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Data.userId));

	//名字
	auto Text_Name1 = (Text*)panel_Dump->getChildByName("Text_Name");
	Text_Name1->setString(Text_Name->getString());

	//ID
	auto Text_UserID1 = (Text*)panel_Dump->getChildByName("Text_UserID");
	Text_UserID1->setString(Text_UserID->getString());

	//赠送（摘花）数量
	TextField_RedFlower = (TextField*)panel_Dump->getChildByName("TextField_RedFlower");
	TextField_RedFlower->setString("");
	TextField_RedFlower->setTextHorizontalAlignment(TextHAlignment::CENTER);
	TextField_RedFlower->setTextVerticalAlignment(TextVAlignment::CENTER);

	//确定
	auto Button_Sure = (Button*)panel_Dump->getChildByName("Button_Sure");
	Button_Sure->addClickEventListener([=](Ref*){
		//确定发送赠花摘花
		SClubRequest_GiveRedFlowers Date;
		int NowNum = 0;
		if (RedFlowerType == 1)
		{
			Date.msg = ClubRequest_GiveRedFlowers;
		}
		else
		{
			Date.msg = ClubRequest_TakeOutRedFlowers;
		}
		Date.targetUserId = Data.userId;
		string str1 = TextField_RedFlower->getString();
		auto name_len1 = strlen(Utf8ToGBK(str1));
		bool isNum1 = Tools::verifyNumber(str1);
		if (isNum1 && name_len1 > 0)
		{
			Date.redFlowers = stoi(str1);
		}
		else if (!isNum1)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为整数数字"));
			return;
		}
		//是否可以摘花判断
		if (RedFlowerType == 2)
		{
			string str_red = Text_Redflower->getString();
			auto name_len = strlen(Utf8ToGBK(str_red));
			bool isNum = Tools::verifyNumber(str_red);
			if (isNum && name_len > 0)
			{
				NowNum = stoi(str_red);
				if (Date.redFlowers > NowNum)
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("当前玩家红花不足，无法执行摘花操作！"));
					return;
				}
			}
		}
		Date.gameNameId = 0;
		Date.clubId = m_iClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_GiveRedFlowers));
		if (panel_Dump)
		{
			panel_Dump->setVisible(false);
		}
	});

	//取消
	auto Button_close_D = (Button*)panel_Dump->getChildByName("Button_close");
	Button_close_D->addClickEventListener([=](Ref*){
		panel_Dump->setVisible(false);
	});

	//默认查询今日战绩
	Query_Day = 0;
	SClubRequest_QueryUserGameLog Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_QueryUserGameLog;
	Date.findUserId = Data.userId;
	Date.query_day = Query_Day;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_QueryUserGameLog));
}

void ClubManag::setSwitchDayList(int Index)
{
	Panel_close->setVisible(false);
	Image_switch->setVisible(false);
	Text_current_tip->setString(GBKToUtf8(TEA_HOUSE_DAYLIST[Index]));
	if (Query_Day == Index)
	{
		return;
	}
	Query_Day = Index;
	SendQueryMember();
}

void ClubManag::setSwitchDayUserInfo(int Index, int userid)
{
	Panel_Close_U->setVisible(false);
	sp_drop_down->setVisible(false);
	text_Time->setString(GBKToUtf8(TEA_HOUSE_DAYLIST[Index]));
	if (Query_Day == Index)
	{
		return;
	}
	Query_Day = Index;
	SClubRequest_QueryUserGameLog Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_QueryUserGameLog;
	Date.findUserId = userid;
	Date.query_day = Query_Day;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_GiveRedFlowers));
}

void ClubManag::setRecordInfo(SClubOperationResponse_QueryUserGameLog* Date)
{
	int Score = 0;
	int roomNum = 0;
	int winplayerNum = 0;
	//更新用户弹窗战绩
	for (int i = 0; i < Date->count; i++)
	{
		auto node = CSLoader::createNode("platform/Club/res/userInfo/UserRecordNode.csb");
		auto panel_clone = (Layout*)node->getChildByName("Panel_clone");

		//时间
		time_t Time = Date->logList[i].date;
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		if (p->tm_hour >= 24)
		{
			p->tm_hour -= 24;
		}
		char BeginTime[64];
		strftime(BeginTime, sizeof(BeginTime), "%Y-%m-%d %H:%M:%S", p);
		auto Text_time = (Text*)panel_clone->getChildByName("Text_time_y");
		Text_time->setString(BeginTime);

		//房间号
		auto Text_roomID = (Text*)panel_clone->getChildByName("Text_roomID");
		//char szDeskPass[20];
		//sprintf(szDeskPass, "%03d%03d", Date->logList[i].roomId, Date->logList[i].deskIndex);
		Text_roomID->setString(Date->logList[i].szDeskPass);

		//分数
		auto Text_score = (Text*)panel_clone->getChildByName("Text_score");
		Text_score->setString(to_string(Date->logList[i].winScore));

		//数目统计
		Score += Date->logList[i].winScore;
		roomNum++;
		if (Date->logList[i].isBigWinner)
		{
			winplayerNum++;
		}

		panel_clone->removeFromParentAndCleanup(true);
		ListView_Record->pushBackCustomItem(panel_clone);
	}
	Text_record->setString(to_string(Score));
	Text_roomNum->setString(to_string(roomNum));
	Text_winplayerNum->setString(to_string(winplayerNum));
}

void ClubManag::setLimitInfo(SClubOperationResponse_QueryDeskmateLimit* Date)
{
	//更新同桌限制
	for (int i = 0; i < Vec_LimitList.size(); i++)
	{
		auto node = CSLoader::createNode("platform/Club/res/Limit/LimitNode.csb");
		auto panel_clone = (Layout*)node->getChildByName("Panel_clone");

		//用户ID,名字
		for (int j = 1; j < 3; j++)
		{
			//ID 
			string str_id = StringUtils::format("Text_userID_%d",j);
			auto Text_userID = (Text*)panel_clone->getChildByName(str_id);
			Text_userID->setString(to_string(Vec_LimitList[i].userIds[j - 1]));

			//名字
			string str_name = StringUtils::format("Text_userName_%d", j);
			auto Text_userName = (Text*)panel_clone->getChildByName(str_name);
			Text_userName->setString(Vec_LimitList[i].szUserNames[j - 1]);
		}

		//移除
		auto Button_Remove = (Button*)panel_clone->getChildByName("Button_Remove");
		Button_Remove->addClickEventListener([=](Ref*){
			SClubRequest_SetDeskmateLimit SendData;
			SendData.clubId = m_iClubID;
			SendData.gameNameId = 0;
			SendData.msg = ClubRequest_SetDeskmateLimit;
			SendData.userId = Vec_LimitList[i].userIds[0];
			SendData.userId2 = Vec_LimitList[i].userIds[1];
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &SendData, sizeof(SClubRequest_SetDeskmateLimit));
		});

		panel_clone->removeFromParentAndCleanup(true);
		ListView_Limit->pushBackCustomItem(panel_clone);
	}
}

void ClubManag::setOpenLimit_Input(int userID)
{
	if (Panel_Limit_Input)
	{
		TextField_Limit->setString("");
		Panel_Limit_Input->setVisible(true);

	}
}

void ClubManag::onTextFiledCallBack(Ref* pSender, TextField::EventType type)
{
	//auto TextField_Remark = (TextField*)pSender;
	//SClubRequest_ModifyMemberNote* Data = (SClubRequest_ModifyMemberNote*)TextField_Remark->getUserData();
	//ATTACH_WITH_IME,
	//	DETACH_WITH_IME,
	//	INSERT_TEXT,
	//	DELETE_BACKWARD,
	switch (type)
	{
	//触摸开始
	case(TextField::EventType::ATTACH_WITH_IME) :
	{
		
	}break;
	//触摸结束
	case(TextField::EventType::DETACH_WITH_IME) :
	{
		onTextField_RemarkCallBack();
	}break;
	//输入文本
	case(TextField::EventType::INSERT_TEXT) :
	{
		
	}break;
	//删除文本
	case(TextField::EventType::DELETE_BACKWARD) :
	{

	}
	}
}

void ClubManag::onTextField_RemarkCallBack()
{
	auto name_len = strlen(Utf8ToGBK(TextField_Remark->getString()));
	if (name_len == 0)
	{
		SClubRequest_ModifyMemberNote Date;
		Date.clubId = m_iClubID;
		Date.msg = ClubRequest_ModifyMemberNote;
		Date.targetUserId = operationID;
		strcpy(Date.szNote, "");
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_ModifyMemberNote));
	}
	else
	{
		SClubRequest_ModifyMemberNote Date;
		Date.clubId = m_iClubID;
		Date.msg = ClubRequest_ModifyMemberNote;
		Date.targetUserId = operationID;
		string Remark = Utf8ToGBK(TextField_Remark->getString());
		strcpy(Date.szNote, Remark.c_str());
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_ModifyMemberNote));
	}
}

void ClubManag::SearchInput()
{
	//搜索成员添加合伙人下级
	string str1 = TextField_SearchAM->getString();
	auto name_len1 = strlen(Utf8ToGBK(str1));
	bool isNum1 = Tools::verifyNumber(str1);
	SClubRequest_SearchMember Date;
	Date.clubId = m_iClubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_SearchMember;
	if (isNum1 && name_len1 > 0)
	{
		Date.searchUserId = stoi(str1);
	}
	else if (name_len1 > 0)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("填写必须为用户ID"));
		return;
	}
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_SearchMember));
}

void ClubManag::UpdateSearchUserList()
{
	Widget* item = ListView_SearchAM->getItem(0);
	if (item != nullptr)
	{
		ListView_SearchAM->removeAllItems();
	}
	for (int i = 0; i < Vec_SearchMember.size(); i++)
	{
		auto node = CSLoader::createNode("platform/Club/res/ClubPartner/SearchMemberNode.csb");
		auto panel_clone = (Layout*)node->getChildByName("Panel_Clone");

		//用户头像
		auto Image_head = (ImageView*)panel_clone->getChildByName("Image_head");
		auto _userHead = GameUserHead::create(Image_head);
		_userHead->show("platform/head/men_head.png");
		_userHead->setName("Temp_Head");
		_userHead->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(Vec_SearchMember[i].userId));

		//用户名字
		auto Text_Name = (Text*)panel_clone->getChildByName("Text_Name");
		Tools::TrimSpace(Vec_SearchMember[i].szNickName);
		string str = Vec_SearchMember[i].szNickName;
		if (str.length() > 12)
		{
			str.erase(13, str.length());
			str.append("...");
			Text_Name->setString(GBKToUtf8(str));
		}
		else
		{
			Text_Name->setString(GBKToUtf8(str));
		}

		//用户ID
		auto Text_UserID = (Text*)panel_clone->getChildByName("Text_UserID");
		Text_UserID->setString(to_string(Vec_SearchMember[i].userId));

		//是否能添加合伙人
		auto Button_Add = (Button*)panel_clone->getChildByName("Button_Add");
		Button_Add->setEnabled(!Vec_SearchMember[i].bInClub);
		Button_Add->addClickEventListener([=](Ref*){
			if (Vec_SearchMember[i].bInClub)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("该玩家存在，无法设置"));
				return;
			}
			SClubRequest_AddMember Date;
			Date.clubId = m_iClubID;
			Date.gameNameId = 0;
			Date.msg = ClubRequest_AddMember;
			Date.targetUserId = Vec_SearchMember[i].userId;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_AddMember));
		});

		panel_clone->removeFromParentAndCleanup(true);
		ListView_SearchAM->pushBackCustomItem(panel_clone);
	}
}
