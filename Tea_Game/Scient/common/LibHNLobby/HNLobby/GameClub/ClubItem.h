#ifndef _GAME_CLUB_ITEM_
#define _GAME_CLUB_ITEM_

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNLobbyExport.h"
#include "HNUIExport.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class ClubItem : public Node
{
public:
	ClubItem();
	~ClubItem();

	static ClubItem* create(int clubID, SClubTeahouseInfo teahouseInfo, SClubTeahouseDeskInfo deskInfo);

	bool init(int clubID, SClubTeahouseInfo teahouseInfo, SClubTeahouseDeskInfo deskInfo);


	typedef std::function<void(int clubID, int max_Player, int max_palycoun, int teaHouseID, int roomID, int gameID, int tableIndex)> ClubItemCallBackLite;

	ClubItemCallBackLite onClubItemCallBackLite = nullptr;
	

private:
	int _clubID;
	int _max_Player;
	int _max_palycoun;
	int _teaHouseID;
	int _roomID;
	int _gameID;
	int _tableIndex;
};

#endif //_GAME_CLUB_ITEM_