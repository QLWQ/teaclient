#ifndef _GAME_CLUB_MANAG_
#define _GAME_CLUB_MANAG_

#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNRoomLogic/HNRoomLogicBase.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class ClubManag :
	public HN::IHNRoomLogicBase,
	public CCLayer,
	public ui::EditBoxDelegate
{
public:
	ClubManag();
	//
	~ClubManag();
	//
	virtual bool init();
	//
	CREATE_FUNC(ClubManag);

	typedef std::function<void()> DissmissClub;
	DissmissClub  _DissmissClubCallBack;
public:
	void SetClubID(int ClubID){ m_iClubID = ClubID; };
	void SetClubOwnerName(const std::string& message){ m_szClubOwnerName = message; };

	void SetClubName(const std::string& message){ m_szClubName = message; };

	void createManagScene();

	void UpdateRoomInfo();

	void UpdateMember();

	//搜索用户ID
	void SearchMember();

	//发送搜索（包含检测）
	void SearchInput();

	//更新搜索列表
	void UpdateSearchMember();

	//void UpdateLimit();

	//发送成员查询
	void SendQueryMember();
private:
	virtual void editBoxReturn(ui::EditBox* editBox) {};

	//初始化设置页面数据（老友圈名称，管理员账户，大赢家最低分）
	void getTeaHouseSetInfo();

	//初始化列表数据
	void initDateList();

	//监听所有动作执行消息返回
	bool getMessageBack(HNSocketMessage* socketMessage);

	//获取删除成员结果
	bool getDeleteMemberResult(HNSocketMessage* socketMessage);

	//获取成员列表结果
	bool getClubMemberInfoResult(HNSocketMessage* socketMessage);

	//更新管理员账户
	void setAdminResult(SClubRequest_GetAccessUsers_Ret* result, int Index);

	//房间详情
	void setRoomUserInfo(SClubTeahouseDeskInfo info, SClubOperationResponse_GetDeskUserInfos* Data);

	//更新战绩详情
	void setRecordInfo(SClubOperationResponse_QueryUserGameLog* Date);

	//更新同桌限制列表
	void setLimitInfo(SClubOperationResponse_QueryDeskmateLimit* Date);

	//切换页面回调
	void BtnCallBack(Ref* pSender);

	//更新搜索列表
	void UpdateSearchUserList();

	//点击成员查看弹窗
	void ShowUserInfoDump(SClubMemberInfo Data);

	//切换日期查看成员记录列表
	void setSwitchDayList(int Index);

	//切换选择日期选项
	void setSwitchDayUserInfo(int Index, int userID);

	//点击同桌限制
	void setOpenLimit_Input(int userID);

	//查询同桌限制列表
	void SendDeskmateLimit();

	//备注输入监听
	void onTextFiledCallBack(Ref* pSender, TextField::EventType type);

	//处理备注输入完成发送
	void onTextField_RemarkCallBack();
private:
	//CSB节点
	Node* Club_ManagNode = nullptr;

	//查看成员详情节点
	Node* userinfoLayer = nullptr;
	TextField* TextField_Remark = nullptr;

	//输入赠花摘花
	Layout* panel_Dump = nullptr;
	TextField* TextField_RedFlower = nullptr;
	Sprite*	Sprite_Type = nullptr;
	Text*		Text_Redflower = nullptr;
	//Text*		Text_Redflower1 = nullptr;
	int RedFlowerType = 0;			//1送花，2摘花
	int operationID = 0;				//操作ID

	//添加成员
	Layout* Panel_AddMembers = nullptr;
	TextField* TextField_SearchAM = nullptr;
	ListView* ListView_SearchAM = nullptr;

	//记录当前操作的时间记录
	int Query_Day = 0;

	//图片背景
	ImageView* Image_bg = nullptr;

	//切换基础容器
	Layout* panel_RoomList = nullptr;
	Layout* panel_Member = nullptr;
	Layout* panel_Limit = nullptr;
	Layout* Panel_teahouseset = nullptr;
	Layout* Panel_RoomInfo = nullptr;

	//成员列表
	ListView* ListView_Member = nullptr;
	//成员列表当前俱乐部显示
	Text* Text_teahouseID = nullptr;
	//输入搜索成员
	TextField* TextField_search = nullptr;
	//茶楼成员以及上限提示
	Text* Text_tip = nullptr;
	//当前显示日期
	Text* Text_current_tip = nullptr;
	//关闭弹出处理
	Layout* Panel_close = nullptr;
	ImageView* Image_switch = nullptr;

	//用户信息弹出处理
	Layout* Panel_Close_U = nullptr;
	Sprite*	sp_drop_down = nullptr;
	//当前选择日期
	Text* text_Time = nullptr;

	//黑白名单选项
	Button* Button_UserList_Sate = nullptr;

	//同桌限制列表
	ListView* ListView_Limit = nullptr;

	//用户信息弹窗战绩列表
	ListView* ListView_Record = nullptr;
	//战绩
	Text* Text_record = nullptr;
	//房间数
	Text* Text_roomNum = nullptr;
	//大赢家次数
	Text* Text_winplayerNum = nullptr;

	//输入同桌限制绑定ID
	Layout* Panel_Limit_Input = nullptr;
	TextField* TextField_Limit = nullptr;

	//老友圈昵称输入
	TextField* TextField_name = nullptr;
	//管理员账户1输入
	TextField* TextField_input1 = nullptr;
	//管理员账户2输入
	TextField* TextField_input2 = nullptr;
	//大赢家最低分数设置
	TextField* TextField_score = nullptr;


	//基础房间信息
	SClubTeahouseDeskInfo m_TempInfo;
	int m_iCurrentP = 0;

	//当前茶楼信息
	int		m_iClubID = 0;
	int		m_iCreaterID = 0;
	std::string		m_szClubOwnerName;
	std::string		m_szClubName;
	int		iClubBasisScore = 0;

	//成员列表信息
	vector<SClubMemberInfo> MemberInfo;

	//搜索成员列表
	vector<SClubMemberInfo> SearchMemberInfo;

	//搜索结果
	vector<SClubSearchMemberInfo> Vec_SearchMember;

	//同桌限制列表
	vector<SClubDeskmateLimit> Vec_LimitList;

public:

};


#endif