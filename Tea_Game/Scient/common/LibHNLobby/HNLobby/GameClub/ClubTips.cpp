#include "ClubTips.h"

static const char *CLUB_TIPS_PATH = "platform/Club/ClubTips.csb";

ClubTips::ClubTips()
{
}

ClubTips::~ClubTips()
{
}


bool ClubTips::init()
{
    if (!Layer::init())
        return false;

    //加载csb文件
    auto Node = CSLoader::createNode(CLUB_TIPS_PATH);
	//
	addChild(Node);
    //背景
	auto Bg = dynamic_cast<ImageView *>(Node->getChildByName("Image_bg"));

	//不换背景换标题
	Image_bg = dynamic_cast<ImageView *>(Bg->getChildByName("Image_top"));

    //阴影层
    auto Panel = dynamic_cast<Layout *>(Node->getChildByName("Panel_1"));
    //文本内容
	TextCount = dynamic_cast<Text *>(Bg->getChildByName("Text_count"));
    //确定按钮
	auto Btn_define = dynamic_cast<Button *>(Bg->getChildByName("Button_define"));
    //取消按钮
	auto Btn_cancel = dynamic_cast<Button *>(Bg->getChildByName("Button_cancel"));
    //添加监听事件
	Panel->addClickEventListener([=](Ref*){
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&]() {
			this->removeFromParent();
		}), nullptr));
	});
	Btn_cancel->addClickEventListener(CC_CALLBACK_1(ClubTips::onButtonDefineCallBack, this));
	Btn_define->addClickEventListener(CC_CALLBACK_1(ClubTips::onButtonDefineCallBack, this));
    return true;
}

void ClubTips::SetImageViewRes(int mun)
{
	std::string TipsClub1 = "platform/Club/res/new/sccy.png";//删除成员
	std::string TipsClub2 = "platform/Club/res/new/jsbl.png";//解散俱乐部
	std::string TipsClub3 = "platform/Club/res/new/tcbl.png";//推出俱乐部

	switch (mun)
	{
	case(1) : Image_bg->loadTexture(TipsClub1); break;
	case(2) : Image_bg->loadTexture(TipsClub2); break;
	case(3) : Image_bg->loadTexture(TipsClub3); break;

	}
}


void ClubTips::setTextCount(const std::string &str)
{
    TextCount->setString(str);
}

void ClubTips::setCallBack(std::function<void()> CallBack)
{
	_Callback = CallBack;
}

void  ClubTips::setCancelCallback(std::function<void()> CallBack)
{
	_CancelCbk = CallBack;
}

void ClubTips::onButtonDefineCallBack(Ref* pSender)
{
	auto Btn = (Button*)pSender;

	string Name = Btn->getName();

	if (Name.compare("Button_define") == 0)
	{
		if (nullptr !=_Callback)
		{
			_Callback();
		}
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&]() {
			this->removeFromParent();
		}), nullptr));
	}
	else if (Name.compare("Button_cancel") == 0)
	{
		if (nullptr != _CancelCbk)
		{
			_CancelCbk();
			return;
		}
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&]() {
			this->removeFromParent();
		}), nullptr));
	}
	
}



