#ifndef _GAME_CLUB_PARTNER_
#define _GAME_CLUB_PARTNER_

#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNRoomLogic/HNRoomLogicBase.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class ClubPartner :
	public HN::IHNRoomLogicBase,
	public CCLayer
{
public:
	ClubPartner();
	//
	~ClubPartner();
	//
	virtual bool init();
	//
	CREATE_FUNC(ClubPartner);
public:
	//初始化合作群界面
	void initPartnerList();

	//初始化成员界面
	void initPartnerMemberList(int PartnerID);

	//监听所有动作执行消息返回
	bool getMessageBack(HNSocketMessage* socketMessage);

	//设置俱乐部ID
	void setClubID(int ClubID){ m_iClubID = ClubID; };
private:
	//查询合作群列表
	void SendQueryPartnerList();

	//查询合作群列表
	void SendQueryParterList(int Day);

	//查询成员列表
	void SendQueryMember(int partnerUserId);

	//更新合作群列表
	void UpdatePartnerList();

	//更新成员列表
	void UpdateMember();

	//更新合伙人成员列表
	void UpDatePartnerMember();

	//设置添加合作群界面
	void setAddPartnerVisible(bool bShow);

	//搜索用户
	void SearchUserByID();

	//管理成员搜索
	void SearchUserInfo();

	//修改红花比例
	void ModifyRedFlowerRate();

	//更新搜索列表
	void UpdateSearchUserList();

	//点击成员查看弹窗
	void ShowUserInfoDump(SClubMemberInfo Data);

	//查询成员战绩记录
	void SendMemberRecord(int UserID);

	//设置成员战绩
	void setRecordInfo(SClubOperationResponse_QueryUserGameLog* Date);

	//查询成员列表
	void SendPartnerUserList();

private:
	//
	int m_iClubID = 0;

	//当前操作的合伙人
	int m_iPartnerID = 0;
	//当前操作的合伙人权限
	UINT	m_uiAccess = 0;

	//
	Node* m_NodePartner = nullptr;

	ListView* m_ListViewPartner = nullptr;
	Text* Text_PartnerNum = nullptr;

	//添加合作群层
	Layout* Panel_AddPartner = nullptr;
	ListView* ListView_SearchM = nullptr;
	TextField* TextField_Search = nullptr;

	//修改合作群红花比例层
	Layout* Panel_ModifyRedFRate = nullptr;
	TextField* TextField_ModifyRedFRate = nullptr;

	//添加合作群成员
	Layout* Panel_AddMembers = nullptr;
	TextField* TextField_SearchAM = nullptr;
	ListView* ListView_SearchAM = nullptr;

	//成员列表层
	Layout* Panel_MemberList = nullptr;
	ListView* ListView_Member = nullptr;

	//成员详情
	Layout* Panel_UserInfo = nullptr;
	ListView* ListView_Record = nullptr;

	//赠花摘花弹窗层
	Layout* panel_Dump = nullptr;
	int RedFlowerType = 0;
	TextField* TextField_RedFlower = nullptr;
	Sprite* Sprite_Type = nullptr;
	Text* Text_Redflower = nullptr;

	//选择时间列表
	ImageView* m_ImgMore = nullptr;
	Text* m_TextTime = nullptr;
	Layout* m_MoreClose = nullptr;

	//合伙人成员列表
	Text* P_Text_redFlowerRate = nullptr;
	Text* P_Text_reap_redFlower = nullptr;
	Text* P_Text_RoomNum = nullptr;
	Text* P_Text_BigWinNum = nullptr;
	Text* P_Text_PPlayerNum = nullptr;
	Text* P_Text_PlayerNum = nullptr;
	Button* P_Button_operation = nullptr;
	Button* P_Button_addMember = nullptr;
	ListView* P_ListView_PartnerMember = nullptr;
	Layout* P_Panel_AddMembers = nullptr;

	//合作群列表数据
	vector<SClubPartnerFilterInfo> Vec_PartnerInfo;

	//搜索结果
	vector<SClubSearchMemberInfo> Vec_SearchMember;

	//合作群红花比例容器
	//std::map<int, float > m_mapRedFlowerRate;
	vector<Text*> Vec_RedFlowerRate;

	//成员列表信息
	vector<SClubMemberInfo> MemberInfo;

	//查询时间记录（0=今日 1=昨日 2=前日）
	int Query_Day = 0;
public:

};
#endif