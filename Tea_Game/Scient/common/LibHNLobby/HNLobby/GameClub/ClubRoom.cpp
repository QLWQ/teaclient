#include "ClubRoom.h"
#include "ClubOperate.h"
#include "ClubTips.h"
const char* CLUB_ROOM_ITEM = "platform/Club/ClubRoomItem.csb";
static const char* USER_MEN_HEAD = "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";
static const char* USER_HEAD_MASK = "platform/common/touxiangdi.png";
extern std::string TextCount; //输入框内容
extern int ClubID;
ClubRoom::ClubRoom()
{
}

ClubRoom::~ClubRoom()
{
	
}

bool ClubRoom::init()
{
	if (!Layer::init()) return false;
	SendRequestClubRoomView();
	return true;
}

void ClubRoom::SendRequestClubRoomView()
{
	//请求获取俱乐部房间列表
	MSG_GP_I_Club_RoomList SendID;
	SendID.iClubID = ClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_JJ_CLUB_ROOMLIST, &SendID, sizeof(MSG_GP_I_Club_RoomList), HN_SOCKET_CALLBACK(ClubRoom::getRoomViewResult, this));
}

void ClubRoom::createRoomView()
{
	CCSize size = Size(840, 450);
	//
	_RoomView = TableView::create(this, size);
	//_RoomView->setAnchorPoint(Vec2(0, 0));
	//_RoomView->setColor(Color3B(255, 75, 65));
	//
	_RoomView->setDirection(extension::ScrollView::Direction::VERTICAL);
	//
	_RoomView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//
	_RoomView->setDelegate(this);
	//
	addChild(_RoomView);

}

//俱乐部房间信息
bool ClubRoom::getRoomViewResult(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{
		if (RoomInfo.size() != 0) RoomInfo.clear();
		auto Head = (MSG_GP_O_Club_RoomList_Head*)SocketMessage->object;
		INT RoomNum = (SocketMessage->objectSize - sizeof(MSG_GP_O_Club_RoomList_Head)) / sizeof(MSG_GP_O_JJClub_RoomList_Data);
		ClubRooms = RoomNum;
		auto Data = (MSG_GP_O_JJClub_RoomList_Data*)(SocketMessage->object + sizeof(MSG_GP_O_Club_RoomList_Head));
		if (RoomNum)
		{
			while (RoomNum--> 0)
			{
				RoomInfo.push_back(*Data);
				Data++;
			}
			if (_RoomView != nullptr)_RoomView->reloadData();
			else createRoomView();
		}
		else
		{
			if (_RoomView != nullptr)_RoomView->reloadData();
		}
		
	}
	return true;
}

//局部更新俱乐部房间信息
bool ClubRoom::updateRoomViewResult(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{

	}
	return true;
}


TableViewCell* ClubRoom::tableCellAtIndex(TableView* table, ssize_t index)  //俱乐部游戏创建列表6.11
{
	TableViewCell* cell = table->dequeueCell();
	Node* Item = nullptr;
	if (!cell)
	{
		cell = new TableViewCell();
		//cell->autorelease();
		Item = CSLoader::createNode(CLUB_ROOM_ITEM);
		Item->setPosition(Vec2(0, 0));
		Item->setTag(1);
		cell->addChild(Item);
	}
	else
	{
		Item = cell->getChildByTag(1);
	}
	//
	auto Roombg = dynamic_cast<ImageView*>(Item->getChildByName("Room_item"));
	Roombg->setSwallowTouches(false);
	//游戏总局数
	auto Count = dynamic_cast<Text*>(Roombg->getChildByName("Room_count"));
	string playcount = "局数:";
	if (RoomInfo[index].iCount == 99)
	{
		playcount = playcount + "一课";
	}
	else
	{
		playcount.append(StringUtils::toString(RoomInfo[index].iCount));
		playcount.append("局");
	}
	Count->setString(GBKToUtf8(playcount));
	//房间创始人
	//auto Creater = dynamic_cast<Text*>(Roombg->getChildByName("Room_creater"));
	/*string str = "房主:";
	str.append(RoomInfo[index].szUserNickName);*/
	//Creater->setString(GBKToUtf8(str));
	//游戏名称
	auto Name = dynamic_cast<Text*>(Roombg->getChildByName("Game_name"));
	string strName = "游戏:";
	strName.append(RoomInfo[index].szGameName);
	Name->setString(GBKToUtf8(strName));
	//人数
	auto PlayerNum = dynamic_cast<Text*>(Roombg->getChildByName("Play_count"));
	string strPlayer = "人数:";
	strPlayer.append(StringUtils::toString(RoomInfo[index].iPlayCount));
	strPlayer.append("人");
	PlayerNum->setString(GBKToUtf8(strPlayer));
	 //玩法
	auto wanfa = dynamic_cast<Text*>(Roombg->getChildByName("wanfa"));
	PlayerNum->setPositionX(PlayerNum->getPositionX()-10);
	string strWanfa = "";
	//游戏名字比对
	char gameNameDDZ[30];
	char gameNameQZMJ[30];
	char gameNameJJMJ[30];
	char gameNamePDK[30];
	strcpy(gameNameDDZ, "斗地主");
	strcpy(gameNameQZMJ, "泉州麻将");
	strcpy(gameNameJJMJ, "晋江麻将");
	strcpy(gameNamePDK, "跑得快");
	int retDDZ;
	int retQZMJ;
	int retJJMJ;
	int retPDK;
	retDDZ = strcmp(RoomInfo[index].szGameName, gameNameDDZ);
	retQZMJ = strcmp(RoomInfo[index].szGameName, gameNameQZMJ);
	retJJMJ = strcmp(RoomInfo[index].szGameName, gameNameJJMJ);
	retPDK = strcmp(RoomInfo[index].szGameName, gameNamePDK);

	//BYTE b = RoomInfo[index].FengTouType; gjd7.5
	if (retJJMJ == 0)
	{

		if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
		{
			strWanfa = "倍数：游金三倍";
		}
		else
		{
			strWanfa = "倍数：游金四倍";
		}

		if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 0)
		{
			strWanfa = strWanfa+"  底分：庄2闲1";
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 1)
		{
			strWanfa = strWanfa+"   底分：庄10闲5";
		}
		else
		{
			strWanfa = strWanfa+"   底分：庄20闲10";
		}

		string strWanfa1 = "";
		if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 0)
		{
			strWanfa = "玩法：不禁风头";
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 1)
		{
			strWanfa = "玩法：跟打";
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 2)
		{
			strWanfa = "玩法：禁风头";
		}
	
		if (asciiTointNum(RoomInfo[index].szDeskConfig[4]) == 0)
		{
			strWanfa1 = ", 双金不平胡";
		}
		else
		{
			strWanfa1 = "";
		}
		string strWanfa2 = "";

		if (asciiTointNum(RoomInfo[index].szDeskConfig[5]) == 0)
		{
			strWanfa2 = ", 抢杠胡";
		}
		else
		{
			strWanfa2 = "";
		}

		//玩法双金不平胡，抢杠胡。6.21
		strWanfa = strWanfa + strWanfa1 + strWanfa2;
		//strWanfa.append(StringUtils::toString(RoomInfo[index].szDeskConfig[2]));
		wanfa->setString(GBKToUtf8(strWanfa));
	}
	else if (retDDZ == 0)
	{
		if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
		{
			strWanfa = "玩法：经典玩法";
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 3)
		{
			strWanfa = "玩法：经典癞子";
		}
		else
		{
			strWanfa = "玩法：天地癞子";
		}
		wanfa->setString(GBKToUtf8(strWanfa));
	}
	else if (retQZMJ == 0)
	{
		if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
		{
			strWanfa = "倍数：游金三倍";
		}
		else
		{
			strWanfa = "倍数：游金四倍";
		}

		if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 0)
		{
			strWanfa = strWanfa + "  底分：庄2闲1";
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 1)
		{
			strWanfa = strWanfa + "   底分：庄10闲5";
		}
		else
		{
			strWanfa = strWanfa + "   底分：庄15闲10";
		}
		wanfa->setString(GBKToUtf8(strWanfa));
	}
	else if (retPDK == 0)
	{
		//跑得快玩法搭配
		if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
		{
			strWanfa = "玩法：首局黑桃3先出";
		}
		else
		{
			strWanfa = "玩法：每局黑桃3先出";
		}

		if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 1)
		{
			strWanfa = strWanfa + "  底分：1分";
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 3)
		{
			strWanfa = strWanfa + "   底分：3分";
		}
		else
		{
			strWanfa = strWanfa + "   底分：5分";
		}

		if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 0)
		{
			
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 1)
		{
			strWanfa = strWanfa + "   大小头：2，1";
		}
		else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 2)
		{
			strWanfa = strWanfa + "   大小头：10，5";
		}
		else
		{
			strWanfa = strWanfa + "   大小头：20，10";
		}
		wanfa->setString(GBKToUtf8(strWanfa));
	}

	//俱乐部玩家列表更新
	for (int i = 0; i < 4; i++)
	{
		string strframe = StringUtils::format("Image_frame_%d", i);
		string strhead = StringUtils::format("Image_head_%d", i);
		//俱乐部头像框
		auto HeadFrame = dynamic_cast<ImageView*>(Roombg->getChildByName(strframe));
		//俱乐部头像底图
		auto Head = dynamic_cast<ImageView*>(Roombg->getChildByName(strhead));
		//创建房间人数少于最大人数隐藏玩家节点
		if (i + 1 > RoomInfo[index].iPlayCount)
		{
			HeadFrame->setVisible(false);
			Head->setVisible(false);
		}
		else
		{
			HeadFrame->setVisible(true);
			Head->setVisible(true);
			//有用户信息
			if (RoomInfo[index].userIdList[i] != NULL)
			{
				//创建头像
				auto HeadView = GameUserHead::create(Head, HeadFrame);
				HeadView->show(USER_HEAD_MASK);//添加底图
				HeadView->loadTexture(USER_MEN_HEAD);//判断是男还是女，没有网络头像情况下剩下
				HeadView->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(RoomInfo[index].userIdList[i]));
			}
			//没有用户信息显示等待玩家
			else
			{
				Head->loadTexture("platform/Club/res/new/wait_player.png");
			}
		}
	}
	//HeadView->loadTextureWithUrl("");//设置网络头像
	//HeadView->onResponseHeadUrl(10244);


	auto Image_lock = dynamic_cast<ImageView*>(Roombg->getChildByName("Image_lock"));
	Image_lock->setZOrder(10);
	auto Join = dynamic_cast<Button*>(Roombg->getChildByName("Button_join"));
	auto IsPlay = dynamic_cast<ImageView*>(Roombg->getChildByName("Image_IsPlay"));
	if (RoomInfo[index].isPlay)
	{
		IsPlay->setVisible(true);
		Join->setEnabled(false);
	}
	//判断是否是密码房
	switch (RoomInfo[index].bIsNeedPassword)
	{
	case(0):
	{ 
		Image_lock->setVisible(false);
		Join->addClickEventListener([=](Ref*){
			// 检测是否有正在进行的比赛
			//Reconnection::getInstance()->getCutRoomInfo(1);
			//Reconnection::getInstance()->onCloseCallBack = [=]() {
				Reconnection::getInstance()->doLoginVipRoom(RoomInfo[index].szDeskPass);
			//};
		});

	}break;
	case(1):
	{ 
		//加密房间弃用
		//Image_lock->setVisible(true);
		//if (PlatformLogic()->loginResult.bLogoID == RoomInfo[index].iLogoID)
		//{
		//	Join->addClickEventListener([=](Ref*){
		//		Reconnection::getInstance()->doLoginVipRoom(RoomInfo[index].szDeskPass);
		//	});
		//}
		//else
		//{
		//	//现在游戏没有加密房间。。。
		//	Join->addClickEventListener([=](Ref*){
		//		ClubOperate* ClubOperateLayer = ClubOperate::create();
		//		auto scene = Director::getInstance()->getRunningScene();
		//		scene->addChild(ClubOperateLayer);
		//		ClubOperateLayer->SetPlaceHolderCount("请输入6位房间密码...");
		//		ClubOperateLayer->SetImageViewRes(7);
		//		ClubOperateLayer->ShowIamgeOfPassWord(true);
		//		auto Callback = [=](){
		//			if (TextCount.compare(StringUtils::toString(RoomInfo[index].szDeskPass)) == 0)
		//			{
		//				Reconnection::getInstance()->doLoginVipRoom(RoomInfo[index].szDeskPass);
		//			}
		//			else
		//			{
		//				ClubTips* ClubTipsLayer = ClubTips::create();
		//				scene->addChild(ClubTipsLayer);
		//				ClubTipsLayer->setTextCount(GBKToUtf8("密码错误！"));
		//			}
		//		};
		//		ClubOperateLayer->SetCallBack([=](){
		//			Callback();
		//		});
		//	});
		//}
	}break;
	};

	return cell;

}

//返回 Item 大小
Size ClubRoom::tableCellSizeForIndex(TableView* table, ssize_t index)
{
	return Size(840, 147);
}

//触摸函数
void ClubRoom::tableCellTouched(TableView* table, TableViewCell* cell)
{
	
}
int ClubRoom::asciiTointNum(int num)
{
	return num - 48;
}
//返回Cell总个数
ssize_t ClubRoom::numberOfCellsInTableView(TableView* table)
{
	return ClubRooms;
}



