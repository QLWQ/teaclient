#include "ClubUserRedFlow.h"
#include "HNPlatform/HNPlatformClubData.h"
#include "../GamePersionalCenter/GameUserHead.h"

static const char* TEA_HOUSE_REDFLOW_SCB_PATH = "platform/Club/res/SafflowerChange/SafflowerChangeLayer.csb";

ClubUserRedFlow::ClubUserRedFlow()
{

}

ClubUserRedFlow::~ClubUserRedFlow()
{
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest);//移除监听消息
}

bool ClubUserRedFlow::init()
{
	if (!Layer::init())
	{
		return false;
	}

	//加载csb文件（红花变化或者待处理）
	auto node = CSLoader::createNode(TEA_HOUSE_REDFLOW_SCB_PATH);
	//断言
	CCASSERT(node != nullptr, "node is null");
	addChild(node);
	CSB_Node = node;

	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, HN_SOCKET_CALLBACK(ClubUserRedFlow::getMessageBack, this));

	return true;
}

void ClubUserRedFlow::initCSBByType(int inttype, int userid, bool isManage)
{
	//背景
	auto Image_bg = (ImageView*)CSB_Node->getChildByName("Image_bg");

	//顶部提示
	auto Sprite_tip = (Sprite*)Image_bg->getChildByName("Sprite_tip");
	if (inttype == 1 || inttype == 3)
	{
		Sprite_tip->setTexture("platform/Club/res/SafflowerChange/word_hhbh.png");
	}
	else
	{
		Sprite_tip->setTexture("platform/Club/res/SafflowerChange/word_daichuli.png");
	}

	//用户ID
	auto Text_UserID = (Text*)Image_bg->getChildByName("Text_UserID");
	Text_UserID->setString(to_string(userid));

	//关闭按钮
	auto Button_close = (Button*)Image_bg->getChildByName("Button_close");
	Button_close->addClickEventListener([=](Ref*){
		this->removeFromParent();
	});

	//红花变化基础容器
	auto Panel_Change = (Layout*)Image_bg->getChildByName("Panel_Change");
	Panel_Change->setVisible(inttype == 1 || inttype == 3);

	//红花变化列表容器
	ListView_Change = (ListView*)Panel_Change->getChildByName("ListView_Change");

	//红花待处理基础容器
	auto Panel_Pending = (Layout*)Image_bg->getChildByName("Panel_Pending");
	Panel_Pending->setVisible(inttype == 2);

	//设置待处理管理员可查看自己和本群
	if (inttype == 2)
	{
		Button_me = (Button*)Panel_Pending->getChildByName("Button_me");
		Button_me->addClickEventListener([=](Ref*){
			Button_player->setEnabled(true);
			Button_me->setEnabled(false);
			//设置纹理
			auto sp_me = (Sprite*)Button_me->getChildByName("Sprite");
			sp_me->setTexture("platform/Club/res/SafflowerChange/word_wddcl_l.png");

			auto sp_player = (Sprite*)Button_player->getChildByName("Sprite");
			sp_player->setTexture("platform/Club/res/SafflowerChange/word_wjdcl_a.png");

			UpdatePendingState();
		});
		Button_player = (Button*)Panel_Pending->getChildByName("Button_player");
		Button_player->addClickEventListener([=](Ref*){
			Button_player->setEnabled(false);
			Button_me->setEnabled(true);
			//设置纹理
			auto sp_me = (Sprite*)Button_me->getChildByName("Sprite");
			sp_me->setTexture("platform/Club/res/SafflowerChange/word_wddcl_a.png");

			auto sp_player = (Sprite*)Button_player->getChildByName("Sprite");
			sp_player->setTexture("platform/Club/res/SafflowerChange/word_wjdcl_l.png");
			
			SendAllPendingLog();
		});
		Button_player->setVisible(isManage);
	}

	//红花待处理列表容器
	ListView_Pending = (ListView*)Panel_Pending->getChildByName("ListView_Pending");

	//发送查询记录
	if (inttype == 1)
	{
		SClubRequest_QueryRedFlowersLog  Data;
		Data.gameNameId = 0;
		Data.msg = ClubRequest_QueryRedFlowersLog;
		Data.clubId = m_iClubID;
		Data.targetUserId = userid;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_QueryRedFlowersLog));
	}
	else if (inttype == 3)
	{
		SClubRequest_QueryRedFlowersLog  Data;
		Data.gameNameId = 0;
		Data.msg = ClubRequest_QueryRedFlowersLog;
		Data.clubId = m_iClubID;
		Data.targetUserId = userid;
		Data.partnerUserId = userid;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_QueryRedFlowersLog));
	}
	else if (inttype == 2)
	{
		SClubRequest_QueryPendingLog  Data;
		Data.gameNameId = 0;
		Data.msg = ClubRequest_QueryPendingLog;
		Data.clubId = m_iClubID;
		Data.targetUserId = userid;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_QueryPendingLog));
		m_isManage = isManage;
	}

	m_iUserID = userid;
	m_iInitType = inttype;
}

//更新红花日志
void ClubUserRedFlow::UpdateFlowersLog()
{
	if (ListView_Change)
	{
		Widget* item = ListView_Change->getItem(0);
		if (item != nullptr)
		{
			ListView_Change->removeAllItems();
		}

		//------//管理员和成员显示内容不一致区分处理(m_isManage)
		//如果是看自己的红花变化使用详细展示
		if (Result.dwUserID != m_iUserID)
		{
			for (int i = 0; i < Vec_FlowersLog.size(); i++)
			{
				auto node = CSLoader::createNode("platform/Club/res/SafflowerChange/ChangeLogNode.csb");
				auto panel_clone = (Layout*)node->getChildByName("Panel_clone");

				//时间
				time_t Time = Vec_FlowersLog[i].date;
				struct tm *p = gmtime(&Time);
				p->tm_hour += 8;
				if (p->tm_hour >= 24)
				{
					p->tm_hour -= 24;
				}
				char BeginTime[64];
				strftime(BeginTime, sizeof(BeginTime), "%Y-%m-%d %H:%M:%S", p);
				auto Text_time = (Text*)panel_clone->getChildByName("Text_time");
				Text_time->setString(BeginTime);

				//原始分数
				auto Text_score_0 = (Text*)panel_clone->getChildByName("Text_score_0");
				string score_0 = "";
				if (m_isManage)
				{
					if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
					{
						score_0 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
					{
						score_0 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
					{
						score_0 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers);
					}
					else
					{
						score_0 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers);
					}
					
				}
				else
				{
					score_0 = to_string((int)Vec_FlowersLog[i].dstOldRedFlowers);
				}
				Text_score_0->setString(score_0);

				//变化分数
				auto Text_score_1 = (Text*)panel_clone->getChildByName("Text_score_1");
				string score_1 = "";
				if (m_isManage)
				{
					//score_1 = StringUtils::format("%.2f", Vec_FlowersLog[i].redFlowers);
					if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
					{
						score_1 = StringUtils::format("+%.2f", Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
					{
						score_1 = StringUtils::format("-%.2f", Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
					{
						score_1 = StringUtils::format("%.2f", Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
					{
						score_1 = StringUtils::format("%.2f", Vec_FlowersLog[i].redFlowers);
					}
				}
				else
				{
					if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
					{
						score_1 = StringUtils::format("+%d", (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
					{
						score_1 = StringUtils::format("-%d", (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
					{
						score_1 = StringUtils::format("%d", (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)//返利含有小数点
					{
						score_1 = StringUtils::format("%.2f", Vec_FlowersLog[i].redFlowers);
					}
				}
				Text_score_1->setString(score_1);

				//变化后分数
				auto Text_score_2 = (Text*)panel_clone->getChildByName("Text_score_2");
				string score_2 = "";
				if (m_isManage)
				{
					if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
					{
						score_2 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers + Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
					{
						score_2 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers - Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
					{
						score_2 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers + Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
					{
						score_2 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers + Vec_FlowersLog[i].redFlowers);
					}
				}
				else
				{
					if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
					{
						score_2 = to_string((int)Vec_FlowersLog[i].dstOldRedFlowers + (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
					{
						score_2 = to_string((int)Vec_FlowersLog[i].dstOldRedFlowers - (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
					{
						score_2 = to_string((int)Vec_FlowersLog[i].dstOldRedFlowers + (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
					{
						score_2 = StringUtils::format("%.2f", Vec_FlowersLog[i].dstOldRedFlowers + Vec_FlowersLog[i].redFlowers);
					}
				}
				Text_score_2->setString(score_2);

				//变化类型
				auto Image_Change_Type = (ImageView*)panel_clone->getChildByName("Image_Change_Type");
				if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
				{
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_huozeng.png");
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut)
				{
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_zhaiqu.png");
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
				{
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_zhaiqu.png");
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
				{
					//结算
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_jiesuan.png");
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
				{
					//结算
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_jianglili.png");
				}

				//谁赠送（谁摘取）ID
				auto Text_user_ID = (Text*)panel_clone->getChildByName("Text_user_ID");
				string str_user_id = m_iUserID == Vec_FlowersLog[i].dstUserId ? to_string(Vec_FlowersLog[i].srcUserId) : to_string(Vec_FlowersLog[i].dstUserId);
				Text_user_ID->setString(str_user_id);

				//谁赠送（谁摘取）名字
				auto Text_user_Name = (Text*)panel_clone->getChildByName("Text_user_Name");
				string str_user_Name = m_iUserID == Vec_FlowersLog[i].dstUserId ? Vec_FlowersLog[i].szSrcUserName : Vec_FlowersLog[i].szDstUserName;
				if (str_user_Name.length() > 16)
				{
					str_user_Name.erase(17, str_user_Name.length());
					str_user_Name.append("...");
					Text_user_Name->setString(GBKToUtf8(str_user_Name));
				}
				else
				{
					Text_user_Name->setString(GBKToUtf8(str_user_Name));
				}

				if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
				{
					Text_user_ID->setVisible(false);
					Text_user_Name->setVisible(false);
				}

				panel_clone->removeFromParentAndCleanup(true);
				ListView_Change->pushBackCustomItem(panel_clone);
			}
		}
		else
		{
			for (int i = 0; i < Vec_FlowersLog.size(); i++)
			{
				auto node = CSLoader::createNode("platform/Club/res/SafflowerChange/RedFlowerLogNode.csb");
				auto panel_clone = (Layout*)node->getChildByName("Panel_clone");

				//时间
				time_t Time = Vec_FlowersLog[i].date;
				struct tm *p = gmtime(&Time);
				p->tm_hour += 8;
				if (p->tm_hour >= 24)
				{
					p->tm_hour -= 24;
				}
				char BeginTime[64];
				strftime(BeginTime, sizeof(BeginTime), "%Y-%m-%d %H:%M:%S", p);
				auto Text_time = (Text*)panel_clone->getChildByName("Text_time");
				Text_time->setString(BeginTime);

				//送花还是摘花
				auto Text_srcS = (Text*)panel_clone->getChildByName("Text_srcS");

				//送花
				auto Text_szSrc = (Text*)panel_clone->getChildByName("Text_szSrc");
				//string str_user_id = to_string(Vec_FlowersLog[i].srcUserId);
				string str_user_Name = Vec_FlowersLog[i].szSrcUserName;
				if (str_user_Name.length() > 16)
				{
					str_user_Name.erase(17, str_user_Name.length());
					str_user_Name.append("...");
					Text_szSrc->setString(GBKToUtf8(StringUtils::format("%s/%d", str_user_Name.c_str(), Vec_FlowersLog[i].srcUserId)));
				}
				else
				{
					Text_szSrc->setString(GBKToUtf8(StringUtils::format("%s/%d", str_user_Name.c_str(), Vec_FlowersLog[i].srcUserId)));
				}
				/////////////////////////////////////////////////////////
				if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
				{
					Text_srcS->setString(GBKToUtf8("送花："));
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
				{
					Text_srcS->setString(GBKToUtf8("摘花："));
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game || Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
				{
					Text_srcS->setVisible(false);
					Text_szSrc->setVisible(false);
				}

				//收花还是被摘花
				auto Text_srcD = (Text*)panel_clone->getChildByName("Text_srcD");

				//收花
				auto Text_szDst = (Text*)panel_clone->getChildByName("Text_szDst");
				//string str_userD_id = to_string(Vec_FlowersLog[i].dstUserId);
				string str_userD_Name = Vec_FlowersLog[i].szDstUserName;
				if (str_userD_Name.length() > 16)
				{
					str_userD_Name.erase(17, str_userD_Name.length());
					str_userD_Name.append("...");
					Text_szDst->setString(GBKToUtf8(StringUtils::format("%s/%d", str_userD_Name.c_str(), Vec_FlowersLog[i].dstUserId)));
				}
				else
				{
					Text_szDst->setString(GBKToUtf8(StringUtils::format("%s/%d", str_userD_Name.c_str(), Vec_FlowersLog[i].dstUserId)));
				}
				////////////////////////
				if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
				{
					Text_srcD->setString(GBKToUtf8("收花："));
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
				{
					Text_srcD->setString(GBKToUtf8("被摘花："));
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game || Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
				{
					Text_srcD->setString(GBKToUtf8("用户："));
					Text_srcD->setPositionY(45.6);
					Text_szDst->setPositionY(45.6);
					//Text_srcD->setVisible(false);
					//Text_szDst->setVisible(false);
				}

				//变化分数
				auto Text_score_1 = (Text*)panel_clone->getChildByName("Text_score_1");
				string score_1 = "";
				if (m_isManage)
				{
					if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
					{
						score_1 = StringUtils::format("+%.2f", Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
					{
						score_1 = StringUtils::format("-%.2f", Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
					{
						score_1 = StringUtils::format("%.2f", Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
					{
						score_1 = StringUtils::format("%.2f", Vec_FlowersLog[i].redFlowers);
					}
				}
				else
				{
					if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
					{
						score_1 = StringUtils::format("+%d", (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut || Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
					{
						score_1 = StringUtils::format("-%d", (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
					{
						score_1 = StringUtils::format("%d", (int)Vec_FlowersLog[i].redFlowers);
					}
					else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Rebate)
					{
						score_1 = StringUtils::format("%.2f", Vec_FlowersLog[i].redFlowers);
					}
				}
				Text_score_1->setString(score_1);

				//变化类型
				auto Image_Change_Type = (ImageView*)panel_clone->getChildByName("Image_Change_Type");
				if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Give)
				{
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_huozeng.png");
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut)
				{
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_zhaiqu.png");
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_TakeOut_Game)
				{
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_zhaiqu.png");
				}
				else if (Vec_FlowersLog[i].type == ClubRedFlowersLogType_Score_Game)
				{
					//结算
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_jiesuan.png");
				}
				else
				{
					Image_Change_Type->loadTexture("platform/Club/res/SafflowerChange/img_jianglili.png");
				}

				panel_clone->removeFromParentAndCleanup(true);
				ListView_Change->pushBackCustomItem(panel_clone);
			}
		}
	}
}

//更新待处理日志
void ClubUserRedFlow::UpdatePendingLog()
{
	if (ListView_Pending)
	{
		Widget* item = ListView_Pending->getItem(0);
		if (item != nullptr)
		{
			ListView_Pending->removeAllItems();
		}

		for (int i = 0; i < Vec_PendingLog.size(); i++)
		{
			auto node = CSLoader::createNode("platform/Club/res/SafflowerChange/PendingFlowerNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("Panel_clone");

			//时间
			time_t Time = Vec_PendingLog[i].date;
			struct tm *p = gmtime(&Time);
			p->tm_hour += 8;
			if (p->tm_hour >= 24)
			{
				p->tm_hour -= 24;
			}
			char BeginTime[64];
			strftime(BeginTime, sizeof(BeginTime), "%Y-%m-%d %H:%M:%S", p);
			auto Text_time = (Text*)panel_clone->getChildByName("Text_time");
			Text_time->setString(BeginTime);

			//待处理红花数量
			auto Text_score_1 = (Text*)panel_clone->getChildByName("Text_score_1");
			Text_score_1->setString(to_string((int)Vec_PendingLog[i].amount));

			//状态
			auto Text_state = (Text*)panel_clone->getChildByName("Text_state");
			Text_state->setString(Vec_PendingLog[i].bProcessed ? GBKToUtf8("已处理") : GBKToUtf8("未处理"));

			//标记玩家(待处理玩家)
			auto Text_szSrc = (Text*)panel_clone->getChildByName("Text_szSrc");
			Text_szSrc->setString(GBKToUtf8(StringUtils::format("%s\n%d", Vec_PendingLog[i].szName, Vec_PendingLog[i].userId)));

			//标记完成
			auto Button_Processed = (Button*)panel_clone->getChildByName("Button_Processed");
			Button_Processed->setVisible(m_isManage);
			if (Vec_PendingLog[i].bProcessed)
			{
				Button_Processed->setVisible(false);
			}
			Button_Processed->addClickEventListener([=](Ref*){
				auto prompt = GamePromptLayer::create(true);
				prompt->showPrompt(GBKToUtf8("您确定完成操作并移除至完成状态？"));
				prompt->setCallBack([=](){
					SClubRequest_ProcessPendingLog  Data;
					Data.gameNameId = 0;
					Data.msg = ClubRequest_ProcessPendingLog;
					Data.clubId = m_iClubID;
					Data.targetUserId = m_iUserID;
					Data.logId = Vec_PendingLog[i].logId;
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_ProcessPendingLog));
				});
			});

			panel_clone->removeFromParentAndCleanup(true);
			ListView_Pending->pushBackCustomItem(panel_clone);
		}
	}
}

//监听所有动作执行消息返回
bool ClubUserRedFlow::getMessageBack(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{
		SClubResponseBase* Data = (SClubResponseBase*)SocketMessage->object;
		if (Data->msg == ClubRequest_QueryRedFlowersLog)
		{
			SClubOperationResponse_QueryRedFlowersLog* Data = (SClubOperationResponse_QueryRedFlowersLog*)SocketMessage->object;
			if (Data->logCount > 0)
			{
				for (int i = 0; i<Data->logCount; i++)
				{
					Vec_FlowersLog.push_back(Data->flowersLogList[i]);
				}
				//更新列表
				UpdateFlowersLog();
			}
		}
		else if (Data->msg == ClubRequest_QueryPendingLog)
		{
			SClubOperationResponse_QueryPendingLog* Data = (SClubOperationResponse_QueryPendingLog*)SocketMessage->object;
			if (Data->logCount > 0)
			{
				for (int i = 0; i<Data->logCount; i++)
				{
					Vec_PendingLog.push_back(Data->pendingLogList[i]);
				}
				//更新列表
				UpdatePendingLog();
			}
		}
		else if (ClubRequest_ProcessPendingLog == Data->msg)
		{
			SClubOperationResponse_ProcessPendingLog* Data = (SClubOperationResponse_ProcessPendingLog*)SocketMessage->object;
			UpdatePendingState();
		}
	}

	return true;
}

void ClubUserRedFlow::UpdatePendingState()
{
	if (Vec_PendingLog.size() > 0) Vec_PendingLog.clear();
	SClubRequest_QueryPendingLog  Data;
	Data.gameNameId = 0;
	Data.msg = ClubRequest_QueryPendingLog;
	Data.clubId = m_iClubID;
	Data.targetUserId = m_iUserID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_QueryPendingLog));
}

void ClubUserRedFlow::SendAllPendingLog()
{
	if (Vec_PendingLog.size() > 0) Vec_PendingLog.clear();
	SClubRequest_QueryPendingLog  Data;
	Data.gameNameId = 0;
	Data.msg = ClubRequest_QueryPendingLog;
	Data.clubId = m_iClubID;
	Data.targetUserId = 0;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_QueryPendingLog));
}