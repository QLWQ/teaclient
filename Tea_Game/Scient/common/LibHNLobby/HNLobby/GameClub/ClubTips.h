#ifndef _CLUB_TIPS_
#define _CLUB_TIPS_

#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class ClubTips : public Layer
{

  public:


    ClubTips();

    ~ClubTips();

    virtual bool init();

    CREATE_FUNC(ClubTips);

  public:

	void setCallBack(std::function<void()> CallBack);

	void  setCancelCallback(std::function<void()> CallBack);

	void setTextCount(const std::string &str);
	
	void onButtonDefineCallBack(Ref* pSender);

	void SetImageViewRes(int mun);
  private:
    Text *TextCount = nullptr;
	ImageView* Image_bg = nullptr;

    std::function<void()>_Callback = nullptr;

	std::function<void()>_CancelCbk = nullptr;
};

#endif