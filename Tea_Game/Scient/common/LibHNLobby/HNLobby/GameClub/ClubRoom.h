#ifndef _CLUB_ROOM_
#define _CLUB_ROOM_


#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;


class ClubRoom :
	public Layer,
	public TableViewDataSource,
	public TableViewDelegate{


public:

	ClubRoom();

	~ClubRoom();

	virtual bool init();

	CREATE_FUNC(ClubRoom);

	
private:
	//发送获取房间列表请求
	void SendRequestClubRoomView();

	//创建房间列表
	void createRoomView();

	//获取房间列表结果
	bool getRoomViewResult(HNSocketMessage* SocketMessage);

	//局部更新房间列表信息
	bool updateRoomViewResult(HNSocketMessage* SocketMessage);
	
	//房间监听事件
	bool RoomEventListener(HNSocketMessage* socketMessage);
	
	//触摸事件
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);

	//Cell大小
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);

	//添加Cell
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

	//设置Cell个数
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	//转换一下桌子信息字符串
	int asciiTointNum(int num);

public:
	//房间数据
	vector<MSG_GP_O_JJClub_RoomList_Data>RoomInfo;
	int ClubRooms = 0;

private:

	TableView* _RoomView = nullptr;


};
#endif 