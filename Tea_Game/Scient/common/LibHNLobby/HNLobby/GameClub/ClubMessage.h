#ifndef _CLUB_MESSAGE_
#define _CLUB_MESSAGE_

#include "HNNetExport.h"
#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class ClubMsg : 
	public Layer,
	public TableViewDataSource,
	public TableViewDelegate{

public:

	virtual bool init();

	CREATE_FUNC(ClubMsg);

public:

	//请求获取俱乐部消息
	void RequestClubMessage(int);

private:

	//请求申请列表信息
	bool getClubMsgResult(HNSocketMessage* socketMessage);

	//获取审核操作结果
	bool getOperateResult(HNSocketMessage* socketMessage);

	//创建TableView
	void CreateTableView();

	//触摸事件
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);

	//Cell大小
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);

	//添加Cell
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

	//设置Cell个数
	virtual ssize_t numberOfCellsInTableView(TableView *table);

private:
	
	vector<MSG_GP_O_Club_ReviewList_Data>Message;
private:

	TableView *	_tableView = nullptr;
	ImageView*	Image_bg = nullptr;
	Text* Text_Tips = nullptr;
	Layout* Panel_2 = nullptr;
};
#endif