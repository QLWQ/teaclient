#ifndef _GAME_CLUB_USERREDFLOW_
#define _GAME_CLUB_USERREDFLOW_

#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNRoomLogic/HNRoomLogicBase.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class ClubUserRedFlow :
	public HN::IHNRoomLogicBase,
	public CCLayer
{
public:
	ClubUserRedFlow();
	~ClubUserRedFlow();
	//
	virtual bool init();
	//
	CREATE_FUNC(ClubUserRedFlow);

	void initCSBByType(int Index, int userid, bool isManage);

	void SetClubID(int ClubID){ m_iClubID = ClubID; };
private:
	//监听所有动作执行消息返回
	bool getMessageBack(HNSocketMessage* socketMessage);

	//更新红花日志
	void UpdateFlowersLog();

	//更新待处理日志
	void UpdatePendingLog();

	//查询待处理更新
	void UpdatePendingState();

	//管理员查询所有待处理
	void SendAllPendingLog();
private:

	//记录当前CSB节点
	Node* CSB_Node = nullptr;

	//红花变化列表
	ListView* ListView_Change = nullptr;

	//红花待处理列表
	ListView* ListView_Pending = nullptr;
	Button* Button_me = nullptr;
	Button* Button_player = nullptr;

	bool m_isManage = false;
	int m_iClubID = 0;
	int m_iUserID = 0;
	int m_iInitType = 0;

	//查询红花变化日志
	vector<SClubFlowersLog> Vec_FlowersLog;

	//待处理日志
	vector<SClubPendingLog> Vec_PendingLog;

	//登录结束信息
	MSG_GP_R_LogonResult &Result = PlatformLogic()->loginResult;
};


#endif