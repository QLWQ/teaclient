#include "GameClub.h"
#include "ClubMessage.h"
#include "ClubRoom.h"
#include "ClubMember.h"
#include "ClubTips.h"
#include "ClubOperate.h"
#include "ClubManag.h"
#include "ClubPartner.h"
#include "ClubUserRedFlow.h"
#include "../GameChildLayer/GameShareLayer.h"
#include "../GameRecord/RecordList/GameRoomRecordClub.h"
#include "ClubItem.h"

static const char* CLUB_CSB_PATH = "platform/Club/ClubScene.csb";
static const char* TEA_HOUSE_SCB_PATH = "platform/Club/GameClubLayer.csb";
static const char* CLUB_ITEM_PATH = "platform/Club/ClubItem.csb";
static const char* CLUB_NOTICE_PATH = "platform/Club/ClubNotice.csb";
static const char* USER_MEN_HEAD = "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";
static const char* USER_HEAD_MASK = "platform/common/touxiangdi.png";
static const char* NO_CLUB = "platform/Club/res/out_game/noclubbg.png";
static const char* HAVE_CLUB = "platform/Club/res/out_game/bg.png";
static const char* allocation_table_please_wait_text = "正在配桌，请稍后......";
static const char* enterGame_please_wait_text = "正在进入游戏，请稍后......";
//static const std::string TEA_HOUSE_TEXT[] = { "大厅", "1层", "2层", "3层", "4层", "5层", "6层" };

std::string CreateorJoin;//区分选中的是自己创建的还是加入的俱乐部列表
bool HaveSelected = false;
std::string SelectedName = "";
int SelectedNum;
int ClubID;				//外部变量俱乐部ID
extern std::string TextCount; //输入框内容
bool isClubCreater;		//是否是俱乐部创始人
int isManage;			//是否是管理员(创建者为1，管理员2，合作群3)
Club::Club()
{
	_roomLogic = new HNRoomLogicBase(this);
}

Club::~Club()
{
	HaveSelected = false;
	isManage = 0;
	isClubCreater = false;
	//RemoveAllNetWorkNewsEventListener();
	RemoveNetWorkNewsEventListener();
	Director::getInstance()->getEventDispatcher()->removeEventListener(_foreground);
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
	_roomLogic->stop();
	HN_SAFE_DELETE(_roomLogic);
}

Club* Club::create(int clubID)
{
	Club* pRet = new Club();
	if (pRet && pRet->init(clubID))
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

Club* Club::createClubScene(int clubID /*= 0*/)
{
	//创建新场景
	Scene* ClubScene = Scene::create();
	//创建层
	auto ClubLayer = Club::create(clubID);
	//
	ClubScene->addChild(ClubLayer);
	//
	//另一场景由整体从左面出现
	Director::getInstance()->replaceScene(TransitionMoveInL::create(0.3f, ClubScene));
	return ClubLayer;
}


bool Club::init(int clubID)
{
	if (!Layer::init())
	{
		return false;
	}
	//加载csb文件（茶楼或者俱乐部）
	auto node = CSLoader::createNode(TEA_HOUSE_SCB_PATH);
	//断言
	CCASSERT(node != nullptr, "node is null");
	//添加csb文件
	addChild(node);

	//茶楼节点
	TeaHouseNode = node;

	//背景
	Image_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	
	//加载俱乐部视图
	//LoadClubView();

	//加载茶楼视图
	LoadTeaHouse(clubID);
	
	HNPlatformConfig()->setSceneState(PlatformConfig::SCENE_STATE::OTHER);

	// 切回前台通知
	_foreground = EventListenerCustom::create(FOCEGROUND, [=](EventCustom* event) {
		if (ClubID != 0)
		{
			//ClubID = 0;
			RequestTeaHouse();
			//updateClubNotice();
		}
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_foreground, 1);

	// 断线重连通知
	_listener = EventListenerCustom::create(RECONNECTION, [=](EventCustom* event) {
		if (ClubID != 0)
		{
			//ClubID = 0;
			RequestTeaHouse();
			//updateClubNotice();
		}
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);

	return true;
}


void Club::LoadClubView()
{
	//左边区域节点
	{
		Node_left = dynamic_cast<Node*>(Image_bg->getChildByName("Node_left"));
		//俱乐部滑动列表
		_ClubLayout = dynamic_cast<Layout*>(Node_left->getChildByName("Club_layout"));
		//左边区域底图
		//Image_left = dynamic_cast<ImageView*>(Node_left->getChildByName("Image_left"));
		//选中方底图
		//Image_di = dynamic_cast<ImageView*>(Node_left->getChildByName("Image_di"));
		//我的创建
		_mycretae = dynamic_cast<ImageView*>(Node_left->getChildByName("Image_create"));
		//我的加入
		_myjoin = dynamic_cast<ImageView*>(Node_left->getChildByName("Image_join"));
		//创建与加入按钮俱乐部
		bottom_create = dynamic_cast<Button*>(Node_left->getChildByName("Button_newclub"));
		//遮罩
		auto Image_create = dynamic_cast<ImageView*>(Node_left->getChildByName("Image_mycreate"));
		auto Image_join = dynamic_cast<ImageView*>(Node_left->getChildByName("Image_myjoin"));
		Image_create->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		Image_join->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		bottom_create->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		bottom_create->setPressedActionEnabled(true); 
		_myjoin->setOpacity(0);
	}
	//创建与加入节点
	{
		Node_createjoin = dynamic_cast<Node*>(Image_bg->getChildByName("Node_createjoin"));
		//创建俱乐部按钮
		auto btn_create = dynamic_cast<Button*>(Node_createjoin->getChildByName("Button_create"));
		//加入俱乐部按钮
		auto btn_join = dynamic_cast<Button*>(Node_createjoin->getChildByName("Button_join"));
		//返回按钮
		auto btn_back = dynamic_cast<Button*>(Node_createjoin->getChildByName("Button_back"));
		//添加创建按钮回调
		btn_create->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_join->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_back->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_join->setPressedActionEnabled(true);
		btn_back->setPressedActionEnabled(true);
		btn_create->setPressedActionEnabled(true);

	}
	//顶部节点
	{
		//顶部按钮和文本节点
		Node_top = dynamic_cast<Node*>(Image_bg->getChildByName("Node_top"));
		//俱乐部名称
		auto _ClubName = dynamic_cast<Text*>(Node_top->getChildByName("Club_name"));
		//俱乐部ID
		auto _Club_ID = dynamic_cast<Text*>(Node_top->getChildByName("Club_id"));
		//设置背景图
		_setView = dynamic_cast<ImageView*>(Node_top->getChildByName("Set_bg"));
		//消息红点
		Image_red = dynamic_cast<ImageView*>(Node_top->getChildByName("Image_red"));
		//解散或退出俱乐部按钮
		btn_disband = dynamic_cast<ImageView*>(_setView->getChildByName("Club_disband"));
		//修改俱乐部名称按钮
		auto btn_change = dynamic_cast<ImageView*>(_setView->getChildByName("Club_changeName"));
		//修改公告按钮
		auto btn_notice = dynamic_cast<ImageView*>(_setView->getChildByName("Club_notice"));
		//加入消息按钮
		auto btn_msg = dynamic_cast<Button*>(Node_top->getChildByName("Button_info"));
		//设置按钮
		auto btn_set = dynamic_cast<Button*>(Node_top->getChildByName("Button_set"));
		//成员按钮
		auto btn_member = dynamic_cast<Button*>(Node_top->getChildByName("Button_member"));
		//邀请按钮
		auto btn_invite = dynamic_cast<Button*>(Node_top->getChildByName("Button_invite"));
		//返回按钮
		auto btn_back = dynamic_cast<Button*>(Node_top->getChildByName("Button_close"));
		//俱乐部房卡数量显示
		score_bg = dynamic_cast<ImageView*>(Node_top->getChildByName("Score_bg"));
		//房卡数量
		score = dynamic_cast<Text*>(score_bg->getChildByName("Club_score"));
		//添加房卡按钮
		auto btn_add = dynamic_cast<Button*>(score_bg->getChildByName("Button_add"));
		//房卡数量显示
		Image_red->setVisible(false);
		//加入消息按钮回调
		btn_add->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_msg->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_back->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_set->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_member->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_disband->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_change->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_notice->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		btn_invite->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		//设置按钮按下时启用缩放效果
		btn_add->setPressedActionEnabled(true);
		btn_msg->setPressedActionEnabled(true);
		btn_back->setPressedActionEnabled(true);
		btn_set->setPressedActionEnabled(true);
		btn_member->setPressedActionEnabled(true);
	}
	//底部节点
	{
		//底部节点
		Node_bottom = dynamic_cast<Node*>(Image_bg->getChildByName("Node_bottom"));
		//创建按钮
		auto Btn_Create = dynamic_cast<Button*>(Node_bottom->getChildByName("Room_Create"));
		//刷新按钮
		auto Btn_Refresh = dynamic_cast<Button*>(Node_bottom->getChildByName("Room_Refresh"));
		//战绩按钮
		auto Btn_Record = dynamic_cast<Button*>(Node_bottom->getChildByName("Club_Record"));
		//添加按钮监听事件
		Btn_Create->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		Btn_Refresh->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		Btn_Record->addClickEventListener(CC_CALLBACK_1(Club::BtnCallBack, this));
		Btn_Refresh->setPressedActionEnabled(true);
		Btn_Create->setPressedActionEnabled(true);
		Btn_Record->setPressedActionEnabled(true);
	}

	//房间节点
	{
		Node_room = dynamic_cast<Node*>(Image_bg->getChildByName("Node_room"));
		//获取房间滑动列表
		RoomView = dynamic_cast<ui::ScrollView*>(Node_room->getChildByName("ScrollView_Room"));
		//RoomView->setVisible(false);
		RoomListView = dynamic_cast<ui::ListView*>(Node_room->getChildByName("ListView_Room"));
		//RoomListView->setVisible(true);
	}
	//拉取俱乐部列表请求
	RequestClubView();
}

//加载茶楼
void Club::LoadTeaHouse(int clubID)
{
	//茶楼信息
	Panel_ClubInfo = (Layout*)TeaHouseNode->getChildByName("Panel_ClubInfo");
	Text_ClubName = (Text*)Panel_ClubInfo->getChildByName("Text_ClubName");

	ClubID = clubID > 0 ? clubID : 0;

	// 玩家信息结构体
	MSG_GP_R_LogonResult& LogonResult = PlatformLogic()->loginResult;

	//用户信息
	auto	Image_head = (ImageView*)Panel_ClubInfo->getChildByName("Image_head");
	auto headFarme = (ImageView*)Panel_ClubInfo->getChildByName("Image_frame");
	auto head = GameUserHead::create(Image_head, headFarme);
	head->show(USER_HEAD_MASK);
	head->loadTextureWithUrl(LogonResult.headUrl);
	head->loadTexture(LogonResult.bBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);//判断是男还是女，没有网络头像情况下剩下

	//用户名字和用户ID
	auto UserName = (Text*)Panel_ClubInfo->getChildByName("Text_name");
	Tools::TrimSpace(LogonResult.nickName);
	std::string nickName(LogonResult.nickName);
	if (!nickName.empty())
	{
		string str = LogonResult.nickName;
		if (str.length() > 12)
		{
			str.erase(13, str.length());
			str.append("...");
			UserName->setString(GBKToUtf8(str));
		}
		else
		{
			UserName->setString(GBKToUtf8(str));
		}
	}
	else
	{
		UserName->setString(GBKToUtf8("未知"));
	}

	auto UserID = (Text*)Panel_ClubInfo->getChildByName("Text_id");
	UserID->setString(StringUtils::format("ID:%d", LogonResult.dwUserID));

	//茶楼ID
	Text_club_id = (Text*)Panel_ClubInfo->getChildByName("Text_club_id");

	//成员管理
	Button_member = (Button*)Panel_ClubInfo->getChildByName("Button_approve");
	Button_member->setVisible(false);
	Button_member->addClickEventListener([=](Ref*){
		Image_Red_Member->setVisible(false);
		ClubMsg* ClubMsgLayer = ClubMsg::create();
		ClubMsgLayer->setName("ClubMsgLayer");
		this->addChild(ClubMsgLayer);
		ClubMsgLayer->RequestClubMessage(ClubID);
	});

	//成员管理红点
	Image_Red_Member = (ImageView*)Button_member->getChildByName("Image_red");
	Image_Red_Member->setVisible(false);

	//群管理
	Button_teaSet = (Button*)Panel_ClubInfo->getChildByName("Button_teaSet");
	Button_teaSet->setVisible(false);
	Button_teaSet->addClickEventListener([=](Ref*){
		MSG_GP_O_Club_List Data = getClubInfoByClubID(ClubID);
		string str = Data.szClubOwnerName;
		ClubManag* ClubManag = ClubManag::create();
		ClubManag->SetClubID(ClubID);
		ClubManag->SetClubName(Text_ClubName->getString());
		ClubManag->SetClubOwnerName(str);
		ClubManag->createManagScene();
		ClubManag->setName("ClubManag");
		ClubManag->_DissmissClubCallBack = [=]()
		{
			MSG_GP_I_DissmissClub Request;
			Request.iClubID = ClubID;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_DISSMISS_CLUB, &Request, sizeof(MSG_GP_I_DissmissClub), HN_SOCKET_CALLBACK(Club::ClubDisbandResult, this));
		};
		this->addChild(ClubManag);
	});

	//合作群
	Button_Cooperation = (Button*)Panel_ClubInfo->getChildByName("Button_Cooperation");
	Button_Cooperation->setVisible(false);
	Button_Cooperation->addClickEventListener([=](Ref*){
		ClubPartner* ClubPartner = ClubPartner::create();
		ClubPartner->setClubID(ClubID);
		ClubPartner->initPartnerList();
		ClubPartner->setName("ClubPartner");
		this->addChild(ClubPartner);
	});

	//成员
	Button_Member = (Button*)Panel_ClubInfo->getChildByName("Button_Member");
	Button_Member->setVisible(false);
	Button_Member->addClickEventListener([=](Ref*){
		ClubPartner* ClubPartner = ClubPartner::create();
		ClubPartner->setClubID(ClubID);
		//ClubPartner->initPartnerList();
		ClubPartner->initPartnerMemberList(LogonResult.dwUserID);
		ClubPartner->setName("ClubPartner");
		this->addChild(ClubPartner);
	});

	//切换老友圈
	auto Button_switch = (Button*)Panel_ClubInfo->getChildByName("Button_switch");
	Button_switch->addClickEventListener([=](Ref*){
		//切换老友圈界面弹出
		RequestTeaHouse();
		Panel_ClubList->setVisible(true);
		Panel_ClubList->stopAllActions();
		Panel_ClubList->runAction(MoveTo::create(0.3, Vec2(1280, 0)));
	});

	//修改楼层
	auto Button_modify = (Button*)Panel_ClubInfo->getChildByName("Button_modify");
	Button_modify->addClickEventListener([=](Ref*){
		//切换楼层界面弹出
		Panel_FloorSwitch->setVisible(true);
		Panel_FloorSwitch->stopAllActions();
		Panel_FloorSwitch->runAction(MoveTo::create(0.3, Vec2(1280, 0)));
	});

	//在线人数
	Text_online_num = (Text*)Panel_ClubInfo->getChildByName("Text_online_num");

	//退出界面
	auto Button_leave = (Button*)Panel_ClubInfo->getChildByName("Button_leave");
	Button_leave->addClickEventListener([=](Ref*){
		//茶楼界面退出
		ClubID = 0;
		//退出茶楼返回大厅发送退出茶楼消息
		MSG_GP_I_DissmissClub Request;
		Request.iClubID = ClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LeaveHall, &Request, sizeof(MSG_GP_I_DissmissClub));
		HNPlatformClubData::getInstance()->RemoveNetEventListener();

		GamePlatform::createNewPlatform();
	});

	//花数目显示
	Image_flow = (ImageView*)Panel_ClubInfo->getChildByName("Image_flow");
	Text_flow_num = (Text*)Panel_ClubInfo->getChildByName("Text_flow_num");

	//当前楼层显示
	Text_tower = (Text*)Panel_ClubInfo->getChildByName("Text_tower");

	//向上切换楼层
	auto Button_up_tower = (Button*)Panel_ClubInfo->getChildByName("Button_up_tower");
	Button_up_tower->addClickEventListener([=](Ref*){
		if (m_MaxTeaHouseIndex > 0)
		{
			m_SelectedTeaHouse++;
			if (m_SelectedTeaHouse > m_MaxTeaHouseIndex)
			{
				m_SelectedTeaHouse = 0;
			}
			//更新当前楼层对应的玩法数据
			if (m_SelectedTeaHouse != 0)
			{
				vector<SClubTeahouseInfo> vec = HNPlatformClubData::getInstance()->getTeaHouseInfo(ClubID);
				if (!vec.empty()) {
					Max_Player = vec[m_SelectedTeaHouse - 1].maxPlayerCount;
					Max_palycount = vec[m_SelectedTeaHouse - 1].maxGames;
					m_TeaHouseID = vec[m_SelectedTeaHouse - 1].teahouseId;
					_roomID = vec[m_SelectedTeaHouse - 1].roomId;
					_GameID = vec[m_SelectedTeaHouse - 1].gameNameId;
				}
			}
			
			SwitchTeaHouseFloor(m_SelectedTeaHouse);
		}
	});

	//向下切换楼层
	auto Button_down_tower = (Button*)Panel_ClubInfo->getChildByName("Button_down_tower");
	Button_down_tower->addClickEventListener([=](Ref*){
		if (m_MaxTeaHouseIndex > 0)
		{
			m_SelectedTeaHouse--;
			if (m_SelectedTeaHouse < 0)
			{
				m_SelectedTeaHouse = m_MaxTeaHouseIndex;	//加1层大厅
			}
			//更新当前楼层对应的玩法数据
			if (m_SelectedTeaHouse != 0)
			{
				vector<SClubTeahouseInfo> vec = HNPlatformClubData::getInstance()->getTeaHouseInfo(ClubID);
				if (!vec.empty()) {
					Max_Player = vec[m_SelectedTeaHouse - 1].maxPlayerCount;
					Max_palycount = vec[m_SelectedTeaHouse - 1].maxGames;
					m_TeaHouseID = vec[m_SelectedTeaHouse - 1].teahouseId;
					_roomID = vec[m_SelectedTeaHouse - 1].roomId;
					_GameID = vec[m_SelectedTeaHouse - 1].gameNameId;
				}
			}
			
			SwitchTeaHouseFloor(m_SelectedTeaHouse);
		}
	});

	//红花记录
	auto Button_Red_flower = (Button*)Panel_ClubInfo->getChildByName("Button_Red_flower");
	Button_Red_flower->addClickEventListener([=](Ref*){
		ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
		ClubRedFlow->SetClubID(ClubID);
		ClubRedFlow->initCSBByType(1, Result.dwUserID, isManage);
		ClubRedFlow->setName("ClubRedFlow");
		this->addChild(ClubRedFlow);
	});

	//待处理红花
	auto Button_flower_pending = (Button*)Panel_ClubInfo->getChildByName("Button_flower_pending");
	Button_flower_pending->addClickEventListener([=](Ref*){
		ClubUserRedFlow* ClubRedFlow = ClubUserRedFlow::create();
		ClubRedFlow->SetClubID(ClubID);
		bool isShow = false;
		if (isManage == 1 || isManage == 2)
		{
			isShow = true;
		}
		ClubRedFlow->initCSBByType(2, Result.dwUserID, isShow);
		ClubRedFlow->setName("ClubRedFlow");
		this->addChild(ClubRedFlow);
	});

	//刷新
	auto Button_Refresh = (Button*)Panel_ClubInfo->getChildByName("Button_Refresh");
	Button_Refresh->addClickEventListener([=](Ref*){
		Button_Refresh->setEnabled(false);
		//更新茶楼楼层
		UpdateTeaHouseView();
		//更新自己权限以及红花数
		RequestTeaHouseUser();
		Button_Refresh->runAction(Sequence::create(DelayTime::create(5.0f), CallFunc::create([=]() {
			Button_Refresh->setEnabled(true);
		}), nullptr));
	});

	//本群战绩
	auto Button_Record = (Button*)Panel_ClubInfo->getChildByName("Button_Record");
	Button_Record->addClickEventListener([=](Ref*){
		bool isManage_T = false;
		if (isManage && isManage < 3)
		{
			isManage_T = true;
		}
		auto record = GameRoomRecordClub::create(isManage_T,ClubID);
		record->setName("record");
		this->addChild(record);
	});

	//本群分享
	auto Button_Share = (Button*)Panel_ClubInfo->getChildByName("Button_Share");
	Button_Share->addClickEventListener([=](Ref*){
		auto shareLayer = GameShareLayer::create();
		shareLayer->setName("shareLayer");
		shareLayer->SetShareCallBack([=](bool success, int platform, const std::string& errorMsg) {

			std::string str;
			if (success)str = GBKToUtf8("分享成功");
			else
			{
				std::string str;
				if (str.empty()) {
					str = GBKToUtf8("分享失败");
				}
				else {
					str = errorMsg;
				}
			}
			runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=]() {
				GamePromptLayer::create()->showPrompt(str);
			}), nullptr));
		});
		// 设置分享链接
		std::string shareUrl = HNPlatformConfig()->getShareUrl();
		// 设置分享标题
		std::string shareTitile = StringUtils::format(GBKToUtf8("老友圈ID：%d"), ClubID);
		// 设置分享内容
		std::string shareContent = StringUtils::format(GBKToUtf8("老友圈ID：%d，请在游戏大厅中打开老友圈，输入老友圈ID，加入老友圈吧！！"), ClubID);
		shareLayer->SetShareInfo(shareContent, shareTitile, shareUrl);
		shareLayer->show();
	});

	//茶楼桌子
	ListView_Table = (ListView*)Panel_ClubInfo->getChildByName("ListView_Table");
	ListView_Table->setAnchorPoint(Vec2(0, 1));
	ListView_Table->setScrollBarEnabled(false);

	//快速游戏
	auto Button_quick_game = (Button*)Panel_ClubInfo->getChildByName("Button_quick_game");
	Button_quick_game->addClickEventListener([=](Ref*){
		auto Rest = HNPlatformClubData::getInstance()->getTeaHouseInfoByState(ClubID);
		if (Rest)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("当前茶楼已暂停开房，请联系管理开启。"));
		}
		else
		{
			m_bFastJoin = true;
			auto update = GameResUpdate::create();
			update->retain();
			update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {
				if (success)
				{
					GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr) {
						if (success)
						{
							//RoomLogic()->close();
							if (RoomLogic()->isConnect())
							{
								_roomLogic->stop();
								RoomLogic()->close();
							}
							_roomLogic->start();
							_roomLogic->requestLogin(_roomID, latitude, longtitude, addr);
						}
						else
						{//不强制开启定位
							if (RoomLogic()->isConnect())
							{
								_roomLogic->stop();
								RoomLogic()->setConnect(false);
							}
							_roomLogic->start();
							_roomLogic->requestLogin(_roomID);
						}
					};
					GameLocation::getInstance()->getLocation();
				}
				update->release();
			};
			update->checkUpdate(_GameID, true);
		}
	});

	//公告按钮
	Button_notice = (Button*)Panel_ClubInfo->getChildByName("Button_notice");
	Button_notice->addClickEventListener([=](Ref*){
		//弹出公告弹窗
		//(群主管理能修改群公告)
		if (isManage && isManage < 3)
		{
			ClubOperate* ClubOperateLayer = ClubOperate::create();
			this->addChild(ClubOperateLayer);
			ClubOperateLayer->SetPlaceHolderCount("请输入公告内容...");
			ClubOperateLayer->SetImageViewRes(5);
			auto CallBack = [=](){
				MSG_GP_I_Club_Notice  Request;
				Request.iClubID = ClubID;
				sprintf(Request.szClubNotice, "%s", Utf8ToGB(TextCount.c_str()));
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE, &Request, sizeof(MSG_GP_I_Club_Notice), HN_SOCKET_CALLBACK(Club::getReleaseNoticeResult, this));
			};
			ClubOperateLayer->SetCallBack([=](){
				CallBack();
			});
		}
	});
	Button_notice->setVisible(false);
	
	auto Panel_5 = (Layout*)Button_notice->getChildByName("Panel_5");
	Text_notice = (Text*)Panel_5->getChildByName("Text_10");

	//-------------切换楼层---------------//
	Panel_FloorSwitch = (Layout*)TeaHouseNode->getChildByName("Panel_FloorSwitch");
	Panel_FloorSwitch->addClickEventListener([=](Ref*){
		//点击范围之外收起
		Panel_FloorSwitch->runAction(MoveTo::create(0.5, Vec2(0, 0)));
	});

	//关闭
	auto Button_close = (Button*)Panel_FloorSwitch->getChildByName("Button_close");
	Button_close->addClickEventListener([=](Ref*){
		Panel_FloorSwitch->runAction(MoveTo::create(0.5, Vec2(0, 0)));
	});

	//切换是否显示大厅楼层
	auto CheckBox_lobby = (CheckBox*)Panel_FloorSwitch->getChildByName("CheckBox_lobby");
	CheckBox_lobby->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		CheckBox* checkBox = dynamic_cast<CheckBox*>(ref);
		if (checkBox)
		{
			if (checkBox->isSelected())
			{
				//切换展开大厅模式
				log("open");
			}
			else
			{
				//切换关闭大厅模式
				log("close");
			}
		}
	});

	//切换楼层列表
	ListView_FloorList = (ListView*)Panel_FloorSwitch->getChildByName("ListView_FloorList");
	ListView_FloorList->setScrollBarEnabled(false);
	//---------------创建加入茶楼-------------------//
	Panel_ClubList = (Layout*)TeaHouseNode->getChildByName("Panel_ClubList");

	//关闭当前茶楼列表
	auto Button_close_c = (Button*)Panel_ClubList->getChildByName("Button_close");
	Button_close_c->addClickEventListener([=](Ref*){
		if (m_bHaveTeaHouse)
		{
			Panel_ClubList->setVisible(false);
			Panel_ClubList->stopAllActions();
			Panel_ClubList->runAction(MoveTo::create(0.5, Vec2(0, 0)));
		}
		else
		{
			ClubID = 0;
			GamePlatform::createNewPlatform();
		}
	});

	//当前茶楼列表
	ListView_club = (ListView*)Panel_ClubList->getChildByName("ListView_club");

	//创建茶楼
	auto Button_create = (Button*)Panel_ClubList->getChildByName("Button_create");
	Button_create->addClickEventListener([=](Ref*){
		//查询创建权限
		//SendQueryTeaHouseAccess();

		//测试无需权限判断
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入茶楼名称...");
		ClubOperateLayer->SetImageViewRes(1);
		auto Callback = [=](){
			MSG_GP_I_CreateClub  name;
			sprintf(name.szClubName, "%s", Utf8ToGB(TextCount.c_str()));
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CREATE_CLUB, &name, sizeof(MSG_GP_I_CreateClub), HN_SOCKET_CALLBACK(Club::CreateClubResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			Callback();
		});

	});

	//加入茶楼
	auto Button_join = (Button*)Panel_ClubList->getChildByName("Button_join");
	Button_join->addClickEventListener([=](Ref*){
		//加入茶楼输入ID界面
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入茶楼ID...");
		ClubOperateLayer->SetImageViewRes(2);
		auto Callback = [=](){
			MSG_GP_I_JoinClub ID;
			ID.iClubID = std::atoi(TextCount.c_str());
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_JOIN_CLUB, &ID, sizeof(MSG_GP_I_JoinClub), HN_SOCKET_CALLBACK(Club::JoinClubResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			Callback();
		});
	});

	//--------------------房间信息------------------------//
	Panel_TeaHouseInfo = (Layout*)TeaHouseNode->getChildByName("Panel_TeaHouseInfo");
	Panel_TeaHouseInfo->setVisible(false);
	Panel_TeaHouseInfo->addClickEventListener([=](Ref*){
		Panel_TeaHouseInfo->setVisible(false);
	});

	auto TeaHouseInfo_close = (Button*)Panel_TeaHouseInfo->getChildByName("Button_close");
	TeaHouseInfo_close->addClickEventListener([=](Ref*){
		Panel_TeaHouseInfo->setVisible(false);
	});

	//查询茶楼列表
	RequestTeaHouse();

	//初始化完成回调检测
	HNPlatformClubData::getInstance()->onRequestCallBack = [=](){
		//更新茶楼楼层
		UpdateTeaHouseView();
		//更新茶楼桌子
		//UpdateTeaHouseTable();
	};

	//设置解散楼层回调
	HNPlatformClubData::getInstance()->onDissolveCallBack = [=](int TeaHouseID){
		if (TeaHouseID = m_TeaHouseID)
		{
			initLobbyTeaHouse();
		}
		else
		{
			m_SelectedTeaHouse = 0;
			Text_tower->setString(GetTeaHouseName(m_SelectedTeaHouse));
			UpdateTeaHouseView();
		}
	};

	//通知用户黑名单回调
	HNPlatformClubData::getInstance()->onBlackListUserCallBack = [=](int callClubID){
		if (ClubID == callClubID)
		{
			ClubID = 0;
			RequestTeaHouse();
		}
	};
}

//查询茶楼列表
void Club::RequestTeaHouse()
{
	if (MyCreate.size() != 0)MyCreate.clear();
	if (MyJoin.size() != 0)MyJoin.clear();
	int Userid = Result.dwUserID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LIST, &Userid, sizeof(Userid), HN_SOCKET_CALLBACK(Club::getTeaHouseResult, this));
}

//发送拉取俱乐部列表请求
void Club::RequestClubView()
{
	//消息容器
	if (MyCreate.size() != 0)MyCreate.clear();
	if (MyJoin.size() != 0)MyJoin.clear();
	int Userid = Result.dwUserID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LIST, &Userid, sizeof(Userid), HN_SOCKET_CALLBACK(Club::getClubViewResult, this));
}

//添加俱乐部网络消息监听事件
void Club::AddClubNetWorkNewsEventListener()
{
	//监听是否被踢
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_KICKUSER_TAR, HN_SOCKET_CALLBACK(Club::IsSelfAtClub, this));
	//监听公告消息
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE_UPDATE, HN_SOCKET_CALLBACK(Club::ClubNoticeChange, this));
	//监听充值俱乐部房卡变化
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUBJEWELSCHANGE, HN_SOCKET_CALLBACK(Club::ClubRoomCardChange, this));
	//俱乐部解散监听
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_DISSMISS_CLUB_NOTIFY, HN_SOCKET_CALLBACK(Club::ClubDisbandEventListener, this));
	//监听是否有用户申请加入俱乐部
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_JOIN_CLUB_TOMASTER, HN_SOCKET_CALLBACK(Club::HaveNewUserJoinClub, this));
	//成员加入与退出变动事件监听
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_USERCHANGE, HN_SOCKET_CALLBACK(Club::ClubMemberChange, this));
	//俱乐部房间变化监听
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_ROOMCHANGE, HN_SOCKET_CALLBACK(Club::RoomEventListener, this));
	//俱乐部房间用户更新监听（加入或者退出）
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_ON_USER_LOGIN_ROOM, HN_SOCKET_CALLBACK(Club::RoomEventListener, this));
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_ON_USER_QUIT_ROOM, HN_SOCKET_CALLBACK(Club::RoomEventListener, this));
}

//添加茶楼网络消息监听事件
void Club::AddTeaHouseNetWorkEventListener()
{
	//监听是否被踢
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_KICKUSER_TAR, HN_SOCKET_CALLBACK(Club::IsSelfAtClub, this));
	//监听公告消息
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE_UPDATE, HN_SOCKET_CALLBACK(Club::ClubNoticeChange, this));
	//俱乐部解散监听
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_DISSMISS_CLUB_NOTIFY, HN_SOCKET_CALLBACK(Club::ClubDisbandEventListener, this));
	//监听是否有用户申请加入俱乐部
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_JOIN_CLUB_TOMASTER, HN_SOCKET_CALLBACK(Club::HaveNewUserJoinClub, this));
	//成员加入与退出变动事件监听
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_USERCHANGE, HN_SOCKET_CALLBACK(Club::ClubMemberChange, this));
}

//移除茶楼网络消息监听事件
void Club::RemoveNetWorkNewsEventListener()
{
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_KICKUSER_TAR);//移除监听被踢消息
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE_UPDATE);//移除监听公告消息
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_USERCHANGE);//移除成员加入与退出变动事件监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_DISSMISS_CLUB_NOTIFY);//移除解散俱乐部监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_JOIN_CLUB_TOMASTER);//移除加入或退出俱乐部监听
}

//移除所有网络消息监听事件
void Club::RemoveAllNetWorkNewsEventListener()
{
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_KICKUSER_TAR);//移除监听被踢消息
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE_UPDATE);//移除监听公告消息
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUBJEWELSCHANGE);//移除俱乐部充值房卡数量监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_DISSMISS_CLUB_NOTIFY);//移除解散俱乐部监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_JOIN_CLUB_TOMASTER);//移除加入或退出俱乐部监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_USERCHANGE);//移除俱乐部成员变动监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_ROOMCHANGE);//移除俱乐部房间变动监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_ON_USER_LOGIN_ROOM);//移除用户加入房间监听
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_ON_USER_QUIT_ROOM);//移除用户离开房间监听
}

void Club::BtnCallBack(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	auto Btn = (Button*)pSender;

	std::string name = Btn->getName();

	if (name.compare("Button_create") == 0)
	{
		_setView->setVisible(false);
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入茶楼名称...");
		ClubOperateLayer->SetImageViewRes(1);
		auto Callback = [=](){
			MSG_GP_I_CreateClub  name;
			sprintf(name.szClubName, "%s", Utf8ToGB(TextCount.c_str()));
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CREATE_CLUB, &name, sizeof(MSG_GP_I_CreateClub), HN_SOCKET_CALLBACK(Club::CreateClubResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			Callback();
		});
	}
	else if (name.compare("Button_join") == 0)
	{
		_setView->setVisible(false);
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入茶楼ID...");
		ClubOperateLayer->SetImageViewRes(2);
		auto Callback = [=](){
			MSG_GP_I_JoinClub ID;
			ID.iClubID = std::atoi(TextCount.c_str());
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_JOIN_CLUB, &ID, sizeof(MSG_GP_I_JoinClub), HN_SOCKET_CALLBACK(Club::JoinClubResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			Callback();
		});
	}
	else if (name.compare("Button_info") == 0)
	{
		_setView->setVisible(false);
		ClubMsg* ClubMsgLayer = ClubMsg::create();
		ClubMsgLayer->setName("ClubMsgLayer");
		this->addChild(ClubMsgLayer);
		ClubMsgLayer->RequestClubMessage(ClubID);
		Image_red->setVisible(false);
	}
	else if (name.compare("Button_add") == 0)
	{
		_setView->setVisible(false);
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("输入充值数量（50张起充）");
		ClubOperateLayer->SetImageViewRes(3);
		auto CallBack = [=](){
			if (stoi(TextCount) < 50)
			{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("茶楼房卡最低50起充"));
			}
			else
			{
				MSG_GP_I_SetClubJewels  Request;
				Request.iClubID = ClubID;
				Request.iJewels = stoi(TextCount.c_str());
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_SET_CLUBJEWELS, &Request, sizeof(MSG_GP_I_SetClubJewels), HN_SOCKET_CALLBACK(Club::onSendAddClubRoomCardResult, this));
			}
		};
		ClubOperateLayer->SetCallBack([=](){
			CallBack();
		});
	}
	else if (name.compare("Room_Create") == 0)
	{
		//隐藏金币房卡房间选项
		GameCreator()->SetCurrentselectSine(1);
		_setView->setVisible(false);
		auto create = CreateRoomLayer::create(1);
		create->setPosition(Vec2::ZERO);
		//create->createGameList(1);
		this->addChild(create);
		GameCreator()->SetCurrentselectSine(0);
	}
	else if (name.compare("Button_back") == 0)
	{
		ClubID = 0;
		_setView->setVisible(false);
		GamePlatform::createNewPlatform();
	}
	else if (name.compare("Button_close") == 0)
	{
		ClubID = 0;
		_setView->setVisible(false);
		GamePlatform::createNewPlatform();
	}
	else if (name.compare("Button_set") == 0)
	{
		if (_setView->isVisible()) _setView->setVisible(false);
		else  _setView->setVisible(true);
	}
	else if (name.compare("Club_disband") == 0)
	{
		_setView->setVisible(false);
		if (isClubCreater)
		{
			auto SendDisbandClub = [=](){
				MSG_GP_I_DissmissClub Request;
				Request.iClubID = ClubID;
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_DISSMISS_CLUB, &Request, sizeof(MSG_GP_I_DissmissClub), HN_SOCKET_CALLBACK(Club::ClubDisbandResult, this));
			};
			ClubTips* ClubTipsLayer = ClubTips::create();
			this->addChild(ClubTipsLayer);
			ClubTipsLayer->setTextCount(GBKToUtf8("您是否要解散茶楼?"));
			ClubTipsLayer->SetImageViewRes(2);
			ClubTipsLayer->setCallBack([=](){
				SendDisbandClub();
			});
		}
		else
		{
			auto SendExitClub = [=](){
				MSG_GP_I_LeaveClub Request;
				Request.iClubID = ClubID;
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_LEAVE_CLUB, &Request, sizeof(MSG_GP_I_LeaveClub), HN_SOCKET_CALLBACK(Club::getExitClubResult, this));
			};
			ClubTips* ClubTipsLayer = ClubTips::create();
			this->addChild(ClubTipsLayer);
			ClubTipsLayer->setTextCount(GBKToUtf8("您是否要退出茶楼?"));
			ClubTipsLayer->SetImageViewRes(3);
			ClubTipsLayer->setCallBack([=](){
				SendExitClub();
			});
		}
	}
	else if (name.compare("Club_changeName") == 0)
	{
		_setView->setVisible(false);
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入需要修改的名称...");
		ClubOperateLayer->SetImageViewRes(4);
		auto CallBack = [=](){
			MSG_GP_I_ChangeName Name;
			Name.iClubID = ClubID;
			sprintf(Name.szNewClubName, "%s", Utf8ToGB(TextCount.c_str()));
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_CHANGENAME, &Name, sizeof(MSG_GP_I_ChangeName), HN_SOCKET_CALLBACK(Club::getChangeNameResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			CallBack();
		});
	}
	else if (name.compare("Button_member") == 0)
	{
		_setView->setVisible(false);
		ClubMember* MemberLayer = ClubMember::create();
		this->addChild(MemberLayer);
	}
	else if (name.compare("Club_notice") == 0)
	{
		_setView->setVisible(false);
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入发布公告内容...");
		ClubOperateLayer->SetImageViewRes(5);
		auto CallBack = [=](){
			MSG_GP_I_Club_Notice  Request;
			Request.iClubID = ClubID;
			sprintf(Request.szClubNotice, "%s", Utf8ToGB(TextCount.c_str()));
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE, &Request, sizeof(MSG_GP_I_Club_Notice), HN_SOCKET_CALLBACK(Club::getReleaseNoticeResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			CallBack();
		});
	}
	else if (name.compare("Room_Refresh") == 0)
	{
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入6位房间号...");
		ClubOperateLayer->SetImageViewRes(6);
		auto CallBack = [=](){
			char Password[20];
			sprintf(Password, "%s", Utf8ToGB(TextCount.c_str()));
			Reconnection::getInstance()->doLoginVipRoom(Password);
		};
		ClubOperateLayer->SetCallBack([=](){
			CallBack();
		});
	}
	else if (name.compare("Button_invite") == 0)
	{
		auto shareLayer = GameShareLayer::create();
		shareLayer->setName("shareLayer");
		shareLayer->SetShareCallBack([=](bool success, int platform, const std::string& errorMsg) {

			std::string str;
			if (success)str = GBKToUtf8("分享成功");
			else
			{
				std::string str;
				if (str.empty()) {
					str = GBKToUtf8("分享失败");
				}
				else {
					str = errorMsg;
				}
			}
			runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=]() {
				GamePromptLayer::create()->showPrompt(str);
			}), nullptr));
		});
		// 设置分享链接
		std::string shareUrl = HNPlatformConfig()->getShareUrl();
		// 设置分享标题
		std::string shareTitile = StringUtils::format(GBKToUtf8("老友圈ID：%d"), ClubID);
		// 设置分享内容
		std::string shareContent = StringUtils::format(GBKToUtf8("老友圈ID：%d，请在游戏大厅中打开老友圈，输入老友圈ID，加入老友圈吧！！"), ClubID);
		shareLayer->SetShareInfo(shareContent, shareTitile, shareUrl);
		shareLayer->show();
	}
	else if (name.compare("Club_Record") ==0)
	{
		//auto record = GameRoomRecord::create(ClubID);
		auto record = GameRoomRecordClub::create(ClubID);
		record->setName("record");
		this->addChild(record);
	}
	else if (name.compare("Image_mycreate") == 0)
	{
		SelectMyCreateOrJoinClub(0);
	}
	else if (name.compare("Image_myjoin") == 0)
	{
		SelectMyCreateOrJoinClub(1);
	}
	else if (name.compare("Button_newclub") == 0)
	{
		if (CreateorJoin.compare("Create") == 0)
		{
			ClubOperate* ClubOperateLayer = ClubOperate::create();
			this->addChild(ClubOperateLayer);
			ClubOperateLayer->SetPlaceHolderCount("请输入茶楼名称...");
			ClubOperateLayer->SetImageViewRes(1);
			auto Callback = [=](){
				MSG_GP_I_CreateClub  name;
				sprintf(name.szClubName, "%s", Utf8ToGB(TextCount.c_str()));
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CREATE_CLUB, &name, sizeof(MSG_GP_I_CreateClub), HN_SOCKET_CALLBACK(Club::CreateClubResult, this));
			};
			ClubOperateLayer->SetCallBack([=](){
				Callback();
			});
		}
		else if (CreateorJoin.compare("Join") == 0)
		{
			ClubOperate* ClubOperateLayer = ClubOperate::create();
			this->addChild(ClubOperateLayer);
			ClubOperateLayer->SetPlaceHolderCount("请输入茶楼ID...");
			ClubOperateLayer->SetImageViewRes(2);
			auto Callback = [=](){
				MSG_GP_I_JoinClub ID;
				ID.iClubID = std::atoi(TextCount.c_str());
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_JOIN_CLUB, &ID, sizeof(MSG_GP_I_JoinClub), HN_SOCKET_CALLBACK(Club::JoinClubResult, this));
			};
			ClubOperateLayer->SetCallBack([=](){
				Callback();
			});
		}
	}
}

//初始化节点
void Club::isHaveClub(bool isShow)
{
	//左边节点
	Node_left->setVisible(isShow);
	//顶部节点
	Node_top->setVisible(isShow);
	//底部节点
	Node_bottom->setVisible(isShow);
	//房间节点
	Node_room->setVisible(isShow);
	//创建与加入节点
	Node_createjoin->setVisible(!(isShow));
	//公告
	auto NoticeLayer = this->getChildByName("NoticeLayer");
	//当不存在俱乐部的时候，如果公告已经被创建那就移除掉（退出/解散时出现）
	if (!isShow)
	{
		if (NoticeLayer) NoticeLayer->removeFromParent();
	}
}
//创建TableView
void Club::CreateClubView()
{
	//创建TableVIew
	_TableView = TableView::create(this, Size(262, 500));
	//设置滚动方向
	_TableView->setDirection(extension::ScrollView::Direction::VERTICAL);
	//设置cell排序方式
	_TableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//设置数据源
	_TableView->setDelegate(this);
	_TableView->setZOrder(5);
	//加入到Layout中
	_ClubLayout->addChild(_TableView);
}

//创建房间层
void Club::createRoomLayer()
{
	//房间层级丢弃处理（ssj_2019.9.27转列表容器显示,方便实时更新用户数据）
	auto RoomLayer = RoomView->getChildByName("ClubRoomLayer");
	if (RoomLayer)
	{
		RoomLayer->removeFromParent();
		auto ClubRoomLayer = ClubRoom::create();
		ClubRoomLayer->setName("ClubRoomLayer");
		ClubRoomLayer->setPosition(Vec2(0, 0));
		RoomView->addChild(ClubRoomLayer);
	}
	else
	{
		auto ClubRoomLayer = ClubRoom::create();
		ClubRoomLayer->setName("ClubRoomLayer");
		ClubRoomLayer->setPosition(Vec2(0, 0));
		RoomView->addChild(ClubRoomLayer);
	}
}

//首次获取俱乐部房间列表信息
void Club::SendRequestClubRoomView()
{
	//请求获取俱乐部房间列表
	MSG_GP_I_Club_RoomList SendID;
	SendID.iClubID = ClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_JJ_CLUB_ROOMLIST, &SendID, sizeof(MSG_GP_I_Club_RoomList), HN_SOCKET_CALLBACK(Club::getRoomViewResult, this));
}

//俱乐部房间信息
bool Club::getRoomViewResult(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{
		if (RoomInfo.size() != 0) RoomInfo.clear();
		auto Head = (MSG_GP_O_Club_RoomList_Head*)SocketMessage->object;
		INT RoomNum = (SocketMessage->objectSize - sizeof(MSG_GP_O_Club_RoomList_Head)) / sizeof(MSG_GP_O_JJClub_RoomList_Data);
		ClubRooms = RoomNum;
		auto Data = (MSG_GP_O_JJClub_RoomList_Data*)(SocketMessage->object + sizeof(MSG_GP_O_Club_RoomList_Head));

		if (RoomNum)
		{
			while (RoomNum--> 0)
			{
				RoomInfo.push_back(*Data);
				Data++;
			}
			UpdateRoomList();
			/*if (_RoomView != nullptr)_RoomView->reloadData();
			else createRoomView();*/
		}
		else
		{
			if (RoomListView->getCurSelectedIndex() != -1)
			{
				RoomListView->removeAllItems();
			}
			//获取房间列表为空
			//if (_RoomView != nullptr)_RoomView->reloadData();
		}

	}
	return true;
}

//左侧俱乐部列表被选中回调
void Club::SelectMyCreateOrJoinClub(int num)
{
	if (num == 0)
	{
		CreateorJoin = "Create";
		
		//Image_left->loadTexture("platform/Club/res/create_di.png");
		//Image_di->setPosition(_myjoin->getPosition());
		_mycretae->setOpacity(255);
		_myjoin->setOpacity(0);
		bottom_create->loadTexturePressed("platform/Club/res/new/chuangjianbuluo.png");
		bottom_create->loadTextureNormal("platform/Club/res/new/chuangjianbuluo.png");
		if (_TableView != nullptr) _TableView->reloadData();
	}
	else
	{
		CreateorJoin = "Join";
		//Image_left->loadTexture("platform/Club/res/join_di.png");
		//Image_di->setPosition(_mycretae->getPosition());
		_mycretae->setOpacity(0); 
		_myjoin->setOpacity(255);
		bottom_create->loadTexturePressed("platform/Club/res/new/jiarubuluo.png");
		bottom_create->loadTextureNormal("platform/Club/res/new/jiarubuluo.png");
		if (_TableView != nullptr) _TableView->reloadData();
	}
}

//顶部俱乐部消息显示
void Club::SetTopClubInfo(vector<MSG_GP_O_Club_List>Clubinfo, int idx)
{
	//俱乐部名称
	auto _ClubName = dynamic_cast<Text*>(Node_top->getChildByName("Club_name"));
	//俱乐部ID
	auto _Club_ID = dynamic_cast<Text*>(Node_top->getChildByName("Club_id"));
	//设置名字
	_ClubName->setString(GBKToUtf8(Clubinfo[idx].szClubName));
	//俱乐部ID
	_Club_ID->setString(GBKToUtf8("茶楼ID:" + StringUtils::toString(Clubinfo[idx].iClubID)));

	//全局变量判断创建的是普通房间还是俱乐部房间
	ClubID = Clubinfo[idx].iClubID;
	//是否是俱乐部创始人（默认一个俱乐部）
	isClubCreater = Clubinfo[idx].bCreater;
	//根据用户是否是俱乐部创始人来加载不同的解散或退出俱乐部图片资源
	if (isClubCreater){
		btn_disband->loadTexture("platform/Club/res/new/jiesanbuluo.png");
		score_bg->setVisible(true);
	}
	else { btn_disband->loadTexture("platform/Club/res/new/tuichubuluo.png"); 
	/*score_bg->setVisible(false);*/ }
	//创建房间层
	createRoomLayer();
	//SendRequestClubRoomView();
	//添加俱乐部监听事件
	AddClubNetWorkNewsEventListener();
	updateClubScore();
	updateClubJewels();

	//更新当前俱乐部成员用户头像信息
	HNPlatformClubData::getInstance()->onQueryClubUserList(ClubID);
}

//查询俱乐部申请加入总人数
void Club::SendQueryJoinClubCount(int Clubid)
{
	MSG_GP_I_Club_ReviewList ID;
	ID.iClubID = Clubid;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_REVIEW_LIST, &ID, sizeof(MSG_GP_I_Club_ReviewList), HN_SOCKET_CALLBACK(Club::GetJoinClubCount, this));
}

void Club::SendQueryTeaHouseAccess()
{
	BYTE a = 0;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_GetCreateAccess, &a, sizeof(BYTE), HN_SOCKET_CALLBACK(Club::GetTeaHouseAccess, this));
}

void Club::updateClubNotice()
{
	//进入俱乐部开始接收公告消息
	MSG_GP_I_Club_EnterClub ID;
	ID.iClubID = ClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_ENTER_CLUB, &ID, sizeof(MSG_GP_I_Club_EnterClub), HN_SOCKET_CALLBACK(Club::getClubInfoResult, this));
}

void Club::RequestTeaHouseUser()
{
	SClubRequestBase  Data;
	Data.gameNameId = 0;
	Data.msg = ClubRequest_QueryUserInfo;
	Data.clubId = ClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequestBase), HN_SOCKET_CALLBACK(Club::getTeaHouseUserResult, this));
}



void Club::updateClubScore()
{
	//进入俱乐部开始接收公告消息
	MSG_GP_I_Club_EnterClub ID;
	ID.iClubID = ClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_ENTER_CLUB, &ID, sizeof(MSG_GP_I_Club_EnterClub), HN_SOCKET_CALLBACK(Club::getClubInfoResult, this));
}

void Club::updateClubJewels()
{
	//俱乐部钻石
	MSG_GP_I_GetClubJewels ID;
	ID.iClubID = ClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_GET_CLUBJEWELS, &ID, sizeof(MSG_GP_I_GetClubJewels), HN_SOCKET_CALLBACK(Club::getClubJewels, this));

}

//获取茶楼列表结果
bool Club::getTeaHouseResult(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		if (socketMessage->messageHead.bDataRule == 1)
		{
			//整个数据包长度除于单个结构体长度得出总的数量
			int num = socketMessage->objectSize / sizeof(MSG_GP_O_Club_List);

			//获取第一个结构体数据
			MSG_GP_O_Club_List* data = (MSG_GP_O_Club_List*)socketMessage->object;
			//茶楼总数
			Image_bg->loadTexture(HAVE_CLUB);
			m_bHaveTeaHouse = true;
			while (num-- > 0)
			{
				//区分我的创建和加入
				if (data->bCreater)MyCreate.push_back(*data);
				else MyJoin.push_back(*data);
				//获取下一个数据地址
				data++;
			}
			HaveSelected = true;
			//更新当前选择的茶楼信息
			updateTeaHouseList();
		}
		else if (socketMessage->messageHead.bDataRule == 0)
		{
			//查询到没有俱乐部信息
			Image_bg->loadTexture(NO_CLUB);
			if (Panel_ClubInfo != nullptr) Panel_ClubInfo->setVisible(false);
			if (Panel_ClubList != nullptr)
			{
				if (ListView_club != nullptr)
				{
					Widget* item = ListView_club->getItem(0);
					if (item != nullptr)
					{
						ListView_club->removeAllItems();
					}
					m_SelectedTeaHouseID = 0;
				}
				Panel_ClubList->setVisible(true);
				Panel_ClubList->setPosition(Vec2(1280, 0));
			}
			m_bHaveTeaHouse = false;
			//查询有俱乐部显示俱乐部信息
			//isHaveClub(false);
			//刷新
			//if (_TableView != nullptr) _TableView->reloadData();
		}
		else
		{
			return true;
		}

	}
	return true;
}

//获取俱乐部信息结果
bool Club::getClubViewResult(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		if (socketMessage->messageHead.bDataRule == 1)
		{
			//整个数据包长度除于单个结构体长度得出总的数量
			int num = socketMessage->objectSize / sizeof(MSG_GP_O_Club_List);
			//获取第一个结构体数据
			MSG_GP_O_Club_List* data = (MSG_GP_O_Club_List*)socketMessage->object;
			//俱乐部总数
			Image_bg->loadTexture(HAVE_CLUB);
			isHaveClub(true);
			while (num-- > 0)
			{
				//区分我的创建和加入
				if (data->bCreater)MyCreate.push_back(*data);
				else MyJoin.push_back(*data);
				//获取下一个数据地址
				data++;
			}
			if (MyCreate.size() == 0 && MyJoin.size() != 0)
			{ 
				SelectMyCreateOrJoinClub(1);
				if (SelectedName.compare("") == 0)SelectedName = "Join";
			}
			else if (MyCreate.size() != 0)
			{ 
				SelectMyCreateOrJoinClub(0); 
				if (SelectedName.compare("") == 0)SelectedName = "Create";
			}
			if (_TableView != nullptr) _TableView->reloadData();
			else CreateClubView();
			HaveSelected = true;
		}
		else if (socketMessage->messageHead.bDataRule == 0)
		{
			//查询到没有俱乐部信息
			Image_bg->loadTexture(NO_CLUB);
			//查询有俱乐部显示俱乐部信息
			isHaveClub(false);
			//刷新
			if (_TableView != nullptr) _TableView->reloadData();
		}
		else
		{
			return true;
		}

	}
	return true;
}



TableViewCell* Club::tableCellAtIndex(TableView* table, ssize_t index)
{
	TableViewCell* Cell = table->dequeueCell();
	Node* Item = nullptr;
	ImageView* Item_bg = nullptr;
	if (!Cell)
	{
		Cell = new TableViewCell();
		Item = CSLoader::createNode(CLUB_ITEM_PATH);
		Item->setName("ClubItem");
		//设置位置
		Item->setPosition(Vec2(-3, 0));
		//添加节点
		Cell->addChild(Item);
		//Item背景
		Item_bg = dynamic_cast<ImageView*>(Item->getChildByName("Club_item"));
		//设置IamgeView可触摸
		Item_bg->setSwallowTouches(false);
		_ClubItemBg.pushBack(Item_bg);
	}
	else
	{
		Item = Cell->getChildByName("ClubItem");
		//Item背景
		Item_bg = dynamic_cast<ImageView*>(Item->getChildByName("Club_item"));
		//设置IamgeView可触摸
		Item_bg->setSwallowTouches(false);
	}
	//成员总数
	auto Members = dynamic_cast<Text*>(Item_bg->getChildByName("Text_member"));
	//房间总数
	auto Rooms = dynamic_cast<Text*>(Item_bg->getChildByName("Text_room"));
	//俱乐部名称
	auto Name = dynamic_cast<Text*>(Item_bg->getChildByName("Text_clubname"));
	//俱乐部ID
	auto Club_ID = dynamic_cast<Text*>(Item_bg->getChildByName("Text_clubid"));
	//俱乐部头像框
	auto HeadFrame = dynamic_cast<ImageView*>(Item_bg->getChildByName("Image_frame"));
	//俱乐部头像底图
	auto Head = dynamic_cast<ImageView*>(Item_bg->getChildByName("Image_head"));
	//创建头像
	auto HeadView = GameUserHead::create(Head, HeadFrame);
	HeadView->show(USER_HEAD_MASK);//添加底图
	HeadView->setSwallowTouches(false);
	Head->setContentSize(Size(80, 80));
	auto SetClubInfo = [=](vector<MSG_GP_O_Club_List>Clubinfo){
		//设置成员数量
		Members->setString(StringUtils::toString(Clubinfo[index].iClubUserNum));
		//设置房间数量
		Rooms->setString(StringUtils::toString(Clubinfo[index].iRoomNum));
		//设置俱乐部名称
		Name->setString(GBKToUtf8(Clubinfo[index].szClubName));
		//设置ID
		Club_ID->setString(GBKToUtf8("茶楼ID:" + StringUtils::toString(Clubinfo[index].iClubID)));
		HeadView->loadTexture(Clubinfo[index].bSex ? USER_MEN_HEAD : USER_WOMEN_HEAD);//判断是男还是女，没有网络头像情况下剩下
		HeadView->loadTextureWithUrl(Clubinfo[index].szHeadURL);//设置网络头像
	};

	if (CreateorJoin.compare("Create") == 0)
	{
		SetClubInfo(MyCreate);
		if (!HaveSelected && index == 0)SetTopClubInfo(MyCreate, 0);
		
	}
	else if (CreateorJoin.compare("Join") == 0)
	{
		SetClubInfo(MyJoin);
		if (!HaveSelected && index == 0)SetTopClubInfo(MyJoin, 0);
		
	}
	//默认没有选中状态
	Item_bg->loadTexture("platform/Club/res/new/wxzl.png");

	if (index == 0 && !HaveSelected)
	{
		Item_bg->loadTexture("platform/Club/res/new/xzl.png");
		Item_bg->setTouchEnabled(false);
	}
	if ((index == SelectedNum) && HaveSelected && (SelectedName.compare(CreateorJoin) == 0))
	{
		Item_bg->loadTexture("platform/Club/res/new/xzl.png");
		Item_bg->setTouchEnabled(false);
	}
	return Cell;
}

//返回 Item 大小
Size Club::tableCellSizeForIndex(TableView* table, ssize_t index)
{
	return Size(330, 130);
}

//触摸函数
void Club::tableCellTouched(TableView* table, TableViewCell* cell)
{
	for (auto iter = _ClubItemBg.begin(); iter != _ClubItemBg.end(); iter++)
	{
		auto _itembg = (ImageView*)(*iter);
		_itembg->loadTexture("platform/Club/res/new/wxzl.png");
	}
	int num = cell->getIdx();
	SelectedNum = num;
	SelectedName = CreateorJoin;
	auto _node = dynamic_cast<Node*>(cell->getChildByName("ClubItem"));
	auto _item = dynamic_cast<ImageView*>(_node->getChildByName("Club_item"));
	_item->loadTexture("platform/Club/res/new/xzl.png");
	HaveSelected = true;
	if (CreateorJoin.compare("Create") == 0)
	{
		SetTopClubInfo(MyCreate, num);
	}
	else if (CreateorJoin.compare("Join") == 0)
	{
		SetTopClubInfo(MyJoin, num);
	}
	SendQueryJoinClubCount(ClubID);
}

//返回Cell总个数
ssize_t Club::numberOfCellsInTableView(TableView* table)
{
	return CreateorJoin.compare("Create") == 0 ? MyCreate.size() : MyJoin.size();
}

//俱乐部公告喇叭
void Club::ClubNoticeShow(std::string &str)
{
	//避免多次创建，如果存在就直接删除
	auto NoticeLayer = this->getChildByName("NoticeLayer");
	//如果公告内容为空的情况下，直接return出去，不做处理
	if (isManage && isManage < 3 && str.compare("") == 0)
	{
		str = "暂无公告                 ";
	}
	else if (str.compare("") == 0 )
	{
		if (NoticeLayer)NoticeLayer->removeFromParent();
		if (Button_notice != nullptr) Button_notice->setVisible(false);
		//str = "测试测试测试。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。";
		return;
	}
	if (NoticeLayer) NoticeLayer->removeFromParent();

	if (ListView_Table == nullptr)
	{
		//创建节点
		auto node = CSLoader::createNode(CLUB_NOTICE_PATH);
		node->setName("NoticeLayer");
		addChild(node);
		//创建动画
		_timeLine = CSLoader::createTimeline(CLUB_NOTICE_PATH);
		node->runAction(_timeLine);
		//开始播放喇叭动画
		_timeLine->play("laba", true);
		auto panel = dynamic_cast<Layout*>(node->getChildByName("Panel_1"));
		auto text_notice = dynamic_cast<Text*>(panel->getChildByName("Text_notice"));
		//设置公告内容
		text_notice->setString(GBKToUtf8(str));
		auto _wordSize = text_notice->getContentSize();
		//文字做左移动作
		text_notice->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.02f, Vec2(-2.0f, 0)), CallFunc::create([=]() {
			if (text_notice->getPositionX() <= -(_wordSize.width + 20))text_notice->setPosition(Vec2(500, 15));
		}), nullptr)));
	}
	else
	{
		Button_notice->setVisible(true);
		Text_notice->setString(GBKToUtf8(str));
		auto _wordSize = Text_notice->getContentSize();
		//文字做左移动作
		Text_notice->stopAllActions();
		Text_notice->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.02f, Vec2(-2.0f, 0)), CallFunc::create([=]() {
			if (Text_notice->getPositionX() <= -(_wordSize.width + 20))Text_notice->setPosition(Vec2(500, 15));
		}), nullptr)));
	}

}

//首次进入俱乐部获取俱乐部公告
bool Club::getClubInfoResult(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_Club_EnterClub* Result = (MSG_GP_O_Club_EnterClub*)SocketMessage->object;
		string Notice = Result->szClubNotice;
		string str = StringUtils::toString(Notice);
		ClubNoticeShow(str);
	}
	return true;
}

//查询红花权限消息结果
bool Club::getTeaHouseUserResult(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		SClubResponseBase* Data = (SClubResponseBase*)SocketMessage->object;
		log("sure======Message===%d", Data->msg);
		if (ClubRequest_QueryUserInfo == Data->msg)
		{
			SClubOperationResponse_QueryUserInfo* Result = (SClubOperationResponse_QueryUserInfo*)SocketMessage->object;
			UINT* Flags = &Result->access;
			if (*Flags & ClubAccess_Admin)
			{
				//管理员
				if (isClubCreater)
				{
					isManage = 1;
				}
				else
				{
					isManage = 2;
				}
			}
			else if (*Flags & ClubAccess_Partner)
			{
				//合作群
				if (isClubCreater)
				{
					isManage = 1;
				}
				else
				{
					isManage = 3;
				}
			}
			if (Text_flow_num)
			{
				if (isManage)
				{
					Image_flow->loadTexture("platform/Club/res/out_game/img_hh_2.png");
					Text_flow_num->setPositionX(244.88);
					Text_flow_num->setString(StringUtils::format("%.2f", Result->redFlowers));
				}
				else
				{
					Image_flow->loadTexture("platform/Club/res/out_game/img_hh.png");
					Text_flow_num->setPositionX(232.77);
					Text_flow_num->setString(to_string((int)Result->redFlowers));
				}
			}

			if (Button_member)
			{
				if (isManage == 3)
				{
					Button_member->setVisible(false);
				}
				else
				{
					Button_member->setVisible(isManage);
				}
			}
			if (Button_teaSet)
			{
				if (isManage == 3)
				{
					Button_teaSet->setVisible(false);
				}
				else
				{
					Button_teaSet->setVisible(isManage);
				}
				//Button_teaSet->setVisible(isManage);
			}
			if (Button_Cooperation)
			{
				if (isManage == 3)
				{
					Button_Cooperation->setVisible(false);
				}
				else
				{
					Button_Cooperation->setVisible(isManage);
				}
				//Button_Cooperation->setVisible(isManage);
			}
			if (Button_Member)
			{
				if (isManage != 3)
				{
					Button_Member->setVisible(false);
				}
				else
				{
					Button_Member->setVisible(isManage);
				}
				//Button_Member->setVisible(isManage);
			}
			return true;
		}
		else if (ClubRequest_QueryOnlineUserCount == Data->msg)
		{
			SClubOperationResponse_QueryOnlineUserCount* Result = (SClubOperationResponse_QueryOnlineUserCount*)SocketMessage->object;
			if (Text_online_num)
			{
				Text_online_num->setString(to_string(Result->count));
			}
			return true;
		}
		else if (ClubRequest_ModifyTeahouseName == Data->msg)
		{
			SClubOperationResponse_ModifyTeahouseName* Result = (SClubOperationResponse_ModifyTeahouseName*)SocketMessage->object;
			//修改楼层名字
			HNPlatformClubData::getInstance()->setTeaHouseNameByTeaHouseID(Result->clubId, Result->teahouseId, Result->szName);
			return true;
		}
	}
	return true;
}

bool Club::getTeaHouseOnlineNumResult(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		SClubOperationResponse_QueryOnlineUserCount* Result = (SClubOperationResponse_QueryOnlineUserCount*)SocketMessage->object;
		Text_online_num->setString(to_string(Result->count));
	}
	return true;
}

bool Club::getClubJewels(HNSocketMessage* SocketMessage) //刷新俱乐部6.3
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_ClubGetJewels* _Result = (MSG_GP_O_ClubGetJewels*)SocketMessage->object;
		score->setString(StringUtils::toString(_Result->iJewels));
	}
	return true;
}

//监听自己是否还在俱乐部中（是否被踢）
bool Club::IsSelfAtClub(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_Club_KickUser* Result = (MSG_GP_O_Club_KickUser*)SocketMessage->object;
		auto UserID = Result->iTargetID;
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("您已被踢出茶楼"));
		HaveSelected = false;
		SelectedNum = 0;
		ClubID = 0;
		//RemoveAllNetWorkNewsEventListener();
		RemoveNetWorkNewsEventListener();
		RequestTeaHouse();
		//RequestClubView();
	}
	return true;
}

//发布公告结果
bool Club::getReleaseNoticeResult(HNSocketMessage* SocketMessage)
{
	switch(SocketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) :
	{
		MSG_GP_O_Club_Notice* Result = (MSG_GP_O_Club_Notice*)SocketMessage->object;
		auto count = Result->szClubNotice;
		string str = StringUtils::toString(count);
		ClubNoticeShow(str);
	}break;
	case(2):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("茶楼ID错误"));
	}break;
	case(3):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("玩家不是会长"));
	}break;
		
	}

	return true;
}
//俱乐部公告监听
bool Club::ClubNoticeChange(HNSocketMessage* SocketMessage)
{
	if (isClubCreater) return true;
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_Club_Notice* Result = (MSG_GP_O_Club_Notice*)SocketMessage->object;
		auto count = Result->szClubNotice;
		string str = StringUtils::toString(count);
		ClubNoticeShow(str);
	}
	return true;
}

//充值俱乐部房卡结果
bool Club::onSendAddClubRoomCardResult(HNSocketMessage* SocketMessage)
{
	switch(SocketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) :
	{
		MSG_GP_O_ClubJewels* Result = (MSG_GP_O_ClubJewels*)SocketMessage->object;
		string str;
		auto count = Result->iJewels;
		str.append(StringUtils::toString(count));
		//房卡数量
		score->setString(str);
		PlatformLogic()->loginResult.iClubJewels = count;
		break;
	}
	case(2) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("茶楼充值房卡不足"));
		break;
	}
	}
	return true;
}

//俱乐部房卡数量变化监听
bool Club::ClubRoomCardChange(HNSocketMessage* SocketMessage)
{
	//if (isClubCreater) return true;
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_ClubJewels* Result = (MSG_GP_O_ClubJewels*)SocketMessage->object;
		string str;
		auto count = Result->iJewels;
		//str.append(StringUtils::toString(count));
		////房卡数量
		//if (score != nullptr)score->setString(str);
	}
	return true;
}

//申请退出俱乐部回调
bool	Club::getExitClubResult(HNSocketMessage* SocketMessage)
{
	switch(SocketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) :
	{
		MSG_GP_O_LeaveClub* ExitResult = (MSG_GP_O_LeaveClub*)SocketMessage->object;
		if (ExitResult->iClubID != 0)
		{
			ClubTips* ClubTipsLayer = ClubTips::create();
			this->addChild(ClubTipsLayer);
			ClubTipsLayer->setTextCount(GBKToUtf8("您已退出茶楼"));
			HaveSelected = false;
			SelectedNum = 0;
			ClubID = 0;
			//RemoveAllNetWorkNewsEventListener();
			RemoveNetWorkNewsEventListener();
			RequestTeaHouse();
			//RequestClubView();
		}
	}break;
	case(2):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("茶楼ID错误"));
	}break;
	case(3):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("玩家未加入该工会"));

	}break;
	case(4):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("有未完成的游戏"));

	}break;
	case(5):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("会长不能退出房间"));

	}break;
	}
	return true;
}

//解散茶楼楼层消息监听
bool Club::ClubDisbandEventListener(HNSocketMessage* SocketMessage)
{
	if (isClubCreater)return true;
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_DissmissClub * Result = (MSG_GP_O_DissmissClub*)SocketMessage->object;
		if (Result->iClubID != 0)
		{
			if (Result->iClubID == ClubID)
			{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("您所在茶楼已被解散"));
				HaveSelected = false;
				SelectedNum = 0;
				ClubID = 0;
				RemoveNetWorkNewsEventListener();
				RequestTeaHouse();
			}
		}
	}
	return true;
}

//解散俱乐部消息回调
bool Club::ClubDisbandResult(HNSocketMessage* SocketMessage)
{
	switch (SocketMessage->messageHead.bHandleCode)
	{
		case(ERR_GP_CLUB_REQUEST_SUCCESS) :
		{
			MSG_GP_O_DissmissClub * Result = (MSG_GP_O_DissmissClub*)SocketMessage->object;
			if (Result->iClubID != 0)
			{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("您已成功解散茶楼"));
				PlatformLogic()->loginResult.iClubJewels = 0;
				HaveSelected = false;
				SelectedNum = 0;
				ClubID = 0;
				//RemoveAllNetWorkNewsEventListener();
				RemoveNetWorkNewsEventListener();
				RequestTeaHouse();
				//RequestClubView();
			}
		}break;
		case(2) :
		{
			ClubTips* ClubTipsLayer = ClubTips::create();
			this->addChild(ClubTipsLayer);
			ClubTipsLayer->setTextCount(GBKToUtf8("茶楼ID错误"));
	
		}break;
		case(3) :
		{
			ClubTips* ClubTipsLayer = ClubTips::create();
			this->addChild(ClubTipsLayer);
			ClubTipsLayer->setTextCount(GBKToUtf8("玩家不是会长"));
		}break;
		case(ERR_GP_CLUB_DeskPlaying) :
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("有正在游戏的桌子"));
			break;
		}
	}
	return true;
}

//新用户申请加入俱乐部监听
bool Club::HaveNewUserJoinClub(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_Club_UserJoin* Result = (MSG_GP_O_Club_UserJoin*)SocketMessage->object;
		//新用户申请加入俱乐部显示处理
		if (Image_Red_Member)
		{
			Image_Red_Member->setVisible(true);
		}
		/*if (Result->iClub == ((SelectedName.compare("Create") == 0) ? MyCreate[SelectedNum].iClubID : MyJoin[SelectedNum].iClubID))
		{
			Image_red->setVisible(true);
		}*/
		
	}
	return true;
}

//创建俱乐部结果
bool Club::CreateClubResult(HNSocketMessage* socketMessage)
{
	DL_O_HALL_CreateClub* Result = (DL_O_HALL_CreateClub*)socketMessage->object;
	switch (socketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) : {
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建成功"));
		//俱乐部创建成功
		//RequestClubView(); 
		//茶楼创建成功
		RequestTeaHouse();
		break; }
	case(ERR_GP_NAME_LIMITE) : {
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！名称中有敏感字")); 
		break; }
	case(3) : 
	{	
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！权限不够")); 
		break;
	}//权限不够
	case(4) : 
	{
	ClubTips* ClubTipsLayer = ClubTips::create();
	this->addChild(ClubTipsLayer);
	ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！超过创建限制"));
	break; 
	}//超过限制
	case(5) : 
	{	
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！超过创建限制")); 
		break; 
	}//加入限制
	case(6) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！名称已被注册"));
		break;
	}
	default:
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败!"));
	}
	}
	
	return true;
}

//加入俱乐部结果
bool Club::JoinClubResult(HNSocketMessage* SocketMessage)
{
	switch(SocketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) :
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("加入成功，请等待会长审核通过"));
		auto callback = [=](){
			MSG_GP_I_DissmissClub Request;
			Request.iClubID = ClubID;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LeaveHall, &Request, sizeof(MSG_GP_I_DissmissClub));
			HNPlatformClubData::getInstance()->RemoveNetEventListener();
			this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
				GamePlatform::createNewPlatform();
			}), nullptr));
		};
		ClubTipsLayer->setCallBack([=](){
			callback();
		});
		ClubTipsLayer->setCancelCallback([=](){
			callback();
		});
		break;
	}
	case(2) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("超过加入茶楼限制"));
		break;
				
	}
	case(3) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("茶楼ID错误"));	
		break;
	}
	case(4):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("茶楼人数超过限制"));
		break;
	}
	case(5):
	{

		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("已被列入黑名单"));
		break;
	}
	case(6):
	{

		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("已经在茶楼内"));
		break;
	}
	case(7) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("请勿重复申请"));
		break;
	}
		
	}
	return true;
}

//修改俱乐部名称结果
bool Club::getChangeNameResult(HNSocketMessage* SocketMessage)
{
	switch (SocketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) :
	{
		MSG_GP_O_ChangeName* Result = (MSG_GP_O_ChangeName*)SocketMessage->object;
		std::string ClubName = Result->szNewClubName;
		//俱乐部名称
		auto _ClubName = dynamic_cast<Text*>(Node_top->getChildByName("Club_name"));
		//设置名字
		_ClubName->setString(GBKToUtf8("茶楼名称:" + ClubName));
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		RequestClubView();
		ClubTipsLayer->setTextCount(GBKToUtf8("修改成功!"));
		break;
	}
	case(2) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("修改失败！名称中有敏感字"));
		break;
	}
	case(3):
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("玩家不是会长"));
		break;
	}
	}
	return true;
}

//删除茶楼楼层结果
bool Club::DeleteTeaHouseResult(HNSocketMessage* SocketMessage)
{
	switch (SocketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) :
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("操作成功"));
		//initLobbyTeaHouse();
		break;
	}
	case(ERR_GP_CLUB_DeskPlaying) :
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("有正在游戏的桌子"));
		break;
	}
	}
	return true;
}

//茶楼解散返回茶楼大厅
void Club::initLobbyTeaHouse()
{
	GamePromptLayer::create()->showPrompt(GBKToUtf8("当前楼层解散"));
	m_SelectedTeaHouse = 0;
	Text_tower->setString(GetTeaHouseName(m_SelectedTeaHouse));
	UpdateTeaHouseView();
}

//成员变动
bool Club::ClubMemberChange(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		RequestTeaHouse();
		//RequestClubView();
	}
	return true;
}

//房间监听
bool Club::RoomEventListener(HNSocketMessage* socketMessage)
{
	if (socketMessage->messageHead.bHandleCode == 0)
	{
		createRoomLayer();
		//MSG_GP_O_Club_RoomChange* Data = (MSG_GP_O_Club_RoomChange*)socketMessage->object;
		//if (Data->bCreate)
		//{
		//	//创建房间
		//	if (Data->_data.isPlay)
		//	{
		//		//createRoomLayer();
		//	}
		//	else
		//	{
		//		//createRoomLayer();
		//	}
		//}
		//else
		//{
		//	//解散房间
		//	//createRoomLayer();
		//}
	}

	return true;
}


//获取俱乐部申请总人数
bool Club::GetJoinClubCount(HNSocketMessage* socketMessage)
{

	if (socketMessage->messageHead.bHandleCode == 0)
	{
		auto head = (MSG_GP_O_Club_ReviewList_Head*)socketMessage->object;
		int usernum = (socketMessage->objectSize - sizeof(MSG_GP_O_Club_ReviewList_Head)) / sizeof(MSG_GP_O_Club_ReviewList_Data);
		if (usernum > 0)Image_red->setVisible(true);
	}


	return true;
}

bool Club::GetTeaHouseAccess(HNSocketMessage* socketMessage)
{
	if (socketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_Club_GetCreateAccess* Result = (MSG_GP_O_Club_GetCreateAccess*)socketMessage->object;
		if (Result->userId == PlatformLogic()->loginResult.dwUserID)
		{
			if (Result->clubAccess == 1)
			{
				//创建茶楼界面
				ClubOperate* ClubOperateLayer = ClubOperate::create();
				this->addChild(ClubOperateLayer);
				ClubOperateLayer->SetPlaceHolderCount("请输入茶楼名称...");
				ClubOperateLayer->SetImageViewRes(1);
				auto Callback = [=](){
					MSG_GP_I_CreateClub  name;
					sprintf(name.szClubName, "%s", Utf8ToGB(TextCount.c_str()));
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CREATE_CLUB, &name, sizeof(MSG_GP_I_CreateClub), HN_SOCKET_CALLBACK(Club::CreateClubResult, this));
				};
				ClubOperateLayer->SetCallBack([=](){
					Callback();
				});
			}
			else
			{
				//暂无权限
				log("Not clubAccess!!!");
			}
		}
		else
		{
			//用户错误
			log("UserID Errer!!!");
		}
	}
	return true;
}

int Club::asciiTointNum(int num)
{
	return num - 48;
}

void Club::UpdateRoomList()
{
	//创建俱乐部房间
	if (RoomListView->getCurSelectedIndex() != -1)
	{
		RoomListView->removeAllItems();
	}
	for (int index = 1; index <= RoomInfo.size(); index++)
	{
		auto Item = CSLoader::createNode("platform/Club/ClubRoomItem.csb");
		ImageView* Roombg = dynamic_cast<ImageView*>(Item->getChildByName("Room_item"));
		Roombg->setPosition(Vec2(0, 0));
		Roombg->setTag(RoomInfo[index].iRoomID);

		//游戏总局数
		auto Count = dynamic_cast<Text*>(Roombg->getChildByName("Room_count"));
		string playcount = "局数:";
		if (RoomInfo[index].iCount == 99)
		{
			playcount = playcount + "一课";
		}
		else
		{
			playcount.append(StringUtils::toString(RoomInfo[index].iCount));
			playcount.append("局");
		}
		Count->setString(GBKToUtf8(playcount));

		//游戏名称
		auto Name = dynamic_cast<Text*>(Roombg->getChildByName("Game_name"));
		string strName = "游戏:";
		strName.append(RoomInfo[index].szGameName);
		Name->setString(GBKToUtf8(strName));
		//人数
		auto PlayerNum = dynamic_cast<Text*>(Roombg->getChildByName("Play_count"));
		string strPlayer = "人数:";
		strPlayer.append(StringUtils::toString(RoomInfo[index].iPlayCount));
		strPlayer.append("人");
		PlayerNum->setString(GBKToUtf8(strPlayer));
		//玩法
		auto wanfa = dynamic_cast<Text*>(Roombg->getChildByName("wanfa"));
		PlayerNum->setPositionX(PlayerNum->getPositionX() - 10);
		string strWanfa = "";
		//游戏名字比对
		char gameNameDDZ[30];
		char gameNameQZMJ[30];
		char gameNameJJMJ[30];
		char gameNamePDK[30];
		strcpy(gameNameDDZ, "斗地主");
		strcpy(gameNameQZMJ, "泉州麻将");
		strcpy(gameNameJJMJ, "晋江麻将");
		strcpy(gameNamePDK, "跑得快");
		int retDDZ;
		int retQZMJ;
		int retJJMJ;
		int retPDK;
		retDDZ = strcmp(RoomInfo[index].szGameName, gameNameDDZ);
		retQZMJ = strcmp(RoomInfo[index].szGameName, gameNameQZMJ);
		retJJMJ = strcmp(RoomInfo[index].szGameName, gameNameJJMJ);
		retPDK = strcmp(RoomInfo[index].szGameName, gameNamePDK);

		if (retJJMJ == 0)
		{

			if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
			{
				strWanfa = "倍数：游金三倍";
			}
			else
			{
				strWanfa = "倍数：游金四倍";
			}

			if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 0)
			{
				strWanfa = strWanfa + "  底分：庄2闲1";
			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 1)
			{
				strWanfa = strWanfa + "   底分：庄10闲5";
			}
			else
			{
				strWanfa = strWanfa + "   底分：庄20闲10";
			}

			string strWanfa1 = "";
			if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 0)
			{
				strWanfa = "玩法：不禁风头";
			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 1)
			{
				strWanfa = "玩法：跟打";
			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 2)
			{
				strWanfa = "玩法：禁风头";
			}

			if (asciiTointNum(RoomInfo[index].szDeskConfig[4]) == 0)
			{
				strWanfa1 = ", 双金不平胡";
			}
			else
			{
				strWanfa1 = "";
			}
			string strWanfa2 = "";

			if (asciiTointNum(RoomInfo[index].szDeskConfig[5]) == 0)
			{
				strWanfa2 = ", 抢杠胡";
			}
			else
			{
				strWanfa2 = "";
			}

			//玩法双金不平胡，抢杠胡。6.21
			strWanfa = strWanfa + strWanfa1 + strWanfa2;
			//strWanfa.append(StringUtils::toString(RoomInfo[index].szDeskConfig[2]));
			wanfa->setString(GBKToUtf8(strWanfa));
		}
		else if (retDDZ == 0)
		{
			if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
			{
				strWanfa = "玩法：经典玩法";
			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 3)
			{
				strWanfa = "玩法：经典癞子";
			}
			else
			{
				strWanfa = "玩法：天地癞子";
			}
			wanfa->setString(GBKToUtf8(strWanfa));
		}
		else if (retQZMJ == 0)
		{
			if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
			{
				strWanfa = "倍数：游金三倍";
			}
			else
			{
				strWanfa = "倍数：游金四倍";
			}

			if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 0)
			{
				strWanfa = strWanfa + "  底分：庄2闲1";
			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 1)
			{
				strWanfa = strWanfa + "   底分：庄10闲5";
			}
			else
			{
				strWanfa = strWanfa + "   底分：庄15闲10";
			}
			wanfa->setString(GBKToUtf8(strWanfa));
		}
		else if (retPDK == 0)
		{
			//跑得快玩法搭配
			if (asciiTointNum(RoomInfo[index].szDeskConfig[1]) == 1)
			{
				strWanfa = "玩法：首局黑桃3先出";
			}
			else
			{
				strWanfa = "玩法：每局黑桃3先出";
			}

			if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 1)
			{
				strWanfa = strWanfa + "  底分：1分";
			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[2]) == 3)
			{
				strWanfa = strWanfa + "   底分：3分";
			}
			else
			{
				strWanfa = strWanfa + "   底分：5分";
			}

			if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 0)
			{

			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 1)
			{
				strWanfa = strWanfa + "   大小头：2，1";
			}
			else if (asciiTointNum(RoomInfo[index].szDeskConfig[3]) == 2)
			{
				strWanfa = strWanfa + "   大小头：10，5";
			}
			else
			{
				strWanfa = strWanfa + "   大小头：20，10";
			}
			wanfa->setString(GBKToUtf8(strWanfa));
		}


		//俱乐部头像框
		auto HeadFrame = dynamic_cast<ImageView*>(Roombg->getChildByName("Image_frame_0"));
		//俱乐部头像底图
		auto Head = dynamic_cast<ImageView*>(Roombg->getChildByName("Image_head_0"));
		//创建头像
		auto HeadView = GameUserHead::create(Head, HeadFrame);
		HeadView->show(USER_HEAD_MASK);//添加底图
		HeadView->loadTexture(USER_MEN_HEAD);//判断是男还是女，没有网络头像情况下剩下
		//HeadView->loadTextureWithUrl(RoomInfo[index].szHeadURL);//设置网络头像


		auto Image_lock = dynamic_cast<ImageView*>(Roombg->getChildByName("Image_lock"));
		Image_lock->setZOrder(10);
		auto Join = dynamic_cast<Button*>(Roombg->getChildByName("Button_join"));
		if (RoomInfo[index].isPlay)
		{
			Join->setEnabled(false);
		}

		//更新数据
		Roombg->removeFromParentAndCleanup(true);
		RoomListView->pushBackCustomItem(Roombg);
	}
}

void Club::showTeaHouse(MSG_GP_O_Club_List pData)
{
	ClubID = pData.iClubID;
	isClubCreater = pData.bCreater;
	if (isClubCreater)
	{
		isManage = 1;
	}
	else
	{
		//查询权限再赋值isManage（管理也拥有权限）
		isManage = 0;
	}
	//更新当前俱乐部成员用户头像信息
	HNPlatformClubData::getInstance()->onQueryClubUserList(ClubID);
	m_SelectedTeaHouse = 0;
	UpdateTeaHouseInfo(pData);
}

void Club::updateTeaHouseList()
{
	//更新前清理列表
	if (ListView_club != nullptr)
	{
		Widget* item = ListView_club->getItem(0);
		if (item != nullptr)
		{
			ListView_club->removeAllItems();
		}
		m_SelectedTeaHouseID = 0;
	}

	if (MyCreate.size() != 0)
	{
		for (auto TeaHouseData: MyCreate)
		{
			createTeaHouseNode(TeaHouseData);
		}
	}
	if (MyJoin.size() != 0)
	{
		for (auto TeaHouseData : MyJoin)
		{
			createTeaHouseNode(TeaHouseData);
		}
	}
}

void Club::createTeaHouseNode(MSG_GP_O_Club_List pData)
{
	if (ListView_club != nullptr)
	{
		auto node = CSLoader::createNode("platform/Club/TeaHouseItemNode.csb");
		auto img_TeaHItem = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
		if (ClubID == NULL)
		{
			img_TeaHItem->loadTexture("platform/Club/res/out_game/img_xinxi_kuang.png");
			ClubID = pData.iClubID;
			isClubCreater = pData.bCreater;
			if (isClubCreater)
			{
				isManage = 1;
			}
			else
			{
				//查询权限再赋值isManage（管理也拥有权限）
				isManage = 0;
			}
			//更新当前俱乐部成员用户头像信息
			HNPlatformClubData::getInstance()->onQueryClubUserList(ClubID);
			UpdateTeaHouseInfo(pData);
		}
		else if (ClubID == pData.iClubID)
		{
			img_TeaHItem->loadTexture("platform/Club/res/out_game/img_xinxi_kuang.png");
		}
		else
		{
			img_TeaHItem->loadTexture("platform/Club/res/out_game/img_xinxi_kuang2.png");
		}

		//退出俱乐部
		auto Button_exit = (Button*)img_TeaHItem->getChildByName("Button_exit");
		Button_exit->addClickEventListener([=](Ref*){
			if (pData.bCreater)
			{
				auto SendDisbandClub = [=](){
					MSG_GP_I_DissmissClub Request;
					Request.iClubID = pData.iClubID;
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_DISSMISS_CLUB, &Request, sizeof(MSG_GP_I_DissmissClub), HN_SOCKET_CALLBACK(Club::ClubDisbandResult, this));
				};
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("您是否要解散茶楼?"));
				ClubTipsLayer->SetImageViewRes(2);
				ClubTipsLayer->setCallBack([=](){
					SendDisbandClub();
				});
			}
			else
			{
				auto SendExitClub = [=](){
					MSG_GP_I_LeaveClub Request;
					Request.iClubID = pData.iClubID;
					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_LEAVE_CLUB, &Request, sizeof(MSG_GP_I_LeaveClub), HN_SOCKET_CALLBACK(Club::getExitClubResult, this));
				};
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("您是否要退出茶楼?"));
				ClubTipsLayer->SetImageViewRes(3);
				ClubTipsLayer->setCallBack([=](){
					SendExitClub();
				});
			}
		});

		//进入茶楼
		auto Button_enter = (Button*)img_TeaHItem->getChildByName("Button_enter");
		Button_enter->addClickEventListener([=](Ref*){
			if (ClubID == pData.iClubID)
			{
				//当前选中的茶楼关闭切换茶楼
				if (Panel_ClubList != nullptr)
				{
					Panel_ClubList->setVisible(false);
					Panel_ClubList->runAction(MoveTo::create(0.5, Vec2(0, 0)));
				}
			}
			else
			{
				ClubID = pData.iClubID;
				isClubCreater = pData.bCreater;
				if (isClubCreater)
				{
					isManage = 1;
				}
				else
				{
					//查询权限再赋值isManage（管理也拥有权限）
					isManage = 0;
				}
				//更新当前俱乐部成员用户头像信息
				HNPlatformClubData::getInstance()->onQueryClubUserList(ClubID);
				bool isPartner = false;
				if (Button_member)
				{
					if (isManage == 3)
					{
						Button_member->setVisible(false);
					}
					else
					{
						Button_member->setVisible(isManage);
					}
				}
				if (Button_teaSet)
				{
					if (isManage == 3)
					{
						Button_teaSet->setVisible(false);
					}
					else
					{
						Button_teaSet->setVisible(isManage);
					}
					//Button_teaSet->setVisible(isManage);
				}
				if (Button_Cooperation)
				{
					if (isManage == 3)
					{
						Button_Cooperation->setVisible(false);
					}
					else
					{
						Button_Cooperation->setVisible(isManage);
					}
					//Button_Cooperation->setVisible(isManage);
				}
				if (Button_Member)
				{
					if (isManage != 3)
					{
						Button_Member->setVisible(false);
					}
					else
					{
						Button_Member->setVisible(isManage);
					}
					//Button_Member->setVisible(isManage);
				}
				//更新茶楼信息
				log("updata TeaHouse Info !!!");
				m_SelectedTeaHouse = 0;
				UpdateTeaHouseInfo(pData);
				//更新完成收起切换
				if (Panel_ClubList != nullptr)
				{
					Panel_ClubList->setVisible(false);
					Panel_ClubList->runAction(MoveTo::create(0.5, Vec2(0, 0)));
				}
			}
		});

		/*if (Button_member)
		{
			Button_member->setVisible(isManage);
		}
		if (Button_teaSet)
		{
			Button_teaSet->setVisible(isManage);
		}*/

		//茶楼排序计数
		m_SelectedTeaHouseID++;
		auto Text_top = (Text*)img_TeaHItem->getChildByName("Text_top");
		Text_top->setString(to_string(m_SelectedTeaHouseID));

		//楼主头像
		auto Image_head = (ImageView*)img_TeaHItem->getChildByName("Image_head");
		auto HeadFrame = (ImageView*)img_TeaHItem->getChildByName("Image_frame");
		auto HeadView = GameUserHead::create(Image_head, HeadFrame);
		HeadView->show(USER_HEAD_MASK);//添加底图
		HeadView->loadTexture(USER_MEN_HEAD);//判断是男还是女，没有网络头像情况下
		HeadView->loadTextureWithUrl(pData.szHeadURL);//设置网络头像

		//茶楼名字
		auto Text_teahouse_name = (Text*)img_TeaHItem->getChildByName("Text_teahouse_name");
		string str = pData.szClubName;
		if (str.length() > 12)
		{
			str.erase(13, str.length());
			str.append("...");
			Text_teahouse_name->setString(GBKToUtf8(str));
		}
		else
		{
			Text_teahouse_name->setString(GBKToUtf8(str));
		}

		//楼主昵称
		auto Text_Group_name = (Text*)img_TeaHItem->getChildByName("Text_Group_name");
		string str1 = pData.szClubOwnerName;
		if (str1.length() > 12)
		{
			str1.erase(13, str.length());
			str1.append("...");
			Text_Group_name->setString(StringUtils::format(GBKToUtf8("群主:%s"), str1.c_str()));
		}
		else
		{
			Text_Group_name->setString(StringUtils::format(GBKToUtf8("群主:%s"), str1.c_str()));
		}

		//茶楼ID
		auto Text_Group_ID = (Text*)img_TeaHItem->getChildByName("Text_Group_ID");
		Text_Group_ID->setString(StringUtils::format("ID:%d", pData.iClubID));

		//等待中的桌子
		auto Text_wait_table = (Text*)img_TeaHItem->getChildByName("Text_wait_table");
		Text_wait_table->setString(StringUtils::format(GBKToUtf8("%d桌等待中"), pData.readyDeskCount));

		//进行中的桌子
		auto Text_start_table = (Text*)img_TeaHItem->getChildByName("Text_start_table");
		Text_start_table->setString(StringUtils::format(GBKToUtf8("%d桌游戏中"), pData.playingDeskCount));

		img_TeaHItem->removeFromParentAndCleanup(true);
		ListView_club->pushBackCustomItem(img_TeaHItem);
	}
}

void Club::UpdateTeaHouseInfo(MSG_GP_O_Club_List pData)
{
	if (Panel_ClubInfo != nullptr)
	{
		Panel_ClubInfo->setVisible(true);
		//茶楼名字
		string str = pData.szClubName;
		if (str.length() > 16)
		{
			str.erase(17, str.length());
			str.append("...");
			Text_ClubName->setString(GBKToUtf8(str));
		}
		else
		{
			Text_ClubName->setString(GBKToUtf8(str));
		}

		//茶楼ID
		Text_club_id->setString(to_string(pData.iClubID));

		//楼层显示
		Text_tower->setString(GBKToUtf8("大厅"));

		HNPlatformClubData::getInstance()->StartNetEventListener();

		//请求公告，请求桌子，添加网络监听
		updateClubNotice();

		//初始化红花数和用户权限
		RequestTeaHouseUser();

		//添加网络监听
		AddTeaHouseNetWorkEventListener();
	}
}

//茶楼信息刷新
void Club::UpdateTeaHouseView()
{
	vector<SClubTeahouseInfo> vec = HNPlatformClubData::getInstance()->getTeaHouseInfo(ClubID);
	int Index = 0;
	
	//初始化清空列表
	if (ListView_FloorList != nullptr)
	{
		Widget* item = ListView_FloorList->getItem(0);
		if (item != nullptr)
		{
			ListView_FloorList->removeAllItems();
		}
	}
	int count = 6;
	int max_number = (vec.size() + 1) >= count ? (vec.size() + 2) : count;
	

	if (ListView_FloorList)
	{
		for (int i = 0; i < max_number; i++)
		{
			Index++;
			auto node = CSLoader::createNode("platform/Club/SwitchFloorNode.csb");
			auto panel_clone = (Layout*)node->getChildByName("panel_clone");

			//选中显示
			auto Image_switch = (ImageView*)panel_clone->getChildByName("Image_switch");
			Image_switch->setVisible(false);

			//楼层显示计数
			auto Text_top = (Text*)panel_clone->getChildByName("Text_top");
			Text_top->setString(to_string(Index));
			//一层大厅(默认选中)
			if (i == 0)
			{
				if (vec.size() == 0)
				{
					m_SelectedTeaHouse = 0;
					Image_switch->setVisible(true);
				}
				else
				{
					Image_switch->setVisible(false);
					//显示默认勾选
					if (m_SelectedTeaHouse == i)
					{
						Image_switch->setVisible(true);
					}
				}
				auto panel_platform = (Layout*)panel_clone->getChildByName("panel_platform");
				panel_platform->setVisible(true);
				//切换按钮
				auto Button_switch = (Button*)panel_platform->getChildByName("Button_switch");
				Button_switch->addClickEventListener([=](Ref*){
					if (m_SelectedTeaHouse != i)
					{
						SwitchTeaHouseFloor(i);
					}
				});
			}
			else
			{
				//有楼层
				if (vec.size() > i - 1)
				{
					if (i == 1 && m_SelectedTeaHouse == -1)
					{
						Image_switch->setVisible(true);
						m_SelectedTeaHouse = 1;
						Text_tower->setString(GetTeaHouseName(m_SelectedTeaHouse));
						Max_Player = vec[i - 1].maxPlayerCount;
						Max_palycount = vec[i - 1].maxGames;
						m_TeaHouseID = vec[i - 1].teahouseId;
						_GameID = vec[i - 1].gameNameId;
						_roomID = vec[i - 1].roomId;
					}
					else
					{
						Image_switch->setVisible(false);
					}

					//显示默认勾选
					if (m_SelectedTeaHouse == i)
					{
						Image_switch->setVisible(true);
					}

					auto panel_Have_Game = (Layout*)panel_clone->getChildByName("panel_Have_Game");
					panel_Have_Game->setVisible(true);

					//游戏名字
					auto Text_gameName = (Text*)panel_Have_Game->getChildByName("Text_gameName");
					Text_gameName->setString(GBKToUtf8(vec[i - 1].szName));

					//游戏人数局数
					auto Text_gameConfig = (Text*)panel_Have_Game->getChildByName("Text_gameConfig");
					Text_gameConfig->setString(StringUtils::format(GBKToUtf8("%d人    %d局"), vec[i - 1].maxPlayerCount, vec[i - 1].maxGames));

					//修改楼层名字
					auto Button_Modify = (Button*)panel_Have_Game->getChildByName("Button_Modify");
					bool isShow = false;
					if (isManage && isManage < 3)
					{
						isShow = true;
					}
					Button_Modify->setVisible(isShow);
					Button_Modify->addClickEventListener([=](Ref*){
						//打开修改楼层名字界面（权限判断）
						SetSwitchTeaHouseName(vec[i - 1].teahouseId);
					});

					//修改楼层玩法
					auto Button_set = (Button*)panel_Have_Game->getChildByName("Button_Manage");
					Button_set->setVisible(isShow);
					Button_set->addClickEventListener([=](Ref*){
						GameCreator()->SetCurrentselectSine(1);
						auto create = CreateRoomLayer::create(5);
						create->setTeaHouseID(vec[i-1].teahouseId);
						create->setTeaHouseGameID(vec[i - 1].gameNameId);
						create->setRedFlowerCheck(vec[i - 1].option);
						create->setPosition(Vec2::ZERO);
						this->addChild(create);
						GameCreator()->SetCurrentselectSine(0);
						Panel_FloorSwitch->runAction(MoveTo::create(0.5, Vec2(0, 0)));
					});

					//删除楼层
					auto Button_Close = (Button*)panel_Have_Game->getChildByName("Button_close");
					Button_Close->setVisible(isShow);
					Button_Close->addClickEventListener([=](Ref*){
						//弹出框提示是否确定删除
						auto prompt = GamePromptLayer::create(true);
						prompt->showPrompt(GBKToUtf8("确定删除楼层？"));
						prompt->setCallBack([=](){
							//删除楼层
							MSG_GP_I_Club_DeleteTeahouse Request;
							Request.clubId = ClubID;
							Request.gameNameId = vec[i - 1].gameNameId;
							Request.teahouseId = vec[i - 1].teahouseId;
							PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_DeleteTeaHouse, &Request, sizeof(MSG_GP_I_Club_DeleteTeahouse), HN_SOCKET_CALLBACK(Club::DeleteTeaHouseResult, this));
						});
					});

					//切换楼层
					auto Button_Switch = (Button*)panel_Have_Game->getChildByName("Button_Switch");
					Button_Switch->addClickEventListener([=](Ref*){
						if (m_SelectedTeaHouse != i)
						{
							//m_SelectedTeaHouse = i;
							Max_Player = vec[i - 1].maxPlayerCount;
							Max_palycount = vec[i - 1].maxGames;
							m_TeaHouseID = vec[i - 1].teahouseId;
							_roomID = vec[i - 1].roomId;
							_GameID = vec[i - 1].gameNameId;
							SwitchTeaHouseFloor(i);
						}
					});

					//楼层详情玩法
					auto Button_Details = (Button*)panel_Have_Game->getChildByName("Button_Details");
					Button_Details->addClickEventListener([=](Ref*){
						//打开详情
						ShowTeaHouseInfo(vec[i - 1].teahouseId);
					});
					Button_Details->setUserData(&vec[i - 1]);
				}
				//没有楼层信息
				else
				{
					Image_switch->setVisible(false);

					auto panel_No_Game = (Layout*)panel_clone->getChildByName("panel_No_Game");
					panel_No_Game->setVisible(true);

					//创建楼层
					auto Button_create = (Button*)panel_No_Game->getChildByName("Button_create");
					bool isShow = false;
					if (isManage && isManage < 3)
					{
						isShow = true;
					}
					Button_create->setVisible(isShow);
					Button_create->addClickEventListener([=](Ref*){
						//打开创建(权限判断）
						GameCreator()->SetCurrentselectSine(1);
						auto create = CreateRoomLayer::create(5);
						create->setTeaHouseID(0);
						create->setTeaHouseGameID(0);
						create->setPosition(Vec2::ZERO);
						this->addChild(create);
						GameCreator()->SetCurrentselectSine(0);
						Panel_FloorSwitch->runAction(MoveTo::create(0.5, Vec2(0, 0)));
					});
				}				
			}

			panel_clone->setName(StringUtils::format("TeaHouse_%d", i));
			panel_clone->removeFromParentAndCleanup(true);
			ListView_FloorList->pushBackCustomItem(panel_clone);
		}

		//SwitchTeaHouseFloor(m_SelectedTeaHouse);
		InitTeaHouseFloor();
		m_MaxTeaHouseIndex = vec.size();

		auto pLayer = dynamic_cast<ClubManag*>(this->getChildByName("ClubManag"));
		if (pLayer)
		{
			pLayer->UpdateRoomInfo();
		}

		//更新茶楼在线人数
		UpdateTeaHouseOnlineNum();
	}
}

void Club::UpdateTeaHouseTable()
{
	auto Button_quick_game = (Button*)Panel_ClubInfo->getChildByName("Button_quick_game");
	Button_quick_game->setEnabled(true);

	SClubTeahouseInfo Result = HNPlatformClubData::getInstance()->getTeaHouseInfoByTeaHouseID(ClubID, m_TeaHouseID);

	vector<SClubTeahouseDeskInfo> vec = HNPlatformClubData::getInstance()->getTeaHouseTable(ClubID, m_TeaHouseID);
	int Index = 0;
	Max_Player = Result.maxPlayerCount;
	if (Result.maxDeskCount != vec.size() && !isSocketNow)
	{
		isSocketNow = true;
		//通知后端桌子数未达到楼层上限是否更新
		MSG_GP_I_Club_ReplenishTeahouseDesk Request;
		Request.clubId = ClubID;
		Request.gameNameId = _GameID;
		Request.teahouseId = m_TeaHouseID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_ReplenishTeahouseDesk, &Request, sizeof(MSG_GP_I_Club_ReplenishTeahouseDesk));
	}
	else if (Result.maxDeskCount == vec.size())
	{
		isSocketNow = false;
	}

	//初始化清空列表
	if (ListView_Table != nullptr)
	{
		Widget* item = ListView_Table->getItem(0);
		if (item != nullptr)
		{
			ListView_Table->removeAllItems();
		}
		Vec_Table.clear();
	}

	if (ListView_Table)
	{
		if (vec.size() > 0)
		{
			int Y = std::ceil((float)vec.size() / 3.0);  //每行放3张桌子
			
			auto Layout_bk = Layout::create();
			Layout_bk->setSize(Size(1050, 260 * Y));
			Layout_bk->setAnchorPoint(Vec2(0, 1));
			Layout_bk->setName("Table_bg");
			for (int i = 0; i < vec.size(); i++)
			{
				if (m_TeaHouseID == vec[i].teahouseId)
				{
					auto node = CSLoader::createNode("platform/Club/FloorTableNode.csb");
					auto panel_clone = (Layout*)node->getChildByName("Panel_clone");

					//桌子信息加载
					if (Max_Player != 0)
					{
						string str = StringUtils::format("Image_Play_%d", (Max_Player < 6 ? Max_Player : 6));
						auto Table = (ImageView*)panel_clone->getChildByName(str);
						if (Table)
						{
							int countPlayer = 0;
							//加载用户头像
							for (int j = 1; j <= Max_Player; j++)
							{
								auto head = (ImageView*)Table->getChildByName(StringUtils::format("Image_head_%d",j));
								if (j < 6 && vec[i].userIDList[j - 1] != 0)
								{
									countPlayer++;
									if (head) {
										auto HeadView = GameUserHead::create(head);
										HeadView->show(USER_HEAD_MASK);//添加底图
										//判断是男还是女，没有网络头像情况下
										HeadView->loadTexture(USER_MEN_HEAD);
										HeadView->loadTextureWithUrl(
											HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(
											vec[i].userIDList[j - 1]));//设置网络头像
									}
								}
								else
								{
									if (head)
										head->setVisible(false);
								}
							}

							//加载桌子信息
							auto Image_table_state = (ImageView*)Table->getChildByName("Image_table_state");
							Image_table_state->loadTexture(vec[i].deskState == 3 ? 
								"platform/Club/res/out_game/word_yxz.png" 
								: "platform/Club/res/out_game/word_ddz.png");

							//人数加载
							auto Text_player = (Text*)Table->getChildByName("Text_player");
							Text_player->setString(StringUtils::format("%d/%d", countPlayer, Max_Player));
							
							//当前局数
							auto Text_play_count = (Text*)Table->getChildByName("Text_play_count");
							if (vec[i].deskState == 3)
							{
								Text_play_count->setString(StringUtils::format(GBKToUtf8("第%d/%d局"), vec[i].curGames, Max_palycount));
								Text_play_count->setVisible(true);
							}
							else
							{
								Text_play_count->setVisible(false);
							}
							Table->setVisible(true);
						}

						//加入桌子按钮
						auto Button_join_table = (Button*)panel_clone->getChildByName("Button_join_table");
						Button_join_table->addClickEventListener([=](Ref*){
							//log("Join deskIndex == %d",vec[i].deskIndex);
							auto Rest = HNPlatformClubData::getInstance()->getTeaHouseInfoByState(ClubID);
/////////////////////////////////////////////////////////////////////////////////////////
							if (Rest)
							{
								GamePromptLayer::create()->showPrompt(GBKToUtf8("当前茶楼已暂停开房，请联系管理开启。"));
							}
							else
							{
								_TableIndex = vec[i].deskIndex;
								auto update = GameResUpdate::create();
								update->retain();
								update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {
									if (success)
									{
										GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr) {
											if (success)
											{
												if (RoomLogic()->isConnect())
												{
													_roomLogic->stop();
													RoomLogic()->close();
													//RoomLogic()->setConnect(false);
												}
												_roomLogic->start();
												_roomLogic->requestLogin(_roomID, latitude, longtitude, addr);

											}
											else
											{//不强制开启定位
												if (RoomLogic()->isConnect())
												{
													_roomLogic->stop();
													RoomLogic()->close();
												}
												_roomLogic->start();
												_roomLogic->requestLogin(_roomID);
											}
										};
										GameLocation::getInstance()->getLocation();
									}
									update->release();
								};
								update->checkUpdate(_GameID, true);
							}
/////////////////////////////////////////////////////////////////////////////////////////
						});
						//Button_join_table->setEnabled(vec[i].deskState == 3 ? false : true);

						//判断自己在用户列表的时候点开桌子列表弹窗

					}					

					panel_clone->setTag(vec[i].deskIndex);
					Vec_Table.push_back(panel_clone);
					panel_clone->removeFromParentAndCleanup(true);

					int x = Index % 3;
					int y = std::floor((float)Index / 3.0);
					panel_clone->setPosition(Vec2((panel_clone->getContentSize().width / 2) + x * 350, (Y * 260 - panel_clone->getContentSize().height / 2) - y * 260));
					Layout_bk->addChild(panel_clone);
					Index++;
				}
			}
			ListView_Table->pushBackCustomItem(Layout_bk);
		}
	}
}

void Club::UpdateLobbyTeaHouse()
{
	auto Button_quick_game = (Button*)Panel_ClubInfo->getChildByName("Button_quick_game");
	Button_quick_game->setEnabled(false);

	vector<SClubTeahouseInfo> InfoVec = HNPlatformClubData::getInstance()->getTeaHouseInfo(ClubID);
	//std::vector<SClubTeahouseDeskInfo> tableVec = HNPlatformClubData::getInstance()->getTeaHouseTable(ClubID);
	int Index = 0;

	//初始化清空列表
	if (ListView_Table != nullptr)
	{
		Widget* item = ListView_Table->getItem(0);
		if (item != nullptr)
		{
			ListView_Table->removeAllItems();
		}
		Vec_Table.clear();
	}

	if (ListView_Table)
	{
		int deskCount = HNPlatformClubData::getInstance()->getTeaHousTableCount();
		int Y = std::ceil((float)deskCount / 3.0);  //每行放3张桌子

		auto Layout_bk = Layout::create();
		Layout_bk->setSize(Size(1050, 260 * Y));
		Layout_bk->setAnchorPoint(Vec2(0, 1));
		Layout_bk->setName("Table_bg");

		for (int infoIndex = 0; infoIndex < InfoVec.size(); infoIndex++) {
			auto infoValue = InfoVec[infoIndex];
			std::vector<SClubTeahouseDeskInfo> vec = HNPlatformClubData::getInstance()->getTeaHouseTable(ClubID, infoValue.teahouseId);
			if (vec.size() > 0)
			{
				for (int i = 0; i < vec.size(); i++)
				{
					ClubItem* pItem = ClubItem::create(ClubID, infoValue, vec[i]);
					pItem->setTag(vec[i].deskIndex);

					Vec_Table.push_back(pItem);
					int x = Index % 3;
					int y = std::floor((float)Index / 3.0);
					pItem->setPosition(Vec2((pItem->getContentSize().width / 2) + x * 350, (Y * 260 - pItem->getContentSize().height / 2) - y * 260));
					Layout_bk->addChild(pItem);
					Index++;

					pItem->onClubItemCallBackLite = [=](int clubID, int max_Player, int max_palycoun, int teaHouseID, int roomID, int gameID, int tableIndex)
					{
						auto Rest = HNPlatformClubData::getInstance()->getTeaHouseInfoByState(clubID);
						/////////////////////////////////////////////////////////////////////////////////////////
						if (Rest)
						{
							GamePromptLayer::create()->showPrompt(GBKToUtf8("当前茶楼已暂停开房，请联系管理开启。"));
						}
						else
						{
							Max_Player = max_Player;
							Max_palycount = max_palycoun;
							m_TeaHouseID = teaHouseID;
							_roomID = roomID;
							_GameID = gameID;
							_TableIndex = tableIndex;
							auto update = GameResUpdate::create();
							update->retain();
							update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {
								if (success)
								{
									GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr) {
										if (success)
										{
											if (RoomLogic()->isConnect())
											{
												_roomLogic->stop();
												RoomLogic()->close();
												//RoomLogic()->setConnect(false);
											}
											_roomLogic->start();
											_roomLogic->requestLogin(_roomID, latitude, longtitude, addr);

										}
										else
										{//不强制开启定位
											if (RoomLogic()->isConnect())
											{
												_roomLogic->stop();
												RoomLogic()->close();
											}
											_roomLogic->start();
											_roomLogic->requestLogin(_roomID);
										}
									};
									GameLocation::getInstance()->getLocation();
								}
								update->release();
							};
							update->checkUpdate(_GameID, true);
						}
					};
				}
			}
		}
		ListView_Table->pushBackCustomItem(Layout_bk);
// 		if (vec.size() > 0)
// 		{
// 			int Y = std::ceil((float)vec.size() / 3.0);  //每行放3张桌子
// 
// 			auto Layout_bk = Layout::create();
// 			Layout_bk->setSize(Size(1050, 260 * Y));
// 			Layout_bk->setAnchorPoint(Vec2(0, 1));
// 			Layout_bk->setName("Table_bg");
// 			for (int i = 0; i < vec.size(); i++)
// 			{
// 				auto node = CSLoader::createNode("platform/Club/FloorTableNode.csb");
// 				auto panel_clone = (Layout*)node->getChildByName("Panel_clone");
// 
// 				auto Image_Table = (ImageView*)panel_clone->getChildByName("Image_Table");
// 				if (Image_Table)
// 				{
// 					Image_Table->setVisible(true);
// 					//楼层玩法显示
// 					auto Sprite_tip = (Sprite*)Image_Table->getChildByName("Sprite_tip");
// 					Sprite_tip->setTexture(StringUtils::format("platform/Club/res/out_game/img_wf%d.png", i + 1));
// 					//楼层桌子名字
// 					auto Text_name = (Text*)Image_Table->getChildByName("Text_name");
// 					Text_name->setString(GBKToUtf8(vec[i].szName));
// 					//楼层玩法显示
// 					auto Text_play_config = (Text*)Image_Table->getChildByName("Text_play_config");
// 					Text_play_config->setString(StringUtils::format(GBKToUtf8("%d人    %d局"),vec[i].maxPlayerCount,vec[i].maxGames));
// 
// 					Image_Table->loadTexture(StringUtils::format("platform/Club/res/out_game/img_%dz.png", vec[i].maxPlayerCount));
// 
// 					//默认大厅点桌子加入对应楼层快速进入
// 					auto Button_join_table = (Button*)panel_clone->getChildByName("Button_join_table");
// 					Button_join_table->addClickEventListener([=](Ref* ref){
// 						auto Rest = HNPlatformClubData::getInstance()->getTeaHouseInfoByState(ClubID);
// 						if (Rest)
// 						{
// 							GamePromptLayer::create()->showPrompt(GBKToUtf8("当前茶楼已暂停开房，请联系管理开启。"));
// 						}
// 						else
// 						{
// 							Button* btn = dynamic_cast<Button*>(ref);
// 							SClubTeahouseInfo info = vec[i];
// 							m_bFastJoin = true;
// 							_GameID = info.gameNameId;
// 							m_TeaHouseID = info.teahouseId;
// 							///////////////////////////////////////////////////////////////////
// 							auto update = GameResUpdate::create();
// 							update->retain();
// 							update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {
// 								if (success)
// 								{
// 									GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr) {
// 										if (success)
// 										{
// 											if (RoomLogic()->isConnect())
// 											{
// 												_roomLogic->stop();
// 												RoomLogic()->close();
// 											}
// 											_roomLogic->start();
// 											_roomLogic->requestLogin(info.roomId, latitude, longtitude, addr);
// 											_roomID = info.roomId;
// 										}
// 										else
// 										{//不强制开启定位
// 											if (RoomLogic()->isConnect())
// 											{
// 												_roomLogic->stop();
// 												RoomLogic()->close();
// 											}
// 											_roomLogic->start();
// 											_roomLogic->requestLogin(info.roomId);
// 											_roomID = info.roomId;
// 										}
// 									};
// 									GameLocation::getInstance()->getLocation();
// 								}
// 								update->release();
// 							};
// 							update->checkUpdate(_GameID, true);
// 							///////////////////////////////////////////////////////////////////
// 						}
// 					});
// 					//Button_join_table->setUserData(&vec[i]);
// 				}
// 
// 				//panel_clone->setTag(vec[i].deskIndex);
// 				Vec_Table.push_back(panel_clone);
// 				panel_clone->removeFromParentAndCleanup(true);
// 
// 				int x = Index % 3;
// 				int y = std::floor((float)Index / 3.0);
// 				panel_clone->setPosition(Vec2((panel_clone->getContentSize().width / 2) + x * 350, (Y * 260 - panel_clone->getContentSize().height / 2) - y * 260));
// 				Layout_bk->addChild(panel_clone);
// 				Index++;
// 			}
// 			ListView_Table->pushBackCustomItem(Layout_bk);
// 		}
	}
}

void Club::SwitchTeaHouseFloor(int Index)
{
	m_SelectedTeaHouse = Index;

	//更新楼层列表信息
	UpdateTeaHouseView();

	//更新茶楼大厅显示楼层
	if (Text_tower)
	{
		Text_tower->setString(GetTeaHouseName(m_SelectedTeaHouse));
	}
}

void Club::InitTeaHouseFloor()
{
	if (m_SelectedTeaHouse != 0)
	{
		UpdateTeaHouseTable();
	}
	else
	{
		UpdateLobbyTeaHouse();
	}
}

std::string Club::GetTeaHouseName(int index)
{
	std::string str = GBKToUtf8("大厅");
	if (0 != index) 
	{
		char name[32] = { 0 };
		sprintf(name, "%d%s", index, GBKToUtf8("层"));
		str = name;
	}
	return str;
}

void Club::onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode)
{
	if (success)
	{
		ComRoomInfo* roomInfo = RoomInfoModule()->getByRoomID(_roomID);
		//RoomInfoModule()->addRoom(roomInfo);
		if ((RoomLogic()->getRoomRule() & GRR_NOTCHEAT)
			&& !(RoomLogic()->getRoomRule() & GRR_QUEUE_GAME)) //单防作弊
		{
			_roomLogic->requestQuickSit();
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(enterGame_please_wait_text), 22);
		}
		else if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME) // 单排队机或者排队机+防作弊
		{
			auto loding = LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(allocation_table_please_wait_text), 22);
			loding->setCancelCallBack([=](){
				_roomLogic->stop();
				RoomLogic()->close();
			});

			// 进入排队游戏
			_roomLogic->requestJoinQueue();

		}
		else // 金币场不扣积分
		{
			if (GameCreator()->getCurrentGameType() == HNGameCreator::BR
				&&roomInfo->uNameID != 11100503 || GameCreator()->getCurrentGameType() == HNGameCreator::SINGLE || GameCreator()->getCurrentGameType() == HNGameCreator::NORMAL)
			{
				if (m_bFastJoin)
				{
					_roomLogic->requestTeaHouseQuickSit(ClubID, m_TeaHouseID);
				}
				else
				{
					/*char szDeskPass[20];
					sprintf(szDeskPass, "%03d%03d", _roomID, _TableIndex);
					_roomLogic->requestQuickSit(_TableIndex, szDeskPass);*/
					_roomLogic->requestTeaHouseQuickSit(ClubID, m_TeaHouseID, _TableIndex);
				}
				m_bFastJoin = false;
				
				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(enterGame_please_wait_text), 22);
			}
			else
			{
				_roomLogic->stop();
				this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
					this->removeFromParent();
				}), nullptr));
			}
		}
		//进入游戏前默认不进行网络更新监听（发送离开俱乐部进入游戏）
		HNPlatformClubData::getInstance()->RemoveNetEventListener();
		MSG_GP_I_DissmissClub Request;
		Request.iClubID = ClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LeaveHall, &Request, sizeof(MSG_GP_I_DissmissClub));
		//HNPlatformClubData::getInstance()->RemoveNetEventListener();
	}
	else
	{
		_roomLogic->stop();
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(message);
		prompt->setCallBack([=](){
			_roomLogic->start();
			_roomLogic->requestLogin(roomID);
		});

		prompt->setCancelCallBack([=](){ 
			//退出茶楼界面
			ClubID = 0;
			//退出茶楼返回大厅发送退出茶楼消息
			MSG_GP_I_DissmissClub Request;
			Request.iClubID = ClubID;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LeaveHall, &Request, sizeof(MSG_GP_I_DissmissClub));
			HNPlatformClubData::getInstance()->RemoveNetEventListener();
			GamePlatform::createNewPlatform(); 
		});
	}
}

void Club::onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo)
{
	_roomLogic->stop();

	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	if (success)
	{
		if (INVALID_DESKNO != deskNo && INVALID_DESKSTATION != seatNo)
		{
			// 启动游戏
			bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
			if (!ret)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"));
			}
		}
	}
	else
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(message);
		prompt->setCallBack([=](){
			if (GameCreator()->getCurrentGameType() == HNGameCreator::SINGLE ||
				GameCreator()->getCurrentGameType() == HNGameCreator::BR)
			{
				RoomLogic()->close();
			}
		});
	}
}

void Club::onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo)
{
	_roomLogic->stop();
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	if (success)
	{
		if (INVALID_DESKNO != deskNo)
		{
			// 启动游戏
			bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
			if (!ret)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"));
			}
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("错误的游戏桌号"));
		}
	}
	else
	{
		RoomLogic()->close();
		GamePromptLayer::create()->showPrompt(message);
	}
}

MSG_GP_O_Club_List Club::getClubInfoByClubID(int ClubID)
{
	MSG_GP_O_Club_List Data;
	memset(&Data, 0, sizeof(MSG_GP_O_Club_List));
	if (ClubID == 0)
		return Data;
	if (MyCreate.size() > 0)
	{
		for (auto item : MyCreate)
		{
			if (item.iClubID == ClubID)
			{
				memcpy(&Data, &item, sizeof(MSG_GP_O_Club_List));
				return Data;
			}
		}
	}
	if (MyJoin.size() > 0)
	{
		for (auto item : MyJoin)
		{
			if (item.iClubID == ClubID)
			{
				memcpy(&Data, &item, sizeof(MSG_GP_O_Club_List));
				return Data;
			}
		}
	}
	return Data;
}

void Club::SetSwitchTeaHouseName(int TeaHouseID)
{
	//修改茶楼楼层名字
	ClubOperate* ClubOperateLayer = ClubOperate::create();
	this->addChild(ClubOperateLayer);
	ClubOperateLayer->SetPlaceHolderCount("请输入楼层名字...");
	ClubOperateLayer->SetImageViewRes(4);
	auto CallBack = [=](){
		SClubRequest_ModifyTeahouseName  Data;
		Data.gameNameId = 0;
		Data.msg = ClubRequest_ModifyTeahouseName;
		Data.clubId = ClubID; 
		Data.teahouseId = TeaHouseID;
		sprintf(Data.szName, "%s", Utf8ToGB(TextCount.c_str()));
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequest_ModifyTeahouseName), HN_SOCKET_CALLBACK(Club::getTeaHouseUserResult, this));
	};
	ClubOperateLayer->SetCallBack([=](){
		CallBack();
	});
}

void Club::UpdateTeaHouseOnlineNum()
{
	SClubRequestBase  Data;
	Data.gameNameId = 0;
	Data.msg = ClubRequest_QueryOnlineUserCount;
	Data.clubId = ClubID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Data, sizeof(SClubRequestBase), HN_SOCKET_CALLBACK(Club::getTeaHouseOnlineNumResult, this));
}

void Club::ShowTeaHouseInfo(int TeaHouseID)
{
	if (Panel_TeaHouseInfo)
	{
		//获取楼层信息
		auto Data = HNPlatformClubData::getInstance()->getTeaHouseInfoByTeaHouseID(ClubID, TeaHouseID);
		Panel_TeaHouseInfo->setVisible(true);

		string str_Name = "";
		string str_Rule = "";		//Data.szDeskConfig[0]付费方式    Data.szDeskConfig[1]
		if (Data.gameNameId == 20200310)
		{
			str_Name = "算牌跑得快";
			if (Data.szDeskConfig[1] - 48 == 1)
			{
				str_Rule = "首局黑桃3先出";
			}
			else
			{
				str_Rule = "每局黑桃3先出";
			}
			if (Data.szDeskConfig[2] - 48 == 1)
			{
				str_Rule.append("，1底分");
			}
			else if (Data.szDeskConfig[2] - 48 == 3)
			{
				str_Rule.append("，3底分");
			}
			else
			{
				str_Rule.append("，5底分");
			}
			if (Data.szDeskConfig[7] - 48 == 1)
			{
				str_Rule.append("，三带二可以带“2”");
			}
			else
			{
				str_Rule.append("，三带二不可以带“2”");
			}
			if (Data.szDeskConfig[8] - 48 == 1)
			{
				str_Rule.append("，显示剩余牌数");
			}
			else
			{
				str_Rule.append("，不显示剩余牌数");
			}
			if (Data.szDeskConfig[9] - 48 == 1)
			{
				str_Rule.append("，剩余1张报警");
			}
			else
			{
				str_Rule.append("，剩余3张报警");
			}
			if (Data.szDeskConfig[10] - 48 == 1)
			{
				str_Rule.append("，15张手牌");
			}
			else
			{
				str_Rule.append("，16张手牌");
			}
			if (Data.szDeskConfig[11] - 48 == 1)
			{
				str_Rule.append("，炸弹不可拆");
			}
			else
			{
				str_Rule.append("，炸弹可拆");
			}
			if (Data.szDeskConfig[12] - 48 == 0)
			{
				str_Rule.append("，可以四带二");
			}
			else if(Data.szDeskConfig[12] - 48 == 1)
			{
				str_Rule.append("，可以四带三");
			}
			else
			{
				str_Rule.append("，四张不可带");
			}
		}
		else if (Data.gameNameId == 20200311)
		{
			str_Name = "上游跑得快";
			if (Data.szDeskConfig[1] - 48 == 1)
			{
				str_Rule = "首局黑桃3先出";
			}
			else
			{
				str_Rule = "每局黑桃3先出";
			}
			if (Data.szDeskConfig[3] - 48 == 1)
			{
				str_Rule.append("，小头5大头10关牌20");
			}
			else if (Data.szDeskConfig[3] - 48 == 2)
			{
				str_Rule.append("，小头10大头20关牌30");
			}
			else if (Data.szDeskConfig[3] - 48 == 3)
			{
				str_Rule.append("，小头20大头30关牌50");
			}
			else
			{
				str_Rule.append("，小头20大头40关牌60");
			}
			if (Data.szDeskConfig[7] - 48 == 1)
			{
				str_Rule.append("，三带二可以带“2”");
			}
			else
			{
				str_Rule.append("，三带二不可以带“2”");
			}
			if (Data.szDeskConfig[8] - 48 == 1)
			{
				str_Rule.append("，显示剩余牌数");
			}
			else
			{
				str_Rule.append("，不显示剩余牌数");
			}
			if (Data.szDeskConfig[9] - 48 == 1)
			{
				str_Rule.append("，剩余1张报警");
			}
			else
			{
				str_Rule.append("，剩余3张报警");
			}
		}
		else if (Data.gameNameId == 12100004)
		{
			str_Name = "十三水";
			if (Data.szDeskConfig[1] - 48 == 0)
			{
				str_Rule = "通比玩法";
			}
			else if (Data.szDeskConfig[1] - 48 == 1)
			{
				str_Rule = "抢庄玩法";
			}
			else
			{
				str_Rule = "固定庄";
			}
			if (Data.szDeskConfig[2] - 48 == 1)
			{
				str_Rule.append("，不加鬼牌");
			}
			else 
			{
				str_Rule.append("，加鬼牌");
			}
			if (Data.szDeskConfig[4] - 48 == 0)
			{
				str_Rule.append("，加两门");
			}
			if (Data.szDeskConfig[6] - 48 == 0)
			{
				str_Rule.append("，加马牌");
			}
			else
			{
				str_Rule.append("，不加马牌");
			}
		}
		else if (Data.gameNameId == 20171121)
		{
			str_Name = "泉州麻将";
		}
		else if (Data.gameNameId == 20200309)
		{
			str_Name = "漳州麻将";
		}
		else if (Data.gameNameId == 20171123)
		{
			str_Name = "晋江麻将";
		}
		else if (Data.gameNameId == 10100003)
		{
			str_Name = "斗地主";
		}
		else if (Data.gameNameId == 12345678)
		{
			str_Name = "跑得快";
			if (Data.szDeskConfig[1] - 48 == 1)
			{
				str_Rule = "首局黑桃3先出";
			}
			else
			{
				str_Rule = "每局黑桃3先出";
			}
			if (Data.szDeskConfig[2] - 48 == 1)
			{
				str_Rule.append("，1底分");
			}
			else if (Data.szDeskConfig[2] - 48 == 3)
			{
				str_Rule.append("，3底分");
			}
			else
			{
				str_Rule.append("，5底分");
			}
			if (Data.szDeskConfig[3] - 48 == 0)
			{
				str_Rule.append("，没有大小头");
			}
			else if (Data.szDeskConfig[3] - 48 == 1)
			{
				str_Rule.append("，大头2小头1");
			}
			else if (Data.szDeskConfig[3] - 48 == 2)
			{
				str_Rule.append("，大头10小头5");
			}
			else
			{
				str_Rule.append("，大头20小头10");
			}
			if (Data.szDeskConfig[4] - 48 == 1)
			{
				str_Rule.append("，3A最小");
			}
			else
			{
				str_Rule.append("，3A最大");
			}

		}
		//茶楼游戏ID
		auto  Text_GameName = (Text*)Panel_TeaHouseInfo->getChildByName("Text_GameName");
		Text_GameName->setString(GBKToUtf8(str_Name));

		//茶楼最大人数
		auto Text_GamePlayer = (Text*)Panel_TeaHouseInfo->getChildByName("Text_GamePlayer");
		Text_GamePlayer->setString(StringUtils::format(GBKToUtf8("%d人房"), Data.maxPlayerCount));

		//茶楼游戏局数
		auto Text_GameCount = (Text*)Panel_TeaHouseInfo->getChildByName("Text_GameCount");
		Text_GameCount->setString(StringUtils::format(GBKToUtf8("%d局"), Data.maxGames));

		//茶楼游戏规则
		auto Text_GameRule = (Text*)Panel_TeaHouseInfo->getChildByName("Text_GameRule");
		Text_GameRule->setString(GBKToUtf8(str_Rule));

		//红花显示
		auto Text_RedF = (Text*)Panel_TeaHouseInfo->getChildByName("Text_RedF");
		Text_RedF->setVisible(Data.option.m_bOpenRedFlowersMode);

		//茶楼红花规则
		auto Text_RedFlower = (Text*)Panel_TeaHouseInfo->getChildByName("Text_RedFlower");
		Text_RedFlower->setVisible(Data.option.m_bOpenRedFlowersMode);
		//Text_RedFlower->setString(GBKToUtf8("默认红花玩法"));
		string str_RedFlower = "";
		if (Data.option.m_bOpenRedFlowersMode)
		{
			if (Data.option.m_Type == _TakeType_AA)
			{
				str_RedFlower = StringUtils::format(GBKToUtf8("1分等于%d朵花,进房最低红花数为%d,AA摘花:每人%d朵."),
					(int)Data.option.m_ScoreToRedFlowers, Data.option.m_MinRedFlowersOnJoinDesk, Data.option.m_AATakeCount);
			}
			else
			{
				str_RedFlower = StringUtils::format(GBKToUtf8("1分等于%d朵花,进房最低红花数为%d,"),
					(int)Data.option.m_ScoreToRedFlowers, Data.option.m_MinRedFlowersOnJoinDesk);
				if (Data.option.m_TakeScoreRanges[1] == 0)
				{
					str_RedFlower.append(StringUtils::format(GBKToUtf8("大赢家固定摘花:总分≥%d分摘%d朵."),
						Data.option.m_TakeScoreRanges[0], Data.option.m_TakeNumRanges[0]));
				}
				else
				{
					if (Data.option.m_TakeScoreRanges[2] == 0)
					{
						str_RedFlower.append(StringUtils::format(GBKToUtf8("大赢家阶梯摘花:总分≥%d分摘%d朵,总分≥%d分摘%d朵."),
							Data.option.m_TakeScoreRanges[0], Data.option.m_TakeNumRanges[0], Data.option.m_TakeScoreRanges[1], Data.option.m_TakeNumRanges[1]));
					}
					else if (Data.option.m_TakeScoreRanges[3] == 0)
					{
						str_RedFlower.append(StringUtils::format(GBKToUtf8("大赢家阶梯摘花:总分≥%d分摘%d朵,总分≥%d分摘%d朵,总分≥%d分摘%d朵."),
							Data.option.m_TakeScoreRanges[0], Data.option.m_TakeNumRanges[0], Data.option.m_TakeScoreRanges[1], Data.option.m_TakeNumRanges[1], Data.option.m_TakeScoreRanges[2], Data.option.m_TakeNumRanges[2]));
					}
					else if (Data.option.m_TakeScoreRanges[4] == 0)
					{
						str_RedFlower.append(StringUtils::format(GBKToUtf8("大赢家阶梯摘花:总分≥%d分摘%d朵,总分≥%d分摘%d朵,总分≥%d分摘%d朵,总分≥%d分摘%d朵."),
							Data.option.m_TakeScoreRanges[0], Data.option.m_TakeNumRanges[0], Data.option.m_TakeScoreRanges[1], Data.option.m_TakeNumRanges[1], Data.option.m_TakeScoreRanges[2], Data.option.m_TakeNumRanges[2], Data.option.m_TakeScoreRanges[3], Data.option.m_TakeNumRanges[3]));
					}
					else
					{
						str_RedFlower.append(StringUtils::format(GBKToUtf8("大赢家阶梯摘花:总分≥%d分摘%d朵,总分≥%d分摘%d朵,总分≥%d分摘%d朵,总分≥%d分摘%d朵,总分≥%d分摘%d朵."),
							Data.option.m_TakeScoreRanges[0], Data.option.m_TakeNumRanges[0], Data.option.m_TakeScoreRanges[1], Data.option.m_TakeNumRanges[1], Data.option.m_TakeScoreRanges[2], Data.option.m_TakeNumRanges[2], Data.option.m_TakeScoreRanges[3], Data.option.m_TakeNumRanges[3], Data.option.m_TakeScoreRanges[4], Data.option.m_TakeNumRanges[4]));
					}
				}
			}
			Text_RedFlower->setString(str_RedFlower);
		}
	}
}