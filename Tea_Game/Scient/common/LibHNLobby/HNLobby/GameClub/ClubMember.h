#ifndef _CLUB_MEMBER_
#define _CLUB_MEMBER_


#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"


USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class ClubMember :
	public Layer,
	public TableViewDataSource,
	public TableViewDelegate{

public:

	ClubMember();

	~ClubMember();

	virtual bool init();

	CREATE_FUNC(ClubMember);

private:

	//获取俱乐部成员数据
	bool getClubMemberInfoResult(HNSocketMessage* SocketMessage);
	//获取成员第三方头像数据
	void getClubMemberHeadurl();
	//创建TableView
	void createMemberView();
	//获取删除成员结果
	bool getDeleteMemberResult(HNSocketMessage* SocketMessage);
	// 网络请求
	void onResponseHeadUrl(int UserID);
	// http请求响应
	void onResponseUrl(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response);
private:
	//添加Cell
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

	//设置cell数量
	virtual ssize_t numberOfCellsInTableView(TableView* table);

	//设置cell大小
	virtual Size tableCellSizeForIndex(TableView* table, ssize_t idx);

	//设置触摸函数
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);

private:
	vector<MSG_GP_O_Club_UserList_Data>MemberInfo;
	vector<string>Vec_UserList_Headurl;
	int Members = 0;
	int OnlineNumber = 0;
	int _index = 0;
private:

	TableView* _TableView = nullptr;

	ui::ScrollView* _ScrollView = nullptr;

	Text* _ClubMembers = nullptr;

};
#endif