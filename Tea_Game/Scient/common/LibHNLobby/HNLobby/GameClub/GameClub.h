#ifndef _GAME_CLUB_
#define _GAME_CLUB_


#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "HNRoomLogic/HNRoomLogicBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class Club :
	public CCLayer,
	public TableViewDataSource,
	public HN::IHNRoomLogicBase,
	public TableViewDelegate{

public:
	
	Club();
	//
	~Club();
	//
	virtual bool init(int clubID);

	static Club* create(int clubID);

public:
	static Club* createClubScene(int clubID = 0);

	void showTeaHouse(MSG_GP_O_Club_List pData);

private:
	//加载视图
	void LoadClubView();

	//加载茶楼视图
	void LoadTeaHouse(int clubID);

	//创建俱乐部按钮回调
	void BtnCallBack(Ref* pSender);
	
	//创建俱乐部列表视图
	void CreateClubView();

	//创建房间层
	void createRoomLayer();
	
	//更新俱乐部财富信息
	void updateClubScore();

	//发送获取俱乐部列表请求
	void RequestClubView();

	//查询茶楼列表
	void RequestTeaHouse();

	//查询茶楼公告
	void updateClubNotice();

	//查询茶楼红花与权限
	void RequestTeaHouseUser();

	//是否有俱乐部
	void isHaveClub(bool isShow);

	//俱乐部公告显示
	void ClubNoticeShow(std::string &str);

	//我的创建与我的加入回调
	void SelectMyCreateOrJoinClub(int num);

	//顶部的俱乐部信息显示
	void SetTopClubInfo(vector<MSG_GP_O_Club_List>Clubinfo, int idx);

	//添加俱乐部网络消息监听事件
	void AddClubNetWorkNewsEventListener();

	//添加茶楼网络消息监听事件
	void AddTeaHouseNetWorkEventListener();

	//移除所有网络消息监听事件
	void RemoveAllNetWorkNewsEventListener();

	//移除茶楼网络消息监听事件
	void RemoveNetWorkNewsEventListener();

	//更新俱乐部房卡数量
	void updateClubJewels();

	//获取俱乐部房卡数量
	bool getClubJewels(HNSocketMessage* socketMessage);

	//查询俱乐部申请加入总人数
	void SendQueryJoinClubCount(int Clubid);

	//查询创建茶楼权限
	void SendQueryTeaHouseAccess();

	//创建茶楼权限返回
	bool GetTeaHouseAccess(HNSocketMessage* socketMessage);

	//获取俱乐部申请总人数
	bool GetJoinClubCount(HNSocketMessage* socketMessage);

	//获取俱乐部列表示视图结果
	bool getClubViewResult(HNSocketMessage* socketMessage);

	//获取茶楼列表结果
	bool getTeaHouseResult(HNSocketMessage* socketMessage);

	//获取房间列表视图结果
	bool getClubRoomViewResult(HNSocketMessage* socketMessage);

	//进入俱乐部开始接收消息结果
	bool getClubInfoResult(HNSocketMessage* SocketMessage);

	//查询红花权限消息结果
	bool getTeaHouseUserResult(HNSocketMessage* SocketMessage);

	//获取当前在线人数结果
	bool getTeaHouseOnlineNumResult(HNSocketMessage* SocketMessage);

	//获取俱乐部成员列表信息
	bool getClubMemberInfoResult(HNSocketMessage* SocketMessage);

	//申请退出俱乐部消息回调
	bool getExitClubResult(HNSocketMessage* SocketMessage);

	//解散俱乐部消息回调
	bool ClubDisbandResult(HNSocketMessage* SocketMessage);

	//创建结果
	bool CreateClubResult(HNSocketMessage* SocketMessage);

	//加入俱乐部结果
	bool JoinClubResult(HNSocketMessage* SocketMessage);

	//删除茶楼楼层结果
	bool DeleteTeaHouseResult(HNSocketMessage* SocketMessage);

	//修改俱乐部名称结果
	bool getChangeNameResult(HNSocketMessage* SocketMessage);

	//充值房卡数量结果
	bool onSendAddClubRoomCardResult(HNSocketMessage* SocketMessage);

	//发布公告结果
	bool getReleaseNoticeResult(HNSocketMessage* SocketMessage);

	//请求获取俱乐部房间列表
	void SendRequestClubRoomView();

	//请求房间列表回调
	bool getRoomViewResult(HNSocketMessage* SocketMessage);

	//转换一下桌子信息字符串
	int asciiTointNum(int num);

	//解散楼层提示(返回茶楼大厅)
	void initLobbyTeaHouse();

	void UpdateRoomList();

	
private:			
	//监听是否被踢
	bool IsSelfAtClub(HNSocketMessage* SocketMessage);

	//监听公告消息变动
	bool ClubNoticeChange(HNSocketMessage* SocketMessage);
	
	//房卡数量变化监听
	bool ClubRoomCardChange(HNSocketMessage* SocketMessage);
	
	//解散俱乐部网络消息监听
	bool ClubDisbandEventListener(HNSocketMessage* SocketMessage);

	//监听新用户加入俱乐部消息
	bool HaveNewUserJoinClub(HNSocketMessage* SocketMessage);

	//监听俱乐部成员变动，加入或退出
	bool ClubMemberChange(HNSocketMessage* SocketMessage);

	//房间监听
	bool RoomEventListener(HNSocketMessage* socketMessage);
private:
	//触摸事件
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);

	//Cell大小
	virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);

	//添加Cell
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

	//设置Cell个数
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	// 登陆房间回调
	virtual void onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode) override;
	virtual void onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo) override;
	virtual void onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo) override;

	//当前俱乐部信息
	MSG_GP_O_Club_List getClubInfoByClubID(int ClubID);

	//更新当前选择的茶楼信息
	void updateTeaHouseList();

	//创建茶楼节点
	void createTeaHouseNode(MSG_GP_O_Club_List TeaHouseData);

private:
	//更新茶楼信息（默认进入的茶楼和切换茶楼的时候）
	void UpdateTeaHouseInfo(MSG_GP_O_Club_List pData);

	//更新茶楼楼层信息（）
	void UpdateTeaHouseView();

	//更新茶楼桌子信息
	void UpdateTeaHouseTable();

	//更新茶楼在线人数
	void UpdateTeaHouseOnlineNum();

	//更新单个楼层信息
	//void UpdateTableByTeaHouseID();

	//更新大厅桌子信息
	void UpdateLobbyTeaHouse();

	//切换楼层
	void SwitchTeaHouseFloor(int Index);

	//修改楼层名字
	void SetSwitchTeaHouseName(int TeaHouseID);

	//打开楼层详情玩法
	void ShowTeaHouseInfo(int TeaHouseID);

	//初始化楼层桌子
	void InitTeaHouseFloor();

	//获取楼层名字
	std::string GetTeaHouseName(int index);

private:
	int ClubNums;			//俱乐部数量
	//图片集
	Vector<ImageView*> _ClubItemBg;
	//vector结构体
	vector<MSG_GP_O_Club_List>MyCreate;
	vector<MSG_GP_O_Club_List>MyJoin;
	//登录结束信息
	MSG_GP_R_LogonResult &Result = PlatformLogic()->loginResult;
	//登录房间链接
	HNRoomLogicBase*		_roomLogic = nullptr;

	//大厅连接
	//HNPlatformLogicBase* _platformlogic = nullptr;

	EventListenerCustom*	_foreground = nullptr;	// 切后台检测监听
	EventListenerCustom*	_listener = nullptr;
private:
///////////////////////////////////////////////////////////////////////
	bool m_bHaveTeaHouse = false;			//是否拥有茶楼
	int m_SelectedTeaHouseID = 0;			//当前的茶楼计数
	//茶楼视图
	Node* TeaHouseNode = nullptr;			//茶楼工程节点
	//------
	Layout* Panel_ClubInfo = nullptr;			//茶楼信息节点
	Text* Text_ClubName = nullptr;			//茶楼名字
	Text* Text_club_id = nullptr;			//茶楼ID
	Button*	Button_member = nullptr;		//成员管理
	ImageView* Image_Red_Member = nullptr;	//成员审核
	Button*	Button_teaSet = nullptr;		//群管理
	Button*	Button_Cooperation = nullptr;	//合作群
	Button*	Button_Member = nullptr;		//成员按钮（合作群）
	Text* Text_online_num = nullptr;			//在线玩家
	ImageView* Image_flow = nullptr;			//红花背景
	Text* Text_flow_num = nullptr;			//拥有的花数目
	Text* Text_tower = nullptr;				//当前楼层（大厅，一层。。。）
	Text* Text_notice = nullptr;				//公告内容
	Button* Button_notice = nullptr;			//是否显示公告
	ListView* ListView_Table = nullptr;		//茶楼桌子列表
	vector<Node*> Vec_Table;
	//----------
	int m_SelectedTeaHouse = 0;			//当前默认选中的楼层
	int m_MaxTeaHouseIndex = 0;			//大厅可切换的楼层数
	int Max_Player = 0;					//当前楼层最大游戏人数（单张桌子）
	int Max_palycount = 0;				//当前楼层最大局数
	int m_TeaHouseID = 0;				//当前选中的楼层ID
	INT	_roomID = -1;					//当前茶楼房间ID
	INT  _GameID = 0;					//当前茶楼游戏ID
	INT  _TableIndex = 255;				//当前桌子Index
	bool m_bFastJoin = false;				//是否选择快速加入
	Layout* Panel_FloorSwitch = nullptr;		//茶楼切换楼层
	ListView* ListView_FloorList = nullptr;		//切换茶楼列表
	//------------------
	Layout* Panel_ClubList = nullptr;			//茶楼列表（没有加入茶楼也用这个）
	ListView* ListView_club = nullptr;			//当前茶楼列表
	bool isSocketNow = false;
	//-------------------
	Layout* Panel_TeaHouseInfo = nullptr;
///////////////////////////////////////////////////////////////////////
	//房间列表
	ui::ScrollView* RoomView;
	ui::ListView* RoomListView;
	//俱乐部列表视图
	TableView* _TableView = nullptr;
	//俱乐部列表视图
	Layout* _ClubLayout = nullptr;
	//背景图片
	ImageView* Image_bg = nullptr;
	//设置区域背景图
	ImageView* _setView = nullptr;
	//左边节点
	Node* Node_left = nullptr;
	//顶部节点
	Node* Node_top = nullptr;
	//底部节点
	Node* Node_bottom = nullptr;
	//房间节点
	Node* Node_room = nullptr;
	//创建与加入节点
	Node* Node_createjoin = nullptr;
	//解散或退出按钮
	ImageView* btn_disband = nullptr;
	//红点
	ImageView* Image_red = nullptr;
	//俱乐部房卡
	Text* score = nullptr;
	//动画
	timeline::ActionTimeline* _timeLine = nullptr;
	//修改昵称

	
	ImageView* score_bg = nullptr;
	//
	ImageView* Image_left = nullptr;
	ImageView* Image_di = nullptr;
	ImageView* _mycretae = nullptr;
	ImageView* _myjoin = nullptr;
	Button* bottom_create = nullptr;

	//房间数据
	vector<MSG_GP_O_JJClub_RoomList_Data>RoomInfo;
	int ClubRooms = 0;
};
#endif