#ifndef _CLUB_OPERATE_
#define _CLUB_OPERATE_

#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class ClubOperate:
	public Layer{


public:
	ClubOperate();

	~ClubOperate();

	virtual bool init();

	CREATE_FUNC(ClubOperate);

public:
	void SetPlaceHolderCount(const std::string &count);

	void SetImageViewRes(int num);

	void SetCallBack(std::function<void ()>CallBack);

	void onButtonCallBack(Ref* pSender);

	void ShowIamgeOfPassWord(bool isShow);

	void onTextFiledCallBack(CCObject* pSender, TextFiledEventType type);
private:
	

	TextField*  _TextField = nullptr;

	std::function<void()>_Callback = nullptr;

	Button* Btn_define = nullptr;
	ImageView* Image_bg = nullptr;
	Sprite* Sprite_top = nullptr;
	ImageView* _ImagePassWord = nullptr;
};
#endif