#include "ClubItem.h"
#include "ClubMessage.h"
#include "ClubRoom.h"
#include "ClubMember.h"
#include "ClubTips.h"
#include "ClubOperate.h"
#include "ClubManag.h"
#include "ClubPartner.h"
#include "ClubUserRedFlow.h"
#include "../GameChildLayer/GameShareLayer.h"
#include "../GameRecord/RecordList/GameRoomRecordClub.h"

static const char* CLUB_CSB_PATH = "platform/Club/ClubScene.csb";
static const char* TEA_HOUSE_SCB_PATH = "platform/Club/GameClubLayer.csb";
static const char* CLUB_ITEM_PATH = "platform/Club/ClubItem.csb";
static const char* CLUB_NOTICE_PATH = "platform/Club/ClubNotice.csb";
static const char* USER_MEN_HEAD = "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";
static const char* USER_HEAD_MASK = "platform/common/touxiangdi.png";
static const char* NO_CLUB = "platform/Club/res/out_game/noclubbg.png";
static const char* HAVE_CLUB = "platform/Club/res/out_game/bg.png";
static const char* allocation_table_please_wait_text = "正在配桌，请稍后......";
static const char* enterGame_please_wait_text = "正在进入游戏，请稍后......";

ClubItem::ClubItem()
: _clubID(0)
, _max_Player(0)
, _max_palycoun(0)
, _teaHouseID(0)
, _roomID(0)
, _gameID(0)
, _tableIndex(0)
{

}

ClubItem::~ClubItem()
{

}

ClubItem* ClubItem::create(int clubID, SClubTeahouseInfo teahouseInfo, SClubTeahouseDeskInfo deskInfo)
{
	ClubItem* ret = new ClubItem();
	if (ret && ret->init(clubID, teahouseInfo, deskInfo))
	{
		ret->autorelease();
		return ret;
	}
	else
	{
		delete ret;
		ret = nullptr;
		return ret;
	}

}

bool ClubItem::init(int clubID, SClubTeahouseInfo teahouseInfo, SClubTeahouseDeskInfo deskInfo)
{
	if (!Node::init()) return false;

	_clubID = clubID;
	_max_Player = teahouseInfo.maxPlayerCount;
	_max_palycoun = teahouseInfo.maxGames;
	_teaHouseID = teahouseInfo.teahouseId;
	_roomID = teahouseInfo.roomId;
	_gameID = teahouseInfo.gameNameId;
	_tableIndex = deskInfo.deskIndex;

	auto node = CSLoader::createNode("platform/Club/FloorTableNode.csb");
	auto panel_clone = (Layout*)node->getChildByName("Panel_clone");
	panel_clone->removeFromParentAndCleanup(true);
	addChild(panel_clone);

	setAnchorPoint(CCPointMake(0.5f, 0.5f));
	setContentSize(panel_clone->getContentSize());
	panel_clone->setPosition(getContentSize() / 2);

	auto Text_name_0 = (Text*)panel_clone->getChildByName("Text_name_0");
	Text_name_0->setString(GBKToUtf8(teahouseInfo.szName));
	Text_name_0->setVisible(true);

	

	string str = StringUtils::format("Image_Play_%d", (_max_Player < 6 ? _max_Player : 6));
	auto Table = (ImageView*)panel_clone->getChildByName(str);
	if (Table)
	{
		int countPlayer = 0;
		//加载用户头像
		for (int j = 1; j <= _max_Player; j++)
		{
			auto head = (ImageView*)Table->getChildByName(StringUtils::format("Image_head_%d", j));
			if (j < 6 && deskInfo.userIDList[j - 1] != 0)
			{
				countPlayer++;
				if (head) 
				{
					auto HeadView = GameUserHead::create(head);
					HeadView->show(USER_HEAD_MASK);//添加底图
					//判断是男还是女，没有网络头像情况下
					HeadView->loadTexture(USER_MEN_HEAD);
					HeadView->loadTextureWithUrl(
						HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(
						deskInfo.userIDList[j - 1]));//设置网络头像
				}
			}
			else
			{
				if (head) head->setVisible(false);
			}
		}

		//加载桌子信息
		auto Image_table_state = (ImageView*)Table->getChildByName("Image_table_state");
		Image_table_state->loadTexture(deskInfo.deskState == 3 ?
			"platform/Club/res/out_game/word_yxz.png"
			: "platform/Club/res/out_game/word_ddz.png");

		//人数加载
		auto Text_player = (Text*)Table->getChildByName("Text_player");
		Text_player->setString(StringUtils::format("%d/%d", countPlayer, _max_Player));

		//当前局数
		auto Text_play_count = (Text*)Table->getChildByName("Text_play_count");
		if (deskInfo.deskState == 3)
		{
			Text_play_count->setString(StringUtils::format(GBKToUtf8("第%d/%d局"), deskInfo.curGames, _max_palycoun));
			Text_play_count->setVisible(true);
		}
		else
		{
			Text_play_count->setVisible(false);
		}
		Table->setVisible(true);
	}

	//加入桌子按钮
	auto Button_join_table = (Button*)panel_clone->getChildByName("Button_join_table");
	Button_join_table->setSwallowTouches(false);
	Button_join_table->addClickEventListener([=](Ref*){

		if (onClubItemCallBackLite) 
		{
			onClubItemCallBackLite(_clubID, _max_Player, _max_palycoun, _teaHouseID, _roomID, _gameID, _tableIndex);
		}
	});

	return true;
}

