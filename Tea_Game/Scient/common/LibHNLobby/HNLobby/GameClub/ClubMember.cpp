#include "ClubMember.h"
#include "ClubTips.h"
static const char* CLUB_MEMBER_PATH = "platform/Club/ClubMember.csb";
static const char* MEMBER_ITEM_PATH = "platform/Club/ClubMemberItem.csb";
static const char* USER_MEN_HEAD = "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";
static const char* USER_HEAD_MASK = "platform/common/touxiangdi.png";
extern int ClubID;
extern bool isClubCreater;
int CreaterID;
ClubMember::ClubMember()
{

}

ClubMember::~ClubMember()
{

}

bool ClubMember::init()
{
	if (!Layer::init()) return false;

	//创建csb文件
	auto Node = CSLoader::createNode(CLUB_MEMBER_PATH);
	//添加节点
	addChild(Node);
	//背景
	auto Image_bg = dynamic_cast<ImageView*>(Node->getChildByName("Image_bg"));
	//加载滑动列表
	auto Panel = dynamic_cast<Layout*>(Node->getChildByName("Panel_1"));
	//成员总数
	_ClubMembers = dynamic_cast<Text*>(Image_bg->getChildByName("Text_members"));
	//加载滑动列表
	_ScrollView = dynamic_cast<ui::ScrollView*>(Image_bg->getChildByName("ScrollView_member"));

	//设置触摸区域外关闭
	{
		Panel->addClickEventListener([=](Ref*){
			this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
				this->removeFromParent();
			}), nullptr));
		});
	}

	//获取俱乐部成员
	{
		if (MemberInfo.size() != 0) MemberInfo.clear();
		MSG_GP_I_Club_UserList ID;
		ID.iClubID = ClubID;
		PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_USERLIST, &ID, sizeof(MSG_GP_I_Club_UserList), HN_SOCKET_CALLBACK(ClubMember::getClubMemberInfoResult, this));
	}

	return true;
}


bool ClubMember::getClubMemberInfoResult(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		auto head = (MSG_GP_O_Club_UserList_Head*)SocketMessage->object;
		CreaterID = head->iCreaterID;
		int num = (SocketMessage->objectSize - sizeof(MSG_GP_O_Club_UserList_Head)) / (sizeof(MSG_GP_O_Club_UserList_Data));
		Members = Members + num;
		auto  Data = (MSG_GP_O_Club_UserList_Data*)(SocketMessage->object + sizeof(MSG_GP_O_Club_UserList_Head));
	
		if (num)
		{
			while (num--)
			{
				if (Data->iOnlineFlag == 1)
				{
					OnlineNumber = OnlineNumber + 1;
				}
				MemberInfo.push_back(*Data);
				Data++;
			}
			//数据接收完成后执行用户第三方头像获取
			//getClubMemberHeadurl();
			if (_TableView != nullptr) _TableView->reloadData();
			else createMemberView();
		}
		else{
			if (_TableView != nullptr) _TableView->reloadData();
		}
	}
	return true;
}

//请求第三方头像数据
void ClubMember::getClubMemberHeadurl()
{
	//请求数据前清空之前的数据
	if (Vec_UserList_Headurl.size() != 0) Vec_UserList_Headurl.clear();
	if (MemberInfo.size() != 0)
	{
		/*for (auto user : MemberInfo)
		{*/
		onResponseHeadUrl(MemberInfo[0].iUserID);
		//}
	}
	//LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("加载中"), 22);
}

// 网络请求
void ClubMember::onResponseHeadUrl(int UserID)
{
	std::string path = StringUtils::format("/hyapp/get_head_url.php?userid=%d", UserID);
	std::string url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
	HttpRequest* request = new (std::nothrow) HttpRequest();
	request->setUrl(url);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(CC_CALLBACK_2(ClubMember::onResponseUrl, this));
	request->setTag("requestUserHeadUrl");

	HttpClient::getInstance()->sendImmediate(request);
	request->release();
}

// http请求响应
void ClubMember::onResponseUrl(cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response)
{
	std::string requestName = response->getHttpRequest()->getTag();
	std::vector<char> *data = response->getResponseData();
	int data_length = data->size();
	std::string responseData;
	for (int i = 0; i < data_length; ++i)
	{
		responseData += (*data)[i];
	}
	responseData += '\0';
	if (requestName.compare("requestUserHeadUrl") == 0)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}
		_index = _index + 1;
		//存在微信头像
		int code = doc["IsWX"].GetInt();
		if (code == 1)
		{
			std::string url = doc["HeadUrl"].GetString();
			Vec_UserList_Headurl.push_back(url);
			//loadTextureWithUrl(url);
		}
		else
		{
			std::string url = doc["HeadUrl"].GetString();
			Vec_UserList_Headurl.push_back(url);
		}
		if (_index < MemberInfo.size())
		{
			onResponseHeadUrl(MemberInfo[_index].iUserID);
		}
	}
	if (Vec_UserList_Headurl.size() == MemberInfo.size())
	{
		_index = 0;
		if (_TableView != nullptr) _TableView->reloadData();
		else createMemberView();
		//LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}
}

//创建TableView
void ClubMember::createMemberView()
{
	//
	_TableView = TableView::create(this, _ScrollView->getContentSize());
	//
	_TableView->setDirection(TableView::Direction::VERTICAL);
	//
	_TableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//
	_TableView->setDelegate(this);
	//
	_ScrollView->addChild(_TableView);

}



TableViewCell* ClubMember::tableCellAtIndex(TableView* table, ssize_t idx)
{
	TableViewCell* cell = table->dequeueCell();
	Node* item = nullptr;
	if (!cell)
	{
		cell = new TableViewCell();
		//创建成员item
		item = CSLoader::createNode(MEMBER_ITEM_PATH);
		//设置位置
		item->setPosition(Vec2(0, 0));
		//设置成员TAG
		item->setTag(1);
		//添加到cell中
		cell->addChild(item);
	}
	else
	{
		item = cell->getChildByTag(1);
	}
	
	//加载背景
	auto image_bg = dynamic_cast<ImageView*>(item->getChildByName("Item_member"));
	//用户名字
	auto name = dynamic_cast<Text*>(image_bg->getChildByName("Text_name"));
	//用户ID
	auto id = dynamic_cast<Text*>(image_bg->getChildByName("Text_id"));
	//用户头像
	auto head = dynamic_cast<ImageView*>(image_bg->getChildByName("Image_head"));
	//用户头像框
	auto frame = dynamic_cast<ImageView*>(image_bg->getChildByName("Image_frame"));
	//用户状态（离线还是在线）
	auto OnlineState = dynamic_cast<ImageView*>(image_bg->getChildByName("Image_OnlineState"));
	//删除按钮
	auto btn_delete = dynamic_cast<ImageView*>(image_bg->getChildByName("Button_delete"));
	//设置ID
	id->setString(GBKToUtf8(StringUtils::format("ID:%d", MemberInfo[idx].iUserID)));
	name->setString(GBKToUtf8(MemberInfo[idx].szUserNickName));
	//创建头像
	auto HeadView = GameUserHead::create(head, frame);
	HeadView->show(USER_HEAD_MASK);//添加底图
	HeadView->loadTexture(MemberInfo[idx].bSex ? USER_MEN_HEAD : USER_WOMEN_HEAD);//判断是男还是女，没有网络头像情况下剩下
	//if (Vec_UserList_Headurl.size() == MemberInfo.size())
	//{
	//	HeadView->loadTextureWithUrl(Vec_UserList_Headurl[idx]);//设置网络头像
	//}
	//HeadView->onResponseHeadUrl(MemberInfo[idx].iUserID);
	HeadView->loadTextureWithUrl(HNPlatformClubData::getInstance()->getUserHeadUrlByUserID(MemberInfo[idx].iUserID));

	if (MemberInfo[idx].iOnlineFlag == 0)
	{
		OnlineState->loadTexture("platform/Club/res/new/Lixian.png");
	}
	else
	{
		OnlineState->loadTexture("platform/Club/res/new/Zaixian.png");
	}
	
	auto SendDelete = [=](){MSG_GP_I_Club_KickUser Request;
	Request.iClubID = ClubID;
	Request.iTargetID = MemberInfo[idx].iUserID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_KICKUSER, &Request, sizeof(MSG_GP_I_Club_KickUser), HN_SOCKET_CALLBACK(ClubMember::getDeleteMemberResult, this)); };
	
	btn_delete->addClickEventListener([=](Ref*){
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		//ClubTipsLayer->autorelease();
		ClubTipsLayer->setTextCount(GBKToUtf8("确认删除此成员吗?"));
		ClubTipsLayer->SetImageViewRes(1);
		ClubTipsLayer->setCallBack([=](){
			SendDelete();
		});
	});

	//设置俱乐部总共有多少成员(更换显示在线人数)
	_ClubMembers->setString(StringUtils::format("%d/%d", OnlineNumber, Members));
	//不是创始人按钮不可见
	if (!isClubCreater)btn_delete->setVisible(false);
	//俱乐部成员第一个为群主，不能删除改成其他图片资源
	if (MemberInfo[idx].iUserID == CreaterID)
	{
		btn_delete->loadTexture("platform/Club/res/new/csr.png");
		btn_delete->ignoreContentAdaptWithSize(true);
		btn_delete->setTouchEnabled(false);
		btn_delete ->setVisible(true);
	}
	return cell;
}

//返回 Item 大小
Size ClubMember::tableCellSizeForIndex(TableView* table, ssize_t index)
{
	return Size(535.00, 75);
}

//触摸函数
void ClubMember::tableCellTouched(TableView* table, TableViewCell* cell)
{
	
}

//返回Cell总个数
ssize_t ClubMember::numberOfCellsInTableView(TableView* table)
{
	return Members;
}


bool ClubMember::getDeleteMemberResult(HNSocketMessage* SocketMessage)
{	
	switch (SocketMessage->messageHead.bHandleCode)
	{
	case(0) :
	{
				if (MemberInfo.size() != 0) MemberInfo.clear();
				MSG_GP_O_Club_KickUser* Result = (MSG_GP_O_Club_KickUser*)SocketMessage->object;
				auto UserID = Result->iTargetID;
				MSG_GP_I_Club_UserList ID;
				ID.iClubID = ClubID;
				//删除用户之后获取列表需要刷新成员数目
				Members = 0;
				OnlineNumber = 0;
				PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_USERLIST, &ID, sizeof(MSG_GP_I_Club_UserList), HN_SOCKET_CALLBACK(ClubMember::getClubMemberInfoResult, this));
	}break;
	case(2) :
	{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("部落ID错误"));
	}break;
	case(3) :
	{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("玩家不是会长"));

	}break;
	}

	return true;
}

