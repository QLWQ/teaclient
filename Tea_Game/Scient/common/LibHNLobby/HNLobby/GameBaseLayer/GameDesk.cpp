/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameDesk.h"
#include "../GameChildLayer/GamePasswordInput.h"
#include "HNUIExport.h"

//////////////////////////////////////////////////////////////////////////
static const char* DESKUI_CSB = "platform/lobbyUi/desklist_Node.csb";
static const char* DESK_ITEM_UI = "platform/lobbyUi/deskItem_Node.csb";
static const char* GAME_PAGESPROMPT_PATH = "platform/common/yuandian1.png";

static const Size pageViewDesksSize(1280, 500);

//////////////////////////////////////////////////////////////////////////

GameDesk* GameDesk::createDesk(ComRoomInfo* roomInfo)
{
	GameDesk* deskList = new GameDesk();
	if (deskList->initData(roomInfo))
	{
		deskList->autorelease();
		return deskList;
	}
	else
	{
		CC_SAFE_DELETE(deskList);
		return nullptr;
	}
}

GameDesk::GameDesk() 
	: _currentSelectedDesk(nullptr)
	, _deskNO (0)
	, _pageLen (0)
	, _canCreate (true)
	, _pageEven (false)
	, _isTouch (true)
{
	
}

GameDesk::~GameDesk()
{
	_deskLogic->stop();
	HN_SAFE_DELETE(_deskLogic);

	for (auto desk : _deskinfos)
	{
		HN_SAFE_DELETE(desk);
	}
}

void GameDesk::closeFunc()
{
	_deskLogic->stop();
	RoomLogic()->close();
	PlatformConfig::getInstance()->setSceneState(PlatformConfig::SCENE_STATE::INPLATFORM);

	if (onCloseCallBack) onCloseCallBack();
}

void GameDesk::onEnterPasswordCallback(const std::string& password)
{
	DeskInfo* deskInfo = static_cast<DeskInfo*>(_currentSelectedDesk->getUserData());
	_deskLogic->requestSit(deskInfo->deskID, password);
}

bool GameDesk::initData(ComRoomInfo* roomInfo)
{
	if (!HNLayer::init()) return false;

	PlatformConfig::getInstance()->setSceneState(PlatformConfig::SCENE_STATE::INROOM);

	auto winSize = Director::getInstance()->getWinSize();

	setCascadeOpacityEnabled(true);

	createDeskList(roomInfo);

	_deskLogic = new HNRoomDeskLogic(this);
	_deskLogic->start();

	return true;
}

void GameDesk::onExit()
{
	HNLayer::onExit();
}

// 创建牌桌列表
void GameDesk::createDeskList(ComRoomInfo* roomInfo)
{
	_roomInfo = roomInfo;
	
	float scalex = 1280 / _winSize.width;
	float scaley = 720 / _winSize.height;

	auto node = CSLoader::createNode(DESKUI_CSB);
	node->setPosition(_winSize / 2);
	addChild(node);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_desks"));
	layout->setScale(_winSize.width / 1280, _winSize.height / 720);

	for (auto child : layout->getChildren())
	{
		child->setScale(scalex, scaley);
	}

	auto img_top = dynamic_cast<ImageView*>(layout->getChildByName("Image_top"));

	// 返回按钮
	auto btn_return = dynamic_cast<Button*>(img_top->getChildByName("Button_return"));
	btn_return->addClickEventListener([=](Ref*) {
		close();
	});

	// 金币
	auto img_gold = dynamic_cast<ImageView*>(img_top->getChildByName("Image_gold"));
	auto text_gold = dynamic_cast<TextAtlas*>(img_gold->getChildByName("AtlasLabel_value"));
	text_gold->setString(std::to_string(PlatformLogic()->loginResult.i64Money));

	// 奖券
	auto img_lottery = dynamic_cast<ImageView*>(img_top->getChildByName("Image_lottery"));
	auto text_lottery = dynamic_cast<TextAtlas*>(img_lottery->getChildByName("AtlasLabel_value"));
	text_lottery->setString(std::to_string(PlatformLogic()->loginResult.iLotteries));

	// 桌子列表
	_pageViewDesks = dynamic_cast<PageView*>(layout->getChildByName("PageView_desks"));
	_pageViewDesks->setCustomScrollThreshold(15);
	_pageViewDesks->setCascadeOpacityEnabled(true);
	_pageViewDesks->scrollToPage(0);
	_pageViewDesks->removeAllPages();
	_pageViewDesks->setIndicatorEnabled(true);
	_pageViewDesks->setIndicatorIndexNodesTexture(GAME_PAGESPROMPT_PATH);
	_pageViewDesks->setIndicatorIndexNodesScale(0.85f);
	_pageViewDesks->setIndicatorSelectedIndexColor(Color3B::WHITE);
	_pageViewDesks->setIndicatorPosition(Vec2(_pageViewDesks->getContentSize().width * 0.5f, _pageViewDesks->getContentSize().height * 0.04f));

	_pageEven = ((_roomInfo->uDeskCount % 6) == 0);
	_pageLen = _roomInfo->uDeskCount / 6;

	createDeskPage();
}

void GameDesk::createDeskPage()
{
	if (!_canCreate) return;

	if (_pageLen > 0)
	{
		_deskinfos.clear();
		for (int m = 0; m < 6; m++)
		{
			createDeskPageInfo(_roomInfo);
			_deskNO++;
		}
		createDeskListPage(_deskinfos);
		_pageLen--;
		if (_pageEven && 0 == _pageLen)
		{
			_canCreate = false;
		}
	}

	if (0 == _pageLen && !_pageEven)
	{
		_deskinfos.clear();
		INT num = _roomInfo->uDeskCount % 6;
		for (int i = 0; i < num; i++)
		{
			createDeskPageInfo(_roomInfo);
			_deskNO++;
		}
		createDeskListPage(_deskinfos);
		_canCreate = false;
	}
	_deskinfos.clear();

	this->runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([this](){
		createDeskPage();
	}), nullptr));
}

// 创建牌桌页面数据
void GameDesk::createDeskPageInfo(ComRoomInfo* roomInfo)
{
	DeskInfo* desk = new DeskInfo;
	memset(desk, 0x0, sizeof(desk));

	if (nullptr != desk)
	{
		desk->deskID			= _deskNO;
		desk->multiple			= roomInfo->iBasePoint;
		desk->goldMin			= roomInfo->iLessPoint;
		desk->goldMax			= roomInfo->iMaxPoint;
		desk->peopleMax			= roomInfo->uDeskPeople;

		// 桌上人数（是否虚拟桌子）
		desk->peopleValue = RoomLogic()->deskStation.bVirtualDesk[desk->deskID] ? roomInfo->uDeskPeople : RoomLogic()->deskStation.bUserCount[desk->deskID];

		// 锁桌（是否加密）
		desk->isLocked = RoomLogic()->deskStation.bDeskLock[desk->deskID];

		// 游戏是否进行中
		desk->isPlaying = RoomLogic()->deskStation.bDeskStation[desk->deskID];
	}
	_deskinfos.push_back(desk);
	_allDeskInfo.push_back(desk);
}

// 创建牌桌列表page
void GameDesk::createDeskListPage(std::vector<DeskInfo*> deskinfos)
{
	//创建房间列表子页面
	auto deskListLayout = Layout::create();
	deskListLayout->setName("page");
	deskListLayout->setContentSize(_pageViewDesks->getContentSize());
	deskListLayout->setPassFocusToChild(true);

	float pageWidth = _pageViewDesks->getContentSize().width;
	float pageHeight = _pageViewDesks->getContentSize().height;

	int idx = 0;
	for (auto desk : deskinfos)
	{
		idx++;
		auto deskNode = addDesk(desk);
		auto button = (Button*)deskNode->getChildByName("Button_desk");
		button->removeFromParentAndCleanup(false);

		float posX = idx % 2 ? 0.26f : 0.74f;
		float posY = 0.0f;
		if (idx <= 2) posY = 0.83f;
		else if (idx > 2 && idx <= 4) posY = 0.54f;
		else posY = 0.25f;

		button->setPosition(Vec2(pageWidth * posX, pageHeight * posY));
		deskListLayout->addChild(button);

		_allDeskUI.push_back(button);
	}

	// 添加子页面进入列表中
	_pageViewDesks->addPage(deskListLayout);
}

// 添加桌子
Node* GameDesk::addDesk(DeskInfo* deskInfo)
{
	auto deskItemNode = CSLoader::createNode(DESK_ITEM_UI);
	if (!deskItemNode) return nullptr;

	auto deskItem = (Button*)deskItemNode->getChildByName("Button_desk");
	if (!deskItem) return nullptr;

	deskItem->setScale(0.9f);
	deskItem->setUserData(deskInfo);
	deskItem->setPropagateTouchEvents(true);
	deskItem->addTouchEventListener(CC_CALLBACK_2(GameDesk::enterTableEventCallBack, this));

	// 桌名
	auto text_deskNo = (TextAtlas*)Helper::seekWidgetByName(deskItem, "AtlasLabel_deskNo");
	text_deskNo->setString(std::to_string(deskInfo->deskID + 1));

	// 桌上人数
	auto textAtlas_people = (TextAtlas*)Helper::seekWidgetByName(deskItem, "AtlasLabel_count");
	textAtlas_people->setString(StringUtils::format("%d/%d", deskInfo->peopleValue, deskInfo->peopleMax));

	// 人数进度条
	float count =  deskInfo->peopleValue * 100.f / deskInfo->peopleMax;
	auto progress = (LoadingBar*)Helper::seekWidgetByName(deskItem, "LoadingBar_count");
	progress->setPercent(count);

	// 房间人数
	auto text_PeopleCount = (Text*)Helper::seekWidgetByName(deskItem, "Text_count");
	text_PeopleCount->setString(StringUtils::format(GBKToUtf8("%d人在玩"), deskInfo->peopleValue));

	// 金币限制
	auto text_limit = (Text*)Helper::seekWidgetByName(deskItem, "Text_limit");
	if (text_limit)
	{
		std::string str;

		if (0 == deskInfo->goldMin)
		{
			str = GBKToUtf8("无限制");
		}
		else if (0 == deskInfo->goldMax)
		{
			if (deskInfo->goldMin <= 10000)
			{
				str = StringUtils::format(GBKToUtf8("%d准入"), deskInfo->goldMin);
			}
			else if (deskInfo->goldMin >= 10000 && deskInfo->goldMin < 100000000)
			{
				str = StringUtils::format(GBKToUtf8("%.2f万+"), deskInfo->goldMin / 10000.f);
			}
			else
			{
				str = StringUtils::format(GBKToUtf8("%.2f亿+"), deskInfo->goldMin / 100000000.f);
			}
		}
		else
		{
			if (deskInfo->goldMax < 10000)
			{
				str = StringUtils::format(GBKToUtf8("%d-%d准入"), deskInfo->goldMin, deskInfo->goldMax);
			}
			else if (deskInfo->goldMax >= 10000 && deskInfo->goldMax < 100000000)
			{
				if (deskInfo->goldMin < 10000)
				{
					str = StringUtils::format(GBKToUtf8("%d-%.2f万"), deskInfo->goldMin, deskInfo->goldMax / 10000.f);
				}
				else
				{
					str = StringUtils::format(GBKToUtf8("%.2f万-%.2f万"), deskInfo->goldMin / 10000.f, deskInfo->goldMax / 10000.f);
				}
			}
			else
			{
				if (deskInfo->goldMin < 10000)
				{
					str = StringUtils::format(GBKToUtf8("%d-%.2f亿"), deskInfo->goldMin, deskInfo->goldMax / 100000000.f);
				}
				else if (deskInfo->goldMin >= 10000 && deskInfo->goldMin < 100000000)
				{
					str = StringUtils::format(GBKToUtf8("%.2f万-%.2f亿"), deskInfo->goldMin / 10000.f, deskInfo->goldMax / 100000000.f);
				}
				else
				{
					str = StringUtils::format(GBKToUtf8("%.2f亿-%.2f亿"), deskInfo->goldMin / 100000000.f, deskInfo->goldMax / 100000000.f);
				}
			}
		}
		text_limit->setString(str);
	}
	return deskItemNode;
}

// 进入游戏桌点击回调
void GameDesk::enterTableEventCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (!_isTouch) return;

	_currentSelectedDesk = dynamic_cast<Button*>(pSender);

	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		_currentSelectedDesk->setColor(Color3B(170, 170, 170));
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
		{
			_isTouch = false;
			this->runAction(Sequence::create(DelayTime::create(15.0f), CallFunc::create([=]()
			{
				_isTouch = true;
			}), nullptr));

			_currentSelectedDesk->setColor(Color3B(255, 255, 255));
			HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

			DeskInfo* deskInfo = static_cast<DeskInfo*>(_currentSelectedDesk->getUserData());

			// 参数校验
			CCAssert(nullptr != deskInfo, "desk is nullptr!");
			if (nullptr == deskInfo)
			{
				_isTouch = true;
				return;
			}

			if (deskInfo->peopleValue == deskInfo->peopleMax)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("桌子人数已满。"));
				_isTouch = true;
				return;
			}

			// 密码桌子
			if(deskInfo->isLocked)
			{
				GamePasswordInput* layer = GamePasswordInput::create();
				layer->setPosition(_winSize / 2);
				this->addChild(layer, 1000);
				layer->onEnterCallback = CC_CALLBACK_1(GameDesk::onEnterPasswordCallback, this);
				_isTouch = true;
				return;
			}

			_deskLogic->requestQuickSit(deskInfo->deskID);
		}
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		_currentSelectedDesk->setColor(Color3B(255, 255, 255));
		break;
	default:
		break;
	}
}

void GameDesk::updateLockDesk()
{
	for(auto &deskInfo: _allDeskInfo)
	{
		deskInfo->isLocked = RoomLogic()->deskStation.bDeskLock[deskInfo->deskID];
	}
}

void GameDesk::onDeskSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo)
{
	_isTouch = true;

	if(success)
	{
		ComRoomInfo* pRoomInfo = RoomLogic()->getSelectedRoom();
		if(!pRoomInfo)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("查找房间失败。"));
		}
		bool bRet = GameCreator()->startGameClient(pRoomInfo->uNameID, deskNo, true);
		if (!bRet)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"));
		}
		else
		{
			_deskLogic->stop();
		}
	}
	else
	{
		GamePromptLayer::create()->showPrompt(message);
	}
}

void GameDesk::onUpdateVirtualDesk(BYTE deskNo, bool isVirtual)
{
	updateDeskPeopleCount(deskNo, isVirtual);
}

void GameDesk::onRoomDeskUserCountChanged(BYTE deskNo)
{
	updateLockDesk();

	updateDeskPeopleCount(deskNo, false);
}

//更新游戏人数
void GameDesk::updateDeskPeopleCount(BYTE deskNo, bool isLock)
{
	if (!_pageViewDesks || _allDeskUI.empty()) return;

	if (deskNo > _allDeskUI.size() - 1) return;

	DeskInfo* info = static_cast<DeskInfo*>(_allDeskUI[deskNo]->getUserData());

	if (isLock)
	{
		info->peopleValue = info->peopleMax;
	}
	else
	{
		info->peopleValue = RoomLogic()->deskStation.bUserCount[info->deskID];
	}

	// 桌上人数
	auto textAtlas_people = (TextAtlas*)Helper::seekWidgetByName(_allDeskUI[deskNo], "AtlasLabel_count");
	textAtlas_people->setString(StringUtils::format("%d/%d", info->peopleValue, info->peopleMax));

	// 人数进度条
	float count = info->peopleValue * 100.f / info->peopleMax;
	auto progress = (LoadingBar*)Helper::seekWidgetByName(_allDeskUI[deskNo], "LoadingBar_count");
	progress->setPercent(count);

	// 房间人数
	auto text_PeopleCount = (Text*)Helper::seekWidgetByName(_allDeskUI[deskNo], "Text_count");
	text_PeopleCount->setString(StringUtils::format(GBKToUtf8("%d人在玩"), info->peopleValue));
}

// 更新桌子状态（是否游戏中）
void GameDesk::onUpdateDeskState(BYTE deskNo, bool isPlaying)
{

}

void GameDesk::refresh()
{
	_roomInfo = RoomLogic()->getSelectedRoom();

	_deskNO = 0;
	_canCreate = true;
	_deskinfos.clear();
	_allDeskUI.clear();
	_allDeskInfo.clear();
	_pageViewDesks->removeAllPages();

	_pageEven = ((_roomInfo->uDeskCount % 6) == 0);
	_pageLen = _roomInfo->uDeskCount / 6;

	createDeskPage();
}