/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GameRoom_SCENE_H__
#define __GameRoom_SCENE_H__



#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNLogicExport.h"
#include "GameLading.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class GameRoom : public HNLayer, public HN::IHNRoomLogicBase
{
public:
	typedef std::function<void()> CloseCallBack;
	CloseCallBack			onCloseCallBack = nullptr;

	typedef std::function<void(ComRoomInfo* roomInfo)> EnterDeskCallBack;
	EnterDeskCallBack		onEnterDeskCallBack = nullptr;

	typedef std::function<void()> ReturnGamesView;
	ReturnGamesView			_returnCallBack = nullptr;

	typedef std::function<void()> NoRooms;
	NoRooms  _NoRoomCallBack;

	HNRoomLogicBase*		_roomLogic = nullptr;

private:
	PageView*				_pageViewRooms = nullptr;	// 房间列表
	Layout*				_pageBjlViewRooms = nullptr;  // 百家乐房间列表
	Button*					_currentSelectedRoom = nullptr;	// 列表按钮
	Button*					_btn_Inter = nullptr;//百家乐进入房间
	std::vector<Button*>	_allRoomUI;							// 所有房间
	std::vector<Layout*>	_BjlRoomUI;							//百家乐房间
	TextAtlas*				_text_gold = nullptr;	// 金币
	Layout*_layout = nullptr;
	Layout* _mPanelBjl = nullptr;
	MoneyChangeNotify*				_delegate;

	INT						_roomID = -1;
	CC_SYNTHESIZE_PASS_BY_REF(bool, _isTouch, CanTouch);

public:
	GameRoom();
	virtual ~GameRoom();

	static GameRoom* createGameRoom(MoneyChangeNotify* delegate);
	void runActionByRight();

	void QuickJionGame();
	void AddImagive();
public:
	virtual bool init() override;

	virtual void onExit() override;

	// 界面回调接口
public:
	// 列表按钮点击回调函数
	void enterRoomEventCallBack(Ref* pSender, Widget::TouchEventType type);

	// 房间密码输入回调
	void onEnterPasswordClickCallback(const std::string& password);

	// 功能接口
private:
	void closeFunc() override;

	// 创建房间列表
	void createRoomList();
	// 创建房间
	void createRoomPage(std::vector<ComRoomInfo*> pages);
	// 创建百家乐房间
	void createBjleRoomPage(std::vector<ComRoomInfo*> pages);
	// 创建房间元素
	Node* createRoomItem(ComRoomInfo* roomInfo);




public:
	//更新房间人数
	void updateRoomPeopleCount(UINT roomID, UINT userCount);

	// 刷新UI(游戏处于此界面时重连刷新UI)
	void refresh();

	// 基类虚接口
public:
	//登陆房间回调
	virtual void onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handldeCode) override;
	virtual void onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo) override;
	virtual void onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo) override;

	// 房间密码
	virtual void onPlatformRoomPassEnter(bool success, UINT roomId);

	// 系统自动赠送
	virtual void onSysGiveMoney(SysGiveMoney* give);


	void setChangeDelegate(MoneyChangeNotify* delegate);



	GameLading *m_loading;
};

#endif // __GameRoom_SCENE_H__
