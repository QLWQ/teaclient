/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef GameLists_h__
#define GameLists_h__


#include "HNNetExport.h"
#include "HNUIExport.h"

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class GameLists : public HNLayer
{
public:
	typedef std::function<void ()> EnterGameCallBack;
	EnterGameCallBack onEnterGameCallBack;
	EnterGameCallBack onRunActionCallBack;

private:
	Layout*						_layout = nullptr;
	PageView*					_pageViewGames = nullptr;			// 游戏列表
	Button*						_currentSelectedGame = nullptr;		// 列表按钮
	std::vector<ImageView*>     _ImageUpdate;                       // 更新图片

	std::vector<Sprite*>		_pagesPrompt;
	INT							_currentPageIdx;
	std::vector<ComNameInfo*>   _gameInfo;
	bool						_isDel = false;
	CC_SYNTHESIZE_PASS_BY_REF(bool, _isTouch, CanTouch);

public:
	GameLists(Layout* layout);
	virtual ~GameLists();

	static GameLists* create(Layout* layout);

public:
	bool init();

	virtual void onEnter() override;

	virtual void onExit() override;

	//关闭
	void close();

	// 界面回调接口
private:
	//pageView拖动回调函数
	void pageViewMoveCallBack(Ref* pSender, PageView::EventType type);

	//列表按钮点击回调函数
	void enterRoomEventCallBack(Ref* pSender, Widget::TouchEventType type);

	// 功能接口
private:
	// 创建游戏列表
	void createGameList();
	// 创建游戏
	void createGamePage(std::vector<ComNameInfo*> games);
	// 创建游戏元素
	Node* createGameItem(ComNameInfo* game);

public:
	//更新游戏人数
	void updateGamePeopleCount(UINT Id, UINT count);

	//检测更新（只检测不更新）
	void checkUpdateGame();
};
#endif // GameLists_h__
