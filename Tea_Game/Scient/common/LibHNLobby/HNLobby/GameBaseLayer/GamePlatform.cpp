/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GamePlatform.h"
#include "GameRoom.h"
#include "GameDesk.h"
#include "GameLists.h"
#include "HNUIExport.h"
#include "HNOpenExport.h"
#include <string>
#include <time.h>
#include "json/stringbuffer.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include <spine/spine-cocos2dx.h>
#include "spine/spine.h"

#include "../GameMenu/GameMenu.h"
#include "../GameChildLayer/GameExitLayer.h"
#include "../GameChildLayer/GameSetLayer.h"
#include "../GameChildLayer/GameRankingList.h"
#include "../GameChildLayer/GameSignLayer.h"
#include "../GameChildLayer/GameOnlineReward.h"
#include "../GameChildLayer/ServiceLayer.h"
#include "../GameChildLayer/GameBankLayer.h"
#include "../GameChildLayer/TurnLayer.h"
#include "../GameChildLayer/GameRules.h"
#include "../GameChildLayer/GameTask.h"

#include "../GameNotice/GameNotice.h"
#include "../GameNotice/HNNoticeLayer.h"
#include "../GameNotice/NoticeList.h"

#include "../GameShop/GameGiftShop.h"
#include "../GameShop/GameStoreLayer.h"

#include "../GamePersionalCenter/BindPhone.h"
#include "../GamePersionalCenter/ModifyPassword.h"
#include "../GamePersionalCenter/GameUserDataLayer.h"
#include "../GamePopularise/GameSpread.h"
#include "../GamePopularise/AgentLayer.h"

#include "../CreateRoom/CreateRoom/CreateRoomLayer.h"
#include "../CreateRoom/CreateRoom/JoinRoomLayer.h"
#include "../CreateRoom/RoomManager/RoomManager.h"
#include "../CreateRoom/CreateRoom/CreateMatchLayer.h"
#include "../GameReconnection/Reconnection.h"

//比赛场列表
#include "../GameMatch/GameMatchRegistration.h"
#include "../GameRecord/RecordList/GameRoomRecord.h"

#include "../GameNotice/Mail/HNMailbox.h"
#include "../GameChildLayer/GameAgentApply.h"
#include "../GameChildLayer/GameIntegralAct.h"
#include "Time.h"
#include "../GameClub/GameClub.h"
#include "../GameClub/ClubTips.h"
#include "../GameClub/ClubOperate.h"
#include "../GameChildLayer/GameShareLayer.h"
#include "../GamePersionalCenter/GameCertification.h"
#include <sstream>

#include "HNNetExport.h"


USING_NS_UM_SOCIAL;
using namespace network;
using namespace spine;


// 说明：
//所有的层，节点的tag标签不能重复
/////////////////////////////////////////////////////////////////////////
static const int PLATFORM_ZORDER_UI			= 100;
static const int PLATFORM_UI_ZORDER_TOP		= PLATFORM_ZORDER_UI + 1;		//
static const int PLATFORM_UI_ZORDER_BOTTOM	= PLATFORM_ZORDER_UI + 1;		//
static const int PLATFORM_UI_ZORDER_NOTICE	= PLATFORM_ZORDER_UI + 1;		//
static const int PLATFORM_UI_ZORDER_POPULARISE	= PLATFORM_ZORDER_UI + 5;	//推广
static const int ROOM_LAYER_ZORDER			= PLATFORM_ZORDER_UI + 2;		// 房间列表层级
static const int GAMELIST_LAYER_ZORDER		= PLATFORM_ZORDER_UI + 2;		// 游戏列表层级
static const int DESKLIST_LAYER_ZORDER		= PLATFORM_ZORDER_UI + 3;		// 桌子列表层级
static const int CHILD_LAYER_ZORDER			= PLATFORM_ZORDER_UI + 4;		// 子节点弹出框层级
//////////////////////////////////////////////////////////////////////////
static const int CHILD_LAYER_TAG			= 1000;
static const int CHILD_NEXTLAYER_TAG		= 100;
static const int CHILD_LAYER_USERHEAD_TAG	= CHILD_LAYER_TAG + 1;			// 图像层标签
static const int CHILD_LAYER_STORE_TAG		= CHILD_LAYER_TAG + 2;			// 商店层标签
static const int CHILD_LAYER_SET_TAG		= CHILD_LAYER_TAG + 3;			// 设置层标签
static const int CHILD_LAYER_BANK_TAG		= CHILD_LAYER_TAG + 4;			// 银行层标签
static const int CHILD_LAYER_SERVICE_TAG	= CHILD_LAYER_TAG + 5;			// 服务层标签
static const int CHILD_LAYER_ROOM_TAG		= CHILD_LAYER_TAG + 6;			// 房间层标签
static const int CHILD_LAYER_GAMES_TAG		= CHILD_LAYER_TAG + 7;			// 游戏层标签
static const int CHILD_LAYER_DESKS_TAG		= CHILD_LAYER_TAG + 8;			// 桌子层标签
static const int CHILD_LAYER_EXIT_TAG		= CHILD_LAYER_TAG + 9;			// 退出层标签
static const int CHILD_LAYER_NOTICE_TAG		= CHILD_LAYER_TAG + 10;		//公告标签
static const int CHILD_LAYER_RANK_TAG		= CHILD_LAYER_TAG + 12;			// 排行榜层标签
static const int CHILD_LAYER_SIGN_TAG		= CHILD_LAYER_TAG + 13;			// 签到层标签
static const int CHILD_LAYER_ONLINE_TAG		= CHILD_LAYER_TAG + 14;			// 在线奖励层标签
static const int CHILD_LAYER_SPREAD_TAG		= CHILD_LAYER_TAG + 15;			// 推广员层标签
static const int CHILD_LAYER_MATCH_TAG		= CHILD_LAYER_TAG + 16;			// 比赛报名层标签
static const int CHILD_LAYER_TASK_TAG		= CHILD_LAYER_TAG + 17;			// 任务层标签
static const int CHILD_LAYER_CERT_TAG		= CHILD_LAYER_TAG + 18;			// 实名认证层标签
static const int CHILD_LAYER_PASS_TAG		= CHILD_NEXTLAYER_TAG;			// 修改密码层标签
static const int CHILD_LAYER_PHONE_TAG		= CHILD_NEXTLAYER_TAG + 1;		// 绑定手机层标签
static const int CHILD_LAYER_RECORD_TAG		= CHILD_NEXTLAYER_TAG + 2;		// 战绩列表层标签
static const int CHILD_LAYER_GIFT_TAG		= CHILD_NEXTLAYER_TAG + 3;		// 礼品兑换层标签
static const int CHILD_LAYER_CLUB_TAG		= CHILD_NEXTLAYER_TAG + 4;		// 俱乐部层标签

static	int malbox = 1;
//////////////////////////////////////////////////////////////////////////

static const char* USER_HEAD_MASK			= "platform/common/touxiangdi.png";
static const char* USER_MEN_HEAD			= "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD			= "platform/head/women_head.png";

//////////////////////////////////////////////////////////////////////////

static const char* PLATFORMUI_CSB	= "platform/lobbyUi/lobbyUi_Node.csb";
//大厅娱乐场
string PLATFORMUI_RECREATION = "platform/common/new/ylcan/";
//大厅比赛场
string PLATFORMUI_MATCH = "platform/common/new/bscan/";
//大厅美女
string PLATFORMUI_FIGURE = "platform/common/new/rwjs/";
//大厅商场
string PLATFORMUI_MALL = "platform/common/new/sctb/";
static int num;
//////////////////////////////////////////////////////////////////////////
extern int ClubID;
extern std::string TextCount; //输入框内容
static LayerType _layerType = PLATFORM;	// 当前场景（大厅，游戏列表，房间列表，桌子列表）
static bool isReturnMatch = false;

void GamePlatform::createPlatform()
{
	if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INPLATFORM
		|| HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INROOM) return;

	_layerType = PLATFORM;
    Scene* scene = Scene::create();
    auto mainlayer = GamePlatform::create();
	mainlayer->requestGameList();
    scene->addChild(mainlayer);
	//本场景变暗消失后另一场景慢慢出现
	Director::getInstance()->replaceScene(TransitionFade::create(0.3f, scene));
	
}

void GamePlatform::SceneEffect(Scene* scene)
{
	srand((int)time(NULL));
	int num = (rand() % 29);
	//HNLOGEX_DEBUG("num = %s", str.c_str());
	switch (num)
	{
	case(0) :
	{
		//本场景向上滑动到另一场景
		Director::getInstance()->replaceScene(TransitionSlideInB::create(0.3f, scene));
		break;
	}
	case(1) :
	{
		//本场景向上滑动到另一场景
		Director::getInstance()->replaceScene(TransitionZoomFlipAngular::create(0.3f, scene));
		break;
	}
	case(2) :
	{
		//本场景小方块消失到另一场景
		Director::getInstance()->replaceScene(TransitionTurnOffTiles::create(0.3f, scene));
		break;
	}
	case(3) :
	{
		//本场景三矩形左右消失后另一场景三矩形左右出现
		Director::getInstance()->replaceScene(TransitionSplitRows::create(0.3f, scene));
		break;
	}
	case(4) :
	{
		//本场景三矩形上下消失后另一场景三矩形上下出现
		Director::getInstance()->replaceScene(TransitionSplitCols::create(0.3f, scene));
		break;
	}
	case(5) :
	{
		//本场景向下滑动到另一场景
		Director::getInstance()->replaceScene(TransitionSlideInT::create(0.3f, scene));
		break;
	}
	case(6) :
	{
		//本场景向左滑动到另一场景
		Director::getInstance()->replaceScene(TransitionSlideInR::create(0.3f, scene));
		break;
	}
	case(7) :
	{
		//本场景向右滑动到另一场景
		Director::getInstance()->replaceScene(TransitionSlideInL::create(0.3f, scene));
		break;
	}
	case(8) :
	{
		//本场景缩小切换到另一场景放大
		Director::getInstance()->replaceScene(TransitionShrinkGrow::create(0.3f, scene));
		break;
	}
	case(9) :
	{
		//本场景旋转消失后另一场景旋转出现
		Director::getInstance()->replaceScene(TransitionRotoZoom::create(0.3f, scene));
		break;
	}
	case(10) :
	{
		//本场景从上到下消失同时另一场景出现
		 Director::getInstance()->replaceScene(TransitionProgressVertical::create(0.3f, scene));
		 break;
	}
	case(11) :
	{
		//本场景顺时针消失到另一场景
		Director::getInstance()->replaceScene(TransitionProgressRadialCW::create(0.3f, scene));
		break;
	}
	case(12) :
	{
		//本场景逆时针消失到另一场景
		Director::getInstance()->replaceScene(TransitionProgressRadialCCW::create(0.3f, scene));
		break;
	}
	case(13) :
	{
		//本场景从四周到中间消失同时另一场景出现
		Director::getInstance()->replaceScene(TransitionProgressOutIn::create(0.3f, scene));
		break;
	}
	case(14) :
	{
		//本场景从中间到四周消失同时另一场景出现
		 Director::getInstance()->replaceScene(TransitionProgressInOut::create(0.3f, scene));
		 break;
	}
	case(15) :
	{
		//本场景从左到右消失同时另一场景出现
		Director::getInstance()->replaceScene(TransitionProgressHorizontal::create(0.3f, scene));
		break;
	}
	case(16) :
	{
		//翻页切换，bool为true是向前翻。
		Director::getInstance()->replaceScene(TransitionPageTurn::create(0.3f, scene, true));
		break;
	}
	case(17) :
	{
		//另一场景由整体从右面出现
		Director::getInstance()->replaceScene(TransitionMoveInR::create(0.3f, scene));
		break;
	}
	case(18) :
	{
		//另一场景由整体从上面出现
		Director::getInstance()->replaceScene(TransitionMoveInT::create(0.3f, scene));
		break;
	}
	case(19) :
	{
		//另一场景由整体从左面出现
		Director::getInstance()->replaceScene(TransitionMoveInL::create(0.3f, scene));
		break;
	}
	case(20) :
	{
		//另一场景由整体从下面出现
		Director::getInstance()->replaceScene(TransitionMoveInB::create(0.3f, scene));
		break;
	}
	case(21) :
	{
		//本场景跳动消失后另一场景跳动出现
		Director::getInstance()->replaceScene(TransitionJumpZoom::create(0.3f, scene));
		break;
	}
	case(22) :
	{
		//本场景翻转消失到另一场景（Y轴）
		Director::getInstance()->replaceScene(TransitionZoomFlipX::create(0.3f, scene, TransitionScene::Orientation::LEFT_OVER));
		break;
	}
	case(23) :
	{
		//本场景从下到上横条消失到另一场景
		Director::getInstance()->replaceScene(TransitionFadeUp::create(0.3f, scene));
		break;
	}
	case(24) :
	{
		//本场景左下角到右上角方块消失到另一场景
		Director::getInstance()->replaceScene(TransitionFadeTR::create(0.3f, scene));
		break;
	}
	case(25) :
	{
		//本场景从上到下横条消失到另一场景
		Director::getInstance()->replaceScene(TransitionFadeDown::create(0.3f, scene));
		break;
	}
	case(26) :
	{
		//本场景右上角到左下角方块消失到另一场景
		Director::getInstance()->replaceScene(TransitionFadeBL::create(0.3f, scene));
		break;
	}
	case(27) :
	{
		//本场景变暗消失后另一场景慢慢出现
		Director::getInstance()->replaceScene(TransitionFade::create(0.3f, scene));
		break;
	}
	case(28) :
	{
		//本场景翻转消失到另一场景（Y轴）
		Director::getInstance()->replaceScene(TransitionZoomFlipY::create(0.3f, scene));
		break;
	}
	}
}

void GamePlatform::createNewPlatform()
{
	_layerType = PLATFORM;
	Scene* scene = Scene::create();
	auto mainlayer = GamePlatform::create();
	mainlayer->requestGameList();
	scene->addChild(mainlayer);
	//本场景翻转消失到另一场景（Y轴）
	Director::getInstance()->replaceScene(TransitionZoomFlipY::create(0.3f, scene));
	HNPlatformConfig()->setSceneState(PlatformConfig::SCENE_STATE::INPLATFORM);
}

void GamePlatform::returnPlatform(LayerType type)
{
	if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INPLATFORM
		|| HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INROOM) return;
	
	_layerType = type;

	// 这里做容错处理，防止排队机比赛场等特殊模式下游戏处理返回大厅界面出错
	if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME)
	{
		_layerType = ROOMLIST;
	}
	else if (RoomLogic()->getRoomRule() & GRR_GAME_BUY
		|| RoomLogic()->getRoomRule() & GRR_CONTEST
		|| RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST
		|| _layerType == GAMELIST)
	{
		createPlatform();
		return;
	}
	
	Scene* scene = Scene::create();
	auto mainlayer = GamePlatform::create();
	mainlayer->createLayerChoose(_layerType);
	scene->addChild(mainlayer);
	//本场景变暗消失后另一场景慢慢出现
	Director::getInstance()->replaceScene(TransitionFade::create(0.3f, scene));
}
void GamePlatform::returnPlatformNode(LayerNode type)
{
	if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INPLATFORM
		|| HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INROOM) return;

	// 这里做容错处理，防止排队机比赛场等特殊模式下游戏处理返回大厅界面出错
	/*if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME)
	{
		_layerType = ROOMLIST;
	}
	else if (RoomLogic()->getRoomRule() & GRR_GAME_BUY
		|| RoomLogic()->getRoomRule() & GRR_CONTEST
		|| RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST
		|| _layerType == GAMELIST)
	{
		createPlatform();
		return;
	}*/

	Scene* scene = Scene::create();
	auto mainlayer = GamePlatform::create();
	mainlayer->requestGameList();
	mainlayer->createLayerByType(type);
	scene->addChild(mainlayer);
	//本场景变暗消失后另一场景慢慢出现
	Director::getInstance()->replaceScene(TransitionFade::create(0.3f, scene));
}

void GamePlatform::createLayerChoose(LayerType type)
{
	_gamesUi->setVisible(false);
	_lobbyUi->setVisible(false);

	switch (type)
	{
	case ROOMLIST:
		_platformlogic->requestRoomList();
		_topUi->setVisible(false);
		_bottomUi->setVisible(false);
		break;
	case DESKLIST:
		_topUi->setVisible(false);
		_bottomUi->setVisible(false);
		break;	
	default:
		RoomLogic()->close();
		requestGameList();
		break;
	}
}

void GamePlatform::createLayerByType(LayerNode type)
{
	switch (type)
	{
	case MATCH:
		//比赛场列表
		isReturnMatch = true;
		//auto winsize = Director::getInstance()->getWinSize();
		//auto match = GameMatchRegistration::create();
		///*match->_ReturniGameId = [=](){
		//return 0;
		//};*/
		//match->setName("match");
		//match->setPosition(Vec2(0, -winsize.height));
		//match->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, GAMELIST_LAYER_ZORDER, CHILD_LAYER_MATCH_TAG, false);
		//match->setChangeDelegate(this);
		malbox = 2;
		break;
	//default:
		//break;
	}
}

GamePlatform::GamePlatform()
	: _Time (0)
	, _isTouch (true)
	, _isRequestClubList(false)
{
	
}

GamePlatform::~GamePlatform()
{
	_platformlogic->stop();
	HN_SAFE_DELETE(_platformlogic);
	HNHttpRequest::getInstance()->removeObserver(this);
	__NotificationCenter::getInstance()->removeObserver(this, "updateIAPPay");
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
	Director::getInstance()->getEventDispatcher()->removeEventListener(_foreground);
	
	unSocketCallBack();
	PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_FINANCE_INFO);
	PlatformLogic()->removeEventSelector(MDM_GP_GET_CONFIG, ASS_GP_GET_CONFIG);
	RemoveNetWorkEventListener();
}

bool GamePlatform::init()
{	
    if (!HNLayer::init()) return false;
	
	HNPlatformConfig()->setSceneState(PlatformConfig::SCENE_STATE::INPLATFORM);

	enableKeyboard();

	Size winSize = Director::getInstance()->getWinSize();

	float scalex = 1280 / winSize.width;
	float scaley = 720 / winSize.height;

	// 玩家信息结构体
	MSG_GP_R_LogonResult& LogonResult = PlatformLogic()->loginResult;


	//CSB场景读取
	auto node	= CSLoader::createNode(PLATFORMUI_CSB);
	node->setPosition(winSize / 2);
	
	//CSB内置动画播放
	auto ation = CSLoader::createTimeline(PLATFORMUI_CSB);
	node->runAction(ation);
	ation->gotoFrameAndPlay(0, false);
	addChild(node);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_BG"));
	layout->setScale(winSize.width / 1280, winSize.height / 720);
	_panelBg = layout;

	_lobbyUi	= dynamic_cast<Layout*>(layout->getChildByName("Panel_lobby"));
	_topUi		= dynamic_cast<Layout*>(layout->getChildByName("Panel_top"));
	_topUi->setScale(scalex, scaley);
	_bottomUi	= dynamic_cast<Layout*>(layout->getChildByName("Panel_bottom"));
	_bottomUi->setScale(scalex, scaley);
	_gamesUi	= dynamic_cast<Layout*>(layout->getChildByName("Panel_games"));
	_gamesUi->setScale(scalex, scaley);
	_rankUi = dynamic_cast<ImageView*>(layout->getChildByName("image_top_bg"));
	_rankUi->setScale(scalex, scaley);

	// 调整层级
	_lobbyUi->setLocalZOrder(1);
	_topUi->setLocalZOrder(1);
	_rankUi->setLocalZOrder(1);
	_bottomUi->setLocalZOrder(1);

	// 返回按钮
	_button_Return = dynamic_cast<Button*>(_gamesUi->getChildByName("Button_return"));
	_button_Return->addClickEventListener([=](Ref*){

		auto gamelist = dynamic_cast<GameLists*>(getChildByName("gamesLayer"));
		if (gamelist)
		{
			_layerType = PLATFORM;
			_button_Return->setEnabled(false);
			_gamesUi->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([=]() {
				
				_button_Return->setEnabled(true);
				_gamesUi->setVisible(false);
				_lobbyUi->setVisible(true);
				_lobbyUi->setOpacity(0);
				_lobbyUi->runAction(FadeIn::create(0.3f));
				gamelist->removeFromParent();
			}), nullptr));
		}
	});

	_lobbyUi->setVisible(true);
	_gamesUi->setVisible(false);

	//顶部按钮以及个人信息
	if (_topUi)
	{
		auto headFarme = dynamic_cast<ImageView*>(_topUi->getChildByName("Image_headFrame"));
		auto tempHead = dynamic_cast<ImageView*>(_topUi->getChildByName("Image_tempHead"));
		// 头像
		_userHead = GameUserHead::create(tempHead, headFarme);
		_userHead->show(USER_HEAD_MASK);
		_userHead->loadTexture(LogonResult.bBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);
		_userHead->setHeadByFaceID(LogonResult.bLogoID);
		_userHead->loadTextureWithUrl(PlatformLogic()->loginResult.headUrl);
		_userHead->setVIPHead("", PlatformLogic()->loginResult.iVipLevel);
		/*_userHead->addClickEventListener([this](Ref* sender)
		{
			GameUserDataLayer* userDataLayer = dynamic_cast<GameUserDataLayer*>(this->getChildByTag(CHILD_LAYER_USERHEAD_TAG));
			if (nullptr == userDataLayer)
			{
				userDataLayer = GameUserDataLayer::create();
				userDataLayer->setChangeDelegate(this);
				userDataLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_USERHEAD_TAG);
			}
		});	*/	

		//auto layout_user = dynamic_cast<Layout*>(_topUi->getChildByName("Panel_user"));
		// 用户昵称
		_label_UserName = dynamic_cast<Text*>(_topUi->getChildByName("Text_nickName"));
		//_label_UserName->setPositionX(_label_UserName->getPositionX() + 20);
		_label_UserName->setFontSize(24);
		if (PlatformLogic()->loginResult.iVipLevel > 0)
		{
			_label_UserName->setColor(Color3B(255, 0, 0));
		}
		else
		{
			_label_UserName->setColor(Color3B(255, 255, 255));
		}

		// 裁剪昵称
		//UITools::ClippingText(layout_user, _label_UserName);
		Tools::TrimSpace(LogonResult.nickName);
		std::string nickName(LogonResult.nickName);
		if (!nickName.empty())
		{
			string str = LogonResult.nickName;
			if (str.length() > 10)
			{
				str.erase(11, str.length());
				str.append("...");
				_label_UserName->setString(GBKToUtf8(str));
			}
			else
			{
				_label_UserName->setString(GBKToUtf8(str));
			}
		}
		else
		{
			_label_UserName->setString(GBKToUtf8("未知"));
		}

		//复制ID
		auto btn_copy = dynamic_cast<Button*>(_topUi->getChildByName("Button_copy"));
		btn_copy->addClickEventListener([=](Ref*){
			Application::getInstance()->copyToClipboard(to_string(LogonResult.dwUserID));
			GamePromptLayer::create()->showPrompt(GBKToUtf8("复制ID成功！"));
		});
		


		// 用户ID
		_label_id = dynamic_cast<Text*>(_topUi->getChildByName("Text_id"));
		_label_id->setString(StringUtils::format("ID:%d", LogonResult.dwUserID));
		//_label_id->setPositionX(_label_id->getPositionX() + 20);
		_label_id->setFontSize(24);
		/*if (PlatformLogic()->loginResult.iVipLevel > 0)
		{
			_label_id->setColor(Color3B(166, 89, 30));
		}*/
		{
			// 用户金币
			auto spr_golds = _topUi->getChildByName("Node_golds");
			_label_golds = dynamic_cast<Text*>(spr_golds->getChildByName("Text_value"));
			updateMoney();

			// 购买金币
			auto btn_add = dynamic_cast<Button*>(spr_golds->getChildByName("btn_add_gold"));
			btn_add->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyTopCallBack, this));

			// 金币ICON
			_spriteGoldLogo = (Sprite*)spr_golds->getChildByName("icon");
		}

		{
			// 用户房卡
			auto spr_jewels = _topUi->getChildByName("Node_jewels");
			_label_Jewels = dynamic_cast<Text*>(spr_jewels->getChildByName("Text_value"));
			updateDiamond();

			// 购买房卡
			auto btn_add = dynamic_cast<Button*>(spr_jewels->getChildByName("btn_add_jewels"));
			btn_add->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyTopCallBack, this));

			// 钻石ICON
			_spriteDiamondLogo = (Sprite*)spr_jewels->getChildByName("icon");
		}

		{
			// 用户奖券
			auto spr_lotterys = dynamic_cast<Sprite*>(_topUi->getChildByName("Sprite_lottery"));
			_label_lotterys = dynamic_cast<Text*>(spr_lotterys->getChildByName("Text_value"));
			updateLotteries();

			// 奖券兑换
			auto btn_convert = dynamic_cast<Button*>(spr_lotterys->getChildByName("btn_convert_lottery"));
			btn_convert->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyTopCallBack, this));
		}
	}

	if (_winSize.width / _winSize.height < 1.78)
	{
		for (auto child : _lobbyUi->getChildren())
		{
			child->setScale(scalex, scaley);
		}
	}
	else
	{
		for (auto child : _lobbyUi->getChildren())
		{
			child->setScale(0.9f, scaley * 0.9f);
		}
	}
	Size _winsize = Director::getInstance()->getWinSize();
	_button_create = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_create"));
	////娱乐场动画
	//SkeletonAnimation* skeletonNode_create = spine::SkeletonAnimation::createWithFile(PLATFORMUI_RECREATION + "ylcan.json", PLATFORMUI_RECREATION + "ylcan.atlas", 1.0F);
	//skeletonNode_create->setPosition(Vec2(_button_create->getPositionX() - 3, _button_create->getPositionY() - 10));
	//skeletonNode_create->setAnimation(0, "animation", true);
	//_lobbyUi->addChild(skeletonNode_create);
	_button_join	= dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_join"));
	_button_Gold = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_games"));
	_button_Match = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_match"));
	_button_create_majio = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_create_majio"));
	_button_create_poke = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_create_poke"));
	//比赛场动画
	//SkeletonAnimation* skeletonNode_match = spine::SkeletonAnimation::createWithFile(PLATFORMUI_MATCH + "bscan.json", PLATFORMUI_MATCH + "bscan.atlas", 1.0F);
	//skeletonNode_match->setPosition(Vec2(_button_Match->getPositionX() + 9, _button_Match->getPositionY() - 8));
	//skeletonNode_match->setAnimation(0, "animation", true);
	//_lobbyUi->addChild(skeletonNode_match);
	//_button_Match->setVisible(false);
	if (_button_create) _button_create->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyMidCallBack, this));
	if (_button_join) _button_join->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyMidCallBack, this));
	if (_button_Gold)	_button_Gold->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyMidCallBack, this));
	if (_button_Match)	_button_Match->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyMidCallBack, this));
	if (_button_create_majio)	_button_create_majio->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyMidCallBack, this));
	if (_button_create_poke)	_button_create_poke->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyMidCallBack, this));
	// 俱乐部按钮
	_panelClub = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_club"));
	_panelClub->addClickEventListener([=](Ref*) {
		Club::createClubScene();
	});
	_ClubListView = dynamic_cast<ListView*>(_lobbyUi->getChildByName("ListView_club_table"));
	
	//创建俱乐部
	Button* Button_create_club = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_create_club"));
	Button_create_club->addClickEventListener([=](Ref*) {
		//测试无需权限判断
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入茶楼名称...");
		ClubOperateLayer->SetImageViewRes(1);
		auto Callback = [=](){
			MSG_GP_I_CreateClub  name;
			sprintf(name.szClubName, "%s", Utf8ToGB(TextCount.c_str()));
			//_selection_club_name = TextCount;
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CREATE_CLUB, &name, sizeof(MSG_GP_I_CreateClub), HN_SOCKET_CALLBACK(GamePlatform::CreateClubResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			Callback();
		});
	});

	//加入俱乐部
	Button* Button_join_club = dynamic_cast<Button*>(_lobbyUi->getChildByName("Button_join_club"));
	Button_join_club->addClickEventListener([=](Ref*) {
		//加入茶楼输入ID界面
		ClubOperate* ClubOperateLayer = ClubOperate::create();
		this->addChild(ClubOperateLayer);
		ClubOperateLayer->SetPlaceHolderCount("请输入茶楼ID...");
		ClubOperateLayer->SetImageViewRes(2);
		auto Callback = [=](){
			MSG_GP_I_JoinClub ID;
			ID.iClubID = std::atoi(TextCount.c_str());
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_JOIN_CLUB, &ID, sizeof(MSG_GP_I_JoinClub), HN_SOCKET_CALLBACK(GamePlatform::JoinClubResult, this));
		};
		ClubOperateLayer->SetCallBack([=](){
			Callback();
		});
	});

	// 大厅美女动画
	/*Sprite* sp = dynamic_cast<Sprite*>(_lobbyUi->getChildByName("Sprite_meinv"));
	SkeletonAnimation* skeletonNode_Figure = spine::SkeletonAnimation::createWithFile(PLATFORMUI_FIGURE + "mn.json", PLATFORMUI_FIGURE + "mn.atlas", 1.0F);
	skeletonNode_Figure->setPosition(Vec2(sp->getPositionX() + 50, sp->getPositionY() - 290));
	skeletonNode_Figure->setAnimation(0, "animation", true);
	_lobbyUi->addChild(skeletonNode_Figure);*/
	//动画
	//_button_create->setPosition(Vec2(_winsize.width + _button_create->getContentSize().width / 2, _winsize.height / 2));
	//_button_join->setPosition(Vec2(_winsize.width + _button_join->getContentSize().width / 2, _winsize.height / 2));
	//_panelClub->setPosition(Vec2(_winsize.width + _panelClub->getContentSize().width / 2, _winsize.height / 2));
	
	//_button_create->runAction(Sequence::create(MoveTo::create(0.5, Vec2(_winsize.width*0.25, _winsize.height / 2 +10)), MoveTo::create(0.1, Vec2(_winsize.width*0.25 - 20, _winsize.height / 2 +10)), MoveTo::create(0.2f, Vec2(_winsize.width*0.25, _winsize.height / 2 +10)), nullptr));
	//_button_join->runAction(Sequence::create(MoveTo::create(0.5, Vec2(_winsize.width*0.525, _winsize.height / 2)), MoveTo::create(0.1, Vec2(_winsize.width*0.525 - 20, _winsize.height / 2)), MoveTo::create(0.2f, Vec2(_winsize.width*0.525, _winsize.height / 2)), nullptr));
	//_panelClub->runAction(Sequence::create(MoveTo::create(0.5, Vec2(_winsize.width*0.8, _winsize.height / 2 + 10)), MoveTo::create(0.1, Vec2(_winsize.width*0.8 - 20, _winsize.height / 2 +10)), MoveTo::create(0.2f, Vec2(_winsize.width*0.8, _winsize.height / 2 +10)), nullptr));

	//////////////////////////////////////////////////////////////////////////////////
	//底部按钮处理
	if (_bottomUi)
	{
		//for (int i = 0; i < 11; i++)
		//{
		//	auto btn = dynamic_cast<Button*>(_bottomUi->getChildByTag(i));
		//	if (btn) btn->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));
		//	//if (btn && i == 10) btn->setVisible(false);
		//	if (btn->getName() == "Button_store") {
		//		_buttonStore = btn;
		//	}
		//	if (btn->getName() == "Button_Bigwheel")
		//	{
		//		/*auto Image_zhen = dynamic_cast<ImageView*>(btn->getChildByName("Image_zhen"));
		//		auto action = RepeatForever::create(RotateBy::create(0.1, 10));
		//		Image_zhen->runAction(action);*/
		//	}
		//}

		//商店
		Button* btn_store = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_store"));
		btn_store->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));
		//商店动画
		/*SkeletonAnimation* skeletonNode_Mall = spine::SkeletonAnimation::createWithFile(PLATFORMUI_MALL + "sctb.json", PLATFORMUI_MALL + "sctb.atlas", 1.0F);
		skeletonNode_Mall->setPosition(Vec2(btn_store->getPositionX() + 10, btn_store->getPositionY() - 70));
		skeletonNode_Mall->setAnimation(0, "animation", true);
		_bottomUi->addChild(skeletonNode_Mall);*/

		//分享
		Button* btn_share = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_share"));
		btn_share->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//战绩
		Button* btn_record = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_record"));
		btn_record->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//活动
		Button* btn_jifen = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_jifen"));
		btn_jifen->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//转盘
		Button* btn_Bigwheel = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_Bigwheel"));
		btn_Bigwheel->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));
		
		//点击更多按钮
		auto btn_more = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_more"));
		btn_more->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//更多按钮弹出
		_image_more = dynamic_cast<ImageView*>(_bottomUi->getChildByName("image_more"));
		_image_more->setVisible(false);

		//实名
		auto btn_autonym = dynamic_cast<Button*>(_image_more->getChildByName("Button_autonym"));
		btn_autonym->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//推广
		auto btn_promote = dynamic_cast<Button*>(_image_more->getChildByName("Button_promote"));
		btn_promote->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//签到
		auto btn_sign = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_sign"));
		btn_sign->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//邮件
		auto btn_mail = dynamic_cast<Button*>(_image_more->getChildByName("Button_mail"));
		btn_mail->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));
		//屏蔽邮件，同时修改更多背景大小，2019.12.15
		btn_mail->setVisible(false);
		//_image_more->setContentSize(Size(_image_more->getContentSize().width / 3 * 2, _image_more->getContentSize().height));

		// 帮助按钮
		auto btn_rule = dynamic_cast<Button*>(_image_more->getChildByName("Button_rule"));
		//btn_rule->setVisible(false);
		btn_rule->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyTopCallBack, this));

		//公告
		auto btn_notice = dynamic_cast<Button*>(_image_more->getChildByName("Button_notice"));
		btn_notice->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//领现金
		auto btn_receive = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_receive"));
		btn_receive->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//新手任务
		auto btn_newbie = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_newbie"));
		btn_newbie->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));
		btn_newbie->setOpacity(0);

		////新手任务动画
		//auto nodeEffect = CSLoader::createNode("platform/Games/Action/xsrw_tb.csb");
		////nodeEffect->setAnchorPoint(Vec2(0.5, 0.5));
		//nodeEffect->setScale(0.9);
		//nodeEffect->setPosition(Vec2(btn_newbie->getPositionX(), btn_newbie->getPositionY() + 35));
		//auto ation = CSLoader::createTimeline("platform/Games/Action/xsrw_tb.csb");
		//nodeEffect->runAction(ation);
		//ation->gotoFrameAndPlay(0, true);
		//nodeEffect->setName("NewbieEffect");
		//_bottomUi->addChild(nodeEffect);

		//新手任务红点
		//_sp_NewbieRed = Sprite::create("platform/common/new/red_new.png");
		//_sp_NewbieRed->setPosition(Vec2(btn_newbie->getPositionX() + 40, btn_newbie->getPositionY() + 65));
		//CCActionInterval *forwardIn = CCFadeIn::create(1);
		//CCActionInterval *backIn = CCFadeOut::create(2);
		//CCAction *actionIn = CCRepeatForever::create(dynamic_cast<CCActionInterval *>(CCSequence::create(forwardIn, DelayTime::create(1.5f), backIn, NULL)));
		///*CCActionInterval *blinkAction = CCBlink::create(1, 1);
		//CCAction *action = CCRepeatForever::create(dynamic_cast<CCActionInterval *>(CCSequence::create(blinkAction, NULL)));*/
		//_sp_NewbieRed->setScale(1.3);
		//_sp_NewbieRed->runAction(actionIn);
		//_sp_NewbieRed->setVisible(false);
		//_bottomUi->addChild(_sp_NewbieRed);

		//任务
		auto btn_Task = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_task"));
		btn_Task->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyBottomCallBack, this));

		//任务红点
		_sp_TaskListRed = (Sprite*)btn_Task->getChildByName("Image_red");
		CCActionInterval *forwardIn1 = CCFadeIn::create(1);
		CCActionInterval *backIn1 = CCFadeOut::create(2);
		CCAction *actionInT = CCRepeatForever::create(dynamic_cast<CCActionInterval *>(CCSequence::create(forwardIn1, DelayTime::create(1.5f), backIn1, NULL)));
		//CCActionInterval *blinkAction = CCBlink::create(2, 1);
		//CCAction *action1 = CCRepeatForever::create(dynamic_cast<CCActionInterval *>(CCSequence::create(blinkAction, NULL)));
		_sp_TaskListRed->setContentSize(Size(34,34));
		_sp_TaskListRed->runAction(actionInT);
		_sp_TaskListRed->setVisible(false);

		//更新任务红点通知
		HNPlatformTaskList::getInstance()->onRequestPlatformCallBack = [=](){
			UpdateTaskRed();
		};

		//提示实名
		_real_tip = dynamic_cast<Sprite*>(_bottomUi->getChildByName("tip_bind"));
		_real_tip->setVisible(false);

		//弹出关闭处理
		auto layout_touch = dynamic_cast<Layout*>(_bottomUi->getChildByName("Panel_touch"));
		layout_touch->setScale(winSize.width / 1280, winSize.height / 720);
		layout_touch->addClickEventListener([=](Ref*){
			//_image_more->runAction(Sequence::create(ScaleTo::create(0.3f, 0.1), CallFunc::create([=](){ _image_more->setVisible(false); }), nullptr));
			//layout_more->setVisible(false);
			layout_touch->setVisible(false);
		});

		// 设置
		auto btn_set = dynamic_cast<Button*>(_image_more->getChildByName("Button_set"));
		btn_set->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyTopCallBack, this));

		// 客服
		auto btn_service = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_service"));
		btn_service->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuLobbyTopCallBack, this));

		//推广按钮取消
		//auto Button_MySpread = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_MySpread"));
	}

	if (_rankUi)
	{
		_vecRankBtnList.clear();
		_vecRankList.clear();
		_vecSp_Rank.clear();
		auto temp = dynamic_cast<Layout*>(_rankUi->getChildByName("pane_rank_make"));
		_rankbg = dynamic_cast<ImageView*>(temp->getChildByName("image_bg"));
		_rankwicker = dynamic_cast<Sprite*>(_rankUi->getChildByName("sp_btn_bg"));

		//_rankYear = dynamic_cast<Sprite*>(_rankUi->getChildByName("newyear_tip"));
		//_rankYear->setOpacity(0);
		//鞭炮动画
		/*auto nodeEffect = CSLoader::createNode("platform/Games/Action/bp.csb");
		nodeEffect->setPosition(Vec2(_rankYear->getContentSize().width / 2 - 5, _rankYear->getContentSize().height / 2 + 6));
		nodeEffect->setScale(0.75);
		auto ation = CSLoader::createTimeline("platform/Games/Action/bp.csb");
		nodeEffect->runAction(ation);
		ation->gotoFrameAndPlay(0, true);
		nodeEffect->setName("BpEffect");
		_rankYear->addChild(nodeEffect);*/

		//_rankYear1 = dynamic_cast<Sprite*>(_rankUi->getChildByName("newyear_tip1"));
		//_rankYear1->setOpacity(0);
		//鞭炮动画1
		/*auto nodeEffect1 = CSLoader::createNode("platform/Games/Action/bp.csb");
		nodeEffect1->setPosition(Vec2(_rankYear1->getContentSize().width / 2 + 16, _rankYear1->getContentSize().height / 2 + 5));
		nodeEffect1->setScale(0.75);
		auto ation1 = CSLoader::createTimeline("platform/Games/Action/bp.csb");
		nodeEffect1->runAction(ation1);
		ation1->gotoFrameAndPlay(0, true);
		nodeEffect1->setName("BpEffect1");
		_rankYear1->addChild(nodeEffect1);*/

		_RankingCloseBtn = dynamic_cast<Button*>(_rankUi->getChildByName("btn_close"));
		_RankingCloseBtn->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuRankCallBack, this));
		_RankingOpenBtn = dynamic_cast<Button*>(_rankUi->getChildByName("btn_open"));
		_RankingOpenBtn->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuRankCallBack, this));
		for (int i = 0; i < 3; i++)
		{
			//排行榜切换按钮
			Button* btn = dynamic_cast<Button*>(_rankUi->getChildByName(StringUtils::format("Button_%d", i+1)));
			_vecRankBtnList.push_back(btn);
			btn->addClickEventListener(CC_CALLBACK_1(GamePlatform::menuRankCallBack, this));
			//排行榜列表
			ListView* list = dynamic_cast<ListView*>(_rankbg->getChildByName(StringUtils::format("listview_%d", i + 1)));
			list->setScrollBarEnabled(false);
			_vecRankList.push_back(list);
			//排行榜按钮纹理
			Sprite* sp = dynamic_cast<Sprite*>(_rankUi->getChildByName(StringUtils::format("sp_%d", i + 1)));
			_vecSp_Rank.push_back(sp);
		}
	}

	HNAudioEngine::getInstance()->playBackgroundMusic(GAME_BACKGROUND_MUSIC, true);

	_platformlogic = new HNPlatformLogicBase(this);
	_platformlogic->start();

	// 添加动画
	//this->addSpine();

	// 重连成功通知
	_listener = EventListenerCustom::create(RECONNECTION, [=](EventCustom* event) {

		updateUserInfo();
		
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);

	// 切回前台通知
	_foreground = EventListenerCustom::create(FOCEGROUND, [=](EventCustom* event) {
		//HNPlatformTaskList::getInstance()->RequestAllTaskList();
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_foreground, 1);

	_lobbyUi->addClickEventListener([=](Ref*){
		/*auto layout_more = dynamic_cast<Layout*>(_bottomUi->getChildByName("Panel_more"));
		if (layout_more->isVisible())
		{
			layout_more->setVisible(false);
		}*/
	});
	if (HNPlatformConfig()->getSceneState() == PlatformConfig::SCENE_STATE::INPLATFORM)
	{
		bool isReconnection = UserDefault::getInstance()->getBoolForKey("Reconnection", false);
		if (!isReconnection)
		{
			if (malbox == 1)
			{
				requestNotice(2);
			};
		}
	};
	
	malbox++;


	_Text_notice = dynamic_cast<Text*>(_panelBg->getChildByName("Panel_notice")->getChildByName("Text_notice"));

	requestClubList();


	AddNetWorkEventListener();
	return true;
}

//查询各种奖励信息
void GamePlatform::checkGiftMessages()
{
	// 在线奖励
	PlatformLogic()->sendData(MDM_GP_ONLINE_AWARD, ASS_GP_ONLINE_AWARD_CHECK, 0, 0, 
		HN_SOCKET_CALLBACK(GamePlatform::checkGiftMessagesEventSelector, this));

	// 签到奖励
	PlatformLogic()->sendData(MDM_GP_SIGN, ASS_GP_SIGN_CHECK, 0, 0, 
		HN_SOCKET_CALLBACK(GamePlatform::checkGiftMessagesEventSelector, this));
}

bool GamePlatform::checkGiftMessagesEventSelector(HNSocketMessage* socketMessage)
{
	switch (socketMessage->messageHead.bMainID)
	{
	case MDM_GP_ONLINE_AWARD:
		{
			CCAssert(sizeof(MSG_GP_S_ONLINE_AWARD_CHECK_RESULT) == socketMessage->objectSize, "MSG_GP_S_ONLINE_AWARD_CHECK_RESULT is error.");
			MSG_GP_S_ONLINE_AWARD_CHECK_RESULT* iCheck = (MSG_GP_S_ONLINE_AWARD_CHECK_RESULT*)socketMessage->object;

			if (1 == socketMessage->messageHead.bHandleCode)
			{
				//auto btn_task = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_task"));
				//auto Prompt_task = dynamic_cast<ImageView*>(btn_task->getChildByName("Image_Prompt"));

				//bool bShow = (iCheck->iLeftTime == 0 && iCheck->iOnLineTimeMoney > 0) ? true : false;

				//Prompt_task->setVisible(bShow);
				//playOrStopJump(btn_task, bShow);

				if (iCheck->iLeftTime > 0)
				{
					_Time = iCheck->iLeftTime;
					schedule(schedule_selector(GamePlatform::updataGetRewardTimeMessage), 1.0f);
				}				
			}
		}
		break;
	case MDM_GP_SIGN:
		{
			CCAssert(sizeof(MSG_GP_S_SIGN_CHECK_RESULT) == socketMessage->objectSize, "MSG_GP_S_SIGN_CHECK_RESULT is error.");
			MSG_GP_S_SIGN_CHECK_RESULT* iCheck = (MSG_GP_S_SIGN_CHECK_RESULT*)socketMessage->object;

			/*auto btn_sign = dynamic_cast<Button*>(_topUi->getChildByName("Button_sign"));
			auto Prompt_sign = dynamic_cast<ImageView*>(btn_sign->getChildByName("Image_Prompt"));
			bool bShow = iCheck->iRs == 0 ? true : false;

			if (Prompt_sign)
			{
				Prompt_sign->setVisible(bShow);
				Prompt_sign->stopAllActions();
				Prompt_sign->runAction(RepeatForever::create(Sequence::create(FadeIn::create(1.0f), FadeOut::create(1.0f), nullptr)));
			}*/
		}
		break;
	default:
		break;
	}

	return true;
}

//卸载奖励监听
void GamePlatform::unSocketCallBack()
{
	PlatformLogic()->removeEventSelector(MDM_GP_ONLINE_AWARD, ASS_GP_ONLINE_AWARD_CHECK);
	PlatformLogic()->removeEventSelector(MDM_GP_SIGN, ASS_GP_SIGN_CHECK);
}

//领奖倒计时
void GamePlatform::updataGetRewardTimeMessage(float dt)
{
	return;

	_Time--;
	if (0 == _Time)
	{
		auto btn_task = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_task"));
		unschedule(schedule_selector(GamePlatform::updataGetRewardTimeMessage));
		auto Prompt_online = (ImageView*)btn_task->getChildByName("Image_Prompt");
		Prompt_online->setVisible(true);

		playOrStopJump(btn_task, true);
	}
}


void GamePlatform::AddNetWorkEventListener()
{
	//监听公告消息
	PlatformLogic()->addEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE_UPDATE, HN_SOCKET_CALLBACK(GamePlatform::ClubNoticeChange, this));
}


void GamePlatform::RemoveNetWorkEventListener()
{
	PlatformLogic()->removeEventSelector(MDM_GP_CLUB, ASS_GP_CLUB_NOTICE_UPDATE); //移除监听公告消息
}


bool GamePlatform::ClubNoticeChange(HNSocketMessage* SocketMessage)
{
	if (SocketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_Club_Notice* Result = (MSG_GP_O_Club_Notice*)SocketMessage->object;
		auto count = Result->szClubNotice;
		string str = StringUtils::toString(count);
		{
			//避免多次创建，如果存在就直接删除
			auto NoticeLayer = this->getChildByName("NoticeLayer");
			//如果公告内容为空的情况下，直接return出去，不做处理
// 			if (/*isManage && isManage < 3 &&*/ str.compare("") == 0)
// 			{
// 				str = "暂无公告                 ";
// 			}
// 			else 
// 			if (str.compare("") == 0)
// 			{
// 				if (NoticeLayer)NoticeLayer->removeFromParent();
// 				//str = "测试测试测试。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。";
// 				return;
// 			}
// 			if (NoticeLayer) NoticeLayer->removeFromParent();

// 			if (ListView_Table == nullptr)
// 			{
// 				//创建节点
// 				auto node = CSLoader::createNode(CLUB_NOTICE_PATH);
// 				node->setName("NoticeLayer");
// 				addChild(node);
// 				//创建动画
// 				_timeLine = CSLoader::createTimeline(CLUB_NOTICE_PATH);
// 				node->runAction(_timeLine);
// 				//开始播放喇叭动画
// 				_timeLine->play("laba", true);
// 				auto panel = dynamic_cast<Layout*>(node->getChildByName("Panel_1"));
// 				auto text_notice = dynamic_cast<Text*>(panel->getChildByName("Text_notice"));
// 				//设置公告内容
// 				text_notice->setString(GBKToUtf8(str));
// 				auto _wordSize = text_notice->getContentSize();
// 				//文字做左移动作
// 				text_notice->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.02f, Vec2(-2.0f, 0)), CallFunc::create([=]() {
// 					if (text_notice->getPositionX() <= -(_wordSize.width + 20))text_notice->setPosition(Vec2(500, 15));
// 				}), nullptr)));
// 			}
// 			else
// 			{
// 				Button_notice->setVisible(true);
				_Text_notice->setString(GBKToUtf8(str));
				auto _wordSize = _Text_notice->getContentSize();
				//文字做左移动作
				_Text_notice->stopAllActions();
				_Text_notice->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.02f, Vec2(-2.0f, 0)), CallFunc::create([=]() {
					if (_Text_notice->getPositionX() <= -(_wordSize.width + 20)) _Text_notice->setPosition(Vec2(0, 23.00));
				}), nullptr)));
// 			}
		
		}
	}
	return true;
}

void GamePlatform::requestGameList()
{
	_platformlogic->requestGameList();
}

void GamePlatform::onPlatformRoomListCallback(bool success, const std::string& message)
{
	if (success)
	{
		auto createroom = (CreateRoomLayer*)this->getChildByName("RoomLayer");
		if (createroom != nullptr)
		{
			if (RoomInfoModule()->getRoomCount() == 0)
			{
				LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
				auto promptlayer = GamePromptLayer::create();
				promptlayer->showPrompt(GBKToUtf8("暂无房间！"));
				return;
			}
			createroom->CreateRoomInfoList();
		}
		//createRoomListLayer();
	}
	else
	{
		GamePromptLayer::create()->showPrompt(message);
		
	}	
}

void GamePlatform::onPlatformGameListCallback(bool success, const std::string& message)
{
	if (success)
	{
		_gamesUi->setVisible(false);
		_lobbyUi->setVisible(false);

		if (_layerType == GAMELIST)
		{
			createGameListLayer();
		}
		else if (_layerType == PLATFORM)
		{
			_lobbyUi->setVisible(true);
		}
		else
		{
			
		}
	}
	else
	{
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(message + GBKToUtf8("请重新获取"));
		prompt->setCallBack([=](){
			requestGameList();
		});
	}
}
//创建房间列表层
void GamePlatform::createRoomListLayer()
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	auto createroom = this->getChildByName("RoomLayer");
	auto gameRoomLayer = GameRoom::createGameRoom(this);
	gameRoomLayer->_NoRoomCallBack = [=](){
		createroom->removeFromParent();
	};
	gameRoomLayer->setName("gameRoomLayer");
	gameRoomLayer->setChangeDelegate(this);
	gameRoomLayer->setPosition(Vec2::ANCHOR_BOTTOM_LEFT);
	ComRoomInfo* roomInfo = RoomInfoModule()->getRoom(0);
	if (RoomInfoModule()->getRoomCount() == 1 && (roomInfo->uKindID == 2 || roomInfo->uKindID == 3))
	{
		gameRoomLayer->setVisible(false);
		if (roomInfo->uNameID == 11100503)
		{
			m_loading = GameLading::create();
			m_loading->OnLadINGbg(roomInfo->uNameID);
			m_loading->setVisible(true);
			addChild(m_loading, 1000);
		}
		gameRoomLayer->QuickJionGame();
	}

	gameRoomLayer->onCloseCallBack = [this](){
		//LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("加载中"), 22);

		//showTopAction(true, nullptr);
		/*	if (GameNotice::getInstance()->getisRunging())
		{
		GameNotice::getInstance()->setRotating(0);
		}*/
		
		_layerType = GAMELIST;
		//_topUi->runAction(MoveTo::create(0.1f, Vec2(_winSize.width / 2, _winSize.height)));
		//_bottomUi->runAction(MoveTo::create(0.1f, Vec2(_winSize.width / 2, -5)));
		//requestGameList();
		
		
		// 玩家登陆房间可能有赠送金币，所以退出时需要刷新
		updateUserInfo();
	};
	gameRoomLayer->onEnterDeskCallBack = [this](ComRoomInfo* roomInfo){

		_layerType = DESKLIST;
		createDeskListLayer(roomInfo);
	};

	createroom->addChild(gameRoomLayer, 1);
	//gameRoomLayer->setOpacity(0);
	gameRoomLayer->setCanTouch(true);
	gameRoomLayer->runActionByRight();
	gameRoomLayer->_returnCallBack = [this](){
		goldgames->removeAllChildren();
	};
	// 	gameRoomLayer->runAction(Sequence::create(FadeIn::create(0.3f), CallFunc::create([=]() {
	// 		
	// 		gameRoomLayer->setCanTouch(true);
	// 	}), nullptr));
}



//创建游戏列表层
void GamePlatform::createGameListLayer()
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	auto createroom = this->getChildByName("RoomLayer");
	auto gamesLayer = GameLists::create(goldgames);
	gamesLayer->setName("gamesLayer");
	gamesLayer->setPosition(Vec2::ANCHOR_BOTTOM_LEFT);
	gamesLayer->onEnterGameCallBack = [this](){
		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("加载中"), 22);
		   
			_layerType = ROOMLIST;
			_platformlogic->requestRoomList();
	};
	createroom->addChild(gamesLayer,1);
	gamesLayer->setOpacity(0);
	gamesLayer->setCanTouch(false);
	gamesLayer->runAction(Sequence::create(FadeIn::create(0.3f), CallFunc::create([=]() {
		_button_Return->setEnabled(true);
		gamesLayer->setCanTouch(true);
	}), nullptr));
}

//创建桌子列表层
void GamePlatform::createDeskListLayer(ComRoomInfo* roomInfo)
{
	if (m_loading != NULL)
	{

		m_loading->removeFromParent();
		m_loading = NULL;
	}
	auto gameDeskLayer = GameDesk::createDesk(roomInfo);
	gameDeskLayer->setPosition(Vec2::ANCHOR_BOTTOM_LEFT);
	gameDeskLayer->setName("gameDeskLayer");
	gameDeskLayer->onCloseCallBack = [this](){

		showTopAction(true, nullptr);


		_layerType = GAMELIST;
		_topUi->runAction(MoveTo::create(0.1f, Vec2(_winSize.width / 2, _winSize.height)));
		_bottomUi->runAction(MoveTo::create(0.1f, Vec2(_winSize.width / 2, -5)));

		requestGameList();

		// 玩家登陆房间可能有赠送金币，所以退出时需要刷新
		updateUserInfo();
	};

	addChild(gameDeskLayer, DESKLIST_LAYER_ZORDER, CHILD_LAYER_DESKS_TAG);
}

void GamePlatform::onPlatformRoomUserCountCallback(UINT roomID, UINT userCount)
{
	GameRoom* roomLayer = dynamic_cast<GameRoom*>(this->getChildByTag(CHILD_LAYER_ROOM_TAG));
	if (nullptr != roomLayer)
	{
		roomLayer->updateRoomPeopleCount(roomID, userCount);
	}
}
void GamePlatform::onPlatformGameUserCountCallback(UINT Id, UINT count)
{	GameLists* gameListLayer = dynamic_cast<GameLists*>(this->getChildByTag(CHILD_LAYER_GAMES_TAG));
	if (nullptr != gameListLayer)
	{
		//gameListLayer->updateGamePeopleCount(Id, count);
	}
}

void GamePlatform::onExit()
{
	_platformlogic->stop();
	HNLayer::onExit();
}

void GamePlatform::updataSet()
{
	if (!UserDefault::getInstance()->isXMLFileExist())
	{
		UserDefault::getInstance();
	}
	float l = UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT, 50) / 100.f;
	HNAudioEngine::getInstance()->setBackgroundMusicVolume(l);
	if (l == 0.0f)
	{
		HNAudioEngine::getInstance()->pauseBackgroundMusic();
	}

	float f = UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT, 50) / 100.f;
	HNAudioEngine::getInstance()->setEffectsVolume(f);
}

void GamePlatform::onEnterTransitionDidFinish()
{
	HNLayer::onEnterTransitionDidFinish();

	releaseUnusedRes();

	requestNotice(1);

	//使用上次记录的设置
	updataSet();


	//请求排行榜数据
	//RequestRankData();

	//查询实名认证数据
	RequestRealName();

	MailData()->getSysMsg();

	//查询各种任务状态
	//HNPlatformTaskList::getInstance()->RequestAllTaskList();
	//PlatformTask();

	getModuleConfig();

	//checkGiftMessages();

	updateUserInfo();

	// 开启检测重连功能
	GameReconnection()->openOrCloseReconnet(true);
    
	// 检测是否需要自动进房间
	GameReconnection()->shareDeskInfo();

	// 是否显示比赛场列表
	if (isReturnMatch)
	{
		isReturnMatch = false;
		if (this->getChildByName("match") != nullptr)
			return;
		auto winsize = Director::getInstance()->getWinSize();
		auto match = GameMatchRegistration::create();
		match->setName("match");
		match->setPosition(Vec2(0, -winsize.height));
		match->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, GAMELIST_LAYER_ZORDER, CHILD_LAYER_MATCH_TAG, false);
		match->setChangeDelegate(this);
	};

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	__NotificationCenter::getInstance()->addObserver(this, (SEL_CallFuncO)(&GamePlatform::updateIAPPay), "updateIAPPay", nullptr);
	updateIAPPay(nullptr);
#endif
}

void GamePlatform::menuLobbyTopCallBack(Ref* pSender)
{
	if (!_isTouch) return;
	_isTouch = false;

	auto btn = (Button*)pSender;
	btn->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
	auto winSize = Director::getInstance()->getWinSize();

	std::string name(btn->getName());
	
	// 商城购买金币
	if (name.compare("btn_add_gold") == 0)
	{
		/*auto storeLayer = GameStoreLayer::createGameStore(this);
		storeLayer->setChangeDelegate(this);
		storeLayer->onCloseCallBack = [this]() {

			updateUserInfo();
		};
		storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_STORE_TAG);
		auto pFunc = [=]()
		{
			auto giftShopLayer = GameGiftShop::createGameGiftShop(this, PlatformLogic()->loginResult.iLotteries);
			giftShopLayer->setName("giftShopLayer");
			giftShopLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_GIFT_TAG);

			giftShopLayer->onCloseCallBack = [=]() {
				updateUserInfo();
			};
		};
		storeLayer->clickChangeEventCallBack(pFunc);
		storeLayer->onCloseCallBack = [this]()
		{
			updateUserInfo();
		};*/
		GameStoreLayer* storeLayer = dynamic_cast<GameStoreLayer*>(this->getChildByTag(CHILD_LAYER_STORE_TAG));
		if (nullptr != storeLayer)
		{
			storeLayer->ClickChangeStore(2);
		}
		else
		{
			storeLayer = GameStoreLayer::createGameStore(this);
			storeLayer->setName("storeLayer");
			storeLayer->setChangeDelegate(this);
			storeLayer->onCloseCallBack = [this]() {

				updateUserInfo();
			};
			storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_STORE_TAG);
			storeLayer->ClickChangeStore(2);
		}
	}
	else if (name.compare("btn_add_jewels") == 0)
	{
		//切换商场购买房卡
		GameStoreLayer* storeLayer = dynamic_cast<GameStoreLayer*>(this->getChildByTag(CHILD_LAYER_STORE_TAG));
		if (nullptr != storeLayer)
		{
			storeLayer->ClickChangeStore(1);
		}
		else
		{
			storeLayer = GameStoreLayer::createGameStore(this);
			storeLayer->setName("storeLayer");
			storeLayer->setChangeDelegate(this);
			storeLayer->onCloseCallBack = [this]() {

				updateUserInfo();
			};
			storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_STORE_TAG);
			storeLayer->ClickChangeStore(1);
		}
	}
	else if (name.compare("btn_convert_lottery") == 0)
	{
		//切换商场兑换
		GameStoreLayer* storeLayer = dynamic_cast<GameStoreLayer*>(this->getChildByTag(CHILD_LAYER_STORE_TAG));
		if (nullptr != storeLayer)
		{
			storeLayer->ClickChangeStore(3);
		}
		else
		{
			storeLayer = GameStoreLayer::createGameStore(this);
			storeLayer->setName("storeLayer");
			storeLayer->setChangeDelegate(this);
			storeLayer->onCloseCallBack = [this]() {

				updateUserInfo();
			};
			storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_STORE_TAG);
			storeLayer->ClickChangeStore(3);
		}
	}
	
	// 设置
	else if (name.compare("Button_set") == 0)
	{
		auto setLayer = GameSetLayer::create();
		setLayer->setName("setLayer");
		setLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SPREAD_TAG);
		setLayer->onExitCallBack = [=]() {
			switchAccount();
		};
	}

	// 客服
	else if (name.compare("Button_service") == 0)
	{
		auto serviceLayer = ServiceLayer::create();
		serviceLayer->setName("serviceLayer");
		serviceLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SERVICE_TAG);
	}

	// 帮助
	else if (name.compare("Button_rule") == 0)
	{
		requestNotice(3);
		//auto rules = GameRules::create(l);
		//rules->setName("rules");
		//rules->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);

	}

	// 其他
	else
	{
		
	}
}

void GamePlatform::showExitLayer()
{
	auto exitLayer = GameExitLayer::create(true);
	exitLayer->setName("exitLayer");
	exitLayer->showExitLayer(this, CHILD_LAYER_ZORDER, CHILD_LAYER_EXIT_TAG);
	exitLayer->onExitCallBack = [this](){

		switchAccount();
	};
}

void GamePlatform::switchAccount()
{
	RoomLogic()->close();
	RoomLogic()->setRoomRule(0x0);
	PlatformLogic()->close();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

	if (PlatformLogic()->loginResult.bLogonType == WeChat)
	{
		UserDefault::getInstance()->setStringForKey(WECHAT_OPEN_ID, "");
		UserDefault::getInstance()->setStringForKey(WECHAT_ACCESS_TOKEN, "");
		UserDefault::getInstance()->setStringForKey(WECHAT_REFRESH_TOKEN, "");
		UserDefault::getInstance()->flush();

		// 删除微信平台的授权
		UMengSocial::getInstance()->doDedeteAuthorize(WEIXIN);
	}
	else if (PlatformLogic()->loginResult.bLogonType == QQLogin)
	{
		UserDefault::getInstance()->setStringForKey(QQ_OPEN_ID, "");
		UserDefault::getInstance()->setStringForKey(QQ_ACCESS_TOKEN, "");
		UserDefault::getInstance()->flush();

		// 删除QQ平台的授权
		UMengSocial::getInstance()->doDedeteAuthorize(QQ);
	}
	else {}
	UserDefault::getInstance()->setBoolForKey(AUTO_LOGIN, false);
#endif			

	GameMenu::createMenu();
}

void GamePlatform::openUMengShare()
{
	auto shareLayer = GameShareLayer::create();
	shareLayer->setName("shareLayer");
	shareLayer->SetShareCallBack([=](bool success, int platform, const std::string& errorMsg) {

		std::string str;
		if (success)
		{
			//str = GBKToUtf8("分享成功");
			//http://192.168.1.50/hy
			//http://shuju.qp888m.com/hyyl
			std::string path = StringUtils::format("/hyyl/TestShare.php?UserID=%d",
				PlatformLogic()->loginResult.dwUserID);
			std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
			HNHttpRequest::getInstance()->addObserver(this);
			HNHttpRequest::getInstance()->request("requestShareEvery", cocos2d::network::HttpRequest::Type::GET, url);
		}
		else
		{
			std::string str;
			if (str.empty()) {
				str = GBKToUtf8("分享失败");
			}
			else {
				str = errorMsg;
			}
			runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=]() {
				GamePromptLayer::create()->showPrompt(str);
			}), nullptr));
		}
	});
	std::string shareStr = HNPlatformLogic::getInstance()->getShareStr().shareStr;
	shareLayer->SetShareInfo(HNPlatformConfig()->getShareContent(), GBKToUtf8(shareStr), HNPlatformConfig()->getShareUrl());
	shareLayer->show();
}

void GamePlatform::menuLobbyMidCallBack(Ref* pSender)
{
	if (!_isTouch) return;
	_isTouch = false;

	auto btn = (Button*)pSender;
	btn->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	std::string name(btn->getName());

	if (name.compare("Button_create") == 0) //游戏大厅按钮
	{
		ClubID = 0;
		enterCreateRoomLayer(1);
	}
	else if (name.compare("Button_join") == 0)
	{
		auto join = JoinRoomLayer::create();
		join->setPosition(Vec2::ZERO);
		join->onEnterGameCallBack = [this](){

			//enterCreateRoomLayer();
		};
		addChild(join);
	}
	else if (name.compare("Button_games") == 0)
	{
// 		_lobbyUi->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([=](){
// 			_lobbyUi->setVisible(false);
// 			_layerType = GAMELIST;
// 			createGameListLayer();
// 		}), nullptr));

		GamePromptLayer::create()->showPrompt(GBKToUtf8("产品更新升级中。"));
		//ClubID = 0;
		//enterCreateRoomLayer(2);
	}
	//比赛场
	else if (name.compare("Button_match") == 0)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("产品更新升级中。"));
		//enterCreateRoomLayer(3);
		/*showTopAction(false, [=]() {
			
			auto match = GameMatchRegistration::create();
			match->setName("match");
			match->onCloseCallBack = [=]() {
				updateUserInfo();

				showTopAction(true, nullptr);

				if (_layerType == GAMELIST)
				{
					_gamesUi->runAction(FadeIn::create(0.3f));
				}
				else
				{
					_lobbyUi->runAction(FadeIn::create(0.3f));
				}
			};

			match->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, GAMELIST_LAYER_ZORDER, CHILD_LAYER_MATCH_TAG, false);
		});

		if (_layerType == GAMELIST)
		{
			_gamesUi->runAction(FadeOut::create(0.3f));
		}
		else
		{
			_lobbyUi->runAction(FadeOut::create(0.3f));
		}*/


		//比赛场列表
		//if (this->getChildByName("match") != nullptr)
		//	return;
		//auto winsize = Director::getInstance()->getWinSize();
		//auto match = GameMatchRegistration::create();
		///*match->_ReturniGameId = [=](){
		//	return 0;
		//};*/
		//match->setName("match");
		//match->setPosition(Vec2(0, -winsize.height));
		//match->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, GAMELIST_LAYER_ZORDER, CHILD_LAYER_MATCH_TAG, false);
		//match->setChangeDelegate(this);
	}
	else if (name.compare("Button_create_poke") == 0) //扑克游戏大厅按钮
	{
		ClubID = 0;
		enterCreateRoomLayer(3);
	}
	else if (name.compare("Button_create_majio") == 0) //麻将游戏大厅按钮
	{
		ClubID = 0;
		enterCreateRoomLayer(4);
	}
	else
	{

	}
}

void GamePlatform::enterCreateRoomLayer(int index)
{
	auto create = CreateRoomLayer::create(index);
	create->setPosition(Vec2::ZERO);
	create->setName("RoomLayer");
	//if (index != 1)create->createGameList(index);
	Layout* gamesui = create->getGamesLayout();
	goldgames = gamesui;
	create->onUpdateJewelCallBack = [this](){
		updateUserInfo();
	};
	create->_createListLayer = [this](){
		createGameListLayer();
	};
	create->onEnterGameCallBack = [this](){
		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("加载中"), 22);

		_layerType = ROOMLIST;
		_platformlogic->requestRoomList();
	};
	/*create->onCloseCallBack = [this](){
		_layerType = GAMELIST;
		updateUserInfo();
	};*/
	_CreateRoomLayer = create;
	addChild(create);
}

//创建俱乐部结果
bool GamePlatform::CreateClubResult(HNSocketMessage* socketMessage)
{
	DL_O_HALL_CreateClub* ClubResult = (DL_O_HALL_CreateClub*)socketMessage->object;
	switch (socketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建成功"));
		//俱乐部创建成功
		//RequestClubView(); 
		//茶楼创建成功
		//RequestTeaHouse();

		ClubTipsLayer->setCallBack([=](){
// 			int Userid = PlatformLogic()->loginResult.dwUserID;
// 			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LIST, &Userid, sizeof(Userid), HN_SOCKET_CALLBACK(GamePlatform::getTeaHouseResult, this)
			Club::createClubScene(0);
		});
		break; 
	}
	case(ERR_GP_NAME_LIMITE) : 
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！名称中有敏感字"));
		break;
	}
	case(3) :
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！权限不够"));
		break;
	}//权限不够
	case(4) :
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！超过创建限制"));
		break;
	}//超过限制
	case(5) :
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！超过创建限制"));
		break;
	}//加入限制
	case(6) :
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败！名称已被注册"));
		break;
	}
	default:
	{
		ClubTips* ClubTipsLayer = ClubTips::create();
		this->addChild(ClubTipsLayer);
		ClubTipsLayer->setTextCount(GBKToUtf8("创建失败!"));
	}
	}

	return true;
}
//加入俱乐部结果
bool GamePlatform::JoinClubResult(HNSocketMessage* SocketMessage)
{
	switch (SocketMessage->messageHead.bHandleCode)
	{
	case(ERR_GP_CLUB_REQUEST_SUCCESS) :
	{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("加入成功，请等待会长审核通过"));
// 				auto callback = [=](){
// 					MSG_GP_I_DissmissClub Request;
// 					Request.iClubID = ClubID;
// 					PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LeaveHall, &Request, sizeof(MSG_GP_I_DissmissClub));
// 					HNPlatformClubData::getInstance()->RemoveNetEventListener();
// 					this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
// 						GamePlatform::createNewPlatform();
// 					}), nullptr));
// 				};
// 				ClubTipsLayer->setCallBack([=](){
// 					callback();
// 				});
// 				ClubTipsLayer->setCancelCallback([=](){
// 					callback();
// 				});
				break;
	}
	case(2) :
	{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("超过加入茶楼限制"));
				break;

	}
	case(3) :
	{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("茶楼ID错误"));
				break;
	}
	case(4) :
	{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("茶楼人数超过限制"));
				break;
	}
	case(5) :
	{

				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("已被列入黑名单"));
				break;
	}
	case(6) :
	{

				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("已经在茶楼内"));
				break;
	}
	case(7) :
	{
				ClubTips* ClubTipsLayer = ClubTips::create();
				this->addChild(ClubTipsLayer);
				ClubTipsLayer->setTextCount(GBKToUtf8("请勿重复申请"));
				break;
	}

	}
	return true;
}
//获取俱乐部列表结果
bool GamePlatform::getTeaHouseResult(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		if (socketMessage->messageHead.bDataRule == 1)
		{
			//整个数据包长度除于单个结构体长度得出总的数量
			int num = socketMessage->objectSize / sizeof(MSG_GP_O_Club_List);
			//获取第一个结构体数据
			MSG_GP_O_Club_List* data = (MSG_GP_O_Club_List*)socketMessage->object;

			if (_isRequestClubList) {
				if (_MyCreate.size() > 0) _MyCreate.clear();
				if (_MyJoin.size() > 0) _MyJoin.clear();

				while (num-- > 0)
				{
					//区分我的创建和加入
					if (data->bCreater)_MyCreate.push_back(*data);
					else _MyJoin.push_back(*data);
					//获取下一个数据地址
					data++;
				}

				_ClubListView->removeAllItems();

				for (auto teaHouseData : _MyCreate)
				{
					createClubListItem(teaHouseData);
				}

				for (auto teaHouseData : _MyJoin)
				{
					createClubListItem(teaHouseData);
				}
			}
		}
		else if (socketMessage->messageHead.bDataRule == 0)
		{
			//查询到没有俱乐部信息
			if (_MyCreate.size() > 0) _MyCreate.clear();
			if (_MyJoin.size() > 0) _MyJoin.clear();
			_ClubListView->removeAllItems();
		}
		_isRequestClubList = false;
	}
	return true;
}


void GamePlatform::createClubListItem(MSG_GP_O_Club_List teaHouseData)
{
	auto node = CSLoader::createNode("platform/lobbyUi/clubItem_Node.csb");
	auto item = dynamic_cast<Layout*>(node->getChildByName("Panel_Lyaout"));

	item->removeFromParentAndCleanup(true);


	auto Text_teahouse_name = dynamic_cast<Text*>(item->getChildByName("Text_teahouse_name"));
	auto Text_Group_name = dynamic_cast<Text*>(item->getChildByName("Text_Group_name"));

	//茶楼名字
	string str = teaHouseData.szClubName;
	if (str.length() > 12)
	{
		str.erase(13, str.length());
		str.append("...");
		Text_teahouse_name->setString(StringUtils::format("%s (%d)", GBKToUtf8(str), teaHouseData.iClubID));
	}
	else
	{
		Text_teahouse_name->setString(StringUtils::format("%s (%d)", GBKToUtf8(str), teaHouseData.iClubID));
	}

	//楼主昵称
	string str1 = teaHouseData.szClubOwnerName;
	if (str1.length() > 12)
	{
		str1.erase(13, str.length());
		str1.append("...");
		Text_Group_name->setString(StringUtils::format(GBKToUtf8("群主:%s"), str1.c_str()));
	}
	else
	{
		Text_Group_name->setString(StringUtils::format(GBKToUtf8("群主:%s"), str1.c_str()));
	}

	Button* pJoinButton = dynamic_cast<Button*>(item->getChildByName("Button_join_club"));
	pJoinButton->setSwallowTouches(false);
	pJoinButton->addClickEventListener([=](Ref*){
		Club* pClub = Club::createClubScene(teaHouseData.iClubID);
		pClub->showTeaHouse(teaHouseData);
	});

	_ClubListView->pushBackCustomItem(item);
}


void GamePlatform::requestClubList()
{
	if (_MyCreate.size() > 0) _MyCreate.clear();
	if (_MyJoin.size() > 0) _MyJoin.clear();
	_ClubListView->removeAllItems();
	_isRequestClubList = true;
	int Userid = PlatformLogic()->loginResult.dwUserID;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_LIST, &Userid, sizeof(Userid), HN_SOCKET_CALLBACK(GamePlatform::getTeaHouseResult, this));
}

void GamePlatform::menuLobbyBottomCallBack(Ref* pSender)
{

	if (!_isTouch) return;
	_isTouch = false;

	auto btn = (Button*)pSender;
	btn->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	auto winSize = Director::getInstance()->getWinSize();

	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	std::string name(btn->getName());

	if (name.compare("Button_share") == 0)
	{
		//截屏分享
		openUMengShare();
	}
	else if (name.compare("Button_record") == 0)
	{
		auto record = GameRoomRecord::create();
		record->setName("record");
		record->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_RECORD_TAG);
	}
	else if (name.compare("Button_store") == 0)
	{
 		auto storeLayer = GameStoreLayer::createGameStore(this);
 		storeLayer->setName("storeLayer");
		storeLayer->ClickChangeStore(1);
 		storeLayer->setChangeDelegate(this);
		storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_STORE_TAG);
		/*auto pFunc = [=]()
		{
			auto giftShopLayer = GameGiftShop::createGameGiftShop(this, PlatformLogic()->loginResult.iLotteries);
			giftShopLayer->setName("giftShopLayer");
			giftShopLayer->open(ACTION_TYPE_LAYER::RIGHT, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_GIFT_TAG);

			giftShopLayer->onCloseCallBack = [=]() {
				updateUserInfo();
			};
		};
		storeLayer->clickChangeEventCallBack(pFunc);*/
 		storeLayer->onCloseCallBack = [this]()
 		{
 			updateUserInfo();
 		};
	}
	else if (name.compare("Button_more") == 0)
	{
		//推广以及实名认证推后
		btn->setTouchEnabled(false);
		if (_image_more->isVisible())
		{
			_real_tip->runAction(FadeIn::create(0.3f));
			_image_more->runAction(Sequence::create(ScaleTo::create(0.3f, 0.1), CallFunc::create([=](){btn->setTouchEnabled(true); _image_more->setVisible(false); }), nullptr));
		}
		else
		{
			_real_tip->runAction(FadeOut::create(0.3f));
			_image_more->runAction(Sequence::create(CallFunc::create([=](){btn->setTouchEnabled(true); _image_more->setVisible(true); }), ScaleTo::create(0.3f, 1), nullptr));
		}
		/*for (auto iter = ActionImage.begin(); iter != ActionImage.end(); iter++)
		{
			auto image = (ImageView*)(*iter);
			if (image->isVisible())
			{
				for (auto btn = ActionButton.begin(); btn != ActionButton.end(); btn++)(*btn)->stopAllActions();
				Vec2 Pos = btn->getPosition();
				auto spawn = Spawn::create(MoveTo::create(0.3f, Pos), ScaleTo::create(0.3f,0.1), nullptr);
				image->runAction(Sequence::create(spawn, CallFunc::create([=](){btn->setTouchEnabled(true); image->setVisible(false);}), nullptr));
			}
			else
			{
				auto spawn = Spawn::create(MoveTo::create(0.3f, Vec2((image->getContentSize().width*(image->getTag() - 10.5) + 20 * (image->getTag()-10)), image->getContentSize().height*1.5 + 10)), ScaleTo::create(0.5f, 1), nullptr);
				auto sequence = Sequence::create(spawn, CallFunc::create([=](){btn->setTouchEnabled(true); }), nullptr);
				image->setVisible(true);
				image->runAction(sequence);
				for (auto btn = ActionButton.begin(); btn != ActionButton.end(); btn++) 
				{ 
					auto sequence = Sequence::create(RotateTo::create(0.1, -7.5), RotateTo::create(0.1, 0), RotateTo::create(0.1, 7.5), RotateTo::create(0.1, 0),nullptr);
					(*btn)->runAction(Repeat::create(sequence,2));
				}
			}
		}*/
	}
	else if (name.compare("Button_autonym") == 0)
	{
		if (_image_more->isVisible())
		{
			_real_tip->runAction(FadeIn::create(0.3f));
			_image_more->runAction(Sequence::create(ScaleTo::create(0.3f, 0.1), CallFunc::create([=](){_image_more->setVisible(false); }), nullptr));
		}
		//实名
		certification();
	}
	else if (name.compare("Button_promote") == 0)
	{
		//推广

	}
	else if (name.compare("Button_notice") == 0)
	{
		requestNotice(2);
	}
	else if (name.compare("Button_task") == 0)
	{
		//任务
		UpdateTaskRed();
		auto gametaskLayer = GameTaskLayer::create();
		gametaskLayer->setName("gametaskLayer");
		gametaskLayer->AllViewListInit();
		gametaskLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TASK_TAG);
		gametaskLayer->onExitCallBack = [=]() {
			updateUserInfo();
		};
		gametaskLayer->onEnterGameCallBack = [=](){
			ClubID = 0;
			enterCreateRoomLayer(1);
		};
		gametaskLayer->onOpenStoreCallBack = [=](){
			OpenStoreLayer(1);
		};
	}
	else if (name.compare("Button_receive") == 0)
	{
		createWithdrawal();
	}
	else if (name.compare("Button_newbie") == 0)
	{
		//新手任务
		UpdateTaskRed();
		auto gametaskLayer = GameTaskLayer::create();
		gametaskLayer->setName("gametaskLayer");
		gametaskLayer->AllNewbieListInit();
		gametaskLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TASK_TAG);
		gametaskLayer->onExitCallBack = [=]() {
			updateUserInfo();
		};
	}
	else if (name.compare("Button_sign") == 0)
	{
		unSocketCallBack();
		GameSignLayer* signLayer = GameSignLayer::create();
		signLayer->setName("signLayer");
		signLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SIGN_TAG);
		signLayer->onUpdataUserMoney = [this](LLONG money)
		{
			walletChanged();
		};

		signLayer->onCloseCallBack = [this]()
		{
			checkGiftMessages();
		};
	}
	else if (name.compare("Button_Bigwheel") == 0)
	{
		TurnLayer* turnLayer = TurnLayer::create();
		this->addChild(turnLayer, 1000);
		turnLayer->setPosition(_winSize / 2);
		turnLayer->onUpdataUserMoney = [=](){
			updateUserInfo();
		};
		turnLayer->onSetUserMoney = [=](){
			updateLotteries();
		};
	}
	/*else if (name.compare("Button_gonggao") == 0)
	{
		NoticeList* noticeLayer = NoticeList::create();
		noticeLayer->setName("noticeLayer");
		noticeLayer->setPosition(Vec2(640,360));
		noticeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SERVICE_TAG);
	}*/
	else if (name.compare("Button_mail") == 0)
	{
		Mailbox* mailbox = Mailbox::create();
		mailbox->setName("mailbox");
		mailbox->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, 1000, 1000);
		mailbox->onUpdateCallBack = [=]() {
			updateUserInfo();
		};
	}
	/*else if (name.compare("Button_zhaodaili") == 0)
	{
		GameAgentApply* agentApply = GameAgentApply::create();
		agentApply->setName("Button_zhaodaili");
		agentApply->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SERVICE_TAG);
	}*/
	else if (name.compare("Button_jifen") == 0)
	{
		GameIntegralAct* integralAct = GameIntegralAct::create();
		//std::string text1 = "每日分享奖励";
		//std::string text2 = "分享微信好友，朋友圈，可领取奖励。";
		//integralAct->addIntegralItem(text1,text2);
		integralAct->setName("Button_jifen");
		integralAct->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SERVICE_TAG);
		integralAct->onOpenStoreCallBack = [=](){
			OpenStoreLayer(0);
		};
	}
	//else if (name.compare("Button_MySpread") == 0)
	//{
	//	auto AgentLayer = AgentLayer::create();
	//	AgentLayer->setName("AgentLayer");
	//	AgentLayer->open(ACTION_TYPE_LAYER::LEFT, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SERVICE_TAG, false);
	//	//AgentLayer->setPosition(-1500, 0);
	//	//AgentLayer->setZOrder(CHILD_LAYER_ZORDER);
	//	//AgentLayer->setTag(CHILD_LAYER_SERVICE_TAG);
	//	//addChild(AgentLayer);
	//	//AgentLayer->runAction(MoveTo::create(0.5f, Vec2::ZERO));
	//}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("程序猿使劲开发中。"));
	}

}

//排行榜按钮回调
void GamePlatform::menuRankCallBack(Ref* pSender)
{
	if (!_isTouch) return;
	_isTouch = false;

	auto btn = (Button*)pSender;
	btn->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	auto winSize = Director::getInstance()->getWinSize();

	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	std::string name(btn->getName());
	std::cout << name << std::endl;
	if (name.compare("Button_1") == 0)
	{
		_vecRankBtnList[0]->setEnabled(false);
		_vecRankBtnList[1]->setEnabled(true);
		_vecRankBtnList[2]->setEnabled(true);
		_vecRankList[0]->setVisible(true);
		_vecRankList[1]->setVisible(false);
		_vecRankList[2]->setVisible(false);
		_vecSp_Rank[0]->setVisible(true);
		_vecSp_Rank[1]->setVisible(false);
		_vecSp_Rank[2]->setVisible(false);
	}
	else if (name.compare("Button_2") == 0)
	{
		_vecRankBtnList[1]->setEnabled(false);
		_vecRankBtnList[0]->setEnabled(true);
		_vecRankBtnList[2]->setEnabled(true);
		_vecRankList[1]->setVisible(true);
		_vecRankList[0]->setVisible(false);
		_vecRankList[2]->setVisible(false);
		_vecSp_Rank[1]->setVisible(true);
		_vecSp_Rank[0]->setVisible(false);
		_vecSp_Rank[2]->setVisible(false);
	}
	else if (name.compare("Button_3") == 0)
	{
		_vecRankBtnList[2]->setEnabled(false);
		_vecRankBtnList[1]->setEnabled(true);
		_vecRankBtnList[0]->setEnabled(true);
		_vecRankList[2]->setVisible(true);
		_vecRankList[1]->setVisible(false);
		_vecRankList[0]->setVisible(false);
		_vecSp_Rank[2]->setVisible(true);
		_vecSp_Rank[1]->setVisible(false);
		_vecSp_Rank[0]->setVisible(false);
	}
	else if (name.compare("btn_close") == 0)
	{
		btn->setVisible(false);
		_rankbg->runAction(Sequence::create(MoveTo::create(0.5f, Vec2(160.88, 592.43)), CallFunc::create([=](){_RankingOpenBtn->setVisible(true); }), nullptr));
		_rankwicker->runAction(Spawn::create(FadeOut::create(0.5f), CallFunc::create([=](){
			_vecSp_Rank[0]->runAction(FadeOut::create(0.5f));
			_vecSp_Rank[1]->runAction(FadeOut::create(0.5f));
			_vecSp_Rank[2]->runAction(FadeOut::create(0.5f));
		}), nullptr));
		//_rankYear->runAction(FadeIn::create(0.5f));
		//_rankYear1->runAction(FadeIn::create(0.5f));
	}
	else if (name.compare("btn_open") == 0)
	{
		btn->setVisible(false);
		_rankbg->runAction(Sequence::create(MoveTo::create(0.5f, Vec2(160.88, 216.43)), CallFunc::create([=](){_RankingCloseBtn->setVisible(true); }), nullptr));
		_rankwicker->runAction(Spawn::create(FadeIn::create(0.5f), CallFunc::create([=](){
			_vecSp_Rank[0]->runAction(FadeIn::create(0.5f));
			_vecSp_Rank[1]->runAction(FadeIn::create(0.5f));
			_vecSp_Rank[2]->runAction(FadeIn::create(0.5f));
		}), nullptr));
		//_rankYear->runAction(FadeOut::create(0.5f));
		//_rankYear1->runAction(FadeOut::create(0.5f));
	}
}

//底部弹出框按钮回调
void GamePlatform::menuMoreCallBack(Ref* pSender)
{
	if (!_isTouch) return;
	_isTouch = false;

	auto btn = (Button*)pSender;
	btn->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	auto winSize = Director::getInstance()->getWinSize();

	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	std::string name(btn->getName());
	std::cout << name << std::endl;
	if (name.compare("Button_sign") == 0)
	{
		unSocketCallBack();
		GameSignLayer* signLayer = GameSignLayer::create();
		signLayer->setName("signLayer");
		signLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SIGN_TAG);
		signLayer->onUpdataUserMoney = [this](LLONG money)
		{
			walletChanged();
		};

		signLayer->onCloseCallBack = [this]()
		{
			checkGiftMessages();
		};
	}

	else if (name.compare("Button_rank") == 0)
	{
		auto rankLayer = GameRankingList::create();
		rankLayer->setName("rankLayer");
		rankLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_RANK_TAG);
	}
	else if (name.compare("Button_mail") == 0)
	{
		/*NoticeList* noticeList = NoticeList::create();
		this->addChild(noticeList, 1000);
		noticeList->setPosition(winSize / 2);*/

		Mailbox* mailbox = Mailbox::create();
		mailbox->setName("mailbox");
		mailbox->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, 1000, 1000);
		mailbox->onUpdateCallBack = [=]() {
			updateUserInfo();
		};
	}
	else if (name.compare("Button_exchange") == 0)
	{
		auto giftShopLayer = GameGiftShop::createGameGiftShop(this, PlatformLogic()->loginResult.iLotteries);
		giftShopLayer->setName("giftShopLayer");
		giftShopLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_GIFT_TAG);

		giftShopLayer->onCloseCallBack = [=]() {
			updateUserInfo();
		};
	}
	else if (name.compare("Button_bank") == 0)
	{
		auto bankLayer = GameBankLayer::createGameBank();
		bankLayer->setName("bankLayer");
		bankLayer->setChangeDelegate(this);

		bankLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_BANK_TAG);
	}
	else if (name.compare("Button_notice") == 0)
	{
		requestNotice(2);
		/*auto notice = NoticeLayer::create();
		notice->setName("notice");
		notice->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);*/
	}
	/*else if (name.compare("Button_jifen") == 0)
	{
		GameIntegralAct* integralAct = GameIntegralAct::create();
		std::string text1 = "每日分享奖励";
		std::string text2 = "分享微信好友，朋友圈，可领取奖励。";
		integralAct->addIntegralItem(text1, text2);
		integralAct->setName("Button_jifen");
		integralAct->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SERVICE_TAG);
		integralAct->onCloseCallBack = [=]() {
			updateUserInfo();
		};
	}*/
}

void GamePlatform::onKeyReleased(EventKeyboard::KeyCode keyCode,Event * pEvent)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if (EventKeyboard::KeyCode::KEY_BACK == keyCode)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (EventKeyboard::KeyCode::KEY_F1 == keyCode)
#endif
	{
		do 
		{
			ServiceLayer* serviceLayer = dynamic_cast<ServiceLayer*>(this->getChildByTag(CHILD_LAYER_SERVICE_TAG));
			if (nullptr != serviceLayer)
			{
				serviceLayer->close();
				break;
			}

			GameSetLayer* setLayer = dynamic_cast<GameSetLayer*>(this->getChildByTag(CHILD_LAYER_SET_TAG));
			if (nullptr != setLayer)
			{
				setLayer->close();
				break;
			}

			GameUserDataLayer* userDataLayer = dynamic_cast<GameUserDataLayer*>(this->getChildByTag(CHILD_LAYER_USERHEAD_TAG));
			if (nullptr != userDataLayer)
			{
				userDataLayer->close();
				break;
			}

			BindPhoneLayer* bindPhone = dynamic_cast<BindPhoneLayer*>(this->getChildByTag(CHILD_LAYER_PHONE_TAG));
			if (nullptr != bindPhone)
			{
				bindPhone->close();
				break;
			}

			ModifyPasswordLayer* modifyPass = dynamic_cast<ModifyPasswordLayer*>(this->getChildByTag(CHILD_LAYER_PASS_TAG));
			if (nullptr != modifyPass)
			{
				modifyPass->close();
				break;
			}

			GameSignLayer* gameSign = dynamic_cast<GameSignLayer*>(this->getChildByTag(CHILD_LAYER_SIGN_TAG));
			if (nullptr != gameSign)
			{
				gameSign->close();
				break;
			}

			GameRankingList* gameRank = dynamic_cast<GameRankingList*>(this->getChildByTag(CHILD_LAYER_RANK_TAG));
			if (nullptr != gameRank)
			{
				gameRank->close();
				break;
			}

			GameBankLayer* bankLayer = dynamic_cast<GameBankLayer*>(this->getChildByTag(CHILD_LAYER_BANK_TAG));
			if (nullptr != bankLayer)
			{
				bankLayer->close();
				break;
			}

			//GameStoreLayer* storeLayer = dynamic_cast<GameStoreLayer*>(this->getChildByTag(CHILD_LAYER_STORE_TAG));
			//if (nullptr != storeLayer)
			//{
			//	storeLayer->close();
			//	break;
			//}

			GameMatchRegistration* matchLayer = dynamic_cast<GameMatchRegistration*>(this->getChildByTag(CHILD_LAYER_MATCH_TAG));
			if (nullptr != matchLayer)
			{
				matchLayer->close();
				break;
			}

			GameOnlineReward* OnlineReward = dynamic_cast<GameOnlineReward*>(this->getChildByTag(CHILD_LAYER_ONLINE_TAG));
			if (nullptr != OnlineReward)
			{
				OnlineReward->close();
				break;
			}

			GameRoomRecord* recordLayer = dynamic_cast<GameRoomRecord*>(this->getChildByTag(CHILD_LAYER_RECORD_TAG));
			if (nullptr != recordLayer)
			{
				recordLayer->close();
				break;
			}

			GameGiftShop* giftLayer = dynamic_cast<GameGiftShop*>(this->getChildByTag(CHILD_LAYER_GIFT_TAG));
			if (nullptr != giftLayer)
			{
				giftLayer->close();
				break;
			}

			//// 调用离开游戏层
			//GameExitLayer* exitLayer = dynamic_cast<GameExitLayer*>(this->getChildByTag(CHILD_LAYER_EXIT_TAG));
			//if (nullptr == exitLayer)
			//{
			//	showExitLayer();
			//}
			//else
			//{
			//	exitLayer->close();
			//}
		} while (0);
	}
}

void GamePlatform::updateUserInfo()
{
	PlatformLogic()->sendData(MDM_GP_BANK, ASS_GP_BANK_FINANCE_INFO, nullptr, 0, [this](HNSocketMessage* socketMessage) {

		CHECK_SOCKET_DATA_RETURN(TMSG_GP_BankFinanceInfo, socketMessage->objectSize, true, "TMSG_GP_BankFinanceInfo size is error");
		TMSG_GP_BankFinanceInfo *financeInfo = (TMSG_GP_BankFinanceInfo*)socketMessage->object;

		if (financeInfo->iUserID == PlatformLogic()->loginResult.dwUserID)
		{
			PlatformLogic()->loginResult.i64Bank = financeInfo->i64BankMoney;
			PlatformLogic()->loginResult.i64Money = financeInfo->i64WalletMoney;
			PlatformLogic()->loginResult.iLotteries = financeInfo->iLotteries;
			PlatformLogic()->loginResult.iJewels = financeInfo->iJewels;

			walletChanged();
		}		

		PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_FINANCE_INFO);

		return true;
	});
}

void GamePlatform::walletChanged()
{
	updateMoney();
	updateDiamond();
	updateLotteries();
}

void GamePlatform::updateMoney()
{
	do 
	{
		CC_BREAK_IF(nullptr == _label_golds);

		std::string str = HNToWan(PlatformLogic()->loginResult.i64Money);
		_label_golds->setString(str);

	} while (0);
}

void GamePlatform::updateDiamond()
{
	do
	{
		CC_BREAK_IF(nullptr == _label_Jewels);

		std::string str = HNToWan(PlatformLogic()->loginResult.iJewels + PlatformLogic()->loginResult.iLockJewels);
		_label_Jewels->setString(str);

	} while (0);
}

void GamePlatform::updateLotteries()
{
	do
	{
		CC_BREAK_IF(nullptr == _label_lotterys);

		std::string str = HNToWan(PlatformLogic()->loginResult.iLotteries);
		_label_lotterys->setString(str);

	} while (0);
}

void GamePlatform::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;

	if (requestName.compare("notice") == 0)
	{
		dealNoticeResp(responseData);
	}
	else if (requestName.compare("updateIAPPay") == 0)
	{
		onIAPGetMoneyCallback(responseData);
	}
	else if (requestName.compare("requestShareEvery") == 0)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str()); 

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}
		int code = doc["code"].GetInt();
		if (code == 0)
		{
			updateUserInfo();
			if (doc.HasMember("msg"))
			{
				std::string rs = doc["msg"].GetString();
				GamePromptLayer::create()->showPrompt(rs);
			}
			//任务进度更新
			//HNPlatformTaskList::getInstance()->RequestAllTaskList();
		}
	}
	else if (requestName.compare("requestRealName") == 0)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}
		int code = doc["verified"].GetInt();
		if (code == 0)
		{
			//还没有实名认证（大厅显示红标）
			_real_tip->setVisible(true);
		}
		else
		{
			//已经实名认证（去掉红标）
			_real_tip->setVisible(false);
			Real_Data.Name = doc["realname"].GetString();
			Real_Data.identity = doc["zj_number"].GetString();
			Real_Data.PhoneNum = doc["phone"].GetString();
			auto pLayer = dynamic_cast<GameCertificationLayer*>(this->getChildByName("GameCertificationLayer"));
			if (pLayer)
			{
				pLayer->updateLayer(Real_Data);
			}
		}
	}
	else if (requestName.compare("requestGold") == 0)
	{
		dealRankData(responseData, 1);
	}
	else if (requestName.compare("requestLottery") == 0)
	{
		dealRankData(responseData, 2);
	}
	else if (requestName.compare("requestRoom") == 0)
	{
		dealRankData(responseData, 0);
	}
	else
	{

	}
}

void GamePlatform::requestNotice(int one)
{
	/*std::string url = HNPlatformConfig()->getNoticeUrl();
	url.append("Type=GetSystemMsg");
	url.append("&pageSize=1");
	string str = StringUtils::format("&pageindex=%u", one);
	url.append(str);*/
	std::string url = "";
	switch (one)
	{
	case 1: //滚动公告
		url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/RNotice.php");
		break;
	case 2: //界面公告
		url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/INotice.php");
		break;
	case 3: //帮助公告
		url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/HNotice.php");
		break;
	default:
		break;
	}

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("notice", HttpRequest::Type::GET, url);
	num = one;
}

void GamePlatform::dealNoticeResp(const std::string& data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError() || !doc.IsObject())
	{
		return;
	}
	if (doc.HasMember("List") && doc["List"].IsArray() && doc["List"].Capacity() > 0)
	{
		if (1 == num)
		{
			for (int i = 0; i < doc["List"].Size(); i++)
			{
				rapidjson::Value& value = doc["List"][(rapidjson::SizeType)i];
				std::string message = value["MContent"].GetString();
				GameNotice::getInstance()->postMessage(message, 3);
			}
		}
		else if (2 == num)
		{
			auto notice = NoticeLayer::create();
			for (int i = 0; i < doc["List"].Size(); i++)
			{
				rapidjson::Value& value = doc["List"][(rapidjson::SizeType)i];
				std::string szTitle = value["MTitle"].GetString();
				std::string szContent = value["MContent"].GetString();
				std::string szType = value["Type"].GetString();
				notice->addTextInfo(szTitle, szContent,szType);
			}
			notice->setName("notice");
			notice->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);
			notice->updateShow();
		}
		else if (3 == num)
		{
			auto rules = GameRules::create();
			for (int i = 0; i < doc["List"].Size(); i++)
			{
				rapidjson::Value& value = doc["List"][(rapidjson::SizeType)i];
				std::string szTitle = value["MTitle"].GetString();
				std::string szContent = value["MContent"].GetString();
				std::string szType = value["Type"].GetString();
				rules->addTextInfo(szTitle, szContent, szType);
			}
			rules->setName("rules");
			rules->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);
			rules->updateShow();
		}
	}
	/*
	if (doc.HasMember("list") && doc["list"].IsArray() && doc["list"].Capacity() > 0)
	{
		rapidjson::Value& value = doc["list"][(rapidjson::SizeType)0];
		std::string message = value["MContent"].GetString();
		if (num == 1)
		{//滚动系统通知
			GameNotice::getInstance()->postMessage(message, 3);
		}
		else if (num == 2)
		{
			//打开公告
			auto notice = NoticeLayer::create();
			notice->setTextinfo(message);
			notice->setName("notice");
			notice->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);
		}

		//if (message.find(GBKToUtf8("欢赢娱乐")) != string::npos)
		//{
		//	GameNotice::getInstance()->postMessage(message, 3);
		//}
		//else
		//{
		//	//string str123;
		//    //str123 = GBKToUtf8(message);
		//
		//}
	}
	*/
}

//显示广播通知
void GamePlatform::onPlatformNewsCallback(const std::string& message)
{
	
}

void GamePlatform::onPlatformDisConnectCallback(const std::string& message)
{
	
}

void GamePlatform::getModuleConfig()
{
	PlatformLogic()->sendData(MDM_GP_GET_CONFIG, ASS_GP_GET_CONFIG, nullptr, 0, [=](HNSocketMessage* socketMessage) {

		CHECK_SOCKET_DATA_RETURN(MSG_GP_S_GET_CONFIG_RES, socketMessage->objectSize, true,
			"MSG_GP_S_GET_CONFIG_RES size of error!");

		MSG_GP_S_GET_CONFIG_RES * config = (MSG_GP_S_GET_CONFIG_RES*)socketMessage->object;
		
		for (int i = 0; i < config->iCount; i++)
		{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			bool value = config->_data[i].bShowIos;
#else
			bool value = config->_data[i].bShowAndroid;
#endif
			Configuration::getInstance()->setValue(config->_data[i].name, Value(value));
		}

		// 复制id
		//auto btn_popularize = dynamic_cast<Button*>(_topUi->getChildByName("Button_copy"));
		//btn_popularize->setVisible(Configuration::getInstance()->getValue("promotion", Value(false)).asBool());

		// 商城
		auto btn_store = dynamic_cast<Button*>(_bottomUi->getChildByName("Button_store"));
		btn_store->setVisible(Configuration::getInstance()->getValue("mall", Value(false)).asBool());

		// 底部位置（从左到右）
		//const int BOTTOM_COUNT = 5;

		//auto layout_more = dynamic_cast<Layout*>(_bottomUi->getChildByName("Panel_more"));

		//// 底部位置
		//Vec2 bottom_position[BOTTOM_COUNT];
		//for (int i = 0; i < BOTTOM_COUNT; i++)
		//{
		//	auto btn = dynamic_cast<Button*>(layout_more->getChildByTag(i));
		//	bottom_position[i] = btn->getPosition();
		//}

		//int index = 0;

		//std::string bottomKey[] = { "coffer", "exchange", "ranklist", "service", "signin" };
		//for (int i = 0; i < 5; i++)
		//{
		//	auto btn = dynamic_cast<Button*>(layout_more->getChildByTag(i));
		//	if (Configuration::getInstance()->getValue(bottomKey[i], Value(false)).asBool())
		//	{
		//		btn->setVisible(true);
		//		btn->setPosition(bottom_position[index++]);
		//	}
		//	else
		//	{
		//		btn->setVisible(false);
		//	}
		//}

		//// 设置按钮框背景缩放
		//auto img_moreBG = dynamic_cast<ImageView*>(layout_more->getChildByName("Image_moreBG"));
		//img_moreBG->setContentSize(Size(133 + index * 73.4, img_moreBG->getContentSize().height));

		PlatformLogic()->removeEventSelector(MDM_GP_GET_CONFIG, ASS_GP_GET_CONFIG);

		return true;
	});
}

void GamePlatform::playOrStopJump(cocos2d::Node* pNode, bool bJump)
{
	if(!pNode) return;

	if (pNode->getUserData())
	{
		pNode->stopAllActions();
		auto data = (cocos2d::__String*)pNode->getUserData();
		pNode->setPositionY(data->floatValue());
	}

	if (bJump)
	{
		_Data.insert(pair<std::string, cocos2d::__String>(pNode->getName(), *__String::createWithFormat("%f", pNode->getPositionY())));
		
		auto data = _Data.find(pNode->getName());
		if (data != _Data.end())
		{
			pNode->setUserData(&data->second);
		}

		auto moveBy = MoveBy::create(0.5f, Vec2(0.f, 40.f));
		auto sequence = Sequence::create(EaseBackIn::create(moveBy), DelayTime::create(0.1f),
			EaseElasticOut::create(moveBy->reverse()), DelayTime::create(0.5f), nullptr);
		pNode->runAction(RepeatForever::create(sequence));
	}	
}

void GamePlatform::showTopAction(bool isShow, std::function<void()> func)
{
	FadeTo* action = nullptr;
	if (isShow)
	{
		action = FadeIn::create(0.3f);
		_topUi->setVisible(true);
		_bottomUi->setVisible(true);
	}
	else
	{
		action = FadeOut::create(0.3f);
	}

	_topUi->runAction(Sequence::create(action, CallFunc::create([=]() {
	
		_topUi->setVisible(isShow);
		_bottomUi->setVisible(isShow);

		if (func) func();
	}), nullptr));

	_bottomUi->runAction(action);
}

void GamePlatform::addSpine()
{
	// 金币
	/*auto coinAni = SkeletonAnimation::createWithFile("platform/animation/coin/default.json", "platform/animation/coin/default.atlas");
	coinAni->setPosition(_spriteGoldLogo->getPosition());
	_spriteGoldLogo->getParent()->addChild(coinAni);
	coinAni->setAnimation(0, "jinbi", true);
	_spriteGoldLogo->setVisible(false);*/

// 	// 钻石
// 	auto diaAni = SkeletonAnimation::createWithFile("platform/animation/diamond/default.json", "platform/animation/diamond/default.atlas");
// 	diaAni->setPosition(_spriteDiamondLogo->getPosition());
// 	_spriteDiamondLogo->getParent()->addChild(diaAni);
// 	diaAni->setAnimation(0, "animation", true);
// 	_spriteDiamondLogo->setVisible(false);

	// 创建房间
// 	auto createAni = SkeletonAnimation::createWithFile("platform/animation/createRoom/default.json", "platform/animation/createRoom/default.atlas");
// 	createAni->setPosition(Vec2(_button_create->getContentSize().width / 2, _button_create->getContentSize().height / 2));
// 	_button_create->addChild(createAni);
// 	createAni->setAnimation(0, "animation", true);

	// 加入房间
// 	auto joinAni = SkeletonAnimation::createWithFile("platform/animation/joinRoom/default.json", "platform/animation/joinRoom/default.atlas");
// 	joinAni->setPosition(Vec2(_button_join->getContentSize().width / 2, _button_join->getContentSize().height / 2));
// 	_button_join->addChild(joinAni);
// 	joinAni->setAnimation(0, "animation", true);

// 	// 金币场
// 	auto coinRoomAni = SkeletonAnimation::createWithFile("platform/animation/coinRoom/default.json", "platform/animation/coinRoom/default.atlas");
// 	coinRoomAni->setPosition(Vec2(_button_Gold->getContentSize().width / 2, _button_Gold->getContentSize().height / 2));
// 	_button_Gold->addChild(coinRoomAni);
// 	coinRoomAni->setAnimation(0, "animation", true);

// 	// 比赛场
// 	auto matchRoomAni = SkeletonAnimation::createWithFile("platform/animation/matchRoom/default.json", "platform/animation/matchRoom/default.atlas");
// 	matchRoomAni->setPosition(Vec2(_button_Match->getContentSize().width / 2, _button_Match->getContentSize().height / 2));
// 	_button_Match->addChild(matchRoomAni);
// 	matchRoomAni->setAnimation(0, "animation", true);

	//// 商城
	//auto storeAni = SkeletonAnimation::createWithFile("platform/animation/store/default.json", "platform/animation/store/default.atlas");
	//storeAni->setPosition(Vec2(70.0f, 45.0f));
	//_buttonStore->addChild(storeAni);
	//storeAni->setAnimation(0, "animation", true);

	// 俱乐部
	/*auto clubAni = SkeletonAnimation::createWithFile("platform/animation/club/default.json", "platform/animation/club/default.atlas");
	clubAni->setPosition(_panelClub->getContentSize() / 2);
	_panelClub->addChild(clubAni);
	clubAni->setAnimation(0, "animation", true);*/

	// 背景
	//auto screenAni = SkeletonAnimation::createWithFile("platform/animation/light/default.json", "platform/animation/light/default.atlas");
	//screenAni->setPosition(Vec2(338.0f, 621.0f));
	//_panelBg->addChild(screenAni);
	//screenAni->setAnimation(0, "animation", true);
}

void GamePlatform::updateIAPPay(Ref* pSender)
{
	std::string requestData = UserDefault::getInstance()->getStringForKey("Redbird_IAPPayData", "");
	if (requestData.compare("") != 0)
	{
		std::string url_tmp = PlatformConfig::getInstance()->getPayCallbackUrl_iOS();
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("updateIAPPay", HttpRequest::Type::POST, url_tmp, requestData);
	}
}

void GamePlatform::supportAlert()
{
	std::string str;
	str.append(GBKToUtf8("发货失败，请联系客服。"));
	GamePromptLayer::create()->showPrompt(str);
}

void GamePlatform::onIAPGetMoneyCallback(const std::string& data)
{
	std::string cache = data;
	if (data.empty())
	{
		log("data is empty");
		supportAlert();
		return;
	}
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError())
	{
		supportAlert();
		return;
	}

	if (!doc.IsObject() || !doc.HasMember("rs") || !doc.HasMember("msg"))
	{
		supportAlert();
		return;
	}

	int rs = doc["rs"].GetInt();
	if (rs == 1)
	{
		if (doc.HasMember("stype"))
		{
			int type = doc["stype"].GetInt();

			if (type == 1)
			{
				if (doc.HasMember("WalletMoney"))
				{
					LLONG money = doc["WalletMoney"].GetInt64();
					std::string strCnt = UserDefault::getInstance()->getStringForKey("ProductNumber", "");
					std::string str = StringUtils::format(GBKToUtf8("购买成功 , 获得%s金币！"), strCnt.c_str());
					GamePromptLayer::create()->showPrompt(str);
					PlatformLogic()->loginResult.i64Money = money;
					walletChanged();
					UserDefault::getInstance()->setStringForKey("Redbird_IAPPayData", "");
					UserDefault::getInstance()->setStringForKey("ProductNumber", "");
				}
			}
			else if (type == 2)
			{
				if (doc.HasMember("Jewels"))
				{
					LLONG Jewels = doc["Jewels"].GetInt64();
					std::string strCnt = UserDefault::getInstance()->getStringForKey("ProductNumber", "");
					std::string str = StringUtils::format(GBKToUtf8("购买成功, 获得:%s房卡！"), strCnt.c_str());
					GamePromptLayer::create()->showPrompt(str);
					PlatformLogic()->loginResult.iJewels = Jewels;
					walletChanged();
					UserDefault::getInstance()->setStringForKey("Redbird_IAPPayData", "");
					UserDefault::getInstance()->setStringForKey("ProductNumber", "");
				}
			}
			else
			{
				supportAlert();
			}
		}
	}
	else if (rs == 2)
	{
		UserDefault::getInstance()->setStringForKey("Redbird_IAPPayData", "");
		UserDefault::getInstance()->setStringForKey("ProductNumber", "");
		GamePromptLayer::create()->showPrompt(GBKToUtf8("您购买的商品已到帐"));
	}
	else
	{
		supportAlert();
	}
	UserDefault::getInstance()->flush();
}

void GamePlatform::MySpread()
{

}

void GamePlatform::RequestRealName()
{
	std::string url = StringUtils::format("http://alipay.qp888m.com/app/get_identity_info.php?userid=%d",
		PlatformLogic()->loginResult.dwUserID);
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("requestRealName", HttpRequest::Type::GET, url);
}

void GamePlatform::RequestRankData()
{
	std::string url = "http://alipay.qp888m.com/c779app/rankingList.php?type=1";
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("requestGold", HttpRequest::Type::GET, url);

	std::string url2 = "http://alipay.qp888m.com/c779app/rankingList.php?type=2";
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("requestLottery", HttpRequest::Type::GET, url2);

	std::string url3 = "http://alipay.qp888m.com/c779app/rankingList.php?type=3";
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("requestRoom", HttpRequest::Type::GET, url3);

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("加载中"), 22);

}

void GamePlatform::dealRankData(const std::string& data, int index)
{
	rapidjson::Document doc;
	RankData RankingData;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError() || !doc.IsArray())
	{
		return;
	}
	rapidjson::Value& arrayRank = doc;
	if (index == 1)_VecGold.clear();
	if (index == 2)_VecLottery.clear();
	if (index == 0)_VecRoomCard.clear();

	for (int i = 0; i < doc.Size(); i++)
	{
		RankingData.Rank = arrayRank[i]["Rank"].GetInt();
		RankingData.UserID = arrayRank[i]["UserID"].GetInt();
		RankingData.Score = arrayRank[i]["Score"].GetInt64();
		RankingData.VipLevel = arrayRank[i]["VipLevel"].GetInt();
		RankingData.NickName = arrayRank[i]["NickName"].GetString();
		RankingData.HeaUrl = arrayRank[i]["HeadUrl"].GetString();

		if (index == 1)
		{
			_VecGold.push_back(RankingData);
		}
		else if (index == 2)
		{
			_VecLottery.push_back(RankingData);
		}
		else
		{
			_VecRoomCard.push_back(RankingData);
		}
	}

	//if (index == 0)
	//{
		UpdataRank(index);
	//}
}

void GamePlatform::UpdataRank(int index)
{
	if (_vecRankList[index])
	{
		//_vecRankList[index]->removeAllItems();
		if (index == 1)			//财富
		{
			for (int i = 0; i < _VecGold.size(); i++)
			{
				auto Item = dynamic_cast<ImageView*>(_vecRankList[index]->getChildByName(StringUtils::format("image_bg_%d", i+1)));
				if (Item)
				{
					//auto Item = dynamic_cast<Layout*>(Node->getChildByName("panel_clone_1"));
					//dynamic_cast<Layout*>(_rankUi->getChildByName(StringUtils::format("panel_clone_", index + 1)))->clone();
					//名次显示
					log("RANK======Gold======%d", _VecGold[i].Rank);
					if (_VecGold[i].Rank < 4)
					{
						ImageView* image_rank = dynamic_cast<ImageView*>(Item->getChildByName("image_rank"));
						image_rank->loadTexture(StringUtils::format("platform/common/new/phb/image_rank_%d.png", _VecGold[i].Rank));
						image_rank->setVisible(true);
					}
					else
					{
						TextAtlas* Text_Rank = dynamic_cast<TextAtlas*>(Item->getChildByName("atlas_num"));
						Text_Rank->setString(to_string(_VecGold[i].Rank));
						Text_Rank->setVisible(true);
					}
					//头像
					ImageView* Image_Head = dynamic_cast<ImageView*>(Item->getChildByName("image_tempHead"));
					ImageView* Frame = dynamic_cast<ImageView*>(Item->getChildByName("image_headFrame"));
					auto head = GameUserHead::create(Image_Head);
					head->show(USER_HEAD_MASK);
					head->loadTextureWithUrl(_VecGold[i].HeaUrl);
					//head->setHeadByFaceID(_VecGold[i].UserID);			//排行榜没有FaceID字段
					head->setSwallowTouches(false);
					Frame->setVisible(false);
					if (_VecGold[i].VipLevel > 0)
					{
						Frame->setVisible(true);
						Frame->loadTexture(StringUtils::format("platform/common/VIP_%d.png", _VecGold[i].VipLevel));
						Frame->setZOrder(10000);
					}
					auto nodeeffect = Frame->getChildByName("EffectNode");
					if (nodeeffect)
					{
						nodeeffect->removeFromParentAndCleanup(true);
					}
					if (_VecGold[i].VipLevel > 2)
					{
						auto nodeEffect = CSLoader::createNode(StringUtils::format("platform/common/vip_%d_effect.csb", _VecGold[i].VipLevel));
						nodeEffect->setAnchorPoint(Vec2(0.5, 0.5));
						nodeEffect->setScale(0.5);
						nodeEffect->setPosition(Vec2(Frame->getContentSize().width / 2, Frame->getContentSize().height / 2));
						auto ation = CSLoader::createTimeline(StringUtils::format("platform/common/vip_%d_effect.csb", _VecGold[i].VipLevel));
						nodeEffect->runAction(ation);
						ation->gotoFrameAndPlay(0, true);
						nodeEffect->setName("EffectNode");
						Frame->addChild(nodeEffect);
					}

					//名字
					auto Text_Name = dynamic_cast<Text*>(Item->getChildByName("text_name"));
					if (_VecGold[i].VipLevel > 1)
					{
						Text_Name->setColor(Color3B(255, 0, 0));
					}
					else
					{
						Text_Name->setColor(Color3B(166, 89, 30));
					}

					//用户名字长度剪切处理
					//string str = StringUtils::toString(_VecGold[i].NickName);
					//if (str.length() > 10)
					//{
					//	str.erase(11, str.length());
					//	str.append("...");
					//	if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
					//	{
					//		Text_Name->setString(GBKToUtf8(str));
					//	}
					//	else
					//	{
					//		Text_Name->setString(Utf8ToGBK(str));
					//	}
					//}
					//else
					//{
					//	if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
					//	{
					//		Text_Name->setString(GBKToUtf8(str));
					//	}
					//	else
					//	{
					//		Text_Name->setString(Utf8ToGBK(str));
					//	}
					//}

					Text_Name->setString(StringUtils::toString(_VecGold[i].NickName));
					//成绩
					auto Text_Score = dynamic_cast<Text*>(Item->getChildByName("text_score"));
					Text_Score->setString(StringUtils::toString(_VecGold[i].Score));
					//压入容器
					Item->setVisible(true);
					//_vecRankList[index]->pushBackCustomItem(Clone_layout);
				}
			}
		}
		else if (index == 2)					//礼券榜
		{
			for (int i = 0; i < _VecLottery.size(); i++)
			{
				auto Item = dynamic_cast<ImageView*>(_vecRankList[index]->getChildByName(StringUtils::format("image_bg_%d", i+1)));
				if (Item)
				{
					//auto Item = dynamic_cast<Layout*>(Node->getChildByName("panel_clone_1"));
					//dynamic_cast<Layout*>(_rankUi->getChildByName(StringUtils::format("panel_clone_", index + 1)))->clone();
					//名次显示
					log("RANK=====Lottery=======%d", _VecLottery[i].Rank);
					if (_VecLottery[i].Rank < 4)
					{
						ImageView* image_rank = dynamic_cast<ImageView*>(Item->getChildByName("image_rank"));
						image_rank->loadTexture(StringUtils::format("platform/common/new/phb/image_rank_%d.png", _VecLottery[i].Rank));
						image_rank->setVisible(true);
					}
					else
					{
						TextAtlas* Text_Rank = dynamic_cast<TextAtlas*>(Item->getChildByName("atlas_num"));
						Text_Rank->setString(to_string(_VecLottery[i].Rank));
						Text_Rank->setVisible(true);
					}
					//头像
					ImageView* Image_Head = dynamic_cast<ImageView*>(Item->getChildByName("image_tempHead"));
					ImageView* Frame = dynamic_cast<ImageView*>(Item->getChildByName("image_headFrame"));
					auto head = GameUserHead::create(Image_Head);
					head->show(USER_HEAD_MASK);
					head->loadTextureWithUrl(_VecLottery[i].HeaUrl);
					head->setSwallowTouches(false);
					Frame->setVisible(false);
					if (_VecLottery[i].VipLevel > 0)
					{
						Frame->setVisible(true);
						Frame->loadTexture(StringUtils::format("platform/common/VIP_%d.png", _VecLottery[i].VipLevel));
						Frame->setZOrder(10000);
					}
					auto nodeeffect = Frame->getChildByName("EffectNode");
					if (nodeeffect)
					{
						nodeeffect->removeFromParentAndCleanup(true);
					}
					if (_VecLottery[i].VipLevel > 2)
					{
						auto nodeEffect = CSLoader::createNode(StringUtils::format("platform/common/vip_%d_effect.csb", _VecLottery[i].VipLevel));
						nodeEffect->setAnchorPoint(Vec2(0.5, 0.5));
						nodeEffect->setScale(0.5);
						nodeEffect->setPosition(Vec2(Frame->getContentSize().width / 2, Frame->getContentSize().height / 2));
						auto ation = CSLoader::createTimeline(StringUtils::format("platform/common/vip_%d_effect.csb", _VecLottery[i].VipLevel));
						nodeEffect->runAction(ation);
						ation->gotoFrameAndPlay(0, true);
						nodeEffect->setName("EffectNode");
						Frame->addChild(nodeEffect);
					}

					//名字
					auto Text_Name = dynamic_cast<Text*>(Item->getChildByName("text_name"));
					if (_VecLottery[i].VipLevel > 1)
					{
						Text_Name->setColor(Color3B(255, 0, 0));
					}
					else
					{
						Text_Name->setColor(Color3B(166, 89, 30));
					}

					//用户名字长度剪切处理
					/*string str = _VecLottery[i].NickName;
					if (str.length() > 10)
					{
						str.erase(11, str.length());
						str.append("...");
						if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
						{
							Text_Name->setString(GBKToUtf8(str));
						}
						else
						{
							Text_Name->setString(str);
						}
					}
					else
					{
						if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
						{
							Text_Name->setString(GBKToUtf8(str));
						}
						else
						{
							Text_Name->setString(str);
						}
					}*/
					Text_Name->setString(StringUtils::toString(_VecLottery[i].NickName));

					//成绩
					auto Text_Score = dynamic_cast<Text*>(Item->getChildByName("text_score"));
					Text_Score->setString(StringUtils::toString(_VecLottery[i].Score));
					//压入容器
					Item->setVisible(true);
					//_vecRankList[index]->pushBackCustomItem(Clone_layout);
				}
			}
		}
		else if (index == 0)				//房卡
		{
			for (int i = 0; i < _VecRoomCard.size(); i++)
			{
				auto Item = dynamic_cast<ImageView*>(_vecRankList[index]->getChildByName(StringUtils::format("image_bg_%d", i + 1)));
				if (Item)
				{
					log("RANK=====Room=======%d", _VecRoomCard[i].Rank);
					//auto Item = dynamic_cast<Layout*>(Node->getChildByName("panel_clone_1"));
					//dynamic_cast<Layout*>(_rankUi->getChildByName(StringUtils::format("panel_clone_", index + 1)))->clone();
					//名次显示
					if (_VecRoomCard[i].Rank < 4)
					{
						ImageView* image_rank = dynamic_cast<ImageView*>(Item->getChildByName("image_rank"));
						image_rank->loadTexture(StringUtils::format("platform/common/new/phb/image_rank_%d.png", _VecRoomCard[i].Rank));
						image_rank->setVisible(true);
					}
					else
					{
						TextAtlas* Text_Rank = dynamic_cast<TextAtlas*>(Item->getChildByName("atlas_num"));
						Text_Rank->setString(to_string(_VecRoomCard[i].Rank));
						Text_Rank->setVisible(true);
					}
					//头像
					ImageView* Image_Head = dynamic_cast<ImageView*>(Item->getChildByName("image_tempHead"));
					ImageView* Frame = dynamic_cast<ImageView*>(Item->getChildByName("image_headFrame"));
					auto head = GameUserHead::create(Image_Head);
					head->show(USER_HEAD_MASK);
					head->loadTextureWithUrl(_VecRoomCard[i].HeaUrl);
					head->setSwallowTouches(false);
					Frame->setVisible(false);
					if (_VecRoomCard[i].VipLevel > 0)
					{
						Frame->setVisible(true);
						Frame->loadTexture(StringUtils::format("platform/common/VIP_%d.png", _VecRoomCard[i].VipLevel));
						Frame->setZOrder(10000);
					}
					auto nodeeffect = Frame->getChildByName("EffectNode");
					if (nodeeffect)
					{
						nodeeffect->removeFromParentAndCleanup(true);
					}
					if (_VecRoomCard[i].VipLevel > 2)
					{
						auto nodeEffect = CSLoader::createNode(StringUtils::format("platform/common/vip_%d_effect.csb", _VecRoomCard[i].VipLevel));
						nodeEffect->setAnchorPoint(Vec2(0.5, 0.5));
						nodeEffect->setScale(0.5);
						nodeEffect->setPosition(Vec2(Frame->getContentSize().width / 2, Frame->getContentSize().height / 2));
						auto ation = CSLoader::createTimeline(StringUtils::format("platform/common/vip_%d_effect.csb", _VecRoomCard[i].VipLevel));
						nodeEffect->runAction(ation);
						ation->gotoFrameAndPlay(0, true);
						nodeEffect->setName("EffectNode");
						Frame->addChild(nodeEffect);
					}

					//名字
					auto Text_Name = dynamic_cast<Text*>(Item->getChildByName("text_name"));
					if (_VecRoomCard[i].VipLevel > 1)
					{
						Text_Name->setColor(Color3B(255, 0, 0));
					}
					else
					{
						Text_Name->setColor(Color3B(166, 89, 30));
					}
					
					//用户名字长度剪切处理
					/*string str = _VecRoomCard[i].NickName;
					if (str.length() > 10)
					{
						str.erase(11, str.length());
						str.append("...");
						if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
						{
							Text_Name->setString(GBKToUtf8(str));
						}
						else
						{
							Text_Name->setString(str);
						}
					}
					else
					{
						if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
						{
							Text_Name->setString(GBKToUtf8(str));
						}
						else
						{
							Text_Name->setString(str);
						}
					}*/
					Text_Name->setString(StringUtils::toString(_VecRoomCard[i].NickName));

					//成绩
					auto Text_Score = dynamic_cast<Text*>(Item->getChildByName("text_score"));
					Text_Score->setString(StringUtils::toString(_VecRoomCard[i].Score));
					//压入容器
					Item->setVisible(true);
					//_vecRankList[index]->pushBackCustomItem(Clone_layout);
				}
			}
		}
	}
}

//弹出实名认证
void GamePlatform::certification()
{
	auto pLayer = GameCertificationLayer::create();
	pLayer->setName("GameCertificationLayer");
	pLayer->onCertificationCallBack = ([=] {
		RequestRealName();
		updateUserInfo();
	});
	pLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_CERT_TAG);
	pLayer->updateLayer(Real_Data);
}

void GamePlatform::createWithdrawal()
{
	Size popwinSize = Director::getInstance()->getWinSize();

	float scalex = 1280 / popwinSize.width;
	float scaley = 720 / popwinSize.height;

	auto node = CSLoader::createNode("platform/lobbytask/WithdrawalLayer.csb");
	//node->setScale(1.2);
	//node->setPosition(Vec2(-107,-73));
	addChild(node);

	_LooseChange = HNPlatformTaskList::getInstance()->getLooseChange();
	_MinStandard = HNPlatformTaskList::getInstance()->getMinStandard();
	_MaxStandard = HNPlatformTaskList::getInstance()->getMaxStandard();

	Layout* panel_close = (Layout*)node->getChildByName("panel_close");
	Button* btn_close = (Button*)panel_close->getChildByName("btn_close");
	btn_close->addClickEventListener([=](Ref*){
		Text_Money = nullptr;
		node->removeFromParent();
	});

	int(_MaxStandard);

	Text_Money = (Text*)panel_close->getChildByName("Text_Money");
	Text* Text_tip = (Text*)panel_close->getChildByName("Text_tip");

	Text* Text_NoRecord = (Text*)panel_close->getChildByName("Text_NoRecord");
	_panel_Take_Record = (ListView*)panel_close->getChildByName("list_take_records");
	Button* btn_sure = (Button*)panel_close->getChildByName("btn_tixian");
	btn_sure->addClickEventListener([=](Ref*){
		if (_LooseChange < _MinStandard)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("提现金额不足\n多做任务领取红包吧！"));
			return;
		}
		//发送提现
		HNPlatformTaskList::getInstance()->SendWithdrawalRequest(_LooseChange);
		//设置更新
		HNPlatformTaskList::getInstance()->onRequestCallBack = [=](){
			_LooseChange = HNPlatformTaskList::getInstance()->getLooseChange();
			if (Text_Money != nullptr)
			{
				Text_Money->setString(StringUtils::format(GBKToUtf8("%.2f元"), _LooseChange));
			}
		};
	});

	HNPlatformTaskList::getInstance()->RequestLooseChange();
	HNPlatformTaskList::getInstance()->onRequestCallBack = [=](){
		_LooseChange = HNPlatformTaskList::getInstance()->getLooseChange();
		if (Text_Money != nullptr)
		{
			Text_Money->setString(StringUtils::format(GBKToUtf8("%.2f元"), _LooseChange));
		}
	};

	HNPlatformTaskList::getInstance()->RequestPocketRecord();
	HNPlatformTaskList::getInstance()->onRequestRecordCallBack = [=](){
		if (Text_Money != nullptr)
		{
			RecordList = HNPlatformTaskList::getInstance()->getRecordList();
			//更新列表（隐藏暂无数据）
			if (RecordList.size() != 0)
			{
				Text_NoRecord->setVisible(false);
				UpDateEnvelopeRecord();
			}
		}
	};

	Text_Money->setString(StringUtils::format(GBKToUtf8("%.2f元"), _LooseChange));
	Text_tip->setString(to_string(int(_MinStandard)));
}

void GamePlatform::UpDateEnvelopeRecord()
{
	if (Text_Money != nullptr)
	{
		if (_panel_Take_Record->getCurSelectedIndex() != -1)
		{
			_panel_Take_Record->removeAllItems();
		}

		for (int i = 0; i < RecordList.size(); i++)
		{
			auto node = CSLoader::createNode("platform/lobbytask/WithdrawalNode.csb");
			auto paneTask = dynamic_cast<Layout*>(node->getChildByName("panel_Task"));
			//红包获取内容
			Text* text_Task = dynamic_cast<Text*>(paneTask->getChildByName("text_Task"));
			text_Task->setString(RecordList[i].TitleStr);

			Text* text_Take = dynamic_cast<Text*>(paneTask->getChildByName("text_TakeNum"));
			text_Take->setString(StringUtils::format(GBKToUtf8("+%.2f元"), (float)RecordList[i].Money / 100.0f));

			paneTask->removeFromParentAndCleanup(true);
			_panel_Take_Record->pushBackCustomItem(paneTask);
		}
	}
}

void GamePlatform::OpenStoreLayer(int Index)
{
	//切换商场购买房卡
	GameStoreLayer* storeLayer = dynamic_cast<GameStoreLayer*>(this->getChildByTag(CHILD_LAYER_STORE_TAG));
	if (nullptr != storeLayer)
	{
		storeLayer->ClickChangeStore(Index);
	}
	else
	{
		storeLayer = GameStoreLayer::createGameStore(this);
		storeLayer->setName("storeLayer");
		storeLayer->setChangeDelegate(this);
		storeLayer->onCloseCallBack = [this]() {

			updateUserInfo();
		};
		storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_STORE_TAG);
		storeLayer->ClickChangeStore(Index);
	}
}

void GamePlatform::UpdateTaskRed()
{
	if (HNPlatformConfig()->getSceneState() != PlatformConfig::SCENE_STATE::INPLATFORM)
		return; //不在大厅的时候不更新,否则容易导致崩溃
	if (_sp_NewbieRed != nullptr)
	{
		bool isShow = HNPlatformTaskList::getInstance()->GetNewbieAllState();
		_sp_NewbieRed->setVisible(isShow);
	}

	if (_sp_TaskListRed != nullptr)
	{
		bool isShow = HNPlatformTaskList::getInstance()->GetTaskListAllState();
		_sp_TaskListRed->setVisible(isShow);
	}
}