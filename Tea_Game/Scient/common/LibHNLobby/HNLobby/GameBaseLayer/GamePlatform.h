/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __PLATFORMFRAME_SCENE_H__
#define __PLATFORMFRAME_SCENE_H__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "HNLogicExport.h"
#include <string>
#include <vector>
#include "network/HttpClient.h"
#include "../GamePersionalCenter/GameUserHead.h"
#include "GameLading.h"

//任务数据
#include "HNPlatform/HNPlatformTaskList.h"

//俱乐部成员头像数据
#include "HNPlatform/HNPlatformClubData.h"
using namespace ui;
using namespace cocostudio;

enum LayerType
{
	ROOMLIST = 0,
	DESKLIST,
	GAMELIST,
	PLATFORM
};

enum LayerNode
{
	MATCH = 0,
};

struct RankData
{
	int Rank = 0;   //排名
	int UserID = 0;  //玩家id
	int64_t Score = 0;   //玩家分数
	std::string NickName = "";   //玩家姓名
	std::string HeaUrl = "";     //玩家头像地址
	int VipLevel = 0;          //VIP等级
};

struct realData
{
	std::string PhoneNum = "";			//手机号
	std::string Name = "";		//姓名
	std::string identity = "";		//身份证
};

class GameUserDataLayer;
class GameNotice;

class GamePlatform 
	: public HNLayer
	, public MoneyChangeNotify
	, public HN::IHNPlatformLogicBase
	, public HNHttpDelegate
{
public:
	HNPlatformLogicBase* _platformlogic = nullptr;
	Layout*	_lobbyUi			= nullptr;	//大厅ui层
	Layout*	_topUi				= nullptr;	//顶部ui层
	Layout*	_bottomUi			= nullptr;	//底部ui层
	Layout*	_gamesUi			= nullptr;	//游戏列表层
	ImageView*   _rankUi			= nullptr;  //大厅排行榜
	ImageView*   _rankbg			= nullptr;	//排行榜背景
	Sprite*	_rankwicker = nullptr;	//排行榜柳条
	Sprite*	_rankYear = nullptr;	//新年鞭炮
	Sprite*	_rankYear1 = nullptr;	//新年鞭炮1
	Layout*	goldgames = nullptr; //金币场
	Text*	_label_UserName		= nullptr;	//昵称
	Text*	_label_id			= nullptr;	//ID
	Text*	_label_Jewels		= nullptr;	//钻石
	Text*	_label_golds		= nullptr;	//金币
	Text*	_label_lotterys		= nullptr;	//奖券
	Layer*	_CreateRoomLayer = nullptr; //游戏大厅层
	GameUserHead* _userHead		= nullptr;	//头像
	Button*	_button_Return		= nullptr;	//返回按钮

	Button*	_button_create		= nullptr;	//创建房间
	Button*	_button_join		= nullptr;	//加入房间
	Button* _button_Match		= nullptr;	//比赛场按钮
	Button* _button_Gold		= nullptr;	//金币场按钮
	Button* _button_create_majio = nullptr;		//麻将房间按钮
	Button* _button_create_poke = nullptr;		//扑克房间按钮

	Vector<ImageView*>ActionImage;			//动画图片
	Vector<Button*>ActionButton;			//动画按钮
	GameLading *m_loading = nullptr;
	std::map<std::string, cocos2d::__String> _Data;

	// 需要播放动画的节点
	Sprite* _spriteGoldLogo     = nullptr; // 金币
	Sprite* _spriteDiamondLogo  = nullptr; // 钻石
	Button* _buttonStore        = nullptr; // 商城
	Button* _panelClub			= nullptr; // 俱乐部
	Layout* _panelBg            = nullptr; // 背景

	//红包记录
	vector<DATE_RECORD>		RecordList;		//红包记录
private:
	bool				_isTouch;
	INT					_Time;

	EventListenerCustom*	_listener		= nullptr;	// 断线重连成功监听
	EventListenerCustom*	_foreground		= nullptr;	// 切后台检测监听

public:
	static void createPlatform();
	static void createNewPlatform();
	static void returnPlatform(LayerType type);
	static void returnPlatformNode(LayerNode type);
	static void SceneEffect(Scene* scene);
	CREATE_FUNC(GamePlatform);

public:
	GamePlatform();
	virtual ~GamePlatform();

    virtual bool init() override;
	virtual void onExit() override;
	virtual void onEnterTransitionDidFinish() override;
	//返回键监听回调函数
	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode,Event * pEvent) override;
private:
	//查询各种奖励信息
	void checkGiftMessages();
	//卸载奖励监听
	void unSocketCallBack();
	//查询信息回调
	bool checkGiftMessagesEventSelector(HNSocketMessage* socketMessage);
	//领奖提醒倒计时
	void updataGetRewardTimeMessage(float dt);

	void AddNetWorkEventListener();
	void RemoveNetWorkEventListener();
	
private:
	//监听公告消息变动
	bool ClubNoticeChange(HNSocketMessage* SocketMessage);

private:
	void createLayerChoose(LayerType type);
	//进入大厅打开比赛场列表
	void createLayerByType(LayerNode type);
	// 获取游戏列表
	void requestGameList();
	//顶部按钮回调
	void menuLobbyTopCallBack(Ref* pSender);
	//中部按钮回调
	void menuLobbyMidCallBack(Ref* pSender);
	//底部按钮回调
	void menuLobbyBottomCallBack(Ref* pSender);
	//底部弹出框按钮回调
	void menuMoreCallBack(Ref* pSender);
	//排行榜按钮回调
	void menuRankCallBack(Ref* pSender);
	// 打开友盟分享组件
	void openUMengShare();

public:
	//创建房间列表层
	void createRoomListLayer();
	//创建游戏列表层
	void createGameListLayer();
	//创建桌子列表层
	void createDeskListLayer(ComRoomInfo* roomInfo);
	// 进入创建VIP房间层
	void enterCreateRoomLayer(int index);		//选择的房间类型

private:
	//创建俱乐部结果
	bool CreateClubResult(HNSocketMessage* socketMessage);

	//加入俱乐部结果
	bool JoinClubResult(HNSocketMessage* socketMessage);

	//获取茶楼列表结果
	bool getTeaHouseResult(HNSocketMessage* socketMessage);

	void createClubListItem(MSG_GP_O_Club_List data);

	void requestClubList();

public:
	// 监听玩家购买金币消息
	virtual void walletChanged() override;

	// 获取公告请求
	void requestNotice(int num);
	void dealNoticeResp(const std::string& data);

	// 更新用户信息
	void updateUserInfo();

	// 更新用户设置
	void updataSet();

	// 更新金币
	void updateMoney();

	// 更新钻石
	void updateDiamond();

	// 更新奖券
	void updateLotteries();

	//我的推广界面
	void MySpread();
public:
	////////////////////////////////////////////////////////////////////////////////////////////
	//申请房间列表回调
	virtual void onPlatformRoomListCallback(bool success, const std::string& message) override;
	//更新房间人数
	virtual void onPlatformRoomUserCountCallback(UINT roomID, UINT userCount) ;

	////////////////////////////////////////////////////////////////////////////////////////////
	//申请游戏列表回调
	virtual void onPlatformGameListCallback(bool success, const std::string& message) override;
	//更新游戏人数
	virtual void onPlatformGameUserCountCallback(UINT Id, UINT count) override;

	//断线通知
	virtual void onPlatformDisConnectCallback(const std::string& message) override;
	//新闻广播消息
	virtual void onPlatformNewsCallback(const std::string& message) override;

	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	//请求排行榜数据
	void RequestRankData();

	//查询实名认证数据
	void RequestRealName();

	//处理排行榜数据
	void dealRankData(const std::string& data, int index);
	//排行榜页面
	//void RankPage();
	//
	void UpdataRank(int index);

	void certification();

private:
	// 显示退出选择页面
	void showExitLayer();

	// 切换账号
	void switchAccount();

	void showTopAction(bool isShow, std::function<void()> func);

	// 播放跳动动画
	void playOrStopJump(cocos2d::Node* pNode, bool bJump);

	// 获取功能开关配置信息
	void getModuleConfig();

	// 添加动画
	void addSpine();
    
    void updateIAPPay(Ref* pSender);
    
    void supportAlert();
    
    void onIAPGetMoneyCallback(const std::string& data);

	//提现
	void createWithdrawal();

	//回调打开商场
	void OpenStoreLayer(int Index);

	//更新红包记录
	void UpDateEnvelopeRecord();

	void UpdateTaskRed();

private:
	std::vector<RankData>       _VecGold;		//财富榜
	std::vector<RankData>       _VecLottery;		//礼券榜
	std::vector<RankData>       _VecRoomCard;	    //房卡榜

	//vector结构体
	vector<MSG_GP_O_Club_List> _MyCreate;
	vector<MSG_GP_O_Club_List> _MyJoin;

	bool _isRequestClubList;

	ListView*	_ClubListView;
	Text*		 _Text_notice;

	Button*                 _btnGold;
	Button*                 _btnLottery;
	Layout*                 _PlaneMask;
	ImageView*			  _image_more;			//更多弹出窗
	Button*                 _RankingCloseBtn;		//关闭排行榜
	Button*				  _RankingOpenBtn;		//打开排行榜
	std::vector<Button*>		  _vecRankBtnList;
	std::vector<Sprite*>		  _vecSp_Rank;
	std::vector<ListView*>		  _vecRankList;

	//实名认证
	realData					Real_Data;
	Sprite*				  _real_tip;			//实名奖励提示

	float		_LooseChange;
	float		_MinStandard;
	float		_MaxStandard;
	Text*		Text_Money;
	ListView*		_panel_Take_Record = nullptr;

	//新手任务红点
	Sprite* _sp_NewbieRed = nullptr;
	Sprite* _sp_TaskListRed = nullptr;

	//std::string _selection_club_name;
};

#endif // __PLATFORMFRAME_SCENE_H__
