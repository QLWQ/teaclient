/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameLists.h"
#include "GameDesk.h"
#include "GameRoom.h"
#include <string>
#include "../GameUpdate/GameResUpdate.h"

//////////////////////////////////////////////////////////////////////////
static const float enter_room_outtime_timer_time = 30.0f;
//////////////////////////////////////////////////////////////////////////
static const char* request_room_list_text = "获取房间列表......";
static const char* game_dev_text		  = "游戏正在开发中。";
//////////////////////////////////////////////////////////////////////////

static const char* GAME_ITEM_UI	= "platform/lobbyUi/gameItem_Node.csb";
static const char* GAME_PAGESPROMPT_PATH = "platform/common/yuandian1.png";


GameLists::GameLists(Layout* layout)
	//: _pageViewGames (nullptr)
	: _layout(layout)
	, _currentSelectedGame (nullptr)
	, _currentPageIdx (0)
	, _isTouch (true)
{

}

GameLists::~GameLists()
{
	
}

void GameLists::close()
{
	if (onRunActionCallBack) onRunActionCallBack();

	//_pageViewGames->addEventListener(PageView::ccPageViewCallback(nullptr));
	if (onEnterGameCallBack)
	{
		onEnterGameCallBack();

	}
	
	//this->removeFromParentAndCleanup(true);
	//_layout->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
	//	
	//	//
	//}), nullptr));
}

GameLists* GameLists::create(Layout* layout)
{
	auto pRet = new (std::nothrow) GameLists(layout);
	if (pRet && pRet->init())
	{
		pRet->autorelease();
	}
	else
	{
		CC_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameLists::init()
{
    if (!HNLayer::init()) return false;

	createGameList();

    return true;
}

void GameLists::onEnter()
{
	HNLayer::onEnter();
}

void GameLists::onExit()
{
	HNLayer::onExit();
}

void GameLists::createGameList()
{
	/*_layout->setOpacity(255);
	_layout->setVisible(true);

	_pageViewGames		= dynamic_cast<PageView*>(_layout->getChildByName("PageView_gamelist"));
	_pageViewGames->setCustomScrollThreshold(15);
	_pageViewGames->scrollToPage(0);
	_pageViewGames->removeAllPages();
	_pageViewGames->setIndicatorEnabled(true);
	_pageViewGames->setIndicatorIndexNodesTexture(GAME_PAGESPROMPT_PATH);
	_pageViewGames->setIndicatorIndexNodesScale(0.85f);
	_pageViewGames->setIndicatorSelectedIndexColor(Color3B::WHITE);*/

	
	std::vector<ComNameInfo*> gameNames;

	// 已过滤掉纯vip房间（比赛场不用过滤，接收到房间列表消息时已经过滤）
	gameNames = GameCreator()->getNormalGames();

	int currentIndex = 0; 
	int pageCount = (gameNames.size() / 3);
	int remainder = (gameNames.size() % 3);

	std::vector<ComNameInfo*> pages;

	if (pageCount > 0)
	{
		for (int currentPage  = 0; currentPage < pageCount; currentPage++)
		{
			pages.clear(); 
			for (int room = 0; room < gameNames.size(); room++, currentIndex++)
			{
				ComNameInfo* roomInfo = gameNames[currentIndex];
				//if (roomInfo->uCanBuy == 1)continue;
				pages.push_back(roomInfo);
				_gameInfo.push_back(roomInfo);
			}
			createGamePage(pages);
		}
	}

	/*if (remainder > 0)
	{
		pages.clear();
		for (int room = 0; room < remainder; room ++, currentIndex ++)
		{
			ComNameInfo* roomInfo = gameNames[currentIndex];
			//if (roomInfo->uCanBuy == 1)continue;
			pages.push_back(roomInfo);
			_gameInfo.push_back(roomInfo);
		}
		createGamePage(pages);
	}*/

	checkUpdateGame();
	_currentPageIdx = Configuration::getInstance()->getValue("gamePageIdx", Value(0)).asInt();
	//_pageViewGames->setCurrentPageIndex(_currentPageIdx);
}

//添加游戏
void GameLists::createGamePage(std::vector<ComNameInfo*> games)
{
	// 创建游戏列表子页面
	/*auto gameLayout = Layout::create();
	gameLayout->setName("page");
	gameLayout->setContentSize(_pageViewGames->getContentSize());*/

	int idx = 0;
	for (auto game : games)
	{
		
		idx++;
		float posX = 0.0f;

		if (games.size() == 1)
		{
			posX = 0.5f;
		}
		else if (games.size() == 2)
		{
			posX = -0.25f + idx * 0.5f;
		}
		else
		{
			posX = idx * 0.25f + (idx - 2) * 0.07f;
		}
		ccBezierConfig bezierpos = {
			Vec2(((((idx - 1) % 3) * 260) + 135), ((idx - 1) / 3 > 0 ? 125 : 397)),
			Vec2(400, 250),
			Vec2(((((idx - 1) % 3) * 260) + 135), ((idx - 1) / 3 > 0 ? 125 : 397)) };
		auto bezier = BezierTo::create(0.7, bezierpos);
		auto gameItem = createGameItem(game);
		auto gameButton = (Button*)gameItem->getChildByName("Button_game");
		gameButton->removeFromParentAndCleanup(false);
		gameButton->setPosition(Vec2(930, 220));
		gameButton->setScale(0.1);
		auto move = MoveTo::create(1.75f, Vec2(((((idx - 1) % 3) * 260) + 135 - 5), ((idx - 1) / 3 > 0 ? 125 : 397 - 5)));
		auto movers = MoveTo::create(1.75f, Vec2(((((idx - 1) % 3) * 260) + 135), ((idx - 1) / 3 > 0 ? 125 : 397)));
		auto sequence = Sequence::create(move, movers, nullptr);
		gameButton->runAction(Sequence::create(Spawn::create(ScaleTo::create(0.7, 1), bezier, nullptr), Repeat::create(sequence, 999), nullptr));
		auto image_icon = dynamic_cast<ImageView*>(gameButton->getChildByName("Image_icon"));
		image_icon->setZOrder(-1);
		image_icon->setRotation(45 * idx);
		image_icon->setPosition(Vec2(gameButton->getContentSize().width / 2 - 4, gameButton->getContentSize().height/2 - 2));
		image_icon->ignoreContentAdaptWithSize(true);
		image_icon->loadTexture("platform/lobbyUi/res/games/GameBg.png");
		auto repeat = RepeatForever::create(RotateBy::create(0.1f, 10));
		image_icon->runAction(repeat);
		_layout->addChild(gameButton, 3, idx);
	}

	////添加子页面进入列表中
	//_pageViewGames->addPage(gameLayout);

	////拖动监听
	//_pageViewGames->addEventListener(PageView::ccPageViewCallback(CC_CALLBACK_2(GameLists::pageViewMoveCallBack, this)));
}

Node* GameLists::createGameItem(ComNameInfo* game)
{
	auto gameItemNode = CSLoader::createNode(GAME_ITEM_UI);

	auto gameButton = (Button*)gameItemNode->getChildByName("Button_game");
	gameButton->setUserData(game);
	gameButton->addTouchEventListener(CC_CALLBACK_2(GameLists::enterRoomEventCallBack, this));

	char str[128];
	// 游戏
	//auto Image_Ico = (ImageView*)Helper::seekWidgetByName(gameButton, "Image_icon");
	sprintf(str, "platform/lobbyUi/res/games/%d.png", game->uNameID);
	if (FileUtils::getInstance()->isFileExist(str))
	{
		gameButton->loadTexturePressed(str);
		gameButton->loadTextureNormal(str);
		gameButton->loadTextureDisabled(str);
	}
	//更新图片
	auto ImageUpdate = (ImageView*)Helper::seekWidgetByName(gameButton, "Image_update");
	ImageUpdate->setVisible(false);
	_ImageUpdate.push_back(ImageUpdate);

	return gameItemNode;
}

/*void GameLists::pageViewMoveCallBack(Ref* pSender, PageView::EventType type)
{
	if (!_pageViewGames) return;
	
	if (_currentPageIdx == _pageViewGames->getCurPageIndex()) return;
	_currentPageIdx = _pageViewGames->getCurPageIndex();

	Configuration::getInstance()->setValue("gamePageIdx", Value(_currentPageIdx));
}*/

//子页面按钮回调
void GameLists::enterRoomEventCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (!_isTouch) return;

	_currentSelectedGame = dynamic_cast<Button*>(pSender);


	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
	{

	auto action = Sequence::create(DelayTime::create(2.0f), CallFunc::create([=]() {_isDel = true; }), nullptr);
		action->setTag(255);
		_currentSelectedGame->runAction(Sequence::create(ScaleTo::create(0.2f, 1.1f), CallFunc::create([=](){
			
		}),nullptr));
		_isDel = false;
		runAction(action);
	}
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
	{
		
		_isTouch = false;

		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
		_currentSelectedGame->runAction(ScaleTo::create(0.2f, 1.0f));

		ComNameInfo* game = static_cast<ComNameInfo*>(_currentSelectedGame->getUserData());

		// 参数校验
		CCAssert(nullptr != game, "game is nullptr!");
		if (nullptr == game)
		{
			_isTouch = true;
			return;
		}

		bool isVisible = (ImageView*)_currentSelectedGame->getChildByName("Image_update")->isVisible();
		bool isLoaded = UpdateInfoModule()->findGame(game->uNameID)->am->getLocalManifest()->isVersionLoaded();
		// 长按删除游戏
		this->stopActionByTag(255);
		// 当满足长按，不需要更新，并且有Manifest文件时，提示删除
		if (_isDel && !isVisible && isLoaded)
		{
			_isDel = false;

			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(GBKToUtf8("是否删除资源？"));
			prompt->setCallBack([=]() {
				auto update = GameResUpdate::create();
				if (update->deleteGameRes(game->uNameID))
				{
					checkUpdateGame();
				}
			});

			_isTouch = true;
			return;
		}

		GameCreator()->setCurrentGame(game->uNameID);
		GameCreator()->setGameKindId(game->uNameID, game->uKindID);

		UINT uCanBuy = game->uCanBuy;
		// 检测是否需要更新
		auto update = GameResUpdate::create();
		update->retain();
		update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {

			if (success)
			{
				close();
				_isTouch = true;
				_isDel = false;
				
			}
			

			update->release();
		};
		update->checkUpdate(game->uNameID, true);
		
	}
	break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		_currentSelectedGame->runAction(ScaleTo::create(0.2f, 1.0f));
		this->stopActionByTag(255);
		break;
	default:
		break;
	}
}

//更新游戏人数
/*void GameLists::updateGamePeopleCount(UINT Id, UINT count)
{
	Vector<Layout*> pages = _pageViewGames->getPages();

	char buffer[64];
	for (auto iter = pages.begin(); iter != pages.end(); ++iter)
	{
		Vector<Node*> childs = (*iter)->getChildren();
		for (auto iter1 = childs.begin(); iter1 != childs.end(); ++iter1)
		{
			auto gameButton = dynamic_cast<Button*>(*iter1);
			auto TextBMFont_PeopleCount = (TextBMFont*)Helper::seekWidgetByName(gameButton, "BitmapFontLabel_PeopleCount");

			if (nullptr != gameButton)
			{
				ComNameInfo* names = static_cast<ComNameInfo*>(gameButton->getUserData());

				if (names->uNameID == Id)
				{
					if (nullptr != TextBMFont_PeopleCount)
					{
						sprintf(buffer, GBKToUtf8("%u人"), count);
						TextBMFont_PeopleCount->setString(buffer);
					}
				}
			}
		}
	}
}*/

//检测更新（只检测不更新）
void GameLists::checkUpdateGame()
{
	int index = 0;
	for (auto game : _gameInfo)
	{
		auto update = GameResUpdate::create();
		update->retain();
		update->onCheckCallBack = [=](bool needUpdate) {
			_ImageUpdate.at(index)->setVisible(needUpdate);
			update->release();
		};
		update->checkUpdate(game->uNameID, false);
		index++;
	}
}