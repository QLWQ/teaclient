/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef GameDesk_h__
#define GameDesk_h__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "HNLogicExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <vector>

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

//桌子信息
typedef struct tagDeskInfo
{
	INT		deskID;				// 桌号
	INT		multiple;			// 倍率
	INT		goldMin;			// 最小携带金钱
	INT		goldMax;			// 最大携带金钱
	INT		peopleValue;		// 桌子当前人数
	INT		peopleMax;			// 桌子最大人数
	bool    isLocked;			// 是否锁桌
	bool	isPlaying;			// 是否游戏进行中
}DeskInfo;

class GameDesk : public HNLayer, public IHNRoomDeskLogic
{
public:
	typedef std::function<void ()> CloseCallBack;
	CloseCallBack				onCloseCallBack = nullptr;

private:
	ComRoomInfo*				_roomInfo;
	PageView*					_pageViewDesks;			// 桌子列表
	Button*						_currentSelectedDesk;	// 列表按钮
	INT							_deskNO;				// 桌子号
	
	// 绘制页临时数据
	std::vector<DeskInfo*>		_deskinfos;

	// 桌子列表所有数据
	std::vector<DeskInfo*>      _allDeskInfo;

	// 所有桌子
	std::vector<Button*>		_allDeskUI;
	
	bool						_pageEven;				//是否整数页
	INT							_pageLen;				//计算出来的总页数
	bool						_canCreate;				//是否还需要创建页面
	CC_SYNTHESIZE_PASS_BY_REF(bool, _isTouch, CanTouch);

public:
	static GameDesk* createDesk(ComRoomInfo* roomInfo);

public:
	GameDesk();
	virtual ~GameDesk();

	bool initData(ComRoomInfo* roomInfo);
	virtual void onExit() override;


public:
	// 创建牌桌列表pageView
	void createDeskList(ComRoomInfo* roomInfo);

	// 分页创建牌桌列表page
	void createDeskPage();

	// 创建牌桌页面数据
	void createDeskPageInfo(ComRoomInfo* roomInfo);

	// 创建牌桌列表page
	void createDeskListPage(std::vector<DeskInfo*> deskinfos);

	// 创建牌桌列表button
	Node* addDesk(DeskInfo* deskInfo);

public:
	// 桌子是否锁定(是否设置密码)更新
	void updateLockDesk();

	// 进入游戏桌点击回调
	void enterTableEventCallBack(Ref* pSender, Widget::TouchEventType type);

	// 输入密码回调
	void onEnterPasswordCallback(const std::string& password);

public:
	// 坐下消息回调
	virtual void onDeskSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo) override;

	// 玩家人数变化通知
	virtual void onRoomDeskUserCountChanged(BYTE deskNo) override;

	// 更新是否虚拟封桌状态
	virtual void onUpdateVirtualDesk(BYTE deskNo, bool isVirtual) override;

	// 更新是否游戏中状态
	virtual void onUpdateDeskState(BYTE deskNo, bool isPlaying) override;

	// 更新游戏人数
	void updateDeskPeopleCount(BYTE deskNo, bool isLock);

	// 刷新UI(游戏处于此界面时重连刷新UI)
	void refresh();

private:
	void closeFunc() override;

protected:
	HNRoomDeskLogic* _deskLogic;
};

#endif // GameDesk_h__