/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameInitial.h"
#include "../GameMenu/GameMenu.h"

Scene* GameInitial::createScene()
{
    auto scene = Scene::create();
    auto layer = GameInitial::create();
    scene->addChild(layer);
    return scene;
}

void GameInitial::onEnterTransitionDidFinish()
{
	HNLayer::onEnterTransitionDidFinish();

	if(!_isCustomer)
	{
		showVideo();
	}
}

bool GameInitial::init()
{
    if ( !HNLayer::init())
    {
        return false;
    }

	Configuration::getInstance()->setValue("bControl", Value(false));
	Configuration::getInstance()->setValue("bBackground", Value(false));

	do 
	{
		std::string filename = "config.json";
		if (FileUtils::getInstance()->isFileExist(filename))
		{
			std::string json = FileUtils::getInstance()->getStringFromFile(filename);
			rapidjson::Document doc;
			doc.Parse<rapidjson::kParseDefaultFlags>(json.c_str());
			if (doc.HasParseError() || !doc.IsObject())
			{
				break;
			}


			std::string select_server = "server";
			if (doc.HasMember("select_server"))
			{
				select_server = doc["select_server"].GetString();
			}
			if (doc.HasMember(select_server.c_str()))
			{
				if (doc[select_server.c_str()].HasMember("isCustomer"))
				{
					_isCustomer = doc[select_server.c_str()]["isCustomer"].GetBool();
				}
			}		
		}
	} while (0);

	PlatformConfig::getInstance()->setSplashLogo("platform/logo.png");

	std::string logo = PlatformConfig::getInstance()->getSplashLogo();

	if (_isCustomer)
	{
		if (FileUtils::getInstance()->isFileExist(logo))
		{
			setBackGroundImage(logo);
			scheduleOnce(schedule_selector(GameInitial::waitting), 1.5f);
		}
		else
		{
			scheduleOnce(schedule_selector(GameInitial::waitting), 0.2f);
		}
	}
	else
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		if (FileUtils::getInstance()->isFileExist(logo))setBackGroundImage(logo);
#endif
	}

	requestShare();

    return true;
}

void GameInitial::showVideo()
{
	HNAudioEngine::getInstance()->playEffect("platform/video/audio.mp3");

	Size size = Director::getInstance()->getVisibleSize();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	
// 	auto videoPlayer = cocos2d::experimental::ui::VideoPlayer::create();
// 	videoPlayer->setPosition(size / 2);
// 	videoPlayer->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
// 	videoPlayer->setContentSize(size);
// 	this->addChild(videoPlayer, 1);
// 	videoPlayer->setName("video");
// 
// 	if (videoPlayer)
// 	{
// 		videoPlayer->setFileName("platform/video/flash.mp4");
// 		videoPlayer->play();
// 		videoPlayer->setFullScreenEnabled(true);
// 	}
// 	videoPlayer->addEventListener(CC_CALLBACK_2(GameInitial::videoEventCallback, this));
	auto sequence = Sequence::create(DelayTime::create(0.1f),
		CallFuncN::create(CC_CALLBACK_0(GameInitial::onPlayVideoOverCallback, this)),
		nullptr);
	this->runAction(sequence);

#else
	auto sequence = Sequence::create(DelayTime::create(1.0f),
		CallFuncN::create(CC_CALLBACK_0(GameInitial::onPlayVideoOverCallback, this)),
		nullptr);
	this->runAction(sequence);

#endif
}

/**
* 视频播放完成的回调函数
*/
void GameInitial::onPlayVideoOverCallback()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	auto video = this->getChildByName("video");
	if(video != nullptr)
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		bool isOld = cocos2d::Configuration::getInstance()->getValue("ios-old-system", cocos2d::Value(false)).asBool();
		if(isOld)
		{
			video->removeFromParent();
		}
#endif

	}
#endif

    Director::getInstance()->resume();
	GameMenu::createMenu();
}

/**
*  视频播放的状态
*  注意这里的代码，此处代码只有在android平台和Ios平台有效
*/
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
void GameInitial::videoEventCallback(Ref* sender, cocos2d::experimental::ui::VideoPlayer::EventType eventType)
{
	switch (eventType) 
	{
	case cocos2d::experimental::ui::VideoPlayer::EventType::PLAYING:
		break;
	case cocos2d::experimental::ui::VideoPlayer::EventType::PAUSED:
		break;
	case cocos2d::experimental::ui::VideoPlayer::EventType::STOPPED:
		break;
	case cocos2d::experimental::ui::VideoPlayer::EventType::COMPLETED:
	{
		onPlayVideoOverCallback();
	}
		break;
	default:
		break;
	}
}
#endif

void GameInitial::waitting(float dt)
{
	GameMenu::createMenu();
}

void GameInitial::requestShare()
{
	std::string param = StringUtils::format("action=GetShareInfo&key=");
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("share", network::HttpRequest::Type::POST, PlatformConfig::getInstance()->getEditUrl(), param);
}

void GameInitial::dealShareResp(const std::string& data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
	if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember("content") || !doc["content"].IsArray())
	{
		return;
	}

	for (auto i = 0; i < doc["content"].Size(); i++)
	{
		if (!doc["content"][i].IsObject() || !doc["content"][i].HasMember("ParaName") || !doc["content"][i].HasMember("ParaValue"))
		{
			continue;
		}

		std::string name = doc["content"][i]["ParaName"].GetString();
		std::string value = doc["content"][i]["ParaValue"].GetString();

		if (name.compare("ShareUrl") == 0)
		{
			// 保存获取到的分享地址
			PlatformConfig::getInstance()->setShareUrl(value);
		}
		else if (name.compare("ShareContent") == 0)
		{
			// 保存获取到的分享内容
			PlatformConfig::getInstance()->setShareContent(value);
		}
		else if (name.compare("ShareBgPic") == 0)
		{
			// 保存获取到的分享图片
			PlatformConfig::getInstance()->setShareImage(value);
		}
		else
		{

		}
	}
}

void GameInitial::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;

	if (requestName.compare("share") == 0)
	{
		dealShareResp(responseData);
	}
}

GameInitial::GameInitial()
	: _isCustomer(false)
{
	
}

GameInitial::~GameInitial()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}