/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMEINITIAL_SCENE_H__
#define __GAMEINITIAL_SCENE_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/UIVideoPlayer.h"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"

class GameInitial : public HNLayer, public HNHttpDelegate
{
public:
	// 创建场景
    static cocos2d::Scene* createScene();

	// 初始化
    virtual bool init() override;
	virtual void onEnterTransitionDidFinish() override;

	// 播放视频
	void showVideo();

	// 创建对象
	CREATE_FUNC(GameInitial);

	// 动画播放回调(win32)
	void onPlayVideoOverCallback();

	/**
	* 视频播放状态，只有在android和ios平台有效
	*/
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	void videoEventCallback(Ref* sender, cocos2d::experimental::ui::VideoPlayer::EventType eventType);
#endif	

protected:
	GameInitial();

	virtual ~GameInitial();

	void waitting(float dt); 

	// 获取分享信息
	void requestShare();
	void dealShareResp(const std::string& data);

	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

private:
	bool _isCustomer;
};

#endif // __GAMEINITIAL_SCENE_H__
