/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef GameLading_h__
#define GameLading_h__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <vector>

USING_NS_CC;

using namespace cocostudio;
using namespace ui;


class GameLading : public HNLayer
{
public:
	GameLading();
	virtual~GameLading();
	virtual bool init();
	CREATE_FUNC(GameLading);
	
public:
	void OnLadINGbg(int nameid);
	void onSetTime(float dt);

	void onbjlSetTime(float dt);
	LoadingBar *m_bar;
	ImageView*bg;

	LoadingBar *m_bar1;
	ImageView*bg1;


};


#endif