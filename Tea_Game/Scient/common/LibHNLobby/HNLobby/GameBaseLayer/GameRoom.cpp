/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/


#include "GameRoom.h"
#include "GameDesk.h"
#include "GamePlatform.h"
#include "GameLading.h"
#include "../GameMenu/GameMenu.h"
#include "../GameChildLayer/GamePasswordInput.h"
#include "../GameShop/GameStoreLayer.h"
#include "HNUIExport.h"
#include <string>
#include "../GameNotice/GameNotice.h"

//////////////////////////////////////////////////////////////////////////
static const float enter_game_desk_outtime_timer = 30.0f;
static const float update_game_room_people_timer = 2.0f;
//////////////////////////////////////////////////////////////////////////
static const char* connect_room_text = "连接房间......";
static const char* login_room_text = "登陆房间......";
static const char* request_room_info_text = "获取房间数据......";
static const char* allocation_table_please_wait_text = "正在配桌，请稍后......";
static const char* enterGame_please_wait_text = "正在进入游戏，请稍后......";
//////////////////////////////////////////////////////////////////////////
static const char* ROOMUI_CSB = "platform/lobbyUi/roomlist_Node.csb";
static const char* ROOM_ITEM_UI = "platform/lobbyUi/roomItem_Node.csb";
static const char* BJLE_HELP_CSB = "platform/lobbyUi/bjleHelp_Node.csb";
static const char* GAME_PAGESPROMPT_PATH = "platform/common/yuandian1.png";
//////////////////////////////////////////////////////////////////////////

GameRoom::GameRoom()
: _isTouch(true)
{
	_roomLogic = new HNRoomLogicBase(this);
}

GameRoom::~GameRoom()
{
	_roomLogic->stop();
	HN_SAFE_DELETE(_roomLogic);
}

void GameRoom::closeFunc()
{
	_roomLogic->stop();
	if (onCloseCallBack) onCloseCallBack();
}

void GameRoom::onEnterPasswordClickCallback(const std::string& password)
{
	ComRoomInfo* pData = static_cast<ComRoomInfo*>(_currentSelectedRoom->getUserData());
	_roomLogic->requestRoomPasword(pData->uRoomID, password);
}

bool GameRoom::init()
{
	if (!HNLayer::init())  return false;


	createRoomList();

	return true;
}

void GameRoom::onExit()
{
	HNLayer::onExit();
}

GameRoom* GameRoom::createGameRoom(MoneyChangeNotify* delegate)
{
	GameRoom *pRet = new GameRoom();
	if (pRet && pRet->init())
	{
		pRet->setChangeDelegate(delegate);
		pRet->autorelease();
		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return nullptr;
	}
}

void GameRoom::createRoomList()
{
	if (RoomInfoModule()->getRoomCount() == 0)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
		auto promptlayer = GamePromptLayer::create();
		promptlayer->showPrompt(GBKToUtf8("暂无房间！"));
		promptlayer->setCallBack([=](){
			if (_NoRoomCallBack)_NoRoomCallBack();
		}); 
		return;
	}
	Size winSize = Director::getInstance()->getWinSize();
	float scalex = 1280 / winSize.width;
	float scaley = 720 / winSize.height;

	auto node = CSLoader::createNode(ROOMUI_CSB);
	node->setPosition(winSize / 2);
	addChild(node);


	//_mPanelBjl = dynamic_cast<Layout*>(node->getChildByName("Panel_Bjl"));
	//_pageBjlViewRooms = dynamic_cast<Layout*>(_mPanelBjl->getChildByName("Panel_roonList"));
	_layout = dynamic_cast<Layout*>(node->getChildByName("Panel_rooms"));
	_layout->setScale(winSize.width / 1280, winSize.height / 720);
	_layout->setSwallowTouches(false);
	for (auto child : _layout->getChildren())
	{
		child->setScale(scalex, scaley);
	}

	auto img_top = dynamic_cast<ImageView*>(_layout->getChildByName("Image_top"));

	// 返回按钮
	auto btn_return = dynamic_cast<Button*>(img_top->getChildByName("Button_return"));
	//btn_return->setVisible(false);
	btn_return->addClickEventListener([=](Ref*){
		//this->setVisible(false);
		if (onCloseCallBack) onCloseCallBack();
		this->removeFromParentAndCleanup(true);
	});


	// 金币
	auto img_gold = dynamic_cast<ImageView*>(img_top->getChildByName("Image_gold"));
	_text_gold = dynamic_cast<TextAtlas*>(img_gold->getChildByName("AtlasLabel_value"));
	//double money = HNToWan(PlatformLogic()->loginResult.i64Money);// *0.01;
	//_text_gold->setString(StringUtils::format("%f", money));
	std::string str = HNToWan(PlatformLogic()->loginResult.i64Money);
	_text_gold->setString(str);
	float Ysize = _text_gold->getContentSize().width;
	if (Ysize > 189)
	{
		_text_gold->setScale(189 / Ysize);
	}


	auto btn_goldAdd = dynamic_cast<Button*>(img_gold->getChildByName("Button_add"));
	/*btn_goldAdd->addClickEventListener([=](Ref*){
		MSG_GP_S_SHOP_RESULT shopResult = HNPlatformLogic::getInstance()->getShopResult();

		auto storeLayer = GameStoreLayer::createGameStore(_delegate);
		storeLayer->setChangeDelegate(_delegate);

		storeLayer->setPayTypeBtn(shopResult.PayCount, shopResult.PayType);
		storeLayer->addAliPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
		storeLayer->addWeChatPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
		storeLayer->addQQPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
		storeLayer->addVIPdialiStore(shopResult.VipCount, shopResult.WXid, shopResult.strName);

		storeLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 200, 2000);
		storeLayer->setZOrder(100000);
	});*/

	// 奖券
	auto img_lottery = dynamic_cast<ImageView*>(img_top->getChildByName("Image_lottery"));
	auto text_lottery = dynamic_cast<TextAtlas*>(img_lottery->getChildByName("AtlasLabel_value"));
	text_lottery->setString(std::to_string(PlatformLogic()->loginResult.iLotteries));



	// 房间列表
	_pageViewRooms = dynamic_cast<PageView*>(_layout->getChildByName("PageView_rooms"));
	_pageViewRooms->setPosition(Vec2(_winSize.width + 640, 300));
	_pageViewRooms->setCustomScrollThreshold(15);
	_pageViewRooms->scrollToPage(0);
	_pageViewRooms->removeAllPages();
	_pageViewRooms->setIndicatorEnabled(false);
	_pageViewRooms->setIndicatorIndexNodesTexture(GAME_PAGESPROMPT_PATH);
	_pageViewRooms->setIndicatorIndexNodesScale(0.85f);
	_pageViewRooms->setIndicatorSelectedIndexColor(Color3B::WHITE);
	_pageViewRooms->setIndicatorPosition(Vec2(_pageViewRooms->getContentSize().width * 0.5f, _pageViewRooms->getContentSize().height * 0.05f));


	/*/百家乐房间列表/提示界面
	{
		auto HelpNode = CSLoader::createNode(BJLE_HELP_CSB);
		HelpNode->setPosition(winSize / 2);
		addChild(HelpNode);
		HelpNode->setVisible(false);
		auto panelHelp = dynamic_cast<Layout*>(HelpNode->getChildByName("Panel_help"));
		auto Btn_rule = dynamic_cast<Button*>(panelHelp->getChildByName("Button_rule0"));
		auto Btn_ludan = dynamic_cast<Button*>(panelHelp->getChildByName("Button_rule1"));
		auto Btn_close = dynamic_cast<Button*>(panelHelp->getChildByName("Button_close"));
		auto List_rule = dynamic_cast<ListView*>(panelHelp->getChildByName("ListView_rule"));
		auto List_ludan = dynamic_cast<ListView*>(panelHelp->getChildByName("ListView_ludan"));


		Btn_close->addClickEventListener([=](Ref*){
			HelpNode->setVisible(false);
		});
		Btn_rule->addClickEventListener([=](Ref*){
			Btn_rule->setEnabled(false);
			Btn_ludan->setEnabled(true);
			List_rule->setVisible(true);
			List_ludan->setVisible(false);
		});

		Btn_ludan->addClickEventListener([=](Ref*){
			Btn_rule->setEnabled(true);
			Btn_ludan->setEnabled(false);
			List_rule->setVisible(false);
			List_ludan->setVisible(true);
		});

		auto btnHelp = dynamic_cast<Button*>(_mPanelBjl->getChildByName("Button_help"));
		auto btn_back = dynamic_cast<Button*>(_mPanelBjl->getChildByName("Button_back"));
		auto Img_titleBg = dynamic_cast<ImageView*>(_mPanelBjl->getChildByName("Image_titleBg"));
		auto Atlas_money = dynamic_cast<TextAtlas*>(Img_titleBg->getChildByName("AtlasLabel_money"));

		btn_back->addClickEventListener([=](Ref*){

			close();
		});

		btnHelp->addClickEventListener([=](Ref*){
			HelpNode->setVisible(true);
		});
		Atlas_money->setString(StringUtils::format("%0.2f", money) + "/");
	}*/

	std::vector<ComRoomInfo*> pages;

	int currentIndex = 0;
	int pageCount = (RoomInfoModule()->getRoomCount() / 6);
	int remainder = (RoomInfoModule()->getRoomCount() % 6);
	if (RoomInfoModule()->getRoom(0)->uNameID == 11100503)
	{

		if (pageCount > 0)
		{
			for (int currentPage = 0; currentPage < pageCount; currentPage++)
			{
				pages.clear();
				for (int room = 0; room < 6; currentIndex++, room++)
				{
					pages.push_back(RoomInfoModule()->getRoom(currentIndex));
				}
				//createBjleRoomPage(pages);
			}
		}

		if (remainder > 0)
		{
			pages.clear();
			for (int room = 0; room < remainder; currentIndex++, room++)
			{
				pages.push_back(RoomInfoModule()->getRoom(currentIndex));
			}
			//createBjleRoomPage(pages);
		}
		/*	if (GameNotice::getInstance()->getisRunging())
		{
		GameNotice::getInstance()->setRotating(-90);
		}*/
	}
	else
	{
		if (pageCount > 0)
		{
			for (int currentPage = 0; currentPage < pageCount; currentPage++)
			{
				pages.clear();
				for (int room = 0; room < 6; currentIndex++, room++)
				{
					pages.push_back(RoomInfoModule()->getRoom(currentIndex));
				}
				createRoomPage(pages);
			}
		}

		if (remainder > 0)
		{
			pages.clear();
			for (int room = 0; room < remainder; currentIndex++, room++)
			{
				pages.push_back(RoomInfoModule()->getRoom(currentIndex));
			}
			createRoomPage(pages);
		}

	}



}

//添加房间
void GameRoom::createRoomPage(std::vector<ComRoomInfo*> pages)
{
	//创建房间列表子页面
	auto roomItemLayout = Layout::create();
	roomItemLayout->setName("page");
	roomItemLayout->setContentSize(_pageViewRooms->getContentSize());

	int idx = 0;
	for (auto room : pages)
	{
		idx++;
		// 		int tempidx = idx;
		// 		if (idx > 3) tempidx -= 3;
		float posX = idx * 0.28f;
		float posY = 0.68f;
		float posDis = 0.075;//间距
		if (pages.size() > 3)
		{
			posX = idx * 0.23f;
			posDis = 0.075;
		}

		auto roomItem = createRoomItem(room);
		auto button = (Button*)roomItem->getChildByName("Button_room");
		button->removeFromParentAndCleanup(false);
		button->setPosition(Vec2(roomItemLayout->getContentSize().width * posX - (roomItemLayout->getContentSize().width * posDis), roomItemLayout->getContentSize().height * posY*0.8));
		roomItemLayout->addChild(button, 3);
		auto image = dynamic_cast<ImageView*>(button->getChildByName("Image_di"));
		image->setZOrder(-1);
		button->loadTexturePressed(StringUtils::format("platform/lobbyUi/res/rooms/%d.png", idx));
		button->loadTextureNormal(StringUtils::format("platform/lobbyUi/res/rooms/%d.png", idx));
		_allRoomUI.push_back(button);
	}

	// 添加子页面进入列表中
	_pageViewRooms->addPage(roomItemLayout);

}


void GameRoom::createBjleRoomPage(std::vector<ComRoomInfo*> pages)
{
	//创建房间列表子页面
	auto roomItemLayout = Layout::create();
	roomItemLayout->setName("page");
	roomItemLayout->setContentSize(_pageViewRooms->getContentSize());
	int idx = 0;
	for (auto room : pages)
	{
		idx++;
		// 		int tempidx = idx;
		// 		if (idx > 3) tempidx -= 3;
		float posX = idx * 0.33f;
		float posY = 1.6f;
		float posDis = 0.05;//间距
		if (pages.size() > 3)
		{
			posX = idx * 0.23f;
			posDis = 0.075;
		}

		auto roomItem = createRoomItem(room);
		auto panel_bjl = (Layout*)roomItem->getChildByName("Panel_Bjl");
		panel_bjl->removeFromParentAndCleanup(false);
		panel_bjl->setPosition(Vec2(roomItemLayout->getContentSize().width * posX - (roomItemLayout->getContentSize().width * posDis), roomItemLayout->getContentSize().height * posY));

		roomItemLayout->addChild(panel_bjl, 30);
		_BjlRoomUI.push_back(panel_bjl);
	}

	// 添加子页面进入列表中
	//_pageBjlViewRooms->addChild(roomItemLayout);
}


Node* GameRoom::createRoomItem(ComRoomInfo* roomInfo)
{
	auto roomItemNode = CSLoader::createNode(ROOM_ITEM_UI);
	if (!roomItemNode) return nullptr;
	if (roomInfo->uNameID == 11100503)
	{
		_layout->setVisible(false);
		//_mPanelBjl->setVisible(true);



		auto BjlRoomItem = (Layout*)roomItemNode->getChildByName("Panel_Bjl");

		auto Img_bottom = dynamic_cast<ImageView*>(BjlRoomItem->getChildByName("Image_bottom"));
		_btn_Inter = dynamic_cast<Button*>(Img_bottom->getChildByName("Button_inter"));

		if (!_btn_Inter) return nullptr;
		_btn_Inter->setUserData(roomInfo);
		_btn_Inter->addTouchEventListener(CC_CALLBACK_2(GameRoom::enterRoomEventCallBack, this));
		BjlRoomItem->setUserData(roomInfo);
		BjlRoomItem->addClickEventListener([=](Ref*){
			_roomLogic->start();
			_roomLogic->requestLogin(roomInfo->uRoomID);
			_roomID = roomInfo->uRoomID;
		});
		if (!BjlRoomItem) return nullptr;


		// 金币限制
		auto Label_MoneyLimit = dynamic_cast<Text*>(Helper::seekWidgetByName(BjlRoomItem, "Text_limit"));// kdsb
		if (Label_MoneyLimit)
		{
			std::string str;
			if (0 == roomInfo->iLessPoint)
			{
				str = GBKToUtf8("无限制");
			}
			else if (0 == roomInfo->iMaxPoint)
			{
				str = StringUtils::format(GBKToUtf8("%d准入"), roomInfo->iLessPoint);
			}
			else
			{
				str = StringUtils::format(GBKToUtf8("%d-%d准入"), roomInfo->iLessPoint, roomInfo->iMaxPoint);
			}
			Label_MoneyLimit->setString(str);
		}
	}
	else
	{
		_layout->setVisible(true);
		//_mPanelBjl->setVisible(false);

		auto roomItem = (Button*)roomItemNode->getChildByName("Button_room");
		if (!roomItem) return nullptr;
		roomItem->setUserData(roomInfo);
		roomItem->addTouchEventListener(CC_CALLBACK_2(GameRoom::enterRoomEventCallBack, this));

		//房间背景
		//auto Image_bg = (ImageView*)Helper::seekWidgetByName(roomItem, "Image_bg");
		//Image_bg->setVisible(false);
		std::string bgUrl = StringUtils::format("platform/lobbyUi/res/rooms/%d_bg%d.png", roomInfo->uNameID, roomInfo->iLessPoint);
		//auto imgbg = Sprite::create(bgUrl);
		//if (imgbg)
		//{
		//	roomItem->addChild(imgbg, 1);
		//	imgbg->setPosition(Image_bg->getPosition());
		//}

		//房间类型
		/*auto Image_Type = (ImageView*)Helper::seekWidgetByName(roomItem, "Image_type");
		Image_Type->setVisible(false);
		std::string typeUrl = StringUtils::format("platform/lobbyUi/res/rooms/%d_pass%d.png", roomInfo->uNameID, roomInfo->iLessPoint);



		auto imgtype = Sprite::create(typeUrl);
		if (imgtype)
		{
			roomItem->addChild(imgtype, 2);
			imgtype->setPosition(Image_Type->getPosition());
		}

		// 房间名称
		auto imgTitle = (ImageView*)_layout->getChildByName(StringUtils::format("Image_%d", roomInfo->uNameID));
		if (imgTitle)
		{
			imgTitle->setVisible(true);
		}


		// 房间人数
		auto Label_PeopleCount = dynamic_cast<Text*>(Helper::seekWidgetByName(roomItem, "Text_count"));
		if (Label_PeopleCount)
		{
			Label_PeopleCount->setString(StringUtils::format(GBKToUtf8("%d人在玩"), roomInfo->uPeopleCount + roomInfo->uVirtualUser));
		}

		// 金币限制
		auto Label_MoneyLimit = dynamic_cast<Text*>(Helper::seekWidgetByName(roomItem, "Text_limit"));// kdsb
		if (Label_MoneyLimit)
		{
			std::string str;
			if (0 == roomInfo->iLessPoint)
			{
				str = GBKToUtf8("无限制");
			}
			else if (0 == roomInfo->iMaxPoint)
			{
				str = StringUtils::format(GBKToUtf8("%d准入"), roomInfo->iLessPoint);
			}
			else
			{
				str = StringUtils::format(GBKToUtf8("%d-%d准入"), roomInfo->iLessPoint, roomInfo->iMaxPoint);
			}
			Label_MoneyLimit->setString(str);
		}
*/

	}
	return roomItemNode;
}

//子页面按钮回调
void GameRoom::enterRoomEventCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (!_isTouch) return;

	_currentSelectedRoom = dynamic_cast<Button*>(pSender);

	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		_currentSelectedRoom->setColor(Color3B(170, 170, 170));
		break;
	case cocos2d::ui::Widget::TouchEventType::MOVED:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
	{
		_isTouch = false;
		this->runAction(Sequence::create(DelayTime::create(15.0f), CCCallFunc::create([=]()
		{
			_isTouch = true;
		}), nullptr));

		Size winSize = Director::getInstance()->getWinSize();
		_currentSelectedRoom->setColor(Color3B(255, 255, 255));
		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

		ComRoomInfo* roomInfo = static_cast<ComRoomInfo*>(_currentSelectedRoom->getUserData());

		// 参数校验
		CCAssert(nullptr != roomInfo, "room is nullptr!");
		if (nullptr == roomInfo)
		{
			_isTouch = true;
			return;
		}

		if (roomInfo->bHasPassword)
		{
			GamePasswordInput* layer = GamePasswordInput::create();
			this->addChild(layer, 1000);
			layer->setPosition(winSize / 2);
			layer->onEnterCallback = CC_CALLBACK_1(GameRoom::onEnterPasswordClickCallback, this);
			_isTouch = true;
			_roomLogic->start();
		}
		else
		{

			_roomLogic->start();
			_roomLogic->requestLogin(roomInfo->uRoomID);
			_roomID = roomInfo->uRoomID;
		}
	}
		break;
	case cocos2d::ui::Widget::TouchEventType::CANCELED:
		_currentSelectedRoom->setColor(Color3B(255, 255, 255));
		break;
	default:
		break;
	}
}

//////////////////////////////////////////////////////////////////////////
void GameRoom::onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode)
{
	if (success)
	{
		HNLOG_WARNING("the user enters a room complete message!");


		ComRoomInfo* roomInfo = RoomInfoModule()->getByRoomID(_roomID);


		if ((RoomLogic()->getRoomRule() & GRR_NOTCHEAT)
			&& !(RoomLogic()->getRoomRule() & GRR_QUEUE_GAME)) //单防作弊
		{
			_roomLogic->requestQuickSit();
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(enterGame_please_wait_text), 22);
		}
		else if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME) // 单排队机或者排队机+防作弊
		{
			auto loding = LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(allocation_table_please_wait_text), 22);
			loding->setCancelCallBack([=](){
				_isTouch = true;
				_roomLogic->stop();
				RoomLogic()->close();
			});

			// 进入排队游戏
			_roomLogic->requestJoinQueue();


		}
		else // 金币场不扣积分
		{
			/*if (GameCreator()->getCurrentGameType() == HNGameCreator::NORMAL)
			{
			_roomLogic->stop();

			this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			if (onEnterDeskCallBack)
			{
			_isTouch = true;
			onEnterDeskCallBack(RoomLogic()->getSelectedRoom());
			}
			this->removeFromParent();
			}), nullptr));
			}*/
			if (GameCreator()->getCurrentGameType() == HNGameCreator::BR
				&&roomInfo->uNameID != 11100503 || GameCreator()->getCurrentGameType() == HNGameCreator::SINGLE || GameCreator()->getCurrentGameType() == HNGameCreator::NORMAL)
			{

				m_loading = GameLading::create();
				m_loading->OnLadINGbg(roomInfo->uNameID);
				m_loading->setVisible(true);
				auto Scene = Director::getInstance()->getRunningScene();
				Scene->addChild(m_loading, 1000);
				_roomLogic->requestQuickSit();

			}
			else
			{
				_roomLogic->stop();

				this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
					if (onEnterDeskCallBack)
					{
						_isTouch = true;
						onEnterDeskCallBack(RoomLogic()->getSelectedRoom());
					}
					this->removeFromParent();
				}), nullptr));

			}
		}
	}
	else
	{
		_isTouch = true;
		_roomLogic->stop();
		RoomLogic()->close();
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(message);
		prompt->setCallBack([=](){
			if (handleCode == 255)
			{
				/*MSG_GP_S_SHOP_RESULT shopResult = HNPlatformLogic::getInstance()->getShopResult();
				auto storeLayer = GameStoreLayer::createGameStore(_delegate);
				storeLayer->setChangeDelegate(_delegate);

				storeLayer->setPayTypeBtn(shopResult.PayCount, shopResult.PayType);
				storeLayer->addAliPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
				storeLayer->addWeChatPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
				storeLayer->addQQPayStore(shopResult.PRoductCount, shopResult.ProductInfo);
				storeLayer->addVIPdialiStore(shopResult.VipCount, shopResult.WXid, shopResult.strName);
				storeLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 200, 2000);
				storeLayer->setZOrder(10000);*/
			}
		});
	}
}

void GameRoom::onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo)
{
	_isTouch = true;
	_roomLogic->stop();

	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	if (m_loading)
	{
		m_loading->close();
		m_loading->removeFromParent();
		m_loading = NULL;
	}
	if (success)
	{
		if (INVALID_DESKNO != deskNo && INVALID_DESKSTATION != seatNo)
		{
			// 启动游戏
			bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
			if (!ret)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"));
			}
		}
	}
	else
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(message);
		prompt->setCallBack([=](){
			if (GameCreator()->getCurrentGameType() == HNGameCreator::SINGLE ||
				GameCreator()->getCurrentGameType() == HNGameCreator::BR)
			{
				RoomLogic()->close();
			}
		});
	}
}

void GameRoom::onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo)
{
	_isTouch = true;
	_roomLogic->stop();
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	if (success)
	{
		if (INVALID_DESKNO != deskNo)
		{
			// 启动游戏
			bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
			if (!ret)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"));
			}
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("错误的游戏桌号"));
		}
	}
	else
	{
		RoomLogic()->close();
		GamePromptLayer::create()->showPrompt(message);
	}
}

void GameRoom::onPlatformRoomPassEnter(bool success, UINT roomId)
{
	if (success)
	{
		_roomLogic->start();
		_roomLogic->requestLogin(roomId);
		_roomID = roomId;
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("房间密码错误"));
	}
}

void GameRoom::onSysGiveMoney(SysGiveMoney* give)
{
	// 如果有系统赠送，则发系统广播
	if (give->iResultCode == 1)
	{
		std::string message = StringUtils::format(GBKToUtf8("您的金币少于%lld，系统第%d次赠送%lld金币，本日还可接受%d次赠送！"),
			give->i64MinMoney, give->iCount, give->i64Money, give->iTotal - give->iCount);
		GameNotice::getInstance()->postMessage(message);
	}

	if (_text_gold){
		//double num = (double)PlatformLogic()->loginResult.i64Money*0.01;
		//_text_gold->setString(StringUtils::format("%.2f", num));
		std::string str = HNToWan(PlatformLogic()->loginResult.i64Money);
		_text_gold->setString(str);
	}
}

//更新房间人数
void GameRoom::updateRoomPeopleCount(UINT roomID, UINT userCount)
{
	if (!_pageViewRooms || _allRoomUI.empty()) return;

	for (auto room : _allRoomUI)
	{
		ComRoomInfo* pRoom = static_cast<ComRoomInfo*>(room->getUserData());
		if (pRoom->uRoomID == roomID)
		{
			pRoom->uPeopleCount = userCount;
			auto text_count = dynamic_cast<Text*>(Helper::seekWidgetByName(room, "Text_count"));
			if (text_count)
			{
				text_count->setString(StringUtils::format(GBKToUtf8("%u人在玩"), userCount));
			}
			break;
		}
	}
}

void GameRoom::refresh()
{
	for (int i = 0; i < RoomInfoModule()->getRoomCount(); i++)
	{
		auto roominfo = RoomInfoModule()->getRoom(i);
		updateRoomPeopleCount(roominfo->uRoomID, roominfo->uPeopleCount);
	}
}

void GameRoom::setChangeDelegate(MoneyChangeNotify* delegate)
{
	_delegate = delegate;
}

void GameRoom::runActionByRight()
{
	if (_pageViewRooms)
	{
		_pageViewRooms->runAction(Sequence::create(MoveTo::create(0.3f, Vec2(640, 300)), CallFunc::create([=]() {
			this->setCanTouch(true);
		}), nullptr));
	}

}



//快速加入游戏
void GameRoom::QuickJionGame()
{

	ComRoomInfo* roomInfo = RoomInfoModule()->getRoom(0);
	// 参数校验
	CCAssert(nullptr != roomInfo, "room is nullptr!");

	if (roomInfo->bHasPassword)
	{

	}
	else
	{

		_roomLogic->start();
		_roomLogic->requestLogin(roomInfo->uRoomID);
		_roomID = roomInfo->uRoomID;
	}


}


// 添加游戏图片
void GameRoom::AddImagive()
{
	ComRoomInfo* roomInfo = RoomInfoModule()->getRoom(0);

	std::string str = StringUtils::format("platform/common/img_%d.png", roomInfo->uNameID);

	auto sp = Sprite::create(str);
	if (sp == NULL)
	{
		return;
	}
	this->addChild(sp, 1000);
	sp->setPosition(Vec2(640, 360));
}





