/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __LayerManger_h__
#define __LayerManger_h__

#include "cocos2d.h"

USING_NS_CC;

class LayerManger
{
public:
	LayerManger();
	virtual ~LayerManger();

private:
	Vector<Node*> _childs;
};

#endif // __LayerManger_h__
