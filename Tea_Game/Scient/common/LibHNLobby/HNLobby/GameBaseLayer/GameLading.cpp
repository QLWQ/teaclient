/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameLading.h"

//////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////

GameLading::GameLading()
{
}

GameLading::~GameLading()
{
	unschedule(schedule_selector(GameLading::onSetTime));
}

//布局文件
static const char* GameLadingLayer = "platform/GameLoding.csb";

bool GameLading::init()
{
	if (!HNLayer::create())
	{
		return false;
	}
	cocos2d::Size winSize = Director::getInstance()->getWinSize();
	auto node = CSLoader::createNode(GameLadingLayer);
	node->setPosition(winSize / 2);
	addChild(node, 10000);


	bg = dynamic_cast<ImageView*>(node->getChildByName("img_bg"));
	auto img = dynamic_cast<ImageView*>(bg->getChildByName("img"));
	 m_bar = dynamic_cast<LoadingBar*>(img->getChildByName("loading_bar"));
	 bg1 = dynamic_cast<ImageView*>(node->getChildByName("Image_bjlbg"));
	 auto img1 = dynamic_cast<ImageView*>(bg1->getChildByName("img"));
	 m_bar1 = dynamic_cast<LoadingBar*>(img1->getChildByName("loading_bar1"));


	


	return  true;
}







//定时刷新定时器
void GameLading::onSetTime(float dt)
{
	m_bar->setPercent(m_bar->getPercent() + 30);
	if (m_bar->getPercent() == 100)
	{
		unschedule(schedule_selector(GameLading::onSetTime));
	}
}

void GameLading::OnLadINGbg(int kindid)
{
	if (kindid == 11100503)
	{
		bg->setVisible(false);
		bg1->setVisible(true);
		schedule(schedule_selector(GameLading::onbjlSetTime), 0.05f);
	}
	else
	{
		bg->setVisible(true);
		bg1->setVisible(false);
		std::string str = StringUtils::format("platform/loading/img_%d.png", kindid);
		bg->loadTexture(str);
		schedule(schedule_selector(GameLading::onSetTime), 0.05f);
	}
	

}

void GameLading::onbjlSetTime(float dt)
{
	m_bar1->setPercent(m_bar1->getPercent() + 30);
	if (m_bar1->getPercent() == 100)
	{
		unschedule(schedule_selector(GameLading::onbjlSetTime));
	}

}
