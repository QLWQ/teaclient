/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "ProductInfo.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

namespace HN 
{
	std::string PRODUCT_INFO::serialize()
	{
		rapidjson::Document doc;
		doc.SetObject();
		rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();
		rapidjson::Value object(rapidjson::kObjectType);
		object.AddMember("identifier", rapidjson::StringRef(identifier.c_str()), allocator);
		object.AddMember("number", number, allocator);
		object.AddMember("price", price, allocator);
		object.AddMember("goodsType", goodsType, allocator);
		object.AddMember("payType", payType, allocator);
		object.AddMember("xmlFile", rapidjson::StringRef(xmlFile.c_str()), allocator);
		object.AddMember("orderID", rapidjson::StringRef(orderID.c_str()), allocator);
		rapidjson::StringBuffer buffer;
		rapidjson::Writer<rapidjson::StringBuffer> write(buffer);
		object.Accept(write);
		return buffer.GetString();
	}

	void PRODUCT_INFO::deserialize(std::string data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
		if (!doc.HasParseError())
		{
			if (doc.IsObject() && doc.HasMember("status"))
			{
				// 商品ID
				if (doc.HasMember("identifier") && doc["identifier"].IsString()) {
					identifier = doc["identifier"].GetString();
				}
				
				// 商品数量
				if (doc.HasMember("number") && doc["number"].IsInt64()) {
					number = doc["number"].GetInt64();
				}
				
				// 商品价格
				if (doc.HasMember("price") && doc["price"].IsDouble()) {
					price = doc["price"].GetDouble();
				}

				// 商品类型
				if (doc.HasMember("goodsType") && doc["goodsType"].IsInt()) {
					goodsType = doc["goodsType"].GetInt();
				}
				
				// 支付类型
				if (doc.HasMember("payType") && doc["payType"].IsInt()) {
					payType = doc["payType"].GetInt();
				}

				// 
				if (doc.HasMember("xmlFile") && doc["xmlFile"].IsString()) {
					xmlFile = doc["xmlFile"].GetString();
				}

				// 订单号
				if (doc.HasMember("orderID") && doc["orderID"].IsString()) {
					orderID = doc["orderID"].GetString();
				}
			}
		}
	}
}                                                  