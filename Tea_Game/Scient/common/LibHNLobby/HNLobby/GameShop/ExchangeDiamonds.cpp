/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "ExchangeDiamonds.h"
#include "GameStoreLayer.h"
#include <string>

static const char* EXCHANGE_DIAMONDS_CSB	= "platform/gameStore/exchangeUI.csb";


ExchangeDiamonds* ExchangeDiamonds::createExchangeMoneyLayer()
{
	ExchangeDiamonds *pRet = new ExchangeDiamonds();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	CC_SAFE_DELETE(pRet);
	return pRet;
}

ExchangeDiamonds::ExchangeDiamonds() : _exchangeRate(0)
{
	
}

ExchangeDiamonds::~ExchangeDiamonds()
{
	PlatformLogic()->removeEventSelector(MDM_GP_MONEY_CHANGE, ASS_GP_CHANGE_CONFIG);
	PlatformLogic()->removeEventSelector(MDM_GP_MONEY_CHANGE, ASS_GP_COINTOJEWELS);
	PlatformLogic()->removeEventSelector(MDM_GP_MONEY_CHANGE, ASS_GP_JEWELSTOCOIN);
}

bool ExchangeDiamonds::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(EXCHANGE_DIAMONDS_CSB);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto textField = dynamic_cast<TextField*>(node->getChildByName("TextField_count"));
	textField->setVisible(false);
	auto editBox_exchange = HNEditBox::createEditBox(textField, this);
	editBox_exchange->setCascadeOpacityEnabled(true);

	auto btn_exchange = dynamic_cast<Button*>(node->getChildByName("Button_sure"));
	btn_exchange->addClickEventListener([=](Ref*){

		std::string number = editBox_exchange->getString();
		if (number.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换数量不能为空"));
			return;
		}

		if (!Tools::verifyNumber(number) 
			|| atoi(editBox_exchange->getString().c_str()) <= 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的兑换数量"));
			return;
		}
		if ((!Tools::verifyNumber(number)
			|| atoi(editBox_exchange->getString().c_str()) > 0) && atoi(editBox_exchange->getString().c_str()) <20)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("单次兑换需大于等于20房卡！"));
			return;
		}

		if (editBox_exchange->getStringLength() >= 9)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("超出单次兑换限额"));
			return;
		}

		MSG_GP_I_ChangeRequest change;
		change.iChangeJewels = atoi(editBox_exchange->getString().c_str());
		change.iUserID = PlatformLogic()->loginResult.dwUserID;

		PlatformLogic()->sendData(MDM_GP_MONEY_CHANGE, ASS_GP_JEWELSTOCOIN, &change, sizeof(change),
			HN_SOCKET_CALLBACK(ExchangeDiamonds::exchangeDiamondsCallBack, this));
	});

	// 获取兑换配置
	PlatformLogic()->sendData(MDM_GP_MONEY_CHANGE, ASS_GP_CHANGE_CONFIG, nullptr, 0, [=](HNSocketMessage* socketMessage){
	
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_ChangeConfig, socketMessage->objectSize, true,
			"MSG_GP_ContestNotice size of error!");

		auto config = (MSG_GP_O_ChangeConfig*)socketMessage->object;

		auto text_cfg = dynamic_cast<Text*>(node->getChildByName("Text_hint"));
		text_cfg->setVisible(true);
		text_cfg->setString(StringUtils::format(GBKToUtf8("1房卡兑换%d金币"), config->iJewelsToCoin));

		return true;
	});

	return true;
}

void ExchangeDiamonds::closeFunc()
{
	
}

bool  ExchangeDiamonds::exchangeDiamondsCallBack(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_O_ChangeRespones) == socketMessage->objectSize, "MSG_GP_O_ChangeRespones is error.");
	MSG_GP_O_ChangeRespones* exchangeResult = (MSG_GP_O_ChangeRespones*)socketMessage->object;

	//(0:兑换成功；1：用户不存在;2：兑换功能关闭;3：金币不足；4：钻石不足)
	switch (socketMessage->messageHead.bHandleCode)
	{
	case 0:
	{
			  PlatformLogic()->loginResult.i64Money = exchangeResult->i64Money;
			  PlatformLogic()->loginResult.iJewels = exchangeResult->iJewels;

			  _delegate->walletChanged();

			  GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换成功！"));
	}break;
	case 1:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("用户不存在！"));
		break;
	case 2:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换功能关闭！"));
		break;
	case 3:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("金币不足！"));
		break;
	case 4:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("房卡不足！"));
		break;
	default:
		break;
	}

	return true;
}

void ExchangeDiamonds::setChangeDelegate(MoneyChangeNotify* delegate)
{
	_delegate = delegate;
}
