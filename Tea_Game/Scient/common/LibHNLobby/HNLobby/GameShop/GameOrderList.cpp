/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameOrderList.h"
#include "GameStoreLayer.h"
#include "HNUIExport.h"
#include "network/HttpRequest.h"
#include "json/rapidjson.h"
#include <string>

// 查询订单
static const char* ORDERLIST_DLG_PATH = "platform/gameStore/QueryOrder.csb";

// 订单选项
static const char* ORDERLIST_ITEM_PATH = "platform/gameStore/OrderItem.csb";

GameOrderList::GameOrderList()
{  
}

GameOrderList::~GameOrderList()
{
    PlatformLogic()->removeEventSelector(MDM_GP_PAIHANGBANG, 0);
    HNHttpRequest::getInstance()->removeObserver(this);
}

void GameOrderList::closeFunc()
{
	if (onCloseCallBack)
	{
		onCloseCallBack();
	}
}

bool GameOrderList::init()
{
	if (!HNLayer::init()) {
		return false;
	}

	setCascadeOpacityEnabled(true);

	// 布局
	auto node = CSLoader::createNode(ORDERLIST_DLG_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}

	// 关闭
	auto btnClose = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btnClose->addClickEventListener([=](Ref*) {
		close();
	});

	// 列表
	_orderListView = (ListView*)img_bg->getChildByName("ListView_orderList");

	// 获取订单记录
    getOrderListData();

    return true;
}

//获取交易记录
void GameOrderList::getOrderListData()
{
    std::string url = PlatformConfig::getInstance()->getNoticeUrl();
    url.append("Type=GetOrderListByUser");
    url.append(StringUtils::format("&userID=%d", PlatformLogic()->loginResult.dwUserID));
    url.append("&pageIndex=1");
    url.append("&pagesize=20");

    HNHttpRequest::getInstance()->addObserver(this);
    HNHttpRequest::getInstance()->request("GetOrderList", cocos2d::network::HttpRequest::Type::GET, url);
}

//申请订单列表回调
void GameOrderList::getOrderListEventSelector(const std::string& data)
{
    if (data.empty())
    {
        log("data is empty");
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("查询失败"));
		prompt->setCallBack([=](){
			close();
		});
        return;
    }

    rapidjson::Document doc;
    doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

    if (doc.HasParseError() || !doc.IsObject())
    {
        log("data HasParseError");
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("查询失败"));
		prompt->setCallBack([=](){
			close();
		});
        return;
    }

	_iOrderList.clear();

	if (!doc.HasMember("list")) {
		return;
	}

    rapidjson::Value &array = doc["list"];
    if (array.IsArray())
    {
        for (UINT i = 0; i < array.Size(); i++)
        {
            rapidjson::Value& item = array[i];

			OrderInfo order;

            if (item.HasMember("orderID") && item["orderID"].IsString())
            {
				order.orderID = item["orderID"].GetString();
            }
            if (item.HasMember("paySuccess") && item["paySuccess"].IsBool())
            {
				order.paySuccess = item["paySuccess"].GetBool();
            }
            if (item.HasMember("TypeInfo") && item["TypeInfo"].IsString())
            {
				order.typeinfo = item["TypeInfo"].GetString();
            }
            if (item.HasMember("PayMoney") && item["PayMoney"].IsInt())
            {
				order.price = item["PayMoney"].GetInt();
            }
            if (item.HasMember("Info"))
            {
                if (item["Info"].IsString()) {
					order.receiptData = item["Info"].GetString();					
                }
                else {
					order.receiptData = "";
                }               
            }

			_iOrderList.push_back(order);
        }

		if (_iOrderList.empty())
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("暂无订单数据！"));
			prompt->setCallBack([=](){
				close();
			});
		}
		else
		{
			//显示订单数据
			createOrderList();
		}
    }
}

void GameOrderList::createOrderList()
{
	_orderListView->removeAllItems();

	for (int i = 0; i < _iOrderList.size(); i++)
	{
		auto node = CSLoader::createNode(ORDERLIST_ITEM_PATH);
		CCAssert(node != nullptr, "node null");

		auto img_item = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
		img_item->removeFromParentAndCleanup(false);

		auto text_num = dynamic_cast<Text*>(img_item->getChildByName("Text_Id"));
		text_num->setString(StringUtils::format("%02d", i));

		auto text_order = dynamic_cast<Text*>(img_item->getChildByName("Text_orderId"));
		text_order->setString(StringUtils::format("%s", _iOrderList.at(i).orderID.c_str()));

		auto btn_state = dynamic_cast<Button*>(img_item->getChildByName("Button_status"));
		btn_state->setEnabled(!_iOrderList.at(i).paySuccess);
		btn_state->setTag(i);
		btn_state->addClickEventListener(CC_CALLBACK_1(GameOrderList::reissueApplyClickCallback, this));

		_orderListView->pushBackCustomItem(img_item);
	}
}

void GameOrderList::reissueApplyClickCallback(Ref* pSender)
{
	_reissueButton = (Button*)pSender;

	std::string requestData;
	// 类型
	requestData.append("type=PaySucceedByIOS");

	// 用户名
	requestData.append("&userName=");
	requestData.append(PlatformLogic()->loginResult.szName);

	// 验证数据
	requestData.append("&receipt_data=");
	requestData.append(_iOrderList[_reissueButton->getTag()].receiptData);

	// 订单号
	requestData.append("&OrderID=");
	requestData.append(_iOrderList[_reissueButton->getTag()].orderID);
	_orderID = _iOrderList[_reissueButton->getTag()].orderID;

	// 支付金额
	requestData.append("&payMoney=");
	char ch[20] = { 0 };
	sprintf(ch, "%lf", _iOrderList[_reissueButton->getTag()].price);
	requestData.append(ch);

	// 支付方式
	requestData.append("&typeInfoLog=IAP");

	log("send>%s", requestData.c_str());
	std::string url_temp = PlatformConfig::getInstance()->getPayCallbackUrl_iOS();

	log("url_temp =%s", PlatformConfig::getInstance()->getPayCallbackUrl_iOS().c_str());
	
	HNHttpRequest::getInstance()->request("PayAgain", cocos2d::network::HttpRequest::Type::POST, url_temp, requestData);
}


void GameOrderList::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{   
    std::string tmp = Utf8ToGB(responseData.c_str());
    
    if (requestName.compare("GetOrderList") == 0)
   {
       if (!isSucceed)
       {
		   auto prompt = GamePromptLayer::create();
		   prompt->showPrompt(GBKToUtf8("查询失败"));
		   prompt->setCallBack([=](){
			   close();
		   });
           return;
       }

	   getOrderListEventSelector(responseData);
   }

   if (requestName.compare("PayAgain") == 0)
   {
       if (!isSucceed)
       {
		   _reissueButton->setEnabled(true);
           GamePromptLayer::create()->showPrompt(GBKToUtf8("重新支付失败！"));
           return;
       }

       PayAgain(responseData);
   }
}

void GameOrderList::PayAgain(const std::string& data)
{
    std::string cache = data;

    if (cache.empty())
    {
        supportAlert();
        return;
    }
    rapidjson::Document doc;
    doc.Parse<rapidjson::kParseDefaultFlags>(cache.c_str());

    if (doc.HasParseError())
    {
        supportAlert();
        return;
    }

    // 解析结果
    if (!doc.IsObject() || !doc.HasMember("rs") || !doc.HasMember("msg"))
    {
        supportAlert();
        return;
    }

    int rs = doc["rs"].GetInt();
    if (rs == 1)
    {
        if (doc.HasMember("WalletMoney"))
        {
            LLONG money = doc["WalletMoney"].GetInt64();
			std::string str = StringUtils::format(GBKToUtf8("补单成功, 当前金币为:%lld!"), money);
			GamePromptLayer::create()->showPrompt(str);
            PlatformLogic()->loginResult.i64Money = money;
            _delegate->walletChanged();

            //显示成功的状态
			_reissueButton->setEnabled(false);
        }
		else if(doc.HasMember("WalletCard"))
		{
			LLONG card = doc["WalletCard"].GetInt64();
			std::string str = StringUtils::format(GBKToUtf8("补单成功, 当前房卡为:%lld!"), card);
			GamePromptLayer::create()->showPrompt(str);
			PlatformLogic()->loginResult.iJewels = card;
			_delegate->walletChanged();

			//显示成功的状态
			_reissueButton->setEnabled(false);
		}
        else
        {          
            supportAlert();
        }

    }
    else
    {
        supportAlert();
    }
}

void GameOrderList::supportAlert()
{
	_reissueButton->setEnabled(true);
	std::string str = StringUtils::format(GBKToUtf8("刷新失败（%s），请联系客服。"), _orderID.c_str());
	GamePromptLayer::create()->showPrompt(str);
}

