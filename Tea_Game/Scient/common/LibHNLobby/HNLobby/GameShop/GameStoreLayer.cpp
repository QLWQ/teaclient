/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameStoreLayer.h"
#include "HNMarketExport.h"
#include <string>
#include "external/tinyxml2/tinyxml2.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"
#include "GameOrderList.h"
#include "ExchangeDiamonds.h"

#define JSON_RESOVE(obj, key) (obj.HasMember(key) && !obj[key].IsNull())

using namespace cocos2d::network;

static const char* NOTIFY_URL_DINPAY				= "http://%s/manage/pay/zhifu/mobilereceive.aspx";
static const char* MECHANT_ID_DINPAY				= "1111110166";
static const char* MECHANT_KEY_DINPAY				= "123456789a123456789_";


static const char* STORE_NODE_PATH		= "platform/gameStore/StoreUI.csb";
static const char* STORE_ITEM_PATH      = "platform/gameStore/StoreItem.csb";
static const char* STORE_PAYTYPE_PATH = "platform/gameStore/PaySelect.csb";
static const char* STORE_COIN_PATH      = "platform/gameStore/res/maipingjinbi1.png";
static const char* STORE_DIAMOND_PATH   = "platform/gameStore/res/maipingzuanshi1.png";
static const char* STORE_PROMPT_PATH = "platform/gameStore/Prompt.csb";
static const char* STORE_ROOMCRAD_PATH = "platform/gameStore/roomcard_bg.png";
static const char* STORE_JBCRAD_PATH = "platform/gameStore/jbscbg.png";
static const std::string STORE_GOODSID[] = { "FK1001", "FK1002", "FK1003", "FK1004", "FK1005", "FK1006","FK1007"};
static const std::string STORE_CARD[] = { "12", "30", "50", "98" };
static const std::string STORE_MONEY[] = { "12", "30", "50", "98" };
static const int STORE_GOLDNUM[] = { 12, 30, 50, 98 };
static const std::string STORE_MENUBUTTON[] = { "Button_card", "Button_gold", "Button_integral", "Button_oder" };
static const std::string STORE_MENUSPRITE[] = { "Sprite_vip", "Sprite_card", "Sprite_gold", "Sprite_integral", "Sprite_oder"};
static const char* ORDER_ITEM_CSB = "platform/gameStore/OrderItem_Node.csb";
static const char* ORDER_DETAILS_CSB = "platform/gameStore/OrderDetails_Node.csb";

enum GameStoreButton
{
	E_VIPStore = 0,
	E_CardStore = 1,
	E_GlodStore = 2,
	E_IntegralStore = 3,
	E_OderStore = 4
};

GameStoreLayer* GameStoreLayer::createGameStore(MoneyChangeNotify* delegate)
{
	GameStoreLayer *pRet = new GameStoreLayer(); 
	if (pRet && pRet->init()) 
	{ 
		pRet->setChangeDelegate(delegate);
		pRet->autorelease(); 
		return pRet; 
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return nullptr;
	}
}

GameStoreLayer::GameStoreLayer() 
	:_product(nullptr)
	,onCloseCallBack (nullptr)
{

}

GameStoreLayer::~GameStoreLayer()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

void GameStoreLayer::closeFunc()
{
	if (onCloseCallBack)
	{
		onCloseCallBack();
	}
}

bool GameStoreLayer::init()
{
	if (!HNLayer::init()) {
		return false;
	}


	auto node = CSLoader::createNode(STORE_NODE_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node,101);
	auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}
	
	//VIP等级
	//_textVipLevel = dynamic_cast<TextAtlas*>(node->getChildByName("AtlasLabel_viplevel"));
	_spVipCurrent = dynamic_cast<Sprite*>(node->getChildByName("Sprite_vip_current"));
	_spVipNext = dynamic_cast<Sprite*>(node->getChildByName("Sprite_vip_next"));
	_vipProgressTimer = dynamic_cast<LoadingBar*>(node->getChildByName("LoadingBar_vip"));
	_curVIPNumber = dynamic_cast<Text*>(_vipProgressTimer->getChildByName("Text_2"));

	/*
		支付按钮
	*/
	auto ScrollView_Node = dynamic_cast<ui::ScrollView*>(node->getChildByName("ScrollView_Node"));
	ScrollView_Node->setScrollBarEnabled(false);
	//_VipList = dynamic_cast<Node*>(ScrollView_Node->getChildByName("Node_vip"));
	_RoomList = dynamic_cast<Node*>(node->getChildByName("Node_roomcard"));
	_GoldList = dynamic_cast<Node*>(node->getChildByName("Node_gold"));
	//_IntegralList = dynamic_cast<Node*>(ScrollView_Node->getChildByName("Node_integral"));
	
	//////////////////////////////商场页面切换管理//////////////////////////////////////
	_vecMenuButton.clear();
	auto btn_vip = dynamic_cast<Button*>(node->getChildByName("Button_vip"));
	if (btn_vip)btn_vip->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::ClickChangeEventForStore, this));
	if (btn_vip)_vecMenuButton.push_back(btn_vip);
	for (int i = 0; i < 4; i++)
	{
		auto btn = dynamic_cast<Button*>(img_bg->getChildByName(STORE_MENUBUTTON[i]));
		if (btn)
		{
			btn->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::ClickChangeEventForStore, this));
			_vecMenuButton.push_back(btn);
			/*if (i >= 1)
			{
				btn->setVisible(false);
			}*/
		}
	}
	/*_vecMenuSprite.clear();
	for (int i = 0; i < 5; i++)
	{
		auto sp = dynamic_cast<Sprite*>(img_bg->getChildByName(STORE_MENUSPRITE[i]));
		if (sp)
		{
			_vecMenuSprite.push_back(sp);
		}
	}*/
	//////////////////////////////////////////////////////////////////////////////////

	/*_OrderList = dynamic_cast<ui::ScrollView*>(node->getChildByName("ScrollView_Order"));
	_OrderList->setVisible(false);
	_OrderTop = dynamic_cast<ImageView*>(node->getChildByName("image_order"));
	_OrderTop->setVisible(false);
	_RoomList->setVisible(false);
	_GoldList->setVisible(false);
	_IntegralList->setVisible(false);
	_VipList->setVisible(true);
	_vecMenuButton[0]->setVisible(false);
	_vecMenuSprite[0]->setVisible(true);*/

	//for (int i = 1; i < 7; i++)
	//{
	//	auto Image_node = _VipList->getChildByName(StringUtils::format("Image_vip_%d", i));
	//	auto btn = dynamic_cast<Button*>(Image_node->getChildByName("btn_pay"));
	//	btn->setEnabled(false);
	//	//加载VIP特效
	//	if (i >= 3)
	//	{		
	//		auto nodeEffect = CSLoader::createNode(StringUtils::format("platform/common/vip_%d_effect.csb", i));
	//		nodeEffect->setScale(0.5);
	//		nodeEffect->setAnchorPoint(Vec2(0.5, 0.5));
	//		nodeEffect->setPosition(Vec2(72, 55));
	//		auto ation = CSLoader::createTimeline(StringUtils::format("platform/common/vip_%d_effect.csb", i));
	//		nodeEffect->runAction(ation);
	//		ation->gotoFrameAndPlay(0, true);
	//		nodeEffect->setName("EffectNode");
	//		Image_node->addChild(nodeEffect);
	//	}
	//	_vecVipButton.push_back(btn);
	//}

	//房卡购买列表
	auto btnstr = { "Image_1", "Image_2", "Image_3", "Image_4"}; // 6.6
	int index = 0;
	for (auto i : btnstr){
		auto img = dynamic_cast<Node*>(_RoomList->getChildByName(i));
		auto btn = dynamic_cast<Button*>(img->getChildByName("Button_buy"));
		btn->addClickEventListener([=](Ref* pRef){
			//支付暂时不进行对接
			/*LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在跳转页面！"), 25);
			this->runAction(Sequence::create(DelayTime::create(3.0f), CallFunc::create([=](){
				LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			     }),nullptr));
			this->getOrderNo(index);*/
		});
		index++;
	}

	//房卡自定义购买


	//金币购买列表
	auto goldstr = { "Image_1", "Image_2", "Image_3", "Image_4" };
	int goldnum = 0;
	for (auto i : goldstr){
		auto img = dynamic_cast<Node*>(_GoldList->getChildByName(i));
		auto btn = dynamic_cast<Button*>(img->getChildByName("Button_buy"));
		btn->addClickEventListener([=](Ref* pRef){
			//支付暂时不进行对接
			/*auto PopLayer = GamePromptLayer::create(true);
			PopLayer->showPrompt(GBKToUtf8("您确定兑换？"));
			PopLayer->setCallBack([=]()
			{
				MSG_GP_I_ChangeRequest change;
				change.iChangeJewels = STORE_GOLDNUM[goldnum];
				change.iUserID = PlatformLogic()->loginResult.dwUserID;
				PlatformLogic()->sendData(MDM_GP_MONEY_CHANGE, ASS_GP_JEWELSTOCOIN, &change, sizeof(change),
					HN_SOCKET_CALLBACK(GameStoreLayer::exchangeDiamondsCallBack, this));
			});*/
		});
		goldnum = goldnum + 1;
	}

	//兑换取消

	//auto integral = { "Image_1", "Image_2", "Image_3", "Image_4", "Image_5", "Image_6", "Image_7", "Image_8", "Image_9" };
	////static const int  GiftPoint[] = { 100, 500, 800, 900, 1100, 1300, 1600,  };
	//int  count = 0;	
	//for (auto i : integral)
	//{
	//	auto img = dynamic_cast<Node*>(_IntegralList->getChildByName(i));
	//	auto btn = dynamic_cast<Button*>(img->getChildByName("Button_buy"));
	//	btn->addClickEventListener([=](Ref* pRef){
	//		auto PopLayer = GamePromptLayer::create(true);
	//		PopLayer->showPrompt(GBKToUtf8("您确定兑换？"));
	//		PopLayer->setCallBack([=]()
	//		//std::string url = "http://alipay.qp888m.com/wxhongbao/CardOrCash.php";		
	//		{
	//			std::string url = (StringUtils::format("http://alipay.qp888m.com/c779app/LotteriesExchange.php?userid=%d&leid=%d",
	//				PlatformLogic()->loginResult.dwUserID, count + 1));

	//			HNHttpRequest::getInstance()->request("RequestExchange", cocos2d::network::HttpRequest::Type::GET, url);
	//			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在兑换！"), 25);
	//		});
	//	});
	//	count++;
	//}


	/*auto imgf = dynamic_cast<Node*>(_GoldList->getChildByName("Image_22"));
	auto exchange_btn = dynamic_cast<Button*>(imgf->getChildByName("Button_buy"));
	exchange_btn->addClickEventListener([=](Ref*){
	 
	 	auto exchangeMoneyLayer = ExchangeDiamonds::createExchangeMoneyLayer();
		exchangeMoneyLayer->open(ACTION_TYPE_LAYER::FADE, this->getParent(), Vec2::ZERO, 1000000 + 1, 10, true, [=](Ref*) {
	 
	 		exchangeMoneyLayer->close();
	 	});
	 	exchangeMoneyLayer->setChangeDelegate(_delegate);
	});*/

		
	
	/*_diamondListView = dynamic_cast<ui::ListView*>(img_bg->getChildByName("ListView_diamond"));
	_coinListView = dynamic_cast<ui::ListView*>(img_bg->getChildByName("ListView_coin"));

	_diamondListView->setVisible(false);
	_coinListView->setVisible(false);*/

	// 关闭
	_btn_close = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	_btn_close->addClickEventListener([=](Ref*){
		_btn_close->setEnabled(false);
		close(); 
	});

	// 钻石
	//auto btnDiamond = dynamic_cast<Button*>(img_bg->getChildByName("Button_buyDiamond"));
	//auto imgDiamondTxt = (ImageView*)btnDiamond->getChildByName("Image_string");
	//_diamondString = imgDiamondTxt;

	//// 金币
	//auto btnCoin = dynamic_cast<Button*>(img_bg->getChildByName("Button_buyCoin"));
	//auto imgCoinTxt = (ImageView*)btnCoin->getChildByName("Image_string");
	//_coinString = imgCoinTxt;
	//// 钻石
	//btnDiamond->addClickEventListener([=](Ref* pRef){

	//	_diamondListView->setVisible(true);
	//	_coinListView->setVisible(false);

	//	btnDiamond->setEnabled(false);
	//	_diamondString->loadTexture("platform/gameStore/res/goumaizuanshi1.png");
	//	
	//	btnCoin->setEnabled(true);
	//	_coinString->loadTexture("platform/gameStore/res/goumaijinbi2.png");
	//});

	//// 金币
	//btnCoin->addClickEventListener([=](Ref*){

	//	_diamondListView->setVisible(false);
	//	_coinListView->setVisible(true);

	//	btnDiamond->setEnabled(true);
	//	_diamondString->loadTexture("platform/gameStore/res/goumaizuanshi2.png");

	//	btnCoin->setEnabled(false);
	//	_coinString->loadTexture("platform/gameStore/res/goumaijinbi1.png");
	//});

	//// 默认显示钻石
	//btnDiamond->setEnabled(false);
	//_diamondString->loadTexture("platform/gameStore/res/goumaizuanshi1.png");
	//btnCoin->setEnabled(true);

	//// 查询
	//auto query_btn = dynamic_cast<Button*>(img_bg->getChildByName("Button_query"));
	//query_btn->addClickEventListener([=](Ref*){

	//	auto OrderLayer = GameOrderList::create();
	//	OrderLayer->setChangeDelegate(_delegate);
	//	OrderLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 100, 103);
	//});

	//// 兑换
	//_Button_change = dynamic_cast<Button*>(img_bg->getChildByName("Button_exchange"));
	//_Button_change->setVisible(false);
// 	exchange_btn->addClickEventListener([=](Ref*){
// 
// 		auto exchangeMoneyLayer = ExchangeDiamonds::createExchangeMoneyLayer();
// 		exchangeMoneyLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 10, 10, true, [=](Ref*) {
// 
// 			exchangeMoneyLayer->close();
// 		});
// 		exchangeMoneyLayer->setChangeDelegate(_delegate);
// 	});

	/*if (0 == ProductManger::getInstance()->getSize())
	{
		ProductManger::getInstance()->_onGetCallBack = [=](bool isSucceed){
		
			if (isSucceed)
			{
				createGoodsList();
			}
			else
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("获取商品信息失败"));
			}
		};

		ProductManger::getInstance()->addProducts("mixproject");
	}
	else {
		createGoodsList();
	}*/


	// 在Cocos2d-3.x中，增加了可以加载网页的API，但必须注意的是：到目前为止，只能在Android和iOS平台进行加载网页，如果在其他的平台，
	// 代码则会编译出错，所以需要加上平台判断的宏定义才能编译通过
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	/*auto webView = cocos2d::experimental::ui::WebView::create();
	webView->setContentSize(_winSize);
	webView->setPosition(_winSize / 2.0);
	webView->loadURL("http://www.baidu.com");
	this->addChild(webView);*/
#endif


	//std::string yongurl = StringUtils::format("http://alipay.qp888m.com/app/get_order_info.php?userid=%d", PlatformLogic()->loginResult.dwUserID);
	//HNHttpRequest::getInstance()->addObserver(this);
	//HNHttpRequest::getInstance()->request("requestOrder", cocos2d::network::HttpRequest::Type::POST, yongurl);

	//std::string vipurl = StringUtils::format("http://alipay.qp888m.com/c779app/get_recharge_info.php?userid=%d", PlatformLogic()->loginResult.dwUserID);
	//HNHttpRequest::getInstance()->addObserver(this);
	//HNHttpRequest::getInstance()->request("requestVip", cocos2d::network::HttpRequest::Type::POST, vipurl);


	return true;
}

void GameStoreLayer::setChangeDelegate(MoneyChangeNotify* delegate)
{
	_delegate = delegate;
}

void GameStoreLayer::createGoodsList()
{
	for (int i = 0; i < ProductManger::getInstance()->getSize(); i ++)
	{
		auto product = ProductManger::getInstance()->getProducts(i);

		switch (product->goodsType)
		{
		case 1://金币
		{
			do 
			{
				auto node = CSLoader::createNode(STORE_ITEM_PATH);
				CC_BREAK_IF(nullptr == node);

				auto btnCoin = dynamic_cast<Button*>(node->getChildByName("Button_item"));
				CC_BREAK_IF(nullptr == btnCoin);
				btnCoin->removeFromParentAndCleanup(false);
				btnCoin->setUserData(const_cast<PRODUCT_INFO*>(product));
				btnCoin->addTouchEventListener(CC_CALLBACK_1(GameStoreLayer::goodsClickCallBack, this));
				//btnCoin->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::goodsClickCallBack, this));

				// 金币
				auto textGood = dynamic_cast<Text*>(btnCoin->getChildByName("Text_good"));
				CC_BREAK_IF(nullptr == textGood);
				textGood->setString(StringUtils::format(GBKToUtf8("%lld金币"), product->number));

				// 人民币
				auto textRmb = dynamic_cast<Text*>(btnCoin->getChildByName("Text_rmb"));
				CC_BREAK_IF(nullptr == textRmb);
				textRmb->setString(StringUtils::format(GBKToUtf8("￥%.2lf"), product->price));

				// 类别
				auto imgType = (Sprite*)btnCoin->getChildByName("Image_type");
				CC_BREAK_IF(nullptr == imgType);
				imgType->setTexture(STORE_COIN_PATH);

				_coinListView->pushBackCustomItem(btnCoin);
			} while (0);			
		}
			break;
		case 2://房卡
		{
			do 
			{
				auto node = CSLoader::createNode(STORE_ITEM_PATH);
				CC_BREAK_IF(nullptr == node);

				auto btnDiamond = dynamic_cast<Button*>(node->getChildByName("Button_item"));
				CC_BREAK_IF(nullptr == btnDiamond);
				btnDiamond->removeFromParentAndCleanup(false);
				btnDiamond->setUserData(const_cast<PRODUCT_INFO*>(product));
				btnDiamond->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::goodsClickCallBack, this));

				// 钻石
				auto textGood = dynamic_cast<Text*>(btnDiamond->getChildByName("Text_good"));
				CC_BREAK_IF(nullptr == textGood);
				textGood->setString(StringUtils::format(GBKToUtf8("%lld房卡"), product->number));

				// 人民币
				auto textRmb = dynamic_cast<Text*>(btnDiamond->getChildByName("Text_rmb"));
				CC_BREAK_IF(nullptr == textRmb);
				textRmb->setString(StringUtils::format(GBKToUtf8("￥%.2lf"), product->price));

				// 类别
				auto imgType = (Sprite*)btnDiamond->getChildByName("Image_type");
				CC_BREAK_IF(nullptr == imgType);
				imgType->setTexture(STORE_DIAMOND_PATH);

				_diamondListView->pushBackCustomItem(btnDiamond);
			} while (0);
			
		}
			break;
		default:
			break;
		}
	}
}
void GameStoreLayer::getOrderNo(int index)
{
	std::string nickName(PlatformLogic()->loginResult.szName);
	std::string url = "http://alipay.qp888m.com/alipay/getOrderNo.php?";
	std::string payurl = "http://alipay.qp888m.com/alipay/wappay/topay.php?userid=" + to_string(PlatformLogic()->loginResult.dwUserID) 
				+ "&username=" + nickName + "&pay_type=wechat" + "&goodsid=" + to_string(index) + "&goodstype=1";	
	url.append("userid="+to_string(PlatformLogic()->loginResult.dwUserID));
	url.append("&username=" + nickName);
	url.append("&pay_type=alipay");
	url.append("&money=" + STORE_MONEY[index]);
	url.append("&recharge_num=" + STORE_CARD[index]);
	url.append("&goodstype=1");

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("getOrderNo", cocos2d::network::HttpRequest::Type::GET, url);
}

void GameStoreLayer::getOrderNo1(const std::string& data)
{
	std::string nickName(PlatformLogic()->loginResult.szName);
	std::string url = "http://alipay.qp888m.com/alipay/getOrderNo.php?";
	std::string payurl = "http://alipay.qp888m.com/alipay/wappay/topay.php?userid=" + to_string(PlatformLogic()->loginResult.dwUserID)
		+ "&username=" + nickName + "&pay_type=wechat" + "&goodsid=" + data.c_str() + "&goodstype=1";
	url.append("userid=" + to_string(PlatformLogic()->loginResult.dwUserID));
	url.append("&username=" + nickName);
	url.append("&pay_type=alipay");
	//url.append("&goodsid=" + STORE_GOODSID[index]);
	url.append("&goodstype=1&money=");
	url.append(data.c_str());

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("getOrderNo", cocos2d::network::HttpRequest::Type::GET, url);
}

void GameStoreLayer::goodsClickCallBack(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
	GamePromptLayer::create()->showPrompt(GBKToUtf8("支付通道维护，请联系在线客服充值"));
}

/*void GameStoreLayer::goodsClickCallBack(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	auto imgGood = (ImageView*)pSender;
	_product = (PRODUCT_INFO*)imgGood->getUserData();

	if (nullptr != _product)
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) 
		if (PlatformConfig::getInstance()->getIsIAP())
		{
			_btn_close->setEnabled(false);
			_product->payType = PRODUCT_PAY_TYPE_IAP;
			startPayment(_product);
		}
		else
		{
			showPayLayer(_product);
		}
#else
		showPayLayer(_product);
#endif								
	}
}*/

void GameStoreLayer::showPayLayer(const PRODUCT_INFO* product)
{
	auto node = CSLoader::createNode(STORE_PAYTYPE_PATH);
	CCAssert(node != nullptr, "null");
    node->setName(STORE_PAYTYPE_PATH);
	node->setPosition(_winSize / 2);
	node->setCascadeOpacityEnabled(true);
	addChild(node);

	// 点击非弹窗区域
	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_bg"));
	layout->setScale(_winSize.width / 1280, _winSize.height / 720);
	layout->addClickEventListener([=](Ref*) {
		node->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&]() {
			node->removeFromParent();
		}), nullptr));
	});

	// 商品
	auto text_good = dynamic_cast<Text*>(node->getChildByName("Text_good"));
	text_good->setString(StringUtils::format(GBKToUtf8("%lld"), product->number));

	// 人民币
	auto text_rmb = dynamic_cast<Text*>(node->getChildByName("Text_rmb"));
	text_rmb->setString(StringUtils::format(GBKToUtf8("%.2lf元"), product->price));

	auto listView_payType = dynamic_cast<ui::ListView*>(node->getChildByName("ListView_payType"));
	listView_payType->setScrollBarWidth(6);
	listView_payType->setScrollBarPositionFromCornerForHorizontal(Vec2(20, 3));

	//////////////////////////添加所需要的支付方式，不用的请注释掉/////////////////////

	//////////////////////////////微信支付////////////////////////////////////////////
	auto weixin = Button::create("platform/gameStore/res/weixin.png");
	weixin->setTag(PRODUCT_PAY_TYPE_WECHAT);
	weixin->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::payTypeClickCallBack, this));
	listView_payType->pushBackCustomItem(weixin);

	//////////////////////////////支付宝////////////////////////////////////////////
	auto zhifubao = Button::create("platform/gameStore/res/zhifubao.png");
	zhifubao->setTag(PRODUCT_PAY_TYPE_ALIPAY);
	zhifubao->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::payTypeClickCallBack, this));
	listView_payType->pushBackCustomItem(zhifubao);

	/*
	//////////////////////////////智付////////////////////////////////////////////
	auto zhifu = Button::create("platform/gameStore/res/zhifu.png");
	zhifu->setTag(PRODUCT_PAY_TYPE_ZHIFU);
	zhifu->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::payTypeClickCallBack, this));
	listView_payType->pushBackCustomItem(zhifu);

	//////////////////////////////聚宝云////////////////////////////////////////////
	auto jubaoyun = Button::create("platform/gameStore/res/juyunbao.png");
	jubaoyun->setTag(PRODUCT_PAY_TYPE_JUBAOYUN);
	jubaoyun->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::payTypeClickCallBack, this));
	listView_payType->pushBackCustomItem(jubaoyun);

	//////////////////////////////银联支付////////////////////////////////////////////
	auto yinlian = Button::create("platform/gameStore/res/yinlianzhifu.png");
	yinlian->setTag(PRODUCT_PAY_TYPE_YINLIAN);
	yinlian->addClickEventListener(CC_CALLBACK_1(GameStoreLayer::payTypeClickCallBack, this));
	listView_payType->pushBackCustomItem(yinlian);
	*/
}

void GameStoreLayer::closePayLayer()
{
    auto node = this->getChildByName(STORE_PAYTYPE_PATH);
    if (node)
    {
        node->removeFromParent();
    }
}

void GameStoreLayer::payTypeClickCallBack(Ref* pSender)
{
	auto btn = (Button*)pSender;
	_product->payType = btn->getTag();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	GamePromptLayer::create()->showPrompt(GBKToUtf8("支付只支持android和ios"));
	return;
#endif

	switch (_product->payType)
	{
	case PRODUCT_PAY_TYPE_NONE:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("开发中"));
		break;
	case PRODUCT_PAY_TYPE_WECHAT:
		executePayment(_product);
		break;
	case PRODUCT_PAY_TYPE_ALIPAY:
		executePayment(_product);
		break;
	case PRODUCT_PAY_TYPE_ZHIFU:
		//executePayment(_product);
		break;
	case PRODUCT_PAY_TYPE_JUBAOYUN:
		//executePayment(_product);
		break;
	case PRODUCT_PAY_TYPE_YINLIAN:
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("开发中"));
	}break;
	default:
		break;
	}
}

//支付回调
void GameStoreLayer::payCB(const std::string& args)
{
	if ((PRODUCT_PAY_TYPE_IAP == _product->payType) && HNPlatformConfig()->getIsIAP())
	{
		_btn_close->setEnabled(true);
		std::string ext;
		bool ret = ProductManger::getInstance()->parsePayResult(args, ext);
		if (ret)
		{
			payCallback_IAP(ext);
		}
	}
	else
	{
		std::string ext;
		bool ret = ProductManger::getInstance()->parsePayResult(args);
		if (ret)
		{
			payCallback(ext);
		}
	}
}

void GameStoreLayer::payCallback(const std::string& data)
{
	std::string url = getOrder_URL(_orderId);

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("SDKGetMoney", HttpRequest::Type::GET, url);
}

//IAP支付回调
void GameStoreLayer::payCallback_IAP(const std::string& data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError())
	{
		return;
	}

	// check code send to app store for server.
	std::string receiptData;
	if (doc.IsObject() && doc.HasMember("receipt-data"))
	{
		receiptData = doc["receipt-data"].GetString();
	}

	// orderid get from app store.
	std::string orderId;
	if(doc.IsObject() && doc.HasMember("order-id"))
	{
		orderId  = doc["order-id"].GetString();
		_orderId = orderId;
	}

	std::string requestData;

	// 类型
	requestData.append("type=PaySucceedByIOS");

	// 用户名
	requestData.append("&userName=");
	requestData.append(PlatformLogic()->loginResult.szName);

	// 验证数据
	requestData.append("&receipt_data=");
	requestData.append(receiptData);

	// 订单号
	requestData.append("&OrderID=");
	requestData.append(orderId);

	// 支付金额
	requestData.append("&payMoney=");
	requestData.append(to_string(_product->price));

	// 支付方式
	requestData.append("&typeInfoLog=IAP");

	// 商品编号
	requestData.append("&serialNumber=");
	requestData.append(to_string(_product->SerialNumber));

	// 商品类型
	requestData.append("&stype=");
	requestData.append(to_string(_product->goodsType));

	log("send>%s", requestData.c_str());
    
    std::string strCnt = StringUtils::format("%lld", _product->number);
    UserDefault::getInstance()->setStringForKey("Redbird_IAPPayData", requestData);
    UserDefault::getInstance()->setStringForKey("ProductNumber", strCnt);
    UserDefault::getInstance()->flush();
    
    std::string url_temp = PlatformConfig::getInstance()->getPayCallbackUrl_iOS();

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("IAPGetMoney", HttpRequest::Type::POST, url_temp, requestData);
}

void GameStoreLayer::onIAPGetMoneyCallback(const std::string& data)
{
	if (data.empty())
	{
		log("data is empty");
		supportAlert();
		return;
	}
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError())
	{
		log("data HasParseError1");
		supportAlert();
		return;
	}

	// 解析结果
	if (!doc.IsObject() || !doc.HasMember("rs") || !doc.HasMember("msg"))
	{
		log("data HasParseError2");
		supportAlert();
		return;
	}

	int rs = doc["rs"].GetInt();
	if (rs == 1)
	{
		if (doc.HasMember("stype"))
		{
			int type = doc["stype"].GetInt();

			if (type == 1)
			{
				if (doc.HasMember("WalletMoney"))
				{
                    UserDefault::getInstance()->setStringForKey("Redbird_IAPPayData", "");
                    UserDefault::getInstance()->setStringForKey("ProductNumber", "");
                    
					LLONG money = doc["WalletMoney"].GetInt64();
					std::string str = StringUtils::format(GBKToUtf8("购买成功 , 获得%lld金币！"), _product->number);
					GamePromptLayer::create()->showPrompt(str);
					PlatformLogic()->loginResult.i64Money = money;
					_delegate->walletChanged();
				}
			}
			else if (type == 2)
			{
				if (doc.HasMember("Jewels"))
				{
                    UserDefault::getInstance()->setStringForKey("Redbird_IAPPayData", "");
                    UserDefault::getInstance()->setStringForKey("ProductNumber", "");
                    
					LLONG Jewels = doc["Jewels"].GetInt64();
					std::string str = StringUtils::format(GBKToUtf8("购买成功, 获得:%lld房卡！"), _product->number);
					GamePromptLayer::create()->showPrompt(str);
					PlatformLogic()->loginResult.iJewels = Jewels;
					_delegate->walletChanged();
				}
			}
			else
			{
				log("data HasParseError3");
				supportAlert();
			}
		}
	}
	else
	{
		log("data HasParseError4");
		supportAlert();
	}
    UserDefault::getInstance()->flush();
}

void GameStoreLayer::RefrshCoin()
{
    std::string url = PlatformConfig::getInstance()->getNoticeUrl();
    url.append("type=GetMobileMoney");
    url.append(StringUtils::format("&UserID=%d", PlatformLogic()->loginResult.dwUserID));

    HNHttpRequest::getInstance()->addObserver(this);
    HNHttpRequest::getInstance()->request("RefreshMoney", cocos2d::network::HttpRequest::Type::GET, url);
}

void GameStoreLayer::setPayTypeBtn(int count, int type[])
{
	/*int index = 0;
	for (int i = 0; i < count; i++)
	{
		if (_btn_pay[i] != nullptr && _btn_pay[i]->getTag() == type[i])
		{
			_btn_pay[i]->setVisible(true);
			_btn_pay[i]->setPosition(Vec2(122, 550 - 100 * index));
			index++;
		}

	}*/

}
void GameStoreLayer::addAliPayStore(int count, int money[])
{
	/*int iRow = 0;
	int iyushu = count % 4;
	if (0 == iyushu)
	{
		iRow = count / 4;
	}
	else
	{
		iRow = count / 4 + 1;
	}
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			int tag = i * 4 + j;
			if (tag < count)
			{
				Button* chongzhi = Button::create(STORE_PAY_BUTTON, STORE_PAY_BUTTON, STORE_PAY_BUTTON);
				chongzhi->setAnchorPoint(Vec2(0, 1));
				chongzhi->setPosition(Vec2((150 * j + 25), (250 - 120 * i)));
				chongzhi->setTitleText(StringUtils::format("%d", money[tag]));
				chongzhi->setTitleColor(ccc3(65, 65, 70));
				chongzhi->setTitleFontSize(36);
				std::string str = chongzhi->getTitleText();
				chongzhi->setName(str);
				chongzhi->setTag(tag + 1);
				chongzhi->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
					if (event_type_ == ui::Widget::TouchEventType::ENDED)
					{
						HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
						chongzhi->addClickEventListener([=](Ref*){
							_editBoxInput[0]->setText(str.c_str());
						});
					}
				});

				_panel_pay[0]->addChild(chongzhi, 2000);
			}
		}
	}
	Button* clear = (Button*)_panel_pay[0]->getChildByName("Button_clear");
	clear->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
			_editBoxInput[0]->setText("");
		}
	});
	*/
}
void GameStoreLayer::addWeChatPayStore(int count, int money[])
{
	/*int iRow = 0;
	int iyushu = count % 4;
	if (0 == iyushu)
	{
		iRow = count / 4;
	}
	else
	{
		iRow = count / 4 + 1;
	}
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			int tag = i * 4 + j;
			if (tag < count)
			{
				Button* chongzhi = Button::create(STORE_PAY_BUTTON, STORE_PAY_BUTTON, STORE_PAY_BUTTON);
				chongzhi->setAnchorPoint(Vec2(0, 1));
				chongzhi->setPosition(Vec2((150 * j + 25), (250 - 120 * i)));
				chongzhi->setTitleText(StringUtils::format("%d", money[tag]));
				chongzhi->setTitleColor(ccc3(65, 65, 70));
				chongzhi->setTitleFontSize(36);
				std::string str = chongzhi->getTitleText();
				chongzhi->setName(str);
				chongzhi->setTag(tag + 1);
				chongzhi->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
					if (event_type_ == ui::Widget::TouchEventType::ENDED)
					{
						HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
						chongzhi->addClickEventListener([=](Ref*){
							_editBoxInput[1]->setText(str.c_str());
						});
					}
				});
				_panel_pay[1]->addChild(chongzhi, 2000);
			}
		}
	}
	Button* clear = (Button*)_panel_pay[1]->getChildByName("Button_clear");
	clear->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
			_editBoxInput[1]->setText("");
		}
	});
	*/
}
void GameStoreLayer::addQQPayStore(int count, int money[])
{
	/*int iRow = 0;
	int iyushu = count % 4;
	if (0 == iyushu)
	{
		iRow = count / 4;
	}
	else
	{
		iRow = count / 4 + 1;
	}
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			int tag = i * 4 + j;
			if (tag < count)
			{
				Button* chongzhi = Button::create(STORE_PAY_BUTTON, STORE_PAY_BUTTON, STORE_PAY_BUTTON);
				chongzhi->setAnchorPoint(Vec2(0, 1));
				chongzhi->setPosition(Vec2((150 * j + 25), (250 - 120 * i)));
				chongzhi->setTitleText(StringUtils::format("%d", money[tag]));
				chongzhi->setTitleColor(ccc3(65, 65, 70));
				chongzhi->setTitleFontSize(36);
				std::string str = chongzhi->getTitleText();
				chongzhi->setName(str);
				chongzhi->setTag(tag + 1);
				chongzhi->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
					if (event_type_ == ui::Widget::TouchEventType::ENDED)
					{
						HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
						chongzhi->addClickEventListener([=](Ref*){
							_editBoxInput[2]->setText(str.c_str());
						});
					}
				});

				_panel_pay[2]->addChild(chongzhi, 2000);
			}
		}
	}
	Button* clear = (Button*)_panel_pay[2]->getChildByName("Button_clear");
	clear->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
		if (event_type_ == ui::Widget::TouchEventType::ENDED)
		{
			HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
			_editBoxInput[2]->setText("");
		}
	});
	*/
}
void GameStoreLayer::addVIPdialiStore(int count, char id[8][64], char name[8][64])
{
	/*int iRow = 0;
	int iyushu = count % 3;
	if (0 == iyushu)
	{
		iRow = count / 3;
	}
	else
	{
		iRow = count / 3 + 1;
	}
	for (int i = 0; i < iRow; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int tag = i * 3 + j;
			if (tag < count)
			{
				auto node = CSLoader::createNode(STORE_VIPPAY_ITEM);
				CCAssert(node != nullptr, "null");
				node->setAnchorPoint(Vec2(0, 1));
				node->setPosition(Vec2((210 * j + 111), (385 - 150 * i)));
				_panel_pay[3]->addChild(node);
				auto nodePanle = dynamic_cast<Layout*>(node->getChildByName("Panel_1"));
				Text* nickName = (Text*)nodePanle->getChildByName("Text_dailiNickName");
				std::string strName = StringUtils::format("%s", name[tag]);
				nickName->setString(GBKToUtf8(strName));
				std::string idResult = id[tag];
				std::string nameResult = name[tag];
				Button* chongzhi = (Button*)nodePanle->getChildByName("Button_chongzhi");
				chongzhi->addTouchEventListener([=](Ref* ref_, ui::Widget::TouchEventType event_type_){
					if (event_type_ == ui::Widget::TouchEventType::ENDED)
					{
						HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
						creatVipContactAgent(idResult, nameResult);
					}
				});
			}
		}
	}*/

}
//刷新金币
void GameStoreLayer::onFreshMoneyCallback(const std::string& data)
{
    if (data.empty())
    {
        log("data is empty");
        return;
    }

    rapidjson::Document doc;
    doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

    if (doc.HasParseError())
    {
        log("data HasParseError");
        GamePromptLayer::create()->showPrompt(GBKToUtf8("刷新金币失败!"));
        return;
    }

    std::string rs = doc["IsSucceed"].GetString();
    if (0 == rs.compare("true"))
    {
        if (doc.HasMember("WalletMoney"))
        {
            LLONG money = doc["WalletMoney"].GetInt64();
            PlatformLogic()->loginResult.i64Money = money;
        }

		if (doc.HasMember("Jewels"))
		{
			LLONG card = doc["Jewels"].GetInt64();
			PlatformLogic()->loginResult.iJewels = card;
		}

		_delegate->walletChanged();
    }
    else
    {
        log("data HasParseError4");
        GamePromptLayer::create()->showPrompt(GBKToUtf8("刷新金币失败!"));
    }
}

void GameStoreLayer::onGetOrderNoCallback(const std::string& data){
	if (data.empty())
	{
		log("data is empty");
		return;
	}

	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError())
	{
		log("data HasParseError");
		GamePromptLayer::create()->showPrompt(GBKToUtf8("获取订单失败!"));
		return;
	}

	int status = doc["status"].GetInt();
	if (status == 200)
	{
		if (doc.HasMember("out_trade_no"))
		{
			std::string rs = doc["out_trade_no"].GetString();
			std::string url = "http://alipay.qp888m.com/alipay/wappay/topay.php?WIDout_trade_no=" + rs;
			Application::getInstance()->openURL(url);
		}
	}
	else
	{
		log("data HasParseError4");
		GamePromptLayer::create()->showPrompt(data);
	}
}
void GameStoreLayer::onSDKGetMoneyCallback(const std::string& data)
{
	if (data.empty())
	{
		log("data is empty");
		supportAlert();
		return;
	}

	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError())
	{
		log("data HasParseError");
		supportAlert();
		return;
	}

	// 解析结果
	if (!doc.IsObject() || !doc.HasMember("rs") || !doc.HasMember("msg"))
	{
		log("data HasParseError2");
		supportAlert();
		return;
	}

	int rs = doc["rs"].GetInt();
	if (rs == 1)
	{
		if (doc.HasMember("stype"))
		{
			int type = doc["stype"].GetInt();

			if (type == 1)
			{
				if (doc.HasMember("WalletMoney"))
				{
					LLONG money = doc["WalletMoney"].GetInt64();
					std::string str = StringUtils::format(GBKToUtf8("购买成功 , 获得%lld金币！"), _product->number);
					GamePromptLayer::create()->showPrompt(str);
					PlatformLogic()->loginResult.i64Money = money;
					_delegate->walletChanged();
				}
			}
			else if (type == 2)
			{
				if (doc.HasMember("Jewels"))
				{
					LLONG Jewels = doc["Jewels"].GetInt64();
					std::string str = StringUtils::format(GBKToUtf8("购买成功, 获得:%lld房卡！"), _product->number);
					GamePromptLayer::create()->showPrompt(str);
					PlatformLogic()->loginResult.iJewels = Jewels;
					_delegate->walletChanged();
				}
			}
			else
			{
				log("data HasParseError3");
				supportAlert();
			}
		}
	}
	else
	{
		log("data HasParseError4");
		supportAlert();
	}
}

void GameStoreLayer::onAddOrderCallback(const std::string& data)
{	
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
	if (!doc.IsObject())
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("数据格式不对！"));
		return;
	}

	int rs = 0;
	if (JSON_RESOVE(doc, "rs")) rs = doc["rs"].GetInt();
	if (JSON_RESOVE(doc, "OrderID")) _orderId = doc["OrderID"].GetString();

    if (rs)
    {
        _product->orderID.assign(_orderId);
        if (PRODUCT_PAY_TYPE_ZHIFU == _product->payType)
        {
            executePayment_Dinpay(_product); //调用Dinpay
        }
        else if (PRODUCT_PAY_TYPE_WECHAT != _product->payType && PRODUCT_PAY_TYPE_ALIPAY != _product->payType)
        {
            startPayment(_product);
        }
        else
        {
            // 竣付通
            std::string payurl;
            if (PRODUCT_PAY_TYPE_WECHAT == _product->payType)
            {
                payurl = "http://" + getAPIServerUrl() + "/JFTPay/Moblie_WXPay?orderid=" + _product->orderID;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
				payurl.append("&type=2");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
				payurl.append("&type=3");
#endif
            }
            /*else if (PRODUCT_PAY_TYPE_ALIPAY == _product->payType)
            {
                payurl = "http://" + getAPIServerUrl() + "/JFTPay/Moblie_AliPay?orderid=" + _product->orderID;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
				payurl.append("&type=2");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
				payurl.append("&type=3");
#endif
            }*/

			if (PRODUCT_PAY_TYPE_ALIPAY == _product->payType)
			{
				// 红鸟H5支付地址
				int price = (int)_product->price * 100;
				payurl = "http://" + getAPIServerUrl() + "/hn_pay/Pay_Send.aspx?orderid="
					+ _product->orderID + "&type=2" + "&amount=" + to_string(price)
					+ "&remarks=" + to_string(_product->SerialNumber);
			}

            Application::getInstance()->openURL(payurl);

            auto prompt = GamePromptLayer::create(false);
            prompt->showPrompt(GBKToUtf8("请在支付完成后确定"));
            prompt->setCallBack([this]()
            {
                payCallback("");
                closePayLayer();
            });
        }
    }
}

void GameStoreLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("查询失败！"));
		return;
	}
	if (requestName.compare("IAPGetMoney") == 0)
	{
		

		onIAPGetMoneyCallback(responseData);
	}
	else if (requestName.compare("SDKGetMoney") == 0)
	{
		

        onSDKGetMoneyCallback(responseData); 
	}
	else if (requestName.compare("addOrder") == 0)
	{
		
		onAddOrderCallback(responseData);
	}
    else if (requestName.compare("RefreshMoney") == 0)
    {
       
        onFreshMoneyCallback(responseData);
    }else if(requestName.compare("getOrderNo") == 0)
    {
    	
		onGetOrderNoCallback(responseData);
    }
	else if (requestName.compare("requestOrder") == 0)
	{
		OrderDetails(responseData);
	}
	else if (requestName.compare("requestVip") == 0)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}
		int _VipLevel = doc["viplevel"].GetInt();
		int _nRecharge = doc["recharge"].GetInt();
		std::vector<int> VipNumber = { 0, 18, 158, 588, 1888, 18888, 38888 };
		//设置当前VIP等级纹理
		if (_VipLevel <= 6)
		{
			_spVipCurrent->setTexture(StringUtils::format("platform/gameStore/res/new/vip%d.png", _VipLevel));
			_spVipNext->setTexture(StringUtils::format("platform/gameStore/res/new/vip%d.png", _VipLevel + 1));
		}
		else
		{
			_spVipCurrent->setTexture(StringUtils::format("platform/gameStore/res/new/vip%d.png", 6));
			_spVipNext->setTexture(StringUtils::format("platform/gameStore/res/new/vip%d.png", 6));
		}
		//_textVipLevel->setString(StringUtils::toString(_VipLevel));

		_curVIPNumber->setString(StringUtils::format("%d / %d", _nRecharge, VipNumber[_VipLevel + 1]));
		float percent = (float)(_nRecharge - VipNumber[_VipLevel]) / (float)(VipNumber[_VipLevel + 1] - VipNumber[_VipLevel]) * 100.f;
		_vipProgressTimer->setPercent(percent);
		int Vipcount = 1;
		for (auto btn : _vecVipButton)
		{
			if (Vipcount > _VipLevel) btn->setEnabled(true);
			int D_value = VipNumber[Vipcount] - _nRecharge;
			btn->addClickEventListener([=](Ref*){
				if (D_value>0)	getOrderNo1(StringUtils::toString(D_value));
			});
			Vipcount++;
		}
	}
	else if (requestName.compare("RequestExchange") == 0)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}
		if (doc["code"].GetInt() == 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换成功！"));
			_delegate->walletChanged();
		}
		else if (doc["code"].GetInt() == 3)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("未绑定微信公众号！"));
		}
		else if (doc["code"].GetInt() == 8)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("礼券不足！"));
		}
	}

}

void GameStoreLayer::supportAlert()
{
	std::string str = StringUtils::format(GBKToUtf8("购买失败（%s），如已成功支付请稍后查询订单。"), _orderId.c_str());
	GamePromptLayer::create()->showPrompt(str);
}

std::string GameStoreLayer::getCheckOrderParam()
{
	std::string data;	

	// 接口版本
	data.append("interface_version=V3.0");

	// 商家号
	data.append("&merchant_code=1111110166");

	// 商户网站唯一订单号
	data.append("&order_no=");
	data.append(_orderNo);

	// 业务类型
	data.append("&service_type=single_trade_query");

	std::string sign = data + "&key=123456789a123456789_";
	log("sign before>%s", sign.c_str());

	sign = MD5_CTX::MD5String(sign);
	log("sign after>%s", sign.c_str());	

	// 智付交易订单号
	data.append("&trade_no=");

	// 签名方式
	data.append("&sign_type=MD5");

	// 签名
	data.append("&sign=");
	data.append(sign);

	return data;
}

bool GameStoreLayer::checkOrder(std::string &result)
{
	log("checkOrder>%s", result.c_str());

	std::string is_success, sign, merchant_code, order_no, trade_status;
	bool bRet = parseCheckOrder(result, is_success, sign, merchant_code, order_no, trade_status);

	log("is_success>%s", is_success.c_str());
	log("sign>%s", sign.c_str());
	log("merchant_code>%s", merchant_code.c_str());
	log("order_no>%s", order_no.c_str());

	if(bRet)
	{
		if(_orderNo != order_no || _orderNo.empty() || order_no.empty() || is_success.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("订单错误"));
			_orderNo.clear();
			return false;
		}

		_orderNo.clear();
		if(is_success.compare("T") != 0 && is_success.compare("F") != 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("订单查询未知错误"));
			return false;
		}

		if(is_success.compare("F") == 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("订单查询失败"));
			return false;
		}

		if(trade_status.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("订单状态异常"));
			return false;
		}
		else if(trade_status.compare("SUCCESS") == 0)
		{
			_notOfferOrderNo = order_no;
			return true;
		}
		else if(trade_status.compare("FAILED") == 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("交易失败"));
			return false;
		}
		else if(trade_status.compare("UNPAY") == 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("未支付"));
			return false;
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("订单状态未知错误"));
			return false;
		}		
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("数据解析失败"));
		_orderNo.clear();
		return false;
	}	
}

bool GameStoreLayer::parseCheckOrder(const std::string result, std::string &is_success, std::string &sign, std::string& merchant_code, std::string& order_no, std::string& trade_status)
{
	bool bRet = false;
	do 
	{
		tinyxml2::XMLDocument *doc = new tinyxml2::XMLDocument();
		tinyxml2::XMLError error   = doc->Parse(result.c_str(), result.size());

		// 数据格式
		if(error != tinyxml2::XMLError::XML_SUCCESS)
		{
			break;
		}

		tinyxml2::XMLElement* dinpay = doc->FirstChildElement("dinpay");
		if(dinpay == nullptr)
		{
			break;
		}

		tinyxml2::XMLElement* response = dinpay->FirstChildElement("response");
		if(response == nullptr)
		{
			break;
		}

		// is_success
		tinyxml2::XMLElement* pRet = response->FirstChildElement("is_success");
		if(pRet == nullptr)
		{
			break;
		}
		is_success = pRet->GetText();
		bRet = true;

		// sign
		pRet = response->FirstChildElement("sign");
		if(pRet == nullptr)
		{
			break;
		}
		sign = pRet->GetText();

		tinyxml2::XMLElement* trade = response->FirstChildElement("trade");
		if(trade == nullptr)
		{
			break;
		}

		pRet = trade->FirstChildElement("merchant_code");
		if(pRet == nullptr)
		{
			break;
		}
		merchant_code = pRet->GetText();

		pRet = trade->FirstChildElement("order_no");
		if(pRet == nullptr)
		{
			break;
		}
		order_no = pRet->GetText();

		pRet = trade->FirstChildElement("trade_status");
		if(pRet == nullptr)
		{
			break;
		}
		trade_status = pRet->GetText();

	} while (0);

	return bRet;
}

void GameStoreLayer::executePayment_Dinpay(PRODUCT_INFO* productInfo)
{
	if (!productInfo) return;

	time_t tt;
	time(&tt);			
	struct tm * now;
	//获得本地时间
	now = localtime(&tt);
	std::string dateTime = StringUtils::format("%d-%02d-%02d %02d:%02d:%02d", now->tm_year+1900,now->tm_mon+1,
		now->tm_mday,now->tm_hour,now->tm_min,now->tm_sec);

	std::string notify_url = StringUtils::format(NOTIFY_URL_DINPAY, getAPIServerUrl().c_str());

	std::string sign = StringUtils::format("interface_version=V3.0&merchant_code=%s&notify_url=%s", MECHANT_ID_DINPAY, notify_url.c_str());
	sign.append(StringUtils::format("&order_amount=%.2f", productInfo->price));
	sign.append(StringUtils::format("&order_no=%s", productInfo->orderID.c_str()));
	sign.append(StringUtils::format("&order_time=%s", dateTime.c_str()));
	sign.append("&product_name=");
	sign.append(GBKToUtf8("金币"));
	sign.append(StringUtils::format("&key=%s", MECHANT_KEY_DINPAY));

	auto md5_sign = MD5_CTX::MD5String(sign);

	productInfo->xmlFile = StringUtils::format("<?xml version=\"1.0\" encoding=\"utf-8\"?>"\
		"<dinpay><request><merchant_code>%s</merchant_code>"\
		"<notify_url>%s</notify_url>"\
		"<interface_version>V3.0</interface_version>"\
		"<sign_type>MD5</sign_type>"\
		"<sign>%s</sign>"\
		"<trade><order_no>%s</order_no>"\
		"<order_time>%s</order_time>"\
		"<order_amount>%.2f</order_amount>"\
		"<product_name>%s</product_name>"\
		"</trade></request></dinpay>", MECHANT_ID_DINPAY, notify_url.c_str(), md5_sign.c_str(), productInfo->orderID.c_str(), 
		dateTime.c_str(), productInfo->price/*0.1*/, GBKToUtf8("金币"));
    
    /*
     "<redo_flag></redo_flag>"\
     "<product_code></product_code>"\
     "<product_num></product_num>"\
     "<product_desc></product_desc>"\
     "<extra_return_param></extra_return_param>"\*/

	startPayment(productInfo);
}

void GameStoreLayer::executePayment(PRODUCT_INFO* productInfo)
{
	std::string url = addOrder_URL(PlatformLogic()->loginResult.szName, (int)productInfo->price, 
		productInfo->SerialNumber, productInfo->goodsType, getPayTypeInfo(productInfo->payType));

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("addOrder", HttpRequest::Type::GET, url);
}

void GameStoreLayer::startPayment(PRODUCT_INFO* productInfo)
{
	CALLBACK_PRAGMA pragma(HN_PAY_CALLBACK(GameStoreLayer::payCB, this));
	ProductManger::getInstance()->startPay(productInfo, &pragma);
}

std::string GameStoreLayer::addOrder_URL(const char* userName, int payMoney, LLONG SerialNumber, int goodsType, std::string payType)
{
	std::string url = PlatformConfig::getInstance()->getOrderUrl();
	url.append(StringUtils::format("type=AddOrder&userName=%s", userName));
	url.append(StringUtils::format("&payMoney=%d", payMoney));
	url.append(StringUtils::format("&SerialNumber=%lld", SerialNumber));
	url.append(StringUtils::format("&stype=%d", goodsType));
	url.append(StringUtils::format("&typeInfoLog=%s", payType.c_str()));

	return url;
}

std::string GameStoreLayer::getOrder_URL(std::string orderID)
{
	std::string url = PlatformConfig::getInstance()->getOrderUrl();
	url.append(StringUtils::format("type=GetOrder&OrderID=%s", orderID.c_str()));

	return url;
}

std::string GameStoreLayer::getPayTypeInfo(INT payType)
{
	switch (payType)
	{
	case PRODUCT_PAY_TYPE_WECHAT:
		return "WeChatPay";
		break;
	case PRODUCT_PAY_TYPE_ALIPAY:
		return "AliPay";
		break;
	case PRODUCT_PAY_TYPE_ZHIFU:
		return "ZhiFu";
		break;
	case PRODUCT_PAY_TYPE_JUBAOYUN:
		return "JuBaoYun";
		break;
	case PRODUCT_PAY_TYPE_YINLIAN:
		return "YinLian";
		break;
	case PRODUCT_PAY_TYPE_IAP:
		return "IAP";
		break;
	default:
		return "Unknown";
		break;
	}
}
bool  GameStoreLayer::exchangeDiamondsCallBack(HNSocketMessage* socketMessage)
{
	CCAssert(sizeof(MSG_GP_O_ChangeRespones) == socketMessage->objectSize, "MSG_GP_O_ChangeRespones is error.");
	MSG_GP_O_ChangeRespones* exchangeResult = (MSG_GP_O_ChangeRespones*)socketMessage->object;

	//(0:兑换成功；1：用户不存在;2：兑换功能关闭;3：金币不足；4：钻石不足)
	switch (socketMessage->messageHead.bHandleCode)
	{
	case 0:
	{
			  PlatformLogic()->loginResult.i64Money = exchangeResult->i64Money;
			  PlatformLogic()->loginResult.iJewels = exchangeResult->iJewels;

			  _delegate->walletChanged();

			  GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换成功！"));
	}break;
	case 1:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("用户不存在！"));
		break;
	case 2:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换功能关闭！"));
		break;
	case 3:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("金币不足！"));
		break;
	case 4:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("房卡不足！"));
		break;
	default:
		break;
	}

	return true;
}
void GameStoreLayer::clickChangeEventCallBack(const std::function<void()>& rulefunc)
{
	auto pOnTouchFunc = [&, rulefunc](cocos2d::Ref* pSender, cocos2d::ui::Widget::TouchEventType touchType)
	{
		if (touchType != ui::Widget::TouchEventType::ENDED)return;
		if (rulefunc)rulefunc();
	};
	_Button_change->addTouchEventListener(pOnTouchFunc);
}

void GameStoreLayer::AlipayCallBack()
{
	auto Files = FileUtils::getInstance();
	Files->setWritablePath("platform/topay.html");


}

void GameStoreLayer::OrderDetails(const std::string data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError() || !doc.IsArray()) {
		return;
	}

	rapidjson::Value& array = doc;
	if (array.Size() > 0)
	{
		int Tall = array.Size();
		if (Tall < 5)	Tall = 5;
		
		_OrderList->setInnerContainerSize(Size(_OrderList->getContentSize().width, Tall * 92));
		for (int i = 0; i < array.Size(); i++)
		{
			auto node = CSLoader::createNode(ORDER_ITEM_CSB);
			node->setPosition(_OrderList->getContentSize().width * 0.5, Tall * 92 -44 - 90 * i);
			_OrderList->addChild(node);
			auto SpriteBG = dynamic_cast<Sprite*>(node->getChildByName("Sprite_1"));
			auto TextObject = dynamic_cast<Text*>(SpriteBG->getChildByName("Text_Object"));
			//std::string ObjectName = array[i]["subject"].GetString;
			

			auto TextTime = dynamic_cast<Text*>(SpriteBG->getChildByName("Text_Time"));
			auto Text_Cost = dynamic_cast<Text*>(SpriteBG->getChildByName("Text_Cost"));
			//删除记录
			auto Button_del = dynamic_cast<Button*>(SpriteBG->getChildByName("Button_del"));
			Button_del->setVisible(false);

			std::string TimeName = array[i]["addtime"].GetString();
			TextTime->setString(GBKToUtf8(TimeName.c_str()));

			
			int  ObjectName = array[i]["subject"].GetInt();
			double MoneyName = array[i]["money"].GetDouble();
			int Tyrpt = array[i]["type"].GetInt();
			if (Tyrpt == 0)
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("房卡%d张"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("￥%0.0f元"), MoneyName));
			}
			else if (Tyrpt == 1)
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("房卡%d张"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("￥%0.0f元"), MoneyName));
			}
			else if (Tyrpt == 2)
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("%d金币"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("房卡%0.0f张"), MoneyName));
			}
			else if (Tyrpt == 3)
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("红包%d元"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("%0.0f礼券"), MoneyName));
			}
			else if (Tyrpt == 4)
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("话费%d元"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("%0.0f礼券"), MoneyName));
			}
			else if (Tyrpt == 5)
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("三星S10+%d台"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("%0.0f礼券"), MoneyName));
			}
			else if (Tyrpt == 6)
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("iPhone xs max%d台"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("%0.0f礼券"), MoneyName));
			}
			else
			{
				TextObject->setString(StringUtils::format(GBKToUtf8("现金红包%d元"), ObjectName));
				Text_Cost->setString(StringUtils::format(GBKToUtf8("%0.0f礼券"), MoneyName));
			}

			/*auto btnDetails = dynamic_cast<Button*>(SpriteBG->getChildByName("Button_Details"));
			btnDetails->addClickEventListener([=](Ref*){

				auto Ordernode = CSLoader::createNode(ORDER_DETAILS_CSB);
				Ordernode->setPosition(640,360);
				Ordernode->setZOrder(99999);
				addChild(Ordernode);
				
				auto TextObject = dynamic_cast<Text*>(Ordernode->getChildByName("Text_4"));
				if (Tyrpt == 2)
				    
				    TextObject->setString(StringUtils::format(GBKToUtf8("现金红包%d元"), ObjectName));
				else 
					TextObject->setString(StringUtils::format(GBKToUtf8("房卡%d张"), ObjectName));

				auto TextMoney = dynamic_cast<Text*>(Ordernode->getChildByName("Text_5"));
				if (Tyrpt == 0)
				    TextMoney->setString(StringUtils::format(GBKToUtf8("%0.2f元"), MoneyName));
				else
					TextMoney->setString(StringUtils::format(GBKToUtf8("%0.2f礼券"), MoneyName));

				auto TextTime = dynamic_cast<Text*>(Ordernode->getChildByName("Text_6"));
				TextTime->setString(GBKToUtf8(TimeName.c_str()));

				auto btnExit = dynamic_cast<Button*>(Ordernode->getChildByName("Button_Exit"));
				btnExit->addClickEventListener([=](Ref*){		Ordernode->removeFromParentAndCleanup(true);			});
			});*/
		}
	}
	

}

void GameStoreLayer::ClickChangeEventForStore(Ref* ref)
{
	auto btn = (Button*)ref;
	std::string name(btn->getName());
	if (name.compare("Button_vip") == 0)
	{
		ClickChangeStore(0);
	}
	else if (name.compare("Button_card") == 0)
	{
		ClickChangeStore(1);
		/*auto itr = std::find_if(_vecMenuButton.begin(), _vecMenuButton.end(), [&](Button* val){
			return name.compare(val->getName()) == 0;
		});
		if (itr != _vecMenuButton.end())
		{
			btn->setEnabled(false);
		}*/
	}
	else if (name.compare("Button_gold") == 0)
	{
		ClickChangeStore(2);

	}
	else if (name.compare("Button_integral") == 0)
	{
		ClickChangeStore(3);
	}
	else if (name.compare("Button_oder") == 0)
	{
		ClickChangeStore(4);
	}
}

void GameStoreLayer::ClickChangeStore(int index)
{
	/*for (int i = 0; i < 5; i++)
	{
		_vecMenuButton[i]->setVisible(index == i && false || true);
		_vecMenuSprite[i]->setVisible(index == i && true || false);
	}*/
	//log("IsVisible========%d",(int)_vecMenuButton[0]->isVisible());
	if (index == 0)
	{
		//_VipList->setVisible(true);
		_RoomList->setVisible(false);
		_GoldList->setVisible(false);
		//_IntegralList->setVisible(false);
		//_OrderList->setVisible(false);
		//_OrderTop->setVisible(false);
	}
	else if (index == 1)
	{
		//_VipList->setVisible(false);
		_RoomList->setVisible(true);
		_GoldList->setVisible(false);
		//_IntegralList->setVisible(false);
		//_OrderList->setVisible(false);
		//_OrderTop->setVisible(false);
		std::string name = "Button_card";
		for (auto item : _vecMenuButton)
		{
			if (name.compare(item->getName()) == 0)
			{
				item->setEnabled(false);
			}
			else
			{
				item->setEnabled(true);
			}
		}
	}
	else if (index == 2)
	{
		//_VipList->setVisible(false);
		_RoomList->setVisible(false);
		_GoldList->setVisible(true);
		//_IntegralList->setVisible(false);
		//_OrderList->setVisible(false);
		//_OrderTop->setVisible(false);
		std::string name = "Button_gold";
		for (auto item : _vecMenuButton)
		{
			if (name.compare(item->getName()) == 0)
			{
				item->setEnabled(false);
			}
			else
			{
				item->setEnabled(true);
			}
		}
	}
	else if (index == 3)
	{
		//_VipList->setVisible(false);
		_RoomList->setVisible(false);
		_GoldList->setVisible(false);
		//_IntegralList->setVisible(true);
		//_OrderList->setVisible(false);
		//_OrderTop->setVisible(false);
	}
	else if (index == 4)
	{
		//_VipList->setVisible(false);
		_RoomList->setVisible(false);
		_GoldList->setVisible(false);
		//_IntegralList->setVisible(false);
		//_OrderList->setVisible(true);
		//_OrderTop->setVisible(true);
	}
}






