/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameGiftShop.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include <string>

// 翻页提示
static const char* GAME_PAGESPROMPT_PATH	= "platform/common/yuandian1.png";

// 兑换布局文件
static const char* GIFT_UI_PATH = "platform/GiftShopUi/GiftUi.csb";

// 商品项
static const char* GIFT_ITEM_PATH = "platform/GiftShopUi/GiftItem.csb";

// 记录项
static const char* RECORD_ITEM_PATH = "platform/GiftShopUi/RecordItem.csb";


GameGiftShop* GameGiftShop::createGameGiftShop(MoneyChangeNotify* delegate, LLONG lottery)
{
	GameGiftShop *pRet = new GameGiftShop(); 
	if (pRet && pRet->init(lottery)) 
	{ 
		pRet->setChangeDelegate(delegate);
		pRet->autorelease(); 
		return pRet; 
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return nullptr;
	}
}

GameGiftShop::GameGiftShop()
{
	
}

GameGiftShop::~GameGiftShop()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

void GameGiftShop::closeFunc()
{
	if (onCloseCallBack)
	{
		onCloseCallBack();
	}
}

bool GameGiftShop::init(LLONG lottery)
{
	if (!HNLayer::init()) {
		return false;
	}

	auto node = CSLoader::createNode(GIFT_UI_PATH);
	node->setPosition(_winSize / 2);
	addChild(node);

	_csNode = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78)
	{
		_csNode->setScale(0.9f);
	}

	// 金币
	char bufferCoin[64] = {0};
	auto moneyText = (Text*)_csNode->getChildByName("Image_coin")->getChildByName("Text_count");
	moneyText->setString(HNToWan(PlatformLogic()->loginResult.iJewels));

	// 奖券
	auto lotteryText = (Text*)_csNode->getChildByName("Image_jiangquan")->getChildByName("Text_count");
	lotteryText->setString(HNToWan(lottery));

	//  奖券商城
	_panel_GiftPage = dynamic_cast<Layout*>(_csNode->getChildByName("Panel_gifts"));
	_panel_GiftPage->setVisible(true);

	// 兑换记录
	_panel_Record = dynamic_cast<Layout*>(_csNode->getChildByName("Panel_jilu"));
	_panel_Record->setVisible(false);

	// 虚拟物品
	_panel_Virtual = dynamic_cast<Layout*>(_csNode->getChildByName("Panel_virtual"));

	// 实物
	_panel_Entity = dynamic_cast<Layout*>(_csNode->getChildByName("Panel_entity"));

	// 商品列表
	_pageView_Gift = dynamic_cast<PageView*>(_panel_GiftPage->getChildByName("PageView_goods"));
	_pageView_Gift->setIndicatorEnabled(true);
	_pageView_Gift->setIndicatorIndexNodesTexture(GAME_PAGESPROMPT_PATH);
	_pageView_Gift->setIndicatorIndexNodesScale(0.85f);
	_pageView_Gift->setIndicatorSelectedIndexColor(Color3B::WHITE);
	_pageView_Gift->setIndicatorPosition(Vec2(_pageView_Gift->getContentSize().width * 0.5f, _pageView_Gift->getContentSize().height * 0.04f));

	// 兑换记录列表
	_listView_Record = dynamic_cast<ListView*>(_panel_Record->getChildByName("ListView_record"));
	_listView_Record->setScrollBarWidth(6);
	_listView_Record->setScrollBarPositionFromCornerForVertical(Vec2(3, 20));

	// 礼品页面按钮
	_btnShop = dynamic_cast<Button*>(_csNode->getChildByName("Button_shangchen"));

	// 兑换记录按钮
	_btnRecord = dynamic_cast<Button*>(_csNode->getChildByName("Button_jilu"));

	_btnShop->addClickEventListener([=](Ref*){
		showPanel(PANEL_TYPE::eExchange);
	});

	_btnRecord->addClickEventListener([=](Ref*){
		_listView_Record->removeAllItems();
		// 查询兑换记录
		getRecordParams();
		showPanel(PANEL_TYPE::eRecord);
	});

	showPanel(PANEL_TYPE::eExchange);

	// 关闭按钮
	auto btn_Close = dynamic_cast<Button*>(_csNode->getChildByName("Button_close"));
	btn_Close->addClickEventListener([=](Ref*){
		close();
	});

	// 查询礼品信息
	getQueryParams();

	return true;
}

void GameGiftShop::getVirtualPage(GiftInfo* info, SpriteFrame* frame)
{
	// 商品图片
	auto giftSprite = dynamic_cast<Sprite*>(_panel_Virtual->getChildByName("Sprite_logo"));
	giftSprite->setDisplayFrame(frame);

	auto imgLogo = (ImageView*)_panel_Virtual->getChildByName("Image_logo");
	giftSprite->setScale(imgLogo->getContentSize().width / giftSprite->getContentSize().width);
	//giftSprite->setScaleY(imgLogo->getContentSize().height / giftSprite->getContentSize().height);

	// 商品名称
	auto textName = dynamic_cast<Text*>(_panel_Virtual->getChildByName("Text_name"));
	textName->setString(info->giftName);

	std::string str;
	LLONG value = info->lotteryNum;
	if (value < 10000)
	{
		str.clear();
		str.append(StringUtils::toString(value));
	}
	else if (value >= 10000 && value < 100000000)
	{
		int money = (int)value / 10000;
		str.clear();
		str.append(StringUtils::toString(money));
		str.append("万");
	}
	else
	{
		float money = (float)value / 100000000;
		char tmp[60] = {0};
		sprintf(tmp, "%.2f", money);
		str.clear();
		str.append(tmp);
		str.append("亿");
	}

	// 所需奖券
	auto textNum = dynamic_cast<Text*>(_panel_Virtual->getChildByName("Text_jiangquan"));
	textNum->setString(GBKToUtf8(str.c_str()));

	// 手机号
	auto TextField_PhoneNum = dynamic_cast<TextField*>(_panel_Virtual->getChildByName("Image_bg")->getChildByName("TextField_phone"));

	// 确定兑换
	auto btn_Ture = dynamic_cast<Button*>(_panel_Virtual->getChildByName("Button_sure"));
	btn_Ture->setUserData(info);
	btn_Ture->addClickEventListener([=](Ref*){
		std::string phoneNum = TextField_PhoneNum->getString();
		if (phoneNum.size() != 11 || !Tools::verifyNumber(phoneNum))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的11位手机号码！"));
			return;
		}
		std::string str = getExchangeParams(info->giftID, "", "", phoneNum);

		requestWithParams(str, "Exchange", PlatformConfig::getInstance()->getPrizeUrl(), HttpRequest::Type::POST);
	});

	// 返回
	auto btnFanhui = (Button*)(_panel_Virtual->getChildByName("Button_fanhui"));
	btnFanhui->addClickEventListener([=](Ref* pRef) {
		showPanel(PANEL_TYPE::eExchange);
	});
}

void GameGiftShop::getEntityPage(GiftInfo* info, SpriteFrame* frame)
{
	// 商品名称
	auto giftSprite = dynamic_cast<Sprite*>(_panel_Entity->getChildByName("Sprite_logo"));
	giftSprite->setDisplayFrame(frame);

	auto imgLogo = (ImageView*)_panel_Entity->getChildByName("Image_logo");
	giftSprite->setScale(imgLogo->getContentSize().width / giftSprite->getContentSize().width);
//	giftSprite->setScaleY(imgLogo->getContentSize().height / giftSprite->getContentSize().height);

	auto textName = dynamic_cast<Text*>(_panel_Entity->getChildByName("Text_name"));
	textName->setString(info->giftName);

	std::string str;
	LLONG value = info->lotteryNum;
	if (value < 10000)
	{
		str.clear();
		str.append(StringUtils::toString(value));
	}
	else if (value >= 10000 && value < 100000000)
	{
		int money = (int)value / 10000;
		str.clear();
		str.append(StringUtils::toString(money));
		str.append("万");
	}
	else
	{
		float money = (float)value / 100000000;
		char buff[60] = {0};
		sprintf(buff, "%.2f", money);
		str.clear();
		str.append(buff);
		str.append("亿");
	}

	// 奖券
	auto textNum = dynamic_cast<Text*>(_panel_Entity->getChildByName("Text_jiangquan"));
	textNum->setString(GBKToUtf8(str.c_str()));

	// 确认兑换
	auto btn_Ture = dynamic_cast<Button*>(_panel_Entity->getChildByName("Button_sure"));
	btn_Ture->addClickEventListener([=](Ref*){

		auto Text_Name = dynamic_cast<TextField*>(_panel_Entity->getChildByName("Image_name")->getChildByName("TextField_name"));
		auto TextField_PhoneNum = dynamic_cast<TextField*>(_panel_Entity->getChildByName("Image_phone")->getChildByName("TextField_phone"));
		auto TextField_Address = dynamic_cast<TextField*>(_panel_Entity->getChildByName("Image_addr")->getChildByName("TextField_addr"));
		auto TextField_DetailedAddress = dynamic_cast<TextField*>(_panel_Entity->getChildByName("Image_detail")->getChildByName("TextField_detail"));

		do
		{
			// 收货人姓名
			std::string name = Utf8ToGB(Text_Name->getString().c_str());
			if (name.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请填写有效的真实姓名！"));
				break;
			}

			// 联系方式
			std::string phoneNum = Utf8ToGB(TextField_PhoneNum->getString().c_str());
			if (phoneNum.size() != 11 || !Tools::verifyNumber(phoneNum))
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的11位手机号码！"));
				break;
			}

			// 省市区地址
			std::string address = Utf8ToGB(TextField_Address->getString().c_str());
			if (address.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请填写有效的省，市，区！"));
				break;
			}

			// 详细地址
			std::string detailedAddress = Utf8ToGB(TextField_DetailedAddress->getString().c_str());
			if (detailedAddress.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请填写有效的街道等详细地址！"));
				break;
			}

			// 地址组合
			std::string temp(address);
			temp.append(detailedAddress);

			// 组合发送内容
			std::string str = getExchangeParams(info->giftID, temp, name, phoneNum);

			std::string xxx(GBKToUtf8(str.c_str()));

			// 发送兑换信息
			requestWithParams(GBKToUtf8(str.c_str()), "Exchange", PlatformConfig::getInstance()->getPrizeUrl(), HttpRequest::Type::POST);
		} while (0);
	});

	// 返回
	auto btnFanhui = (Button*)(_panel_Entity->getChildByName("Button_fanhui"));
	btnFanhui->addClickEventListener([=](Ref* pRef) {
		showPanel(PANEL_TYPE::eExchange);
	});
}

void GameGiftShop::setChangeDelegate(MoneyChangeNotify* delegate)
{
	_delegate = delegate;
}

// 获取兑换记录数据
void GameGiftShop::getRecordParams()
{
	char str[64] = {0};
	sprintf(str, "action=ExchangeRecord&userid=%d", PlatformLogic()->loginResult.dwUserID);
	requestWithParams(str, "Record", PlatformConfig::getInstance()->getPrizeUrl(), HttpRequest::Type::POST);
}

// 兑换记录回应
void GameGiftShop::onHttpRecordCompleted(const std::string& data)
{
	char str[100] = {0};
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
	if (doc.HasParseError() || !doc.IsObject())	{
		return;
	}

	if (doc.HasMember("content"))
	{
		rapidjson::Value& array = doc["content"];
		if(array.IsArray())
		{
			char str[64];
			//解析兑换记录数据
			for (UINT i = 0; i < array.Size(); i++)
			{
				rapidjson::Value& arraydoc = array[i];
				do
				{
					// 兑换记录子列表背景图
					auto csNode = CSLoader::createNode(RECORD_ITEM_PATH);
					CC_BREAK_IF(nullptr == csNode);

					Layout* panel = (Layout*)csNode->getChildByName("Panel_bg");
					CC_BREAK_IF(nullptr == panel);
					panel->removeFromParentAndCleanup(false);

					// 订单号
					if(arraydoc.HasMember("OrderNo") && arraydoc["OrderNo"].IsString())
					{
						std::string orderNo = arraydoc["OrderNo"].GetString();					 	
						sprintf(str, "%s %s", orderNo.substr(0, 8).c_str(), orderNo.substr(8).c_str());
						Text* Text_orderId = (Text*)panel->getChildByName("Text_orderId");
						CC_BREAK_IF(nullptr == Text_orderId);
						Text_orderId->setString(str);
					}

					// 礼品名称
					if(arraydoc.HasMember("Award_Name") && arraydoc["Award_Name"].IsString())
					{
						std::string name = arraydoc["Award_Name"].GetString();						
						sprintf(str, "%s", name.c_str());
						Text* Text_name = (Text*)panel->getChildByName("Text_name");
						CC_BREAK_IF(nullptr == Text_name);
						Text_name->setString(str);
					}

					// 兑换时间
					if(arraydoc.HasMember("AwardTime") && arraydoc["AwardTime"].IsString())
					{
						std::string time = arraydoc["AwardTime"].GetString();		
						sprintf(str, "%s %s", time.substr(0, 10).c_str(), time.substr(10).c_str());
						Text* Text_time = (Text*)panel->getChildByName("Text_time");
						CC_BREAK_IF(nullptr == Text_time);
						Text_time->setString(str);
					}

					// 发货状态
					if(arraydoc.HasMember("IsCash") && arraydoc["IsCash"].IsBool())
					{
						bool state = arraydoc["IsCash"].GetBool();
						std::string temp = state ? "已发货" : "未发货";
						Text* Text_status = (Text*)panel->getChildByName("Text_status");
						CC_BREAK_IF(nullptr == Text_status);
						Text_status->setString(GBKToUtf8(temp));
					}

					_listView_Record->pushBackCustomItem(panel);
				} while (0);
			}
		}
	}
}

//获取查询数据
void GameGiftShop::getQueryParams()
{
	requestWithParams("action=PrizeList", "Query", PlatformConfig::getInstance()->getPrizeUrl(), HttpRequest::Type::POST);
}

// 查询请求回应
void GameGiftShop::onHttpQueryCompleted(const std::string& data)
{
	_gifts.clear();

	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
	if (doc.HasParseError() || !doc.IsObject())	{
		return;
	}

	if (doc.HasMember("content"))
	{
		rapidjson::Value& array = doc["content"];
		if(array.IsArray())
		{
			//根据商品个数创建列表页面，3个一页
			int pageCount = (array.Size() / 3);
			int remainder = (array.Size() % 3);

			int pageLen = (remainder == 0) ? pageCount : pageCount + 1;

			//创建页面
			for (int i = 0; i < pageLen; i++)
			{
				createGiftPages();
			}

			//解析商品数据
			for (UINT i = 0; i < array.Size(); i++)
			{
				rapidjson::Value& arraydoc = array[i];

				GiftInfo giftInfo;
				do
				{
					if(arraydoc.HasMember("Award_ID"))
					{
						int id = arraydoc["Award_ID"].GetInt();
						giftInfo.giftID = id;
					}

					if(arraydoc.HasMember("Award_Name"))
					{
						std::string name = arraydoc["Award_Name"].GetString();
						giftInfo.giftName = name;
					}

					if(arraydoc.HasMember("Award_Info"))
					{
						std::string info = arraydoc["Award_Info"].GetString();
						giftInfo.giftInfo = info;
					}

					if(arraydoc.HasMember("Award_Pic"))
					{
						std::string pic = arraydoc["Award_Pic"].GetString();
						giftInfo.giftPic = pic;
						char str[24];
						sprintf(str, "Award_Pic%d", i);
						requestWithParams("", str, pic, HttpRequest::Type::GET);
					}

					if(arraydoc.HasMember("Award_Num"))
					{
						int num = arraydoc["Award_Num"].GetInt();
						giftInfo.giftNum = num;
					}

					if(arraydoc.HasMember("Award_MoneyCost"))
					{
						LLONG lottery = arraydoc["Award_MoneyCost"].GetInt64();
						giftInfo.lotteryNum = lottery;
					}

					if(arraydoc.HasMember("Award_Type"))
					{
						int type = arraydoc["Award_Type"].GetInt();
						giftInfo.giftType = type;
					}

					if(arraydoc.HasMember("Award_AddDate"))
					{
						std::string date = arraydoc["Award_AddDate"].GetString();
						giftInfo.addDate = date;
					}

					_gifts.push_back(giftInfo);
				} while (0);
			}
		}
	}
}

// 创建商品页面
void GameGiftShop::createGiftPages()
{
	// 创建游戏列表子页面
	auto gameLayout = Layout::create();
	gameLayout->setName("page");
	gameLayout->setContentSize(Size(1280, 550));
	gameLayout->setCascadeOpacityEnabled(true);

	//添加子页面进入列表中
	_pageView_Gift->addPage(gameLayout);
	//拖动监听
	_pageView_Gift->addEventListener(PageView::ccPageViewCallback(CC_CALLBACK_2(GameGiftShop::pageViewMoveCallBack, this)));
}

// 下载图片回应
void GameGiftShop::onHttpDownloadPicCompleted(const std::string& tag, const std::string& data)
{
	//请求的图片顺序不一定，所以把对应的商品和图片查找配对
	char str[24] = {0};
	for (UINT i = 0; i < _gifts.size(); i++)
	{
		sprintf(str, "Award_Pic%d", i);
		if(tag.compare(str) == 0)
		{
			//一页3个商品，获取第几页
			int pageIdx = i / 3;
			auto page = _pageView_Gift->getPage(pageIdx);

			auto giftNode = CSLoader::createNode(GIFT_ITEM_PATH);
			page->addChild(giftNode);

			auto giftBtn = dynamic_cast<Button*>(giftNode->getChildByName("Button_gift"));
			giftBtn->removeFromParentAndCleanup(false);
			page->addChild(giftBtn);
			giftBtn->addTouchEventListener(CC_CALLBACK_2(GameGiftShop::btnEventCallBack, this));

			int index = i % 3;
			float posX = 1.0f / 6 * (2 * index + 1);

			giftBtn->setPosition(Vec2(page->getContentSize().width * posX, page->getContentSize().height * 0.5f));

			auto textName = dynamic_cast<Text*>(giftBtn->getChildByName("Text_name"));
			textName->setString(_gifts.at(i).giftName.c_str());

			std::string info;
			LLONG value = _gifts.at(i).lotteryNum;
			if (value < 10000)
			{
				info.clear();
				info.append(StringUtils::toString(value));
				info.append("积分");
			}
			else if (value >= 10000 && value < 100000000)
			{
				int money = (int)value / 10000;
				info.clear();
				info.append(StringUtils::toString(money));
				info.append("万积分");
			}
			else
			{
				float money = (float)value / 100000000;
				sprintf(str, "%.2f", money);
				info.clear();
				info.append(str);
				info.append("亿积分");
			}
			auto textLottery = dynamic_cast<Text*>(giftBtn->getChildByName("Text_jiangquan"));
			textLottery->setString(GBKToUtf8(info.c_str()));

////////////////////////////////////////////////////////////////////////////////////////////

			// 图片缓存，先清理之前的
			std::string frameName = StringUtils::format("gift_%d.png", i);
			SpriteFrame * spriteFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName);

			// 清理旧的缓存
			if (nullptr != spriteFrame) {
				SpriteFrameCache::getInstance()->removeSpriteFrameByName(frameName);
			}

			// 创建帧缓存
			std::vector<char> buffer;
			buffer.assign(data.begin(), data.end());

			if (buffer.empty()) {
				break;
			}

			Image* image = new Image;
			bool isOk = image->initWithImageData((BYTE*)buffer.data(), buffer.size());
			if (!isOk){
				image->release();
				break;
			}

			// 缓存
			std::string giftpath = StringUtils::format("gift_img_%d", i);

			// 清理缓存
			if (Director::getInstance()->getTextureCache()->getTextureForKey(giftpath)) {
				Director::getInstance()->getTextureCache()->removeTextureForKey(giftpath);
			}

			Texture2D* texture = Director::getInstance()->getTextureCache()->addImage(image, giftpath);
			if (!texture) {
				break;
			}

			spriteFrame = SpriteFrame::createWithTexture(texture, Rect(Vec2::ZERO, texture->getContentSize()));
			SpriteFrameCache::getInstance()->addSpriteFrame(spriteFrame, frameName);
			//texture->release();			

			// 更换商品信息图片为网络获取到的spriteFrame
			auto giftSprite = dynamic_cast<Sprite*>(giftBtn->getChildByName("Sprite_logo"));
			giftSprite->setSpriteFrame(spriteFrame);
			auto imgGift = (ImageView*)giftBtn->getChildByName("Image_logo");
			giftSprite->setScale(imgGift->getContentSize().width / giftSprite->getContentSize().width);
			//giftSprite->setScaleY(imgGift->getContentSize().height / giftSprite->getContentSize().height);

			giftBtn->setUserObject(spriteFrame);
			giftBtn->setUserData(&_gifts.at(i));

			break;
		}
	}
}

// 获取兑换请求数据
std::string GameGiftShop::getExchangeParams(int awardid, const std::string address, const std::string name, const std::string mobile)
{
	std::vector<std::string> params;

	char chars[128] = {0};
	std::string str;

	// 操作类型
	params.push_back("action=ExchangeGift");

	// 用户ID
	str = "userid=";
	sprintf(chars, "%d", PlatformLogic()->loginResult.dwUserID);
	str.append(chars);
	params.push_back(str);

	// 奖品记录ID
	str = "awardid=";
	sprintf(chars, "%d", awardid);
	str.append(chars);
	params.push_back(str);

	// 收货地址
	str = "address=";
	sprintf(chars, "%s", address.c_str());
	str.append(chars);
	params.push_back(str);

	// 收货人姓名
	str = "name=";
	sprintf(chars, "%s", name.c_str());
	str.append(chars);
	params.push_back(str);

	// 联系电话
	str = "mobile=";
	sprintf(chars, "%s", mobile.c_str());
	str.append(chars);
	params.push_back(str);

	// 信息
	params.push_back("remark=");

	std::string paramStr = signParams(params);

	return paramStr;
}

void GameGiftShop::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed)
	{
		GamePromptLayer::create()->showPrompt(responseData);
		return;
	}

	if (requestName.compare("Exchange") == 0)
	{
		onHttpExchangeGiftCompleted(responseData);
	}
	else if (requestName.compare("Record") == 0)
	{
		onHttpRecordCompleted(responseData);
	}
	else if (requestName.compare("Query") == 0)
	{
		onHttpQueryCompleted(responseData);
	}
	else
	{
		size_t pos = requestName.find("Award_Pic");
		if (pos != std::string::npos)
		{
			onHttpDownloadPicCompleted(requestName, responseData);
		}
		
	}
}

// 兑换礼品回应
void GameGiftShop::onHttpExchangeGiftCompleted(const std::string& data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
	
	if (doc.HasParseError() || !doc.IsObject())	{
		return;
	}

	do
	{
		if (doc.HasMember("status"))
		{
			int statu = doc["status"].GetInt();
			if (statu == -1)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("兑换失败，积分数量不足！"));
				break;
			}
			else if (statu == 0)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("服务器繁忙，请稍候再试！"));
				break;
			}
			else
			{					
			}
		}

		if (doc.HasMember("content"))
		{
			rapidjson::Value& array = doc["content"];
			if(array.IsArray())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("恭喜你，兑换成功！"));
				//解析兑换奖品返回数据
				for (UINT i = 0; i < array.Size(); i++)
				{
					rapidjson::Value& arraydoc = array[i];
					do
					{
						if (arraydoc.HasMember("WalletMoney"))
						{
							LLONG money = arraydoc["WalletMoney"].GetInt64();
							
							// 金币
							auto moneyText = (Text*)_csNode->getChildByName("Image_coin")->getChildByName("Text_count");
							moneyText->setString(HNToWan(money));

						}

						if (arraydoc.HasMember("Lotteries"))
						{
							LLONG lottery = arraydoc["Lotteries"].GetInt64();
							
							// 奖券
							auto lotteryText = (Text*)_csNode->getChildByName("Image_jiangquan")->getChildByName("Text_count");
							lotteryText->setString(HNToWan(lottery));
						}
					} while (0);
				}
			}
		}
	} while (0);
}

// 参数签名
std::string GameGiftShop::signParams(std::vector<std::string>& params)
{
	std::string paramStr;

	//std::sort(params.begin(), params.end());
	for(size_t i = 0; i < params.size(); i++)
	{
		if(i != 0)
		{
			paramStr += "&";
		}
		paramStr += params.at(i);
	}

	return paramStr;
}

//发送数据接口
void GameGiftShop::requestWithParams(const std::string& params, const std::string& tag, const std::string& url, HttpRequest::Type type)
{
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request(tag, type, url, params);
}

//列表滑动监听
void GameGiftShop::pageViewMoveCallBack(Ref* pSender, PageView::EventType type)
{
	if (_currentPageIdx == _pageView_Gift->getCurPageIndex()) {
		return;
	}

	_currentPageIdx = _pageView_Gift->getCurPageIndex();

	auto iter = _pagesPrompt.begin();
	for (int i = 0; iter != _pagesPrompt.end(); iter++, i++)
	{
		auto page = (Sprite*)*iter;
		page->setColor(Color3B(150, 150, 150));
		page->setScale(0.8f);
		if (i == _currentPageIdx)
		{
			page->setScale(1.0f);
			page->setColor(Color3B(255, 255, 255));
		}
	}
}

//列表按钮点击回调函数
void GameGiftShop::btnEventCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (Widget::TouchEventType::ENDED != type) {
		return;
	}
	auto btn = (Button*)pSender;

	GiftInfo* info = (GiftInfo*)btn->getUserData();
	if (!info)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("错误的商品信息！"));
		return;
	}
	
	SpriteFrame* frame = (SpriteFrame*)btn->getUserObject();

	//_btnRecord->setEnabled(false);
	_panel_GiftPage->setVisible(false);

	if (info->giftType == 1)
	{
		showPanel(PANEL_TYPE::eVirtual);
		getVirtualPage(info, frame);
	}
	else if (info->giftType == 2)
	{
		showPanel(PANEL_TYPE::eEntity);
		getEntityPage(info, frame);
	}
}

void GameGiftShop::showPanel(PANEL_TYPE type)
{
	if (PANEL_TYPE::eNone == type) {
		return;
	}

	// 面板显示控制
	_panel_Virtual->setVisible(PANEL_TYPE::eVirtual == type);
	_panel_Entity->setVisible(PANEL_TYPE::eEntity == type);	
	_panel_GiftPage->setVisible(PANEL_TYPE::eExchange == type);
	_panel_Record->setVisible(PANEL_TYPE::eRecord == type);

	// 点击按钮控制
	_btnShop->setEnabled(PANEL_TYPE::eRecord == type);
	_btnRecord->setEnabled(PANEL_TYPE::eRecord != type);

	// 点击按钮显示控制
	std::string jqsc1 = "platform/GiftShopUi/Res/jiangquanshangchen2.png";
	std::string jqsc2 = "platform/GiftShopUi/Res/jiangquanshangchen1.png";
	std::string dhjl1 = "platform/GiftShopUi/Res/duihuanjilu2.png";
	std::string dhjl2 = "platform/GiftShopUi/Res/duihuanjilu1.png";

	auto imgShop = (ImageView*)_btnShop->getChildByName("Image_name");
	auto imgRecord = (ImageView*)_btnRecord->getChildByName("Image_name");

	if (PANEL_TYPE::eRecord == type)
	{
		imgShop->loadTexture(jqsc1);
		imgRecord->loadTexture(dhjl2);
	}
	else
	{
		imgShop->loadTexture(jqsc2);
		imgRecord->loadTexture(dhjl1);
	}
}