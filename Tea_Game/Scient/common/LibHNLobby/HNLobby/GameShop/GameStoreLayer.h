/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMESTORE_LAYER_H__
#define __GAMESTORE_LAYER_H__

#include "ShopManager.h"
#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "network/HttpClient.h"



USING_NS_CC;
using namespace HN;
using namespace ui;
using namespace cocos2d;
using namespace cocostudio;
using namespace cocos2d::ui;



class GameStoreLayer : public HNLayer, public HNHttpDelegate
{
public:
	typedef std::function<void ()> CloseCallBack;
	CloseCallBack	onCloseCallBack;

public:
	static GameStoreLayer* createGameStore(MoneyChangeNotify* delegate);

	GameStoreLayer();

	virtual ~GameStoreLayer();

	virtual bool init() override;

	void setChangeDelegate(MoneyChangeNotify* delegate);

    //刷新金币回调
    void RefrshCoin();
	//设置支付方式的按钮
	void setPayTypeBtn(int count, int type[]);
	//支付宝支付
	void addAliPayStore(int count, int money[]);
	//微信支付
	void addWeChatPayStore(int count, int money[]);
	//qq支付
	void addQQPayStore(int count, int money[]);
	//加载vip代理列表
	void addVIPdialiStore(int count, char id[8][64], char name[8][64]);

	//兑换按钮回调
	void clickChangeEventCallBack(const std::function<void()>& rulefunc);

	//更新商场界面（1.VIP商场，2.房卡，3.金币，4.兑换，5订单）
	void ClickChangeEventForStore(Ref* ref);

	void ClickChangeStore(int index);

	void AlipayCallBack();
private:
	void closeFunc() override;

	// 创建商品列表
	void createGoodsList();

	// 显示支付方式选择页面
	void showPayLayer(const PRODUCT_INFO* product);

    // 关闭支付选项
    void closePayLayer();
private:
	// 商品点击回调
	void goodsClickCallBack(Ref* pSender);

	// 支付方式选择回调
	void payTypeClickCallBack(Ref* pSender);

	void getOrderNo(int index);
	void getOrderNo1(const std::string& data);
	bool exchangeDiamondsCallBack(HNSocketMessage* socketMessage);
	
private:
	void payCB(const std::string& args);
    
    // 支付成功回调
    void payCallback(const std::string& data);

	// 支付成功回调
	void payCallback_IAP(const std::string& data);
    
    // http请求响应
	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	void onSDKGetMoneyCallback(const std::string& data);
	void onIAPGetMoneyCallback(const std::string& data);
	void onAddOrderCallback(const std::string& data);
    void onFreshMoneyCallback(const std::string& data);
	void onGetOrderNoCallback(const std::string& data);
    
    // 发货失败提示
    void supportAlert();

	// 获取查询订单参数
	std::string getCheckOrderParam();

	// 订单查询接口(从智付端验证）
	bool checkOrder(std::string &result);

	// 解析订单查询返回参数
	bool parseCheckOrder(const std::string result, std::string &is_success, std::string &sign, std::string& merchant_code, std::string& order_no,
		std::string& trade_status);

	//执行支付
	void executePayment_Dinpay(PRODUCT_INFO* productInfo);
	//执行支付
	void executePayment(PRODUCT_INFO* productInfo);
	//执行支付
	void startPayment(PRODUCT_INFO* productInfo);

	std::string addOrder_URL(const char* userName, int payMoney, LLONG SerialNumber, int goodsType, std::string payType);

	std::string getOrder_URL(std::string orderID);

	std::string getPayTypeInfo(INT payType);
    
	//订单详情
	void OrderDetails(const std::string data);

private:
    // 订单号
    std::string						_orderId;
    
    PRODUCT_INFO*					_product;
	MoneyChangeNotify*				_delegate;

	// 订单号
	std::string                     _orderNo;
	// 未给金币的订单号
	std::string                     _notOfferOrderNo;

	Button*							_btn_close		= nullptr;
	Button*							_OldButton = nullptr;
	ui::ListView*					_diamondListView	= nullptr;
	//ui::TextAtlas*					_textVipLevel = nullptr;
	Sprite*						_spVipCurrent = nullptr;
	Sprite*						_spVipNext = nullptr;
	ui::ListView*					_coinListView	= nullptr;
	ui::Text*						_curVIPNumber = nullptr;
	ui::LoadingBar*					_vipProgressTimer = nullptr;
	Node*							_VipList = nullptr;
	Node*							_RoomList = nullptr;
	Node*							_GoldList = nullptr;
	Node*							_IntegralList = nullptr;
	ImageView*                      _diamondString = nullptr;
	ImageView*                      _coinString = nullptr;
	Button*							_Button_change = nullptr;
	ImageView*						_OrderTop;
	ui::ScrollView*                      _OrderList;
	std::vector<Button*>				_vecVipButton;//购买
	std::vector<Button*>				_vecMenuButton;//购买页面按钮
	std::vector<Sprite*>				_vecMenuSprite;//购买页面按钮纹理
	int								_OldButtonIndex;
};

#endif // __GAMESTORE_LAYER_H__
