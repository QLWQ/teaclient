/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_GAMEORDER_LAYER_H__
#define __HN_GAMEORDER_LAYER_H__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "network/HttpClient.h"
#include <vector>

USING_NS_CC;

using namespace HN;
using namespace ui;
using namespace std;

struct OrderInfo
{
    std::string orderID;
    std::string typeinfo;
    std::string receiptData;
    DOUBLE	    price;
    bool        paySuccess;
};

class GameOrderList : public HNLayer, public HNHttpDelegate
{
public:
    typedef std::function<void()> CloseCallBack;
    CloseCallBack	onCloseCallBack;

public:
	GameOrderList();

	virtual ~GameOrderList();

    virtual bool init() override;
 
    void setChangeDelegate(MoneyChangeNotify* delegate)
    {
        _delegate = delegate;
    }

    CREATE_FUNC(GameOrderList);

private:
	void closeFunc() override;

private:
	// 补单操作
	void reissueApplyClickCallback(Ref* pSender);

private:
    // 获取支付订单列表
    void getOrderListData();

    // 获取支付订单列表回调
	void getOrderListEventSelector(const std::string& data);

	// 创建订单列表
	void createOrderList();

    // 补单操作
    void PayAgain(const std::string& data);

    // 发货失败提示
    void supportAlert();

private:
	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

private:
	MoneyChangeNotify*	_delegate		= nullptr;

	vector<OrderInfo>	_iOrderList;

	std::string			_orderID = "";

	ui::ListView*		_orderListView = nullptr;

	Button*				_reissueButton = nullptr;
};


#endif // __HN_GAMEORDER_LAYER_H__