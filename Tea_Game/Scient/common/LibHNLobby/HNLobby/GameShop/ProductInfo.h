/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __ProductInfo_H__
#define __ProductInfo_H__

#include "HNNetExport.h"
#include <string>

namespace HN {

	//{
	//	"identifier": "11111111111111111111",
	//	"number": 10000,
	//	"price": 2
	//}

#define PRODUCT_PAY_TYPE_NONE		-1	//无类型
#define PRODUCT_PAY_TYPE_WECHAT		0	//微信支付
#define PRODUCT_PAY_TYPE_ALIPAY		1	//支付宝支付
#define PRODUCT_PAY_TYPE_ZHIFU   	2	//智付
#define PRODUCT_PAY_TYPE_JUBAOYUN   3	//聚宝云
#define PRODUCT_PAY_TYPE_YINLIAN	4	//银联支付
#define PRODUCT_PAY_TYPE_IAP   		5	//苹果内购

    // 商品结构
    struct PRODUCT_INFO
    {
    public:
		PRODUCT_INFO() {}
		PRODUCT_INFO(LLONG number, DOUBLE price, INT goodsType, const std::string& identifier)
			: number(number)
			, price(price)
			, goodsType(goodsType)
            , identifier(identifier)
			, SerialNumber(-1)
			, payType(PRODUCT_PAY_TYPE_NONE)
        {
            
        }

		// 序列化
		std::string serialize();

		// 反序列化
		void deserialize(std::string data);

    public:
        // 购买数量
        LLONG					number = 0;
        // 商品价格
        DOUBLE					price = 0.0;
		// 商品类型(1金币，2房卡)
		INT						goodsType = 1;
        // 商品ID
        std::string             identifier;
		// 支付方式(sdk辨别)
		INT						payType = PRODUCT_PAY_TYPE_NONE;
		// xmlFile(sdk使用)
		std::string				xmlFile;
		// 订单号
		std::string             orderID;
		// 商品编号
		LLONG					SerialNumber = -1;
    };
}

#endif