/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_EXCHANGE_MONEY_H__
#define HN_EXCHANGE_MONEY_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "network/HttpClient.h"

USING_NS_CC;
using namespace HN;
using namespace ui;
using namespace cocostudio;
using namespace cocos2d::network;

class ExchangeDiamonds : public HNLayer, public ui::EditBoxDelegate
{
public:
	ExchangeDiamonds();

	virtual ~ExchangeDiamonds();

	static ExchangeDiamonds* createExchangeMoneyLayer();

	virtual bool init() override;

	void setChangeDelegate(MoneyChangeNotify* delegate);

private:
	void closeFunc() override;

	bool exchangeDiamondsCallBack(HNSocketMessage* socketMessage);

protected:
	virtual void editBoxReturn(ui::EditBox* editBox) {};

	void editBoxTextChanged(ui::EditBox* editBox, const std::string& text) {};

	void editBoxEditingDidBegin(ui::EditBox* editBox) {};
	

private:
	MoneyChangeNotify*	_delegate;

	INT _exchangeRate;
};

#endif // HN_EXCHANGE_MONEY_H__
