/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNUpdate.h"
#include "HNMarketExport.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"

USING_NS_CC_EXT;
using namespace std;

namespace HN
{
	static const std::string UPDATE_PATH("platform/UpdateUi/Node_update.csb");

	HNUpdate* HNUpdate::create()
	{
		HNUpdate* update = new HNUpdate();
		if(update != nullptr && update->init())
		{
			update->autorelease();
			return update;
		}
		CC_SAFE_DELETE(update);
		return nullptr;
	}

	void HNUpdate::checkUpdate()
	{
		getOnlineVersion();
	}

	HNUpdate::HNUpdate()
		: _isMustUpdate(false),
		_UpdateTime(0.0),
		_NowTime(0.0),
		_Progress(0.0),
		_downloadIdentifier("")
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		_storePath = "/sdcard/";
#else
		_storePath = FileUtils::getInstance()->getWritablePath();
#endif
	}

	HNUpdate::~HNUpdate()
	{
		HNHttpRequest::getInstance()->removeObserver(this);
	}

	bool HNUpdate::init()
	{
		if(!HNLayer::init()) return false;
		
		auto node = CSLoader::createNode(UPDATE_PATH);
		if (!node) return false;
		node->setPosition(_winSize / 2);
		addChild(node);

		float scalex = 1280 / _winSize.width;
		float scaley = 720 / _winSize.height;

		_layout = (Layout*)node->getChildByName("Panel_update");
		_layout->setScale(_winSize.width / 1280, _winSize.height / 720);

		for (auto child : _layout->getChildren())
		{
			child->setScale(scalex, scaley);
		}

		_progressBg = (ImageView*)_layout->getChildByName("Image_lodingBG");
		_progressBg->setVisible(false);
		_progressTimer = (LoadingBar*)_progressBg->getChildByName("LoadingBar_update");
		_textPercent = (Text*)_progressBg->getChildByName("Text_update");

		_textMessage = (Text*)_layout->getChildByName("Text_tip");
		_textMessage->setString(GBKToUtf8("版本更新检测中"));

		return true;
	}

	void HNUpdate::onSetTime(float dt)
	{
		_UpdateTime = _UpdateTime + 1.0;
		if (_UpdateTime - _NowTime > 5.0f)
		{
			/*if (_progressTimer != nullptr)
			{
			_progressTimer->setPercent(100);
			}*/
			if (_Progress < 0.1f)
			{
				this->schedule(schedule_selector(HNUpdate::onTaskProgressUpdate), 3.0f);
			}
		}
	}

	void HNUpdate::onTaskProgressUpdate(float dt)
	{
		/*if (_progressTimer != nullptr)
		{
		_progressTimer->setPercent(100);
		}*/
		_Progress = _Progress + 1.0;
		if (_progressTimer != nullptr)
		{
			float percent = (float)_Progress / (float)100 * 100.f;
			if (percent >= 100)
			{
				_progressTimer->setPercent(99);
			}
			else
			{
				_progressTimer->setPercent(percent);
			}
		}
		if (_textPercent != nullptr)
		{
			float percent = (float)_Progress / (float)100 * 100.f;
			if (percent >= 100)
			{
				percent = 99;
			}
			string str = StringUtils::format("下载进度%.2f%%", percent);
			_textPercent->setString(GBKToUtf8(str));
		}
	}

	bool HNUpdate::getOnlineVersion()
	{
		_textMessage->setString(GBKToUtf8("版本更新检测中"));

		//发送请求，获取版本号和安装包地址7.9
		requestWithParams("action=GetInfoByKey", "update", HNPlatformConfig()->getAppInfoUrl().c_str(), HttpRequest::Type::POST);

		return true;
	}

	bool HNUpdate::hasNewVersion()
	{
		return HN::Operator::requestChannel("sysmodule", "getVersion").compare(_onlineVersion) != 0;
	
	}

	void HNUpdate::onTaskProgress(const DownloadTask& task, int64_t bytesReceived, int64_t totalBytesReceived, int64_t totalBytesExpected)
	{
		if (task.identifier.compare(_downloadIdentifier) == 0)
		{
			_NowTime = _UpdateTime;
			if (_progressTimer != nullptr)
			{
				float percent = (float)totalBytesReceived / (float)totalBytesExpected * 100.f;
				_progressTimer->setPercent(percent);
			}
			else
			{
				if (_layout != nullptr)
				{
					_progressBg = (ImageView*)_layout->getChildByName("Image_lodingBG");
					_progressBg->setVisible(true);
					_progressTimer = (LoadingBar*)_progressBg->getChildByName("LoadingBar_update");
				}
			}

			if (_textPercent != nullptr)
			{
				//char str[100] = {0};
				//sprintf(str, "%.2fKB / %.2fKB", (double)totalBytesReceived / 1024, (double)totalBytesExpected / 1024);
				float percent = (float)totalBytesReceived / (float)totalBytesExpected * 100.f;
				string str = StringUtils::format("下载进度%.2f%%", percent);//("%.2fKB / %.2fKB", (double)totalBytesReceived / 1024, (double)totalBytesExpected / 1024);
				_textPercent->setString(GBKToUtf8(str));
			}
			else
			{
				if (_layout != nullptr)
				{
					_progressBg = (ImageView*)_layout->getChildByName("Image_lodingBG");
					_progressBg->setVisible(true);
					_textPercent = (Text*)_progressBg->getChildByName("Text_update");
				}
			}
		}
		else
		{
			if (_progressTimer != nullptr)
			{
				float percent = 0.5f / 1.0f * 100.f;
				_progressTimer->setPercent(percent);
			}
			if (_textPercent != nullptr)
			{
				//char str[100] = { 0 };
				//sprintf(str, "%.2fKB / %.2fKB", (double)totalBytesReceived / 1024, (double)totalBytesExpected / 1024);
				float percent = (float)totalBytesReceived / (float)totalBytesExpected * 100.f;
				string str = StringUtils::format("下载进度%.2f%%", percent);//("%.2fKB / %.2fKB", (double)totalBytesReceived / 1024, (double)totalBytesExpected / 1024);
				_textPercent->setString(GBKToUtf8(str));
			}
		}
	}

	void HNUpdate::onFileTaskSuccess(const DownloadTask& task)
	{
		this->unschedule(schedule_selector(HNUpdate::onSetTime));
		if (task.identifier.compare(_downloadIdentifier) == 0)
		{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			do 
			{
				if (_Progress > 0.0f)
				{
					unschedule(schedule_selector(HNUpdate::onTaskProgressUpdate));
				}
				if (_progressTimer != nullptr)
				{
					_progressTimer->setPercent(100);
					if (_textPercent != nullptr)
					{
						_textPercent->setString(GBKToUtf8("下载完成!"));
					}
				}
				CC_BREAK_IF(onFinishCallback == nullptr);
				
				onFinishCallback(true, GBKToUtf8("下载完成"), _storePath);
			} while (0);
#endif
		}
		else
		{
		
		}
	}


	void HNUpdate::onTaskError(const DownloadTask& task, int errorCode, int errorCodeInternal, const std::string& errorStr)
	{
		this->unschedule(schedule_selector(HNUpdate::onSetTime));
		if (_Progress != 0.0)
		{
			this->unschedule(schedule_selector(HNUpdate::onTaskProgressUpdate));
		}
		onFinishCallback(false, errorStr, "");
	}

	//发送数据接口
	void HNUpdate::requestWithParams(const std::string& params, const std::string& tag, const std::string& url, HttpRequest::Type type)
	{
		std::string requestData;
		requestData.append(params);
		requestData.append("&key=");
		requestData.append(PlatformConfig::getInstance()->getAppKey());

		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request(tag, type, url, requestData);
	}

	void HNUpdate::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		HNLOG_DEBUG("App信息%s", responseData.c_str());
		if (requestName.compare("update") != 0) return;

		if (!isSucceed)
		{
			std::string errorMsg = GBKToUtf8("网络连接失败,请检查网络（网络是否正常,应用是否授权网络连接.）");
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(errorMsg);
			prompt->setCallBack([this](){
				this->removeFromParent();
			});
			return;
		}

		HNLOG("appInfo>%s", responseData.c_str());
		
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());
		if (doc.HasParseError() || !doc.IsObject())
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("数据解析失败"));
			prompt->setCallBack([this]() {
				this->removeFromParent();
			});
			return;
		}
		do
		{
			rapidjson::Value& array = doc["content"];
			if (array.IsArray())
			{
				//解析返回的数据
				for (UINT i = 0; i < array.Size(); i++)
				{
					rapidjson::Value& arraydoc = array[i];
					do
					{
						if (arraydoc.HasMember("AutoLaunch"))
						{
							_isMustUpdate = arraydoc["AutoLaunch"].GetBool();
						}
						if (arraydoc.HasMember("PlistUrl"))
						{
							_plistUrl = arraydoc["PlistUrl"].GetString();
							HNPlatformConfig()->setDownloadPlistUrl(_plistUrl);
						}
						if (arraydoc.HasMember("id"))
						{
							PlatformConfig::getInstance()->setAppId(arraydoc["id"].GetInt());
						}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
						if (arraydoc.HasMember("AndroidVersion"))
						{
							_onlineVersion = arraydoc["AndroidVersion"].GetString();
							
						}
						if (arraydoc.HasMember("AndroidName"))
						{
							_downloadUrl = arraydoc["AndroidName"].GetString();
						
						}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
						if (arraydoc.HasMember("AppleVersion"))
						{
							_onlineVersion = arraydoc["AppleVersion"].GetString();
						}
						if (arraydoc.HasMember("AppleName"))
						{
							_downloadUrl = arraydoc["AppleName"].GetString();
						}
#endif
					} while (0);
				}
			}
		} while (0);

		if (hasNewVersion())
		{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
			_downloadIdentifier = "download_" + _onlineVersion;
#endif
			needUpdate();
		}
		else
		{
			onFinishCallback(false, "", "");
			this->removeFromParent();
		}
	}

	void HNUpdate::onClickDownloadCallback()
	{
		/*std::string url = "https://down.qp888n.com";
		Application::getInstance()->openURL(url);*/
		_downloader = std::make_shared<Downloader>();
		_downloader->onTaskProgress = std::bind(&HNUpdate::onTaskProgress, this,
			std::placeholders::_1,
			std::placeholders::_2,
			std::placeholders::_3,
			std::placeholders::_4);

		_downloader->onFileTaskSuccess = std::bind(&HNUpdate::onFileTaskSuccess, this,
			std::placeholders::_1);

		_downloader->onTaskError = std::bind(&HNUpdate::onTaskError, this,
			std::placeholders::_1,
			std::placeholders::_2,
			std::placeholders::_3,
			std::placeholders::_4);

		_localVersion = HN::Operator::requestChannel("sysmodule", "getVersion");
		_onlineVersion = _localVersion;
		this->schedule(schedule_selector(HNUpdate::onSetTime), 1.0f);

		if (_progressTimer != nullptr)
		{
			_progressBg->setVisible(true);
			_progressTimer->setPercent(0.f);
			_textPercent->setString("0.0 / 100");
		}
		if (_textMessage != nullptr)
		{
			_textMessage->setString(GBKToUtf8("新版下载中..."));
		}

		size_t locate = _downloadUrl.find_last_of("/");
		if (locate == std::string::npos)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("文件名出错"));
			return;
		}
		std::string filename = _downloadUrl.substr(locate + 1);

		_storePath.append(filename);

		//因为没有清理缓存文件？所以下载进度出错？
		if (FileUtils::getInstance()->isFileExist(_storePath))
		{
			FileUtils::getInstance()->removeFile(_storePath);
		}
		if (FileUtils::getInstance()->isFileExist(_storePath + ".tmp"))
		{
			FileUtils::getInstance()->removeFile(_storePath + ".tmp");
		}

		_downloader->createDownloadFileTask(_downloadUrl, _storePath, _downloadIdentifier);

		//_downloader->downloadAsync(_downloadUrl, _storePath, "download");
	}

	//有更新
	void HNUpdate::needUpdate()
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
		auto prompt = GamePromptLayer::create(!_isMustUpdate);
		if (_isMustUpdate)
		{
			prompt->showPrompt(GBKToUtf8("发现新版本，更新进入游戏"));
		}
		else
		{
			prompt->showPrompt(GBKToUtf8("发现新版本，是否现在下载？"));
		}
		prompt->setCallBack(CC_CALLBACK_0(HNUpdate::onClickDownloadCallback, this));
		prompt->setCancelCallBack([this](){
			this->removeFromParent();
		});
		onClickDownloadCallback();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		_layout->setVisible(false);
		onFinishCallback(true, GBKToUtf8("更新成功"), _plistUrl);
#else

#endif
	}
}