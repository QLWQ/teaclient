/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameResUpdate.h"
#include "HNMarketExport.h"

static const int updatePromptTag = 10010;   //更新弹窗标签

GameResUpdate::GameResUpdate()
{
}

GameResUpdate::~GameResUpdate()
{
	onUpdateCallBack = nullptr;
	onCheckCallBack = nullptr;
}

bool GameResUpdate::init()
{
	return true;
}

void GameResUpdate::release()
{
	if (_curUpdate) _curUpdate->am->onUpdateCallBack = nullptr;
	Ref::release();
}

void GameResUpdate::checkUpdate(int GameID, bool bDownload)
{
	_curUpdate = UpdateInfoModule()->findGame(GameID);

	// 如果此游戏没有配置进游戏更新列表，则不更新
	if (!_curUpdate)
	{
		if (bDownload)
		{
			if (onUpdateCallBack) onUpdateCallBack(true, (EventAssetsManagerEx::EventCode)0);
		}
		else
		{
			if (onCheckCallBack) onCheckCallBack(false);
		}
		return;
	}

	if (_curUpdate->am->getLocalManifest()->isLoaded())
	{
		// 如果已经更新过资源，则不需要检测更新
		if (_curUpdate->isUpdated)
		{
			if (bDownload)
			{
				if (onUpdateCallBack) onUpdateCallBack(true, (EventAssetsManagerEx::EventCode)1);
			}
			else
			{
				if (onCheckCallBack) onCheckCallBack(false);
			}
			return;
		}

		// 已经检测过则直接返回结果
		if (_curUpdate->isAlreadyCheck && !bDownload && onCheckCallBack)
		{
			onCheckCallBack(_curUpdate->eventCode == extension::EventAssetsManagerEx::EventCode::NEW_VERSION_FOUND);
			return;
		}

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在检测更新..."), 25);
		
		// 更新回调
		_curUpdate->am->onUpdateCallBack = [=](EventAssetsManagerEx* event) {
		
			auto eventCode = event->getEventCode();
			_curUpdate->eventCode = eventCode;

			switch (eventCode)
			{
			case extension::EventAssetsManagerEx::EventCode::NEW_VERSION_FOUND:
			{
				if (_curUpdate->isAlreadyCheck) break;

				LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
				_curUpdate->isAlreadyCheck = true;

				if (bDownload)
				{
					doDownload();
				}
				else
				{
					if (onCheckCallBack)
					{
						onCheckCallBack(true);
					}
				}
				break;
			}//发现新版本，需要更新

			case extension::EventAssetsManagerEx::EventCode::ERROR_NO_LOCAL_MANIFEST:
			case extension::EventAssetsManagerEx::EventCode::ALREADY_UP_TO_DATE:
			case extension::EventAssetsManagerEx::EventCode::UPDATE_FINISHED:
			{
				LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

				if (Director::getInstance()->getRunningScene()->getChildByTag(updatePromptTag))
				{
					Director::getInstance()->getRunningScene()->removeChildByTag(updatePromptTag);
				}

				_curUpdate->isUpdated = true;
				_curUpdate->isAlreadyCheck = true;

				if (bDownload)
				{
					if (onUpdateCallBack)
					{
						onUpdateCallBack(true, eventCode);
					}
				}
				else
				{
					if (onCheckCallBack)
					{
						onCheckCallBack(false);
					}
				}
				break;
			}//文件已经存在/没有本地文件/更新完毕

			case extension::EventAssetsManagerEx::EventCode::UPDATE_PROGRESSION:
			{
				if (event->getAssetId().compare("@version") == 0
					|| event->getAssetId().compare("@manifest") == 0)
				{
					break;
				}

				float percent = event->getPercentByFile();
				std::string text = StringUtils::format(GBKToUtf8("正在更新资源：%d%%"), int(percent));
				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), text, 25);
				break;
			}//正在下载

			case extension::EventAssetsManagerEx::EventCode::ASSET_UPDATED:
				break;
			case extension::EventAssetsManagerEx::EventCode::UPDATE_FAILED:
			case extension::EventAssetsManagerEx::EventCode::ERROR_PARSE_MANIFEST:
			case extension::EventAssetsManagerEx::EventCode::ERROR_DOWNLOAD_MANIFEST:
			case extension::EventAssetsManagerEx::EventCode::ERROR_DECOMPRESS:
			case extension::EventAssetsManagerEx::EventCode::ERROR_UPDATING:
			default:
			{
				LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

				if (Director::getInstance()->getRunningScene()->getChildByTag(updatePromptTag) == nullptr)
				{
					auto prompt = GamePromptLayer::create(true);
					prompt->showPrompt(GBKToUtf8("更新出错了，请重试！"));
					prompt->setTag(updatePromptTag);
					prompt->setCallBack([=]() {

						if (_curUpdate->isAlreadyCheck)
						{
							_curUpdate->am->downloadFailedAssets();
						}
						else
						{
							_curUpdate->am->checkUpdate();
						}
						LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在更新..."), 25);
					});
					prompt->setCancelCallBack([=]() {

						if (bDownload)
						{
							if (onUpdateCallBack)
							{
								onUpdateCallBack(false, eventCode);
							}
						}
						else
						{
							if (onCheckCallBack)
							{
								onCheckCallBack(true);
							}
						}
					});
				}
				break;
			}//下载出错
			}
		};

		// 已经检测过更新，直接下载资源
		if (_curUpdate->isAlreadyCheck)
		{
			doDownload();
		}
		else
		{
			_curUpdate->am->checkUpdate();
		}
	}
	else
	{
		// 如果没放manifest文件，表示资源存放本地，不做更新
		if (bDownload)
		{
			if (onUpdateCallBack)
			{
				onUpdateCallBack(true, EventAssetsManagerEx::EventCode::ERROR_NO_LOCAL_MANIFEST);
			}
		}
		else
		{
			if (onCheckCallBack)
			{
				onCheckCallBack(false);
			}
		}
	}
}

void GameResUpdate::doDownload()
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	// 延迟一帧再执行下载，避免匿名函数中调用导致偶现崩溃
	Director::getInstance()->getScheduler()->schedule([=](float fDelta)
	{
		// wifi环境下不提示直接更新
		std::string type = Operator::requestChannel("sysmodule", "getNetType");
		if (type.compare("WIFI") == 0)
		{
			_curUpdate->am->update();
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("准备更新..."), 25);
		}
		else
		{
			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(GBKToUtf8("当前使用移动网络，是否下载？"));
			prompt->setCallBack([=]() {

				_curUpdate->am->update();
				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("准备更新..."), 25);
			});

			prompt->setCancelCallBack([=]() {

				if (onUpdateCallBack)
				{
					onUpdateCallBack(false, (EventAssetsManagerEx::EventCode) 0);
				}
			});
		}
	}, this, 0.0f, 0, 0.0f, false, "delayDoDownload");
}

bool GameResUpdate::deleteGameRes(int GameID)
{
	updateStruct* current = UpdateInfoModule()->findGame(GameID);

	if (!current) return false;

	// 删除下载的资源文件
	if (FileUtils::getInstance()->isDirectoryExist(current->am->getStoragePath()))
	{
		FileUtils::getInstance()->removeDirectory(current->am->getStoragePath());
		UpdateInfoModule()->resetGame(GameID);
		return true;
	}

	return false;
}