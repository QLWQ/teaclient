/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_GameResUpdate_h__
#define HN_GameResUpdate_h__


#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include <string>

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;

class GameResUpdate : public Ref
{
public:
	typedef std::function<void(bool success, extension::EventAssetsManagerEx::EventCode code)> UpdateCallBack;
	UpdateCallBack		onUpdateCallBack = nullptr;

	typedef std::function<void(bool needUpdate)> CheckCallBack;
	CheckCallBack		onCheckCallBack = nullptr;

	typedef std::function<void(int iGameID)> HideCallBack;
	HideCallBack	onHideCallBack = nullptr;

	// 检测当前游戏需不需要更新资源（是否需要下载）
	void checkUpdate(int GameID, bool bDownload);

	// 删除单个游戏资源
	bool deleteGameRes(int GameID);

public:
	GameResUpdate();

	virtual ~GameResUpdate();

	virtual bool init();

	void release();

	CREATE_FUNC(GameResUpdate);

private:
	void doDownload();

protected:
	updateStruct* _curUpdate = nullptr;
};
#endif // HN_GameResUpdate_h__
