/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_NewsHelper_h__
#define __HN_NewsHelper_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"

USING_NS_CC;

class NewsHelper : public Ref, public HNHttpDelegate
{
public:
	typedef std::function<void(std::vector<RichTextStruct> vec)> NewsCallBack;
	NewsCallBack		onNewsCallBack = nullptr;

public:
	// 请求数据
	void request(const std::string& url);

	virtual bool init();

	// 创建
	CREATE_FUNC(NewsHelper);

protected:
	// 回调
	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	// 构造函数
	NewsHelper();

	// 析构函数
	virtual ~NewsHelper();

private:


};

#endif // __HN_NewsHelper_h__
