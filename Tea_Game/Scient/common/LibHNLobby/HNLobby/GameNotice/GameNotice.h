/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GameNotice_h__
#define __GameNotice_h__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include <queue>

USING_NS_CC;

struct NotifyItem
{
	int repeat;
	std::string message;

	NotifyItem(const std::string& message, int repeat) : message(message), repeat(repeat) {}
};

class GameNotice :public HNLayer
{
	
public:
	GameNotice();

	virtual ~GameNotice();

	static GameNotice* getInstance();

	bool init();

	// ������ʾλ��
	//void setPosition(const Vec2& pos);

	

public:
	void postMessage(const std::string& message, int repeat = 1, Vec2 pos = Vec2::ZERO);

	void setCallBack(std::function<void ()> sure);

private:
	Node*	_node		= nullptr;
	Node*	_parent		= nullptr;
	Size	_showSize	= Size::ZERO;
	Size	_wordSize	= Size::ZERO;
	Vec2	_defaultPos = Vec2::ZERO;
	Vec2	_realPos	= Vec2::ZERO;
	Text*	_text		= nullptr;
	bool	_runing		= false;
	bool	_isChangePos = false;
	timeline::ActionTimeline* _timeLine = nullptr;
	std::queue<NotifyItem> _notifyMsgs;

private:
	void start();
	void stop();
	void clear();
	void updateNextMessage();
	
private:
	void moveWord();

private:
	std::function<void ()> _sure;
};

#endif // __GameNotice_h__
