/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "NoticeList.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"

USING_NS_CC_EXT;
using namespace network;

// 对话框布局文件
static const char* NOTICE_LIST_CSB = "platform/NoticeList/NoticeList.csb";

// 列表项布局文件
static const char* NOTICE_LIST_ITEM = "platform/NoticeList/NoticeItem.csb";

class NoticeItem: public TableViewCell
{
public:
	// 创建
	CREATE_FUNC(NoticeItem);

	// 初始化
	bool init()
	{
		if(!TableViewCell::init()) {
			return false;
		}

		bool ret = false;

		do 
		{
			// 布局
			auto csLoader = CSLoader::createNode(NOTICE_LIST_ITEM);
			CC_BREAK_IF(csLoader == nullptr);
			this->addChild(csLoader);
			csLoader->setPosition(Vec2(402.0f, 70.0f));

			// 标题
			_title = dynamic_cast<ui::Text*>(csLoader->getChildByName("Text_title"));
			CC_BREAK_IF(_title == nullptr);

			// 内容
			_content = dynamic_cast<ui::Text*>(csLoader->getChildByName("Text_content"));
			CC_BREAK_IF(_content == nullptr);

			// 时间
			_time = dynamic_cast<ui::Text*>(csLoader->getChildByName("Text_time"));
			CC_BREAK_IF(_time == nullptr);

			ret = true;
		} while (0);		

		return ret;
	}

	// 修改标题
	void setTitle(const std::string& title)
	{
		_title->setString(title);
	}

	// 修改内容
	void setContent(const std::string& content)
	{
		_content->setString(content);
	}

	// 修改时间
	void setTime(const std::string& time)
	{
		_time->setString(time);
	}

	NoticeItem(){}

private:
	ui::Text* _title   = nullptr;
	ui::Text* _content = nullptr;
	ui::Text* _time    = nullptr;
};

bool NoticeList::init()
{
	if (!HNLayer::init()) {
		return false;
	}

	// 加载布局
	auto csNode = CSLoader::createNode(NOTICE_LIST_CSB);
	CCAssert(csNode != nullptr, "null");

	this->addChild(csNode);
	csNode->setPosition(Vec2(640,360));

	// 背景缩放
	auto layoutBg = (Layout*)csNode->getChildByName("Panel_bg");
	layoutBg->setScale(_winSize.width / 1280, _winSize.height / 720);

	// 列表背景
	ImageView_bg = dynamic_cast<ui::ImageView*>(csNode->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		ImageView_bg->setScale(_winSize.height / 720);
	}

	auto layout_frame = dynamic_cast<Layout*>(ImageView_bg->getChildByName("Panel_frame"));
	CCAssert(layout_frame != nullptr, "null");

	// 关闭按钮
	auto Button_close = dynamic_cast<ui::Button*>(ImageView_bg->getChildByName("Button_close"));
	CCAssert(Button_close != nullptr, "null");
	Button_close->addTouchEventListener(CC_CALLBACK_2(NoticeList::onCloseClickCallback, this));

	// 添加列表
	_tableView = TableView::create(this, layout_frame->getContentSize());
	_tableView->setDirection(extension::ScrollView::Direction::VERTICAL);
	_tableView->setVerticalFillOrder(extension::TableView::VerticalFillOrder::TOP_DOWN);
	ImageView_bg->addChild(_tableView);
	_tableView->setPosition(layout_frame->getPosition());

	return true;
}

Size NoticeList::tableCellSizeForIndex(extension::TableView *table, ssize_t idx)
{
	return Size(804, 140);
}

extension::TableViewCell* NoticeList::tableCellAtIndex(extension::TableView *table, ssize_t idx)
{
	TableViewCell* cell = table->dequeueCell();

	if(cell == nullptr)	{
		cell = NoticeItem::create();
	}

	auto item = (NoticeItem*)cell;
	item->setTitle(_datas[idx].title);
	item->setContent(_datas[idx].content);
	item->setTime(_datas[idx].time);

	return cell;
}

// 列表项数量
ssize_t NoticeList::numberOfCellsInTableView(extension::TableView *table)
{
	return _datas.size();
}

// 列表项被点击
void NoticeList::tableCellTouched(extension::TableView* table, extension::TableViewCell* cell)
{

}

void NoticeList::queryNoticeList()
{
	std::string url = PlatformConfig::getInstance()->getNoticeUrl();
	url.append("Type=GetSystemMsg");
	url.append("&pageSize=-1");
	url.append("&pageindex=2");

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("notice", HttpRequest::Type::GET, url);
}


void NoticeList::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (requestName.compare("notice") != 0)	{
		return;
	}

	if (!isSucceed)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("数据查询失败"));
		return;
	}

	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

	if (doc.HasParseError() || !doc.IsObject())	{
		return;
	}

	if (doc.HasMember("list") && doc["list"].IsArray() && doc["list"].Capacity() > 0)
	{
		_datas.clear();
		for (rapidjson::SizeType i = 0; i < doc["list"].Capacity(); i++)
		{
			rapidjson::Value& value = doc["list"][i];
			DataNode node;

			if (value.HasMember("MTitle") && value["MTitle"].IsString()) {
				node.title = value["MTitle"].GetString();
			}

			if (value.HasMember("MContent") && value["MContent"].IsString()) {
				node.content = value["MContent"].GetString();
			}

			if (value.HasMember("AddTime") && value["AddTime"].IsString()) {
				node.time = value["AddTime"].GetString();
			}			

			_datas.push_back(node);
		}

		_tableView->reloadData();
	}
}

void NoticeList::onEnter()
{
	HNLayer::onEnter();

	queryNoticeList();
}

void NoticeList::onCloseClickCallback(Ref* pSender, Widget::TouchEventType type)
{
	if(type != Widget::TouchEventType::ENDED) {
		return;
	}

	this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			this->removeFromParent();
	}), nullptr));
}

NoticeList::NoticeList()
{

}

NoticeList::~NoticeList()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}