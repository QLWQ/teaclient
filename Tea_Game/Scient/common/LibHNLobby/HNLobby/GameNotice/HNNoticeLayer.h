#ifndef	_NOTICE_LAYER_
#define	_NOTICE_LAYER_

#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNNetExport.h"
#include "extensions/cocos-ext.h"
USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class NoticeLayer : public HNLayer
{

public:
	virtual bool init() override;

	NoticeLayer();

	virtual ~NoticeLayer();

	CREATE_FUNC(NoticeLayer);

	void addTextInfo(const std::string& title, const std::string content,const std::string& type);

	void updateShow();  
private:
	void onCloseClickCallback(Ref* pSender, Widget::TouchEventType type); 

	void onClickCallback(Ref* pSender);

	void addListItem(const std::string& title); 

	void updateTextHeight();  

	void updateText(const std::string& content);

	void updateImage(const std::string& img_url);
private:
	std::unordered_map<std::string, std::string> m_ShowContentMap; 
	std::unordered_map<std::string, std::string> m_ShowTypeMap;
	ListView* m_pListView = nullptr;
	ui::ScrollView*	m_pScrollView;
	Label* m_pLabel = nullptr;
	ImageView* m_pImage;
	Button*  m_pSelectButton;
	int		m_nUrlTimes;
};
#endif