/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNNewsHelper.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"


NewsHelper::NewsHelper()
{

}

NewsHelper::~NewsHelper()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

void NewsHelper::request(const std::string& url)
{
	std::string param = StringUtils::format("action=GetServiceInfo&key=");
	HNHttpRequest::getInstance()->request("news", network::HttpRequest::Type::POST, PlatformConfig::getInstance()->getEditUrl(), param);
	HNHttpRequest::getInstance()->addObserver(this);
}

bool NewsHelper::init()
{
	return true;
}

void NewsHelper::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (requestName.compare("news") == 0)
	{
		if (!isSucceed)	{
			return;
		}

		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());
		if (doc.HasParseError() || !doc.IsObject() || !doc.HasMember("content") || !doc["content"].IsArray()) {
			return;
		}

		for (auto i = 0; i < doc["content"].Size(); i++)
		{
			if (doc["content"][i].IsObject() && doc["content"][i].HasMember("ParaName") && doc["content"][i].HasMember("ParaValue"))
			{
				
			}
		}
	}
}