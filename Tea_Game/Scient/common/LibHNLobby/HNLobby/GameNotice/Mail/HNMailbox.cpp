/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNMailbox.h"
#include "HNMailMessageHead.h"

USING_NS_CC_EXT;


static const char* MAIL_LIST_CSB	= "platform/mailbox/mailbox.csb";
static const char* MAIL_ITEM		= "platform/mailbox/mailItem.csb";
static const char* ATTACHMENT_ITEM	= "platform/mailbox/attachment.csb";

class MailItem: public TableViewCell
{
public:
	// 创建
	CREATE_FUNC(MailItem);

	// 初始化
	bool init()
	{
		if(!TableViewCell::init()) return false;

		do 
		{
			// 布局
			auto csLoader = CSLoader::createNode(MAIL_ITEM);
			CC_BREAK_IF(csLoader == nullptr);
			this->addChild(csLoader);

			_img_item = dynamic_cast<ImageView*>(csLoader->getChildByName("Image_item"));

			setContentSize(_img_item->getContentSize());
			_img_item->setPosition(getContentSize() / 2);

			// 标题
			_title = dynamic_cast<ui::Text*>(_img_item->getChildByName("Text_title"));
			CC_BREAK_IF(_title == nullptr);

			// 发件人
			_name = dynamic_cast<ui::Text*>(_img_item->getChildByName("Text_name"));
			CC_BREAK_IF(_name == nullptr);

			// 时间
			_time = dynamic_cast<ui::Text*>(_img_item->getChildByName("Text_time"));
			CC_BREAK_IF(_time == nullptr);

			// 邮件图标
			_mail = dynamic_cast<ui::ImageView*>(_img_item->getChildByName("Image_mail"));
			CC_BREAK_IF(_mail == nullptr);

			return true;
		} while (0);		

		return false;
	}

	void buildCell(DataNode* mail)
	{
		_mailid = mail->mailID;
		setTitle(mail->title);
		setSendName(mail->name);
		setTime(mail->iKeepTime, mail->iSendTime);
		updateMailState(mail);

		if (mail->isSelect)
		{
			_title->setTextColor(Color4B(178, 94, 43, 255));
			_name->setTextColor(Color4B(208, 121, 35, 255));
			_time->setTextColor(Color4B(208, 121, 35, 255));
			_img_item->loadTexture("platform/mailbox/res/juxing2.png");
		}
		else
		{
			_title->setTextColor(Color4B(66, 94, 170, 255));
			_name->setTextColor(Color4B(80, 109, 188, 255));
			_time->setTextColor(Color4B(80, 109, 188, 255));
			_img_item->loadTexture("platform/mailbox/res/juxing.png");
		}
	}

	// 修改标题
	void setTitle(const std::string& title)
	{
		_title->setString(GBKToUtf8(title));
	}

	// 修改发件人
	void setSendName(const std::string& name)
	{
		_name->setString(GBKToUtf8(name));
	}

	// 修改时间
	void setTime(LLONG iKeepTime, LLONG sendTime)
	{
		LLONG nowTime = time(NULL);
		LLONG temp = iKeepTime * 24 * 60 * 60 + sendTime;
		LLONG Time = temp - nowTime;

		int day = Time / (24 * 60 * 60);
		int hour = (Time % (24 * 60 * 60)) / 3600;

		if (0 == iKeepTime)
		{
			_time->setString(GBKToUtf8("剩余时间:无限制"));
		}
		else if (day > 0)
		{
			_time->setString(StringUtils::format(GBKToUtf8("剩余时间:%d天%d小时"), day, hour));
		}
		else
		{
			_time->setString(GBKToUtf8("剩余时间:少于1天"));
		}

		//_time->setString(StringUtils::format(GBKToUtf8("有效期%d天%d小时"), day, hour));
	}

	// 更新邮件状态
	void updateMailState(DataNode* mail)
	{
		switch (mail->iState)
		{
		case 0:
			_mail->loadTexture("platform/mailbox/res/youjian1.png");
			break;
		case 1:
		case 4:
			if (mail->isSelect)
			{
				_mail->loadTexture("platform/mailbox/res/youjian6.png");
			}
			else
			{
				_mail->loadTexture("platform/mailbox/res/youjian2.png");
			}
			break;
		case 2:
			_mail->loadTexture("platform/mailbox/res/youjian3.png");
			break;
		case 3:
			if (mail->isSelect)
			{
				_mail->loadTexture("platform/mailbox/res/youjian5.png");
			}
			else
			{
				_mail->loadTexture("platform/mailbox/res/youjian4.png");
			}
			break;
		default:
			break;
		}
	}

	MailItem(){}

private:
	ui::Text* _title		= nullptr;// 邮件标题
	ui::Text* _name			= nullptr;// 发件人
	ui::Text* _time			= nullptr;// 有效期
	ImageView* _img_item	= nullptr;
	ImageView* _mail		= nullptr;
	CC_SYNTHESIZE(INT, _mailid, MailID);// 当前邮件id
};

////////////////////////////////////////////////////////////////////////////////////////////
void Mailbox::onEnter()
{
	HNLayer::onEnter();
}

Mailbox::Mailbox()
{

}

Mailbox::~Mailbox()
{
	PlatformLogic()->removeEventSelector(MDM_GP_MAIL, ASS_GP_OPEN_MAIL);
	PlatformLogic()->removeEventSelector(MDM_GP_MAIL, ASS_GP_DEL_MAIL);
	PlatformLogic()->removeEventSelector(MDM_GP_MAIL, ASS_GP_GET_ATTACHMENT);
	Director::getInstance()->getEventDispatcher()->removeEventListener(_mailListener);
}

void Mailbox::closeFunc()
{

}

bool Mailbox::init()
{
	if (!HNLayer::init()) return false;

	// 加载布局
	auto csNode = CSLoader::createNode(MAIL_LIST_CSB);
	CCAssert(csNode != nullptr, "null");
	csNode->setPosition(_winSize / 2);
	addChild(csNode,101);

	// 列表背景
	auto img_bg = dynamic_cast<ui::ImageView*>(csNode->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}

	loadListUI(img_bg);

	loadTextUI(img_bg);

	loadButton(img_bg);

	// 邮件变化监听
	_mailListener = EventListenerCustom::create("mailUpdate", [=](EventCustom* event) {

		_tableView->reloadData();
		
		updateButtonState();

		DataNode* mail = (DataNode*)event->getUserData();

		// 获取不到邮件内容，表示为获取邮件列表，不显示邮件正文（断线重连重新获取邮件列表）
		// 邮件id为当前正在阅读的邮件id，表示此邮件过期被删除，关闭邮件正文显示
		if (!mail || mail->mailID == _mailID)
		{
			_layout_text->setVisible(false);
		}
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_mailListener, 1);

	return true;
}

// 加载列表UI
void Mailbox::loadListUI(Node* parent)
{
	auto img_mailbg = dynamic_cast<ImageView*>(parent->getChildByName("Image_listBG"));
	auto layout_mail = dynamic_cast<Layout*>(img_mailbg->getChildByName("Panel_mail"));
	CCAssert(layout_mail != nullptr, "null");

	// 无邮件提示
	_img_none = dynamic_cast<ImageView*>(img_mailbg->getChildByName("Image_none"));

	// 邮件列表
	_tableView = TableView::create(this, layout_mail->getContentSize());
	_tableView->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	_tableView->setDelegate(this);
	_tableView->setDirection(extension::ScrollView::Direction::VERTICAL);
	_tableView->setVerticalFillOrder(extension::TableView::VerticalFillOrder::TOP_DOWN);
	img_mailbg->addChild(_tableView);
	_tableView->setPosition(layout_mail->getPosition());
}

// 加载正文UI
void Mailbox::loadTextUI(Node* parent)
{
	_layout_text = dynamic_cast<Layout*>(parent->getChildByName("Panel_text"));
	_layout_text->setVisible(false);
	auto list_attachment = dynamic_cast<ui::ListView*>(_layout_text->getChildByName("ListView_attachment"));
	list_attachment->setScrollBarEnabled(false);

	auto img_textbg = dynamic_cast<ImageView*>(_layout_text->getChildByName("Image_textBG"));
	auto layout_text = dynamic_cast<Layout*>(img_textbg->getChildByName("Panel_txt"));

	// 正文
	auto textView = TextView::createWithSize(layout_text->getContentSize());
	textView->setPosition(layout_text->getPosition());
	textView->setTextColor(Color4B(66, 94, 170, 255));
	textView->setFontSize(22);
	textView->setName("textView");
	textView->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
	img_textbg->addChild(textView);

	// 获取按钮
	auto btn_get = dynamic_cast<Button*>(_layout_text->getChildByName("Button_get"));
	btn_get->addClickEventListener([=](Ref*) {
	
		getAttachment(_mailID);
	});

	// 删除按钮
	auto btn_del = dynamic_cast<ui::Button*>(_layout_text->getChildByName("Button_del"));
	btn_del->addClickEventListener([=](Ref*) {

		sendDeleteMail(_mailID);
	});
}

// 加载操作按钮
void Mailbox::loadButton(Node* parent)
{
	// 关闭按钮
	auto btn_close = dynamic_cast<ui::Button*>(parent->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*) {
	
		// 关闭前把所有邮件选中状态都恢复掉
		MailData()->transform([&](DataNode* node, INT mailID) {

			DataNode d = *node;
			d.isSelect = false;
			MailData()->updateMail(&d);
		});
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&]() {
			this->removeFromParent();
		}), nullptr));
	});

	// 一键领取
	_btn_onekeyGet = dynamic_cast<ui::Button*>(parent->getChildByName("Button_onekeyGet"));
	_btn_onekeyGet->addClickEventListener([=](Ref*) {

		getAttachment();
	});

	// 一键删除
	_btn_onekeydel = dynamic_cast<ui::Button*>(parent->getChildByName("Button_onekeyDel"));
	_btn_onekeydel->addClickEventListener([=](Ref*) {

		sendDeleteMail();
	});
	
	updateButtonState();
}

// 更新按钮状态
void Mailbox::updateButtonState()
{
	// 查找邮件列表中有无可领取的附件
	bool isFind = false;
	MailData()->transform([&](DataNode* node, INT mailID) {

		if (node->iState == 2 || node->iState == 3)
		{
			isFind = true;
		}
	});
	_btn_onekeyGet->setEnabled(isFind);

	_btn_onekeydel->setEnabled(MailData()->getMailsCount() > 0);

	_img_none->setVisible(MailData()->getMailsCount() == 0);
}

Size Mailbox::tableCellSizeForIndex(extension::TableView *table, ssize_t idx)
{
	return Size(399, 102);
}

extension::TableViewCell* Mailbox::tableCellAtIndex(extension::TableView *table, ssize_t idx)
{
	TableViewCell* cell = table->dequeueCell();

	if(cell == nullptr)	{
		cell = MailItem::create();
	}

	auto item = (MailItem*)cell;
	if (item) item->buildCell(MailData()->getMailAtIndex(idx));

	return cell;
}

// 列表项数量
ssize_t Mailbox::numberOfCellsInTableView(extension::TableView *table)
{
	int count = MailData()->getMailsCount();
	return count;
}

// 列表项被点击
void Mailbox::tableCellTouched(extension::TableView* table, extension::TableViewCell* cell)
{
	auto item = (MailItem*)cell;
	int touchID = item->getMailID();
	_offset = _tableView->getContentOffset();

	// 如果邮件内容已经获取过，则直接显示
	if (MailData()->isRead(touchID))
	{
		// 更新列表选中状态
		MailData()->transform([&](DataNode* node, INT mailID) {

			DataNode d = *node;
			if (touchID == mailID)
			{
				d.isSelect = true;
				MailData()->updateMail(&d);
			}
			else
			{
				DataNode d = *node;
				d.isSelect = false;
				MailData()->updateMail(&d);
			}
		});

		// 刷新邮件列表
		_tableView->reloadData();
		_tableView->setContentOffset(_offset);

		// 显示邮件内容
		showMail(touchID);
	}
	else
	{
		readMail(touchID);
	}
}

// 阅读邮件
void Mailbox::readMail(int mailID)
{
	MSG_GP_I_Open_Mail read;
	read.iMailID = mailID;

	PlatformLogic()->sendData(MDM_GP_MAIL, ASS_GP_OPEN_MAIL, &read, sizeof(MSG_GP_I_Open_Mail),
		HN_SOCKET_CALLBACK(Mailbox::readMailCallback, this));

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("处理中..."), 25);
}

// 删除邮件
void Mailbox::sendDeleteMail(int mailID/* = 0*/)
{
	MSG_GP_I_Delete_Mail del;
	del.iMailID = mailID;

	PlatformLogic()->sendData(MDM_GP_MAIL, ASS_GP_DEL_MAIL, &del, sizeof(MSG_GP_I_Delete_Mail),
		HN_SOCKET_CALLBACK(Mailbox::deleteMailCallback, this));

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("处理中..."), 25);
}

// 获取邮件附件
void Mailbox::getAttachment(int mailID/* = 0*/)
{
	MSG_GP_I_Get_Attachment get;
	get.iMailID = mailID;

	PlatformLogic()->sendData(MDM_GP_MAIL, ASS_GP_GET_ATTACHMENT, &get, sizeof(MSG_GP_I_Get_Attachment),
		HN_SOCKET_CALLBACK(Mailbox::getAttachmentCallback, this));

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("处理中..."), 25);
}

// 阅读邮件回调
bool Mailbox::readMailCallback(HNSocketMessage* socketMessage)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	std::string str("");
	switch (socketMessage->messageHead.bHandleCode)
	{
	case 0:
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_Open_Mail, socketMessage->objectSize, true,
			"MSG_GP_O_Open_Mail size of error!");
		auto mail = (MSG_GP_O_Open_Mail*)socketMessage->object;

		DataNode data = *MailData()->getMail(mail->iMailID);
		data.message = mail->szMailMessage;
		data.iJewels = mail->iJewels;
		data.iLotteries = mail->iLotteries;
		data.iMoney = mail->i64Money;
		data.isSelect = true;
		data.iState = mail->bMailState;

		MailData()->transform([&](DataNode* node, INT mailID) {

			if (mailID == mail->iMailID)
			{
				MailData()->updateMail(&data);
			}
			else
			{
				DataNode d = *node;
				d.isSelect = false;
				MailData()->updateMail(&d);
			}
		});

		// 刷新邮件列表
		_tableView->reloadData();
		_tableView->setContentOffset(_offset);

		// 显示邮件内容
		showMail(mail->iMailID);
	} break;
	case 1:
		str = "用户信息错误";
		break;
	case 2:
		str = "邮件不存在";
		break;
	case 3:
		str = "不是该玩家的邮件";
		break;
	case 4:
		str = "邮件已删除";
		break;
	default:
		str = "未知错误";
		break;
	}

	if (!str.empty())
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8(str));
	}

	return true;
}

// 获取附件回调
bool Mailbox::getAttachmentCallback(HNSocketMessage* socketMessage)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	std::string str("");
	switch (socketMessage->messageHead.bHandleCode)
	{
	case 0:
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_Get_Attachment, socketMessage->objectSize, true,
			"MSG_GP_O_Get_Attachment size of error!");
		auto mail = (MSG_GP_O_Get_Attachment*)socketMessage->object;

		// id为0是一键领取
		if (mail->iMailID == 0)
		{
			MailData()->transform([&](DataNode* node, INT mailID) {

				// 遍历所有邮件，都变成已读已领取
				DataNode d = *node;
				if (d.iState == 0)
				{
					d.iState = 1;
				}

				if (d.iState == 2 || d.iState ==3)
				{
					d.iState = 4;
				}
				MailData()->updateMail(&d);
			});
		}
		else
		{
			// 更新列表邮件状态
			DataNode data = *MailData()->getMail(mail->iMailID);
			data.iState = mail->bMailState;
			MailData()->updateMail(&data);
		}

		auto btn_get = dynamic_cast<Button*>(_layout_text->getChildByName("Button_get"));
		btn_get->setEnabled(false);
		_tableView->reloadData();
		updateButtonState();

		GamePromptLayer::create()->showPrompt(GBKToUtf8("恭喜您，领取成功！"));

		// 更新玩家信息
		if (onUpdateCallBack)
		{
			onUpdateCallBack();
		}
	} break;
	case 1:
		str = "用户信息错误";
		break;
	case 2:
		str = "邮件不存在";
		break;
	case 3:
		str = "不是该玩家的邮件";
		break;
	case 4:
		str = "附件已领取或无附件";
		break;
	default:
		str = "未知错误";
		break;
	}

	if (!str.empty())
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8(str));
	}
	return true;
}

// 删除邮件回调
bool Mailbox::deleteMailCallback(HNSocketMessage* socketMessage)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	std::string str("");
	switch (socketMessage->messageHead.bHandleCode)
	{
	case 0:
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_Delete_Mail, socketMessage->objectSize, true,
			"MSG_GP_O_Delete_Mail size of error!");
		auto mail = (MSG_GP_O_Delete_Mail*)socketMessage->object;
		bool isFind = false;

		if (mail->iMailID == 0)
		{
			std::vector<DataNode> vec;

			MailData()->transform([&](DataNode* node, INT mailID) {

				// 查找所有不带附件或附件已领取的邮件进行删除
				if (node->iState != 2 && node->iState != 3)
				{
					vec.push_back(*node);
					if (_mailID == mailID) isFind = true;
				}
			});

			for (auto m : vec)
			{
				MailData()->deleteMail(m.mailID);
			}

			if (vec.size() > 0)
			{
				str = StringUtils::format(GBKToUtf8("已删除%d封邮件！"), vec.size());
			}
			else
			{
				str = GBKToUtf8("邮件附件未领取");
			}
		}
		else
		{
			MailData()->deleteMail(mail->iMailID);

			str = GBKToUtf8("删除邮件成功！");
		}
		
		// 当前正在阅读的邮件被删除，需要关闭邮件正文UI
		if (_mailID == mail->iMailID || isFind)
		{
			_layout_text->setVisible(false);
		}

		_tableView->reloadData();
		updateButtonState();
	} break;
	case 1:
		str = GBKToUtf8("用户信息错误");
		break;
	case 2:
		str = GBKToUtf8("邮件不存在");
		break;
	case 3:
		str = GBKToUtf8("不是该玩家的邮件");
		break;
	case 4:
		str = GBKToUtf8("邮件附件未领取");
		break;
	default:
		str = GBKToUtf8("未知错误");
		break;
	}

	if (!str.empty())
	{
		GamePromptLayer::create()->showPrompt(str);
	}
	return true;
}


// 显示邮件内容
void Mailbox::showMail(int mailID)
{
	_mailID = mailID;
	auto data = MailData()->getMail(mailID);
	
	_layout_text->setVisible(true);
	auto btn_get = dynamic_cast<Button*>(_layout_text->getChildByName("Button_get"));
	auto text_name = dynamic_cast<Text*>(_layout_text->getChildByName("Text_name"));
	auto text_title = dynamic_cast<Text*>(_layout_text->getChildByName("Text_title"));
	auto img_textbg = dynamic_cast<ImageView*>(_layout_text->getChildByName("Image_textBG"));
	auto textView = dynamic_cast<TextView*>(img_textbg->getChildByName("textView"));
	auto list_attachment = dynamic_cast<ui::ListView*>(_layout_text->getChildByName("ListView_attachment"));

	btn_get->setVisible(data->iState == 2 || data->iState == 3 || data->iState == 4);
	btn_get->setEnabled(data->iState == 2 || data->iState == 3);
	text_name->setString(GBKToUtf8(data->name));
	text_title->setString(GBKToUtf8(data->title));
	textView->setString(GBKToUtf8(data->message));

	list_attachment->removeAllItems();

	// 创建附件内容
	auto func = [=](std::string filePath, LLONG value) {
	
		auto node = CSLoader::createNode(ATTACHMENT_ITEM);
		auto img_attachment = dynamic_cast<ImageView*>(node->getChildByName("Image_attachment"));
		img_attachment->removeFromParentAndCleanup(false);
		auto img_icon = dynamic_cast<ImageView*>(img_attachment->getChildByName("Image_icon"));
		img_icon->loadTexture(filePath);
		auto text_value = dynamic_cast<Text*>(img_attachment->getChildByName("Text_value"));
		text_value->setString(to_string(value));
		list_attachment->pushBackCustomItem(img_attachment);
	};

	if (data->iMoney > 0)
	{
		func("platform/common/jinbi.png", data->iMoney);
	}

	if (data->iJewels > 0)
	{
		func("platform/common/img_fangka.png", data->iJewels);
	}
	
	if (data->iLotteries > 0)
	{
		func("platform/common/jiangquan1.png", data->iLotteries);
	}
}