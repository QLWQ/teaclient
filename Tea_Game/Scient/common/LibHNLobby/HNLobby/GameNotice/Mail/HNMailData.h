/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_HNMailData_h__
#define _HN_HNMailData_h__

#include "HNNetExport.h"
#include <vector>
#include "cocos2d.h"
#include <string>
#include "HNMailMessageHead.h"

namespace HN
{
	// 数据节点
	struct DataNode
	{
		INT mailID;			// 邮件ID
		INT	iState;			// 邮件状态  0无附件未读，1无附件已读，2有附件未读，3有附件已读未领取，4有附件已读已领取
		INT	iKeepTime;		// 邮件有效时间（多少天）
		bool isSelect;		// 是否选中
		LLONG iMoney;		// 附件金币数
		LLONG iJewels;		// 附件钻石数
		LLONG iLotteries;	// 附件奖券数
		LLONG iSendTime;	// 邮件发送时间
		std::string title;	// 邮件标题
		std::string name;	// 发件人
		std::string message;// 正文

		DataNode()
		{
			mailID = 0;
			iState = 0;
			iKeepTime = 0;
			isSelect = false;
			iMoney = 0;
			iJewels = 0;
			iLotteries = 0;
			iSendTime = 0;
			title = "";
			name = "";
			message = "";
		}
	};

	class HNMailData
	{
		typedef std::function<void(DataNode*, INT)> TransformMailFunc;

	public:
		// 俱乐部数据单例
		static HNMailData* getInstance();

		// 查询邮件记录
		void getMailsInfo();

		// 获取系统公告
		void getSysMsg();

		// 清空数据
		void clear();


	public:
		// 添加邮件
		void addMail(MSG_GP_O_Mail_List_Data* mail);

		// 清理邮件数据
		void deleteMail(INT mailID);

		// 获取邮件
		DataNode* getMail(INT mailID);

		// 获取邮件
		DataNode* getMailAtIndex(INT index);

		// 更新邮件状态
		void updateMail(DataNode* data);

		// 获取邮件数量
		INT getMailsCount() { return _mails.size(); };

		// 遍历邮件数据
		void transform(const TransformMailFunc& func);

		// 已缓存邮件内容
		bool isRead(INT mailID);

		void dispatch(std::string name, void* data = nullptr);

	
	private:
		// 获取系统公告回调
		bool getSysMsgCallback(HNSocketMessage* socketMessage);

		// 查询邮件列表回调
		bool getMailsInfoCallback(HNSocketMessage* socketMessage);

		// 邮件列表更新回调
		bool mailUpdateCallback(HNSocketMessage* socketMessage);

	private:
		std::map<INT, DataNode> _mails;//邮件信息
		EventListenerCustom*	_listener = nullptr;	// 断线重连成功监听

	private:
		HNMailData();

		~HNMailData();
	};
}

#define MailData() HNMailData::getInstance()

#endif // _HN_HNClubData_h__
