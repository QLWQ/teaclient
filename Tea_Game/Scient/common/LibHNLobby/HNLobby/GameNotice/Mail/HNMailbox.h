/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_HNMailbox_h__
#define _HN_HNMailbox_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"
#include "HNMailData.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;

// 邮件系统
class Mailbox : public HNLayer, public extension::TableViewDataSource, public extension::TableViewDelegate
{
public:
	typedef std::function<void()> UpdateCallBack;
	UpdateCallBack	onUpdateCallBack = nullptr;

public:
	// 初始化
	virtual bool init() override;

	// 显示弹窗
	virtual void onEnter() override;

	// 构造函数
	Mailbox();

	// 析构函数
	virtual ~Mailbox();

	// 创建弹窗
	CREATE_FUNC(Mailbox);

private:
	void closeFunc() override;

	// 加载列表UI
	void loadListUI(Node* parent);

	// 加载正文UI
	void loadTextUI(Node* parent);

	// 加载操作按钮
	void loadButton(Node* parent);

	// 更新按钮状态
	void updateButtonState();

private:
	// 阅读邮件
	void readMail(int mailID);

	// 删除邮件
	void sendDeleteMail(int mailID = 0);

	// 获取邮件附件
	void getAttachment(int mailID = 0);

private:
	// 阅读邮件回调
	bool readMailCallback(HNSocketMessage* socketMessage);

	// 领取附件回调
	bool getAttachmentCallback(HNSocketMessage* socketMessage);

	// 删除邮件回调
	bool deleteMailCallback(HNSocketMessage* socketMessage);

private:
	// 显示邮件内容
	void showMail(int mailID);


protected:
	// 列表项大小
	virtual Size tableCellSizeForIndex(extension::TableView *table, ssize_t idx) override;

	// 列表项内容
	virtual extension::TableViewCell* tableCellAtIndex(extension::TableView *table, ssize_t idx) override;

	// 列表项数量
	virtual ssize_t numberOfCellsInTableView(extension::TableView *table) override;

	// 列表项被点击
	virtual void tableCellTouched(extension::TableView* table, extension::TableViewCell* cell) override;

private:
	// 列表
	extension::TableView* _tableView = nullptr;
	ImageView*	_img_none			= nullptr;	// 无邮件提示
	Layout*		_layout_text		= nullptr;	// 正文层
	TextView*	_textView			= nullptr;	// 邮件正文textview
	Button*		_btn_onekeyGet		= nullptr;	// 一键获取
	Button*		_btn_onekeydel		= nullptr;	// 一键删除
	int			_mailID				= 0;

	cocos2d::Size _offset			= cocos2d::Size::ZERO;

	EventListenerCustom* _mailListener = nullptr;	// 邮件变化监听
};

#endif // _HN_HNMailbox_h__
