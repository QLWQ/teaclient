/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNMailData.h"
#include "HNCommon/HNCommonMarco.h"
#include "../GameNotice.h"

namespace HN
{
	HNMailData* HNMailData::getInstance()
	{
		static HNMailData sMailData;
		return &sMailData;
	}

	HNMailData::HNMailData()
	{
		// 重连成功通知
		_listener = EventListenerCustom::create(RECONNECTION, [=](EventCustom* event) {

			PlatformLogic()->removeEventSelector(MDM_GP_MAIL, ASS_GP_NOTIFY_NEW_MAIL);
			getMailsInfo();
		});

		Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);
	}

	HNMailData::~HNMailData()
	{
		clear();

		PlatformLogic()->removeEventSelector(MDM_GP_MAIL, ASS_GP_GET_SYSMSG);
		PlatformLogic()->removeEventSelector(MDM_GP_MAIL, ASS_GP_MAIL_LIST);
		PlatformLogic()->removeEventSelector(MDM_GP_MAIL, ASS_GP_NOTIFY_NEW_MAIL);
		Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
	}

	// 清空数据
	void HNMailData::clear()
	{
		_mails.clear();
	}

	// 获取系统公告
	void HNMailData::getSysMsg()
	{
		PlatformLogic()->sendData(MDM_GP_MAIL, ASS_GP_GET_SYSMSG, nullptr, 0,
			HN_SOCKET_CALLBACK(HNMailData::getSysMsgCallback, this));
	}

	// 查询邮件记录
	void HNMailData::getMailsInfo()
	{
		clear();

		PlatformLogic()->sendData(MDM_GP_MAIL, ASS_GP_MAIL_LIST, nullptr, 0,
			HN_SOCKET_CALLBACK(HNMailData::getMailsInfoCallback, this));
	}

	// 获取系统公告回调
	bool HNMailData::getSysMsgCallback(HNSocketMessage* socketMessage)
	{
		if (0 == socketMessage->messageHead.bHandleCode)
		{
			CHECK_SOCKET_DATA_RETURN(MSG_GP_O_SysMsg, socketMessage->objectSize, true,
				"MSG_GP_O_SysMsg size of error!");
			auto data = (MSG_GP_O_SysMsg*)socketMessage->object;

			GameNotice::getInstance()->postMessage(GBKToUtf8(data->szMessage));

			/*time_t tt = time(NULL);
			struct tm* Time = localtime(&tt);
			char showTime[32];
			strftime(showTime, sizeof(showTime), "%T", Time);

			log("SysMsg:%s----Time:%s", GBKToUtf8(data->szMessage), showTime);*/
		}
		else
		{

		}
		return true;
	}

	// 查询邮件列表回调
	bool HNMailData::getMailsInfoCallback(HNSocketMessage* socketMessage)
	{
		if (0 == socketMessage->messageHead.bHandleCode)
		{
			auto head = (MSG_GP_O_Mail_List_Head*)socketMessage->object;

			UINT mailCount = (socketMessage->objectSize - sizeof(MSG_GP_O_Mail_List_Head)) / sizeof(MSG_GP_O_Mail_List_Data);
			auto mail = (MSG_GP_O_Mail_List_Data *)(socketMessage->object + sizeof(MSG_GP_O_Mail_List_Head));

			if (0 == mailCount)
			{
				dispatch("mailUpdate");
				// 邮件列表变化监听
				PlatformLogic()->addEventSelector(MDM_GP_MAIL, ASS_GP_NOTIFY_NEW_MAIL,
					HN_SOCKET_CALLBACK(HNMailData::mailUpdateCallback, this));
			}
			else
			{
				while (mailCount-- > 0)
				{
					addMail(mail++);
				}
			}
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("用户信息错误"));
		}

		return true;
	}

	// 邮件列表更新回调
	bool HNMailData::mailUpdateCallback(HNSocketMessage* socketMessage)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_Mail_Update, socketMessage->objectSize, true,
			"MSG_GP_O_Mail_Update size of error!");
		auto data = (MSG_GP_O_Mail_Update*)socketMessage->object;

		if (data->bNew)
		{
			if (!getMail(data->mail.iMailID))
			{
				addMail(&data->mail);
				dispatch("mailUpdate", &data->mail);

				GameNotice::getInstance()->postMessage(GBKToUtf8("您有新的邮件信息，请注意查看！"));
			}
		}
		else
		{
			if (getMail(data->mail.iMailID))
			{
				deleteMail(data->mail.iMailID);
				dispatch("mailUpdate", &data->mail);
			}
		}

		return true;
	}

	void HNMailData::dispatch(std::string name, void* data/* = nullptr*/)
	{
		EventCustom event(name);
		event.setUserData(data);
		Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
	}

	// 添加邮件
	void HNMailData::addMail(MSG_GP_O_Mail_List_Data* mail)
	{
		if (!mail) return;

		DataNode data;
		data.mailID = mail->iMailID;
		data.title = mail->szMailTitle;
		data.name = mail->szSendName;
		data.iState = mail->bMailState;
		data.iKeepTime = mail->iKeepTime;
		data.iSendTime = mail->i64MailTime;

		updateMail(&data);
	}

	// 清理邮件数据
	void HNMailData::deleteMail(INT mailID)
	{
		auto iter = _mails.find(mailID);
		if (iter != _mails.end())
		{
			_mails.erase(iter);
		}
	}

	// 获取邮件
	DataNode* HNMailData::getMail(INT mailID)
	{
		auto iter = _mails.find(mailID);
		if (iter != _mails.end())
		{
			return &iter->second;
		}
		return nullptr;
	}

	// 获取邮件
	DataNode* HNMailData::getMailAtIndex(INT index)
	{
		int idx = 0;
		for (auto &mail : _mails)
		{
			if (idx == index)
			{
				return &mail.second;
			}
			idx++;
		}
		return nullptr;
	}

	// 更新邮件状态
	void HNMailData::updateMail(DataNode* data)
	{
		auto iter = _mails.find(data->mailID);
		if (iter != _mails.end())
		{
			iter->second = *data;
		}
		else
		{
			_mails.insert(std::make_pair(data->mailID, *data));
		}
	}

	// 遍历邮件数据
	void HNMailData::transform(const TransformMailFunc& func)
	{
		for (auto &mail : _mails)
		{
			func(&mail.second, mail.first);
		}
	}

	// 已缓存邮件内容
	bool HNMailData::isRead(INT mailID)
	{
		DataNode* data = getMail(mailID);
		if (data && !data->message.empty())
		{
			return true;
		}
		return false;
	}
}
