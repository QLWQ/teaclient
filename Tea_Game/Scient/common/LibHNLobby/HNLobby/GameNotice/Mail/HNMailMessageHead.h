/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _HN_HNMailMessageHead_H_
#define _HN_HNMailMessageHead_H_

#include "HNBaseType.h"


/********************************************************************************************/
//俱乐部功能
/********************************************************************************************/
#define MDM_GP_MAIL  116						// 邮件系统主ID

enum HALL_ASSID_MAIL							// MDM_GP_MAIL
{
	ASS_GP_MAIL_LIST		= 0,				// 获取邮件列表
	ASS_GP_OPEN_MAIL		= 1,				// 打开邮件
	ASS_GP_GET_ATTACHMENT	= 2,				// 获取附件
	ASS_GP_DEL_MAIL			= 3,				// 删除邮件
	ASS_GP_NOTIFY_NEW_MAIL	= 4,				// 提示新邮件
	ASS_GP_GET_SYSMSG		= 5,				// 获取系统跑马灯消息
};

#pragma pack(1)

// 获取邮件列表
struct MSG_GP_O_Mail_List_Head
{
	int		iDataNum;				// 邮件总数
};
struct MSG_GP_O_Mail_List_Data
{
	int		iMailID;				// 邮件ID
	char	szMailTitle[25];		// 邮件标题
	char	szSendName[25];			// 发件人
	LLONG	i64MailTime;			// 发件时间
	BYTE	bMailState;				// 邮件状态  0无附件未读，1无附件已读，2有附件未读，3有附件已读未领取，4有附件已读已领取
	BYTE	iKeepTime;				// 有效时间（天数）
};

// 更新邮件列表（新增邮件，删除邮件）
struct MSG_GP_O_Mail_Update
{
	int		iUserID;				// ID
	bool	bNew;					// true新邮件，false删除邮件
	MSG_GP_O_Mail_List_Data mail;	// 邮件内容
};

// 打开邮件
struct MSG_GP_I_Open_Mail
{
	int		iMailID;
};

struct MSG_GP_O_Open_Mail
{
	int		iMailID;				// 邮件ID
	BYTE	bMailState;				// 邮件状态
	int		iJewels;				// 钻石附件
	int		iLotteries;				// 奖券附件
	LLONG	i64Money;				// 金币附件
	char	szMailMessage[650];		// 邮件正文
};

// 获取附件
struct MSG_GP_I_Get_Attachment
{
	int		iMailID;
};
struct MSG_GP_O_Get_Attachment
{
	int		iMailID;
	int		iJewels;
	int		iLotteries;
	LLONG	i64Money;
	BYTE	bMailState;
};

// 删除邮件
struct MSG_GP_I_Delete_Mail
{
	int		iMailID;
};
struct MSG_GP_O_Delete_Mail
{
	int		iMailID;
	BYTE	bMailState;
};

// 系统消息 （跑马灯）
struct MSG_GP_O_SysMsg
{
	CHAR	szMessage[150];		//消息内容
};

#pragma pack()

#endif // !_HN_HNMailMessageHead_H_
