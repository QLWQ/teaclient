/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameNotice.h"
#include "cocostudio/CocoStudio.h"

using namespace cocostudio;
using namespace ui;

// 公告布局文件
static const char* NOTICE_PATH = "platform/notice/noticeNode.csb";

static GameNotice* __g_ganme_notice__ = nullptr;

GameNotice::GameNotice()
{
	Size winSize = Director::getInstance()->getWinSize();
	_defaultPos = Vec2(winSize.width / 2, winSize.height * 0.87f);
}

GameNotice::~GameNotice()
{
	
}

GameNotice* GameNotice::getInstance()
{
	if (nullptr == __g_ganme_notice__)
	{
		__g_ganme_notice__ = new GameNotice();
	}
	return __g_ganme_notice__;
}

bool GameNotice::init()
{
	Size winSize = Director::getInstance()->getWinSize();
	_parent = Director::getInstance()->getRunningScene();
	// 布局
	_node = CSLoader::createNode(NOTICE_PATH);
	CCAssert(_node != nullptr, "node null");
	
	_parent->addChild(_node);

	_node->setPosition(_defaultPos.x,_defaultPos.y-15);


	auto layout_notice = dynamic_cast<Layout*>(_node->getChildByName("Panel_bg"));
	_showSize = layout_notice->getContentSize();

	// 内容
	_text = dynamic_cast<Text*>(layout_notice->getChildByName("Text_notice"));
	_text->setString("");

	_timeLine = CSLoader::createTimeline(NOTICE_PATH);
	_node->runAction(_timeLine);
	//_timeLine->play("begin", false);

	return true;
}

void GameNotice::postMessage(const std::string& message, int repeat, Vec2 pos)
{
	// 过滤换行符
	std::string text1 = message;
	std::string text2 = Tools::replace_all_distinct(text1, "\r", "");
	std::string msg = Tools::replace_all_distinct(text2, "\n", "");

	//下一帧再检测是否有切换场景操作
	Director::getInstance()->getScheduler()->schedule([=](float fDelta)
	{
		if (!_node || _parent != Director::getInstance()->getRunningScene())
		{
			_runing = false;
			clear();
			init();
		}

		_notifyMsgs.push(NotifyItem(msg, repeat));

		if (pos != Vec2::ZERO && _node->getPosition() != pos)
		{
			_node->setPosition(pos);
			_node->setZOrder(10);
		}

		if (!_notifyMsgs.empty()) start();

	}, this, 0.0f, 0, 0.0f, false, "delayShowMsg");
}

void GameNotice::moveWord()
{		
	if (_text->getString().empty())
	{
		updateNextMessage();
	}
	else
	{
		_text->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.02f, Vec2(-2.0f, 0)), CallFunc::create([=]() {

			if (_text->getPositionX() <= -_wordSize.width / 2)
			{
				updateNextMessage();
			}
		}), nullptr)));
	}
}

void GameNotice::start()
{
	if (!_runing)
	{
		_runing = true;
		moveWord();

		// 播放喇叭
		_timeLine->play("laba", true);
	}
}

void GameNotice::stop()
{
	if (_runing)
	{
		_runing = false;
		_text->setString("");
		if (_sure) _sure();

		// 停止播放喇叭
		if (_timeLine->isPlaying()) {
			_timeLine->stop();
		}

		_node->removeFromParent();
		_node = nullptr;	
	}
}

void GameNotice::clear()
{
	std::queue<NotifyItem> empty;
	_notifyMsgs.swap(empty);
}

void GameNotice::updateNextMessage()
{
	_text->stopAllActions();

	if (!_notifyMsgs.empty())
	{
		NotifyItem msg = _notifyMsgs.front();
		std::cout << msg.message << endl;
		_text->setString(msg.message);
		_wordSize = _text->getContentSize();
		_text->setPositionX(_showSize.width + _wordSize.width / 2);
		
		_notifyMsgs.pop();
		if (msg.repeat-- > 0)
		{
			_notifyMsgs.push(msg);
		}
		moveWord();
	}
	else
	{
		stop();
	}
}

void GameNotice::setCallBack(std::function<void ()> sure)
{
	_sure = sure;
}

