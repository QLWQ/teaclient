#ifndef __HN_AgentLayer_h__
#define __HN_AgentLayer_h__


#include "HNLogicExport.h"
#include "HNUIExport.h"
#include "HNHttp/HNHttp.h"
#include "cocos2d.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;

namespace HN
{

	struct AgentStruct
	{
		int rid = 0;  //推广奖励ID
		int count     = 0;  //累计人数
		int num     = 0;  //推广奖励要求人数
		int money    = 0;  //红包金额
		int jewel     = 0;  //房卡数
		int recevied    = 0; //领取情况
	};
	struct AgentCommission
	{
		int fromid = 0;           //下级次级ID
		int  consume = 0;     //消费金额
		std::string addtime = "";    //消费时间

	};
	struct AgentMy
	{
		std::string userid = "";       //
		double balance = 0;           //余额
		double onesum = 0;           //一级金额
		double twosum = 0;           //次级金额
		int count = 0;             //一级人数
	};
	class AgentLayer : public HNLayer , public HNHttpDelegate
	{
		public:
			AgentLayer();

			virtual ~AgentLayer();

			// 初始化
			virtual bool init() override;

			CREATE_FUNC(AgentLayer);
	    protected:
			//创建页面
			void creatPage();

			//按钮回调
			void ButtonCallBack(Ref* pSender);
			//网络回调
			void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData);
			//推广返利数据
			void DataFanLi(const std::string& data);
			//返利界面
			void PageFanLi();
			//累计界面
			void PageLeiJi();
			//我的推广界面
			void PageMy(AgentMy data);
			//佣金页面
			void PageCommission(int count);
			void CreatItem(int count);
			//累计推广数据
			void dealQueryGetInfoCumulative(const std::string& data);
			//我的推广数据
			void dealQueryGetInfoWode(const std::string& data);
			//佣金数据
			void dealQueryGetInfoCommission(const std::string& data);
			//提现提示
			void TipCashWithdrawal(const std::string& data);
			//领取奖励提示
			void TipReward(const std::string& data);

			void JumpWechat(float dt);
			cocos2d::Node* getQR(std::string uri, std::string colorStr);
	    private:
			
			bool 	_isTouch         = true;      //防误触判断
			std::vector<Button*>    _VecButton;   //按钮容器
		    std::vector<Layout*>    _vecLayout;   //基础容器
			ui::ScrollView*         _LeiJi;	    //累计福利
			ui::Layout*           _FanLi;     //推广返利
			ui::Layout*           _WoDe;	    //我的推广	
			ui::Layout*           _Rule;     //推广规则
			Sprite*             _CopyTip;    //复制成功提示
			std::vector<AgentStruct>         _vecLeiJiData;    //累计页面数据   AgentCommission
			std::vector<AgentCommission>     _vecOnelv;      //一级数据
			std::vector<AgentCommission>     _vecTwolv;      //次级数据
			ui::ScrollView*         _Commission;	    //佣金
			Text*               _textLeiji;           //累计金额显示
			double               _LeijiMoney = 0.00;  //累计金额
			std::vector<Button*>               _BtnState;          //领取状态
			int                            _Rid = -1;

	};
}

#endif