//2019.3.5   C
//推广返利界面


#include "AgentLayer.h"
#include "../GameChildLayer/QR_Encode.h"
#include "UMeng/UMengSocial.h"
#include "../GameChildLayer/GameShareLayer.h"


namespace HN
{
	//推广界面
	static const char* SPREAD_CSB      = "platform/SpreadUi/AgentLayer.csb";
	static const char* COMMISSION_CSB = "platform/SpreadUi/details_Node.csb";
	static const char* COMMISSIONITEM_CSB = "platform/SpreadUi/detailsItem_Node.csb";
	static const char* ITEM_CSB        =  "platform/SpreadUi/accumulateItem_Node.csb";
	static const char* ICON_SPRITE = "platform/SpreadUi/res/Icon.png";
	static const char* SATISFY_PASH     = "platform/SpreadUi/res/lq.png";
	static const char* NOSATISFY_PASH   = "platform/SpreadUi/res/dlq.png";
	static const char* RECEIVED_PASH    = "platform/SpreadUi/res/ylq.png";
	static const char* FRAMEONE_PASH = "platform/SpreadUi/res/anniuxuanzhong.png";
	static const char* FRAMETWO_PASH = "platform/SpreadUi/res/anniuweixuanzhong.png";
	AgentLayer::AgentLayer()
	{

	}
	AgentLayer::~AgentLayer()
	{
		HNHttpRequest::getInstance()->removeObserver(this);
	}

	bool AgentLayer::init()
	{
		if (!HNLayer::init()) {
			return false;
		}
		
    //LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在查询推广员信息"), 25);
	//	queryBindInfo();
		creatPage();
		//累计信息请求
		std::string url = StringUtils::format("http://alipay.qp888m.com/app/get_extend_info.php?userid=%d", PlatformLogic()->loginResult.dwUserID);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestLeiji", cocos2d::network::HttpRequest::Type::POST, url);
		//我的推广信息
		std::string wourl = StringUtils::format("http://alipay.qp888m.com/app/get_bouns_info.php?userid=%d", PlatformLogic()->loginResult.dwUserID);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestWode", cocos2d::network::HttpRequest::Type::POST, wourl);
		//佣金信息
		std::string yongurl = StringUtils::format("http://alipay.qp888m.com/app/get_consume_info.php?userid=%d", PlatformLogic()->loginResult.dwUserID);
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("requestYong", cocos2d::network::HttpRequest::Type::POST, yongurl);


		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在查询页面信息"), 25);
		return true;
	}
	void AgentLayer::creatPage()
	{
		auto node = CSLoader::createNode(SPREAD_CSB);
		node->setPosition(Vec2(0,0));
		addChild(node);

		auto PlaneBg = dynamic_cast<Layout*>(node->getChildByName("Panel_Bg"));
		//退出按钮
		auto Exit = dynamic_cast<Button*>(node->getChildByName("Button_Exit"));
		Exit->addClickEventListener(CC_CALLBACK_1(AgentLayer::ButtonCallBack, this));
		//推广返利
		auto Button_FanLi = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_FanLi"));
		Button_FanLi->addClickEventListener(CC_CALLBACK_1(AgentLayer::ButtonCallBack, this));
		_VecButton.push_back(Button_FanLi);

		_FanLi = dynamic_cast<Layout*>(PlaneBg->getChildByName("Panel_FanLi"));
		_FanLi->setVisible(true);
		_CopyTip = dynamic_cast<Sprite*>(_FanLi->getChildByName("Sprite_CopyTip"));
		_CopyTip->setVisible(false);

		//我的推广
		auto Button_WoDe = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_WoDe"));
		Button_WoDe->addClickEventListener(CC_CALLBACK_1(AgentLayer::ButtonCallBack, this));
		_VecButton.push_back(Button_WoDe);

		/*_WoDe = dynamic_cast<Layout*>(PlaneBg->getChildByName("Panel_WoDe"));
		_WoDe->setVisible(false);
	
		_Rule = dynamic_cast<Layout*>(PlaneBg->getChildByName("Panel_Rule"));
		_Rule->setVisible(false);*/
		//累计福利
		auto Button_Accumulate = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Accumulate"));
		Button_Accumulate->addClickEventListener(CC_CALLBACK_1(AgentLayer::ButtonCallBack, this));
		_VecButton.push_back(Button_Accumulate);

		_LeiJi = dynamic_cast<ui::ScrollView*>(PlaneBg->getChildByName("ScrollView_accumulate"));
		_LeiJi->setVisible(false);

		PageFanLi();

	}

	void AgentLayer::ButtonCallBack(Ref* pSender)
	{
	
		auto btn = (Button*)pSender;
	
		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
		auto winSize = Director::getInstance()->getWinSize();

		std::string name(btn->getName());
		if (btn->getName() != "Button_ZhiYing")
		{
			for (auto btnLeft : _VecButton)
			{
				if (btn->getName() == btnLeft->getName())
				{
					btnLeft->setOpacity(255);
				}
				else
				{
					btnLeft->setOpacity(0);
				}
			}
		}
		if (name.compare("Button_Exit") == 0)
		{
			this->runAction(Sequence::create(MoveTo::create(0.2f, Vec2(-1500, 0)), CallFunc::create([=]()
			   {
				       this->removeFromParentAndCleanup(true);
			}), nullptr));
			
		}
		else if (name.compare("Button_FanLi")==0)
		{
			_LeiJi->setVisible(false);
			_FanLi->setVisible(true);
			_WoDe->setVisible(false);
			_Rule->setVisible(false);
		}
		else if (name.compare("Button_WoDe")==0)
		{
			_LeiJi->setVisible(false);
			_FanLi->setVisible(false);
			_WoDe->setVisible(true);
			_Rule->setVisible(false);
		}
		else if (name.compare("Button_ZhiYing")==0)
		{
			_LeiJi->setVisible(false);
			_FanLi->setVisible(false);
			_WoDe->setVisible(false);
			_Rule->setVisible(false);
		}
		else if (name.compare("Button_Accumulate")==0)
		{
			_LeiJi->setVisible(true);
			_FanLi->setVisible(false);
			_WoDe->setVisible(false);
			_Rule->setVisible(false);
		}
	}


	void AgentLayer::DataFanLi(const std::string& data)
	{

	}
	//返利页面
	void AgentLayer::PageFanLi()
	{
		//复制ID到剪贴板
		std::string  strID = StringUtils::format("%d", PlatformLogic()->loginResult.dwUserID);
		auto TextID = dynamic_cast<Text*>(_FanLi->getChildByName("Text_IDnumber"));
		TextID->setText(strID);
		auto btnCopy = dynamic_cast<Button*>(_FanLi->getChildByName("Button_Copy"));
		btnCopy->addClickEventListener([=](Ref* sender){
			std::string idstr = TextID->getString();
			Application::getInstance()->copyToClipboard(idstr);
			_CopyTip->setVisible(true);
			_CopyTip->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=](){
				_CopyTip->setVisible(false);
			}), nullptr));
		});
		//复制专属链接
		auto zhuangSshu = dynamic_cast<Text*>(_FanLi->getChildByName("Text_ZhuanShu"));

		std::string  strZhungShu = StringUtils::format("http://alipay.qp888m.com/wx/\ntoShare.php?game_userid=%d", PlatformLogic()->loginResult.dwUserID);
		zhuangSshu->setText(strZhungShu);
		auto btnZCopy = dynamic_cast<Button*>(_FanLi->getChildByName("Button_ZhuangShu"));
		btnZCopy->addClickEventListener([=](Ref* sender){
			//std::string Zhuangstr = zhuangSshu->getString();
			Application::getInstance()->copyToClipboard(StringUtils::format("http://alipay.qp888m.com/wx/toShare.php?game_userid=%d", PlatformLogic()->loginResult.dwUserID));
			_CopyTip->setVisible(true);
			_CopyTip->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=](){
				_CopyTip->setVisible(false);
			}), nullptr));
		});
		//跳转微信
		auto BtnWeChat = dynamic_cast<Button*>(_FanLi->getChildByName("Button_WeChat"));
		BtnWeChat->addClickEventListener([=](Ref* sender){
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在跳转微信..."), 36);
			this->scheduleOnce(schedule_selector(AgentLayer::JumpWechat), 3.0f);
		});
		//二维码	
		auto nodeqr = getQR(StringUtils::format("http://alipay.qp888m.com/wx/toShare.php?game_userid=%d",PlatformLogic()->loginResult.dwUserID), "hello");
		nodeqr->setPosition(130, 100);
		nodeqr->setContentSize(Size(180,180));
		_FanLi->addChild(nodeqr);

		auto GameIcon = Sprite::create(ICON_SPRITE);
		nodeqr->addChild(GameIcon);
		GameIcon->setPosition(90, 90);

		//保存按钮
		auto btnSave = dynamic_cast<Button*>(_FanLi->getChildByName("Button_Save"));
		btnSave->addClickEventListener([=](Ref*){
		
			//UMengSocial::getInstance()->doShare("", "", "", "", 1, nodeqr);
			auto shareLayer = GameShareLayer::create();
			shareLayer->setName("shareLayer");
			shareLayer->SetShareInfo("", "", "", "", nodeqr);
			shareLayer->show();
		});
		//代理指引按钮
		auto btnGuide = dynamic_cast<Button*>(_FanLi->getChildByName("Button_ZhiYing"));
		btnGuide->addClickEventListener(CC_CALLBACK_1(AgentLayer::ButtonCallBack, this));
	}
	void AgentLayer::JumpWechat(float dt)
	{
		Application::getInstance()->openURL("weixin://");
		CCLOG("123");
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}
	//累计页面
	void AgentLayer::PageLeiJi()
	{
		if (_vecLeiJiData.size() > 0)
		{
			_BtnState.clear();
			int count = 0;
			for (auto structData : _vecLeiJiData)
			{
				auto node = CSLoader::createNode(ITEM_CSB);
				node->setPosition(Vec2(_LeiJi->getContentSize().width / 2 - 80, _LeiJi->getContentSize().height - 104 - count * 65.5));
				_LeiJi->addChild(node);

				auto PlaneBg = dynamic_cast<Layout*>(node->getChildByName("Panel_BG"));
				auto textRid = dynamic_cast<Text*>(PlaneBg->getChildByName("Text_Codition"));
				textRid->setString(StringUtils::format(GBKToUtf8("满%d人"),structData.num));

				auto textNum = dynamic_cast<Text*>(PlaneBg->getChildByName("Text_People"));
				textNum->setString(StringUtils::format(GBKToUtf8("%d人"), structData.count));

				auto textMoney = dynamic_cast<Text*>(PlaneBg->getChildByName("Text_NRed"));
				textMoney->setString(StringUtils::format("x%d", structData.money));

				auto textJewel = dynamic_cast<Text*>(PlaneBg->getChildByName("Text_nFanCard"));
				textJewel->setString(StringUtils::format("x%d", structData.jewel));

			
				auto btnState = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_1"));
				_BtnState.push_back(btnState);
				btnState->addClickEventListener([=](Ref* sender){
					_Rid = structData.rid;
					std::string stateurl = StringUtils::format("http://alipay.qp888m.com/wxhongbao/CardAndCash.php");
					std::string param = StringUtils::format("{  \"UserID\" :%d, \"RID\":%d }", PlatformLogic()->loginResult.dwUserID, structData.rid);
					HNHttpRequest::getInstance()->addObserver(this);
					HNHttpRequest::getInstance()->request("requestReceiveLeiji", cocos2d::network::HttpRequest::Type::POST, stateurl, param);

					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在兑换中..."), 25);

				});
				if (structData.recevied == -1)  //不满足
				{
					btnState->loadTextures(NOSATISFY_PASH, NOSATISFY_PASH, NOSATISFY_PASH);
					btnState->setEnabled(false);
				}
				else if (structData.recevied == 0)  //满足未领取
				{
					btnState->loadTextures(SATISFY_PASH, SATISFY_PASH, SATISFY_PASH);
					btnState->setEnabled(true);
				}
				else   //满足已领取
				{
					btnState->loadTextures(RECEIVED_PASH, RECEIVED_PASH, RECEIVED_PASH);
					btnState->setEnabled(false);
				}
				count++;
			}
		}
	}
	void AgentLayer::PageMy(AgentMy data)
	{
		auto  TextOne = dynamic_cast<Text*>(_WoDe->getChildByName("Text_YongJin1"));
		TextOne->setString(StringUtils::format("%0.2f", data.onesum));

		auto  TextTwo = dynamic_cast<Text*>(_WoDe->getChildByName("Text_YongJin2"));
		TextTwo->setString(StringUtils::format("%0.2f", data.twosum ));

		auto  TextLeiji = dynamic_cast<Text*>(_WoDe->getChildByName("Text_LeiJI"));
		TextLeiji->setString(StringUtils::format("%0.2f", data.balance ));
		_LeijiMoney = data.balance;
		_textLeiji = TextLeiji;

		auto  peopleCount = dynamic_cast<Text*>(_WoDe->getChildByName("Text_RenSshu"));
		peopleCount->setString(StringUtils::format(GBKToUtf8("%d人"), data.count));


		auto btnOne = dynamic_cast<Button*>(_WoDe->getChildByName("Button_CKXQ1"));
		auto btnTwo = dynamic_cast<Button*>(_WoDe->getChildByName("Button_CKXQ"));
		auto btnReceive = dynamic_cast<Button*>(_WoDe->getChildByName("Button_LingQu"));
		btnReceive->addClickEventListener([=](Ref* sender){
			if (_LeijiMoney > 10.0)
			{

				std::string Receiveurl = StringUtils::format("http://alipay.qp888m.com/wxhongbao/DrawCash.php");
				std::string param = StringUtils::format("{  \"UserID\" :%d , \"CashNum\":%s }", PlatformLogic()->loginResult.dwUserID, GBKToUtf8(TextLeiji->getString().c_str()));
				HNHttpRequest::getInstance()->addObserver(this);
				HNHttpRequest::getInstance()->request("requestReceive", cocos2d::network::HttpRequest::Type::POST, Receiveurl, param);

				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在兑换中..."), 25);
			}
			else
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("金额不足，领取金额需大于等于10.."));
			}
			
		});
		btnOne->addClickEventListener([=](Ref* sender){
			PageCommission(1);
		});
		
		btnTwo->addClickEventListener([=](Ref* sender){
			PageCommission(2);
		});

	}
	void AgentLayer::PageCommission(int count)
	{
		auto node = CSLoader::createNode(COMMISSION_CSB);
		node->setPosition(Vec2(640, 360));
		addChild(node);

		auto PlaneBg = dynamic_cast<Layout*>(node->getChildByName("Panel_Bg"));
		auto btnOne = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Noe"));
		auto btnTwo = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Two"));
		btnOne->addClickEventListener([=](Ref* sender){
			btnOne->loadTextures(FRAMEONE_PASH, FRAMEONE_PASH, FRAMEONE_PASH);
			btnTwo->loadTextures(FRAMETWO_PASH, FRAMETWO_PASH, FRAMETWO_PASH);
			CreatItem(1);		
		});

		btnTwo->addClickEventListener([=](Ref* sender){
			btnTwo->loadTextures(FRAMEONE_PASH, FRAMEONE_PASH, FRAMEONE_PASH);
			btnOne->loadTextures(FRAMETWO_PASH, FRAMETWO_PASH, FRAMETWO_PASH);
			CreatItem(2);
			
		});
		auto btnExit = dynamic_cast<Button*>(PlaneBg->getChildByName("Button_Exit"));
		btnExit->addClickEventListener([=](Ref* sender){
			node->removeAllChildrenWithCleanup(true);
		});
		_Commission = dynamic_cast<ui::ScrollView*>(PlaneBg->getChildByName("ScrollView_1"));
		if (count == 1)
		{
			btnOne->loadTextures(FRAMEONE_PASH, FRAMEONE_PASH, FRAMEONE_PASH);
			btnTwo->loadTextures(FRAMETWO_PASH, FRAMETWO_PASH, FRAMETWO_PASH);
			CreatItem(1);
		}
		else
		{
			btnTwo->loadTextures(FRAMEONE_PASH, FRAMEONE_PASH, FRAMEONE_PASH);
			btnOne->loadTextures(FRAMETWO_PASH, FRAMETWO_PASH, FRAMETWO_PASH);
			CreatItem(2);
		}
		
	}
	void AgentLayer::CreatItem(int count)
	{
		_Commission->removeAllChildren();
		std::vector<AgentCommission> temporary;
		if (count == 1)
		{
			temporary = _vecOnelv;
		}
		else
		{
			temporary = _vecTwolv;
		}
		if (temporary.size() > 0)
		{
			int count = temporary.size();
			if (count < 5)		count = 5;
			_Commission->setInnerContainerSize(Size(_Commission->getContentSize().width, count * 95));
			for (int i = 0; i < temporary.size(); i++)
			{
				auto node = CSLoader::createNode(COMMISSIONITEM_CSB);
				node->setPosition(_Commission->getContentSize().width / 2, _Commission->getContentSize().height - 20 - i * 90);
				_Commission->addChild(node);

				auto ItemBg = dynamic_cast<Sprite*>(node->getChildByName("Sprite_Bg"));
				auto text1 = dynamic_cast<Text*>(ItemBg->getChildByName("Text_ID"));
				text1->setString(StringUtils::format("%d", temporary[i].fromid));
				auto text2 = dynamic_cast<Text*>(ItemBg->getChildByName("Text_Momey"));
				text2->setString(StringUtils::format(GBKToUtf8("%d元"), temporary[i].consume));
				auto text3 = dynamic_cast<Text*>(ItemBg->getChildByName("Text_Time"));
			
				text3->setString(StringUtils::format("%s", temporary[i].addtime.c_str()));

			}
		}
		
		
	}
	void AgentLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		if (!isSucceed) return;

		if (requestName.compare("requestLeiji") == 0)
		{
			//dealQueryGetInfoCumulative(responseData);
		}
		else if (requestName.compare("requestWode") == 0)
		{
			//dealQueryGetInfoWode(responseData);
		}
		else if (requestName.compare("requestYong") == 0)
		{
			//dealQueryGetInfoCommission(responseData);
		}
		else if (requestName.compare("requestReceive") == 0)
		{
			//TipCashWithdrawal(responseData);
		}
		else if (requestName.compare("requestReceiveLeiji") == 0)
		{
			
			TipReward(responseData);
			
		}
	}
	void AgentLayer::dealQueryGetInfoCumulative(const std::string& data)
	{
		
		AgentStruct LeiJiData;
		_vecLeiJiData.clear();

		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
		
		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.IsObject())
		{

			rapidjson::Value& array = doc["condition"];
			for (int i = 0; i < array.Size(); i++)
			{
				if (array[i].IsObject())
				{
					rapidjson::Value& object = array[i];
					LeiJiData.rid = object["rid"].GetInt();
					LeiJiData.num = object["num"].GetInt();
					LeiJiData.money = object["money"].GetInt();
					LeiJiData.jewel = object["jewel"].GetInt();
					LeiJiData.recevied = object["received"].GetInt(); 
					LeiJiData.count = object["count"].GetInt();

					_vecLeiJiData.push_back(LeiJiData);
				}
			}
			PageLeiJi();
		}

	}
	void AgentLayer::dealQueryGetInfoWode(const std::string& data)
	{
		AgentMy AgentMyData;
		
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.IsObject())
		{
			AgentMyData.balance = doc["balance"].GetDouble();
			AgentMyData.count = doc["count"].GetInt();
			AgentMyData.onesum = doc["onesum"].GetDouble();
			AgentMyData.twosum = doc["twosum"].GetDouble();
			//PageMy(AgentMyData);
		}
	}
	//佣金数据
	void AgentLayer::dealQueryGetInfoCommission(const std::string& data)
	{
		AgentCommission AgentCommissionData;
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.IsObject())
		{
			rapidjson::Value& arrayOnelv = doc["onelv"];
			if (arrayOnelv.Size() > 0)
			{
				for (int i = 0; i < arrayOnelv.Size(); i++)
				{
					AgentCommissionData.fromid = arrayOnelv[i]["fromid"].GetInt();
					AgentCommissionData.addtime = arrayOnelv[i]["addtime"].GetString();
					AgentCommissionData.consume = arrayOnelv[i]["consume"].GetInt();
				}
				_vecOnelv.push_back(AgentCommissionData);
			}
			rapidjson::Value& arrayTwolv = doc["twolv"];
			if (arrayTwolv.Size() > 0)
			{
				for (int i = 0; i < arrayTwolv.Size(); i++)
				{
					AgentCommissionData.fromid = arrayTwolv[i]["fromid"].GetInt();
					AgentCommissionData.addtime = arrayTwolv[i]["addtime"].GetString();
					AgentCommissionData.consume = arrayTwolv[i]["consume"].GetInt();
				}
				_vecTwolv.push_back(AgentCommissionData);
			}
		}
	}
	void AgentLayer::TipCashWithdrawal(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.IsObject())
		{
			int code = -1;
			code = doc["code"].GetInt();
			if (code == 3)
				GamePromptLayer::create()->showPrompt(GBKToUtf8("领取失败，未绑定游戏公众号.."));
			else if (code == 4)
				GamePromptLayer::create()->showPrompt(GBKToUtf8("领取失败，账户余额不足.."));
			else if (code == 0)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("领取成功"));
				_textLeiji->setString("0.00");
				_LeijiMoney = 0;
			}
		}

	}
	void AgentLayer::TipReward(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.IsObject())
		{
			int code = -1;
			code = doc["code"].GetInt();
			if (code == 3)
				GamePromptLayer::create()->showPrompt(GBKToUtf8("领取失败，未绑定游戏公众号.."));
			else if (code == 5)
				GamePromptLayer::create()->showPrompt(GBKToUtf8("领取失败，奖励不存在.."));
			else if (code == 6)
				GamePromptLayer::create()->showPrompt(GBKToUtf8("领取失败，已领取过该奖励.."));
			else if (code == 0)
			{
				if (_Rid > -1)
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("领取成功"));
					_BtnState[_Rid - 1]->setEnabled(false);
					_BtnState[_Rid - 1]->loadTextures(RECEIVED_PASH, RECEIVED_PASH, RECEIVED_PASH);
				}
			}

		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cocos2d::Node* AgentLayer::getQR(std::string uri, std::string colorStr)
	{
		CQR_Encode m_QREncode;
		bool bRet = m_QREncode.EncodeData(0, 0, 1, -1, const_cast<char *> (uri.c_str()));

		// 添加生成图像代码, 这边我采用的是CCDrawNode这个类直接绘制

		if (bRet)
		{
			int nSize = 5;			// 格子大小
			int originalSize = m_QREncode.m_nSymbleSize + (QR_MARGIN * 2);
			CCDrawNode *pQRNode = CCDrawNode::create();

			CCPoint pt[6];
			ccColor4F color;

			pt[0] = Vec2(0, 0);
			pt[1] = Vec2((m_QREncode.m_nSymbleSize + QR_MARGIN * 2)*nSize, (m_QREncode.m_nSymbleSize + QR_MARGIN * 2)*nSize);
			pt[2] = Vec2((m_QREncode.m_nSymbleSize + QR_MARGIN * 2)*nSize, 0);

			pt[3] = pt[0];
			pt[4] = Vec2(0, (m_QREncode.m_nSymbleSize + QR_MARGIN * 2)*nSize);
			pt[5] = pt[1];
			color = ccc4f(1, 1, 1, 1);
			pQRNode->drawPolygon(pt, 6, color, 0, color);

			for (int i = 0; i < m_QREncode.m_nSymbleSize; ++i)
			{
				for (int j = 0; j < m_QREncode.m_nSymbleSize; ++j)
				{
					pt[0] = Vec2((i + QR_MARGIN)*nSize, (j + QR_MARGIN)*nSize);
					pt[1] = Vec2(((i + QR_MARGIN) + 1)*nSize, ((j + QR_MARGIN) + 1)*nSize);
					pt[2] = Vec2(((i + QR_MARGIN) + 1)*nSize, ((j + QR_MARGIN) + 0)*nSize);

					pt[3] = pt[0];
					pt[4] = Vec2(((i + QR_MARGIN) + 0)*nSize, ((j + QR_MARGIN) + 1)*nSize);
					pt[5] = pt[1];
					if (m_QREncode.m_byModuleData[i][j] == 1)
					{
						if (!colorStr.empty()) {
							vector<float> rgb(3);
							char szValue[6];
							strcpy(szValue, colorStr.c_str());

							char * pch;
							pch = strtok(szValue, ":");
							int index = 0;
							while (pch != NULL)
							{
								char ch[6];
								int nValude = 0;
								scanf(pch, "%x", &nValude);
								rgb[index] = (float)nValude / 255;
								pch = strtok(NULL, ":");
								index++;
							}
							color = ccc4f(rgb[0], rgb[1], rgb[2], 1);
						}
						else {
							color = ccc4f(0, 0, 0, 1);
						}
					}
					else
					{
						color = ccc4f(1, 1, 1, 1);
					}
					pQRNode->drawPolygon(pt, 6, color, 0, color);
				}
			}

			return pQRNode;
		}
		return nullptr;
	}


}