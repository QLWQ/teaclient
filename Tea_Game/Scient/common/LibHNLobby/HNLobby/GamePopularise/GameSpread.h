/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_GameSpread_h__
#define __HN_GameSpread_h__


#include "HNLogicExport.h"
#include "HNUIExport.h"
#include "HNHttp/HNHttp.h"
#include "cocos2d.h"

USING_NS_CC;

namespace HN
{
	struct SpreadStruct
	{
		int PrAwardMoney     = 0;
		int PrAwardJewels    = 0;
		int WasPrAwardMoney  = 0;
		int WasPrAwardJewels = 0;
		int money            = 0;
		int jewels           = 0;
		int count            = 0;
		std::string code;

		SpreadStruct()
		{
			memset(this, 0x0, sizeof(SpreadStruct));
			code = "";
		}
	};

	class GameSpread 
		: public HNLayer
		, public ui::EditBoxDelegate
		, public HNHttpDelegate
	{
	public:
		GameSpread();

		virtual ~GameSpread();

		// 初始化
		virtual bool init() override;

		CREATE_FUNC(GameSpread);

	public:
		void show(Node* parent, int orderZ, int tagV);

		void close();

		void setChangeDelegate(MoneyChangeNotify* delegate);

	private:
		// 查询有没有绑定推广码
		void queryBindInfo();

		// 绑定推广码
		void bindPromotionCode(std::string code);

		// 查询奖励和推广码信息
		void queryRewardInfo();

		// 领取推广奖励
		void getReward();

	private:
		void dealQueryBindInfoResp(const std::string& data);

		void dealBindPromotionCodeResp(const std::string& data);

		void dealQueryRewardInfoResp(const std::string& data);

		void dealGetReward(const std::string& data);

	private:
		// 显示绑定页面
		void showBindLayer();

		// 显示领取奖励页面
		void showRewardLayer(SpreadStruct* spreadData);

	private:
		virtual void editBoxReturn(ui::EditBox* editBox) {};

	private:
		virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	protected:
		HNEditBox*				_editBoxCode = nullptr;
		MoneyChangeNotify*		_delegate	= nullptr;
	};

}
#endif // __HN_GameSpread_h__

