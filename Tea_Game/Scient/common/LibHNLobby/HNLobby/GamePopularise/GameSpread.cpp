/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameSpread.h"
#include "HNMarketExport.h"
#include "HNUIExport.h"
#include "HNMarketExport.h"
#include "HNOpenExport.h"
#include "../GameChildLayer/GameShareLayer.h"

namespace HN
{
	// 推广奖励规则
	static const char* SPREAD_CSB		= "platform/SpreadUi/ruleUi.csb";

	// 绑定代理商
	static const char* BIND_CSB			= "platform/SpreadUi/bindUi.csb";


	GameSpread::GameSpread()
	{
	}

	GameSpread::~GameSpread()
	{
		HNHttpRequest::getInstance()->removeObserver(this);
	}

	void GameSpread::show(Node* parent, int orderZ, int tagV)
	{
		parent->addChild(this, orderZ, tagV);
	}

	void GameSpread::close()
	{
		this->removeFromParentAndCleanup(true);
	}

	bool GameSpread::init()
	{
		if (!HNLayer::init()) {
			return false;
		}

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在查询推广员信息"), 25);
		queryBindInfo();

		return true;
	}

	void GameSpread::setChangeDelegate(MoneyChangeNotify* delegate)
	{
		_delegate = delegate;
	}

	void GameSpread::queryBindInfo()
	{
		std::string url = HNPlatformConfig()->getOrderUrl();

		std::string requestData = "type=CheckUserBindPromotionCode";
		requestData.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));

		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("queryBindInfo", cocos2d::network::HttpRequest::Type::POST, url, requestData);
	}

	void GameSpread::bindPromotionCode(std::string code)
	{
		std::string url = HNPlatformConfig()->getOrderUrl();

		std::string requestData = "type=UserBindPromotionCode";
		requestData.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));
		requestData.append("&code=" + code);

		HNHttpRequest::getInstance()->request("bindPromotionCode", cocos2d::network::HttpRequest::Type::POST, url, requestData);
	}

	void GameSpread::queryRewardInfo()
	{
		std::string url = HNPlatformConfig()->getOrderUrl();

		std::string requestData = "type=GetPromotionInfo";
		requestData.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));

		HNHttpRequest::getInstance()->request("queryRewardInfo", cocos2d::network::HttpRequest::Type::POST, url, requestData);
	}

	void GameSpread::getReward()
	{
		std::string url = HNPlatformConfig()->getOrderUrl();

		std::string requestData = "type=UserPromotionSettlement";
		requestData.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));

		HNHttpRequest::getInstance()->request("getReward", cocos2d::network::HttpRequest::Type::POST, url, requestData);
	}

	void GameSpread::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		if (!isSucceed) return;

		if (requestName.compare("queryBindInfo") == 0)
		{
			dealQueryBindInfoResp(responseData);
		}
		else if (requestName.compare("bindPromotionCode") == 0)
		{
			dealBindPromotionCodeResp(responseData);
		}
		else if (requestName.compare("queryRewardInfo") == 0)
		{
			dealQueryRewardInfoResp(responseData);
		}
		else if (requestName.compare("getReward") == 0)
		{
			dealGetReward(responseData);
		}
		else
		{

		}		
	}

	void GameSpread::dealQueryBindInfoResp(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.HasMember("code") && doc["code"].IsString())
		{
			std::string code = doc["code"].GetString();

			if (code.empty())
			{
				showBindLayer();
			}
			else
			{
				queryRewardInfo();
			}
		}
	}

	void GameSpread::dealBindPromotionCodeResp(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.HasMember("res") && doc["res"].IsInt())
		{
			int res = doc["res"].GetInt();
			std::string msg;
			if (1 == res)
			{
				msg = GBKToUtf8("绑定成功");

				if (doc.HasMember("money") && doc["money"].IsInt())
				{
					int money = doc["money"].GetInt();
					if (money > 0)
					{
						PlatformLogic()->loginResult.i64Money += money;
						msg.append(StringUtils::format(GBKToUtf8("，获得%d金币奖励"), money));
					}
				}

				if (doc.HasMember("jewels") && doc["jewels"].IsInt())
				{
					int jewels = doc["jewels"].GetInt();
					if (jewels > 0)
					{
						PlatformLogic()->loginResult.iJewels += jewels;
						msg.append(StringUtils::format(GBKToUtf8("，获得%d房卡奖励"), jewels));
					}
				}
			}
			else
			{
				msg = GBKToUtf8("绑定失败");
			}

			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(msg);
			prompt->setCallBack([=](){

				_delegate->walletChanged();

				close();
			});
		}
	}

	void GameSpread::dealQueryRewardInfoResp(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		SpreadStruct spreadData;

		if (doc.HasMember("code") && doc["code"].IsString())
		{
			spreadData.code = doc["code"].GetString();
		}

		if (doc.HasMember("BindCount") && doc["BindCount"].IsInt())
		{
			spreadData.count = doc["BindCount"].GetInt();
		}

		if (doc.HasMember("money") && doc["money"].IsInt())
		{
			spreadData.money = doc["money"].GetInt();
		}

		if (doc.HasMember("jewels") && doc["jewels"].IsInt())
		{
			spreadData.jewels = doc["jewels"].GetInt();
		}

		if (doc.HasMember("PrAwardMoney") && doc["PrAwardMoney"].IsInt())
		{
			spreadData.PrAwardMoney = doc["PrAwardMoney"].GetInt();
		}

		if (doc.HasMember("PrAwardJewels") && doc["PrAwardJewels"].IsInt())
		{
			spreadData.PrAwardJewels = doc["PrAwardJewels"].GetInt();
		}

		if (doc.HasMember("WasPrAwardMoney") && doc["WasPrAwardMoney"].IsInt())
		{
			spreadData.WasPrAwardMoney = doc["WasPrAwardMoney"].GetInt();
		}

		if (doc.HasMember("WasPrAwardJewels") && doc["WasPrAwardJewels"].IsInt())
		{
			spreadData.WasPrAwardJewels = doc["WasPrAwardJewels"].GetInt();
		}

		showRewardLayer(&spreadData);
	}

	void GameSpread::dealGetReward(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (doc.HasParseError() || !doc.IsObject()) {
			return;
		}

		if (doc.HasMember("res") && doc["res"].IsInt())
		{
			int res = doc["res"].GetInt();
			std::string msg;
			if (1 == res)
			{
				msg = GBKToUtf8("领取成功");

				if (doc.HasMember("money") && doc["money"].IsInt())
				{
					int money = doc["money"].GetInt();
					if (money > 0)
					{
						PlatformLogic()->loginResult.i64Money += money;
						msg.append(StringUtils::format(GBKToUtf8("，获得%d金币奖励"), money));
					}
				}

				if (doc.HasMember("jewels") && doc["jewels"].IsInt())
				{
					int jewels = doc["jewels"].GetInt();
					if (jewels > 0)
					{
						PlatformLogic()->loginResult.iJewels += jewels;
						msg.append(StringUtils::format(GBKToUtf8("，获得%d房卡奖励"), jewels));
					}
				}
			}
			else
			{
				msg = GBKToUtf8("领取失败");
			}

			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(msg);
			prompt->setCallBack([=](){

				_delegate->walletChanged();

				close();
			});			
		}
	}

	void GameSpread::showBindLayer()
	{
		// 布局文件
		auto node = CSLoader::createNode(BIND_CSB);
		addChild(node);

		// 背景
		auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_bind"));
		layout->setScale(_winSize.width / 1280, _winSize.height / 720);

		auto img_BG = dynamic_cast<ImageView*>(node->getChildByName("Image_BG"));
		if (_winSize.width / _winSize.height > 1.78f)
		{
			img_BG->setScale(_winSize.height / 720);
		}

		// 输入的推广码
		auto textField_bind = dynamic_cast<TextField*>(img_BG->getChildByName("TextField_bind"));
		textField_bind->setVisible(false);
		_editBoxCode = HNEditBox::createEditBox(textField_bind, this);

		// 确定
		auto btn_sure = dynamic_cast<Button*>(img_BG->getChildByName("Button_sure"));
		btn_sure->addClickEventListener([=](Ref*){
		
			std::string code = _editBoxCode->getString();

			if (code.size() != 6 || !Tools::verifyNumberAndEnglish(code))
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的推广码"));
				return;
			}
			
			bindPromotionCode(code);
		});

		// 关闭
		auto btn_close = dynamic_cast<Button*>(img_BG->getChildByName("Button_close"));
		btn_close->addClickEventListener([=](Ref*) {
			node->removeFromParent();
			queryRewardInfo();
		});
	}

	void GameSpread::showRewardLayer(SpreadStruct* spreadData)
	{
		// 布局
		auto node = CSLoader::createNode(SPREAD_CSB);
		addChild(node);

		// 背景
		auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_spread"));
		layout->setScale(_winSize.width / 1280, _winSize.height / 720);

		auto img_BG = dynamic_cast<ImageView*>(node->getChildByName("Image_BG"));
		if (_winSize.width / _winSize.height > 1.78f)
		{
			img_BG->setScale(_winSize.height / 720);
		}

		// 推广码
		auto text_code = dynamic_cast<Text*>(img_BG->getChildByName("Text_code"));
		if (text_code) text_code->setString(StringUtils::format(GBKToUtf8("%s"), spreadData->code.c_str()));

		// 推广人数
		auto text_count = dynamic_cast<Text*>(img_BG->getChildByName("Text_people"));
		if (text_count) text_count->setString(StringUtils::format(GBKToUtf8("%d人"), spreadData->count));

		// 推广金币奖励
		auto text_gold = dynamic_cast<Text*>(img_BG->getChildByName("Text_gold"));
		if (text_gold)
		{
			text_gold->setVisible(0 != spreadData->money);
			text_gold->setString(StringUtils::format(GBKToUtf8("推广奖励：%d金币"), spreadData->money));
		}

		// 推广钻石奖励
		auto text_jewels = dynamic_cast<Text*>(img_BG->getChildByName("Text_jewels"));
		if (text_jewels)
		{
			text_jewels->setVisible(0 != spreadData->jewels);
			text_jewels->setString(StringUtils::format(GBKToUtf8("推广奖励：%d房卡"), spreadData->jewels));
		}

		// 领取
		auto btn_get = dynamic_cast<Button*>(img_BG->getChildByName("Button_get"));
		bool canGet = (spreadData->money > 0 || spreadData->jewels > 0);
		btn_get->setEnabled(canGet);
		auto imgName = (ImageView*)btn_get->getChildByName("Image_name");
		imgName->loadTexture(canGet ? "platform/SpreadUi/res/lingquziti.png" : "platform/SpreadUi/res/lingquziti2.png");
		btn_get->addClickEventListener([=](Ref*){
			getReward();
		});

		// 设置分享链接
		std::string shareUrl = PlatformConfig::getInstance()->getShareUrl();

		// 设置分享标题
		std::string shareTitile = StringUtils::format(GBKToUtf8("新手奖励码：%s"), spreadData->code.c_str());

		// 设置分享内容
		std::string shareContent = StringUtils::format(GBKToUtf8("欢迎下载，绑定推广码：%s即可获得新手奖励"), spreadData->code.c_str());

		// 分享
		auto btn_share = dynamic_cast<Button*>(img_BG->getChildByName("Button_share"));
		btn_share->addClickEventListener([=](Ref*){			
			auto shareLayer = GameShareLayer::create();
			shareLayer->setName("shareLayer");
			shareLayer->SetShareInfo(shareContent, shareTitile, shareUrl);
			shareLayer->show();
		});

		// 关闭
		auto btn_close = dynamic_cast<Button*>(img_BG->getChildByName("Button_close"));
		btn_close->addClickEventListener([=](Ref*) {
			close();
		});
	}
}