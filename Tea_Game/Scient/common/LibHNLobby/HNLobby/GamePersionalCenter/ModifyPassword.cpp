/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "ModifyPassword.h"
#include <string>

static const char* MODIFYPASSWORD_PATH	= "platform/modifyPassWord/modifyPassWordNode.csb";

ModifyPasswordLayer::ModifyPasswordLayer()
{	
}

ModifyPasswordLayer::~ModifyPasswordLayer()
{
	PlatformLogic()->removeEventSelector(MDM_GP_USERINFO, ASS_GP_USERINFO_ACCEPT);
	PlatformLogic()->removeEventSelector(MDM_GP_USERINFO, ASS_GP_USERINFO_NOTACCEPT);
}

void ModifyPasswordLayer::closeFunc()
{
	
}

bool ModifyPasswordLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(MODIFYPASSWORD_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto imgBg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78)
	{
		imgBg->setScale(0.9f);
	}

	// �ر�
	auto btnClose = (Button*)imgBg->getChildByName("Button_close");
	if (nullptr != btnClose)
	{
		btnClose->addClickEventListener([=](Ref*) {
			close();
		});
	}

	// ������ 
	auto textField_old = dynamic_cast<TextField*>(imgBg->getChildByName("TextField_oldPass"));
	textField_old->setVisible(false);
	auto editBox_old = HNEditBox::createEditBox(textField_old, this);
	editBox_old->setPasswordEnabled(true);

	// ������
	auto textField_new = dynamic_cast<TextField*>(imgBg->getChildByName("TextField_newPass"));
	textField_new->setVisible(false);
	auto editBox_new = HNEditBox::createEditBox(textField_new, this);
	editBox_new->setPasswordEnabled(true);

	// ȷ������
	auto textField_sure = dynamic_cast<TextField*>(imgBg->getChildByName("TextField_surePass"));
	textField_sure->setVisible(false);
	auto editBox_sure = HNEditBox::createEditBox(textField_sure, this);
	editBox_sure->setPasswordEnabled(true);

	auto btn_sure = dynamic_cast<Button*>(imgBg->getChildByName("Button_sure"));
	btn_sure->addClickEventListener([=](Ref*){

		std::string oldPassword = editBox_old->getString();
		std::string newPassword = editBox_new->getString();
		std::string surePassword = editBox_sure->getString();
		
		std::string MD5oldPass = MD5_CTX::MD5String(oldPassword).c_str();

		std::string tmp(PlatformLogic()->loginResult.szMD5Pass);

		do
		{
			if (oldPassword.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("����������롣")); break;
			}

			if (newPassword.size() < 6 ||
				newPassword.size() > 16 ||
				!Tools::verifyNumberAndEnglish(newPassword))
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("������6-16λӢ�Ļ��������롣")); break;
			}

			if (surePassword.empty())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("������ȷ�����롣")); break;
			}
			if (0 != tmp.compare(MD5oldPass))
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("�������������")); break;
			}
			if (0 != newPassword.compare(surePassword))
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("�����������벻ͬ��")); break;
			}

			_userPswd = MD5_CTX::MD5String(newPassword).c_str();

			MSG_GP_S_ChPassword ChPassword;
			ChPassword.dwUserID = PlatformLogic()->loginResult.dwUserID;
			strcpy(ChPassword.szMD5OldPass, PlatformLogic()->loginResult.szMD5Pass);
			strcpy(ChPassword.szMD5NewPass, _userPswd.c_str());
			PlatformLogic()->sendData(MDM_GP_USERINFO, ASS_GP_USERINFO_UPDATE_PWD, &ChPassword, sizeof(ChPassword));
			PlatformLogic()->addEventSelector(MDM_GP_USERINFO, ASS_GP_USERINFO_ACCEPT, 
				HN_SOCKET_CALLBACK(ModifyPasswordLayer::modifyPasswordSelector, this));

			PlatformLogic()->addEventSelector(MDM_GP_USERINFO, ASS_GP_USERINFO_NOTACCEPT, 
				HN_SOCKET_CALLBACK(ModifyPasswordLayer::modifyPasswordSelector, this));
		} while (0);
	});

	return true;
}

bool ModifyPasswordLayer::modifyPasswordSelector(HNSocketMessage* socketMessage)
{
	if (ASS_GP_USERINFO_ACCEPT == socketMessage->messageHead.bAssistantID)	// ������ѽ���
	{
		strcpy(PlatformLogic()->loginResult.szMD5Pass, _userPswd.c_str());

		auto userDefault = UserDefault::getInstance();
		if (userDefault->getBoolForKey(SAVE_TEXT))
		{
			userDefault->setStringForKey(PASSWORD_TEXT, _userPswd);
			userDefault->flush();
		}
		_userPswd.clear();
		GamePromptLayer::create()->showPrompt(GBKToUtf8("�����޸ĳɹ���"));
	}
	else if (ASS_GP_USERINFO_NOTACCEPT == socketMessage->messageHead.bAssistantID)	// �����δ�ܽ���
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("�����޸�ʧ�ܡ�"));
	}
	else
	{

	}
	return true;
}