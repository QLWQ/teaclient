/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GameCertification_h__
#define __GameCertification_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "../GameBaseLayer/GamePlatform.h"
USING_NS_CC;

class GameCertificationLayer : public HNLayer, public ui::EditBoxDelegate, public HNHttpDelegate
{
public:
	CREATE_FUNC(GameCertificationLayer);

	GameCertificationLayer();
	virtual ~GameCertificationLayer();

	typedef std::function<void()> CertificationCallBack;
	CertificationCallBack onCertificationCallBack = nullptr;
public:
	virtual bool init() override;

	void updateLayer(const realData& data);
private:
	void closeFunc() override;
private:
	virtual void editBoxReturn(ui::EditBox* editBox) override {};
	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	void onClickRealBtnCallback(Ref* ref);

	bool CheckID(const std::string& str);

	//获取验证码的时间限制
	void setLimitTime();

	//倒计时
	void updateLimitTime(float dt);
private:

	HNEditBox*	_editBox_PhoneNum = nullptr;
	HNEditBox*	_editBox_Code = nullptr;
	HNEditBox*	_editBox_Name = nullptr;
	HNEditBox*	_editBox_No = nullptr;
	Text*		_timeText = nullptr;
	Layout*		_timeLayout = nullptr;
	int			_restTime = 60;//倒计时时间
	Layout*   _real_unverified = nullptr;
	Layout*   _real_verified = nullptr;
};

#endif // __GameCertification_h__
