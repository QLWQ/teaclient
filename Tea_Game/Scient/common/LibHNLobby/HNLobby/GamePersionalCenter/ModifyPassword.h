/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __ModifyPassword_h__
#define __ModifyPassword_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"

USING_NS_CC;

class ModifyPasswordLayer : public HNLayer, public ui::EditBoxDelegate
{
	std::string _userPswd;

public:
	CREATE_FUNC(ModifyPasswordLayer);

	ModifyPasswordLayer();
	virtual ~ModifyPasswordLayer();

public:
	virtual bool init() override;

private:
	void closeFunc() override;

	bool modifyPasswordSelector(HNSocketMessage* socketMessage);

private:
	virtual void editBoxReturn(ui::EditBox* editBox) {};
};

#endif //__ModifyPassword_h__