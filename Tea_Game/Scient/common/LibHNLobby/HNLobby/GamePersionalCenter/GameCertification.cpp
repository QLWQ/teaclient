/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameCertification.h"
#include <string>
#include "../GameChildLayer/ServiceLayer.h"

static const char* CERTIFICATION_PATH = "platform/Chat/RealCertificationLayer.csb";

GameCertificationLayer::GameCertificationLayer()
{

}

GameCertificationLayer::~GameCertificationLayer()
{
	onCertificationCallBack = nullptr;
	unschedule(schedule_selector(GameCertificationLayer::updateLimitTime));
}

void GameCertificationLayer::closeFunc()
{
	
}

bool GameCertificationLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(CERTIFICATION_PATH);
	addChild(node);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("panel_make"));
	layout->setScale(_winSize.width / 1280, _winSize.height / 720);

	auto image_bg = dynamic_cast<ImageView*>(node->getChildByName("image_bg"));
	if (_winSize.width / _winSize.height > 1.78)
	{
		image_bg->setScale(0.9f);
	}

	auto pDesc = dynamic_cast<Text*>(image_bg->getChildByName("Text_desc"));
	pDesc->setVisible(false);

	auto pRichText = RichText::create();
	// 创建文本Element
	RichElementText* re0 = RichElementText::create(0, Color3B(150, 99, 39), 255, GBKToUtf8("按照文化部《网络游戏管理暂行方法》规定，您的账号需要进行实名注册以保障您的个人权益。"), pDesc->getFontName(), pDesc->getFontSize(), NONE_FLAG, "");
	pRichText->pushBackElement(re0);
	RichElementText* re1 = RichElementText::create(0, Color3B(255, 0, 0), 255, GBKToUtf8(" (注册信息仅可提交一次，请慎重对待。该信息仅用于实名注册，不会泄露于任何第三方。)"), pDesc->getFontName(), pDesc->getFontSize(), NONE_FLAG, "");
	pRichText->pushBackElement(re1);
	pRichText->setAnchorPoint(pDesc->getAnchorPoint());
	pRichText->setPosition(pDesc->getPosition());
	pRichText->ignoreContentAdaptWithSize(pDesc->isIgnoreContentAdaptWithSize());
	pRichText->setContentSize(pDesc->getContentSize());
	image_bg->addChild(pRichText);

	//未认证界面
	_real_unverified = dynamic_cast<Layout*>(image_bg->getChildByName("panel_unverified"));
	_real_unverified->setVisible(true);

	auto phone_editBox = dynamic_cast<TextField*>(_real_unverified->getChildByName("TextField_phone"));
	phone_editBox->setVisible(false);
	_editBox_PhoneNum = HNEditBox::createEditBox(phone_editBox, this);

	auto identity_editBox = dynamic_cast<TextField*>(_real_unverified->getChildByName("TextField_identity"));
	identity_editBox->setVisible(false);
	_editBox_No = HNEditBox::createEditBox(identity_editBox, this);

	auto verification_editBox = dynamic_cast<TextField*>(_real_unverified->getChildByName("TextField_verification"));
	verification_editBox->setVisible(false);
	_editBox_Code = HNEditBox::createEditBox(verification_editBox, this);

	auto name_editBox = dynamic_cast<TextField*>(_real_unverified->getChildByName("TextField_name"));
	name_editBox->setVisible(false);
	_editBox_Name = HNEditBox::createEditBox(name_editBox, this);

	auto btn_send = dynamic_cast<Button*>(_real_unverified->getChildByName("btn_send"));
	btn_send->addClickEventListener(CC_CALLBACK_1(GameCertificationLayer::onClickRealBtnCallback, this));

	auto btn_afreshsend = dynamic_cast<Button*>(_real_unverified->getChildByName("btn_afreshsend"));
	//btn_afreshsend->setVisible(false);
	btn_afreshsend->addClickEventListener(CC_CALLBACK_1(GameCertificationLayer::onClickRealBtnCallback, this));

	_timeText = dynamic_cast<Text*>(btn_afreshsend->getChildByName("text_time"));
	_timeLayout = dynamic_cast<Layout*>(btn_afreshsend->getChildByName("panel_make"));

	auto limitTime = UserDefault::getInstance()->getStringForKey("LimitStartTime", "noTime");
	//为"noTime"表示现在没有获取验证码
	if (limitTime != "noTime")
	{
		//获取当前时间，用来和保存的时间比较，如果时间间隔小于60s,就让获取验证码按钮不可点击，否则可点击
		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		std::string nowTime = StringUtils::format("%02d%02d%02d%02d", now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
		int nowtime = atoi(nowTime.c_str());
		int oldtime = atoi(limitTime.c_str());
		int intervalTime = nowtime - oldtime;
		if (intervalTime > 0 && intervalTime <= 60)
		{
			//显示当前绑定的手机号
			phone_editBox->setString(UserDefault::getInstance()->getStringForKey("Mobilephone", " "));
			//显示倒计时的时间
			_restTime = 60 - intervalTime;
			_timeText->setString(GBKToUtf8(std::to_string(_restTime) + " 秒"));
			btn_send->setVisible(false);
			setLimitTime();
		}
		else
		{
			_timeLayout->setVisible(false);
			_timeText->setVisible(false);
		}
	}

	auto btn_sure = dynamic_cast<Button*>(_real_unverified->getChildByName("btn_sure"));
	btn_sure->addClickEventListener(CC_CALLBACK_1(GameCertificationLayer::onClickRealBtnCallback, this));

	//认证界面
	_real_verified = dynamic_cast<Layout*>(image_bg->getChildByName("panel_verified"));
	_real_verified->setVisible(false);

	auto btn_service = dynamic_cast<Button*>(_real_verified->getChildByName("btn_service"));
	btn_service->addClickEventListener(CC_CALLBACK_1(GameCertificationLayer::onClickRealBtnCallback, this));

	auto btn_close = dynamic_cast<Button*>(image_bg->getChildByName("btn_close"));
	btn_close->addClickEventListener([=](Ref* sender)
	{
		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
		close(ACTION_TYPE_LAYER::FADE);
	}
	);

	return true;
}
void GameCertificationLayer::onClickRealBtnCallback(Ref* ref)
{
	auto btn = (Button*)ref;
	std::string name(btn->getName());

	if (name.compare("btn_send") == 0)
	{
		//检验输入的手机号
		std::string phoneNumber(_editBox_PhoneNum->getString());
		int length = phoneNumber.length();
		if (phoneNumber.empty()
			|| length != 11 || !Tools::verifyNumber(phoneNumber))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的手机号！"));
			return;
		}
		btn->setVisible(false);

		//通知发送验证码
		std::string url = StringUtils::format("http://alipay.qp888m.com/c779app/sendyzm.php?userid=%d&telphone=%s", PlatformLogic()->loginResult.dwUserID, phoneNumber.c_str());
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("sendReal", network::HttpRequest::Type::GET, url);

		//冷却倒计时
		//保存当前时间，用于关闭这个界面后，再次进入获取验证码按钮的状态
		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		std::string startTime = StringUtils::format("%02d%02d%02d%02d", now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
		UserDefault::getInstance()->setStringForKey("LimitStartTime", startTime);

		_restTime = 60;
		//设置60s内获取按钮不可点
		setLimitTime();
		//暂时保存手机号
		UserDefault::getInstance()->setStringForKey("Mobilephone", phoneNumber);
		UserDefault::getInstance()->flush();
	}
	else if (name.compare("btn_afreshsend") == 0)
	{
		//检验输入的手机号
		std::string phoneNumber(_editBox_PhoneNum->getString());
		int length = phoneNumber.length();
		if (phoneNumber.empty()
			|| length != 11 || !Tools::verifyNumber(phoneNumber))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的手机号！"));
			return;
		}
		//通知发送验证码
		std::string url = StringUtils::format("http://alipay.qp888m.com/c779app/sendyzm.php?userid=%d&telphone=%s", PlatformLogic()->loginResult.dwUserID, phoneNumber.c_str());
		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("sendReal",network::HttpRequest::Type::GET, url);

		//冷却倒计时
		//保存当前时间，用于关闭这个界面后，再次进入获取验证码按钮的状态
		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		std::string startTime = StringUtils::format("%02d%02d%02d%02d", now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
		UserDefault::getInstance()->setStringForKey("LimitStartTime", startTime);

		_restTime = 60;
		//设置60s内获取按钮不可点
		setLimitTime();
		//暂时保存手机号
		UserDefault::getInstance()->setStringForKey("Mobilephone", phoneNumber);
		UserDefault::getInstance()->flush();
	}
	else if (name.compare("btn_sure") == 0)
	{
		//检验输入的手机号
		std::string phoneNumber(_editBox_PhoneNum->getString());
		int length = phoneNumber.length();
		if (phoneNumber.empty()
			|| length != 11 || !Tools::verifyNumber(phoneNumber))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的手机号！"));
			return;
		}
		//验证身份证号码是否正确
		std::string identityNumber(_editBox_No->getString());
		if (CheckID(identityNumber) == false)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的身份证号！"));
			return;
		}
		//验证名字是否输入
		std::string Name(_editBox_Name->getString());
		if (Name.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入真实姓名！"));
			return;
		}

		//验证码是否输入
		std::string verification(_editBox_Code->getString());
		if (verification.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入验证码！"));
			return;
		}

		std::string url = "http://alipay.qp888m.com/c779app/Authentication.php";
		std::string requestData = "";
		requestData.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));
		requestData.append(StringUtils::format("&realname=%s", Name.c_str()));
		requestData.append(StringUtils::format("&idno=%s", identityNumber.c_str()));
		requestData.append(StringUtils::format("&telphone=%s", phoneNumber.c_str()));
		requestData.append(StringUtils::format("&yzm=%s", verification.c_str()));

		HNHttpRequest::getInstance()->addObserver(this);
		HNHttpRequest::getInstance()->request("sendRealData", network::HttpRequest::Type::POST, url, requestData);

	}
	else if (name.compare("btn_service") == 0)
	{
		auto serviceLayer = ServiceLayer::create();
		serviceLayer->setName("serviceLayer");
		serviceLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, this->getZOrder() + 1, this->getTag() + 1);
	}
}

void GameCertificationLayer::setLimitTime()
{
	schedule(schedule_selector(GameCertificationLayer::updateLimitTime), 1.0f);

	_timeText->setVisible(true);
	_timeLayout->setVisible(true);
}

void GameCertificationLayer::updateLimitTime(float dt)
{
	_restTime -= 1;
	if (_restTime < 0)
	{
		unschedule(schedule_selector(GameCertificationLayer::updateLimitTime));
		UserDefault::getInstance()->setStringForKey("LimitStartTime", "noTime");
		UserDefault::getInstance()->setStringForKey("Mobilephone", "noMobilePhone");
		UserDefault::getInstance()->flush();
		_timeLayout->setVisible(false);
		_timeText->setVisible(false);
		_timeText->setString(GBKToUtf8("60 秒"));
	}
	else
	{
		_timeText->setString(GBKToUtf8(std::to_string(_restTime) + " 秒"));
	}
}


bool GameCertificationLayer::CheckID(const std::string& ID_num)/*身份证校验*/
{
	int weight[] = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
	char validate[] = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };
	int sum = 0;
	int mode = 0;
	int i = 0;
	int length = ID_num.length();
	if (length == 18)
	{
		for (i = 0; i < length - 1; i++)
			sum = sum + (ID_num[i] - '0') * weight[i];
		mode = sum % 11;
		if (validate[mode] == ID_num[17])
			return true;
	}
	return false;
}

void GameCertificationLayer::updateLayer(const realData& data)
{
	bool isReal = !data.identity.empty() && !data.Name.empty() && !data.PhoneNum.empty();
	if (isReal)
	{
		if (_real_unverified)
			_real_unverified->setVisible(false);
		if (_real_verified == nullptr)
			return;
		_real_verified->setVisible(true);

		auto text_phone = dynamic_cast<Text*>(_real_verified->getChildByName("text_phone"));
		text_phone->setString(data.PhoneNum);

		auto text_name = dynamic_cast<Text*>(_real_verified->getChildByName("text_name"));
		text_name->setString(data.Name);

		auto text_identity = dynamic_cast<Text*>(_real_verified->getChildByName("text_identity"));
		text_identity->setString(data.identity);
	}
	else
	{
		if (_real_unverified)
			_real_unverified->setVisible(true);
		if (_real_verified)
			_real_verified->setVisible(false);
	}

}

void GameCertificationLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;

	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

	if (doc.HasParseError() || !doc.IsObject())
	{
		return;
	}

	if (requestName.compare("sendRealData") == 0)
	{	
		int code = doc["code"].GetInt();
		if (code != 0)
		{
			GamePromptLayer::create()->showPrompt(doc["msg"].GetString());
		}
		else
		{//实名认证成功
			GamePromptLayer::create()->showPrompt(doc["msg"].GetString());
			if (onCertificationCallBack)
			{
				onCertificationCallBack();
			}
		}
	}
	else if (requestName.compare("sendReal") == 0)
	{
		int code = doc["code"].GetInt();
		if (code != 0)
		{
			GamePromptLayer::create()->showPrompt(doc["msg"].GetString());
		}
	}
}