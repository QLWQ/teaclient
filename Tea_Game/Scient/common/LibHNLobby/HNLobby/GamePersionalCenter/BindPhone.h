/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __BindPhone_h__
#define __BindPhone_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
USING_NS_CC;

class BindPhoneLayer : public HNLayer, public ui::EditBoxDelegate
{
	HNEditBox*	_editBox_PhoneNum	= nullptr;
	HNEditBox*	_editBox_Code		= nullptr;
	Button*		_btn_getCode		= nullptr;
	Text*		_timeText			= nullptr;
	int			_restTime			= 60;//倒计时时间
	std::string	_verifyCode			= "";

public:
	CREATE_FUNC(BindPhoneLayer);

	BindPhoneLayer();
	virtual ~BindPhoneLayer();

public:
	virtual bool init() override;

private:
	void closeFunc() override;

	void verifyCodeClickCallBack(Ref* pSender);

	void sureBtnClickCallBack(Ref* pSender);

private:
	bool verifyCodeSelector(HNSocketMessage* socketMessage);

	bool bindPhoneSelector(HNSocketMessage* socketMessage);

	bool modifyUserInfoSelector(HNSocketMessage* socketMessage);


	virtual void editBoxReturn(ui::EditBox* editBox) {};

	// 更新用户信息
	void updateUserDetail();

	//获取验证码的时间限制
	void setLimitTime();

	//倒计时
	void updateLimitTime(float dt);
};

#endif // __BindPhone_h__
