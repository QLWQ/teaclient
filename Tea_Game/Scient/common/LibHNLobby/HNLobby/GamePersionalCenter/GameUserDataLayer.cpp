/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameUserDataLayer.h"
#include "ModifyPassword.h"
#include "BindPhone.h"
#include "../GameChildLayer/GameShareLayer.h"
#include <string>

static const char* USER_HEAD_MASK	= "platform/common/touxiangdi_192x192.png";
static const char* MEN_HEAD			= "platform/head/men_head.png";							//user head 
static const char* WOMEN_HEAD		= "platform/head/women_head.png";						//user head 

static const int MODIFYPSDLAYER_TAG = 100;
static const int BINDPHONELAYER_TAG = 101;

static const char* USERDATA_PATH	= "platform/userData/userDataNode.csb";
static const char* BIND_CSB			= "platform/userData/bindAgent.csb";

// 用户信息
static const char* USER_INFO_PATH	= "platform/userData/UserInfo.csb";

GameUserDataLayer::GameUserDataLayer()
{
	
}

GameUserDataLayer::~GameUserDataLayer()
{
	HNHttpRequest::getInstance()->removeObserver(this);
}

void GameUserDataLayer::closeFunc()
{
	
}

bool GameUserDataLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(USER_INFO_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	if (_winSize.width / _winSize.height > 1.78)
	{
		img_bg->setScale(0.9f);
	}

	// 关闭
	auto btnClose = (Button*)img_bg->getChildByName("Button_close");
	if (nullptr != btnClose)
	{
		btnClose->addClickEventListener([=](Ref*) {
			close();
		});
	}

	MSG_GP_R_LogonResult& LogonResult = PlatformLogic()->loginResult;

	auto img_tempHead = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_head"));
	auto tempHead = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_kuang"));
	 //玩家头像	
	auto userHead = GameUserHead::create(img_tempHead, tempHead);
	userHead->show(USER_HEAD_MASK);
	userHead->loadTexture(LogonResult.bBoy ? MEN_HEAD : WOMEN_HEAD);
	userHead->setHeadByFaceID(LogonResult.bLogoID);
	userHead->loadTextureWithUrl(LogonResult.headUrl);
	userHead->setVIPHead("",LogonResult.iVipLevel);
	// 昵称
	auto text_nick = dynamic_cast<Text*>(img_bg->getChildByName("Text_nicheng"));
	//text_nick->setString(GBKToUtf8(LogonResult.nickName));
	Tools::TrimSpace(LogonResult.nickName);
	std::string nickName(LogonResult.nickName);
	if (!nickName.empty())
	{
		string str = LogonResult.nickName;
		if (str.length() > 10)
		{
			str.erase(11, str.length());
			str.append("...");
			text_nick->setString(GBKToUtf8(str));
		}
		else
		{
			text_nick->setString(GBKToUtf8(str));
		}
	}
	else
	{
		text_nick->setString(GBKToUtf8("未知"));
	}
	if (LogonResult.iVipLevel > 0)
	{
		text_nick->setColor(Color3B(255, 0, 0));
	}
	// 房卡
	_text_fangka = (Text*)img_bg->getChildByName("Image_fangka")->getChildByName("Text_fangka");
	std::string str_fangka = HNToWan(LogonResult.iJewels + LogonResult.iLockJewels);
	_text_fangka->setString(str_fangka);

	// 金币
	_text_jinbi = (Text*)img_bg->getChildByName("Image_jinbi")->getChildByName("Text_jinbi");
	std::string str_jinbi = HNToWan(LogonResult.i64Money);
	_text_jinbi->setString(str_jinbi);

	// 礼券
	_text_liquan = (Text*)img_bg->getChildByName("Image_liquan")->getChildByName("Text_liquan");
	std::string str_liquan = HNToWan(LogonResult.iLotteries);
	_text_liquan->setString(str_liquan);

	// 玩家ID
	auto text_UserID = (Text*)img_bg->getChildByName("Text_id");
	text_UserID->setString("ID:"+to_string(LogonResult.dwUserID));

	/*
	// 手机号
	_text_phone = (Text*)img_bg->getChildByName("Text_shouji");
	_text_phone->setString(LogonResult.szMobileNo);

	// VIP进度条
	_vipLoadBar = (LoadingBar*)img_bg->getChildByName("LoadingBar_1");

	_vipProgress = (Text*)img_bg->getChildByName("Text_2");

	_vipLevel = (Text*)img_bg->getChildByName("Text_1");

	// 修改密码
	auto btn_ModifyPsd = (Button*)img_bg->getChildByName("Button_xiugaimima");
	btn_ModifyPsd->addClickEventListener([=](Ref*){

		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			auto modifyPasswordLayer = ModifyPasswordLayer::create();
			modifyPasswordLayer->open(ACTION_TYPE_LAYER::FADE, this->getParent(), Vec2::ZERO, this->getParent()->getZOrder() + 1100, MODIFYPSDLAYER_TAG);
			this->removeFromParent();
		}), nullptr));
	});
	//btn_ModifyPsd->setVisible(LogonResult.bLogonType == 1);
	btn_ModifyPsd->setVisible(false);

	// 绑定手机
	_btn_bindPhone = (Button*)img_bg->getChildByName("Button_bangding");
	_btn_bindPhone->addClickEventListener([=](Ref*){

		_btn_bindPhone->setEnabled(false);
		// 绑定过，解绑
		if (PlatformLogic()->loginResult.iBindMobile == 1)
		{
			MSG_GP_S_BindMobile bindMobile;
			bindMobile.dwUserID = PlatformLogic()->loginResult.dwUserID;
			bindMobile.dwCommanType = 0;
			memset(bindMobile.szMobileNo, 0x00, sizeof(bindMobile.szMobileNo));
			PlatformLogic()->sendData(MDM_GP_BIND_MOBILE, ASS_GP_BIND_MOBILE, &bindMobile, sizeof(bindMobile),
				HN_SOCKET_CALLBACK(GameUserDataLayer::unbindPhoneCallback, this));
		}
		// 未绑定过，绑定
		else
		{
			this->runAction(Sequence::create(FadeOut::create(0.2f), CallFunc::create([&](){
				auto bindPhoneLayer = BindPhoneLayer::create();
				bindPhoneLayer->open(ACTION_TYPE_LAYER::FADE, this->getParent(), Vec2::ZERO, this->getParent()->getZOrder() + 1100, BINDPHONELAYER_TAG);
				this->removeFromParent();
			}), nullptr));
		}
	});
	//updateUserDetail();
	*/

	// 绑定代理商
	_btn_bindAgent = (Button*)img_bg->getChildByName("Button_bind");
	_btn_bindAgent->addClickEventListener([=](Ref*){

		showBindLayer();
	});

	// 用户界面分享游戏
	auto Btn_share = (Button*)img_bg->getChildByName("Button_share");
	Btn_share->addClickEventListener([=](Ref*){
		//node->removeFromParent();
		auto shareLayer = GameShareLayer::create();
		shareLayer->setName("shareLayer");
		shareLayer->SetShareCallBack([=](bool success, int platform, const std::string& errorMsg) {

			std::string str;
			if (success)
			{
				//str = GBKToUtf8("分享成功");
				//http://192.168.1.50/hy
				//http://shuju.qp888m.com/hyyl
				std::string path = StringUtils::format("/hyyl/TestShare.php?UserID=%d",
					PlatformLogic()->loginResult.dwUserID);
				std::string	url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), path);
				HNHttpRequest::getInstance()->addObserver(this);
				HNHttpRequest::getInstance()->request("requestShareEvery", cocos2d::network::HttpRequest::Type::GET, url);
			}
			else
			{
				std::string str;
				if (str.empty()) {
					str = GBKToUtf8("分享失败");
				}
				else {
					str = errorMsg;
				}
				runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=]() {
					GamePromptLayer::create()->showPrompt(str);
				}), nullptr));
			}
		});
		std::string shareStr = HNPlatformLogic::getInstance()->getShareStr().shareStr;
		shareLayer->SetShareInfo(HNPlatformConfig()->getShareContent(), GBKToUtf8(shareStr), HNPlatformConfig()->getShareUrl());
		shareLayer->show();
	});

	// 更新显示
	//updateUserDetail();

	// 查询代理信息
	queryAgentInfo();

	return true;
}

void GameUserDataLayer::setChangeDelegate(MoneyChangeNotify* delegate)
{
	_delegate = delegate;
}

void GameUserDataLayer::showBindLayer()
{
	auto node = CSLoader::createNode(BIND_CSB);
	node->setName("BindLayer");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_bg"));
	layout->setScale(_winSize.width / 1280, _winSize.height / 720);
	layout->addClickEventListener([=](Ref*){
		node->removeFromParent();
	});

	auto TextField_input = dynamic_cast<TextField*>(node->getChildByName("TextField_input"));
	TextField_input->setVisible(false);
	_editBoxCode = HNEditBox::createEditBox(TextField_input, this);

	auto btn_sure = dynamic_cast<Button*>(node->getChildByName("Button_sure"));
	btn_sure->addClickEventListener([=](Ref*){

		std::string code = _editBoxCode->getString();
		if (code.empty() || !Tools::verifyNumber(code))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的代理商ID"));
			return;
		}

		bindAgent(code);
	});

	auto btn_close = dynamic_cast<Button*>(node->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*){
		node->removeFromParent();
	});
}
//
//bool GameUserDataLayer::unbindPhoneCallback(HNSocketMessage* socketMessage)
//{
//	CHECK_SOCKET_DATA_RETURN(MSG_GP_R_BindMobile, socketMessage->objectSize, true, "MSG_GP_R_BindMobile size of error!");
//	MSG_GP_R_BindMobile *bindMobile = (MSG_GP_R_BindMobile *)socketMessage->object;
//	
//	// 请求解除绑定
//	if(bindMobile->dwCommanType == 0)
//	{
//		PlatformLogic()->loginResult.iBindMobile = bindMobile->dwCommanResult;
//		if(bindMobile->dwCommanResult == 0)
//		{
//			GamePromptLayer::create()->showPrompt(GBKToUtf8("手机解除绑定成功。"));
//			memset(PlatformLogic()->loginResult.szMobileNo, 0x00, sizeof(PlatformLogic()->loginResult.szMobileNo));
//			updateUserDetail();
//		}
//		else
//		{
//			GamePromptLayer::create()->showPrompt(GBKToUtf8("手机解除绑定失败。"));
//		}
//	}
//
//
//	return true;
//}
//
//void GameUserDataLayer::updateUserDetail()
//{
//	MSG_GP_R_LogonResult &userInfo = PlatformLogic()->loginResult;
//
//	// 修改显示文字
//	Sprite* spName = (Sprite*)_btn_bindPhone->getChildByName("Sprite_name");
//	if (nullptr == spName) {
//		return;
//	}
//
//	// 已经绑定
//	if(userInfo.iBindMobile == 1)
//	{
//		_text_phone->setString(userInfo.szMobileNo);
//		spName->setTexture("platform/userData/res/jiebanganniuziti.png");
//	}
//	// 未绑定
//	else
//	{
//		_text_phone->setString(GBKToUtf8("未绑定"));
//		//_btn_bindPhone->setBright(true);
//		spName->setTexture("platform/userData/res/bangdinganniuziti.png");
//	}
//}

void GameUserDataLayer::queryAgentInfo()
{
	std::string url = HNPlatformConfig()->getOrderUrl();

	std::string requestData = "type=CheckUserBindAgency";
	requestData.append(StringUtils::format("&userid=%d", PlatformLogic()->loginResult.dwUserID));

	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("queryAgentInfo", cocos2d::network::HttpRequest::Type::POST, url, requestData);

	/*
	std::string vipurl = StringUtils::format("http://alipay.qp888m.com/c779app/get_recharge_info.php?userid=%d", PlatformLogic()->loginResult.dwUserID);
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("requestVip", cocos2d::network::HttpRequest::Type::POST, vipurl);
	*/
}

void GameUserDataLayer::bindAgent(std::string AgentID)
{
	std::string url = HNPlatformConfig()->getBindDiliUrl();

	std::string requestData = "";
	url.append(StringUtils::format("userid=%d", PlatformLogic()->loginResult.dwUserID));
	url.append("&agencyid=" + AgentID);

	HNHttpRequest::getInstance()->request("bindAgent", cocos2d::network::HttpRequest::Type::GET, url);
}

void GameUserDataLayer::dealQueryAgentInfoResp(const std::string& data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError() || !doc.IsObject()) {
		return;
	}

	if (doc.HasMember("status") && doc["status"].IsInt())
	{
		int status = doc["status"].GetInt();

		if (1 == status)
		{
			int code = doc["agencyid"].GetInt();
			_btn_bindAgent->setTitleText(StringUtils::toString(code));
			_btn_bindAgent->setEnabled(false);
		}
		else
		{
			_btn_bindAgent->setTitleText(GBKToUtf8("未绑定"));
			_btn_bindAgent->setEnabled(true);
		}		
	}
}

void GameUserDataLayer::dealQueryVipLevelResp(const std::string& data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError() || !doc.IsObject()) {
		return;
	}
	/*
	int _VipLevel = doc["viplevel"].GetInt();
	int _nRecharge = doc["recharge"].GetInt();
	std::vector<int> VipNumber = { 0, 18, 158, 588, 1888, 18888, 38888 };
	_vipLevel->setString(StringUtils::format("VIP%d", _VipLevel));
	_vipProgress->setString(StringUtils::format("%d / %d", _nRecharge, VipNumber[_VipLevel + 1]));
	float percent = (float)(_nRecharge - VipNumber[_VipLevel]) / (float)(VipNumber[_VipLevel + 1] - VipNumber[_VipLevel]) * 100.f;
	_vipLoadBar->setPercent(percent);
	*/
}

void GameUserDataLayer::dealBindAgentResp(const std::string& data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

	if (doc.HasParseError() || !doc.IsObject()) return;
	int rs = doc["status"].GetInt();
	if (rs == 1)
	{
		if (doc.HasMember("msg") && doc["msg"].IsString())
		{
			std::string msg = doc["msg"].GetString();
			GamePromptLayer* prompt = GamePromptLayer::create();
			prompt->showPrompt(msg);
			prompt->setCallBack([=](){
			
				if (rs == 1 && getChildByName("BindLayer"))
				{
					removeChildByName("BindLayer");
					queryAgentInfo();
					_delegate->updateUserInfo();
					// 金币
					_text_jinbi->setString(to_string(PlatformLogic()->loginResult.iJewels));
				}
			});
		}
	}
	else
	{
		GamePromptLayer* prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("申请绑定失败，请重新绑定"));
		prompt->setCallBack([=](){
			removeChildByName("BindLayer");
		});
	}
}

void GameUserDataLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) {
		return;
	}

	// 查询代理商信息
	if (requestName.compare("queryAgentInfo") == 0)	{
		dealQueryAgentInfoResp(responseData);
	}

	else if (requestName.compare("requestVip") == 0){
		dealQueryVipLevelResp(responseData);
	}

	// 绑定代理商信息
	else if (requestName.compare("bindAgent") == 0)	{
		dealBindAgentResp(responseData);
	}
	else {

	}
}