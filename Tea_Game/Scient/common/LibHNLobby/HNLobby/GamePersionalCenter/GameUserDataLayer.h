/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMEUSERDATA_LAYER_H__
#define __GAMEUSERDATA_LAYER_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "GameUserHead.h"
#include "HNHttp/HNHttp.h"

USING_NS_CC;

class GameUserDataLayer 
	: public HNLayer
	, public HNHttpDelegate
	, public ui::EditBoxDelegate
{
public:
	CREATE_FUNC(GameUserDataLayer);

	GameUserDataLayer();
	virtual ~GameUserDataLayer();

	void setChangeDelegate(MoneyChangeNotify* delegate);

private:
	virtual bool init();

	void closeFunc() override;

	// 解绑回调
	bool unbindPhoneCallback(HNSocketMessage* socketMessage);

	// 更新用户信息
	//void updateUserDetail();

private:
	void showBindLayer();

private:
	void queryAgentInfo();

	void bindAgent(std::string AgentID);

	void dealQueryAgentInfoResp(const std::string& data);

	void dealQueryVipLevelResp(const std::string& data);

	void dealBindAgentResp(const std::string& data);

	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

private:
	virtual void editBoxReturn(ui::EditBox* editBox) {};

protected:
	HNEditBox*				_editBoxCode	= nullptr;
	MoneyChangeNotify*		_delegate		= nullptr;
	std::string				_verifyCode;
	Text*					_text_phone		= nullptr;
	Button*					_btn_bindPhone	= nullptr;
	Button*					_btn_bindAgent	= nullptr;
	Text*					_text_fangka	= nullptr;
	Text*					_text_jinbi		= nullptr;
	Text*					_text_liquan	= nullptr;
	/*
	//VIP相关
	LoadingBar*			_vipLoadBar;
	Text*					_vipProgress;
	Text*					_vipLevel;
	*/
};
#endif //__GAMEUSERDATA_LAYER_H__