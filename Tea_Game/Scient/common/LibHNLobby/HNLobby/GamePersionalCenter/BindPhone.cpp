/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "BindPhone.h"
#include <string>

static const char* BINDPHONE_PATH	= "platform/bindPhone/bindPhoneNode.csb";

BindPhoneLayer::BindPhoneLayer()
{
	
}

BindPhoneLayer::~BindPhoneLayer()
{
	PlatformLogic()->removeEventSelector(MDM_GP_SMS, ASS_GP_SMS_VCODE);
	PlatformLogic()->removeEventSelector(MDM_GP_BIND_MOBILE, ASS_GP_BIND_MOBILE);
}

void BindPhoneLayer::closeFunc()
{
	
}

bool BindPhoneLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(BINDPHONE_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto imgBg = (ImageView*)node->getChildByName("Image_bg");
	if (_winSize.width / _winSize.height > 1.78)
	{
		imgBg->setScale(0.9f);
	}
	
	//手机号输入框 
	auto PhoneNumber = (TextField*)imgBg->getChildByName("TextField_phone");
	PhoneNumber->setVisible(false);
	_editBox_PhoneNum = HNEditBox::createEditBox(PhoneNumber, this);

	//验证码输入框
	auto code = (TextField*)imgBg->getChildByName("TextField_code");
	code->setVisible(false);
	_editBox_Code = HNEditBox::createEditBox(code, this);

	//确定按钮
	auto btn_sure = (Button*)imgBg->getChildByName("Button_sure");
	btn_sure->addClickEventListener(CC_CALLBACK_1(BindPhoneLayer::sureBtnClickCallBack, this));

	//获取验证码
	_btn_getCode = (Button*)imgBg->getChildByName("Button_getCode");
	_btn_getCode->addClickEventListener(CC_CALLBACK_1(BindPhoneLayer::verifyCodeClickCallBack, this));
	Size contentSize = _btn_getCode->getContentSize();

	// 验证码倒计时
	_timeText = (Text*)imgBg->getChildByName("Text_time");
	_timeText->setString(GBKToUtf8("60"));

	// 关闭
	auto btnClose = (Button*)imgBg->getChildByName("Button_close");
	if (nullptr != btnClose)
	{
		btnClose->addClickEventListener([=](Ref*) {
			close();
		});
	}

	auto limitTime = UserDefault::getInstance()->getStringForKey("LimitStartTime", "noTime");
	//为"noTime"表示现在没有获取验证码
	if (limitTime != "noTime")
	{
		//获取当前时间，用来和保存的时间比较，如果时间间隔小于60s,就让获取验证码按钮不可点击，否则可点击
		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		std::string nowTime = StringUtils::format("%02d%02d%02d%02d", now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
		int nowtime = atoi(nowTime.c_str());
		int oldtime = atoi(limitTime.c_str());
		int intervalTime = nowtime - oldtime;
		if (intervalTime > 0 &&  intervalTime <= 60)
		{
			//显示当前绑定的手机号
			_editBox_PhoneNum->setString(UserDefault::getInstance()->getStringForKey("Mobilephone", " "));
			_verifyCode = UserDefault::getInstance()->getStringForKey("curVCode", " ");
			//显示倒计时的时间
			_restTime = 60 - intervalTime;
			std::string str;
			str.append(StringUtils::toString(_restTime));
			_timeText->setString(GBKToUtf8(str.c_str()));
			setLimitTime();
		}
		else
		{
			_editBox_Code->setEnabled(true);
			_editBox_Code->setBright(true);
			_timeText->setVisible(false);
		}
	}
	else
	{
		_timeText->setVisible(false);
	}
	return true;
}

void BindPhoneLayer::sureBtnClickCallBack(Ref* pSender)
{
	std::string phoneNumber = _editBox_PhoneNum->getString();
	std::string verifyCode = _editBox_Code->getString();
	do
	{
		if (phoneNumber.empty()
			|| _editBox_PhoneNum->getStringLength() != 11
			|| !Tools::verifyNumber(phoneNumber))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的手机号码。")); break;
		}

		if (verifyCode.empty()
			|| _editBox_Code->getStringLength() != 6
			|| !Tools::verifyNumber(verifyCode)
			|| MD5_CTX::MD5String(verifyCode).compare(_verifyCode) != 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的验证码。")); break;
		}

		MSG_GP_S_BindMobile bindMobile;
		bindMobile.dwUserID = PlatformLogic()->loginResult.dwUserID;
		bindMobile.dwCommanType = 1;
		strcpy(bindMobile.szMobileNo, phoneNumber.c_str());
		PlatformLogic()->sendData(MDM_GP_BIND_MOBILE, ASS_GP_BIND_MOBILE, &bindMobile, sizeof(bindMobile),
			HN_SOCKET_CALLBACK(BindPhoneLayer::bindPhoneSelector, this));
	} while (0);
}

void BindPhoneLayer::verifyCodeClickCallBack(Ref* pSender)
{
	std::string phoneNumber = _editBox_PhoneNum->getString();
	do
	{
		if (phoneNumber.empty()
			|| _editBox_PhoneNum->getStringLength() != 11
			|| !Tools::verifyNumber(phoneNumber))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的手机号码。")); break;
		}
		
		MSG_GP_SmsVCode SmsVCode;
		SmsVCode.nType = 4;
		strcpy(SmsVCode.szName, PlatformLogic()->loginResult.szName);
		strcpy(SmsVCode.szMobileNo, phoneNumber.c_str());
		PlatformLogic()->sendData(MDM_GP_SMS, ASS_GP_SMS_VCODE, &SmsVCode, sizeof(SmsVCode), 
			HN_SOCKET_CALLBACK(BindPhoneLayer::verifyCodeSelector, this));
	
		//保存当前时间，用于关闭这个界面后，再次进入获取验证码按钮的状态
		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		std::string startTime = StringUtils::format("%02d%02d%02d%02d", now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
		UserDefault::getInstance()->setStringForKey("LimitStartTime", startTime);
		
		_restTime = 60;
		//设置60s内获取按钮不可点
		setLimitTime();
		//暂时保存手机号
		UserDefault::getInstance()->setStringForKey("Mobilephone", phoneNumber);
		UserDefault::getInstance()->flush();

	} while (0);
}

bool BindPhoneLayer::verifyCodeSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(MSG_GP_SmsVCode, socketMessage->objectSize, true, "MSG_GP_SmsVCode size of error!");
	MSG_GP_SmsVCode *smsVCode = (MSG_GP_SmsVCode *)socketMessage->object;

	_verifyCode = smsVCode->szVCode;
	UserDefault::getInstance()->setStringForKey("curVCode", _verifyCode);
	UserDefault::getInstance()->flush();
	return true;
}

bool BindPhoneLayer::bindPhoneSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(MSG_GP_R_BindMobile, socketMessage->objectSize, true, "MSG_GP_R_BindMobile size of error!");
	MSG_GP_R_BindMobile *bindMobile = (MSG_GP_R_BindMobile *)socketMessage->object;

	// 请求解除绑定
	if(bindMobile->dwCommanType == 0)
	{
		if(bindMobile->dwCommanResult == 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("手机解除绑定成功。"));
			PlatformLogic()->loginResult.iBindMobile = 0;
			memset(PlatformLogic()->loginResult.szMobileNo, 0x00, sizeof(PlatformLogic()->loginResult.szMobileNo));
			updateUserDetail();
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("手机解除绑定失败。"));
		}
	}

	// 请求绑定
	else if(bindMobile->dwCommanType == 1)
	{
		if(bindMobile->dwCommanResult == 0)
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("手机绑定成功。"));
			PlatformLogic()->loginResult.iBindMobile = 1;
			memcpy(PlatformLogic()->loginResult.szMobileNo, _editBox_PhoneNum->getString().c_str(), sizeof(PlatformLogic()->loginResult.szMobileNo));
			updateUserDetail();
			prompt->setCallBack([=]() {		
				close();
			});
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("手机绑定失败。"));
		}
	}

	_restTime = 0;

	return true;
}

void BindPhoneLayer::updateUserDetail()
{
	MSG_GP_UserInfo userInfo = PlatformLogic()->loginResult;
	std::string phoneNumber = _editBox_PhoneNum->getString();
	strcpy(userInfo.szMobileNo, phoneNumber.c_str());
	PlatformLogic()->sendData(MDM_GP_USERINFO, ASS_GP_USERINFO_UPDATE_DETAIL, &userInfo, sizeof(userInfo));

	PlatformLogic()->addEventSelector(MDM_GP_USERINFO, ASS_GP_USERINFO_ACCEPT, HN_SOCKET_CALLBACK(BindPhoneLayer::modifyUserInfoSelector, this));
	PlatformLogic()->addEventSelector(MDM_GP_USERINFO, ASS_GP_USERINFO_NOTACCEPT, HN_SOCKET_CALLBACK(BindPhoneLayer::modifyUserInfoSelector, this));
}

bool BindPhoneLayer::modifyUserInfoSelector(HNSocketMessage* socketMessage)
{
	if (ASS_GP_USERINFO_ACCEPT == socketMessage->messageHead.bAssistantID)	// 服务端已接受
	{
		std::string phoneNumber = _editBox_PhoneNum->getString();
		strcpy(PlatformLogic()->loginResult.szMobileNo, phoneNumber.c_str());
	}
	else if (ASS_GP_USERINFO_NOTACCEPT == socketMessage->messageHead.bAssistantID)	// 服务端未能接受
	{
		
	}
	else
	{

	}
	return true;
}

//获取验证码的时间限制
void BindPhoneLayer::setLimitTime()
{
	this->schedule(schedule_selector(BindPhoneLayer::updateLimitTime), 1.0f);

	_timeText->setVisible(true);
	_btn_getCode->setEnabled(false);
	_btn_getCode->setBright(false);
}

void BindPhoneLayer::updateLimitTime(float dt)
{
	char str[64];
	_restTime -= 1;
	if (_restTime < 0)
	{
		this->unschedule(schedule_selector(BindPhoneLayer::updateLimitTime));
		UserDefault::getInstance()->setStringForKey("LimitStartTime", "noTime");
		UserDefault::getInstance()->setStringForKey("Mobilephone", "noMobilePhone");
		UserDefault::getInstance()->flush();
		_btn_getCode->setEnabled(true);
		_btn_getCode->setBright(true);
		_timeText->setVisible(false);
	}
	else
	{
		std::string str;
		str.append(StringUtils::toString(_restTime));
		_timeText->setString(GBKToUtf8(str.c_str()));
	}
}