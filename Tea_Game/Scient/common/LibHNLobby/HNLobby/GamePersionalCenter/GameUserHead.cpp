/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameUserHead.h"
#include <string>
const char* USERHEAD_VIP = "platform/common/VIP_%d.png";
namespace HN
{
	GameUserHead* GameUserHead::create(Node* tempHead, Node* headFrame/* = nullptr*/)
	{
		GameUserHead *pRet = new GameUserHead();
		if (pRet && pRet->init(tempHead, headFrame))
		{
			pRet->autorelease();
		}
		else
		{
			delete pRet;
			pRet = nullptr;
		}

		return pRet;
	}

	bool GameUserHead::init(Node* tempHead, Node* headFrame)
	{
		if (!ImageUrl::init(tempHead->getBoundingBox())) return false;

		_tempHead = tempHead;
		_tempHead->setVisible(false);

		if (headFrame)
		{
			_headFrame = headFrame;
			headFrame->setLocalZOrder(2);
		}
		
		return true;
	}

	void GameUserHead::setVisible(bool isVisible)
	{
		ImageUrl::setVisible(isVisible);
		if (_headFrame) _headFrame->setVisible(isVisible);
	}

	void GameUserHead::setHeadByFaceID(int faceID)
	{
		std::string name = StringUtils::format("platform/common/Head/Head%d.jpg", faceID);
		bool isExist = FileUtils::getInstance()->isFileExist(name);
		if (isExist)
		{
			this->loadTexture(name);
		}
	}

	void GameUserHead::setVIPHead(const std::string& stencilFile, int viplevel)
	{
		//屏蔽VIP头像框
		if (true)
		{
			return;
		}
		auto headframe = _tempHead->getParent()->getChildByName("HeadFrame");
		if (headframe)
		{
			headframe->removeFromParentAndCleanup(true);
		}
		Sprite* stencil = nullptr;
		if (!stencilFile.empty())
		{
			// 根据文件名称创建VIP头像框
			stencil = Sprite::create(stencilFile);
		}
		else
		{
			string filename = StringUtils::format(USERHEAD_VIP, viplevel);
			if (FileUtils::getInstance()->isFileExist(filename))
			{
				stencil = Sprite::create(filename);
			}
		}

		if (nullptr != stencil)
		{
			stencil->setPosition(_tempHead->getPosition());
			stencil->setLocalZOrder(5);
			stencil->setName("HeadFrame");
			_tempHead->getParent()->addChild(stencil);

			auto EffectNode = stencil->getChildByName("EffectNode");
			if (EffectNode)
			{
				EffectNode:removeFromParentAndCleanup(true);
			}
			if (viplevel > 2)
			{
				auto nodeEffect = CSLoader::createNode(StringUtils::format("platform/common/vip_%d_effect.csb", viplevel));
				nodeEffect->setAnchorPoint(Vec2(0.5, 0.5));
				nodeEffect->setPosition(Vec2(stencil->getContentSize().width / 2, stencil->getContentSize().height / 2));
				auto ation = CSLoader::createTimeline(StringUtils::format("platform/common/vip_%d_effect.csb", viplevel));
				nodeEffect->runAction(ation);
				ation->gotoFrameAndPlay(0, true);
				nodeEffect->setName("EffectNode");
				stencil->addChild(nodeEffect);
			}
		}

	}
	void GameUserHead::show(const std::string& stencilFile/* = ""*/)
	{
		Sprite* stencil = nullptr;
		if (!stencilFile.empty())
		{
			// 创建裁剪模板
			stencil = Sprite::create(stencilFile);
		}
		else
		{
			auto data = ((ImageView*)_tempHead)->getRenderFile();
			switch (data.type)
			{
			case (int)Widget::TextureResType::LOCAL:
				stencil = Sprite::create(data.file);
				break;
			case (int)Widget::TextureResType::PLIST:
				if (!data.plist.empty())
				{
					stencil = Sprite::createWithSpriteFrameName(data.plist);
				}
				else
				{
					if (!data.file.empty())
					{
						stencil = Sprite::createWithSpriteFrameName(data.file);
					}
				}
				break;
			default:
				break;
			}
		}

		if (stencil)
		{
			stencil->getTexture()->setAntiAliasTexParameters();

			// 创建裁剪节点
			ClippingNode* clipper = ClippingNode::create(stencil);
			clipper->setPosition(_tempHead->getPosition());
			clipper->setAlphaThreshold(0.5f);
			clipper->setCascadeOpacityEnabled(true);
			clipper->addChild(this);
			_tempHead->getParent()->addChild(clipper);
		}
		else
		{
			setPosition(_tempHead->getPosition());
			_tempHead->getParent()->addChild(this);
		}
	}



}