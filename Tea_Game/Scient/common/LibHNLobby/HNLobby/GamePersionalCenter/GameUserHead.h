/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_Game_UserHead_h__
#define __HN_Game_UserHead_h__

#include "HNUIExport.h"
#include "cocos2d.h"

namespace HN
{
	class GameUserHead : public ImageUrl
	{
	public:
		static GameUserHead* create(Node* tempHead, Node* headFrame = nullptr);

		// 显示玩家头像（若需要裁剪，传参裁剪模板，各种矩形透明边图片）
		// 裁剪节点无法设置大小，所以如果需要裁剪请传入所需大小的模板，无法进行缩放
		void show(const std::string& stencilFile = "");

		void setVisible(bool isVisible);

		void setHeadByFaceID(int faceID);

		CC_DEPRECATED_ATTRIBUTE void loadTextureFromUrl(const std::string& url, int userID = INVALID_USER_ID) { ImageUrl::loadTextureWithUrl(url); }

		void setVIPHead(const std::string& stencilFile ,int viplevel);

	protected:
		bool init(Node* tempHead, Node* headFrame);

	protected:
		Node*	_headFrame	= nullptr;
		Node*	_tempHead	= nullptr;
	};
}


#endif // __HN_Game_UserHead_h__