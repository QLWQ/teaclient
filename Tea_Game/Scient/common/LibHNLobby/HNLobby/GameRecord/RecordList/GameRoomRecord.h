/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_GAME_ROOMRECORD_H__
#define __HN_GAME_ROOMRECORD_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "GameTableViewDelegate.h"

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include <vector>

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

class GameRoomRecord : public HNLayer, public ui::EditBoxDelegate, public HNHttpDelegate
{
public:
	~GameRoomRecord();

	bool init(int clubID);

	static GameRoomRecord* create(int clubID = 0);

private:
	void closeFunc() override;

	bool loadUI(const char *sFileName);

	void otherRecord();

private:
	// 获取战绩（0，：VIP战绩，1：普通战绩, 2:单局战绩）
	void getRecord(RecordType type, int count, int recID = 0);

	// 获取回放文件地址
	void getRecordUrl(std::string recCode);

	// 获取大结算战绩信息
	bool getAllRecordCallback(HNSocketMessage* socketMessage);

	// 获取单局结算战绩信息
	bool getSingleRecordCallback(HNSocketMessage* socketMessage);

	// 获取回放文件地址
	bool getRecordUrlCallback(HNSocketMessage* socketMessage);

protected:
	virtual void editBoxReturn(ui::EditBox* editBox) {};

	void editBoxTextChanged(ui::EditBox* editBox, const std::string& text) {};

	void editBoxEditingDidBegin(ui::EditBox* editBox) {};

	void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)override;

	// 播放回放文件
	void playRcordVideo(const std::string& recordInfo);

protected:
	TableView*				_tableView	= nullptr;
	GameTableViewDelegate*	_delegate	= nullptr;
	GameDataViewDelegate*	_dataSource	= nullptr;
	ImageView*				_img_tip	= nullptr;
	ImageView*				_img_top  = nullptr;
	ImageView*				_img_code	= nullptr;
	ImageView*				_img_roomID = nullptr;
	RecordType				_recType	= ALLREC;	//当前显示战绩类型
	INT						_iRecordID	= 0;		//当前战绩ID
	INT						_iGameID = 0;			//当前回放的游戏ID
	bool					_haveNormal = true;		//是否需要普通场战绩
	bool					_isAddRec	= false;	//是否是下拉刷新
	CC_SYNTHESIZE_PASS_BY_REF(Vec2, _norListOffset, NorListOffset);//普通场结算列表当前滑动位置

	INT						_clubID = 0;	// 当前进入俱乐部ID
	char					_szRecordCode[10];	// 当前回放码				

protected:
	GameRoomRecord();
	DISALLOW_COPY_AND_ASSIGN(GameRoomRecord);

private:

};

#endif