/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__GameTableViewCell__
#define __HN__GameTableViewCell__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

namespace HN
{
	enum RecordType
	{
		ALLREC = 0,	//总VIP战绩
		NORREC,		//普通战绩
		ONEREC		//单局vip战绩
	};

	enum RecordListType
	{
		CLUBREC = 0,	//本群战绩
		MYREC		//自己的战绩
	};

	//用于显示的战绩结构体
	struct RecordStruct
	{
		bool		isOpen;				//列表是否展开
		bool		isRefresh;			//是否是刷新数据
		bool		isPlayBack;			//是否开启回放
		RecordType	iType;				//0：vip战绩，1：普通战绩
		int			iRecordID;			//战绩ID	
		int			iPlayerCount;		//玩家人数
		int			iScore[10];			//玩家成绩
		LLONG		iPlayTime;			//时间
		char		szRecordCode[10];	//回放码
		char		szGameName[32];		//游戏名称
		char		szPassWord[10];		//桌子密码（房号）
		char		szNickName[10][50];	//游戏玩家昵称

		RecordStruct()
		{
			memset(this, 0, sizeof(RecordStruct));
		}
	};
	//用于显示的茶楼战绩结构体
	struct RecordStruct_T
	{
		bool		isOpen;				//列表是否展开
		bool		isRefresh;			//是否是刷新数据
		bool		isPlayBack;			//是否开启回放
		RecordType		iType;				//0：vip战绩，1：普通战绩
		int		iIndex;				//记录计数
		char		szDeskPass[10];			// 房间号	
		int			iPlayerCount;		//玩家人数
		int		userScore[ClubTeahouseDeskMaxUsers];			//玩家成绩
		float		redflowersList[ClubTeahouseDeskMaxUsers];
		UINT		date;			//时间
		BYTE		roundCount;		// 游戏局数
		int		winScore;		// 分数
		char		szRoomName[32];		//游戏名称
		//char		szPassWord[10];		//桌子密码（房号）
		char		szUserNames[ClubTeahouseDeskMaxUsers][20];
		char		szGameSN[10];

		RecordStruct_T()
		{
			memset(this, 0, sizeof(RecordStruct_T));
		}
	};

	class GameTableViewCell : public TableViewCell
	{
	public:
		typedef std::function<void(int recID)> getSingleListCallBack;
		// 获取单局列表回调
		getSingleListCallBack onGetSingleListCallBack = nullptr;

		typedef std::function<void(bool isOpen, int index)> openCallBack;
		// 展开/收起 列表回调
		openCallBack onOpenCallBack = nullptr;

		typedef std::function<void(std::string recCode)> playRecCallBack;
		// 查看回放回调
		playRecCallBack onPlayCallBack = nullptr;

	public:
		GameTableViewCell();
		~GameTableViewCell();

		static GameTableViewCell* create();

		// 创建/刷新 cell
		void buildCell(RecordStruct* record);

		// 列表点击展开/收起
		void showCellOpenOrClose();

	private:
		RecordType	_type			= ALLREC;
		RecordStruct _record;
		Layout*		_layout			= nullptr;
		ListView*	_list_user		= nullptr;//展开列表
	};

}

#endif //__HN__GameTableViewCell__
