/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameTableViewDelegate.h"

namespace HN
{
	#define SPACEING   15    //间距
	//////////////////////////////////////////////////////////////////////////
	////				TableViewDelegate
	//////////////////////////////////////////////////////////////////////////
	GameTableViewDelegate::GameTableViewDelegate()
	{

	}

	GameTableViewDelegate::~GameTableViewDelegate()
	{

	}

	void GameTableViewDelegate::tableCellTouched(TableView* table, TableViewCell* cell)
	{
		log("tableCellTouched:%d", cell->getIdx());

		((GameTableViewCell*)cell)->showCellOpenOrClose();
	}

	void GameTableViewDelegate::tableCellHighlight(TableView* table, TableViewCell* cell)
	{

	}

	void GameTableViewDelegate::tableCellWillRecycle(TableView* table, TableViewCell* cell)
	{

	}

	//////////////////////////////////////////////////////////////////////////
	////				TableViewDataSource
	//////////////////////////////////////////////////////////////////////////
	GameDataViewDelegate::GameDataViewDelegate()
	{

	}

	GameDataViewDelegate::~GameDataViewDelegate()
	{

	}

	TableViewCell* GameDataViewDelegate::tableCellAtIndex(TableView *table, ssize_t idx)
	{
		GameTableViewCell* cell = dynamic_cast<GameTableViewCell*>(table->dequeueCell());

		bool isRefresh = cell;
		if (!cell)
		{
			cell = GameTableViewCell::create();

			cell->onGetSingleListCallBack = [=](int recID){

				// 记录进入小结算列表时大结算滑动的位置
				_allListOffset = table->getContentOffset();

				if (onGetSingleListCallBack) onGetSingleListCallBack(recID);
			};

			cell->onOpenCallBack = [=](bool isOpen, int index){

				_isOpenVec[index] = isOpen;

				Vec2 pt = table->getContentOffset();

				table->reloadData();

				if (_isOpenVec.size() >= 4 && index == _isOpenVec.size() - 1)
				{
					table->setContentOffset(pt);
				}
				else
				{
					if (isOpen)
					{
						table->setContentOffset(Vec2(pt.x, pt.y - 97));
					}
					else
					{
						table->setContentOffset(Vec2(pt.x, pt.y + 97));
					}
				}
			};

			cell->onPlayCallBack = [=](std::string recCode){

				if (onPlayCallBack) onPlayCallBack(recCode);
			};
		}

		RecordStruct record;
		record.isOpen = _isOpenVec[idx];
		record.isRefresh = isRefresh;
		record.iType = _recType;

		switch (_recType)
		{
		case HN::ALLREC:
			if (!_allVec.empty())
			{
				record.iPlayerCount = _allVec[idx].iPlayerCount;
				record.iPlayTime = _allVec[idx].iPlayTime;
				record.iRecordID = _allVec[idx].iRecordID;
				for (int i = 0; i < _allVec[idx].iPlayerCount; i++)
				{
					record.iScore[i] = _allVec[idx].iScore[i];
					strcpy(record.szNickName[i], _allVec[idx].szNickName[i]);
				}
				strcpy(record.szGameName, _allVec[idx].szGameName);
				strcpy(record.szPassWord, _allVec[idx].szPassWord);
			}
			break;
		case HN::NORREC:
			if (!_normalVec.empty())
			{
				record.iPlayerCount = _normalVec[idx].iPlayerCount;
				record.iPlayTime = _normalVec[idx].iPlayTime;
				record.iRecordID = _normalVec[idx].iRecordID;
				for (int i = 0; i < _normalVec[idx].iPlayerCount; i++)
				{
					record.iScore[i] = _normalVec[idx].iScore[i];
					strcpy(record.szNickName[i], _normalVec[idx].szNickName[i]);
				}
				strcpy(record.szGameName, _normalVec[idx].szGameName);
				strcpy(record.szPassWord, _normalVec[idx].szPassWord);
			}
			break;
		case HN::ONEREC:
			if (!_singleVec.empty())
			{
				auto game = GamesInfoModule()->findGameName(_singleVec[idx].iGameID);
				if (game) strcpy(record.szGameName, game->szGameName);
				record.iPlayerCount = _singleVec[idx].iPlayerCount;
				record.iPlayTime = _singleVec[idx].iPlayTime;
				record.isPlayBack = _singleVec[idx].bPlayBack;
				INT userId = PlatformLogic()->loginResult.dwUserID;
				for (int i = 0; i < _singleVec[idx].iPlayerCount; i++)
				{
					record.iScore[i] = _singleVec[idx].iScore[i];
					strcpy(record.szNickName[i], _singleVec[idx].szNickName[i]);
					if (userId == _singleVec[idx].szUserID[i])
					{
						strcpy(record.szRecordCode, _singleVec[idx].szRecordCode[i]);
					}
				}
				strcpy(record.szPassWord, _singleVec[idx].szPassWord);
			}
			break;
		default:
			break;
		}

		cell->setIdx(idx);
		cell->buildCell(&record);

		// 上拉刷新
		if (onRefreshCallBack)
		{
			switch (_recType)
			{
			case HN::ALLREC:
				if (!_allVec.empty() && (_allVec[idx].iTotalCount > _allVec.size()) && (_allVec.size() == idx + 1))
				{
					_oldOffset = table->getContentOffset();
					_oldCount = _allVec.size() + 1;
					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在刷新"), 25);
					table->runAction(Sequence::create(DelayTime::create(0.6f), CallFunc::create([=](){
						onRefreshCallBack(_allVec.size() + 1);
					}), nullptr));
				}
				break;
			case HN::NORREC:
				if (!_normalVec.empty() && (_normalVec[idx].iTotalCount > _normalVec.size()) && (_normalVec.size() == idx + 1))
				{
					_oldOffset = table->getContentOffset();
					_oldCount = _normalVec.size() + 1;
					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在刷新"), 25);
					table->runAction(Sequence::create(DelayTime::create(0.6f), CallFunc::create([=](){
						onRefreshCallBack(_normalVec.size() + 1);
					}), nullptr));
				}
				break;
			case HN::ONEREC:
				if (!_singleVec.empty() && (_singleVec[idx].iTotalCount > _singleVec.size()) && (_singleVec.size() == idx + 1))
				{
					_oldOffset = table->getContentOffset();
					_oldCount = _singleVec.size() + 1;
					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在刷新"), 25);
					table->runAction(Sequence::create(DelayTime::create(0.6f), CallFunc::create([=](){
						onRefreshCallBack(_singleVec.size() + 1);
					}), nullptr));
				}
				break;
			default:
				break;
			}
		}

		return cell;
	}

	ssize_t GameDataViewDelegate::numberOfCellsInTableView(TableView *table)
	{
		switch (_recType)
		{
		case HN::ALLREC:
			return _allVec.size();
			break;
		case HN::NORREC:
			return _normalVec.size();
			break;
		case HN::ONEREC:
			return _singleVec.size();
			break;
		default:
			break;
		}
		return 0;
	}

	Size GameDataViewDelegate::tableCellSizeForIndex(TableView *table, ssize_t idx)
	{
		if (_isOpenVec[idx])
		{
			return Size(963, 194 + SPACEING);
		}

		return Size(963, 97 + SPACEING);
	}

	// 设置所有大结算数据
	void GameDataViewDelegate::setAllVipRecordData(MSG_GP_O_GetTotalRecord* record, bool reset/* = false*/)
	{
		if (reset)
		{
			_isOpenVec.clear();
			_allVec.clear();
		}
		else
		{
			if (record)
			{
				_allVec.push_back(*record);
				_isOpenVec.push_back(false);
			}
		}
	}

	// 设置普通房间数据
	void GameDataViewDelegate::setNormalRecordData(MSG_GP_O_GetTotalRecord* record, bool reset/* = false*/)
	{
		if (reset)
		{
			_normalVec.clear();
			_allVec.clear();
		}
		else
		{
			if (record)
			{
				_normalVec.push_back(*record);
				_isOpenVec.push_back(false);
			}
		}
	}

	// 设置单个房间小结算数据
	void GameDataViewDelegate::setSingleRecordData(MSG_GP_O_GetSingleRecord* record, bool reset/* = false*/)
	{
		if (reset)
		{
			_singleVec.clear();
		}
		else
		{
			if (record)
			{
				_singleVec.push_back(*record);
				_isOpenVec.push_back(false);
			}
		}
	}

	bool GameDataViewDelegate::isEmpty()
	{
		switch (_recType)
		{
		case HN::ALLREC:
			return _allVec.empty();
			break;
		case HN::NORREC:
			return _normalVec.empty();
			break;
		case HN::ONEREC:
			return _singleVec.empty();
			break;
		default:
			break;
		}

		return true;
	}

	Vec2 GameDataViewDelegate::getRefreshOffset()
	{
		switch (_recType)
		{
		case HN::ALLREC:
			if (_allVec.size() == 1) return _oldOffset;
			return Vec2(_oldOffset.x, _oldOffset.y - (_allVec.size() - _oldCount) * 97);
			break;
		case HN::NORREC:
			if (_normalVec.size() == 1) return _oldOffset;
			return Vec2(_oldOffset.x, _oldOffset.y - (_normalVec.size() - _oldCount) * 97);
			break;
		case HN::ONEREC:
			if (_singleVec.size() == 1) return _oldOffset;
			return Vec2(_oldOffset.x, _oldOffset.y - (_singleVec.size() - _oldCount) * 97);
			break;
		default:
			return Vec2::ZERO;
			break;
		}
	}
}
