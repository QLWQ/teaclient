/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRoomRecordClub.h"
#include "GameTableViewCellClub.h"
#include "GameTableViewDelegateClub.h"
#include "../RecordPlay/BackPlay/BackPlayMaskLayer.h"
#include "../RecordPlay/RecordParse/RecordVideoController.h"
#include "../../GameUpdate/GameResUpdate.h"

USING_NS_CC;
using namespace HN;


#define DEBUG_RECORD 0


static const char* RECORD_CSB	= "platform/GameRecord/Node_record_club.csb";
static const char* LOOK_CSB		= "platform/GameRecord/Node_look.csb";

static const std::string TEA_HOUSE_DAYLIST[] = { "今日战绩", "昨日战绩", "前日战绩" };

GameRoomRecordClub::GameRoomRecordClub()
{
	memchr(_szRecordCode, 0, sizeof(_szRecordCode));
}

GameRoomRecordClub::~GameRoomRecordClub()
{
	HN_SAFE_DELETE(_delegate);
	HN_SAFE_DELETE(_dataSource); 
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GETTOTALRECORD);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GETSINGLERECORD);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GET_RECORDURL);
}

GameRoomRecordClub* GameRoomRecordClub::create(bool isManage, int clubID/* = 0*/)
{
	GameRoomRecordClub *pRet = new GameRoomRecordClub();
	if (pRet && pRet->init(clubID,isManage))
	{
		pRet->autorelease();
	}
	else
	{
		delete pRet;
		pRet = nullptr;
	}

	return pRet;
}

void GameRoomRecordClub::closeFunc()
{
	
}

bool GameRoomRecordClub::init(int clubID, bool isManage)
{
	if (!HNLayer::init()) return false;

	_clubID = clubID;

	_IsManage = isManage;

	if (!loadUI(RECORD_CSB)) return false;

	return true;
}

bool GameRoomRecordClub::loadUI(const char *sFileName)
{
	if (!sFileName || !FileUtils::getInstance()->isFileExist(sFileName))
		return false;

	auto rootNode = CSLoader::createNode(sFileName);
	if (!rootNode) return false;
	rootNode->setPosition(_winSize / 2);
	addChild(rootNode,101);

	auto img_bg = dynamic_cast<ImageView*>(rootNode->getChildByName("Image_BG"));
	auto panel_bg = dynamic_cast<Layout*>(rootNode->getChildByName("Panel_bg"));
	panel_bg->addClickEventListener([=](Ref*){
		this->removeFromParent();
	});
	if (_clubID)panel_bg->setVisible(true);
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}

	auto Panel_child = (Layout*)img_bg->getChildByName("Panel_child");

	// 关闭按钮
	auto btn_close = dynamic_cast<Button*>(Panel_child->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*)
	{
		close();
	});

	auto layout_rec = dynamic_cast<Layout*>(Panel_child->getChildByName("Panel_rec"));

	// 创建代理
	_delegate = new GameTableViewDelegateClub();
	_dataSource = new GameDataViewDelegateClub();

	// 战绩列表
	_tableView = TableView::create(_dataSource, layout_rec->getContentSize());
	_tableView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	_tableView->setDelegate(_delegate);
	layout_rec->addChild(_tableView);

	// 无战绩提示
	_img_tip = dynamic_cast<ImageView*>(Panel_child->getChildByName("Image_tip"));
	_img_tip->setVisible(false);
	_img_top = dynamic_cast<ImageView*>(Panel_child->getChildByName("Image_top"));
	_img_top->setVisible(false);

	/*auto img_recBG = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_recBG"));
	_img_code = dynamic_cast<ImageView*>(img_recBG->getChildByName("Image_code"));
	_img_roomID = dynamic_cast<ImageView*>(img_recBG->getChildByName("Image_room"));
	auto img_details = dynamic_cast<ImageView*>(img_recBG->getChildByName("Image_details"));*/

	// vip房战绩
	//auto btn_vip = dynamic_cast<Button*>(Panel_child->getChildByName("Button_vip"));
	////auto sp_vip = dynamic_cast<ImageView*>(Panel_child->getChildByName("sp_vip"));
	//// 普通场战绩
	//auto btn_normal = dynamic_cast<Button*>(Panel_child->getChildByName("Button_normal"));
	////auto sp_normal = dynamic_cast<ImageView*>(Panel_child->getChildByName("sp_normal"));
	//btn_normal->setVisible(false);
	//// 俱乐部战绩
	//auto btn_club = dynamic_cast<Button*>(Panel_child->getChildByName("Button_club"));
	////auto sp_club = dynamic_cast<ImageView*>(Panel_child->getChildByName("sp_club"));
	//btn_club->setVisible(_clubID != 0);
	//sp_club->setVisible(_clubID != 0);
	//btn_club->setVisible(true);
	//auto spr_vip = dynamic_cast<Sprite*>(btn_vip->getChildByName("Sprite_type"));
	//auto spr_normal = dynamic_cast<Sprite*>(btn_normal->getChildByName("Sprite_type"));

	//btn_vip->addClickEventListener([=](Ref*){
	//	btn_vip->setEnabled(false);
	//	//sp_vip->setVisible(true);
	//	btn_normal->setEnabled(true);
	//	//sp_normal->setVisible(false);

	//	if (_recType == NORREC)
	//	{
	//		_norListOffset = _tableView->getContentOffset();
	//	}

	//	_recType = ALLREC;
	//	_dataSource->setRecordType(ALLREC);
	//	_tableView->reloadData();
	//	_tableView->setContentOffset(_dataSource->getAllListOffset());

	//	_img_tip->setVisible(_dataSource->isEmpty());
	//	_img_top->setVisible(!_dataSource->isEmpty());
	//});

	//btn_normal->addClickEventListener([=](Ref*){
	//	btn_vip->setEnabled(true);
	//	//sp_vip->setVisible(false);
	//	btn_normal->setEnabled(false);
	//	//sp_normal->setVisible(true);
	//	if (_recType == ALLREC)
	//	{
	//		_dataSource->setAllListOffset(_tableView->getContentOffset());
	//	}

	//	_recType = NORREC;
	//	_dataSource->setRecordType(NORREC);

	//	if (_dataSource->isEmpty())
	//	{
	//		getRecord(NORREC, 0);
	//	}
	//	else
	//	{
	//		_tableView->reloadData();
	//		_tableView->setContentOffset(_norListOffset);
	//	}

	//	_img_tip->setVisible(_dataSource->isEmpty());
	//	_img_top->setVisible(!_dataSource->isEmpty());
	//});

	//btn_vip->setEnabled(false);
	//btn_vip->setVisible(_haveNormal && _clubID == 0);
	////sp_vip->setVisible(_haveNormal && _clubID == 0);
	//btn_normal->setVisible(_haveNormal && _clubID == 0);
	//sp_normal->setVisible(_haveNormal && _clubID == 0);
	//btn_normal->setVisible(false);

	// 查看他人回放
	auto btn_other = dynamic_cast<Button*>(Panel_child->getChildByName("Button_otherRec"));
	//btn_other->setVisible(false);
	btn_other->addClickEventListener([=](Ref*)
	{
		otherRecord();
	});

	//顶部按钮
	auto Panel_tab = (Layout*)img_bg->getChildByName("Panel_tab");

	//本群战绩
	_btn_GroupRecord = (Button*)Panel_tab->getChildByName("btn_GroupRecord");
	_btn_GroupRecord->setVisible(_IsManage);
	_btn_GroupRecord->addClickEventListener(CC_CALLBACK_1(GameRoomRecordClub::btnClickCallback, this));

	//显示统计数据
	_Panel_GroupRecord = (Layout*)Panel_tab->getChildByName("Panel_GroupRecord");

	_Panel_PersonalRecord = (Layout*)Panel_tab->getChildByName("Panel_PersonalRecord");

	//我的战绩
	_btn_PersonalRecord = (Button*)Panel_tab->getChildByName("btn_PersonalRecord");
	if (_IsManage)
	{
		_Panel_GroupRecord->setVisible(true);
		_Panel_PersonalRecord->setVisible(false);
		_Record_Type = CLUBREC;
		_btn_PersonalRecord->setEnabled(true);
		_dataSource->setShowRecordType(_Record_Type);
	}
	else
	{
		_Panel_GroupRecord->setVisible(false);
		_Panel_PersonalRecord->setVisible(true);
		_Record_Type = MYREC;
		_btn_PersonalRecord->setEnabled(false);
		auto sp = (Sprite*)_btn_PersonalRecord->getChildByName("sp_button");
		sp->setTexture("platform/GameRecord/res/new/out_game/word_wode_l.png");
		_btn_PersonalRecord->setPosition(_btn_GroupRecord->getPosition());
		_dataSource->setShowRecordType(_Record_Type);
	}
	_btn_PersonalRecord->addClickEventListener(CC_CALLBACK_1(GameRoomRecordClub::btnClickCallback, this));

	//查看他人回放
	auto Button_other = (Button*)_Panel_PersonalRecord->getChildByName("Button_other");
	Button_other->addClickEventListener([=](Ref*){
		otherRecord();
	});

	//点击更多时间选择
	_Button_more = (Button*)Panel_tab->getChildByName("Button_more");
	_Button_more->addClickEventListener([=](Ref*){
		if (_Panel_close)
		{
			_Panel_close->setVisible(true);
			_Image_more->setVisible(true);
		}
	});

	//当前选择的时间处理
	_Text_Time = (Text*)_Button_more->getChildByName("Text_Time");

	//点击关闭更多
	_Panel_close = (Layout*)Panel_tab->getChildByName("Panel_close");
	_Panel_close->addClickEventListener([=](Ref*){
		if (_Image_more)
		{
			_Image_more->setVisible(false);
			_Panel_close->setVisible(false);
		}
	});

	//更多按钮背景
	_Image_more = (ImageView*)Panel_tab->getChildByName("Image_more");

	//今日战绩
	auto btn_TodayRecord = (Button*)_Image_more->getChildByName("btn_TodayRecord");
	btn_TodayRecord->addClickEventListener([=](Ref*){
		SetQueryDay(0);
	});

	//昨日战绩
	auto btn_YesterdayRecord = (Button*)_Image_more->getChildByName("btn_YesterdayRecord");
	btn_YesterdayRecord->addClickEventListener([=](Ref*){
		SetQueryDay(1);
	});

	//前日战绩
	auto btn_QianriRecord = (Button*)_Image_more->getChildByName("btn_QianriRecord");
	btn_QianriRecord->addClickEventListener([=](Ref*){
		SetQueryDay(2);
	});

	// 返回按钮
	auto btn_return = dynamic_cast<Button*>(Panel_tab->getChildByName("Button_return"));
	btn_return->addClickEventListener([=](Ref*)
	{
		//btn_vip->setVisible(_haveNormal && _clubID == 0);
		////sp_vip->setOpacity(255);
		//btn_normal->setVisible(_haveNormal && _clubID == 0);//
		//sp_normal->setOpacity(255);
		btn_return->setVisible(false);
		//btn_club->setVisible(_clubID != 0);
		//sp_club->setOpacity(255);
		/*img_details->setVisible(true);
		_img_code->setVisible(false);*/
		//_img_roomID->setVisible(true);

		//_recType = ALLREC;
		//_dataSource->setRecordType(ALLREC);
		_dataSource->setRecID(-1);
		_tableView->reloadData();
		_tableView->setContentOffset(_dataSource->getAllListOffset());
		//_img_tip->setVisible(_dataSource->isEmpty());
		//_img_top->loadTexture("platform/GameRecord/res/new/zjbt1.png");
		//_img_top->setVisible(!_dataSource->isEmpty());
	});

	// 获取单局列表
	_dataSource->onGetSingleListCallBack = [=](int recID){

		//btn_vip->setVisible(false);
		////sp_vip->setOpacity(0);
		//btn_normal->setVisible(false);
		//sp_normal->setOpacity(0);
		btn_return->setVisible(true);
		//btn_club->setVisible(false);
		//sp_club->setOpacity(0);
		/*img_details->setVisible(false);*/

		// 清理数据
		//_dataSource->setSingleRecordData(nullptr, true);
		//getRecord(ONEREC, 1, recID);
		_tableView->reloadData();
	};

	// 上拉刷新
	_dataSource->onRefreshCallBack = [=](int startcount){
		//getRecord(_recType, startcount, _iRecordID);
	};

	// 查看回放
	_dataSource->onPlayCallBack = [=](std::string recCode){
		getRecordUrl(recCode);
	};

	// 初始获取数据
	//getRecord(ALLREC, 1);

	//获取初始化战绩记录
	SendAllRecordList();

	return true;
}

void GameRoomRecordClub::otherRecord()
{
	auto rootNode = CSLoader::createNode(LOOK_CSB);
	if (!rootNode) return;
	addChild(rootNode,102);

	auto layout = dynamic_cast<ui::Layout*>(rootNode->getChildByName("Panel_look"));
	if (!layout) return;
	layout->setPosition(_winSize / 2);

	Size layOutSize = layout->getContentSize();
	layout->setScaleX(_winSize.width / layOutSize.width);
	layout->setScaleY(_winSize.height / layOutSize.height);

	auto img_bg = dynamic_cast<ImageView*>(layout->getChildByName("Image_BG"));

	if (_winSize.width / _winSize.height < 1.78)
	{
		img_bg->setScale(layOutSize.width / _winSize.width, layOutSize.height / _winSize.height);
	}
	else
	{
		img_bg->setScale(0.9f, layOutSize.height / _winSize.height * 0.9f);
	}

	// 回放码输入框
	auto textField_rec = (TextField*)img_bg->getChildByName("TextField_rec");
	textField_rec->setVisible(false);
	auto editBox_rec = HNEditBox::createEditBox(textField_rec, this);

	//关闭按钮
	auto btn_close = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*){
		img_bg->runAction(Sequence::create(FadeOut::create(0.15f), CallFunc::create([=](){
			rootNode->removeFromParent();
		}), nullptr));
	});

	// 取消按钮
	auto btn_cancle = dynamic_cast<Button*>(img_bg->getChildByName("Button_cancle"));
	btn_cancle->addClickEventListener([=](Ref*){
		img_bg->runAction(Sequence::create(FadeOut::create(0.15f), CallFunc::create([=](){
			rootNode->removeFromParent();
		}), nullptr));
	});

	// 确认按钮
	auto btn_sure = dynamic_cast<Button*>(img_bg->getChildByName("Button_sure"));
	btn_sure->addClickEventListener([=](Ref*){
		std::string code = editBox_rec->getString();
		if (code.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("回放码不能为空"));
			return;
		}

		if (!Tools::verifyNumber(code))//|| code.size() != 7)	回放修改不是固定9位数
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的回放码"));
			return;
		}

		getRecordUrl(code);
	});
}

void GameRoomRecordClub::SetQueryDay(int Index)
{
	_Image_more->setVisible(false);
	_Panel_close->setVisible(false);
	_Text_Time->setString(GBKToUtf8(TEA_HOUSE_DAYLIST[Index]));
	if (Index == _Query_Day)
	{
		return;
	}
	_dataSource->setRecID(-1);
	_Query_Day = Index;
	SendAllRecordList();
}

void GameRoomRecordClub::SendAllRecordList()
{
	if (Vec_SClubQunRoomLog.size() > 0)Vec_SClubQunRoomLog.clear();
	if (Vec_SClubMeRoomLog.size() > 0)Vec_SClubMeRoomLog.clear();
	SClubRequest_QueryQunGameLog Date;
	Date.clubId = _clubID;
	Date.gameNameId = 0;
	Date.msg = ClubRequest_QueryQunGameLog;
	Date.query_day = _Query_Day;
	PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_OperationRequest, &Date, sizeof(SClubRequest_QueryQunGameLog), HN_SOCKET_CALLBACK(GameRoomRecordClub::getAllRecordListResult, this));
}

void GameRoomRecordClub::getRecord(RecordType type, int count, int recID/* = 0*/)
{
	_recType = type;
	_iRecordID = recID;
	_isAddRec = count > 1;
	_dataSource->setRecordType(type);

	if (type == ONEREC)
	{
		MSG_GP_I_GetSingleRecord get;
		get.iRecordID = recID;
		get.iStartCount = count;
		get.iUserID = PlatformLogic()->loginResult.dwUserID;

		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GETSINGLERECORD, &get, sizeof(MSG_GP_I_GetSingleRecord),
			HN_SOCKET_CALLBACK(GameRoomRecordClub::getSingleRecordCallback, this));
	}
	else
	{
		MSG_GP_I_GetTotalRecord get;
		get.iType = type;
		get.iStartCount = count;
		get.iClubID = _clubID;
		get.iUserID = PlatformLogic()->loginResult.dwUserID;

		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GETTOTALRECORD, &get, sizeof(MSG_GP_I_GetTotalRecord),
			HN_SOCKET_CALLBACK(GameRoomRecordClub::getAllRecordCallback, this));
	}

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在获取信息"), 25);
}

bool GameRoomRecordClub::getAllRecordListResult(HNSocketMessage* SocketMessage)
{
	if (0 == SocketMessage->messageHead.bHandleCode)
	{
		SClubResponseBase* Data = (SClubResponseBase*)SocketMessage->object;
		if (ClubRequest_QueryQunGameLog == Data->msg)
		{
			SClubOperationResponse_QueryQunGameLog* Result = (SClubOperationResponse_QueryQunGameLog*)SocketMessage->object;
			//本群战绩还是自己战绩
			/*if (_Record_Type == CLUBREC)
			{*/
				int RecordNum = 0;
				int MeBigWinner = 0;
				auto Text_total_RoomNum = (Text*)_Panel_GroupRecord->getChildByName("Text_total_RoomNum");
				Text_total_RoomNum->setString(to_string(Result->roomCount));
				auto Text_Today_RoomNum = (Text*)_Panel_GroupRecord->getChildByName("Text_Today_RoomNum");
				Text_Today_RoomNum->setString(to_string(Result->todayRoomCount));
				auto Text_Yesterday_RoomNum = (Text*)_Panel_GroupRecord->getChildByName("Text_Yesterday_RoomNum");
				Text_Yesterday_RoomNum->setString(to_string(Result->yesterdayRoomCount));
				auto Text_SevenDay_RoomNum = (Text*)_Panel_GroupRecord->getChildByName("Text_SevenDay_RoomNum");
				Text_SevenDay_RoomNum->setString(to_string(Result->weekdayRoomCount));
				auto Text_30Day_RoomNum = (Text*)_Panel_GroupRecord->getChildByName("Text_30Day_RoomNum");
				Text_30Day_RoomNum->setString(to_string(Result->monthRoomCount));
				auto Text_RoomNum = (Text*)_Panel_GroupRecord->getChildByName("Text_RoomNum");
				Text_RoomNum->setString(to_string(Result->roomCount));
				auto Text_Big_Winner = (Text*)_Panel_GroupRecord->getChildByName("Text_Big_Winner");
				Text_Big_Winner->setString(to_string(Result->bigWinnerCount));

				if (Result->count > 0)
				{
					for (int i = 0; i < Result->count; i++)
					{
						Vec_SClubQunRoomLog.push_back(Result->logList[i]);
						int Score[ClubTeahouseDeskMaxUsers] = { 0, 0, 0, 0, 0, 0 };
						for (int j = 0; j < ClubTeahouseDeskMaxUsers; j++)
						{
							string Name = Result->logList[i].szUserNames[j];
							if (Name.empty())
							{
								break;
							}
							else
							{
								int MaxScore = 0;
								for (int z = 0; z < Result->logList[i].roundCount; z++)
								{
									Score[j] += Result->logList[i].userScore[j][z];
								}
								if (j > 0)
								{
									if (Score[j] >= MaxScore)
									{
										MaxScore = Score[j];
									}
								}
								else
								{
									MaxScore = Score[j];
								}
								if (!strcmp(PlatformLogic()->loginResult.nickName, Result->logList[i].szUserNames[j]))
								{
									RecordNum += Result->logList[i].winScore;
									if (MaxScore == Result->logList[i].winScore && MaxScore > 0)
									{
										MeBigWinner++;
									}
									Vec_SClubMeRoomLog.push_back(Result->logList[i]);
									break;
								}
							}
						}
					}
				}
				//刷新界面
				_dataSource->setAllRecordData(Vec_SClubQunRoomLog);
			/*}
			else
			{*/

				auto Text_RoomNum_1 = (Text*)_Panel_PersonalRecord->getChildByName("Text_RoomNum");
				Text_RoomNum_1->setString(to_string(Result->roomCount));

				auto Text_Big_Winner_1 = (Text*)_Panel_PersonalRecord->getChildByName("Text_Big_Winner");
				Text_Big_Winner_1->setString(to_string(MeBigWinner));

				//_Panel_GroupRecord->setVisible(false);
				//_Panel_PersonalRecord->setVisible(true);

				/*if (Result->count > 0)
				{
					for (int i = 0; i < Result->count; i++)
					{
						Vec_SClubQunRoomLog.push_back(Result->logList[i]);
						for (int j = 0; j < ClubTeahouseDeskMaxUsers; j++)
						{
							string Name = Result->logList[i].szUserNames[j];
							if (Name.empty())
							{
								break;
							}
							else
							{
								if (!strcmp(PlatformLogic()->loginResult.nickName, Result->logList[i].szUserNames[j]))
								{
									RecordNum += Result->logList[i].winScore;
									Vec_SClubMeRoomLog.push_back(Result->logList[i]);
									break;
								}
							}
						}
					}
				}*/

				auto Text_RecordNum = (Text*)_Panel_PersonalRecord->getChildByName("Text_RecordNum");
				Text_RecordNum->setString(to_string(RecordNum));

				//刷新界面
				//_dataSource->setAllRecordData(Vec_SClubMeRoomLog);
			//}
			_tableView->reloadData();
		}
	}
	return true;
}

bool GameRoomRecordClub::getAllRecordCallback(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetTotalRecord, socketMessage->objectSize, true,
			"MSG_GP_O_GetTotalRecord size of error!");
		auto record = (MSG_GP_O_GetTotalRecord*)socketMessage->object;

		if (0 == record->iType)
		{
			_dataSource->setAllVipRecordData(record);
		}
		else
		{
			_dataSource->setNormalRecordData(record);
		}
	}
	else
	{
		// 下拉刷新，则reload之后回到原来的位置
		if (_isAddRec)
		{
			Vec2 offset = _dataSource->getRefreshOffset();
			_tableView->reloadData();
			_tableView->setContentOffset(_dataSource->getRefreshOffset());
		}
		else
		{
			_tableView->reloadData();
		}

		_img_top->loadTexture("platform/GameRecord/res/new/zjbt1.png");
		//_img_tip->setVisible(_dataSource->isEmpty());
		//_img_top->setVisible(!_dataSource->isEmpty());

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}

	return true;
}

bool GameRoomRecordClub::getSingleRecordCallback(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetSingleRecord, socketMessage->objectSize, true,
			"MSG_GP_O_GetSingleRecord size of error!");
		auto record = (MSG_GP_O_GetSingleRecord*)socketMessage->object;

		_dataSource->setSingleRecordData(record);

		/*_img_code->setVisible(record->bPlayBack);*/
		//_img_roomID->setVisible(!record->bPlayBack);
	}
	else
	{
		// 下拉刷新，则reload之后回到原来的位置
		if (_isAddRec)
		{
			_tableView->reloadData();
			_tableView->setContentOffset(_dataSource->getRefreshOffset());
		}
		else
		{
			_tableView->reloadData();
		}

		_img_top->loadTexture("platform/GameRecord/res/new/zjbt1_1.png");
		//_img_tip->setVisible(_dataSource->isEmpty());
		//_img_top->setVisible(!_dataSource->isEmpty());

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}

	return true;
}

void GameRoomRecordClub::getRecordUrl(std::string recCode)
{
	MSG_GP_I_GetRecordUrl getUrl;
	strcpy(getUrl.szRecordCode, recCode.c_str());

	PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GET_RECORDURL, &getUrl, sizeof(MSG_GP_I_GetRecordUrl),
		HN_SOCKET_CALLBACK(GameRoomRecordClub::getRecordUrlCallback, this));
}

bool GameRoomRecordClub::getRecordUrlCallback(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetRecordUrl, socketMessage->objectSize, true,
		"MSG_GP_O_GetRecordUrl size of error!");
	auto recUrl = (MSG_GP_O_GetRecordUrl*)socketMessage->object;

	if (0 == socketMessage->messageHead.bHandleCode)
	{
		std::string url = "http://" + getAPIServerUrl() + "/PlayBack/" + StringUtils::format("%s.txt", recUrl->szRecordCode);

		strcpy(_szRecordCode, recUrl->szRecordCode);

		if (!url.empty())
		{
			_iGameID = recUrl->iGameNameID;
			HNHttpRequest::getInstance()->addObserver(this);
			HNHttpRequest::getInstance()->request("DownloadRecord", network::HttpRequest::Type::GET, url, "", "GameVideoRecord.dat");
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("回放记录下载中"), 20.0f);
		}
	}

	return true;
}

void GameRoomRecordClub::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	HNHttpRequest::getInstance()->removeObserver(this);
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	if (!isSucceed)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("回放文件下载失败,请重试"));
		return;
	}

	if (requestName.compare("DownloadRecord") == 0)
	{
		// 检测是否需要更新
		auto update = GameResUpdate::create();
		update->retain();
		update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {

			if (success)
			{
				playRcordVideo("GameVideoRecord.dat");
			}

			update->release();
		};
		update->checkUpdate(_iGameID, true);
	}
}

void GameRoomRecordClub::playRcordVideo(const std::string& recordInfo)
{
	std::string filePath = FileUtils::getInstance()->getWritablePath() + recordInfo;

	auto v = new VedioMask();

	HNLOG_DEBUG("%s", filePath.c_str());
	RecordVideoControllerIns()->startVideo(filePath, _iGameID, v, _szRecordCode);
}

void GameRoomRecordClub::btnClickCallback(Ref* pSender)
{
	auto selectbtn = dynamic_cast<Button*>(pSender);
	std::string name(selectbtn->getName());
	if (name.compare("btn_GroupRecord") == 0)
	{
		_Record_Type = CLUBREC;
		_btn_GroupRecord->setEnabled(false);
		auto sp = (Sprite*)_btn_GroupRecord->getChildByName("sp_button");
		sp->setTexture("platform/GameRecord/res/new/out_game/word_bqzj_l.png");

		_btn_PersonalRecord->setEnabled(true);
		auto sp_1 = (Sprite*)_btn_PersonalRecord->getChildByName("sp_button");
		sp_1->setTexture("platform/GameRecord/res/new/out_game/word_wode_a.png");

		_Panel_GroupRecord->setVisible(true);
		_Panel_PersonalRecord->setVisible(false);

		//更新数据
		/*if (_Query_Day != 0)
		{

		}
		else
		{

		}*/
		//SendAllRecordList();
		_dataSource->setShowRecordType(CLUBREC);
		_dataSource->setRecID(-1);
	}
	else if (name.compare("btn_PersonalRecord") == 0)
	{
		_Record_Type = MYREC;
		_btn_GroupRecord->setEnabled(true);
		auto sp = (Sprite*)_btn_GroupRecord->getChildByName("sp_button");
		sp->setTexture("platform/GameRecord/res/new/out_game/word_bqzj_a.png");

		_btn_PersonalRecord->setEnabled(false);
		auto sp_1 = (Sprite*)_btn_PersonalRecord->getChildByName("sp_button");
		sp_1->setTexture("platform/GameRecord/res/new/out_game/word_wode_l.png");

		_Panel_GroupRecord->setVisible(false);
		_Panel_PersonalRecord->setVisible(true);

		//更新数据
		/*if (_Query_Day != 0)
		{

		}
		else
		{

		}*/
		//SendAllRecordList();
		_dataSource->setShowRecordType(MYREC);
		_dataSource->setRecID(-1);
	}
	_tableView->reloadData();
}
