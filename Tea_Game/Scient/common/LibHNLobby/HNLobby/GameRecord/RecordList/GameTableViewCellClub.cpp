/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameTableViewCellClub.h"
#include "Tool/Tools.h"

namespace HN
{
	static const char *ITEM_CSB = "platform/GameRecord/Node_item_club.csb";
	static const char *CHILD_ITEM_CSB = "platform/GameRecord/Node_childItem_club.csb";

	GameTableViewCellClub::GameTableViewCellClub()
	{
		
	}

	GameTableViewCellClub::~GameTableViewCellClub()
	{
	}

	GameTableViewCellClub* GameTableViewCellClub::create()
	{
		GameTableViewCellClub* cell = new GameTableViewCellClub();

		if (cell && cell->init())
		{
			cell->autorelease();
			return cell;
		}
		CC_SAFE_DELETE(cell);
		return NULL;
	}

	void GameTableViewCellClub::buildCell(RecordStruct_T* rec)
	{
		_record = *rec;

		if (!_record.isRefresh)
		{
			auto rootNode = CSLoader::createNode(ITEM_CSB);
			if (!rootNode) return;

			_layout = dynamic_cast<ui::Layout*>(rootNode->getChildByName("Panel_item"));
			if (!_layout) return;
			_layout->removeFromParentAndCleanup(false);
			addChild(_layout);
		}

		if (!_layout) return;

		auto img_open = dynamic_cast<ImageView*>(_layout->getChildByName("Image_open"));

		// 展开/收起 重置cell大小
		if (!_record.isOpen)
		{
			setContentSize(Size(1220, 75));
			img_open->setRotation(0);
		}
		else
		{
			setContentSize(Size(1220, 188));
			img_open->setRotation(180);
		}
		_layout->setPosition(Vec2(getContentSize().width / 2, getContentSize().height));

		// 列表序号
		auto text_num = dynamic_cast<Text*>(_layout->getChildByName("Text_number"));
		text_num->setString(StringUtils::format("%d", getIdx() + 1));

		// 游戏名称
		auto text_gameName = dynamic_cast<Text*>(_layout->getChildByName("Text_game"));
		text_gameName->setString(GBKToUtf8(_record.szRoomName));

		// 房号
		auto text_roomNum = dynamic_cast<Text*>(_layout->getChildByName("Text_roomNum"));

		switch (_record.iType)
		{
		case ALLREC:
			text_roomNum->setString(_record.szDeskPass);
			break;
		case NORREC:
			text_roomNum->setString(GBKToUtf8("金币场"));
			break;
		case ONEREC:
		{
			if (_record.isPlayBack)
			{
				text_roomNum->setString(_record.szGameSN);
			}
			else
			{
				text_roomNum->setString(_record.szDeskPass);
			}
		} break;
		default:
			break;
		}

		int score = 0;
		bool bHade = true;
		for (int i = 0; i < _record.iPlayerCount; i++)
		{
			if (!strcmp(PlatformLogic()->loginResult.nickName, _record.szUserNames[i]))
			{
				bHade = false;
				score = _record.userScore[i]; break;
			}
		}

		// 输赢得分
		auto text_score = dynamic_cast<Text*>(_layout->getChildByName("Text_score"));
		text_score->setString(to_string(score));
		text_score->setVisible(!bHade);

		time_t Time = _record.date;
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		if (p->tm_hour >= 24)
		{
			p->tm_hour -= 24;
		}
		char BeginTime[64];
		strftime(BeginTime, sizeof(BeginTime), "%Y-%m-%d\n (%H:%M)", p);

		// 战绩时间
		auto text_time = dynamic_cast<Text*>(_layout->getChildByName("Text_time"));
		text_time->setString(StringUtils::format("%s", BeginTime));

		auto layout_details = dynamic_cast<Layout*>(_layout->getChildByName("Panel_details"));
		layout_details->setVisible(_record.isOpen);

		_list_user = dynamic_cast<ListView*>(layout_details->getChildByName("ListView_details"));
		_list_user->removeAllItems();
		_list_user->setScrollBarEnabled(false);

		// 点击展开之后创建详细信息列表
		if (_record.isOpen)
		{
			for (int i = 0; i < _record.iPlayerCount; i++)
			{
				string str_name = _record.szUserNames[i];
				if (str_name.empty())
				{
					break;
				}
				auto node = CSLoader::createNode(CHILD_ITEM_CSB);
				auto img_user = dynamic_cast<ImageView*>(node->getChildByName("Image_item"));
				img_user->removeFromParentAndCleanup(false);

				auto text_userName = dynamic_cast<Text*>(img_user->getChildByName("Text_name"));
				string str = _record.szUserNames[i];
				if (str.length() > 10)
				{
					str.erase(11, str.length());
					str.append("...");
					text_userName->setString(GBKToUtf8(str));
				}
				else
				{
					text_userName->setString(GBKToUtf8(str));
				}

				auto text_userScore = dynamic_cast<Text*>(img_user->getChildByName("Text_score"));
				text_userScore->setString(to_string(_record.userScore[i]));

				auto Text_RedFlower = (Text*)img_user->getChildByName("Text_RedFlower");
				Text_RedFlower->setString(to_string((int)_record.redflowersList[i]));

				_list_user->pushBackCustomItem(img_user);
			}
		}

		//只有麻将显示回放录像，其他都是显示单局详情
		char gameNameQZMJ[32];
		char gameNameJJMJ[32];
		strcpy(gameNameQZMJ, "泉州麻将");
		strcpy(gameNameJJMJ, "晋江麻将");
		int retQZMJ;
		int retJJMJ;
		retQZMJ = strcmp(_record.szRoomName, gameNameQZMJ);
		retJJMJ = strcmp(_record.szRoomName, gameNameJJMJ);
		bool isMJGame = false;
		if (retQZMJ == 0 || retJJMJ == 0)
		{
			isMJGame = true;
		}


		// 查看回放
		auto btn_look = dynamic_cast<Button*>(_layout->getChildByName("Button_look"));
		btn_look->loadTextureNormal("platform/GameRecord/res/new/out_game/btn_luxiang.png");
		btn_look->setVisible(_record.iType == ONEREC && _record.isPlayBack && isMJGame);
		btn_look->addClickEventListener([=](Ref*){

			if (onPlayCallBack) onPlayCallBack(_record.szGameSN);
		});

		// 查看单局结算列表
		auto btn_single = dynamic_cast<Button*>(_layout->getChildByName("Button_single"));
		btn_single->setVisible(_record.iType == ALLREC);
		if (isMJGame)
		{
			btn_single->loadTextureNormal("platform/GameRecord/res/new/out_game/btn_huifang.png");
		}
		else
		{
			btn_single->loadTextureNormal("platform/GameRecord/res/new/out_game/btn_xiangqing.png");
		}
		btn_single->addClickEventListener([=](Ref*){

			if (onGetSingleListCallBack) onGetSingleListCallBack(_record.iIndex);
		});
	}

	void GameTableViewCellClub::showCellOpenOrClose()
	{
		if (onOpenCallBack) onOpenCallBack(!_record.isOpen, getIdx());
	}
}