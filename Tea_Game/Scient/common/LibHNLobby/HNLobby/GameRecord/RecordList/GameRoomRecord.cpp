/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRoomRecord.h"
#include "GameTableViewCell.h"
#include "GameTableViewDelegate.h"
#include "../RecordPlay/BackPlay/BackPlayMaskLayer.h"
#include "../RecordPlay/RecordParse/RecordVideoController.h"
#include "../../GameUpdate/GameResUpdate.h"

USING_NS_CC;
using namespace HN;


#define DEBUG_RECORD 0


static const char* RECORD_CSB = "platform/GameRecord/Node_record.csb";
static const char* LOOK_CSB = "platform/GameRecord/Node_look.csb";
#define BTN_NORMAL_COLOR	Color3B(161,86,41)    //普通颜色
#define BTN_SELECT_COLOR	Color3B(242,232,210)  //选中颜色

GameRoomRecord::GameRoomRecord()
{
	memchr(_szRecordCode, 0, sizeof(_szRecordCode));
}

GameRoomRecord::~GameRoomRecord()
{
	HN_SAFE_DELETE(_delegate);
	HN_SAFE_DELETE(_dataSource);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GETTOTALRECORD);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GETSINGLERECORD);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GET_RECORDURL);
}

GameRoomRecord* GameRoomRecord::create(int clubID/* = 0*/)
{
	GameRoomRecord *pRet = new GameRoomRecord();
	if (pRet && pRet->init(clubID))
	{
		pRet->autorelease();
	}
	else
	{
		delete pRet;
		pRet = nullptr;
	}

	return pRet;
}

void GameRoomRecord::closeFunc()
{

}

bool GameRoomRecord::init(int clubID)
{
	if (!HNLayer::init()) return false;

	_clubID = clubID;

	if (!loadUI(RECORD_CSB)) return false;

	return true;
}

bool GameRoomRecord::loadUI(const char *sFileName)
{
	if (!sFileName || !FileUtils::getInstance()->isFileExist(sFileName))
		return false;

	auto rootNode = CSLoader::createNode(sFileName);
	if (!rootNode) return false;
	rootNode->setPosition(_winSize / 2);
	addChild(rootNode, 101);

	auto img_bg = dynamic_cast<ImageView*>(rootNode->getChildByName("Image_BG"));
	auto panel_bg = dynamic_cast<Layout*>(rootNode->getChildByName("Panel_bg"));
	panel_bg->addClickEventListener([=](Ref*){
		this->removeFromParent();
	});
	if (_clubID)panel_bg->setVisible(true);
	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_bg->setScale(0.9f);
	}

	// 关闭按钮
	auto btn_close = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*)
	{
		close();
	});

	auto layout_rec = dynamic_cast<Layout*>(img_bg->getChildByName("Panel_rec"));

	// 创建代理
	_delegate = new GameTableViewDelegate();
	_dataSource = new GameDataViewDelegate();

	// 战绩列表
	_tableView = TableView::create(_dataSource, layout_rec->getContentSize());
	_tableView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	_tableView->setDelegate(_delegate);
	layout_rec->addChild(_tableView);

	// 无战绩提示
	_img_tip = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_tip"));
	_img_top = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_top"));

	// vip房战绩(房卡场)
	auto btn_vip = dynamic_cast<Button*>(img_bg->getChildByName("Button_vip"));
	// 普通场战绩（金币场）
	auto btn_normal = dynamic_cast<Button*>(img_bg->getChildByName("Button_normal"));
	btn_normal->setVisible(false);
	// 俱乐部战绩
	auto btn_club = dynamic_cast<Button*>(img_bg->getChildByName("Button_club"));

	btn_club->setVisible(_clubID != 0);
	btn_club->loadTextureNormal(_clubID != 0 ? "platform/common/new/yxsm_an.png" : "");
	btn_club->setTitleColor(_clubID != 0 ? BTN_SELECT_COLOR : BTN_NORMAL_COLOR);

	btn_vip->loadTextureNormal("platform/common/new/yxsm_an.png");
	btn_vip->setTitleColor(BTN_SELECT_COLOR);
	btn_normal->loadTextureNormal("");
	btn_normal->setTitleColor(BTN_NORMAL_COLOR);
	btn_vip->addClickEventListener([=](Ref*){

		btn_vip->loadTextureNormal("platform/common/new/yxsm_an.png");
		btn_normal->loadTextureNormal("");
		btn_vip->setTitleColor(BTN_SELECT_COLOR);
		btn_normal->setTitleColor(BTN_NORMAL_COLOR);
		if (_recType == NORREC)
		{
			_norListOffset = _tableView->getContentOffset();
		}

		_recType = ALLREC;
		_dataSource->setRecordType(ALLREC);
		_tableView->reloadData();
		_tableView->setContentOffset(_dataSource->getAllListOffset());

		_img_tip->setVisible(_dataSource->isEmpty());
		_img_top->setVisible(!_dataSource->isEmpty());
	});

	btn_normal->addClickEventListener([=](Ref*){
		btn_vip->loadTextureNormal("");
		btn_normal->loadTextureNormal("platform/common/new/yxsm_an.png");
		btn_vip->setTitleColor(BTN_NORMAL_COLOR);
		btn_normal->setTitleColor(BTN_SELECT_COLOR);

		if (_recType == ALLREC)
		{
			_dataSource->setAllListOffset(_tableView->getContentOffset());
		}

		_recType = NORREC;
		_dataSource->setRecordType(NORREC);

		if (_dataSource->isEmpty())
		{
			getRecord(NORREC, 0);
		}
		else
		{
			_tableView->reloadData();
			_tableView->setContentOffset(_norListOffset);
		}

		_img_tip->setVisible(_dataSource->isEmpty());
		_img_top->setVisible(!_dataSource->isEmpty());
	});

	btn_vip->setVisible(_haveNormal && _clubID == 0);
	btn_normal->setVisible(_haveNormal && _clubID == 0);

	// 查看他人回放
	auto btn_other = dynamic_cast<Button*>(img_bg->getChildByName("Button_otherRec"));
	float fScale = ((btn_other->getContentSize().height + 16) / btn_other->getContentSize().height);
	btn_other->setContentSize(btn_other->getContentSize() * fScale);
	btn_other->addClickEventListener([=](Ref*)
	{
		otherRecord();
	});

	// 返回按钮
	auto btn_return = dynamic_cast<Button*>(img_bg->getChildByName("Button_return"));
	btn_return->setVisible(false);
	btn_return->addClickEventListener([=](Ref*)
	{
		btn_vip->setVisible(_haveNormal && _clubID == 0);
		btn_normal->setVisible(_haveNormal && _clubID == 0);//
		btn_return->setVisible(false);
		btn_club->setVisible(_clubID != 0);

		_recType = ALLREC;
		_dataSource->setRecordType(ALLREC);
		_tableView->reloadData();
		_tableView->setContentOffset(_dataSource->getAllListOffset());
		_img_tip->setVisible(_dataSource->isEmpty());
		_img_top->loadTexture("platform/GameRecord/res/new/lmbt_dt.png");
		_img_top->setVisible(!_dataSource->isEmpty());
	});

	// 获取单局列表
	_dataSource->onGetSingleListCallBack = [=](int recID){

		btn_vip->setVisible(false);
		btn_normal->setVisible(false);
		btn_return->setVisible(true);
		btn_club->setVisible(false);

		// 清理数据
		_dataSource->setSingleRecordData(nullptr, true);
		getRecord(ONEREC, 1, recID);
	};

	// 上拉刷新
	_dataSource->onRefreshCallBack = [=](int startcount){
		getRecord(_recType, startcount, _iRecordID);
	};

	// 查看回放
	_dataSource->onPlayCallBack = [=](std::string recCode){
		getRecordUrl(recCode);
	};

	// 初始获取数据
	getRecord(ALLREC, 1);

	return true;
}

void GameRoomRecord::otherRecord()
{
	auto rootNode = CSLoader::createNode(LOOK_CSB);
	if (!rootNode) return;
	addChild(rootNode, 102);

	auto layout = dynamic_cast<ui::Layout*>(rootNode->getChildByName("Panel_look"));
	if (!layout) return;
	layout->setPosition(_winSize / 2);

	Size layOutSize = layout->getContentSize();
	layout->setScaleX(_winSize.width / layOutSize.width);
	layout->setScaleY(_winSize.height / layOutSize.height);

	auto img_bg = dynamic_cast<ImageView*>(layout->getChildByName("Image_BG"));

	if (_winSize.width / _winSize.height < 1.78)
	{
		img_bg->setScale(layOutSize.width / _winSize.width, layOutSize.height / _winSize.height);
	}
	else
	{
		img_bg->setScale(0.9f, layOutSize.height / _winSize.height * 0.9f);
	}

	// 回放码输入框
	auto textField_rec = (TextField*)img_bg->getChildByName("TextField_rec");
	textField_rec->setVisible(false);
	auto editBox_rec = HNEditBox::createEditBox(textField_rec, this);

	// 取消按钮
	auto btn_cancle = dynamic_cast<Button*>(img_bg->getChildByName("Button_cancle"));
	btn_cancle->addClickEventListener([=](Ref*){
		img_bg->runAction(Sequence::create(FadeOut::create(0.15f), CallFunc::create([=](){
			rootNode->removeFromParent();
		}), nullptr));
	});

	// 确认按钮
	auto btn_sure = dynamic_cast<Button*>(img_bg->getChildByName("Button_sure"));
	btn_sure->addClickEventListener([=](Ref*){
		std::string code = editBox_rec->getString();
		if (code.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("回放码不能为空"));
			return;
		}

		if (!Tools::verifyNumber(code))//|| code.size() != 7)	回放修改不是固定9位数
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的回放码"));
			return;
		}

		getRecordUrl(code);
	});

	// 关闭按钮
	auto btn_close = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*){
		rootNode->removeFromParent();
	});
}

void GameRoomRecord::getRecord(RecordType type, int count, int recID/* = 0*/)
{
	_recType = type;
	_iRecordID = recID;
	_isAddRec = count > 1;
	_dataSource->setRecordType(type);

	if (type == ONEREC)
	{
		MSG_GP_I_GetSingleRecord get;
		get.iRecordID = recID;
		get.iStartCount = count;
		get.iUserID = PlatformLogic()->loginResult.dwUserID;

		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GETSINGLERECORD, &get, sizeof(MSG_GP_I_GetSingleRecord),
			HN_SOCKET_CALLBACK(GameRoomRecord::getSingleRecordCallback, this));
	}
	else
	{
		MSG_GP_I_GetTotalRecord get;
		get.iType = type;
		get.iStartCount = count;
		get.iClubID = _clubID;
		get.iUserID = PlatformLogic()->loginResult.dwUserID;

		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GETTOTALRECORD, &get, sizeof(MSG_GP_I_GetTotalRecord),
			HN_SOCKET_CALLBACK(GameRoomRecord::getAllRecordCallback, this));
	}

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在获取信息"), 25);
}

bool GameRoomRecord::getAllRecordCallback(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetTotalRecord, socketMessage->objectSize, true,
			"MSG_GP_O_GetTotalRecord size of error!");
		auto record = (MSG_GP_O_GetTotalRecord*)socketMessage->object;

		if (0 == record->iType)
		{
			_dataSource->setAllVipRecordData(record);
		}
		else
		{
			_dataSource->setNormalRecordData(record);
		}
	}
	else
	{
		// 下拉刷新，则reload之后回到原来的位置
		if (_isAddRec)
		{
			Vec2 offset = _dataSource->getRefreshOffset();
			_tableView->reloadData();
			_tableView->setContentOffset(_dataSource->getRefreshOffset());
		}
		else
		{
			_tableView->reloadData();
		}

		_img_top->loadTexture("platform/GameRecord/res/new/lmbt_dt.png");
		_img_tip->setVisible(_dataSource->isEmpty());
		_img_top->setVisible(!_dataSource->isEmpty());

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}

	return true;
}

bool GameRoomRecord::getSingleRecordCallback(HNSocketMessage* socketMessage)
{
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetSingleRecord, socketMessage->objectSize, true,
			"MSG_GP_O_GetSingleRecord size of error!");
		auto record = (MSG_GP_O_GetSingleRecord*)socketMessage->object;

		_dataSource->setSingleRecordData(record);

	}
	else
	{
		// 下拉刷新，则reload之后回到原来的位置
		if (_isAddRec)
		{
			_tableView->reloadData();
			_tableView->setContentOffset(_dataSource->getRefreshOffset());
		}
		else
		{
			_tableView->reloadData();
		}

		_img_top->loadTexture("platform/GameRecord/res/new/lmmc.png");
		_img_tip->setVisible(_dataSource->isEmpty());
		_img_top->setVisible(!_dataSource->isEmpty());

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}

	return true;
}

void GameRoomRecord::getRecordUrl(std::string recCode)
{
	MSG_GP_I_GetRecordUrl getUrl;
	strcpy(getUrl.szRecordCode, recCode.c_str());

	PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GET_RECORDURL, &getUrl, sizeof(MSG_GP_I_GetRecordUrl),
		HN_SOCKET_CALLBACK(GameRoomRecord::getRecordUrlCallback, this));
}

bool GameRoomRecord::getRecordUrlCallback(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(MSG_GP_O_GetRecordUrl, socketMessage->objectSize, true,
		"MSG_GP_O_GetRecordUrl size of error!");
	auto recUrl = (MSG_GP_O_GetRecordUrl*)socketMessage->object;

	if (0 == socketMessage->messageHead.bHandleCode)
	{
		std::string url = "http://" + getAPIServerUrl() + "/PlayBack/" + StringUtils::format("%s.txt", recUrl->szRecordCode);

		strcpy(_szRecordCode, recUrl->szRecordCode);

		if (!url.empty())
		{
			_iGameID = recUrl->iGameNameID;
			HNHttpRequest::getInstance()->addObserver(this);
			HNHttpRequest::getInstance()->request("DownloadRecord", network::HttpRequest::Type::GET, url, "", "GameVideoRecord.dat");
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("回放记录下载中"), 20.0f);
		}
	}

	return true;
}

void GameRoomRecord::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	HNHttpRequest::getInstance()->removeObserver(this);
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	if (!isSucceed)
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("回放文件下载失败,请重试"));
		return;
	}

	if (requestName.compare("DownloadRecord") == 0)
	{
		// 检测是否需要更新
		auto update = GameResUpdate::create();
		update->retain();
		update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {

			if (success)
			{
				playRcordVideo("GameVideoRecord.dat");
			}

			update->release();
		};
		update->checkUpdate(_iGameID, true);
	}
}

void GameRoomRecord::playRcordVideo(const std::string& recordInfo)
{
	std::string filePath = FileUtils::getInstance()->getWritablePath() + recordInfo;

	auto v = new VedioMask();

	HNLOG_DEBUG("%s", filePath.c_str());
	RecordVideoControllerIns()->startVideo(filePath, _iGameID, v, _szRecordCode);
}

