/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__GameTableViewCell_Club__
#define __HN__GameTableViewCell_Club__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include "GameTableViewCell.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

namespace HN
{
	class GameTableViewCellClub : public TableViewCell
	{
	public:
		typedef std::function<void(int szGameSN)> getSingleListCallBack;
		// 获取单局列表回调
		getSingleListCallBack onGetSingleListCallBack = nullptr;

		typedef std::function<void(bool isOpen, int index)> openCallBack;
		// 展开/收起 列表回调
		openCallBack onOpenCallBack = nullptr;

		typedef std::function<void(std::string recCode)> playRecCallBack;
		// 查看回放回调
		playRecCallBack onPlayCallBack = nullptr;

	public:
		GameTableViewCellClub();
		~GameTableViewCellClub();

		static GameTableViewCellClub* create();

		// 创建/刷新 cell
		void buildCell(RecordStruct_T* record);

		// 列表点击展开/收起
		void showCellOpenOrClose();

	private:
		RecordType	_type			= ALLREC;
		RecordStruct_T _record;
		Layout*		_layout			= nullptr;
		ListView*	_list_user		= nullptr;//展开列表
	};

}

#endif //__HN__GameTableViewCell_Club__
