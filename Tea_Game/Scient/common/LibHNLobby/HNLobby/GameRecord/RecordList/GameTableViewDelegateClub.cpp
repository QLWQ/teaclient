/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameTableViewDelegateClub.h"

namespace HN
{
	//////////////////////////////////////////////////////////////////////////
	////				TableViewDelegate
	//////////////////////////////////////////////////////////////////////////
	GameTableViewDelegateClub::GameTableViewDelegateClub()
	{

	}

	GameTableViewDelegateClub::~GameTableViewDelegateClub()
	{

	}

	void GameTableViewDelegateClub::tableCellTouched(TableView* table, TableViewCell* cell)
	{
		log("tableCellTouched:%d", cell->getIdx());

		((GameTableViewCellClub*)cell)->showCellOpenOrClose();
	}

	void GameTableViewDelegateClub::tableCellHighlight(TableView* table, TableViewCell* cell)
	{

	}

	void GameTableViewDelegateClub::tableCellWillRecycle(TableView* table, TableViewCell* cell)
	{

	}

	//////////////////////////////////////////////////////////////////////////
	////				TableViewDataSource
	//////////////////////////////////////////////////////////////////////////
	GameDataViewDelegateClub::GameDataViewDelegateClub()
	{

	}

	GameDataViewDelegateClub::~GameDataViewDelegateClub()
	{

	}

	TableViewCell* GameDataViewDelegateClub::tableCellAtIndex(TableView *table, ssize_t idx)
	{
		GameTableViewCellClub* cell = dynamic_cast<GameTableViewCellClub*>(table->dequeueCell());

		bool isRefresh = cell;
		if (!cell)
		{
			cell = GameTableViewCellClub::create();

			cell->onGetSingleListCallBack = [=](int recID){

				// 记录进入小结算列表时大结算滑动的位置
				_allListOffset = table->getContentOffset();

				setRecID(recID);
				if (onGetSingleListCallBack) onGetSingleListCallBack(recID);
			};

			cell->onOpenCallBack = [=](bool isOpen, int index){
				
				_isOpenVec[index] = isOpen;

				Vec2 pt = table->getContentOffset();

				table->reloadData();

				if (_isOpenVec.size() >= 4 && index == _isOpenVec.size() - 1)
				{
					table->setContentOffset(pt);
				}
				else
				{
					if (isOpen)
					{
						table->setContentOffset(Vec2(pt.x, pt.y - 112));
					}
					else
					{
						table->setContentOffset(Vec2(pt.x, pt.y + 112));
					}
				}
			};

			cell->onPlayCallBack = [=](std::string recCode){

				if (onPlayCallBack) onPlayCallBack(recCode);				
			};
		}

		RecordStruct_T record;
		record.isOpen = _isOpenVec[idx];
		record.isRefresh = isRefresh;
		record.iIndex = idx;
		//record.iType = 0;
		if (m_RecID != -1)
		{
			record.iType = ONEREC;
			strcpy(record.szDeskPass, Now_Record.szDeskPass);
			record.isPlayBack = false;
			record.date = Now_Record.date;
			record.roundCount = Now_Record.roundCount;
			strcpy(record.szRoomName, Now_Record.szRoomName);
			strcpy(record.szGameSN, Now_Record.szGameSN[idx]);
			//strcpy(record.szPassWord, Now_Record.szDeskPass);
			int player_count = 0;
			for (int i = 0; i < ClubTeahouseDeskMaxUsers; i++)
			{
				string Name = Now_Record.szUserNames[i];
				if (Name.empty())
				{
					break;
				}
				player_count++;
				strcpy(record.szUserNames[i], Now_Record.szUserNames[i]);
				record.userScore[i] = Now_Record.userScore[i][idx];
				record.redflowersList[i] = Now_Record.redflowersList[i][idx];
			}
			record.iPlayerCount = player_count;
		}
		else
		{
			switch (Now_ShowType)
			{
			case CLUBREC:
			{
				auto Record = Vec_Record_Group[idx];
				record.iType = ALLREC;
				strcpy(record.szDeskPass, Record.szDeskPass);
				record.isPlayBack = false;
				record.date = Record.date;
				record.winScore = Record.winScore;
				record.roundCount = Record.roundCount;
				strcpy(record.szRoomName, Record.szRoomName);
				strcpy(record.szGameSN, Record.szGameSN[idx]);
				//strcpy(record.szPassWord, Now_Record.szDeskPass);
				int player_count = 0;
				for (int i = 0; i < ClubTeahouseDeskMaxUsers; i++)
				{
					string Name = Record.szUserNames[i];
					if (Name.empty())
					{
						break;
					}
					player_count++;
					strcpy(record.szUserNames[i], Record.szUserNames[i]);
					int Score = 0;
					float redflower = 0.0;
					for (int j = 0; j < Record.roundCount; j++)
					{
						Score += Record.userScore[i][j];
						redflower += Record.redflowersList[i][j];
					}
					record.userScore[i] = Score;
					record.redflowersList[i] = redflower;
				}
				record.iPlayerCount = player_count;
			}
				break;
			case MYREC:
			{
				auto Record = Vec_Record_My[idx];
				record.iType = ALLREC;
				strcpy(record.szDeskPass, Record.szDeskPass);
				record.isPlayBack = false;
				record.date = Record.date;
				record.winScore = Record.winScore;
				record.roundCount = Record.roundCount;
				strcpy(record.szRoomName, Record.szRoomName);
				strcpy(record.szGameSN, Record.szGameSN[idx]);
				//strcpy(record.szPassWord, Now_Record.szDeskPass);
				int player_count = 0;
				for (int i = 0; i < ClubTeahouseDeskMaxUsers; i++)
				{
					string Name = Record.szUserNames[i];
					if (Name.empty())
					{
						break;
					}
					player_count++;
					strcpy(record.szUserNames[i], Record.szUserNames[i]);
					int Score = 0;
					float redflower = 0.0;
					for (int j = 0; j < Record.roundCount; j++)
					{
						Score += Record.userScore[i][j];
						redflower += Record.redflowersList[i][j];
					}
					record.userScore[i] = Score;
					record.redflowersList[i] = redflower;
				}
				record.iPlayerCount = player_count;
			}
				break;
			default:

				break;
			}
		}
		


		/*switch (_recType)
		{
		case HN::ALLREC:
			if (!_allVec.empty())
			{
				record.iPlayerCount = _allVec[idx].iPlayerCount;
				record.iPlayTime = _allVec[idx].iPlayTime;
				record.iRecordID = _allVec[idx].iRecordID;
				for (int i = 0; i < _allVec[idx].iPlayerCount; i++)
				{
					record.iScore[i] = _allVec[idx].iScore[i];
					strcpy(record.szNickName[i], _allVec[idx].szNickName[i]);
				}
				strcpy(record.szGameName, _allVec[idx].szGameName);
				strcpy(record.szPassWord, _allVec[idx].szPassWord);		
			}
			break;
		case HN::NORREC:
			if (!_normalVec.empty())
			{
				record.iPlayerCount = _normalVec[idx].iPlayerCount;
				record.iPlayTime = _normalVec[idx].iPlayTime;
				record.iRecordID = _normalVec[idx].iRecordID;
				for (int i = 0; i < _normalVec[idx].iPlayerCount; i++)
				{
					record.iScore[i] = _normalVec[idx].iScore[i];
					strcpy(record.szNickName[i], _normalVec[idx].szNickName[i]);
				}
				strcpy(record.szGameName, _normalVec[idx].szGameName);
				strcpy(record.szPassWord, _normalVec[idx].szPassWord);
			}
			break;
		case HN::ONEREC:
			if (!_singleVec.empty())
			{
				auto game = GamesInfoModule()->findGameName(_singleVec[idx].iGameID);
				if (game) strcpy(record.szGameName, game->szGameName);
				record.iPlayerCount = _singleVec[idx].iPlayerCount;
				record.iPlayTime = _singleVec[idx].iPlayTime;
				record.isPlayBack = _singleVec[idx].bPlayBack;
				INT userId = PlatformLogic()->loginResult.dwUserID;
				for (int i = 0; i < _singleVec[idx].iPlayerCount; i++)
				{
					record.iScore[i] = _singleVec[idx].iScore[i];
					strcpy(record.szNickName[i], _singleVec[idx].szNickName[i]);
					if (userId == _singleVec[idx].szUserID[i])
					{
						strcpy(record.szRecordCode, _singleVec[idx].szRecordCode[i]);
					}
				}			
				strcpy(record.szPassWord, _singleVec[idx].szPassWord);
			}
			break;
		default:
			break;
		}*/

		cell->setIdx(idx);
		cell->buildCell(&record);

		// 上拉刷新
		if (onRefreshCallBack)
		{
			/*switch (_recType)
			{
			case HN::ALLREC:
				if (!_allVec.empty() && (_allVec[idx].iTotalCount > _allVec.size()) && (_allVec.size()  == idx + 1))
				{
					_oldOffset = table->getContentOffset();
					_oldCount = _allVec.size() + 1;
					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在刷新"), 25);
					table->runAction(Sequence::create(DelayTime::create(0.6f), CallFunc::create([=](){
						onRefreshCallBack(_allVec.size() + 1);
					}), nullptr));
				}
				break;
			case HN::NORREC:
				if (!_normalVec.empty() && (_normalVec[idx].iTotalCount > _normalVec.size()) && (_normalVec.size() == idx + 1))
				{
					_oldOffset = table->getContentOffset();
					_oldCount = _normalVec.size() + 1;
					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在刷新"), 25);
					table->runAction(Sequence::create(DelayTime::create(0.6f), CallFunc::create([=](){
						onRefreshCallBack(_normalVec.size() + 1);
					}), nullptr));
				}
				break;
			case HN::ONEREC:
				if (!_singleVec.empty() && (_singleVec[idx].iTotalCount > _singleVec.size()) && (_singleVec.size() == idx + 1))
				{
					_oldOffset = table->getContentOffset();
					_oldCount = _singleVec.size() + 1;
					LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在刷新"), 25);
					table->runAction(Sequence::create(DelayTime::create(0.6f), CallFunc::create([=](){
						onRefreshCallBack(_singleVec.size() + 1);
					}), nullptr));
				}
				break;
			default:
				break;
			}*/
		}
		
		return cell;
	}

	ssize_t GameDataViewDelegateClub::numberOfCellsInTableView(TableView *table)
	{
		switch (Now_ShowType)
		{
		case CLUBREC:
		{
			if (m_RecID != -1)
			{
				return Now_Record.roundCount;
			}
			else
			{
				return Vec_Record_Group.size();
			}
		}
			break;
		case MYREC:
		{
			if (m_RecID != -1)
			{
				return Now_Record.roundCount;
			}
			else
			{
				return Vec_Record_My.size();
			}
		}
			break;
		default:

			break;
		}
		/*switch (_recType)
		{
		case HN::ALLREC:
			return _allVec.size();
			break;
		case HN::NORREC:
			return _normalVec.size();
			break;
		case HN::ONEREC:
			return _singleVec.size();
			break;
		default:
			break;
		}*/
		return 0;
	}

	Size GameDataViewDelegateClub::tableCellSizeForIndex(TableView *table, ssize_t idx)
	{
		if (idx > _isOpenVec.size() - 1)
		{
			_isOpenVec.push_back(false);
		}
		if (_isOpenVec[idx])
		{
			return Size(1220, 188);
		}
		
		return Size(1220, 75);
	}

	// 设置所有大结算数据
	void GameDataViewDelegateClub::setAllVipRecordData(MSG_GP_O_GetTotalRecord* record, bool reset/* = false*/)
	{
		if (reset)
		{
			_isOpenVec.clear();
			_allVec.clear();
		}
		else
		{
			if (record)
			{
				_allVec.push_back(*record);
				_isOpenVec.push_back(false);
			}
		}
	}

	// 设置大结算数据
	void GameDataViewDelegateClub::setAllRecordData(vector<SClubQunRoomLog> Vec)
	{
		_isOpenVec.clear();
		Vec_Record_Group.clear();
		Vec_Record_My.clear();
		//b
		for (int i = 0; i < Vec.size(); i++)
		{
			Vec_Record_Group.push_back(Vec[i]);
			_isOpenVec.push_back(false);
			for (int j = 0; j < ClubTeahouseDeskMaxUsers; j++)
			{
				string Name = Vec[i].szUserNames[j];
				if (Name.empty())
				{
					break;
				}
				else
				{
					if (!strcmp(PlatformLogic()->loginResult.nickName, Vec[i].szUserNames[j]))
					{
						Vec_Record_My.push_back(Vec[i]);
						break;
					}
				}
			}
		}

		//w
	}

	void GameDataViewDelegateClub::setRecID(int recid)
	{
		m_RecID = recid;
		if (_isOpenVec.size() > 0)
		{
			_isOpenVec.clear();
			_isOpenVec.push_back(false);
		}
		if (recid != -1)
		{
			switch (Now_ShowType)
			{
			case CLUBREC:
			{
				Now_Record = Vec_Record_Group[recid];
			}break;
			case MYREC:
			{
				Now_Record = Vec_Record_Group[recid];
			}
			break;
				/*for (auto item : Vec_Record_Group)
				{
				if (item.gameSN == recid)
				{
				Now_Record = item;
				break;
				}
				}*/
			}
		}
	}

	// 设置普通房间数据
	void GameDataViewDelegateClub::setNormalRecordData(MSG_GP_O_GetTotalRecord* record, bool reset/* = false*/)
	{
		if (reset)
		{
			_normalVec.clear();
			_allVec.clear();
		}
		else
		{
			if (record)
			{
				_normalVec.push_back(*record);
				_isOpenVec.push_back(false);
			}
		}
	}

	// 设置单个房间小结算数据
	void GameDataViewDelegateClub::setSingleRecordData(MSG_GP_O_GetSingleRecord* record, bool reset/* = false*/)
	{
		if (reset)
		{
			_singleVec.clear();
		}
		else
		{
			if (record)
			{
				_singleVec.push_back(*record);
				_isOpenVec.push_back(false);
			}
		}
	}

	bool GameDataViewDelegateClub::isEmpty()
	{
		if (m_RecID != 0)
		{
			return true;
		}
		else
		{
			switch (Now_ShowType)
			{
			case CLUBREC:
				return Vec_Record_Group.empty();
				break;
			case MYREC:
				return Vec_Record_Group.empty();
				break;
			default:
				break;
				/*case HN::ALLREC:
				return _allVec.empty();
				break;
				case HN::NORREC:
				return _normalVec.empty();
				break;
				case HN::ONEREC:
				return _singleVec.empty();
				break;
				default:
				break;*/
			}
		}
		

		return true;
	}

	Vec2 GameDataViewDelegateClub::getRefreshOffset()
	{	
		return Vec2::ZERO;
		/*switch (_recType)
		{
		case HN::ALLREC:
			if (_allVec.size() == 1) return _oldOffset;
			return Vec2(_oldOffset.x, _oldOffset.y - (_allVec.size() - _oldCount) * 70);
			break;
		case HN::NORREC:
			if (_normalVec.size() == 1) return _oldOffset;
			return Vec2(_oldOffset.x, _oldOffset.y - (_normalVec.size() - _oldCount) * 70);
			break;
		case HN::ONEREC:
			if (_singleVec.size() == 1) return _oldOffset;
			return Vec2(_oldOffset.x, _oldOffset.y - (_singleVec.size() - _oldCount) * 70);
			break;
		default:
			return Vec2::ZERO;
			break;
		}*/
	}
}
