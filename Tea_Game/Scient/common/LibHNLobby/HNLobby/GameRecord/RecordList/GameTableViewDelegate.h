/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__GameTableViewDelegate__
#define __HN__GameTableViewDelegate__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include <vector>
#include "GameTableViewCell.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

namespace HN
{
	class GameTableViewDelegate : public TableViewDelegate
	{
	public:
		GameTableViewDelegate();

		~GameTableViewDelegate();

		// cell被点击
		virtual void tableCellTouched(TableView* table, TableViewCell* cell) override;

		// cell高亮
		virtual void tableCellHighlight(TableView* table, TableViewCell* cell) override;

		// cell被回收
		virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell) override;
	};

	class GameDataViewDelegate : public TableViewDataSource
	{
	public:
		typedef std::function<void(int startcount)> refreshCallBack;
		// 上拉刷新回调
		refreshCallBack onRefreshCallBack = nullptr;

		typedef std::function<void(int recID)> getSingleListCallBack;
		// 获取单局列表回调
		getSingleListCallBack onGetSingleListCallBack = nullptr;

		typedef std::function<void(std::string recCode)> playRecCallBack;
		// 查看回放回调
		playRecCallBack onPlayCallBack = nullptr;

	public:
		GameDataViewDelegate();

		~GameDataViewDelegate();

		//创建cell
		virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

		//Cell个数
		virtual ssize_t numberOfCellsInTableView(TableView *table);

		//Cell大小
		virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);

	public:
		// 设置所有大结算数据
		void setAllVipRecordData(MSG_GP_O_GetTotalRecord* record, bool reset = false);

		// 设置普通房间数据
		void setNormalRecordData(MSG_GP_O_GetTotalRecord* record, bool reset = false);

		// 设置单个房间小结算数据
		void setSingleRecordData(MSG_GP_O_GetSingleRecord* record, bool reset = false);

		// 是否有数据
		bool isEmpty();

		// 获取刷新后列表滑动偏移量
		Vec2 getRefreshOffset();
		

	private:
		CC_SYNTHESIZE_PASS_BY_REF(RecordType, _recType, RecordType);//显示列表类型
		CC_SYNTHESIZE_PASS_BY_REF(Vec2, _allListOffset, AllListOffset);//大结算列表当前滑动位置
		std::vector<MSG_GP_O_GetTotalRecord>	_allVec;	//大结算战绩列表
		std::vector<MSG_GP_O_GetTotalRecord>	_normalVec;	//普通场战绩列表
		std::vector<MSG_GP_O_GetSingleRecord>	_singleVec;	//小结算战绩列表
		std::vector<bool> _isOpenVec;	//列表是否展开
		Vec2 _oldOffset = Vec2::ZERO;	//刷新列表前滑动偏移量
		int _oldCount = 0;				//刷新前列表个数
		
	};

}

#endif //__HN__GameTableViewDelegate__