/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_GAME_ROOMRECORD_CLUB_H__
#define __HN_GAME_ROOMRECORD_CLUB_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "GameTableViewDelegateClub.h"

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include <vector>

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

class GameRoomRecordClub : public HNLayer, public ui::EditBoxDelegate, public HNHttpDelegate
{
public:
	~GameRoomRecordClub();

	bool init(int clubID, bool isManage);

	static GameRoomRecordClub* create(bool isManage, int clubID = 0);

private:
	void closeFunc() override;

	bool loadUI(const char *sFileName);

	void otherRecord();

private:
	// 获取战绩（0，：VIP战绩，1：普通战绩, 2:单局战绩）
	void getRecord(RecordType type, int count, int recID = 0);

	// 按钮切换监听
	void btnClickCallback(Ref* pSender);

	// 获取回放文件地址
	void getRecordUrl(std::string recCode);

	// 获取大结算战绩信息
	bool getAllRecordCallback(HNSocketMessage* socketMessage);

	// 获取单局结算战绩信息
	bool getSingleRecordCallback(HNSocketMessage* socketMessage);

	// 获取回放文件地址
	bool getRecordUrlCallback(HNSocketMessage* socketMessage);

	// 获取所有战绩列表
	bool getAllRecordListResult(HNSocketMessage* socketMessage);

	// 获取战绩记录
	void SendAllRecordList();

	// 切换时间点
	void SetQueryDay(int Index);
protected:
	virtual void editBoxReturn(ui::EditBox* editBox) {};

	void editBoxTextChanged(ui::EditBox* editBox, const std::string& text) {};

	void editBoxEditingDidBegin(ui::EditBox* editBox) {};

	void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)override;

	// 播放回放文件
	void playRcordVideo(const std::string& recordInfo);

protected:
	TableView*				_tableView	= nullptr;
	GameTableViewDelegateClub*	_delegate	= nullptr;
	GameDataViewDelegateClub*	_dataSource = nullptr;
	ImageView*				_img_tip	= nullptr;
	ImageView*				_img_top  = nullptr;
	ImageView*				_img_code	= nullptr;
	ImageView*				_img_roomID = nullptr;
	//按钮是否显示（管理员拥有我的战绩群战绩）
	Button*					_btn_GroupRecord = nullptr;
	Button*					_btn_PersonalRecord = nullptr;
	//战绩统计数据
	Layout*					_Panel_GroupRecord = nullptr;
	Layout*					_Panel_PersonalRecord = nullptr;

	//更多日期选择
	Button*					_Button_more = nullptr;
	Text*						_Text_Time = nullptr;
	//更多关闭
	Layout*					_Panel_close = nullptr;
	//更多选择节点
	ImageView*				_Image_more = nullptr;

	RecordType				_recType	= ALLREC;	//当前显示战绩类型
	RecordListType						_Record_Type = CLUBREC;	//当前战绩类目（0==本群战绩，1==自己战绩）
	INT						_iRecordID	= 0;		//当前战绩ID
	INT						_iGameID = 0;			//当前回放的游戏ID
	bool					_haveNormal = true;		//是否需要普通场战绩
	bool					_isAddRec	= false;	//是否是下拉刷新
	CC_SYNTHESIZE_PASS_BY_REF(Vec2, _norListOffset, NorListOffset);//普通场结算列表当前滑动位置

	//战绩记录
	vector<SClubQunRoomLog> Vec_SClubQunRoomLog;
	vector<SClubQunRoomLog> Vec_SClubMeRoomLog;

	INT						_clubID = 0;	// 当前进入俱乐部ID
	INT						_Query_Day = 0;
	bool						_IsManage = false;	//是否是管理员（管理员有群战绩和个人战绩）
	char					_szRecordCode[10];	// 当前回放码				

protected:
	GameRoomRecordClub();
	DISALLOW_COPY_AND_ASSIGN(GameRoomRecordClub);

private:

};

#endif