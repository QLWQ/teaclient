/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "BackPlayMaskLayer.h"
#include "HNLogicExport.h"
#include "../../../GameBaseLayer/GamePlatform.h"
#include "../RecordParse/RecordVideoController.h"

// 对话框布局文件
static const char* BACKPLAYMASK_CSB = "platform/BackPlayUi/BackPlayMaskNode.csb";

BackPlayMaskLayer::BackPlayMaskLayer()
{
	_listener = EventListenerCustom::create("gameRecordReady", [=](EventCustom* event){
		gameRecordReady();
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);
}


BackPlayMaskLayer::~BackPlayMaskLayer()
{
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
}

BackPlayMaskLayer *BackPlayMaskLayer::create(char* RecordCode)
{
	BackPlayMaskLayer *p = new BackPlayMaskLayer();
	if (p && p->init())
	{
		p->_RecordCode = RecordCode;
		p->autorelease();
		return p;
	}
	else
	{
		CC_SAFE_DELETE(p);
		return NULL;
	}
}

bool BackPlayMaskLayer::init()
{
	if (!HNLayer::init()) return false;

	do {
		Size winSize = Director::getInstance()->getWinSize();

		// 布局
		auto csLoader = CSLoader::createNode(BACKPLAYMASK_CSB);
		addChild(csLoader);

		// 标题
		Layout* layout = dynamic_cast<ui::Layout*>(csLoader->getChildByName("Panel"));
		layout->setPosition(winSize / 2);
		layout->setScale(winSize.width / 1280, winSize.height / 720);

		auto img_BG = dynamic_cast<ImageView*>(layout->getChildByName("Image_BG"));
		img_BG->setScale(1280 / winSize.width, 720 / winSize.height);
		
		// 关闭按钮
		auto Button_close = dynamic_cast<ui::Button*>(layout->getChildByName("Button_Close"));
		Button_close->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));
		Button_close->setScale(1280 / winSize.width, 720 / winSize.height);

		_stBtn.Button_Back = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_Back"));
		_stBtn.Button_Back->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));

		_stBtn.Button_Fast = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_Fast"));
		_stBtn.Button_Fast->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));

		_stBtn.Button_Restart = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_Restart"));
		_stBtn.Button_Restart->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));

		_stBtn.Button_Start = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_Start"));
		_stBtn.Button_Start->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));

		_stBtn.Button_Stop = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_Stop"));
		_stBtn.Button_Stop->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));

		_stBtn.Button_One = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_One"));
		_stBtn.Button_One->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));
		_stBtn.Button_Two = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_Two"));
		_stBtn.Button_Two->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));
		_stBtn.Button_Three = dynamic_cast<ui::Button*>(img_BG->getChildByName("Button_Three"));
		_stBtn.Button_Three->addTouchEventListener(CC_CALLBACK_2(BackPlayMaskLayer::onMenuCallBack, this));

		_Text_speed = dynamic_cast<Text*>(img_BG->getChildByName("Text_speed"));
		_Text_speed->setString(GBKToUtf8("倍数X1"));
		
		auto img_loadBG = dynamic_cast<ImageView*>(img_BG->getChildByName("Image_LoadBg"));
		_pLoadingBar = dynamic_cast<ui::LoadingBar*>(img_loadBG->getChildByName("LoadingBar"));
		_pText_Progress = dynamic_cast<ui::Text*>(img_loadBG->getChildByName("Text_Progress"));

		RecordVideoControllerIns()->_startCallBack = [this](const RecordVideoController::RecordVideoData &d)
		{
			_stBtn.Button_Back->setEnabled(false);
			_stBtn.Button_Fast->setEnabled(false);
			updateProcess(d.fPercent);
		};
		RecordVideoControllerIns()->_playCallBack = [this](const RecordVideoController::RecordVideoData &d)
		{
			//游戏里部分信息载入需要时间,此时如果快进,游戏会崩溃
			//m_stBtn.Button_Back->setEnabled(true);
			//m_stBtn.Button_Fast->setEnabled(true);
			
			updateProcess(d.fPercent);
		};
		RecordVideoControllerIns()->_endCallBack = [this](const RecordVideoController::RecordVideoData &d)
		{
			_stBtn.Button_Restart->setVisible(true);
			_stBtn.Button_Start->setVisible(false);
			_stBtn.Button_Stop->setVisible(false);
			
			_stBtn.Button_Back->setEnabled(false);
			_stBtn.Button_Fast->setEnabled(false);

			_stBtn.Button_One->setVisible(true);
			_stBtn.Button_Two->setVisible(false);
			_stBtn.Button_Three->setVisible(false);
		};
		RecordVideoControllerIns()->_stepCallBack = [this](const RecordVideoController::RecordVideoData &d)
		{
			updateProcess(d.fPercent);
		};
	} while (0);


	return true;
}
void BackPlayMaskLayer::onMenuCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (Widget::TouchEventType::ENDED != type) return;
	Button* btn = (Button*)pSender;
	std::string name(btn->getName());
	if (name.compare("Button_Close") == 0)
	{
		RecordVideoControllerIns()->endVideo();
	}
	else if (name.compare("Button_Back") == 0)
	{
		RecordVideoControllerIns()->stepBack();
	}
	else if (name.compare("Button_Fast") == 0)
	{
		RecordVideoControllerIns()->stepFront();
	}
	else if (name.compare("Button_Restart") == 0)
	{
		_stBtn.Button_Restart->setVisible(false);
		_stBtn.Button_Start->setVisible(false);
		_stBtn.Button_Stop->setVisible(true);
		__NotificationCenter::getInstance()->postNotification("removeCalculateBoard");
		RecordVideoControllerIns()->reStart();
	}
	else if (name.compare("Button_Start") == 0)
	{
		_stBtn.Button_Start->setVisible(false);
		_stBtn.Button_Stop->setVisible(true);
		RecordVideoControllerIns()->playOrResume();
	}
	else if (name.compare("Button_Stop") == 0)
	{
		_stBtn.Button_Start->setVisible(true);
		_stBtn.Button_Stop->setVisible(false);
		RecordVideoControllerIns()->pause();
	}
	else if (name.compare("Button_One") == 0)
	{
		_stBtn.Button_One->setVisible(false);
		_stBtn.Button_Two->setVisible(true);
		_stBtn.Button_Three->setVisible(false);
		RecordVideoControllerIns()->changePlayRate(1.0 / 1);
		_Text_speed->setString(GBKToUtf8("倍数X1"));
	}
	else if (name.compare("Button_Two") == 0)
	{
		_stBtn.Button_One->setVisible(false);
		_stBtn.Button_Two->setVisible(false);
		_stBtn.Button_Three->setVisible(true);
		RecordVideoControllerIns()->changePlayRate(1.0 / 2);
		_Text_speed->setString(GBKToUtf8("倍数X2"));
	}
	else if (name.compare("Button_Three") == 0)
	{
		_stBtn.Button_One->setVisible(true);
		_stBtn.Button_Two->setVisible(false);
		_stBtn.Button_Three->setVisible(false);
		RecordVideoControllerIns()->changePlayRate(1.0 / 3);
		_Text_speed->setString(GBKToUtf8("倍数X3"));
	}
}

void BackPlayMaskLayer::gameRecordReady()
{
	_stBtn.Button_Back->setEnabled(true);
	_stBtn.Button_Fast->setEnabled(true);
}


void BackPlayMaskLayer::updateProcess(float percent)
{	
	int percentage = std::floor(percent*100);
	if (percentage > 100) percentage = 100;
	if (percentage < 0) percentage = 0;
	_pLoadingBar->setPercent(percentage);
	_pText_Progress->setString(StringUtils::format("%d%%", percentage));
}
void VedioMask::addToViewport(cocos2d::Node *parent, const int nZOrder, char* RecordCode)
{
	parent->addChild(BackPlayMaskLayer::create(RecordCode), nZOrder);
}
void VedioMask::removeFromViewport()
{
	VideoViewDelegate::removeFromViewport();
	RoomLogic()->close();
	GamePlatform::createPlatform();
}

