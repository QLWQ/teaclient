/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_BACKPLAY_LAYER_H__
#define __HN_BACKPLAY_LAYER_H__

#include "HNUIExport.h"
#include "../RecordParse/RecordVideoController.h"


class BackPlayMaskLayer : public HNLayer
{
	struct stButton{
		Button *Button_Back;
		Button *Button_Start;
		Button *Button_Restart;
		Button *Button_Stop;
		Button *Button_Fast;
		Button *Button_One;
		Button *Button_Two;
		Button *Button_Three;
	};
	//继承

public:
	BackPlayMaskLayer();
	~BackPlayMaskLayer();
	static BackPlayMaskLayer* create(char* RecordCode);
	virtual bool init() override;

	void updateProcess(float percent);

	void gameRecordReady(); //游戏回放载入完毕
private:
	void onMenuCallBack(Ref* pSender, Widget::TouchEventType type);
private:
	LoadingBar*		_pLoadingBar = nullptr;
	Text*			_pText_Progress = nullptr;
	Text*			_Text_speed = nullptr;
	stButton		_stBtn;
	char*			_RecordCode;

	EventListenerCustom*	_listener = nullptr;
};
class VedioMask : public VideoViewDelegate
{
public:
	virtual void addToViewport(cocos2d::Node *parent, const int nZOrder, char* RecordCode);
	virtual void removeFromViewport();
};

#endif //__HN_BACKPLAY_LAYER_H__