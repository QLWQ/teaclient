/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_RECORDCOMMANDQUEUE_H__
#define __HN_RECORDCOMMANDQUEUE_H__

#include <deque>
#include <map>
#include "MessageCommand.h"
#include "HNLogicExport.h"

class RecordCommandQueue
{
public:
	typedef std::deque<RecordCommand*> CommandQueue;
public:
	~RecordCommandQueue();

	void pushCommand(eCommandType type, RecordCommand *command);
	RecordCommand* frontCommand(eCommandType type);
	void popFront(eCommandType type);


	CommandQueue* getCommandQueue(eCommandType type);
	RecordCommand* getCommand(eCommandType type, const int nIndex);

	void setCurrentCommandQueueIndex(eCommandType type, const int index);
	int getCurrentCommandQueueIndex(eCommandType type)const;
	int getCommandQueueCount(eCommandType type);

	void clearCommandByType(eCommandType type);
	void clearAllCommand();

	//派发首个命令，会删除该命令
	void dispatchCommand(eCommandType type);
	//派发指定命令，不会删除该命令
	void dispatchCurrentCommand(eCommandType type,const int nIndex);
protected:
	RecordCommandQueue();
	RecordCommandQueue(const RecordCommandQueue &r) = delete;
	RecordCommandQueue& operator=(const RecordCommandQueue &r) = delete;

private:
	void releaseFirstCommand();
private:
	std::map<eCommandType, std::deque<RecordCommand*> > _mapRecordCommand;
	std::map<eCommandType, int> _mapCurrentIndexs;
	RecordCommand *_firstCommand;		//第一个命令，处理用户坐下和添加用户信息
private:
	friend class RecordFacade;
};

#endif