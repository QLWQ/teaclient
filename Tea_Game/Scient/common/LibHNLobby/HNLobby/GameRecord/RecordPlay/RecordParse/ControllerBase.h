/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_CONTROLLERBASE_H__
#define __HN_CONTROLLERBASE_H__

#include <unordered_map>
#include "MessageCommand.h"

template < class _Kty,
	class _Ty,
	bool bTyIsDynamicAlloc = false,
	class _Hasher = std::hash<_Kty>,
	class _Keyeq = std::equal_to<_Kty>,
	class _Alloc = std::allocator < std::pair<const _Kty, _Ty> >> 
class ControllerBase
{
public:
	typedef std::unordered_map<_Kty, _Ty,_Hasher, _Keyeq, _Alloc> ControllerContain;
	typedef ControllerBase<_Kty, _Ty, bTyIsDynamicAlloc,_Hasher, _Keyeq, _Alloc> MyType;
	typedef typename ControllerContain::iterator iterator;
	typedef typename ControllerContain::const_iterator const_iterator;
	typedef _Kty key_type;
	typedef _Ty value_type;

	virtual ~ControllerBase()
	{
		Clear();
	}

	void Add(const typename remove_const_refrence<_Kty>::type &k, typename std::remove_reference<_Ty>::type &v)
	{
		iterator it = _contain.find(k);
		if (it != _contain.end())
		{
			if (_bValueIsDynamicAlloc)
				delete it->second;
			it->second = v;
			return;
		}

		_contain.insert(std::make_pair(k, v));
	}

	bool Find(const typename remove_const_refrence<_Kty>::type &k, typename remove_const_refrence<_Ty>::type &vOut)
	{
		iterator it=_contain.find(k);
		if (it != _contain.end())
		{
			vOut = it->second;
			return true;
		}

		return false;
	}

	bool Find(const typename remove_const_refrence<_Kty>::type &k)
	{
		iterator it = _contain.find(k);
		if (it != _contain.end())
			return true;

		return false;
	}

	void Remove(const typename remove_const_refrence<_Kty>::type &k)
	{
		iterator it = _contain.find(k);
		if (it != _contain.end())
		{
			if (_bValueIsDynamicAlloc)
				delete it->second;

			_contain.erase(it);
		}
	}

	void Clear()
	{
		if (_bValueIsDynamicAlloc)
		{
			for (iterator it = _contain.begin(); it != _contain.end(); ++it)
				delete it->second;
		}

		_contain.clear();
	}

protected:
	ControllerBase()
	{
	}

private:
	ControllerContain _contain;
	static const bool _bValueIsDynamicAlloc = bTyIsDynamicAlloc;
};


struct RegistTool
{
	template<typename Node,typename NodeContain>
	static bool RegistNode(typename remove_const_refrence_pointer<Node>::type *node
		, typename remove_const_refrence_pointer<NodeContain>::type *contain)
	{
		static_assert(std::is_same<typename std::add_pointer<Node>::type, typename NodeContain::value_type>::value, "RegistTool Node is error_type");
		assert(node && contain);

		if (!node || !contain)
			return false;

		if (contain->Find(node->getName()))
			return false;
		else
			contain->Add(node->getName(), node);

		return true;
	}
};


#endif