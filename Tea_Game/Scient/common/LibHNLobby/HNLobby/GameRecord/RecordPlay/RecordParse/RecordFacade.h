/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_RECORDMEDIATOR_H__
#define __HN_RECORDMEDIATOR_H__

#include "FileParser.h"
#include "RecordCommandQueue.h"
/*
author syf
RecordFacade is a facade to use FileParser and RecordCommandQueue
*/
class RecordFacade:public FileParserDelegate
{
public:
	enum class eVideoState
	{
		None,
		Start,
		Processing,
		Ended,
	};

	typedef std::function<void(bool, const std::string&)> ParseResultFunc;

public:
	//文件解析错误处理
	ParseResultFunc _resultCallBack;
public:
	~RecordFacade();

	static RecordFacade* create(const std::string &sName="");
	//FileParseDelegate 接口
	virtual void pushCommand(eCommandType type, const MsgHead &head, CHAR *obj)override;
	void handleParseResult(FileParser::eErrorType type);

	//解析回放文件
	bool parseRecordFile(const char *sFilePath);
	//派发命令,根据当前命令索引派发
	void dispatchCommand(eCommandType type);
	//设置命令索引
	void setCommandIndex(eCommandType type, const int nIndex);
	//获得当前命令索引
	int getCurrentCommandIndex(eCommandType type);

	//清空所有命令
	void clearAllCommands();
	//重置所有命令索引
	void resetAllCommandIndex();

	int getCommandCount(eCommandType type);

	inline bool isVideoEnded()const{ return _eState == eVideoState::Ended; }
	inline bool isParsed()const{ return _bAreadyParse; }
	inline eVideoState getState()const{ return _eState; }

	void setName(const std::string &name);
	std::string getName()const;

	void setDelegate(HN::IGameMessageDelegate *d);

	//处理命令队列中的首个命令
	bool handlerFirstCommand();

	bool isValidCommand(eCommandType type,const int nIndex)const;
	/*
		获取有效的Command
		param1 Command类型
		param2 当前的阈值
		param3 要获取的索引
	*/
	int validCommandForIndex(eCommandType type, const int nCurrent,const int nIndex);
protected:
	RecordFacade();
	RecordFacade(HN::IGameMessageDelegate *d);	//指定某个游戏逻辑

	RecordCommand* makeCommand(const MsgHead &head, CHAR *obj, eCommandType type);
private:
	//regist to RecordVideoController
	RecordFacade* registSelf();

	int adjustLogicSeatNo(int bSrc, int bBasic,int nTotal);
private:
	FileParser *_parser;
	RecordCommandQueue *_commandQueue;
	HN::IGameMessageDelegate *_delegate;
	eVideoState _eState;
	std::string _sName;
	bool _bAreadyParse;
};

#endif