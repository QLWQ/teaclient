/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_RECORDVIDEOCONTROLLER_H__
#define __HN_RECORDVIDEOCONTROLLER_H__

#include "ControllerBase.h"
#include "RecordFacade.h"
#include "cocos2d.h"

namespace HN
{
	class IGameMessageDelegate;
}

typedef ControllerBase<std::string, RecordFacade*, true> RecordVideoControllerBase; 

class VideoViewDelegate;
/*
author syf
instance class
use InstanceCreator to create it
*/
class RecordVideoController :public RecordVideoControllerBase,public cocos2d::Ref
{
public:
	struct RecordVideoData
	{
		float fPercent;
		float fRate;
		float fTotalTime;

		RecordVideoData()
		{
			memset(this, 0, sizeof(RecordVideoData));
		}

		RecordVideoData(const float percent, const float rate, const float totalTime) :fPercent(percent), fRate(rate), fTotalTime(totalTime)
		{
		}
	};
	/*
		param1: current percent the video play
		param2: total real time is the video consume
	*/
	typedef std::function<void(const RecordVideoData&)> VideoPlayCallBack;

	VideoPlayCallBack _startCallBack;	//每一局回放开始时候(包括重播)
	VideoPlayCallBack _playCallBack;	//每一局回放派发一个命令的时候
	VideoPlayCallBack _pauseCallBack;	//每一局回放暂停的时候
	VideoPlayCallBack _resumeCallBack;	//每一局回放唤醒的时候
	VideoPlayCallBack _endCallBack;		//每一局回放结束的时候(不是关掉回放界面的时候，而是播完一局)
	VideoPlayCallBack _stepCallBack;	//快进快退回调
public:
	~RecordVideoController();

	inline void setCurrentFacade(RecordFacade *facade){ _currentFacade = facade; }
	inline void setCurrentMessageDelegate(HN::IGameMessageDelegate *d){ _messageDelegate = d; }
	inline HN::IGameMessageDelegate* getCurrentMessageDelegate(){ return _messageDelegate; }

	/*
	start video and view
	param1 the parse file's path
	param2 the game id
	param3 the view delegate(dynamic new memory)
	*/
	void startVideo(const std::string &sFilePath, const unsigned int uGameID, VideoViewDelegate *d, char* RecordCode);
	/*
	close video and view
	*/
	void endVideo();

	void reStart();
	void pause();
	void playOrResume();

	void stepFront();
	void stepBack();
	/*
	change video play speed
	param1 the rate for basicInterval(destInterval=srcInterval * fRate)
	*/
	void changePlayRate(const float fRate);
	//release memory
	void removeCurrentFacade();
protected:
	void playWithInterval(const float interval,bool bPause);
	bool createVideo(const std::string &sFilePath, const unsigned int uGameID);
	void start(HN::IGameMessageDelegate *d);
	void end();
	void step(const int nStep);

	RecordVideoController();

	//注册消息事件
	void registNotification();
	//反注册消息事件
	void unRegistNotification();
	//消息事件
	void startNotification(EventCustom* event);

	float getPercent();

	void setViewDelegate(VideoViewDelegate *d);

	void resetCallBack();
	void resetData();

	void readConfig(const char *sFilePath);
private:
	//初始化资源
	void initResource();
	//资源清理
	void clearResource();
private:
	RecordFacade *_currentFacade;
	float _fBasicInterval; //基础间隔时间(config file)
	float _fCurrentInterval; //当前间隔时间
	float _fTotalTime;	//总共消耗的时间(真实时间)
	int _nStepSpace;	//快进和后退的步数
	bool _bPause;
	bool _bSaveParser; //是否保存解析的内容(config file)
	char* _RecordCode;
	HN::IGameMessageDelegate *_messageDelegate;
	VideoViewDelegate *_viewDelegate;

	template<typename T,bool>
	friend class InstanceCreator;

	static char *s_schduleName;

	EventListenerCustom*	_listener = nullptr;
};

#define RecordVideoControllerIns() InstanceCreator<RecordVideoController>::getInstance()
#define RecordVideoControllerDestroy() InstanceCreator<RecordVideoController>::deleteInstance()


class VideoViewDelegate
{
public:
	~VideoViewDelegate(){}

	virtual void addToViewport(cocos2d::Node *parent, const int nZOrder, char* RecordCode) = 0;
	virtual void removeFromViewport()
	{
		RecordVideoControllerIns()->removeCurrentFacade();
	}
protected:
	VideoViewDelegate(){}
	CC_DISALLOW_COPY_AND_ASSIGN(VideoViewDelegate);
};

#endif