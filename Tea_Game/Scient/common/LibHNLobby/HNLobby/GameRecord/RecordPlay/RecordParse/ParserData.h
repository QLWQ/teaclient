/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_PARSERDATA_H__
#define __HN_PARSERDATA_H__

#include <string>
#include <assert.h>
/*
author syf
ParserData use to parse record file 
*/
template<typename T,int SIZE>
class ParserData
{
public:
	typedef ParserData<T, SIZE> MyType;
	typedef T value_type;
	typedef int diff_type;

	enum
	{
		T_SIZE = sizeof(T),
	};
public:
	struct ScalarData
	{};

	struct NoScalarData
	{};
public:
	ParserData()
	{
		_data = new T[SIZE];
		memset(_data, 0, sizeof(T)*SIZE);
		_currentOffset = 0;
		_size = 0;
	}

	~ParserData()
	{
		if (_data)
		{
			delete _data;
			_data = nullptr;
		}
	}

	ParserData(const ParserData<T,SIZE> &other)
	{
		copyFrom(other);
	}

	ParserData& operator=(const ParserData<T, SIZE> &other)
	{
		if (&other == this)
			return *this;

		copyFrom(other);

		return *this;
	}

	value_type* getData(){ return _data+_currentOffset; }
	void setData(value_type *src, const diff_type count)
	{
		copyFrom(src, count, typename std::conditional<std::is_scalar<value_type>::value,
			ScalarData, NoScalarData>::type());

		_size = count;
		_currentOffset = 0;
	}

	void resetData()
	{
		_size = _currentOffset = 0;
	}

	inline bool isEmpty()const{ return (_size == 0); }

	diff_type getMaxSize(){ return SIZE; }
	diff_type getSize(){ return _size; }
	diff_type getOffset(){ return _currentOffset; }
	diff_type getReverseOffset(){ return _size - _currentOffset; }
	void setOffset(const diff_type nPos)
	{
		assert(_currentOffset + nPos <= _size);
		if (_currentOffset + nPos > _size)
			return;

		_currentOffset += nPos; 
	}
	bool isOverBoundary(const diff_type nIndex)
	{
		return nIndex > _size;
	}
protected:
	value_type* getAllData(){ return _data; }

	void copyFrom(const ParserData<T, SIZE> &other)
	{
		if (!_data)
			_data = new value_type[other.getMaxSize()];

		copyFrom(other.getAllData(), other.getSize(), std::conditional<std::is_scalar<value_type>::value,
			ScalarData, NoScalarData>::type());

		_currentOffset = other.getOffset();
		_size = other.getSize();
	}

	void copyFrom(value_type *src,diff_type count, ScalarData)
	{
		memcpy(_data, src, count * T_SIZE);
	}

	void copyFrom(value_type *src, diff_type count, NoScalarData)
	{
		for (diff_type i = 0; i < count; ++i)
			_data[i] = src[i];
	}
private:
	value_type *_data;
	diff_type _currentOffset;
	diff_type _size;
};

//10KB for parse record data
typedef ParserData<char, RECORD_PARSEDATA_NUM> RecordParserData;

#endif