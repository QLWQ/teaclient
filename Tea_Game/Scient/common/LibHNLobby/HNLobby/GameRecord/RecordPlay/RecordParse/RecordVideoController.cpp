/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RecordVideoController.h"
#include "../../../GameReconnection/Reconnection.h"

USING_NS_CC;

static const char *s_sConfigFilePath = "platform/BackPlayUi/RecordControllConfig.json";

RecordVideoController::RecordVideoController() :_currentFacade(nullptr)
	, _fBasicInterval(1.0f)
	, _fCurrentInterval(_fBasicInterval)
	, _fTotalTime(0.0f)
	, _nStepSpace(0)
	, _startCallBack(nullptr)
	, _playCallBack(nullptr)
	, _endCallBack(nullptr)
	, _pauseCallBack(nullptr)
	, _resumeCallBack(nullptr)
	, _stepCallBack(nullptr)
	, _bPause(false)
	, _messageDelegate(nullptr)
	, _viewDelegate(nullptr)
	, _bSaveParser(false)
{
	_RecordCode = NULL;
	registNotification();
	readConfig(s_sConfigFilePath);			//获取默认配置数据
}

RecordVideoController::~RecordVideoController()
{
	unRegistNotification();
}

void RecordVideoController::playWithInterval(const float interval, bool bPause)
{
	Director::getInstance()->getScheduler()->schedule([this](float fDelta)
	{
		float fPercent = getPercent();
		_currentFacade->dispatchCommand(eCommandType::GameMessage);

		_fTotalTime += fDelta;
		if (_playCallBack)
		{
			RecordVideoData d(fPercent, _fCurrentInterval / _fBasicInterval, _fTotalTime);
			_playCallBack(d);
		}

		if (_currentFacade->isVideoEnded())
			end();

	}, this, interval, bPause, s_schduleName);
}

void RecordVideoController::start(HN::IGameMessageDelegate *d)
{
	if (!d)
		return;

	if (!_currentFacade)
		return;

	resetData();

	//设置逻辑代理
	_currentFacade->setDelegate(d);
	//重置命令索引
	_currentFacade->resetAllCommandIndex();
	//派发命令头
	_currentFacade->dispatchCommand(eCommandType::GameHead);

	//返回第一个消息
	step(-_currentFacade->getCommandCount(eCommandType::GameMessage));
	//playWithInterval(_fCurrentInterval, false);

	/*if (_startCallBack)
	{
		RecordVideoData d;
		d.fPercent = 0.0f;
		d.fRate = _fCurrentInterval / _fBasicInterval;
		d.fTotalTime = _fTotalTime;

		_startCallBack(d);
	}*/
}

void RecordVideoController::startNotification(EventCustom* event)
{
	RefValue<IGameMessageDelegate*> *rValue = CAST_REFVALUE(IGameMessageDelegate*, (Ref*)event->getUserData());
	if (!rValue)
		return;

	RecordVideoControllerIns()->setCurrentMessageDelegate(rValue->getValue());

	//下一帧再启动，等待logic初始化完成
	Director::getInstance()->getScheduler()->schedule([](float fDelta)
	{
		RecordVideoControllerIns()->start(RecordVideoControllerIns()->getCurrentMessageDelegate());
	}, this, 0.0f, 0, 0.0f, false, "delayStart");
}

void RecordVideoController::pause()
{
	if (_bPause)
		return;

	_bPause = true;
	Director::getInstance()->getScheduler()->pauseTarget(this);
	if (_pauseCallBack)
	{
		RecordVideoData d(getPercent(), _fCurrentInterval / _fBasicInterval, _fTotalTime);
		_pauseCallBack(d);
	}
}

void RecordVideoController::playOrResume()
{
	if (!Director::getInstance()->getScheduler()->isScheduled(s_schduleName, this))
	{
		playWithInterval(_fCurrentInterval, false);
		return;
	}

	if (!_bPause)
		return;

	_bPause = false;
	Director::getInstance()->getScheduler()->resumeTarget(this);
	if (_resumeCallBack)
	{
		RecordVideoData d(getPercent(), _fCurrentInterval / _fBasicInterval, _fTotalTime);
		_resumeCallBack(d);
	}
}

void RecordVideoController::end()
{
	Director::getInstance()->getScheduler()->unschedule(s_schduleName, this);
	
	if (_endCallBack)
	{
		RecordVideoData d(getPercent(), _fCurrentInterval / _fBasicInterval, _fTotalTime);
		_endCallBack(d);
	}
}

void RecordVideoController::registNotification()
{
	_listener = EventListenerCustom::create("RecordVideoController-start", [=](EventCustom* event){
		startNotification(event);
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);
}

void RecordVideoController::unRegistNotification()
{
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
}

bool RecordVideoController::createVideo(const std::string &sFilePath, const unsigned int uGameID)
{
	if (!FileUtils::getInstance()->isFileExist(sFilePath))
		return false;
	
	RecordFacade::create(sFilePath);
	if (!_currentFacade)
		return false;

	log("currentFacade %d", _currentFacade);
	if (!_currentFacade->isParsed() && !_currentFacade->parseRecordFile(sFilePath.c_str()))
		return false;

	initResource();
	
	//启动游戏
	bool ret = GameCreator()->startGameClient(uGameID, RECORD_PLAY_DESKNO, false, true);
	if (!ret) return false;

	return true;
}

void RecordVideoController::clearResource()
{
	UserInfoModule()->clear();
	RoomInfoModule()->clear();
	RoomLogic()->setSelectedRoom(nullptr);
	HNSocketProtocolData::GameCheckCode = INVALID_VALUE;
}

void RecordVideoController::initResource()
{
	_currentFacade->handlerFirstCommand();
}

void RecordVideoController::reStart()
{
	if (!_currentFacade)
		return;

	resetData();

	//派发命令头
	_currentFacade->dispatchCommand(eCommandType::GameHead);
	//返回第一个消息
	step(-_currentFacade->getCommandCount(eCommandType::GameMessage));

	playWithInterval(_fCurrentInterval, false);

	if (_startCallBack)
	{
		RecordVideoData d;
		d.fPercent = 0.0f;
		d.fRate = _fCurrentInterval / _fBasicInterval;
		d.fTotalTime = _fTotalTime;

		_startCallBack(d);
	}
}

void RecordVideoController::removeCurrentFacade()
{
	if (!_bSaveParser)
	{
		Remove(_currentFacade->getName());
		_currentFacade = nullptr;
	}
}

float RecordVideoController::getPercent()
{
	if (0 == _currentFacade->getCommandCount(eCommandType::GameMessage))
		return 0.0f;
	return (float)(_currentFacade->getCurrentCommandIndex(eCommandType::GameMessage) + 1) / _currentFacade->getCommandCount(eCommandType::GameMessage);
}

void RecordVideoController::endVideo()
{
	Director::getInstance()->getScheduler()->unschedule(s_schduleName, this);
	if (_viewDelegate)
	{
		_viewDelegate->removeFromViewport();
		setViewDelegate(nullptr);
	}

	//退出回放模式,再次开启断线重连
	Reconnection::getInstance()->openOrCloseReconnet(true);

	clearResource();
	resetCallBack();
	InstanceCreator<RecordCommandFactory>::deleteInstance();
}

#define RECORD_VIEW_DELEGATEZORDER 200000000
void RecordVideoController::startVideo(const std::string &sFilePath, const unsigned int uGameID, VideoViewDelegate *d, char* RecordCode)
{
	//放回放 不需要断线重连
	Reconnection::getInstance()->openOrCloseReconnet(false);

	//记录游戏回放码
	_RecordCode = RecordCode;
	//strcpy(_RecordCode, RecordCode);

	Director::getInstance()->getScheduler()->schedule([=](float fDelta)
	{
		if (!createVideo(sFilePath, uGameID))
			return;

		setViewDelegate(d);

		Director::getInstance()->getScheduler()->schedule([=](float fDelta)
		{
			_viewDelegate->addToViewport(Director::getInstance()->getRunningScene(), RECORD_VIEW_DELEGATEZORDER, _RecordCode);
			if (_startCallBack)
			{
				RecordVideoData d;
				d.fPercent = 0.0f;
				d.fRate = _fCurrentInterval / _fBasicInterval;
				d.fTotalTime = _fTotalTime;
				_startCallBack(d);
			}
		}, this, 0.0f, 0, 0.0f, false, "delayAddViewPort");

	}, this, 0.0f, 0, 0.0f, false, "delayStartGame");

}

void RecordVideoController::setViewDelegate(VideoViewDelegate *d)
{
	if (_viewDelegate)
		delete _viewDelegate;

	_viewDelegate = d;
}

void RecordVideoController::resetCallBack()
{
	_startCallBack = _playCallBack = _endCallBack = _resumeCallBack = _pauseCallBack = _stepCallBack= nullptr;
}

void RecordVideoController::changePlayRate(const float fRate)
{
	_fCurrentInterval = fRate * _fBasicInterval;

	//若定时器还没有注册，则不做处理
	if (!Director::getInstance()->getScheduler()->isScheduled(s_schduleName, this))
		return;

	playWithInterval(_fCurrentInterval, _bPause);
}

void RecordVideoController::resetData()
{
	_fCurrentInterval = 1.0f;
	_fTotalTime = 0.0f;
	_bPause = false;
}

void RecordVideoController::readConfig(const char *sFilePath)
{
	if (!sFilePath || !FileUtils::getInstance()->isFileExist(sFilePath))
		return;

	bool bRet = true;
	do 
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(FileUtils::getInstance()->getStringFromFile(sFilePath).c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			bRet = false;
			break;
		}

		_fBasicInterval = doc["interval"].GetDouble();
		_bSaveParser = doc["saveParse"].GetBool();
		_nStepSpace = doc["step"].GetInt();
	} while (0);

	if (!bRet)
	{
		_fBasicInterval = 1.0f;
		_bSaveParser = false;
		_nStepSpace = 5;
	}
}

void RecordVideoController::stepFront()
{
	step(_nStepSpace);
	if (_stepCallBack)
	{
		RecordVideoData d(getPercent(), _fCurrentInterval / _fBasicInterval, _fTotalTime);
		_stepCallBack(d);
	}
}

void RecordVideoController::stepBack()
{
	step(-_nStepSpace);
	if (_stepCallBack)
	{
		RecordVideoData d(getPercent(), _fCurrentInterval / _fBasicInterval, _fTotalTime);
		_stepCallBack(d);
	}
}

void RecordVideoController::step(const int nStep)
{
	if (!_currentFacade)
		return;

	int nCurrentIndex = _currentFacade->getCurrentCommandIndex(eCommandType::GameMessage);
	int nCurrentStep = nCurrentIndex + nStep;
	if (nCurrentStep < 0)
		nCurrentStep = 0;

	int nMaxGameMessage = _currentFacade->getCommandCount(eCommandType::GameMessage);
	if (nCurrentStep >= nMaxGameMessage)
		nCurrentStep = nMaxGameMessage - 1;

	int nValidCurrentIndex = _currentFacade->validCommandForIndex(eCommandType::GameStation, nCurrentIndex,nCurrentStep);

	if (-1 != nValidCurrentIndex)
	{
		_currentFacade->setCommandIndex(eCommandType::GameStation, nValidCurrentIndex);
		_currentFacade->setCommandIndex(eCommandType::GameMessage, nValidCurrentIndex);
		_currentFacade->dispatchCommand(eCommandType::GameMessage);
		_currentFacade->dispatchCommand(eCommandType::GameStation);
	}
}

char * RecordVideoController::s_schduleName = "RecordSchdule";

