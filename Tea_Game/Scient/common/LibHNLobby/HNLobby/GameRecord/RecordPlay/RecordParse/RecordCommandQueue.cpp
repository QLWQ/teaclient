/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RecordCommandQueue.h"

using namespace HN;
void RecordCommandQueue::pushCommand(eCommandType type, RecordCommand *command)
{
	if (eCommandType::GameHead == type)
		_firstCommand = command;
	else
	{
		CommandQueue *qCommand = getCommandQueue(type);
		if (!qCommand)
		{
			CommandQueue q;
			q.push_back(command);
			_mapRecordCommand.insert(std::make_pair(type, q));
			_mapCurrentIndexs.insert(std::make_pair(type, 0));
		}
		else
			qCommand->push_back(command);
	}
}

RecordCommandQueue::CommandQueue* RecordCommandQueue::getCommandQueue(eCommandType type)
{
    if(_mapRecordCommand.size()==0)
        return nullptr;
    
	auto it=_mapRecordCommand.find(type);
	if (it == _mapRecordCommand.end())
		return nullptr;

	return &it->second;
}


RecordCommand* RecordCommandQueue::frontCommand(eCommandType type)
{
	if (eCommandType::GameHead == type)
		return _firstCommand;

	CommandQueue *q=getCommandQueue(type);
	if (!q || q->empty())
		return nullptr;

	return q->front();
}

RecordCommandQueue::RecordCommandQueue(): _firstCommand(nullptr)
{

}

RecordCommandQueue::~RecordCommandQueue()
{
	clearAllCommand();
}

void RecordCommandQueue::popFront(eCommandType type)
{
	CommandQueue *q = getCommandQueue(type);
	if (!q || q->empty())
		return;

	//release dynamic command in memPool and it's will call the command destructor
	InstanceCreator<RecordCommandFactory>::getInstance()->releaseCommand(q->front());
	q->pop_front();
}

void RecordCommandQueue::clearCommandByType(eCommandType type)
{
	if (eCommandType::GameHead == type)
		releaseFirstCommand();

	CommandQueue *q = getCommandQueue(type);
	if (!q || q->empty())
		return;

	for (size_t i = 0; i < q->size();++i)
		InstanceCreator<RecordCommandFactory>::getInstance()->releaseCommand(q->at(i));

	q->clear();

	_mapRecordCommand.erase(type);
	_mapCurrentIndexs.erase(type);
}

void RecordCommandQueue::clearAllCommand()
{
	for (auto it = _mapRecordCommand.begin(); it != _mapRecordCommand.end(); ++it)
	{
		for (size_t i = 0; i < it->second.size();++i)
			InstanceCreator<RecordCommandFactory>::getInstance()->releaseCommand(it->second.at(i));

		it->second.clear();
	}

	_mapRecordCommand.clear();
	_mapCurrentIndexs.clear();

	releaseFirstCommand();
}

void RecordCommandQueue::dispatchCommand(eCommandType type)
{
	if (eCommandType::GameHead == type)
	{
		_firstCommand->execute(0.0f);
	}
	else
	{
		RecordCommand *command = frontCommand(type);
		if (command)
		{
			command->execute(0.0f);
			popFront(type);
		}
	}
}

int RecordCommandQueue::getCurrentCommandQueueIndex(eCommandType type)const
{
	auto it = _mapCurrentIndexs.find(type);
	if (it == _mapCurrentIndexs.end())
		return -1;

	return it->second;
}

void RecordCommandQueue::setCurrentCommandQueueIndex(eCommandType type, const int index)
{
	auto it = _mapCurrentIndexs.find(type);
	if (it == _mapCurrentIndexs.end())
		return;

	it->second = index;
}

void RecordCommandQueue::dispatchCurrentCommand(eCommandType type, const int nIndex)
{
	if (eCommandType::GameHead == type)
		_firstCommand->execute(0.0f);
	else
	{
		CommandQueue *q = getCommandQueue(type);
		if (!q)
			return;

		if (nIndex < 0 || nIndex >= q->size())
			return;

		q->at(nIndex)->execute(0.0f);
	}
}

int RecordCommandQueue::getCommandQueueCount(eCommandType type)
{
	CommandQueue *q = getCommandQueue(type);
	if (!q)
		return -1;

	return q->size();
}

void RecordCommandQueue::releaseFirstCommand()
{
	if (_firstCommand)
	{
		InstanceCreator<RecordCommandFactory>::getInstance()->releaseCommand(_firstCommand);
		_firstCommand = nullptr;
	}
}

RecordCommand* RecordCommandQueue::getCommand(eCommandType type, const int nIndex)
{
	CommandQueue *q = getCommandQueue(type);
	if (!q)
		return nullptr;

	return (*q)[nIndex];
}
