/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_MESSAGECOMMAND_H__
#define __HN_MESSAGECOMMAND_H__

#include <functional>
#include <assert.h>
#include "../MemPool/TinyMemPool.h"
#include "../RecordParse/InstanceCreator.h"
#include "HNBaseType.h"
#include "HNNetProtocol/HNProtocolExport.h"
/*
author syf
MessageCommand will push to the command queue
*/
#define RECORD_PARSEDATA_NUM 10240

template<typename T,typename Ret,typename ...Args>
struct MessageCommand
{
	typedef T value_type;
	typedef Ret Return_Type;
	typedef std::function<Ret(Args...)> Func_Type;

	MessageCommand() :callFunc(nullptr)
	{
	}

	~MessageCommand()
	{
		int a = 0;
	}

	Return_Type execute(const typename std::remove_reference<Args>::type& ...args)
	{
		assert(callFunc);
		return callFunc(args...);
	}

	void setValue(const value_type &v)
	{
		value = v;
	}

	Func_Type callFunc;
	value_type value;
};

struct RecordSocketMessage
{
	RecordSocketMessage()
	{
		memset(this, 0, sizeof(RecordSocketMessage));
	}

	~RecordSocketMessage()
	{
		int a = 0;
	}

	void setMessage(const HN::NetMessageHead* head, CHAR* obj, INT Size)
	{
		messageHead = *head;
		objectSize = Size;
		memcpy(object, obj, objectSize);
		CHAR buf[16] = { 0 };
		sprintf(buf, "%u_%u", messageHead.bMainID, messageHead.bAssistantID);
		strKey.assign(buf);
	}

	HN::NetMessageHead messageHead;
	UINT objectSize;			// message size
	CHAR object[RECORD_PARSEDATA_NUM];
	emSocketStatus socketStatus;
	std::string strKey;
};

struct RecordCommand :MessageCommand<RecordSocketMessage, void, float>
{
	RecordCommand()
	{
	}

	~RecordCommand()
	{
	}

	void setMessage(const HN::NetMessageHead *head, CHAR *obj, INT size)
	{
		value.setMessage(head, obj, size);
	}
};

template<typename Alloc>
class CommandFactory
{
	typedef Alloc Alloc_Type;
	typedef typename Alloc_Type::value_type value_type;
	typedef CommandFactory<Alloc> MyType;

public:
	~CommandFactory()
	{
	}

	value_type* createCommand()
	{
		value_type *c = _allocate.Alloc();
		return c;
	}

	void releaseCommand(value_type *c)
	{
		_allocate.Free(c);
	}

protected:
	CommandFactory()
	{
	}

private:
	friend class InstanceCreator<MyType,false>;
private:
	Alloc_Type _allocate;
};

typedef CommandFactory<syf::TinyMemCreator<RecordCommand> > RecordCommandFactory;


#define RECORD_PLAY_DESKNO 100
#define RECORD_BASICUSERID 10010
#define RECORD_PLAY_COUNT 3  /*
								must be changed for different game,because the different game has different maxPlayers(HNGameLogicBase is not support getMaxPlayers),
								so it can't trigger a assert
							*/
#define RECORD_GAME_CHECKCODE 1
								
#pragma pack(1)
struct MsgHead
{
	int msgID;
	int msgLen;
	int stationLen;
	LLONG lTime;

	MsgHead()
	{
		memset(this, 0, sizeof(MsgHead));
	}
};

struct DynamicUserInfo
{
	char		szNickName[61];			//昵称
	char		szheadUrl[256];			//头像地址
	UINT		nLogoID;				//头像ID
	LLONG		i64UserMoney;			//玩家金币
	bool		bBoy;					//性别

	DynamicUserInfo()
	{
		memset(this, 0, sizeof(DynamicUserInfo));
	}
};

struct StaticUserInfo
{
	BYTE		bWatchDeskView;						//观看的玩家作为

	char		szDeskPassWord[20];					//桌子密码

	bool		bVIPRoom;							//是否VIP房间
	bool		bBuyRoom;							//是否可购买房间

	bool		bHasPassword;						//是否有密码

	int			nPlayerNum;

	UINT		nComType;							//游戏类型
	UINT		nKindID;							//游戏类型ID
	UINT		nNameID;							//游戏名称ID
	UINT		dwRoomRule;							//游戏房间规则

	int			nBasePoint;							//基础倍数

	int			iVipGameCount;						//购买桌子局数
	int			iRunGameCount;						//当前局数

	StaticUserInfo()
	{
		memset(this, 0, sizeof(StaticUserInfo));
	}
};

struct RecordUserInfo
{
	StaticUserInfo userInfoS;
	DynamicUserInfo *userInfoD;
	RecordUserInfo()
	{
		memset(this, 0, sizeof(RecordUserInfo));
	}

	~RecordUserInfo()
	{
		if (userInfoD)
			delete[] userInfoD;
	}
};
#pragma pack()

enum class eCommandType
{
	GameHead,
	GameMessage,
	GameStation,
};

template<class T>
struct remove_const_refrence
{
	typedef typename std::remove_const<typename std::remove_reference<T>::type>::type type;
};

template<class T>
struct remove_const_refrence_pointer
{
	typedef typename std::remove_pointer<typename remove_const_refrence<T>::type>::type type;
};

#endif
