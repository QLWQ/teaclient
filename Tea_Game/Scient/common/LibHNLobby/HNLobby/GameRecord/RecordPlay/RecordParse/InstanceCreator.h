/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_INSTANCECREATOR_H__
#define __HN_INSTANCECREATOR_H__

#include <memory>
#include <mutex>

//��������
/*
author syf
instance create
*/
template<typename T, bool THREADSAFE = false>
class InstanceCreator
{
public:
	~InstanceCreator(){}

	static T* getInstance()
	{
		if (_safeThread)
			return onSafeThreadInstance();
		else
			return onNoSafeThreadInstance();
	}

	static void deleteInstance()
	{
		if (_safeThread)
			onSafeDeleteInstance();
		else
			onNoSafeDeleteInstance();
	}

protected:
	static T* onNoSafeThreadInstance()
	{
		if (!_autoInstance.get())
			_autoInstance.reset(new T());

		return _autoInstance.get();
	}

	static T* onSafeThreadInstance()
	{
		if(!_autoInstance.get())
		{
			_instanceMutex.lock();
			if (!_autoInstance.get())
				_autoInstance.reset(new T());
			_instanceMutex.unlock();
		}

		return _autoInstance.get();
	}

	static void onNoSafeDeleteInstance()
	{
		_autoInstance.reset();
	}

	static void onSafeDeleteInstance()
	{
		if (_autoInstance.get() != nullptr)
		{
			_instanceMutex.lock();
			_autoInstance.reset();
			_instanceMutex.unlock();
		}
	}
protected:
	InstanceCreator(){}
	InstanceCreator(const InstanceCreator &i) = delete;
	InstanceCreator& operator=(const InstanceCreator &i) = delete;

private:
	static std::auto_ptr<T> _autoInstance;
	static const bool _safeThread=THREADSAFE;
	static std::mutex _instanceMutex;
};

template<typename T, bool THREADSAFE /*= false*/>
std::mutex InstanceCreator<T, THREADSAFE>::_instanceMutex;

template<typename T, bool THREADSAFE /*= false*/>
std::auto_ptr<T> InstanceCreator<T, THREADSAFE>::_autoInstance = std::auto_ptr<T>(nullptr);

#endif