/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_FILEPARSER_H__
#define __HN_FILEPARSER_H__

#include "MessageCommand.h"
#include "ParserData.h"

class FileParserDelegate;
class FileParser
{
public:
	enum class eErrorType
	{
		ParseSuccess,
		ParseFailed,
		FileNotExist,
		FileOpenFailed,
	};
public:
	typedef RecordParserData::diff_type diff_type;
	typedef RecordParserData::value_type value_type;
public:
	enum class eType
	{
		TXT,
		BINARY,
	};

	~FileParser();

	eErrorType parseRecordFile(const char *sFilePath, eType parseType = eType::BINARY);

	//void errorHandler(eErrorType type);
protected:
	bool parseRecordData(RecordParserData &data);

	void resetData();
protected:
	FileParser(FileParserDelegate *d);
	FileParser(const FileParser &f) = delete;
	FileParser& operator=(const FileParser &f) = delete;
private:
	RecordParserData _data;
	//�ļ�ָ��ƫ����
	diff_type _fileOffset;

	FileParserDelegate *_delegate;

	eCommandType _commandType;
private:
	friend class RecordFacade;
};

class FileParserDelegate
{
public:
	virtual ~FileParserDelegate()
	{
	}
public:
	virtual void pushCommand(eCommandType type, const MsgHead &head, CHAR *obj) = 0;
};
#endif