/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "FileParser.h"
#include "cocos2d.h"
#include "HNNetExport.h"

USING_NS_CC;
using namespace HN;
FileParser::FileParser(FileParserDelegate *d) :_fileOffset(0), _delegate(d)
{

}

FileParser::~FileParser()
{
}

FileParser::eErrorType FileParser::parseRecordFile(const char *sFilePath, eType parseType/*=eType::BINARY*/)
{
	HNLOG_DEBUG("biaoji");
	if (!sFilePath || !FileUtils::getInstance()->isFileExist(sFilePath))
		return eErrorType::FileNotExist;
	HNLOG_DEBUG("biaoji");
	std::string sOpenMode;
	if (parseType == eType::BINARY)
		sOpenMode = "rb";
	else
		sOpenMode = "r";
	HNLOG_DEBUG("biaoji");
	FILE *pFile=fopen(sFilePath, sOpenMode.c_str());
	if (!pFile)
		return eErrorType::FileOpenFailed;
	HNLOG_DEBUG("biaoji");
	value_type sBufferTemp[RECORD_PARSEDATA_NUM];
	HNLOG_DEBUG("biaoji");
	//获取文件总大小
	fseek(pFile, 0, SEEK_END);
	long totalSize=ftell(pFile);
	if (0 == totalSize)
		return eErrorType::ParseSuccess;
	HNLOG_DEBUG("biaoji");
	bool bShouldBreak = false;
	//移动文件指针至起始处
	fseek(pFile, 0, SEEK_SET);
	_fileOffset = 0;
	HNLOG_DEBUG("biaoji");
	while (!feof(pFile))
	{
		HNLOG_DEBUG("biaoji");
		//每次读取_data的最大容量
		int nRead=(int)fread(sBufferTemp, sizeof(char), _data.getMaxSize(), pFile);
		HNLOG_DEBUG("biaoji");
		//此时文件已经读完
		if (_fileOffset + nRead >= totalSize)
			bShouldBreak = true;
		HNLOG_DEBUG("biaoji");
		//重置_data
		_data.resetData();
		_data.setData(sBufferTemp, nRead);
		//解析_data
		if (parseRecordData(_data)) //解析成功
			//重新设置文件指针
			fseek(pFile, _fileOffset, SEEK_SET);
		else //解析失败
		{
			fclose(pFile);
			resetData();
			return eErrorType::ParseFailed;
		}
		HNLOG_DEBUG("biaoji");
		if (bShouldBreak)
			break;
	}
	HNLOG_DEBUG("biaoji");
	fclose(pFile);
	resetData();
	return eErrorType::ParseSuccess;
}

bool FileParser::parseRecordData(RecordParserData &data)
{
	HNLOG_DEBUG("biaoji");
	while (1)
	{
		if (data.getReverseOffset() < sizeof(MsgHead))
			break;

		value_type *pContent=data.getData();
		MsgHead *pHead = (MsgHead*)pContent;
		if (!pHead)
			return false;

		diff_type nIndex = 0;
		nIndex += (int)sizeof(MsgHead);
		nIndex += pHead->msgLen + pHead->stationLen;
		//判断此时是否越界
		if (data.isOverBoundary(data.getOffset() + nIndex))
			break;
		else
		{
			//偏移一个消息头的大小
			data.setOffset(sizeof(MsgHead));
			
			eCommandType type;

			//分别处理第一个消息包和后续消息包
			if (0 == _fileOffset)
				type = eCommandType::GameHead;
			else
			{
				//此时一个消息体中包含GameMessage和GameStation，需要分别进行处理
				type = eCommandType::GameStation;
				if (pHead->stationLen > 0)
				{
					HNLOG_DEBUG("biaoji");
					_delegate->pushCommand(type, *pHead, data.getData());
					data.setOffset(pHead->stationLen);
				}
				else
				{
					MsgHead headTemp;
					headTemp.msgID = pHead->msgID;
					headTemp.stationLen = 0;
					_delegate->pushCommand(type, headTemp, nullptr);
				}

				type = eCommandType::GameMessage;
			}
				
			_delegate->pushCommand(type, *pHead, data.getData());
			//偏移一个消息体的大小
			data.setOffset(pHead->msgLen);

			//重新设置文件指针偏移量
			_fileOffset += nIndex;
		}
	}
	HNLOG_DEBUG("biaoji");
	return true;
}

void FileParser::resetData()
{
	_data.resetData();
	_fileOffset = 0;
}


