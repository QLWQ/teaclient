/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RecordFacade.h"
#include "RecordVideoController.h"


USING_NS_CC;
using namespace HN;

RecordFacade::RecordFacade(HN::IGameMessageDelegate *d)
{
	//call the default construct
	new(this) RecordFacade();
	_delegate = d;
}

RecordFacade::RecordFacade() :_parser(new(std::nothrow) FileParser(this)),
_eState(eVideoState::None), _resultCallBack(nullptr), _commandQueue(new(std::nothrow) RecordCommandQueue()), _bAreadyParse(false)
{
}

RecordFacade::~RecordFacade()
{
	CC_SAFE_DELETE(_parser);
	CC_SAFE_DELETE(_commandQueue);
}

void RecordFacade::pushCommand(eCommandType type, const MsgHead &head, CHAR *obj)
{
	RecordCommand *command=makeCommand(head,obj,type);
	_commandQueue->pushCommand(type, command);
}

void RecordFacade::handleParseResult(FileParser::eErrorType type)
{
	std::string sMessage;
	bool bRet = false;
	switch (type)
	{
	case FileParser::eErrorType::ParseSuccess:
		bRet = true;
		_bAreadyParse = true;
		sMessage = "文件解析成功";
		break;
	case FileParser::eErrorType::ParseFailed:
		_commandQueue->clearAllCommand();	//清空所有命令
		sMessage = "文件解析错误";
		break;
	case FileParser::eErrorType::FileNotExist:
		sMessage = "文件不存在";
		break;
	case FileParser::eErrorType::FileOpenFailed:
		sMessage = "文件打开错误";
		break;
	default:
		sMessage = "未知错误";
		break;
	}

	if (_resultCallBack)
		_resultCallBack(bRet, sMessage);
}

bool RecordFacade::parseRecordFile(const char *sFilePath)
{
	HNLOG_DEBUG("biaoji");
	if (_parser)
	{
		HNLOG_DEBUG("biaoji");
		FileParser::eErrorType type=_parser->parseRecordFile(sFilePath);
		HNLOG_DEBUG("biaoji");
		handleParseResult(type);
		HNLOG_DEBUG("biaoji");
		if (FileParser::eErrorType::ParseSuccess != type)
			return false;

		return true;
	}

	return false;
}

void RecordFacade::dispatchCommand(eCommandType type)
{
	if (!_commandQueue)
		return;

	if (eCommandType::GameHead == type)
	{
		_commandQueue->dispatchCurrentCommand(eCommandType::GameHead, 0);
		_eState = eVideoState::Start;
	}
	else
	{
		int nIndex = _commandQueue->getCurrentCommandQueueIndex(type);
		if (nIndex == _commandQueue->getCommandQueueCount(type) - 1)
			_eState = eVideoState::Ended;
		else
			_eState = eVideoState::Processing;

		_commandQueue->dispatchCurrentCommand(type, nIndex);
		setCommandIndex(type, nIndex + 1);
	}
}

void RecordFacade::setCommandIndex(eCommandType type, const int nIndex)
{
	if (!_commandQueue)
		return;

	int nTemp = nIndex;
	if (nIndex < 0)
		nTemp = 0;
	else if (nIndex >= _commandQueue->getCommandQueueCount(type))
		nTemp = _commandQueue->getCommandQueueCount(type) - 1;

	_commandQueue->setCurrentCommandQueueIndex(type, nTemp);
}

int RecordFacade::getCommandCount(eCommandType type)
{
	if (_commandQueue)
		return _commandQueue->getCommandQueueCount(type);

	return -1;
}

void RecordFacade::setName(const std::string &name)
{
	_sName = name;
}

std::string RecordFacade::getName() const
{
	return _sName;
}

RecordFacade* RecordFacade::create(const std::string &sName/*=""*/)
{
	RecordFacade *pFacade = new(std::nothrow) RecordFacade();
	pFacade->setName(sName);
	pFacade=pFacade->registSelf();

	return pFacade;
}

void RecordFacade::setDelegate(HN::IGameMessageDelegate *d)
{
	_delegate = d;
}

RecordFacade* RecordFacade::registSelf()
{
	RecordFacade *d = const_cast<RecordFacade*>(this);
	if (!d)
		return nullptr;

	//注册节点
	bool bRet = RegistTool::RegistNode<RecordFacade, RecordVideoController>(d, RecordVideoControllerIns());

	if (!bRet)
	{
		RecordFacade *temp = d;
		RecordVideoControllerIns()->Find(d->getName(), d);
		delete temp;
	}
	
	//设置当前的facade
	RecordVideoControllerIns()->setCurrentFacade(d);

	return d;
}

void RecordFacade::resetAllCommandIndex()
{
	setCommandIndex(eCommandType::GameStation, 0);
	setCommandIndex(eCommandType::GameMessage, 0);
}

void RecordFacade::clearAllCommands()
{
	_commandQueue->clearAllCommand();
	_bAreadyParse = false;
}

RecordCommand* RecordFacade::makeCommand(const MsgHead &head, CHAR *obj, eCommandType type)
{
	NetMessageHead msgHead;
	msgHead.bMainID = MDM_GM_GAME_NOTIFY;
	msgHead.bAssistantID = head.msgID;
	msgHead.bHandleCode = 0;

	RecordCommand* command = InstanceCreator<RecordCommandFactory>::getInstance()->createCommand();

	switch (type)
	{
	case eCommandType::GameMessage:
		msgHead.uMessageSize = head.msgLen + sizeof(NetMessageHead);
		command->setMessage(&msgHead, obj, head.msgLen);
		command->callFunc = [command, this](float fTime)
		{
			if (_delegate)
				_delegate->onGameMessage(&command->value.messageHead, command->value.object, command->value.objectSize);
		};
		break;
	case eCommandType::GameStation:
		msgHead.uMessageSize = head.stationLen + sizeof(NetMessageHead);
		if (head.stationLen > 0)
		{
			command->setMessage(&msgHead, obj, head.stationLen);
			command->callFunc = [command, this](float fTime)
			{
				if (_delegate)
				{
					_delegate->I_R_M_GameStation(command->value.object, command->value.objectSize);
					_delegate->I_R_M_GamePlayBack(true);
				}
			};
		}
		else
			command->callFunc = [](float fTime)
			{
			};
		break;
	case eCommandType::GameHead:
		msgHead.uMessageSize = head.msgLen + sizeof(NetMessageHead);
		command->setMessage(&msgHead, obj, head.msgLen);
		break;
	default:
		break;
	}

	return command;
}

bool RecordFacade::handlerFirstCommand()
{
	//房间信息添加
	{
		ComRoomInfo info;
		info.bVIPRoom = true;
		info.uRoomID = 10000;
		info.dwRoomRule = GRR_RECORD_GAME;

		RoomInfoModule()->addRoom(&info);
		RoomLogic()->setSelectedRoom(RoomInfoModule()->findRoom(info.uRoomID));
		RoomLogic()->setRoomRule(info.dwRoomRule);
		HNSocketProtocolData::GameCheckCode = RECORD_GAME_CHECKCODE;
	}

	//用户信息添加
	RecordUserInfo recordInfo;
	memcpy(&recordInfo, _commandQueue->_firstCommand->value.object, sizeof(StaticUserInfo));
	
	recordInfo.userInfoD = new DynamicUserInfo[recordInfo.userInfoS.nPlayerNum]();
	for (int i = 0; i < recordInfo.userInfoS.nPlayerNum;++i)
	{
		DynamicUserInfo* dInfo = (DynamicUserInfo*)(_commandQueue->_firstCommand->value.object + sizeof(StaticUserInfo)+i*sizeof(DynamicUserInfo));
		recordInfo.userInfoD[i] = *dInfo;
	}

	UserInfoModule()->clear();
	for (int i = 0; i < recordInfo.userInfoS.nPlayerNum; ++i)
	{
		UserInfoStruct info;
		info.bDeskNO = RECORD_PLAY_DESKNO;
		info.bDeskStation = i;
		info.i64Money = recordInfo.userInfoD[i].i64UserMoney;
		info.bBoy = recordInfo.userInfoD[i].bBoy;
		info.dwUserID = RECORD_BASICUSERID + i;
		strncpy(info.headUrl, recordInfo.userInfoD[i].szheadUrl, sizeof(info.headUrl));
		strncpy(info.nickName, recordInfo.userInfoD[i].szNickName, sizeof(info.nickName)*sizeof(char));

		if (0 == adjustLogicSeatNo(i, recordInfo.userInfoS.bWatchDeskView, recordInfo.userInfoS.nPlayerNum))
		{
			// 更新自己信息
			RoomLogic()->loginResult.pUserInfoStruct.bDeskNO = RECORD_PLAY_DESKNO;
			RoomLogic()->loginResult.pUserInfoStruct.bDeskStation = recordInfo.userInfoS.bWatchDeskView;
			RoomLogic()->loginResult.pUserInfoStruct.i64Money = recordInfo.userInfoD[i].i64UserMoney;
			RoomLogic()->loginResult.pUserInfoStruct.dwUserID = PlatformLogic()->loginResult.dwUserID;
			strncpy(RoomLogic()->loginResult.pUserInfoStruct.headUrl, recordInfo.userInfoD[i].szheadUrl, sizeof(RoomLogic()->loginResult.pUserInfoStruct.headUrl));
			strncpy(RoomLogic()->loginResult.pUserInfoStruct.nickName, recordInfo.userInfoD[i].szNickName, sizeof(RoomLogic()->loginResult.pUserInfoStruct.nickName)*sizeof(char));
			info.dwUserID = PlatformLogic()->loginResult.dwUserID;
		}

		UserInfoModule()->updateUser(&info);
	}

	_commandQueue->_firstCommand->callFunc = [this](float fDeltaTime)
	{
		std::vector<UserInfoStruct*> vecUserInfos;
		UserInfoModule()->findDeskUsers(RECORD_PLAY_DESKNO, vecUserInfos);
		if (_delegate)
		{
			//设置回放的时候不显示同IP玩家（回放玩家IP数据没有保存）
			_delegate->I_R_M_GamePlayBack(true);
		}
		for (int i = 0; i < (int)vecUserInfos.size();++i)
		{
			MSG_GR_R_UserSit sit;
			memset(&sit, 0, sizeof(MSG_GR_R_UserSit));
			sit.bDeskIndex = vecUserInfos[i]->bDeskNO;
			sit.bDeskStation = vecUserInfos[i]->bDeskStation;
			sit.dwUserID = vecUserInfos[i]->dwUserID;

			if (_delegate)
				_delegate->I_R_M_UserSit(&sit, vecUserInfos[i]);
		}
	};

	return true;
}

int RecordFacade::adjustLogicSeatNo(int bSrc, int bBasic, int nTotal)
{
	return (bSrc - bBasic + nTotal) % nTotal;
}

int RecordFacade::getCurrentCommandIndex(eCommandType type)
{
	return _commandQueue->getCurrentCommandQueueIndex(type);
}

bool RecordFacade::isValidCommand(eCommandType type, const int nIndex) const
{
	RecordCommand *cmd=_commandQueue->getCommand(type, nIndex);
	if (!cmd)
		return false;

	return (cmd->value.objectSize > 0);
}

int RecordFacade::validCommandForIndex(eCommandType type, const int nCurrent, const int nIndex)
{
	int nMin = 0;
	int nMax = getCommandCount(type);
	if (nIndex<0 || nIndex >= nMax)
		return -1;

	if (isValidCommand(type, nIndex))
		return nIndex;

	bool bFront = (nIndex - nCurrent)>0;

	int nSub = nIndex, nAdd = nIndex;
	while (nSub > 0 && nAdd < nMax-1)
	{
		if (!bFront || nSub-1 > nCurrent)
		{
			if (isValidCommand(type, --nSub))
				return nSub;
		}

		if (bFront || nAdd+1 < nCurrent)
		{
			if (isValidCommand(type,++nAdd))
				return nAdd;
		}
	}

	return -1;
}
