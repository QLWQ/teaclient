/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_TINYMEMPOOLMAROC__
#define __HN_TINYMEMPOOLMAROC__

//only support C++11
//author syf
#include <assert.h>

#undef  SYF_BEGIN
#define SYF_BEGIN namespace syf{
#undef  SYF_END
#define SYF_END }

#undef US_NS_SYF
#define US_NS_SYF using namespace syf

#undef POOL_ASSERT
#define POOL_ASSERT(x) assert(x) 

#undef POOL_STATIC_ASSERT
#define POOL_STATIC_ASSERT(x) static_assert(x)

#endif
