/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/


#include "GameLocation.h"
#include "HNUIExport.h"
#include "HNNetExport.h"
#include "HNMarketExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

using namespace network;

#define JSON_RESOVE(obj, key) (obj.HasMember(key) && !obj[key].IsNull())

namespace HN
{
	static GameLocation* location = nullptr;

	GameLocation* GameLocation::getInstance()
	{
		if (nullptr == location)
		{
			location = new (std::nothrow) GameLocation();
		}
		return location;
	}

	void GameLocation::destroyInstance()
	{
		CC_SAFE_DELETE(location);
	}

	GameLocation::GameLocation()
	{
	}

	GameLocation::~GameLocation()
	{
		
	}

	void GameLocation::getLocation()
	{
#if (CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID && CC_TARGET_PLATFORM != CC_PLATFORM_IOS) 
		int lat = (rand() % (40 - 23)) + 23;
		int lon = (rand() % (117 - 98)) + 98;

		//纬度
		std::string latitude = StringUtils::format("%d", lat);
		//经度
		std::string longtitude = StringUtils::format("%d", lon);

		std::string url = "http://api.map.baidu.com/geocoder/v2/?ak=72ZLMg7P17Dl092USPcpGWsCqwdhoFb2&location=" + latitude + "," + longtitude + "&output=json";

		HNHttpRequest::getInstance()->addObserver(location);
		HNHttpRequest::getInstance()->request("getLocation", cocos2d::network::HttpRequest::Type::GET, url);
		return;
#endif

		auto callback = CALLBACK_PRAGMA([=](const std::string& args){

			rapidjson::Document doc;
			doc.Parse<rapidjson::kParseDefaultFlags>(args.c_str());
			if (!doc.HasParseError() && doc.IsObject())
			{
				bool isSuccess = doc["success"].GetBool();
				if (isSuccess)
				{

					//纬度
					std::string latitude = doc["latitude"].GetString();
					//经度
					std::string longtitude = doc["lontitude"].GetString();
            
					std::string url = "http://api.map.baidu.com/geocoder/v2/?ak=72ZLMg7P17Dl092USPcpGWsCqwdhoFb3&location=" + latitude + "," + longtitude + "&output=json";
            
					//百度地图webapi请求地址
					//http://api.map.baidu.com/geocoder/v2/?ak=72ZLMg7P17Dl092USPcpGWsCqwdhoFb3&location=22.545624,113.948572&output=json
            
					HNHttpRequest::getInstance()->addObserver(location);
					HNHttpRequest::getInstance()->request("getLocation", cocos2d::network::HttpRequest::Type::GET, url);
				}
				else
				{
					//不强制开启定位
					/*
					// 如果在登陆界面重连进需要定位的房间但是定位失败，则不允许进入，且断开已连接tcp
					if (PlatformConfig::getInstance()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
					{
						PlatformLogic()->close();
					}
					*/

					if (onGetLocationCallBack)
					{
						onGetLocationCallBack(false, 0, 0, "", "", "", "", "", "");
                        onGetLocationCallBack = nullptr;
					}
					

					if (onGetLocationCallBackLite)
					{
						onGetLocationCallBackLite(false, 0, 0, "");
                        onGetLocationCallBackLite = nullptr;
					}					
				}
			}
		});

		std::string pos = Operator::requestChannel("hnLocation", "getLocation", "", &callback);
	}

	void GameLocation::getLocation(std::string& ip)
	{
		if (ip.empty())
		{
			if (onGetLocationCallBackLite)
			{
				onGetLocationCallBackLite(false, 0.0f, 0.0f, "");
				onGetLocationCallBackLite = nullptr;
			}

			return;
		}

		std::string url = "http://api.map.baidu.com/location/ip?ak=72ZLMg7P17Dl092USPcpGWsCqwdhoFb3&coor=bd09ll&ip=" + ip;

		HNHttpRequest::getInstance()->addObserver(location);
		HNHttpRequest::getInstance()->request("getLocationIP", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void GameLocation::getAddrByLocation(float x, float y)
	{
		std::string url = StringUtils::format("http://alipay.qp888m.com/app/GLI.php?location=%f,%f", x, y);

		HNHttpRequest::getInstance()->addObserver(location);
		HNHttpRequest::getInstance()->request("getAddr", cocos2d::network::HttpRequest::Type::GET, url);
	}

	void GameLocation::getDistanceByLocation(float fromX, float fromY, float toX1, float toY1, float toX2, float toY2, float toX3, float toY3)
	{
		std::string url = StringUtils::format("http://alipay.qp888m.com/app/GDI.php?from=%f,%f&to=%f,%f", fromX, fromY, toX1, toY1);
		if (toX2 != 0)
		{
			string str = StringUtils::format(";%f,%f", toX2, toY2);
			url = url + str;
		}
		if (toX3 != 0)
		{
			string str = StringUtils::format(";%f,%f", toX3, toY3);
			url = url + str;
		}

		HNHttpRequest::getInstance()->addObserver(location);
		HNHttpRequest::getInstance()->request("getDistance", cocos2d::network::HttpRequest::Type::GET, url);
	}
    
    void GameLocation::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
    {
		if (requestName.compare("getLocation") != 0 && requestName.compare("getLocationIP") != 0 && requestName.compare("getAddr") != 0 && requestName.compare("getDistance") != 0) return;

		HNHttpRequest::getInstance()->removeObserver(location);

        if (!isSucceed)
        {
            GamePromptLayer::create()->showPrompt(GBKToUtf8("获取位置信息失败"));

			//不强制开启定位
			/*
			// 如果在登陆界面重连进需要定位的房间但是定位失败，则不允许进入，且断开已连接tcp
			if (PlatformConfig::getInstance()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
			{
				PlatformLogic()->close();
			}
			*/
            return;
        }
        
        if (requestName.compare("getLocation") == 0)
        {
            getLocationResult(responseData);
        }

		if (requestName.compare("getLocationIP") == 0)
		{
			getLocationIPResult(responseData);
		}

		if (requestName.compare("getAddr") == 0)
		{
			getAddrResult(responseData);
		}

		if (requestName.compare("getDistance") == 0)
		{
			getDistanceResult(responseData);
		}
    }
    
    void GameLocation::getLocationResult(const std::string& data)
    {
        rapidjson::Document doc;
        doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
        
        if (JSON_RESOVE(doc, "errcode") || !doc.IsObject())
        {
            GamePromptLayer::create()->showPrompt(GBKToUtf8("获取位置信息失败"));

			//不强制开启定位
			/*
			// 如果在登陆界面重连进需要定位的房间但是定位失败，则不允许进入，且断开已连接tcp
			if (PlatformConfig::getInstance()->getSceneState() == PlatformConfig::SCENE_STATE::OTHER)
			{
				PlatformLogic()->close();
			}
			*/
            return;
        }
        
        if (JSON_RESOVE(doc, "result"))
        {
            //纬度
            double latitude = doc["result"]["location"]["lat"].GetDouble();
            //经度
            double longtitude = doc["result"]["location"]["lng"].GetDouble();
            
            //详细地址
            std::string addr = doc["result"]["formatted_address"].GetString();
            //国家
            std::string country = doc["result"]["addressComponent"]["country"].GetString();
            //省份
            std::string province = doc["result"]["addressComponent"]["province"].GetString();
            //城市
            std::string city = doc["result"]["addressComponent"]["city"].GetString();
            //区
            std::string district = doc["result"]["addressComponent"]["district"].GetString();
            //街道
            std::string street = doc["result"]["addressComponent"]["street"].GetString();

			HNLog::logDebug("latitude:%lf, longtitude:%lf, addr:%s, country:%s, province:%s, city:%s, district:%s, street:%s",
				latitude, longtitude, addr.c_str(), country.c_str(), province.c_str(), city.c_str(), district.c_str(), street.c_str());

			if (onGetLocationCallBack)
			{
				// 由于跟平台协商的地址长度为64字节，考虑到太长客户端也不好显示，故详细地址只取市，区，街道
				std::string location = city + district + street;

				onGetLocationCallBack(true, latitude, longtitude, location, country,
					province, city, district, street);
				onGetLocationCallBack = nullptr;
			}

			if (onGetLocationCallBackLite)
			{
				// 由于跟平台协商的地址长度为64字节，考虑到太长客户端也不好显示，故详细地址只取市，区，街道
				std::string location = city + district + street;

				onGetLocationCallBackLite(true, latitude, longtitude, location);
				onGetLocationCallBackLite = nullptr;
			}		
        }
		if (onGetLocationCallBack)
		{
			onGetLocationCallBack(false, 0, 0, "", "", "", "", "", "");
			onGetLocationCallBack = nullptr;
		}


		if (onGetLocationCallBackLite)
		{
			onGetLocationCallBackLite(false, 0, 0, "");
			onGetLocationCallBackLite = nullptr;
		}
    }

	void GameLocation::getLocationIPResult(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (JSON_RESOVE(doc, "errcode") || !doc.IsObject())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("获取位置信息失败"));
			return;
		}

		log("%s", data.c_str());
		if (JSON_RESOVE(doc, "content"))
		{
			//纬度
			float latitude = atof(doc["content"]["point"]["x"].GetString());
			//经度
			float longtitude = atof(doc["content"]["point"]["y"].GetString());

			//详细地址
			std::string addr = doc["content"]["address"].GetString();
			//省份
			std::string province = doc["content"]["address_detail"]["province"].GetString();
			//城市
			std::string city = doc["content"]["address_detail"]["city"].GetString();

			if (onGetLocationIP)
			{
				onGetLocationIP(true, latitude, longtitude, addr);
				onGetLocationIP = nullptr;
			}
		}
	}

	void GameLocation::getAddrResult(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (JSON_RESOVE(doc, "errcode") || !doc.IsObject())
		{
			//GamePromptLayer::create()->showPrompt(GBKToUtf8("获取位置信息失败"));
			return;
		}

		if (doc["status"].GetInt() != 0)
		{
			if (onGetAddrByLocation)
			{
				//onGetAddrByLocation(false, GBKToUtf8("未知区域"), "1", "1");
				onGetAddrByLocation = nullptr;
			}
		}

		log("%s", data.c_str());
		if (JSON_RESOVE(doc, "result"))
		{
			//详细地址
			std::string addr = doc["result"]["ad_info"]["name"].GetString();
			//省份
			std::string province = doc["result"]["ad_info"]["province"].GetString();
			//城市
			std::string city = doc["result"]["ad_info"]["city"].GetString();

			if (onGetAddrByLocation)
			{
				onGetAddrByLocation(true, addr, province, city);
				onGetAddrByLocation = nullptr;
			}
		}
	}

	void GameLocation::getDistanceResult(const std::string& data)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());

		if (JSON_RESOVE(doc, "errcode") || !doc.IsObject())
		{
			//GamePromptLayer::create()->showPrompt(GBKToUtf8("获取位置信息失败"));
			return;
		}

		if (doc["status"].GetInt() != 0)
		{
			if (onGetLocationDistance)
			{
				//onGetAddrByLocation(false, GBKToUtf8("未知区域"), "1", "1");
				onGetLocationDistance = nullptr;
			}
		}
		
		if (JSON_RESOVE(doc, "result"))
		{
			rapidjson::Value& result = doc["result"];
			double Distance1 = 0;
			double Distance2 = 0;
			double Distance3 = 0;
			for (size_t i = 0; i < result["elements"].Size(); i++)
			{
				rapidjson::Value& Content = result["elements"][i];
				if (i == 0)
				{
					Distance1 = Content["distance"].GetDouble();
				}
				if (i == 1)
				{
					Distance2 = Content["distance"].GetDouble();
				}
				if (i == 2)
				{
					Distance3 = Content["distance"].GetDouble();
				}
			}
			if (onGetLocationDistance)
			{
				onGetLocationDistance(true, Distance1, Distance2, Distance3);
				onGetLocationDistance = nullptr;
			}
		}
		else
		{
			if (onGetLocationDistance)
			{
				onGetLocationDistance(false, 0, 0, 0);
				onGetLocationDistance = nullptr;
			}
		}
	}
}
