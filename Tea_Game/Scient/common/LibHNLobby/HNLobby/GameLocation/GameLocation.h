/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HNGameLocation_h__
#define __HNGameLocation_h__

#include "HNHttp/HNHttp.h"
#include "cocos2d.h"

USING_NS_CC;

namespace HN
{
	// 客户项目需要重新注册百度地图账号，申请应用秘钥
    static const std::string baiduApiKey         = "FAIXZwPvzVj70RaqtjOpzUKkCZ3xdxRR";
    
	class GameLocation : public HNHttpDelegate
	{
	public:
		typedef std::function<void(bool success, float latitude, float longtitude,
        const std::string& addr, const std::string& country, const std::string& province,
        const std::string& city, const std::string& district, const std::string& street)> LocationCallBack;

		//获取定位详细信息（包含国家，省，市，区，街道详细信息）
		LocationCallBack onGetLocationCallBack = nullptr;

		typedef std::function<void(bool success, float latitude, float longtitude,
			const std::string& addr)> LocationCallBackLite;

		// 获取定位简化信息（只有经纬度和一个整体位置信息）
		LocationCallBackLite onGetLocationCallBackLite = nullptr;

		LocationCallBackLite onGetLocationIP = nullptr;

		typedef std::function<void(bool success, float Distance1, float Distance2, float Distance3)> LocationDistanceCallBack;

		LocationDistanceCallBack onGetLocationDistance = nullptr;

		typedef std::function<void(bool success, std::string& addr, std::string& province, std::string& city)> LocationCallBackAddr;

		LocationCallBackAddr onGetAddrByLocation = nullptr;

	public:
		static GameLocation* getInstance();

		// 销毁单例
		static void destroyInstance();

	public:
		GameLocation();

		virtual ~GameLocation();
        
    public:
        // 获取定位信息
        void getLocation();

		// 通过ip地址获取位置信息
		void getLocation(std::string& ip);

		// 通过经纬度获取地理位置信息
		void getAddrByLocation(float X, float Y);

		// 通过经纬度获取地理位置距离
		void getDistanceByLocation(float fromX, float fromY, float toX1, float toY1, float toX2, float toY2, float toX3, float toY3);
        
    private:
        virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;
        
        // 获取定位信息结果
        void getLocationResult(const std::string& data);

		// IP获取定位信息结果
		void getLocationIPResult(const std::string& data);

		// 获取地理位置信息结果
		void getAddrResult(const std::string& data);

		// 获取地理位置信息结果
		void getDistanceResult(const std::string& data);
	};
}

#endif // __HNGameLocation_h__
