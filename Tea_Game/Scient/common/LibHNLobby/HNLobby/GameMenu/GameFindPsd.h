/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef GameFindPsd_h__
#define GameFindPsd_h__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "HNLogicExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "ui/UICheckBox.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class GameFindPsd : public HNLayer, public ui::EditBoxDelegate, public HN::IHNPlatformRegist
{

public:
	typedef std::function<void (std::string uName, std::string pWord)> CloseCallBack;
	CloseCallBack onCloseCallBack = nullptr;	

public:
	GameFindPsd(std::string userName);
	virtual ~GameFindPsd();

	virtual bool init() override;

	static GameFindPsd* create(std::string userName);

private:
	void verifyCodeClickCallBack(Ref* pSender);

	void changePsdClickCallback(Ref* pSender);

	bool resetPassWordSelector(HNSocketMessage* socketMessage);

private:
	//获取验证码的时间限制
	void setLimitTime();

	//倒计时
	void updateLimitTime(float dt);

	virtual void editBoxReturn(ui::EditBox* editBox) {};

private:
	void closeFunc() override;

private:
	// 获取验证码结果
	virtual void onPlatformGetVerifyCode(bool success, const std::string& phone,
		const std::string& code, const std::string& message) override;

private:
	HNEditBox*	_editBoxNewPsd = nullptr;
	HNEditBox*	_editBoxSure = nullptr;
	HNEditBox*	_editBoxPhone = nullptr;
	HNEditBox*	_editBoxCode = nullptr;
	Button*		_btn_getCode = nullptr;
	std::string _userName = "";
	std::string _passWord = "";
	Text*		_timeText = nullptr;
	int			_restTime = 60;//倒计时时间
	std::string	_verifyCode = "";

	HNPlatformRegist*	_gameRegist = nullptr;
};

#endif // GameFindPsd_h__
