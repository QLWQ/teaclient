/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef GameRegisterLayer_h__
#define GameRegisterLayer_h__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "ui/UICheckBox.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;
using namespace HN;

class RegisterLayer : public HNLayer, public ui::EditBoxDelegate
{

public:
	typedef std::function<void (const std::string& name, const std::string& psd)> RegisterCallBack;
	RegisterCallBack onRegisterCallBack = nullptr;

	typedef std::function<void ()> CloseCallBack;
	CloseCallBack onCloseCallBack = nullptr;

private:
	HNEditBox*	_editBoxUserName = nullptr;
	HNEditBox*	_editBoxPassWord = nullptr;
	CheckBox*   _checkBox_agree = nullptr;
	CheckBox*   _checkBox_showPsd = nullptr;

public:
	CREATE_FUNC(RegisterLayer);

public:
	RegisterLayer();
	virtual ~RegisterLayer();

	virtual bool init() override;

private:
	void closeFunc() override;

private:
	void registerClickCallback(Ref* pSender);

	virtual void editBoxReturn(ui::EditBox* editBox) {};
};

#endif // GameRegisterLayer_h__
