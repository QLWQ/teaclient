/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRegisterLayer.h"
#include "HNMarketExport.h"

static const char* REGISTERUI_PATH = "platform/LoginorRegistUi/regist/registUI.csb";


RegisterLayer::RegisterLayer()
{	
	
}

RegisterLayer::~RegisterLayer()
{	

}

void RegisterLayer::closeFunc()
{
	if (nullptr != onCloseCallBack)
	{
		onCloseCallBack();
	}
}

bool RegisterLayer::init()
{
	if (!HNLayer::init()) return false;
	
	auto node = CSLoader::createNode(REGISTERUI_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto imgBG = dynamic_cast<ImageView*>(node->getChildByName("Image_BG"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		imgBG->setScale(0.9f);
	}

	// 关闭按钮
	auto btn_close = dynamic_cast<Button*>(imgBG->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*) {
		close(ACTION_TYPE_LAYER::FADE);
	});

	// 账号输入框
	auto textField_UserName = (TextField*)imgBG->getChildByName("TextField_userName");
	textField_UserName->setVisible(false);
	_editBoxUserName = HNEditBox::createEditBox(textField_UserName, this);
	_editBoxUserName->setCascadeOpacityEnabled(true);

	// 密码输入框
	auto textField_PassWord = (TextField*)imgBG->getChildByName("TextField_passWord");
	textField_PassWord->setVisible(false);
	_editBoxPassWord = HNEditBox::createEditBox(textField_PassWord, this);
	_editBoxPassWord->setPasswordEnabled(true);
	_editBoxPassWord->setCascadeOpacityEnabled(true);

	// 同意复选框
	_checkBox_agree = (CheckBox*)imgBG->getChildByName("CheckBox_agree");
	_checkBox_agree->setSelected(false);

	// 显示密码复选框
	_checkBox_showPsd = (CheckBox*)imgBG->getChildByName("CheckBox_showPass");
	_checkBox_showPsd->setSelected(false);
	_checkBox_showPsd->addClickEventListener([=](Ref*){

		_editBoxPassWord->setPasswordEnabled(!_editBoxPassWord->isPasswordEnabled());
		_editBoxPassWord->setString(_editBoxPassWord->getString());
	});

	// 注册按钮
	Button * btn_register = (Button*)imgBG->getChildByName("Button_registOK");
	btn_register->addClickEventListener(CC_CALLBACK_1(RegisterLayer::registerClickCallback, this));

	// 重置按钮
	Button * btn_Reset = (Button*)imgBG->getChildByName("Button_reset");
	btn_Reset->addClickEventListener([=](Ref*){

		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

		_editBoxUserName->setText("");
		_editBoxPassWord->setText("");
	});
	
	//协议按钮
	Button * btn_agree = (Button*)imgBG->getChildByName("Button_agree");
	btn_agree->addClickEventListener([=](Ref*){

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		auto webViewLayer = GameWebViewLayer::create();
		webViewLayer->setTitle(GBKToUtf8("用户协议"));
		webViewLayer->showWebView(PlatformConfig::getInstance()->getProtocolUrl());
#endif
	});

	return true;
}

void RegisterLayer::registerClickCallback(Ref* pSender)
{
	if (!_checkBox_agree->isSelected())
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("请勾选同意用户协议！"));
		return;
	}	

	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	do 
	{
		// 获取输入框账号
		std::string userName = _editBoxUserName->getString();

		if (userName.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("账号不能为空！"));
			break;
		}	

		if (_editBoxUserName->getStringLength() < 6 ||
			_editBoxUserName->getStringLength() > 12 ||
			!Tools::verifyNumberAndEnglish(userName))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入6-12位英文或数字帐号！"));
			_editBoxUserName->setString("");
			break;
		}

		// 获取输入框密码
		std::string passWord = _editBoxPassWord->getString();
		if (passWord.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("密码不能为空！"));
			break;
		}

		if (_editBoxPassWord->getStringLength() < 6 || 
			_editBoxPassWord->getStringLength() > 16 ||
			!Tools::verifyNumberAndEnglish(passWord))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入6-16位英文或数字密码！"));
			_editBoxPassWord->setString("");
			break;
		}

		if (nullptr != onRegisterCallBack)
		{
			onRegisterCallBack(userName, passWord);
		}

	} while (0);
}

