/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GAMELAND_LAYER_H__
#define __GAMELAND_LAYER_H__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class LandLayer : public HNLayer, public ui::EditBoxDelegate
{
public:
	typedef std::function<void (const std::string& name, const std::string& psd)> LoginCallBack;
	LoginCallBack	onLoginCallBack = nullptr;

	typedef std::function<void ()> CloseCallBack;
	CloseCallBack	onCloseCallBack = nullptr;

	typedef std::function<void ()> RegistCallBack;
	RegistCallBack  onRegistCallBack = nullptr;

	typedef std::function<void(std::string userName)> FindPsdCallBack;
	FindPsdCallBack  onFindCallBack = nullptr;

private:
	HNEditBox*	_editBoxUserName = nullptr;
	HNEditBox*	_editBoxPassWord = nullptr;
	CheckBox*	_checkBoxSave	= nullptr;

	bool _isGetPassWord = false;
	bool  _isOpenLayer = false;

public:
	CREATE_FUNC(LandLayer);

public:
	LandLayer();
	virtual ~LandLayer();

	virtual bool init() override;

private:
	// ��½�ص�����
	void loginClickCallback(Ref* pSender);

	virtual void editBoxReturn(ui::EditBox* editBox) {};

	void editBoxTextChanged(ui::EditBox* editBox, const std::string& text);

	void editBoxEditingDidBegin(ui::EditBox* editBox);

	void closeFunc() override;
};

#endif // __GAMELAND_LAYER_H__