/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/


#include "GameMenu.h"
#include "GameLandLayer.h"
#include "GameFindPsd.h"
#include "GameRegisterLayer.h"
#include "HNUIExport.h"
#include "HNMarketExport.h"
#include "HNOpenExport.h"
#include "../GameBaseLayer/GamePlatform.h"
#include "../GameChildLayer/GameExitLayer.h"
#include "../GameReconnection/Reconnection.h"
#include "../GameUpdate/HNUpdate.h"

using namespace HN;

static const int   CHILD_TAG_LAND				= 100;
static const int   CHILD_TAG_REGISTER			= 101;
static const int   CHILD_TAG_FIND				= 102;

#define Word_Loading		"正在登陆..."
#define Word_Register		"正在注册..."
#define Word_Empty_Name		"账号不能为空!"
#define Word_Empty_Pass		"密码不能为空!"
#define Word_Wrong_Name		"登陆名字错误!"
#define Word_Wrong_Pass		"用户密码错误!"
#define Word_Logined		"帐号已经登录!"
#define Word_Empty_Name		GBKToUtf8("账号不能为空!")
#define Word_Empty_Pass		GBKToUtf8("密码不能为空!")

static const char* GUEST_LOGIN_DEFAULT_PASSWORD = "123456";	

static const char* MENU_BG						= "platform/LoginorRegistUi/denglubeijing.png";
static const char* XIEYI_BG						= "platform/LoginorRegistUi/Login/Agreement.csb";
static const char* TTF_PATH = "platform/common/Mini.ttf";
static const char* GUESTLOGIN_MENU_N			= "platform/LoginorRegistUi/youkedenglu.png";

//static const char* REGISTER_MENU_N				= "platform/LoginorRegistUi/zhanghaodenglu.png";

static const char* ACCOUNTLOGIN_MENU_N			= "platform/LoginorRegistUi/zhanghaodenglu.png";

static const char* WEIXINLOGIN_MENU_N			= "platform/LoginorRegistUi/wxdl.png";

static const int CHILD_LAYER_EXIT_TAG		    = 1000;			// 退出层标签

static const char* LOGIN_BG						= "platform/LoginorRegistUi/Login/WechatLogin.csb";
void GameMenu::createMenu()
{
	Scene* scene = Scene::create();
	auto mainlayer = GameMenu::create();
	scene->addChild(mainlayer);
	//本场景变暗消失后另一场景慢慢出现
	Director::getInstance()->replaceScene(TransitionFade::create(0.3f, scene));

	PlatformConfig::getInstance()->setSceneState(PlatformConfig::SCENE_STATE::OTHER);
}

GameMenu::GameMenu() 
{
	_gameRegist = new HNPlatformRegist(this);
	_gameLogin = new HNPlatformLogin(this);
	
}

GameMenu::~GameMenu()
{
	_gameRegist->stop();
	_gameLogin->stop();
	HN_SAFE_DELETE(_gameRegist);
	HN_SAFE_DELETE(_gameLogin);
}

void GameMenu::checkUpdate()
{
// 	HNUpdate* update = HNUpdate::create();
// 	if (update != nullptr)
// 	{
// 		this->addChild(update, 100);
// 		update->onFinishCallback = std::bind(&GameMenu::updateCheckFinishCallback, this,
// 			std::placeholders::_1,
// 			std::placeholders::_2,
// 			std::placeholders::_3);
// 		update->checkUpdate();
// 		update->setName("update");
// 	}
}

void GameMenu::updateCheckFinishCallback(bool updated, const std::string& message, const std::string& installPath)
{
	HNUpdate* update = (HNUpdate*)this->getChildByName("update");
	if(updated)
	{		
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		HN::Operator::requestChannel("sysmodule", "installApp", installPath);
		if (update) update->removeFromParent();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		Application::getInstance()->openURL(installPath);//HNPlatformConfig()->getOnlineInstallUrl_iOS(installPath));
		if (update && !update->getMustUpdate())
		{
			update->removeFromParent();
		}
#else
		if (update) update->removeFromParent();
#endif
		/*bool Auto_Login = UserDefault::getInstance()->getBoolForKey(AUTO_LOGIN, false);
		if (Auto_Login == true)
		{
			onWeChatLoginClicked(nullptr);
		}*/
	}
	else
	{
		if (!message.empty())
		{
			std::string str(GBKToUtf8("更新失败！"));
			str.append(message);
			GamePromptLayer::create()->showPrompt(str);
			if (update) update->removeFromParent();
		}
		else
		{
			bool Auto_Login = UserDefault::getInstance()->getBoolForKey(AUTO_LOGIN, false);
			if (Auto_Login == true)
			{
				onWeChatLoginClicked(nullptr);
			}
		}
	}
}

void GameMenu::plantSeed()
{
	timeval tv;
	gettimeofday(&tv, NULL);
	unsigned long reed = tv.tv_sec * 1000 + tv.tv_usec / 1000;

	//srand()中传入一个随机数种子
	srand(reed);
}

bool GameMenu::init()
{
	if (!HNLayer::init()) return false;	

	plantSeed();
	enableKeyboard();
	//setBackGroundImage(MENU_BG);

	
	Size winSize = Director::getInstance()->getWinSize();
	_login_bg = CSLoader::createNode(LOGIN_BG);
	_login_bg->setPosition(winSize / 2);
	/*auto ation = CSLoader::createTimeline(LOGIN_BG);
	_login_bg->runAction(ation);
	ation->gotoFrameAndPlay(0, true);*/
	this->addChild(_login_bg);
	Panel_bg = dynamic_cast<Layout*>(_login_bg->getChildByName("Panel_bg"));
	CheckBox_xieyi = dynamic_cast<CheckBox*>(_login_bg->getChildByName("CheckBox_xieyi"));
	Image_Tips = dynamic_cast<ImageView*>(_login_bg->getChildByName("Image_Tips"));
	auto btn_define = dynamic_cast<Button*>(Image_Tips->getChildByName("Button_define"));
	btn_define->addClickEventListener([=](Ref*){
		Image_Tips->setVisible(false);
		Panel_bg->setVisible(false);
	});
	Panel_bg->addClickEventListener([=](Ref*){
		Image_Tips->setVisible(false);
		Panel_bg->setVisible(false);
	});
	Image_Tips->setZOrder(2);
	//
	auto Image_xieyi = dynamic_cast<ImageView*>(_login_bg->getChildByName("Image_xieyi"));
	Image_xieyi->setTouchEnabled(true);
	Image_xieyi->addClickEventListener([=](Ref*){
		//屏蔽用户协议显示
		/*auto _xieyi = CSLoader::createNode(XIEYI_BG);
		_xieyi->setPosition(Vec2(-640, -360));
		_login_bg->addChild(_xieyi, 2);
		auto Panel_bg = dynamic_cast<Layout*>(_xieyi->getChildByName("Panel_bg"));
		Panel_bg->addClickEventListener([=](Ref*){_xieyi->removeFromParent(); });
		auto btn_close = dynamic_cast<Button*>(_xieyi->getChildByName("Button_close"));
		btn_close->addClickEventListener([=](Ref*){_xieyi->removeFromParent(); });*/
	});

	//// 账号输入框
	//auto TextField_userName = (TextField*)_login_bg->getChildByName("TextField_userName");
	//TextField_userName->setVisible(false);
	//_editBoxUserName = HNEditBox::createEditBox(TextField_userName, this);
	//_editBoxUserName->setString(UserDefault::getInstance()->getStringForKey(USERNAME_TEXT));
	//_editBoxUserName->setCascadeOpacityEnabled(true);

	//// 密码输入框
	//auto textField_PassWord = (TextField*)_login_bg->getChildByName("TextField_passWord");
	//textField_PassWord->setVisible(false);
	//_editBoxPassWord = HNEditBox::createEditBox(textField_PassWord, this);
	//_editBoxPassWord->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
	//_editBoxPassWord->setCascadeOpacityEnabled(true);
	//if (UserDefault::getInstance()->getBoolForKey(SAVE_TEXT))
	//{
	//	_editBoxPassWord->setString("******");
	//	_isGetPassWord = true;
	//}

	std::string gameLogo = PlatformConfig::getInstance()->getGameLogo();
	if (FileUtils::getInstance()->isFileExist(gameLogo))
	{
		_logo = Sprite::create(gameLogo);
		_logo->setPosition(Vec2(_winSize.width / 2, _winSize.height * 0.55f));
		addChild(_logo, 2);
	}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	//微信登陆
	_buttonWeiXinLogin = Button::create(WEIXINLOGIN_MENU_N);
	_buttonWeiXinLogin->setPosition(Vec2(_winSize.width / 2, _winSize.height * 0.25f));
	_buttonWeiXinLogin->addClickEventListener(CC_CALLBACK_1(GameMenu::onWeChatLoginClicked, this));
	addChild(_buttonWeiXinLogin);
	_buttonWeiXinLogin->setVisible(false);
#endif

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	 	// 游客登录按钮
	 	_buttonGuestLogin = Button::create(GUESTLOGIN_MENU_N);
	 	_buttonGuestLogin->setPosition(Vec2(-400, -200));
		_buttonGuestLogin->setScale(0.1);
	 	_buttonGuestLogin->addClickEventListener(CC_CALLBACK_1(GameMenu::guestLoginEventCallback, this));
		_login_bg->addChild(_buttonGuestLogin, 1);
		_buttonGuestLogin->runAction(Spawn::create(RotateBy::create(0.5, 720), ScaleTo::create(0.5, 1), nullptr));

	 	// 账号登录按钮
	 	_buttonAccountLogin = Button::create(ACCOUNTLOGIN_MENU_N);
	 	_buttonAccountLogin->setPosition(Vec2(-100, -200));
		_buttonAccountLogin->setScale(0.1);
	 	_buttonAccountLogin->addClickEventListener(CC_CALLBACK_1(GameMenu::accountLoginEventCallback, this));
		_login_bg->addChild(_buttonAccountLogin, 1);
		_buttonAccountLogin->runAction(Spawn::create(RotateBy::create(0.5, 720), ScaleTo::create(0.5, 1), nullptr));

		//UserDefault::getInstance()->setBoolForKey(AUTO_LOGIN, true);
//#endif
	//账号注册
	//  _buttonRegister = (Button*)_login_bg->getChildByName("Button_reg");
	//	_buttonRegister->addClickEventListener(CC_CALLBACK_1(GameMenu::RegisterEventCallback, this));
	// 	_buttonRegister->addClickEventListener([=](Ref*){
	// 		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_CLOSE);
	//		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
	// 			if (onRegistCallBack) onRegistCallBack();
	// 			this->removeFromParent();
	// 		}), nullptr));
	//  	});

// 	// 保存密码复选框
// 	_checkBoxSave = (CheckBox*)_login_bg->getChildByName("CheckBox_save");
// 	_checkBoxSave->setSelected(UserDefault::getInstance()->getBoolForKey(SAVE_TEXT));
// 	_checkBoxSave->addClickEventListener([=](Ref*){
// 
// 		if (!_checkBoxSave->isSelected())
// 		{
// 			UserDefault::getInstance()->setStringForKey(PASSWORD_TEXT, "");
// 		}
// 		UserDefault::getInstance()->setBoolForKey(SAVE_TEXT, _checkBoxSave->isSelected());
// 		UserDefault::getInstance()->flush();
// 	});

	// 游戏版本
	std::string version = Operator::requestChannel("sysmodule", "getVersion");
	auto gameVersion = Label::createWithTTF(StringUtils::format(GBKToUtf8("版本：%s"), version.c_str()), TTF_PATH, 18);
	gameVersion->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	gameVersion->setPosition(Vec2(_winSize.width - (gameVersion->getContentSize().width +10), 10.f));
	addChild(gameVersion, 2);

	// 预存储游戏配置
	auto userDefault = UserDefault::getInstance();
	int music = userDefault->getIntegerForKey(MUSIC_VALUE_TEXT, -1);
	int sound = userDefault->getIntegerForKey(SOUND_VALUE_TEXT, -1);

	if (INVALID_MUSIC_VALUE == music)
	{
		music = 60;
		userDefault->setIntegerForKey(MUSIC_VALUE_TEXT, 60);
	}
	if (INVALID_SOUND_VALUE == sound)
	{
		sound = 50;
		userDefault->setIntegerForKey(SOUND_VALUE_TEXT, 50);
	}

	// 播放背景音乐
	float musicValue = music / 100.f;
	float effectValue = sound / 100.f;
	if (musicValue <= 0.0f || effectValue <= 0.0f)
	{
		HNAudioEngine::getInstance()->setSwitchOfMusic(false);
	}
	HNAudioEngine::getInstance()->setBackgroundMusicVolume(musicValue);
	HNAudioEngine::getInstance()->setEffectsVolume(effectValue);
	return true;
}

void GameMenu::onEnterTransitionDidFinish()
{
	HNLayer::onEnterTransitionDidFinish();

	releaseUnusedRes();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS/* || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32*/)
	this->checkUpdate();
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    cmdAutoLogin();
#endif
}

// 手机返回键监听回调函数
void GameMenu::onKeyReleased(EventKeyboard::KeyCode keyCode,Event * pEvent)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if (EventKeyboard::KeyCode::KEY_BACK == keyCode)
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (EventKeyboard::KeyCode::KEY_F1 == keyCode)
#endif
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		do 
		{
			HNUpdate* update = (HNUpdate*)this->getChildByName("update");
			if (update != nullptr)
			{
				return;
			}

			auto registerLayer = (RegisterLayer*)this->getChildByTag(CHILD_TAG_REGISTER);
			if (nullptr != registerLayer)
			{
				registerLayer->close(ACTION_TYPE_LAYER::FADE);
				break;
			}

			auto landLayer = (LandLayer*)this->getChildByTag(CHILD_TAG_LAND);
			if (nullptr != landLayer)
			{
				landLayer->close(ACTION_TYPE_LAYER::FADE);
				break;
			}

			auto findLayer = (GameFindPsd*)this->getChildByTag(CHILD_TAG_FIND);
			if (nullptr != findLayer)
			{
				findLayer->close(ACTION_TYPE_LAYER::FADE);
				break;
			}

			// 调用离开游戏层
			/*GameExitLayer* exitLayer = dynamic_cast<GameExitLayer*>(this->getChildByTag(CHILD_LAYER_EXIT_TAG));
			if (nullptr == exitLayer)
			{
				auto exitLayer = GameExitLayer::create();
				exitLayer->showExitLayer(this, 100000000, CHILD_LAYER_EXIT_TAG);
			}
			else
			{
				exitLayer->close();
			}*/
		} while (0);
	}
}

//游客登陆回调
void GameMenu::guestLoginEventCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	HNUpdate* update = (HNUpdate*)this->getChildByName("update");
	if (update && update->getMustUpdate() && update->hasNewVersion())
	{
		update->needUpdate();
		return;
	}
#endif

	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("获取游客账号"), 25);

	//游客登陆
	_gameRegist->start();
	_gameRegist->requestRegist(ThirdLoginType::Guest);
}

// 账号登陆回调
void GameMenu::accountLoginEventCallback(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	HNUpdate* update = (HNUpdate*)this->getChildByName("update");
	if (update && update->getMustUpdate() && update->hasNewVersion())
	{
		update->needUpdate();
		return;
	}
#endif

	
	LandLayer* landLayer = LandLayer::create();
	landLayer->onLoginCallBack = [this](const std::string& name, const std::string& psd)
	{
		_userName = name;
		_userPsd = psd;

		//账号登陆
		enterGame(_userName, _userPsd, ThirdLoginType::Account);
	};

	landLayer->onCloseCallBack = [this]()
	{

	};

	//注册按钮接口
	landLayer->onRegistCallBack = [this]()
	{
		openRegisterLayer();
	};

	//找回密码按钮接口
	landLayer->onFindCallBack = [this](std::string userName)
	{
		openFindPsdLayer(userName);
	};

	landLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 3, CHILD_TAG_LAND);


// 	do
// 	{
// 		// 获取输入框账号
// 		std::string userName = _editBoxUserName->getString();
// 		if (userName.empty())
// 		{
// 			GamePromptLayer::create()->showPrompt(Word_Empty_Name);
// 			break;
// 		}
// 
// 		// 获取输入框密码
// 		std::string passWord = _editBoxPassWord->getString();
// 		if (passWord.empty())
// 		{
// 			GamePromptLayer::create()->showPrompt(Word_Empty_Pass);
// 			break;
// 		}
// 
// 		if (nullptr != onLoginCallBack)
// 		{
// 			if (_isGetPassWord)
// 			{
// 				onLoginCallBack(userName, UserDefault::getInstance()->getStringForKey(PASSWORD_TEXT));
// 			}
// 			else
// 			{
// 				passWord = MD5_CTX::MD5String(passWord);
// 				onLoginCallBack(userName, passWord);
// 			}
// 		}
// 	} while (0);
	
}

// 账号注册按钮回调函数
void GameMenu::RegisterEventCallback(Ref* pSender)
{	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	HNUpdate* update = (HNUpdate*)this->getChildByName("update");
	if (update && update->getMustUpdate() && update->hasNewVersion())
	{
		update->needUpdate();
		return;
	}
#endif

	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
	openRegisterLayer();
	
}

// 微信登陆按钮回调函数
void GameMenu::onWeChatLoginClicked(Ref* pSender)
{
#if (CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID && CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
	GamePromptLayer::create()->showPrompt(GBKToUtf8("仅支持android和ios"));
	return;
#endif

	UserDefault::getInstance()->setBoolForKey(AUTO_LOGIN, true);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	HNUpdate* update = (HNUpdate*)this->getChildByName("update");
	if (update && update->getMustUpdate() && update->hasNewVersion())
	{
		update->needUpdate();
		return;
	}
#endif
	//判定是否选中协议
	if (CheckBox_xieyi->isSelected())
	{
		UMengSocial::getInstance()->onAuthorizeCallBack = [=](bool success, ThirdPartyLoginInfo* info, const std::string& errorMsg) {

			if (success)
			{
				log("isSuccess");
				_gameRegist->start();
				_gameRegist->requestRegist(ThirdLoginType::WeChat, "", "", "", Utf8ToGB(info->nickName), info->unionid, info->headUrl, info->bBoy);

				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在登录，请稍候..."), 25);
			}
			else
			{
				LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
				GamePromptLayer::create()->showPrompt(GBKToUtf8(errorMsg.c_str()));
				
			}
		};

		UMengSocial::getInstance()->doThirdLogin(WEIXIN);
	}
	else
	{
		Panel_bg->setVisible(true);
		Image_Tips->setVisible(true);
	}

}

// QQ登陆按钮回调
void GameMenu::onQQLoginClicked(Ref* pSender)
{
#if (CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID && CC_TARGET_PLATFORM != CC_PLATFORM_IOS)
	GamePromptLayer::create()->showPrompt(GBKToUtf8("仅支持android和ios"));
	return;
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	HNUpdate* update = (HNUpdate*)this->getChildByName("update");
	if (update && update->getMustUpdate() && update->hasNewVersion())
	{
		update->needUpdate();
		return;
	}
#endif

	UMengSocial::getInstance()->onAuthorizeCallBack = [=](bool success, ThirdPartyLoginInfo* info, const std::string& errorMsg) {

		if (success)
		{
			log("isSuccess");
			//_gameRegist->start();
			//_gameRegist->requestRegist(ThirdLoginType::QQLogin, "", "", "", Utf8ToGB(info->nickName.c_str()), info->unionid, info->bBoy);

			//LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在登录，请稍候..."), 25);
		}
		else
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			GamePromptLayer::create()->showPrompt(GBKToUtf8(errorMsg.c_str()));
		}
	};

	UMengSocial::getInstance()->doThirdLogin(QQ);
}

//注册按钮回调
void GameMenu::openRegisterLayer()
{
	auto winSize = Director::getInstance()->getWinSize();
	RegisterLayer* registerLayer = RegisterLayer::create();
	registerLayer->onRegisterCallBack = [this](const std::string& name, const std::string& psd)
	{
		_userName = name;
		_userPsd = psd;

		//注册
		_gameRegist->start();
		_gameRegist->requestRegist(ThirdLoginType::Account, _userName, MD5_CTX::MD5String(_userPsd));
		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(Word_Register), 25);
	};

	registerLayer->onCloseCallBack = [this]()
	{		
	};

	registerLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 3, CHILD_TAG_REGISTER);
}

void GameMenu::openFindPsdLayer(std::string userName)
{
	auto winSize = Director::getInstance()->getWinSize();
	GameFindPsd* findLayer = GameFindPsd::create(userName);
	findLayer->onCloseCallBack = [this](const std::string& name, const std::string& psd)
	{
		if (!name.empty() && !psd.empty())
		{
			_userName = name;
			_userPsd = psd;

			//账号登陆
			enterGame(_userName, _userPsd, ThirdLoginType::Account);
		}	
	};

	findLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 3, CHILD_TAG_FIND);
}

void GameMenu::onPlatformRegistCallback(bool success, ThirdLoginType registType, const std::string& message,
	const std::string&name, const std::string& pwd)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	_gameRegist->stop();

	if (success)
	{	
		if (ThirdLoginType::Account == registType)
		{
			UserDefault::getInstance()->setBoolForKey(SAVE_TEXT, true);
			std::string str = StringUtils::format(GBKToUtf8("请牢记你的用户名以及密码。\n用户名称: %s\n用户密码: %s"), name.c_str(), _userPsd.c_str());
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(str);
			prompt->setCallBack(CC_CALLBACK_0(GameMenu::enterGame, this, name, pwd, registType));
		}
		else
		{
			enterGame(name, pwd, registType);
		}
	}
	else
	{
		PlatformLogic()->close();
		GamePromptLayer::create()->showPrompt(message);
	}
}

void GameMenu::onPlatformLoginCallback(bool success, const std::string& message,
							 const std::string& name, const std::string& pwd)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	_gameLogin->stop();

	auto userDefault = UserDefault::getInstance();
	if (success)
	{
		if (Account == _loginType)
		{
			saveUserInfo(name, pwd);
		}

		//调用断线重连逻辑检测是否存在断线状态
		Reconnection::getInstance()->saveInfoAndCheckReconnection(name, pwd);
	}
	else
	{
		PlatformLogic()->close();
		GamePromptLayer::create()->showPrompt(message);
	}
}

void GameMenu::saveUserInfo(const std::string& userName, const std::string& userPswd)
{
	auto userDefault = UserDefault::getInstance();

	if (userDefault->getBoolForKey(SAVE_TEXT))
	{
		if (!userName.empty())
		{
			userDefault->setStringForKey(USERNAME_TEXT, userName);
		}

		if (!userPswd.empty())
		{
			userDefault->setStringForKey(PASSWORD_TEXT, userPswd);
		}
	}
	else
	{
		userDefault->setStringForKey(USERNAME_TEXT, "");
		userDefault->setStringForKey(PASSWORD_TEXT, "");
	}
	userDefault->flush();
}

void GameMenu::enterGame(const std::string& userName, const std::string& userPswd, ThirdLoginType loginType)
{
	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在登录，请稍候..."), 25);
	_loginType = loginType;
	_gameLogin->start();
	_gameLogin->requestLogin(userName, userPswd, loginType);
}


#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
void GameMenu::cmdAutoLogin()
{
    int argc = 0;
    LPWSTR *argv = ::CommandLineToArgvW(::GetCommandLineW(), &argc);
    if (argc > 1)
    {
        _userName = converLPSTRToStr(argv[1]);
        _userPsd = converLPSTRToStr(argv[2]);
        string passWord = MD5_CTX::MD5String(_userPsd);
        //账号登陆
        enterGame(_userName, passWord, ThirdLoginType::Account);
    }
    LocalFree(argv);
}

std::string GameMenu::converLPSTRToStr(LPWSTR lpcwszStr)
{
    std::string str;
    DWORD dwMinSize = 0;
    LPSTR lpszStr = NULL;
    dwMinSize = WideCharToMultiByte(CP_OEMCP, NULL, lpcwszStr, -1, NULL, 0, NULL, FALSE);
    if (0 == dwMinSize)
    {
        return "";
    }
    lpszStr = new char[dwMinSize];
    WideCharToMultiByte(CP_OEMCP, NULL, lpcwszStr, -1, lpszStr, dwMinSize, NULL, FALSE);
    str = lpszStr;
    delete[] lpszStr;
    lpszStr = NULL;
    return str;
}



#endif