/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameFindPsd.h"
#include "HNMarketExport.h"

static const char* FINDPSD_PATH = "platform/LoginorRegistUi/Login/findPsd_Node.csb";


GameFindPsd::GameFindPsd(std::string userName)
:_userName(userName)
{	
	_gameRegist = new HNPlatformRegist(this);
}

GameFindPsd::~GameFindPsd()
{	

}

GameFindPsd* GameFindPsd::create(std::string userName)
{
	GameFindPsd *pRet = new GameFindPsd(userName);
	if (pRet && pRet->init())
	{
		pRet->autorelease();
	}
	else
	{
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

void GameFindPsd::closeFunc()
{
	if (nullptr != onCloseCallBack)
	{
		onCloseCallBack(_userName, _passWord);
	}
}

bool GameFindPsd::init()
{
	if (!HNLayer::init()) return false;
	
	auto node = CSLoader::createNode(FINDPSD_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto imgBG = dynamic_cast<ImageView*>(node->getChildByName("Image_BG"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		imgBG->setScale(0.9f);
	}

	// 关闭按钮
	auto btn_close = dynamic_cast<Button*>(imgBG->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*) {
		close(ACTION_TYPE_LAYER::FADE);
	});

	// 密码输入框
	auto textField_newPsd = (TextField*)imgBG->getChildByName("TextField_newPsd");
	textField_newPsd->setVisible(false);
	_editBoxNewPsd = HNEditBox::createEditBox(textField_newPsd, this);

	// 确认密码输入框
	auto textField_sure = (TextField*)imgBG->getChildByName("TextField_sure");
	textField_sure->setVisible(false);
	_editBoxSure = HNEditBox::createEditBox(textField_sure, this);

	// 手机号输入框
	auto textField_phone = (TextField*)imgBG->getChildByName("TextField_phone");
	textField_phone->setVisible(false);
	_editBoxPhone = HNEditBox::createEditBox(textField_phone, this);

	// 验证码输入框
	auto textField_code = (TextField*)imgBG->getChildByName("TextField_code");
	textField_code->setVisible(false);
	_editBoxCode = HNEditBox::createEditBox(textField_code, this);

	// 获取验证码
	_btn_getCode = dynamic_cast<Button*>(imgBG->getChildByName("Button_getCode"));
	_btn_getCode->addClickEventListener(CC_CALLBACK_1(GameFindPsd::verifyCodeClickCallBack, this));

	// 确认按钮
	Button * btn_sure = dynamic_cast<Button*>(imgBG->getChildByName("Button_sure"));
	btn_sure->addClickEventListener(CC_CALLBACK_1(GameFindPsd::changePsdClickCallback, this));

	//倒计时
	_timeText = dynamic_cast<Text*>(_btn_getCode->getChildByName("Text_time"));
	_timeText->setString("60");

	auto limitTime = UserDefault::getInstance()->getStringForKey("LimitStartTime", "noTime");
	//为"noTime"表示现在没有获取验证码
	if (limitTime != "noTime")
	{
		//获取当前时间，用来和保存的时间比较，如果时间间隔小于60s,就让获取验证码按钮不可点击，否则可点击
		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		std::string nowTime = StringUtils::format("%02d%02d%02d%02d", now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
		int nowtime = atoi(nowTime.c_str());
		int oldtime = atoi(limitTime.c_str());
		int intervalTime = nowtime - oldtime;
		if (intervalTime > 0 && intervalTime <= 60)
		{
			//显示当前绑定的手机号
			_editBoxPhone->setString(UserDefault::getInstance()->getStringForKey("Mobilephone", " "));
			_verifyCode = UserDefault::getInstance()->getStringForKey("curVCode", " ");
			//显示倒计时的时间
			_restTime = 60 - intervalTime;
			_timeText->setString(std::to_string(_restTime));
			setLimitTime();
		}
		else
		{
			_editBoxCode->setEnabled(true);
			_editBoxCode->setBright(true);
			_timeText->setVisible(false);
		}
	}
	else
	{
		_timeText->setVisible(false);
	}

	return true;
}

void GameFindPsd::verifyCodeClickCallBack(Ref* pSender)
{
	std::string phoneNumber = _editBoxPhone->getString();
	do
	{
		if (phoneNumber.empty()
			|| _editBoxPhone->getStringLength() != 11
			|| !Tools::verifyNumber(phoneNumber))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的手机号码。")); break;
		}

		_gameRegist->start();
		_gameRegist->getVerifyCode(phoneNumber);

		//保存当前时间，用于关闭这个界面后，再次进入获取验证码按钮的状态
		time_t tt;
		time(&tt);
		struct tm * now;
		now = localtime(&tt);
		std::string startTime = StringUtils::format("%02d%02d%02d%02d", now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);
		UserDefault::getInstance()->setStringForKey("LimitStartTime", startTime);

		_restTime = 60;
		//设置60s内获取按钮不可点
		setLimitTime();
		//暂时保存手机号
		UserDefault::getInstance()->setStringForKey("Mobilephone", phoneNumber);
		UserDefault::getInstance()->flush();

	} while (0);
}

//获取验证码的时间限制
void GameFindPsd::setLimitTime()
{
	this->schedule(schedule_selector(GameFindPsd::updateLimitTime), 1.0f);

	_timeText->setVisible(true);
	_btn_getCode->setEnabled(false);
	_btn_getCode->setBright(false);
}

void GameFindPsd::updateLimitTime(float dt)
{
	char str[64];
	_restTime -= 1;
	if (_restTime < 0)
	{
		this->unschedule(schedule_selector(GameFindPsd::updateLimitTime));
		UserDefault::getInstance()->setStringForKey("LimitStartTime", "noTime");
		UserDefault::getInstance()->setStringForKey("Mobilephone", "noMobilePhone");
		UserDefault::getInstance()->flush();
		_btn_getCode->setEnabled(true);
		_btn_getCode->setBright(true);
		_timeText->setVisible(false);
	}
	else
	{
		_timeText->setString(std::to_string(_restTime));
	}
}

void GameFindPsd::changePsdClickCallback(Ref* pSender)
{	
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);

	do 
	{
		// 获取输入框密码
		std::string passWord1 = _editBoxNewPsd->getString();
		std::string passWord2 = _editBoxSure->getString();
		if (passWord1.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("密码不能为空！"));
			break;
		}

		if (passWord1.compare(passWord2) != 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("两次密码输入不同！"));
			break;
		}

		if (_editBoxNewPsd->getStringLength() < 6 ||
			_editBoxNewPsd->getStringLength() > 16 ||
			!Tools::verifyNumberAndEnglish(passWord1))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入6-16位英文或数字密码！"));
			_editBoxNewPsd->setString("");
			_editBoxSure->setString("");
			break;
		}

		if (_editBoxPhone->getString().empty()
			|| _editBoxPhone->getStringLength() != 11
			|| !Tools::verifyNumber(_editBoxPhone->getString()))
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的手机号码。")); 
			break;
		}

		std::string code = _editBoxCode->getString();

		if (code.empty()
			|| _editBoxCode->getStringLength() != 6
			|| !Tools::verifyNumber(code)
			|| MD5_CTX::MD5String(code).compare(_verifyCode) != 0)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("请输入正确的验证码。"));
			break;
		}

		_passWord = MD5_CTX::MD5String(passWord1);

		MSG_GP_I_ForgetPWD forgetPWD;
		strcpy(forgetPWD.szUserName, _userName.c_str());
		strcpy(forgetPWD.szNewPWD, _passWord.c_str());
		strcpy(forgetPWD.szPhoneNum, _editBoxPhone->getString().c_str());
		PlatformLogic()->sendData(MDM_GP_USERINFO, ASS_GP_USERINFO_FORGET_PWD, &forgetPWD, sizeof(forgetPWD),
			HN_SOCKET_CALLBACK(GameFindPsd::resetPassWordSelector, this));

		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在修改密码"), 25);
	} while (0);
}

void GameFindPsd::onPlatformGetVerifyCode(bool success, const std::string& phone,
	const std::string& code, const std::string& message)
{
	_gameRegist->stop();

	if (success)
	{
		_verifyCode = code;
	}
	else
	{
		PlatformLogic()->close();
		GamePromptLayer::create()->showPrompt(message);
	}	
}

bool GameFindPsd::resetPassWordSelector(HNSocketMessage* socketMessage)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	switch (socketMessage->messageHead.bHandleCode)
	{
	case 0:
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("修改密码成功，请使用新密码登录"));
		prompt->setCallBack([=](){
			close(ACTION_TYPE_LAYER::FADE);
		});
	}break;
	case 1:
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("用户信息错误"));
	}break;
	case 2:
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("用户未绑定手机"));
	}break;
	case 3:
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("手机号不匹配"));
	}break;
	case 4:
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("游客和第三方登录账号不能找回密码"));
	}break;
	default:
		break;
	}

	if (0 != socketMessage->messageHead.bHandleCode)
	{
		_passWord = "";
	}
	
	return true;
}

