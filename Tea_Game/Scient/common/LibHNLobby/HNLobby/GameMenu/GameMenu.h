/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __GameMenu_H__
#define __GameMenu_H__


#include "HNNetExport.h"
#include "HNLogicExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>

USING_NS_CC;
using namespace ui;
using namespace cocostudio;

class GameMenu : public HNLayer, public HN::IHNPlatformLogin, public HN::IHNPlatformRegist, public ui::EditBoxDelegate
{
public:
	typedef std::function<void(const std::string& name, const std::string& psd)> LoginCallBack;
	LoginCallBack	onLoginCallBack = nullptr;

	typedef std::function<void()> RegistCallBack;
	RegistCallBack  onRegistCallBack = nullptr;
	// 进入登陆界面
	static void createMenu();

	// 构造函数
	GameMenu();

	// 析构函数
	virtual ~GameMenu();

	// 更新检查
	void checkUpdate();

	// 种植种子
	void plantSeed();

	// 更新检测
	void updateCheckFinishCallback(bool updated, const std::string& message, const std::string& installPath);

	// 初始化
	virtual bool init() override;

	// 显示完成
	virtual void onEnterTransitionDidFinish() override;

	// 游客登陆按钮回调函数
	void guestLoginEventCallback(Ref* pSender);

	// 注册登陆按钮回调函数
	void accountLoginEventCallback(Ref* pSender);

	// 账号注册按钮回调函数
	void RegisterEventCallback(Ref* pSender);

	// 微信登陆按钮回调函数
	void onWeChatLoginClicked(Ref* pSender);

	// QQ登陆按钮回调
	void onQQLoginClicked(Ref* pSender);

	// 进入游戏
	void enterGame(const std::string& userName, const std::string& userPswd, ThirdLoginType loginType);

	// 创建
	CREATE_FUNC(GameMenu);
	virtual void editBoxReturn(ui::EditBox* editBox) {};
private:
	//打开注册页面
	void openRegisterLayer();

	//打开找回密码页面
	void openFindPsdLayer(std::string userName);

	//保存用户账号
	void saveUserInfo(const std::string& userName, const std::string& userPswd);
	bool _isGetPassWord = false;

public:
	//手机返回键监听回调函数
	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode,Event * pEvent) override;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    void cmdAutoLogin();                 //配置参数自动登录
    std::string converLPSTRToStr(LPWSTR lpcwszStr);//字符转换
#endif

public:
	//注册回调
	virtual void onPlatformRegistCallback(bool success, ThirdLoginType registType, const std::string& message,
		const std::string&name, const std::string& pwd)  override;

	//登陆回调
	virtual void onPlatformLoginCallback(bool success, const std::string& message,
		const std::string& name, const std::string& pwd)  override;

public:
	Node*				_login_bg				= nullptr;
	Layout*				Panel_bg = nullptr;
	ImageView*			Image_Tips = nullptr;
	CheckBox*				CheckBox_xieyi = nullptr;
	HNPlatformLogin*	_gameLogin				= nullptr;
	HNPlatformRegist*	_gameRegist				= nullptr;
	Sprite*				_logo					= nullptr;
	Sprite*				_backGround				= nullptr;
	Button*				_buttonGuestLogin		= nullptr;
	Button*				_buttonAccountLogin		= nullptr;
	Button*				_buttonRegister			= nullptr;
	Button*				_buttonWeiXinLogin		= nullptr;
	CheckBox*			_checkBoxSave			= nullptr;
	ThirdLoginType		_loginType				= Guest;
	std::string			_userName				= "";
	std::string			_userPsd				= "";

	HNEditBox*	_editBoxUserName = nullptr;
	HNEditBox*	_editBoxPassWord = nullptr;
};

#endif // __GameMenu_H__