/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameLandLayer.h"

static const char* LANDUI_PATH = "platform/LoginorRegistUi/Login/loginUI.csb";

#define Word_Empty_Name		GBKToUtf8("�˺Ų���Ϊ��!")
#define Word_Empty_Pass		GBKToUtf8("���벻��Ϊ��!")

LandLayer::LandLayer()
{
}

LandLayer::~LandLayer()
{	

}

void LandLayer::closeFunc()
{
	if (nullptr != onCloseCallBack)
	{
		onCloseCallBack();
	}
}

bool LandLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(LANDUI_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto imgBG = dynamic_cast<ImageView*>(node->getChildByName("Image_BG"));
	if (_winSize.width / _winSize.height > 1.78f)
	{
		imgBG->setScale(0.9f);
	}

	// �رհ�ť
	auto btn_close = dynamic_cast<Button*>(imgBG->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*) {
		close(ACTION_TYPE_LAYER::FADE);
	});

	// �˺������
	auto TextField_userName = (TextField*)imgBG->getChildByName("TextField_userName");
	TextField_userName->setVisible(false);
	_editBoxUserName = HNEditBox::createEditBox(TextField_userName, this);
	_editBoxUserName->setInputFlag(cocos2d::ui::EditBox::InputFlag::SENSITIVE);
	_editBoxUserName->setString(UserDefault::getInstance()->getStringForKey(USERNAME_TEXT));
	_editBoxUserName->setCascadeOpacityEnabled(true);

	// ���������
	auto textField_PassWord = (TextField*)imgBG->getChildByName("TextField_passWord");
	textField_PassWord->setVisible(false);
	_editBoxPassWord = HNEditBox::createEditBox(textField_PassWord, this);
	_editBoxPassWord->setInputFlag(cocos2d::ui::EditBox::InputFlag::PASSWORD);
	_editBoxPassWord->setCascadeOpacityEnabled(true);
	if (UserDefault::getInstance()->getBoolForKey(SAVE_TEXT))
	{
		_editBoxPassWord->setString("******");
		_isGetPassWord = true;
	}

	// �������븴ѡ��
	_checkBoxSave = (CheckBox*)imgBG->getChildByName("CheckBox_save");
	_checkBoxSave->setSelected(UserDefault::getInstance()->getBoolForKey(SAVE_TEXT));
	_checkBoxSave->addClickEventListener([=](Ref*){

		if (!_checkBoxSave->isSelected())
		{
			UserDefault::getInstance()->setStringForKey(PASSWORD_TEXT, "");
		}
		UserDefault::getInstance()->setBoolForKey(SAVE_TEXT, _checkBoxSave->isSelected());
		UserDefault::getInstance()->flush();
	});

	// ��½��ť
	Button* Btn_login = (Button*)imgBG->getChildByName("Button_login");
	Btn_login->addClickEventListener(CC_CALLBACK_1(LandLayer::loginClickCallback, this));

	// ע���ʺŰ�ť
	Button* Btn_regist = (Button*)imgBG->getChildByName("Button_regist");
	Btn_regist->addClickEventListener([=](Ref*){

		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_CLOSE);

		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
			if (onRegistCallBack) onRegistCallBack();
			this->removeFromParent();
		}), nullptr));
	});

	// �һ����밴ť
	Button* Btn_find = (Button*)imgBG->getChildByName("Button_find");
	Btn_find->setVisible(false);
	Btn_find->addClickEventListener([=](Ref*){

		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_CLOSE);

		std::string userName = _editBoxUserName->getString();

		if (userName.empty())
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("�˺Ų���Ϊ�գ�"));
			return;
		}

		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([=](){
			if (onFindCallBack) onFindCallBack(userName);
			this->removeFromParent();
		}), nullptr));
	});

	return true;
}

void LandLayer::loginClickCallback(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
	do 
	{
		// ��ȡ������˺�
		std::string userName = _editBoxUserName->getString();
		if (userName.empty())
		{
			GamePromptLayer::create()->showPrompt(Word_Empty_Name);
			break;
		}

		// ��ȡ���������
		std::string passWord = _editBoxPassWord->getString(); 
		if (passWord.empty())
		{
			GamePromptLayer::create()->showPrompt(Word_Empty_Pass);
			break;
		}

		if (nullptr != onLoginCallBack)
		{
			if (_isGetPassWord)
			{
				onLoginCallBack(userName, UserDefault::getInstance()->getStringForKey(PASSWORD_TEXT));
			}
			else
			{
				passWord = MD5_CTX::MD5String(passWord);
				onLoginCallBack(userName, passWord);
			}
		}
	} while (0);
}

void LandLayer::editBoxEditingDidBegin(ui::EditBox* editBox)
{
	_isOpenLayer = true;
}

void LandLayer::editBoxTextChanged(ui::EditBox* editBox, const std::string& text)
{
	if (!_isOpenLayer) return;
	
	if (editBox == _editBoxPassWord && text != "******")
	{
		if (_isGetPassWord)
		{
			UserDefault::getInstance()->setStringForKey(PASSWORD_TEXT, "");
			UserDefault::getInstance()->flush();
			_isGetPassWord = false;
		}
	}
}
