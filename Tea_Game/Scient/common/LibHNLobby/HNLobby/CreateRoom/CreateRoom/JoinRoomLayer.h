/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_JoinRoomLayer_h__
#define HN_JoinRoomLayer_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"
#include "network/HttpClient.h"
#include "HNLobby/GameBaseLayer/GameLists.h"
#include "CreateRoomLayer.h"
#include "HNLogicExport.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;


class JoinRoomLayer : public HNLayer
	, public HN::IHNRoomLogicBase
	, public IHNRoomDeskLogic
{
public:
	typedef std::function<void()> EnterGameCallBack;
	EnterGameCallBack onEnterGameCallBack = nullptr;

public:
	virtual bool init() override;

	JoinRoomLayer();

	virtual ~JoinRoomLayer();

	CREATE_FUNC(JoinRoomLayer);

private:
	void closeFunc() override;

	// 数字按钮回调
	void onNumberBtnClickCallback(Ref* pSender);

	// 创建房间回调
	void createRoomClickCallback(Ref* pSender);

	// 删除按钮回调
	void deleteBtnClickCallback(Ref* pSender);

	// 重置按钮回调
	void resetBtnClickCallback(Ref* pSender);

private:	
	std::string _currentPassWord = "";  //当前输入的房间密码

	Vector<ui::TextAtlas*> _numberVec;
};

#endif // HN_JoinRoomLayer_h__
