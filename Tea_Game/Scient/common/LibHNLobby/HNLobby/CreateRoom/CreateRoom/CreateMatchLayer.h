
#ifndef _MATCH_SCENE_
#define _MATCH_SCENE_
#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class MatchEntrance :public HNLayer, MoneyChangeNotify,
	public TableViewDelegate,
	public TableViewDataSource
{
public:
	MatchEntrance();

	~MatchEntrance();

	virtual bool init();

	CREATE_FUNC(MatchEntrance);

	typedef function<void()> onCloseCallBack;
	onCloseCallBack _onCloseCallBack;

	typedef std::function<int()>SelectediGameId;
	SelectediGameId _SelectediGameId;
	// 监听玩家购买金币消息
	virtual void walletChanged() override;
private:

	void onButtonClickCallBack(Ref* pSender);
	void onMatchItemsClickCallBack(Ref* pSender, Widget::TouchEventType type);
	void EnterMatchList(int matchID);
	typedef function<void()> ReturnCallBack;
	ReturnCallBack	_ReturnCallBack;

	

	// 获取公告请求
	void requestNotice(int num);
	void dealNoticeResp(const std::string& data);

	// 更新用户信息
	void updateUserInfo();

	// 更新金币
	void updateMoney();

	// 更新钻石
	void updateDiamond();

	// 更新奖券
	void updateLotteries();
	
private:

	void CreateMatchItems();

	//添加Cell
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

	//设置cell数量
	virtual ssize_t numberOfCellsInTableView(TableView* table);

	//设置cell大小
	virtual Size tableCellSizeForIndex(TableView* table, ssize_t idx);

	//设置触摸函数
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);

	//获取背包信息
	bool getBagList(HNSocketMessage* socketMessage);


private:
	ui::ScrollView* _PanelMatchs = nullptr;
	ImageView* _Imagebg = nullptr;
	TableView* _MatchItems = nullptr;
	ImageView* _ImageMatchs = nullptr;

	Text* _label_golds;
	Text* _label_Jewels;
	std::vector<char*>   _BagData;                      //订单号
	std::map<const char*, int>   _mapBag;                //订单号和钱数

};

#endif // !_MATCH_SCENE_


