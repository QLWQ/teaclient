/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "VipRoomController.h"
#include "HNNetExport.h"
#include <string>
#include "UMeng/UMengSocial.h"
#include "../../GameBaseLayer/GamePlatform.h"
#include "../../GameReconnection/Reconnection.h"
#include "../../GameClub/GameClub.h"
#include "../../GameChildLayer/GameShareLayer.h"

static const char* DISMISS_CSB		= "platform/dismissDesk/disPrompt_Node.csb";
static const char* DISLIST_CSB		= "platform/dismissDesk/disList_Node.csb";
static const char* DISITEM_CSB		= "platform/dismissDesk/disItem_Node.csb";
static const char* ACTIVITY_CSB    = "platform/Games/RoomController/RoomControllerLayer.csb";

static const char* DISITEMBTN		= "platform/dismissDesk/res/cancelRoom.png";
static const char* RETURNBTN		= "platform/dismissDesk/res/returnLobby.png";

static const char* INVITATION		= "platform/dismissDesk/res/invitation.png";
static const char* ROOMRESULT		 = "platform/dismissDesk/res/btn_xinxi.png";
static const char* GAMEACTIVITY    = "platform/Games/JB.png";
extern int ClubID;
static const char* FONT_NAME		= "platform/common/RTWSYueRoudGoG0v1-Regular.ttf";
static const std::string CSB_NAME[] = {"platform/Games/Award/box.csb","platform/Games/Award/hb.csb"};
//////////////////////////////////////////////////////////////////////////////////
#define	UG_HUA_MASK					0xF0			//1111 0000
#define	UG_VALUE_MASK				0x0F			//0000 1111
////////////////////扑克花色//////////////////////////////
#define UG_FANG_KUAI				0			//方块	0000 0000
#define UG_MEI_HUA					1			//梅花	0001 0000
#define UG_HONG_TAO					2			//红桃	0010 0000
#define UG_HEI_TAO					3			//黑桃	0011 0000
///////////////////////////////////////////////////////////////////
VipRoomController::VipRoomController(int deskNo, bool bShowBtn)
: _isShowBtn (bShowBtn)
, _vipRoomObserver(nullptr)
{
	_logic = VipRoomLogic::create(this);
	_logic->setDeskNo(deskNo);
	_logic->retain();

    memset(&_deskinfo, 0, sizeof(_deskinfo));
}

VipRoomController::~VipRoomController()
{
	_logic->release();
	Director::getInstance()->getEventDispatcher()->removeEventListener(_listener);
	Director::getInstance()->getEventDispatcher()->removeEventListener(_foreground);
}

VipRoomController* VipRoomController::create(int deskNo, bool bShowBtn/* = true*/)
{
	VipRoomController *pRet = new VipRoomController(deskNo, bShowBtn);
	if (pRet && pRet->init()) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool VipRoomController::init()
{
	if (!HNLayer::init()) {
		return false;
	}
	//添加键盘监听事件
	//enableKeyboard();

	_btn_dismiss = Button::create(DISITEMBTN);
	_btn_dismiss->setPosition(Vec2(_winSize.width * 0.93f, _winSize.height * 0.93f));
	_btn_dismiss->addClickEventListener(CC_CALLBACK_1(VipRoomController::dismissBtnCallBack, this));
	addChild(_btn_dismiss);
	_btn_dismiss->setVisible(false);

	//返回大厅
	_btn_return = Button::create(RETURNBTN);
	_btn_return->setPosition(Vec2(_winSize.width * 0.83f, _winSize.height * 0.93f));
	_btn_return->addClickEventListener(CC_CALLBACK_1(VipRoomController::returnBtnCallBack, this));
	addChild(_btn_return);
	_btn_return->setVisible(false);

	//邀请好友
	_btn_Invitation = Button::create(INVITATION);
	_btn_Invitation->setPosition(Vec2(_winSize.width  / 2, _winSize.height * 0.45f));
	_btn_Invitation->addClickEventListener(CC_CALLBACK_1(VipRoomController::invitationBtnCallBack, this));
	addChild(_btn_Invitation);
	_btn_Invitation->setVisible(false);

	//查看房间信息
	_btn_RoomResult = Button::create(ROOMRESULT);
	_btn_RoomResult->setPosition(Vec2(_winSize.width - 230, _winSize.height - 50));
	_btn_RoomResult->addClickEventListener(CC_CALLBACK_1(VipRoomController::roomresultBtnCallBack, this));
	addChild(_btn_RoomResult);
	_btn_RoomResult->setVisible(false);

	_text_roomNum = Text::create();
	_text_roomNum->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	_text_roomNum->setFontSize(20);
	_text_roomNum->setFontName(FONT_NAME);
	_text_roomNum->setPosition(Vec2(_winSize.width * 0.18f, _winSize.height * 0.98f));
	_text_roomNum->setString("");
	addChild(_text_roomNum);

	_text_playCount = Text::create();
	_text_playCount->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	_text_playCount->setFontSize(20);
	_text_playCount->setFontName(FONT_NAME);
	_text_playCount->setPosition(Vec2(_winSize.width * 0.18f, _winSize.height * 0.94f));
	_text_playCount->setString("");
	addChild(_text_playCount);

	_text_difen = Text::create();
	_text_difen->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	_text_difen->setFontSize(20);
	_text_difen->setFontName(FONT_NAME);
	_text_difen->setPosition(Vec2(_winSize.width * 0.18f, _winSize.height * 0.90f));
	_text_difen->setString("");
	addChild(_text_difen);

	//房卡游戏内红包活动
	auto node_csb = CSLoader::createNode(ACTIVITY_CSB);
	addChild(node_csb, 2);
	node_csb->getParent()->setLocalZOrder(99999);

	_btn_Activity = (Button*)node_csb->getChildByName("btn_redbag");
	_btn_Activity->addClickEventListener(CC_CALLBACK_1(VipRoomController::ButtonRedBagCallBack, this));
	_btn_Activity->setVisible(false);

	//提示红包个数
	_image_Tip = (ImageView*)_btn_Activity->getChildByName("image_tip");

	_CanTakeNum = (Text*)_image_Tip->getChildByName("text_tip");

	//提示任务内容背景
	_panel_ActivityM = (Layout*)node_csb->getChildByName("panel_make");			//移动条背景
	_sp_TaskBg = (Sprite*)_panel_ActivityM->getChildByName("sp_bg");

	_text_Activity = (Text*)_sp_TaskBg->getChildByName("text_toal");
	_text_Activity->setString("0/5");

	_text_Activity_num = (Text*)_sp_TaskBg->getChildByName("text_num");
	_text_Activity_num->setString("5");

	_panel_Activity = (Layout*)node_csb->getChildByName("panel_receive");

	Button* btn_close = (Button*)_panel_Activity->getChildByName("btn_close");
	btn_close->addClickEventListener([=](Ref*) {
		_panel_Activity->setVisible(false);
	});

	for (int i = 0; i < 6; i++)
	{
		string strName = StringUtils::format("btn_%d", i);
		Button* btn = (Button*)_panel_Activity->getChildByName(strName);
		btn->setTag(i);
		btn->addClickEventListener(CC_CALLBACK_1(VipRoomController::ButtonRedClick, this));
	}

	// 监听重连
	_listener = EventListenerCustom::create(RECONNECTION, [=](EventCustom* event) {

		if (_dismissLayer)
		{
			_dismissLayer->removeFromParent();
			_dismissLayer = nullptr;
		}

		if (_actionListLayer)
		{
			_actionListLayer->removeFromParent();
			_actionListLayer = nullptr;
		}

		_isTouch = true;
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_listener, 1);

	// 切回前台通知
	_foreground = EventListenerCustom::create(FOCEGROUND, [=](EventCustom* event) {
		
		UINT nowTime = time(NULL);
		UINT lastTime = Configuration::getInstance()->getValue("bkTime", Value(0)).asUnsignedInt();
		if (0 == lastTime) return;

		// 切后台所消耗时间
		UINT useTime = nowTime - lastTime;
		_bkGameTime = useTime;
		// 倒计时剩余时间
		_iTime -= useTime;
	});

	Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_foreground, 1);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(VipRoomController::onTouchBegan, this);
	listener->setSwallowTouches(true);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

void VipRoomController::setDismissBtnPos(Vec2 pos)
{
	_btn_dismiss->setPosition(pos);
}

void VipRoomController::setDismissBtnTexture(std::string normalPath)
{
	_btn_dismiss->loadTextureNormal(normalPath);
}

void VipRoomController::resetDismissBtnParent(Node* parent, Vec2 pos)
{
	CCAssert(parent != nullptr, "parent is null");

	if (parent)
	{
		_btn_dismiss->removeFromParentAndCleanup(false);
		parent->addChild(_btn_dismiss);
		_btn_dismiss->setPosition(pos);
	}	
}

void VipRoomController::setReturnBtnPos(Vec2 pos)
{
	_btn_return->setPosition(pos);
}

void VipRoomController::setReturnBtnTexture(std::string normalPath)
{
	_btn_return->loadTextureNormal(normalPath);
}

void VipRoomController::resetReturnBtnParent(Node* parent, Vec2 pos)
{
	CCAssert(parent != nullptr, "parent is null");

	if (parent)
	{
		_btn_return->removeFromParentAndCleanup(false);
		parent->addChild(_btn_return);
		_btn_return->setPosition(pos);
	}
}

void VipRoomController::setInvitationBtnPos(Vec2 pos)
{
	_btn_Invitation->setPosition(pos);
}

void VipRoomController::setInvitationBtnTexture(std::string normalPath)
{
	_btn_Invitation->loadTextureNormal(normalPath);
}

void VipRoomController::setRoomNumPos(Vec2 pos)
{
	_text_roomNum->setPosition(pos);
}

void VipRoomController::setRoomNumColorAndSize(float size, Color3B color)
{
	_text_roomNum->setColor(color);
	_text_roomNum->setFontSize(size);
}

void VipRoomController::resetRoomNumParent(Node* parent, Vec2 pos)
{
	CCAssert(parent != nullptr, "parent is null");

	if (parent)
	{
		_text_roomNum->removeFromParentAndCleanup(false);
		parent->addChild(_text_roomNum);
		_text_roomNum->setPosition(pos);
	}
}

void VipRoomController::setPlayCountPos(Vec2 pos)
{
	_text_playCount->setPosition(pos);
}

// 设置局数颜色与大小
void VipRoomController::setPlayCountColorAndSize(float size, Color3B color)
{
	_text_playCount->setColor(color);
	_text_playCount->setFontSize(size);
}

void VipRoomController::resetPlayCountParent(Node* parent, Vec2 pos)
{
	CCAssert(parent != nullptr, "parent is null");

	if (parent)
	{
		_text_playCount->removeFromParentAndCleanup(false);
		parent->addChild(_text_playCount);
		_text_playCount->setPosition(pos);
	}
}

void VipRoomController::setDifenPos(Vec2 pos)
{
	_text_difen->setPosition(pos);
}

void VipRoomController::setDifenString(string str)
{
	_text_difen->setString(str);
}

void VipRoomController::setDifenColorAndSize(float size /*= 20.0f*/, Color3B color /*= Color3B(0, 255, 0)*/)
{
	_text_difen->setColor(color);
	_text_difen->setFontSize(size);
}

void VipRoomController::setDifenStr()
{
	if (_gameName == "泉州麻将" || _gameName == "漳州麻将" || _gameName == "厦门麻将")
	{
		if (asciiTointNum(_deskinfo.DeskConfig[2]) == 0)
		{
			_text_difen->setString(GBKToUtf8("底分：庄2闲1"));
		}
		else if (asciiTointNum(_deskinfo.DeskConfig[2]) == 1)
		{
			_text_difen->setString(GBKToUtf8("底分：庄10闲5"));
		}
		else
		{
			_text_difen->setString(GBKToUtf8("底分：庄20闲10"));
		}
	}
	else if (_gameName == "抢庄牛牛")
	{
		_text_difen->setString(StringUtils::format("%s%d",GBKToUtf8("底分:"), (asciiTointNum(_deskinfo.DeskConfig[1]) + 1)));
	}
	else if (_gameName == "斗地主" || _gameName == "十三水")
	{
		_text_difen->setString(GBKToUtf8("底分：1"));
	}
}

void VipRoomController::setRoomInfo(VipDeskInfoResult data)
{
	_deskinfo = data;
}

void VipRoomController::setShareRule(std::string text)
{
	_shareRule = text;
}

void VipRoomController::setVipDeskInfo(std::string password, int masterID, int playCount, int nowPlayCount, int type, int seconds)
{
	_isGameStart = (0 != nowPlayCount);

	// 更新 返回/解散 按钮状态
	if (_isShowBtn)
	{
		if (_isDismiss)
		{
			_btn_dismiss->setVisible(false);
		}
		else
		{
			_btn_Invitation->setVisible(!_isGameStart);
			_btn_return->setVisible(!_isGameStart);

			if (masterID == PlatformLogic()->loginResult.dwUserID)
			{
				_btn_dismiss->setVisible(true);
			}
			else
			{
				_btn_dismiss->setVisible(_isGameStart);
			}
		}
	}

	_text_roomNum->setString(StringUtils::format(GBKToUtf8("房号：%s"), password.c_str()));

	if (0 == type)
	{
		_text_playCount->setString(StringUtils::format(GBKToUtf8("局数：%d/%d"), nowPlayCount, playCount));
	}
	else if (1 == type)
	{
		_text_playCount->setString(StringUtils::format(GBKToUtf8("%d胡场"), playCount));
		if (playCount == 99)
		{
			_text_playCount->setString(GBKToUtf8("课数：1课"));
		}
	}
	else if (2 == type)
	{
		_text_playCount->setString(StringUtils::format(GBKToUtf8("圈数：%d/%d"), nowPlayCount, playCount));
	}
	else if (3 == type)
	{
		_text_playCount->setString(secondToTimeStr(seconds).c_str());
		unschedule(schedule_selector(VipRoomController::updateRestTime));
		schedule(schedule_selector(VipRoomController::updateRestTime), 10.0f);
	}
	else
	{

	}
	setDifenStr();
}

// 获取房间信息通知
void VipRoomController::onRoomGetRoomInfoCallback(bool success, VipDeskInfoResult* info)
{
    memcpy(&_deskinfo, info, sizeof(_deskinfo));
	setRoomResult1(info);
	if (success)
	{
		_gameName = info->szGameName;

		setVipDeskInfo(info->szPassWord, info->iMasterID, info->iPlayCount, info->iNowCount, info->iType, info->iSeconds);

		// 通知ui更新房主标志显示
		if (onSetVipInfoCallBack) onSetVipInfoCallBack();

        if (_vipRoomObserver)
        {
            _vipRoomObserver->onRoomCountChange(info->iPlayCount, info->iNowCount);
            _vipRoomObserver->onRoomHostChange(info->iMasterID);
            _vipRoomObserver->onRoomPwdChange(info->szPassWord);
        }
		if (_gameName == "十三水")
		{
			getCardValue();
		}
	}
	else
	{
		showTipBox(GBKToUtf8("未能获取当前桌子配置信息"));
	}
}

// 设置房间规则
void VipRoomController::setRoomResult1(VipDeskInfoResult* pData)
{ 
	m_deskinfo = pData;
	if (onRoomResultCallBack != nullptr)
	{
		onRoomResultCallBack(pData);
	}
};

// 申请解散通知
void VipRoomController::onRoomDismissNotifyCallback(VipDeskDismissNotify* notify)
{
	_isTouch = false;

	_applyUserID = notify->iUserID;
	_iTime = notify->iTimeCount;

	if (notify->iUserID != PlatformLogic()->loginResult.dwUserID)
	{
		if (!_dismissLayer)
		{
			auto node = CSLoader::createNode(DISMISS_CSB);
			CCAssert(node != nullptr, "null");
			node->setPosition(_winSize / 2);
			addChild(node, 1);
			node->getParent()->setLocalZOrder(99999);
			_dismissLayer = dynamic_cast<Layout*>(node->getChildByName("Panel_dismiss"));
			auto layout_BG = dynamic_cast<Layout*>(_dismissLayer->getChildByName("Panel_BG"));
			layout_BG->setScale(_winSize.width / 1280, _winSize.height / 720);
		}

		auto imgBG = dynamic_cast<ImageView*>(_dismissLayer->getChildByName("Image_BG"));
		imgBG->setScale(0);
		imgBG->runAction(ScaleTo::create(0.2f, 1.0f));

		auto text_name = dynamic_cast<Text*>(imgBG->getChildByName("Text_name"));

		auto userinfo = UserInfoModule()->findUser(notify->iUserID);
		if (userinfo)
		{
			text_name->setString(GBKToUtf8(userinfo->nickName));
		}

		_text_optTime = dynamic_cast<Text*>(imgBG->getChildByName("Text_time"));

		auto btn_agree = dynamic_cast<Button*>(imgBG->getChildByName("Button_agree"));
		btn_agree->addClickEventListener([=](Ref*) {
			_logic->userDismissAction(true);
		});

		auto btn_refuse = dynamic_cast<Button*>(imgBG->getChildByName("Button_refuse"));
		btn_refuse->addClickEventListener([=](Ref*) {
			_logic->userDismissAction(false);
		});
	}
	else
	{
		showUserActionList(INVALID_USER_ID, true);
	}

	// 启动倒计时
	schedule(schedule_selector(VipRoomController::showCountdown), 1.0f);
}

// 解散结果通知
void VipRoomController::onRoomDissmissCallback(bool success)
{
	_isTouch = true;

	if (_actionListLayer)
	{
		_actionListLayer->getParent()->removeFromParent();
		_actionListLayer = nullptr;
	}

	if (_dismissLayer)
	{
		auto imgBG = dynamic_cast<ImageView*>(_dismissLayer->getChildByName("Image_BG"));
		imgBG->runAction(Sequence::create(ScaleTo::create(0.2f, 0.0f), CallFunc::create([=]() {
			_dismissLayer->getParent()->removeFromParent();
			_dismissLayer = nullptr;
		}), nullptr));
	}

	if (success)
	{
		_isDismiss = true;

		// 房间解散后暂时关闭重连功能
		GameReconnection()->openOrCloseReconnet(false);

		if (_isShowBtn)
		{
			_btn_dismiss->setVisible(false);
			_btn_return->setVisible(true);
		}

		// 第一局游戏开始之后解散房间不再弹出提示，因为会跟大结算表冲突
		if (!_isGameStart)
		{
			auto callBack = [=]() {
				RoomLogic()->close();
				if (ClubID > 0)
				{
					GamePlatform::createPlatform();
					Club::createClubScene();
				}
				else
				{
					GamePlatform::createPlatform();
				}
			};
			showTipBox(GBKToUtf8("桌子已解散"), false, "", callBack);
		}
		else
		{
			if (_gameName != "十三水")
			{
				if (onDissmissCallBack) onDissmissCallBack();
			}
			else if (_gameName == "十三水" &&  _deskinfo.iNowCount == 1)
			{
				RoomLogic()->close();
				GamePlatform::createPlatform();
			}
		}
	}
	else
	{
		_applyUserID = INVALID_USER_ID;
		showTipBox(GBKToUtf8("解散失败"));
	}

	// 移除倒计时
	if (isScheduled(schedule_selector(VipRoomController::showCountdown)))
	{
		unschedule(schedule_selector(VipRoomController::showCountdown));
	}
}

// 解散消息玩家操作通知
void VipRoomController::onRoomDissmissActionCallback(UINT userID, bool isAgree)
{
	showUserActionList(userID, isAgree);
}

// 断线重连消息通知
void VipRoomController::onRoomNetCutCallback(NetCutDismissNotify* notify)
{
	//_isTouch = false;
	_isTouch = true;
	_applyUserID = notify->iUserID;
	_iTime = notify->iTimeCount;

	for (int i = 0; i < 20; i++)
	{
		if (0 == notify->iUserAgreeID[i]) continue;
		showUserActionList(notify->iUserAgreeID[i], true);
	}

	if (!isScheduled(schedule_selector(VipRoomController::showCountdown)))
	{
		// 启动倒计时
		schedule(schedule_selector(VipRoomController::showCountdown), 1.0f);
	}
}

// IP/距离过近通知
void VipRoomController::onRoomDistanceCallback(std::vector<MSG_GR_S_Position_Notice *> queueNotice)
{
	std::string str;

	if (!queueNotice.empty())
	{
		for (auto notic : queueNotice)
		{
			auto userA = UserInfoModule()->findUser(notic->iUserIDA);
			auto userB = UserInfoModule()->findUser(notic->iUserIDB);

			if (!userA || !userB) continue;

			str.append(StringUtils::format(GBKToUtf8("%s与%s"),
				GBKToUtf8(userA->nickName), GBKToUtf8(userB->nickName)));

			if (notic->bDistance)
			{
				str.append(StringUtils::format(GBKToUtf8("距离%d米"), notic->iDistance));
			}

			if (notic->bSameIP)
			{
				str.append(GBKToUtf8("IP相同"));
			}

			str.append("\n");
		}

		str.append(GBKToUtf8("是否继续游戏？"));

		auto callBack = [=]() {

			_logic->userDistanceAction(true);
		};

		auto cancelCB = [=]() {

			_logic->userDistanceAction(false);
		};

		showTipBox(str, true, "prompt", callBack, cancelCB);

		/*auto prompt = GamePromptLayer::create(true); 
		prompt->showPrompt(str);
		prompt->setCallBack(callBack);
		prompt->setCancelCallBack(cancelCB);*/
	}
}

// IP/距离过近玩家操作通知
void VipRoomController::onRoomDistanceActionCallback(UINT userID, bool isAgree)
{
	if (isAgree)
	{
		// 距离过近或ip相同，有玩家同意继续游戏，则添加进同意列表
		showUserActionList(userID, isAgree);
	}
	else
	{
		// 玩家拒绝继续游戏，则拒绝的玩家退出当前房间，其他玩家弹出开局失败提示
		// 如果是房主拒绝继续游戏，则解散当前房间
		GamePromptLayer * prompt = (GamePromptLayer*)Director::getInstance()->getRunningScene()->getChildByName("prompt");
		if (prompt) prompt->removeFromParent();

		auto user = UserInfoModule()->findUser(userID);
		std::string str = GBKToUtf8("有人拒绝参与游戏，开局失败");
		if (user)
		{
			str = StringUtils::format(GBKToUtf8("%s拒绝参与游戏，开局失败"), GBKToUtf8(user->nickName));
		}

		showTipBox(str);

		if (_actionListLayer)
		{
			_actionListLayer->removeFromParent();
			_actionListLayer = nullptr;
		}
	}
}

// 所有玩家同意游戏开始通知
void VipRoomController::onRoomAllUserAgreeCallback(bool isAgree)
{
	if (isAgree)
	{
		if (_actionListLayer)
		{
			_actionListLayer->removeFromParent();
			_actionListLayer = nullptr;
		}
	}
}

// 返回大厅通知
void VipRoomController::onRoomReturnPlatformCallback(bool success, std::string message)
{
	if (success)
	{
		if (ClubID > 0)
		{	
			GamePlatform::createPlatform();
			Club::createClubScene();
		}
		else
		{
			GamePlatform::createPlatform();
		}
	}
	else
	{
		showTipBox(message);
	}
}

void VipRoomController::showCountdown(float dt)
{
	if (0 > --_iTime)
	{
		unschedule(schedule_selector(VipRoomController::showCountdown));
	}
	
	if (_dismissLayer && _text_optTime) _text_optTime->setString(StringUtils::format(GBKToUtf8("解散倒计时%ds"), _iTime));

	if (_actionListLayer && _text_listTime) _text_listTime->setString(StringUtils::format("%ds", _iTime));
}

void VipRoomController::showUserActionList(INT userID, bool isAgree)
{
	if (userID == _applyUserID) return;

	if (!_actionListLayer)
	{
		auto node = CSLoader::createNode(DISLIST_CSB);
		CCAssert(node != nullptr, "null");
		node->setPosition(Vec2(0, _winSize.height * 0.9f));
		addChild(node);

		_actionListLayer = dynamic_cast<Layout*>(node->getChildByName("Panel_dismiss"));
		_actionListLayer->setScale(0.7f);

		auto text_name = dynamic_cast<Text*>(_actionListLayer->getChildByName("Text_name"));
		if (_applyUserID == INVALID_USER_ID)
		{
			text_name->setString(GBKToUtf8("防作弊提示"));
		}
		else
		{
			auto user = UserInfoModule()->findUser(_applyUserID);
			if (user)
			{
				text_name->setString(StringUtils::format(GBKToUtf8("%s申请解散"), GBKToUtf8(user->nickName)));
			}
			else
			{
				text_name->setString(StringUtils::format(GBKToUtf8("%u申请解散"), _applyUserID));
			}
		}

		_text_listTime = dynamic_cast<Text*>(_actionListLayer->getChildByName("Text_time"));

		auto img_btn = dynamic_cast<ImageView*>(_actionListLayer->getChildByName("Image_btn"));
		img_btn->addClickEventListener([=](Ref*){

			img_btn->setEnabled(false);
			int value = _actionListLayer->getPositionX() == 0 ? -1 : 1;
			_actionListLayer->runAction(Sequence::create(
				MoveBy::create(0.15f, Vec2(value * _actionListLayer->getContentSize().width * 0.96f * 0.7f, 0.0f)),
				CallFunc::create([=](){
				img_btn->setEnabled(true);
			}), nullptr));
		});
	}

	if (userID == PlatformLogic()->loginResult.dwUserID && _dismissLayer)
	{
		auto imgBG = dynamic_cast<ImageView*>(_dismissLayer->getChildByName("Image_BG"));
		imgBG->runAction(Sequence::create(ScaleTo::create(0.2f, 0.0f), CallFunc::create([=]() {
			_dismissLayer->getParent()->removeFromParent();
			_dismissLayer = nullptr;
		}), nullptr));
	}

	auto list_user = dynamic_cast<ui::ListView*>(_actionListLayer->getChildByName("ListView_user"));

	// 对于已经存在操作列表中的玩家，只是更新信息
	bool exist = false;
	for (auto item : list_user->getItems())
	{
		if (item->getTag() == userID)
		{
			auto layout_item = dynamic_cast<Layout*>(item->getChildByName("Panel_item"));

			auto text_tip = dynamic_cast<Text*>(layout_item->getChildByName("Text_tip"));
			text_tip->setString(GBKToUtf8("已同意"));

			auto btn_cancle = dynamic_cast<Button*>(layout_item->getChildByName("Button_cancle"));
			btn_cancle->setVisible(false);
			exist = true;
			break;
		}
	}

	// 对于未存在操作列表中的玩家，添加进列表
	auto userInfo = UserInfoModule()->findUser(userID);
	if (userInfo && !exist)
	{
		auto node_item = CSLoader::createNode(DISITEM_CSB);
		node_item->setTag(userID);

		auto layout_item = dynamic_cast<Layout*>(node_item->getChildByName("Panel_item"));
		auto text_name = dynamic_cast<Text*>(layout_item->getChildByName("Text_name"));
		text_name->setString(GBKToUtf8(userInfo->nickName));

		auto text_tip = dynamic_cast<Text*>(layout_item->getChildByName("Text_tip"));
		text_tip->setString(GBKToUtf8("已同意"));

		auto btn_cancle = dynamic_cast<Button*>(layout_item->getChildByName("Button_cancle"));
		btn_cancle->setVisible(false);
		if (userID == PlatformLogic()->loginResult.dwUserID)
		{
			//btn_cancle->setVisible(res->bAgree);
			btn_cancle->addClickEventListener([=](Ref*){
				_logic->userDismissAction(false);
			});
		}

		layout_item->removeFromParentAndCleanup(false);
		list_user->pushBackCustomItem(layout_item);
	}
}

void VipRoomController::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	if (EventKeyboard::KeyCode::KEY_BACK != keyCode) return;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (EventKeyboard::KeyCode::KEY_F1 != keyCode) return;
#endif

	// VIP场由控制器接管按键监听，移除掉游戏监听
	Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget(getParent());

	//游戏未开局直接站起
	if (!_isGameStart)
	{
		returnBtnCallBack(nullptr);
	}
	else
	{
		//游戏已解散直接返回大厅
		if (_isDismiss)
		{
			RoomLogic()->close();
			GamePlatform::createPlatform();
		}
		else {
			_logic->requestDismissDesk();
		}
	}
}

bool VipRoomController::onTouchBegan(Touch* touch, Event *event)
{
	if (_actionListLayer && _actionListLayer->getPositionX() == 0)
	{
		_actionListLayer->runAction(MoveBy::create(0.1f, Vec2(-_actionListLayer->getContentSize().width * 0.96f * 0.7f, 0.0f)));

		return true;
	}
	
	return false;
}


void VipRoomController::doNotShowAnyDelegateButtons()
{
    _isShowBtn = false;
    _btn_dismiss->setVisible(false);
    _btn_return->setVisible(false);
    _btn_Invitation->setVisible(false);
    _text_roomNum->setVisible(false);
    _text_playCount->setVisible(false);
}

// 解散房间按钮回调
void VipRoomController::dismissBtnCallBack(Ref* ref)
{
	if (!_isTouch) return;
	_isTouch = false;

	if (!_isGameStart && PlatformLogic()->loginResult.dwUserID != HNPlatformConfig()->getMasterID())
	{
		returnBtnCallBack(nullptr);
	}
	else
	{
		auto callBack = [=]() {
			if (_bCooling)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("申请过于频繁，请稍后再申请！"));
				_isTouch = true;
			}
			else
			{
				_bCooling = true;
				_logic->requestDismissDesk();
			}
			this->runAction(Sequence::create(DelayTime::create(15.0f), CallFunc::create([=]()
			{
				_bCooling = false;
			}), nullptr));
		};
		auto cancelCB = [=]() {
			_isTouch = true;
		};
		showTipBox(GBKToUtf8("是否解散房间？"), true, "", callBack, cancelCB);
	}
}

//点击红包按钮
void VipRoomController::ButtonRedBagCallBack(Ref* ref)
{
	if (_isActivity)
	{
		//任务达标处理
		_panel_Activity->setVisible(true);
	}
	else
	{
		_sp_TaskBg->stopAllActions();
		if (_ShowStater)
		{
			_sp_TaskBg->runAction(MoveTo::create(0.2, Vec2(-128, 33)));
			_ShowStater = false;
		}
		else
		{
			_sp_TaskBg->runAction(MoveTo::create(0.2, Vec2(128, 33)));
			_ShowStater = true;
		}
		
	}
}

//点击红包领取
void VipRoomController::ButtonRedClick(Ref* ref)
{
	Button* btn = (Button*)ref;
	int Index = btn->getTag();

	if (onClickButtonCallBack != nullptr)
	{
		onClickButtonCallBack(Index);
		_panel_Activity->setVisible(false);
		//onClickButtonCallBack = nullptr;
	}
}

// 邀请好友按钮回调
void VipRoomController::invitationBtnCallBack(Ref* ref)
{
	if (!_isTouch) return;
	_isTouch = false;

	this->runAction(Sequence::create(DelayTime::create(3.f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	auto shareLayer = GameShareLayer::create();
	shareLayer->setName("shareLayer");
	shareLayer->SetShareCallBack([=](bool success, int platform, const std::string& errorMsg) {
		_isTouch = true;
	});
	shareLayer->SetInviteInfo(_gameName, HNPlatformConfig()->getVipRoomNum(), false);
	shareLayer->show();
}

void VipRoomController::getCardValue()
{
	if (asciiTointNum(_deskinfo.DeskConfig[6]) == 1)
	{
		return;
	}
	else
	{
		int cardValue = 0;
		auto num = _deskinfo.DeskConfig[7];
		int iHuaKind = asciiTointNum(_deskinfo.DeskConfig[7]);
		int cardNum = asciiTointNum(_deskinfo.DeskConfig[8]) + asciiTointNum(_deskinfo.DeskConfig[9]);
		cardValue = iHuaKind * 16 + cardNum -1;

		std::string str = StringUtils::format("platform/cards/0x%02X.png", cardValue);
		auto _Sp_mapaiCard = Sprite::create(str);
		_Sp_mapaiCard->setPosition(Vec2(50,650));
		_Sp_mapaiCard->setScale(0.5f);
		this->addChild(_Sp_mapaiCard, 99);
		auto _Sp_mapai = Sprite::create("platform/cards/sp_mapai.png");
		_Sp_mapai->setScale(0.15);
		_Sp_mapai->setPosition(Vec2(_Sp_mapaiCard->getContentSize().width - 30, _Sp_mapaiCard->getContentSize().height - 30));
		_Sp_mapaiCard->addChild(_Sp_mapai);
	}
}


void VipRoomController::addVipRoomObserver(HNVipRoomDelegate* obs)
{
    _vipRoomObserver = obs;
    doNotShowAnyDelegateButtons();
}

void VipRoomController::returnBtnCallBack(Ref* ref)
{
	auto callBack = [=]() {

		_logic->requestReturnPlatform();
	};

	auto cancelCB = [=]() {
		_isTouch = true;
	};

	showTipBox(GBKToUtf8("是否退出房间？"), true, "", callBack, cancelCB);
}

void VipRoomController::showTipBox(std::string tips, bool bCanSelect /*= false*/, std::string name /*= ""*/, 
	std::function<void()> sure /*= nullptr*/, std::function<void()> cancel /*= nullptr*/, bool isRemove /*= true*/)
{
	GamePromptLayer *prompt = nullptr;
	if (name.compare("") != 0)
	{
		prompt = (GamePromptLayer*)Director::getInstance()->getRunningScene()->getChildByName(name);
	}
	if (isRemove)
	{
		if (nullptr != prompt)
			prompt->removeFromParent();
		prompt = nullptr;
	}

	if (nullptr == prompt)
	{
		prompt = GamePromptLayer::create(bCanSelect);
		if (nullptr != cancel)
			prompt->setCancelCallBack(cancel);
		if (nullptr != sure)
			prompt->setCallBack(sure);
	}
	if (name.compare("") != 0)
	{
		prompt->setName(name);
	}

	prompt->showPrompt(tips);
}


// 申请解散 - 不带额外弹窗
void VipRoomController::dissmissWithoutPrompt()
{
    if (!_isGameStart && PlatformLogic()->loginResult.dwUserID != HNPlatformConfig()->getMasterID())
    {
        returnWithoutPrompt();
    }
    else
    {
        _logic->requestDismissDesk();
    }
}

// 返回大厅 - 不带额外弹窗
void VipRoomController::returnWithoutPrompt()
{
    _logic->requestReturnPlatform();
}

std::string VipRoomController::secondToTimeStr(int second)
{
	int hours = second / 3600;
	int minutes = (second % 3600) / 60;
	int seconds = (second % 3600) % 60;

	std::string str = StringUtils::format("%s", GBKToUtf8("剩余时间:"));

	if (hours > 0)
	{
		if (minutes > 0)
		{
			str.append(StringUtils::format("%02d:%2d", hours, minutes));
		}
		else
		{
			str.append(StringUtils::format("%d%s", hours, GBKToUtf8("小时")));
		}
	}
	else if (minutes > 0)
	{
		str.append(StringUtils::format("%d%s", minutes, GBKToUtf8("分钟")));
	}
	else if (seconds > 0)
	{
		str.append(StringUtils::format("%s", GBKToUtf8("不足一分钟")));
	}
	
	return str;
}

void VipRoomController::updateRestTime(float dt)
{
	if (HNPlatformConfig()->getRestTime() <= 30)
	{
		unschedule(schedule_selector(VipRoomController::updateRestTime));
	}
	else
	{
		int restTime = HNPlatformConfig()->getRestTime() - 10;
		if (_bkGameTime > 0)
		{
			restTime -= _bkGameTime;
			_bkGameTime = 0;
		}
		HNPlatformConfig()->setRestTime(restTime);

		_text_playCount->setString(secondToTimeStr(HNPlatformConfig()->getRestTime()).c_str());
	}
}

void VipRoomController::roomresultBtnCallBack(Ref* ref)
{
	if (_gameName == "泉州麻将")
	{
		getQZMJRoomInfo();
	}
	else if (_gameName == "抢庄牛牛")
	{
		getNNRoomInfo();
	}
	else if (_gameName == "漳州麻将")
	{
		getZZMJRoomInfo();
	}
	else if (_gameName == "厦门麻将")
	{
		getXMMJRoomInfo();
	}
	else if (_gameName == "斗地主")
	{
		getDDZRoomInfo();
	}
	else if (_gameName == "十三水")
	{
		getSSSRoomInfo();
	}
	else if (_gameName == "晋江麻将")
	{
		getJJMJRoomInfo();
	}
}

int VipRoomController::asciiTointNum(int num)
{
	return num - 48;
}

void VipRoomController::setRoomResultBtnPos(Vec2 pos)
{
	_btn_RoomResult->setPosition(pos);
}

void VipRoomController::getDDZRoomInfo()
{
	auto resultNode = CSLoader::createNode("platform/dismissDesk/Result_10100003.csb");
	addChild(resultNode, 2000);
	auto resultPanel = (Layout*)resultNode->getChildByName("Panel_1");
	auto closeBtn = (Button*)resultPanel->getChildByName("Button_close");
	closeBtn->addClickEventListener([=](Ref*) {
		if (resultNode)
		{
			resultNode->setVisible(false);
			resultNode->removeFromParent();
		}
	});
	auto roomIDtxt = (Text*)resultPanel->getChildByName("Text_roomID");
	auto roomCounttxt = (Text*)resultPanel->getChildByName("Text_roomCount");
	auto roomFufeitxt = (Text*)resultPanel->getChildByName("Text_roomFufei");
	auto roomWanfa = (Text*)resultPanel->getChildByName("Text_roomWanfa");
	roomIDtxt->setString(_deskinfo.szPassWord);
	roomCounttxt->setString(StringUtils::format(GBKToUtf8("%d / %d局"), _deskinfo.iNowCount, _deskinfo.iPlayCount));
	if (asciiTointNum(_deskinfo.DeskConfig[0]) == 0)
	{
		roomFufeitxt->setString(GBKToUtf8("房主付费"));
	}
	else
	{
		roomFufeitxt->setString(GBKToUtf8("AA付费"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[1]) == 1)
	{
		roomWanfa->setString(GBKToUtf8("经典模式"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[1]) == 3)
	{
		roomWanfa->setString(GBKToUtf8("经典癞子"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[1]) == 5)
	{
		roomWanfa->setString(GBKToUtf8("天地癞子"));
	}
}

void VipRoomController::getNNRoomInfo()
{
	auto resultNode = CSLoader::createNode("platform/dismissDesk/Result_20170708.csb");
	addChild(resultNode, 2000);
	auto resultPanel = (Layout*)resultNode->getChildByName("Panel_1");
	auto closeBtn = (Button*)resultPanel->getChildByName("Button_close");
	closeBtn->addClickEventListener([=](Ref*) {
		if (resultNode)
		{
			resultNode->setVisible(false);
			resultNode->removeFromParent();
		}
	});
	auto roomName = (Text*)resultPanel->getChildByName("Text_roomName");//房间模式名称
	auto roomIDtxt = (Text*)resultPanel->getChildByName("Text_roomID");//房号
	auto roomCounttxt = (Text*)resultPanel->getChildByName("Text_roomCount");//局数
	auto roomBettxt = (Text*)resultPanel->getChildByName("Text_roomBet");//倍数
	auto roomDifentxt = (Text*)resultPanel->getChildByName("Text_roomDifen");//底分
	auto roomFufeitxt = (Text*)resultPanel->getChildByName("Text_roomFufei");//付费
	auto roomZhuangtxt = (Text*)resultPanel->getChildByName("Text_roomWanfa");//玩法
	auto roomPaixingtxt = (Text*)resultPanel->getChildByName("Text_roomPai");//特殊牌型
	roomIDtxt->setString(_deskinfo.szPassWord);
	roomCounttxt->setString(StringUtils::format(GBKToUtf8("%d / %d局"), _deskinfo.iNowCount, _deskinfo.iPlayCount));
	if (asciiTointNum(_deskinfo.DeskConfig[0]) == 0)
	{
		roomFufeitxt->setString(GBKToUtf8("房主付费"));
	}
	else
	{
		roomFufeitxt->setString(GBKToUtf8("AA付费"));
	}
	roomDifentxt->setString(StringUtils::format(GBKToUtf8("*%d"), (asciiTointNum(_deskinfo.DeskConfig[1]) + 1)));
	switch (asciiTointNum(_deskinfo.DeskConfig[2]))
	{
	case 0:
		roomZhuangtxt->setString(GBKToUtf8("自由抢庄"));
		break;
	case 1:
		roomZhuangtxt->setString(GBKToUtf8("牛牛上庄"));
		break;
	case 2:
		roomZhuangtxt->setString(GBKToUtf8("没牛下庄"));
		break;
	case 3:
		roomZhuangtxt->setString(GBKToUtf8("固定坐庄"));
		break;
	case 4:
		roomZhuangtxt->setString(GBKToUtf8("无庄家"));
		break;
	default:
		break;
	}
	if (asciiTointNum(_deskinfo.DeskConfig[3]) == 0)
	{
		roomBettxt->setString(GBKToUtf8("牛牛x4  牛九x3  牛八x2  牛七x2"));
	}
	else
	{
		roomBettxt->setString(GBKToUtf8("牛牛x5  牛九x4  牛八x3  牛七x2"));
	}
	switch (asciiTointNum(_deskinfo.DeskConfig[4]))
	{
	case 0:
		roomName->setString(GBKToUtf8("牛牛-明牌抢庄"));
		break;
	case 1:
		roomName->setString(GBKToUtf8("牛牛-经典模式"));
		break;
	case 2:
		roomName->setString(GBKToUtf8("牛牛-通比模式"));
		break;
	default:
		break;
	}
	vector<std::string> paiVec;
	paiVec.push_back("顺子牛 ");
	paiVec.push_back("五花牛 ");
	paiVec.push_back("同花牛 ");
	paiVec.push_back("葫芦牛 ");
	paiVec.push_back("炸弹牛 ");
	paiVec.push_back("五小牛 ");
	paiVec.push_back("同花顺牛 ");
	std::string paiStr = "";
	for (int i = 5; i < 12; i++)
	{
		if (asciiTointNum(_deskinfo.DeskConfig[i]) == 1)
		{
			paiStr += paiVec.at(i - 5);
		}
	}
	roomPaixingtxt->setString(GBKToUtf8(paiStr));
}

void VipRoomController::getSSSRoomInfo()
{
	auto resultNode = CSLoader::createNode("platform/dismissDesk/Result_12100004.csb");
	addChild(resultNode, 2000);
	auto resultPanel = (Layout*)resultNode->getChildByName("Panel_1");
	auto closeBtn = (Button*)resultPanel->getChildByName("Button_close");
	closeBtn->addClickEventListener([=](Ref*) {
		if (resultNode)
		{
			resultNode->setVisible(false);
			resultNode->removeFromParent();
		}
	});
	auto roomIDtxt = (Text*)resultPanel->getChildByName("Text_roomID");
	auto roomCounttxt = (Text*)resultPanel->getChildByName("Text_roomCount");
	auto roomFufeitxt = (Text*)resultPanel->getChildByName("Text_roomFufei");
	auto roomShangzhuang = (Text*)resultPanel->getChildByName("Text_roomShangzhuang");
	auto roomJiagui = (Text*)resultPanel->getChildByName("Text_roomJiagui");
	auto roomJiama = (Text*)resultPanel->getChildByName("Text_roomJiama");
	auto roomQuanda = (Text*)resultPanel->getChildByName("Text_roomQuanda");
	auto roomLiangmen = (Text*)resultPanel->getChildByName("Text_roomLiangmen");
	auto roomRenshu = (Text*)resultPanel->getChildByName("Text_roomRenshu");

	roomIDtxt->setString(_deskinfo.szPassWord);
	roomCounttxt->setString(StringUtils::format(GBKToUtf8("%d / %d局"), _deskinfo.iNowCount, _deskinfo.iPlayCount));
	if (asciiTointNum(_deskinfo.DeskConfig[0]) == 0)
	{
		roomFufeitxt->setString(GBKToUtf8("房主付费"));
	}
	else
	{
		roomFufeitxt->setString(GBKToUtf8("AA付费"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[1]) == 0)
	{
		roomShangzhuang->setString(GBKToUtf8("通比模式"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[1]) == 1)
	{
		roomShangzhuang->setString(GBKToUtf8("抢庄模式"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[1]) == 2)
	{
		roomShangzhuang->setString(GBKToUtf8("固定庄家"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[2]) == 0)
	{
		roomJiagui->setString(GBKToUtf8("加鬼"));
	}
	else
	{
		roomJiagui->setString(GBKToUtf8("不加鬼"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[3] ) == 0)
	{
		roomQuanda->setString(GBKToUtf8("是"));
	}
	else
	{
		roomQuanda->setString(GBKToUtf8("否"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[4] ) == 0)
	{
		roomLiangmen->setString(GBKToUtf8("是"));
		if (asciiTointNum(_deskinfo.DeskConfig[6]) == 1)
		{
			roomLiangmen->setString(GBKToUtf8("是(红桃方片)"));
		}
		else 
		{
			int cardValue = 0;
			auto num = _deskinfo.DeskConfig[7];
			int iHuaKind = asciiTointNum(_deskinfo.DeskConfig[7]);
			if (iHuaKind == 0 || iHuaKind == 2)
			{
				roomLiangmen->setString(GBKToUtf8("是(黑桃梅花)"));
			}
			else
			{
				roomLiangmen->setString(GBKToUtf8("是(红桃方片)"));
			}
		}
	}
	else
	{
		roomLiangmen->setString(GBKToUtf8("否"));
	
	}
	roomRenshu->setString(StringUtils::format("%d", asciiTointNum(_deskinfo.DeskConfig[5])));
	if (asciiTointNum(_deskinfo.DeskConfig[6]) == 1)
	{
		roomJiama->setString(GBKToUtf8("不加马"));
	}
	else
	{
		roomJiama->setString(GBKToUtf8("加马"));
	}
}

bool VipRoomController::getJJMJRoomInfo()
{
	//晋江麻将玩法信息。6.22
	if (asciiTointNum(_deskinfo.DeskConfig[3]) == 2)
	{
		return true;
	}
	else
	{
		return false;
	}
}
void VipRoomController::getQZMJRoomInfo()
{
	auto resultNode = CSLoader::createNode("platform/dismissDesk/Result_20171121.csb");
	addChild(resultNode, 2000);
	auto resultPanel = (Layout*)resultNode->getChildByName("Panel_1");
	auto closeBtn = (Button*)resultPanel->getChildByName("Button_close");
	closeBtn->addClickEventListener([=](Ref*) {
		if (resultNode)
		{
			resultNode->setVisible(false);
			resultNode->removeFromParent();
		}
	});
	auto roomIDtxt = (Text*)resultPanel->getChildByName("Text_roomID");
	auto roomCounttxt = (Text*)resultPanel->getChildByName("Text_roomCount");
	auto roomBettxt = (Text*)resultPanel->getChildByName("Text_roomBet");
	auto roomDifentxt = (Text*)resultPanel->getChildByName("Text_roomDifen");
	auto roomFufeitxt = (Text*)resultPanel->getChildByName("Text_roomFufei");
	roomIDtxt->setString(_deskinfo.szPassWord);
	if (_deskinfo.iPlayCount == 99 && _deskinfo.iType == 1)
	{
		roomCounttxt->setString(GBKToUtf8("1课"));
	}
	else
	{
		roomCounttxt->setString(StringUtils::format(GBKToUtf8("%d / %d局"), _deskinfo.iNowCount, _deskinfo.iPlayCount));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[1]) == 0)
	{
		roomBettxt->setString(GBKToUtf8("游金*4"));
	}
	else
	{
		roomBettxt->setString(GBKToUtf8("游金*3"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[2]) == 0)
	{
		roomDifentxt->setString(GBKToUtf8("庄2闲1"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[2]) == 1)
	{
		roomDifentxt->setString(GBKToUtf8("庄10闲5"));
	}
	else
	{
		roomDifentxt->setString(GBKToUtf8("庄20闲10"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[0]) == 0)
	{
		roomFufeitxt->setString(GBKToUtf8("房主付费"));
	}
	else
	{
		roomFufeitxt->setString(GBKToUtf8("AA付费"));
	}
}

void VipRoomController::getXMMJRoomInfo()
{
	auto resultNode = CSLoader::createNode("platform/dismissDesk/Result_20171122.csb");
	addChild(resultNode, 2000);
	auto resultPanel = (Layout*)resultNode->getChildByName("Panel_1");
	auto closeBtn = (Button*)resultPanel->getChildByName("Button_close");
	closeBtn->addClickEventListener([=](Ref*) {
		if (resultNode)
		{
			resultNode->setVisible(false);
			resultNode->removeFromParent();
		}
	});
	auto roomIDtxt = (Text*)resultPanel->getChildByName("Text_roomID");
	auto roomCounttxt = (Text*)resultPanel->getChildByName("Text_roomCount");
	auto roomBettxt = (Text*)resultPanel->getChildByName("Text_roomBet");
	auto roomDifentxt = (Text*)resultPanel->getChildByName("Text_roomDifen");
	auto roomFufeitxt = (Text*)resultPanel->getChildByName("Text_roomFufei");
	auto roomDapai = (Text*)resultPanel->getChildByName("Text_roomDapai");
	auto roomJiesuan = (Text*)resultPanel->getChildByName("Text_roomJiesuan");
	auto roomFanghu = (Text*)resultPanel->getChildByName("Text_roomFanghu");
	roomIDtxt->setString(_deskinfo.szPassWord);
	if (_deskinfo.iPlayCount == 99 && _deskinfo.iType == 1)
	{
		roomCounttxt->setString(GBKToUtf8("1课"));
	}
	else
	{
		roomCounttxt->setString(StringUtils::format(GBKToUtf8("%d / %d局"), _deskinfo.iNowCount, _deskinfo.iPlayCount));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[1]) == 0)
	{
		roomBettxt->setString(GBKToUtf8("游金*4"));
	}
	else
	{
		roomBettxt->setString(GBKToUtf8("游金*3"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[2]) == 0)
	{
		roomDifentxt->setString(GBKToUtf8("庄2闲1"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[2]) == 1)
	{
		roomDifentxt->setString(GBKToUtf8("庄10闲5"));
	}
	else
	{
		roomDifentxt->setString(GBKToUtf8("庄20闲10"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[3]) == 0)
	{
		roomDapai->setString(GBKToUtf8("有大牌"));
	}
	else
	{
		roomDapai->setString(GBKToUtf8("无大牌"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[4]) == 0)
	{
		roomJiesuan->setString(GBKToUtf8("比金"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[4]) == 1)
	{
		roomJiesuan->setString(GBKToUtf8("打课"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[4]) == 2)
	{
		roomJiesuan->setString(GBKToUtf8("花金"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[5]) == 0)
	{
		roomFanghu->setString(GBKToUtf8("放胡单赔"));
	}
	else
	{
		roomFanghu->setString(GBKToUtf8("放胡通赔"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[3]) == 0)
	{
		roomFufeitxt->setString(GBKToUtf8("房主付费"));
	}
	else
	{
		roomFufeitxt->setString(GBKToUtf8("AA付费"));
	}
}

void VipRoomController::getZZMJRoomInfo()
{
	auto resultNode = CSLoader::createNode("platform/dismissDesk/Result_20171120.csb");
	addChild(resultNode, 2000);
	auto resultPanel = (Layout*)resultNode->getChildByName("Panel_1");
	auto closeBtn = (Button*)resultPanel->getChildByName("Button_close");
	closeBtn->addClickEventListener([=](Ref*) {
		if (resultNode)
		{
			resultNode->setVisible(false);
			resultNode->removeFromParent();
		}
	});
	auto roomIDtxt = (Text*)resultPanel->getChildByName("Text_roomID");
	auto roomCounttxt = (Text*)resultPanel->getChildByName("Text_roomCount");
	auto roomBettxt = (Text*)resultPanel->getChildByName("Text_roomBet");
	auto roomDifentxt = (Text*)resultPanel->getChildByName("Text_roomDifen");
	auto roomFufeitxt = (Text*)resultPanel->getChildByName("Text_roomFufei");
	roomIDtxt->setString(_deskinfo.szPassWord);
	if (_deskinfo.iPlayCount == 99 && _deskinfo.iType == 1)
	{
		roomCounttxt->setString(GBKToUtf8("1课"));
	}
	else
	{
		roomCounttxt->setString(StringUtils::format(GBKToUtf8("%d / %d局"), _deskinfo.iNowCount, _deskinfo.iPlayCount));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[1]) == 0)
	{
		roomBettxt->setString(GBKToUtf8("游金*4"));
	}
	else
	{
		roomBettxt->setString(GBKToUtf8("游金*3"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[2]) == 0)
	{
		roomDifentxt->setString(GBKToUtf8("庄2闲1"));
	}
	else if (asciiTointNum(_deskinfo.DeskConfig[2]) == 1)
	{
		roomDifentxt->setString(GBKToUtf8("庄10闲5"));
	}
	else
	{
		roomDifentxt->setString(GBKToUtf8("庄20闲10"));
	}
	if (asciiTointNum(_deskinfo.DeskConfig[0]) == 0)
	{
		roomFufeitxt->setString(GBKToUtf8("房主付费"));
	}
	else
	{
		roomFufeitxt->setString(GBKToUtf8("AA付费"));
	}
}

Button* VipRoomController::getRoomInfoBtn()
{
	return _btn_RoomResult;
}

void VipRoomController::setOpenActivity(bool isOpen)
{
	if (_btn_Activity)
	_btn_Activity->setVisible(isOpen);
}

void VipRoomController::setActivityPosition(float x, float y)
{
	_btn_Activity->setPositionX(_btn_Activity->getPositionX() + x);
	_btn_Activity->setPositionY(_btn_Activity->getPositionY() + y);

	_panel_ActivityM->setPositionX(_panel_ActivityM->getPositionX() + x);
	_panel_ActivityM->setPositionY(_panel_ActivityM->getPositionY() + y);
}

void VipRoomController::setCurrentTask(int Current, int Toal)
{
	if (_text_Activity)
	{
		_text_Activity->setString(StringUtils::format("%d / %d", Current, Toal));
	}
	if (_text_Activity_num)
	{
		_text_Activity_num->setString(to_string(Toal));
	}
}

void VipRoomController::setIsStandard(bool isActivity)
{
	_isActivity = isActivity;
	if (!isActivity)
	{
		_image_Tip->setVisible(isActivity);
	}
}

void VipRoomController::setCanTakeNum(int TakeNum)
{
	_image_Tip->setVisible(true);
	_CanTakeNum->setString(to_string(TakeNum));
}

//AnimationType(奖励类型0、1为金币，2、3为房卡，4、5为红包),AwardNum以分为单位（红包类型需要除于100）
void VipRoomController::setAnimationForTake(int AnimationType, int AwardNum)
{
	int Index = 0;
	float DelayTime = 0;
	int Type = 0;
	if (AnimationType == 0 || AnimationType == 1)
	{
		Index = 0;
		DelayTime = 1.0f;
		Type = 0;
	}
	else if (AnimationType == 2 || AnimationType == 3)
	{
		Index = 0;
		DelayTime = 1.0f;
		Type = 1;
	}
	else if (AnimationType == 4 || AnimationType == 5)
	{
		Index = 1;
		DelayTime = 1.5f;
		Type = 2;
	}
	auto nodeEffect = CSLoader::createNode(CSB_NAME[Index]);
	nodeEffect->setPosition(Vec2(_winSize.width * 0.5, _winSize.height * 0.5));
	auto ation = CSLoader::createTimeline(CSB_NAME[Index]);
	ation->gotoFrameAndPlay(0, false);
	nodeEffect->runAction(ation);
	nodeEffect->runAction(Sequence::create(DelayTime::create(DelayTime), CallFunc::create([=]() {
		AddAnimation(Type, AwardNum, nodeEffect);
	}), DelayTime::create(2.0f), RemoveSelf::create(), 0));
	this->addChild(nodeEffect);

	if (onReceiveCallBack != nullptr)
	{
		onReceiveCallBack();
		//onReceiveCallBack = nullptr;
	}
}

void VipRoomController::AddAnimation(int Type, int AwardNum, Node* node)
{
	if (Type == 0)
	{
		Sprite* sp = Sprite::create("platform/Games/res/tx_jb.png");
		sp->setPosition(Vec2(-57,116.5));
		node->addChild(sp);

		Text* txt = Text::create();//Text::create(AwardNum, "platform/common/hkljhtjw8.ttc", 48);
		txt->setFontName("platform/common/hkljhtjw8.ttc");
		txt->setFontSize(48);
		txt->setColor(Color3B(255, 0, 0));
		txt->setPosition(Vec2(71.5, 113.5));
		txt->setString(StringUtils::format("+%d",AwardNum));
		node->addChild(txt);
	}
	else if (Type == 1)
	{
		Sprite* sp = Sprite::create("platform/Games/res/tx_fk.png");
		sp->setPosition(Vec2(-57, 116.5));
		node->addChild(sp);

		Text* txt = Text::create();//Text::create(AwardNum, "platform/common/hkljhtjw8.ttc", 48);
		txt->setFontName("platform/common/hkljhtjw8.ttc");
		txt->setFontSize(48);
		txt->setColor(Color3B(255, 0, 0));
		txt->setPosition(Vec2(71.5, 113.5));
		txt->setString(StringUtils::format("+%d", AwardNum));
		node->addChild(txt);
	}
	else if (Type == 2)
	{
		Text* txt = Text::create();//Text::create(AwardNum, "platform/common/hkljhtjw8.ttc", 48);
		txt->setAnchorPoint(Vec2(0, 0.5));
		txt->setFontName("platform/common/hkljhtjw8.ttc");
		txt->setFontSize(28);
		txt->setColor(Color3B(255, 0, 0));
		txt->setPosition(Vec2(-20, 71.5));
		txt->setString(StringUtils::format(GBKToUtf8("+%.2f元"), (float)AwardNum / 100));
		node->addChild(txt);
	}
}

