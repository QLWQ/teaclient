/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CreateRoomLayer.h"
#include "JoinRoomLayer.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "ui/UICheckBox.h"
#include "../RoomManager/RoomManager.h"
#include "../../GameReconnection/Reconnection.h"
#include "../../GameLocation/GameLocation.h"
//按钮点击所需
#include "../../GameRecord/RecordList/GameRoomRecord.h"
#include "../../GameChildLayer/ServiceLayer.h"
#include "../../GameChildLayer/GameRules.h"
#include "../../GameChildLayer/GameSetLayer.h"
#include "../../GameMenu/GameMenu.h"
#include "CreateMatchLayer.h"
#include "../../GameBaseLayer/GamePlatform.h"
#include "../../GameNotice/GameNotice.h"
#include "../../GameNotice/HNNoticeLayer.h"

//切换账户
#include "HNOpenExport.h"
//输入金币房间密码
#include "../../GameChildLayer/GamePasswordInput.h"

//更新游戏
#include "../../GameUpdate/GameResUpdate.h"
//////////////////////////////////////////////////////////
static const char* connect_room_text = "连接房间......";
static const char* login_room_text = "登陆房间......";
static const char* request_room_info_text = "获取房间数据......";
static const char* allocation_table_please_wait_text = "正在配桌，请稍后......";
static const char* enterGame_please_wait_text = "正在进入游戏，请稍后......";
//////////////////////////////////////////////////////////
extern int ClubID;
// 对话框布局文件
static const char* CREATEROOM_CSB	= "platform/createRoomUI/createUi/createUi_Node.csb";
static const char* NEWSCENE_CSB = "platform/createRoomUI/createUi/Node_Dzz.csb";

static const char* USER_HEAD_MASK = "platform/common/touxiangdi.png";
static const char* USER_MEN_HEAD = "platform/head/men_head.png";
static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";

#define	UG_HUA_MASK					0xF0			//1111 0000
#define	UG_VALUE_MASK				0x0F			//0000 1111
////////////////////扑克花色//////////////////////////////
#define UG_FANG_KUAI				0			//方块	0000 0000
#define UG_MEI_HUA					1			//梅花	0001 0000
#define UG_HONG_TAO					2			//红桃	0010 0000
#define UG_HEI_TAO					3			//黑桃	0011 0000
///////////////////////////////////////////////////////////////////
//层级
static const int PLATFORM_ZORDER_UI = 100;
static const int CHILD_LAYER_ZORDER = PLATFORM_ZORDER_UI + 4;		// 子节点弹出框层级

//tag
static const int CHILD_LAYER_TAG = 1000;
static const int CHILD_LAYER_SET_TAG = CHILD_LAYER_TAG + 3;			// 设置层标签
static const int CHILD_LAYER_RECORD_TAG = CHILD_LAYER_TAG + 2;		// 战绩列表层标签
static const int CHILD_LAYER_SERVICE_TAG = CHILD_LAYER_TAG + 5;			// 服务层标签
static const int CHILD_LAYER_SPREAD_TAG = CHILD_LAYER_TAG + 4;			// 推广员层标签

//跑得快2人以及3人局数控制（局数3配置未按照服务端配置来）
static const int PLAYER_2_BUYCOUNT = 24;
static const int PLAYER_3_BUYCOUNT = 32;

#define  NORMAL_TEXT_COLOR		Color4B(161,86,41,255)     //未选中颜色以及描边颜色
#define  SELECT_TEXT_COLOR		Color4B(255,241,124,255)  //选中文本颜色

#define	NORMAL_REDFLOWER_COLOR		Color4B(122,122,122,255)	//未选中颜色
#define	SELECT_REDFLOWER_COLOR		Color4B(130,75,41,255)  //选中颜色

#define  BTN_SELECT_IMG			"platform/createRoomUI/createUi/res/new/btn_1.png"		//左侧列表通用选中按钮
#define  BTN_NORMAL_IMG			"platform/createRoomUI/createUi/res/new/btn_2.png"		//左侧列表通用选中按钮
#define  BTN_SIZE				Size(192,63)							//左侧按钮大小
#define  BTN_NORMAL_COLOR		Color3B(130,75,41)						//左侧按钮未选中颜色				
#define  BTN_SELECT_COLOR		Color3B(225,225,225)					//左侧按钮选中颜色
#define  BTN_FONTNAME			"platform/common/blcak.ttf"			//左侧按钮字体
#define  BTN_FONTSIZE			30										//左侧按钮字体大小

enum CREATE_TYPE
{
	CREATE_VIPROOM = 1,		//所有游戏的创建房间
	CREATE_GLODROOM,		//金币场房间
	CREATE_TYPEROOM_POKE,			//分类创建房间(扑克)
	CREATE_TYPEROOM_MAJIO,			//分类创建房间(麻将)
	CREATE_TEAHOUSE_ROOM,			//茶楼创建楼层
};

static const UINT POKE_GAMELIST[] = { 20200310, 20200311, 12100004, 12345678, 10100003/*, 20170708*/ };		//算牌跑得快（16张）， 上游跑得快（15张）,十三水，跑得快，斗地主
static const UINT MAJIO_GAMELIST[] = { 20171121, 20171123, 20200321};//20171121, 20171123, 20200321 };//,20171121, 20171123, };//20200309 ，20200321};泉州麻将，晋江麻将
static const UINT TEAHOUSE_GAMELIST[] = { 20170708, 20200310, 20171121 };

//static const std::string OUTCARD_LIST[] = { "15秒", "30秒", "60秒", "90秒" };
//static const int OUTCARD_TIME[] = { 15, 30, 60, 90 };
//static const int MIN_OUTCARD_NUM = 0;
//static const int MAX_OUTCARD_NUM = 3; //(0-2)

#define	 NORMAL_FK_IMG_FILE		"platform/createRoomUI/createUi/res/new/f_2.png"	//未选中房卡图标
#define	 SELECT_FK_IMG_FILE		"platform/createRoomUI/createUi/res/new/f_1.png"   //房卡选中图标
static int _notice_num = 1;  //请求通知公告的序号
CreateRoomLayer::CreateRoomLayer()
:_isTouch(true)
{
	_jushuVec.clear();
	_roomLogic = new HNRoomLogicBase(this);
	_iLocation = 1;  //默认开启定位
}

// static const int gameIds[] = { 20171121, 20171122, 20200309, 20171123, 10100003, 12345678, 20200310, 20200311, 20200321, 12100004, 20170708, 0 };
// static int gameIndex = 0;
// void updateGames(int idx) {
// 	// 检测是否需要更新
// 	auto update = GameResUpdate::create();
// 	update->retain();
// 	update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {
// 		update->release();
// 		gameIndex++;
// 		if (gameIds[gameIndex] != 0) {
// 			updateGames(gameIndex);
// 		}
// 	};
// 	int gameId = gameIds[idx];
// 	update->checkUpdate(gameId, true);
// }
// 

CreateRoomLayer::~CreateRoomLayer()
{
	_PickFlowerSelectCheckBoxs.clear();
	_panel_text_make.clear();
	_Text_tip.clear();
	HNHttpRequest::getInstance()->removeObserver(this);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_BUY_DESK);
	_roomLogic->stop();
	HN_SAFE_DELETE(_roomLogic);
}

void CreateRoomLayer::closeFunc()
{
	_roomLogic->stop();
	if (onCloseCallBack) onCloseCallBack();
}

//CreateType设置创建房间类型（房卡场创建房间列表为1，金币场创建房间为2）
CreateRoomLayer* CreateRoomLayer::create(int CreateType)
{
	CreateRoomLayer *pRet = new CreateRoomLayer();
	if (pRet && pRet->init(CreateType))
	{
		pRet->autorelease();
	}
	else
	{
		delete pRet;
		pRet = nullptr;
	}

	return pRet;
}

bool CreateRoomLayer::init(int CreateType)
{
	if (!HNLayer::init()) return false;
	
// 	gameIndex = 0;
// 	updateGames(gameIndex);

	auto node = CSLoader::createNode("platform/createRoomUI/createUi/createUi_Node.csb");
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize / 2);
	addChild(node);

	auto img_BG = dynamic_cast<ImageView*>(node->getChildByName("Image_BG"));

	if (_winSize.width / _winSize.height > 1.78f)
	{
		img_BG->setScale(0.9f);
	}

	auto btn_close = dynamic_cast<Button*>(img_BG->getChildByName("Button_close"));
	if (btn_close) btn_close->addClickEventListener([=](Ref*){
		close();
	});

	_btnBack = dynamic_cast<Button*>(img_BG->getChildByName("Button_back"));
	_btnBack->addClickEventListener([=](Ref*){
		PageGetBack();
	});

	//用户信息
	auto sp_top = dynamic_cast<Sprite*>(img_BG->getChildByName("Panel_top_mask")->getChildByName("sp_top"));
	_Text_RoomCard = dynamic_cast<Text*>(sp_top->getChildByName("text_RoomCard"));
	_Text_GoldNum = dynamic_cast<Text*>(sp_top->getChildByName("text_GoldNum"));
	_Text_Integral = dynamic_cast<Text*>(sp_top->getChildByName("text_Integral"));

	//游戏列表
	_list_RoomGame = dynamic_cast<ListView*>(img_BG->getChildByName("ListView_RoomGame"));
	_list_RoomGame->setScrollBarEnabled(false);
	_list_GoldGame = dynamic_cast<ListView*>(img_BG->getChildByName("ListView_GoldGame"));
	_list_GoldGame->setScrollBarEnabled(false);
	_list_RoomInfo = dynamic_cast<ListView*>(img_BG->getChildByName("ListView_RoomInfo"));
	_list_RoomInfo->setScrollBarEnabled(false);

	//游戏按钮
	_panel_bottom = dynamic_cast<Layout*>(img_BG->getChildByName("panel_bottom"));

	//按钮
	auto btn_record = dynamic_cast<Button*>(_panel_bottom->getChildByName("btn_record"));
	btn_record->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::menuBottomCallBack, this));
	//btn_record->setVisible(false);
	auto btn_service = dynamic_cast<Button*>(_panel_bottom->getChildByName("btn_service"));
	btn_service->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::menuBottomCallBack, this));
	btn_service->setVisible(false);
	auto btn_help = dynamic_cast<Button*>(_panel_bottom->getChildByName("btn_help"));
	btn_help->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::menuBottomCallBack, this));
	auto btn_set = dynamic_cast<Button*>(_panel_bottom->getChildByName("btn_set"));
	btn_set->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::menuBottomCallBack, this));

	// 玩家信息结构体
	MSG_GP_R_LogonResult& LogonResult = PlatformLogic()->loginResult;
	auto headFarme = dynamic_cast<ImageView*>(_panel_bottom->getChildByName("Image_headFrame"));
	auto tempHead = dynamic_cast<ImageView*>(_panel_bottom->getChildByName("Image_tempHead"));
	// 头像
	auto _userHead = GameUserHead::create(tempHead, headFarme);
	_userHead->show(USER_HEAD_MASK);
	_userHead->loadTexture(LogonResult.bBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);
	_userHead->setHeadByFaceID(LogonResult.bLogoID);
	_userHead->loadTextureWithUrl(LogonResult.headUrl);
	_userHead->setVIPHead("", LogonResult.iVipLevel);

	auto text_nickname = dynamic_cast<Text*>(_panel_bottom->getChildByName("text_nickname"));
	if (LogonResult.iVipLevel > 0)
	{
		text_nickname->setColor(Color3B(255, 0, 0));
	}
	else
	{
		text_nickname->setColor(Color3B(255, 255, 255));
	}

	std::string nickName(LogonResult.nickName);
	if (!nickName.empty())
	{
		string str = LogonResult.nickName;
		if (str.length() > 10)
		{
			str.erase(11, str.length());
			str.append("...");
			text_nickname->setString(GBKToUtf8(str));
		}
		else
		{
			text_nickname->setString(GBKToUtf8(str));
		}
	}
	else
	{
		text_nickname->setString(GBKToUtf8("未知"));
	}

	//VIP等级
	auto text_vip_level = dynamic_cast<Text*>(_panel_bottom->getChildByName("text_vip_level"));
	text_vip_level->setString(StringUtils::format("VIP %d", LogonResult.iVipLevel));
	if (LogonResult.iVipLevel > 0)
	{
		text_vip_level->setColor(Color3B(255, 0, 0));
	}

	//ID
	auto text_ID = dynamic_cast<Text*>(_panel_bottom->getChildByName("text_ID"));
	/*if (LogonResult.iVipLevel > 0)
	{
		text_ID->setColor(Color3B(166, 89, 30));
	}*/
	text_ID->setString(StringUtils::format("ID:%d", LogonResult.dwUserID));

	_layout_gameList = (Layout*)img_BG->getChildByName("Panel_gamelist");
	_layout_gameList->setVisible(false);

	_layout_PanelBlack = (Layout*)node->getChildByName("Panel_black");
	if (_winSize.width / _winSize.height > 1.78f)
	{
		_layout_PanelBlack->setScale(0.9f);
	}
	
	_layout_games = dynamic_cast<Layout*>(_layout_PanelBlack->getChildByName("Panel_Games"));
	_list_btn = dynamic_cast<ListView*>(_layout_PanelBlack->getChildByName("ListView_Btn"));
	_list_btn->setScrollBarEnabled(false);
	_list_btn->setItemsMargin(15.0f);

	_btnCard = dynamic_cast<Button*>(img_BG->getChildByName("Button_card"));
	_btnCard->setVisible(false);
	_btnCard->addClickEventListener([=](Ref*){
		createGameList(1);
	});
	_btnGold = dynamic_cast<Button*>(img_BG->getChildByName("Button_gold"));
	_btnGold->setVisible(false);
	_btnGold->addClickEventListener([=](Ref*){
		createGameList(2);
	});
	_btnMatch = dynamic_cast<Button*>(img_BG->getChildByName("Button_match"));
	_btnMatch->setVisible(false);
	_btnMatch->addClickEventListener([=](Ref*){
		createGameList(3);
	});
	//_Particle = dynamic_cast<ParticleSystemQuad*>(img_BG->getChildByName("Particle_4"));
	if (GameCreator()->GetCurrentselectSine() == 1)
	{
		//_btnCard->setVisible(false);
		//_btnGold->setVisible(false);
		//_btnMatch->setVisible(false);
	}
	_createButtons.pushBack(_btnCard);
	_createButtons.pushBack(_btnGold);
	_createButtons.pushBack(_btnMatch);

	for (auto game : _layout_games->getChildren()) {
		game->setVisible(false);
	}
	auto creatCloseBtn = (Button*)_layout_PanelBlack->getChildByName("Button_creatClose");
	creatCloseBtn->addClickEventListener([this](Ref* sender)
	{
		_layout_PanelBlack->setVisible(false);
		if (_select_game)
		{
			_select_game = nullptr;
			_firthSelectCheckBoxs.clear();
			_secondSelectCheckBoxs.clear();
			_thirdSelectCheckBoxs.clear();
			_fourthSelectCheckBoxs.clear();
			_fifthSelectCheckBoxs.clear();
			_sixthSelectCheckBoxs.clear();
			_seventhSelectCheckBoxs.clear();
			_fufeiRuleCheckBoxs.clear();
			_renshuSelectCheckBoxs.clear();
			_wanfaSelectCheckBoxs.clear();
			_outcardSelectCheckBoxs.clear();
			_outbombSelectCheckBoxs.clear();
			_begincardSelectCheckBoxs.clear();
			_deskSelectCheckBoxs.clear();
			_NNPaixing = nullptr;
			_NNFanbei = nullptr;
			_NNzhuangjia = nullptr;
			_Button_change = nullptr;
			_Text_jiama = nullptr;
			m_checkBoxMethod = nullptr;
			m_DisarmBomb = nullptr;
			_cardValue = 0;
			_iPlayerNum = 0;
			m_Text_OutCardTime = nullptr;
		}
		if (onCloseCallBack) onCloseCallBack();
	});

	_btn_create = dynamic_cast<Button*>(_layout_PanelBlack->getChildByName("Button_create"));
	_btn_create->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::onChickCreateCallBack, this));

	_Text_CostNum = dynamic_cast<Text*>(_layout_PanelBlack->getChildByName("text_CostNum"));

	//红花选项
	auto Button_RedFlower = (Button*)_layout_PanelBlack->getChildByName("Button_RedFlower");
	//是否开启红花房选项
	Button_RedFlower->setVisible(ClubID > 0);
	Button_RedFlower->addClickEventListener([=](Ref*){
		if (_Panel_RedFlower)
		{
			_Panel_RedFlower->setVisible(true);
		}
	});

	//是否勾选红花选项
	_sp_RedFlower = (Sprite*)Button_RedFlower->getChildByName("Sprite_RedFlower");

	//红花选项页面
	_Panel_RedFlower = (Layout*)_layout_PanelBlack->getChildByName("Panel_RedFlower");
	
	//关闭红花选项
	auto Button_Close = (Button*)_Panel_RedFlower->getChildByName("Button_Close");
	Button_Close->addClickEventListener([=](Ref*){
		_Panel_RedFlower->setVisible(false);
	});

	//确定按钮
	auto Button_Sure = (Button*)_Panel_RedFlower->getChildByName("Button_Sure");
	Button_Sure->addClickEventListener([=](Ref*){
		_Panel_RedFlower->setVisible(false);
	});

	//红花复选基础容器
	Panel_CheckBox = (Layout*)_Panel_RedFlower->getChildByName("Panel_CheckBox");

	//初始化红花选项界面
	initRedFlowerCheck();

	auto btn_manager = dynamic_cast<Button*>(img_BG->getChildByName("Button_manager"));
	btn_manager->addClickEventListener([=](Ref*) {
	
		//runAction(FadeOut::create(0.3f));
		 
		auto record = RoomManager::create();
		record->setName("manager");
		record->openManager(this->getParent(), Vec2::ZERO);
		record->onRefreshCallBack = [=]() {

			if (onUpdateJewelCallBack)
			{
				onUpdateJewelCallBack();
			}
		};

// 		record->onCloseCallBack = [=]() {
// 
// 			runAction(FadeIn::create(0.3f));
// 		};
	});

// 	_listView_games = dynamic_cast<ui::ListView*>(img_BG->getChildByName("ListView_games"));
// 	_listView_games->setScrollBarPositionFromCornerForVertical(Vec2(3, 20));
// 	_listView_games->setScrollBarWidth(6.f);

	UpdateUsetInfo();

	if (CreateType == CREATE_VIPROOM)
	{
		img_BG->setVisible(true);
		_layout_PanelBlack->setVisible(false);
		//游戏列表容器压入
		CreateRoomGameList();
		createGameList(CreateType);
	}
	else if (CreateType == CREATE_TYPEROOM_POKE || 
		CREATE_TYPEROOM_MAJIO == CreateType || CreateType == CREATE_TEAHOUSE_ROOM)
	{
		_layout_PanelBlack->setVisible(true);
		img_BG->setVisible(false);
		//更新创建房间界面
		updataGamelist(CreateType);
	}
	else if (CreateType == CREATE_GLODROOM)
	{
		img_BG->setVisible(true);
		_layout_PanelBlack->setVisible(false);
		//游戏列表容器压入
		CreateGoldGameList();
		createGameList(CreateType);
	}

	
	// 创建游戏列表
	//runAction(Sequence::create(DelayTime::create(0.1f),nullptr))//, CallFunc::create([=](){  createGameList(1);  }), nullptr));
	return true;
}

void CreateRoomLayer::setTeaHouseID(int TeaHouseID)
{
	m_iTeaHouseID = TeaHouseID;
}

void CreateRoomLayer::setTeaHouseGameID(int GameID)
{
	m_iTeaHouseGameID = GameID;
}

int CreateRoomLayer::getCardnum(BYTE card)
{
	return (card&UG_VALUE_MASK) + 1;
}


std::string  CreateRoomLayer::get_userDefined()
{
	return _userDefined;
}

void CreateRoomLayer::CreateRoomGameList()
{
	std::vector<ComNameInfo*> gameNames;

	// 已过滤掉纯vip房间（比赛场不用过滤，接收到房间列表消息时已经过滤）
	gameNames = GameCreator()->getValidGames();
	int index = 0;
	Size BtnSize = Size::ZERO;
	for (auto game : gameNames)
	{
		//房卡游戏
		std::string nodeName = StringUtils::format("Node_%u", game->uNameID);
		if (FileUtils::getInstance()->isFileExist("platform/createRoomUI/createUi/res/new/" + nodeName + ".png"))
		{
			index = index + 1;
		}
		//泉州麻将
		if (game->uNameID == 20171121)
		{
			_QZgame_info = game;
		}
		else if (game->uNameID == 20171123)
		{
			_JJgame_info = game;
		}
	}

	if (index == 1)
	{
		//一个有效游戏设置两个区域（1个敬请期待）
		BtnSize = Size(640, 500);
	}
	else if (index == 2)
	{
		BtnSize = Size(427, 500);
	}
	else if (index == 3)
	{
		BtnSize = Size(320, 500);
	}
	else
	{
		BtnSize = Size(331, 500);
	}

	for (auto game : GameCreator()->getValidGames())
	{
		//房卡房加入
		std::string nodeName = StringUtils::format("Node_%u", game->uNameID);
		//创建房间文件存在
		if (FileUtils::getInstance()->isFileExist("platform/createRoomUI/createUi/games/" + nodeName + ".csb"))
		{
			//图标存在
			if (FileUtils::getInstance()->isFileExist("platform/createRoomUI/createUi/res/new/" + nodeName + ".png"))
			{
				auto node = CSLoader::createNode("platform/createRoomUI/createUi/GameItemLayer.csb");
				auto btnCoin = dynamic_cast<Layout*>(node->getChildByName("panel_clone"));
				btnCoin->setContentSize(BtnSize);
				auto btn = dynamic_cast<Button*>(btnCoin->getChildByName("btn_game"));
				if (index == 1)
				{
					btn->setPosition(Vec2(btnCoin->getContentSize().width / 2 + 60, btnCoin->getContentSize().height / 2));
				}
				else
				{
					btn->setPosition(Vec2(btnCoin->getContentSize().width / 2, btnCoin->getContentSize().height / 2));
				}
				auto img = ImageView::create("platform/createRoomUI/createUi/res/new/" + nodeName + ".png");
				btn->loadTextures("platform/createRoomUI/createUi/res/new/" + nodeName + ".png", "",
					"platform/createRoomUI/createUi/res/new/" + nodeName + ".png");
				btn->setUserData(game);
				btn->setContentSize(img->getSize());
				btn->setTag(game->uNameID);
				btn->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::gameClickCallback, this));
				btnCoin->removeFromParentAndCleanup(true);
				_list_RoomGame->pushBackCustomItem(btnCoin);
				//_list_RoomGame->setVisible(true);
			}
		}
	}

	//压入敬请期待
	auto node = CSLoader::createNode("platform/createRoomUI/createUi/GameItemLayer.csb");
	auto btnCoin = dynamic_cast<Layout*>(node->getChildByName("panel_clone"));
	btnCoin->setContentSize(BtnSize);
	auto btn = dynamic_cast<Button*>(btnCoin->getChildByName("btn_game"));
	if (index == 1)
	{
		btn->setPosition(Vec2(btnCoin->getContentSize().width / 2 - 60, btnCoin->getContentSize().height / 2));
	}
	else
	{
		btn->setPosition(Vec2(btnCoin->getContentSize().width / 2, btnCoin->getContentSize().height / 2));
	}
	auto img = ImageView::create("platform/createRoomUI/createUi/res/new/jqqd.png");
	btn->loadTextures("platform/createRoomUI/createUi/res/new/jqqd.png", "",
		"platform/createRoomUI/createUi/res/new/jqqd.png");
	//btn->setUserData(game);
	//btn->setTag(game->uNameID);
	btn->setContentSize(img->getSize());
	btn->addClickEventListener([=](Ref*){
		//GamePromptLayer::create()->showPrompt(GBKToUtf8("敬请期待！"));
	});
	btnCoin->removeFromParentAndCleanup(true);
	_list_RoomGame->pushBackCustomItem(btnCoin);
}

void CreateRoomLayer::updataGamelist(int Gametype)
{
	std::vector<ComNameInfo*> gameNames = GameCreator()->getValidGames();
	// 已过滤掉纯vip房间（比赛场不用过滤，接收到房间列表消息时已经过滤）
	std::vector<UINT> temp_GameList;
	if (Gametype == CREATE_TYPEROOM_POKE)
	{
		for (int i = 0; i < sizeof(POKE_GAMELIST) / sizeof(UINT); i++)
		{
			auto itr = std::find_if(gameNames.begin(), gameNames.end(), [&](ComNameInfo* val){
				return val->uNameID == POKE_GAMELIST[i];
			});
			if (itr != gameNames.end())
				temp_GameList.push_back(POKE_GAMELIST[i]);
		}
		//俱乐部开启所有游戏创建界面
		/*if (ClubID > 0)
		{
		for (int i = 0; i < sizeof(MAJIO_GAMELIST) / sizeof(UINT); i++)
		{
		auto itr = std::find_if(gameNames.begin(), gameNames.end(), [&](ComNameInfo* val){
		return val->uNameID == MAJIO_GAMELIST[i];
		});
		if (itr != gameNames.end())
		temp_GameList.push_back(MAJIO_GAMELIST[i]);
		}
		}*/
	}
	else if (Gametype == CREATE_TYPEROOM_MAJIO)
	{
		for (int i = 0; i < sizeof(MAJIO_GAMELIST) / sizeof(UINT); i++)
		{
			auto itr = std::find_if(gameNames.begin(), gameNames.end(), [&](ComNameInfo* val){
				return val->uNameID == MAJIO_GAMELIST[i];
			});
			if (itr != gameNames.end())
				temp_GameList.push_back(MAJIO_GAMELIST[i]);
		}
	}
	else if (Gametype == CREATE_TEAHOUSE_ROOM)
	{
		for (int i = 0; i < sizeof(TEAHOUSE_GAMELIST) / sizeof(UINT); i++)
		{
			auto itr = std::find_if(gameNames.begin(), gameNames.end(), [&](ComNameInfo* val){
				return val->uNameID == TEAHOUSE_GAMELIST[i];
			});
			if (itr != gameNames.end())
				temp_GameList.push_back(TEAHOUSE_GAMELIST[i]);
		}
	}
	if (_list_btn)
	{
		_list_btn->removeAllItems();
	}
	gameNames = GameCreator()->getValidGames();
	if (_list_btn)
	{
		for (int i = 0; i < temp_GameList.size(); i++)
		{
			string szTitle = "";
			switch (temp_GameList[i])
			{
			case 20171121:
				szTitle = GBKToUtf8("泉州麻将");
				break;
			case 20171122:
				szTitle = GBKToUtf8("厦门麻将");
				break;
			case 20200309:
				szTitle = GBKToUtf8("漳州麻将");
				break;
			case 20171123:
				szTitle = GBKToUtf8("晋江麻将");
				break;
			case 10100003:
				szTitle = GBKToUtf8("斗地主");
				break;
			case 12345678:
				szTitle = GBKToUtf8("跑得快");
				break;
			case 20200310:
				szTitle = GBKToUtf8("算牌跑得快");
				break;
			case 20200311:
				szTitle = GBKToUtf8("上游跑得快");
				break;
			case 20200321:
				szTitle = GBKToUtf8("漳浦麻将");
				break;
			case 12100004:
				szTitle = GBKToUtf8("十三水");
				break;
			case 20170708:
				szTitle = GBKToUtf8("牛牛");
				break;
			}	
			//创建相关游戏列表信息
			Button* btn = Button::create();
			//btn->setTitleColor(i == 0 ? BTN_SELECT_COLOR : BTN_NORMAL_COLOR);
			btn->loadTextureNormal(i == 0 ? BTN_SELECT_IMG : BTN_NORMAL_IMG);
			btn->setSize(BTN_SIZE);
			Text* str = Text::create(szTitle, BTN_FONTNAME, BTN_FONTSIZE);
			str->setPosition(Vec2(85,33));
			str->setColor(i == 0 ? BTN_SELECT_COLOR : BTN_NORMAL_COLOR);
			str->setName("btn_GameName");
			btn->addChild(str);
			/*btn->setTitleText(szTitle);
			btn->setTitleFontName(BTN_FONTNAME);
			btn->setTitleFontSize(BTN_FONTSIZE);*/
			btn->setName(szTitle);
			btn->setTag(temp_GameList[i]);
			//btn->ignoreContentAdaptWithSize(false);
			btn->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::SwitchGameBtnCallBack, this));
			_list_btn->pushBackCustomItem(btn);
		}
	}
	if (temp_GameList.size() > 0)
	{
		//默认设置第一个游戏请求
		GameCreator()->setCurrentGame(temp_GameList[0]);
		// 获取创建房间配置信息
		getCreateRoomConfig(temp_GameList[0]);
	}
}

void CreateRoomLayer::CreateRoomInfoList()
{
	std::vector<ComRoomInfo*> RoomInfo;
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	//printf("Windows=============");
	int CurrentIndex = RoomInfoModule()->getRoomCount();
	for (int currentIndex = 0; currentIndex < CurrentIndex; currentIndex++)
	{
		RoomInfo.push_back(RoomInfoModule()->getRoom(currentIndex));
	}
	////gameNames = GameCreator()->getValidGames();
	int index = 0;
	int Level = 0;
	Size BtnSize = Size::ZERO;
	for (auto game : RoomInfo)
	{
	//	//
		Level++;
		std::string nodeName = StringUtils::format("RoomInfo_%d", Level - 1);
		if (FileUtils::getInstance()->isFileExist("platform/createRoomUI/createUi/res/new/" + nodeName + ".png"))
		{
			index = index + 1;
		}
	}

	if (index == 1)
	{
		//一个有效游戏设置两个区域（1个敬请期待）
		BtnSize = Size(640, 500);
	}
	else if (index == 2)
	{
		BtnSize = Size(427, 500);
	}
	else if (index == 3)
	{
		BtnSize = Size(320, 500);
	}
	else if (index == 0)
	{
		BtnSize = Size(1280, 500);
	}
	else
	{
		BtnSize = Size(331, 500);
	}

	int level = 0;
	_list_RoomInfo->removeAllItems();
	for (auto game : RoomInfo)
	{
		//金币房加入
		level++;
		std::string nodeName = StringUtils::format("RoomInfo_%d", level - 1);
		//图标存在
		if (FileUtils::getInstance()->isFileExist("platform/createRoomUI/createUi/res/new/" + nodeName + ".png"))
		{
			auto node = CSLoader::createNode("platform/createRoomUI/createUi/GameItemLayer.csb");
			auto btnCoin = dynamic_cast<Layout*>(node->getChildByName("panel_clone"));
			btnCoin->setContentSize(BtnSize);
			auto btn = dynamic_cast<Button*>(btnCoin->getChildByName("btn_game"));
			if (index == 2)
			{
				//两个居中靠处理
				if (level == 1)
				{
					btn->setPosition(Vec2(btnCoin->getContentSize().width / 2 + 60, btnCoin->getContentSize().height / 2));
				}
				else
				{
					btn->setPosition(Vec2(btnCoin->getContentSize().width / 2 - 60, btnCoin->getContentSize().height / 2));
				}
			}
			else
			{
				btn->setPosition(Vec2(btnCoin->getContentSize().width / 2, btnCoin->getContentSize().height / 2));
			}
			btn->loadTextures("platform/createRoomUI/createUi/res/new/" + nodeName + ".png", "",
				"platform/createRoomUI/createUi/res/new/" + nodeName + ".png");
			btn->setUserData(game);
			//btn->setTag(game->uNameID);
			btn->addTouchEventListener(CC_CALLBACK_2(CreateRoomLayer::enterRoomEventCallBack, this));
			btnCoin->removeFromParentAndCleanup(false);
			_list_RoomInfo->pushBackCustomItem(btnCoin);
			//_list_RoomInfo->setVisible(true);
		}
	}
	createGameList(4);
}

void CreateRoomLayer::CreateGoldGameList()
{
	std::vector<ComNameInfo*> gameNames;

	// 已过滤掉纯vip房间（比赛场不用过滤，接收到房间列表消息时已经过滤）
	gameNames = GameCreator()->getNormalGames();
	int index = 0;
	Size BtnSize = Size::ZERO;
	for (auto game : gameNames)
	{
		//金币游戏
		std::string nodeName = StringUtils::format("Node_%u", game->uNameID);
		if (FileUtils::getInstance()->isFileExist("platform/createRoomUI/createUi/res/new/gold" + nodeName + ".png"))
		{
			index = index + 1;
		}
	}

	if (index == 1)
	{
		//一个有效游戏设置两个区域（1个敬请期待）
		BtnSize = Size(331, 368);
	}
	else if (index == 2)
	{
		BtnSize = Size(331, 368);
	}
	else if (index == 3)
	{
		BtnSize = Size(331, 368);
	}
	else if (index == 0)
	{
		BtnSize = Size(1280, 368);
	}
	else
	{
		BtnSize = Size(331, 368);
	}

	for (auto game : GameCreator()->getNormalGames())
	{
		//金币游戏
		std::string nodeName = StringUtils::format("Node_%u", game->uNameID);
		//图标存在
		if (FileUtils::getInstance()->isFileExist("platform/createRoomUI/createUi/res/new/gold" + nodeName + ".png"))
		{
			auto node = CSLoader::createNode("platform/createRoomUI/createUi/GameItemLayer.csb");
			auto btnCoin = dynamic_cast<Layout*>(node->getChildByName("panel_clone"));
			btnCoin->setContentSize(BtnSize);
			auto btn = dynamic_cast<Button*>(btnCoin->getChildByName("btn_game"));
			if (index == 1)
			{
				btn->setPosition(Vec2(btnCoin->getContentSize().width / 2 + 60, btnCoin->getContentSize().height / 2));
			}
			else
			{
				btn->setPosition(Vec2(btnCoin->getContentSize().width / 2, btnCoin->getContentSize().height / 2));
			}
			btn->loadTextures("platform/createRoomUI/createUi/res/new/gold" + nodeName + ".png", "",
				"platform/createRoomUI/createUi/res/new/gold" + nodeName + ".png");
			btn->setUserData(game);
			btn->setTag(game->uNameID);
			btn->addClickEventListener([=](Ref*) {
				GameCreator()->setCurrentGame(game->uNameID);
				GameCreator()->setGameKindId(game->uNameID, game->uKindID);
				//进入房间列表
				// 检测是否需要更新
				auto update = GameResUpdate::create();
				update->retain();
				update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {
					if (success)
					{
						onEnterGameCallBack();
					};
					update->release();
				};
				update->checkUpdate(game->uNameID, true);
				_LAYER_INDEX = 1;
			});
			btnCoin->removeFromParentAndCleanup(true);
			_list_GoldGame->pushBackCustomItem(btnCoin);
			//_list_GoldGame->setVisible(true);
		}
	}
	//压入敬请期待
	auto node = CSLoader::createNode("platform/createRoomUI/createUi/GameItemLayer.csb");
	auto btnCoin = dynamic_cast<Layout*>(node->getChildByName("panel_clone"));
	btnCoin->setContentSize(BtnSize);
	auto btn = dynamic_cast<Button*>(btnCoin->getChildByName("btn_game"));
	if (index == 1)
	{
		btn->setPosition(Vec2(btnCoin->getContentSize().width / 2 - 60, btnCoin->getContentSize().height / 2));
	}
	else
	{
		btn->setPosition(Vec2(btnCoin->getContentSize().width / 2, btnCoin->getContentSize().height / 2));
	}
	btn->loadTextures("platform/createRoomUI/createUi/res/new/jqqd.png", "",
		"platform/createRoomUI/createUi/res/new/jqqd.png");
	//btn->setUserData(game);
	//btn->setTag(game->uNameID);
	btn->addClickEventListener([=](Ref*){
		//GamePromptLayer::create()->showPrompt(GBKToUtf8("敬请期待！"));
	});
	btnCoin->removeFromParentAndCleanup(true);
	_list_GoldGame->pushBackCustomItem(btnCoin);
}

void CreateRoomLayer::UpdateUsetInfo()
{
	do
	{
		CC_BREAK_IF(nullptr == _Text_GoldNum);

		std::string str = HNToWan(PlatformLogic()->loginResult.i64Money);
		_Text_GoldNum->setString(str);

		CC_BREAK_IF(nullptr == _Text_RoomCard);

		std::string str1 = HNToWan(PlatformLogic()->loginResult.iJewels + PlatformLogic()->loginResult.iLockJewels);
		_Text_RoomCard->setString(str1);


		CC_BREAK_IF(nullptr == _Text_Integral);

		std::string str2 = HNToWan(PlatformLogic()->loginResult.iLotteries);
		_Text_Integral->setString(str2);
	} while (0);
}

void CreateRoomLayer::menuBottomCallBack(Ref* ref)
{
	if (!_isTouch) return;
	_isTouch = false;

	auto btn = (Button*)ref;
	btn->runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	std::string name(btn->getName());

	if (name.compare("btn_record") == 0)
	{
		auto record = GameRoomRecord::create();
		record->setName("record");
		record->open(ACTION_TYPE_LAYER::SCALE, this->getParent(), Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_RECORD_TAG);
	}
	else if (name.compare("btn_service") == 0)
	{
		auto serviceLayer = ServiceLayer::create();
		serviceLayer->setName("serviceLayer");
		serviceLayer->open(ACTION_TYPE_LAYER::SCALE, this->getParent(), Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SERVICE_TAG);
	}
	else if (name.compare("btn_help") == 0)
	{
		/*
		auto rules = GameRules::create();
		rules->setName("rules");
		rules->open(ACTION_TYPE_LAYER::SCALE, this->getParent(), Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);
		*/
		requestNotice(3);
	}
	else if (name.compare("btn_set") == 0)
	{
		auto setLayer = GameSetLayer::create();
		setLayer->setName("setLayer");
		setLayer->open(ACTION_TYPE_LAYER::SCALE, this->getParent(), Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_SPREAD_TAG);
		setLayer->onExitCallBack = [=]() {
			switchAccount();
		};
	}
}

void CreateRoomLayer::switchAccount()
{
	RoomLogic()->close();
	RoomLogic()->setRoomRule(0x0);
	PlatformLogic()->close();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

	if (PlatformLogic()->loginResult.bLogonType == WeChat)
	{
		UserDefault::getInstance()->setStringForKey(WECHAT_OPEN_ID, "");
		UserDefault::getInstance()->setStringForKey(WECHAT_ACCESS_TOKEN, "");
		UserDefault::getInstance()->setStringForKey(WECHAT_REFRESH_TOKEN, "");
		UserDefault::getInstance()->flush();

		// 删除微信平台的授权
		UMengSocial::getInstance()->doDedeteAuthorize(WEIXIN);
	}
	else if (PlatformLogic()->loginResult.bLogonType == QQLogin)
	{
		UserDefault::getInstance()->setStringForKey(QQ_OPEN_ID, "");
		UserDefault::getInstance()->setStringForKey(QQ_ACCESS_TOKEN, "");
		UserDefault::getInstance()->flush();

		// 删除QQ平台的授权
		UMengSocial::getInstance()->doDedeteAuthorize(QQ);
	}
	else {}

#endif			

	GameMenu::createMenu();
}

std::string CreateRoomLayer::getNumString()
{
	std::string str = "";
	int cardnum =getCardnum(_cardValue);
	if (cardnum > 10)
	{
		if (cardnum == 11)
		{
			str = "J";
		}
		if(cardnum == 12)
		{
			str = "Q";
		}
		if(cardnum == 13)
		{
			str = "K";
		}
		if (cardnum == 14)
		{
			str = "A";
		}

	}
	else
	{
		str = StringUtils::toString(getCardnum(_cardValue));
	}
	
	return str;
}


int CreateRoomLayer::getCardHua(BYTE card)
{
	int iHuaKind = (card&UG_HUA_MASK) >> 4;
	return iHuaKind;

}

void CreateRoomLayer::getSSSJiaMaCardType()
{
	int index = random(0, 51);
	BYTE cardValue[52] = {
		0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,		//方块 2 - A
		0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,		//梅花 2 - A
		0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,		//红桃 2 - A
		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D,		//黑桃 2 - A
	};
	_cardValue = cardValue[index];
	if (_Button_change)
	{
		_Button_change->setVisible(true);
	}
	std::string str = "";

	int iHuaKind = getCardHua(cardValue[index]);
	if (iHuaKind == UG_FANG_KUAI)
	{
		str = StringUtils::format("%s", GBKToUtf8("方块"));
		
	}
	else if (iHuaKind == UG_MEI_HUA)
	{
		str = StringUtils::format("%s", GBKToUtf8("梅花"));
	}
	else if (iHuaKind == UG_HONG_TAO)
	{
		str = StringUtils::format("%s", GBKToUtf8("红桃"));
	}
	else if (iHuaKind == UG_HEI_TAO)
	{

		str = StringUtils::format("%s", GBKToUtf8("黑桃"));
	}

	str = str + getNumString();
	std::string str1 = StringUtils::format("%s", GBKToUtf8("买马"));

	str = str + str1;
	if (_Text_jiama)
	{
		_Text_jiama->setString(str);
	}
}

void CreateRoomLayer::changeButtonClickCallBack(Ref* ref, Widget::TouchEventType type)
{
	auto btn = (Button*)ref;
	if (Widget::TouchEventType::BEGAN == type)
	{
		btn->setScale(1.1f);
	}
	if (Widget::TouchEventType::CANCELED == type)
	{
		btn->setScale(1.0f);
	}
	if (Widget::TouchEventType::ENDED == type)
	{
		btn->setScale(1.0f);
		getSSSJiaMaCardType();
	}
}

//创建游戏列表层
//void CreateRoomLayer::createGameListLayer()
//{
//	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
//
//	auto gamesLayer = GameLists::create(_layout_gameList);
//	gamesLayer->setName("gamesLayer");
//	gamesLayer->setPosition(Vec2::ANCHOR_BOTTOM_LEFT);
//
//	gamesLayer->onEnterGameCallBack = [this](){
//		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("加载中"), 22);
//
//		/*showTopAction(false, [=]() {
//
//			_layerType = ROOMLIST;
//			
//		});*/
//	};
//	_logicbase->requestRoomList();
//	addChild(gamesLayer, 1);
//
//	gamesLayer->setOpacity(0);
//	gamesLayer->setCanTouch(false);
//	gamesLayer->runAction(Sequence::create(FadeIn::create(0.3f), CallFunc::create([=]() {
//		_button_Return->setEnabled(true);
//		gamesLayer->setCanTouch(true);
//	}), nullptr));
//}

Layout* CreateRoomLayer::getGamesLayout()
{
	return _layout_gameList;
}

void CreateRoomLayer::ShowGameList()
{
	_layout_gameList->setVisible(true);
}

void CreateRoomLayer::PageGetBack()
{
	if (_LAYER_INDEX == 1)
	{
		_list_GoldGame->setVisible(true);
		auto spawn_gold = Spawn::create(MoveTo::create(0.5f, Vec2(0, 109.08)), FadeIn::create(0.5f), nullptr);
		_list_GoldGame->runAction(spawn_gold);
		auto spawn_Roominfo = Spawn::create(MoveTo::create(0.5f, Vec2(1280, 109.08)), FadeOut::create(0.5f), nullptr);
		_list_RoomInfo->runAction(spawn_Roominfo);
		_LAYER_INDEX = 0;
	}
	else
	{
		_btnBack->setVisible(false);
		auto spawn = Spawn::create(MoveTo::create(0.5f, Vec2(384, 360.00)), FadeIn::create(0.5f), nullptr);
		_btnCard->runAction(spawn);
		_btnGold->runAction(Spawn::create(MoveTo::create(0.5f, Vec2(896, 360.00)), FadeIn::create(0.5f), nullptr));
		auto spawn_room = Spawn::create(MoveTo::create(0.5f, Vec2(1280, 109.08)), FadeOut::create(0.5f), nullptr);
		_list_RoomGame->runAction(Sequence::create(spawn_room, CallFunc::create([=](){ _list_RoomGame->setVisible(false); }), nullptr));
		auto spawn_gold = Spawn::create(MoveTo::create(0.5f, Vec2(1280, 109.08)), FadeOut::create(0.5f), nullptr);
		_list_GoldGame->runAction(Sequence::create(spawn_gold, CallFunc::create([=](){ _list_GoldGame->setVisible(false); }), nullptr));
		auto spawn_Roominfo = Spawn::create(MoveTo::create(0.5f, Vec2(1280, 109.08)), FadeOut::create(0.5f), nullptr);
		_list_RoomInfo->runAction(spawn_Roominfo);
	}
}

void CreateRoomLayer::createGameList(int num)
{
	//////////////////////////////////////////
	//int j = 1;
	//for (auto btn: _createButtons)
	//{

	//	btn->setTouchEnabled(true);
	//	/*auto image_dan = dynamic_cast<ImageView*>(btn->getChildByName("Image_dan"));
	//	image_dan->loadTexture("platform/createRoomUI/createUi/res/Intactegg.png");
	//	image_dan->ignoreContentAdaptWithSize(true);*/
	//	if (num == j)
	//	{
	//		btn->setTouchEnabled(false);
	//		//image_dan->loadTexture("platform/createRoomUI/createUi/res/Brokeneggs.png");
	//		//_Particle->setPosition(btn->getPositionX() -7,btn->getPositionY() + 54);

	//	}
	//	j++;
	//}
	//////////////////////////////////////////
	//移除金币场房卡场按钮
	//auto spawn = Spawn::create(MoveTo::create(0.5f, Vec2(-160, 360)), FadeOut::create(0.5f),nullptr);
	//if (GameCreator()->GetCurrentselectSine() == 1)
	//{
	//	_btnCard->runAction(spawn);
	//}
	//else
	//{ 
	//	_btnCard->runAction(Sequence::create(spawn, CallFunc::create([=](){ _btnBack->setVisible(true); }), nullptr)); 
	//}
	//
	//_btnGold->runAction(Spawn::create(MoveTo::create(0.5f, Vec2(1440, 360)), FadeOut::create(0.5f),nullptr));

	_layout_gameList->removeAllChildren();
	switch (num)
	{
	//
	case 1:
	{
			  _list_RoomGame->setVisible(true);
			  auto spawn_room = Spawn::create(MoveTo::create(0.5f, Vec2(0, 109.08)), FadeIn::create(0.5f), nullptr);
			  _list_RoomGame->runAction(spawn_room);
			  break;
	}
	case 2:
	{
			  _list_GoldGame->setVisible(true);
			  auto spawn_gold = Spawn::create(MoveTo::create(0.5f, Vec2(0, 109.08)), FadeIn::create(0.5f), nullptr);
			  _list_GoldGame->runAction(spawn_gold);
			  //runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=](){  _createListLayer();  }), nullptr));
			  break;
	}
	case 3:
	{
			//比赛场过滤
	}
	case 4:
	{
		auto spawn_gold = Spawn::create(MoveTo::create(0.5f, Vec2(1280, 109.08)), FadeOut::create(0.5f), nullptr);
		_list_GoldGame->runAction(Sequence::create(spawn_gold, CallFunc::create([=](){ _list_GoldGame->setVisible(false); }), nullptr));
		_list_RoomInfo->setVisible(true);
		auto spawn_roominfo = Spawn::create(MoveTo::create(0.5f, Vec2(0, 109.08)), FadeIn::create(0.5f), nullptr);
		_list_RoomInfo->runAction(spawn_roominfo);
			  break;
	}
	default:
		break;
	}
}
void CreateRoomLayer::gameClickCallback(Ref* pSender)
{
	auto selectbtn = dynamic_cast<Button*>(pSender);
	//auto gamebg = dynamic_cast<ImageView*>(selectbtn->getChildByName("gamebg"));
	/*if (type == Widget::TouchEventType::BEGAN)
	{
		gamebg->setScale(1.1);
	}
	else if (type == Widget::TouchEventType::ENDED)
	{*/
		//gamebg->setScale(1.0);
		auto game = (ComNameInfo*)selectbtn->getUserData();
		GameCreator()->setCurrentGame(game->uNameID);
		// 获取创建房间配置信息
		getCreateRoomConfig(game->uNameID);
	//}
	
}

void CreateRoomLayer::checkBoxCallback(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox_round = (CheckBox*)pSender;
	if (CheckBox::EventType::UNSELECTED == type) {
		
	}
	else {
		
	}
}


void CreateRoomLayer::createRoomClickCallback(Ref* pSender)
{
	// 如果需要定位，则先定位再创建，防止成功创建但是却无法进入
	if (_iLocation || (_gameRule && _gameRule->getSelectLocation()))
	{
		GameLocation::getInstance()->onGetLocationCallBackLite = [&](bool success, float latitude, float longtitude, const std::string& addr) {

			if (success)
			{
				createRoom();
			}
			else
			{//不强制开启定位
				createRoom();
				//MessageBox(GBKToUtf8("定位失败，请在[设置]中打开定位服务或者检查网络!"), GBKToUtf8("提示"));
			}
		};

		GameLocation::getInstance()->getLocation();
	}
	else
	{
		createRoom();
	}
}

// 执行创建
void CreateRoomLayer::createRoom()
{
	// 创建桌子
	MSG_GP_S_BUY_DESK buyDesk;

	_iPay = _iPay == 1 ? 2 : _iPay;
	buyDesk.iUserID = PlatformLogic()->loginResult.dwUserID;
	buyDesk.iPlayCount = _iCount;
	buyDesk.bPositionLimit = _iLocation;
	buyDesk.bPayType = _iPay;
	buyDesk.iGameID = GameCreator()->getCurrentGameNameID();
	buyDesk.bPlayerNum = _iPlayerNum;
	buyDesk.bFinishCondition = _iType;

	PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_BUY_DESK, &buyDesk, sizeof(MSG_GP_S_BUY_DESK),
		HN_SOCKET_CALLBACK(CreateRoomLayer::createRoomMessagesEventSelector, this));

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("创建房间，请稍候..."), 28);
}

bool CreateRoomLayer::createTeaHouseRoomResult(HNSocketMessage* socketMessage)
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	if (socketMessage->messageHead.bHandleCode == 0)
	{
		MSG_GP_O_Club_CreateTeahouse* result = (MSG_GP_O_Club_CreateTeahouse*)socketMessage->object;
		if (result->isOK == 1)
		{
			log("OK!!!!!!!!!!!!!!!!!!!!");
			//创建楼层成功(刷新楼层回调)
			//HNPlatformClubData::getInstance()->TeaHouseUpdate();
			GamePromptLayer::create()->showPrompt(GBKToUtf8("操作成功"));
			close();
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8(result->szErrorInfo));
			close();
		}
	}
	return true;
}

bool CreateRoomLayer::createClubRoomResult(HNSocketMessage* socketMessage) //俱乐部的创建房间
{
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	switch (socketMessage->messageHead.bHandleCode)
	{
	case 0 : 
	{
		MSG_GP_O_Club_BuyDesk* result = (MSG_GP_O_Club_BuyDesk*)socketMessage->object;
		auto data = result->_RoomData;
		std::string psd(data.szDeskPass);
		Reconnection::getInstance()->doLoginVipRoom(psd); //进入创建房间
		//std::string tips = StringUtils::format(GBKToUtf8("创建成功，【房号：%s】，是否进入"), data.szDeskPass);
		//auto prompt = GamePromptLayer::create(true);
		//prompt->showPrompt(tips);
		//prompt->setCallBack([=]() {
		//	Reconnection::getInstance()->doLoginVipRoom(psd); //进入创建房间
		//});
		//prompt->setCancelCallBack([=](){
		//	if (onUpdateJewelCallBack)
		//	{
		//		onUpdateJewelCallBack();
		//	}
		//});
	
	}; break;
		
	case 1 : 
		GamePromptLayer::create()->showPrompt(GBKToUtf8("用户不存在")); break;
	case 2 :
		GamePromptLayer::create()->showPrompt(GBKToUtf8("部落ID错误")); break; 
	case 3 : 
		GamePromptLayer::create()->showPrompt(GBKToUtf8("玩家未加入该工会")); break; 
	case 4 : 
		GamePromptLayer::create()->showPrompt(GBKToUtf8("不可代开时不能重复购买")); break; 
	case 5 : 
		GamePromptLayer::create()->showPrompt(GBKToUtf8("部落房间每人只能创建一个")); break;
	case 6 : 
		//GamePromptLayer::create()->showPrompt(GBKToUtf8("局数错误"));
		  //break;
	case 7 : 
		GamePromptLayer::create()->showPrompt(GBKToUtf8("部落创建所需房卡不足")); break; 
	case 8 : 
		GamePromptLayer::create()->showPrompt(GBKToUtf8("桌子已购买完")); break;
	}
	return true;
}


bool CreateRoomLayer::createRoomMessagesEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(MSG_GP_S_BUY_DESK_RES, socketMessage->objectSize, true, "MSG_GP_S_BUY_DESK_RES size of error!");
	MSG_GP_S_BUY_DESK_RES* result = (MSG_GP_S_BUY_DESK_RES*)socketMessage->object;

	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	switch (socketMessage->messageHead.bHandleCode)
	{
	case 1:
	{
		if (_isInvoice)
		{
			std::string psd(result->szPassWord);
			Reconnection::getInstance()->doLoginVipRoom(psd); //gjd01 6.3
			//std::string tips = StringUtils::format(GBKToUtf8("创建成功，【房号：%s】，是否进入"), result->szPassWord);
			//auto prompt = GamePromptLayer::create(true);
			//prompt->showPrompt(tips);
			//prompt->setCallBack([=]() {
			//	Reconnection::getInstance()->doLoginVipRoom(psd); //gjd01 6.3
			//});
			//prompt->setCancelCallBack([=](){
			//	if (onUpdateJewelCallBack)
			//	{
			//		onUpdateJewelCallBack();
			//	}
			//});
		}
		else
		{
			Reconnection::getInstance()->doLoginVipRoom(result->szPassWord);
		}

	}break;
	case 2:
		 GamePromptLayer::create()->showPrompt(GBKToUtf8("用户不存在"));
		break;
	case 3:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("携带房卡不足"));
		break;
	case 4:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("用户房卡不合法"));
		break;
	case 5:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("桌子已购买完"));
		break;
	case 6:
	{
		if (GameCreator()->getCurrentGameNameID() == result->iGameNameID)
		{
			Reconnection::getInstance()->doLoginVipRoom(result->szPassWord);
		}
		else
		{
			auto info = GamesInfoModule()->getGameNameByGameID(result->iGameNameID);
			if (!info) return true;

			bool bFind = false;
			for (auto game : GameCreator()->getValidGames())
			{
				if (game->uNameID == result->iGameNameID)
				{
					bFind = true;
					break;
				}				
			}

			if (bFind)
			{
				GameCreator()->setCurrentGame(result->iGameNameID);
				std::string psd(result->szPassWord);
				std::string tips = StringUtils::format(GBKToUtf8("您已在游戏:%s中【房号：%s】，确认进入"), GBKToUtf8(info->szGameName), result->szPassWord);

				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(tips);
				prompt->setCallBack([=](){
					Reconnection::getInstance()->doLoginVipRoom(psd);
				});
			}
			else
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏信息错误，请检查服务端游戏配置"));
			}
		}
	}break;
	case 7:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("未成功购买"));
		break;
	case 8:
	{
		auto prompt =  GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("您有正在进行的游戏"));
		prompt->setCallBack([=](){			 
			Reconnection::getInstance()->getCutRoomInfo();
		});
	}break;
	case 9:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("创建的房间超过最大数量"));
		break;
	case 10:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("暂无房间"));
		break;

	default:
		break;
	}

	return true;
}

void CreateRoomLayer::getCreateRoomConfig(int gameNameID)
{
	// 获取桌子配置
	MSG_GP_I_BuyDeskConfig getConfig;
	getConfig.iGameID = gameNameID;

	PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GET_DESKCONFIG, &getConfig, sizeof(MSG_GP_I_BuyDeskConfig),
		HN_SOCKET_CALLBACK(CreateRoomLayer::getConfigEventSelector, this));

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在获取配置信息"), 28);
}

void CreateRoomLayer::SwitchGameBtnCallBack(Ref* pSender)
{
	auto btn = (Button*)pSender;
	std::string name(btn->getName());

	if (_list_btn)
	{//刷新左侧按钮显示
		auto childrens = _list_btn->getChildren();
		for (int i = 0; i < childrens.size(); i++)
		{
			auto c_btn = (Button*)childrens.at(i);
			if (c_btn != nullptr && c_btn->getName() == name)
			{
				//c_btn->setTitleColor(BTN_SELECT_COLOR);
				auto text = dynamic_cast<Text*>(c_btn->getChildByName("btn_GameName"));
				text->setColor(BTN_SELECT_COLOR);
				c_btn->loadTextureNormal(BTN_SELECT_IMG);
				c_btn->setSize(BTN_SIZE);
			}
			else
			{
				//c_btn->setTitleColor(BTN_NORMAL_COLOR);
				auto text = dynamic_cast<Text*>(c_btn->getChildByName("btn_GameName"));
				text->setColor(BTN_NORMAL_COLOR);
				c_btn->loadTextureNormal(BTN_NORMAL_IMG);
				c_btn->setSize(BTN_SIZE);
			}
		}
	}
	if (name.compare("btn_QZgame") == 0)
	{
		GameCreator()->setCurrentGame(_QZgame_info->uNameID);
		// 获取创建房间配置信息
		getCreateRoomConfig(_QZgame_info->uNameID);
	}
	else if (name.compare("btn_JJgame") == 0)
	{	
		GameCreator()->setCurrentGame(_JJgame_info->uNameID);
		// 获取创建房间配置信息
		getCreateRoomConfig(_JJgame_info->uNameID);
	}
	else
	{
		UINT NameID = (UINT)btn->getTag();
		GameCreator()->setCurrentGame(NameID);
		// 获取创建房间配置信息
		getCreateRoomConfig(NameID);
	}
}

void CreateRoomLayer::enterRoomEventCallBack(Ref* pSender, Widget::TouchEventType type)
{
	_currentSelectedRoom = dynamic_cast<Button*>(pSender);
	switch (type)
	{
	case cocos2d::ui::Widget::TouchEventType::BEGAN:
		break;
	case cocos2d::ui::Widget::TouchEventType::ENDED:
	{
		HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
		Size winSize = Director::getInstance()->getWinSize();
		ComRoomInfo* roomInfo = static_cast<ComRoomInfo*>(_currentSelectedRoom->getUserData());
		// 参数校验
		CCAssert(nullptr != roomInfo, "room is nullptr!");
		if (nullptr == roomInfo)
		{
			return;
		}

		// 检测是否需要更新
		auto update = GameResUpdate::create();
		update->retain();
		update->onUpdateCallBack = [=](bool success, cocos2d::extension::EventAssetsManagerEx::EventCode code) {

			if (success)
			{
				_isTouch = true;
				if (roomInfo->bHasPassword)
				{
					GamePasswordInput* layer = GamePasswordInput::create();
					this->addChild(layer, 1000);
					layer->setPosition(winSize / 2);
					layer->onEnterCallback = CC_CALLBACK_1(CreateRoomLayer::onEnterPasswordClickCallback, this);
					_roomLogic->start();
				}
				else
				{
					bool bLocation = true;  //默认开启定位
					if (bLocation)
					{
						GameLocation::getInstance()->onGetLocationCallBackLite = [=](bool success, float latitude, float longtitude, const std::string& addr) {
							if (success)
							{
								_roomLogic->start();
								_roomLogic->requestLogin(roomInfo->uRoomID, latitude, longtitude, addr);
								_roomID = roomInfo->uRoomID;
							}
							else
							{//不强制开启定位
								_roomLogic->start();
								_roomLogic->requestLogin(roomInfo->uRoomID);
								_roomID = roomInfo->uRoomID;
								//MessageBox(GBKToUtf8("定位失败，请在[设置]中打开定位服务或者检查网络!"), GBKToUtf8("提示"));
							}
						};
						GameLocation::getInstance()->getLocation();
					}
					else
					{
						_roomLogic->start();
						_roomLogic->requestLogin(roomInfo->uRoomID);
						_roomID = roomInfo->uRoomID;
					}
				}
			}


			update->release();
		};
		update->checkUpdate(roomInfo->uNameID, true);
		break;
	}
	default:
		break;
	}
}

void CreateRoomLayer::onEnterPasswordClickCallback(const std::string& password)
{
	ComRoomInfo* pData = static_cast<ComRoomInfo*>(_currentSelectedRoom->getUserData());
	_roomLogic->requestRoomPasword(pData->uRoomID, password);
}

void CreateRoomLayer::onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handleCode)
{
	if (success)
	{
		HNLOG_WARNING("the user enters a room complete message!");
		ComRoomInfo* roomInfo = RoomInfoModule()->getByRoomID(_roomID);
		if ((RoomLogic()->getRoomRule() & GRR_NOTCHEAT)
			&& !(RoomLogic()->getRoomRule() & GRR_QUEUE_GAME)) //单防作弊
		{
			_roomLogic->requestQuickSit();
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(enterGame_please_wait_text), 22);
		}
		else if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME) // 单排队机或者排队机+防作弊
		{
			auto loding = LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8(allocation_table_please_wait_text), 22);
			loding->setCancelCallBack([=](){
				_isTouch = true;
				_roomLogic->stop();
				RoomLogic()->close();
			});

			// 进入排队游戏
			_roomLogic->requestJoinQueue();

		}
		else // 金币场不扣积分
		{
			if (GameCreator()->getCurrentGameType() == HNGameCreator::BR
				&&roomInfo->uNameID != 11100503 || GameCreator()->getCurrentGameType() == HNGameCreator::SINGLE || GameCreator()->getCurrentGameType() == HNGameCreator::NORMAL)
			{

				m_loading = GameLading::create();
				m_loading->OnLadINGbg(roomInfo->uNameID);
				m_loading->setVisible(true);
				auto Scene = Director::getInstance()->getRunningScene();
				Scene->addChild(m_loading, 1000);
				_roomLogic->requestQuickSit();

			}
			else
			{
				_roomLogic->stop();

				this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
					/*if (onEnterDeskCallBack)
					{
						_isTouch = true;
						onEnterDeskCallBack(RoomLogic()->getSelectedRoom());
					}*/
					this->removeFromParent();
				}), nullptr));

			}
		}
	}
	else
	{
		_isTouch = true;
		_roomLogic->stop();
		RoomLogic()->close();
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(message);
		prompt->setCallBack([=](){
			if (handleCode == 255)
			{

			}
		});
	}
}

void CreateRoomLayer::onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo)
{
	_isTouch = true;
	_roomLogic->stop();

	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	if (m_loading)
	{
		m_loading->close();
		m_loading->removeFromParent();
		m_loading = NULL;
	}
	if (success)
	{
		if (INVALID_DESKNO != deskNo && INVALID_DESKSTATION != seatNo)
		{
			// 启动游戏
			bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
			if (!ret)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"));
			}
		}
	}
	else
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(message);
		prompt->setCallBack([=](){
			if (GameCreator()->getCurrentGameType() == HNGameCreator::SINGLE ||
				GameCreator()->getCurrentGameType() == HNGameCreator::BR)
			{
				RoomLogic()->close();
			}
		});
	}
}

void CreateRoomLayer::onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo)
{
	_isTouch = true;
	_roomLogic->stop();
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	if (success)
	{
		if (INVALID_DESKNO != deskNo)
		{
			// 启动游戏
			bool ret = GameCreator()->startGameClient(RoomLogic()->getSelectedRoom()->uNameID, deskNo, true);
			if (!ret)
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏启动失败。"));
			}
		}
		else
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("错误的游戏桌号"));
		}
	}
	else
	{
		RoomLogic()->close();
		GamePromptLayer::create()->showPrompt(message);
	}
}

void CreateRoomLayer::onPlatformRoomPassEnter(bool success, UINT roomId)
{
	if (success)
	{
		_roomLogic->start();
		_roomLogic->requestLogin(roomId);
		_roomID = roomId;
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("房间密码错误"));
	}
}

bool CreateRoomLayer::getConfigEventSelector(HNSocketMessage* socketMessage)
{
	CHECK_SOCKET_DATA_RETURN(MSG_GP_O_BuyDeskConfig, socketMessage->objectSize, true, "MSG_GP_O_BuyDeskConfig size of error!");
	MSG_GP_O_BuyDeskConfig* result = (MSG_GP_O_BuyDeskConfig*)socketMessage->object;
	switch (result->iGameID)
	{
	case 20171120:
		GamePromptLayer::create()->showPrompt(GBKToUtf8("产品升级中..."));
		//creatNewRoomNode();
		break;
	case 20171122:

		GamePromptLayer::create()->showPrompt(GBKToUtf8("产品升级中..."));
		break;
	default:
		showConfigLayout(result);
		break;
	}
	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	return true;
}

// 显示创建房间配置
void CreateRoomLayer::showConfigLayout(MSG_GP_O_BuyDeskConfig* config) //6.6创建房间
{
	deskConfig = *config;
	std::cout << deskConfig.iGameID << std::endl;
	//特殊处理 切换泉州和晋江麻将的时候需要释放当前清空容器
	if (_select_game) {
		_select_game->setVisible(false);
		_select_game->removeFromParent();
		_select_game = nullptr;
		_firthSelectCheckBoxs.clear();
		_secondSelectCheckBoxs.clear();
		_thirdSelectCheckBoxs.clear();
		_fourthSelectCheckBoxs.clear();
		_fifthSelectCheckBoxs.clear();
		_sixthSelectCheckBoxs.clear();
		_seventhSelectCheckBoxs.clear();
		_fufeiRuleCheckBoxs.clear();
		_renshuSelectCheckBoxs.clear();
		_wanfaSelectCheckBoxs.clear();
		_outcardSelectCheckBoxs.clear();
		_outbombSelectCheckBoxs.clear();
		_begincardSelectCheckBoxs.clear();
		_deskSelectCheckBoxs.clear();
		_NNPaixing = nullptr;
		_NNFanbei = nullptr;
		_NNzhuangjia = nullptr;
		_Button_change = nullptr;
		_Text_jiama = nullptr;
		m_LayoutTese = nullptr;
		m_LayoutMore = nullptr;
		m_checkBoxShuangjin = nullptr;
		m_checkBoxQianggang = nullptr;
		m_checkBoxMethod = nullptr;
		m_DisarmBomb = nullptr;
		_cardValue = 0;
		_iPlayerNum = 0;
		m_Text_OutCardTime = nullptr;
	}
	string szTitle = "";
	switch (deskConfig.iGameID)
	{
	case 20171121:
		szTitle = GBKToUtf8("泉州麻将");
		break;
	case 20200321:
		szTitle = GBKToUtf8("漳浦麻将");
		break;
	case 20200309:
		szTitle = GBKToUtf8("漳州麻将");
		break;
	case 20171123:
		szTitle = GBKToUtf8("晋江麻将");
		break;
	case 10100003:
		szTitle = GBKToUtf8("斗地主");
		break;
	case 12345678:
		szTitle = GBKToUtf8("跑得快");
		break;
	case 12100004:
		szTitle = GBKToUtf8("十三水");
		break;
	case 20170708:
		szTitle = GBKToUtf8("牛牛");
		break;
	}
	//增加当前游戏的列表按钮（泉州与晋江麻将选择）
	/*if (deskConfig.iGameID == 20171121 || 20171123 == deskConfig.iGameID)
	{
		if (deskConfig.iGameID == 20171121)
		{
			_list_btn->removeAllItems();

			Button* btn_qz = Button::create();
			btn_qz->loadTextureNormal(BTN_SELECT_IMG);
			btn_qz->loadTexturePressed(BTN_SELECT_IMG);
			btn_qz->loadTextureDisabled(BTN_SELECT_IMG);
			btn_qz->setTitleColor(BTN_SELECT_COLOR);
			btn_qz->setSize(BTN_SIZE);
			btn_qz->setTitleText(GBKToUtf8("游金麻将"));
			btn_qz->setTitleFontName(BTN_FONTNAME);
			btn_qz->setTitleFontSize(BTN_FONTSIZE);
			btn_qz->setName("btn_QZgame");
			btn_qz->ignoreContentAdaptWithSize(false);
			btn_qz->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::SwitchGameBtnCallBack, this));
			_list_btn->pushBackCustomItem(btn_qz);

			Button* btn_jj = Button::create();
			btn_jj->loadTextureNormal(BTN_NORMAL_IMG);
			btn_jj->loadTexturePressed(BTN_SELECT_IMG);
			btn_jj->loadTextureDisabled(BTN_SELECT_IMG);
			btn_jj->setTitleColor(BTN_NORMAL_COLOR); 	
			btn_jj->setTitleText(GBKToUtf8("晋江麻将"));
			btn_jj->setTitleFontName(BTN_FONTNAME);
			btn_jj->setTitleFontSize(BTN_FONTSIZE);
			btn_jj->setName("btn_JJgame");
			btn_jj->ignoreContentAdaptWithSize(false);
			btn_jj->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::SwitchGameBtnCallBack, this));
			_list_btn->pushBackCustomItem(btn_jj);
			btn_jj->setSize(BTN_SIZE);
		}
	}*/
	/*else
	{
		_list_btn->removeAllItems();

		Button* btn = Button::create();
		btn->setTitleColor(BTN_SELECT_COLOR);
		btn->loadTextureNormal(BTN_SELECT_IMG);
		btn->setSize(BTN_SIZE);
		btn->setTitleText(szTitle);
		btn->setTitleFontName(BTN_FONTNAME);
		btn->setTitleFontSize(BTN_FONTSIZE);
		btn->setName("btn_JJgame");
		btn->ignoreContentAdaptWithSize(false);
		_list_btn->pushBackCustomItem(btn);
	}*/

	_layout_PanelBlack->setVisible(true);
	_layout_games->removeAllChildren();
	_isInvoice = deskConfig.bInvoice;
	switch (deskConfig.iGameID)
	{
	case 20171121:
		creatQZMJRoomNode();
		break;
	case 20200309:
		creatYJMJRoomNode();
		break;
	case 20200321:
		createZPMJRoomNode();
		break;
	case 20200310:
		createSFRunCrazyRoomNode();
		break;
	case 20200311:
		createSYRunCrazyRoomNode();
		break;
	case 20171123:
		creatJJMJRoomNode();
		break;
	case 20170708:
		creatNNRoomNode();
		break;
	case 10100003:
		creatDDZRoomNode();
		break;
	case 12345678:
		creatRunCrazyRoomNode();
		break;
	case 20171120:
		creatZZMJRoomNode();
		break;
	case 20171122:
		creatXMMJRoomNode();
		break;
	case 12100004:
		creatSSSRoomNode();
		break;
	case 10207004:
		create510KRoomNode();
		break;
	default:
		break;
	}
	//创建房间游戏层请按照相同命名格式去做，例如斗地主游戏id：10100003，命名为Node_10100003，这样可以统一处理
	//	std::string nodeName = StringUtils::format("Node_%u", deskConfig.iGameID);
	//	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	//
	//	if (!_select_game)
	//	{
	//		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
	//		_select_game->setName(nodeName);
	//		_layout_games->addChild(_select_game);
	//		_select_game->setPosition(_layout_games->getContentSize() / 2);
	//	}
	//	else
	//	{
	//		_select_game->setVisible(true);
	//	}
	//
	//	auto layout_game = dynamic_cast<Layout*>(_select_game->getChildByName("Panel_game"));
	//
	//	// 是否支持代开
	//	_isInvoice = deskConfig.bInvoice;
	//
	//	// 结算条件（0:按局数，1:按总分，2:按圈数）(后续可能有客户有按局数和圈数的多选要求，这就得自己去处理了)
	//	_iType = deskConfig.bFinishCondition;
	//
	//	// 默认局数勾选
	//	_iCount = deskConfig.buyCounts[0].iBuyCount;
	//	for (int i = 0; i < 3; i++)
	//	{
	//		auto ck_count = dynamic_cast<CheckBox*>(layout_game->getChildByName(StringUtils::format("CheckBox_count%d", i)));
	//		if (ck_count)
	//		{
	//			ck_count->setTag(deskConfig.buyCounts[i].iBuyCount);
	//			ck_count->setVisible(deskConfig.buyCounts[i].iBuyCount != 0);
	//
	//			ck_count->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
	//
	//				if (type == CheckBox::EventType::UNSELECTED)
	//				{
	//					ck_count->setSelectedState(true);
	//					return;					
	//				}
	//
	//				for (int j = 0; j < 3; j++)
	//				{
	//					auto ck_box = dynamic_cast<CheckBox*>(layout_game->getChildByName(StringUtils::format("CheckBox_count%d", j)));
	//					if (ck_box) ck_box->setSelected(ck_box == ck_count);
	//				}
	//
	//				// 选择的局数/总胡息数/圈数
	//				_iCount = ck_count->getTag();				
	//			});
	//		}
	//
	//		auto ck_pay = dynamic_cast<CheckBox*>(layout_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
	//		if (ck_pay) ck_pay->setSelected(0 == i);
	//
	//		// 显示局数勾选配置
	//		auto jewels = dynamic_cast<Text*>(layout_game->getChildByName(StringUtils::format("Text_jewels%d", i)));
	//		if (jewels) showGameFinishCondition(jewels, deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iJewels, deskConfig.bFinishCondition);
	//	}
	//
	//	// 房费支付方式
	//	for (int i = 0; i < 2; i++)
	//	{
	//		auto ck_pay = dynamic_cast<CheckBox*>(layout_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
	//		if (ck_pay)
	//		{
	//			ck_pay->setTag(i);
	//			ck_pay->addEventListener([=](Ref* ref, ui::CheckBox::EventType type) {
	//
	//				if (type == CheckBox::EventType::UNSELECTED)
	//				{
	//					ck_pay->setSelectedState(true);
	//					return;
	//				}
	//	
	//				for (int j = 0; j < 2; j++)
	//				{
	//					auto ck_box = dynamic_cast<CheckBox*>(layout_game->getChildByName(StringUtils::format("CheckBox_pay%d", j)));
	//					if (ck_box) ck_box->setSelected(ck_box == ck_pay);
	//				}
	//
	//				for (int j = 0; j < 3; j++)
	//				{
	//					auto ck_count = dynamic_cast<CheckBox*>(_select_game->getChildByName(StringUtils::format("CheckBox_count%d", j)));
	//					if (ck_count) ck_count->setVisible(deskConfig.buyCounts[j].iBuyCount!= 0);
	//
	//					auto jewels = dynamic_cast<Text*>(layout_game->getChildByName(StringUtils::format("Text_jewels%d", j)));
	//					if (jewels)
	//					{
	//						// 房主付费
	//						if (0 == ck_pay->getTag())
	//						{
	//							showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
	//						}
	//						// AA制付费
	//						else
	//						{
	//							showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iAAJewels, deskConfig.bFinishCondition, true);
	//						}
	//					}
	//				}
	//
	//				// 房费支付方式（0:房主，1:AA）
	//				_iPay = ck_pay->getTag();
	//			});
	//		}
	//	}
	//	
	//	// 是否开启定位防作弊
	//	for (int i = 0; i < 2; i++)
	//	{
	//		auto ck_location = dynamic_cast<CheckBox*>(layout_game->getChildByName(StringUtils::format("CheckBox_location%d", i)));
	//		if (ck_location)
	//		{
	//			ck_location->setTag(i);
	//			ck_location->addEventListener([=](Ref* ref, ui::CheckBox::EventType type) {
	//
	//				if (type == CheckBox::EventType::UNSELECTED)
	//				{
	//					ck_location->setSelectedState(true);
	//					return;
	//				}
	//				for (int j = 0; j < 2; j++)
	//				{
	//					auto ck_box = dynamic_cast<CheckBox*>(layout_game->getChildByName(StringUtils::format("CheckBox_location%d", j)));
	//					if (ck_box) ck_box->setSelected(ck_box == ck_location);
	//				}
	//
	//				// 是否定位（0:关闭，1:开启）
	//				_iLocation = ck_location->getTag();
	//			});
	//		}
	//	}

	/////////////////////////////////////////////////其他规则玩法//////////////////////////////////////////////////
	//if (_gameRule)
	//	{
	//		_gameRule->removeFromParent();
	//		_gameRule = nullptr;
	//	}


}

// 显示局数，总分，圈数勾选项
void CreateRoomLayer::showGameFinishCondition(Text* text, int count, int jewels, BYTE bFinishCondition, bool isAA/* = false*/)
{
	std::string str = "";
	if (_Text_CostNum)
	{
		std::string str_cost = "";
		switch (bFinishCondition)
		{
		case 0:
			if (count == 99)
			{
				if (jewels == 1)
				{
					jewels = 1;
				}
				else if (jewels > 3)
				{
					jewels = 5;
				}
				else
				{
					jewels = 3;
				}
				str = GBKToUtf8("1课");//StringUtils::format(GBKToUtf8("1课 * %d"), jewels);
			}
			else
			{
				str = StringUtils::format(GBKToUtf8("%d局"), count);
			}
			break;
		case 1:
			str = StringUtils::format(GBKToUtf8("%d胡息"), count);
			break;
		case 2:
			str = StringUtils::format(GBKToUtf8("%d圈"), count);
			break;
		default:
			str = StringUtils::format(GBKToUtf8("%d局"), count);
			break;
		}
		str_cost = StringUtils::format("X%d", jewels);

		if (isAA)
		{
			str_cost.append(GBKToUtf8("/人"));
		}

		if (text && ClubID <= 0)
			text->setString(str);
		_Text_CostNum->setString(str_cost);
	}
	else
	{
		switch (bFinishCondition)
		{
		case 0:
			if (count == 99)
			{
				if (jewels == 1)
				{
					jewels = 1;
				}
				else if (jewels > 3)
				{
					jewels = 5;
				}
				else
				{
					jewels = 3;
				}
				str = StringUtils::format(GBKToUtf8("1课       *%d"), jewels);
			}
			else
			{
				str = StringUtils::format(GBKToUtf8("%d局%s*%d"), count, count > 10 ? "     " : "       ", jewels);
			}
			break;
		case 1:
			str = StringUtils::format(GBKToUtf8("%d胡息%d钻石"), count, jewels);
			break;
		case 2:
			str = StringUtils::format(GBKToUtf8("%d圈%d钻石"), count, jewels);
			break;
		default:
			str = StringUtils::format(GBKToUtf8("%d局%d钻石"), count, jewels);
			break;
		}

		if (isAA) str.append(GBKToUtf8("/人"));

		text->setString(str);
	}
	//text->setVisible(count != 0);
}

void CreateRoomLayer::creatQZMJRoomNode()
{
	//创建房间游戏层请按照相同命名格式去做，例如斗地主游戏id：10100003，命名为Node_10100003，这样可以统一处理
		std::string nodeName = StringUtils::format("Node_%u", 20171121);
		//if (_select_game) _select_game->setVisible(false);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));

	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(647.07, 395.91));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	_jushuVec.clear();
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	//更新为滑动选择
	auto ScrollView_Create = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_Create");
	
	//0局、1课
	int jushu_tags[] = {8, 16};
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(jushu_tags[i]);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		auto jewels = dynamic_cast<Text*>(checkBox->getChildByName("Text"));
		if (jewels) jewels->setString(StringUtils::format(GBKToUtf8("%d局"), jushu_tags[i]));

		// push_back 字符串变量.push_back(var)作用，在字符串最后插入一个字符
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//倍数：0 *4、1 *3
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_beishu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}

	//底分：0 1分 1 5分 2 10分
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);

		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);

	}

	//人数
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i+2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_renshuSelectCheckBoxs.push_back(checkBox);
	}
	//默认勾选2人
	_iPlayerNum = 2;

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}

	////设置出牌时间
	//m_Text_OutCardTime = (Text*)ScrollView_Create->getChildByName("Text_outcardTime_0");
	//m_Text_OutCardTime->setString(GBKToUtf8(OUTCARD_LIST[_iOutCardTime]));
	//m_btn_add = (Button*)ScrollView_Create->getChildByName("btn_add");
	//m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	//m_btn_sub = (Button*)ScrollView_Create->getChildByName("btn_sub");
	//m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	////设置超时操作
	//for (int i = 0; i < 2; i++)
	//{
	//	std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
	//	auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
	//	checkBox->setTag(i);
	//	checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
	//	if (checkBox->isSelected())
	//	{
	//		checkBox->setTouchEnabled(false);
	//	}
	//	_outcardSelectCheckBoxs.push_back(checkBox);
	//}

	//俱乐部创建房间显示区别
	if (ClubID > 0)isRoomNeedPassWord(ScrollView_Create, true);
	else isRoomNeedPassWord(ScrollView_Create, false);
}

void CreateRoomLayer::createZPMJRoomNode()
{
	/*
	[0]付费
	[1-2]自定义规则补全（游金数量，底分）
	[3]台数类型 ：1=漳浦33制   2=佛坛88制    3=赤湖48制
	[4]游戏玩法 ：0=硬胡       1=非硬胡
	[5]游戏选项 ：0x01=不可探牌 0x02=不可吃牌  0x04=去一色  0x08=超时暂停  0x10=超时托管
	[6]算分方式 ：0=算台制     1涨台制
	[7]涨分设置 ：1=10涨5      2=20涨10      3=30涨20     4=50涨20     5=100涨50
	[8]插花分   ：0-100
	[9]超时时间 ：1-4 
	*/

	//创建房间游戏层请按照相同命名格式去做，例如斗地主游戏id：10100003，命名为Node_10100003，这样可以统一处理
	std::string nodeName = StringUtils::format("Node_%u", 20200321);
	//if (_select_game) _select_game->setVisible(false);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));

	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(649.64, 395.76));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	_jushuVec.clear();
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	auto ScrollView_Create = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_Create");
	
	//8局、16局、24局
	int juzhu_tags[] = {8, 16, 24};
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		// push_back 字符串变量.push_back(var)作用，在字符串最后插入一个字符
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//台数算法：漳浦33制，佛坛88制，漳浦48制(1,2,3)
	int taishu_tags[] = { 1, 3 }; //(目前只有漳浦33,48制)
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_taishu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(taishu_tags[i]);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}

	//游戏玩法：硬胡  非硬胡  不可探牌 不可吃牌（硬胡和非硬胡互斥）
	for (int i = 0; i < 4; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_wanfa%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);

		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);

	}

	//人数，4,3,2
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(4 - i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_renshuSelectCheckBoxs.push_back(checkBox);
	}
	//默认勾选2人
	_iPlayerNum = 4;

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
// 			for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; j--)
// 			{
// 				if (_firthSelectCheckBoxs.at(j) == nullptr) continue;
// 				auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
// 				if (jewels)
// 				{
// 					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
// 				}
// 				_jushuVec.insert(_jushuVec.begin(), jewels);
// 			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}

	//算分  算台制   算分制
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_suanfen%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkForthSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fourthSelectCheckBoxs.push_back(checkBox);
	}

	//涨分  10涨5,20涨10,30涨20,50涨20,100涨50
	for (int i = 0; i < 5; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_zfset%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i + 1);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFithSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fifthSelectCheckBoxs.push_back(checkBox);
	}
	if (_fourthSelectCheckBoxs.size() > 0)
	{
		bool bCanSelected = _fourthSelectCheckBoxs.at(0)->isSelected() ? false : true;
		for (auto itr : _fifthSelectCheckBoxs)
		{
			itr->setEnabled(bCanSelected);
		}
	}

	//插花 1-100
	for (int i = 0; i < 1; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_chahua%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSixSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_sixthSelectCheckBoxs.push_back(checkBox);

		
		//编辑框
		auto pTextField = (TextField*)checkBox->getChildByName("TextField");
		if (pTextField)
		{
			pTextField->setVisible(false);
			auto pEditBoxCode = HNEditBox::createEditBox(pTextField, this);
			pEditBoxCode->setName("TextField_chahua");
			pEditBoxCode->setTag(i);
			pEditBoxCode->setInputMode(ui::EditBox::InputMode::NUMERIC);
			pEditBoxCode->setMaxLength(3);
			pEditBoxCode->setTouchEnabled(checkBox->isSelected());
		}
		/*textField->setTag(i);
		textField->setMaxLength(3);
		textField->setMaxLengthEnabled(true);
		textField->setTextHorizontalAlignment(TextHAlignment::CENTER);
		textField->setTextVerticalAlignment(TextVAlignment::CENTER);
		textField->addEventListener([=](Ref* ref, TextField::EventType type)
		{
		TextField control = (TextField*)ref;
		int tag = control->getTag();
		switch (type)
		{
		case TextField::EventType::ATTACH_WITH_IME:
		break;
		case TextField::EventType::DELETE_BACKWARD:
		break;
		case TextField::EventType::DETACH_WITH_IME:
		break;
		case TextField::EventType::INSERT_TEXT:
		break;
		default:
		break;
		}
		printf("xxxxxxxxxxx");
		});*/
	}

	//防作弊 开启防作弊功能
	for (int i = 0; i < 1; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_cheat%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSevenSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_seventhSelectCheckBoxs.push_back(checkBox);
	}

	std::vector<int> times = { 15, 30, 60, 99 };
	_OutCardTimes.swap(times);
	//设置出牌时间
	m_Text_OutCardTime = (Text*)ScrollView_Create->getChildByName("Text_outcardTime_0");
	m_Text_OutCardTime->setString(StringUtils::format(GBKToUtf8("%d秒"), _OutCardTimes[_iOutCardTime]));
	m_btn_add = (Button*)ScrollView_Create->getChildByName("btn_add");
	m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	m_btn_sub = (Button*)ScrollView_Create->getChildByName("btn_sub");
	m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	//设置超时操作
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_outcardSelectCheckBoxs.push_back(checkBox);
	}

	//俱乐部创建房间显示区别
	if (ClubID > 0)isRoomNeedPassWord(ScrollView_Create, true);
	else isRoomNeedPassWord(ScrollView_Create, false);
}

void CreateRoomLayer::creatYJMJRoomNode()
{
	//创建房间游戏层请按照相同命名格式去做，例如斗地主游戏id：10100003，命名为Node_10100003，这样可以统一处理
	std::string nodeName = StringUtils::format("Node_%u", 20200309);
	//if (_select_game) _select_game->setVisible(false);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));

	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(641.51, 384.93));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	_jushuVec.clear();
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	//俱乐部创建房间显示区别
	if (ClubID > 0)isRoomNeedPassWord(nodePanel, true);
	else isRoomNeedPassWord(nodePanel, false);
	//0局、1课
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		// push_back 字符串变量.push_back(var)作用，在字符串最后插入一个字符
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//倍数：0 *4、1 *3
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_beishu%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}

	//底分：0 1分 1 5分 2 10分
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);

		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);

	}

	//人数
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i + 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_renshuSelectCheckBoxs.push_back(checkBox);
	}
	//默认勾选2人
	_iPlayerNum = 2;

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
			for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; j--)
			{
				if (_firthSelectCheckBoxs.at(j) == nullptr) continue;
				auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
				if (jewels)
				{
					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
				}
				_jushuVec.insert(_jushuVec.begin(), jewels);
			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}
}

//创建上游跑得快（大小头没有底分）
void  CreateRoomLayer::createSYRunCrazyRoomNode()
{
	//自定义规则（付费方式之外）第一位：1、首局黑桃3先出   2、每局黑桃3先出
	//第二位：1、1底分，3、3底分，5、5底分（默认1底分）
	//第三位：0、没有大小头，1、5/10/20，2、10/20/30，3、20/30/50，4、20/40/60
	//第四位：3A最大==0，最小==1
	//第五位：超时时间（OUTCARD_LIST）1开始计数1==15、2==30...
	//第六位：0超时暂停，1超时托管
	//第七位：0、三带二不可以带“2”，1、三带二可以带“2”
	//第八位：是否显示剩余牌数(0不显示)
	//第九位：报警张数提醒（0=3张，1=1张）
	std::string nodeName = StringUtils::format("Node_%u", 20200311);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(647.21, 397.12));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");

	//更新为滑动选择
	auto ScrollView_Table = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_Table");

	//局数
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//玩法：1、首局黑桃3先出   2、每局黑桃3先出
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_wanfa%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(1 + i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}
	//底分：1、1底分，3、3底分，5、5底分
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(1 + i * 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);
	}
	//人数(2-3)
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i + 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_renshuSelectCheckBoxs.push_back(checkBox);
	}

	//默认勾选2人
	_iPlayerNum = 2;

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
			//局数不同消费不同
// 			for (int j = _firthSelectCheckBoxs.size() - 1; j >=0; j--)
// 			{
// 				if (_firthSelectCheckBoxs.at(j) == nullptr) continue;
// 				auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
// 				if (jewels)
// 				{
// 					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
// 				}
// 				//_jushuVec.push_back(jewels);
// 				_jushuVec.insert(_jushuVec.begin(), jewels);
// 			}
			/*auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(i)->getChildByName("Text"));
			if (jewels)
			{
				showGameFinishCondition(jewels, deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iJewels, deskConfig.bFinishCondition);
			}*/
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}

	//特色玩法记录
	Layout* nodetese = (Layout*)ScrollView_Table->getChildByName("panel_tese");
	m_LayoutTese = nodetese;
	//m_LayoutTese->setVisible(false);
	//m_checkBoxShuangjin = (CheckBox*)nodetese->getChildByName("CheckBox_tese0");			//大小头
	//m_checkBoxShuangjin->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::shuangjinpinghu, this)));

	//特色更多显示
	m_LayoutMore = (Layout*)m_LayoutTese->getChildByName("panel_tese_more");
	//m_LayoutMore->setVisible(false);

	//大小头更多选择
	for (int i = 0; i < 4; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_tese_more%d", i);
		auto checkBox = (CheckBox*)m_LayoutMore->getChildByName(nodeName);
		checkBox->setTag(i + 1);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkForthSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fourthSelectCheckBoxs.push_back(checkBox);
	}

	//规则：3A最大==0，最小==1
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_guize%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFithSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fifthSelectCheckBoxs.push_back(checkBox);
	}

	std::vector<int> times = { 15, 30, 60, 90 };
	_OutCardTimes.swap(times);

	//设置出牌时间
	m_Text_OutCardTime = (Text*)ScrollView_Table->getChildByName("Text_outcardTime");
	m_Text_OutCardTime->setString(StringUtils::format(GBKToUtf8("%d秒"), _OutCardTimes[_iOutCardTime]));
	m_btn_add = (Button*)ScrollView_Table->getChildByName("btn_add");
	m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	m_btn_sub = (Button*)ScrollView_Table->getChildByName("btn_sub");
	m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	//设置超时操作
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_outcardSelectCheckBoxs.push_back(checkBox);
	}

	//三代二是否可带2
	m_checkBoxMethod = (CheckBox*)ScrollView_Table->getChildByName("CheckBox_method0");
	m_checkBoxMethod->setTag(1);

	//是否显示剩余牌数(0不显示)
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_function%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSixSelectCallback, this)));
		_sixthSelectCheckBoxs.push_back(checkBox);
	}

	//报警张数提醒（0=3张，1=1张）
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_bottom%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSevenSelectCallback, this)));
		_seventhSelectCheckBoxs.push_back(checkBox);
	}

	//俱乐部创建房间显示区别
	if (ClubID >0)isRoomNeedPassWord(ScrollView_Table, true);
	else isRoomNeedPassWord(ScrollView_Table, false);
}

//创建算分跑得快（没有大小头有底分）
void  CreateRoomLayer::createSFRunCrazyRoomNode()
{
	//自定义规则（付费方式之外）第一位：1、首局黑桃3先出   2、每局黑桃3先出
	//第二位：1、1底分，3、3底分，5、5底分
	//第三位：0、没有大小头，1、大2小1，2、大10小5，3、大20小10（默认没有大小头）
	//第四位：3A最大==0，最小==1
	//第五位：超时时间（OUTCARD_LIST）1开始计数1==15、2==30...
	//第六位：0超时暂停，1超时托管
	//第七位：0、三带二不可以带“2”，1、三带二可以带“2”
	//第八位：是否显示剩余牌数(0不显示)
	//第九位：报警张数提醒（0=3张，1=1张）
	//第十位：0、16张，1、15张
	//11：0、炸弹可拆，1、炸弹不可拆
	//12：0、可以4带2，1、可以4带3，2、四张不可带
	std::string nodeName = StringUtils::format("Node_%u", 20200310);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(647.21, 397.12));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");

	//更新为滑动选择
	auto ScrollView_Table = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_Table");

	//局数
	int jushu_tags[] = { 1, 2 };
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(jushu_tags[i]);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}

		auto jewels = dynamic_cast<Text*>(checkBox->getChildByName("Text"));
		if (jewels) jewels->setString(StringUtils::format(GBKToUtf8("%d局"), jushu_tags[i]));

		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//玩法：1、首局黑桃3先出   2、每局黑桃3先出
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_wanfa%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(1 + i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}
	//底分：1、1底分，3、3底分，5、5底分
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(1 + i * 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);
	}
	//人数(2-3)
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i + 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_renshuSelectCheckBoxs.push_back(checkBox);
	}

	//默认勾选2人
	_iPlayerNum = 2;

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
			//跑得快根据人数显示局数（2人选项局数最后一个为24、3人为32）
// 			for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; j--)
// 			{
// 				if (_firthSelectCheckBoxs.at(j) == nullptr) continue;
// 				auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
// 				if (jewels)
// 				{
// 					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
// 				}
// 				_jushuVec.insert(_jushuVec.begin(), jewels);
// 			}
			/*auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(i)->getChildByName("Text"));
			if (jewels)
			{
				showGameFinishCondition(jewels, deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iJewels, deskConfig.bFinishCondition);
			}*/
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}

	//特色玩法记录
	Layout* nodetese = (Layout*)ScrollView_Table->getChildByName("panel_tese");
	m_LayoutTese = nodetese;
	m_LayoutTese->setVisible(false);
	//m_checkBoxShuangjin = (CheckBox*)nodetese->getChildByName("CheckBox_tese0");			//大小头
	//m_checkBoxShuangjin->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::shuangjinpinghu, this)));

	//特色更多显示
	m_LayoutMore = (Layout*)m_LayoutTese->getChildByName("panel_tese_more");
	m_LayoutMore->setVisible(false);

	//大小头更多选择
	/*for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_tese_more%d", i);
		auto checkBox = (CheckBox*)m_LayoutMore->getChildByName(nodeName);
		checkBox->setTag(i + 1);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkForthSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fourthSelectCheckBoxs.push_back(checkBox);
	}*/

	//规则：3A最大==0，最小==1
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_guize%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFithSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fifthSelectCheckBoxs.push_back(checkBox);
	}
	std::vector<int> times = { 15, 30, 60, 90 };
	_OutCardTimes.swap(times);
	//设置出牌时间
	m_Text_OutCardTime = (Text*)ScrollView_Table->getChildByName("Text_outcardTime");
	m_Text_OutCardTime->setString(StringUtils::format(GBKToUtf8("%d秒"), _OutCardTimes[_iOutCardTime]));
	m_btn_add = (Button*)ScrollView_Table->getChildByName("btn_add");
	m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	m_btn_sub = (Button*)ScrollView_Table->getChildByName("btn_sub");
	m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	//设置超时操作
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_outcardSelectCheckBoxs.push_back(checkBox);
	}

	//三代二是否可带2
	m_checkBoxMethod = (CheckBox*)ScrollView_Table->getChildByName("CheckBox_method0");
	m_checkBoxMethod->setTag(1);

	//是否显示剩余牌数(0不显示)
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_function%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSixSelectCallback, this)));
		_sixthSelectCheckBoxs.push_back(checkBox);
	}

	//报警张数提醒（0=3张，1=1张）
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_bottom%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSevenSelectCallback, this)));
		_seventhSelectCheckBoxs.push_back(checkBox);
	}

	//牌张数选择
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_cardNum%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkCardNumSelectCallback, this)));
		_begincardSelectCheckBoxs.push_back(checkBox);
	}

	//是否可拆
	m_DisarmBomb = (CheckBox*)ScrollView_Table->getChildByName("CheckBox_DisarmBomb");
	m_DisarmBomb->setTag(1);

	//四带三，四带二，不可带选择
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_bomb%d", i);
		auto checkBox = (CheckBox*)ScrollView_Table->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkBombOptionsSelectCallback, this)));
		_outbombSelectCheckBoxs.push_back(checkBox);
	}

	//俱乐部创建房间显示区别
	if (ClubID >0)isRoomNeedPassWord(ScrollView_Table, true);
	else isRoomNeedPassWord(ScrollView_Table, false);
}

void  CreateRoomLayer::creatJJMJRoomNode(){
	//创建房间游戏层请按照相同命名格式去做，例如斗地主游戏id：10100003，命名为Node_10100003，这样可以统一处理
	std::string nodeName = StringUtils::format("Node_%u", 20171123);
	//if (_select_game) _select_game->setVisible(false);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	//_iCount = 16;
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(647.07, 395.91)); //_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	_jushuVec.clear();
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");

	//更新为滑动选择
	auto ScrollView_Create = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_Create");

	
	//16局、8局
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		// push_back 字符串变量.push_back(var)作用，在字符串最后插入一个字符
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//倍数：0 *4、1 *3
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_beishu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}

	//底分：0 1分 1 5分 2 10分
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);

		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);

	}

	//人数
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i + 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_renshuSelectCheckBoxs.push_back(checkBox);
	}

	//玩法
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_genda%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkWanFaSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_wanfaSelectCheckBoxs.push_back(checkBox);
	}

	//默认勾选2人
	_iPlayerNum = 2;

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
// 			for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; j--)
// 			{
// 				if (_firthSelectCheckBoxs.at(j) == nullptr) continue;
// 				auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
// 				if (jewels)
// 				{
// 					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iAAJewels * _iPlayerNum, deskConfig.bFinishCondition);
// 				}
// 				_jushuVec.insert(_jushuVec.begin(), jewels);
// 			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}

	m_checkBoxShuangjin = (CheckBox*)ScrollView_Create->getChildByName("CheckBox_shuagnjin");
	//m_checkBoxShuangjin->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::shuangjinpinghu, this)));

	m_checkBoxQianggang = (CheckBox*)ScrollView_Create->getChildByName("CheckBox_qianggang");
	m_checkBoxQianggang->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::qiangganghu, this)));

	////设置出牌时间
	//m_Text_OutCardTime = (Text*)ScrollView_Create->getChildByName("Text_outcardTime_0");
	//m_Text_OutCardTime->setString(GBKToUtf8(OUTCARD_LIST[_iOutCardTime]));
	//m_btn_add = (Button*)ScrollView_Create->getChildByName("btn_add");
	//m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	//m_btn_sub = (Button*)ScrollView_Create->getChildByName("btn_sub");
	//m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	////设置超时操作
	//for (int i = 0; i < 2; i++)
	//{
	//	std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
	//	auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
	//	checkBox->setTag(i);
	//	checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
	//	if (checkBox->isSelected())
	//	{
	//		checkBox->setTouchEnabled(false);
	//	}
	//	_outcardSelectCheckBoxs.push_back(checkBox);
	//}

	//俱乐部创建房间显示区别
	if (ClubID > 0)isRoomNeedPassWord(ScrollView_Create, true);
	else isRoomNeedPassWord(ScrollView_Create, false);

}
void CreateRoomLayer::shuangjinpinghu(Ref* pSender, CheckBox::EventType type)
{
	if (deskConfig.iGameID == 12345678)
	{
		CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
		if (checkBox->isSelected())
		{
			if (m_LayoutMore)
			{
				m_LayoutMore->setVisible(true);
			}
		}
		else
		{
			if (m_LayoutMore)
			{
				m_LayoutMore->setVisible(false);
			}
		}
	}

	//refreshAllCheckBox(_layout_games);
}
void CreateRoomLayer::qiangganghu(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox->isSelected())
	{
		//checkBox->setTouchEnabled(false);
	}

	//refreshAllCheckBox(_layout_games);
}


void CreateRoomLayer::checkFuFeiSelectCallback(Ref* pSender, CheckBox::EventType type) //支付方式gjd6.10
{

	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (_fufeiRuleCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _fufeiRuleCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}

		// 房主付费
		if (checkBox->getTag() == 0)
		{
			if (_firthSelectCheckBoxs.size() > 0)
			{
				for (int i = 0; i < _firthSelectCheckBoxs.size(); i++)
				{
					if (_firthSelectCheckBoxs[i]->isSelected())
					{
						showGameFinishCondition(_jushuVec.at(i), deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iJewels, deskConfig.bFinishCondition);
					}
				}
			}
		}
		else
		{
			if (_firthSelectCheckBoxs.size() > 0)
			{
				for (int i = 0; i < _firthSelectCheckBoxs.size(); i++)
				{
					if (_firthSelectCheckBoxs[i]->isSelected())
					{
						showGameFinishCondition(_jushuVec.at(i), deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iAAJewels, deskConfig.bFinishCondition, true);
					}
				}
			}
		}

		/*
		for (int j = 0; j < _jushuVec.size(); j++)
		{
			if (_jushuVec.at(j))
			{
				// 房主付费
				if (checkBox->getTag() == 0)
				{
					if (deskConfig.iGameID == 20171123 || deskConfig.iGameID == 20171121 || deskConfig.iGameID == 20200309)
					{
						showGameFinishCondition(_jushuVec.at(j), deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iAAJewels * _iPlayerNum, deskConfig.bFinishCondition);
					}
					//跑得快根据人数显示局数（2人选项局数最后一个为24、3人为32）
					else if (deskConfig.iGameID == 12345678)
					{
						if (_iPlayerNum == 2 && j == 2)
						{
							showGameFinishCondition(_jushuVec.at(j), PLAYER_2_BUYCOUNT, deskConfig.buyCounts[j].iAAJewels * _iPlayerNum, deskConfig.bFinishCondition);
							_firthSelectCheckBoxs[j]->setTag(PLAYER_2_BUYCOUNT);
						}
						else if (_iPlayerNum == 3 && j == 2)
						{
							showGameFinishCondition(_jushuVec.at(j), PLAYER_3_BUYCOUNT, deskConfig.buyCounts[j].iAAJewels * _iPlayerNum, deskConfig.bFinishCondition);
							_firthSelectCheckBoxs[j]->setTag(PLAYER_3_BUYCOUNT);
						}
						else
						{
							showGameFinishCondition(_jushuVec.at(j), deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iAAJewels * _iPlayerNum, deskConfig.bFinishCondition);
						}
					}
					else
					{
						showGameFinishCondition(_jushuVec.at(j), deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
					}					
				}
				// AA制付费
				else
				{
					//跑得快根据人数显示局数（2人选项局数最后一个为24、3人为32）
					if (deskConfig.iGameID == 12345678)
					{
						if (_iPlayerNum == 2 && j == 2)
						{
							//AA制不用显示多少/人的消费
							showGameFinishCondition(_jushuVec.at(j), PLAYER_2_BUYCOUNT, deskConfig.buyCounts[j].iAAJewels, deskConfig.bFinishCondition, true);
							_firthSelectCheckBoxs[j]->setTag(PLAYER_2_BUYCOUNT);
						}
						else if (_iPlayerNum == 3 && j ==2)
						{
							showGameFinishCondition(_jushuVec.at(j), PLAYER_3_BUYCOUNT, deskConfig.buyCounts[j].iAAJewels, deskConfig.bFinishCondition, true);
							_firthSelectCheckBoxs[j]->setTag(PLAYER_3_BUYCOUNT);
						}
						else
						{
							showGameFinishCondition(_jushuVec.at(j), deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iAAJewels, deskConfig.bFinishCondition, true);
						}
					}
					else if (deskConfig.iGameID == 20171121 || deskConfig.iGameID == 20171123 || deskConfig.iGameID == 20200309)
					{
						showGameFinishCondition(_jushuVec.at(j), deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iAAJewels, deskConfig.bFinishCondition, true);
					}
					else
					{
						showGameFinishCondition(_jushuVec.at(j), deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iAAJewels, deskConfig.bFinishCondition, true);
					}
				}
			}
		}*/
	}

	//refreshAllCheckBox(_layout_games);
}
void CreateRoomLayer::checkFirstSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (checkBox->isSelected() && deskConfig.iGameID == 20171122)
		{
			if (checkBox->getTag() == 99)
			{
				for (int i = 0; i < _fifthSelectCheckBoxs.size(); i++)
				{
					if (i == 1)
					{
						_fifthSelectCheckBoxs.at(i)->setSelected(true);
						_fifthSelectCheckBoxs.at(i)->setTouchEnabled(false);
					}
					else
					{
						_fifthSelectCheckBoxs.at(i)->setSelected(false);
						_fifthSelectCheckBoxs.at(i)->setTouchEnabled(true);
					}
				}
				_fifthSelectCheckBoxs.at(0)->setVisible(false);
				_fifthSelectCheckBoxs.at(2)->setVisible(false);
				_fifthSelectCheckBoxs.at(1)->setVisible(true);
			}
			else
			{
				for (int i = 0; i < _fifthSelectCheckBoxs.size(); i++)
				{
					if (i == 0)
					{
						_fifthSelectCheckBoxs.at(i)->setSelected(true);
						_fifthSelectCheckBoxs.at(i)->setTouchEnabled(false);
					}
					else
					{
						_fifthSelectCheckBoxs.at(i)->setSelected(false);
						_fifthSelectCheckBoxs.at(i)->setTouchEnabled(true);
					}
				}
				_fifthSelectCheckBoxs.at(0)->setVisible(true);
				_fifthSelectCheckBoxs.at(2)->setVisible(true);
				_fifthSelectCheckBoxs.at(1)->setVisible(false);
			}
		}
		if (_firthSelectCheckBoxs.size() > 0)
		{
			/*for (CheckBox* iter : _firthSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}*/
			if (_firthSelectCheckBoxs.size() > 0)
			{
				for (int i = 0; i < _firthSelectCheckBoxs.size(); i++)
				{
					if (checkBox != _firthSelectCheckBoxs[i])
					{
						_firthSelectCheckBoxs[i]->setTouchEnabled(true);
						_firthSelectCheckBoxs[i]->setSelected(false);
					}
					else if (_firthSelectCheckBoxs[i]->isSelected())
					{
						_firthSelectCheckBoxs[i]->setSelected(true);
						_firthSelectCheckBoxs[i]->setTouchEnabled(false);
						for (int j = 0; j < _fufeiRuleCheckBoxs.size(); j++)
						{
							// 房主付费
							if ((_fufeiRuleCheckBoxs[j]->isSelected() && j == 0))
							{
								showGameFinishCondition(_jushuVec.at(i), deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iJewels, deskConfig.bFinishCondition);
							}
							else if (_fufeiRuleCheckBoxs[j]->isSelected() && j == 1)
							{
								showGameFinishCondition(_jushuVec.at(i), deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iAAJewels, deskConfig.bFinishCondition, true);
							}
						}
					}
				}
			}
		}

		if (deskConfig.iGameID == 20171120 || deskConfig.iGameID == 20171121 || deskConfig.iGameID == 20171122 || deskConfig.iGameID == 20200309)
		{
			if (checkBox->isSelected() && checkBox->getTag() == 99)
			{
				for (int i = 0; i < _thirdSelectCheckBoxs.size(); i++)
				{
					if (i == 1)
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(true);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(false);
						_thirdSelectCheckBoxs.at(i)->setVisible(true);
					}
					else
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(false);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(true);
						_thirdSelectCheckBoxs.at(i)->setVisible(false);
					}
					_thirdSelectCheckBoxs.at(2)->setVisible(true);
				}
			}
			else if (checkBox->isSelected())
			{
				for (int i = 0; i < _thirdSelectCheckBoxs.size(); i++)
				{
					if (i == 0)
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(true);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(false);
						_thirdSelectCheckBoxs.at(i)->setVisible(true);
					}
					else
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(false);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(true);
						_thirdSelectCheckBoxs.at(i)->setVisible(false);
					}
				}
			}
		}
		if (deskConfig.iGameID == 20171123) // 晋江麻将创建房间局数回调6.6
		{
			if (_fufeiRuleCheckBoxs.size() > 0)
			{
				for (int j = 0; j < _fufeiRuleCheckBoxs.size(); j++)
				{
					// 房主付费
					if (_fufeiRuleCheckBoxs[j]->isSelected() && j == 0)
					{
						for (int z = 0; z < _jushuVec.size(); z++)
						{
							if (_jushuVec.at(z))
							{
								showGameFinishCondition(_jushuVec.at(z), deskConfig.buyCounts[z].iBuyCount, deskConfig.buyCounts[z].iAAJewels * _iPlayerNum, deskConfig.bFinishCondition);
							}
						}
					}
				}
			}

			for (CheckBox* iter : _firthSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}

	//refreshAllCheckBox(_layout_games);
}
void CreateRoomLayer::checkSecondSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{

		if (_secondSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _secondSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}

	}

	//refreshAllCheckBox(_layout_games);
}
void CreateRoomLayer::checkRenShuSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	int index = checkBox->getTag();
	if (checkBox)
	{
		if (_renshuSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _renshuSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
					_iPlayerNum = index;				
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}
	if (deskConfig.iGameID == 12345678)
	{
		if (_iPlayerNum == 2)
		{
			if (m_LayoutTese)
			{
				m_LayoutTese->setVisible(false);
			}
		}
		else if (_iPlayerNum == 3)
		{
			if (m_LayoutTese)
			{
				m_LayoutTese->setVisible(true);
			}
		}
	}
	if (deskConfig.iGameID == 20171121 || deskConfig.iGameID == 20171123 || deskConfig.iGameID == 20200309)
	{
		if (_fufeiRuleCheckBoxs.size() > 0)
		{
			for (int j = 0; j < _fufeiRuleCheckBoxs.size(); j++)
			{
				// 房主付费
				if ((_fufeiRuleCheckBoxs[j]->isSelected() && j == 0))
				{
					for (int z = 0; z < _jushuVec.size(); z++)
					{
						if (_jushuVec.at(z))
						{
							showGameFinishCondition(_jushuVec.at(z), deskConfig.buyCounts[z].iBuyCount, deskConfig.buyCounts[z].iAAJewels*index, deskConfig.bFinishCondition);
						}
					}
				}
				//AA付费
				else if (_fufeiRuleCheckBoxs[j]->isSelected() && j == 1)
				{
					for (int z = 0; z < _jushuVec.size(); z++)
					{
						if (_jushuVec.at(z))
						{
							showGameFinishCondition(_jushuVec.at(z), deskConfig.buyCounts[z].iBuyCount, deskConfig.buyCounts[z].iAAJewels, deskConfig.bFinishCondition,true);
						}
					}
				}
			}
		}
	}
	//跑得快根据人数显示局数（2人选项局数最后一个为24、3人为32）
	else if (deskConfig.iGameID == 12345678)
	{
		//跑得快人数3人局数有变动
		for (int z = 0; z < _jushuVec.size(); z++)
		{
			if (_jushuVec.at(z))
			{
				if (_fufeiRuleCheckBoxs.size() > 0)
				{
					if (index == 2 && z == 2)
					{
						//房主付费
						if (_fufeiRuleCheckBoxs[0]->isSelected())
						{
							showGameFinishCondition(_jushuVec.at(z), PLAYER_2_BUYCOUNT, deskConfig.buyCounts[z].iAAJewels*index, deskConfig.bFinishCondition);
							_firthSelectCheckBoxs[z]->setTag(PLAYER_2_BUYCOUNT);
						}
						else
						{
							showGameFinishCondition(_jushuVec.at(z), PLAYER_2_BUYCOUNT, deskConfig.buyCounts[z].iAAJewels, deskConfig.bFinishCondition,true);
							_firthSelectCheckBoxs[z]->setTag(PLAYER_2_BUYCOUNT);
						}
					}
					else if (index == 3 && z == 2)
					{
						//房主付费
						if (_fufeiRuleCheckBoxs[0]->isSelected())
						{
							showGameFinishCondition(_jushuVec.at(z), PLAYER_3_BUYCOUNT, deskConfig.buyCounts[z].iAAJewels*index, deskConfig.bFinishCondition);
							_firthSelectCheckBoxs[z]->setTag(PLAYER_3_BUYCOUNT);
						}
						else
						{
							showGameFinishCondition(_jushuVec.at(z), PLAYER_3_BUYCOUNT, deskConfig.buyCounts[z].iAAJewels, deskConfig.bFinishCondition, true);
							_firthSelectCheckBoxs[z]->setTag(PLAYER_3_BUYCOUNT);
						}
					}
					else
					{
						//房主付费
						if (_fufeiRuleCheckBoxs[0]->isSelected())
						{
							showGameFinishCondition(_jushuVec.at(z), deskConfig.buyCounts[z].iBuyCount, deskConfig.buyCounts[z].iAAJewels*index, deskConfig.bFinishCondition);
						}
						else
						{
							showGameFinishCondition(_jushuVec.at(z), deskConfig.buyCounts[z].iBuyCount, deskConfig.buyCounts[z].iAAJewels, deskConfig.bFinishCondition, true);
						}
					}
				}
			}
		}
	}
	else
	{
		//其他游戏也需要根据人数修改消费？
		//for (int z = 0; z < _jushuVec.size(); z++)
		//{
		//	if (_jushuVec.at(z))
		//	{
		//		if (_fufeiRuleCheckBoxs.size() > 0)
		//		{
		//			//房主付费
		//			if (_fufeiRuleCheckBoxs[0]->isSelected())
		//			{
		//				showGameFinishCondition(_jushuVec.at(z), deskConfig.buyCounts[z].iBuyCount, deskConfig.buyCounts[z].iJewels, deskConfig.bFinishCondition);
		//			}
		//			else
		//			{
		//				showGameFinishCondition(_jushuVec.at(z), deskConfig.buyCounts[z].iBuyCount, deskConfig.buyCounts[z].iAAJewels, deskConfig.bFinishCondition, true);
		//			}
		//		}
		//	}
		//}
	}

	//refreshAllCheckBox(_layout_games);
}
void  CreateRoomLayer::checkWanFaSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (_wanfaSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _wanfaSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}

	//refreshAllCheckBox(_layout_games);
}

void CreateRoomLayer::checkThireSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (deskConfig.iGameID == 20200321)
		{//硬胡和非硬胡互斥
			int tag = checkBox->getTag();
			bool isSelected = checkBox->isSelected();
			if (tag < 2 && _thirdSelectCheckBoxs.size() >= 2)
			{
				if (tag == 0)  
				{
					_thirdSelectCheckBoxs.at(0)->setSelected(isSelected);
					_thirdSelectCheckBoxs.at(0)->setTouchEnabled(!isSelected);
					_thirdSelectCheckBoxs.at(1)->setSelected(!isSelected);
					_thirdSelectCheckBoxs.at(1)->setTouchEnabled(isSelected);
				}
				else
				{
					_thirdSelectCheckBoxs.at(0)->setSelected(!isSelected);
					_thirdSelectCheckBoxs.at(0)->setTouchEnabled(isSelected);
					_thirdSelectCheckBoxs.at(1)->setSelected(isSelected);
					_thirdSelectCheckBoxs.at(1)->setTouchEnabled(!isSelected);
				}
			}
			return;
		}
		if (_thirdSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _thirdSelectCheckBoxs)
			{

				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}

	//refreshAllCheckBox(_layout_games);
}

void CreateRoomLayer::checkForthSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (deskConfig.iGameID == 20200321)
		{//算台 算分（算分情况下涨分可选）
			int tag = checkBox->getTag();
			bool bCanSelected = tag == 0 ? false : true;
			for (auto itr : _fifthSelectCheckBoxs)
			{
				itr->setEnabled(bCanSelected);
			}
		}
		if (checkBox->getTag() == 0 && deskConfig.iGameID == 20170708 && checkBox->isSelected())
		{
			if (_NNFanbei)
			{
				_NNFanbei->setString(GBKToUtf8("牛牛x4  牛九x3  牛八x2  牛七x2"));
			}
		}
		else if (checkBox->getTag() == 1 && deskConfig.iGameID == 20170708 && checkBox->isSelected())
		{
			if (_NNFanbei)
			{
				_NNFanbei->setString(GBKToUtf8("牛牛x5  牛九x4  牛八x3  牛七x2"));
			}
		}
		std::string pName = checkBox->getName();
		if (_fourthSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _fourthSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}

		}

	}

	//refreshAllCheckBox(_layout_games);
}

void CreateRoomLayer::checkFithSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (checkBox->isSelected() && (!_thirdSelectCheckBoxs.empty()) && deskConfig.iGameID == 20170708)
		{
			switch (checkBox->getTag())
			{
			case 0:
				for (int i = 0; i < _thirdSelectCheckBoxs.size(); i++)
				{
					if (i == 0)
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(true);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(false);
					}
					else
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(false);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(true);
					}
				}
				_thirdSelectCheckBoxs.at(0)->setVisible(true);
				_thirdSelectCheckBoxs.at(1)->setVisible(true);
				_thirdSelectCheckBoxs.at(2)->setVisible(true);
				_thirdSelectCheckBoxs.at(3)->setVisible(false);
				_thirdSelectCheckBoxs.at(4)->setVisible(false);
				break;
			case 1:
				for (int i = 0; i < _thirdSelectCheckBoxs.size(); i++)
				{
					if (i == 0)
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(true);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(false);
					}
					else
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(false);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(true);
					}
				}
				_thirdSelectCheckBoxs.at(0)->setVisible(true);
				_thirdSelectCheckBoxs.at(1)->setVisible(true);
				_thirdSelectCheckBoxs.at(2)->setVisible(true);
				_thirdSelectCheckBoxs.at(3)->setVisible(true);
				_thirdSelectCheckBoxs.at(4)->setVisible(false);
				break;
			case 2:
				for (int i = 0; i < _thirdSelectCheckBoxs.size(); i++)
				{
					if (i == 4)
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(true);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(false);
					}
					else
					{
						_thirdSelectCheckBoxs.at(i)->setSelected(false);
						_thirdSelectCheckBoxs.at(i)->setTouchEnabled(true);
					}
				}
				_thirdSelectCheckBoxs.at(0)->setVisible(false);
				_thirdSelectCheckBoxs.at(1)->setVisible(false);
				_thirdSelectCheckBoxs.at(2)->setVisible(false);
				_thirdSelectCheckBoxs.at(3)->setVisible(false);
				_thirdSelectCheckBoxs.at(4)->setVisible(true);
				break;
			default:
				break;
			}
		}
		if (_fifthSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _fifthSelectCheckBoxs)
			{
				if (checkBox->isSelected() && (!_sixthSelectCheckBoxs.empty()) && deskConfig.iGameID == 12100004)
				{
					for (int i = 0; i < _sixthSelectCheckBoxs.size(); i++)
					{
						_sixthSelectCheckBoxs.at(i)->setVisible(true);
						if (i == 0)
						{
							_sixthSelectCheckBoxs.at(i)->setSelected(true);
							_sixthSelectCheckBoxs.at(i)->setTouchEnabled(false);
						}
						else
						{
							_sixthSelectCheckBoxs.at(i)->setTouchEnabled(true);
							_sixthSelectCheckBoxs.at(i)->setSelected(false);
						}
					}
				}
				else if (!checkBox->isSelected() && (!_sixthSelectCheckBoxs.empty()) && deskConfig.iGameID == 12100004)
				{
					for (int i = 0; i < _sixthSelectCheckBoxs.size(); i++)
					{
						if (i < 3)
						{
							_sixthSelectCheckBoxs.at(i)->setVisible(true);
						}
						else
						{
							_sixthSelectCheckBoxs.at(i)->setVisible(false);
						}
						if (i == 0)
						{
							_sixthSelectCheckBoxs.at(i)->setSelected(true);
							_sixthSelectCheckBoxs.at(i)->setTouchEnabled(false);
						}
						else
						{
							_sixthSelectCheckBoxs.at(i)->setTouchEnabled(true);
							_sixthSelectCheckBoxs.at(i)->setSelected(false);
						}
					}
				}
				else
				{
					if (checkBox != iter)
					{
						iter->setTouchEnabled(true);
						iter->setSelected(false);
					}
					else
					{
						iter->setSelected(true);
						iter->setTouchEnabled(false);
					}
				}
			}
		}
	}

	//refreshAllCheckBox(_layout_games);
}

void CreateRoomLayer::checkSixSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	int selectCount = 0;
	if (checkBox)
	{
		if (deskConfig.iGameID == 20200321)
		{
			auto pTextField = (HNEditBox*)checkBox->getChildByName("TextField_chahua");
			pTextField->setTouchEnabled(checkBox->isSelected());
			return;
		}
		if (_sixthSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _sixthSelectCheckBoxs)
			{
				if (deskConfig.iGameID == 20170708)
				{
					if (iter->isSelected())
					{
						selectCount++;
					}
				}
				else if (deskConfig.iGameID == 12100004)
				{
					if (checkBox != iter)
					{
						iter->setTouchEnabled(true);

						iter->setSelected(false);
					}
					else
					{
						iter->setSelected(true);
						iter->setTouchEnabled(false);
					}
					if (iter->isSelected())
					{
						_iPlayerNum = iter->getTag();
					}
				}
				else
				{
					if (checkBox != iter)
					{
						iter->setTouchEnabled(true);

						iter->setSelected(false);
					}
					else
					{
						iter->setSelected(true);
						iter->setTouchEnabled(false);
					}
				}
			}
		}
	}
	if (selectCount == _sixthSelectCheckBoxs.size())
	{
		if (_NNPaixing)
		{
			_NNPaixing->setString(GBKToUtf8("全部勾选"));
		}
	}
	else
	{
		if (_NNPaixing)
		{
			_NNPaixing->setString(GBKToUtf8("部分勾选"));
		}
	}

	//refreshAllCheckBox(_layout_games);
}

void CreateRoomLayer::checkSevenSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	if (deskConfig.iGameID == 20200321)
	{
		return;
	}
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (_seventhSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _seventhSelectCheckBoxs)
			{
				if (deskConfig.iGameID == 12100004)
				{
					if (iter->isSelected())
					{
						getSSSJiaMaCardType();
					}
					else
					{
						_cardValue = 0;
						if (_Button_change)
						{
							_Button_change->setVisible(false);
						}
						if (_Text_jiama)
						{
							_Text_jiama->setString(GBKToUtf8("不加马"));
						}
					}
				}
				else
				{
					if (checkBox != iter)
					{
						iter->setTouchEnabled(true);
						iter->setSelected(false);
					}
					else
					{
						iter->setSelected(true);
						iter->setTouchEnabled(false);
					}
				}
			}
		}
	}

	//refreshAllCheckBox(_layout_games);
}

void CreateRoomLayer::checkOutCardTimeSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (_outcardSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _outcardSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}
}

//牌张数选择
void CreateRoomLayer::checkCardNumSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (_begincardSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _begincardSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}
}

//炸弹选项
void CreateRoomLayer::checkBombOptionsSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (_outbombSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _outbombSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}
}

//摘花选项
void CreateRoomLayer::checkPickFlowerSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		m_iCheckRedFlower = checkBox->getTag();
		if (_PickFlowerSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _PickFlowerSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
		//文字提示修改颜色
		if (_Text_tip.size() > 0)
		{
			for (int i = 0; i < _Text_tip.size(); i++)
			{
				if (checkBox->getTag() == 2)
				{
					_Text_tip[i]->setTextColor(i >= checkBox->getTag() ? SELECT_REDFLOWER_COLOR : NORMAL_REDFLOWER_COLOR);
				}
				else
				{
					_Text_tip[i]->setTextColor(i == checkBox->getTag() ? SELECT_REDFLOWER_COLOR : NORMAL_REDFLOWER_COLOR);
				}
			}
		}

		//遮罩层更新
		if (_panel_text_make.size() > 0)
		{
			for (int i = 0; i < _panel_text_make.size(); i++)
			{
				if (checkBox->getTag() == i)
				{
					_panel_text_make[i]->setVisible(false);
				}
				else
				{
					_panel_text_make[i]->setVisible(true);
				}
			}
		}
	}
}

//桌子数选择
void CreateRoomLayer::checkDeskSelectCallback(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* checkBox = dynamic_cast<CheckBox*>(pSender);
	if (checkBox)
	{
		if (_deskSelectCheckBoxs.size() > 0)
		{
			for (CheckBox* iter : _deskSelectCheckBoxs)
			{
				if (checkBox != iter)
				{
					iter->setTouchEnabled(true);
					iter->setSelected(false);
				}
				else
				{
					iter->setSelected(true);
					iter->setTouchEnabled(false);
				}
			}
		}
	}
}

void CreateRoomLayer::ClickOutCardTime(Ref* pSender)
{
	auto selectbtn = dynamic_cast<Button*>(pSender);
	std::string name(selectbtn->getName());
	if (m_Text_OutCardTime)
	{
		if (name.compare("btn_add") == 0)
		{
			_iOutCardTime++;
			if (_iOutCardTime > _OutCardTimes.size() - 1)
			{
				_iOutCardTime = 0;
			}
			m_Text_OutCardTime->setString(StringUtils::format(GBKToUtf8("%d秒"), _OutCardTimes[_iOutCardTime]));
		}
		else
		{
			_iOutCardTime--;
			if (_iOutCardTime < 0)
			{
				_iOutCardTime = _OutCardTimes.size() - 1;
			}
			m_Text_OutCardTime->setString(StringUtils::format(GBKToUtf8("%d秒"), _OutCardTimes[_iOutCardTime]));
		}
	}
}

void CreateRoomLayer::onChickCreateCallBack(Ref* pSender)
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_BUTTON);
	// 创建桌子
	
	// 检测是否有正在进行的比赛
	//Reconnection::getInstance()->getCutRoomInfo(1);
	//Reconnection::getInstance()->onCloseCallBack = [this]() {
		_userDefined = "";
		//付费
		std::vector<CheckBox*>::iterator  itr;
		for (itr = _fufeiRuleCheckBoxs.begin(); itr != _fufeiRuleCheckBoxs.end(); ++itr)
		{
			if ((*itr)->isSelected())
			{
				int tag = (*itr)->getTag();
				_iPay = tag;
				if (tag == 1)
				{
					_iPay = 2;
				}
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
		}
		//第一行局数,0局1课
		for (itr = _firthSelectCheckBoxs.begin(); itr != _firthSelectCheckBoxs.end(); ++itr)
		{
			if ((*itr)->isSelected())
			{
				int tag = (*itr)->getTag();
				_iCount = tag;
				if (_iCount == 99)
				{
					_iType = 1;
				}
				else
				{
					_iType = 0;
				}
			}
		}
		//自定义规则补全（游金数量，底分）
		if (deskConfig.iGameID == 20200321)
		{
			char text[250] = { 0 };
			sprintf(text, "%02d", 0);
			_userDefined += text;
		}
		//第二行
		for (itr = _secondSelectCheckBoxs.begin(); itr != _secondSelectCheckBoxs.end(); ++itr)
		{

			if ((*itr)->isSelected())
			{
				int tag = (*itr)->getTag();
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}

		}
		//第三行
		for (itr = _thirdSelectCheckBoxs.begin(); itr != _thirdSelectCheckBoxs.end(); ++itr)
		{
			if (deskConfig.iGameID == 20200321)
			{//漳浦麻将
				if (_thirdSelectCheckBoxs.size() >= 4 && _outcardSelectCheckBoxs.size() >= 2)
				{
					bool bMustHu = _thirdSelectCheckBoxs.at(0)->isSelected(); //硬胡
					char text[4] = { 0 };
					sprintf(text, "%d", bMustHu ? 0 : 1);  //0硬胡  1非硬胡
					_userDefined += text;

					bool bNotShowCard = _thirdSelectCheckBoxs.at(2)->isSelected(); //不可探牌
					bool bNotEatCard = _thirdSelectCheckBoxs.at(3)->isSelected(); //不可吃牌
					int flagValue = 0x0;
					if (bNotShowCard)
						flagValue |= 0x01;   //不可探牌
					if (bNotEatCard)
						flagValue |= 0x02;  //不可吃牌	

					//设置超时托管方式
					int timeOutValue = 0x0;
					if (_outcardSelectCheckBoxs.at(0)->isSelected())
					{
						timeOutValue = 0x08;  //超时暂停
					}
					else if (_outcardSelectCheckBoxs.at(1)->isSelected())
					{
						timeOutValue = 0x10;  //超时托管
					}
					flagValue |= timeOutValue; 	

					memset(text, 0, sizeof(text));
					sprintf(text, "%c", flagValue + 48);
					_userDefined += text;

				}
				break;
			}
			else if (deskConfig.iGameID == 12100004)
			{
				if ((*itr)->isSelected())
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 0);
					_userDefined += text;
				}
				else
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 1);
					_userDefined += text;
				}
			}
			else
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}
		}

		if (deskConfig.iGameID == 12345678)
		{
			if (m_checkBoxShuangjin->isSelected())
			{
				//选择大小头根据大小头更多选择发送
			}
			else
			{
				int tag = 0;
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
		}
		else if (deskConfig.iGameID == 20200310)
		{
			//默认没有大小头
			int tag = 0;
			char text[250] = { 0 };
			sprintf(text, "%d", tag);
			_userDefined += text;
		}

		//第四行
		for (itr = _fourthSelectCheckBoxs.begin(); itr != _fourthSelectCheckBoxs.end(); ++itr)
		{

			if (deskConfig.iGameID == 12100004)
			{
				if ((*itr)->isSelected())
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 0);
					_userDefined += text;
				}
				else
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 1);
					_userDefined += text;
				}
			}
			else if (deskConfig.iGameID == 12345678)
			{
				//大小头勾选的情况下进行大小头选择处理
				if (m_checkBoxShuangjin->isSelected())
				{
					if ((*itr)->isSelected())
					{
						int tag = (*itr)->getTag();
						char text[250] = { 0 };
						sprintf(text, "%d", tag);
						_userDefined += text;
					}
				}
			}
			else if (deskConfig.iGameID == 20200311)
			{
				//大小头勾选的情况下进行大小头选择处理
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}
			else
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}

		}

		//第五行
		for (itr = _fifthSelectCheckBoxs.begin(); itr != _fifthSelectCheckBoxs.end(); ++itr)
		{
			if (deskConfig.iGameID == 20200321)
			{
				if (_fourthSelectCheckBoxs.size() > 0 && _fourthSelectCheckBoxs.at(0)->isSelected())
				{//算台制 则没有涨分设置
					char text[4] = { 0 };
					sprintf(text, "%d", 0);
					_userDefined += text;
					break;
				}
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[4] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
					break;
				}
			}
			else if (deskConfig.iGameID == 12100004)
			{
				if ((*itr)->isSelected())
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 0);
					_userDefined += text;
				}
				else
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 1);
					_userDefined += text;
				}
			}
			else
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}
		}

		//十三水暂时不做托管时间通讯
		if (deskConfig.iGameID != 12100004 && deskConfig.iGameID != 20200321)
		{
			//设置托管时间处理
			if (m_Text_OutCardTime)
			{
				char text[250] = { 0 };
				sprintf(text, "%d", _iOutCardTime + 1);
				_userDefined += text;
			}
			//设置超时托管方式
			for (itr = _outcardSelectCheckBoxs.begin(); itr != _outcardSelectCheckBoxs.end(); ++itr)
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}
		}

		if (m_checkBoxMethod)
		{
			if (m_checkBoxMethod->isSelected())
			{
				int tag = m_checkBoxMethod->getTag();
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
			else
			{
				//int tag = m_checkBoxMethod->getTag();
				char text[250] = { 0 };
				sprintf(text, "%d", 0);
				_userDefined += text;
			}
		}

		//第六行
		for (itr = _sixthSelectCheckBoxs.begin(); itr != _sixthSelectCheckBoxs.end(); ++itr)
		{
			if (deskConfig.iGameID == 20200321)
			{
				bool bchahua = _sixthSelectCheckBoxs.at(0)->isSelected();
				int value = 0;
				if (bchahua)
				{
					auto pTextField = (HNEditBox*)_sixthSelectCheckBoxs.at(0)->getChildByName("TextField_chahua");
					if (pTextField)
					{
						value = atoi(pTextField->getString().c_str());
					}
				}
				char text[4] = { 0 };
				sprintf(text, "%c", value + 48);
				_userDefined += text;
				break;
			}
			else if (deskConfig.iGameID == 20170708)
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", 1);
					_userDefined += text;
				}
				else
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 0);
					_userDefined += text;
				}
			}
			else
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}

		}
		//第七行
		for (itr = _seventhSelectCheckBoxs.begin(); itr != _seventhSelectCheckBoxs.end(); ++itr)
		{
			if (deskConfig.iGameID == 20200321)
				break;
			if (deskConfig.iGameID == 12100004)
			{
				if ((*itr)->isSelected())
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 0);
					_userDefined += text;
					char hua[250] = { 0 };
					sprintf(hua, "%d", getCardHua(_cardValue));
					_userDefined += hua;
					if (getCardnum(_cardValue) >= 10)
					{
						char num[250] = { 0 };
						sprintf(num, "%d", 9);
						_userDefined += num;
						char num2[250] = { 0 };
						sprintf(num2, "%d", getCardnum(_cardValue) - 9);
						_userDefined += num2;
					}
					else
					{
						char num[250] = { 0 };
						sprintf(num, "%d", getCardnum(_cardValue));
						_userDefined += num;
						char num2[250] = { 0 };
						sprintf(num2, "%d", 0);
						_userDefined += num2;
					}
				}
				else
				{
					char text[250] = { 0 };
					sprintf(text, "%d", 1);
					_userDefined += text;
				}
			}
			else
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}

		}

		//牌张数选择
		for (itr = _begincardSelectCheckBoxs.begin(); itr != _begincardSelectCheckBoxs.end(); ++itr)
		{
			if ((*itr)->isSelected())
			{
				int tag = (*itr)->getTag();
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
		}

		//是否可拆
		if (m_DisarmBomb)
		{
			if (m_DisarmBomb->isSelected())
			{
				int tag = m_DisarmBomb->getTag();
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
			else
			{
				int tag = 0;
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
		}
		
		//四带三，四带二，不可带
		for (itr = _outbombSelectCheckBoxs.begin(); itr != _outbombSelectCheckBoxs.end(); ++itr)
		{
			if ((*itr)->isSelected())
			{
				int tag = (*itr)->getTag();
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
		}

		for (itr = _renshuSelectCheckBoxs.begin(); itr != _renshuSelectCheckBoxs.end(); ++itr)
		{
			if ((*itr)->isSelected())
			{
				int tag = (*itr)->getTag();
				_iPlayerNum = tag;
			}
		}
		if (deskConfig.iGameID == 20171123)
		{
			for (itr = _wanfaSelectCheckBoxs.begin(); itr != _wanfaSelectCheckBoxs.end(); ++itr)
			{
				if ((*itr)->isSelected())
				{
					int tag = (*itr)->getTag();
					char text[250] = { 0 };
					sprintf(text, "%d", tag);
					_userDefined += text;
				}
			}
			if (m_checkBoxShuangjin->isSelected())
			{
				int tag = 0;
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
			else
			{
				int tag = 1;
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}

			if (m_checkBoxQianggang->isSelected())
			{
				int tag = 0;
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
			else
			{
				int tag = 1;
				char text[250] = { 0 };
				sprintf(text, "%d", tag);
				_userDefined += text;
			}
		}

		if (deskConfig.iGameID == 20200321)
		{
			//设置托管时间处理
			if (m_Text_OutCardTime)
			{
				char text[4] = { 0 };
				sprintf(text, "%d", _iOutCardTime + 1);
				_userDefined += text;
			}
		}

		//bool bPositionLimit = deskConfig.iGameID == 20200321 ? (_seventhSelectCheckBoxs.size() > 0 ? _seventhSelectCheckBoxs.at(0)->isSelected() : false) : _iLocation;
		//创建房间发给服务器数据gjd6.14
		if (ClubID > 0)
		{
			if (!CheckRedFlowerInput())
			{
				GamePromptLayer::create()->showPrompt(GBKToUtf8("红花选项存在输入错误！"));
				return;
			}
			std::string     _string = "";
			if (deskConfig.iGameID == 20200310)
			{
				_string = "算牌跑得快";
			}
			else if (deskConfig.iGameID == 20200311)
			{
				_string = "上游跑得快";
			}
			else if (deskConfig.iGameID == 12100004)
			{
				_string = "十三水";
			}
			else if (deskConfig.iGameID == 20171121)
			{
				_string = "泉州麻将";
			}
			else if (deskConfig.iGameID == 20200309)
			{
				_string = "漳州麻将";
			}
			else if (deskConfig.iGameID == 20200321)
			{
				_string = "漳浦麻将";
			}
			else if (deskConfig.iGameID == 20171123)
			{
				_string = "晋江麻将";
			}
			else if (deskConfig.iGameID == 10100003)
			{
				_string = "斗地主";
			}
			else if (12345678 == deskConfig.iGameID)
			{
				_string = "跑得快";
			}
			else if (20170708 == deskConfig.iGameID)
			{
				_string = "牛牛";
			}

			int deskCount = 1;
			std::vector<CheckBox*>::iterator  itr;
			for (itr = _deskSelectCheckBoxs.begin(); itr != _deskSelectCheckBoxs.end(); itr ++)
			{
				if ((*itr)->isSelected())
				{
					deskCount = (*itr)->getTag();
					break;
				}
			}
			 

			//茶楼创建的为楼层
			MSG_GP_I_Club_CreateTeahouse buyDesk;
			memset(&buyDesk, 0, sizeof(MSG_GP_I_Club_CreateTeahouse));
			buyDesk.iClubID = ClubID;
			buyDesk.teahouseId = m_iTeaHouseID;			//(创建茶楼为0，修改设置当前茶楼ID)
			if (m_iTeaHouseGameID != 0)
			{
				buyDesk.gameNameId = m_iTeaHouseGameID;	//(修改玩法之前的游戏ID)
				buyDesk.newGameNameId = deskConfig.iGameID;
			}
			else
			{
				buyDesk.gameNameId = deskConfig.iGameID;
				buyDesk.newGameNameId = 0;
			}
			strcpy(buyDesk.szName, _string.c_str());
			buyDesk.iPlayeCount = _iCount;
			buyDesk.maxDeskCount = deskCount;		//设置默认楼层桌子数目
			buyDesk.bPlayerNum = _iPlayerNum;	//设置游戏人数
			memcpy(buyDesk.szDeskConfig, _userDefined.c_str(), _userDefined.size());
			//创建或者修改的时候获取当前红花配置
			buyDesk.option = detectionRedFlower();
			PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_CreateTeaHouse, &buyDesk, sizeof(MSG_GP_I_Club_CreateTeahouse), HN_SOCKET_CALLBACK(CreateRoomLayer::createTeaHouseRoomResult, this));
			if (m_iTeaHouseID == 0)
			{
				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("创建楼层，请稍候..."), 28);
			}
			else
			{
				LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("修改楼层，请稍候..."), 28);
			}
			//m_iTeaHouseID = 0;
			

			////创建桌子
			//MSG_GP_I_Club_BuyDesk buyDesk;
			//memset(&buyDesk, 0, sizeof(MSG_GP_I_Club_BuyDesk));
			////_iPay = _iPay == 1 ? 2 : _iPay;
			//buyDesk.iUserID = PlatformLogic()->loginResult.dwUserID;
			//buyDesk.iClubID = ClubID;
			//buyDesk.iPlayeCount = _iCount;
			//buyDesk.bIsNeedPassword = _isNeedPassword;
			//buyDesk.bPositionLimit = _iLocation;
			//buyDesk.bPayType = _iPay;
			//buyDesk.iGameID = GameCreator()->getCurrentGameNameID();
			//buyDesk.bPlayerNum = _iPlayerNum;
			//buyDesk.bFinishCondition = _iType;


			//memcpy(buyDesk.szDeskConfig, _userDefined.c_str(), _userDefined.size());
			//PlatformLogic()->sendData(MDM_GP_CLUB, ASS_GP_CLUB_CREATEROOM, &buyDesk, sizeof(MSG_GP_I_Club_BuyDesk),
			//	HN_SOCKET_CALLBACK(CreateRoomLayer::createClubRoomResult, this));
			//LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("创建房间，请稍候..."), 28);
		}
		else{

			MSG_GP_S_BUY_DESK buyDesk;
			memset(&buyDesk, 0, sizeof(MSG_GP_S_BUY_DESK));

			buyDesk.iUserID = PlatformLogic()->loginResult.dwUserID;
			buyDesk.iPlayCount = _iCount;
			buyDesk.bPositionLimit = _iLocation;
			buyDesk.bPayType = _iPay;
			buyDesk.iGameID = GameCreator()->getCurrentGameNameID();
			buyDesk.bPlayerNum = _iPlayerNum;
			buyDesk.bFinishCondition = _iType;

			memcpy(buyDesk.szDeskConfig, _userDefined.c_str(), _userDefined.size());
			PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_BUY_DESK, &buyDesk, sizeof(MSG_GP_S_BUY_DESK),
				HN_SOCKET_CALLBACK(CreateRoomLayer::createRoomMessagesEventSelector, this));
			LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("创建房间，请稍候..."), 28);
		}
	//};
}


void CreateRoomLayer::creatNNRoomNode()
{
	std::string nodeName = StringUtils::format("Node_%u", 20170708);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		//_select_game->setPosition(Vec2(778, 330));
		_select_game->setPosition(_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	auto scrollview = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_1");
	
	auto listPanel = (Node*)scrollview->getChildByName("Panel_difen");

	float offsetY = ClubID > 0 ? -60 : 0;

	//玩法：0明牌抢庄、1经典模式、2通比模式
	auto Text_wanfa = (Text*)nodePanel->getChildByName("Text_wanfa");
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_wanfa%d", i);
		auto checkBox = (CheckBox*)Text_wanfa->getChildByName(nodeName);
		//auto checkBox = (CheckBox*)scrollview->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFithSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fifthSelectCheckBoxs.push_back(checkBox);
	}

	//人数设置
	{
		int renshuCounts[] = { 6, 8 };
		auto Text_renshu = listPanel->getChildByName("Text_renshu");
		Text_renshu->setPositionY(Text_renshu->getPositionY() + offsetY);
		for (int i = 0; i < 2; ++i)
		{
			std::string nodeName = StringUtils::format("CheckBox_%d", i);
			auto checkBox = (CheckBox*)Text_renshu->getChildByName(nodeName);
			checkBox->setTag(renshuCounts[i]);
			auto text_number = dynamic_cast<Text*>(checkBox->getChildByName("Text_number"));
			text_number->setString(StringUtils::format("%d", renshuCounts[i]));

			checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
			if (checkBox->isSelected())
			{
				checkBox->setTouchEnabled(false);
			}
			_renshuSelectCheckBoxs.push_back(checkBox);
		}
		_iPlayerNum = renshuCounts[0];
	}
	
	//底分：1/2/3/4/5
	auto Text_difen = (Text*)listPanel->getChildByName("Text_difen");
	Text_difen->setPositionY(Text_difen->getPositionY() + offsetY);
	for (int i = 0; i < 5; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)Text_difen->getChildByName(nodeName);
		//auto checkBox = (CheckBox*)listPanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}

	//局数
	auto Text_jushu = (Text*)listPanel->getChildByName("Text_jushu");
	Text_jushu->setPositionY(Text_jushu->getPositionY() + offsetY);
	int jushu_tags[] = {8, 16, 20};
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_jushu%d", i);
		auto checkBox = (CheckBox*)Text_jushu->getChildByName(nodeName);
		//auto checkBox = (CheckBox*)listPanel->getChildByName(nodeName);
		checkBox->setTag(jushu_tags[i]);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		auto jewels = dynamic_cast<Text*>(checkBox->getChildByName("Text"));
		if (jewels) jewels->setString(StringUtils::format(GBKToUtf8("%d局"), jushu_tags[i]));
		_firthSelectCheckBoxs.push_back(checkBox);
	}

	//付费：0房主、1AA
	auto Text_Titel_pay = (Text*)listPanel->getChildByName("Text_Titel_pay");
	Text_Titel_pay->setPositionY(Text_Titel_pay->getPositionY() + offsetY);
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)Text_Titel_pay->getChildByName(nodeName);
		//auto checkBox = (CheckBox*)listPanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);
	}

	//翻倍规则：0（4/3/2/2）、1（5/4/3/2）
	auto Text_fbgz = (Text*)listPanel->getChildByName("Text_fbgz");
	Text_fbgz->setPositionY(Text_fbgz->getPositionY() + offsetY);
	auto Button_fbgz = (Button*)Text_fbgz->getChildByName("Button_fbgz");
	auto Image_fanbei = (ImageView*)Text_fbgz->getChildByName("Image_fanbei");

	_NNFanbei = (Text*)Button_fbgz->getChildByName("Text_guize");

	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_%d", i);
		auto checkBox = (CheckBox*)Image_fanbei->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkForthSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fourthSelectCheckBoxs.push_back(checkBox);
	}

	Button_fbgz->addClickEventListener([=](Ref*) {
		if (Image_fanbei->isVisible())
		{
			Image_fanbei->setVisible(false);
		}
		else
		{
			Image_fanbei->setVisible(true);
		}

	});
	
	//牌型设置
	auto Text_paixing = (Text*)listPanel->getChildByName("Text_paixing");
	Text_paixing->setPositionY(Text_paixing->getPositionY() + offsetY);
	auto Button_paixing = (Button*)Text_paixing->getChildByName("Button_paixing");
	auto Image_guize = (ImageView*)Text_paixing->getChildByName("Image_guize");

	_NNPaixing = (Text*)Button_paixing->getChildByName("Text_gouxuan");

	Button_paixing->addClickEventListener([=](Ref*) {
		if (Image_guize->isVisible())
		{
			Image_guize->setVisible(false);
		}
		else
		{
			Image_guize->setVisible(true);
		}
	});

	//特殊牌型
	for (int i = 0; i < 7; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_%d", i);
		auto checkBox = (CheckBox*)Image_guize->getChildByName(nodeName);
		if (checkBox->isVisible()) 
		{
			checkBox->setTag(i);
			checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSixSelectCallback, this)));
			_sixthSelectCheckBoxs.push_back(checkBox);
		}
	}

	//庄家：0自由抢庄，1牛牛上庄，2没牛下庄，3固定坐庄,4无庄家
	_NNzhuangjia = (Text*)listPanel->getChildByName("Text_zhuangjia");
	_NNzhuangjia->setPositionY(_NNzhuangjia->getPositionY() + offsetY);
	for (int i = 0; i < 5; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_zhuangjia%d", i);
		auto checkBox = (CheckBox*)_NNzhuangjia->getChildByName(nodeName);
		//auto checkBox = (CheckBox*)listPanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		if (i > 2)
		{
			checkBox->setVisible(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);
	}

	//俱乐部创建房间显示区别
	if (ClubID >0)isRoomNeedPassWord(listPanel, true);
	else isRoomNeedPassWord(listPanel, false);

}

void CreateRoomLayer::creatZZMJRoomNode()
{
	std::string nodeName = StringUtils::format("Node_%u", 20171120);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	//0局、1课
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//倍数：0 *4、1 *3
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_beishu%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}

	//底分：0 庄10闲5、1 庄15闲10
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);

		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);

	}
	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
			for (int j = 0; j < 2; j++)
			{
				auto jewels = dynamic_cast<Text*>(nodePanel->getChildByName(StringUtils::format("Text_jushu%d", j)));
				if (jewels)
				{
					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
				}
				_jushuVec.push_back(jewels);
			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}
}

void CreateRoomLayer::creatDDZRoomNode()
{
	std::string nodeName = StringUtils::format("Node_%u", 10100003);
	//if (_select_game) _select_game->setVisible(false);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(647.07, 395.91));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	//更新为滑动选择
	auto ScrollView_Create = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_Create");
	//俱乐部创建房间显示区别
	
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//玩法：1经典3经典癞子5天地
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_wanfa%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(1 + 2 * i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}
	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
// 			for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; j--)
// 			{
// 				if (_firthSelectCheckBoxs.at(j) == nullptr) continue;
// 				auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
// 				if (jewels)
// 				{
// 					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
// 				}
// 				_jushuVec.insert(_jushuVec.begin(), jewels);
// 			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);
	}

	////设置出牌时间
	//m_Text_OutCardTime = (Text*)ScrollView_Create->getChildByName("Text_outcardTime_0");
	//m_Text_OutCardTime->setString(GBKToUtf8(OUTCARD_LIST[_iOutCardTime]));
	//m_btn_add = (Button*)ScrollView_Create->getChildByName("btn_add");
	//m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	//m_btn_sub = (Button*)ScrollView_Create->getChildByName("btn_sub");
	//m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	////设置超时操作
	//for (int i = 0; i < 2; i++)
	//{
	//	std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
	//	auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
	//	checkBox->setTag(i);
	//	checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
	//	if (checkBox->isSelected())
	//	{
	//		checkBox->setTouchEnabled(false);
	//	}
	//	_outcardSelectCheckBoxs.push_back(checkBox);
	//}

	if (ClubID > 0)isRoomNeedPassWord(ScrollView_Create, true);
	else isRoomNeedPassWord(ScrollView_Create, false);
}

void CreateRoomLayer::creatRunCrazyRoomNode()
{
	//自定义规则（付费方式之外）第一位：1、首局黑桃3先出   2、每局黑桃3先出
	//第二位：1、1底分，3、3底分，5、5底分
	//第三位：0、没有大小头，1、大2小1，2、大10小5，3、大20小10
	//第四位：3A最大==0，最小==1
	//第五位：超时时间（OUTCARD_LIST）1开始计数1==15、2==30...
	//第六位：0超时暂停，1超时托管
	std::string nodeName = StringUtils::format("Node_%u", 12345678);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(647.07, 395.91));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	//更新为滑动选择
	auto ScrollView_Create = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_Create");
	
	//局数
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//玩法：1、首局黑桃3先出   2、每局黑桃3先出
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_wanfa%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(1 + i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}
	//底分：1、1底分，3、3底分，5、5底分
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(1 + i*2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);
	}
	//人数(2-3)
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i + 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkRenShuSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_renshuSelectCheckBoxs.push_back(checkBox);
	}

	//默认勾选2人
	_iPlayerNum = 2;

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
			//跑得快根据人数显示局数（2人选项局数最后一个为24、3人为32）
// 			for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; j--)
// 			{
// 				if (_firthSelectCheckBoxs.at(j) == nullptr) continue;
// 				auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
// 				if (jewels)
// 				{
// 					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
// 				}
// 				_jushuVec.insert(_jushuVec.begin(), jewels);
// 			}

		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}

	//特色玩法记录
	Layout* nodetese = (Layout*)ScrollView_Create->getChildByName("panel_tese");
	m_LayoutTese = nodetese;
	m_LayoutTese->setVisible(false);
	m_checkBoxShuangjin = (CheckBox*)nodetese->getChildByName("CheckBox_tese0");			//大小头
	m_checkBoxShuangjin->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::shuangjinpinghu, this)));

	//特色更多显示
	m_LayoutMore = (Layout*)m_LayoutTese->getChildByName("panel_tese_more");
	m_LayoutMore->setVisible(false);

	//大小头更多选择
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_tese_more%d", i);
		auto checkBox = (CheckBox*)m_LayoutMore->getChildByName(nodeName);
		checkBox->setTag(i+1);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkForthSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fourthSelectCheckBoxs.push_back(checkBox);
	}

	//规则：3A最大==0，最小==1
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_guize%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFithSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fifthSelectCheckBoxs.push_back(checkBox);
	}
	std::vector<int> times = { 15, 30, 60, 90 };
	_OutCardTimes.swap(times);
	//设置出牌时间
	m_Text_OutCardTime = (Text*)ScrollView_Create->getChildByName("Text_outcardTime_0");
	m_Text_OutCardTime->setString(StringUtils::format(GBKToUtf8("%d秒"), _OutCardTimes[_iOutCardTime]));
	m_btn_add = (Button*)ScrollView_Create->getChildByName("btn_add");
	m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	m_btn_sub = (Button*)ScrollView_Create->getChildByName("btn_sub");
	m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	//设置超时操作
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
		auto checkBox = (CheckBox*)ScrollView_Create->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_outcardSelectCheckBoxs.push_back(checkBox);
	}

	//俱乐部创建房间显示区别
	if (ClubID > 0)isRoomNeedPassWord(ScrollView_Create, true);
	else isRoomNeedPassWord(ScrollView_Create, false);
}

void CreateRoomLayer::create510KRoomNode()
{
	std::string nodeName = StringUtils::format("Node_%u", 10100008);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");

	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
			for (int j = 0; j < 2; j++)
			{
				auto jewels = dynamic_cast<Text*>(nodePanel->getChildByName(StringUtils::format("Text_jushu%d", j)));
				if (jewels)
				{
					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
				}
				_jushuVec.push_back(jewels);
			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}
}

void CreateRoomLayer::creatSSSRoomNode()
{
	std::string nodeName = StringUtils::format("Node_%u", 12100004);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(Vec2(646.80, 396.55));//_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	//auto listView = (ListView*)_select_game->getChildByName("ListView_1");
	//listView->setScrollBarEnabled(false);
	auto nodePanel = (Layout*)_select_game->getChildByName("Panel_game");
	auto scrollview = (ui::ScrollView*)nodePanel->getChildByName("ScrollView_1");
	_Button_change = (Button*)scrollview->getChildByName("Button_change");
	_Button_change->addTouchEventListener(CC_CALLBACK_2(CreateRoomLayer::changeButtonClickCallBack, this));
	_Button_change->setVisible(false);
	//_Text_jiama = (Text*)scrollview->getChildByName("Text_jiama");

	//0局、1课
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//上庄：0通比，1抢庄，2上庄
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_zhuang%d", i);
		auto checkBox = (CheckBox*)scrollview->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);
	}
	//玩法：0加鬼1不加
	{
		std::string jiaguiName = StringUtils::format("CheckBox_wanfa%d", 0);
		auto jiaguicheckBox = (CheckBox*)scrollview->getChildByName(jiaguiName);
		_thirdSelectCheckBoxs.push_back(jiaguicheckBox);

	}

	//全打：0是，1否
	{
		std::string quandanodeName = StringUtils::format("CheckBox_gaoji%d", 0);
		auto quandacheckBox = (CheckBox*)scrollview->getChildByName(quandanodeName);
		_fourthSelectCheckBoxs.push_back(quandacheckBox);
	}
	//加两门
	{
		std::string jiamennodeName = StringUtils::format("CheckBox_gaoji%d", 1);
		auto jiamencheckBox = (CheckBox*)scrollview->getChildByName(jiamennodeName);
		jiamencheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFithSelectCallback, this)));
		_fifthSelectCheckBoxs.push_back(jiamencheckBox);
	}
	//人数
	for (int i = 0; i < 5; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_renshu%d", i);
		auto checkBox = (CheckBox*)scrollview->getChildByName(nodeName);
		checkBox->setTag(i + 2);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSixSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_sixthSelectCheckBoxs.push_back(checkBox);
	}

	_iPlayerNum = 2; //(默认游戏人数2人)
	//加马
	{
		std::string jiamanodeName = StringUtils::format("CheckBox_wanfa%d", 1);
		auto jiamacheckBox = (CheckBox*)scrollview->getChildByName(jiamanodeName);
		_Text_jiama = (Text*)jiamacheckBox->getChildByName("Text");
		jiamacheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSevenSelectCallback, this)));
		if (jiamacheckBox->isSelected())
		{
			getSSSJiaMaCardType();
		}
		else
		{
			_Text_jiama->setString(GBKToUtf8("不加马"));
		}
		_seventhSelectCheckBoxs.push_back(jiamacheckBox);
	}

	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)scrollview->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
// 			for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; j--)
// 			{
// 				auto checkBox1 = (CheckBox*)nodePanel->getChildByName(StringUtils::format("CheckBox_count%d", j));
// 				auto jewels = dynamic_cast<Text*>(checkBox1->getChildByName("Text"));
// 				if (jewels)
// 				{
// 					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
// 				}
// 				_jushuVec.insert(_jushuVec.begin(), jewels);
// 			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);
	}
	std::vector<int> times = { 15, 30, 60, 90 };
	_OutCardTimes.swap(times);
	//设置出牌时间
	m_Text_OutCardTime = (Text*)scrollview->getChildByName("Text_outcardTime");
	m_Text_OutCardTime->setString(StringUtils::format(GBKToUtf8("%d秒"), _OutCardTimes[_iOutCardTime]));
	m_btn_add = (Button*)scrollview->getChildByName("btn_add");
	m_btn_add->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));
	m_btn_sub = (Button*)scrollview->getChildByName("btn_sub");
	m_btn_sub->addClickEventListener(CC_CALLBACK_1(CreateRoomLayer::ClickOutCardTime, this));

	//设置超时操作
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_outTime_%d", i);
		auto checkBox = (CheckBox*)scrollview->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkOutCardTimeSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_outcardSelectCheckBoxs.push_back(checkBox);
	}

	//俱乐部创建房间显示区别
	if (ClubID > 0)isRoomNeedPassWord(scrollview, true);
	else isRoomNeedPassWord(scrollview, false);
}

void CreateRoomLayer::creatXMMJRoomNode()
{
	std::string nodeName = StringUtils::format("Node_%u", 20171122);
	_select_game = dynamic_cast<Node*>(_layout_games->getChildByName(nodeName));
	_jushuVec.clear();
	if (!_select_game)
	{
		_select_game = CSLoader::createNode("platform/createRoomUI/createUi/games/" + nodeName + ".csb");
		_select_game->setName(nodeName);
		_layout_games->addChild(_select_game);
		_select_game->setPosition(_layout_games->getContentSize() / 2);
	}
	else
	{
		_select_game->setVisible(true);
	}
	auto nodeList = (ListView*)_select_game->getChildByName("ListView_1");
	nodeList->setScrollBarEnabled(false);
	auto nodePanel = (Layout*)nodeList->getChildByName("Panel_game");
	//0局、1课
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_count%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFirstSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_firthSelectCheckBoxs.push_back(checkBox);
	}
	//倍数：0 *4、1 *3
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_beishu%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSecondSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_secondSelectCheckBoxs.push_back(checkBox);

	}

	//底分：0 庄10闲5、1 庄15闲10
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_difen%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkThireSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_thirdSelectCheckBoxs.push_back(checkBox);

	}

	//大牌：0 有、1 无
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_dapai%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);

		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkForthSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fourthSelectCheckBoxs.push_back(checkBox);

	}
	//结算：0 比金、1打课、2花金
	for (int i = 0; i < 3; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_jeisuan%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		if (i == 1)
		{
			checkBox->setVisible(false);
		}
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFithSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_fifthSelectCheckBoxs.push_back(checkBox);

	}
	//放胡：0单，1通
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_fanghu%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkSixSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
		}
		_sixthSelectCheckBoxs.push_back(checkBox);

	}
	//付费：0房主、1AA
	for (int i = 0; i < 2; i++)
	{
		std::string nodeName = StringUtils::format("CheckBox_pay%d", i);
		auto checkBox = (CheckBox*)nodePanel->getChildByName(nodeName);
		checkBox->setTag(i);
		checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkFuFeiSelectCallback, this)));
		if (checkBox->isSelected())
		{
			checkBox->setTouchEnabled(false);
			for (int j = 0; j < 2; j++)
			{
				auto jewels = dynamic_cast<Text*>(nodePanel->getChildByName(StringUtils::format("Text_jushu%d", j)));
				if (jewels)
				{
					showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
				}
				_jushuVec.push_back(jewels);
			}
		}
		_fufeiRuleCheckBoxs.push_back(checkBox);

	}
}

void CreateRoomLayer::isRoomNeedPassWord(Node* Parent, bool isShow)
{
	_jushuVec.clear();
	for (int j = _firthSelectCheckBoxs.size() - 1; j >= 0; --j)
	{
		auto checkBox = _firthSelectCheckBoxs.at(j);
		if (checkBox)
		{
			auto jewels = dynamic_cast<Text*>(_firthSelectCheckBoxs.at(j)->getChildByName("Text"));
			if (!isShow) {
// 				if (checkBox->isSelected())
// 				{
				checkBox->setTag(deskConfig.buyCounts[j].iBuyCount);
					if (jewels)
					{
						showGameFinishCondition(jewels, deskConfig.buyCounts[j].iBuyCount, deskConfig.buyCounts[j].iJewels, deskConfig.bFinishCondition);
					}
//				}
			}
			_jushuVec.insert(_jushuVec.begin(), jewels);
		}
	}

	if (!Parent)return;

	//加密方式
	auto Text_jiami = dynamic_cast<Text*>(Parent->getChildByName("Text_jiami"));
	if (Text_jiami)
	{
		Text_jiami->setVisible(false);//俱乐部6.11加密选项取消
		for (int i = 0; i < 2; i++)
		{
			auto NodeName1 = StringUtils::format("CheckBox_jiami%d", i);
			auto NodeName2 = StringUtils::format("Text_jiami%d", i);
			auto CheckBox_jiami = dynamic_cast<CheckBox*>(Parent->getChildByName(NodeName1));
			auto Text_jiami = dynamic_cast<Text*>(Parent->getChildByName(NodeName2));
			CheckBox_jiami->setVisible(false);
			if (Text_jiami)
				Text_jiami->setVisible(false);
		}
	}

	//付费方式
	auto CheckBox_pay1 = dynamic_cast<CheckBox*>(Parent->getChildByName("CheckBox_pay1"));
	if (!CheckBox_pay1)
	{
		auto Text_Titel_pay = Parent->getChildByName("Text_Titel_pay");
		CheckBox_pay1 = dynamic_cast<CheckBox*>(Text_Titel_pay->getChildByName("CheckBox_pay1"));
	}
	if (CheckBox_pay1)
		CheckBox_pay1->setVisible(!isShow);

	if (isShow)
	{
		auto CheckBox_pay0 = dynamic_cast<CheckBox*>(Parent->getChildByName("CheckBox_pay0"));
		if (!CheckBox_pay0)
		{
			auto Text_Titel_pay = Parent->getChildByName("Text_Titel_pay");
			CheckBox_pay0 = dynamic_cast<CheckBox*>(Text_Titel_pay->getChildByName("CheckBox_pay0"));
		}
		if (CheckBox_pay0)
		{
			auto pText = dynamic_cast<Text*>(CheckBox_pay0->getChildByName("Text_pay0"));
			if (pText)
				pText->setString(GBKToUtf8("楼主付费"));
		}
	}

	//桌子数
	auto Text_zhuozi = Parent->getChildByName("Text_zhuozi");
	if (Text_zhuozi) 
	{
		Text_zhuozi->setVisible(isShow);
		if (isShow) 
		{
			//int zhuoziNumbers[] = { 1, 2, 3, 6 };
			for (int i = 0; i < 4; i++)
			{
				auto NodeName = StringUtils::format("CheckBox_%d", i);
				auto checkBox = dynamic_cast<CheckBox*>(Text_zhuozi->getChildByName(NodeName));
				checkBox->setTag(deskConfig.buyCounts[i].iBuyCount);
				checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkDeskSelectCallback, this)));
				if (checkBox->isSelected())
				{
					checkBox->setTouchEnabled(false);
					showGameFinishCondition(nullptr, deskConfig.buyCounts[i].iBuyCount, deskConfig.buyCounts[i].iJewels, deskConfig.bFinishCondition);

					auto jewels = dynamic_cast<Text*>(checkBox->getChildByName("Text_number"));
					if (jewels)
					{
						jewels->setString(StringUtils::format("%d", deskConfig.buyCounts[i].iBuyCount));
					}
				}
				_deskSelectCheckBoxs.push_back(checkBox);
			}
		}
	}



}
void CreateRoomLayer::requestNotice(int one)
{
	/*std::string url = HNPlatformConfig()->getNoticeUrl();
	url.append("Type=GetSystemMsg");
	url.append("&pageSize=1");
	string str = StringUtils::format("&pageindex=%u", one);
	url.append(str);*/
	std::string url = "";
	switch (one)
	{
	case 1: //滚动公告
		url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/RNotice.php");
		break;
	case 2: //界面公告
		url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/INotice.php");
		break;
	case 3: //帮助公告
		url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), "/hyyl/HNotice.php");
		break;
	default:
		break;
	}
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("create_room_notice", HttpRequest::Type::GET, url);
	_notice_num = one;
}

void CreateRoomLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;

	if (requestName.compare("create_room_notice") == 0)
	{
		rapidjson::Document doc;
		doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

		if (doc.HasParseError() || !doc.IsObject())
		{
			return;
		}
		if (doc.HasMember("List") && doc["List"].IsArray() && doc["List"].Capacity() > 0)
		{
			if (1 == _notice_num)
			{
				for (int i = 0; i < doc["List"].Size(); i++)
				{
					rapidjson::Value& value = doc["List"][(rapidjson::SizeType)i];
					std::string message = value["MContent"].GetString();
					GameNotice::getInstance()->postMessage(message, 3);
				}
			}
			else if (2 == _notice_num)
			{
				auto notice = NoticeLayer::create();
				for (int i = 0; i < doc["List"].Size(); i++)
				{
					rapidjson::Value& value = doc["List"][(rapidjson::SizeType)i];
					std::string szTitle = value["MTitle"].GetString();
					std::string szContent = value["MContent"].GetString();
					std::string szType = value["Type"].GetString();
					notice->addTextInfo(szTitle, szContent, szType);
				}
				notice->setName("notice");
				notice->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);
				notice->updateShow();
			}
			else if (3 == _notice_num)
			{
				auto rules = GameRules::create();
				for (int i = 0; i < doc["List"].Size(); i++)
				{
					rapidjson::Value& value = doc["List"][(rapidjson::SizeType)i];
					std::string szTitle = value["MTitle"].GetString();
					std::string szContent = value["MContent"].GetString();
					std::string szType = value["Type"].GetString();
					rules->addTextInfo(szTitle, szContent,szType);
				}
				rules->setName("rules");
				rules->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, CHILD_LAYER_ZORDER, CHILD_LAYER_TAG);
				rules->updateShow();
			}
		}
	}
}

void CreateRoomLayer::refreshAllCheckBox(Node* parent)
{
	if (parent == nullptr) return;

	auto allChildrens = parent->getChildren();
	for (auto it = allChildrens.begin(); it != allChildrens.end();it++)
	{
		if (*it == nullptr) continue;
		auto pCheckBox = dynamic_cast<CheckBox*>(*it);
		if (pCheckBox)
		{
			auto pImg = dynamic_cast<ImageView*>(pCheckBox->getChildByName("Image_child"));
			if (pImg)
			{
				if (pCheckBox->isSelected())
				{
					pImg->loadTexture(SELECT_FK_IMG_FILE);
				}
				else
				{
					pImg->loadTexture(NORMAL_FK_IMG_FILE);
				}
			}
			auto pText = dynamic_cast<Text*>(pCheckBox->getChildByName("Text"));
			if (pText)
			{
				if (pCheckBox->isSelected())
				{
					pText->setTextColor(SELECT_TEXT_COLOR);
					pText->enableOutline(NORMAL_TEXT_COLOR);
				}
				else
				{
					pText->setTextColor(NORMAL_TEXT_COLOR);
					pText->enableOutline(Color4B(255,255,255,0),1);
				}
			}
		}
		else
		{
			refreshAllCheckBox(*it);
		}
	}
}

void CreateRoomLayer::initRedFlowerCheck()
{
	if (_Panel_RedFlower)
	{
		//默认是否开启红花房
 		auto CheckBox_RedFlower = (CheckBox*)_Panel_RedFlower->getChildByName("CheckBox_RedFlower");
		
		CheckBox_RedFlower->addEventListener([=](Ref* ref, ui::CheckBox::EventType type) {
			//
			CheckBox* checkBox = dynamic_cast<CheckBox*>(ref);
			if (checkBox)
			{
				if (checkBox->isSelected())
				{
					_sp_RedFlower->setTexture("platform/createRoomUI/createUi/res/out_game/btn_zhaihua_l.png");
					Panel_CheckBox->setVisible(true);
					m_bCheckRedFlower = true;
				}
				else
				{
					_sp_RedFlower->setTexture("platform/createRoomUI/createUi/res/out_game/btn_zhaihua_a.png");
					Panel_CheckBox->setVisible(false);
					m_bCheckRedFlower = false;
				}
			}
		});

		_Panel_RedFlower->setVisible(ClubID > 0);
		if (_Panel_RedFlower->isVisible()) 
		{
			CheckBox_RedFlower->setSelected(true);
			_sp_RedFlower->setTexture("platform/createRoomUI/createUi/res/out_game/btn_zhaihua_l.png");
			Panel_CheckBox->setVisible(true);
			m_bCheckRedFlower = true;
		}
		else
		{
			Panel_CheckBox->setVisible(false);
			_sp_RedFlower->setTexture("platform/createRoomUI/createUi/res/out_game/btn_zhaihua_a.png");
			m_bCheckRedFlower = false;
		}

		//摘花选项
		for (int i = 0; i < 3; i++)
		{
			std::string nodeName = StringUtils::format("CheckBox_PickFlower_%d", i);
			auto checkBox = (CheckBox*)Panel_CheckBox->getChildByName(nodeName);
			checkBox->setTag(i);
			checkBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CreateRoomLayer::checkPickFlowerSelectCallback, this)));
			_PickFlowerSelectCheckBoxs.push_back(checkBox);
		}

		//摘花提示显示
		for (int i = 2; i < 9; i++)
		{
			std::string nodeName = StringUtils::format("Text_tip_%d", i);
			auto Text_tip = (Text*)Panel_CheckBox->getChildByName(nodeName);
			_Text_tip.push_back(Text_tip);
			if (i == 2)
			{
				Text_tip->setTextColor(SELECT_REDFLOWER_COLOR);
			}
			else
			{
				Text_tip->setTextColor(NORMAL_REDFLOWER_COLOR);
			}
		}

		//屏蔽输入框基础容器
		for (int i = 1; i < 4; i++)
		{
			std::string nodeName = StringUtils::format("panel_text_make_%d", i);
			auto panel_text = (Layout*)Panel_CheckBox->getChildByName(nodeName);
			_panel_text_make.push_back(panel_text);
			if (i == 1)
			{
				panel_text->setVisible(false);
			}
			else
			{
				panel_text->setVisible(true);
			}
		}

		//初始化输入组件
		TextF_ScoreToRedFlowers = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_1");
		TextF_ScoreToRedFlowers->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_ScoreToRedFlowers->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_MinRedFlowersForJoin = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_2");
		TextF_MinRedFlowersForJoin->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_MinRedFlowersForJoin->setTextVerticalAlignment(TextVAlignment::CENTER);
		
		TextF_AATakeCount = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_3");
		TextF_AATakeCount->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_AATakeCount->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeScoreWinker = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_4");
		TextF_TakeScoreWinker->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeScoreWinker->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeNumWinker = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_5");
		TextF_TakeNumWinker->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeNumWinker->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeScoreRanges1 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_6");
		TextF_TakeScoreRanges1->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeScoreRanges1->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeNumRanges1 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_7");
		TextF_TakeNumRanges1->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeNumRanges1->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeScoreRanges2 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_8");
		TextF_TakeScoreRanges2->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeScoreRanges2->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeNumRanges2 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_9");
		TextF_TakeNumRanges2->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeNumRanges2->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeScoreRanges3 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_10");
		TextF_TakeScoreRanges3->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeScoreRanges3->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeNumRanges3 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_11");
		TextF_TakeNumRanges3->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeNumRanges3->setTextVerticalAlignment(TextVAlignment::CENTER);
		
		TextF_TakeScoreRanges4 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_12");
		TextF_TakeScoreRanges4->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeScoreRanges4->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeNumRanges4 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_13");
		TextF_TakeNumRanges4->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeNumRanges4->setTextVerticalAlignment(TextVAlignment::CENTER);
		
		TextF_TakeScoreRanges5 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_14");
		TextF_TakeScoreRanges5->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeScoreRanges5->setTextVerticalAlignment(TextVAlignment::CENTER);

		TextF_TakeNumRanges5 = (TextField*)Panel_CheckBox->getChildByName("TextField_RedF_15");
		TextF_TakeNumRanges5->setTextHorizontalAlignment(TextHAlignment::CENTER);
		TextF_TakeNumRanges5->setTextVerticalAlignment(TextVAlignment::CENTER);

	}
}

void CreateRoomLayer::setRedFlowerCheck(SClubTeahouseOption Data)
{
	if (_Panel_RedFlower)
	{
		//默认是否开启红花房
		auto CheckBox_RedFlower = (CheckBox*)_Panel_RedFlower->getChildByName("CheckBox_RedFlower");
		if (Data.m_bOpenRedFlowersMode)
		{
			CheckBox_RedFlower->setSelected(true);
			_sp_RedFlower->setTexture("platform/createRoomUI/createUi/res/out_game/btn_zhaihua_l.png");
			Panel_CheckBox->setVisible(true);
			m_bCheckRedFlower = true;

			//分数红花比例
			TextF_ScoreToRedFlowers->setString(to_string((int)Data.m_ScoreToRedFlowers));

			//最低进场分数
			TextF_MinRedFlowersForJoin->setString(to_string(Data.m_MinRedFlowersOnJoinDesk));

			if (Data.m_Type == _TakeType_AA)
			{
				checkPickFlowerSelectCallback(_PickFlowerSelectCheckBoxs[0], CheckBox::EventType::SELECTED);
				TextF_AATakeCount->setString(to_string(Data.m_AATakeCount));
			}
			else
			{
				if (Data.m_TakeScoreRanges[1] == 0)
				{
					checkPickFlowerSelectCallback(_PickFlowerSelectCheckBoxs[1], CheckBox::EventType::SELECTED);
					TextF_TakeScoreWinker->setString(to_string(Data.m_TakeScoreRanges[0]));
					TextF_TakeNumWinker->setString(to_string(Data.m_TakeNumRanges[0]));
				}
				else
				{
					checkPickFlowerSelectCallback(_PickFlowerSelectCheckBoxs[2], CheckBox::EventType::SELECTED);
					if (Data.m_TakeScoreRanges[2] == 0)
					{
						TextF_TakeScoreRanges1->setString(to_string(Data.m_TakeScoreRanges[0]));
						TextF_TakeNumRanges1->setString(to_string(Data.m_TakeNumRanges[0]));
						TextF_TakeScoreRanges2->setString(to_string(Data.m_TakeScoreRanges[1]));
						TextF_TakeNumRanges2->setString(to_string(Data.m_TakeNumRanges[1]));
					}
					else if (Data.m_TakeScoreRanges[3] == 0)
					{
						TextF_TakeScoreRanges1->setString(to_string(Data.m_TakeScoreRanges[0]));
						TextF_TakeNumRanges1->setString(to_string(Data.m_TakeNumRanges[0]));
						TextF_TakeScoreRanges2->setString(to_string(Data.m_TakeScoreRanges[1]));
						TextF_TakeNumRanges2->setString(to_string(Data.m_TakeNumRanges[1]));
						TextF_TakeScoreRanges3->setString(to_string(Data.m_TakeScoreRanges[2]));
						TextF_TakeNumRanges3->setString(to_string(Data.m_TakeNumRanges[2]));
					}
					else if (Data.m_TakeScoreRanges[4] == 0)
					{
						TextF_TakeScoreRanges1->setString(to_string(Data.m_TakeScoreRanges[0]));
						TextF_TakeNumRanges1->setString(to_string(Data.m_TakeNumRanges[0]));
						TextF_TakeScoreRanges2->setString(to_string(Data.m_TakeScoreRanges[1]));
						TextF_TakeNumRanges2->setString(to_string(Data.m_TakeNumRanges[1]));
						TextF_TakeScoreRanges3->setString(to_string(Data.m_TakeScoreRanges[2]));
						TextF_TakeNumRanges3->setString(to_string(Data.m_TakeNumRanges[2]));
						TextF_TakeScoreRanges4->setString(to_string(Data.m_TakeScoreRanges[3]));
						TextF_TakeNumRanges4->setString(to_string(Data.m_TakeNumRanges[3]));
					}
					else
					{
						TextF_TakeScoreRanges1->setString(to_string(Data.m_TakeScoreRanges[0]));
						TextF_TakeNumRanges1->setString(to_string(Data.m_TakeNumRanges[0]));
						TextF_TakeScoreRanges2->setString(to_string(Data.m_TakeScoreRanges[1]));
						TextF_TakeNumRanges2->setString(to_string(Data.m_TakeNumRanges[1]));
						TextF_TakeScoreRanges3->setString(to_string(Data.m_TakeScoreRanges[2]));
						TextF_TakeNumRanges3->setString(to_string(Data.m_TakeNumRanges[2]));
						TextF_TakeScoreRanges4->setString(to_string(Data.m_TakeScoreRanges[3]));
						TextF_TakeNumRanges4->setString(to_string(Data.m_TakeNumRanges[3]));
						TextF_TakeScoreRanges5->setString(to_string(Data.m_TakeScoreRanges[4]));
						TextF_TakeNumRanges5->setString(to_string(Data.m_TakeNumRanges[4]));
					}
				}
			}
		}
	}
}

SClubTeahouseOption CreateRoomLayer::detectionRedFlower()
{
	SClubTeahouseOption Result;
	memset(&Result, 0, sizeof(SClubTeahouseOption));
	if (m_bCheckRedFlower)
	{
		Result.m_bOpenRedFlowersMode = true;
		//检查输入（分数红花比例，最低进入红花数量）
		string str_score = TextF_ScoreToRedFlowers->getString();
		auto score_len = strlen(Utf8ToGBK(str_score));
		bool isNum = Tools::verifyNumber(str_score);
		if (isNum && score_len > 0)
		{
			Result.m_ScoreToRedFlowers = std::stof(str_score);
		}
		else
		{
			Result.m_ScoreToRedFlowers = 1.0;
		}
		str_score = TextF_MinRedFlowersForJoin->getString();
		score_len = strlen(Utf8ToGBK(str_score));
		isNum = Tools::verifyNumber(str_score);
		if (isNum && score_len > 0)
		{
			Result.m_MinRedFlowersOnJoinDesk = std::stoi(str_score);
		}
		else
		{
			Result.m_MinRedFlowersOnJoinDesk = 1;
		}
		//区分摘花选择
		if (m_iCheckRedFlower == 0)
		{
			//AA摘花
			Result.m_Type = _TakeType_AA;
			str_score = TextF_AATakeCount->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_AATakeCount = std::stoi(str_score);
			}
			else
			{
				Result.m_AATakeCount = 1;
			}
		}
		else if (m_iCheckRedFlower == 1)
		{
			//大赢家固定摘花
			Result.m_Type = _TakeType_Step;
			str_score = TextF_TakeScoreWinker->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeScoreRanges[0] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeScoreRanges[0] = 1;
			}
			str_score = TextF_TakeNumWinker->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeNumRanges[0] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeNumRanges[0] = 1;
			}
		}
		else
		{
			//大赢家阶梯摘花
			Result.m_Type = _TakeType_Step;
			//一阶
			str_score = TextF_TakeScoreRanges1->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeScoreRanges[0] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeScoreRanges[0] = 1;
			}
			str_score = TextF_TakeNumRanges1->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeNumRanges[0] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeNumRanges[0] = 1;
			}
			//二阶
			str_score = TextF_TakeScoreRanges2->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeScoreRanges[1] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeScoreRanges[1] = 0;
			}
			str_score = TextF_TakeNumRanges2->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeNumRanges[1] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeNumRanges[1] = 0;
			}
			//三阶
			str_score = TextF_TakeScoreRanges3->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeScoreRanges[2] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeScoreRanges[2] = 0;
			}
			str_score = TextF_TakeNumRanges3->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeNumRanges[2] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeNumRanges[2] = 0;
			}
			//四阶
			str_score = TextF_TakeScoreRanges4->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeScoreRanges[3] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeScoreRanges[3] = 0;
			}
			str_score = TextF_TakeNumRanges4->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeNumRanges[3] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeNumRanges[3] = 0;
			}
			//五阶
			str_score = TextF_TakeScoreRanges5->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeScoreRanges[4] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeScoreRanges[4] = 0;
			}
			str_score = TextF_TakeNumRanges5->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				Result.m_TakeNumRanges[4] = std::stoi(str_score);
			}
			else
			{
				Result.m_TakeNumRanges[4] = 0;
			}
		}
	}
	else
	{
		Result.m_bOpenRedFlowersMode = false;
		Result.m_Type = _TakeType_None;
	}

	return Result;
}

bool CreateRoomLayer::CheckRedFlowerInput()
{
	if (m_bCheckRedFlower)
	{
		//Result.m_bOpenRedFlowersMode = true;
		//检查输入（分数红花比例，最低进入红花数量）
		string str_score = TextF_ScoreToRedFlowers->getString();
		auto score_len = strlen(Utf8ToGBK(str_score));
		bool isNum = Tools::verifyNumber(str_score);
		if (isNum && score_len > 0)
		{
			//Result.m_ScoreToRedFlowers = std::stof(str_score);
		}
		else
		{
			//GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为数字"));
			return false;
		}
		str_score = TextF_MinRedFlowersForJoin->getString();
		score_len = strlen(Utf8ToGBK(str_score));
		isNum = Tools::verifyNumber(str_score);
		if (isNum && score_len > 0)
		{
			//Result.m_MinRedFlowersOnJoinDesk = std::stoi(str_score);
		}
		else
		{
			//GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为数字"));
			return false;
		}
		//区分摘花选择
		if (m_iCheckRedFlower == 0)
		{
			//AA摘花
			//Result.m_Type = _TakeType_AA;
			str_score = TextF_AATakeCount->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				//Result.m_AATakeCount = std::stoi(str_score);
			}
			else
			{
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为数字"));
				return false;
			}
		}
		else if (m_iCheckRedFlower == 1)
		{
			//大赢家固定摘花
			//Result.m_Type = _TakeType_Step;
			str_score = TextF_TakeScoreWinker->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				//Result.m_TakeScoreRanges[0] = std::stoi(str_score);
			}
			else
			{
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为数字"));
				return false;
			}
			str_score = TextF_TakeNumWinker->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				//Result.m_TakeNumRanges[0] = std::stoi(str_score);
			}
			else
			{
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为数字"));
				return false;
			}
		}
		else
		{
			//大赢家阶梯摘花
			//Result.m_Type = _TakeType_Step;
			str_score = TextF_TakeScoreRanges1->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				//Result.m_TakeScoreRanges[0] = std::stoi(str_score);
			}
			else
			{
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为数字"));
				return false;
			}
			str_score = TextF_TakeNumRanges1->getString();
			score_len = strlen(Utf8ToGBK(str_score));
			isNum = Tools::verifyNumber(str_score);
			if (isNum && score_len > 0)
			{
				//Result.m_TakeNumRanges[0] = std::stoi(str_score);
			}
			else
			{
				//GamePromptLayer::create()->showPrompt(GBKToUtf8("输入必须为数字"));
				return false;
			}
		}
	}
	return true;
}
void CreateRoomLayer::editBoxEditingDidBegin(ui::EditBox* editBox)
{
	HNLOG("CreateRoomLayer::editBoxEditingDidBegin");
	std::string szName = editBox->getName();
	int tag = editBox->getTag();
	if (szName == "TextField_chahua")  //插花
	{

	}
}

void CreateRoomLayer::editBoxEditingDidEnd(ui::EditBox* editBox)
{
	std::string szName = editBox->getName();
	int tag = editBox->getTag();
	std::string szText = editBox->getText();
	if (szName == "TextField_chahua")//插花
	{
		if (tag == 0)
		{
			int num = atoi(szText.c_str());
			if (num > 100)
			{
				editBox->setText("100");
			}
			else if (num < 1)
			{
				editBox->setText("1");
			}
		}
	}
}

void CreateRoomLayer::editBoxTextChanged(ui::EditBox* editBox, const std::string& text)
{
	std::string szName = editBox->getName();
	int tag = editBox->getTag();
	std::string szText = editBox->getText();
	if (szName == "TextField_chahua")//插花
	{
		if (tag == 0)
		{
			int num = atoi(szText.c_str());
			if (num > 100)
			{
				editBox->setText("100");
			}
		}
	}
}

void CreateRoomLayer::editBoxReturn(ui::EditBox* editBox)
{
	HNLOG("CreateRoomLayer::editBoxReturn");
	std::string szName = editBox->getName();
	int tag = editBox->getTag();
	std::string szText = editBox->getText();
	if (szName == "TextField_chahua")//插花
	{
		if (tag == 0)
		{
			int num = atoi(szText.c_str());
			if (num > 100)
			{
				editBox->setText("100");
			}
			else if (num < 1)
			{
				editBox->setText("1");
			}
		}
	}
}