#ifndef  _GAMERECORD_H_
#define	 _GAMERECORD_H_
#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNLobbyExport.h"
#include "HNNetExport.h"
#include "HNOpenExport.h"
#include "HNMarketExport.h"

USING_NS_CC;
using namespace cocostudio;
using namespace HN;

class GameRecord :public HNLayer
{
	public:
		GameRecord();
		~GameRecord();
		static GameRecord* createWithData(CHAR* Name[], int score[8][10],int count,int PlayerCount);
		virtual bool initWithData(CHAR* Name[], int score[8][10], int count, int PlayerCount);

		static GameRecord* createsssWithData(CHAR* Name[], int score[8][24], int count, int PlayerCount);
		virtual bool initsssWithData(CHAR* Name[], int score[8][24], int count, int PlayerCount);

		void setNameLong(Text* _label, string str);
};

#endif