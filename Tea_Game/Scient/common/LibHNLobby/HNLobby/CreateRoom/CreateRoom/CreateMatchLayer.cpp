#include "CreateMatchLayer.h"
#include "../../GameMatch/GameMatchRegistration.h"
#include "../../GameShop/GameStoreLayer.h"
#include "../../GameChildLayer/ServiceLayer.h"
using namespace HN;
static const char* MATCH_ENTRANCE_PATH = "platform/createRoomUI/createUi/Match_enter.csb";


MatchEntrance::MatchEntrance()
{

}

MatchEntrance::~MatchEntrance()
{

}

bool MatchEntrance::init()
{
	if (!Layer::init()) return false;
	auto winsize = Director::getInstance()->getWinSize();
	auto node = CSLoader::createNode(MATCH_ENTRANCE_PATH);
	node->setPosition(Vec2(winsize.width/2, winsize.height/2));
	addChild(node);

	_Imagebg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	_Imagebg->setSwallowTouches(true);
	_PanelMatchs = dynamic_cast<ui::ScrollView*>(_Imagebg->getChildByName("Panel_matchs"));
	auto BtnBack = dynamic_cast<Button*>(_Imagebg->getChildByName("Button_back"));
	auto Service = dynamic_cast<Button*>(_Imagebg->getChildByName("Button_service"));
	Service->addClickEventListener(CC_CALLBACK_1(MatchEntrance::onButtonClickCallBack, this));

	auto RoomCard = dynamic_cast<ImageView*>(_Imagebg->getChildByName("Image_roomcrad"));
	auto Gold = dynamic_cast<ImageView*>(_Imagebg->getChildByName("Image_gold"));
	auto RoomCardAdd = dynamic_cast<Button*>(RoomCard->getChildByName("Button_add"));
	auto GoldAdd = dynamic_cast<Button*>(Gold->getChildByName("Button_add"));
	_label_Jewels = dynamic_cast<Text*>(RoomCard->getChildByName("Text_card"));
	std::string str = HNToWan(PlatformLogic()->loginResult.iJewels);
	_label_Jewels->setString(str);

	_label_golds = dynamic_cast<Text*>(Gold->getChildByName("Text_gold"));
	std::string str1 = HNToWan(PlatformLogic()->loginResult.i64Money);
	_label_golds->setString(str1);
	auto Button_exchange = dynamic_cast<Button*>(_Imagebg->getChildByName("Button_exchange"));
	auto Button_bag = dynamic_cast<Button*>(_Imagebg->getChildByName("Button_bag"));
	Button_bag->setVisible(false);
	Button_exchange->setVisible(false);
	Button_exchange->addClickEventListener(CC_CALLBACK_1(MatchEntrance::onButtonClickCallBack, this));
	Button_bag->addClickEventListener(CC_CALLBACK_1(MatchEntrance::onButtonClickCallBack, this));
	BtnBack->addClickEventListener(CC_CALLBACK_1(MatchEntrance::onButtonClickCallBack, this));
	RoomCardAdd->addClickEventListener(CC_CALLBACK_1(MatchEntrance::onButtonClickCallBack, this));
	GoldAdd->addClickEventListener(CC_CALLBACK_1(MatchEntrance::onButtonClickCallBack, this));
	
	_ReturnCallBack = [=](){
		auto action = Spawn::create(MoveTo::create(0.3f, Vec2(-winsize.width / 2, winsize.height)), ScaleTo::create(0.3f, 0.1), nullptr);
		this->runAction(Sequence::create(action, RemoveSelf::create(), nullptr));
	};
	CreateMatchItems();

	return true;
}
bool MatchEntrance::getBagList(HNSocketMessage* socketMessage)
{
	UINT count = socketMessage->objectSize / sizeof(MSG_GP_O_BugList_Data);
	MSG_GP_O_BugList_Data * tmp = (MSG_GP_O_BugList_Data *)socketMessage->object;
	while (count-- > 0)
	{
		_BagData.push_back(tmp->danhao);
		_mapBag.insert(pair<const char*, int>(tmp->danhao, tmp->reward));
	}
	return true;
}

void MatchEntrance::walletChanged()
{
	updateMoney();
	updateDiamond();
}
void MatchEntrance::updateMoney()
{
	do
	{
		CC_BREAK_IF(nullptr == _label_golds);

		std::string str = HNToWan(PlatformLogic()->loginResult.i64Money);
		_label_golds->setString(str);

	} while (0);
}

void MatchEntrance::updateDiamond()
{
	do
	{
		CC_BREAK_IF(nullptr == _label_Jewels);

		std::string str = HNToWan(PlatformLogic()->loginResult.iJewels + PlatformLogic()->loginResult.iLockJewels);
		_label_Jewels->setString(str);

	} while (0);
}
void MatchEntrance::updateUserInfo()
{
	PlatformLogic()->sendData(MDM_GP_BANK, ASS_GP_BANK_FINANCE_INFO, nullptr, 0, [this](HNSocketMessage* socketMessage) {

		CHECK_SOCKET_DATA_RETURN(TMSG_GP_BankFinanceInfo, socketMessage->objectSize, true, "TMSG_GP_BankFinanceInfo size is error");
		TMSG_GP_BankFinanceInfo *financeInfo = (TMSG_GP_BankFinanceInfo*)socketMessage->object;

		if (financeInfo->iUserID == PlatformLogic()->loginResult.dwUserID)
		{
			PlatformLogic()->loginResult.i64Bank = financeInfo->i64BankMoney;
			PlatformLogic()->loginResult.i64Money = financeInfo->i64WalletMoney;
			PlatformLogic()->loginResult.iLotteries = financeInfo->iLotteries;
			PlatformLogic()->loginResult.iJewels = financeInfo->iJewels;

			walletChanged();
		}

		PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_FINANCE_INFO);

		return true;
	});
}

void MatchEntrance::onButtonClickCallBack(Ref* pSender)
{
	auto Btn = (Button*)pSender;
	string Name = Btn->getName();
	auto winsize = Director::getInstance()->getWinSize();
	if (Name.compare("Button_back") == 0)
	{
		if (_ReturnCallBack)_ReturnCallBack();
		if (_onCloseCallBack)_onCloseCallBack();
	}
	else if (Name.compare("Button_add") == 0)
	{
		auto storeLayer = GameStoreLayer::createGameStore(this);
		storeLayer->setChangeDelegate(this);
		storeLayer->onCloseCallBack = [this]() {

			updateUserInfo();
		};
		storeLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, 1000000+1, 0);
		storeLayer->onCloseCallBack = [this]()
		{
			updateUserInfo();
		};
	}
	else if (Name.compare("Button_service") == 0)
	{
		auto serviceLayer = ServiceLayer::create();
		serviceLayer->setName("serviceLayer");
		serviceLayer->open(ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, 1000000 + 1, 0);
	}
	else if (Name.compare("Button_bag") == 0)
	{
	}
	else if (Name.compare("Button_exchange") == 0)
	{

	}

}



void MatchEntrance::CreateMatchItems()
{
	_MatchItems = TableView::create(this, _PanelMatchs->getContentSize());
	_MatchItems->setDirection(TableView::Direction::HORIZONTAL);
	_MatchItems->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	_MatchItems->setDelegate(this);
	_PanelMatchs->addChild(_MatchItems);
	_PanelMatchs->setPosition(Vec2(-_PanelMatchs->getContentSize().width, 0));
	_PanelMatchs->setVisible(true);
	_PanelMatchs->runAction(Sequence::create(DelayTime::create(0.3f), MoveTo::create(0.3f, Vec2(0, 0)), nullptr));
}


TableViewCell* MatchEntrance::tableCellAtIndex(TableView* table, ssize_t index)
{
	TableViewCell* cell = table->dequeueCell();
	if (!cell)
	{
		cell = new TableViewCell();
	}
	else
	{
		cell->removeAllChildren();
	}

	string name = StringUtils::format("platform/createRoomUI/createUi/matchres/match_%d.png", index);
	bool isExist = FileUtils::getInstance()->isFileExist(name);
	if (isExist)
	{
		auto items = Sprite::create(name);//, name);
		items->setAnchorPoint(Vec2(0, 0));
		items->setPosition(Vec2(0, 30));
		items->setTag(index);
		//items->setSwallowTouches(false);
	/*	if (index == 1 || index == 0)
		{
			items->addTouchEventListener(CC_CALLBACK_2(MatchEntrance::onMatchItemsClickCallBack, this));
		}
		else
		{
			items->addClickEventListener([=](Ref*){
				GamePromptLayer::create()->showPrompt(GBKToUtf8("�����ڴ���"));
			});
		}*/
		auto point1 = items->getAnchorPoint();
		auto point2 = cell->getAnchorPoint();
		auto pos1 = cell->getPosition();
		auto pos2 = items->getPosition();
		cell->addChild(items);
	}
	
	return cell;
}


ssize_t MatchEntrance::numberOfCellsInTableView(TableView* table)
{
	return 5;
}

Size MatchEntrance::tableCellSizeForIndex(TableView* table, ssize_t index)
{
	return Size(362.5, 600);
}

void MatchEntrance::tableCellTouched(TableView* table, TableViewCell* cell)
{
	
	auto index = cell->getIdx();
     if (index == 1 || index == 0)
	{
		EnterMatchList(index);
		LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("���Ժ�..."), 25);
	}
	else
	{
		GamePromptLayer::create()->showPrompt(GBKToUtf8("��δ���ţ������ڴ���"));
	}
	


}
void MatchEntrance::EnterMatchList(int matchID)
{
	auto Tag = matchID;
	auto winsize = Director::getInstance()->getWinSize();
	auto match = GameMatchRegistration::create();
	match->_ReturniGameId = [=](){
		return _SelectediGameId();
	};
	match->setName("match");
	match->setPosition(Vec2(0, -winsize.height));
	this->addChild(match);
	switch (Tag)
	{
	case 0:
		match->_Selectedreward = match->Selectedreward::GOLD;
		break;
	case 1:
		match->_Selectedreward = match->Selectedreward::MONEY;
		break;
	case 2:
		match->_Selectedreward = match->Selectedreward::TELEPHONEBILL;
		break;
	}
	match->setChangeDelegate(this);

	_PanelMatchs->runAction(Sequence::create(MoveTo::create(0.3f, Vec2(0, -_PanelMatchs->getContentSize().height)), CallFunc::create([=](){
		match->runAction(Sequence::create(MoveTo::create(0.3f, Vec2(0, 0)), CallFunc::create([=](){
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			
			_ReturnCallBack = [=](){
				match->runAction(Sequence::create(MoveTo::create(0.3f, Vec2(0, 0)), RemoveSelf::create(), CallFunc::create([=](){
					
					_PanelMatchs->runAction(MoveTo::create(0.3f, Vec2(0, 0)));

					_ReturnCallBack = [=](){
						auto winsize = Director::getInstance()->getWinSize();
						auto action = Spawn::create(MoveTo::create(0.3f, Vec2(-winsize.width / 2, winsize.height)), ScaleTo::create(0.3f, 0.1), nullptr);
						this->runAction(Sequence::create(action, RemoveSelf::create(), nullptr));
					};

				}), nullptr));
			};
		}), nullptr));
	}), nullptr));
}
