#include "GameRecord.h"

static const char* RECORD_CSB = "platform/Record/Node_record.csb";
static const char* Item_CSB = "platform/Record/Node_item.csb";
static const char* ChildTtem_CSB = "platform/Record/Node_childItem.csb";

GameRecord::GameRecord()
{

}

GameRecord::~GameRecord()
{

}

GameRecord* GameRecord::createWithData(CHAR* Name[], int score[8][10], int count, int PlayerCount)
{
	GameRecord *pRet = new GameRecord();
	if (pRet && pRet->initWithData(Name, score,count,PlayerCount))
	{
		pRet->autorelease();
	}
	else
	{
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameRecord::initWithData(CHAR* Name[], int score[8][10], int count, int PlayerCount)
{
	if (!HNLayer::init())
	{
		return false;
	}
	Size winSize = Director::getInstance()->getWinSize();

	auto rootNode = CSLoader::createNode(RECORD_CSB);
	if (!rootNode)
	{
		return false;
	}
	rootNode->setPosition(winSize / 2);
	addChild(rootNode, 1000000);

	auto rootBg = (ImageView*)rootNode->getChildByName("Image_1");
	auto panel_bg_mask = dynamic_cast<Layout*>(rootNode->getChildByName("Panel_mask"));
	panel_bg_mask->addClickEventListener([=](Ref* ref) {
		rootNode->setVisible(false);
		rootNode->removeFromParentAndCleanup(true);

	});
	auto panel_mask = dynamic_cast<Layout*>(rootBg->getChildByName("Panel_top_mask"));
	panel_mask->setVisible(true);
	Sprite* pbiaoti = Sprite::create("platform/Record/record.png");
	pbiaoti->setPosition(Vec2(rootNode->getContentSize().width / 2, rootNode->getContentSize().height + 250));
	rootNode->addChild(pbiaoti,2);
	
	rootBg->loadTexture("platform/Record/Imagebg2.png");
	auto btn_close = (Button*)rootBg->getChildByName("Button_close");
	auto panel = (ListView*)rootNode->getChildByName("ListView_2");
	panel->setClippingEnabled(true);
	btn_close->addClickEventListener([=](Ref* ref) {
		rootNode->setVisible(false);
		rootNode->removeFromParentAndCleanup(true);

	});
	int igameCount = count;
	if (igameCount>10)
	{
		igameCount = 10;
	}
	if (count>0)
	{
		
		for (int i = igameCount-1; i >= 0; i--)
		{
			Widget* pContain = Widget::create();
			pContain->setContentSize(Size(700, 180));
			auto gameList = CSLoader::createNode(Item_CSB);
			auto listBg = (Layout*)gameList->getChildByName("Panel_item");
			auto Text_count = (Text*)listBg->getChildByName("Text_Count");
			auto gameCount = StringUtils::format(GBKToUtf8("第%d局"), (i+1));

			if (count>10)
			{
				int index = count - (9 - i);
				 gameCount = StringUtils::format(GBKToUtf8("第%d局"), index);
			}
			Text_count->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
			Text_count->setString(gameCount);
			pContain->addChild(gameList);
			gameList->setPositionY(gameList->getPositionY() + 90);

			for (int j = 0; j < PlayerCount;j++)
			{
				auto Record = CSLoader::createNode(ChildTtem_CSB);
				auto RecordBg = (ImageView*)Record->getChildByName("Image_item");
				//RecordBg->setOpacity(0);
				auto Text_name = (Text*)Record->getChildByName("Text_name");
				Text_name->setPositionY(Text_name->getPositionY() - 5);
				Text_name->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
				auto Text_score = (Text*)Record->getChildByName("Text_score");
				Text_score->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
				Text_score->setString(StringUtils::format("%d", score[j][i]));
				Text_score->setColor(Color3B::BLACK);
				//Text_name->setString(GBKToUtf8(Name[j]));
				setNameLong(Text_name, Name[j]);
				Text_name->setColor(Color3B::BLACK);
				if (PlayerCount>3)
				{
					RecordBg->setScaleX(0.6);
					Record->setPosition(gameList->getPosition().x + j * 176 * 0.7, gameList->getPosition().y - 186);
					Text_score->setPositionX(Text_score->getPositionX() - 33);
					Text_name->setPositionX(Text_name->getPositionX() - 33);
				}
				else
				{
					Record->setPosition(gameList->getPosition().x + j * 176, gameList->getPosition().y - 186);
				}
				
				gameList->addChild(Record);
			}
			panel->pushBackCustomItem(pContain);
		}
	}

	return true;
}

GameRecord* GameRecord::createsssWithData(CHAR* Name[], int score[8][24], int count, int PlayerCount)
{
	GameRecord *pRet = new GameRecord();
	if (pRet && pRet->initsssWithData(Name, score, count, PlayerCount))
	{
		pRet->autorelease();
	}
	else
	{
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameRecord::initsssWithData(CHAR* Name[], int score[8][24], int count, int PlayerCount)
{
	if (!HNLayer::init())
	{
		return false;
	}
	Size winSize = Director::getInstance()->getWinSize();

	auto rootNode = CSLoader::createNode(RECORD_CSB);
	if (!rootNode)
	{
		return false;
	}
	rootNode->setPosition(winSize / 2);
	addChild(rootNode, 1000000);

	Sprite* pbiaoti = Sprite::create("platform/Record/record.png");
	pbiaoti->setPosition(Vec2(rootNode->getContentSize().width / 2, rootNode->getContentSize().height + 250-12));
	rootNode->addChild(pbiaoti, 1);

	auto rootBg = (ImageView*)rootNode->getChildByName("Image_1");
	auto panel_bg_mask = dynamic_cast<Layout*>(rootNode->getChildByName("Panel_mask"));
	panel_bg_mask->addClickEventListener([=](Ref* ref) {
		rootNode->setVisible(false);
		rootNode->removeFromParentAndCleanup(true);

	});
	auto panel_mask = dynamic_cast<Layout*>(rootBg->getChildByName("Panel_top_mask"));
	panel_mask->setVisible(true);

	auto btn_close = (Button*)rootBg->getChildByName("Button_close");
	auto panel = (ListView*)rootNode->getChildByName("ListView_2");
	panel->setClippingEnabled(true);
	btn_close->addClickEventListener([=](Ref* ref) {
		rootNode->setVisible(false);
		rootNode->removeFromParentAndCleanup(true);

	});
	int igameCount = count;
	/*if (igameCount>10)
	{
		igameCount = 10;
	}*/
	if (count>0)
	{

		for (int i = igameCount - 1; i >= 0; i--)
		{
			Widget* pContain = Widget::create();
			pContain->setContentSize(Size(700, 180));
			auto gameList = CSLoader::createNode(Item_CSB);
			auto listBg = (Layout*)gameList->getChildByName("Panel_item");
			auto Text_count = (Text*)listBg->getChildByName("Text_Count");
			auto gameCount = StringUtils::format(GBKToUtf8("第%d局"), (i + 1));

			/*if (count>10)
			{
				int index = count - (9 - i);
				gameCount = StringUtils::format(GBKToUtf8("第%d局"), index);
			}*/
			Text_count->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
			Text_count->setString(gameCount);
			pContain->addChild(gameList);
			gameList->setPositionY(gameList->getPositionY() + 90);

			for (int j = 0; j < PlayerCount; j++)
			{
				auto Record = CSLoader::createNode(ChildTtem_CSB);
				auto RecordBg = (ImageView*)Record->getChildByName("Image_item");
				//RecordBg->setOpacity(0);
				auto Text_name = (Text*)Record->getChildByName("Text_name");
				Text_name->setPositionY(Text_name->getPositionY() - 5);
				Text_name->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
				auto Text_score = (Text*)Record->getChildByName("Text_score");
				Text_score->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
				Text_score->setString(StringUtils::format("%d", score[j][i]));
				Text_score->setColor(Color3B::BLACK);
				//Text_name->setString(GBKToUtf8(Name[j]));
				setNameLong(Text_name, Name[j]);
				Text_name->setColor(Color3B::BLACK);
				if (PlayerCount>3)
				{
					RecordBg->setScaleX(0.6);
					Record->setPosition(gameList->getPosition().x + j * 176 * 0.7, gameList->getPosition().y - 186);
					Text_score->setPositionX(Text_score->getPositionX() - 33);
					Text_name->setPositionX(Text_name->getPositionX() - 33);
				}
				else
				{
					Record->setPosition(gameList->getPosition().x + j * 176, gameList->getPosition().y - 186);
				}

				gameList->addChild(Record);
			}
			panel->pushBackCustomItem(pContain);
		}
	}

	return true;
}

void GameRecord::setNameLong(Text* _label, string str)
{
	string strCopy = str;
	if (strCopy.length() >6)
	{
		strCopy.erase(7, strCopy.length());
		strCopy.append("...");
		_label->setString(GBKToUtf8(strCopy));
	}
	else
	{
		_label->setString(GBKToUtf8(strCopy));
	}
}