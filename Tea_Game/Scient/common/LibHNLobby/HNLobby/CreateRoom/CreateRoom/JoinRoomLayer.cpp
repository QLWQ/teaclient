/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "JoinRoomLayer.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "../../GameReconnection/Reconnection.h"

static const char* JOINROOM_CSB = "platform/createRoomUI/joinUi/joinUi_Node.csb";

JoinRoomLayer::JoinRoomLayer()
{
}

JoinRoomLayer::~JoinRoomLayer()
{
	
}

void JoinRoomLayer::closeFunc()
{
	
}

bool JoinRoomLayer::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(JOINROOM_CSB);
	CCAssert(node != nullptr, "null");
	node->setPosition(_winSize.width / 2, _winSize.height/2);
	addChild(node);

	auto imgBG = dynamic_cast<ImageView*>(node->getChildByName("Image_BG"));
	if (_winSize.width / _winSize.height < 1.78f)
	{
		imgBG->setScale(0.9f);
	}

	// 关闭按钮
	auto btn_close = dynamic_cast<Button*>(imgBG->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*) {
		close();
	});

	auto showLayout = dynamic_cast<Layout*>(imgBG->getChildByName("Panel_show"));
	auto btnLayout = dynamic_cast<Layout*>(imgBG->getChildByName("Panel_button"));

	for (int i = 0; i < 10; i++)
	{
		auto btn_number = dynamic_cast<Button*>(btnLayout->getChildByName(StringUtils::format("Button_num%d", i)));
		btn_number->setTag(i);
		btn_number->addClickEventListener(CC_CALLBACK_1(JoinRoomLayer::onNumberBtnClickCallback, this));

		if (i >= 6) continue;
		auto text = dynamic_cast<ui::TextAtlas*>(showLayout->getChildByName(StringUtils::format("AtlasLabel_num%d", i)));
		text->setVisible(false);
		_numberVec.pushBack(text);
	}

	// 创建房间按钮
	auto btn_create = dynamic_cast<Button*>(btnLayout->getChildByName("Button_create"));
	btn_create->addClickEventListener(CC_CALLBACK_1(JoinRoomLayer::createRoomClickCallback, this));
	btn_create->setVisible(false);

	// 删除按钮
	auto btn_del = dynamic_cast<Button*>(btnLayout->getChildByName("Button_del"));
	btn_del->addClickEventListener(CC_CALLBACK_1(JoinRoomLayer::deleteBtnClickCallback, this));

	// 重置按钮
	auto btn_reset = dynamic_cast<Button*>(btnLayout->getChildByName("Button_reset"));
	btn_reset->addClickEventListener(CC_CALLBACK_1(JoinRoomLayer::resetBtnClickCallback, this));

	return true;
}

void JoinRoomLayer::onNumberBtnClickCallback(Ref* pSender)
{
	auto clickedBtn = (Button*)pSender;
	auto clickedNum = clickedBtn->getTag();

	if (_currentPassWord.length() < 6)
	{
		auto showNumber = _numberVec.at(_currentPassWord.length());
		if (showNumber)
		{
			showNumber->setString(StringUtils::format("%d", clickedNum));
			showNumber->setVisible(true);
		}
		
		_currentPassWord = _currentPassWord.append(StringUtils::format("%d", clickedNum));

		// 调用重连逻辑进入创建房间
		if (6 == _currentPassWord.length())	{	
			// 检测是否有正在进行的比赛
			//Reconnection::getInstance()->getCutRoomInfo(1);
			//Reconnection::getInstance()->onCloseCallBack = [this]() {
				Reconnection::getInstance()->doLoginVipRoom(_currentPassWord);
			//};
		}
	}	
}

void JoinRoomLayer::createRoomClickCallback(Ref* pSender)
{
	this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){

		if (onEnterGameCallBack) {
			onEnterGameCallBack();
		}
		removeFromParent();
	}), nullptr));
}

void JoinRoomLayer::deleteBtnClickCallback(Ref* pSender)
{
	if (_currentPassWord.empty()) return;

	int passwordLen = _currentPassWord.length();
	if (passwordLen <= 6)
	{
		_numberVec.at(passwordLen-1)->setVisible(false);
		
		if (1 == passwordLen) {
			_currentPassWord = "";
		}
		else {
			_currentPassWord = _currentPassWord.substr(0, _currentPassWord.length() - 1);
		}
	}
}

void JoinRoomLayer::resetBtnClickCallback(Ref* pSender)
{
	if (_currentPassWord.empty()) return;

	for (auto num : _numberVec)
	{
		num->setVisible(false);
	}

	_currentPassWord = "";
}