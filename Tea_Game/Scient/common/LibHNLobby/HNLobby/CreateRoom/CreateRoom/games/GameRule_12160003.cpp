/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRule_12160003.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

GameRule_12160003::GameRule_12160003()
	: HNGameRule()
{
}

GameRule_12160003* GameRule_12160003::create(Node* node)
{
	GameRule_12160003 *pRet = new GameRule_12160003();
	if (pRet && pRet->initData(node)) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameRule_12160003::initData(Node* node)
{
	if (!HNGameRule::init()) return false;
	_csNode = node;
	//玩家人数
	auto Panel_game = (Layout*)node->getChildByName("Panel_game");
	for (int i = 0; i < 4; i++)
	{
		 _ck_wanfa[i] = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_wanfa%d", i)));
		 if (_ck_wanfa[i])
		{
			 if (i == 0)
				 _ck_wanfa[i]->setTag(1);
			 else if (i == 1)
				 _ck_wanfa[i]->setTag(3);
			 else if (i == 2)
				 _ck_wanfa[i]->setTag(5);
			 else if (i == 3)
				 _ck_wanfa[i]->setTag(2);
			 _ck_wanfa[i]->setVisible(true);

			 _ck_wanfa[i]->addEventListener([=](Ref* ref, ui::CheckBox::EventType type) {

				if (type == CheckBox::EventType::UNSELECTED)
				{
					_ck_wanfa[i]->setSelectedState(true);
					return;
				}
				for (int j = 0; j < 4; j++)
				{
					auto ck_box = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_wanfa%d", j)));
					if (ck_box) ck_box->setSelected(ck_box == _ck_wanfa[i]);
				}

				// 选择的玩法
				_12160003GameRule.iWanFa = _ck_wanfa[i]->getTag();
			});
		}
	}
	return true;
}

void GameRule_12160003::getGameRule(char szDeskConfig[])
{
	szDeskConfig[0] = _12160003GameRule.iWanFa;
}

//选择局数
void GameRule_12160003::setJushuType()
{
	//游戏局数
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 0; i < 3; i++)
	{
		auto ck_count = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_count%d", i)));
		if (ck_count)
		{
			if (ck_count->isSelected())
			{
				_12160003GameRule.iJushuType = i;
				break;
			}			
		}
	}
}

void GameRule_12160003::setFangFeiType()
{
	//玩家人数
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 0; i < 3; i++)
	{
		auto ck_pay = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
		if (ck_pay)
		{
			if (ck_pay->isSelected())
			{
				_12160003GameRule.iFangfeiType = i;
				break;
			}
		}
	}
}

//房间配置
void GameRule_12160003::setRoomConfig(const MSG_GP_O_BuyDeskConfig config)
{
	_config = config;
}

//获取游戏规则，组装成json
std::string GameRule_12160003::getRuleJson()
{
	setFangFeiType();
	setJushuType();
	rapidjson::Document doc;
	doc.SetObject();
	rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();
	rapidjson::Value object(rapidjson::kObjectType);
	object.AddMember("iWanFa", _12160003GameRule.iWanFa, allocator);
	object.AddMember("iFangfeiType", _12160003GameRule.iFangfeiType, allocator);
	object.AddMember("iJushuType", _12160003GameRule.iJushuType, allocator);
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> write(buffer);
	object.Accept(write);
	return buffer.GetString();
}

//读取上次选择的数据，解析json
void GameRule_12160003::readRuleJson(std::string rule)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(rule.c_str());
	if (!doc.HasParseError() && doc.IsObject())
	{
		if (doc.HasMember("iWanFa") && doc["iWanFa"].IsInt())
		{
			_12160003GameRule.iWanFa = doc["iWanFa"].GetInt();
		}
		if (doc.HasMember("iFangfeiType") && doc["iFangfeiType"].IsInt())
		{
			_12160003GameRule.iFangfeiType = doc["iFangfeiType"].GetInt();
		}
		if (doc.HasMember("iJushuType") && doc["iJushuType"].IsInt())
		{
			_12160003GameRule.iJushuType = doc["iJushuType"].GetInt();
		}
	}
	else
	{
		//初始化，经典玩法
		_12160003GameRule.iWanFa = 1;
		_12160003GameRule.iFangfeiType = 0;
		_12160003GameRule.iJushuType = 0;
	}
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 3; i >= 0; i--)
	{
		//玩法
		_ck_wanfa[i]->setSelected(_ck_wanfa[i]->getTag() == _12160003GameRule.iWanFa);
		//付费方式
		auto ck_pay = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
		if (ck_pay)
		{
			ck_pay->setSelected(_12160003GameRule.iFangfeiType == i);
			if (ck_pay->isSelected())
			{
				for (int j = 0; j < 3; j++)
				{
					auto jewels = dynamic_cast<Text*>(Panel_game->getChildByName(StringUtils::format("Text_jewels%d", j)));
					if (jewels)
					{
						// 房主付费
						if (0 == ck_pay->getTag())
						{
							if (_config.iBuyCount[j] > 50)
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d胡息%d钻石"), _config.iBuyCount[j], _config.iJewels[j]));
							}
							else
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d局%d钻石"), _config.iBuyCount[j], _config.iJewels[j]));
							}
						}
						// AA制付费
						else
						{
							if (_config.iBuyCount[j] > 50)
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d胡息%d钻石/人"), _config.iBuyCount[j], _config.iAAJewels[j]));
							}
							else
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d局%d钻石/人"), _config.iBuyCount[j], _config.iAAJewels[j]));
							}
						}
					}
				}
			}
		}
		//局数选择
		auto ck_count = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_count%d", i)));
		if (ck_count)
		{
			if (!ck_count->isVisible() && _12160003GameRule.iJushuType >= i)
				_12160003GameRule.iJushuType = 0;
			ck_count->setSelected(_12160003GameRule.iJushuType == i);
		}
	}
}
