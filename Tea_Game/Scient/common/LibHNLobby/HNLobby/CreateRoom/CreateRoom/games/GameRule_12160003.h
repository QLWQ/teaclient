/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#pragma once

#include "HNUIExport.h"
#include "HNNetExport.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;

struct GAME_RULE_12160003
{
	INT			iJushuType;
	INT			iFangfeiType;
	INT			iWanFa;	//玩法
	GAME_RULE_12160003()
	{
		memset(this, 0, sizeof(GAME_RULE_12160003));
	}
};

class GameRule_12160003 : public HNGameRule
{
public:
	static GameRule_12160003* create(Node* node);

	bool initData(Node* node);

	GameRule_12160003();

	void getGameRule(char szDeskConfig[]);

	//获取游戏规则，组装成json
	virtual std::string getRuleJson();

	//读取上次选择的数据，解析json
	virtual void readRuleJson(std::string rule);
	
	//选择局数
	void setJushuType();

	//选择付费方式
	void setFangFeiType();

	virtual void setRoomConfig(const MSG_GP_O_BuyDeskConfig config);
private:
	GAME_RULE_12160003 _12160003GameRule;
	MSG_GP_O_BuyDeskConfig _config;
	//人数选项
	CheckBox* _ck_wanfa[4];
	Node* _csNode;
};
