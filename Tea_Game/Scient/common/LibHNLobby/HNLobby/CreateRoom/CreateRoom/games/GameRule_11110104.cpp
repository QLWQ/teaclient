/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRule_11110104.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"
BYTE RENSHU[3] = { 2, 3, 4 };

GameRule_11110104::GameRule_11110104()
	: HNGameRule()
{
}

GameRule_11110104* GameRule_11110104::create(Node* node)
{
	GameRule_11110104 *pRet = new GameRule_11110104();
	if (pRet && pRet->initData(node)) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameRule_11110104::initData(Node* node)
{
	if (!HNGameRule::init()) return false;
	_csNode = node;
	//玩家人数
	auto Panel_game = (Layout*)node->getChildByName("Panel_game");
	for (int i = 0; i < 3; i++)
	{
		_ck_renshu[i] = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_renzhu%d", i)));
		if (_ck_renshu[i])
		{
			_ck_renshu[i]->setVisible(true);

			_ck_renshu[i]->addEventListener([=](Ref* ref, ui::CheckBox::EventType type) {

				if (type == CheckBox::EventType::UNSELECTED)
				{
					_ck_renshu[i]->setSelectedState(true);
					return;
				}
				for (int j = 0; j < 3; j++)
				{
					auto ck_box = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_renzhu%d", j)));
					if (ck_box) ck_box->setSelected(ck_box == _ck_renshu[i]);
					// 选择的人数
					if (ck_box == _ck_renshu[i])
						selectIdx_renzhu = j;
				}

			});
		}
	}

	return true;
}

//房间配置
void GameRule_11110104::setRoomConfig(const MSG_GP_O_BuyDeskConfig config)
{
	_config = config;
	setShowJuShu();
}
void GameRule_11110104::setShowJuShu()
{
	//玩家人数
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 0; i < 3; i++)
	{
		auto ck_count = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_count%d", i)));
		if (ck_count)
		{
			ck_count->setVisible(_config.buyCounts[i].iBuyCount != 0);
			ck_count->addClickEventListener([=](Ref*){
				for (int j = 0; j < 3; j++)
				{
					auto ck_box = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_count%d", j)));
					if (ck_box) ck_box->setSelected(ck_box == ck_count);
					if (ck_box == ck_count)
						selectIdx_jushu = j;
				}
			});
		}
	}
}
void GameRule_11110104::setFangFeiTag()
{
	//玩家人数
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 0; i < 3; i++)
	{
		auto ck_pay = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
		if (ck_pay)
		{
			if (ck_pay->isSelected())
				selectIdx_fangfei = ck_pay->getTag();
		}
	}
}
//获取游戏规则，组装成json
std::string GameRule_11110104::getRuleJson()
{
	setFangFeiTag();
	rapidjson::Document doc;
	rapidjson::Value value(rapidjson::Type::kObjectType);

	value.AddMember("LUNZHUANG", selectIdx_jushu, doc.GetAllocator());
	value.AddMember("RENSHU", selectIdx_renzhu, doc.GetAllocator());
	value.AddMember("FANGFEI", selectIdx_fangfei, doc.GetAllocator());

	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	value.Accept(writer);

	return buffer.GetString();
}

//读取上次选择的数据，解析json
void GameRule_11110104::readRuleJson(std::string data)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(data.c_str());
	bool isExist = false;
	if (!doc.HasParseError())
	{
		if (doc.IsObject())
		{
			// 商品ID
			if (doc.HasMember("LUNZHUANG") && doc["LUNZHUANG"].IsInt()) {
				selectIdx_jushu = doc["LUNZHUANG"].GetInt();
			}
			if (doc.HasMember("RENSHU") && doc["RENSHU"].IsInt()) {
				selectIdx_renzhu = doc["RENSHU"].GetInt();
			}
			if (doc.HasMember("FANGFEI") && doc["FANGFEI"].IsInt()) {
				selectIdx_fangfei = doc["FANGFEI"].GetInt();
			}
			isExist = true;
		}
	}
	if (!isExist)
		return;
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 2; i >= 0; i--)
	{
		auto ck_count = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_count%d", i)));
		auto ck_renzhu = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_renzhu%d", i)));
		auto ck_pay = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
		if (ck_count)
		{
			if (!ck_count->isVisible() && selectIdx_jushu >= i)
				selectIdx_jushu = 0;
			ck_count->setSelected(selectIdx_jushu == i);
		}
		if (ck_renzhu)
		{
			if (!ck_renzhu->isVisible() && selectIdx_renzhu >= i)
				selectIdx_renzhu = 0;
			ck_renzhu->setSelected(selectIdx_renzhu == i);
		}
		if (ck_pay)
		{
			if (!ck_pay->isVisible() && selectIdx_fangfei >= i)
				selectIdx_fangfei = 0;
			ck_pay->setSelected(selectIdx_fangfei == i);
			if (ck_pay->isSelected())
			{
				for (int j = 0; j < 3; j++)
				{
					auto jewels = dynamic_cast<Text*>(Panel_game->getChildByName(StringUtils::format("Text_jewels%d", j)));
					if (jewels)
					{
						// 房主付费
						if (0 == ck_pay->getTag())
						{
							if (_config.buyCounts[j].iBuyCount > 50)
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d胡息%d钻石"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iJewels));
							}
							else
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d局%d钻石"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iJewels));
							}
						}
						// AA制付费
						else
						{
							if (_config.buyCounts[j].iBuyCount > 50)
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d胡息%d钻石/人"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iAAJewels));
							}
							else
							{
								jewels->setString(StringUtils::format(GBKToUtf8("%d局%d钻石/人"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iAAJewels));
							}
						}
					}
				}
			}
		}
	}

}
void GameRule_11110104::getGameRule(char szDeskConfig[])
{
	GAME_RULE_11110104 _11110104GameRule;
	// 选择的押注上限
	_11110104GameRule.iLnkBankerCnt = _config.buyCounts[selectIdx_jushu].iBuyCount;
	_11110104GameRule.iUserCount = RENSHU[selectIdx_renzhu];
	
	memcpy(szDeskConfig, &_11110104GameRule, sizeof(_11110104GameRule));
}