/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRule_12150006.h"
#include "json/rapidjson.h"
#include "json/document.h"

GameRule_12150006::GameRule_12150006()
{
	memset(_paixing, true, sizeof(_paixing));
	memset(_gaoji, 0, sizeof(_gaoji));
	memset(_iDiFenIdx, 0, sizeof(_iDiFenIdx));
	memset(_iCountIdx, 0, sizeof(_iCountIdx));
	memset(_iLocation, 0, sizeof(_iLocation));
	memset(_iFanBeiIdx, 0, sizeof(_iFanBeiIdx));
	memset(_iShangzhuang, 0, sizeof(_iShangzhuang));
	memset(_iBeishu, 0, sizeof(_iBeishu));
	memset(_iTuizhuIdx, 0, sizeof(_iTuizhuIdx));
	memset(_iPay, 0, sizeof(_iPay));
}

GameRule_12150006::~GameRule_12150006()
{
	
}

GameRule_12150006* GameRule_12150006::create(Node* node)
{
	GameRule_12150006 *pRet = new GameRule_12150006();
	if (pRet && pRet->initData(node)) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameRule_12150006::initData(Node* node)
{
	if (!HNGameRule::init()) return false;
	_layout = dynamic_cast<Layout*>(node->getChildByName(StringUtils::format("Panel_game")));

	//5种玩法
	std::string str("");
	for (int i = 0; i < 5; i++)
	{
		str = StringUtils::format("Button_wanfa_%d", i);
		Btn_Wanfa[i] = dynamic_cast<Button*>(_layout->getChildByName(str));
		Btn_Wanfa[i]->addClickEventListener(CC_CALLBACK_1(GameRule_12150006::wanfaBtnCallback, this));
		Btn_Wanfa[i]->setTag(i);
		Btn_Wanfa[i]->setEnabled(true);

		str = StringUtils::format("ScrollView_wanfa_%d", i);
		ScrollView_wanfa[i] = dynamic_cast<ui::ScrollView*>(_layout->getChildByName(str));
		ScrollView_wanfa[i]->setVisible(false);
	}
	for (int type = 0; type < 5; type++)
	{
		//底分
		auto layerDifen = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_difen"));
		if (layerDifen)
		{
			for (int i = 0; i < 4; i++)
			{
				_textDiFen[type][i] = dynamic_cast<Text*>(layerDifen->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxDiFen[type][i] = dynamic_cast<CheckBox*>(layerDifen->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxDiFen[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxDiFen[type][i]->setTag(i);
				_checkBoxDiFen[type][i]->setUserData((void*)"difenCheckBox");
			}
		}

		//局数
		auto layerJushu = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_jushu"));
		if (layerJushu)
		{
			for (int i = 0; i < 2; i++)
			{
				_textJuShu[type][i] = dynamic_cast<Text*>(layerJushu->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxJuShu[type][i] = dynamic_cast<CheckBox*>(layerJushu->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxJuShu[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxJuShu[type][i]->setTag(i);
				_checkBoxJuShu[type][i]->setUserData((void*)"jushuCheckBox");
			}
		}

		//定位
		auto layerDingWei = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_dingwei"));
		if (layerDingWei)
		{
			for (int i = 0; i < 2; i++)
			{
				_textDingWei[type][i] = dynamic_cast<Text*>(layerDingWei->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxDingWei[type][i] = dynamic_cast<CheckBox*>(layerDingWei->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxDingWei[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxDingWei[type][i]->setTag(i);
				_checkBoxDingWei[type][i]->setUserData((void*)"dingweiCheckBox");
			}
		}

		//房费
		auto layerFangFei = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_fangfei"));
		if (layerFangFei)
		{
			for (int i = 0; i < 2; i++)
			{
				_textFangFei[type][i] = dynamic_cast<Text*>(layerFangFei->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxFangFei[type][i] = dynamic_cast<CheckBox*>(layerFangFei->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxFangFei[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxFangFei[type][i]->setTag(i);
				_checkBoxFangFei[type][i]->setUserData((void*)"fangfeiCheckBox");
			}
		}

		//翻倍规则
		auto layerFanBei = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_fanbei"));
		if (layerFanBei)
		{
			_btnShowFanBei[type] = dynamic_cast<Button*>(layerFanBei->getChildByName("Button_show"));
			_btnShowFanBei[type]->addClickEventListener(CC_CALLBACK_1(GameRule_12150006::fanbeiBtnCallback, this));
			_btnHideFanBei[type] = dynamic_cast<Button*>(layerFanBei->getChildByName("Button_hide"));
			_btnHideFanBei[type]->addClickEventListener(CC_CALLBACK_1(GameRule_12150006::fanbeiBtnCallback, this));
			_textContentFanBei[type] = dynamic_cast<Text*>(layerFanBei->getChildByName("Text_content"));
			_imgFanBei[type] = dynamic_cast<ImageView*>(layerFanBei->getChildByName("Image_prompt"));
			for (int i = 0; i < 2; i++)
			{
				_textFanBei[type][i] = dynamic_cast<Text*>(_imgFanBei[type]->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxFanBei[type][i] = dynamic_cast<CheckBox*>(_imgFanBei[type]->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxFanBei[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxFanBei[type][i]->setTag(i);
				_checkBoxFanBei[type][i]->setUserData((void*)"fanbeiCheckBox");
			}
		}

		//特殊牌型
		auto layerPaiXing = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_paixing"));
		if (layerPaiXing)
		{
			_btnShowPaixing[type] = dynamic_cast<Button*>(layerPaiXing->getChildByName("Button_show"));
			_btnShowPaixing[type]->addClickEventListener(CC_CALLBACK_1(GameRule_12150006::paixingBtnCallback, this));
			_btnHidePaixing[type] = dynamic_cast<Button*>(layerPaiXing->getChildByName("Button_hide"));
			_btnHidePaixing[type]->addClickEventListener(CC_CALLBACK_1(GameRule_12150006::paixingBtnCallback, this));
			_textContentPaixing[type] = dynamic_cast<Text*>(layerPaiXing->getChildByName("Text_content"));
			_imgPaixing[type] = dynamic_cast<ImageView*>(layerPaiXing->getChildByName("Image_prompt"));
			for (int i = 0; i < 6; i++)
			{
				_textPaixing[type][i] = dynamic_cast<Text*>(_imgPaixing[type]->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxPaixing[type][i] = dynamic_cast<CheckBox*>(_imgPaixing[type]->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxPaixing[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxPaixing[type][i]->setTag(i);
				_checkBoxPaixing[type][i]->setUserData((void*)"paixingCheckBox");
			}
		}

		//上庄
		auto layerShangzhuang = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_shangzhaung"));
		if (layerShangzhuang)
		{
			for (int i = 0; i < 4; i++)
			{
				_textShangzhuang[type][i] = dynamic_cast<Text*>(layerShangzhuang->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxShangzhuang[type][i] = dynamic_cast<CheckBox*>(layerShangzhuang->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxShangzhuang[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxShangzhuang[type][i]->setTag(i);
				_checkBoxShangzhuang[type][i]->setUserData((void*)"shangzhuangCheckBox");
			}
		}

		//最大抢庄
		auto layerbeishu = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_beishu"));
		if (layerbeishu)
		{
			for (int i = 0; i < 4; i++)
			{
				_textBeishu[type][i] = dynamic_cast<Text*>(layerbeishu->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxBeishu[type][i] = dynamic_cast<CheckBox*>(layerbeishu->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxBeishu[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxBeishu[type][i]->setTag(i);
				_checkBoxBeishu[type][i]->setUserData((void*)"beishuCheckBox");
			}
		}

		//推注
		auto layerTuizhu = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_tuizhu"));
		if (layerTuizhu)
		{
			for (int i = 0; i < 4; i++)
			{
				_textTuizhu[type][i] = dynamic_cast<Text*>(layerTuizhu->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxTuizhu[type][i] = dynamic_cast<CheckBox*>(layerTuizhu->getChildByName(StringUtils::format("CheckBox_%d", i)));
				_checkBoxTuizhu[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
				_checkBoxTuizhu[type][i]->setTag(i);
				_checkBoxTuizhu[type][i]->setUserData((void*)"tuizhuCheckBox");
			}
			_btnTishiTuizhu[type] = dynamic_cast<Button*>(layerTuizhu->getChildByName("Button_tishi"));
			_btnTishiTuizhu[type]->addClickEventListener(CC_CALLBACK_1(GameRule_12150006::tishiBtnCallback, this));
			_imgTishiTuizhu[type] = dynamic_cast<ImageView*>(layerTuizhu->getChildByName("Image_tishi"));
		}

		//高级
		auto layerGaoji = dynamic_cast<Layout*>(ScrollView_wanfa[type]->getChildByName("Panel_gaoji"));
		if (layerGaoji)
		{
			for (int i = 0; i < 3; i++)
			{
				_textGaoji[type][i] = dynamic_cast<Text*>(layerGaoji->getChildByName(StringUtils::format("Text_%d", i)));
				_checkBoxGaoji[type][i] = dynamic_cast<CheckBox*>(layerGaoji->getChildByName(StringUtils::format("CheckBox_%d", i)));
				if (_checkBoxGaoji[type][i])
				{
					_checkBoxGaoji[type][i]->addEventListener(CC_CALLBACK_2(GameRule_12150006::checkBoxCallback, this));
					_checkBoxGaoji[type][i]->setTag(i);
					_checkBoxGaoji[type][i]->setUserData((void*)"gaojiCheckBox");
				}
			}
			_btnTishiGaoji[type] = dynamic_cast<Button*>(layerGaoji->getChildByName("Button_tishi"));
			_btnTishiGaoji[type]->addClickEventListener(CC_CALLBACK_1(GameRule_12150006::tishiBtnCallback, this));
			_imgTishiGaoji[type] = dynamic_cast<ImageView*>(layerGaoji->getChildByName("Image_tishi"));
		}
	}
	setShowWanFa(WANFA_TYPE::NNSZ);

	return true;
}

void GameRule_12150006::wanfaBtnCallback(Ref* pSender)
{
	Button* btn = (Button*)pSender;
	auto btnTag = btn->getTag();
	setShowWanFa((WANFA_TYPE)btnTag);
}

void GameRule_12150006::fanbeiBtnCallback(Ref* pSender)
{
	auto btn = (Button*)pSender;
	_imgFanBei[_wanfaType]->setVisible(btn == _btnShowFanBei[_wanfaType]);
	_btnShowFanBei[_wanfaType]->setVisible(!(btn == _btnShowFanBei[_wanfaType]));
	_btnHideFanBei[_wanfaType]->setVisible(!(btn == _btnHideFanBei[_wanfaType]));
}

void GameRule_12150006::paixingBtnCallback(Ref* pSender)
{
	auto btn = (Button*)pSender;

	_imgPaixing[_wanfaType]->setVisible(btn == _btnShowPaixing[_wanfaType]);
	_btnShowPaixing[_wanfaType]->setVisible(!(btn == _btnShowPaixing[_wanfaType]));
	_btnHidePaixing[_wanfaType]->setVisible(!(btn == _btnHidePaixing[_wanfaType]));
}

void GameRule_12150006::tishiBtnCallback(Ref* pSender)
{
	//创建一个遮挡层
	auto layerColor = LayerColor::create(Color4B(0, 0, 0, 0));
	layerColor->setPosition(_winSize / 2);
	_layout->addChild(layerColor, 10);
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = [=](Touch* touch, Event* event){
		_imgTishiTuizhu[_wanfaType]->setVisible(false);
		_imgTishiGaoji[_wanfaType]->setVisible(false);
		_eventDispatcher->removeEventListenersForTarget(layerColor);
		layerColor->removeFromParent();
		return true;
	};
	touchListener->setSwallowTouches(false);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, layerColor);

	auto btn = (Button*)pSender;
	if (btn == _btnTishiTuizhu[_wanfaType])
	{
		_imgTishiTuizhu[_wanfaType]->setVisible(true);
	}
	if (btn == _btnTishiGaoji[_wanfaType])
	{
		_imgTishiGaoji[_wanfaType]->setVisible(true);
	}
}

void GameRule_12150006::checkBoxCallback(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox = (CheckBox*)pSender;
	//auto checkBoxName = checkBox->getName();
    auto checkUserData = checkBox->getUserData();
	std::string str = StringUtils::format("%s", checkUserData);
	auto checkBoxTag = checkBox->getTag();
	if (type == CheckBox::EventType::SELECTED)
	{
		if (checkBox == checkUserData)
		{

		}
		if (str.compare("difenCheckBox") == 0)
		{
			for (int i = 0; i < 4; i++)
			{
				_checkBoxDiFen[_wanfaType][i]->setSelected((_checkBoxDiFen[_wanfaType][i] == checkBox));
				_textDiFen[_wanfaType][i]->setTextColor((_checkBoxDiFen[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iDiFenIdx[_wanfaType] = checkBoxTag;
		}
		if (str.compare("jushuCheckBox") == 0)
		{
			for (int i = 0; i < 2; i++)
			{
				_checkBoxJuShu[_wanfaType][i]->setSelected((_checkBoxJuShu[_wanfaType][i] == checkBox));
				_textJuShu[_wanfaType][i]->setTextColor((_checkBoxJuShu[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iCountIdx[_wanfaType] = checkBoxTag;
		}
		if (str.compare("dingweiCheckBox") == 0)
		{
			for (int i = 0; i < 2; i++)
			{
				_checkBoxDingWei[_wanfaType][i]->setSelected((_checkBoxDingWei[_wanfaType][i] == checkBox));
				_textDingWei[_wanfaType][i]->setTextColor((_checkBoxDingWei[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iLocation[_wanfaType] = checkBoxTag;
		}
		if (str.compare("fangfeiCheckBox") == 0)
		{
			for (int i = 0; i < 2; i++)
			{
				_checkBoxFangFei[_wanfaType][i]->setSelected((_checkBoxFangFei[_wanfaType][i] == checkBox));
				_textFangFei[_wanfaType][i]->setTextColor((_checkBoxFangFei[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iPay[_wanfaType] = checkBoxTag + 1;
			setShowJuShu();
		}
		if (str.compare("fanbeiCheckBox") == 0)
		{
			for (int i = 0; i < 2; i++)
			{
				_checkBoxFanBei[_wanfaType][i]->setSelected((_checkBoxFanBei[_wanfaType][i] == checkBox));
				_textFanBei[_wanfaType][i]->setTextColor((_checkBoxFanBei[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iFanBeiIdx[_wanfaType] = checkBoxTag;
			_textContentFanBei[_wanfaType]->setString(_textFanBei[_wanfaType][checkBoxTag]->getString());
		}
		if (str.compare("paixingCheckBox") == 0)
		{
			_textPaixing[_wanfaType][checkBoxTag]->setTextColor(Color4B::RED);
			_paixing[_wanfaType][checkBoxTag] = true;
			bool isAllPaixing = true;
			for (int i = 0; i < 6; i++)
			{
				if (!_paixing[_wanfaType][i])
				{
					isAllPaixing = false;
				}
			}
			if (isAllPaixing)
			{
				_textContentPaixing[_wanfaType]->setString(GBKToUtf8("全部勾选"));
			}
			else
			{
				_textContentPaixing[_wanfaType]->setString(GBKToUtf8("部分勾选"));
			}
		}
		if (str.compare("shangzhuangCheckBox") == 0)
		{
			for (int i = 0; i < 4; i++)
			{
				_checkBoxShangzhuang[_wanfaType][i]->setSelected((_checkBoxShangzhuang[_wanfaType][i] == checkBox));
				_textShangzhuang[_wanfaType][i]->setTextColor((_checkBoxShangzhuang[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iShangzhuang[_wanfaType] = checkBoxTag;
		}
		if (str.compare("beishuCheckBox") == 0)
		{
			for (int i = 0; i < 4; i++)
			{
				_checkBoxBeishu[_wanfaType][i]->setSelected((_checkBoxBeishu[_wanfaType][i] == checkBox));
				_textBeishu[_wanfaType][i]->setTextColor((_checkBoxBeishu[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iBeishu[_wanfaType] = checkBoxTag;
		}
		if (str.compare("tuizhuCheckBox") == 0)
		{
			for (int i = 0; i < 4; i++)
			{
				_checkBoxTuizhu[_wanfaType][i]->setSelected((_checkBoxTuizhu[_wanfaType][i] == checkBox));
				_textTuizhu[_wanfaType][i]->setTextColor((_checkBoxTuizhu[_wanfaType][i] == checkBox) ? Color4B::RED : Color4B(66, 94, 170, 255));
			}
			_iTuizhuIdx[_wanfaType] = checkBoxTag;
		}
		if (str.compare("gaojiCheckBox") == 0)
		{
			_textGaoji[_wanfaType][checkBoxTag]->setTextColor(Color4B::RED);
			_gaoji[_wanfaType][checkBoxTag] = true;
		}
	}
	else
	{
		if (str.compare("paixingCheckBox") == 0)
		{
			_textPaixing[_wanfaType][checkBoxTag]->setTextColor(Color4B(66, 94, 170, 255));
			_paixing[_wanfaType][checkBoxTag] = false;
			bool isHavePaixing = false;
			for (int i = 0; i < 6; i++)
			{
				if (_paixing[_wanfaType][i])
				{
					isHavePaixing = true;
				}
			}
			if (isHavePaixing)
			{
				_textContentPaixing[_wanfaType]->setString(GBKToUtf8("部分勾选"));
			}
			else
			{
				_textContentPaixing[_wanfaType]->setString(GBKToUtf8("未勾选"));
			}
		}
		else if (str.compare("gaojiCheckBox") == 0)
		{
			_textGaoji[_wanfaType][checkBoxTag]->setTextColor(Color4B(66, 94, 170, 255));
			_gaoji[_wanfaType][checkBoxTag] = false;
		}
		else
		{
			checkBox->setSelected(true);
		}
	}
}

void GameRule_12150006::setShowWanFa(WANFA_TYPE type)
{
	_wanfaType = type;
	for (int i = 0; i < 5; i++)
	{
		Btn_Wanfa[i]->setEnabled(!(i == type));
		ScrollView_wanfa[i]->setVisible(i == type);
	}
	setShowJuShu();
}

void GameRule_12150006::setShowJuShu()
{
	std::string str("");
	for (int i = 0; i < 2; i++)
	{
		str = StringUtils::format("%d%s%d%s%s", _config.buyCounts[i].iBuyCount, GBKToUtf8("局"),
			(_iPay[_wanfaType] == 2) ? _config.buyCounts[i].iAAJewels : _config.buyCounts[i].iJewels, GBKToUtf8("钻石"), (_iPay[_wanfaType] == 2) ? GBKToUtf8("/人") : "");
		_textJuShu[_wanfaType][i]->setString(str);
	}
}

//房间配置
void GameRule_12150006::setRoomConfig(const MSG_GP_O_BuyDeskConfig config)
{
	_config = config;
	setShowJuShu();
}

//局数
int GameRule_12150006::getSelectCount()
{
	return _config.buyCounts[_iCountIdx[_wanfaType]].iBuyCount;
}

int GameRule_12150006::getSelectLocation()
{
	return _iLocation[_wanfaType];
}

int GameRule_12150006::getSelectPay()
{
	return _iPay[_wanfaType];
}

BYTE GameRule_12150006::getMidEnter()
{
	if (_checkBoxGaoji[_wanfaType][0]->isSelected())
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

void* GameRule_12150006::getGameRule()
{
	int rule = 0;
	//玩法
	if (_wanfaType == 0)
	{
		rule |= 0x00000002;
	}
	else if (_wanfaType == 1)
	{
		rule |= 0x00000004;
	}
	else if (_wanfaType == 2)
	{
		rule |= 0x00000001;
	}
	else if (_wanfaType == 3)
	{
		rule |= 0x00000008;
	}
	else if (_wanfaType == 4)
	{
		rule |= 0x00000010;
	}
	else
	{ 
	}

	//底分
	if (_iDiFenIdx[_wanfaType] == 0)
	{
		rule |= 0x00000020;
	}
	else if (_iDiFenIdx[_wanfaType] == 1)
	{
		rule |= 0x00000040;
	}
	else if (_iDiFenIdx[_wanfaType] == 2)
	{
		rule |= 0x00000080;
	}
	else if (_iDiFenIdx[_wanfaType] == 3)
	{
		rule |= 0x00000100;
	}

	//翻倍规则
	if (_iFanBeiIdx[_wanfaType] == 1)
	{
		rule |= 0x08000000;
	}
	//牌型
	if (_paixing[_wanfaType][0])
	{
		rule |= 0x00000200;
	}
	if (_paixing[_wanfaType][1])
	{
		rule |= 0x00000400;
	}
	if (_paixing[_wanfaType][2])
	{
		rule |= 0x00000800;
	}
	if (_paixing[_wanfaType][3])
	{
		rule |= 0x00001000;
	}
	if (_paixing[_wanfaType][4])
	{
		rule |= 0x00002000;
	}
	if (_paixing[_wanfaType][5])
	{
		rule |= 0x02000000;
	}
	//上庄
	if (_iShangzhuang[_wanfaType] == 0)
	{
		rule |= 0x10000000;
	}
	else if (_iShangzhuang[_wanfaType] == 1)
	{
		rule |= 0x20000000;
	}
	else if (_iShangzhuang[_wanfaType] == 2)
	{
		rule |= 0x40000000;
	}
	else if (_iShangzhuang[_wanfaType] == 3)
	{
		rule |= 0x80000000;
	}
	//倍数
	if (_iBeishu[_wanfaType] == 0)
	{
		rule |= 0x00004000;
	}
	if (_iBeishu[_wanfaType] == 1)
	{
		rule |= 0x00008000;
	}
	if (_iBeishu[_wanfaType] == 2)
	{
		rule |= 0x00010000;
	}
	if (_iBeishu[_wanfaType] == 3)
	{
		rule |= 0x00020000;
	}
	//推注
	if (_iTuizhuIdx[_wanfaType] == 0)
	{
		rule |= 0x00040000;
	}
	if (_iTuizhuIdx[_wanfaType] == 1)
	{
		rule |= 0x00080000;
	}
	if (_iTuizhuIdx[_wanfaType] == 2)
	{
		rule |= 0x00100000;
	}
	if (_iTuizhuIdx[_wanfaType] == 3)
	{
		rule |= 0x00200000;
	}
	//高级
	if (_gaoji[_wanfaType][0])
	{
		rule |= 0x00400000;
	}
	if (_gaoji[_wanfaType][1])
	{
		rule |= 0x00800000;
	}
	if (_gaoji[_wanfaType][2])
	{
		rule |= 0x01000000;
	}

	setGameRule(&rule, sizeof(int));

	return HNGameRule::getGameRule();
}
