/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRule_13000306.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

GameRule_13000306::GameRule_13000306()
	: HNGameRule()
{
}

GameRule_13000306* GameRule_13000306::create(Node* node)
{
	GameRule_13000306 *pRet = new GameRule_13000306();
	if (pRet && pRet->initData(node)) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameRule_13000306::initData(Node* node)
{
	if (!HNGameRule::init()) return false;
	_csNode = node;
	gameRule = new MSG_GR_CaiShenThirteenCard;

	memset(gameRule, 0, sizeof(gameRule));
	gameRule->iPeopleNum = 2;
	gameRule->bShuangHua = true;
	gameRule->bSiHua = true;
	gameRule->iPlayMode = 1;
	gameRule->bTongSha = false;
	gameRule->bJiaLiangMen = true;

	auto Panel_game = (Node*)node->getChildByName("Panel_game");

	//财神
	_checkCaiShen0 = (CheckBox*)Panel_game->getChildByName("CheckBox_caishen0");
	_checkCaiShen0->setVisible(true);
	if (_checkCaiShen0->isSelected())
	{
		gameRule->bShuangHua = true;
	}
	else
	{
		gameRule->bShuangHua = false;
	}
	auto Text_caishen0 = (Text*)Panel_game->getChildByName("Text_caishen0");
	Text_caishen0->setVisible(true);
	auto Text_caishen1 = (Text*)Panel_game->getChildByName("Text_caishen1");
	Text_caishen1->setVisible(true);
	_checkCaiShen1 = (CheckBox*)Panel_game->getChildByName("CheckBox_caishen1");
	_checkCaiShen1->setVisible(true);
	if (_checkCaiShen1->isSelected())
	{
		gameRule->bSiHua = true;
	}
	else
	{
		gameRule->bSiHua = false;
	}
	_checkCaiShen0->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
			gameRule->bShuangHua = true;
			if (type == CheckBox::EventType::UNSELECTED)
			{
				gameRule->bShuangHua = false;
			}
	});

	_checkCaiShen1->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
			gameRule->bSiHua = true;
			if (type == CheckBox::EventType::UNSELECTED)
			{
				gameRule->bSiHua = false;
			}
	});
	//人数
	_checkRenShu0 = (CheckBox*)Panel_game->getChildByName("CheckBox_renshu0");
	if (_checkRenShu0->isSelected())
	{
		gameRule->iPeopleNum = 2;
	}
	_checkRenShu1 = (CheckBox*)Panel_game->getChildByName("CheckBox_renshu1");
	if (_checkRenShu1->isSelected())
	{
		gameRule->iPeopleNum = 3;
	}
	_checkRenShu2 = (CheckBox*)Panel_game->getChildByName("CheckBox_renshu2");
	if (_checkRenShu2->isSelected())
	{
		gameRule->iPeopleNum = 4;
	}

	_checkRenShu3 = (CheckBox*)Panel_game->getChildByName("CheckBox_renshu3");
	if (_checkRenShu3->isSelected())
	{
		gameRule->iPeopleNum = 5;
	}

	_checkRenShu4 = (CheckBox*)Panel_game->getChildByName("CheckBox_renshu4");
	if (_checkRenShu4->isSelected())
	{
		gameRule->iPeopleNum = 6;
	}

	_txtRenShu3 = (Text*)Panel_game->getChildByName("Text_renshu3");
	_txtRenShu4 = (Text*)Panel_game->getChildByName("Text_renshu4");

	_checkRenShu0->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		_checkRenShu0->setSelected(true);
		_checkRenShu1->setSelected(false);
		_checkRenShu2->setSelected(false);
		_checkRenShu3->setSelected(false);
		_checkRenShu4->setSelected(false);
		gameRule->iPeopleNum = 2;
	});
	_checkRenShu1->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		_checkRenShu0->setSelected(false);
		_checkRenShu1->setSelected(true);
		_checkRenShu2->setSelected(false);
		_checkRenShu3->setSelected(false);
		_checkRenShu4->setSelected(false);
		gameRule->iPeopleNum = 3;
	});
	_checkRenShu2->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		_checkRenShu0->setSelected(false);
		_checkRenShu1->setSelected(false);
		_checkRenShu2->setSelected(true);
		_checkRenShu3->setSelected(false);
		_checkRenShu4->setSelected(false);
		gameRule->iPeopleNum = 4;
	});

	_checkRenShu3->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		_checkRenShu0->setSelected(false);
		_checkRenShu1->setSelected(false);
		_checkRenShu2->setSelected(false);
		_checkRenShu3->setSelected(true);
		_checkRenShu4->setSelected(false);
		gameRule->iPeopleNum = 5;
	});

	_checkRenShu4->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		_checkRenShu0->setSelected(false);
		_checkRenShu1->setSelected(false);
		_checkRenShu2->setSelected(false);
		_checkRenShu3->setSelected(false);
		_checkRenShu4->setSelected(true);
		gameRule->iPeopleNum = 6;
	});
	//玩法
	_checkWanFa0 = (CheckBox*)Panel_game->getChildByName("CheckBox_wanfa0");
	if (_checkWanFa0->isSelected())
	{
		gameRule->iPlayMode = 1; //温州
	}
	_checkWanFa1 = (CheckBox*)Panel_game->getChildByName("CheckBox_wanfa1");
	if (_checkWanFa1->isSelected())
	{
		gameRule->iPlayMode = 2; //福建
	}
	_checkWanFa0->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		_checkWanFa0->setSelected(true);
		_checkWanFa1->setSelected(false);
		gameRule->iPlayMode = 1; //温州
	});
	_checkWanFa1->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		_checkWanFa0->setSelected(false);
		_checkWanFa1->setSelected(true);
		gameRule->iPlayMode = 2; //福建
	});

	//全垒打和加两门
	_teShu0 = (CheckBox*)Panel_game->getChildByName("CheckBox_teshu0");
	if (_teShu0->isSelected())
	{
		gameRule->bTongSha = true;
	}
	else
	{
		gameRule->bTongSha = false;
	}
	_teShu0->addEventListener([=](Ref* ref,ui::CheckBox::EventType type){
		if (type == ui::CheckBox::EventType::UNSELECTED)
		{
			gameRule->bTongSha = false;
		}
		else
		{
			gameRule->bTongSha = true;
		}
	});
	_teShu1 = (CheckBox*)Panel_game->getChildByName("CheckBox_teshu1");
	if (_teShu1->isSelected())
	{
		gameRule->bJiaLiangMen = true;
	}
	else
	{
		gameRule->bJiaLiangMen = false;
	}
	_teShu1->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
		if (type == ui::CheckBox::EventType::UNSELECTED)
		{
			_checkRenShu0->setSelected(true);
			_checkRenShu1->setSelected(false);
			_checkRenShu2->setSelected(false);
			_checkRenShu3->setSelected(false);
			_checkRenShu4->setSelected(false);

			showMovePeople(false);

			gameRule->iPeopleNum = 2;
			gameRule->bJiaLiangMen = false;
		}
		else
		{
			showMovePeople(true);

			gameRule->bJiaLiangMen = true;
		}
	});
	return true;
}

void GameRule_13000306::getGameRule(char szDeskConfig[])
{
	szDeskConfig[0] = gameRule->bShuangHua;
	szDeskConfig[1] = gameRule->bSiHua;
	szDeskConfig[2] = gameRule->iPeopleNum;
	szDeskConfig[3] = gameRule->iPlayMode;
	szDeskConfig[4] = gameRule->bTongSha;
	szDeskConfig[5] = gameRule->bJiaLiangMen;
}

//选择局数
void GameRule_13000306::setJushuType()
{
	//游戏局数
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 0; i < 3; i++)
	{
		auto ck_count = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_count%d", i)));
		if (ck_count)
		{
			if (ck_count->isSelected())
			{
				gameRule->iJushuType = i;
				break;
			}
		}
	}
}

void GameRule_13000306::setFangFeiType()
{
	//玩家人数
	auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
	for (int i = 0; i < 3; i++)
	{
		auto ck_pay = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
		if (ck_pay)
		{
			if (ck_pay->isSelected())
			{
				gameRule->iFangfeiType = i;
				break;
			}
		}
	}
}

//房间配置
void GameRule_13000306::setRoomConfig(const MSG_GP_O_BuyDeskConfig config)
{
	_config = config;
}

//获取游戏规则，组装成json
std::string GameRule_13000306::getRuleJson()
{
	setFangFeiType();
	setJushuType();
	rapidjson::Document doc;
	doc.SetObject();
	rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();
	rapidjson::Value object(rapidjson::kObjectType);
	object.AddMember("bShuangHua", gameRule->bShuangHua, allocator);
	object.AddMember("bSiHua", gameRule->bSiHua, allocator);
	object.AddMember("bTongSha", gameRule->bTongSha, allocator);
	object.AddMember("bJiaLiangMen", gameRule->bJiaLiangMen, allocator);
	object.AddMember("iPeopleNum", gameRule->iPeopleNum, allocator);
	object.AddMember("iPlayMode", gameRule->iPlayMode, allocator);
	object.AddMember("iFangfeiType", gameRule->iFangfeiType, allocator);
	object.AddMember("iJushuType", gameRule->iJushuType, allocator);

	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> write(buffer);
	object.Accept(write);
	return buffer.GetString();
}

//读取上次选择的数据，解析json
void GameRule_13000306::readRuleJson(std::string rule)
{
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(rule.c_str());
	if (!doc.HasParseError() && doc.IsObject())
	{
		if (doc.HasMember("bShuangHua") && doc["bShuangHua"].IsBool())
		{
			gameRule->bShuangHua = doc["bShuangHua"].GetBool();
		}
		if (doc.HasMember("bSiHua") && doc["bSiHua"].IsBool())
		{
			gameRule->bSiHua = doc["bSiHua"].GetBool();
		}
		if (doc.HasMember("bTongSha") && doc["bTongSha"].IsBool())
		{
			gameRule->bTongSha = doc["bTongSha"].GetBool();
		}
		if (doc.HasMember("bJiaLiangMen") && doc["bJiaLiangMen"].IsBool())
		{
			gameRule->bJiaLiangMen = doc["bJiaLiangMen"].GetBool();
		}
		if (doc.HasMember("iPeopleNum") && doc["iPeopleNum"].IsInt())
		{
			gameRule->iPeopleNum = doc["iPeopleNum"].GetInt();
		}
		if (doc.HasMember("iPlayMode") && doc["iPlayMode"].IsInt())
		{
			gameRule->iPlayMode = doc["iPlayMode"].GetInt();
		}
		if (doc.HasMember("iFangfeiType") && doc["iFangfeiType"].IsInt())
		{
			gameRule->iFangfeiType = doc["iFangfeiType"].GetInt();
		}
		if (doc.HasMember("iJushuType") && doc["iJushuType"].IsInt())
		{
			gameRule->iJushuType = doc["iJushuType"].GetInt();
		}
		_checkCaiShen0->setSelected(gameRule->bShuangHua);
		_checkCaiShen1->setSelected(gameRule->bSiHua);
		_checkRenShu0->setSelected(gameRule->iPeopleNum == 2);
		_checkRenShu1->setSelected(gameRule->iPeopleNum == 3);
		_checkRenShu2->setSelected(gameRule->iPeopleNum == 4);
		_checkRenShu3->setSelected(gameRule->iPeopleNum == 5);
		_checkRenShu4->setSelected(gameRule->iPeopleNum == 6);
		_checkWanFa0->setSelected(gameRule->iPlayMode == 1);
		_checkWanFa1->setSelected(gameRule->iPlayMode == 2);
		_teShu0->setSelected(gameRule->bTongSha);
		_teShu1->setSelected(gameRule->bJiaLiangMen);
		showMovePeople(_teShu1->isSelected());
		//付费方式
		auto Panel_game = (Layout*)_csNode->getChildByName("Panel_game");
		for (int i = 2; i >= 0; i--)
		{
			//付费方式
			auto ck_pay = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_pay%d", i)));
			if (ck_pay)
			{
				ck_pay->setSelected(gameRule->iFangfeiType == i);
				log("##################gameRule->iFangfeiType = %d ", gameRule->iFangfeiType);
				if (ck_pay->isSelected())
				{
					for (int j = 0; j < 3; j++)
					{
						auto jewels = dynamic_cast<Text*>(Panel_game->getChildByName(StringUtils::format("Text_jewels%d", j)));
						if (jewels)
						{
							// 房主付费
							if (0 == ck_pay->getTag())
							{
								if (_config.buyCounts[j].iBuyCount > 50)
								{
									jewels->setString(StringUtils::format(GBKToUtf8("%d胡息%d钻石"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iJewels));
								}
								else
								{
									jewels->setString(StringUtils::format(GBKToUtf8("%d局%d钻石"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iJewels));
								}
							}
							// AA制付费
							else
							{
								if (_config.buyCounts[j].iBuyCount > 50)
								{
									jewels->setString(StringUtils::format(GBKToUtf8("%d胡息%d钻石/人"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iAAJewels));
								}
								else
								{
									jewels->setString(StringUtils::format(GBKToUtf8("%d局%d钻石/人"), _config.buyCounts[j].iBuyCount, _config.buyCounts[j].iAAJewels));
								}
							}
						}
					}
				}
			}
			//局数选择
			auto ck_count = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_count%d", i)));
			if (ck_count)
			{
				if (!ck_count->isVisible() && gameRule->iJushuType >= i)
					gameRule->iJushuType = 0;
				ck_count->setSelected(gameRule->iJushuType == i);
			}
		}
	}
}

void GameRule_13000306::showMovePeople(bool show)
{
	_txtRenShu3->setVisible(show);
	_txtRenShu4->setVisible(show);
	_checkRenShu3->setVisible(show);
	_checkRenShu4->setVisible(show);
}