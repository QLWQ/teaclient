/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#pragma once

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"
#include "network/HttpClient.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;


class GameRule_12150006 : public HNGameRule
{
	enum WANFA_TYPE
	{
		NNSZ = 0, //牛牛上庄
		GDZJ = 1, //固定庄家
		ZYQZ = 2, //自由抢庄
		MPQZ = 3, //明牌抢庄setRoomConfig
		TBNN = 4, //通比牛牛
	};

public:
	typedef std::function<void()> EnterGameCallBack;
	EnterGameCallBack onEnterGameCallBack = nullptr;
public:
	bool initData(Node* node);

	GameRule_12150006();

	virtual ~GameRule_12150006();

	static GameRule_12150006* create(Node* node);

	//局数
	virtual int getSelectCount() override;

	//是否定位（0:不定位，1:定位）
	virtual int getSelectLocation() override;

	//支付类型（0:房主，1:AA）
	virtual int getSelectPay() override;

	//中途加入
	virtual BYTE getMidEnter() override;
	//扩展规则
	virtual void* getGameRule() override;

	//房间配置
	virtual void setRoomConfig(const MSG_GP_O_BuyDeskConfig config) override;
private:
	void wanfaBtnCallback(Ref* pSender); //玩法按钮回调

	void fanbeiBtnCallback(Ref* pSender); //翻倍规则显示按钮回调

	void paixingBtnCallback(Ref* pSender); //牌型显示按钮回调

	void tishiBtnCallback(Ref* pSender); //提示按钮回调

	void checkBoxCallback(Ref* pSender, CheckBox::EventType type);//牛牛上庄复选框点击的回调
	 
	void setShowWanFa(WANFA_TYPE type); //设置显示的玩法页面

	void setShowJuShu(); //设置局数显示
private:
	Layout* _layout = nullptr;
	Button* Btn_Wanfa[5]; //五种玩法
	ui::ScrollView* ScrollView_wanfa[5];//五种玩法的界面

	//底分项
	Text* _textDiFen[5][4];
	CheckBox* _checkBoxDiFen[5][4];

	//局数项
	Text* _textJuShu[5][2];
	CheckBox* _checkBoxJuShu[5][2];

	//定位项
	Text* _textDingWei[5][2];
	CheckBox* _checkBoxDingWei[5][2];

	//房费项
	Text* _textFangFei[5][2];
	CheckBox* _checkBoxFangFei[5][2];

	//翻倍规则项
	Button* _btnShowFanBei[5];
	Button* _btnHideFanBei[5];
	Text* _textContentFanBei[5];
	ImageView* _imgFanBei[5];
	Text* _textFanBei[5][2];
	CheckBox* _checkBoxFanBei[5][2];

	//牌型则项
	Button* _btnShowPaixing[5];
	Button* _btnHidePaixing[5];
	Text* _textContentPaixing[5];
	ImageView* _imgPaixing[5];
	Text* _textPaixing[5][6];
	CheckBox* _checkBoxPaixing[5][6];

	//上庄项
	Text* _textShangzhuang[5][4];
	CheckBox* _checkBoxShangzhuang[5][4];

	//最大抢庄项
	Text* _textBeishu[5][4];
	CheckBox* _checkBoxBeishu[5][4];

	//推注项
	Text* _textTuizhu[5][4];
	CheckBox* _checkBoxTuizhu[5][4];
	Button* _btnTishiTuizhu[5];
	ImageView* _imgTishiTuizhu[5];

	//高级
	Text* _textGaoji[5][3];
	CheckBox* _checkBoxGaoji[5][3];
	Button* _btnTishiGaoji[5];
	ImageView* _imgTishiGaoji[5];

	MSG_GP_O_BuyDeskConfig _config;
	WANFA_TYPE      _wanfaType = WANFA_TYPE::NNSZ;     //玩法类型
	int				_iPay[5];   	   //支付方式（0:房主，1:AA）
	int             _iDiFenIdx[5];    //底分下标
	int				_iCountIdx[5];	   //创建局数下标
	int				_iLocation[5];	   //是否定位（0:不定位，1:定位）
	int             _iFanBeiIdx[5];   //翻倍选项
	bool            _paixing[5][6];       //记录选择的牌型
	int				_iShangzhuang[5]; //上庄下标
	int				_iBeishu[5];      //倍数下标
	int             _iTuizhuIdx[5];   //推注选项
	bool            _gaoji[5][3];         //记录高级选择的牌型
};