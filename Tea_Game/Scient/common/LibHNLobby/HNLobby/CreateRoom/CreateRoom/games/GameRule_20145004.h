/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#pragma once

#include "HNUIExport.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;


class GameRule_20145004 : public HNGameRule
{
public:
	static GameRule_20145004* create(Node* node);

	bool initData(Node* node);

	GameRule_20145004();
	virtual ~GameRule_20145004();
	//扩展规则
	virtual void getGameRule(char szDeskConfig[])override;
	//房间配置
	virtual void setRoomConfig(const MSG_GP_O_BuyDeskConfig config) override;
	void setShowJuShu();
private:
	Text* _textJuShu[2];
	int   _iPay = 0;
	CheckBox* _checkFangFei[2];
	CheckBox* _checkGameMode[2];
	CheckBox* _checkGameRule[5];
	MSG_GP_O_BuyDeskConfig _config;
};
