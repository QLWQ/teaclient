/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#pragma once

#include "HNUIExport.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;


class GameRule_12100704 : public HNGameRule
{
public:
	static GameRule_12100704* create(Node* node);

	bool initData(Node* node);

	GameRule_12100704();
};
