/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#pragma once

#include "HNUIExport.h"
#include "HNNetExport.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;


class GameRule_13000306 : public HNGameRule
{
public:
	static GameRule_13000306* create(Node* node);

	bool initData(Node* node);

	GameRule_13000306();

	MSG_GR_CaiShenThirteenCard* gameRule;

	void getGameRule(char szDeskConfig[]);

	//获取游戏规则，组装成json
	virtual std::string getRuleJson();

	//读取上次选择的数据，解析json
	virtual void readRuleJson(std::string rule);

	//选择局数
	void setJushuType();

	//选择付费方式
	void setFangFeiType();

	void showMovePeople(bool show);

	virtual void setRoomConfig(const MSG_GP_O_BuyDeskConfig config);
private:
	CheckBox* _checkCaiShen0;
	CheckBox* _checkCaiShen1;
	CheckBox* _checkRenShu0;
	CheckBox* _checkRenShu1;
	CheckBox* _checkRenShu2;
	CheckBox* _checkRenShu3;
	CheckBox* _checkRenShu4;
	Text*     _txtRenShu3;
	Text*     _txtRenShu4;
	CheckBox* _checkWanFa0;
	CheckBox* _checkWanFa1;
	CheckBox* _teShu0;
	CheckBox* _teShu1;
	Node* _csNode;
	MSG_GP_O_BuyDeskConfig _config;
};
