/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRule_12100704.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"

GameRule_12100704::GameRule_12100704()
	: HNGameRule()
{
}

GameRule_12100704* GameRule_12100704::create(Node* node)
{
	GameRule_12100704 *pRet = new GameRule_12100704();
	if (pRet && pRet->initData(node)) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}

bool GameRule_12100704::initData(Node* node)
{
	if (!HNGameRule::init()) return false;

	auto Panel_game = (Node*)node->getChildByName("Panel_game");
	auto checkCaiShen0 = (CheckBox*)Panel_game->getChildByName("CheckBox_caishen0");
	checkCaiShen0->setVisible(true);
	auto Text_caishen0 = (Text*)Panel_game->getChildByName("Text_caishen0");
	Text_caishen0->setVisible(true);
	auto Text_caishen1 = (Text*)Panel_game->getChildByName("Text_caishen1");
	Text_caishen1->setVisible(true);
	auto checkCaiShen1 = (CheckBox*)Panel_game->getChildByName("CheckBox_caishen1");
	checkCaiShen1->setVisible(true);

	sprintf(_gameRule, "%s", "true");
	
	checkCaiShen0->addClickEventListener([=](Ref*){
		checkCaiShen0->setSelected(true);
		checkCaiShen1->setSelected(false);
		sprintf(_gameRule, "%s", "true");
	});

	checkCaiShen1->addClickEventListener([=](Ref*){
		checkCaiShen1->setSelected(true);
		checkCaiShen0->setSelected(false);
		sprintf(_gameRule, "%s", "false");
	});

	return true;
}