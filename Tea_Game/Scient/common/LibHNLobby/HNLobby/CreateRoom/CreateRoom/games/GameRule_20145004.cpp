/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameRule_20145004.h"
#include "HNNetExport.h"
#include "json/rapidjson.h"
#include "json/document.h"

GameRule_20145004::GameRule_20145004()
	: HNGameRule()
{
}

GameRule_20145004* GameRule_20145004::create(Node* node)
{
	GameRule_20145004 *pRet = new GameRule_20145004();
	if (pRet && pRet->initData(node)) {
		pRet->autorelease();
	}
	else {
		HN_SAFE_DELETE(pRet);
	}
	return pRet;
}
GameRule_20145004::~GameRule_20145004()
{

}
bool GameRule_20145004::initData(Node* node)
{
	if (!HNGameRule::init()) return false;

	auto Panel_game = (Node*)node->getChildByName("Panel_game");
	
	for (int i = 0; i < 2; i++)
	{
		_textJuShu[i] = (Text*)Panel_game->getChildByName(StringUtils::format("Text_jewels%d", i));
	}

	for (int i = 0; i < 2; i++)
	{
		_checkFangFei[i] = (CheckBox*)Panel_game->getChildByName(StringUtils::format("CheckBox_pay%d", i));
		_checkFangFei[i]->addEventListener([=](Ref* ref, ui::CheckBox::EventType type){
			_checkFangFei[0]->setSelected(false);
			_checkFangFei[1]->setSelected(false);

			_checkFangFei[i]->setSelected(true);
			_iPay = i;
			setShowJuShu();

		});
	}

	for (int i = 0; i < 2; i++)
	{
		_checkGameMode[i] = (CheckBox*)Panel_game->getChildByName(StringUtils::format("CheckBox_playmode%d",i));
		_checkGameMode[i]->addEventListener([=](Ref* ref, ui::CheckBox::EventType type) {

			if (type == CheckBox::EventType::UNSELECTED)
			{
				_checkGameMode[i]->setSelectedState(true);
				return;
			}
			for (int j = 0; j < 2; j++)
			{
				auto ck_box = dynamic_cast<CheckBox*>(Panel_game->getChildByName(StringUtils::format("CheckBox_playmode%d", j)));
				if (ck_box) ck_box->setSelected(ck_box == _checkGameMode[i]);
			}
		});
	}

	for (int i = 0; i < 5; i++)
	{
		_checkGameRule[i] = (CheckBox*)Panel_game->getChildByName(StringUtils::format("CheckBox_rule%d", i));
	}
	
	return true;
}

void GameRule_20145004::getGameRule(char szDeskConfig[])
{
	szDeskConfig[0] = _checkGameMode[0]->isSelected() ? '1' : '0';
	szDeskConfig[1] = _checkGameMode[1]->isSelected() ? '1' : '0';
	szDeskConfig[2] = _checkGameRule[0]->isSelected() ? '1' : '0';
	szDeskConfig[3] = _checkGameRule[1]->isSelected() ? '1' : '0';
	szDeskConfig[4] = _checkGameRule[2]->isSelected() ? '1' : '0';
	szDeskConfig[5] = _checkGameRule[3]->isSelected() ? '1' : '0';
	szDeskConfig[6] = _checkGameRule[4]->isSelected() ? '1' : '0';
}

//��������
void GameRule_20145004::setRoomConfig(const MSG_GP_O_BuyDeskConfig config)
{
	_config = config;
	setShowJuShu();
}

void GameRule_20145004::setShowJuShu()
{
	std::string str("");
	for (int i = 0; i < 2; i++)
	{
		str = StringUtils::format("%d%s%d%s%s", _config.buyCounts[i].iBuyCount, GBKToUtf8("Ȧ"),
			(_iPay == 1) ? _config.buyCounts[i].iAAJewels : _config.buyCounts[i].iJewels, GBKToUtf8("��ʯ"), (_iPay == 1) ? GBKToUtf8("/��") : "");
		_textJuShu[i]->setString(str);
	}
}
