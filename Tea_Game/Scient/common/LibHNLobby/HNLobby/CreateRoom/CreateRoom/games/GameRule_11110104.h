/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#pragma once

#include "HNUIExport.h"
#include "HNNetExport.h"

USING_NS_CC;
using namespace ui;
using namespace cocostudio;
#pragma pack(1)
struct GAME_RULE_11110104
{
	int iLnkBankerCnt;//连庄次数
	BYTE iUserCount;		//游戏人数
	GAME_RULE_11110104()
	{
		memset(this, 0, sizeof(GAME_RULE_11110104));
	}
};
#pragma pack()
class GameRule_11110104 : public HNGameRule
{
public:
	static GameRule_11110104* create(Node* node);

	bool initData(Node* node);

	GameRule_11110104();

	void setShowJuShu();
	void setFangFeiTag();
	virtual void setRoomConfig(const MSG_GP_O_BuyDeskConfig config);
	virtual void getGameRule(char szDeskConfig[]);
	virtual std::string getRuleJson();
	virtual void readRuleJson(std::string localStr);
private:
	int selectIdx_jushu = 0;
	int selectIdx_renzhu = 0;
	int selectIdx_fangfei = 0;
	MSG_GP_O_BuyDeskConfig _config;
	Node* _csNode;
	//人数选项
	CheckBox* _ck_renshu[3];
};
