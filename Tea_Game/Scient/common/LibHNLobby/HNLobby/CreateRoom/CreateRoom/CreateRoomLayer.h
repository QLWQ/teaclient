/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_CreateRoomLayer_h__
#define HN_CreateRoomLayer_h__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "HNLogicExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "extensions/cocos-ext.h"
#include "ui/UICheckBox.h"
#include "../../GamePersionalCenter/GameUserHead.h"
#include "../../GameBaseLayer/GameLading.h"
USING_NS_CC;
using namespace ui;
using namespace cocostudio;
using namespace HN;

class CreateRoomLayer : public HNLayer, public HN::IHNRoomLogicBase, public HNHttpDelegate, public ui::EditBoxDelegate
{
public:
	typedef std::function<void()> UpdateJewelCallBack;
	UpdateJewelCallBack onUpdateJewelCallBack = nullptr;

	typedef std::function<void()> CreateListLayer;
	CreateListLayer	_createListLayer = nullptr;

	typedef std::function<void()> EnterGameCallBack;
	EnterGameCallBack  onEnterGameCallBack = nullptr;

	typedef std::function<void()> CloseRoomCallBack;
	CloseRoomCallBack	onCloseCallBack = nullptr;
	
public:
	static CreateRoomLayer* create(int CreateType);

	// 初始化
	bool init(int CreateType);

	// 构造函数
	CreateRoomLayer();

	// 析构函数
	virtual ~CreateRoomLayer();

private:
	// 游戏列表点击回调
	void gameClickCallback(Ref* pSender);

	// 创建房间按钮回调
	void createRoomClickCallback(Ref* pSender);
	void onChickCreateCallBack(Ref* pSender); //确定创建房间回调6.3

	// 选择超时时间按钮回调
	void ClickOutCardTime(Ref* pSender);
	//光剑房间界面关闭按钮

	// 执行创建
	void createRoom();
	// 复选框回调
	void checkBoxCallback(Ref* pSender, CheckBox::EventType type);
private:

	//创建俱乐部房间结果
	bool createClubRoomResult(HNSocketMessage* socketMessage);

	// 创建房间回调
	bool createRoomMessagesEventSelector(HNSocketMessage* socketMessage);

	// 更新游戏分类列表
	void updataGamelist(int Gametype);

	//创建茶楼楼层结果
	bool createTeaHouseRoomResult(HNSocketMessage* socketMessage);

public:
	Layout* getGamesLayout();
	void ShowGameList();
	int getCardnum(BYTE card);

	int getCardHua(BYTE card);
	std::string getNumString();

	// 创建游戏列表
	void createGameList(int index); //1房卡场 2金币场 3 比赛场

	void CreateRoomInfoList();

	void getSSSJiaMaCardType();
	void changeButtonClickCallBack(Ref* ref, Widget::TouchEventType type);

	std::string get_userDefined();  //获取玩法gjd6.18

	//金币房间进入需要的通讯
	HNRoomLogicBase*		_roomLogic = nullptr;

	//登陆房间回调
	virtual void onRoomLoginCallback(bool success, const std::string& message, UINT roomID, UINT handldeCode) override;
	virtual void onRoomSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo, BYTE seatNo) override;
	virtual void onRoomQueueSitCallback(bool success, const std::string& message, UINT roomID, BYTE deskNo) override;

	// 房间密码
	virtual void onPlatformRoomPassEnter(bool success, UINT roomId);

	void requestNotice(int num);

	// 设置TeaHouseID
	void setTeaHouseID(int TeaHouseID);

	// 设置游戏ID
	void setTeaHouseGameID(int GameID);

	//恢复红花选项
	void setRedFlowerCheck(SClubTeahouseOption Data);

	virtual void onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData) override;

	GameLading *m_loading;
private:
	//金币房间接口↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	/////////////////////////////////////////////////////////////
	// 功能接口
	void closeFunc();

	// 列表按钮点击回调函数
	void enterRoomEventCallBack(Ref* pSender, Widget::TouchEventType type);

	// 房间密码输入回调
	void onEnterPasswordClickCallback(const std::string& password);
	/////////////////////////////////////////////////////////////

	// 获取创建房间配置
	void getCreateRoomConfig(int gameNameID);

	// 获取配置回调
	bool getConfigEventSelector(HNSocketMessage* socketMessage);

	// 显示创建房间配置
	void showConfigLayout(MSG_GP_O_BuyDeskConfig* config);

	void creatNewRoomNode();

	// 显示局数，总分，圈数勾选项
	void showGameFinishCondition(Text* text, int count, int jewels, BYTE bFinishCondition, bool isAA = false);
	//房间是否加密显示/不显示
	void isRoomNeedPassWord(Node* Parent, bool isShow);
	//返回
	void PageGetBack();

	void CreateRoomGameList();

	void CreateGoldGameList();

	//更新用户信息
	void UpdateUsetInfo();

	//底部按钮回调
	void menuBottomCallBack(Ref* pSender);

	//创建房间游戏切换
	void SwitchGameBtnCallBack(Ref* pSender);

	void switchAccount();
	
	//初始化红花选项
	void initRedFlowerCheck();

	//刷新所有复选框
	void refreshAllCheckBox(Node* parent);

	//创建房间获取红花选项
	SClubTeahouseOption detectionRedFlower();

	bool CheckRedFlowerInput();
	////////////////////////////////////////////////////////////////////
	//创建房间节点复选框点击回调事件
	void checkFirstSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkSecondSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkThireSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkForthSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkFithSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkSixSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkFuFeiSelectCallback(Ref* pSender, CheckBox::EventType type);
	void shuangjinpinghu(Ref* pSender, CheckBox::EventType type);	//双金平胡
	void qiangganghu(Ref* pSender, CheckBox::EventType type);	//抢杠胡
	void checkRenShuSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkWanFaSelectCallback(Ref* pSender, CheckBox::EventType type); //玩法回调6.6
	void checkSevenSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkOutCardTimeSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkCardNumSelectCallback(Ref* pSender, CheckBox::EventType type);	//牌张数选择
	void checkBombOptionsSelectCallback(Ref* pSender, CheckBox::EventType type);	//炸弹选项
	void checkPickFlowerSelectCallback(Ref* pSender, CheckBox::EventType type);
	void checkDeskSelectCallback(Ref* pSender, CheckBox::EventType type);
	///////////////////////////////////////////////////////////////////
	//存储复选框的容器
	std::vector<CheckBox*>	_firthSelectCheckBoxs;
	std::vector<CheckBox*>	_secondSelectCheckBoxs;
	std::vector<CheckBox*>	_thirdSelectCheckBoxs;
	std::vector<CheckBox*>	_fourthSelectCheckBoxs;
	std::vector<CheckBox*>	_fifthSelectCheckBoxs;
	std::vector<CheckBox*>	_sixthSelectCheckBoxs;
	std::vector<CheckBox*>	_fufeiRuleCheckBoxs;
	std::vector<CheckBox*>	_seventhSelectCheckBoxs;
	std::vector<CheckBox*>	_renshuSelectCheckBoxs;
	std::vector<CheckBox*>	_wanfaSelectCheckBoxs;
	std::vector<CheckBox*>	_outcardSelectCheckBoxs;
	std::vector<CheckBox*>	_begincardSelectCheckBoxs;
	std::vector<CheckBox*>	_outbombSelectCheckBoxs;
	std::vector<CheckBox*>	_PickFlowerSelectCheckBoxs;
	std::vector<CheckBox*>	_deskSelectCheckBoxs;
	std::vector<Layout*>		_panel_text_make;
	std::vector<Text*>		_Text_tip;
	///////////////////////////////////////////////////////////////////
	//创建泉州麻将房间节点
	void creatQZMJRoomNode();
	//创建晋江麻将房间节点
	void creatJJMJRoomNode();
	//创建牛牛游戏房间节点
	void creatNNRoomNode();
	//创建漳州麻将节点
	void creatZZMJRoomNode();
	//创建斗地主
	void creatDDZRoomNode();
	//创建跑得快
	void creatRunCrazyRoomNode();
	//创建十三水
	void creatSSSRoomNode();
	//创建厦门麻将
	void creatXMMJRoomNode();
	//创建510K
	void create510KRoomNode();
	//创建游金麻将房间节点
	void creatYJMJRoomNode();
	//创建上游跑得快
	void createSYRunCrazyRoomNode();
	//创建算分跑得快
	void createSFRunCrazyRoomNode();
	//创建漳浦麻将
	void createZPMJRoomNode();
	
	//根据index获取牌值
	//BYTE getCardValue1(BYTE card);
	//获取十三水加马的牌型
	//void getSSSJiaMaCardType1();
	//十三水换马牌按钮回调
	

	///////////////////////////////////////////////////////////////////
	Button*			_btn_create			= nullptr;
	ListView*		_listView_games		= nullptr;	//游戏列表
	Layout*			_layout_gameList = nullptr;//存放游戏按钮的基础容器
	Layout*			_layout_PanelBlack	= nullptr;//创建界面黑色背景交互层
	Layout*			_layout_games		= nullptr;	//配置页面总容器
	ListView*			_list_btn			= nullptr;	//创建房间游戏按钮列表
	Node*			_select_game		= nullptr;	//当前选择的游戏配置页
	HNGameRule*		_gameRule			= nullptr;	//特殊游戏规则解析页
	Button*			_btnCard			= nullptr;	//房卡场
	Button*			_btnGold			= nullptr;	//金币场
	Button*			_btnMatch			= nullptr;	//比赛场
	ParticleSystemQuad*  _Particle          = nullptr;  //光效 
	Button*			_btnBack			= nullptr;	//返回键
	Sprite*			_sp_RedFlower		= nullptr;	//红花选项
	Layout*			_Panel_RedFlower	= nullptr;	//红花界面选项
	Layout*			Panel_CheckBox		= nullptr;	//红花复选显示框
	////////////////////////////////////////////////////////////////////
	TextField*			TextF_ScoreToRedFlowers = nullptr;	//红花比例
	TextField*			TextF_MinRedFlowersForJoin = nullptr;	//进入最低红花
	TextField*			TextF_AATakeCount = nullptr;		//AA摘花数量
	TextField*			TextF_TakeScoreWinker = nullptr;		//大赢家分数（摘花）
	TextField*			TextF_TakeNumWinker = nullptr;		//大赢家摘花
	TextField*			TextF_TakeScoreRanges1 = nullptr;		//大赢家阶梯分数
	TextField*			TextF_TakeScoreRanges2 = nullptr;		//大赢家阶梯分数
	TextField*			TextF_TakeScoreRanges3 = nullptr;		//大赢家阶梯分数
	TextField*			TextF_TakeScoreRanges4 = nullptr;		//大赢家阶梯分数
	TextField*			TextF_TakeScoreRanges5 = nullptr;		//大赢家阶梯分数
	TextField*			TextF_TakeNumRanges1 = nullptr;			//大赢家阶梯摘花
	TextField*			TextF_TakeNumRanges2 = nullptr;			//大赢家阶梯摘花
	TextField*			TextF_TakeNumRanges3 = nullptr;			//大赢家阶梯摘花
	TextField*			TextF_TakeNumRanges4 = nullptr;			//大赢家阶梯摘花
	TextField*			TextF_TakeNumRanges5 = nullptr;			//大赢家阶梯摘花
	////////////////////////////////////////////////////////////////////
	bool				_isTouch;
	//二级界面用户信息
	Text*				_Text_RoomCard		= nullptr;  //房卡数
	Text*				_Text_GoldNum		= nullptr;  //金币数
	Text*				_Text_Integral		= nullptr;  //礼券数

	//创建房间消费
	Text*				_Text_CostNum		= nullptr;	//创房需要的消费

	//二级界面底部按钮
	Layout*			_panel_bottom		= nullptr;
	ImageView*		_Image_head		= nullptr;
	Text*				_text_vip_level		= nullptr;

	//二级界面游戏列表
	ListView*			_list_RoomGame		= nullptr;	//房卡游戏列表
	ListView*			_list_GoldGame		= nullptr;	//金币游戏列表
	ListView*			_list_RoomInfo		= nullptr;	//金币房间列表

	ComNameInfo*		_JJgame_info		= nullptr;  //晋江游戏
	ComNameInfo*		_QZgame_info		= nullptr;  //泉州游戏

	Button*			_QZBtn			= nullptr;	//泉州麻将
	Button*			_JJBtn			= nullptr;	//晋江麻将

	INT						_roomID = -1;
	Button*					_currentSelectedRoom = nullptr;	// 列表按钮

	Vector<Button*>	_createButtons;
	int				_iType		= 0;	//创建类型（0:按局数，1:按总分数，2:按圈数）
	int				_iCount		= 0;	//创建局数（总分数）
	int				_isNeedPassword = 0;
	int				_iLocation	= 0;	//是否定位（0:不定位，1:定位）
	int				_iPay		= 0;	//支付方式（0:房主，1:AA）
	int				_iPlayerNum = 0;	//游戏限制人数（0:与游戏人数相同，其它人数填写对应值）
	bool			_isInvoice	= false;//是否支持代开
	int             _needRoomCard = 0;//需要的房卡数
	int				_iOutCardTime = 0;//当前设置的出牌时间
	std::string     _userDefined = "";//创建房间自定义的数组
	MSG_GP_O_BuyDeskConfig deskConfig;
	Text* _NNPaixing = nullptr;
	Text* _NNFanbei = nullptr;
	Text* _NNzhuangjia = nullptr;
	vector<Text*> _jushuVec;
	vector<Text*> _renshuVec;
	Button* _Button_change = nullptr;
	Text* _Text_jiama = nullptr;
	BYTE _cardValue = 0;
	CheckBox* m_checkBoxShuangjin;	//双金不平胡
	CheckBox* m_checkBoxQianggang;	//抢杠胡（自摸）
	Layout* m_LayoutTese = nullptr;		//是否显示大小头
	Layout* m_LayoutMore = nullptr;		//大小头更多选项
	Text* m_Text_OutCardTime = nullptr;		//显示出牌时间
	Button* m_btn_add = nullptr;			//增加出牌时间
	Button* m_btn_sub = nullptr;			//减少出牌时间
	//记录茶楼楼层ID
	int m_iTeaHouseID = 0;	
	//记录茶楼楼层游戏ID
	int m_iTeaHouseGameID = 0;

	CheckBox* m_checkBoxMethod = nullptr;	//特殊可勾选玩法
	CheckBox* m_DisarmBomb = nullptr;		//是否可拆炸弹(默认勾选不可拆)

	//管理当前层级标签
	INT				_LAYER_INDEX = 0;

	//检查是否勾选红花选项
	bool m_bCheckRedFlower = false;
	//当前摘花类型(AA==0,大赢家固定==1，大赢家梯阶==2)
	int m_iCheckRedFlower = 0;


	//出牌时间设置
	std::vector<int> _OutCardTimes;  //出牌时间

	/********editBox回调 *******/
public:
	/**
	* This method is called when an edit box gains focus after keyboard is shown.
	* @param editBox The edit box object that generated the event.
	*/
	virtual void editBoxEditingDidBegin(ui::EditBox* editBox) override;
	/**
	* This method is called when an edit box loses focus after keyboard is hidden.
	* @param editBox The edit box object that generated the event.
	*/
	virtual void editBoxEditingDidEnd(ui::EditBox* editBox) override;

	/**
	* This method is called when the edit box text was changed.
	* @param editBox The edit box object that generated the event.
	* @param text The new text.
	*/
	virtual void editBoxTextChanged(ui::EditBox* editBox, const std::string& text) override;

	/**
	* This method is called when the return button was pressed or the outside area of keyboard was touched.
	* @param editBox The edit box object that generated the event.
	*/
	virtual void editBoxReturn(ui::EditBox* editBox) override;
};

#endif // HN_CreateRoomLayer_h__
