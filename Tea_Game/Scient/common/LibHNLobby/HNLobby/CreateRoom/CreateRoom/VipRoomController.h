/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef HN_VipRoomController_h__
#define HN_VipRoomController_h__

#include "HNUIExport.h"
#include "HNLogicExport.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "HNSocket/HNSocketMessage.h"

using namespace HN;
using namespace ui;
using namespace cocostudio;

class VipRoomController : public HNLayer, public IHNVIPRoomLogicBase
{
public:
	typedef std::function<void()> DismissCallBack;
	DismissCallBack onDissmissCallBack = nullptr;

	typedef std::function<void()> SetVipInfoCallBack;
	SetVipInfoCallBack onSetVipInfoCallBack = nullptr;

	typedef std::function<void(int Index)> ClickButtonCallBack;
	ClickButtonCallBack onClickButtonCallBack = nullptr;

	typedef std::function<void()> ReceiveCallBack;
	ReceiveCallBack onReceiveCallBack = nullptr;

	typedef std::function<void(VipDeskInfoResult* pData)> RoomResultCallBack;
	RoomResultCallBack onRoomResultCallBack = nullptr;

public:
	VipRoomController(int deskNo, bool bShowBtn);
	virtual ~VipRoomController();

	static VipRoomController* create(int deskNo, bool bShowBtn = true);

public:
	// 设置解散按钮位置
	void setDismissBtnPos(Vec2 pos);

	// 设置解散按钮图片
	void setDismissBtnTexture(std::string normalPath);

	// 更换解散按钮父节点
	void resetDismissBtnParent(Node* parent, Vec2 pos);

	// 设置返回按钮位置
	void setReturnBtnPos(Vec2 pos);

	// 设置返回按钮图片
	void setReturnBtnTexture(std::string normalPath);

	// 更换返回按钮父节点
	void resetReturnBtnParent(Node* parent, Vec2 pos);

	// 设置邀请按钮位置
	void setInvitationBtnPos(Vec2 pos);

	// 设置邀请按钮图片
	void setInvitationBtnTexture(std::string normalPath);

	//设置邀请按钮是否可见
	void setInvitationBtnVisible(bool isVisible){ _btn_Invitation->setVisible(isVisible); }

	// 设置房号位置
	void setRoomNumPos(Vec2 pos);

	// 设置房号颜色与大小
	void setRoomNumColorAndSize(float size = 20.0f, Color3B color = Color3B(0, 255, 0));

	// 更换房号父节点
	void resetRoomNumParent(Node* parent, Vec2 pos);

	// 设置局数位置
	void setPlayCountPos(Vec2 pos);

	// 设置局数颜色与大小
	void setPlayCountColorAndSize(float size = 20.0f, Color3B color = Color3B(0, 255, 0));

	// 更换局数父节点
	void resetPlayCountParent(Node* parent, Vec2 pos);

	// 设置分享规则内容
	void setShareRule(std::string text);
	//获取桌子信息
	VipDeskInfoResult getRoomResult(){ return _deskinfo; };

	VipDeskInfoResult* getRoomResult1(){ return m_deskinfo; };

	void setRoomResult1(VipDeskInfoResult* pData);

	//设置桌子信息

	void setRoomInfo(VipDeskInfoResult data);

	//设置查看房间信息按钮的位置
	void setRoomResultBtnPos(Vec2 pos);

	// 设置底分位置
	void setDifenPos(Vec2 pos);

	// 设置底分颜色与大小
	void setDifenColorAndSize(float size = 20.0f, Color3B color = Color3B(0, 255, 0));

	// 自定义信息设置
	void setDifenString(string str);

	//设置底分的文本
	void setDifenStr();
	//拿到房间信息按钮
	Button* getRoomInfoBtn();
public:
	// 解散房间按钮回调
	void dismissBtnCallBack(Ref* ref);

	// 返回大厅按钮回调
	void returnBtnCallBack(Ref* ref);

	// 邀请好友按钮回调
	void invitationBtnCallBack(Ref* ref);
	//将扑克花色数值转换成文字
	void getCardValue();
	//查看房间信息回调
	void roomresultBtnCallBack(Ref* ref);
	//斗地主房间信息
	void getDDZRoomInfo();
	//牛牛房间信息
	void getNNRoomInfo();
	//十三水房间信息
	void getSSSRoomInfo();
	//泉州麻将房间信息
	void getQZMJRoomInfo();
	//晋江麻将房间信息
	bool getJJMJRoomInfo();
	//厦门麻将房间信息
	void getXMMJRoomInfo();
	//漳州麻将房间信息
	void getZZMJRoomInfo();

	Text* getTextPlayCount() { return _text_playCount; }

    // 房卡消息监听代理
    void addVipRoomObserver(HNVipRoomDelegate* obs);

    // 申请解散 - 不带额外弹窗
    void dissmissWithoutPrompt();

    // 返回大厅 - 不带额外弹窗
    void returnWithoutPrompt();

    // 房卡场游戏是否开始
    bool isVipRoomGameStarted() { return _isGameStart; }

    // 是否房主
    bool isMaster(int userid) { return _deskinfo.iMasterID == userid; }

    // 当前玩家是否房主
    bool isMeMaster() { return _deskinfo.iMasterID == PlatformLogic()->loginResult.dwUserID; }

    // 获取房间密码gjd6.18
    std::string getVipRoomPwd() { return _deskinfo.szPassWord; }

	//获取特殊玩法
	BYTE getDeskConfig() { return _deskinfo.DeskConfig[2]; }

	//获取桌子特殊规则
	BYTE getDeskConfigByIndex(int index){ return _deskinfo.DeskConfig[index]; }

	//打开游戏奖励红包机制
	void setOpenActivity(bool isOpen);

	//设置红包按钮位置
	void setActivityPosition(float X,float Y);

	//更新当前任务状态
	void setCurrentTask(int Index, int toal);

	//更新任务达标
	void setIsStandard(bool bActivity);

	//更新红包奖励个数
	void setCanTakeNum(int TakeNum);

	//播放中奖动画
	void setAnimationForTake(int AnimationType, int AwardNum);

    //// 请求解散
    //void requestDismiss() { _logic->requestDismissDesk(); }

    //// 请求回到大厅
    //void requestReturnPlatform() { _logic->requestReturnPlatform(); }
private:
	// 初始化
	virtual bool init() override;

	// 设置显示桌子信息
	void setVipDeskInfo(std::string password, int masterID, int playCount, int nowPlayCount, int type, int seconds);

	virtual void showTipBox(std::string tips, bool bCanSelect = false, std::string name = "", 
		std::function<void()> sure = nullptr, std::function<void()> cancel = nullptr, bool isRemove = true);

	std::string secondToTimeStr(int second); //将秒数转换成字符串

	void updateRestTime(float dt); //更新剩余时间

	void AddAnimation(int Type, int AwardNum, Node* node);
private:
	// 获取房间信息通知
	virtual void onRoomGetRoomInfoCallback(bool success, VipDeskInfoResult* info) override;

	// 申请解散通知
	virtual void onRoomDismissNotifyCallback(VipDeskDismissNotify* notify) override;

	// 解散结果通知
	virtual void onRoomDissmissCallback(bool success) override;

	// 解散消息玩家操作通知
	virtual void onRoomDissmissActionCallback(UINT userID, bool isAgree) override;

	// 断线重连消息通知
	virtual void onRoomNetCutCallback(NetCutDismissNotify* notify) override;

	// IP/距离过近通知
	virtual void onRoomDistanceCallback(std::vector<MSG_GR_S_Position_Notice *> queueNotice) override;

	// IP/距离过近玩家操作通知
	virtual void onRoomDistanceActionCallback(UINT userID, bool isAgree) override;

	// 所有玩家同意游戏开始通知
	virtual void onRoomAllUserAgreeCallback(bool isAgree) override;

	// 返回大厅通知
	virtual void onRoomReturnPlatformCallback(bool success, std::string message) override;

private:
	// 显示玩家解散房间操作列表
	void showUserActionList(INT userID, bool isAgree);

	// 显示解散倒计时
	void showCountdown(float dt);
	//转换一下桌子信息字符串
	int asciiTointNum(int num);

	//红包查看
	void ButtonRedBagCallBack(Ref* pSender);

	//领取红包点击
	void ButtonRedClick(Ref* pSender);

private:
	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event) override;

	bool onTouchBegan(Touch* touch, Event *event);

    void doNotShowAnyDelegateButtons();
private:
	VipRoomLogic* _logic		= nullptr;
	int		_iTime				= 0;				// 解散倒计时剩余时间
	int		_bkGameTime			= 0;				// 切后台时长
	UINT	_applyUserID		= INVALID_USER_ID;	// 申请解散玩家ID
	bool	_isDismiss			= false;			// 房间是否已解散
	bool	_isGameStart		= false;			// 牌局是否开始
	bool	_isShowBtn			= true;				// 是否需要通用按钮
	bool	_isTouch			= true;				// 按钮是否可以点击
	Button* _btn_dismiss		= nullptr;			// 申请解散按钮
	Button* _btn_return			= nullptr;			// 返回大厅按钮
	Button* _btn_Invitation		= nullptr;			// 邀请好友按钮
	Button* _btn_RoomResult		= nullptr;			// 查看房间信息按钮
	Layout*	_dismissLayer		= nullptr;			// 解散操作弹出框
	Layout*	_actionListLayer	= nullptr;			// 已操作玩家列表

	Text*	_text_roomNum		= nullptr;			// 房号文本
	Text*	_text_playCount		= nullptr;			// 局数显示
	Text*	_text_optTime		= nullptr;			// 等待同意倒计时
	Text*	_text_listTime		= nullptr;			// 同意列表倒计时
	Text*	_text_difen			= nullptr;			// 底分显示
	std::string	_shareRule		= "";				// 分享玩法内容

	Button* _btn_Activity		= nullptr;			//红包活动
	Layout* _panel_ActivityM = nullptr;			//移动条背景
	Sprite* _sp_TaskBg		= nullptr;			//红包任务背景
	Layout* _panel_Activity	= nullptr;			//领取奖励页面
	Text*	  _text_Activity		= nullptr;			//红包活动文档
	Text*	  _text_Activity_num  = nullptr;			//红包活动文档
	Text*  _CanTakeNum		= nullptr;			//能拆的红包个数
	ImageView* _image_Tip	= nullptr;			//提示红包个数是否显示
	bool _isActivity			= false;			//是否达到奖励
	bool _ShowStater		= false;			//任务展开状态

	bool _bCooling = false;					//解散按钮是否可点击

	CC_SYNTHESIZE(std::string, _gameName, GameName);	//当前游戏名称

	EventListenerCustom*	_listener = nullptr;
	EventListenerCustom*	_foreground = nullptr;

    VipDeskInfoResult _deskinfo;
    HNVipRoomDelegate* _vipRoomObserver;



	VipDeskInfoResult* m_deskinfo;
};

#endif // HN_VipRoomController_h__
