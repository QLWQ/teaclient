/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__ManagerDelegate__
#define __HN__ManagerDelegate__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include <vector>
#include "managerCell.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

namespace HN
{
	class ManagerDelegate : public TableViewDelegate
	{
	public:
		ManagerDelegate();

		~ManagerDelegate();

		// cell被点击
		virtual void tableCellTouched(TableView* table, TableViewCell* cell) override;

		// cell高亮
		virtual void tableCellHighlight(TableView* table, TableViewCell* cell) override;

		// cell被回收
		virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell) override;
	};

	class ManagerDataSource : public TableViewDataSource
	{
	public:
		typedef std::function<void(std::string)> getUsersCallBack;
		// 获取玩家信息回调
		getUsersCallBack onGetusersCallBack = nullptr;

		typedef std::function<void(std::string psd)> DismissCallBack;
		// 解散房间回调
		DismissCallBack onDisCallBack = nullptr;

		typedef std::function<void(std::string psd)> DeleteCallBack;
		// 删除记录回调
		DeleteCallBack onDelCallBack = nullptr;

	public:
		ManagerDataSource();

		~ManagerDataSource();

		//创建cell
		virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);

		//Cell个数
		virtual ssize_t numberOfCellsInTableView(TableView *table);

		//Cell大小
		virtual Size tableCellSizeForIndex(TableView *table, ssize_t idx);

	public:
		// 设置有效房间数据
		void setRoomData(MSG_GP_O_GetBuyDeskList* desk, bool isClear = false);

		// 设置房间玩家
		void setRoomUsers(std::string passWord, MSG_GP_O_GetDeskUser* deskUser, bool isClear = false);

		// 是否有数据
		bool isEmpty();

		// 获取刷新后列表滑动偏移量
		Vec2 getRefreshOffset();
		

	private:
		std::vector<MSG_GP_O_GetBuyDeskList>	_roomList;		//房间列表
		std::vector<bool> _isOpenVec;							//列表是否展开
		Vec2	_oldOffset = Vec2::ZERO;						//刷新列表前滑动偏移量
		int		_oldCount = 0;									//刷新前列表个数
		CC_SYNTHESIZE_PASS_BY_REF(bool, _isInvalid, IsInvalid);	//当前列表是否有效房间

		std::map<std::string, std::vector<MSG_GP_O_GetDeskUser>> _roomUsers;//房间玩家信息
		
	};

}

#endif //__HN__ManagerDelegate__