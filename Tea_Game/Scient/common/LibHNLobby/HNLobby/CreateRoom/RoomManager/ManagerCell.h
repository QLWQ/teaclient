/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN__ManagerCell__
#define __HN__ManagerCell__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace extension;

namespace HN
{
	//用于显示的列表结构体
	struct RoomsStruct
	{
		bool		isOpen;				//列表是否展开
		bool		isRefresh;			//是否是刷新数据
		bool		isInvalid;			//房间是否有效(true:无效，false:有效)
		bool		isGameStart;		//游戏是否开始
		int			iPlayerCount;		//玩家人数
		LLONG		iPlayTime;			//时间
		std::string	szGameName;			//游戏名称
		std::string	szPassWord;			//桌子密码（房号）

		RoomsStruct()
		{
			memset(this, 0, sizeof(RoomsStruct));
		}
	};

	class ManagerCell : public TableViewCell
	{
	public:
		typedef std::function<void(bool isOpen, int index, std::string)> openCallBack;
		// 展开/收起 列表回调
		openCallBack onOpenCallBack = nullptr;

		typedef std::function<void(std::string psd)> DismissCallBack;
		DismissCallBack onDisCallBack = nullptr;

		typedef std::function<void(std::string psd)> DeleteCallBack;
		DeleteCallBack onDelCallBack = nullptr;

	public:
		ManagerCell();
		~ManagerCell();

		static ManagerCell* create();

		// 创建/刷新 cell
		void buildCell(RoomsStruct* room);

		// 列表点击展开/收起
		void showCellOpenOrClose();

		// 进入桌子
		void enterDesk();

		// 解散桌子
		void dismissDesk();

		// 删除记录
		void deleteDesk();

		// 显示玩家
		void showUsers(std::vector<MSG_GP_O_GetDeskUser>* users);

	private:
		RoomsStruct		_room;
		Layout*			_layout = nullptr;
		ListView*		_list_user		= nullptr;//展开列表
	};

}

#endif //__HN__ManagerCell__
