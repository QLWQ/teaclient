/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RoomManager.h"
#include "ManagerCell.h"
#include "ManagerDelegate.h"

USING_NS_CC;
using namespace HN;


#define DEBUG_RECORD 0


static const char* RECORD_CSB	= "platform/RoomManager/Node_manager.csb";
#define BTN_NORMAL_COLOR	Color3B(161,86,41)  
#define BTN_SELECT_COLOR	Color3B(242,232,210)

RoomManager::RoomManager()
	: onRefreshCallBack(nullptr)
{

}

RoomManager::~RoomManager()
{
	HN_SAFE_DELETE(_delegate);
	HN_SAFE_DELETE(_dataSource); 
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GETTOTALRECORD);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GETSINGLERECORD);
	PlatformLogic()->removeEventSelector(MDM_GP_DESK_VIP, ASS_GP_GET_RECORDURL);
}

void RoomManager::close()
{
	HNAudioEngine::getInstance()->playEffect(GAME_SOUND_CLOSE);

	if (onCloseCallBack)
	{
		onCloseCallBack();
	}

	this->runAction(Sequence::create(FadeOut::create(0.3f),
		CallFunc::create([&]()
	{
		this->removeFromParent();
	}), nullptr));
}

void RoomManager::openManager(Node* parent, Vec2 vec)
{
	CCAssert(nullptr != parent, "parent is nullptr");

	setPosition(vec);
	parent->addChild(this);
	this->runAction(FadeIn::create(0.3f));
}

bool RoomManager::init()
{
	if (!HNLayer::init()) return false;

	if (!loadUI(RECORD_CSB)) return false;

	return true;
}

bool RoomManager::loadUI(const char *sFileName)
{
	if (!sFileName || !FileUtils::getInstance()->isFileExist(sFileName))
		return false;

	auto rootNode = CSLoader::createNode(sFileName);
	if (!rootNode) return false;
	addChild(rootNode);

	auto layout = dynamic_cast<ui::Layout*>(rootNode->getChildByName("Panel_manager"));
	if (!layout) return false;
	layout->setPosition(_winSize / 2);

	Size layOutSize = layout->getContentSize();
	layout->setScaleX(_winSize.width / layOutSize.width);
	layout->setScaleY(_winSize.height / layOutSize.height);

	auto img_bg = dynamic_cast<ImageView*>(layout->getChildByName("Image_BG"));

	if (_winSize.width / _winSize.height < 1.78)
	{
		img_bg->setScale(layOutSize.width / _winSize.width, layOutSize.height / _winSize.height);
	}
	else
	{
		img_bg->setScale(0.9f, layOutSize.height / _winSize.height * 0.9f);
	}

	// 关闭按钮
	auto btn_close = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btn_close->addClickEventListener([=](Ref*)
	{
		close();
	});

	auto img_managerBG = dynamic_cast<ImageView*>(img_bg->getChildByName("Image_managerBG"));
	//auto img_share = dynamic_cast<ImageView*>(img_managerBG->getChildByName("Image_share"));
	//auto img_finish = dynamic_cast<ImageView*>(img_managerBG->getChildByName("Image_finish"));

	auto layout_manager = dynamic_cast<Layout*>(img_bg->getChildByName("Panel_manager"));

	// 创建代理
	_delegate = new ManagerDelegate();
	_dataSource = new ManagerDataSource();

	// 解散房间
	_dataSource->onDisCallBack = [=](std::string psd) {
	
		if (psd.empty()) return;

		MSG_GP_I_DismissDesk dis;
		memcpy(dis.szDeskPass, psd.c_str(), sizeof(dis.szDeskPass));

		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_DISMISS_DESK, &dis, sizeof(MSG_GP_I_DismissDesk),
			HN_SOCKET_CALLBACK(RoomManager::dismissCallback, this));
	};

	// 删除记录
	_dataSource->onDelCallBack = [=](std::string psd) {

		if (psd.empty()) return;

		MSG_GP_I_DeleteRecord del;
		memcpy(del.szDeskPass, psd.c_str(), sizeof(del.szDeskPass));

		PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_DELETE_RECORD, &del, sizeof(MSG_GP_I_DeleteRecord),
			HN_SOCKET_CALLBACK(RoomManager::deleteCallback, this));
	};

	// 房间列表
	_tableView = TableView::create(_dataSource, layout_manager->getContentSize());
	_tableView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	_tableView->setDelegate(_delegate);
	layout_manager->addChild(_tableView);

	// 有效房间
	auto btn_effective = dynamic_cast<Button*>(img_bg->getChildByName("Button_effective"));

	// 无效房间
	auto btn_invalid = dynamic_cast<Button*>(img_bg->getChildByName("Button_invalid"));


	btn_effective->loadTextureNormal("platform/common/new/yxsm_an.png");
	btn_invalid->loadTextureNormal("");
	btn_effective->setTitleColor(BTN_SELECT_COLOR);
	btn_invalid->setTitleColor(BTN_NORMAL_COLOR);
	btn_effective->addClickEventListener([=](Ref*){
		btn_effective->loadTextureNormal("platform/common/new/yxsm_an.png");
		btn_invalid->loadTextureNormal("");
		btn_effective->setTitleColor(BTN_SELECT_COLOR);
		btn_invalid->setTitleColor(BTN_NORMAL_COLOR);
		//img_share->setVisible(true);
		//img_finish->setVisible(false);

		getRoomList(false);
	});

	btn_invalid->addClickEventListener([=](Ref*){
		btn_effective->loadTextureNormal("");
		btn_invalid->loadTextureNormal("platform/common/new/yxsm_an.png");
		btn_effective->setTitleColor(BTN_NORMAL_COLOR);
		btn_invalid->setTitleColor(BTN_SELECT_COLOR);
		//img_share->setVisible(false);
		//img_finish->setVisible(true);


		getRoomList(true);
	});

	// 刷新房间
	auto btn_refresh = dynamic_cast<Button*>(img_bg->getChildByName("Button_refresh"));
	btn_refresh->addClickEventListener([=](Ref*) {
	
		getRoomList(btn_invalid->getTitleColor() == BTN_SELECT_COLOR);
	});

	// 获取玩家信息
	_dataSource->onGetusersCallBack = [=](std::string deskPass){

		getUserInfo(deskPass);
	};

	// 初始获取数据
	getRoomList(false);

	return true;
}

void RoomManager::getRoomList(bool isInvalid/* = false*/)
{
	MSG_GP_I_GetBuyDeskList get;
	get.bType = isInvalid ? 1 : 0;
	get.iUserID = PlatformLogic()->loginResult.dwUserID;

	PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GET_DESKLIST, &get, sizeof(MSG_GP_I_GetBuyDeskList),
		HN_SOCKET_CALLBACK(RoomManager::getRoomListCallback, this));

	_dataSource->setRoomData(nullptr, true);

	// 更新玩家信息，处理因为房间状态改变导致的玩家钻石变化
	if (onRefreshCallBack) onRefreshCallBack();

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在获取信息"), 25);
}

void RoomManager::getUserInfo(std::string deskPass)
{
	MSG_GP_I_GetDeskUser get;
	strcpy(get.szDeskPass, deskPass.c_str());

	PlatformLogic()->sendData(MDM_GP_DESK_VIP, ASS_GP_GET_DESKUSER, &get, sizeof(MSG_GP_I_GetDeskUser),
		HN_SOCKET_CALLBACK(RoomManager::getUserInfoCallback, this));

	LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在获取信息"), 25);
}

bool RoomManager::getRoomListCallback(HNSocketMessage* socketMessage)
{
	// 返回消息可能接受到多次，HandleCode为0时表示还有消息未发完
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		MSG_GP_O_GetBuyDeskList* pData = (MSG_GP_O_GetBuyDeskList*)socketMessage->object;
		INT Count = socketMessage->objectSize / sizeof(MSG_GP_O_GetBuyDeskList);
		Count = Count < 0 ? 0 : Count;
		while (Count-- > 0)
		{
			MSG_GP_O_GetBuyDeskList* pInfo = pData++;
			_dataSource->setRoomData(pInfo);
		}
	}
	else
	{
		_tableView->reloadData();

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}

	return true;
}

bool RoomManager::getUserInfoCallback(HNSocketMessage* socketMessage)
{
	// 返回消息可能接受到多次，HandleCode为0时表示还有消息未发完
	if (0 == socketMessage->messageHead.bHandleCode)
	{
		//每条消息前部分是桌子密码，后部分是n个玩家信息
		MSG_GP_I_GetDeskUser* psd = (MSG_GP_I_GetDeskUser*)socketMessage->object;

		MSG_GP_O_GetDeskUser* pData = (MSG_GP_O_GetDeskUser*)(((CHAR*)socketMessage->object) + sizeof(MSG_GP_I_GetDeskUser));

		INT Count = (socketMessage->objectSize - sizeof(MSG_GP_I_GetDeskUser)) / sizeof(MSG_GP_O_GetDeskUser);
		Count = Count < 0 ? 0 : Count;
		while (Count-- > 0)
		{
			MSG_GP_O_GetDeskUser* pUser = pData++;
			_dataSource->setRoomUsers(psd->szDeskPass, pUser, false);
		}
	}
	else
	{
		_tableView->reloadData();

		_tableView->setContentOffset(_dataSource->getRefreshOffset());

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
	}

	return true;
}

bool RoomManager::dismissCallback(HNSocketMessage* socketMessage)
{
	auto prompt = GamePromptLayer::create();

	switch (socketMessage->messageHead.bHandleCode)
	{
	case ERR_GP_DISMISSDESK_SUCCESS:
		prompt->showPrompt(GBKToUtf8("解散成功"));
		getRoomList(false);
		break;
	case ERR_GP_NOT_FIND_USER_CLR:
		prompt->showPrompt(GBKToUtf8("错误的玩家信息"));
		break;
	case ERR_GP_NOT_DESK_MASTER:
		prompt->showPrompt(GBKToUtf8("不是此房间房主"));
		break;
	case ERR_GP_DESKPASSS_ERROR:
		prompt->showPrompt(GBKToUtf8("房号错误"));
		break;
	case ERR_GP_DESK_INGAME:
		prompt->showPrompt(GBKToUtf8("此房间游戏已经开始"));
		break;
	default:
		break;
	}

	return true;
}

bool RoomManager::deleteCallback(HNSocketMessage* socketMessage)
{
	auto prompt = GamePromptLayer::create();

	switch (socketMessage->messageHead.bHandleCode)
	{
	case ERR_GP_DELETE_RECORD_SUCCESS:
		prompt->showPrompt(GBKToUtf8("删除成功"));
		getRoomList(true);
		break;
	case ERR_GP_NOT_FIND_USER_DEL:
		prompt->showPrompt(GBKToUtf8("错误的玩家信息"));
		break;
	case ERR_GP_NOT_DESK_MASTER_DEL:
		prompt->showPrompt(GBKToUtf8("不是此房间房主"));
		break;
	case ERR_GP_DESKPASSS_ERROR_DEL:
		prompt->showPrompt(GBKToUtf8("房号错误"));
		break;
	default:
		break;
	}

	return true;
}

