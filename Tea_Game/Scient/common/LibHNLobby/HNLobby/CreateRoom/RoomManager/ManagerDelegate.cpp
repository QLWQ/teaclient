/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "ManagerDelegate.h"

namespace HN
{
	#define SPACEING   15    //间距
	//////////////////////////////////////////////////////////////////////////
	////				TableViewDelegate
	//////////////////////////////////////////////////////////////////////////
	ManagerDelegate::ManagerDelegate()
	{

	}

	ManagerDelegate::~ManagerDelegate()
	{

	}

	void ManagerDelegate::tableCellTouched(TableView* table, TableViewCell* cell)
	{
		// 点击列表时进入当前桌子
		((ManagerCell*)cell)->enterDesk();
	}

	void ManagerDelegate::tableCellHighlight(TableView* table, TableViewCell* cell)
	{

	}

	void ManagerDelegate::tableCellWillRecycle(TableView* table, TableViewCell* cell)
	{

	}

	//////////////////////////////////////////////////////////////////////////
	////				TableViewDataSource
	//////////////////////////////////////////////////////////////////////////
	ManagerDataSource::ManagerDataSource()
		: _isInvalid (false)
	{

	}

	ManagerDataSource::~ManagerDataSource()
	{

	}

	TableViewCell* ManagerDataSource::tableCellAtIndex(TableView *table, ssize_t idx)
	{
		ManagerCell* cell = dynamic_cast<ManagerCell*>(table->dequeueCell());

		bool isRefresh = cell;

		if (!cell)
		{
			cell = ManagerCell::create();

			cell->onOpenCallBack = [=](bool isOpen, int index, std::string passWord){
				
				_isOpenVec[index] = isOpen;

				_oldOffset = table->getContentOffset();
				
				if (isOpen)
				{
					_oldOffset.y -= 100;
				}
				else
				{
					_oldOffset.y += 100;
				}

				if (_isOpenVec.size() >= 4 && index == _isOpenVec.size() - 1)
				{
					_oldOffset = Vec2::ZERO;
				}

				// 展开列表的时候获取玩家信息，获取成功之后再reloadData，这样就能把玩家信息显示出来
				if (isOpen)
				{
					if (onGetusersCallBack)
					{
						auto ite = _roomUsers.find(passWord);

						if (ite != _roomUsers.end())
						{
							ite->second.clear();
						}

						onGetusersCallBack(passWord);
					}
				}
				else
				{
					table->reloadData();

					table->setContentOffset(_oldOffset);
				}
			};
		}

		RoomsStruct rooms;
		rooms.isOpen = _isOpenVec[idx];
		rooms.isRefresh = isRefresh;
		rooms.isInvalid = _isInvalid;
		rooms.isGameStart = _roomList[idx].iGameStart;
		rooms.iPlayTime = _roomList[idx].i64BuyTime;
		rooms.szGameName = _roomList[idx].szGameName;
		rooms.szPassWord = _roomList[idx].szDeskPass;

		cell->setIdx(idx);
		cell->buildCell(&rooms);

		// 列表处于展开状态，则显示玩家列表
		if (_isOpenVec[idx])
		{
			auto ite = _roomUsers.find(_roomList[idx].szDeskPass);

			if (ite != _roomUsers.end())
			{
				cell->showUsers(&ite->second);
			}
		}

		cell->onDelCallBack = [=](std::string psd) {
		
			if (onDelCallBack)
			{
				onDelCallBack(psd);
			}
		};

		cell->onDisCallBack = [=](std::string psd) {

			if (onDisCallBack)
			{
				onDisCallBack(psd);
			}
		};
		
		return cell;
	}

	ssize_t ManagerDataSource::numberOfCellsInTableView(TableView *table)
	{	
		return _roomList.size();
	}

	Size ManagerDataSource::tableCellSizeForIndex(TableView *table, ssize_t idx)
	{
		if (_isOpenVec[idx])
		{
			return Size(961, 95 + SPACEING);
		}
		
		return Size(961, 95 + SPACEING);
	}

	void ManagerDataSource::setRoomData(MSG_GP_O_GetBuyDeskList* desk, bool isClear/* = false*/)
	{
		if (isClear)
		{
			_isOpenVec.clear();
			_roomList.clear();
		}
		else
		{
			_isInvalid = (1 == desk->bType);

			_roomList.push_back(*desk);
			_isOpenVec.push_back(false);
		}
	}

	bool ManagerDataSource::isEmpty()
	{
		return _roomList.empty();
	}

	Vec2 ManagerDataSource::getRefreshOffset()
	{
		return _oldOffset;
	}

	// 设置房间玩家
	void ManagerDataSource::setRoomUsers(std::string passWord, MSG_GP_O_GetDeskUser* deskUser, bool isClear/* = false*/)
	{
		auto ite = _roomUsers.find(passWord);

		if (ite == _roomUsers.end())
		{
			std::vector<MSG_GP_O_GetDeskUser> users;
			if (!isClear) users.push_back(*deskUser);

			_roomUsers.insert(std::make_pair(passWord, users));
			//_roomUsers.insert((std::pair<std::string, std::vector<MSG_GP_O_GetDeskUser>>(passWord, users)));
			//_roomUsers.insert(std::map<std::string, std::vector<MSG_GP_O_GetDeskUser>>::value_type(passWord, users));
		}
		else
		{
			if (isClear)
			{
				ite->second.clear();
			}
			else
			{
				ite->second.push_back(*deskUser);
			}
		}
	}
}
