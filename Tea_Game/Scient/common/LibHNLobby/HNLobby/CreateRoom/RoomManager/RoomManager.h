/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_GAME_ROOMMANAGER_H__
#define __HN_GAME_ROOMMANAGER_H__

#include "HNUIExport.h"
#include "HNNetExport.h"
#include "ManagerDelegate.h"

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "extensions/cocos-ext.h"
#include <vector>

USING_NS_CC;
USING_NS_CC_EXT;

using namespace HN;
using namespace ui;
using namespace std;
using namespace extension;

class RoomManager : public HNLayer
{
public:
	typedef std::function<void()> RefreshCallBack;
	RefreshCallBack	onRefreshCallBack;

	typedef std::function<void()> CloseCallBack;
	CloseCallBack	onCloseCallBack;

	~RoomManager();

	virtual bool init()override;

	CREATE_FUNC(RoomManager);

	void close();

	void openManager(Node* parent, Vec2 vec);

private:
	bool loadUI(const char *sFileName);

private:
	// 获取房间列表
	void getRoomList(bool isInvalid = false);

	// 获取玩家信息
	void getUserInfo(std::string deskPass);

private:
	bool getRoomListCallback(HNSocketMessage* socketMessage);

	bool getUserInfoCallback(HNSocketMessage* socketMessage);

	bool dismissCallback(HNSocketMessage* socketMessage);

	bool deleteCallback(HNSocketMessage* socketMessage);

protected:
	TableView*				_tableView	= nullptr;
	ManagerDelegate*		_delegate	= nullptr;
	ManagerDataSource*		_dataSource	= nullptr;
	CC_SYNTHESIZE_PASS_BY_REF(Vec2, _norListOffset, NorListOffset);//普通场结算列表当前滑动位置

protected:
	RoomManager();
	DISALLOW_COPY_AND_ASSIGN(RoomManager);


};

#endif