/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "ManagerCell.h"
#include "Tool/Tools.h"
#include "UMeng/UMengSocial.h"
#include "../../GamePersionalCenter/GameUserHead.h"
#include "../../GameReconnection/Reconnection.h"
#include "../../GameChildLayer/GameShareLayer.h"

namespace HN
{
	static const char *ITEM_CSB			= "platform/RoomManager/Node_item.csb";
	static const char *CHILD_ITEM_CSB	= "platform/RoomManager/Node_childItem.csb";

	static const char* USER_HEAD_MASK	= "platform/common/touxiangdi.png";
	static const char* USER_MEN_HEAD	= "platform/head/men_head.png";
	static const char* USER_WOMEN_HEAD	= "platform/head/women_head.png";

	ManagerCell::ManagerCell()
	{
		
	}

	ManagerCell::~ManagerCell()
	{
	}

	ManagerCell* ManagerCell::create()
	{
		ManagerCell* cell = new ManagerCell();

		if (cell && cell->init())
		{
			cell->autorelease();
			return cell;
		}
		CC_SAFE_DELETE(cell);
		return NULL;
	}

	void ManagerCell::buildCell(RoomsStruct* room)
	{
		_room = *room;

		if (!_room.isRefresh)
		{
			auto rootNode = CSLoader::createNode(ITEM_CSB);
			if (!rootNode) return;

			_layout = dynamic_cast<ui::Layout*>(rootNode->getChildByName("Panel_item"));
			if (!_layout) return;
			_layout->removeFromParentAndCleanup(false);
			addChild(_layout);
		}

		// 展开/收起 重置cell大小
		if (!_room.isOpen)
		{
			setContentSize(Size(961, 95));
		}
		else
		{
			setContentSize(Size(961, 95));
		}
		_layout->setPosition(Vec2(getContentSize().width / 2, getContentSize().height));

		// 列表序号
		auto text_num = dynamic_cast<Text*>(_layout->getChildByName("Text_number"));
		text_num->setString(std::to_string(getIdx() + 1));

		// 游戏名称
		auto text_gameName = dynamic_cast<Text*>(_layout->getChildByName("Text_game"));
		text_gameName->setString(GBKToUtf8(_room.szGameName));

		// 房号
		auto text_roomNum = dynamic_cast<Text*>(_layout->getChildByName("Text_roomNum"));
		text_roomNum->setString(_room.szPassWord);

		time_t Time = _room.iPlayTime;
		struct tm *p = gmtime(&Time);
		p->tm_hour += 8;
		char BeginTime[64];
		strftime(BeginTime, sizeof(BeginTime), "%m/%d %H:%M", p);

		// 时间
		auto text_time = dynamic_cast<Text*>(_layout->getChildByName("Text_time"));
		text_time->setString(StringUtils::format("%s", BeginTime));

		// 列表展开
		auto layout_users = dynamic_cast<Layout*>(_layout->getChildByName("Panel_users"));
		layout_users->setVisible(_room.isOpen);

		auto img_jewels = dynamic_cast<ImageView*>(_layout->getChildByName("Image_jewels"));
		//img_jewels->setVisible(_room.isInvalid);

		auto ck_start = dynamic_cast<CheckBox*>(_layout->getChildByName("CheckBox_start"));
		//ck_start->setSelectedState(_room.isGameStart);
		//ck_start->setVisible(_room.isInvalid);

		// 玩家列表
		_list_user = dynamic_cast<ListView*>(layout_users->getChildByName("ListView_users"));
		_list_user->removeAllItems();
		_list_user->setScrollBarEnabled(false);

		std::string filename = _room.isOpen ? "platform/RoomManager/res/new/shouhui.png" : "platform/RoomManager/res/new/zhankai.png";

		// 展开/收起 按钮
		auto btn_open = dynamic_cast<Button*>(_layout->getChildByName("Button_open"));
		btn_open->addClickEventListener([=](Ref*) {
		
			showCellOpenOrClose();
		});
		btn_open->loadTextures(filename, filename, filename);

		// 解散房间按钮
		auto btn_dis = dynamic_cast<Button*>(_layout->getChildByName("Button_dis"));
		btn_dis->addClickEventListener([=](Ref*) {

			dismissDesk();
		});
		btn_dis->setVisible(!_room.isInvalid);
		btn_dis->setEnabled(!_room.isGameStart);

		// 删除记录按钮
		auto btn_del = dynamic_cast<Button*>(_layout->getChildByName("Button_del"));
		btn_del->addClickEventListener([=](Ref*) {

			deleteDesk();
		});
		btn_del->setVisible(_room.isInvalid);

		// 分享
		auto btn_share = dynamic_cast<Button*>(_layout->getChildByName("Button_share"));
		btn_share->setVisible(!_room.isInvalid);
		btn_share->setEnabled(!_room.isGameStart);
		btn_share->addClickEventListener([=](Ref*){

			auto shareLayer = GameShareLayer::create();
			shareLayer->setName("shareLayer");
			shareLayer->SetInviteInfo(_room.szGameName, _room.szPassWord, false);
			shareLayer->show();
		});
	}

	void ManagerCell::showCellOpenOrClose()
	{
		if (onOpenCallBack) onOpenCallBack(!_room.isOpen, getIdx(), _room.szPassWord);
	}

	// 进入桌子
	void ManagerCell::enterDesk()
	{
		if (_room.isInvalid) return;

		if (_room.isGameStart)
		{
			GamePromptLayer::create()->showPrompt(GBKToUtf8("此房间游戏已经开始，停止加入"));
			return;
		}

		// 检测是否有正在进行的比赛
		Reconnection::getInstance()->getCutRoomInfo(1);
		Reconnection::getInstance()->onCloseCallBack = [this]() {
			auto prompt = GamePromptLayer::create(true);
			prompt->showPrompt(StringUtils::format(GBKToUtf8("是否进入<%s>游戏？\n房号：%s"), GBKToUtf8(_room.szGameName), _room.szPassWord.c_str()));
			prompt->setCallBack([=]() {

				Reconnection::getInstance()->doLoginVipRoom(_room.szPassWord);
			});
		};
	}

	// 解散桌子
	void ManagerCell::dismissDesk()
	{
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(GBKToUtf8("是否解散此房间？"));
		prompt->setCallBack([=]() {

			if (onDisCallBack)
			{
				onDisCallBack(_room.szPassWord);
			}
		});
	}

	// 删除记录
	void ManagerCell::deleteDesk()
	{
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(GBKToUtf8("是否删除此条记录？"));
		prompt->setCallBack([=]() {

			if (onDelCallBack)
			{
				onDelCallBack(_room.szPassWord);
			}
		});
	}

	void ManagerCell::showUsers(std::vector<MSG_GP_O_GetDeskUser>* users)
	{
		for (auto user : *users)
		{
			auto node = CSLoader::createNode(CHILD_ITEM_CSB);
			auto img_user = dynamic_cast<ImageView*>(node->getChildByName("Image_item"));
			img_user->removeFromParentAndCleanup(false);

			auto text_userName = dynamic_cast<Text*>(img_user->getChildByName("Text_name"));
			text_userName->setString(GBKToUtf8(user.szNickName));

			auto img_head = dynamic_cast<ImageView*>(img_user->getChildByName("Image_head"));
			auto img_frame = dynamic_cast<ImageView*>(img_user->getChildByName("Image_frame"));

			auto userHead = GameUserHead::create(img_head, img_frame);
			userHead->show(USER_HEAD_MASK);
			userHead->loadTexture(user.bBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);
			userHead->setHeadByFaceID(user.iLogoID);
			userHead->loadTextureWithUrl(user.szUserHeadUrl);

			_list_user->pushBackCustomItem(img_user);
		}
	}
}