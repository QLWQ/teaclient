/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "GameNotifyCenter.h"
#include "../GameNotice/GameNotice.h"
#include "../GameMatch/GameMatchWaiting.h"
#include "HNData/HNMatchInfoCache.h"

namespace HN
{
	static GameNotifyCenter* notify = nullptr;

	GameNotifyCenter* GameNotifyCenter::getInstance()
	{
		if (nullptr == notify)
		{
			notify = new (std::nothrow) GameNotifyCenter();
		}
		return notify;
	}

	void GameNotifyCenter::destroyInstance()
	{
		CC_SAFE_DELETE(notify);
	}

	GameNotifyCenter::GameNotifyCenter()
	{
	}

	GameNotifyCenter::~GameNotifyCenter()
	{
		PlatformLogic()->removeEventSelector(MDM_GP_MESSAGE, ASS_GP_NEWS_SYSMSG);
		PlatformLogic()->removeEventSelector(MDM_GP_SPEAKER, ASS_GP_SPEAKER_SEND);
		PlatformLogic()->removeEventSelector(MDM_GP_CONTEST, ASS_GP_CONTEST_NOTICE);
		PlatformLogic()->removeEventSelector(MDM_GP_BANK, ASS_GP_BANK_TRANSFER);
	}

	void GameNotifyCenter::startListening()
	{
		// 添加系统消息（广播，公告）监听
		addSysMsgListenr();

		// 添加大喇叭消息监听
		//addSpeakerListener();

		// 添加比赛开赛通知消息监听
		addMatchStartListener();

		// 添加玩家邮件消息监听
		//addEmailListener();

		// 添加玩家赠送礼物消息监听（道具，房卡）
		//addGiftListener();

		// 添加玩家转账消息监听
		//addUserGiroMsgListener();
	}

	void GameNotifyCenter::addSysMsgListenr()
	{
		PlatformLogic()->addEventSelector(MDM_GP_MESSAGE, ASS_GP_NEWS_SYSMSG, [=](HNSocketMessage* socketMessage) {

			/*CHECK_SOCKET_DATA_RETURN(MSG_GR_RS_NormalTalk, socketMessage->objectSize, true,
				"MSG_GR_RS_NormalTalk size of error!");*/

			MSG_GR_RS_NormalTalk *sockInfo = (MSG_GR_RS_NormalTalk*)socketMessage->object;
			std::string message = sockInfo->szMessage;

			GameNotice::getInstance()->postMessage(GBKToUtf8(message.c_str()));

			// 使用消息中心通知出去，其他需要使用的地方监听接受消息
			EventCustom event(SYSMSG_NOTIFICATION);
			event.setUserData(socketMessage);
			Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);

			return true;
		});
	}

	void GameNotifyCenter::addSpeakerListener()
	{
		PlatformLogic()->addEventSelector(MDM_GP_SPEAKER, ASS_GP_SPEAKER_SEND, [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(MSG_GP_S_SPEAKER_SEND_RES, socketMessage->objectSize, true,
				"MSG_GP_S_SPEAKER_SEND_RES size of error!");

			MSG_GP_S_SPEAKER_SEND_RES *sockInfo = (MSG_GP_S_SPEAKER_SEND_RES*)socketMessage->object;

			EventCustom event(SPEAKER_NOTIFICATION);
			event.setUserData(socketMessage);
			Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);

			return true;
		});
	}

	void GameNotifyCenter::addMatchStartListener()
	{
		PlatformLogic()->addEventSelector(MDM_GP_CONTEST, ASS_GP_CONTEST_NOTICE, [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(MSG_GP_ContestNotice, socketMessage->objectSize, true,
				"MSG_GP_ContestNotice size of error!");

			MSG_GP_ContestNotice *notice = (MSG_GP_ContestNotice*)socketMessage->object;

			// 此通知只会给已经报名的人发，如果当前已经处于参赛房间中，则不再弹出提示
			if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				if (RoomLogic()->getSelectedRoom() != nullptr)
				{
					log("matchRoomId:%d, selectRoomId:%d", notice->iRoomID, RoomLogic()->getSelectedRoom()->uRoomID);
					//auto match = HNMatchInfoCache::getInstance()->getSelectMatch();
					if (notice->iRoomID == RoomLogic()->getSelectedRoom()->uRoomID) return true;
				}
			}

			if (!Director::getInstance()->getRunningScene()->getChildByName("prompt"))
			{
				std::string msg;
				time_t Time = notice->beginTime;
				struct tm *p = gmtime(&Time);
				p->tm_hour += 8;
				if (p->tm_hour >= 24)
				{
					p->tm_hour -= 24;
				}
				char BeginTime[64];
				strftime(BeginTime, sizeof(BeginTime), "%H:%M", p);

				auto user = UserInfoModule()->findUser(PlatformLogic()->loginResult.dwUserID);
				int matchID = notice->ContestID;
				if (!user)
				{
					msg = StringUtils::format(GBKToUtf8("您报名的【%s】\n将于%s开赛，\n开赛前也可在大厅报名处进入。"), GBKToUtf8(notice->szRoomName), BeginTime);

					auto prompt = GamePromptLayer::create(true);
					prompt->showPrompt(msg);
					prompt->setName("prompt");
					prompt->setCallBack([=](){
						RoomLogic()->close();
						// 进入备战区
						GameMatchWaiting::createMatchWaiting(matchID);
						//GameMatchWaiting::sendRegistOrRetire(true);
					});
					prompt->runAction(Sequence::create(DelayTime::create(50.0f), CallFunc::create([=](){
						prompt->closeView();
					}), nullptr));
					return true;
				}
				

				// 游戏过程中不能直接切换进备战区
				if (user->bUserState == USER_PLAY_GAME)
				{
					if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
					{
						msg = StringUtils::format(GBKToUtf8("您报名的【%s】\n将于%s开赛，\n请于开赛前解散当前房间从大厅报名处进入。"), GBKToUtf8(notice->szRoomName), BeginTime);
					}
					else
					{
						msg = StringUtils::format(GBKToUtf8("您报名的【%s】\n将于%s开赛，\n请于开赛前结束当局从大厅报名处进入。"), GBKToUtf8(notice->szRoomName), BeginTime);
					}

					auto prompt = GamePromptLayer::create();
					prompt->showPrompt(msg);
					prompt->setName("prompt");
					prompt->runAction(Sequence::create(DelayTime::create(50.0f), CallFunc::create([=](){
						prompt->closeView();
					}), nullptr));
				}
				else
				{
					msg = StringUtils::format(GBKToUtf8("您报名的【%s】\n将于%s开赛，\n开赛前也可在大厅报名处进入。"), GBKToUtf8(notice->szRoomName), BeginTime);

					auto prompt = GamePromptLayer::create(true);
					prompt->showPrompt(msg);
					prompt->setName("prompt");
					prompt->setCallBack([=](){
						RoomLogic()->close();
						// 进入备战区
						GameMatchWaiting::createMatchWaiting(matchID);
						//GameMatchWaiting::sendRegistOrRetire(true);
					});
					prompt->runAction(Sequence::create(DelayTime::create(50.0f), CallFunc::create([=](){
						prompt->closeView();
					}), nullptr));
				}
			}

			return true;
		});
	}

	void GameNotifyCenter::addEmailListener()
	{

	}

	void GameNotifyCenter::addGiftListener()
	{
		/*PlatformLogic()->addEventSelector(MDM_GP_BANK, ASS_GP_BANKTRANSFER_ROOMCARDS, [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(MSG_GP_S_BankTransfer_RoomCards_Result, socketMessage->objectSize, true,
			"MSG_GP_S_BankTransfer_RoomCards_Result size of error!");

			MSG_GP_S_BankTransfer_RoomCards_Result *sockInfo = (MSG_GP_S_BankTransfer_RoomCards_Result*)socketMessage->object;

			__NotificationCenter::getInstance()->postNotification(GIFT_NOTIFICATION, (Ref*)socketMessage);
			return true;
			});*/
	}

	void GameNotifyCenter::addUserGiroMsgListener()
	{
		PlatformLogic()->addEventSelector(MDM_GP_BANK, ASS_GP_BANK_TRANSFER, [=](HNSocketMessage* socketMessage) {

			CHECK_SOCKET_DATA_RETURN(TMSG_GP_BankTransfer, socketMessage->objectSize, true,
				"TMSG_GP_BankTransfer size of error!");

			TMSG_GP_BankTransfer *sockInfo = (TMSG_GP_BankTransfer*)socketMessage->object;

			EventCustom event(GIRO_NOTIFICATION);
			event.setUserData(socketMessage);
			Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);

			return true;
		});
	}
}