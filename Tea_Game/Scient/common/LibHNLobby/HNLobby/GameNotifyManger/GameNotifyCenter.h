/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_GameNotifyCenter_h__
#define __HN_GameNotifyCenter_h__

#include "cocos2d.h"
#include "HNNetExport.h"
#include <vector>
#include <string>

USING_NS_CC;

namespace HN
{
	static const char* SYSMSG_NOTIFICATION	= "sysMsg";		//系统消息消息
	static const char* SPEAKER_NOTIFICATION = "speaker";	//喇叭消息
	static const char* EMAIL_NOTIFICATION	= "email";		//邮件消息
	static const char* GIFT_NOTIFICATION	= "gift";		//礼物消息
	static const char* GIRO_NOTIFICATION	= "giro";		//转账消息

	class GameNotifyCenter
	{
	public:
		// 获取单例
		static GameNotifyCenter* getInstance();

		// 销毁单例
		static void destroyInstance();

	public:
		GameNotifyCenter();

		~GameNotifyCenter();

		// 开始监听
		void startListening();

	private:
		// 添加系统消息（广播，公告）监听
		void addSysMsgListenr();

		// 添加大喇叭消息监听
		void addSpeakerListener();

		// 添加比赛开赛通知消息监听
		void addMatchStartListener();

		// 添加玩家邮件消息监听
		void addEmailListener();

		// 添加玩家赠送礼物消息监听（道具，房卡）
		void addGiftListener();

		// 添加玩家转账消息监听
		void addUserGiroMsgListener();

#define HNGameNotifyCenter() GameNotifyCenter::getInstance()
	};
}


#endif // __HN_GameNotifyCenter_h__