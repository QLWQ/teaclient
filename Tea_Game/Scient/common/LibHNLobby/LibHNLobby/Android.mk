LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../LibHNNet)
$(call import-add-path,$(LOCAL_PATH)/../../LibHNUI)
$(call import-add-path,$(LOCAL_PATH)/../../LibHNMarket)
$(call import-add-path,$(LOCAL_PATH)/../../LibHNOpen)

LOCAL_MODULE := hn_lobby_static

LOCAL_MODULE_FILENAME := libHNLobby

LOCAL_SRC_FILES := ../HNLobby/GameMenu/GameLandLayer.cpp \
                   ../HNLobby/GameMenu/GameMenu.cpp \
				   ../HNLobby/GameMenu/GameFindPsd.cpp \
                   ../HNLobby/GameMenu/GameRegisterLayer.cpp \
				   ../HNLobby/GameUpdate/HNUpdate.cpp \
				   ../HNLobby/GameUpdate/GameResUpdate.cpp \
				   ../HNLobby/GameBaseLayer/GameInitial.cpp \
                   ../HNLobby/GameBaseLayer/GameLists.cpp \
				   ../HNLobby/GameBaseLayer/GamePlatform.cpp \
				   ../HNLobby/GameBaseLayer/LayerManger.cpp \
				   ../HNLobby/GameBaseLayer/GameLading.cpp \
				   ../HNLobby/GameBaseLayer/GameRoom.cpp \
				   ../HNLobby/GameBaseLayer/GameDesk.cpp \
                   ../HNLobby/GameChildLayer/GameExitLayer.cpp \
				   ../HNLobby/GameChildLayer/GameExitChangeLayer.cpp \
                   ../HNLobby/GameChildLayer/GameBankLayer.cpp \
                   ../HNLobby/GameChildLayer/GameRankingList.cpp \
                   ../HNLobby/GameChildLayer/GameSetLayer.cpp \
				   ../HNLobby/GameChildLayer/GameShareLayer.cpp \
				   ../HNLobby/GameChildLayer/ServiceLayer.cpp \
				   ../HNLobby/GameChildLayer/GameSignLayer.cpp \
				   ../HNLobby/GameChildLayer/QR_Encode.cpp\
				   ../HNLobby/GameChildLayer/GameOnlineReward.cpp \
				   ../HNLobby/GameChildLayer/GamePasswordInput.cpp \
				   ../HNLobby/GameChildLayer/TurnLayer.cpp \
                   ../HNLobby/GameChildLayer/GameRules.cpp \
				   ../HNLobby/GameChildLayer/GameAgentApply.cpp \
				   ../HNLobby/GameChildLayer/GameIntegralAct.cpp \
				   ../HNLobby/GameChildLayer/GameTask.cpp \
				   ../HNLobby/GameChildLayer/GameUserPopupInfoLayer.cpp \
				   ../HNLobby/GameNotice/NoticeList.cpp \
				   ../HNLobby/GameNotice/GameNotice.cpp \
				   ../HNLobby/GameNotice/HNNewsHelper.cpp \
				   ../HNLobby/GameNotice/Mail/HNMailData.cpp \
				   ../HNLobby/GameNotice/Mail/HNMailbox.cpp \
				   ../HNLobby/GameNotice/HNNoticeLayer.cpp \
				   ../HNLobby/GamePopularise/GameSpread.cpp \
				   ../HNLobby/GamePopularise/AgentLayer.cpp \
				   ../HNLobby/GamePersionalCenter/BindPhone.cpp \
                   ../HNLobby/GamePersionalCenter/GameUserDataLayer.cpp \
				   ../HNLobby/GamePersionalCenter/GameCertification.cpp \
				   ../HNLobby/GamePersionalCenter/GameUserHead.cpp \
                   ../HNLobby/GamePersionalCenter/ModifyPassword.cpp \
				   ../HNLobby/GameShop/GameOrderList.cpp \
				   ../HNLobby/GameShop/ProductInfo.cpp \
                   ../HNLobby/GameShop/ShopManager.cpp \
				   ../HNLobby/GameShop/GameGiftShop.cpp \
				   ../HNLobby/GameShop/ExchangeDiamonds.cpp \
				   ../HNLobby/GameShop/GameStoreLayer.cpp \
				   ../HNLobby/GameVoice/GameChatLayer.cpp \
				   ../HNLobby/GameVoice/GameVoiceManager.cpp \
				   ../HNLobby/CreateRoom/CreateRoom/CreateRoomLayer.cpp \
				   ../HNLobby/CreateRoom/CreateRoom/JoinRoomLayer.cpp \
				   ../HNLobby/CreateRoom/CreateRoom/GameRecord.cpp \
				   ../HNLobby/CreateRoom/CreateRoom/VipRoomController.cpp \
				   ../HNLobby/CreateRoom/CreateRoom/CreateMatchLayer.cpp \
                   ../HNLobby/CreateRoom/RoomManager/ManagerCell.cpp \
				   ../HNLobby/CreateRoom/RoomManager/ManagerDelegate.cpp \
				   ../HNLobby/CreateRoom/RoomManager/RoomManager.cpp \
				   ../HNLobby/GameLocation/GameLocation.cpp \
				   ../HNLobby/GameReconnection/Reconnection.cpp \
				   ../HNLobby/GameNotifyManger/GameNotifyCenter.cpp \
				   ../HNLobby/GameMatch/GameMatchController.cpp \
				   ../HNLobby/GameMatch/GameMatchWaiting.cpp \
				   ../HNLobby/GameMatch/GameMatchRegistration.cpp \
				   ../HNLobby/GameRecord/RecordList/GameRoomRecord.cpp \
				   ../HNLobby/GameRecord/RecordList/GameTableViewCell.cpp \
				   ../HNLobby/GameRecord/RecordList/GameTableViewDelegate.cpp \
				   ../HNLobby/GameRecord/RecordList/GameRoomRecordClub.cpp \
				   ../HNLobby/GameRecord/RecordList/GameTableViewCellClub.cpp \
				   ../HNLobby/GameRecord/RecordList/GameTableViewDelegateClub.cpp \
				   ../HNLobby/GameRecord/RecordPlay/BackPlay/BackPlayMaskLayer.cpp \
				   ../HNLobby/GameRecord/RecordPlay/RecordParse/FileParser.cpp \
				   ../HNLobby/GameRecord/RecordPlay/RecordParse/RecordCommandQueue.cpp \
				   ../HNLobby/GameRecord/RecordPlay/RecordParse/RecordFacade.cpp \
				   ../HNLobby/GameRecord/RecordPlay/RecordParse/RecordVideoController.cpp \
				   ../HNLobby/GameClub/GameClub.cpp \
				   ../HNLobby/GameClub/ClubMember.cpp \
				   ../HNLobby/GameClub/ClubMessage.cpp \
				   ../HNLobby/GameClub/ClubOperate.cpp \
				   ../HNLobby/GameClub/ClubRoom.cpp \
				   ../HNLobby/GameClub/ClubTips.cpp \
				   ../HNLobby/GameClub/ClubManag.cpp \
				   ../HNLobby/GameClub/ClubUserRedFlow.cpp \
				   ../HNLobby/GameClub/ClubPartner.cpp \
				   ../HNLobby/GameClub/ClubItem.cpp \
				   
				   
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/.. \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameClub \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMenu \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameShop \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMatch \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameVoice \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameNotice \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/CreateRoom \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameRecord \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameLocation \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameBaseLayer \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameChildLayer \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GamePopularise \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameReconnection \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameNotifyManger \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GamePersionalCenter \

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/.. \
                           $(LOCAL_PATH)/../../../../cocos2d \
                           $(LOCAL_PATH)/../../../../cocos2d/cocos \

LOCAL_EXPORT_LDLIBS := -llog \
                        -lz \
                        -landroid
                        
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_net_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_market_static 
LOCAL_WHOLE_STATIC_LIBRARIES += hn_logic_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_open_static 

include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,LibHNMarket)
$(call import-module,LibHNUI)
$(call import-module,LibHNNet)
$(call import-module,LibHNOpen)
