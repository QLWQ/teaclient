#ifndef XMENMJ_Result_h__
#define XMENMJ_Result_h__
#include "XMENMJ_MessageHead.h"

namespace XMENMJ
{
	class MJGameResult:public HNLayer
	{
	private:
		struct CPlayerData
		{
			cocos2d::Node*			_mainNode;
			cocos2d::ui::ImageView*	_headImage;
			cocos2d::ui::Text*		_nameText;
			cocos2d::ui::Text*		_idText;
			cocos2d::Node*			_fangZhuTip;
			cocos2d::ui::Text*		_mingGangText;
			cocos2d::ui::Text*		_anGangText;
			cocos2d::ui::Text*		_zimoText;
			cocos2d::ui::Text*		_dianpaoText;
			cocos2d::ui::Text*		_jiepaoText;
			cocos2d::ui::Text*		_allRoomScoreText;
			cocos2d::Node*			_curMainNode;
			cocos2d::Node*			_curCardNode;
			cocos2d::ui::Text*		_curNameText;
			cocos2d::ui::TextAtlas*	_curZongFenText;;
			cocos2d::ui::TextAtlas*	_curPanshuText;
			cocos2d::Vec2			_curCardNodePos;
			cocos2d::Node*			_xiangxiLayer;
			cocos2d::ui::ImageView*	 _curIcon;
			cocos2d::ui::ImageView*	 _img_title;
			cocos2d::ui::TextAtlas*	_lab_num;
			cocos2d::ui::ImageView*	_img_zhuang;
			float					_curCardScale;
		};
	public:
		MJGameResult();
		~MJGameResult();

		virtual bool init();
		CREATE_FUNC(MJGameResult);
		
		void setShowUserInfo(UserInfoStruct* pUserInfo, bool isfangzhu, int allRoomScore,sitDir dir);
		void setShowBeiLvInfo(UserInfoStruct* pUserInfo, int zimoNum, int dianpaoNum, int jiepaoNum, int mingGangNum, int anGangNum,sitDir dir);
		void setShowCurCardArray(UserInfoStruct* pUserInfo, const char* pCardArray, char cardCount, const char pActArray[][10], char actCount, char hupaiCardId,sitDir dir);
		void setShowCurFen(UserInfoStruct* pUserInfo, bool isZimo, bool isTianhu, bool IsSanjindao, bool isQiangjin, int zongFen, sitDir dir, bool isYoijin[3], int zongpan, char toHuPaiCardId, bool bahuayou,bool isZhuang);
		void setShowCurPan(int jinpaipan, int huapaipan, int kezipan, int bugangpan, int minggangpai, int angangpan, int zipaipengpan, sitDir dir);
		void ShowReSoultTitle(char hutype);

		void StartAction(char cardId, std::function<void()> func);
		void HideAll();
		void onChickjieCallBack(Ref* pSender, Widget::TouchEventType type);
		void onChickMXCallBack(Ref* pSender, Widget::TouchEventType type);
		void showAllResultNode(bool isvisible);
		void setIsKe(bool iske);
	private:   

		void clearPlayerData();
		
	private:
		cocos2d::Node*				m_pZongChengJiNode;
		cocos2d::Node*				m_pCurChengJiNode;
		std::vector<CPlayerData*>	m_PlayerDataVec;
		cocos2d::ui::ImageView*		m_imgShui = nullptr;
		std::vector<Node*>	_resultList;
		Sprite*				_bgSprite;
		bool				_IsShowCard;
		bool				_isGameEnd = false;
		bool				_isKe = false;
		cocos2d::ui::Button*	m_clost_btn = nullptr;
		cocos2d::ui::Button*	m_mingxi_btn = nullptr;
		cocos2d::ui::Button*	m_share_btn = nullptr;
		cocos2d::ui::Button*	m_allcount_btn = nullptr;
	};

}



#endif //XMENMJs_Result_h__



