#ifndef _XMENMJ_CONCEALEDKONG_H_
#define _XMENMJ_CONCEALEDKONG_H_

#include "XMENMJ_poolaction.h"

namespace XMENMJ
{

	class ConcealedKong :
		public PoolAction
	{
	public:
		ConcealedKong(void);
		~ConcealedKong(void);

		virtual void run() override;

		CREATE_FUNC(ConcealedKong);

	};

}
#endif