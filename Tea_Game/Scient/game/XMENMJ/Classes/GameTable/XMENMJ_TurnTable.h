#ifndef XMENMJ_TurnTable_h__
#define XMENMJ_TurnTable_h__

#include "cocos2d.h"
#include "XMENMJ_MessageHead.h"
using namespace cocos2d;

namespace  XMENMJ
{
	class XMENMJ_TurnTable :public Layer
	{
	public:
		XMENMJ_TurnTable();
		~XMENMJ_TurnTable();

		CREATE_FUNC(XMENMJ_TurnTable);

		void turnTableDir(sitDir dir);
		void setTurnInitDir(sitDir dir);
		void setCardCount(int count);
		void setLeftCardVisble(bool isVisble);
		void setAllinit(sitDir dir);
		void showTimeCountAction(bool isVisble,int time);
		void updateTimeCount(float dt);
	private:
		virtual bool init();

	private:
		Node*	_loader;
		Sprite*	_qianHouSiTips;
		Text*	_qianSiHouSiText;
		TextAtlas* _timeCount;
		int _nowTime;
	};
}



#endif // HSMJ_TurnTable_h__
