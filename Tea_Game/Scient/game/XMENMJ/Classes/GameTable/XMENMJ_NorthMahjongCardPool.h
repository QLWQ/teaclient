#ifndef _XMENMJ_NORTHMAHJONGCARDPOOL_H_
#define _XMENMJ_NORTHMAHJONGCARDPOOL_H_

#include "XMENMJ_MahjongCardPool.h"
#include "XMENMJ_NorthHandCard.h"

namespace XMENMJ
{
	class NorthMahjongCardPool :
		public MahjongCardPool
	{
	public:
		NorthMahjongCardPool(void);
		~NorthMahjongCardPool(void);

		CREATE_COUNT(NorthMahjongCardPool);
		virtual bool init(INT count);

		virtual void setHandCardPos(INT catchCard) override;							// ��������
		virtual cocos2d::Vec2 getCatchPos()override;
		virtual int  getZhuaPaiZOrder();
		virtual  void refreshAllShowCard()override;
		virtual Vec2 getOutToDeskPos()override;


		//���ˣ�����
		virtual void showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> allHandCards,INT huCard,bool visibleAllCard) override;
		virtual void showHuTips(std::vector<INT> huTip)override;
		virtual void changeCardToHead(std::vector<INT> cards)override;

		
	};

}

#endif