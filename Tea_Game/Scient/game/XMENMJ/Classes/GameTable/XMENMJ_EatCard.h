#ifndef _XMENMJ_EATACTION_H_
#define _XMENMJ_EATACTION_H_
#include "XMENMJ_poolaction.h"

namespace XMENMJ
{

	class EatCard :
		public PoolAction
	{
	public:
		EatCard(void);
		~EatCard(void);

		virtual void run() override;

		CREATE_FUNC(EatCard);

	};

}
#endif