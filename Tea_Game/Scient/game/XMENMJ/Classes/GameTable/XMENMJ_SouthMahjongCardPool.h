#ifndef _XMENMJ_SOUTHMAHJONGCARDPOOL_H_
#define _XMENMJ_SOUTHMAHJONGCARDPOOL_H_

#include "XMENMJ_MahjongCardPool.h"
#include "XMENMJ_SouthHandCard.h"

namespace XMENMJ
{
	class SouthMahjongCardPool :
		public MahjongCardPool
	{
	public:
		SouthMahjongCardPool(void);
		~SouthMahjongCardPool(void);

		CREATE_COUNT(SouthMahjongCardPool);
		virtual bool init(INT count);

		virtual void setHandCardPos(INT catchCard) override;							// 安置手牌
		virtual cocos2d::Vec2 getCatchPos()override;
		virtual int  getZhuaPaiZOrder();
		virtual  void refreshAllShowCard()override;
		virtual void TouchCard(Card* tcard)override;
		virtual Vec2 getOutToDeskPos()override;


		/*
		* 进入听牌操作
		*
		* @param handcards			手牌
		* @param canOutCard			能够出的牌，出一张后就听
		* @param canKouCards		能扣的牌组合,每张牌代表3张 
		* @param kouGroupArray		能组合的扣牌
		* @return					
		*/
		void enterTingHandle(std::vector<INT>handcards,std::vector<INT>canOutCard,
			std::vector<INT>canKouCards);

		//胡了，倒牌
		virtual void showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> allHandCards,INT huCard,bool visibleAllCard) override;
		virtual void showHuTips(std::vector<INT> huTip)override;

		/*
		*进入交换牌
		*/
		virtual void enterChangeCards() override;

		/*
		*退出交换牌
		*/
		virtual void exitChangeCards(int changeSezi,std::vector<INT>outCard,std::vector<INT> inCard,std::vector<INT> handCardList) override;

		virtual void changeCardToHead(std::vector<INT> cards)override;

	private:
		void sendOutCard(INT cardValue);
		void getOneCard(INT cardValue);
		void ChangeClickCallBack(Ref* ref,Widget::TouchEventType type);

	private:
		Node*				_changeTips;
		
	};

}

#endif