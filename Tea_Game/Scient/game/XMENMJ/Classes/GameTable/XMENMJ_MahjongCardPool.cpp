#include "XMENMJ_MahjongCardPool.h"

namespace XMENMJ
{

	MahjongCardPool::MahjongCardPool(void):
		_huTips(nullptr)
	{
	}


	MahjongCardPool::~MahjongCardPool(void)
	{
	}



	bool MahjongCardPool::init()
	{
		if (! CardPool::init())
		{
			return false;
		}
		return true;
	}


}