#include "XMENMJ_GMLayer.h"
#include "XMENMJ_MessageHead.h"
#include "XMENMJ_MahjongCard.h"

namespace XMENMJ
{
	XMENMJ_GMLayer::XMENMJ_GMLayer()
	{
	}

	XMENMJ_GMLayer::~XMENMJ_GMLayer()
	{
		RoomLogic()->removeEventSelector(MDM_GM_GAME_NOTIFY,GAME_PROTOCOL_S2C_RequestLeftCard);
	}

	
	bool XMENMJ_GMLayer::init()
	{
		if (!HNLayer::init())
		{
			return false;
		}


		//
		Size size = Director::getInstance()->getWinSize();
		m_pMainNode = CSLoader::createNode(COCOS_PATH+"HSZ_gm_layer.csb");
		m_pMainNode->setPosition(cocos2d::Vec2(size.width/2.0f, size.height/2.0f));
		this->addChild(m_pMainNode, 100);

		auto pBgNode = m_pMainNode->getChildByName("bg");
		Button* closeBtn = dynamic_cast<Button*>(m_pMainNode->getChildByName("closeButton"));
		closeBtn->addTouchEventListener(CC_CALLBACK_2(XMENMJ_GMLayer::clickCloseEventCallback, this));

		GameProtoC2SRequestLeftCard_t temp;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY,GAME_PROTOCOL_C2S_RequestLeftCard,&temp,sizeof(temp));
		RoomLogic()->addEventSelector(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_S2C_RequestLeftCard, HN_SOCKET_CALLBACK(XMENMJ_GMLayer::LeftCardCallBack, this));
		
		return true;
	}

	void XMENMJ_GMLayer::setUIData(std::vector<UserInfoStruct> users, std::vector<std::vector<INT>> cards)
	{
		auto pBgNode = m_pMainNode->getChildByName("bg");
		int index =0;
		for (auto v:users)
		{
			
			auto node = m_pMainNode->getChildByName(StringUtils::format("player%d", index));
			auto text = dynamic_cast<ui::Text*>(node->getChildByName("name"));
			text->setString(v.nickName);

		/*	头像
			ImageView* head = dynamic_cast<ImageView*>(node->getChildByName("head"));
			std::string pic = HeadManager::getHeadImage(v.bBoy,v.bLogoID);
			head->loadTexture(pic);

			VipHeadSprite* sprite = VipHeadSprite::create(head,v.dwUserID,v.headUrl,v.iVipTime);
			sprite->setPosition(head->getPosition());
			head->getParent()->addChild(sprite);
*/
			index++;
		}

		int line = 0;
		//牌
		for (auto v: cards)
		{
			int cardIndex = 0;
			for (auto vv: v)
			{
				MahjongCard* pCard = MahjongCard::create(DI_SOUTH_STAND,SOUTH_DIR,vv);
				pCard->setScale(0.5f);
				pCard->setPosition(Vec2(130+43*cardIndex,310-line*100));
				pBgNode->addChild(pCard);
				cardIndex++;
			}

			line++;
		}
	}


	void XMENMJ_GMLayer::clickCloseEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}

		this->removeFromParentAndCleanup(true);

	}

	bool XMENMJ_GMLayer::LeftCardCallBack(HNSocketMessage* socketMessage)
	{
		GameProtoS2CRequestLeftCard_t* card = (GameProtoS2CRequestLeftCard_t*)(socketMessage->object);
		if (socketMessage->objectSize != sizeof(GameProtoS2CRequestLeftCard_t))
		{
			return true;
		}

		auto pBgNode = m_pMainNode->getChildByName("bg");
		//取最多六张
		//int max = MIN(card->leftCount,6);
		const int AllShowCardNum = 9;
		int beginIndex = card->leftCount - AllShowCardNum;
		int endIndex = card->leftCount;
		if(beginIndex<0) beginIndex = 0;
		for (int i=beginIndex; i<endIndex; ++i)
		{
			MahjongCard* pCard = MahjongCard::create(DI_SOUTH_STAND,SOUTH_DIR,card->leftCardArray[i]);
			pCard->setScale(0.5f);
			pCard->setPosition(Vec2(130+50*i,40));
			pBgNode->addChild(pCard);
		}

		return true;
	}

}

