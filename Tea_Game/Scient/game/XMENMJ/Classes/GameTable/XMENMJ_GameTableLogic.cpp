﻿#include "XMENMJ_GameTableLogic.h"
#include "XMENMJ_GameTableUI.h"
#include "XMENMJ_GameTableUICallback.h"
#include "XMENMJ_GameManager.h"
#include "XMENMJ_Card.h"
#include "tinyxml2/tinyxml2.h"
#include "XMENMJ_Protocol.h"
#include "XMENMJ_Result.h"

namespace XMENMJ
{

using namespace cocostudio;
using namespace std;
using namespace ui;
using namespace HN;

	GameTableLogic* GameTableLogic::_instance = nullptr;

	GameTableLogic::GameTableLogic(GameTableUICallBack* uiCallback,BYTE deskNo,bool bAutoCreate) 
		: HNGameLogicBase(deskNo, PLAY_COUNT, bAutoCreate, uiCallback)
	{
		_callBack = (GameTableUI *)uiCallback;
		_instance = this;
		_hasSetGameStation = false;
		_userInfo = RoomLogic()->loginResult.pUserInfoStruct;

	}

	GameTableLogic::~GameTableLogic()
	{
	}
	/*-----------------------------------------------------------------------------------------------*/

	void GameTableLogic::dealGameStartResp(INT bDeskNO)
	{
		_callBack->showWechat(false);
		_callBack->showBackBtn(false);
	}

	void GameTableLogic::dealGameEndResp(INT bDeskNO)
	{
		
	}

	void GameTableLogic::dealUserAgreeResp(MSG_GR_R_UserAgree* agree)
	{
		onUserAgree(*((MSG_GR_R_UserAgree*)agree));
	}


	//处理玩家坐下
	void GameTableLogic::dealUserSitResp(MSG_GR_R_UserSit * userSit,  const UserInfoStruct* user)
	{
		HNGameLogicBase::dealUserSitResp(userSit, user);
		auto deskUser = getUserByUserID(user->dwUserID);
		bool isMe = (user->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
		if (isMe)
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				_callBack->removeUser(getUserDir(i));
			}

			loadUsers();
		}
		else
		{
			BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
			_callBack->addUser(getUserDir(userSit->bDeskStation), deskUser);
			// 显示房主标志
			if (userSit->bDeskMaster)
			{
				//GameManager::getInstance()->setFangzhu(seatNo);
			}
		}

	}

	// 用户站起
	void GameTableLogic::dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		if (!_hasSetGameStation)
		{
			return;
		}
		// 清除庄家显示
		for (auto i = 0; i < PLAY_COUNT; i++)
		{
			COCOS_NODE(Sprite, StringUtils::format("zhuang%d", i))->setVisible(false);          // 庄家
		}
		// 清除本地界面的ui显示
		if (userSit->dwUserID == _userInfo.dwUserID)
		{
			

			_callBack->dealLeaveDesk();
		}
		else
		{
			if (userSit->bDeskStation >= 0 && userSit->bDeskStation <= 3)
				_callBack->removeUser(getUserDir(userSit->bDeskStation));
		}
		
	}


	// 游戏状态
	void GameTableLogic::dealGameStationResp(void* object, INT objectSize)
	{
		setGameStation(object, objectSize);
	}

	//设置回放模式
	void GameTableLogic::dealGamePlayBack(bool isback)
	{
		//GameManager::getInstance()->setPlayBack(isback);
	}

	//房主消息
	void GameTableLogic::dealDeskOwnertResp(void* object, INT objectSize)
	{
		/*HNGameLogicBase::dealDeskOwnertResp(object,objectSize);
		_callBack->setDissloveBtState();
		std::vector<int> defines = AnalysisUserDefine(_userDefined);
		GameManager::getInstance()->setUserDefine(defines);*/
		

	}

	// 游戏消息（游戏的主体消息来自这里）
	void GameTableLogic::dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		int test = _mySeatNo;
		if (_hasSetGameStation == false)
		{
			return;
		}
		switch(messageHead->bAssistantID)
		{
		case GAME_PROTOCOL_S2C_GameBegin:
			{
				GameManager::getInstance()->playCommonSound("Begin");
				dealGameBegin((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_RandomDice:
			{
				GameProtoS2CRandomDice_t* pProtData = (GameProtoS2CRandomDice_t*)object;
				auto dir = getUserDir(pProtData->bankerNo);
				auto catchDir = getUserDir((pProtData->bankerNo + pProtData->dingZhuangSezi)%PLAY_COUNT);
				GameManager::getInstance()->resetBeginStageUI(pProtData->bankerNo,dir,catchDir,pProtData->dingZhuangSezi,pProtData->bankcount);
			}
			break;
		case GAME_PROTOCOL_S2C_SendCard:
			{
				dealOnSendAllCardEx((char*)object, objectSize);
			}
			break;
		case  GAME_PROTOCOL_S2C_SHOWFLOWER:
			{
				
				dealShowCardFlowerEx((char*)object, objectSize);
			}
			break;
		case  GAME_PROTOCOL_S2C_SHOWFLOWER_FINSHG:
			{

				dealShowCardFlowerFinshEx((char*)object, objectSize);
			}
			break;
		
		case  GAME_PROTOCOL_S2C_Send_GoldData:
			{

				dealShowGoldCardEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_ChangeCardBegin:
			{
				GameProtoS2CChangeCardTime* pProtData = (GameProtoS2CChangeCardTime*)object;
				CCLOG("AAAAAAAAAAAAAA");
				GameManager::getInstance()->changeCardBegin();
			}
			break;
		case GAME_PROTOCOL_S2C_ChangeCardFinish:
			{
				CCAssert(sizeof(GameProtoS2CChangeCardFinish) == objectSize, "Broken message GameProtoS2CChangeCardFinish.");
				GameProtoS2CChangeCardFinish* pProtData = (GameProtoS2CChangeCardFinish*)object;
				CCLOG("AAAAAAAAAAAAAA");
				GameManager::getInstance()->changeCardEnd(pProtData);
			}
			break;
		case GAME_PROTOCOL_S2C_ChangeCard:
			{
				CCAssert(sizeof(GameProtoS2CChangeCard) == objectSize, "Broken message GameProtoS2CChangeCard.");

				GameProtoS2CChangeCard* pProtData = (GameProtoS2CChangeCard*)object;
				auto dir = getUserDir(pProtData->seatNo);
				GameManager::getInstance()->changeCardToHead(dir,pProtData->cards,3);
				CCLOG("AAAAAAAAAAAAAA");	
			}
			break;
		case GAME_PROTOCOL_S2C_BeginOut:
			{
				GameManager::getInstance()->playGame();
				dealGameBegin((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_OutCard:
			{
				dealOnOutCardEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_DispatchCard:
			{
				dealOnZhuaCardEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_ActionAppear:
			{
				dealOnActionAppearEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_ActionEat:
			{
				dealOnActionEatEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_Hu:
			{
				dealOnPlayerHu((char*)object,objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_GameFinish:
			{
				dealOnGameFinish((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_TingPai:
			{
				dealOnTingPaiEx((char*)object, objectSize);
			}break;
		case GAME_PROTOCOL_S2C_FlowZhuang:
			{
				dealOnFlowZhuang((char*)object, objectSize);
			}break;
		case S_C_GAME_RECORD_RESULT:
			{
			CHECK_SOCKET_DATA(S_C_GameRecordResult, sizeof(S_C_GameRecordResult), "S_C_GameRecordResult size error!");
			S_C_GameRecordResult* pSuperUser = (S_C_GameRecordResult*)(object);
			_callBack->sendRecordData(pSuperUser);
			}break;
		case GAME_PROTOCOL_S2C_TuoGuan:
			{
				dealAuto((char*)object, objectSize);
			}
		break;
		}

	}

	void GameTableLogic::dealGameDeskNotFound(void* object, INT objectSize)
	{
		_callBack->showGameDeskNotFound();
	}

	void GameTableLogic::dealUserCutMessageResp(INT userId, BYTE seatNo)
	{
		GameManager::getInstance()->userOffLine(getUserDir(seatNo));
	}

	void GameTableLogic::dealOnFlowZhuang(char* object,int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CFlowZhuang_t) == objectSize, "Broken message GameProtoS2CFlowZhuang_t.");
		GameProtoS2CFlowZhuang_t* pProtData = (GameProtoS2CFlowZhuang_t*)object;
		//播放分饼动画
		GameManager::getInstance()->SHowFengbinAnimation();
	}

	void GameTableLogic::setGameStation(void* pBuffer,int nLen)
	{
		
		GameStatusHead_t* pHead = (GameStatusHead_t*)pBuffer;
	
	
		if (pHead->bIsSuperUser)
		{
			GameManager::getInstance()->showGMButton();
		}

		GameManager::getInstance()->resetInit();
		GameManager::getInstance()->updateActionScore(pHead->scoreArray);
		
		
		switch (pHead->status)
		{
		case GameStatus::Free:
		case GameStatus::Agree:      // 等候玩家同意
			{
				GameStatusAgree_t* pProtData = (GameStatusAgree_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				waitAgree();
			}
			break;
		case GameStatus::GameBegin:
			{
				CCAssert(sizeof(GameStatusGameBegin_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusGameBegin_t.");
				GameStatusGameBegin_t* pProtData = (GameStatusGameBegin_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				GameManager::getInstance()->playCommonSound("Begin");

				auto dir = getUserDir(pProtData->bankerNo);
				auto catchDir = getUserDir((pProtData->bankerNo + pProtData->dingZhuangSezi)%PLAY_COUNT);
				GameManager::getInstance()->resetBeginStageUI(pProtData->bankerNo,dir,catchDir,pProtData->dingZhuangSezi,pProtData->bankcount);
			}
			break;
		case GameStatus::ChangeCard:
			{
				CCAssert(sizeof(GameStatusChangeCard_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusChangeCard_t.");
				GameStatusChangeCard_t* pProtData = (GameStatusChangeCard_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				auto dir = getUserDir(pProtData->bankNo);
				//GameManager::getInstance()->resetChangeCardStageUI(dir,pProtData);
			}
			break;
		case GameStatus::SendCard:
			{
				CCAssert(sizeof(GameStatusSendCard_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusSendCard_t.");
				GameStatusSendCard_t* pProtData = (GameStatusSendCard_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				auto dir = getUserDir(pProtData->bankNo);
				GameManager::getInstance()->resetSendCardStageUI(dir,pProtData);
				//_isGamePlaying = true;
			}
			break;
		case GameStatus::DispatchCard:
		case GameStatus::OutCard:
			{
				CCAssert(sizeof(GameStatusOutCard_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusOutCard_t.");
				GameStatusOutCard_t* pProtData = (GameStatusOutCard_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				auto dir = getUserDir(pProtData->bankerSeatNo);
				GameManager::getInstance()->resetOutCatchCardStageUI(dir,pProtData,pHead->status==GameStatus::OutCard);
				for (int i = 0; i < PLAY_COUNT; i++)
				{
					auto dir = getUserDir(i);
					GameManager::getInstance()->setAutoImgeVisibal(dir, pProtData->bTuoGuanArray[i]);
					if (i == _mySeatNo)
					{
						GameManager::getInstance()->setTingLayer(pProtData->tingHuCardArray[i], pProtData->tingHuCountArray[i]);
					}			
				}
				GameManager::getInstance()->showTimeCountByAction(true, 10);
				//_isGamePlaying = true;
			}
			break;
		case GameStatus::GameFinish:
			{
				CCAssert(sizeof(GameStatusGameFinish_t) + sizeof(GameStatusHead_t)== nLen, "Broken message GameStatusGameFinish_t.");
				GameStatusGameFinish_t* pProtData = (GameStatusGameFinish_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				dealOnGameFinish((char*)pProtData, sizeof(GameStatusGameFinish_t));
				//_isGamePlaying = false;
			}
			break;
		}

		//_callBack->showWechat();
		//_callBack->showBackBtn(!_isGamePlaying);

		if (/*!_isGamePlaying */ _hasSetGameStation == false)
		{
			_callBack->checkIpSameTips();
		}

		_hasSetGameStation = true;
	}

	

	void GameTableLogic::onUserAgree(const MSG_GR_R_UserAgree& msg)
	{
		log("\n onUserAgree   : ");
		log("bAgreeGame  %d   ", msg.bAgreeGame);
		log("bDeskNO  %d   ", msg.bDeskNO);
		log("bDeskStation  %d   ", msg.bDeskStation);
		_callBack->agreeGame(getUserDir(msg.bDeskStation));
	}

	
	void GameTableLogic::dealOnSendAllCardEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CSendCard_t) == datasize, "Broken message GameProtoS2CSendCard_t.");
		GameProtoS2CSendCard_t* pProtData = (GameProtoS2CSendCard_t*)pData;
		
		std::vector<std::vector<INT>> vv;
		auto playerIndex = _userInfo.bDeskStation;
		std::vector<INT> v;

		// 生成手牌
		for (int i=playerIndex; i<playerIndex+PLAY_COUNT; i++)
		{
			int toindex = i % PLAY_COUNT;
			for (auto j = 0; j < pProtData->countArray[toindex]; j++)
			{
				v.push_back(pProtData->cardArray[toindex][j]);
			}
			vv.push_back(v);
			v.clear();
		}
		GameManager::getInstance()->startGame();
		GameManager::getInstance()->initAllHandCard(vv);
	}

	//处理玩家补花
	void GameTableLogic::dealShowCardFlowerEx(char* pData, int datasize)
	{
		CCAssert(sizeof(CMD_S_SHOW_FLOWER) == datasize, "Broken message GameProtoS2CSendCard_t.");
		CMD_S_SHOW_FLOWER* pProtData = (CMD_S_SHOW_FLOWER*)pData;
		auto dir = getUserDir(pProtData->wChairID);
		
		for (int  i = 0; i < pProtData->cbCardCount; i++)
		{

			bool isMan = GameManager::getInstance()->isMan(pProtData->wChairID);
			GameManager::getInstance()->showBuhuaTip(true);
			GameManager::getInstance()->showBuhuaCard(dir, pProtData->cbCardData[i]);
			PoolAction::playSexNumberSound(isMan, 98);
			//_callBack->outCard(dir, pProtData->cbCardData[i]);  // 出牌

			std::vector<INT> handCards;
			for (auto j = 0; j < pProtData->countArray[pProtData->wChairID]; j++)
			{
				handCards.push_back(pProtData->cardArray[pProtData->wChairID][j]);
			}

			GameManager::getInstance()->refreshHandCardValue(dir, handCards);//刷新手牌
			GameManager::getInstance()->buhuaCard(dir, handCards.at(0));
		}
		//GameManager::getInstance()->setCurrOperDir(turnDir);
	}



	void  GameTableLogic::dealGameBegin(char*pData, int datasize)
	{
	
		CCAssert(sizeof(GameProtoS2CBeginOut_t) == datasize, "Broken message GameProtoS2CSendCard_t.");
		GameProtoS2CBeginOut_t* pProtData = (GameProtoS2CBeginOut_t*)pData;
		auto turnDir = getUserDir(pProtData->wChairID);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->playAnimation();
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			auto dir = getUserDir(i);
			GameManager::getInstance()->setAutoImgeVisibal(dir, false);
		}
	}

	//补花完成
	void GameTableLogic::dealShowCardFlowerFinshEx(char* pData, int datasize)
	{
		CCAssert(sizeof(CMD_S_SHOW_FLOWER_FINSH) == datasize, "Broken message GameProtoS2CSendCard_t.");
		CMD_S_SHOW_FLOWER_FINSH* pProtData = (CMD_S_SHOW_FLOWER_FINSH*)pData;
		auto turnDir = getUserDir(pProtData->wChairID);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->showBuhuaTip(false);
	}

	//用户翻金
	void GameTableLogic::dealShowGoldCardEx(char* pData, int datasize)
	{
		CCAssert(sizeof(CMD_S_CARD_DATA) == datasize, "Broken message GameProtoS2CSendCard_t.");
		CMD_S_CARD_DATA* pProtData = (CMD_S_CARD_DATA*)pData;
		auto turnDir = getUserDir(pProtData->wCurrentUser);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		if (pProtData->cbCardData >= CMjEnum::MJ_TYPE_FCHUN)
		{
			return;
		}
		GameManager::getInstance()->SetGoldCard(pProtData->cbCardData);
		GameManager::getInstance()->playCommonSound("gold");
		//播放金币旋转动画
		GameManager::getInstance()->SHowGoldCardAnimation();
	
	}


	void GameTableLogic::dealOnOutCardEx(char* pData, int datasize)
	{
		log("dealOnNotifyOutCardResp :");
		CCAssert(sizeof(GameProtoS2COutCard_t) == datasize, "Broken message GameProtoS2COutCard_t.");

		GameProtoS2COutCard_t* pProtData = (GameProtoS2COutCard_t*)pData;
		bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);
		PoolAction::playSexNumberSound(isMan, pProtData->outCardId);
		auto dir = getUserDir(pProtData->seatNo);
		GameManager::getInstance()->setAutoBtnEnable(true);
		_callBack->outCard(dir, pProtData->outCardId);  // 出牌
		GameManager::getInstance()->showTimeCountByAction(true,10);
		std::vector<INT> handCards;
		for (auto j = 0; j < pProtData->countArray[pProtData->seatNo]; j++)
		{
			handCards.push_back(pProtData->cardArray[pProtData->seatNo][j]);
		}


		GameManager::getInstance()->refreshHandCardValue(dir,handCards);//刷新手牌

		if (pProtData->seatNo == _mySeatNo)
		{
			GameManager::getInstance()->setTingLayer(pProtData->tingHuCardArray[_mySeatNo], pProtData->tingHuCountArray[_mySeatNo]);
		}

		if (pProtData->bAction)
		{
			GameManager::getInstance()->setCurrOperDir(sitDir(-1));
		}
		
	}

	void GameTableLogic::dealOnZhuaCardEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CDispatchCard_t) == datasize, "Broken message GameProtoS2CDispatchCard_t.");
		GameProtoS2CDispatchCard_t* pProtData = (GameProtoS2CDispatchCard_t*)pData;
		GameManager::getInstance()->playCommonSound("zhuapai");

		auto dir = getUserDir(pProtData->seatNo);                // 取方向
		_callBack->catchCard(dir, pProtData->dispCardId);                // 抓牌
		if (_isHaveAction)
		{
			_isHaveAction = false;
			GameManager::getInstance()->showTimeCountByAction(false, 0);
			GameManager::getInstance()->showTimeCountByAction(true,10);
		}
		
	}

	void GameTableLogic::dealOnActionAppearEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CActionAppear_t) == datasize, "Broken message GameProtoS2CActionAppear_t.");
		GameProtoS2CActionAppear_t* pProtData = (GameProtoS2CActionAppear_t*)pData;
		GameManager::getInstance()->playCommonSound("Block");
		GameManager::getInstance()->showTimeCountByAction(true,10);
		_isHaveAction = true;
		auto index = _userInfo.bDeskStation;
		if (pProtData->seatNo==index)
		{
			log("dealOnNotifyBlockResp \n");
			GameManager::getInstance()->showMeAction(pProtData->isHavePeng,pProtData->isHaveChi, pProtData->isHaveGang, pProtData->isHaveHu, pProtData->isHaveTing,pProtData->isHaveQiang,pProtData->isHaveDao,pProtData
				->isHaveYou,pProtData->ishaveTianhu,pProtData->ishavezimo,pProtData->nyoujingcount);
			GameManager::getInstance()->setIsHasAction(true);
			GameManager::getInstance()->setIsHasChiAction(pProtData->isHaveChi);
			if (pProtData->isHaveChi)
			{
				std::vector<std::vector<INT>> chiCardList(pProtData->actCount);
				for (int i = 0; i < pProtData->actCount; i++)
				{
					for (int j = 2; j < 5; j++)
					{
						chiCardList.at(i).push_back(pProtData->actCardArray[i][j]);
					}
				}
				GameManager::getInstance()->_chiPaiCard= pProtData->toCardId;
				if (chiCardList.size()>0)
				{
					GameManager::getInstance()->seChiPaiList(chiCardList);
				}

			}

		}
		
	}

	void GameTableLogic::dealOnTingPaiEx(char* pData, int datasize)
	{
		GameProtoS2CTingPai_t* pProtData = (GameProtoS2CTingPai_t*)pData;

		auto dir = getUserDir(pProtData->seatNo);
		std::vector<INT> handCards;
		for (int i=0;i<pProtData->cardCount;++i)
		{
			handCards.push_back(pProtData->cardArray[i]);
		}

		std::vector<INT> canOutCards;
		for (int i=0;i<pProtData->canOutCount;++i)
		{
			canOutCards.push_back(pProtData->canOutArray[i]);
		}

		std::vector<INT> canKouCards;

		GameManager::getInstance()->showTimeCountByAction(true, 10);
		if (dir==sitDir::SOUTH_DIR)
		{
			//处理听牌，提示玩家可以出的牌
			GameManager::getInstance()->enterTingHandle(handCards,canOutCards,canKouCards);
		}	
		INT index=INT(dir);
		GameManager::getInstance()->setUserTingPaiState(true,index);//听牌后明牌
		bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);	
		PoolAction::playSexActionSound(isMan,"ting");
		
	}

	void GameTableLogic::dealOnActionEatEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CActionEat_t) == datasize, "Broken message GameProtoS2CActionEat_t.");
		GameProtoS2CActionEat_t* pProtData = (GameProtoS2CActionEat_t*)pData;
		auto userDir = getUserDir(pProtData->seatNo);
		int cardId = pProtData->actionArray[1];
		if (pProtData->actionType == 1)
		{
			cardId = GameManager::getInstance()->getLastOutCard()->getCardSumNumber();
		}
		bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);

		sitDir turnDir = getUserDir(pProtData->turnSeatNo);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->turnTableDir(turnDir);

		std::vector<INT> handCard;
		for (int i=0;i<pProtData->cardCount;++i)
		{
			handCard.push_back(pProtData->cardArray[i]);
		}

		char chiCardArray[3] = { 0 };

		for (int i = 0; i < CHICARDCOUNT; i++)
		{
			chiCardArray[i] = pProtData->actionArray[i + 1];
		}
		
		switch (pProtData->actionType)
		{
		case 1: /* 吃 */
			{
				GameManager::getInstance()->playThingAction(pProtData->actionType, userDir, cardId, isMan, chiCardArray);

				GameManager::getInstance()->refreshHandCardValue(userDir, handCard);

			}
			break;
		case 2: /* 碰 */
			{
			GameManager::getInstance()->playThingAction(pProtData->actionType, userDir, cardId, isMan, chiCardArray);
				GameManager::getInstance()->refreshHandCardValue(userDir,handCard);
			}
			break;
		case 3: /* 明杠 */
		case 4: /* 暗杠 */
		case 5: /* 枪杠 */
			{
				
			GameManager::getInstance()->playThingAction(pProtData->actionType, userDir, cardId, isMan, chiCardArray);
				GameManager::getInstance()->refreshHandCardValue(userDir,handCard);
			
				for (int i = 0; i < PLAY_COUNT; i++)
				{
					auto dir = getUserDir(i);
					GameManager::getInstance()->showScoreAnimation(dir, pProtData->scoreArray[i]);
				}

				
			}
			break;
		}
	}


	void GameTableLogic::dealOnGameFinish(char* pData, int datasize)
	{
		
		int size = sizeof(GameProtoS2CGameFinish_t);
		CCAssert(sizeof(GameProtoS2CGameFinish_t) == datasize, "Broken message GameProtoS2CGameFinish_t.");
		GameProtoS2CGameFinish_t* pProtData = (GameProtoS2CGameFinish_t*)pData;
		MJGameResult* pResultUI = (MJGameResult*)_callBack->createGameFinishNode();
		GameManager::getInstance()->updateActionScore(pProtData->alltotalScoreArray);
		GameManager::getInstance()->showTimeCountByAction(false,0);
		UserInfoStruct** userInfos = GameManager::getInstance()->getVecUser();
		for (int i =0;i<PLAY_COUNT;++i)
		{
			UserInfoStruct* pUser = *(userInfos+i);
			if (pUser == nullptr)
			{
				continue;
			}

			int seatNo = pUser->bDeskStation;
			if(seatNo<0 || seatNo>=PLAY_COUNT) continue;
			int mGangNum = 0;
			int aGangNum = 0;
			for (int jj=0; jj<pProtData->actCountArray[seatNo]; jj++)
			{
				int actType = pProtData->actCardArray[seatNo][jj][0];
				if (actType == 3 || actType==5) mGangNum++;
				if(actType==4)aGangNum++;
			}

			int allRoomScore = 0;
			for (int jj=0;jj<PLAY_COUNT;++jj)
			{
				if (pProtData->allPlayerUser[jj] == pUser->dwUserID)
				{
					allRoomScore = pProtData->allRoomScoreArray[jj];
				}
			}
		
			for (int k=0; k<PLAY_COUNT; k++)
			{
				if (pProtData->huTypeArray[k]!=0)
				{
					pResultUI->ShowReSoultTitle(pProtData->huTypeArray[k]);
				}
				if (pProtData->isliuju)
				{
					pResultUI->ShowReSoultTitle(0);
				}



				if (seatNo!=k) continue;
				sitDir dir = getUserDir(seatNo);
				GameManager::getInstance()->refreshUserMoney(dir,*pUser);
				char toHuPaiCardId = pProtData->huCardId[seatNo];
				if(!pProtData->isHu[seatNo]) 
				{
					toHuPaiCardId = -1;
				}
				bool IsZHuang = false;
				if (k == pProtData->bankUser)
				{
					IsZHuang = true;
				}
				pResultUI->setShowUserInfo(pUser, pUser->dwUserID, allRoomScore,dir);
				pResultUI->setShowBeiLvInfo(pUser, (int)pProtData->allZimoArray[seatNo], pProtData->allDianpaoArray[seatNo],
					pProtData->allJiepaoArray[seatNo], pProtData->allMinggangArray[seatNo], pProtData->allAngangArray[seatNo],dir);
				pResultUI->setShowCurCardArray(pUser, pProtData->handCardArray[k], pProtData->handCountArray[k], 
					pProtData->actCardArray[k], pProtData->actCountArray[k], toHuPaiCardId,dir);
				pResultUI->setShowCurFen(pUser, pProtData->isZiMo[k], pProtData->istianhu[k], pProtData->issanjindao[k], pProtData->isqiangjing[k], pProtData->totalScoreArray[k], dir, pProtData->isyoujin[k], pProtData->Allpan[k], toHuPaiCardId, pProtData->isbahuayou[k], IsZHuang);
				pResultUI->setShowCurPan(pProtData->jinPan[k], pProtData->huaPan[k], pProtData->keziPan[k], pProtData->bugangPan[k], pProtData->minggangPan[k], pProtData->angangPan[k], pProtData->ziPaipengPan[k], dir);
			}
		}

		pResultUI->StartAction(-1, nullptr);	
		//倒牌
		for (int i=0;i<PLAY_COUNT;i++)
		{
			std::vector<CardPool::CGroupCardData> groupCards;
			sitDir dir = GTLogic()->getUserDir(i);
			for (int j=0;j<pProtData->actCountArray[i];j++)
			{
				CardPool::CGroupCardData cardData;
				int count = pProtData->actCardArray[i][j][2];
				std::vector<INT> cards;
				for (int z=3;z<3+count;z++)
				{
					cards.push_back(pProtData->actCardArray[i][j][z]);
				}
				switch (pProtData->actCardArray[i][j][0])
				{
				case ChiPai:
					break;
				case PengPai:
					cardData._iCardId = cards.at(0);
					cardData._iCount  = cards.size();
					cardData._iType   = CardPool::CGroupCard_Peng;
					groupCards.push_back(cardData);

					break;
				case MingGang:
				case QiangGang:
					cardData._iCardId = cards.at(0);
					cardData._iCount  = cards.size();
					cardData._iType   = CardPool::CGroupCard_MingGang;
					groupCards.push_back(cardData);

					break;
				case AnGang:
					cardData._iCardId = cards.at(0);
					cardData._iCount  = cards.size();
					cardData._iType   = CardPool::CGroupCard_AnGang;
					groupCards.push_back(cardData);

					break;
				default:
					break;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			std::vector<INT> handCards;
			for (int j=0;j<pProtData->handCountArray[i];j++)
			{
				handCards.push_back(pProtData->handCardArray[i][j]);
			}

			GameManager::getInstance()->showHandCardHu(dir,groupCards,handCards,pProtData->isZiMo[i],pProtData->huCardId[i],true,pProtData->istianhu[i],pProtData->isqiangjing[i],pProtData->issanjindao[i],pProtData->isyoujin[i],pProtData->isbahuayou[i]);
		}

		//
		GameManager::getInstance()->resetDiKuang();
		COCOS_NODE(Button, "start")->setVisible(true);

	}

	void GameTableLogic::dealOnPlayerHu(char* pData,int datasize)
	{
		CCAssert(sizeof(GameProtoS2CHu) == datasize, "Broken message GameProtoS2CHu.");
		GameProtoS2CHu* pProtData = (GameProtoS2CHu*)pData;

		sitDir turnDir = getUserDir(pProtData->turnSeatNo);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->turnTableDir(turnDir);

		std::vector<INT> handCards;
		for (int j=0;j<pProtData->cardCount;j++)
		{
			handCards.push_back(pProtData->cardArray[j]);
		}


		PoolAction * action = nullptr;
	
		
		//播放一下声音
		bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);
		bool is = true;
		for (int i = 0; i < 3; i++)
		{
			
			if (pProtData->isyoujin[i])
			{
				std::string str = StringUtils::format("you%d", i + 1);
				PoolAction::playSexActionSound(isMan, str);
				is = false;
			}
		}
		if (is)
		{
			PoolAction::playSexActionSound(isMan, "hu");
		}
		//显示手牌
		std::vector<CardPool::CGroupCardData> groupCards = finCardGroup(pProtData->actionCard,pProtData->actionCount,pProtData->seatNo);
		sitDir dir = getUserDir(pProtData->seatNo);
		GameManager::getInstance()->showHandCardHu(dir,groupCards,handCards,pProtData->isZimo,pProtData->huCard,true,pProtData->istianhu,pProtData->isqiangjing,pProtData->issanjindao,pProtData->isyoujin,pProtData->isBahuayou);


		//播放胡的动画

		GameManager::getInstance()->ShowHu(dir, pProtData->huCard);

		//播放胡的闪电动画

		GameManager::getInstance()->ShowShanDian(dir);
	}

	/*****************************************************************************************/
	void GameTableLogic::waitAgree()
	{
	
		_userInfo = RoomLogic()->loginResult.pUserInfoStruct;
		// 显示玩家
		vector<UserInfoStruct *> vec;
		UserInfoModule()->findDeskUsers(_userInfo.bDeskNO, vec);
		for (auto &v : vec)
		{
			_callBack->addUser(getUserDir(v->bDeskStation), v);
		}
	}


	sitDir GameTableLogic::getUserDir(const INT& deskStation)
	{
		//char text[100] ={0};
		if (deskStation == -1)
		{
			return sitDir(-1);
		}
		// sprintf(text,"getUserDir--%d",_userInfo.bDeskStation);
		CCLOG("getUserDir--%c",_userInfo.bDeskStation);
		return  sitDir((deskStation - _userInfo.bDeskStation+PLAY_COUNT)%PLAY_COUNT);
		
	}

	INT GameTableLogic::getUserStation(const sitDir& dir)
	{
		return (INT(dir) - INT(sitDir::SOUTH_DIR) + _userInfo.bDeskStation)%PLAY_COUNT;
	}

	std::vector<CardPool::CGroupCardData> GameTableLogic::finCardGroup(char actionCard[ACTION_MAX_COUNT][10],INT actionCount,INT station)
	{
		std::vector<CardPool::CGroupCardData> groupCards;
		sitDir dir = getUserDir(station);

		for (int j=0;j<actionCount;j++)
		{
			CardPool::CGroupCardData cardData;
			int count = actionCard[j][2];
			std::vector<INT> cards;
			for (int z=3;z<3+count;z++)
			{
				cards.push_back(actionCard[j][z]);
			}
			switch (actionCard[j][0])
			{
			case ChiPai:
				break;
			case PengPai:
				cardData._iCardId = cards.at(0);
				cardData._iCount  = cards.size();
				cardData._iType   = CardPool::CGroupCard_Peng;
				groupCards.push_back(cardData);

				break;
			case MingGang:
			case QiangGang:
				cardData._iCardId = cards.at(0);
				cardData._iCount  = cards.size();
				cardData._iType   = CardPool::CGroupCard_MingGang;
				groupCards.push_back(cardData);

				break;
			case AnGang:
				cardData._iCardId = cards.at(0);
				cardData._iCount  = cards.size();
				cardData._iType   = CardPool::CGroupCard_AnGang;
				groupCards.push_back(cardData);

				break;
			default:
				break;
			}
		}
		return groupCards;
	}

	void GameTableLogic::clearDesk()
	{

	}

	void GameTableLogic::dealUserInfoChangeResp(const UserInfoStruct* user)
	{

		auto deskUser = getUserByUserID(user->dwUserID);
		if (deskUser)
		{
			float fMoney = (float)user->i64Money;
			BYTE seatNo = logicToViewSeatNo(deskUser->bDeskStation);
			//_callBack->showUserProfit(seatNo, fMoney);
		}

	}

	void GameTableLogic::dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		switch (messageHead->bAssistantID)
		{
		case ASS_GR_VOIEC:
		{
			//效验数据
			CCAssert(sizeof(VoiceInfo) == objectSize, "VoiceInfo size is error.");
			VoiceInfo* voiceInfo = (VoiceInfo*)object;
			auto userInfo = getUserByUserID(voiceInfo->uUserID);
			if (!userInfo) return;
			if (userInfo->bDeskNO == _deskNo)
			{
				_callBack->onChatVoiceMsg(voiceInfo->uUserID, voiceInfo->uVoiceID, voiceInfo->iVoiceTime);
			}
		}
		break;
		default:
			break;
		}
	}

	void GameTableLogic::dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk)
	{
		auto user = UserInfoModule()->findUser(normalTalk->dwSendID);
		_callBack->onChatTextMsg(user->bDeskStation, normalTalk->szMessage);
	}

	void GameTableLogic::sendStandUp()
	{
		do
		{
			HNGameLogicBase::sendUserUp();
		} while (0);
	}

	void GameTableLogic::loadUsers()
	{

		BYTE seatNo = INVALID_DESKNO;
		char str[64] = { 0 };
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			const UserInfoStruct* pUser = getUserBySeatNo(i);		
			if (_existPlayer[i] && pUser)
			{
				auto deskUser = getUserByUserID(pUser->dwUserID);
				seatNo = logicToViewSeatNo(i);
				_callBack->addUser(getUserDir(i), deskUser);
			}
		}


	}

	void GameTableLogic::dealAuto(char*object, int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CTuoGuan_t) == objectSize, "Broken message GameProtoS2CFlowZhuang_t.");
		GameProtoS2CTuoGuan_t* pProtData = (GameProtoS2CTuoGuan_t*)object;
		auto dir = getUserDir(pProtData->seatNo);
		auto visible = pProtData->bTuoGuan;
		GameManager::getInstance()->setAutoImgeVisibal(dir, visible);
	}

	void GameTableLogic::dealGameClean()
	{

	}
	void GameTableLogic::dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum)
	{

	}
	void GameTableLogic::dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum)
	{

	}
	void GameTableLogic::dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum)
	{

	}
}
