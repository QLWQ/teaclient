#ifndef _XMENMJ_CARDPOOL_H_
#define _XMENMJ_CARDPOOL_H_

#include "XMENMJ_Card.h"
#include <unordered_map>


namespace XMENMJ
{
	class PoolAction;
	class HandCard;

	class CardPool :
		public Layer
	{
	public:
		/////////////////各个位置初始化位置////////////////////////////
		static Vec2 SounthDirOutCardPos;
		static Vec2 EastDirOutCardPos;
		static Vec2 NorthDirOutCardPos;
		static Vec2 WestDirOutCardPos;

		//碰杠牌动作位置
		static Vec2 SounthPGActionCardPos;
		static Vec2 WestPGActionCardPos;
		static Vec2 EastPGActionCardPos;
		static Vec2 NorthPGActionCardPos;
		static const int CHICARDARRAY = 3;

	public:

		enum CGroupCardType
		{
			CGroupCard_Peng		= 0,
			CGroupCard_AnGang	= 1,
			CGroupCard_MingGang = 2,
			CGroupCard_Chi	=3,
		};

		struct CGroupCardData
		{
			int					_iCount;
			int					_iCardId;
			char				_iChiCardArray[CHICARDARRAY];
			CGroupCardType		_iType;	
		};

		//////////////////////////////////////////////////////////////////////////
	public:
		CardPool(void);
		virtual ~CardPool(void);

		const sitDir& getSitDir();									// 方向
		void setOutCardCount(const INT& count) { _sendCardCount = count; }
		const INT& getOutCardCount() { return _sendCardCount; }
		void reSetData();											// 重置数据，新局开始
		bool addGoupGangPeng(int count, int cardId, CGroupCardType cgtype, char chiCardArray[CHICARDARRAY]=0); /* 增加碰杠牌 */
		bool refreshGoupCardCount(int cardId, CGroupCardType cgtype, int setnum, char chiCardArray[CHICARDARRAY]=0);
		void ClearGroupData();
		void refreshHandCardValue(std::vector<INT> v);				//刷新手牌（不含碰杠听）
		std::vector<INT> getHandCardList();
		void moveChangeCard(sitDir dir);

	public:
		virtual cocos2d::Vec2 getCatchPos()=0;
		virtual void addHandCard(INT cardValue);
		virtual void sendSomeCard(const std::vector<INT> vec);			// 开场发一些牌
		virtual bool init() override;       
		virtual void setHandCardPos(INT catchCard) {};								// 安置手牌
		virtual void refreshAllShowCard(){};
		virtual int  getZhuaPaiZOrder();
		virtual void addSomeOutCards(const INT& count, const INT& number, CGroupCardType cgtype, bool hideLastOutCard, char chiCardArray[CHICARDARRAY]); // 碰牌
		virtual void finishGame();
		virtual Vec2 getOutToDeskPos() { return Vec2::ZERO; }// 出牌桌面位置
		virtual void showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> handCards,INT huCard,bool visibleAllCard){};//胡了，倒牌
		virtual void showHuTips(std::vector<INT> huTip){};//显示胡什么牌

		/*
		*进入交换牌
		*/
		virtual void enterChangeCards();

		/*
		*退出交换牌
		*/
		virtual void exitChangeCards(int changeSezi,std::vector<INT>outCard,std::vector<INT> inCard,std::vector<INT> handCardList);

		/*
		*交换牌移到前方
		*/
		virtual void changeCardToHead(std::vector<INT> cards);

	protected:
		void sortCard();													// 整理牌堆
		void moveOneCardToLast(int card);

	protected:
		sitDir _dir;														// 方位
		int										m_iMaxZOrder;
		INT										_sendCardCount;				// 出牌数
		std::vector<CGroupCardData*>			m_GroupCardDataVec;
		std::vector<INT>						_handCardList;				//剩余手牌（不含已经碰杠的牌）
		std::vector<INT>						_changeCardList;			//交换的牌
		HandCard*								_handCard;

		CC_SYNTHESIZE(bool,_isTingState,IsTingState);						//是否可以听牌
		CC_SYNTHESIZE(bool,_isInChangeState,IsInChangeState);				//是否在交换牌阶段
		bool _isCatchCard = false;
	};

}

#endif // _CARDPOOL_H_
