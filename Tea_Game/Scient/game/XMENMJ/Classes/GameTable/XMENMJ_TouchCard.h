#ifndef _XMENMJ_TOUCHCARD_H_
#define _XMENMJ_TOUCHCARD_H_

#include "XMENMJ_poolaction.h"

namespace XMENMJ
{
	class TouchCard :
		public PoolAction
	{
	public:
		TouchCard(void);
		~TouchCard(void);

		virtual void run() override;

		CREATE_FUNC(TouchCard);
	};

}

#endif