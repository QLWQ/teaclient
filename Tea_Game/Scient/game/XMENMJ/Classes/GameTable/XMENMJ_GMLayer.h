#ifndef XMENMJ_GMLayer_h__
#define XMENMJ_GMLayer_h__
#include "HNNetExport.h"

namespace XMENMJ
{
	class XMENMJ_GMLayer : public HNLayer
	{
	public:
		XMENMJ_GMLayer();
		~XMENMJ_GMLayer();

		CREATE_FUNC(XMENMJ_GMLayer);

		void setUIData(std::vector<UserInfoStruct> users,std::vector<std::vector<INT>> cards);
	private:
		virtual bool init() override;
		void clickCloseEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		
		bool LeftCardCallBack(HNSocketMessage* socketMessage);

	private:
		cocos2d::Node*	m_pMainNode;
	};
}



#endif // HSMJ_GMLayer_h__
