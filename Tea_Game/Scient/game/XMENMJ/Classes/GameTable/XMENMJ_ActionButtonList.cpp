#include "XMENMJ_ActionButtonList.h"
#include "cocostudio/CocoStudio.h"
#include "XMENMJ_GameManager.h"
#include "XMENMJ_MessageHead.h"

namespace XMENMJ
{


	XMENMJ_ActionButtonList::XMENMJ_ActionButtonList() :
		_loader(nullptr),
		_PosOrigin(Vec2::ZERO)
	{

	}

	XMENMJ_ActionButtonList::~XMENMJ_ActionButtonList()
	{
		
	}



	bool XMENMJ_ActionButtonList::init()
	{
		if (!Node::init())
		{
			return false;
		}
		
		//杠
		_loader = CSLoader::createNode(COCOS_PATH+"ActionButton.csb");
		auto action1 = CSLoader::createTimeline(COCOS_PATH + "ActionButton.csb");
		_loader->runAction(action1);
		action1->gotoFrameAndPlay(0, true);
		this->addChild(_loader);
		Button* gang=(Button*)_loader->getChildByName("Button_gang");
		gang->setVisible(false);
		gang->setTag(GANG);
		gang->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(gang);

		//碰
		Button* peng=(Button*)_loader->getChildByName("Button_peng");
		peng->setVisible(false);
		peng->setTag(PENG);

		peng->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(peng);

		//吃
		Button* chi = (Button*)_loader->getChildByName("Button_chi");
		chi->setVisible(false);
		chi->setTag(CHI);

		chi->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(chi);


		//游金
		Button* youjin = (Button*)_loader->getChildByName("Button_youjin");
		youjin->setVisible(false);
		youjin->setTag(YOUJING);

		youjin->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(youjin);


		//三金倒
		Button* sanjingdao = (Button*)_loader->getChildByName("Button_sanjindao");
		sanjingdao->setVisible(false);
		sanjingdao->setTag(SANJINGDAO);

		sanjingdao->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(sanjingdao);

		//双游
		Button* shuangyou = (Button*)_loader->getChildByName("Button_shuangyou");
		shuangyou->setVisible(false);
		shuangyou->setTag(SHUANGYOU);

		shuangyou->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(shuangyou);

		//三游

		Button* sanyou = (Button*)_loader->getChildByName("Button_sanyou");
		sanyou->setVisible(false);
		sanyou->setTag(SANYOU);

		sanyou->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(sanyou);


		//天胡
		Button* tianhu = (Button*)_loader->getChildByName("Button_tianhu");
		tianhu->setVisible(false);
		tianhu->setTag(TIANHU);

		tianhu->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(tianhu);

		//抢金
		Button* qiangjing = (Button*)_loader->getChildByName("Button_qiangjing");
		qiangjing->setVisible(false);
		qiangjing->setTag(QIANGJING);

		qiangjing->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(qiangjing);


		//自摸
		Button* zimo = (Button*)_loader->getChildByName("Button_zimo");
		zimo->setVisible(false);
		zimo->setTag(ZIMO);

		zimo->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(zimo);



		//胡
		Button* hu=(Button*)_loader->getChildByName("Button_hu");
		hu->setVisible(false);
		hu->setTag(HU);
		hu->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(hu);


		//八花游
		Button* bahuayou = (Button*)_loader->getChildByName("Button_bahuayou");
		bahuayou->setVisible(false);
		bahuayou->setTag(BAHUAYOU);
		bahuayou->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(bahuayou);



	Button* ting = (Button*)_loader->getChildByName("Button_ting");
		ting->setVisible(false);
		ting->setTag(TING);
		ting->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(ting);

		//过
		Button* guo=(Button*)_loader->getChildByName("Button_guo");
		guo->addTouchEventListener(CC_CALLBACK_2(XMENMJ_ActionButtonList::clickBtEventCallBack, this));
		guo->setTag(GUO);
		_PosOrigin=guo->getPosition();
		return true;
	}

	void XMENMJ_ActionButtonList::clickBtEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		Button* bt=(Button*)pSender;
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		int x=bt->getTag();
		switch (bt->getTag())
		{
		case GANG :
			{

				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SActionEat_t toProtData;
				toProtData.actionType = 3;
				memset(toProtData.chiCardArray, 0, sizeof(toProtData.chiCardArray));
				toProtData.chiCardCount = 0;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ActionEat, &toProtData, sizeof(toProtData));
			}
			break;
		case PENG :
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SActionEat_t toProtData;
				toProtData.actionType = 2;
				memset(toProtData.chiCardArray, 0, sizeof(toProtData.chiCardArray));
				toProtData.chiCardCount = 0;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ActionEat, &toProtData, sizeof(toProtData));
			}	
			break;
		case CHI:
			{
				GameManager::getInstance()->ShowChiTip();
			}
			break;

		case HU:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_HuPai, &toProtData, sizeof(toProtData));
			}
			break;

		case GUO  :
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SPassAction_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_PassAction, &toProtData, sizeof(toProtData));
			}
			break;
		case SANJINGDAO:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_SANJINGDAO, &toProtData, sizeof(toProtData));
			}
			break;
		case QIANGJING:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_QIANGJING, &toProtData, sizeof(toProtData));
			}
			break;
		case YOUJING:
			{	
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_YOUJING, &toProtData, sizeof(toProtData));
			}
			break;
		case SHUANGYOU:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_YOUJING, &toProtData, sizeof(toProtData));
			}
			break;
		case SANYOU:
			{
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_YOUJING, &toProtData, sizeof(toProtData));
			}
			break;
		case ZIMO:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_HuPai, &toProtData, sizeof(toProtData));
			}
			break;
		case TIANHU:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_HuPai, &toProtData, sizeof(toProtData));
			}
			break;
		case BAHUAYOU:
		{
			GameManager::getInstance()->setIsHasChiAction(false);
			GameProtoC2SHuPai_t toProtData;
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_BAHUAYOU, &toProtData, sizeof(toProtData));
		}
			break;
		case TING:
		{
			GameManager::getInstance()->setIsHasChiAction(false);
			GameProtoC2SHuPai_t toProtData;
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_TingPai, &toProtData, sizeof(toProtData));
		}
			break;
		default:
			break;
		}
		this->runAction(RemoveSelf::create(false));
		GameManager::getInstance()->setIsHasAction(false);
		GameManager::getInstance()->showTimeCountByAction(true,10);
	}

	void XMENMJ_ActionButtonList::showActionBotton(bool hasHu, bool canchi, bool hasPeng, bool hasGang, bool canTing, bool canQiang, bool canDao, bool canyou, bool cantianhu, bool canzimo, char num)
	{
		if (_loader)
		{
			_loader->getChildByName("Button_hu")->setVisible(hasHu);
			_loader->getChildByName("Button_chi")->setVisible(canchi);
			_loader->getChildByName("Button_gang")->setVisible(hasGang);
			_loader->getChildByName("Button_peng")->setVisible(hasPeng);
			_loader->getChildByName("Button_qiangjing")->setVisible(canQiang);
			_loader->getChildByName("Button_sanjindao")->setVisible(canDao);

			//_loader->getChildByName("Button_sanjindao")->setVisible(canDao);
			//游金
			if (num ==1)
			{
				_loader->getChildByName("Button_youjin")->setVisible(canyou);
			}
			//双游
			else if (num == 2)
			{
				_loader->getChildByName("Button_shuangyou")->setVisible(canyou);
			}
			//三游
			else if (num == 3)
			{
				_loader->getChildByName("Button_sanyou")->setVisible(canyou);
			}
			//天胡
			_loader->getChildByName("Button_tianhu")->setVisible(cantianhu);
			//自摸
			_loader->getChildByName("Button_zimo")->setVisible(canzimo);


			//听牌
			_loader->getChildByName("Button_ting")->setVisible(canTing);
			

			reSetButtonPos();
		}
	}

	void XMENMJ_ActionButtonList::reSetButtonPos()
	{
		int index=0;
		for (auto iter=_buttonVec.begin();iter!=_buttonVec.end();iter++)
		{
			Button* bt=*iter;
			if (bt->isVisible())
			{
				index++;
				bt->setPositionX(_PosOrigin.x-index*bt->getContentSize().width);
			}
		}
	}

}
