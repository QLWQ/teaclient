#include "XMENMJ_WestHandCard.h"
#include "XMENMJ_GameManager.h"

namespace XMENMJ
{
	static cocos2d::Vec2 beginDirPos = Vec2(270, 710);
	static cocos2d::Vec2 endDirPos = Vec2(135, 115);
	
	XMENMJ_WestHandCard::XMENMJ_WestHandCard()
	{
		_groupSize = Size(50,31.5);
		_normalSize = Size(50,31.5);
		_tingSize = Size(40,38);
		_beginZorder=3000;
	}

	XMENMJ_WestHandCard::~XMENMJ_WestHandCard()
	{
	}

	bool XMENMJ_WestHandCard::init()
	{
		if (!HandCard::init())
		{
			return false;
		}

		return true;
	}


	void XMENMJ_WestHandCard::refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards)
	{
		//处理碰杠牌，碰杠牌下边,扣放上边
		std::vector<CardPool::CGroupCardData> pengGangList;
		for (auto value:groupCards)
		{
			if (value._iType==CardPool::CGroupCard_Peng || 
				value._iType==CardPool::CGroupCard_AnGang || 
				value._iType == CardPool::CGroupCard_Chi||
				value._iType==CardPool::CGroupCard_MingGang)
			{
				pengGangList.push_back(value);
			}
		}

		//摆牌碰杠
		for(auto pengGang:pengGangList)
		{
			Vec2 secondPos = Vec2::ZERO;
			for (int i=0; i<pengGang._iCount; i++)
			{
				Card* pCard = nullptr;
				cocos2d::Sprite*ZheZhao = NULL;
				if (pengGang._iType == CardPool::CGroupCard_AnGang && i<3)
				{

					pCard = GameManager::getInstance()->createBeiPai(WEST_DIR);
				}
				else if (pengGang._iType == CardPool::CGroupCard_Chi)
				{
					pCard = GameManager::getInstance()->createPengGangFront(WEST_DIR, pengGang._iChiCardArray[i]);
					if (pengGang._iChiCardArray[i] == pengGang._iCardId)
					{
						ZheZhao = Sprite::create(SPRITE_PATH + "PgkW.png");
					}
				}
				else
				{
					if (pengGang._iType == CardPool::CGroupCard_Peng)
					{
						if (i == 0)
						{
							ZheZhao = Sprite::create(SPRITE_PATH + "PgkW.png");
						}
					}
					pCard = GameManager::getInstance()->createPengGangFront(WEST_DIR, pengGang._iCardId);
				}

				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setCardEnableTouch(false);
				pCard->setVisible(true);
				if (ZheZhao != NULL)
				{
					ZheZhao->setAnchorPoint(Vec2(0.5f, 0.5f));
					ZheZhao->setScale(0.9);
					ZheZhao->setPosition(Vec2(35,29));
					pCard->addChild(ZheZhao, 1);
					ZheZhao->setVisible(true);
				}
				
				if (i==3)
				{
					pCard->setLocalZOrder(_beginZorder+3);
					pCard->setPosition(Vec2(secondPos.x-8,secondPos.y+15));
				}
				else
				{
					_prePoint = getNextPosition(_prePoint,_groupSize.height);
					pCard->setLocalZOrder(_beginZorder++);
					pCard->setPosition(_prePoint);
					if (i==1)
					{
						secondPos = _prePoint;
					}
				}
				this->addChild(pCard);
				_groupCardsList.push_back(pCard);
			}

			//每个碰杠留出空格
			_prePoint = getNextPosition(_prePoint,_groupSize.height/3.5f);
		}

	}


	cocos2d::Vec2 XMENMJ_WestHandCard::getNextPosition(Vec2 prePoint, float deltLen)
	{
		cocos2d::Vec2 nodeDir = endDirPos-beginDirPos;
		nodeDir.normalize();
		nodeDir.scale(deltLen);
		return prePoint+nodeDir;

	}

	Vec2 XMENMJ_WestHandCard::refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>normalCards, INT catchCard)
	{
		resetData();
		refreshPengGang(groupCards);


		//牌还没出的手牌
		std::vector<cocos2d::Node*> nodelist;
		for (auto iter = normalCards.begin(); iter != normalCards.end(); iter++)
		{
			
			_prePoint = getNextPosition(_prePoint,_normalSize.height);
			Card* pCard= MahjongCard::create(mahjongCreateType::DI_WEST_STAND, WEST_DIR);
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			pCard->setLocalZOrder(_beginZorder++);
			pCard->setVisible(true);
			pCard->setCardEnableTouch(false);
			pCard->setPosition(_prePoint);
			this->addChild(pCard);
			_handCardsList.push_back(pCard);
		}
		
		return Vec2::ZERO;
		
	}

	
	Vec2 XMENMJ_WestHandCard::refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>tingCards)
	{
		resetData();

		refreshPengGang(groupCards);
		int size=tingCards.size();
		int num=0;
		//牌还没出的手牌
		for (auto iter = tingCards.begin(); iter != tingCards.end(); iter++)
		{
			num++;
			_prePoint = getNextPosition(_prePoint,_groupSize.height);
			Card* pCard=nullptr;
            if(size==num)
			{ //用于提示其他玩家已听牌
			   pCard= MahjongCard::create(mahjongCreateType::DI_WEST_BACK, WEST_DIR);
			   num=0;
			}
			else
			{
				pCard= MahjongCard::create(mahjongCreateType::DI_WEST_STAND, WEST_DIR);
			}
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			pCard->setLocalZOrder(_beginZorder++);
			pCard->setVisible(true);
			pCard->setCardEnableTouch(false);
			pCard->setPosition(_prePoint);
			this->addChild(pCard);
			_handCardsList.push_back(pCard);
		}
	
		return Vec2::ZERO;
	}


	Vec2 XMENMJ_WestHandCard::refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>handCards, INT huCardId, bool visibleAllCard)
	{
		resetData();
		_groupCards.clear();
		for (auto v:groupCards)
		{
			_groupCards.push_back(v);
		}

		refreshPengGang(groupCards);
		for (auto iter = handCards.begin(); iter != handCards.end(); iter++)
		{
			_prePoint = getNextPosition(_prePoint,_groupSize.height);
			if (visibleAllCard)
			{
				Card* pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_WEST, WEST_DIR,*iter);
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setLocalZOrder(_beginZorder++);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);

				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard->setHuVisible(true);
				}

			}
			else
			{
				Card* pCard	= nullptr;
				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_WEST, WEST_DIR, *iter);
					pCard->setHuVisible(true);
				}
				else
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_WEST_BACK, WEST_DIR);
				}
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setLocalZOrder(_beginZorder++);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);
			}
		}
		return Vec2::ZERO;
	}

	void XMENMJ_WestHandCard::setSingleGroupSize(Size groupSize)
	{
		_groupSize = groupSize;
	}

	void XMENMJ_WestHandCard::setNormalCardSize(Size normalSize)
	{
		_normalSize = normalSize;
	}

	void XMENMJ_WestHandCard::setTingCardSize(Size tingSize)
	{
		_tingSize = tingSize;
	}

	cocos2d::Vec2 XMENMJ_WestHandCard::getCatchWorldPos()
	{
		return convertToWorldSpace(getNextPosition(_prePoint,90));
	}

	void XMENMJ_WestHandCard::resetData()
	{
		for (auto v:_groupCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_handCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}

		_groupCards.clear();
		_groupCardsList.clear();
		_handCardsList.clear();
		_changeCardsInHead.clear();
		_prePoint=beginDirPos;
		_beginZorder=3000;
	}

	void XMENMJ_WestHandCard::putChangeCardToHead(std::vector<INT> cards)
	{
		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}
		_changeCardsInHead.clear();

		float startX = 180;
		int index = 0;
		int zorder = 500;
		std::vector<Point> v_pos_temp;
		for (auto v:cards)
		{
			auto pCard = MahjongCard::create(DI_WEST_BACK,WEST_DIR);
			pCard->setPosition(Vec2(startX+index*8,360+index*38));
			pCard->setLocalZOrder(zorder--);
			pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
			this->addChild(pCard);
			_changeCardsInHead.push_back(pCard);
			MoveBy* moveBy = MoveBy::create(0.5f,Vec2(120,0));
			pCard->runAction(moveBy);
			v_pos_temp.push_back(pCard->getPosition()+Vec2(120, 0));
			index++;
		}

		GameManager::getInstance()->addChangeCardPos(sitDir::WEST_DIR, v_pos_temp);
	}

	void XMENMJ_WestHandCard::moveChangeCard(sitDir dir)
	{
		Vec2 posBy = Vec2::ZERO;
		
		int i = 0;
		for (auto v:_changeCardsInHead)
		{
			switch (dir)
			{
			case XMENMJ::SOUTH_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CCW, bezDir::WEST_SOUTH_DIR, sitDir::SOUTH_DIR, i);
				break;
			case XMENMJ::EAST_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::STRAIGHT, bezDir::BEZ_MID_DIR, sitDir::EAST_DIR, i);
				break;
			case XMENMJ::NORTH_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CW, bezDir::WEST_NORTH_DIR, sitDir::NORTH_DIR, i);
				break;
			default:
				break;
			}
			i++;
		}
		_changeCardsInHead.clear();
	}

}
