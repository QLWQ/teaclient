#ifndef _XMENMJ_TOUCHKONG_H_
#define _XMENMJ_TOUCHKONG_H_

#include "XMENMJ_poolaction.h"

namespace XMENMJ
{

	class TouchKong :
		public PoolAction
	{
	public:
		TouchKong(void);
		~TouchKong(void);

		virtual void run() override;

		CREATE_FUNC(TouchKong);
	};

}

#endif
