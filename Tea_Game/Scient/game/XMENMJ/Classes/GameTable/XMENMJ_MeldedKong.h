#ifndef _XMENMJ_MELDEDKONG_H_
#define _XMENMJ_MELDEDKONG_H_

#include "XMENMJ_poolaction.h"

namespace XMENMJ
{

	class MeldedKong :
		public PoolAction
	{
	public:
		MeldedKong(void);
		~MeldedKong(void);

		virtual void run() override;

		CREATE_FUNC(MeldedKong);
	};

}

#endif