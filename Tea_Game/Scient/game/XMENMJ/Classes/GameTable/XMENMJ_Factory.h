#ifndef _XMENMJ_FACTORYCARDPOOL_H_
#define _XMENMJ_FACTORYCARDPOOL_H_

#include "XMENMJ_CardPool.h"
#include "XMENMJ_MahjongCardPool.h"
#include "XMENMJ_SouthMahjongCardPool.h"
#include "XMENMJ_WestMahjongCardPool.h"
#include "XMENMJ_EastMahjongCardPool.h"
#include "XMENMJ_NorthMahjongCardPool.h"

#include "XMENMJ_TouchCard.h"
#include "XMENMJ_MeldedKong.h"
#include "XMENMJ_ConcealedKong.h"
#include "XMENMJ_TouchKong.h"
#include "XMENMJ_HuCard.h"
#include "XMENMJ_EatCard.h"

namespace XMENMJ
{

	class Factory
	{
	public:
		static CardPool* createEastPool(INT count);
		static CardPool* createWestPool(INT count);
		static CardPool* createSouthPool(INT count);
		static CardPool* createNorthPool(INT count);

		static PoolAction* createTouchCardAction();
		static PoolAction* createMeldedKongAction();
		static PoolAction* createConcealedKongAction();
		static PoolAction* createTouchKongAction();
		static PoolAction* createTouchEatAction();
		static PoolAction* createHuCardAction();
	};

}

#endif