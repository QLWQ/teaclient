LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNNet)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNUI)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNMarket)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLobby)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLogic)

LOCAL_MODULE := XMENMJ_static

LOCAL_MODULE_FILENAME := libXMENMJ

LOCAL_SRC_FILES := ../Classes/GameTable/XMENMJ_GameTableLogic.cpp \
                   ../Classes/GameTable/XMENMJ_GameManager.cpp \
                   ../Classes/GameTable/XMENMJ_ResourceLoader.cpp \
				   ../Classes/GameTable/XMENMJ_CardPool.cpp \
				   ../Classes/GameTable/XMENMJ_GameTableUI.cpp \
				   ../Classes/GameTable/XMENMJ_PoolAction.cpp \
				   ../Classes/GameTable/XMENMJ_HuCard.cpp \
				   ../Classes/GameTable/XMENMJ_Card.cpp \
				   ../Classes/GameTable/XMENMJ_SouthMahjongCardPool.cpp \
				   ../Classes/GameTable/XMENMJ_EastMahjongCardPool.cpp \
				   ../Classes/GameTable/XMENMJ_WestHandCard.cpp \
				   ../Classes/GameTable/XMENMJ_SouthHandCard.cpp \
				   ../Classes/GameTable/XMENMJ_EastHandCard.cpp \
				   ../Classes/GameTable/XMENMJ_WestMahjongCardPool.cpp \
				   ../Classes/GameTable/XMENMJ_MahjongCard.cpp \
				   ../Classes/GameTable/XMENMJ_NorthMahjongCardPool.cpp \
				   ../Classes/GameTable/XMENMJ_NorthHandCard.cpp \
				   ../Classes/GameTable/XMENMJ_ConcealedKong.cpp \
				   ../Classes/GameTable/XMENMJ_Factory.cpp \
				   ../Classes/GameTable/XMENMJ_MahjongCardPool.cpp \
				   ../Classes/GameTable/XMENMJ_MeldedKong.cpp \
				   ../Classes/GameTable/XMENMJ_TouchCard.cpp \
				   ../Classes/GameTable/XMENMJ_TouchKong.cpp \
				   ../Classes/GameTable/XMENMJ_Result.cpp \
				   ../Classes/GameTable/XMENMJ_TurnTable.cpp \
				   ../Classes/GameTable/XMENMJ_GMLayer.cpp \
				   ../Classes/GameTable/XMENMJ_HandCard.cpp \
				   ../Classes/GameTable/XMENMJ_ActionButtonList.cpp \
				   ../Classes/GameTable/XMENMJ_SetLayer.cpp \
				   ../Classes/GameTable/XMENMJ_EatCard.cpp \

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMessage \
                    $(LOCAL_PATH)/../Classes/GameTable \
                    $(LOCAL_PATH)/../../ \
                    $(LOCAL_PATH)/../ 

LOCAL_EXPORT_LDLIBS := -llog \
                       -lz \
                       -landroid     
                   
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_net_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_market_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_lobby_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_logic_static

LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static


include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)
$(call import-module,editor-support/cocostudio)
$(call import-module,extensions)

$(call import-module,LibHNUI)
$(call import-module,LibHNNet)
$(call import-module,LibHNMarket)
$(call import-module,LibHNLogic)
$(call import-module,LibHNLobby)