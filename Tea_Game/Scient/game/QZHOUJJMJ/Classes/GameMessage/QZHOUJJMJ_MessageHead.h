﻿#ifndef	 _QZHOUJJMJ_Message_MahJong_h_
#define  _QZHOUJJMJ_Message_MahJong_h_

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNBaseType.h"

#include "HNLobbyExport.h"
#include "HNNetExport.h"

#include <string>
#include "QZHOUJJMJ_Protocol.h"

namespace QZHOUJJMJ
{

	// 自定义
#pragma pack(1)

#define COCOS_NODE(_nodeType, _nodeName) dynamic_cast<_nodeType *>(GameManager::getInstance()->getCR()->getNode(_nodeName))
#define PLIST_NODE(_nodeName) dynamic_cast<Sprite *>(GameManager::getInstance()->getPR()->getNode(_nodeName))


	const int topZorder = 9999;
	enum sitDir{ MID_DIR = -1,SOUTH_DIR = 0, EAST_DIR = 1,NORTH_DIR =2,WEST_DIR = 3,DIR_MAX};
	enum bezDir { BEZ_MID_DIR = -1, EAST_SOUTH_DIR = 0, EAST_NORTH_DIR = 1, WEST_NORTH_DIR = 2, WEST_SOUTH_DIR = 3, BEZ_DIR_MAX };
	enum changeActType { NONE = -1, STRAIGHT = 0,CCW = 2,CW = 1, CAT_MAX };//直行，顺时针，逆时针
	enum mahjongColor 
	{ 
		INVALID	=-1,
		WAN		= 0, 
		TIAO	= 1, 
		TONG	= 2,
		FANG	= 3,
		HUA		= 4
	};
	enum mahjongCreateType 
	{ 
		DI_WEST_STAND,	//西方站立
		DI_EAST_STAND,	//东方站立
		DI_SOUTH_STAND, //南方站立
		DI_NORTH_STAND, //北方站立

		DI_WEST_BACK,	//西方背面
		DI_EAST_BACK,	//东方背面
		DI_SOUTH_BACK,  //南方背面
		DI_NORTH_BACK,	//北方背面

		DI_FRONT_WEST,	//西方正面
		DI_FRONT_EAST,	//东方正面
		DI_FRONT_SOUTH,	//南方正面
		DI_FRONT_NORTH,	//北方正面
		DI_FRONT_ZHENG,	//正面

		//出牌方向
		DI_OUT_CARD_WEST,		//西方向出牌
		DI_OUT_CARD_EAST,		//东方向出牌
		DI_OUT_CARD_SOUTH,	//南方向出牌
		DI_OUT_CARD_NORTH,	//北方向出牌
	};

	// 宏
#define MY_DEBUG

	const Vec2 NOT_MOVE = Vec2(8888.0, 8888.0);
	const INT IS_BUTTON = 6666;
	const INT CHICARDCOUNT = 3;


	const std::string GAME_SOURCE_PAHT = "Games/QZHOUJJMJ/";
	const std::string MUSIC_PATH = GAME_SOURCE_PAHT + "Music/";
	const std::string MUSIC_SEX_PATH = MUSIC_PATH + "putong/";
	const std::string MUSIC_BG_PATH = MUSIC_PATH + "background/";
	const std::string MUSIC_COMMON_PATH = MUSIC_PATH + "common/";

	const std::string COCOS_PATH = GAME_SOURCE_PAHT + "cocos/";
	const std::string SPRITE_PATH = GAME_SOURCE_PAHT + "sprite/";
	const std::string RESOULT_PATH = GAME_SOURCE_PAHT + "Result/";
	const std::string PLIST_PATH = GAME_SOURCE_PAHT + "plist/";
	const std::string FNT_PATH = SPRITE_PATH + "fnt/";
	const std::string PIC_PATH = SPRITE_PATH +"pic/";
	const std::string ANM_PATH = SPRITE_PATH + "goldcard/";
	const std::string ANM1_PATH = SPRITE_PATH + "fenbing/";
	const std::string ANM2_PATH = SPRITE_PATH + "jiance/";
	const std::string ANM3_PATH = SPRITE_PATH + "gang/";
	const std::string ANM4_PATH = SPRITE_PATH + "hu/";
	const std::string Player_Normal_M = "Games/QZHOUJJMJ/cocos/man.png";
	const std::string Player_Normal_W = "Games/QZHOUJJMJ/cocos/woman.png";
#define MAKE_A_MJ(hs,pd)  ( (hs) * 10 +(pd))
	///lym2010-05-07重新定义麻将值
	///字：11-13
	///风：14-17
	///条：21-29
	///同：31-39
	///万：41-49
	///花牌：51-58 或者更多

	class CMjEnum
	{
	public:

		enum MJ_TYPE_HUA_SE
		{
			MJ_TYPE_HUA_SE_NONE=-10,//lu无花色
			MJ_TYPE_HUA_SE_BIN=3,//lu同
			MJ_TYPE_HUA_SE_WAN=4,//lu万
			MJ_TYPE_HUA_SE_TIAO=2,//lu条
			MJ_TYPE_HUA_SE_FENG=1//lu风
		};

		enum MJ_TYPE_PAI//牌类型
		{
			MJ_TYPE_PAI_NONE=-10,//lu无花色
			MJ_TYPE_PAI_WAN=0,//lu万
			MJ_TYPE_PAI_TIAO=1,//lu条
			MJ_TYPE_PAI_BING=2,//lu同
			MJ_TYPE_PAI_FENG=3,//lu风
			MJ_TYPE_PAI_HUA=4//lu花
		};

		enum MJ_TYPE_PAI_DIAN//lu牌点
		{
			MJ_TYPE_PAI_DIAN_NONE=-1,

			MJ_TYPE_PAI_DIAN_1 = 1,
			MJ_TYPE_PAI_DIAN_2 = 2,
			MJ_TYPE_PAI_DIAN_3 = 3,
			MJ_TYPE_PAI_DIAN_4 = 4,
			MJ_TYPE_PAI_DIAN_5 = 5,
			MJ_TYPE_PAI_DIAN_6 = 6,
			MJ_TYPE_PAI_DIAN_7 = 7,
			MJ_TYPE_PAI_DIAN_8 = 8,
			MJ_TYPE_PAI_DIAN_9 = 9

		};


		enum MJ_TYPE
		{
			MJ_TYPE_NONE=0,

			MJ_TYPE_FCHUN=MAKE_A_MJ(MJ_TYPE_PAI_FENG,11),
			MJ_TYPE_FXIA=MAKE_A_MJ(MJ_TYPE_PAI_FENG,12),
			MJ_TYPE_FQIU=MAKE_A_MJ(MJ_TYPE_PAI_FENG,13),
			MJ_TYPE_FDONG=MAKE_A_MJ(MJ_TYPE_PAI_FENG,14),
			MJ_TYPE_FMEI=MAKE_A_MJ(MJ_TYPE_PAI_FENG,15),
			MJ_TYPE_FLAN=MAKE_A_MJ(MJ_TYPE_PAI_FENG,16),
			MJ_TYPE_FZHU=MAKE_A_MJ(MJ_TYPE_PAI_FENG,17),
			MJ_TYPE_FJU=MAKE_A_MJ(MJ_TYPE_PAI_FENG,18),


			MJ_TYPE_CAISHEN=MAKE_A_MJ(MJ_TYPE_PAI_FENG,19),
			MJ_TYPE_YUANBAO=MAKE_A_MJ(MJ_TYPE_PAI_FENG,20),
			MJ_TYPE_MAO=MAKE_A_MJ(MJ_TYPE_PAI_FENG,21),
			MJ_TYPE_LAOXU=MAKE_A_MJ(MJ_TYPE_PAI_FENG,22),



			MJ_TYPE_FD=MAKE_A_MJ(MJ_TYPE_PAI_FENG,MJ_TYPE_PAI_DIAN_1),///东
			MJ_TYPE_FN=MAKE_A_MJ(MJ_TYPE_PAI_FENG,MJ_TYPE_PAI_DIAN_2),///南
			MJ_TYPE_FX=MAKE_A_MJ(MJ_TYPE_PAI_FENG,MJ_TYPE_PAI_DIAN_3),///西
			MJ_TYPE_FB=MAKE_A_MJ(MJ_TYPE_PAI_FENG,MJ_TYPE_PAI_DIAN_4),///北
			MJ_TYPE_ZHONG=MAKE_A_MJ(MJ_TYPE_PAI_FENG,MJ_TYPE_PAI_DIAN_5),///红中
			MJ_TYPE_FA=MAKE_A_MJ(MJ_TYPE_PAI_FENG,MJ_TYPE_PAI_DIAN_6),///发财
			MJ_TYPE_BAI=MAKE_A_MJ(MJ_TYPE_PAI_FENG,MJ_TYPE_PAI_DIAN_7),///白板


			MJ_TYPE_W1=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_1),///万
			MJ_TYPE_W2=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_2),
			MJ_TYPE_W3=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_3),
			MJ_TYPE_W4=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_4),
			MJ_TYPE_W5=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_5),
			MJ_TYPE_W6=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_6),
			MJ_TYPE_W7=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_7),
			MJ_TYPE_W8=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_8),
			MJ_TYPE_W9=MAKE_A_MJ(MJ_TYPE_PAI_WAN,MJ_TYPE_PAI_DIAN_9),

			MJ_TYPE_T1=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_1),///条
			MJ_TYPE_T2=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_2),
			MJ_TYPE_T3=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_3),
			MJ_TYPE_T4=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_4),
			MJ_TYPE_T5=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_5),
			MJ_TYPE_T6=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_6),
			MJ_TYPE_T7=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_7),
			MJ_TYPE_T8=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_8),
			MJ_TYPE_T9=MAKE_A_MJ(MJ_TYPE_PAI_TIAO,MJ_TYPE_PAI_DIAN_9),

			MJ_TYPE_B1=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_1),///同
			MJ_TYPE_B2=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_2),
			MJ_TYPE_B3=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_3),
			MJ_TYPE_B4=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_4),
			MJ_TYPE_B5=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_5),
			MJ_TYPE_B6=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_6),
			MJ_TYPE_B7=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_7),
			MJ_TYPE_B8=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_8),
			MJ_TYPE_B9=MAKE_A_MJ(MJ_TYPE_PAI_BING,MJ_TYPE_PAI_DIAN_9)
		};

	};

	enum gameInfoxx
	{
		NAME_ID = 20171123,
		KIND_ID = 1,
	};

	
	enum ActType
	{
		ChiPai		= 1,	/* 吃 */
		PengPai		= 2,	/* 碰 */
		MingGang	= 3,	/* 明杠，已碰三张，摸到一张, 明杠在逻辑外面检测 */
		AnGang		= 4,	/* 暗杠，摸到四张 */
		QiangGang	= 5,	/* 枪杠，摸到三张，杠对方出牌 */
	};


#pragma pack()


}
#endif // _HSMJ_Message_h_
