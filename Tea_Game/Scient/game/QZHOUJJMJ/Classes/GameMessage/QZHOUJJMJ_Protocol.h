#ifndef __QZHOUJJMJ_PROTOCOL_GAME_H__
#define __QZHOUJJMJ_PROTOCOL_GAME_H__

/************************************************************************/
/* 泉州晋江麻将                                                          */
/************************************************************************/

namespace QZHOUJJMJ
{
	enum QZGameInfo
	{
		PLAY_COUNT = 4,
		PLAYER_COUNT				= PLAY_COUNT,		/* 玩家数量 */
		TOTAL_CARD_MAX_COUNT		= 144,				/* 卡牌总数 */
		ACTION_MAX_COUNT			= 20,				/* 动作的最大数量 */
		USERCARD_MAX_COUNT			= 17,				/* 用户最大牌数 */


		/* 游戏协议 */
		//C2S
		GAME_PROTOCOL_C2S_GameBegin				= 100,		/* 同意开始 */
		GAME_PROTOCOL_C2S_SendCardFinish		= 101,		/* 发牌完成 */
		GAME_PROTOCOL_C2S_ChangeCard			= 102,		/* 换牌 */
		GAME_PROTOCOL_C2S_OutCard				= 103,		/* 出牌 */
		GAME_PROTOCOL_C2S_TuoGuan				= 104,		/* 托管 */
		GAME_PROTOCOL_C2S_ActionEat				= 105,		/* 吃碰杠牌 */
		GAME_PROTOCOL_C2S_HuPai					= 116,		/* 胡牌 */
		GAME_PROTOCOL_C2S_PassAction			= 117,		/* 过牌，不吃碰杠胡听 */
		GAME_PROTOCOL_C2S_RequestLeftCard		= 118,		/* 请求剩余卡牌 */
		GAME_PROTOCOL_C2S_RequestOneCard		= 119,		/* 请求一张牌 */
		GAME_PROTOCOL_C2S_TingPai				= 120,		/* 听牌 */
		GAME_PROTOCOL_C2S_SANJINGDAO			= 121,		/*三金倒*/
		GAME_PROTOCOL_C2S_QIANGJING				= 122,		/*抢金*/
		GAME_PROTOCOL_C2S_YOUJING				= 123,		/*游金*/
		GAME_PROTOCOL_C2S_BAHUAYOU				= 124,		/*八花游*/
		GAME_PROTOCOL_C2S_GETYOUJINGTIS			= 125,		//游金按钮点击状态
		GAME_PROTOCOL_C2S_GETWANFATIS			= 126,		//玩法规则定义


		//S2C
		GAME_PROTOCOL_S2C_GameBegin				= 200,		/* 同意开始 */
		GAME_PROTOCOL_S2C_SendCard				= 201,		/* 发牌， server没有发牌完成 */
		GAME_PROTOCOL_S2C_RandomDice			= 202,		/* 随机骰子，用于随机庄家 */
		GAME_PROTOCOL_S2C_OutCard				= 203,		/* 出牌 */
		GAME_PROTOCOL_S2C_DispatchCard			= 204,		/* 发牌 */
		GAME_PROTOCOL_S2C_GameFinish			= 205,		/* 游戏结束 */
		GAME_PROTOCOL_S2C_TuoGuan				= 206,		/* 托管 */
		GAME_PROTOCOL_S2C_ActionAppear			= 208,		/* 吃碰杠牌, 出现 */
		GAME_PROTOCOL_S2C_ActionEat				= 209,		/* 吃碰杠牌，执行 */
		GAME_PROTOCOL_S2C_BeginOut				= 210,		/* 开始出牌，换好牌后出现 */
		GAME_PROTOCOL_S2C_RequestLeftCard		= 211,		/* 请求剩余卡牌 */
		GAME_PROTOCOL_S2C_ChangeCardBegin		= 212,		/* 通知进入换牌阶段 */
		GAME_PROTOCOL_S2C_ChangeCardFinish		= 213,		/* 换牌结束 */
		GAME_PROTOCOL_S2C_ChangeCard			= 214,		/* 转发用户换牌信息*/
		GAME_PROTOCOL_S2C_Hu					= 215,		/* 胡牌*/
		GAME_PROTOCOL_S2C_TingPai				= 216,       //听牌
		GAME_PROTOCOL_S2C_FlowZhuang			= 217,       //跟庄
		GAME_PROTOCOL_S2C_SHOWFLOWER			= 218,		/*用于玩家亮花*/
		GAME_PROTOCOL_S2C_SHOWFLOWER_FINSHG		= 219,		/*用于玩家亮花完成*/
		GAME_PROTOCOL_S2C_Send_GoldData			= 220,		/*用户翻金*/
		GAME_PROTOCOL_S2C_GenDaTiSi             = 221,		/* 跟打提示 */
		GAME_PROTOCOL_S2C_TingPaiTiSi           = 222,		// 听牌提示
		GAME_PROTOCOL_S2C_YouJinTiSi			= 223,		// 尤金提示	
		GAME_PROTOCOL_S2C_JinFengTouTiSi		= 224,		// 禁风头提示
		GAME_PROTOCOL_S2C_HandCardWrong		= 225,		//* 手牌出错 */

		S_C_GAME_RECORD							= 161,		//战绩请求消息
		S_C_GAME_RECORD_RESULT					= 162,		//返回战绩

	};
	enum GameStatus
	{
		Free			= 0,	/* 无 */
		Agree			= 1,	/* 同意阶段 */
		GameBegin		= 2,	/* 同意阶段 */
		SendCard		= 3,	/* 发牌阶段 */
		ChangeCard		= 4,	/* 交换牌	*/
		OutCard			= 5,	/* 出牌阶段 */
		DispatchCard	= 6,	/* 抓牌		*/
		GameFinish		= 7,	/* 结束阶段 */
	};

	enum HuType
	{

		HuType_NO = 0,						//没有胡
		TYPE_PINGHU = 1,					//普通胡	
		TYPE_ZIMO = 2,						//自摸
		TYPE_YOUJIN = 3,					//游金
		TYPE_SHUANGYOU = 4,					//双游
		YTPE_SANYOU = 5,					//三游
		TYPE_QIANGJIN = 6,					//抢金
		TYPE_SANJINDAO = 7,					//三金倒
		TYPE_TIANHU = 8,					//天胡
		TYPE_BAHUAYOU = 9					//八花游

	};

	//enum HuType
	//{
	//	HuType_NO,						//没有胡
	//	HuType_PuTongHu,				//普通胡
	//	HuType_QingYiSe,				//清一色
	//	HuType_YiTiaoLong,				//一条龙
	//	HuType_DuiDuiHu,				//对对胡
	//	HuType_LongQiDui,				//龙七对
	//	HuType_ShiSanYao,				//十三幺
	//};
	enum Addition_Hu
	{
		Addition_NO,					//没有附加
		Addition_GangShangHua,			//杠上花
		Addition_GangShangPao,			//杠上炮
		Addition_QiangGangHu,			//枪杠胡
	};

#pragma pack(1)
	//////////////////////////////////////////////////////////////////////////
	/* 协议结构体 */
	/* 游戏状态协议 */
	struct GameStatusHead_t /* 协议头 */
	{
		int			scoreArray[PLAYER_COUNT];	/* 玩家当前分数，基本分加上这个数，就是当前显示金币 */
		int			status;  /* 当前状态 */

		int			baseBeiLv;			/* 基本分 */
		int			iTax;					//台费
		bool        bIsSuperUser;           //是否是超端用户
		bool		bForceTuoGuan;			//是否强退托管
		bool		bShowTax;				//是否显示台费
		bool		btuoguan[PLAYER_COUNT];//是否托管
	};
	struct GameStatusAgree_t
	{
		bool		isAgreeArray[PLAYER_COUNT];
	};
	struct GameStatusGameBegin_t
	{
		char	bankerNo;
		int		bankcount;										//连庄数目
		char	dingZhuangSezi;
		char	bySeziEr;
	};
	struct GameStatusSendCard_t
	{
		float	leftTime;
		char	cardArray[PLAYER_COUNT][USERCARD_MAX_COUNT]; /* user的卡牌 */
		char	countArray[PLAYER_COUNT];	/* 玩家手牌数量 */
		char	bankNo;	/* 庄家 */
		int		bankcount;										//连庄数目
	};

	struct GameStatusChangeCard_t
	{
		char	cardArray[PLAYER_COUNT][USERCARD_MAX_COUNT];	//user的卡牌 
		char	countArray[PLAYER_COUNT];						//玩家手牌数量
		char	bankNo;											//庄家
		int		bankcount;										//连庄数目
		char	turnSeatNo;										//轮到谁的，若有动作则为-1
		char	changeCardArray[PLAYER_COUNT][3];				//交换牌(需要判断是否为0，0表示没交换)
		char	changeCardCount[PLAYER_COUNT];					//交换牌数量
		char	afterchangeCardArray[PLAYER_COUNT][3];			//交换牌之后
		char	afterchangeCardCount[PLAYER_COUNT];				//交换牌之后数量
		char	changeCardSezi;									//交换牌筛子
		bool	isAllApply;										//是否所有人都申请了
		int		changeScore[PLAYER_COUNT];						//分数变动，不同花色需要扣分
	};

	struct GameStatusOutCard_t /* 出牌 */
	{
		float	leftTime;
		char	actionCardArray[PLAYER_COUNT][ACTION_MAX_COUNT][10];	/* 0下标存入类型吃碰杠倒，1下标保存点炮对方座位，2下标存卡组卡牌数量，后面存卡牌 */
		char	actionCountArray[PLAYER_COUNT];  /* 动作数量 */
		char	toCardId;		/* 出牌或发牌id */
		char	cardArray[PLAYER_COUNT][USERCARD_MAX_COUNT]; /* user手中的卡牌 */
		char	allOutCardArray[PLAYER_COUNT][60];
		char	allOutCountArray[PLAYER_COUNT];
		char	leftCardCount;	/* 剩余卡牌 */
		char	countArray[PLAYER_COUNT];	/* 玩家手牌数量 */
		char	seatNo;	/* 上一家 */
		char	turnSeatNo;	/* 轮到谁的，若有动作则为-1 */
		char	tingHuCardArray[PLAY_COUNT][USERCARD_MAX_COUNT];
		char	tingHuCountArray[PLAY_COUNT];
		bool	bAction;	/* 是否在等动作 */
		bool	bIsHu[PLAYER_COUNT];	/* 是否胡了 */
		char	huCardId[PLAYER_COUNT];
		char	bankerSeatNo;	/* 庄家 */
		int		bankcount;										//连庄数目
		bool	bTuoGuanArray[PLAYER_COUNT];	/* 是否托管 */
		bool	isHaveHu;
		bool	isHaveChi;
		bool	isHavePeng;
		bool	isHaveGang;
		bool	isHaveTing;
		//int	    TingPaiTiSi[17][17];
		bool	isTingArray[PLAYER_COUNT];	/* 是否听牌 */
		bool	isHaveQiang;
		bool	isHaveDao;
		bool	isHaveYou;
		bool	isHaveYoujin;
		bool	ishaveTianhu;
		bool	ishavezimo;
		int     num;
		char	toGoldCard;
		char    actCount;			/*动作数据长度*/
		char	actCardArray[ACTION_MAX_COUNT][10];/*动作数据*/
		char    btingtip;		/*听牌提示*/
		int     igenda;		//玩法规则（跟打，禁风头）
	};

	struct GameStatusGameFinish_t
	{
		char	allZimoArray[PLAYER_COUNT];			/*所有自摸次数*/
		char	allDianpaoArray[PLAYER_COUNT];		/*所有点炮次数*/
		char allYouJinArray[PLAYER_COUNT];		/*所有游金次数*/
		char	allSanJinDaoArray[PLAYER_COUNT];		/*所有三金倒次数*/
		char	allJiepaoArray[PLAYER_COUNT];		/*所有接炮次数*/
		char	allMinggangArray[PLAYER_COUNT];		/*所有明杠次数*/
		char	allAngangArray[PLAYER_COUNT];		/*所有暗杠次数*/
		bool	isHu[PLAYER_COUNT];					/*是否是胡*/
		bool	isZiMo[PLAYER_COUNT];				/*是否是自摸*/
		bool	isqiangjing[PLAYER_COUNT];			//是否抢金
		bool	issanjindao[PLAYER_COUNT];			//是否三金倒
		bool	istianhu[PLAYER_COUNT];				//是否天胡
		bool	isyoujin[PLAYER_COUNT][3];			//是否游金
		bool	isbahuayou[PLAYER_COUNT];			//是否八花游
		bool    isliuju;							//是否是流局
		int		minggangPan[PLAYER_COUNT];			/*明杠盘*/
		int		angangPan[PLAYER_COUNT];			/*暗杠盘*/
		int		bugangPan[PLAYER_COUNT];			/*补杠盘*/
		int		keziPan[PLAYER_COUNT];				/*刻子盘*/
		int		huaPan[PLAYER_COUNT];				/*花盘*/
		int		jinPan[PLAYER_COUNT];				/*金盘*/
		int		ziPaipengPan[PLAYER_COUNT];			/*字牌盘*/
		int		Allpan[PLAYER_COUNT];				/*总盘数*/
		int		allRoomScoreArray[PLAYER_COUNT];
		int		allPlayerUser[PLAYER_COUNT];
		int		totalScoreArray[PLAYER_COUNT];		/* 总得分 */
		char	finishCardId;						/* 最后一张牌，出的牌或者发的牌 */
		char	actCardArray[PLAYER_COUNT][ACTION_MAX_COUNT][10];	 /* 吃碰杠听，所有动作的倍数，0下标是动作类型，1下标保存点炮对方座位，2下标是卡组数量，后面是卡牌 */
		char	actCountArray[PLAYER_COUNT];		/* 动作数量 */
		char	handCardArray[PLAYER_COUNT][30];	/* 手牌 */
		char	handCountArray[PLAYER_COUNT];		/* 手牌数量*/
		char	huCardId[PLAYER_COUNT];				/* 胡的牌 */
		char	huTypeArray[PLAYER_COUNT];			/* 胡牌类型 */
		char	additionHuTypeArray[PLAYER_COUNT];	/* 附加胡*/
		int  	alltotalScoreArray[PLAYER_COUNT];	/*所有总得分*/
		int		bankUser;							/*庄家位置*/
	};


	//////////////////////////////////////////////////////////////////////////
	/* 游戏协议 */
	//游戏开始
	struct GameProtoC2SGameBegin_t
	{
		bool	bAutoBegin;
	};
	struct GameProtoS2CGameBegin_t
	{
		bool		bAutoBegin;
	};
	//////////////////////////////////////////////////////////////////////////
	//随机骰子
	struct GameProtoS2CRandomDice_t
	{
		char	bankerNo;
		int		bankcount;										//连庄数目
		char	dingZhuangSezi;
		char	bySeziEr;
	};
	//////////////////////////////////////////////////////////////////////////
	//发牌
	struct GameProtoS2CSendCard_t
	{
		float	leftTime;	/* 剩余时间 */
		char	cardArray[PLAYER_COUNT][USERCARD_MAX_COUNT]; /* user的卡牌 */
		char	countArray[PLAYER_COUNT];		/* 玩家手牌数量 */
		char	leftCardCount;	/* 发完所有人的牌后，剩余多少 */
		char	bankNo;	/* 庄家 */
	};
	//发完牌后，提示前端出牌
	struct GameProtoS2CBeginOut_t
	{
		char							wChairID;								//亮花玩家
	};

	//亮花消息
	struct CMD_S_SHOW_FLOWER
	{
		char							wChairID;								//亮花玩家
		char							cbCardCount;							//亮花张数
		char							cbCardData[USERCARD_MAX_COUNT];					//花牌
		char							cbGetCardData[USERCARD_MAX_COUNT];				//补牌
		char	cardArray[PLAYER_COUNT][USERCARD_MAX_COUNT]; /* user的卡牌 */
		char	countArray[PLAYER_COUNT];	/* 玩家手牌数量 */
	};

	//亮花完成消息
	struct CMD_S_SHOW_FLOWER_FINSH
	{
		char							wChairID;								//当前操作玩家
	};

	//麻将消息
	struct CMD_S_CARD_DATA
	{
		char							wCurrentUser;							//用户
		char							cbCardData;								//数据
	};

	//////////////////////////////////////////////////////////////////////////
	//出牌
	struct GameProtoC2SOutCard_t
	{
		char	outCardId;	/* 出的牌 */
	};
	struct GameProtoS2COutCard_t
	{
		float	leftTime;	/* 倒计时时间 */
		char	cardArray[PLAYER_COUNT][USERCARD_MAX_COUNT]; /* user的卡牌 */
		char	outCardId;	/* 出牌 */
		char	countArray[PLAYER_COUNT];		/* 玩家手牌数量 */
		char	tingHuCardArray[PLAY_COUNT][USERCARD_MAX_COUNT];
		char	tingHuCountArray[PLAY_COUNT];
		char	seatNo;		/* 谁出的牌 */
		char	turnSeatNo;	/* 若没有动作发生，则轮到该座位 */
		bool	bAction;	/* 是否等待action发生, 若有则leftTime有效 */
		char    btingtip;		/*听牌提示*/
	};
	//////////////////////////////////////////////////////////////////////////
	//抓牌
	struct GameProtoS2CDispatchCard_t
	{
		char	dispCardId;	/* 发的牌 */
		float	leftTime;	/* 倒计时时间 */
		char	seatNo;	/* 发给谁 */
		bool	isDirect;	/* 抓牌方向，true表示从前头，false表示从末尾抓 */
	};

	/* 听牌 */
	struct GameProtoC2STingPai_t
	{

	};
	struct GameProtoS2CTingPai_t
	{
		char	cardArray[USERCARD_MAX_COUNT]; /* user的卡牌 */
		char	canOutArray[USERCARD_MAX_COUNT]; /* 能够出的牌，出一张后就听 */
		char	cardCount;	/* 手牌数量 */
		char	canOutCount;
		char	seatNo;	/* 对象玩家 */
		char	turnSeatNo;		/* 轮到该座位出牌 */
		char    tinghupaiarray[20];//能胡的牌的数组
		char	tingcout[USERCARD_MAX_COUNT];//能胡的数量
	};
	//////////////////////////////////////////////////////////////////////////
	//吃碰杠动作 响应
	struct GameProtoC2SActionEat_t
	{
		char	actionType;			/* 动作类型，碰杠吃  1-吃、2-碰 3-杠 */
		char	chiCardArray[5];	/* 吃牌时候，选择吃的牌 */
		char	chiCardCount;		/* 吃牌数量，如果为0，表示任意吃一组 */
	};

	struct GameProtoS2CActionEat_t /* 只负责吃碰杠 */
	{
		int		scoreArray[PLAYER_COUNT];	/* 分值变化，例如：杠一下，自己加2，其他减1 */
		char	cardArray[USERCARD_MAX_COUNT];
		char	actionArray[5]; /* 动作卡组, 0下标是卡组数量, 后面是卡组 */
		char	actionType;		/* 动作类型 */
		char	cardCount;
		char	seatNo;			/* 对象玩家 */
		char	turnSeatNo;		/* 轮到该座位 */
	};
	//////////////////////////////////////////////////////////////////////////
	//动作按钮
	struct GameProtoS2CActionAppear_t
	{
		char	seatNo;	 /* 对应的座位 */
		char    actCount;			/*动作数据长度*/
		char	actCardArray[ACTION_MAX_COUNT][10];/*动作数据*/
		char	toCardId;	/* 产生动作的卡牌id */
		bool	isHaveHu;
		bool	isHaveChi;
		bool	isHavePeng;
		bool	isHaveGang;
		bool	isHaveTing;
		bool	isHaveQiang;
		bool	isHaveDao;
		bool	isHaveYou;
		bool	ishaveTianhu;
		bool	ishavezimo;
		char	nyoujingcount;
		bool    isbazhanghua;

	};
	//放弃执行动作
	struct GameProtoC2SPassAction_t
	{

	};
	//////////////////////////////////////////////////////////////////////////
	//托管
	struct GameProtoC2STuoGuan_t
	{
		bool	bTuoGuan;	/* 是否托管 */
	};
	struct GameProtoS2CTuoGuan_t
	{
		char	seatNo;		/* 动作对象 */
		bool	bTuoGuan;	/* 是否托管 */
	};
	//////////////////////////////////////////////////////////////////////////

	//主动胡牌
	struct GameProtoC2SHuPai_t
	{

	};
	typedef GameStatusGameFinish_t GameProtoS2CGameFinish_t;
	//////////////////////////////////////////////////////////////////////////

	//请求剩余手牌
	struct GameProtoC2SRequestLeftCard_t
	{

	};
	struct GameProtoS2CRequestLeftCard_t
	{
		char	leftCardArray[TOTAL_CARD_MAX_COUNT];
		char	leftCount;
	};

	//通知换牌
	struct GameProtoS2CChangeCardTime
	{

	};

	//客户端发送换牌数据
	struct GameProtoC2SChangeCard
	{
		char	seatNo;
		char	cards[3];
	};

	//服务器转发客户端换牌数据
	typedef GameProtoC2SChangeCard GameProtoS2CChangeCard;

	//交换牌结束
	struct GameProtoS2CChangeCardFinish
	{
		char	changeSezi;
		char	changeCard[PLAYER_COUNT][3];	//玩家用于交换的牌
		char	getCard[PLAYER_COUNT][3];		//玩家获得的牌
		char	countArray[PLAYER_COUNT];		//玩家手牌数量
		char	cardArray[PLAYER_COUNT][USERCARD_MAX_COUNT]; //user的卡牌
		int		changeScore[PLAYER_COUNT];		//分数变动，不同花色需要扣分
	};

	struct GameProtoS2CHu
	{
		char	seatNo;
		char	turnSeatNo;
		char	actionCard[ACTION_MAX_COUNT][10];	/* 0下标存入类型吃碰杠倒，1下标保存点炮对方座位，2下标存卡组卡牌数量，后面存卡牌 */
		char	actionCount;						/* 动作数量 */
		char	cardCount;							//玩家手牌数量
		char	cardArray[USERCARD_MAX_COUNT];		//玩家的卡牌
		bool	isZimo;								//是否是自摸
		char	huCard;								//胡的牌
		char	isqiangjing;						//是否抢金
		char	issanjindao;						//是否三金倒
		char	istianhu;							//是否天胡
		bool	isyoujin[3];						//是否游金
		bool	isBahuayou;							//是否八花游


	};

	//跟打提示
	struct GameProtoS2CGenDaTiSi_t
	{
		INT	GenDATiSi[17];		/* 跟打提示数组 */
	};

	//禁风头提示
	struct GameProtoS2CJinFengTouTiSi_t
	{
		int	JinFengTouTiSi[17];		/* 禁风头提示数组 */
	};


	//听牌提示
	struct GameProtoS2CTingPaiTiSi_t
	{
		INT TingPaiTiSi[17][17];
	};

	//尤金提示
	struct GameProtoS2CYouJinTiSi_t
	{
		INT	YouJinCardArray[17];
	};

	//尤金玩家座位号
	struct GameProtoS2CYouJin
	{
		char	seatNo;
	};

	//跟庄
	struct GameProtoS2CFlowZhuang_t 
	{
		int		scoreArray[PLAYER_COUNT];	/* 分值变化 */
		char	seatNo;			/* 对象玩家 */
	};
	//战绩信息
	struct S_C_GameRecordResult
	{
		int JuCount[8][10];	//每局的玩家输赢分数
		int GameCount;

		S_C_GameRecordResult()
		{
			memset(this, 0, sizeof(S_C_GameRecordResult));
		}
	};

	
#pragma pack()
}


#endif
