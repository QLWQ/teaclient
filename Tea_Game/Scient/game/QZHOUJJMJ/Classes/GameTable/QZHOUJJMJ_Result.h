#ifndef QZHOUJJMJ_Result_h__
#define QZHOUJJMJ_Result_h__
#include "QZHOUJJMJ_MessageHead.h"

namespace QZHOUJJMJ
{
	class MJGameResult:public HNLayer
	{
	private:
		struct CPlayerData
		{
			cocos2d::Node*			_mainNode;
			cocos2d::ui::ImageView*	_headImage;
			cocos2d::ui::Text*		_nameText;
			cocos2d::ui::Text*		_idText;
			cocos2d::Node*			_fangZhuTip;
			cocos2d::ui::Text*		_pinghuText;
			cocos2d::ui::Text*		_zimoText;
			cocos2d::ui::Text*		_sanjindaoText;
			cocos2d::ui::Text*		_youjinText;
			cocos2d::ui::Text*		_shuangjinText;
			cocos2d::ui::Text*		_sanjinText;
			cocos2d::ui::Text*		_allRoomScoreText;
			cocos2d::Node*			_curMainNode;
			cocos2d::Node*			_curCardNode;
			cocos2d::ui::Text*		_curNameText;
			cocos2d::ui::TextAtlas*	_curZongFenText;;
			cocos2d::ui::TextAtlas*	_curPanshuText;
			cocos2d::Vec2			_curCardNodePos;
			cocos2d::Node*			_xiangxiLayer;
			cocos2d::ui::ImageView*	 _curIcon;
			cocos2d::ui::ImageView*	 _img_title;
			cocos2d::ui::TextAtlas*	_lab_num;
			cocos2d::ui::ImageView*	_img_zhuang;
			float					_curCardScale;
		};
	public:
		MJGameResult();
		~MJGameResult();

		virtual bool init();
		CREATE_FUNC(MJGameResult);
		
		void setShowUserInfo(UserInfoStruct* pUserInfo, bool isfangzhu, int allRoomScore,int dir);
		void setShowBeiLvInfo(UserInfoStruct* pUserInfo, int pinghuNum,
			int zimoNum, int sanjindaoNum, int youjinNum, int dir);
		void setShowCurCardArray(UserInfoStruct* pUserInfo, const char* pCardArray, char cardCount, const char pActArray[][10], char actCount, char hupaiCardId, int dir);
		void setShowCurFen(UserInfoStruct* pUserInfo, bool isZimo, bool isTianhu, bool IsSanjindao, bool isQiangjin, int zongFen, int dir, bool isYoijin[3], int zongpan, char toHuPaiCardId, bool bahuayou, bool isZhuang);
		void setShowCurPan(int jinpaipan, int huapaipan, int kezipan, int bugangpan, int minggangpai, int angangpan, int zipaipengpan, int dir);
		void ShowReSoultTitle(char hutype);

		void StartAction(char cardId, std::function<void()> func);
		void HideAll();
		void onChickjieCallBack(Ref* pSender, Widget::TouchEventType type);
		void pCloseCurJuNodeFunc(Ref* pSender, Widget::TouchEventType type);
		void onChickMXCallBack(Ref* pSender, Widget::TouchEventType type);
		void showAllResultNode(bool isvisible);
	private:   

		void clearPlayerData();
		
	private:
		cocos2d::Node*				m_pZongChengJiNode;
		cocos2d::Node*				m_pCurChengJiNode;
		Button*				m_btnshare;			//����
		Button*				m_btnshowtotal;			//�ܽ���
		//Button*				m_btnmingxi;			//��ϸ
		std::vector<CPlayerData*>	m_PlayerDataVec;

		std::vector<Node*>	_resultList;
		Sprite*				_bgSprite;
		bool				_IsShowCard;
		bool				_isGameEnd = false;
	};

}



#endif //QZHOUJJMJs_Result_h__



