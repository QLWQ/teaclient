#ifndef _QZHOUJJMJ_MELDEDKONG_H_
#define _QZHOUJJMJ_MELDEDKONG_H_

#include "QZHOUJJMJ_poolaction.h"

namespace QZHOUJJMJ
{

	class MeldedKong :
		public PoolAction
	{
	public:
		MeldedKong(void);
		~MeldedKong(void);

		virtual void run() override;

		CREATE_FUNC(MeldedKong);
	};

}

#endif