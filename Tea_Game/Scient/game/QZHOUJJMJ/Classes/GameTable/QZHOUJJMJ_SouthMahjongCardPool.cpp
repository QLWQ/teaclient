#include "QZHOUJJMJ_SouthMahjongCardPool.h"
#include "QZHOUJJMJ_GameManager.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include <algorithm>

namespace QZHOUJJMJ
{

	SouthMahjongCardPool::SouthMahjongCardPool(void):
		_changeTips(nullptr)
	{

	}


	SouthMahjongCardPool::~SouthMahjongCardPool(void)
	{

	}

	bool SouthMahjongCardPool::init(INT count)
	{
		if (! MahjongCardPool::init())
		{
			return false;
		}
	
		Size size = Director::getInstance()->getWinSize();


		//////////////////////////////////////////////////////////////////////////
		_changeTips = CSLoader::createNode(COCOS_PATH+"ChangeCardTips.csb");
		this->addChild(_changeTips);
		_changeTips->setVisible(false);
		_changeTips->setPosition(cocos2d::Vec2(size.width/2.0f, 100));
		if (_changeTips)
		{
			Button* changeOk = dynamic_cast<Button*>(_changeTips->getChildByName("Button_OK"));
			changeOk->addTouchEventListener(CC_CALLBACK_2(SouthMahjongCardPool::ChangeClickCallBack, this));
		}
		

		_dir = sitDir::SOUTH_DIR;
		m_iMaxZOrder = 1000;
	
		Size winSize = Director::getInstance()->getWinSize();
		_handCard = QZHOUJJMJ_SouthHandCard::create();
		_handCard->setTingCardSize(Size(43.2,40));
		_handCard->setSingleGroupSize(Size(50,70));
		_handCard->setNormalCardSize(Size(72,80));
		//_handCard->setScale(0.9f);
		//_handCard->setPositionY(-50);
		this->addChild(_handCard);


		return true;
	}

	void SouthMahjongCardPool::setHandCardPos(INT catchCard)
	{
		std::vector<CGroupCardData> temp;
		for (auto v:m_GroupCardDataVec)
		{
			temp.push_back(*v);
		}
		_isCatchCard = false;
		if (_isTingState)
		{
			if (catchCard == 0)
			{
				_isCatchCard = false;
			}
			else
			{
				_isCatchCard = true;
			}
		}
		if (_isTingState || catchCard == 0)
		{
			refreshAllShowCard();
		}
		else
		{
			//处理刚刚摸到的牌
			catchCard = *_handCardList.rbegin();
			Vec2 endPos =_handCard->refreshHandCard(temp,_handCardList,catchCard);

			Size size = Director::getInstance()->getWinSize();
			float dif = (size.width-endPos.x-50)/2;
			_handCard->setPositionX(dif);
		}
	}


	int SouthMahjongCardPool::getZhuaPaiZOrder()
	{
		return m_iMaxZOrder + 50;
	}

	void SouthMahjongCardPool::reconnectionAllShowCard()
	{
		std::vector<CGroupCardData> temp;
		for (auto v : m_GroupCardDataVec)
		{
			temp.push_back(*v);
		}


		Vec2 endPos = Vec2::ZERO;
		Size size = Director::getInstance()->getWinSize();
		if (_isTingState)
		{
			if (_isCatchCard)
			{
				endPos = _handCard->refreshHandCard(temp, _handCardList, _handCardList.at(_handCardList.size() - 1));
				_isCatchCard = false;
			}
			else
			{
				endPos = _handCard->refreshHandCardTing(temp, _handCardList);
			}
		}
		else
		{
			//断线重连自己出牌时显示摸牌间隙
			endPos = _handCard->refreshHandAllCard(temp, _handCardList, 0);
		}

		float dif = (size.width - endPos.x) / 2;
		_handCard->setPositionX(dif);
	}

	void SouthMahjongCardPool::refreshAllShowCard()
	{
		std::vector<CGroupCardData> temp;
		for (auto v:m_GroupCardDataVec)
		{
			temp.push_back(*v);
		}


		Vec2 endPos = Vec2::ZERO;
		Size size = Director::getInstance()->getWinSize();
		if (_isTingState)
		{
			if (_isCatchCard)
			{
				endPos = _handCard->refreshHandCard(temp, _handCardList, _handCardList.at(_handCardList.size() - 1));
				_isCatchCard = false;
			}
			else
			{
				endPos = _handCard->refreshHandCardTing(temp, _handCardList);
			}			
		}
		else
		{
			 endPos = _handCard->refreshHandCard(temp,_handCardList,0);
		}

		float dif = (size.width-endPos.x)/2;
		_handCard->setPositionX(dif);
	}

	//清空切后台前发的牌
	void SouthMahjongCardPool::EmptyAllShowCard()
	{
		_handCardList.clear();
	}

	//切回游戏刷新手牌
	void SouthMahjongCardPool::RefreshShowCard()
	{

	}

	
	void SouthMahjongCardPool::TouchCard(Card* tcard)
	{
		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);

		if (southHand)
		{
			if (_isInChangeState)
			{
				southHand->selectChangeCards(tcard);
			}
			else
			{
				bool isCanOut = southHand->setTouchCardIsCanOut(tcard);
				if (isCanOut)
				{
					if (GameManager::getInstance()->getCurrOperDir() == SOUTH_DIR)
					{
						if (tcard->getIsChange())
						{
							_handCard->removeChangeCard(tcard->getCardSumNumber());
						}
						sendOutCard(tcard->getCardSumNumber());
						_outCardPos = convertToWorldSpace(tcard->getPosition());
						//换的牌记录一轮
						_changeCardList.clear();
						_handCard->setLeftChangeCard(_changeCardList);
					}
					else
					{
						getOneCard(tcard->getCardSumNumber());
					}

				}
			}
		}
	}


	void SouthMahjongCardPool::sendOutCard(INT cardValue)
	{
		
		if (GameManager::getInstance()->getCurrOperDir()!=SOUTH_DIR )
		{
			return;
		}

		if (GameManager::getInstance()->getIsHasAction())
		{
			return;
		}

		GameProtoC2SOutCard_t toProtData;
		toProtData.outCardId =cardValue;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_OutCard, &toProtData, sizeof(toProtData));
	}

	void SouthMahjongCardPool::getOneCard(INT cardValue)
	{
		GameProtoC2SOutCard_t toProtData;
		toProtData.outCardId =cardValue;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_RequestOneCard, &toProtData, sizeof(toProtData));
	}

	void SouthMahjongCardPool::ChangeClickCallBack(Ref* ref,Widget::TouchEventType type)
	{
		if (type!= cocos2d::ui::Widget::TouchEventType::BEGAN)
		{
			return;
		}
		
		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);
		if (southHand == nullptr)
		{
			return;
		}

		std::vector<INT>changeCards =  southHand->getChangeCards();
		if (changeCards.size() == 3)
		{
			_changeTips->setVisible(false);

			GameProtoC2SChangeCard toProtData;
			toProtData.seatNo = -1;//服务器会填充;
			for (int i = 0; i < 3; i++)
			{
				toProtData.cards[i] = changeCards.at(i);
			}
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ChangeCard, &toProtData, sizeof(toProtData));
		}
	}


	cocos2d::Vec2 SouthMahjongCardPool::getOutToDeskPos()
	{
		return getCatchPos();
	}
	void SouthMahjongCardPool::YouJin(GameProtoS2CYouJin *pData)
	{
		//尤金
		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);
		if (southHand == nullptr)
		{
			return;
		}
		southHand->YouJin(pData);
	}

	void SouthMahjongCardPool::enterYouJinTis(GameProtoS2CYouJinTiSi_t *pDada)
	{
		//提示尤金
		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);
		if (southHand == nullptr)
		{
			return;
		}
		southHand->TisYouJin(pDada);
	}
	void SouthMahjongCardPool::enterJinFengTou(GameProtoS2CJinFengTouTiSi_t *pData)
	{
		//禁风头
		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);
		if (southHand == nullptr)
		{
			return;
		}

		southHand->TisJinFengTou(pData);
	}

	void SouthMahjongCardPool::enterTingCardTis(GameProtoS2CTingPaiTiSi_t *pData)
	{
		//提示听牌
		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);
		if (southHand == nullptr)
		{
			return;
		}

		southHand->TisTingCard(pData);
	}

	void SouthMahjongCardPool::enterGenda(GameProtoS2CGenDaTiSi_t *pData)
	{
		//进入跟打
		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);
		if (southHand == nullptr)
		{
			return;
		}

		southHand->setHandCardFllowe(pData);

	}
	void SouthMahjongCardPool::enterTingHandle(std::vector<INT>handcards,std::vector<INT>canOutCard, std::vector<INT>canKouCards)
	{
		//_handCardList.clear();
		//for (auto v:handcards)
		//{
		//	_handCardList.push_back(v);
		//}
	
		//sortCard();
	
		Size winSize = Director::getInstance()->getWinSize();
		std::vector<CGroupCardData> temp;
		for (auto v:m_GroupCardDataVec)
		{
			temp.push_back(*v);
		}

		QZHOUJJMJ_SouthHandCard* southHand = dynamic_cast<QZHOUJJMJ_SouthHandCard*>(_handCard);
		if (southHand == nullptr)
		{
			return;
		}

 		float width = southHand->handleTing(_handCardList,canOutCard,canKouCards,temp);
		/*if (width>winSize.width)
		{
			_handCard->setPositionX(winSize.width/2);
		}
		else
		{
			float delt = (winSize.width-width)/2;
			_handCard->setPositionX(winSize.width/2+delt);
		}*/

		
	}

	void SouthMahjongCardPool::showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> handCards,INT huCard,bool visibleAllCard)
	{
		_handCardList.clear();
		for (auto v:handCards)
		{
			_handCardList.push_back(v);
		}

		sortCard();
		moveOneCardToLast(huCard);
		Vec2 endPos = _handCard->refreshHandCardHu(group,_handCardList,huCard,visibleAllCard);

		if (_huTips)
		{
			_huTips->removeFromParent();
			_huTips = nullptr;
		}

		Size size = Director::getInstance()->getWinSize();
		float dif = (size.width-endPos.x)/2;
		_handCard->setPositionX(dif);

	}


	cocos2d::Vec2 SouthMahjongCardPool::getCatchPos()
	{
		return _handCard->getCatchWorldPos();
	}

	void SouthMahjongCardPool::showHuTips(std::vector<INT> huTip)
	{
		if (_huTips)
		{
			_huTips->removeFromParent();
		}
		
		if (huTip.empty())
		{
			return;
		}

		_huTips = Node::create();
		_huTips->setPosition(Vec2(250,150));
		this->addChild(_huTips);

		Size size = Size((huTip.size())*70,110);
		Sprite* hu = Sprite::create(SPRITE_PATH+"img_hu.png");
		hu->setPosition(Vec2(20,size.height/2));
		_huTips->addChild(hu);

		
		ui::Scale9Sprite* bg = ui::Scale9Sprite::create(SPRITE_PATH+"bg_hu.png");
		bg->setAnchorPoint(Vec2(0.0f,0.5f));
		bg->setContentSize(size);
		bg->setPosition(80,size.height/2);
		_huTips->addChild(bg);

		int index = 0;
		for (auto v:huTip)
		{
			MahjongCard* card = MahjongCard::create(DI_SOUTH_STAND,SOUTH_DIR,v);
			card->setAnchorPoint(Vec2(0.0f,0.5f));
			card->setScale(0.8f);
			card->setPosition(Vec2(3+index*70,size.height/2));
			bg->addChild(card);
			index++;
		}
		
	}


	void SouthMahjongCardPool::enterChangeCards()
	{
		CardPool::enterChangeCards();
		if (_changeTips)
		{
			_changeTips->setVisible(true);
		}

		_handCard->enterChangeCards();
	}

	void SouthMahjongCardPool::exitChangeCards(int changeSezi,std::vector<INT>outCard,std::vector<INT> inCard,std::vector<INT> handCardList)
	{
		CardPool::exitChangeCards(changeSezi,outCard,inCard,handCardList);
		if (_changeTips)
		{
			_changeTips->setVisible(false);
		}
	}


	void SouthMahjongCardPool::changeCardToHead(std::vector<INT> cards)
	{
		for (auto v: cards)
		{
			auto itr = std::find(_handCardList.begin(),_handCardList.end(),v);
			if (itr != _handCardList.end())
			{
				_handCardList.erase(itr);
			}
		}

		std::vector<CardPool::CGroupCardData>groupCards;
		for (auto v:m_GroupCardDataVec)
		{
			groupCards.push_back(*v);
		}

		_handCard->refreshHandCard(groupCards,_handCardList,0);

		//把牌摆在前面
		_handCard->putChangeCardToHead(cards);

	}

}