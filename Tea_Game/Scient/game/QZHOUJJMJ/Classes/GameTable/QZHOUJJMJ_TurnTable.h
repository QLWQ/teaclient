#ifndef QZHOUJJMJ_TurnTable_h__
#define QZHOUJJMJ_TurnTable_h__

#include "cocos2d.h"
#include "QZHOUJJMJ_MessageHead.h"
using namespace cocos2d;

namespace  QZHOUJJMJ
{
	class QZHOUJJMJ_TurnTable :public Layer
	{
	public:
		QZHOUJJMJ_TurnTable();
		~QZHOUJJMJ_TurnTable();

		CREATE_FUNC(QZHOUJJMJ_TurnTable);

		void turnTableDir(sitDir dir);
		void setTurnInitDir(sitDir dir);
		void setCardCount(int count);
		void setLeftCardVisble(bool isVisble);
		void setAllinit(sitDir dir);
		void showTimeCountAction(bool isVisble,int time);
		void updateTimeCount(float dt);
	private:
		virtual bool init();

	private:
		Node*	_loader;
		Sprite*	_qianHouSiTips;
		Text*	_qianSiHouSiText;
		TextAtlas* _timeCount;
		TextBMFont* _text_CardNum;
		int _nowTime;
	};
}



#endif // HSMJ_TurnTable_h__
