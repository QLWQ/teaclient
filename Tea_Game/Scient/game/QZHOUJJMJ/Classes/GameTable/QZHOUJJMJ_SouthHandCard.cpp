#include "QZHOUJJMJ_SouthHandCard.h"
#include "QZHOUJJMJ_GameManager.h"
namespace QZHOUJJMJ
{
	static cocos2d::Vec2 beginDirPos = Vec2(0, 20);
	static cocos2d::Vec2 endDirPos = Vec2(1100, 20);

	QZHOUJJMJ_SouthHandCard::QZHOUJJMJ_SouthHandCard()
	{
		_groupSize = Size(74,80);
		_normalSize = Size(72,80);
		_tingSize = Size(72,80);
		memset(m_tingcards, 0, sizeof(m_tingcards));
		_isInChange = false;
	}

	QZHOUJJMJ_SouthHandCard::~QZHOUJJMJ_SouthHandCard()
	{
	}

	cocos2d::Vec2 QZHOUJJMJ_SouthHandCard::getNextPosition(Vec2 prePoint, float deltLen)
	{
		cocos2d::Vec2 nodeDir = endDirPos-beginDirPos;
		nodeDir.normalize();
		nodeDir.scale(deltLen);
		return prePoint+nodeDir;
	}

	bool QZHOUJJMJ_SouthHandCard::init()
	{
		if (!HandCard::init())
		{
			return false;
		}

		return true;
	}


	void QZHOUJJMJ_SouthHandCard::refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards)
	{
		//处理碰杠牌，碰杠牌放左边,扣放右边
		std::vector<CardPool::CGroupCardData> pengGangList;
		for (auto value:groupCards)
		{
			if ( value._iType==CardPool::CGroupCard_AnGang 
				|| value._iType == CardPool::CGroupCard_MingGang
				|| value._iType == CardPool::CGroupCard_Chi
				|| value._iType ==CardPool::CGroupCard_Peng)
			{
				pengGangList.push_back(value);
			}
		}

		//摆牌碰杠
		for(auto pengGang:pengGangList)
		{
			for (int i=0; i<pengGang._iCount; i++)
			{
				/*if (pengGang._iChiCardArray[i] <= 0)
				{
					return;
				}*/
				Card* pCard = nullptr;
				cocos2d::Sprite*ZheZhao = NULL;
				//暗杠不显示（自己显示）
				if (pengGang._iType == CardPool::CGroupCard_AnGang && i<3)
				{
					pCard = GameManager::getInstance()->createBeiPai(SOUTH_DIR);
				}
				else if (pengGang._iType == CardPool::CGroupCard_Chi)
				{
					pCard = GameManager::getInstance()->createPengGangFront(SOUTH_DIR, pengGang._iChiCardArray[i]);
					if (pengGang._iChiCardArray[i] == pengGang._iCardId)
					{
						ZheZhao = Sprite::create(SPRITE_PATH + "chusouthzhezhao.png");
					}
				}
				else
				{
					if (pengGang._iType == CardPool::CGroupCard_Peng)
					{
						if (i == 0)
						{
							ZheZhao = Sprite::create(SPRITE_PATH + "chusouthzhezhao.png");
						}
					}
					pCard = GameManager::getInstance()->createPengGangFront(SOUTH_DIR, pengGang._iCardId);
				}

				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setCardEnableTouch(false);
				pCard->setVisible(true);
				pCard->setGlobalZOrder(100 - i);
				if (ZheZhao != NULL)
				{
					//ZheZhao->setScale(0.9);
					ZheZhao->setPosition(Vec2(36,41));
					pCard->addChild(ZheZhao, 1);
					ZheZhao->setVisible(true);
				}
				if (i==3)
				{
					pCard->setGlobalZOrder(100 + i);
					Vec2 pos = getNextPosition(_prePoint,-_groupSize.width);
					pCard->setPosition(Vec2(pos.x,pos.y+18));
				}
				else
				{
					_prePoint = getNextPosition(_prePoint,_groupSize.width);
					pCard->setGlobalZOrder(100 + i);
					pCard->setPosition(_prePoint);
				}
				this->addChild(pCard);
				_groupCardsList.push_back(pCard);
			}
			_prePoint = getNextPosition(_prePoint,_groupSize.width/3.5f);
		}

	}

	void QZHOUJJMJ_SouthHandCard::refreshLastOutCard(std::vector<INT>handcards, std::vector<INT>canOutCard, bool isCatchCard)
	{
		//牌还没出的手牌
		int index = 0;
		for (auto iter = handcards.begin(); iter != handcards.end(); iter++)
		{
			std::vector<int>::iterator itLast = handcards.end() - 1;
			if (itLast == iter)
			{
				//最后一个刚刚摸到的牌留点空隙
				if (isCatchCard)
				{
					_prePoint = getNextPosition(_prePoint, _normalSize.width*1.2f);
				}
				else
				{
					_prePoint = getNextPosition(_prePoint, _normalSize.width);
				}
			}
			else
			{
				_prePoint = getNextPosition(_prePoint, _normalSize.width);
			}

			Card* pCard = MahjongCard::create(mahjongCreateType::DI_SOUTH_STAND, SOUTH_DIR, *iter);
			pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
			pCard->setGlobalZOrder(100 - index++);
			pCard->setVisible(true);
			this->addChild(pCard);
			pCard->setPosition(_prePoint);
			_handCardsList.push_back(pCard);

			//auto roomInfo = GameManager::getInstance()->getRoomInfo();
			//if ((roomInfo.DeskConfig[3] - 48) == 2)
			//{
			//	//getJinfengtou();
			//}

			bool isDraw = isCanDrawChangeCard(pCard->getCardSumNumber());
			if (isDraw)
			{
				pCard->setChangeColor();
				addDrawChangeCard(pCard->getCardSumNumber());
				pCard->setIsChange(true);
			}
			else
			{
				pCard->setIsChange(false);
			}

			std::vector<INT>::iterator findItr = std::find(canOutCard.begin(), canOutCard.end(), *iter);
			//跟打禁风头听牌优先显示跟打禁风头
			//if (findItr != canOutCard.end()&&GameManager::getInstance()->SetOrGetHuaSwitch(true))
			//{
			//	//可以出
			//	pCard->setCardEnableTouch(true);
			//	pCard->setCardTouchEvent();
			//}
			//else
			//{
			//	//不可以出(听牌也能出其他牌8-10)
			//	//pCard->setCardEnableTouch(false);
			//}
			if (GameManager::getInstance()->SetOrGetHuaSwitch(true))
			{
				if (Vec_CardJFT.size() == 0 && Vec_CardGD.size() == 0)
				{
					pCard->setCardColor(Color3B(255, 255, 255));
					pCard->setCardEnableTouch(true);
					pCard->setCardTouchEvent();
				}
				else
				{
					if (Vec_CardJFT.size() > 0)
					{
						for (auto card : _handCardsList)
						{
							for (auto card1 : Vec_CardJFT)
							{
								if (card->getCardSumNumber() == card1->getCardSumNumber())
								{
									card->setCardColor(Color3B(200, 255, 195));
									card->setTingVisible(true);
									card->setCardEnableTouch(true);
								}
								else
								{
									card->setCardColor(Color3B(128, 128, 128));
									card->setCardEnableTouch(false);
								}
							}
						}
					}
					else if (Vec_CardGD.size() > 0)
					{
						for (auto card : _handCardsList)
						{
							for (auto card1 : Vec_CardGD)
							{
								if (card->getCardSumNumber() == card1->getCardSumNumber())
								{
									card->setCardColor(Color3B(200, 255, 195));
									card->setTingVisible(true);
									card->setCardEnableTouch(true);
								}
								else
								{
									card->setCardColor(Color3B(128, 128, 128));
									card->setCardEnableTouch(false);
								}
							}
						}
					}
				}
			}
		}

		if (handcards.size()>0)
		{
			_prePoint = getNextPosition(_prePoint, _normalSize.width / 3);
		}
	}


	void QZHOUJJMJ_SouthHandCard::refreshOutCard(std::vector<INT>handcards, std::vector<INT>canOutCard, INT catchCard)
	{
		//牌还没出的手牌
		int index = 0;
		for (auto iter = handcards.begin(); iter != handcards.end(); iter++)
		{
            std::vector<int>::iterator itLast = handcards.end() - 1;
			if (itLast == iter)
			{
				//最后一个刚刚摸到的牌留点空隙
				if (*itLast == catchCard)
				{
					_prePoint = getNextPosition(_prePoint,_normalSize.width*1.2f);
				}
				else
				{
					_prePoint = getNextPosition(_prePoint,_normalSize.width);
				}
			}
			else
			{
				_prePoint = getNextPosition(_prePoint,_normalSize.width);
			}

			Card* pCard= MahjongCard::create(mahjongCreateType::DI_SOUTH_STAND, SOUTH_DIR, *iter);
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			pCard->setGlobalZOrder(100 - index++);
			pCard->setVisible(true);
			this->addChild(pCard);
			pCard->setPosition(_prePoint);
			_handCardsList.push_back(pCard);

			//auto roomInfo = GameManager::getInstance()->getRoomInfo();
			//if ((roomInfo.DeskConfig[3] - 48) == 2)
			//{
			//	//getJinfengtou();
			//}

			bool isDraw = isCanDrawChangeCard(pCard->getCardSumNumber());
			if (isDraw)
			{
				pCard->setChangeColor();
				addDrawChangeCard(pCard->getCardSumNumber());
				pCard->setIsChange(true);
			}
			else
			{
				pCard->setIsChange(false);
			}

			std::vector<INT>::iterator findItr = std::find(canOutCard.begin(),canOutCard.end(),*iter);
			//跟打禁风头听牌优先显示跟打禁风头
			//if (findItr != canOutCard.end()&&GameManager::getInstance()->SetOrGetHuaSwitch(true))
			//{
			//	//可以出
			//	pCard->setCardEnableTouch(true);
			//	pCard->setCardTouchEvent();
			//}
			//else
			//{
			//	//不可以出(听牌也能出其他牌8-10)
			//	//pCard->setCardEnableTouch(false);
			//}
			if (GameManager::getInstance()->SetOrGetHuaSwitch(true))
			{
				if (Vec_CardJFT.size() == 0 && Vec_CardGD.size() == 0)
				{
					pCard->setCardColor(Color3B(255, 255, 255));
					pCard->setCardEnableTouch(true);
					pCard->setCardTouchEvent();
				}
				else
				{
					if (Vec_CardJFT.size() > 0)
					{
						for (auto card : _handCardsList)
						{
							for (auto card1 : Vec_CardJFT)
							{
								if (card->getCardSumNumber() == card1->getCardSumNumber())
								{
									card->setCardColor(Color3B(200, 255, 195));
									card->setTingVisible(true);
									card->setCardEnableTouch(true);
								}
								else
								{
									card->setCardColor(Color3B(128, 128, 128));
									card->setCardEnableTouch(false);
								}
							}
						}
					}
					else if (Vec_CardGD.size() > 0)
					{
						for (auto card : _handCardsList)
						{
							for (auto card1 : Vec_CardGD)
							{
								if (card->getCardSumNumber() == card1->getCardSumNumber())
								{
									card->setCardColor(Color3B(200, 255, 195));
									card->setTingVisible(true);
									card->setCardEnableTouch(true);
								}
								else
								{
									card->setCardColor(Color3B(128, 128, 128));
									card->setCardEnableTouch(false);
								}
							}
						}
					}
				}
			}
		}

		if (handcards.size()>0)
		{
			_prePoint = getNextPosition(_prePoint,_normalSize.width/3);
		}
	}

	void QZHOUJJMJ_SouthHandCard::getJinfengtou()
	{
		//禁风头
		bool bTouch = true;

		for (auto card : _handCardsList)
		{
			if (card->getCardSumNumber() >= 31 && card->getCardSumNumber() <= 37)
			{
				bTouch = false;
			}
		}

		std::vector<Card*> pCardJFT;
		for (auto card : _handCardsList)
		{
			if (card->getCardSumNumber() >= 31 && card->getCardSumNumber() <= 37)
			{
				card->setCardColor(Color3B(200, 255, 195));
				card->setTingVisible(true);
				card->setCardEnableTouch(true);
				//card->setSelect(true);
				pCardJFT.push_back(card);
				break;
			}
		/*	else
			{
				card->setCardColor(Color3B(128, 128, 128));
				card->setCardEnableTouch(false);
				card->setSelect(false);
			}*/

			for (auto cardFeng : pCardJFT)
			{
				if (card->getCardSumNumber() == cardFeng->getCardSumNumber())
				{
					continue;
				}
				else
				{
					card->setCardColor(Color3B(200, 255, 195));
					card->setCardEnableTouch(false);
					//card->setSelect(false);
				}
			}

			if (bTouch)
			{
				card->setCardColor(Color3B(200, 255, 195));
				card->setCardEnableTouch(true);
				//card->setSelect(true);
			}

			for (auto card : _handCardsList)
			{
				auto a = card->getCardSumNumber();
				auto count = 0;
				for (auto card1 : _handCardsList)
				{
					if (a == card1->getCardSumNumber())
					{
						count++;
					}
					//这里就是有这个对子了
					if (count == 2)
					{
						card->setCardColor(Color3B(128, 128, 128));
						//card->setSelect(false);
						card1->setCardColor(Color3B(128, 128, 128));
						//card1->setSelect(false);
					}
				}
			}
		}
	}
	

	void QZHOUJJMJ_SouthHandCard::resetData()
	{
		m_isTing = false;
		pCardYJTS.clear();
		Vec_CardJFT.clear();
		Vec_CardGD.clear();
		memset(m_YouJinCardArray, 0, sizeof(m_YouJinCardArray));
		memset(m_tingcards, 0, sizeof(m_tingcards));
		for (auto v:_groupCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_handCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}

		_groupCards.clear();
		_groupCardsList.clear();
		_handCardsList.clear();
		_selectChangeCards.clear();
		_changeCardsInHead.clear();
		_drawChangeCard.clear();
		_prePoint=beginDirPos;
		
	}

	Vec2 QZHOUJJMJ_SouthHandCard::refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>normalCards, INT catchCard)
	{
		resetData();
		_groupCards.clear();
		for (auto v : groupCards)
		{
			_groupCards.push_back(v);
		}

		refreshPengGang(groupCards);

		//都可以出
		refreshOutCard(normalCards,normalCards,catchCard);

		Vec2 pos = Vec2(_prePoint.x+_normalSize.width,_prePoint.y);
		return pos;
		
	}

	Vec2 QZHOUJJMJ_SouthHandCard::refreshHandAllCard(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>normalCards, INT catchCard)
	{
		resetData();
		_groupCards.clear();
		for (auto v : groupCards)
		{
			_groupCards.push_back(v);
		}

		refreshPengGang(groupCards);

		//都可以出
		refreshLastOutCard(normalCards, normalCards, true);

		Vec2 pos = Vec2(_prePoint.x + _normalSize.width, _prePoint.y);
		return pos;
	}

	
	Vec2 QZHOUJJMJ_SouthHandCard::refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>tingCards)
	{
		
		resetData();
		refreshPengGang(groupCards);


		//牌还没出的手牌
		int index = 0;
		std::vector<cocos2d::Node*> nodelist;
		for (auto iter = tingCards.begin(); iter != tingCards.end(); iter++)
		{
			_prePoint = getNextPosition(_prePoint,_tingSize.width*1.7f);
			Card* pCard=pCard= MahjongCard::create(mahjongCreateType::DI_SOUTH_STAND, SOUTH_DIR, *iter);
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			pCard->setGlobalZOrder(100 - index++);
			pCard->setVisible(true);
			pCard->setCardEnableTouch(false);
			this->addChild(pCard);
			pCard->setPosition(_prePoint);
			//pCard->setCardTouchEvent();

			_handCardsList.push_back(pCard);
		}

		if (tingCards.size()>0)
		{
			_prePoint = getNextPosition(_prePoint,_tingSize.width/3);
		}

		Vec2 pos = Vec2(_prePoint.x+_tingSize.width,_prePoint.y);
		return pos;
		
	}


	Vec2 QZHOUJJMJ_SouthHandCard::refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>handCards, INT huCardId, bool visibleAllCard)
	{
		resetData();
		_groupCards.clear();
		for (auto v:groupCards)
		{
			_groupCards.push_back(v);
		}

		refreshPengGang(groupCards);

		for (auto iter = handCards.begin(); iter != handCards.end(); iter++)
		{
			if (*iter == 0)
			{
				//过滤结算报错（数据为空）
				break;
			}
			_prePoint = getNextPosition(_prePoint, _groupSize.width);
			if (visibleAllCard)
			{
				Card* pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_SOUTH, SOUTH_DIR,*iter);
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setGlobalZOrder(100);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);

				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard->setHuVisible(true);
				}

			}
			else
			{
				Card* pCard	= nullptr;
				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_SOUTH, SOUTH_DIR,*iter);
					pCard->setHuVisible(true);
				}
				else
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_SOUTH_BACK, SOUTH_DIR);
				}
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setGlobalZOrder(100);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);
			}
			
		}
		
		Vec2 pos = Vec2(_prePoint.x + _groupSize.width, _prePoint.y);
		return pos;
	}

	void QZHOUJJMJ_SouthHandCard::setSingleGroupSize(Size groupSize)
	{
		_groupSize = groupSize;
	}

	void QZHOUJJMJ_SouthHandCard::setNormalCardSize(Size normalSize)
	{
		_normalSize = normalSize;
	}

	void QZHOUJJMJ_SouthHandCard::setTingCardSize(Size tingSize)
	{
		_tingSize = tingSize;
	}

	bool QZHOUJJMJ_SouthHandCard::setTouchCardIsCanOut(Card* tCard)
	{
		if (tCard == nullptr)
		{
			return false;
		}

		for (auto card : _handCardsList)
		{
			if (card != tCard)
			{
			card->setSelect(false);
			card->setCardColor(Color3B(255, 255, 255));
			card->setPosition(card->getPositionX(),beginDirPos.y);
			}
		}

		if (tCard->getSelect())
		{
			GameManager::getInstance()->getOutCardByNum(0);
			if (GameManager::getInstance()->getCurrOperDir() != SOUTH_DIR)
			{
				tCard->setSelect(false);
				tCard->setCardColor(Color3B(255, 255, 255));
				tCard->setPosition(tCard->getPositionX(),beginDirPos.y);
			}
			else
			{
				//选中
				//tCard->setPosition(tCard->getPositionX(), beginDirPos.y + 20);
				return true;
			}
		}
		else
		{
			tCard->setCardColor(Color3B(200, 255, 195));   // 淡绿色
			//tCard->setPosition(tCard->getPositionX(),beginDirPos.y+20);
			tCard->setPosition(tCard->getPositionX(), 40);
			tCard->setSelect(true);
			GameManager::getInstance()->getOutCardByNum(tCard->getCardSumNumber());

			//if (m_cards>0)
			{
				bool b = false;
				m_pActArray[USERCARD_MAX_COUNT] = {};
				int count = 0;
				int curRow = 0; //当前行
				for (int i = 0; i < 17; i++)
				{
					if (tCard->getCardSumNumber() == m_tingcards[i][0])
					{
						curRow = i;
						for (int j = 1; j < 17; j++)
						{
							if (m_tingcards[curRow][j]>0)
							{
								m_pActArray[count] = m_tingcards[curRow][j];
								b = true;
								count++;
							}
						}
						break;
					}
				}
				if (b && m_isTing == true)
				{
					GameManager::getInstance()->setTingLayer(0, m_pActArray, count); //显示听牌提示
				}
				else
				{
					if (pCardYJTS.size() > 0)
					{
						GameManager::getInstance()->setTingLayer(1, 0, 0); //显示游金提示
					}
					else
					{
						GameManager::getInstance()->setTingLayer(0, m_pActArray, count); //取消显示听牌提示
					}
				}
			}
		}
		return false;

	}

	void QZHOUJJMJ_SouthHandCard::selectKouCard(Card* tCard)
	{
		if (tCard == nullptr)
		{
			return ;
		}

		for (auto card : _handCardsList)
		{
			if (card != tCard)
			{
				card->setSelect(false);
				card->setCardColor(Color3B(255, 255, 255));
			}
		}

		bool isSelect = !tCard->getSelect();
		tCard->setSelect(isSelect);
	
		//同样牌扣三个
		int count = 0;
		for (auto card : _handCardsList)
		{
			if (count>=3)
			{
				break;
			}
			if (card->getCardSumNumber() == tCard->getCardSumNumber())
			{
				card->setSelect(isSelect);
				if (card->getSelect())
				{
					card->setCardColor(Color3B(200, 255, 195));   // 淡绿色
				}
				else
				{
					card->setCardColor(Color3B(255, 255, 255));
				}
				count++;
			}
		}
		
	}

	void QZHOUJJMJ_SouthHandCard::selectChangeCards(Card* tCard)
	{
		if (tCard == nullptr)
		{
			return ;
		}

		bool isSelect = !tCard->getSelect();
		if (isSelect)
		{
			if (_selectChangeCards.size()>=3)
			{
				return;
			}
			_selectChangeCards.push_back(tCard);
		}
		else
		{
			std::vector<Card*>::iterator itr = std::find(_selectChangeCards.begin(),_selectChangeCards.end(),tCard);
			if (itr != _selectChangeCards.end())
			{
				_selectChangeCards.erase(itr);
			}
		}

		tCard->setSelect(isSelect);
		if (tCard->getSelect())
		{
			tCard->setCardColor(Color3B(200, 255, 195));   // 淡绿色
			//tCard->setPositionY(40);
		}
		else
		{
			tCard->setCardColor(Color3B(255, 255, 255));
			//tCard->setPositionY(20);
		}
	}

	cocos2d::Vec2 QZHOUJJMJ_SouthHandCard::getCatchWorldPos()
	{
		return  getNextPosition(_prePoint,_normalSize.width);
	}

	void QZHOUJJMJ_SouthHandCard::YouJin(GameProtoS2CYouJin *pData)
	{
		GameManager::getInstance()->setCurrOperDir(GTLogic()->getUserDir(pData->seatNo));
		sitDir turnDir = GTLogic()->getUserDir(pData->seatNo);
		if (turnDir == SOUTH_DIR)
		{
			GameManager::getInstance()->setTingLayer(1, 0, 0);
		}
		
		for (int j = 0; j < 17; j++)
		{
			if (m_YouJinCardArray[j])
			{
				for (auto card : _handCardsList)
				{
					if (card->getCardSumNumber() == m_YouJinCardArray[j])
					{
						pCardYJTS.push_back(card);
					}
					else
					{
						card->setCardEnableTouch(false);
						card->setSelect(false);
					}
				}
				for (auto card : _handCardsList)
				{
					for (auto card1 : pCardYJTS)
					{
						if (card->getCardSumNumber() == card1->getCardSumNumber())
						{
							card->setCardColor(Color3B(200, 255, 195));
							card->setCardEnableTouch(true);
							//card->setSelect(true);
							card->setCardTouchEvent();
							card->setTingVisible(true);
						}
					}
				}
			}
		}
	}

	void QZHOUJJMJ_SouthHandCard::TisYouJin(GameProtoS2CYouJinTiSi_t *pData)
	{
		if (pData == nullptr)
		{
			return;
		}
		for (int i = 0; i < 17; i++)
		{
			m_YouJinCardArray[i] = pData->YouJinCardArray[i];
		}
		for (int j = 0; j < 17; j++)
		{
			if (m_YouJinCardArray[j])
			{
				for (auto card : _handCardsList)
				{
					if (card->getCardSumNumber() == m_YouJinCardArray[j])
					{
						pCardYJTS.push_back(card);
					}
					else
					{
						card->setCardEnableTouch(false);
						card->setSelect(false);
					}
				}
				for (auto card : _handCardsList)
				{
					for (auto card1 : pCardYJTS)
					{
						if (card->getCardSumNumber() == card1->getCardSumNumber())
						{
							card->setCardColor(Color3B(200, 255, 195));
							card->setCardEnableTouch(true);
							//card->setSelect(true);
							card->setCardTouchEvent();
							card->setTingVisible(true);
						}
					}
				}
			}
		}
	}
	void QZHOUJJMJ_SouthHandCard::TisJinFengTou(GameProtoS2CJinFengTouTiSi_t *pData)
	{
		//禁风头标识gjd
		Vec_CardJFT.clear();
		for (auto card : _handCardsList)
		{
			for (int i = 0; i < 17; i++)
			{
				if (card->getCardSumNumber() == pData->JinFengTouTiSi[i])
				{
					Vec_CardJFT.push_back(card);
				}
				else
				{
					card->setCardColor(Color3B(128, 128, 128));
					card->setSelect(false);
					card->setCardEnableTouch(false);
				}
			}
		}

		for (auto card : _handCardsList)
		{
			for (auto card1 : Vec_CardJFT)
			{
				if (card->getCardSumNumber() == card1->getCardSumNumber())
				{
					card->setCardColor(Color3B(200, 255, 195));
					card->setTingVisible(true);
					card->setCardEnableTouch(true);
					//card->setSelect(true);
				}
			}
			if (Vec_CardJFT.size() == 0)
			{
				card->setCardColor(Color3B(255, 255, 255));
				card->setCardEnableTouch(true);
				//card->setSelect(true);
			}
		}

	}

	void QZHOUJJMJ_SouthHandCard::TisTingCard(GameProtoS2CTingPaiTiSi_t *pData)
	{
		if (pData == nullptr)
		{
			return;
		}
		GameProtoS2CTingPaiTiSi_t* data = pData;
		memset(m_tingcards, 0, sizeof(m_tingcards));

		for (int i = 0; i < 17; i++)
		{
			for (int  j = 0; j < 17; j++)
			{
				m_tingcards[i][j] = data->TingPaiTiSi[i][j];
				if (m_tingcards[i][1]>0)
				{
					m_isTing = true;
				}
			}
		}
	}

	void QZHOUJJMJ_SouthHandCard::setHandCardFllowe(GameProtoS2CGenDaTiSi_t *pData)
	{
		//跟打标识gjd6.17
		Vec_CardGD.clear();
		for (int i = 0; i < 17; i++)
		{
			for (auto card : _handCardsList)
			{
				if (card->getCardSumNumber() == pData->GenDATiSi[i])
				{
					/*card->setCardColor(Color3B(200, 255, 195));
					card->setTingVisible(true);*/
					Vec_CardGD.push_back(card);
				}
				else
				{
					card->setCardColor(Color3B(128, 128, 128));
					card->setCardEnableTouch(false);
				}
			}
		}
		for (auto card : _handCardsList)
		{
			for (auto card1 : Vec_CardGD)
			{
				if (card->getCardSumNumber() == card1->getCardSumNumber())
				{
					card->setCardColor(Color3B(200, 255, 195));
					card->setTingVisible(true);
					card->setCardEnableTouch(true);
					//card->setSelect(true);
				}
			}
			if (Vec_CardGD.size() == 0)
			{
				card->setCardColor(Color3B(255, 255, 255));
				card->setCardEnableTouch(true);
				//card->setSelect(true);
			}
		}

	}

	float QZHOUJJMJ_SouthHandCard::handleTing(std::vector<INT>handcards, std::vector<INT>canOutCard, std::vector<INT>canKouCards, std::vector<CardPool::CGroupCardData>groupCards)
	{
		
		resetData();
		_groupCards.clear();
		for (auto v:groupCards)
		{
			_groupCards.push_back(v);
		}
		refreshPengGang(_groupCards);			//--？？？
		
		if (canKouCards.empty())
		{
			//听出牌
			refreshLastOutCard(handcards, canOutCard, true);

			for (auto card : _handCardsList)
			{
				auto itr = std::find(canOutCard.begin(),canOutCard.end(),card->getCardSumNumber());
				if (itr != canOutCard.end())
				{
					card->setTingStateVisible(true); //显示打出牌的标识。6.17
				}
				//card->setCardEnableTouch(true);
			}
		}
		else
		{
			refreshLastOutCard(handcards, canKouCards, true);
			for (auto card : _handCardsList)
			{
				auto itr = std::find(canKouCards.begin(),canKouCards.end(),card->getCardSumNumber());
				if (itr != canKouCards.end())
				{
					card->setKouVisible(true);
				}
			}
		}
		
		return 0.0f;
	}


	std::vector<INT> QZHOUJJMJ_SouthHandCard::getChangeCards()
	{
		std::vector<INT> temp;
		for (auto v:_selectChangeCards)
		{
			temp.push_back(v->getCardSumNumber());
		}

		return temp;
	}

	void QZHOUJJMJ_SouthHandCard::enterChangeCards()
	{
		_isInChange = true;
		_selectChangeCards.clear();

		for (auto v:_handCardsList)
		{
			v->setCardColor(Color3B(255, 255, 255));
			v->setSelect(false);
		}
	}

	void QZHOUJJMJ_SouthHandCard::exitChangeCards()
	{
		HandCard::exitChangeCards();
		_isInChange = false;
	}

	void QZHOUJJMJ_SouthHandCard::putChangeCardToHead(std::vector<INT> cards)
	{
		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}
		_changeCardsInHead.clear();
		_selectChangeCards.clear();

		float startX = Director::getInstance()->getWinSize().width/2;
		int index = -2;
		std::vector<Point> v_pos_temp;
		for (auto v:cards)
		{
			auto pCard = MahjongCard::create(DI_SOUTH_BACK,SOUTH_DIR);
			pCard->setPosition(Vec2(startX+index*48,50));
			pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
			this->addChild(pCard);
			_changeCardsInHead.push_back(pCard);
			MoveBy* moveBy = MoveBy::create(0.5f,Vec2(0,150));
			pCard->runAction(moveBy);
			v_pos_temp.push_back(pCard->getPosition()+Vec2(0,150));
			index++;
		}
		GameManager::getInstance()->addChangeCardPos(sitDir::SOUTH_DIR,v_pos_temp);

	}

	void QZHOUJJMJ_SouthHandCard::setHandCardTouch(bool isTouch)
	{
		for (auto card:_handCardsList)
		{
			card->setCardEnableTouch(isTouch);
		}
	}

	void QZHOUJJMJ_SouthHandCard::moveChangeCard(sitDir dir)
	{
		int i = 0;
		for (auto v:_changeCardsInHead)
		{
			switch (dir)
			{
			case QZHOUJJMJ::WEST_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CW, bezDir::WEST_SOUTH_DIR, sitDir::WEST_DIR, i);
				break;
			case QZHOUJJMJ::EAST_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CCW, bezDir::EAST_SOUTH_DIR, sitDir::EAST_DIR, i);
				break;
			case QZHOUJJMJ::NORTH_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::STRAIGHT, bezDir::BEZ_MID_DIR, sitDir::NORTH_DIR, i);
				break;
			default:
				break;
			}
			i++;
		}
		_changeCardsInHead.clear();
	}

}
