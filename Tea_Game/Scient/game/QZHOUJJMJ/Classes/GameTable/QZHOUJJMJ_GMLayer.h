#ifndef QZHOUJJMJ_GMLayer_h__
#define QZHOUJJMJ_GMLayer_h__
#include "HNNetExport.h"

namespace QZHOUJJMJ
{
	class QZHOUJJMJ_GMLayer : public HNLayer
	{
	public:
		QZHOUJJMJ_GMLayer();
		~QZHOUJJMJ_GMLayer();

		CREATE_FUNC(QZHOUJJMJ_GMLayer);

		void setUIData(std::vector<UserInfoStruct> users,std::vector<std::vector<INT>> cards);
	private:
		virtual bool init() override;
		void clickCloseEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		
		bool LeftCardCallBack(HNSocketMessage* socketMessage);

	private:
		cocos2d::Node*	m_pMainNode;
	};
}



#endif // HSMJ_GMLayer_h__
