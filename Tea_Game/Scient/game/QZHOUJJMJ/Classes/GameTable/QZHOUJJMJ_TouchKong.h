#ifndef _QZHOUJJMJ_TOUCHKONG_H_
#define _QZHOUJJMJ_TOUCHKONG_H_

#include "QZHOUJJMJ_poolaction.h"

namespace QZHOUJJMJ
{

	class TouchKong :
		public PoolAction
	{
	public:
		TouchKong(void);
		~TouchKong(void);

		virtual void run() override;

		CREATE_FUNC(TouchKong);
	};

}

#endif
