#ifndef _QZHOUJJMJ_FACTORYCARDPOOL_H_
#define _QZHOUJJMJ_FACTORYCARDPOOL_H_

#include "QZHOUJJMJ_CardPool.h"
#include "QZHOUJJMJ_MahjongCardPool.h"
#include "QZHOUJJMJ_SouthMahjongCardPool.h"
#include "QZHOUJJMJ_WestMahjongCardPool.h"
#include "QZHOUJJMJ_EastMahjongCardPool.h"
#include "QZHOUJJMJ_NorthMahjongCardPool.h"

#include "QZHOUJJMJ_TouchCard.h"
#include "QZHOUJJMJ_MeldedKong.h"
#include "QZHOUJJMJ_ConcealedKong.h"
#include "QZHOUJJMJ_TouchKong.h"
#include "QZHOUJJMJ_HuCard.h"
#include "QZHOUJJMJ_EatCard.h"

namespace QZHOUJJMJ
{

	class Factory
	{
	public:
		static CardPool* createEastPool(INT count);
		static CardPool* createWestPool(INT count);
		static CardPool* createSouthPool(INT count);
		static CardPool* createNorthPool(INT count);

		static PoolAction* createTouchCardAction();
		static PoolAction* createMeldedKongAction();
		static PoolAction* createConcealedKongAction();
		static PoolAction* createTouchKongAction();
		static PoolAction* createTouchEatAction();
		static PoolAction* createHuCardAction();
	};

}

#endif