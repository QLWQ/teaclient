﻿#include "QZHOUJJMJ_GameTableLogic.h"
#include "QZHOUJJMJ_GameTableUI.h"
#include "QZHOUJJMJ_GameTableUICallback.h"
#include "QZHOUJJMJ_GameManager.h"
#include "QZHOUJJMJ_Card.h"
#include "tinyxml2/tinyxml2.h"
#include "QZHOUJJMJ_Protocol.h"
#include "QZHOUJJMJ_Result.h"

namespace QZHOUJJMJ
{

using namespace cocostudio;
using namespace std;
using namespace ui;
using namespace HN;

	GameTableLogic* GameTableLogic::_instance = nullptr;

	GameTableLogic::GameTableLogic(GameTableUICallBack* uiCallback,BYTE deskNo,bool bAutoCreate) 
		: HNGameLogicBase(deskNo, PLAY_COUNT, bAutoCreate, uiCallback)
	{
		_callBack = (GameTableUI *)uiCallback;
		_instance = this;
		_hasSetGameStation = false;
		_userInfo = RoomLogic()->loginResult.pUserInfoStruct;

	}

	GameTableLogic::~GameTableLogic()
	{
	}
	/*-----------------------------------------------------------------------------------------------*/

	void GameTableLogic::dealGameStartResp(INT bDeskNO)
	{
		_callBack->showWechat(false);
		_callBack->showBackBtn(false);
	}

	void GameTableLogic::dealGameClean()
	{
		//关闭游戏结算
		_callBack->onCloseResult();
		GameManager::getInstance()->onGameStartClean();
	}

	void GameTableLogic::dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum)
	{
		GameManager::getInstance()->ContestRoundNum(contestRoundNum);
	}
	void GameTableLogic::dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum)
	{
		GameManager::getInstance()->ContestInnings(contestRoundNum);
	}
	void GameTableLogic::dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum)
	{
		GameManager::getInstance()->ContestInfoupdate(contestRoundNum);
	}

	void GameTableLogic::dealGameEndResp(INT bDeskNO)
	{
		
	}

	void GameTableLogic::dealUserAgreeResp(MSG_GR_R_UserAgree* agree)
	{
		onUserAgree(*((MSG_GR_R_UserAgree*)agree));
	}


	//处理玩家坐下
	void GameTableLogic::dealUserSitResp(MSG_GR_R_UserSit * userSit,  const UserInfoStruct* user) //6.11玩家坐下。
	{
		HNGameLogicBase::dealUserSitResp(userSit, user);
		auto deskUser = getUserByUserID(user->dwUserID);
		bool isMe = (user->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
		if (isMe)
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				_callBack->removeUser(getUserDir(i));
			}

			loadUsers();
		}
		else
		{
			BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
			_callBack->addUser(getUserDir(userSit->bDeskStation), deskUser);
			// 显示房主标志
			if (userSit->bDeskMaster)
			{
				//GameManager::getInstance()->setFangzhu(seatNo);
			}
		}
		_callBack->UpDateUserLocation(0);
	}

	// 用户站起
	void GameTableLogic::dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		if (!_hasSetGameStation)
		{
			return;
		}
		// 清除庄家显示
		for (auto i = 0; i < PLAY_COUNT; i++)
		{
			COCOS_NODE(Sprite, StringUtils::format("zhuang%d", i))->setVisible(false);          // 庄家
		}
		// 清除本地界面的ui显示
		if (userSit->dwUserID == _userInfo.dwUserID)
		{
			

			_callBack->dealLeaveDesk();
		}
		else
		{
			if (userSit->bDeskStation >= 0 && userSit->bDeskStation <= 3)
				_callBack->removeUser(getUserDir(userSit->bDeskStation));
		}
		
	}


	// 游戏状态
	void GameTableLogic::dealGameStationResp(void* object, INT objectSize)
	{
		setGameStation(object, objectSize);
	}

	//设置回放模式
	void GameTableLogic::dealGamePlayBack(bool isback)
	{
		GameManager::getInstance()->setPlayBack(isback);
	}

	//房主消息
	void GameTableLogic::dealDeskOwnertResp(void* object, INT objectSize)
	{
		/*HNGameLogicBase::dealDeskOwnertResp(object,objectSize);
		_callBack->setDissloveBtState();
		std::vector<int> defines = AnalysisUserDefine(_userDefined);
		GameManager::getInstance()->setUserDefine(defines);*/
		

	}

	// 游戏消息（游戏的主体消息来自这里）
	void GameTableLogic::dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		int test = _mySeatNo;
		if (_hasSetGameStation == false)
		{
			return;
		}
		switch(messageHead->bAssistantID)
		{
		case GAME_PROTOCOL_S2C_GameBegin:
			{
				GameManager::getInstance()->playCommonSound("Begin");
				dealGameBegin((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_RandomDice: //定庄
			{
				GameProtoS2CRandomDice_t* pProtData = (GameProtoS2CRandomDice_t*)object;
				auto dir = getUserDir(pProtData->bankerNo);
				auto catchDir = getUserDir((pProtData->bankerNo + pProtData->dingZhuangSezi)%PLAY_COUNT);
				if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
				{
					// 比赛开赛消息使用消息中心通知出去，比赛控制中心接收显示开赛动画
					EventCustom event(RECONNECTION);
					Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
				}
				GameManager::getInstance()->resetBeginStageUI(pProtData->bankerNo,dir,catchDir,pProtData->dingZhuangSezi,pProtData->bankcount);
			}
			break;
		case GAME_PROTOCOL_S2C_SendCard:
			{
				dealOnSendAllCardEx((char*)object, objectSize);
			}
			break;
		case  GAME_PROTOCOL_S2C_SHOWFLOWER:
			{
				dealShowCardFlowerEx((char*)object, objectSize);
			}
			break;
		case  GAME_PROTOCOL_S2C_SHOWFLOWER_FINSHG:
			{

				dealShowCardFlowerFinshEx((char*)object, objectSize);
			}
			break;
		
		case  GAME_PROTOCOL_S2C_Send_GoldData:
			{

				dealShowGoldCardEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_ChangeCardBegin:
			{
				GameProtoS2CChangeCardTime* pProtData = (GameProtoS2CChangeCardTime*)object;
				//CCLOG("AAAAAAAAAAAAAA");
				GameManager::getInstance()->changeCardBegin();
			}
			break;
		case GAME_PROTOCOL_S2C_ChangeCardFinish:
			{
				CCAssert(sizeof(GameProtoS2CChangeCardFinish) == objectSize, "Broken message GameProtoS2CChangeCardFinish.");
				GameProtoS2CChangeCardFinish* pProtData = (GameProtoS2CChangeCardFinish*)object;
				//CCLOG("AAAAAAAAAAAAAA");
				GameManager::getInstance()->changeCardEnd(pProtData);
			}
			break;
		case GAME_PROTOCOL_S2C_ChangeCard:
			{
				CCAssert(sizeof(GameProtoS2CChangeCard) == objectSize, "Broken message GameProtoS2CChangeCard.");

				GameProtoS2CChangeCard* pProtData = (GameProtoS2CChangeCard*)object;
				auto dir = getUserDir(pProtData->seatNo);
				GameManager::getInstance()->changeCardToHead(dir,pProtData->cards,3);
				//CCLOG("AAAAAAAAAAAAAA");	
			}
			break;
		case GAME_PROTOCOL_S2C_BeginOut:
			{
				GameManager::getInstance()->playGame();
				GameManager::getInstance()->setRefreshBtn(true);
				dealGameBegin((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_OutCard:
			{
				dealOnOutCardEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_DispatchCard:
			{
				dealOnZhuaCardEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_ActionAppear:
			{
				dealOnActionAppearEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_ActionEat:
			{
				//CCLOG("AAAAAAAAAAAAAA-----EAT");
				dealOnActionEatEx((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_Hu:
			{
				dealOnPlayerHu((char*)object,objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_GameFinish:
			{
				//每局结束检查活动进展情况
				if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
				{
					GameManager::getInstance()->CheckActivity();
				}
				GameManager::getInstance()->setRefreshBtn(false);
				dealOnGameFinish((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_TingPai://先
			{
				dealOnTingPaiEx((char*)object, objectSize);
			}break;
		case GAME_PROTOCOL_S2C_FlowZhuang:
			{
				dealOnFlowZhuang((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_GenDaTiSi:
			{
				dealOnFollowPlay((char*)object, objectSize);//跟打
			}
			break;
		case GAME_PROTOCOL_S2C_JinFengTouTiSi:
			{
				dealOnJinFengTou((char*)object, objectSize);//禁风头
			}
			break;
		case GAME_PROTOCOL_S2C_TingPaiTiSi://听牌提示
			{
				dealOnFollowTingTis((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_YouJinTiSi://尤金提示(标记)
			{
				dealOnYouJinTis((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_C2S_YOUJING:
			{
				dealOnYouJin((char*)object, objectSize);
			}
			break;
		case GAME_PROTOCOL_S2C_TuoGuan:
			{
				dealAuto((char*)object, objectSize);
		}break;
		case S_C_GAME_RECORD_RESULT:
		{
			CHECK_SOCKET_DATA(S_C_GameRecordResult, sizeof(S_C_GameRecordResult), "S_C_GameRecordResult size error!");
			S_C_GameRecordResult* pSuperUser = (S_C_GameRecordResult*)(object);
			_callBack->sendRecordData(pSuperUser);
		}break;
		case GAME_PROTOCOL_S2C_HandCardWrong:
		{
			GameManager::getInstance()->OutCardError();
		}break;
		}

	}

	void GameTableLogic::dealGameDeskNotFound(void* object, INT objectSize)
	{
		_callBack->showGameDeskNotFound();
	}

	void GameTableLogic::dealUserCutMessageResp(INT userId, BYTE seatNo)
	{
		GameManager::getInstance()->userOffLine(getUserDir(seatNo));
	}

	void GameTableLogic::dealOnFlowZhuang(char* object,int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CFlowZhuang_t) == objectSize, "Broken message GameProtoS2CFlowZhuang_t.");
		GameProtoS2CFlowZhuang_t* pProtData = (GameProtoS2CFlowZhuang_t*)object;
		//播放分饼动画
		GameManager::getInstance()->SHowFengbinAnimation();
	}

	void GameTableLogic::dealOnJinFengTou(char* object, int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CJinFengTouTiSi_t) == objectSize, "Broken message GameProtoS2CJinFengTouTiSi_t.");
		GameProtoS2CJinFengTouTiSi_t* pProtData = (GameProtoS2CJinFengTouTiSi_t*)object;
		//禁风头

		GameManager::getInstance()->refreshHandJinFengTou(pProtData);

	}

	void GameTableLogic::dealOnFollowPlay(char* object, int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CGenDaTiSi_t) == objectSize, "Broken message GameProtoS2CGenDaTiSi_t.");
		GameProtoS2CGenDaTiSi_t* pProtData = (GameProtoS2CGenDaTiSi_t*)object;
		//跟打标记
		
		GameManager::getInstance()->refreshHandCardFollow(pProtData);

	}

	void GameTableLogic::dealOnYouJin(char* object, int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CYouJin) == objectSize, "Broken message GameProtoS2CYouJin.");
		GameProtoS2CYouJin* pProtData = (GameProtoS2CYouJin*)object;
		//尤金
		GameManager::getInstance()->YouJin(pProtData);
	}

	void GameTableLogic::dealOnYouJinTis(char* object, int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CYouJinTiSi_t) == objectSize, "Broken message GameProtoS2CYouJinTiSi_t.");
		GameProtoS2CYouJinTiSi_t* pProtData = (GameProtoS2CYouJinTiSi_t*)object;
		//尤金提示
		GameManager::getInstance()->refreshYouJin(pProtData);

	}

	void GameTableLogic::dealOnFollowTingTis(char* object, int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CTingPaiTiSi_t) == objectSize, "Broken message GameProtoS2CTingPaiTiSi_t.");
		GameProtoS2CTingPaiTiSi_t* pProtData = (GameProtoS2CTingPaiTiSi_t*)object;
		//听牌提示
		GameManager::getInstance()->refreshHandCardTing(pProtData);
	}

	void GameTableLogic::setGameStation(void* pBuffer,int nLen)
	{
		
		GameStatusHead_t* pHead = (GameStatusHead_t*)pBuffer;
	
	
		if (pHead->bIsSuperUser)
		{
			GameManager::getInstance()->showGMButton();
		}

		GameManager::getInstance()->resetInit();
		GameManager::getInstance()->setActionScore(pHead->scoreArray);
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			GameManager::getInstance()->CheckActivity();
		}
		_callBack->setBottomScore(pHead->baseBeiLv);
		//关闭游戏结算
		_callBack->onCloseResult();
		switch (pHead->status)
		{
		case GameStatus::Free:
		case GameStatus::Agree:
			{
				GameStatusAgree_t* pProtData = (GameStatusAgree_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				waitAgree();
				if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
				{
					GameManager::getInstance()->onGameStar(1); //考虑断线重连
				}
			}
			break;
		case GameStatus::GameBegin:
			{
				CCAssert(sizeof(GameStatusGameBegin_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusGameBegin_t.");
				GameStatusGameBegin_t* pProtData = (GameStatusGameBegin_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				GameManager::getInstance()->playCommonSound("Begin");

				auto dir = getUserDir(pProtData->bankerNo);
				auto catchDir = getUserDir((pProtData->bankerNo + pProtData->dingZhuangSezi)%PLAY_COUNT);
				GameManager::getInstance()->resetBeginStageUI(pProtData->bankerNo,dir,catchDir,pProtData->dingZhuangSezi,pProtData->bankcount);
			}
			break;
		case GameStatus::ChangeCard:
			{
				CCAssert(sizeof(GameStatusChangeCard_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusChangeCard_t.");
				GameStatusChangeCard_t* pProtData = (GameStatusChangeCard_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				auto dir = getUserDir(pProtData->bankNo);
				//GameManager::getInstance()->resetChangeCardStageUI(dir,pProtData);
			}
			break;
		case GameStatus::SendCard:
			{
				CCAssert(sizeof(GameStatusSendCard_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusSendCard_t.");
				GameStatusSendCard_t* pProtData = (GameStatusSendCard_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				auto dir = getUserDir(pProtData->bankNo);
				GameManager::getInstance()->resetSendCardStageUI(dir,pProtData);
				//_isGamePlaying = true;
			}
			break;
		case GameStatus::DispatchCard:
		case GameStatus::OutCard:
			{
				CCAssert(sizeof(GameStatusOutCard_t)+ sizeof(GameStatusHead_t) == nLen, "Broken message GameStatusOutCard_t.");
				GameStatusOutCard_t* pProtData = (GameStatusOutCard_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				auto dir = getUserDir(pProtData->bankerSeatNo);
				GameManager::getInstance()->resetOutCatchCardStageUI(dir,pProtData,pHead->status==GameStatus::OutCard);
				for (int i = 0; i < PLAY_COUNT; i++)
				{
					auto dir = getUserDir(i);
					GameManager::getInstance()->setAutoImgeVisibal(dir, pProtData->bTuoGuanArray[i]);
					if (i == _mySeatNo)
					{
						//bool isTing = true;
						//if (pProtData->btingtip == 0 && pProtData->tingHuCountArray[i] <= 0)
						//{
						//	isTing = false;
						//}
						//if (isTing)
						//{
						//	//GameManager::getInstance()->setTingLayer(pProtData->btingtip, pProtData->tingHuCardArray[i], pProtData->tingHuCountArray[i]); //显示听牌按钮？6.14
						//	//更换点击某张牌提示听什么牌
						//	/*GameProtoS2CTingPaiTiSi_t* pProtData;
						//	memset(pProtData, 0, sizeof(pProtData));
						//	for (int i = 0; i < 17; i++)
						//	{
						//		for (int j = 0; j < 17; j++)
						//		{
						//			pProtData->TingPaiTiSi[i][j] = pProtData->TingPaiTiSi[i][j];
						//		}
						//	}
						//	GameManager::getInstance()->refreshHandCardTing(pProtData);*/
						//}
						//游金提示优先
						if (pProtData->isHaveYoujin)
						{
							if (GameManager::getInstance()->getIsHasAction() == false)
							{
								GameProtoC2SHuPai_t toProtData;
								RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_GETYOUJINGTIS, &toProtData, sizeof(toProtData));
							}
						}
						//有游金情况下不触发听牌提示
						else if (pProtData->isHaveTing && !pProtData->isHaveYoujin)
						{
							sitDir turnDir = GTLogic()->getUserDir(pProtData->turnSeatNo);
							if (turnDir != SOUTH_DIR)
							{
								GameManager::getInstance()->setTingLayer(pProtData->btingtip, pProtData->tingHuCardArray[i], pProtData->tingHuCountArray[i]);
							}
							if (GameManager::getInstance()->getIsHasAction() == false)
							{
								GameProtoC2SHuPai_t toProtData;
								RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_TingPai, &toProtData, sizeof(toProtData));
							}
						}
						sitDir turnDir = GTLogic()->getUserDir(pProtData->turnSeatNo);
						if (!pProtData->bAction && turnDir == SOUTH_DIR  && !pProtData->isHaveYoujin)
						{
							if (pProtData->igenda == 1 || pProtData->igenda == 2)
							{
								GameProtoC2SHuPai_t toProtData;
								RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_GETWANFATIS, &toProtData, sizeof(toProtData));
							}
						}
					}			
				}
				//金币场比赛场出牌时间调整
				if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
				{
					GameManager::getInstance()->showTimeCountByAction(true, 15);
				}
				else
				{
					GameManager::getInstance()->showTimeCountByAction(true, 120);
				}
				GameManager::getInstance()->setRefreshBtn(true);
				//_isGamePlaying = true;
			}
			break;
		case GameStatus::GameFinish:
			{
				CCAssert(sizeof(GameStatusGameFinish_t) + sizeof(GameStatusHead_t)== nLen, "Broken message GameStatusGameFinish_t.");
				GameStatusGameFinish_t* pProtData = (GameStatusGameFinish_t*)((char*)pBuffer + sizeof(GameStatusHead_t));
				dealOnGameFinish((char*)pProtData, sizeof(GameStatusGameFinish_t));
				GameManager::getInstance()->setRefreshBtn(false);
				//_isGamePlaying = false;
			}
			break;
		}

		//_callBack->showWechat();
		//_callBack->showBackBtn(!_isGamePlaying);

		if (/*!_isGamePlaying */ _hasSetGameStation == false)
		{
			_callBack->checkIpSameTips();
		}

		_hasSetGameStation = true;
	}

	

	void GameTableLogic::onUserAgree(const MSG_GR_R_UserAgree& msg)
	{
		log("\n onUserAgree   : ");
		log("bAgreeGame  %d   ", msg.bAgreeGame);
		log("bDeskNO  %d   ", msg.bDeskNO);
		log("bDeskStation  %d   ", msg.bDeskStation);
		_callBack->agreeGame(getUserDir(msg.bDeskStation));
	}

	
	void GameTableLogic::dealOnSendAllCardEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CSendCard_t) == datasize, "Broken message GameProtoS2CSendCard_t.");
		GameProtoS2CSendCard_t* pProtData = (GameProtoS2CSendCard_t*)pData;
		
		std::vector<std::vector<INT>> vv;
		auto playerIndex = _userInfo.bDeskStation;
		std::vector<INT> v;

		// 生成手牌
		for (int i=playerIndex; i<playerIndex+PLAY_COUNT; i++)
		{
			int toindex = i % PLAY_COUNT;
			log("===================================%d",i);
			for (auto j = 0; j < pProtData->countArray[toindex]; j++)
			{
				v.push_back(pProtData->cardArray[toindex][j]);
				log("%d===%d", pProtData->cardArray[toindex][j],toindex);
			}
			vv.push_back(v);
			v.clear();
			log("===================================");
		}
		GameManager::getInstance()->startGame();
		GameManager::getInstance()->initAllHandCard(vv);
	}

	//处理玩家补花
	void GameTableLogic::dealShowCardFlowerEx(char* pData, int datasize)
	{
		CCAssert(sizeof(CMD_S_SHOW_FLOWER) == datasize, "Broken message GameProtoS2CSendCard_t.");
		CMD_S_SHOW_FLOWER* pProtData = (CMD_S_SHOW_FLOWER*)pData;
		auto dir = getUserDir(pProtData->wChairID);
		/*log("%d==========wChairID", (int)pProtData->wChairID);
		auto deskUser = getUserByUserID(PlatformLogic()->loginResult.dwUserID);
		if (deskUser->bDeskStation != pProtData->wChairID)
		{
			log("return==============%d", deskUser->bDeskStation);
			return;
		}*/
		for (int  i = 0; i < pProtData->cbCardCount; i++)
		{
			bool isMan = GameManager::getInstance()->isMan(pProtData->wChairID);
			GameManager::getInstance()->showBuhuaTip(true);
			GameManager::getInstance()->showBuhuaCard(dir, pProtData->cbCardData[i]);
			PoolAction::playSexNumberSound(isMan, 98);

			std::vector<INT> handCards;
			handCards.clear();
			for (auto j = 0; j < pProtData->countArray[pProtData->wChairID]; j++)
			{
				handCards.push_back(pProtData->cardArray[pProtData->wChairID][j]);
				log("%d==Flower==%d", pProtData->cardArray[pProtData->wChairID][j], j);
			}
			log("%d============dir", dir);
			GameManager::getInstance()->refreshBuhuaHandCardValue(dir, handCards,pProtData->cbGetCardData[0]);//刷新手牌
		}
	}



	void  GameTableLogic::dealGameBegin(char*pData, int datasize)
	{
	
		CCAssert(sizeof(GameProtoS2CBeginOut_t) == datasize, "Broken message GameProtoS2CSendCard_t.");
		GameProtoS2CBeginOut_t* pProtData = (GameProtoS2CBeginOut_t*)pData;
		auto turnDir = getUserDir(pProtData->wChairID);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->playAnimation();
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			auto dir = getUserDir(i);
			GameManager::getInstance()->setAutoImgeVisibal(dir, false);
		}
	}

	//补花完成
	void GameTableLogic::dealShowCardFlowerFinshEx(char* pData, int datasize)
	{
		CCAssert(sizeof(CMD_S_SHOW_FLOWER_FINSH) == datasize, "Broken message GameProtoS2CSendCard_t.");
		CMD_S_SHOW_FLOWER_FINSH* pProtData = (CMD_S_SHOW_FLOWER_FINSH*)pData;
		auto turnDir = getUserDir(pProtData->wChairID);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->showBuhuaTip(false);
	}

	//用户翻金
	void GameTableLogic::dealShowGoldCardEx(char* pData, int datasize)
	{
		CCAssert(sizeof(CMD_S_CARD_DATA) == datasize, "Broken message GameProtoS2CSendCard_t.");
		CMD_S_CARD_DATA* pProtData = (CMD_S_CARD_DATA*)pData;
		auto turnDir = getUserDir(pProtData->wCurrentUser);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		if (pProtData->cbCardData >= CMjEnum::MJ_TYPE_FCHUN)
		{
			return;
		}
		GameManager::getInstance()->SetGoldCard(pProtData->cbCardData);
		GameManager::getInstance()->playCommonSound("gold");
		//播放金币旋转动画
		GameManager::getInstance()->SHowGoldCardAnimation();
	
	}


	void GameTableLogic::dealOnOutCardEx(char* pData, int datasize)
	{
		log("dealOnNotifyOutCardResp :");
		CCAssert(sizeof(GameProtoS2COutCard_t) == datasize, "Broken message GameProtoS2COutCard_t.");

		GameProtoS2COutCard_t* pProtData = (GameProtoS2COutCard_t*)pData;
		bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);
		PoolAction::playSexNumberSound(isMan, pProtData->outCardId);
		auto dir = getUserDir(pProtData->seatNo);
		GameManager::getInstance()->setAutoBtnEnable(true);
		_callBack->outCard(dir, pProtData->outCardId);  // 出牌
		//金币场比赛场出牌时间调整
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			GameManager::getInstance()->showTimeCountByAction(true, 15);
		}
		else
		{
			GameManager::getInstance()->showTimeCountByAction(true, 120);
		}
		std::vector<INT> handCards;
		for (auto j = 0; j < pProtData->countArray[pProtData->seatNo]; j++)
		{
			handCards.push_back(pProtData->cardArray[pProtData->seatNo][j]);
			printf("\n服务器下000000发手牌%d", pProtData->cardArray[pProtData->seatNo][j]);
		}
		if (pProtData->seatNo == _mySeatNo)
		{
			if (pProtData->btingtip != 1)//屏蔽下发的游金提示
			GameManager::getInstance()->setTingLayer(pProtData->btingtip,pProtData->tingHuCardArray[_mySeatNo], pProtData->tingHuCountArray[_mySeatNo]); //显示听牌层。
		}

		if (pProtData->bAction)
		{
			GameManager::getInstance()->setCurrOperDir(sitDir(-1));
		}
		GameManager::getInstance()->refreshHandCardValue(dir, handCards);//刷新手牌
	}

	void GameTableLogic::dealOnZhuaCardEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CDispatchCard_t) == datasize, "Broken message GameProtoS2CDispatchCard_t.");
		GameProtoS2CDispatchCard_t* pProtData = (GameProtoS2CDispatchCard_t*)pData;
		GameManager::getInstance()->playCommonSound("zhuapai");

		auto dir = getUserDir(pProtData->seatNo);                // 取方向
		_callBack->catchCard(dir, pProtData->dispCardId);                // 抓牌
		if (_isHaveAction)
		{
			_isHaveAction = false;
			GameManager::getInstance()->showTimeCountByAction(false, 0);
			//金币场比赛场出牌时间调整
			if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
			{
				GameManager::getInstance()->showTimeCountByAction(true, 15);
			}
			else
			{
				GameManager::getInstance()->showTimeCountByAction(true, 120);
			}
		}
		
	}

	void GameTableLogic::dealOnActionAppearEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CActionAppear_t) == datasize, "Broken message GameProtoS2CActionAppear_t.");
		GameProtoS2CActionAppear_t* pProtData = (GameProtoS2CActionAppear_t*)pData;
		GameManager::getInstance()->playCommonSound("Block");
		//金币场比赛场出牌时间调整
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			GameManager::getInstance()->showTimeCountByAction(true, 15);
		}
		else
		{
			GameManager::getInstance()->showTimeCountByAction(true, 120);
		}
		_isHaveAction = true;
		auto index = _userInfo.bDeskStation;
		if (pProtData->seatNo==index)
		{
			log("dealOnNotifyBlockResp \n");
			if (GameManager::getInstance()->getPlayBack())
			{
				auto dir = getUserDir(pProtData->seatNo);
				GameManager::getInstance()->showActionByDir(dir, pProtData->isHavePeng, pProtData->isHaveChi, pProtData->isHaveGang, pProtData->isHaveHu, pProtData->isHaveTing, pProtData->isHaveQiang, pProtData->isHaveDao, pProtData
					->isHaveYou, pProtData->ishaveTianhu, pProtData->ishavezimo, pProtData->nyoujingcount);
			}
			else
			{
				GameManager::getInstance()->showMeAction(pProtData->isHavePeng, pProtData->isHaveChi, pProtData->isHaveGang, pProtData->isHaveHu, pProtData->isHaveTing, pProtData->isHaveQiang, pProtData->isHaveDao, pProtData
					->isHaveYou, pProtData->ishaveTianhu, pProtData->ishavezimo, pProtData->nyoujingcount);
			}
			//听牌按钮取消
			if (pProtData->isHaveTing == false || pProtData->isHavePeng || pProtData->isHaveChi || pProtData->isHaveGang || pProtData->isHaveHu || pProtData->isHaveQiang || pProtData->isHaveDao || pProtData
				->isHaveYou || pProtData->ishaveTianhu || pProtData->ishavezimo)
			{
				GameManager::getInstance()->setIsHasAction(true);
			}
			GameManager::getInstance()->setIsHasChiAction(pProtData->isHaveChi);
			if (pProtData->isHaveChi)
			{
				std::vector<std::vector<INT>> chiCardList(pProtData->actCount);
				for (int i = 0; i < pProtData->actCount; i++)
				{
					for (int j = 2; j < 5; j++)
					{
						chiCardList.at(i).push_back(pProtData->actCardArray[i][j]);
					}
				}
				GameManager::getInstance()->_chiPaiCard= pProtData->toCardId;
				for (int i = 0; i<pProtData->actCount;i++)
				{
					if (pProtData->actCardArray[i][2] == pProtData->actCardArray[i][3]
						&& pProtData->actCardArray[i][2] == pProtData->actCardArray[i][4]
						&& pProtData->actCardArray[i][4] == pProtData->actCardArray[i][3])
					{
						chiCardList.erase(chiCardList.begin() + i);
						break;
					}
				}
				if (chiCardList.size()>0)
				{
					GameManager::getInstance()->seChiPaiList(chiCardList);
				}
			}
		}
		else
		{
			//其他用户的吃碰杠显示

			log("Other  Player=========%d", pProtData->seatNo);
			auto dir = getUserDir(pProtData->seatNo);
			GameManager::getInstance()->showActionByDir(dir,pProtData->isHavePeng, pProtData->isHaveChi, pProtData->isHaveGang, pProtData->isHaveHu, pProtData->isHaveTing, pProtData->isHaveQiang, pProtData->isHaveDao, pProtData
				->isHaveYou, pProtData->ishaveTianhu, pProtData->ishavezimo, pProtData->nyoujingcount);
		}
		
	}

	void GameTableLogic::dealOnTingPaiEx(char* pData, int datasize)
	{
		GameProtoS2CTingPai_t* pProtData = (GameProtoS2CTingPai_t*)pData;

		auto dir = getUserDir(pProtData->seatNo);
		std::vector<INT> handCards;
		for (int i=0;i<pProtData->cardCount;++i)
		{
			handCards.push_back(pProtData->cardArray[i]);
		}

		std::vector<INT> canOutCards;
		for (int i=0;i<pProtData->canOutCount;++i)
		{
			canOutCards.push_back(pProtData->canOutArray[i]);
		}

		std::vector<INT> canKouCards;

		//金币场比赛场出牌时间调整
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			GameManager::getInstance()->showTimeCountByAction(true, 15);
		}
		else
		{
			GameManager::getInstance()->showTimeCountByAction(true, 120);
		}
		if (dir==sitDir::SOUTH_DIR)
		{
			//处理听牌，提示玩家可以出的牌（听牌按钮取消不做动作）
			GameManager::getInstance()->setIsHasAction(false);
			GameManager::getInstance()->enterTingHandle(handCards,canOutCards,canKouCards);
		}	
		INT index=INT(dir);
		//听牌提示每局只播放一次
		bool isTingState = GameManager::getInstance()->getUserTingPaiState(index);
		if (isTingState == false)
		{
			bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);
			PoolAction::playSexActionSound(isMan, "ting");
		}
		GameManager::getInstance()->setUserTingPaiState(true,index);//听牌后明牌	
	}

	void GameTableLogic::dealOnActionEatEx(char* pData, int datasize)
	{
		CCAssert(sizeof(GameProtoS2CActionEat_t) == datasize, "Broken message GameProtoS2CActionEat_t.");
		GameProtoS2CActionEat_t* pProtData = (GameProtoS2CActionEat_t*)pData;
		auto userDir = getUserDir(pProtData->seatNo);
		int cardId = pProtData->actionArray[1];
		if (pProtData->actionType == 1)
		{
			cardId = GameManager::getInstance()->getLastOutCard()->getCardSumNumber();
		}
		bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);

		sitDir turnDir = getUserDir(pProtData->turnSeatNo);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->turnTableDir(turnDir);

		std::vector<INT> handCard;
		for (int i=0;i<pProtData->cardCount;++i)
		{
			handCard.push_back(pProtData->cardArray[i]);
		}

		char chiCardArray[3] = { 0 };

		for (int i = 0; i < CHICARDCOUNT; i++)
		{
			chiCardArray[i] = pProtData->actionArray[i + 1];
		}

		//停止显示动作
		GameManager::getInstance()->resetChiList();
		GameManager::getInstance()->resetDiKuang();
		
		switch (pProtData->actionType)
		{
		case 1: /* 吃 */
			{
				GameManager::getInstance()->playThingAction(pProtData->actionType, userDir, cardId, isMan, chiCardArray);

				GameManager::getInstance()->refreshHandCardValue(userDir, handCard);
				GameManager::getInstance()->actionCardonRight(userDir,handCard.at(0));

			}
			break;
		case 2: /* 碰 */
			{
			GameManager::getInstance()->playThingAction(pProtData->actionType, userDir, cardId, isMan, chiCardArray);
				GameManager::getInstance()->refreshHandCardValue(userDir,handCard);
				GameManager::getInstance()->actionCardonRight(userDir, handCard.at(0));
			}
			break;
		case 3: /* 明杠 */
		case 4: /* 暗杠 */
		case 5: /* 枪杠 */
			{
				
			GameManager::getInstance()->playThingAction(pProtData->actionType, userDir, cardId, isMan, chiCardArray);
				GameManager::getInstance()->refreshHandCardValue(userDir,handCard);
			
				for (int i = 0; i < PLAY_COUNT; i++)
				{
					auto dir = getUserDir(i);
					GameManager::getInstance()->showScoreAnimation(dir, pProtData->scoreArray[i]);
				}

				
			}
			break;
		}
	}


	void GameTableLogic::dealOnGameFinish(char* pData, int datasize)
	{
		if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
		{
			GameManager::getInstance()->resetInit();
			return;
		}
		int size = sizeof(GameProtoS2CGameFinish_t);
		CCAssert(sizeof(GameProtoS2CGameFinish_t) == datasize, "Broken message GameProtoS2CGameFinish_t.");
		GameProtoS2CGameFinish_t* pProtData = (GameProtoS2CGameFinish_t*)pData;
		if (pProtData->isliuju)
		{
			GameManager::getInstance()->setLiujuCardCount();
		}
		MJGameResult* pResultUI = (MJGameResult*)_callBack->createGameFinishNode();
		GameManager::getInstance()->addActionScore(pProtData->totalScoreArray);
		GameManager::getInstance()->showTimeCountByAction(false,0);
		UserInfoStruct** userInfos = GameManager::getInstance()->getVecUser();
		int Index = 0;
		for (int i =0;i<PLAY_COUNT;++i)
		{
			UserInfoStruct* pUser = *(userInfos+i);
			if (pUser == nullptr)
			{
				continue;
			}

			int seatNo = pUser->bDeskStation;
			if(seatNo<0 || seatNo>=PLAY_COUNT) continue;
			int mGangNum = 0;
			int aGangNum = 0;
			for (int jj=0; jj<pProtData->actCountArray[seatNo]; jj++)
			{
				int actType = pProtData->actCardArray[seatNo][jj][0];
				if (actType == 3 || actType==5) mGangNum++;
				if(actType==4)aGangNum++;
			}

			int allRoomScore = 0;
			for (int jj=0;jj<PLAY_COUNT;++jj)
			{
				if (pProtData->allPlayerUser[jj] == pUser->dwUserID)
				{
					allRoomScore = pProtData->allRoomScoreArray[jj];
				}
			}
			
			for (int k=0; k<PLAY_COUNT; k++)
			{
				UserInfoStruct* pUser = *(userInfos + i);
				if (pUser == nullptr)
				{
					sitDir dir = getUserDir(seatNo);
					pResultUI->setShowUserInfo(nullptr, false, 0, Index);
					continue;
				}
				if (pProtData->huTypeArray[k]!=0)
				{
					pResultUI->ShowReSoultTitle(pProtData->huTypeArray[k]);
				}
				if (pProtData->isliuju)
				{
					pResultUI->ShowReSoultTitle(0);
				}



				if (seatNo!=k) continue;
				sitDir dir = getUserDir(seatNo);
				GameManager::getInstance()->refreshUserMoney(dir,*pUser);
				char toHuPaiCardId = pProtData->huCardId[seatNo];
				if(!pProtData->isHu[seatNo]) 
				{
					toHuPaiCardId = -1;
				}
				bool IsZHuang = false;
				if (k == pProtData->bankUser)
				{
					IsZHuang = true;
				}
				pResultUI->setShowUserInfo(pUser, pUser->dwUserID, allRoomScore, Index);
				pResultUI->setShowBeiLvInfo(pUser, (int)pProtData->allJiepaoArray[seatNo], pProtData->allZimoArray[seatNo],
					pProtData->allSanJinDaoArray[seatNo], pProtData->allYouJinArray[seatNo], Index);
				pResultUI->setShowCurCardArray(pUser, pProtData->handCardArray[k], pProtData->handCountArray[k], 
					pProtData->actCardArray[k], pProtData->actCountArray[k], toHuPaiCardId, Index);
				pResultUI->setShowCurFen(pUser, pProtData->isZiMo[k], pProtData->istianhu[k], pProtData->issanjindao[k], pProtData->isqiangjing[k], pProtData->totalScoreArray[k], Index, pProtData->isyoujin[k], pProtData->Allpan[k], toHuPaiCardId, pProtData->isbahuayou[k], IsZHuang);
				pResultUI->setShowCurPan(pProtData->jinPan[k], pProtData->huaPan[k], pProtData->keziPan[k], pProtData->bugangPan[k], pProtData->minggangPan[k], pProtData->angangPan[k], pProtData->ziPaipengPan[k], Index);
				Index++;
			}
		}

		pResultUI->StartAction(-1, nullptr);	
		//倒牌
		for (int i=0;i<PLAY_COUNT;i++)
		{
			UserInfoStruct* pUser = *(userInfos + i);
			if (pUser == nullptr)
			{
				continue;
			}
			std::vector<CardPool::CGroupCardData> groupCards;
			sitDir dir = GTLogic()->getUserDir(i);
			for (int j=0;j<pProtData->actCountArray[i];j++)
			{
				CardPool::CGroupCardData cardData;
				int count = pProtData->actCardArray[i][j][2];
				std::vector<INT> cards;
				for (int z=3;z<3+count;z++)
				{
					cards.push_back(pProtData->actCardArray[i][j][z]);
				}
				switch (pProtData->actCardArray[i][j][0])
				{
				case ChiPai:
					for (int f = 0; f < count; f++)
					{
						cardData._iChiCardArray[f] = cards[f];
					}
					cardData._iCardId = -1;
					cardData._iCount = cards.size();
					cardData._iType = CardPool::CGroupCard_Chi;
					groupCards.push_back(cardData);
					break;
				case PengPai:
					cardData._iCardId = cards.at(0);
					cardData._iCount  = cards.size();
					cardData._iType   = CardPool::CGroupCard_Peng;
					groupCards.push_back(cardData);

					break;
				case MingGang:
				case QiangGang:
					cardData._iCardId = cards.at(0);
					cardData._iCount  = cards.size();
					cardData._iType   = CardPool::CGroupCard_MingGang;
					groupCards.push_back(cardData);

					break;
				case AnGang:
					cardData._iCardId = cards.at(0);
					cardData._iCount  = cards.size();
					cardData._iType   = CardPool::CGroupCard_AnGang;
					groupCards.push_back(cardData);

					break;
				default:
					break;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			std::vector<INT> handCards;
			for (int j=0;j<pProtData->handCountArray[i];j++)
			{
				handCards.push_back(pProtData->handCardArray[i][j]);
			}

			GameManager::getInstance()->showHandCardHu(dir,groupCards,handCards,pProtData->isZiMo[i],pProtData->huCardId[i],true,pProtData->istianhu[i],pProtData->isqiangjing[i],pProtData->issanjindao[i],pProtData->isyoujin[i],pProtData->isbahuayou[i]);
		}

		//
		GameManager::getInstance()->resetDiKuang();
		//COCOS_NODE(Button, "start")->setVisible(true);

	}

	void GameTableLogic::dealOnPlayerHu(char* pData,int datasize)
	{
		CCAssert(sizeof(GameProtoS2CHu) == datasize, "Broken message GameProtoS2CHu.");
		GameProtoS2CHu* pProtData = (GameProtoS2CHu*)pData;

		sitDir turnDir = getUserDir(pProtData->turnSeatNo);
		GameManager::getInstance()->setCurrOperDir(turnDir);
		GameManager::getInstance()->turnTableDir(turnDir);

		std::vector<INT> handCards;
		for (int j=0;j<pProtData->cardCount;j++)
		{
			handCards.push_back(pProtData->cardArray[j]);
		}

		GameManager::getInstance()->setIsHasChiAction(false);
		GameManager::getInstance()->resetDiKuang();

		PoolAction * action = nullptr;
	
		
		//播放一下声音
		bool isMan = GameManager::getInstance()->isMan(pProtData->seatNo);
		bool is = true;
		for (int i = 0; i < 3; i++)
		{
			
			if (pProtData->isyoujin[i])
			{
				std::string str = StringUtils::format("you%d", i + 1);
				PoolAction::playSexActionSound(isMan, str);
				is = false;
			}
		}
		if (is)
		{
			PoolAction::playSexActionSound(isMan, "hu");
		}
		//显示手牌
		std::vector<CardPool::CGroupCardData> groupCards = finCardGroup(pProtData->actionCard,pProtData->actionCount,pProtData->seatNo);
		sitDir dir = getUserDir(pProtData->seatNo);
		GameManager::getInstance()->showHandCardHu(dir,groupCards,handCards,pProtData->isZimo,pProtData->huCard,true,pProtData->istianhu,pProtData->isqiangjing,pProtData->issanjindao,pProtData->isyoujin,pProtData->isBahuayou);


		//播放胡的动画

		GameManager::getInstance()->ShowHu(dir, pProtData->huCard);

		//播放胡的闪电动画

		GameManager::getInstance()->ShowShanDian(dir);
	}

	/*****************************************************************************************/
	void GameTableLogic::waitAgree()
	{
	
		_userInfo = RoomLogic()->loginResult.pUserInfoStruct;
		// 显示玩家
		vector<UserInfoStruct *> vec;
		UserInfoModule()->findDeskUsers(_userInfo.bDeskNO, vec);
		for (auto &v : vec)
		{
			_callBack->addUser(getUserDir(v->bDeskStation), v);
		}
		_callBack->UpDateUserLocation(0);
	}


	sitDir GameTableLogic::getUserDir(const INT& deskStation) //获取玩家方向 gjd6.3
	{
		char text[100] ={0};
		char text2[100] = { 0 };
		if (deskStation == -1)
		{
			return sitDir(-1);
		}
		sprintf(text,"getUserDir--%d",_userInfo.bDeskStation);
		sprintf(text2, "getDeskStation--%d", deskStation);
		//CCLOG(text);
		//CCLOG(text2);
		//int _player = GameManager::getInstance()->getPlayerNum();
		ComRoomInfo* pInfo = RoomLogic()->getInstance()->getSelectedRoom();
		//int a = PlatformLogic()->getPlayerCount();
		//int maxPlayer = HNPlatformConfig()->getAllCount();
		//HNPlatformConfig()->getPlayerNum();
		int maxPlayer = 3;
		//auto roomInfo = GameManager::getInstance()->getRoomInfo();
		if (maxPlayer == 2)
		{	
			if (deskStation == _userInfo.bDeskStation){
				return SOUTH_DIR;
			}
			else
			{
				return NORTH_DIR;
			}
		}
		else
		{
			return  sitDir((deskStation - _userInfo.bDeskStation + PLAY_COUNT) % PLAY_COUNT);
		}
	
		//return  sitDir((deskStation - _userInfo.bDeskStation + PLAY_COUNT) % PLAY_COUNT);
	}

	INT GameTableLogic::getUserStation(const sitDir& dir)
	{
		return (INT(dir) - INT(sitDir::SOUTH_DIR) + _userInfo.bDeskStation)%PLAY_COUNT;
	}

	std::vector<CardPool::CGroupCardData> GameTableLogic::finCardGroup(char actionCard[ACTION_MAX_COUNT][10], INT actionCount, INT station)
	{
		std::vector<CardPool::CGroupCardData> groupCards;
		sitDir dir = getUserDir(station);

		for (int j=0;j<actionCount;j++)
		{
			CardPool::CGroupCardData cardData;
			int count = actionCard[j][2];
			std::vector<INT> cards;
			for (int z=3;z<3+count;z++)
			{
				cards.push_back(actionCard[j][z]);
			}
			switch (actionCard[j][0])
			{
			case ChiPai:
				for (int f = 0; f < count; f++)
				{
					cardData._iChiCardArray[f] = cards[f];
				}
				cardData._iCardId = -1;
				cardData._iCount = cards.size();
				cardData._iType = CardPool::CGroupCard_Chi;
				groupCards.push_back(cardData);
				break;
			case PengPai:
				cardData._iCardId = cards.at(0);
				cardData._iCount  = cards.size();
				cardData._iType   = CardPool::CGroupCard_Peng;
				groupCards.push_back(cardData);

				break;
			case MingGang:
			case QiangGang:
				cardData._iCardId = cards.at(0);
				cardData._iCount  = cards.size();
				cardData._iType   = CardPool::CGroupCard_MingGang;
				groupCards.push_back(cardData);

				break;
			case AnGang:
				cardData._iCardId = cards.at(0);
				cardData._iCount  = cards.size();
				cardData._iType   = CardPool::CGroupCard_AnGang;
				groupCards.push_back(cardData);

				break;
			default:
				break;
			}
		}
		return groupCards;
	}

	void GameTableLogic::clearDesk()
	{

	}

	void GameTableLogic::dealUserInfoChangeResp(const UserInfoStruct* user)
	{

		auto deskUser = getUserByUserID(user->dwUserID);
		if (deskUser)
		{
			float fMoney = (float)user->i64Money;
			BYTE seatNo = logicToViewSeatNo(deskUser->bDeskStation);
			//_callBack->showUserProfit(seatNo, fMoney);
		}

	}

	//比赛场会调用（执行坐下以及请求游戏消息）
	void GameTableLogic::dealQueueUserSitMessage(bool success, const std::string& message)
	{
		_userInfo = RoomLogic()->loginResult.pUserInfoStruct;

		HNGameLogicBase::dealQueueUserSitMessage(success, message);

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		if (success)
		{
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				_callBack->removeUser(getUserDir(i));
			}
			loadUsers();
			sendGameInfo();
		}
		else
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(message);
			prompt->setCallBack([=](){
				stop();
				RoomLogic()->close();
				GamePlatform::returnPlatform(ROOMLIST);
			});
		}
	}

	void GameTableLogic::dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		switch (messageHead->bAssistantID)
		{
		case ASS_GR_VOIEC:
		{
			//效验数据
			CCAssert(sizeof(VoiceInfo) == objectSize, "VoiceInfo size is error.");
			VoiceInfo* voiceInfo = (VoiceInfo*)object;
			auto userInfo = getUserByUserID(voiceInfo->uUserID);
			if (!userInfo) return;
			if (userInfo->bDeskNO == _deskNo)
			{
				_callBack->onChatVoiceMsg(voiceInfo->uUserID, voiceInfo->uVoiceID, voiceInfo->iVoiceTime);
			}
		}
		break;
		default:
			break;
		}
	}

	void GameTableLogic::dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk)
	{
		if (normalTalk->dwTargetID == normalTalk->dwSendID)
		{
			auto user = UserInfoModule()->findUser(normalTalk->dwSendID);
			_callBack->onChatTextMsg(user->bDeskStation, normalTalk->szMessage);
		}
		else
		{
			/*auto SendUser = UserInfoModule()->findUser(normalTalk->dwSendID);
			auto TargetUser = UserInfoModule()->findUser(normalTalk->dwTargetID);
			_callBack->onActionChatMsg(SendUser->bDeskStation, TargetUser->bDeskStation, normalTalk->szMessage);*/
		}
	}

	//特效道具聊天
	void GameTableLogic::dealUserActionMessage(DoNewProp* normalAction)
	{
		auto pUser = UserInfoModule()->findUser(normalAction->dwUserID);
		if (pUser != nullptr)
		{
			auto SendUser = UserInfoModule()->findUser(normalAction->dwUserID);
			auto TargetUser = UserInfoModule()->findUser(normalAction->dwDestID);
			_callBack->onActionChatMsg(SendUser->bDeskStation, TargetUser->bDeskStation, normalAction->iPropNum);
			//发送特效道具扣除用户金币
			GameManager::getInstance()->afterScoreByDeskStation(SendUser->bDeskStation, -50);
		}
	}

	//红包活动
	void GameTableLogic::dealGameActivityResp(MSG_GR_GameNum* pData)
	{
		int GameNum = pData->iGameNum;
		int dangqianjieduan = pData->idangqianjieduan;
		int CanTakeNum = pData->iCanTakeNum;
		_callBack->setActivityNum(GameNum, dangqianjieduan, CanTakeNum);
	}

	void GameTableLogic::dealGameActivityReceive(MSG_GR_Set_JiangLi* pData)
	{
		//领取奖励类型
		int ReceiveType = pData->iRandnum;
		int Index = pData->iJieDuan;
		if (Index == 1)				//阶段一奖励类型为金币，房卡（0-3金币，4-5房卡）
		{
			if (ReceiveType <= 3)
			{
				ReceiveType = 1;
			}
			else
			{
				ReceiveType = 2;
			}
		}
		int AwardNum = pData->iAwardNum;
		//显示中奖处理
		_callBack->setReceiveState(ReceiveType, AwardNum);
	}

	//提示玩家金币不足
	void GameTableLogic::dealUserNotEnoughMoney()
	{
		//GamePromptLayer::create()->showPrompt(GBKToUtf8("金币不足,无法发送!"));
		GameManager::getInstance()->PopDownView();
	}

	void GameTableLogic::sendStandUp()
	{
		do
		{
			HNGameLogicBase::sendUserUp();
		} while (0);
	}

	void GameTableLogic::loadUsers()
	{

		BYTE seatNo = INVALID_DESKNO;
		char str[64] = { 0 };
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			const UserInfoStruct* pUser = getUserBySeatNo(i);
			if (pUser != nullptr)
			{
				auto deskUser = getUserByUserID(pUser->dwUserID);
				if (_existPlayer[i] && pUser != nullptr)
				{
					seatNo = logicToViewSeatNo(i);
					_callBack->addUser(getUserDir(i), deskUser);
				}
			}
		}
	}

	void GameTableLogic::dealAuto(char*object, int objectSize)
	{
		CCAssert(sizeof(GameProtoS2CTuoGuan_t) == objectSize, "Broken message GameProtoS2CFlowZhuang_t.");
		GameProtoS2CTuoGuan_t* pProtData = (GameProtoS2CTuoGuan_t*)object;
		auto dir = getUserDir(pProtData->seatNo);
		auto visible = pProtData->bTuoGuan;
		GameManager::getInstance()->setAutoImgeVisibal(dir, visible);
	}

}
