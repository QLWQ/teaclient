#include "QZHOUJJMJ_ActionButtonList.h"
#include "cocostudio/CocoStudio.h"
#include "QZHOUJJMJ_GameManager.h"
#include "QZHOUJJMJ_MessageHead.h"

namespace QZHOUJJMJ
{


	QZHOUJJMJ_ActionButtonList::QZHOUJJMJ_ActionButtonList() :
		_loader(nullptr),
		_PosOrigin(Vec2::ZERO)
	{

	}

	QZHOUJJMJ_ActionButtonList::~QZHOUJJMJ_ActionButtonList()
	{
		
	}



	bool QZHOUJJMJ_ActionButtonList::init()
	{
		if (!Node::init())
		{
			return false;
		}
		
		//杠
		_loader = CSLoader::createNode(COCOS_PATH+"ActionButton.csb");
		auto action1 = CSLoader::createTimeline(COCOS_PATH + "ActionButton.csb");
		_loader->runAction(action1);
		action1->gotoFrameAndPlay(0, true);
		this->addChild(_loader);
		Button* gang=(Button*)_loader->getChildByName("Button_gang");
		gang->setVisible(false);
		gang->setTag(GANG);
		gang->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(gang);

		//碰
		Button* peng=(Button*)_loader->getChildByName("Button_peng");
		peng->setVisible(false);
		peng->setTag(PENG);

		peng->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(peng);

		//吃
		Button* chi = (Button*)_loader->getChildByName("Button_chi");
		chi->setVisible(false);
		chi->setTag(CHI);

		chi->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(chi);


		//游金
		Button* youjin = (Button*)_loader->getChildByName("Button_youjin");
		youjin->setVisible(false);
		youjin->setTag(YOUJING);

		youjin->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(youjin);


		//三金倒
		Button* sanjingdao = (Button*)_loader->getChildByName("Button_sanjindao");
		sanjingdao->setVisible(false);
		sanjingdao->setTag(SANJINGDAO);

		sanjingdao->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(sanjingdao);

		//双游
		Button* shuangyou = (Button*)_loader->getChildByName("Button_shuangyou");
		shuangyou->setVisible(false);
		shuangyou->setTag(SHUANGYOU);

		shuangyou->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(shuangyou);

		//三游

		Button* sanyou = (Button*)_loader->getChildByName("Button_sanyou");
		sanyou->setVisible(false);
		sanyou->setTag(SANYOU);

		sanyou->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(sanyou);


		//天胡
		Button* tianhu = (Button*)_loader->getChildByName("Button_tianhu");
		tianhu->setVisible(false);
		tianhu->setTag(TIANHU);

		tianhu->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(tianhu);

		//抢金
		Button* qiangjing = (Button*)_loader->getChildByName("Button_qiangjing");
		qiangjing->setVisible(false);
		qiangjing->setTag(QIANGJING);

		qiangjing->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(qiangjing);


		//自摸
		Button* zimo = (Button*)_loader->getChildByName("Button_zimo");
		zimo->setVisible(false);
		zimo->setTag(ZIMO);

		zimo->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(zimo);



		//胡
		Button* hu=(Button*)_loader->getChildByName("Button_hu");
		hu->setVisible(false);
		hu->setTag(HU);
		hu->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(hu);


		//八花游
		Button* bahuayou = (Button*)_loader->getChildByName("Button_bahuayou");
		bahuayou->setVisible(false);
		bahuayou->setTag(BAHUAYOU);
		bahuayou->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(bahuayou);


		//听牌
		Button* ting = (Button*)_loader->getChildByName("Button_ting");
		ting->setVisible(false);
		ting->setTag(TING);
		ting->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		_buttonVec.push_back(ting);

		//过
		Button* guo=(Button*)_loader->getChildByName("Button_guo");
		guo->addTouchEventListener(CC_CALLBACK_2(QZHOUJJMJ_ActionButtonList::clickBtEventCallBack, this));
		guo->loadTextures(COCOS_PATH + "Sprite/btn_guo1.png", COCOS_PATH + "Sprite/btn_guo2.png", COCOS_PATH + "Sprite/btn_guo1.png");
		guo->setVisible(false);
		guo->setTag(GUO);
		_PosOrigin=guo->getPosition();

		//回放显示的按钮精灵
		Sprite* sp_gang = Sprite::create("platform/Games/Btn/gang.png");
		sp_gang->setPosition(_PosOrigin);
		sp_gang->setName("Sp_gang");
		_loader->addChild(sp_gang);
		sp_gang->setVisible(false);
		_spVec.push_back(sp_gang);

		Sprite* sp_peng = Sprite::create("platform/Games/Btn/peng.png");
		sp_peng->setPosition(_PosOrigin);
		sp_peng->setName("Sp_peng");
		_loader->addChild(sp_peng);
		sp_peng->setVisible(false);
		_spVec.push_back(sp_peng);

		Sprite* sp_chi = Sprite::create("platform/Games/Btn/chi.png");
		sp_chi->setPosition(_PosOrigin);
		sp_chi->setName("Sp_chi");
		_loader->addChild(sp_chi);
		sp_chi->setVisible(false);
		_spVec.push_back(sp_chi);

		Sprite* sp_youjin = Sprite::create("platform/Games/Btn/youjin.png");
		sp_youjin->setPosition(_PosOrigin);
		sp_youjin->setName("Sp_youjin");
		_loader->addChild(sp_youjin);
		sp_youjin->setVisible(false);
		_spVec.push_back(sp_youjin);

		Sprite* sp_sanjingdao = Sprite::create("platform/Games/Btn/sanjindao.png");
		sp_sanjingdao->setPosition(_PosOrigin);
		sp_sanjingdao->setName("Sp_sanjindao");
		_loader->addChild(sp_sanjingdao);
		sp_sanjingdao->setVisible(false);
		_spVec.push_back(sp_sanjingdao);

		Sprite* sp_shuangyou = Sprite::create("platform/Games/Btn/shuangyou.png");
		sp_shuangyou->setPosition(_PosOrigin);
		sp_shuangyou->setName("Sp_shuangyou");
		_loader->addChild(sp_shuangyou);
		sp_shuangyou->setVisible(false);
		_spVec.push_back(sp_shuangyou);

		Sprite* sp_sanyou = Sprite::create("platform/Games/Btn/sanyou.png");
		sp_sanyou->setPosition(_PosOrigin);
		sp_sanyou->setName("Sp_sanyou");
		_loader->addChild(sp_sanyou);
		sp_sanyou->setVisible(false);
		_spVec.push_back(sp_sanyou);

		Sprite* sp_tianhu = Sprite::create("platform/Games/Btn/tianhu.png");
		sp_tianhu->setPosition(_PosOrigin);
		sp_tianhu->setName("Sp_tianhu");
		_loader->addChild(sp_tianhu);
		sp_tianhu->setVisible(false);
		_spVec.push_back(sp_tianhu);

		Sprite* sp_qiangjing = Sprite::create("platform/Games/Btn/qiangjin.png");
		sp_qiangjing->setPosition(_PosOrigin);
		sp_qiangjing->setName("Sp_qiangjing");
		_loader->addChild(sp_qiangjing);
		sp_qiangjing->setVisible(false);
		_spVec.push_back(sp_qiangjing);

		Sprite* sp_zimo = Sprite::create("platform/Games/Btn/zimo.png");
		sp_zimo->setPosition(_PosOrigin);
		sp_zimo->setName("Sp_zimo");
		_loader->addChild(sp_zimo);
		sp_zimo->setVisible(false);
		_spVec.push_back(sp_zimo);

		Sprite* sp_hu = Sprite::create("platform/Games/Btn/hu.png");
		sp_hu->setPosition(_PosOrigin);
		sp_hu->setName("Sp_hu");
		_loader->addChild(sp_hu);
		sp_hu->setVisible(false);
		_spVec.push_back(sp_hu);

		Sprite* sp_bahuayou = Sprite::create("platform/Games/Btn/bahuayou.png");
		sp_bahuayou->setPosition(_PosOrigin);
		sp_bahuayou->setName("Sp_bahuayou");
		_loader->addChild(sp_bahuayou);
		sp_bahuayou->setVisible(false);
		_spVec.push_back(sp_bahuayou);

		//听不需要~
		/*Sprite* sp_hu = Sprite::create("platform/Games/Btn/gang.png");
		sp_hu->setPosition(_PosOrigin);
		sp_hu->setName("Sp_hu");
		_loader->addChild(sp_hu);*/

		Sprite* sp_guo = Sprite::create("platform/Games/Btn/guo.png");
		sp_guo->setPosition(_PosOrigin);
		sp_guo->setName("Sp_guo");
		sp_guo->setVisible(false);
		_loader->addChild(sp_guo);
		//_spVec.push_back(sp_guo);

		return true;
	}

	void QZHOUJJMJ_ActionButtonList::clickBtEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		Button* bt=(Button*)pSender;
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		int x=bt->getTag();
		switch (bt->getTag())
		{
		case GANG :
			{

				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SActionEat_t toProtData;
				toProtData.actionType = 3;
				memset(toProtData.chiCardArray, 0, sizeof(toProtData.chiCardArray));
				toProtData.chiCardCount = 0;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ActionEat, &toProtData, sizeof(toProtData));
			}
			break;
		case PENG :
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SActionEat_t toProtData;
				toProtData.actionType = 2;
				memset(toProtData.chiCardArray, 0, sizeof(toProtData.chiCardArray));
				toProtData.chiCardCount = 0;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ActionEat, &toProtData, sizeof(toProtData));
			}	
			break;
		case CHI:
			{
				GameManager::getInstance()->ShowChiTip();
			}
			break;

		case HU:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_HuPai, &toProtData, sizeof(toProtData));
			}
			break;

		case GUO  :
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SPassAction_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_PassAction, &toProtData, sizeof(toProtData));
				//this->runAction(RemoveSelf::create(false));
			}
			break;
		case SANJINGDAO:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_SANJINGDAO, &toProtData, sizeof(toProtData));
			}
			break;
		case QIANGJING:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_QIANGJING, &toProtData, sizeof(toProtData));
			}
			break;
		case YOUJING:
			{	
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_YOUJING, &toProtData, sizeof(toProtData));
			}
			break;
		case SHUANGYOU:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_YOUJING, &toProtData, sizeof(toProtData));
			}
			break;
		case SANYOU:
			{
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_YOUJING, &toProtData, sizeof(toProtData));
			}
			break;
		case ZIMO:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_HuPai, &toProtData, sizeof(toProtData));
			}
			break;
		case TIANHU:
			{
				GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_HuPai, &toProtData, sizeof(toProtData));
			}
			break;
		case BAHUAYOU:
		{
			GameManager::getInstance()->setIsHasChiAction(false);
			GameProtoC2SHuPai_t toProtData;
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_BAHUAYOU, &toProtData, sizeof(toProtData));
		}
			break;
		case TING:
		{
			GameManager::getInstance()->setIsHasChiAction(false);
			GameProtoC2SHuPai_t toProtData;
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_TingPai, &toProtData, sizeof(toProtData));
		}
			break;
		default:
			break;
		}
		this->runAction(RemoveSelf::create(false));
		GameManager::getInstance()->setIsHasAction(false);
		//金币场比赛场出牌时间调整
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			GameManager::getInstance()->showTimeCountByAction(true, 15);
		}
		else
		{
			GameManager::getInstance()->showTimeCountByAction(true, 120);
		}
	}

	void QZHOUJJMJ_ActionButtonList::showActionBotton(bool hasHu, bool canchi, bool hasPeng, bool hasGang, bool canTing, bool canQiang, bool canDao, bool canyou, bool cantianhu, bool canzimo, char num)
	{
		if (_loader)
		{
			_loader->getChildByName("Button_hu")->setVisible(hasHu);
			_loader->getChildByName("Button_chi")->setVisible(canchi);
			_loader->getChildByName("Button_gang")->setVisible(hasGang);
			_loader->getChildByName("Button_peng")->setVisible(hasPeng);
			_loader->getChildByName("Button_qiangjing")->setVisible(canQiang);
			_loader->getChildByName("Button_sanjindao")->setVisible(canDao);

			_loader->getChildByName("Button_sanjindao")->setVisible(canDao);
			//游金
			if (num ==1)
			{
				_loader->getChildByName("Button_youjin")->setVisible(canyou);
			}
			//双游
			else if (num == 2)
			{
				_loader->getChildByName("Button_shuangyou")->setVisible(canyou);
			}
			//三游
			else if (num == 3)
			{
				_loader->getChildByName("Button_sanyou")->setVisible(canyou);
			}
			//天胡
			_loader->getChildByName("Button_tianhu")->setVisible(cantianhu);
			//自摸
			_loader->getChildByName("Button_zimo")->setVisible(canzimo);


			//听牌(不显示听牌按钮直接操作)
			//_loader->getChildByName("Button_ting")->setVisible(canTing);
			if (canTing & (hasHu || canchi || hasPeng || hasGang ||
				canQiang || canDao || canyou || cantianhu || canzimo))
			{
				//又听又有其他动作？不做调整处理
				_loader->getChildByName("Button_guo")->setVisible(true);
				reSetButtonPos();
			}
			else if (canTing)
			{
				//只有听牌
				/*GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_TingPai, &toProtData, sizeof(toProtData));*/
				/*_loader->getChildByName("Button_ting")->setVisible(canTing);
				_loader->getChildByName("Button_guo")->setVisible(true);
				reSetButtonPos();*/
			}
			else
			{
				_loader->getChildByName("Button_guo")->setVisible(true);
				reSetButtonPos();
				//reSetSpPos();
			}
		}
	}

	void QZHOUJJMJ_ActionButtonList::showActionByDir(const sitDir& dir, bool hasHu, bool canchi, bool hasPeng, bool hasGang, bool canTing, bool canQiang, bool canDao, bool canyou, bool cantianhu, bool canzimo, char num)
	{
		if (_loader)
		{
			_loader->getChildByName("Sp_hu")->setVisible(hasHu);
			_loader->getChildByName("Sp_chi")->setVisible(canchi);
			_loader->getChildByName("Sp_gang")->setVisible(hasGang);
			_loader->getChildByName("Sp_peng")->setVisible(hasPeng);
			_loader->getChildByName("Sp_qiangjing")->setVisible(canQiang);
			_loader->getChildByName("Sp_sanjindao")->setVisible(canDao);

			_loader->getChildByName("Sp_sanjindao")->setVisible(canDao);
			//游金
			if (num == 1)
			{
				_loader->getChildByName("Sp_youjin")->setVisible(canyou);
			}
			//双游
			else if (num == 2)
			{
				_loader->getChildByName("Sp_shuangyou")->setVisible(canyou);
			}
			//三游
			else if (num == 3)
			{
				_loader->getChildByName("Sp_sanyou")->setVisible(canyou);
			}
			//天胡
			_loader->getChildByName("Sp_tianhu")->setVisible(cantianhu);
			//自摸
			_loader->getChildByName("Sp_zimo")->setVisible(canzimo);

			//听牌(不显示听牌按钮直接操作)
			//_loader->getChildByName("Sp_ting")->setVisible(canTing);
			if (canTing & (hasHu || canchi || hasPeng || hasGang ||
				canQiang || canDao || canyou || cantianhu || canzimo))
			{
				//又听又有其他动作？不做调整处理
				_loader->getChildByName("Sp_guo")->setVisible(true);
				//reSetButtonPos();
				reSetSpPos(dir);
			}
			else if (canTing)
			{
				//只有听牌
				/*GameManager::getInstance()->setIsHasChiAction(false);
				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_TingPai, &toProtData, sizeof(toProtData));*/
				/*_loader->getChildByName("Sp_ting")->setVisible(canTing);
				_loader->getChildByName("Sp_guo")->setVisible(true);
				reSetButtonPos();*/
			}
			else
			{
				_loader->getChildByName("Sp_guo")->setVisible(true);
				//reSetButtonPos();
				reSetSpPos(dir);
			}
		}
	}

	void QZHOUJJMJ_ActionButtonList::reSetButtonPos()
	{
		int index=0;
		for (auto iter=_buttonVec.begin();iter!=_buttonVec.end();iter++)
		{
			Button* bt=*iter;
			if (bt->isVisible())
			{
				index++;
				bt->setPositionX(_PosOrigin.x - index*(bt->getContentSize().width + 30));
			}
		}
	}

	void QZHOUJJMJ_ActionButtonList::reSetSpPos(const sitDir& dir)
	{
		int index = 0;
		Vec2 TempPos = Vec2(0,0);
		switch (dir)
		{
			case sitDir::EAST_DIR:
				TempPos = Vec2(400,120);
				break;
			case sitDir::NORTH_DIR:
				TempPos = Vec2(100, 250);
				break;
			case sitDir::SOUTH_DIR:
				TempPos = Vec2(380, -150);
				break;
			case sitDir::WEST_DIR:
				TempPos = Vec2(-320, 120);
				break;
			default:
				break;
		}
		for (auto iter = _spVec.begin(); iter != _spVec.end(); iter++)
		{
			Sprite* bt = *iter;
			if (bt->isVisible())
			{
				index++;
				if (_loader)_loader->getChildByName("Sp_guo")->setPosition(TempPos);
				bt->setPositionX(TempPos.x - index*bt->getContentSize().width);
				bt->setPositionY(TempPos.y);
			}
		}
	}

}
