#ifndef _QZHOUJJMJ_EATACTION_H_
#define _QZHOUJJMJ_EATACTION_H_
#include "QZHOUJJMJ_poolaction.h"

namespace QZHOUJJMJ
{

	class EatCard :
		public PoolAction
	{
	public:
		EatCard(void);
		~EatCard(void);

		virtual void run() override;

		CREATE_FUNC(EatCard);

	};

}
#endif