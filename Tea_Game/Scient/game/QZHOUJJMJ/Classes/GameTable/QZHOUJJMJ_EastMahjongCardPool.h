#ifndef _QZHOUJJMJ_EASTMAHJONGCARDPOOL_H_
#define _QZHOUJJMJ_EASTMAHJONGCARDPOOL_H_


#include "QZHOUJJMJ_EastHandCard.h"
#include "QZHOUJJMJ_MahjongCardPool.h"

namespace QZHOUJJMJ
{
	class EastMahjongCardPool :
		public MahjongCardPool
	{
	public:
		EastMahjongCardPool(void);
		~EastMahjongCardPool(void);

		CREATE_COUNT(EastMahjongCardPool);
		virtual bool init(INT count);

		virtual void setHandCardPos(INT catchCard) override;					 // ��������
		virtual int  getZhuaPaiZOrder();
		virtual void refreshAllShowCard()override;
		virtual void EmptyAllShowCard()override;
		virtual void RefreshShowCard()override;
		virtual cocos2d::Vec2 getCatchPos()override;
		virtual Vec2 getOutToDeskPos()override;

		virtual void showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> allHandCards,INT huCard,bool visibleAllCard) override;
		virtual void showHuTips(std::vector<INT> huTip)override;
		virtual void changeCardToHead(std::vector<INT> cards)override;
	};

}

#endif