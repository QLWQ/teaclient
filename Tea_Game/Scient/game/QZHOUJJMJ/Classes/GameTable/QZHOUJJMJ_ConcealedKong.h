#ifndef _QZHOUJJMJ_CONCEALEDKONG_H_
#define _QZHOUJJMJ_CONCEALEDKONG_H_

#include "QZHOUJJMJ_poolaction.h"

namespace QZHOUJJMJ
{

	class ConcealedKong :
		public PoolAction
	{
	public:
		ConcealedKong(void);
		~ConcealedKong(void);

		virtual void run() override;

		CREATE_FUNC(ConcealedKong);

	};

}
#endif