﻿#ifndef _QZHOUJJMJ_GameTableUICallback_H_
#define _QZHOUJJMJ_GameTableUICallback_H_

#include "QZHOUJJMJ_MessageHead.h"

/************************************************************************/
/* 类型：回调接口                                                        */
/* 功能：声明牌桌逻辑在收到消息时调用界面的函数接口                        */
/************************************************************************/

namespace QZHOUJJMJ
{
	class GameTableUICallBack : public IHNGameLogicBase
	{
	public:
		virtual void dealLeaveDesk() = 0;
		virtual void addUser(const sitDir& dir,UserInfoStruct* user) = 0;												// 增加玩家
		virtual void removeUser(const sitDir& dir) = 0;                                                                 // 移除玩家
		virtual void agreeGame(const sitDir& dir) = 0;
		
		virtual void UpDateUserLocation(int Index) = 0;

		virtual void outCard(const sitDir& dir, const INT& number) = 0;  // 出牌

		virtual void catchCard(const sitDir& dir, const INT& number) = 0;

		
		virtual void setDissloveBtState() = 0;

		virtual void setGamecount(int counts) = 0;

		virtual void showWechat(bool visible) = 0;

		virtual Node* createGameFinishNode()= 0;

		virtual void checkIpSameTips()=0;
		virtual void showGameDeskNotFound()=0;
		// 显示左上角桌子信息
		virtual void setDeskInfo() = 0;
		// 文本聊天消息
		virtual void onChatTextMsg(BYTE seatNo, CHAR msg[]) = 0;
		// 动作聊天信息
		virtual void onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index) = 0;

		virtual void sendRecordData(S_C_GameRecordResult* data) = 0;										//战绩数据
		virtual void	setBottomScore(int Score) = 0;
		virtual void setActivityNum(int Current, int toal, int CanTake) = 0;
		virtual void setReceiveState(int ReceiveType, int AwardNum) = 0;
	};



}

#endif //_SXZP_GameTableUICallback_H_