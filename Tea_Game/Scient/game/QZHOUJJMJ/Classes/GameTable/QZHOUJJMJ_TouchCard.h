#ifndef _QZHOUJJMJ_TOUCHCARD_H_
#define _QZHOUJJMJ_TOUCHCARD_H_

#include "QZHOUJJMJ_poolaction.h"

namespace QZHOUJJMJ
{
	class TouchCard :
		public PoolAction
	{
	public:
		TouchCard(void);
		~TouchCard(void);

		virtual void run() override;

		CREATE_FUNC(TouchCard);
	};

}

#endif