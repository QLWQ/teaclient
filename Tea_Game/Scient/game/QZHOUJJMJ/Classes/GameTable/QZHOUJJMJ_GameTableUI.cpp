#include "QZHOUJJMJ_GameTableUI.h"

#include "QZHOUJJMJ_SetLayer.h"

#include "HNLogicExport.h"
#include "HNLobby/GameChildLayer/GameUserPopupInfoLayer.h"
using namespace std;
namespace QZHOUJJMJ
{
	/**************************************************************************/

	const int maxOrder = 200;
	const int tipOrder = 99;
	const int Max_Zorder = 100;
	GameTableUI::GameTableUI()
		:
		_labekaCount(nullptr),
		_backBtn(nullptr)
	{
		_isGameEnd = false;
		_isFangzhuFufei = false;
		_isShowAllBtn = false;
	}

	GameTableUI::~GameTableUI()
	{
		delete _tableLogic;
		_tableLogic = nullptr;
		//Social::SocialSysManager::getInstance()->removeObserver(this);
	}

	GameTableUI* GameTableUI::create(INT deskNo, bool autoCreate)
	{
		auto ui = new GameTableUI;
		if (ui && ui->init(deskNo, autoCreate))
		{
			ui->autorelease();
			return ui;
		}
		CC_SAFE_DELETE(ui);
		return nullptr;
	}

	bool GameTableUI::init(INT deskNo, bool bAutoCreate)
	{
		if (!HNGameUIBase::init())
		{
			return false;
		}
		//创建按钮框
		_BtnLayout = Layout::create();
		_BtnLayout->setPosition(Vec2(1140, 670));
		//_BtnLayout->setAnchorPoint(Vec2::ZERO);
		_BtnLayout->setSize(Size(400, 50));
		_BtnLayout->setZOrder(Max_Zorder + 200);
		this->addChild(_BtnLayout);
		//HNPreLoadResource::getInstance()->setLoadResPath("Games/QZHOUJJMJ/Music/sound.xml");
		_deskNo = deskNo;
		Size winSize = Director::getInstance()->getWinSize();
		//Social::SocialSysManager::getInstance()->addObserver(this);
		_mahjongManager = GameManager::create();
		_mahjongManager->setName("_mahjongManager");
		this->addChild(_mahjongManager, 10);

		int roomID = 0;
		ComRoomInfo* pInfo = RoomLogic()->getInstance()->getSelectedRoom();
		if (pInfo)
		{
			roomID = pInfo->uRoomID;
		}

		_tableLogic = new GameTableLogic(this, deskNo, bAutoCreate);
		_tableLogic->sendGameInfo();

		//设置按钮
		auto set = Button::create("platform/Games/sz.png", "platform/Games/sz.png", "");
		set->addTouchEventListener(CC_CALLBACK_2(GameTableUI::clickSetEventCallBack, this));
		set->setPosition(Vec2(winSize.width - 45, _winSize.height - 60));
		_mahjongManager->addChild(set, 10);

		//初始化比赛场控制器
		if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
		{
			GameMatchController* matchController = GameMatchController::create(deskNo);
			matchController->setRankingTipPos(Vec2(_winSize.width * 0.78f, _winSize.height * 0.95f), Color4B::WHITE, 22);
			this->addChild(matchController, 10);
		}

		initDissolveRoomOpervation();
		//初始化VIP房间控制器

		//if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			roomController = VipRoomController::create(deskNo);
			VipDeskInfoResult Info = roomController->getRoomResult();
			VipDeskInfoResult* p = roomController->getRoomResult1();
			roomController->onSetVipInfoCallBack = [this](){
				// 更新房主标志显示
				//setDeskInfo();
				VipDeskInfoResult Info = roomController->getRoomResult();
				GameManager::getInstance()->setRozomInfo(roomController->getRoomResult());
			};
			roomController->onDissmissCallBack = [this]() {
				_allRoundsEnd = _mahjongManager->getIsAllRoundEnd();
				if (!_allRoundsEnd)
				{
					RoomLogic()->close();
					GamePlatform::createPlatform();
					/*auto prompt = GamePromptLayer::create();
					prompt->showPrompt(GBKToUtf8("桌子已解散"));
					prompt->setCallBack([=]() {
					});*/
				}
				else
				{
					if (m_pResultUI)
					{
						m_pResultUI->showAllResultNode(true);
					}
					else
					{
						MJGameResult* pResultUI = (MJGameResult*)createGameFinishNode();
						pResultUI->showAllResultNode(true);
					}

				}
			};
			roomController->onClickButtonCallBack = [this](int Index){
				//发送领取红包
				if (Index < 0 || Index > 5)
				{
					return;
				}
				else
				{
					//发送
					GameManager::getInstance()->CheckReceive();
				}
			};
			roomController->onReceiveCallBack = [this](){
				GameManager::getInstance()->CheckActivity();
			};
			addChild(roomController, Max_Zorder + 200);
			if (roomController->getRoomResult().iType == 1)
			{
				_isKe = true;
			}

			//if (roomController != nullptr)
			//{
			//	
			//}

			//m_Wanfa = roomController->getRoomResult().DeskConfig[2];
			//邀请好友按钮
			roomController->setInvitationBtnTexture("QZHOUJJMJ/cocos/new/yq.png");
			// 修改邀请好友位置
			roomController->setInvitationBtnPos(Vec2(_winSize.width / 2, 60));
			// 修改返回位置 隐藏
			roomController->setReturnBtnPos(Vec2(_winSize.width + 500.0f, 0));
			// 修改解散位置 隐藏
			roomController->setDismissBtnPos(Vec2(_winSize.width + 500.0f, 0));
			//修改查看房间信息按钮位置
			roomController->setRoomResultBtnPos(Vec2(_winSize.width + 190, _winSize.height - 50));
			//修改房间号位置
			roomController->setRoomNumPos(Vec2(5, _winSize.height * 0.98f));
			//修改局数位置
			roomController->setPlayCountPos(Vec2(5, _winSize.height * 0.93f));
			//修改底分位置
			roomController->setDifenPos(Vec2(5, _winSize.height * 0.88f));

			//调整红包任务位置
			roomController->setActivityPosition(281, 125.0f);

			//}
			gameChatLayer();
			//语言按钮
			Button* voice = Button::create("platform/Games/yy.png", "", "platform/Games/yy.png"); //语音6.3
			voice->setPosition(Vec2(winSize.width - 45, 200));
			voice->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);
			_mahjongManager->addChild(voice, 10);
			//聊天按钮
			Button* chat = Button::create("platform/Games/dz.png", "", "platform/Games/dz.png");//聊天6.3
			chat->setPosition(Vec2(winSize.width - 45, 300));
			_mahjongManager->addChild(chat, 10);

			voice->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
				if (_chatLayer == nullptr)
				{
					//显示聊天界面
					gameChatLayer();
				}
				_chatLayer->voiceChatUiButtonCallBack(pSender, type);
			});
			chat->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
				if (_chatLayer == nullptr)
				{
					//显示聊天界面
					gameChatLayer();
				}
				if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
				{
					_chatLayer->setCustomchat();
				}
				_chatLayer->showChatLayer();
			});
			//战绩
			/*Button* record = Button::create("QZHOUJJMJ/cocos/Button/btn_record.png", "", "QZHOUJJMJ/cocos/Button/btn_record.png");
			record->setPosition(Vec2(-30, 0));
			_BtnLayout->addChild(record, maxOrder);
			record->addClickEventListener([=](Ref* ref) {
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, S_C_GAME_RECORD, 0, NULL);

			});*/
			//菜单收起展开
			/*Button* btnDrop = Button::create("QZHOUJJMJ/cocos/Button/btn_drop.png", "", "QZHOUJJMJ/cocos/Button/btn_drop.png");
			btnDrop->setPosition(Vec2(1230, _BtnLayout->getPositionY() + 2));
			this->addChild(btnDrop, maxOrder + 201);
			btnDrop->addClickEventListener([=](Ref* ref) {
				if (!_isShowAllBtn)
				{
					btnDrop->runAction(Sequence::create(MoveBy::create(0.5f, Vec2(-320, 0)), ScaleTo::create(0.1f, -1), NULL));
					roomController->getRoomInfoBtn()->runAction(MoveBy::create(0.5f, Vec2(-320, 0)));
					_mahjongManager->_tuoguanBtn->runAction(MoveBy::create(0.5f, Vec2(-320, 0)));
					_BtnLayout->runAction(MoveBy::create(0.5f, Vec2(-320, 0)));
					_isShowAllBtn = true;
				}
				else if (_isShowAllBtn)
				{
					btnDrop->runAction(Sequence::create(MoveBy::create(0.5f, Vec2(320, 0)), ScaleTo::create(0.1f, 1), NULL));
					roomController->getRoomInfoBtn()->runAction(MoveBy::create(0.5f, Vec2(320, 0)));
					_mahjongManager->_tuoguanBtn->runAction(MoveBy::create(0.5f, Vec2(320, 0)));
					_BtnLayout->runAction(MoveBy::create(0.5f, Vec2(320, 0)));
					_isShowAllBtn = false;

				}
			});*/

			return true;
		}
	}



	void GameTableUI::showBackBtn(bool visible)
	{
		_backBtn->setVisible(visible);
	}

	void GameTableUI::dealLeaveDesk()
	{
		HNAudioEngine::getInstance()->resumeBackgroundMusic();
		if (_isGameEnd)
		{
			return;
		}
		RoomLogic()->close();
		GamePlatform::returnPlatform(LayerType::PLATFORM);
	}

	void GameTableUI::agreeGame(const sitDir& dir)
	{
		_mahjongManager->userAgree(dir);
	}



	/*****************************************************************************************/

	void GameTableUI::outCard(const sitDir& dir, const INT& number)
	{
		_mahjongManager->outCard(dir, number);

	}

	void GameTableUI::catchCard(const sitDir& dir, const INT& number)
	{
		_mahjongManager->catchCard(dir, number);
	}



	void GameTableUI::setDissloveBtState()
	{	
		
	}

	void GameTableUI::setGamecount(int counts)
	{
		if (_labekaCount)
		{
			/*std::string str3 = PromptDictionary::getInstance().findPromptByKey("HNRoom_HNRoomLogic_SystemMes101");
			_labekaCount->setString(StringUtils::format(str3.c_str(),counts+1));
			_labekaCount->setVisible(true);*/
		}
	}




	//这一课是否结束
	void GameTableUI::setIsKeEnd(bool Isend)
	{
		if (Isend)
		{
			_isGameEnd = true;
			///*GamePromptLayerHN* tipLayer = GamePromptLayerHN::create();*/
			//tipLayer->setCallBack([&]()
			//{
			//	GamePlatform::returnPlatform();
			//});
		/*	std::string str = PromptDictionary::getInstance().findPromptByKey("HNRoom_HNRoomLogic_SystemMes88");
			tipLayer->setPrompt(str);
			this->addChild(tipLayer, tipOrder);*/
		}
	}


	//设置是否是房主付费
	void GameTableUI::setFuFeiRule(bool isFangzhu)
	{

		_isFangzhuFufei = isFangzhu;


	}

	void GameTableUI::showWechat(bool visible)
	{
		_inviteBtn->setVisible(visible);
	}


	/*****************************************************************************************/
	void GameTableUI::addUser(const sitDir& dir, UserInfoStruct *user)
	{
		_mahjongManager->addUser(dir, user);

	}

	void GameTableUI::UpDateUserLocation(int Index)
	{
		_mahjongManager->UpDateUserLocation(Index);
	}

	void GameTableUI::removeUser(const sitDir& dir)
	{
		_mahjongManager->userLeave(dir);
	}

	void GameTableUI::openChatDialog()
	{
	}

	void GameTableUI::onSocketMessage(UINT MainID, UINT AssistantID,const rapidjson::Document& doc)
	{
		

	}
	void GameTableUI::clickWebchatEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		

		//std::string str = PromptDictionary::getInstance().findPromptByKey("Word_QZHOUJJMJ_GameTableUI_tips3");
		//if (!_isFangzhuFufei)
		//{
		//	str = PromptDictionary::getInstance().findPromptByKey("Word_QZHOUJJMJ_GameTableUI_tips5");
		//}
		//char num[200]={0};
		//sprintf(num,str.c_str(),_deskNo);
		//std::string web = GameConfig::getInstance()->getShareIOPic();
		//std::string pic = GameConfig::getInstance()->getSharePic();
		////std::string imagePath = FileUtils::getInstance()->fullPathForFilename(pic);
		////ThridShare::getInstance()->shareWeChat(web.c_str(),num,"",imagePath.c_str());
		//ThridLogin::getInstance()->share("", num, web, pic);
	}

	void GameTableUI::clickBackEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		GamePlatform::returnPlatform(LayerType::PLATFORM);
	}

	void GameTableUI::clickSetEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		auto setLayer = GameSetLayer::create();
		setLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 301);
		setLayer->setName("setLayer");
		setLayer->onExitCallBack = [=]() {
			if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				RoomLogic()->close();
				GamePlatform::returnPlatform(LayerType::PLATFORM);
			}
			else
			{
				if (roomController)
				{
					roomController->returnBtnCallBack(pSender);
				}
				else
				{
					_tableLogic->sendStandUp();
				}
			}
			removeSetLayer();
		};

		setLayer->onDisCallBack = [=]() {

			if (roomController) roomController->dismissBtnCallBack(pSender);
			removeSetLayer();
		};
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			//设置游戏状态按钮
			setLayer->setGameStateView();
		}
		else
		{
			//设置金币场退出
			setLayer->setGameGlodExit();
		}
		/*
		auto setLayer = QZHOUJJMJSetLayer::create();
		setLayer->showSet(this, 301);
		setLayer->setName("setLayer");
		setLayer->onExitCallBack = [=]() {
			if (roomController)
			{
				roomController->returnBtnCallBack(pSender);
			}
			else
			{
				_tableLogic->sendStandUp();
			}
			removeSetLayer();
		};

		setLayer->onDisCallBack = [=]() {

			if (roomController) roomController->dismissBtnCallBack(pSender);
			removeSetLayer();
		};
		*/
	}

	void GameTableUI::initDissolveRoomOpervation()
	{
		//Size winsize=Director::getInstance()->getWinSize();
		//_dissloveRoom =DissolveRoom::create(_deskNo,PlatformLogic()->loginResult.dwUserID);
		//_dissloveRoom->setAnchorPoint(Vec2(0.5,0.5));
		//_dissloveRoom->setPosition(Vec2(winsize.width+10,730));		
		//_dissloveRoom->setGameLogic(_tableLogic);
		//_dissloveRoom->onFangZhuCallBack(true);
		//_dissloveRoom->setVisible(true);
		//this->addChild(_dissloveRoom,maxOrder);

	}


	void GameTableUI::calSameIpUser(std::vector<UserInfoStruct*>& same)
	{
		std::vector<UserInfoStruct*> users;
		UserInfoModule()->findDeskUsers(_deskNo, users);
		if (PLAYER_COUNT != users.size())
		{
			return;
		}
		
		for (size_t i = 0; i < users.size(); i++)
		{
			std::vector<UserInfoStruct*> findSameIp;
			findSameIp.push_back(users.at(i));
			for (size_t j = i+1;j < users.size(); j++)
			{
				if (users.at(i)->dwUserIP == users.at(j)->dwUserIP)
				{
					findSameIp.push_back(users.at(j));
				}
			}
			
			if (findSameIp.size()>=2)
			{
				for (auto v:findSameIp)
				{
					same.push_back(v);
				}
				return;
			}
		}
	}

	Node* GameTableUI::createGameFinishNode()
	{
		if (m_pResultUI)
		{
			m_pResultUI = NULL;
		}
		_mahjongManager->setIsAllRoundEnd(true);
		//_mahjongManager->setPingbiTing(false);
		m_pResultUI = MJGameResult::create();
		this->addChild(m_pResultUI, 1000);
		return m_pResultUI;
	}

	void GameTableUI::checkIpSameTips()
	{
		//
		std::vector<UserInfoStruct*> same;
		calSameIpUser(same);

		/*if (same.empty()==false)
		{
		std::string tips;
		for (size_t i =0;i<same.size();++i)
		{
		tips+=GBKToUtf8(same.at(i)->nickName);
		if (i!=same.size()-1)
		{
		tips+=PromptDictionary::getInstance().findPromptByKey("Word_QZHOUJJMJ_GameTableUI_he");
		}
		tips+="\n";
		}
		tips+=PromptDictionary::getInstance().findPromptByKey("Word_QZHOUJJMJ_GameTableUI_XiangTongIp");

		GameTips* test = GameTips::getInstance();
		test->setLocalZOrder(maxOrder);
		test->setDissolveRoom(_dissloveRoom);
		test->setTipsText(tips);


		}*/

	}

	void GameTableUI::showGameDeskNotFound()
	{
		
	}

	void GameTableUI::onGameDisconnect(bool isReconnect)
	{
		
	}

	void GameTableUI::onCloseResult()
	{
		if (m_pResultUI)
		{
			m_pResultUI->HideAll();
		}
	}

	void GameTableUI::onChatTextMsg(BYTE seatNo, CHAR msg[])
	{
		auto userInfo = _tableLogic->getUserBySeatNo(seatNo);
		auto viewSeatNo = _tableLogic->logicToViewSeatNo(seatNo);

		auto herd = COCOS_NODE(ImageView, StringUtils::format("head%d", viewSeatNo));
		Vec2 bubblePostion = herd->getPosition();

		bool isFilpped = true;

		if (viewSeatNo == 0 || viewSeatNo == 3)
		{
			isFilpped = false;
			bubblePostion.x = herd->getPositionX() + 50;
		}
		else
		{
			isFilpped = true;
			bubblePostion.x = herd->getPositionX() - 50;
		}

		_chatLayer->onHandleTextMessage(herd->getParent(), bubblePostion, msg, userInfo->nickName, userInfo->bBoy, isFilpped, viewSeatNo);
	}

	void GameTableUI::onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index)
	{
		if (_mahjongManager)
		{
			auto pLayer = dynamic_cast<GameUserPopupInfoLayer*>(_mahjongManager->getChildByName(GAME_POPUPINFO_LAYER_NAME));
			if (pLayer == nullptr)
			{
				pLayer = GameUserPopupInfoLayer::create();
				pLayer->setMaxPlayerCount(PLAY_COUNT);
			}
			auto viewSSeatNo = _tableLogic->logicToViewSeatNo(sendSeatNo);
			auto viewTSeatNo = _tableLogic->logicToViewSeatNo(TargetSeatNo);
			auto herd = COCOS_NODE(ImageView, StringUtils::format("head%d", viewSSeatNo));
			auto herd1 = COCOS_NODE(ImageView, StringUtils::format("head%d", viewTSeatNo));
			pLayer->doAction(sendSeatNo, TargetSeatNo, index, herd, herd1);
		}
	}

	void GameTableUI::onChatVoiceMsg(int userID, int voiceID, int voiceTime)
	{
		if (nullptr == _chatLayer) return;

		UserInfoStruct* userInfo = _tableLogic->getUserByUserID(userID);

		if (nullptr == userInfo) return;
		auto viewSeatNo = _tableLogic->logicToViewSeatNo(userInfo->bDeskStation);
		auto herd = COCOS_NODE(ImageView, StringUtils::format("head%d", viewSeatNo));

		Vec2 bubblePostion = herd->getPosition();

		Size headSize = herd->getContentSize();

		bool isFilpped = true;

		if (viewSeatNo == 0 || viewSeatNo == 3)
		{
			isFilpped = false;
			bubblePostion.x = herd->getPositionX() + 50;
		}
		else
		{
			isFilpped = true;
			bubblePostion.x = herd->getPositionX() - 50;
		}
		_chatLayer->onHandleVocieMessage(herd->getParent(), bubblePostion, voiceID, userInfo->nickName, isFilpped, viewSeatNo, voiceTime);
	}

	void GameTableUI::setDeskInfo()
	{
		auto info = _tableLogic->getUserByUserID(HNPlatformConfig()->getMasterID());
		if (info)
		{
			auto viewSeatNo = _tableLogic->logicToViewSeatNo(info->bDeskStation);
			COCOS_NODE(Sprite, StringUtils::format("zhuang%d", viewSeatNo))->setVisible(true);
		}

// 		if (HNPlatformConfig()->getNowCount() > 0)
// 		{
// 			Layout* tableLayout = (Layout*)(_tableWidget->getChildByName("gold_Poker"));
// 			Layout* layout_buttons = (Layout*)(tableLayout->getChildByName("layout_middle"));
// 			Button* btn_start = (Button*)(layout_buttons->getChildByName("btn_start"));
// 			btn_start->setPositionX(_winSize.width / 2);
// 		}
	}

	void GameTableUI::removeSetLayer()
	{
		auto setLayout = this->getChildByName("setLayer");
		if (setLayout)
		{
			setLayout->removeFromParent();
		}
	}

	void GameTableUI::gameChatLayer()
	{
		_chatLayer = GameChatLayer::create();
		addChild(_chatLayer, Max_Zorder + 500);
		_chatLayer->setPosition(_winSize / 2);
		_chatLayer->onSendTextCallBack = [=](const std::string msg) {
			if (!msg.empty())
			{
				_tableLogic->sendChatMsg(msg);
			}
		};
	}

	void GameTableUI::sendRecordData(S_C_GameRecordResult* data)
	{
		int PlayerCount = 0;
		CHAR* szName[10];
		for (int j = 0; j < 10; j++)
		{
			szName[j] = "";
		}
		int index = 0;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* pUser = _tableLogic->getUserBySeatNo(i);
			if (pUser)
			{
				PlayerCount++;
				szName[index] = pUser->nickName;
				index++;
			}
		}

		auto gameRecord = GameRecord::createWithData(szName, data->JuCount, data->GameCount, PlayerCount);
		addChild(gameRecord, 100000000);
	}
	void GameTableUI::setBottomScore(int Score)
	{
		if (Score > 0)
		{
			if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
			{
				//CCLOG(const char*(RoomLogic()->getRoomRule()));
			}
			else
			{
				//屏蔽比赛场显示倍数
				if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
					return;
				auto room = RoomLogic()->getSelectedRoom();
				if (room == nullptr) return;
				std::string str = "";
				if (room->iLessPoint == 1000)
				{//低倍场
					str = GBKToUtf8("低倍场");
				}
				else if (room->iLessPoint == 10000)
				{//高倍场
					str = GBKToUtf8("高倍场");
				}
				std::string name = StringUtils::format(GBKToUtf8("%s\n倍数:%d"), str.c_str(), Score);
				if (this->getChildByName("BottonScore") == nullptr)
				{
					_BottonScore = Label::createWithTTF(name, "Games/QZHOUMJ/cocos/RTWSYueRoudGoG0v1-Regular.ttf", 24);
					_BottonScore->setPosition(10, _winSize.height - 10);
					_BottonScore->setAnchorPoint(Vec2(0, 1));
					_BottonScore->setAlignment(TextHAlignment::LEFT, TextVAlignment::TOP);
					_BottonScore->setName("BottonScore");
					_BottonScore->setZOrder(100);
					addChild(_BottonScore);
				}
				else
				{
					_BottonScore->setString(name);
				}
			}
		}
		
		
	}

	void GameTableUI::setActivityNum(int current, int toal, int CanTake)
	{
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			if (roomController)
			{
				roomController->setOpenActivity(true);
				roomController->setCurrentTask(current, toal);
				if (CanTake > 0)
				{
					roomController->setIsStandard(true);
					roomController->setCanTakeNum(CanTake);
				}
				else
				{
					roomController->setIsStandard(false);
				}
			}
		}
	}
	
	void GameTableUI::setReceiveState(int ReceiveType, int AwardNum)
	{
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			if (roomController)
			{
				roomController->setAnimationForTake(ReceiveType, AwardNum);
				//if (ReceiveType == 0 || ReceiveType == 1)
				//{
				//	//中金币
				//	
				//}
				//else if (ReceiveType == 2 || ReceiveType == 3)
				//{
				//	//中房卡

				//}
				//else if (ReceiveType == 4 || ReceiveType == 5)
				//{
				//	//中红包

				//}
			}
		}
	}

};
