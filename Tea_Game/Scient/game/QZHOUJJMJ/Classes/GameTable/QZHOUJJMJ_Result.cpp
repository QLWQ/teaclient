#include "QZHOUJJMJ_Result.h"
#include "QZHOUJJMJ_MessageHead.h"
#include "QZHOUJJMJ_MahjongCard.h"
#include "QZHOUJJMJ_GameTableUI.h"
#include "HNLogicExport.h"
#include "HNLobbyExport.h"
#include "HNOpenExport.h"
#include "HNLobby/GameChildLayer/GameShareLayer.h"


static void static_quicksort(int data[], size_t left, size_t right) {
	size_t p = (left + right) / 2;
	int pivot = data[p];
	for (size_t i = left, j = right; i < j;) {
		while (! (i >= p || pivot < data[i]))
			i++;
		if (i < p) {
			data[p] = data[i];
			p = i;
		}
		while (! (j <= p || data[j] < pivot))
			j--;
		if (j > p) {
			data[p] = data[j];
			p = j;
		}
	}
	data[p] = pivot;
	if (p - left > 1)
		static_quicksort(data, left, p - 1);
	if (right - p > 1)
		static_quicksort(data, p + 1, right);
}
namespace QZHOUJJMJ
{
	MJGameResult::MJGameResult()
		: m_pZongChengJiNode(nullptr)
		, m_pCurChengJiNode(nullptr)
	{
		_IsShowCard = true;
	}

	MJGameResult::~MJGameResult()
	{
		clearPlayerData();
	}

	bool MJGameResult::init()
	{
		if (!HNLayer::init())
		{
			return false;
		}
		//quicklyShade(150);
		clearPlayerData();
		cocos2d::Size winSize = Director::getInstance()->getWinSize();
		
		//触摸事件屏蔽？
		/*auto callback = [](Touch * ,Event *)      
		{     
			return true;          
		};  
		auto listener = EventListenerTouchOneByOne::create();  
		listener->onTouchBegan = callback;  
		listener->setSwallowTouches(true);  
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);  */

		{
			//总成绩界面
			m_pZongChengJiNode = CSLoader::createNode("platform/Games/Result/JieSuanNode1.csb");
			m_pZongChengJiNode->setPosition(cocos2d::Vec2(winSize.width/2.0f, winSize.height/2.0f));
			this->addChild(m_pZongChengJiNode, 400);

			auto pCloseZongCJNodeFunc = [&](cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
			{
				if (Widget::TouchEventType::ENDED != touchtype)	return;
				//GameManager::getInstance()->onGameStar(1);
				if (_isGameEnd)
				{
					m_pZongChengJiNode->setVisible(false);
					m_pCurChengJiNode->setVisible(false);
					RoomLogic()->close();
					GamePlatform::createPlatform();
				}
				else
				{
					m_pZongChengJiNode->setVisible(false);
					m_pCurChengJiNode->setVisible(true);
				}
			};
			auto  pBgNode = dynamic_cast<ImageView*>(m_pZongChengJiNode->getChildByName("bg"));
			Button* closeBtn = dynamic_cast<Button*>(pBgNode->getChildByName("closeButton"));
			closeBtn->addTouchEventListener(pCloseZongCJNodeFunc);

			auto shareButton = dynamic_cast<Button*>(pBgNode->getChildByName("share_bt"));
			shareButton->addTouchEventListener(CC_CALLBACK_2(MJGameResult::onChickjieCallBack,this));
			m_PlayerDataVec.clear();
			for (int i=0; i<PLAY_COUNT; i++)
			{
				CPlayerData* pData = new CPlayerData();
				pData->_mainNode = pBgNode->getChildByName(StringUtils::format("player%d", i));
				pData->_mainNode->setVisible(false);
				pData->_headImage = dynamic_cast<ui::ImageView*>(pData->_mainNode->getChildByName("headnode")->getChildByName("head"));
				pData->_nameText = dynamic_cast<ui::Text*>(pData->_mainNode->getChildByName("name"));
				pData->_idText = dynamic_cast<ui::Text*>(pData->_mainNode->getChildByName("id"));
				pData->_fangZhuTip = pData->_mainNode->getChildByName("fangzhu");
				pData->_fangZhuTip->setVisible(false);
				pData->_allRoomScoreText = dynamic_cast<ui::Text*>(pData->_mainNode->getChildByName("scoreNode")->getChildByName("text"));
				//pData->_allRoomScoreText->setColor(Color3B(255, 255, 255));

				cocos2d::Node* pValueNode = pData->_mainNode->getChildByName("valuenode");
				pData->_pinghuText = dynamic_cast<ui::Text*>(pValueNode->getChildByName("pinghunum"));
				pData->_zimoText = dynamic_cast<ui::Text*>(pValueNode->getChildByName("zimonum"));
				pData->_sanjindaoText = dynamic_cast<ui::Text*>(pValueNode->getChildByName("sanjinnum"));
				pData->_youjinText = dynamic_cast<ui::Text*>(pValueNode->getChildByName("youjinnum"));
				pData->_shuangjinText = dynamic_cast<ui::Text*>(pValueNode->getChildByName("shuangyounum"));
				pData->_sanjinText = dynamic_cast<ui::Text*>(pValueNode->getChildByName("sanyounum"));

				m_PlayerDataVec.push_back(pData);
			}
		}
		{
			//当前局界面
			m_pCurChengJiNode = CSLoader::createNode(COCOS_PATH+"BenjuJiesunNode.csb");
			m_pCurChengJiNode->setPosition(cocos2d::Vec2(winSize.width/2.0f, winSize.height/2.0f));
			this->addChild(m_pCurChengJiNode, 300);
			auto  pBgNode = dynamic_cast<ImageView*>(m_pCurChengJiNode->getChildByName("bg"));
			Button* closeBtn = dynamic_cast<Button*>(pBgNode->getChildByName("close_btn"));
			closeBtn->loadTextures("platform/Games/Result/jryx.png", "platform/Games/Result/jryx.png", "");
			closeBtn->setContentSize(Size(218,67));
			closeBtn->setPosition(Vec2(347, 67));
			//游戏结束的时候

			closeBtn->addTouchEventListener(CC_CALLBACK_2(MJGameResult::pCloseCurJuNodeFunc, this));
			m_btnshare = dynamic_cast<Button*>(pBgNode->getChildByName("share_bt"));
			m_btnshare->setPosition(Vec2(936, 67));
			m_btnshare->loadTextures("platform/Games/Result/fxyx.png", "platform/Games/Result/fxyx.png", "");
			m_btnshare->setContentSize(Size(218, 67));
			m_btnshare->addTouchEventListener(CC_CALLBACK_2(MJGameResult::onChickjieCallBack, this));

			auto mingxiButton = dynamic_cast<Button*>(pBgNode->getChildByName("mingxi_btn"));
			mingxiButton->addTouchEventListener(CC_CALLBACK_2(MJGameResult::onChickMXCallBack, this));
			mingxiButton->setVisible(false);

			auto pShowTotalNodeFunc = [&](cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
			{
				if (Widget::TouchEventType::ENDED != touchtype)	return;
				m_pZongChengJiNode->setVisible(true);
			};
			m_btnshowtotal = dynamic_cast<Button*>(pBgNode->getChildByName("showtotal_btn"));
			m_btnshowtotal->setVisible(false);
			m_btnshowtotal->loadTextures("platform/Games/Result/ckzcj.png", "platform/Games/Result/ckzcj.png", "");
			m_btnshowtotal->setContentSize(Size(218, 67));
			m_btnshowtotal->setPosition(Vec2(641.5, 67));
			m_btnshowtotal->addTouchEventListener(pShowTotalNodeFunc);

			for (int i=0; i<PLAYER_COUNT; i++)
			{
				cocos2d::Node* pNode = pBgNode->getChildByName(StringUtils::format("player%d", i));
				pNode->setVisible(false);
				cocos2d::Node* pCardLayer = pBgNode->getChildByName(StringUtils::format("Card%d", i));
				CPlayerData* pData = m_PlayerDataVec[i];
				
				pData->_curNameText = dynamic_cast<ui::Text*>(pNode->getChildByName("nickname"));
				cocos2d::Node* pCardNode = pNode->getChildByName("cardnode");
				pCardNode->setScale(0.4f);
				pCardNode->setVisible(false);
				pData->_curCardNodePos = Vec2(pCardNode->getPositionX()-70,pCardNode->getPositionY() - 40);
				pData->_curCardScale = pCardNode->getScale();
		
				pData->_curZongFenText = dynamic_cast<ui::TextAtlas*>(pNode->getChildByName("zongfen"));
				pData->_curPanshuText = dynamic_cast<ui::TextAtlas*>(pNode->getChildByName("panshu"));
				pData->_xiangxiLayer = dynamic_cast<Node*>(pNode->getChildByName("xiangxiLayer"));
				pData->_xiangxiLayer->setVisible(false);
				pData->_curIcon = dynamic_cast<ui::ImageView*>(pNode->getChildByName("Icon"));
				pData->_img_zhuang = dynamic_cast<ui::ImageView*>(pNode->getChildByName("zhuang"));
				pData->_img_zhuang->setLocalZOrder(2000);
				pData->_curCardNode = pCardLayer;
				pData->_curMainNode = pNode;			
			}
		}
		m_pZongChengJiNode->setVisible(false);
		return true;
	}

	void MJGameResult::pCloseCurJuNodeFunc(Ref* pSender, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::BEGAN == type)
		{
			//GameManager::getInstance()->setIsAllRoundEnd(false);
			if (_isGameEnd)
			{
				m_pZongChengJiNode->setVisible(true);
				m_pCurChengJiNode->setVisible(false);
			}
			else
			{
				GameManager::getInstance()->userAuto(false, true, false);
				if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
				{
					GameManager::getInstance()->onGameStar(1);
				}
				//处理游戏中途解散显示结算
				m_pZongChengJiNode->setVisible(false);
				m_pCurChengJiNode->setVisible(false);
				//this->removeFromParentAndCleanup(true);
			/*if (_isGameEnd)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("当前房间已用完，请重新创建。"));
				prompt->setCallBack([=]() {
					RoomLogic()->close();
					GamePlatform::createPlatform();
				});
			}*/
			}
		}
	}

	void MJGameResult::onChickjieCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (type != cocos2d::ui::Widget::TouchEventType::ENDED)
		{
			return;
		}  //截屏

		auto shareLayer = GameShareLayer::create();
		shareLayer->setName("shareLayer");
		shareLayer->SetShareInfo("", "", "");
		shareLayer->show();
	}



	//查看明细
	void MJGameResult::onChickMXCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (type != cocos2d::ui::Widget::TouchEventType::ENDED)
		{
			
			return;
		} 
		//显示牌局
		_IsShowCard = !_IsShowCard;
		if (_IsShowCard)
		{
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				CPlayerData* pData = m_PlayerDataVec[i];
				pData->_curCardNode->setVisible(true);
				pData->_xiangxiLayer->setVisible(false);
			}
			//这里是显示查看明细
			auto mingxiButton = dynamic_cast<Button*>(pSender);
			mingxiButton->loadTextures(SPRITE_PATH + "btn_mingxi", SPRITE_PATH + "btn_mingxi1", SPRITE_PATH + "btn_mingxi1");
			
		}
		else
		{
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				CPlayerData* pData = m_PlayerDataVec[i];
				pData->_curCardNode->setVisible(false);
				pData->_xiangxiLayer->setVisible(true);
			}
		
			auto mingxiButton = dynamic_cast<Button*>(pSender);
			//这里显示查看手牌
			mingxiButton->loadTextures(SPRITE_PATH + "btn_shoupai", SPRITE_PATH + "btn_shoupai1", SPRITE_PATH + "btn_shoupai1");
		}
		
	}









	void MJGameResult::setShowUserInfo(UserInfoStruct* pUserInfo, bool isfangzhu, int allRoomScore, int dir)
	{
		int index = dir; 
		CPlayerData* pData = m_PlayerDataVec[index];
		//pData->_fangZhuTip->setVisible(isfangzhu);
		if (pUserInfo == nullptr)
		{
			auto  pBgNode = dynamic_cast<ImageView*>(m_pCurChengJiNode->getChildByName("bg"));
			cocos2d::Node* pNode = pBgNode->getChildByName(StringUtils::format("player%d", index));
			pNode->setVisible(false);
			return;
		}
		pData->_headImage->setVisible(true);
		pData->_mainNode->setVisible(true);
		auto  pBgNode = dynamic_cast<ImageView*>(m_pCurChengJiNode->getChildByName("bg"));
		cocos2d::Node* pNode = pBgNode->getChildByName(StringUtils::format("player%d", index));
		pNode->setVisible(true);
		//获取头像
	/*	cocos2d::Node* headParentNode = pData->_headImage->getParent();
		std::string headPic = HeadManager::getHeadImage(pUserInfo->bBoy,pUserInfo->bLogoID);
		pData->_headImage->loadTexture(headPic);

		std::string homePage =  pUserInfo->headUrl;
		VipHeadSprite* sprite = VipHeadSprite::create(pData->_headImage,pUserInfo->dwUserID,homePage,pUserInfo->iVipTime);
		sprite->setPosition(pData->_headImage->getPosition());			
		headParentNode->addChild(sprite);*/
		Tools::TrimSpace(pUserInfo->nickName);
		string strName = pUserInfo->nickName;
		if (strName.length() > 12)
		{
			strName.erase(13, strName.length());
			strName.append("...");
		}
		pData->_nameText->setString(GBKToUtf8(strName));
		pData->_curNameText->setString(GBKToUtf8(strName));

		pData->_idText->setString(StringUtils::format("ID:%d",pUserInfo->dwUserID));

		pData->_allRoomScoreText->setString(StringUtils::format("%d",allRoomScore));

		GameUserHead*	benjuHead = nullptr;
		benjuHead = GameUserHead::create(pData->_curIcon);
		benjuHead->show();
		std::string benjuname = pUserInfo->bBoy ? Player_Normal_M : Player_Normal_W;
		benjuHead->loadTexture(benjuname);
		benjuHead->setHeadByFaceID(pUserInfo->bLogoID);
		benjuHead->loadTextureWithUrl(pUserInfo->headUrl);
		//benjuHead->setVIPHead("", pUserInfo->iVipLevel);

		GameUserHead*	jiesuanHead = nullptr;
		jiesuanHead = GameUserHead::create(pData->_headImage);
		jiesuanHead->setAnchorPoint(Vec2::ZERO);
		jiesuanHead->show();
		std::string jiesuanname = pUserInfo->bBoy ? Player_Normal_M : Player_Normal_W;
		jiesuanHead->loadTexture(jiesuanname);
		jiesuanHead->setHeadByFaceID(pUserInfo->bLogoID);
		jiesuanHead->loadTextureWithUrl(pUserInfo->headUrl);
		//jiesuanHead->setVIPHead("", pUserInfo->iVipLevel);
		
	}

	void MJGameResult::setShowBeiLvInfo(UserInfoStruct* pUserInfo, int pinghuNum,
		int zimoNum, int sanjindaoNum, int youjinNum, int dir)
	{
		int index = dir;
		CPlayerData* pData = m_PlayerDataVec[index];
		//晋江麻将没有双金三金
		pData->_pinghuText->setString(StringUtils::format("%d", pinghuNum));
		//pData->_zimoText->setString(StringUtils::format("%d", anGangNum));
		pData->_zimoText->setString(StringUtils::format("%d", zimoNum));
		pData->_sanjindaoText->setString(StringUtils::format("%d", sanjindaoNum));
		pData->_youjinText->setString(StringUtils::format("%d", youjinNum));
	}

	void MJGameResult::ShowReSoultTitle(char hutype)
	{
		
		auto  pBgNode = dynamic_cast<ImageView*>(m_pCurChengJiNode->getChildByName("bg"));

		auto  img_titlebg = dynamic_cast<ImageView*>(pBgNode->getChildByName("img_titlebg"));

		auto  img_title = dynamic_cast<ImageView*>(img_titlebg->getChildByName("img_title"));
		auto  lable_num = dynamic_cast<TextAtlas*>(img_titlebg->getChildByName("lable_num"));
		img_title->setScale(0.8f);


		std::string str = "/2";
		std::string str1 = "/2";
		std::string str2 = "/2";
		std::string str3 = "/3";
		std::string str4 = "/4";
		std::string str5 = "/8";
		std::string str6 = "/16";
		std::string str7 = "/1";
		std::string str8 = "/4";
		switch (hutype)
		{
		case HuType::TYPE_PINGHU:
			{
				img_title->loadTexture(RESOULT_PATH + "img_pinghu.png");
				lable_num->setString(str7);
			}
		break;
		case HuType::TYPE_ZIMO:
			{
				img_title->loadTexture(RESOULT_PATH + "img_zimo.png");
				lable_num->setString(str);
			}
			break;
		case HuType::TYPE_TIANHU:
			{
				img_title->loadTexture(RESOULT_PATH + "img_zimo.png");
				lable_num->setString(str1);
			}
			break;
		case HuType::TYPE_SANJINDAO:
			{
				img_title->loadTexture(RESOULT_PATH + "img_sanjindao.png");
				lable_num->setString(str3);
			}
			break;
		case HuType::TYPE_YOUJIN:
			{
				img_title->loadTexture(RESOULT_PATH + "img_youjin.png");
				lable_num->setString(str4);
			}
			break;
		case HuType::TYPE_SHUANGYOU:
			{
				img_title->loadTexture(RESOULT_PATH + "img_shuangyou.png");
				lable_num->setString(str5);
			}
			break;
		case HuType::YTPE_SANYOU:
			{
				img_title->loadTexture(RESOULT_PATH + "img_sanyou.png");
				lable_num->setString(str6);
			}
			break;
		case HuType::TYPE_BAHUAYOU:
			{
				img_title->loadTexture(RESOULT_PATH + "img_bahuayou.png");
				lable_num->setString(str8);
			}
			break;
		case HuType::HuType_NO:
		{
			img_title->loadTexture(RESOULT_PATH + "img_liu.png");
			lable_num->setString("");
		}
			break;
		default:
			break;
		}
	
	
	}

















	void MJGameResult::setShowCurFen(UserInfoStruct* pUserInfo, bool isZimo, bool isTianhu, bool IsSanjindao, bool isQiangjin, int zongFen, int dir, bool isYoijin[3], int zongpan, char toHuPaiCardId, bool bahuayou, bool isZhuang)
	{
		int index = dir;
		CPlayerData* pData = m_PlayerDataVec[index];
		//总分
		{
			int toFen = zongFen;
			std::string toTextFormat = StringUtils::format("%d", toFen);
			if (toFen<0) toTextFormat = StringUtils::format("<%d", -toFen);
			if (toFen>0) toTextFormat = StringUtils::format(";%d", toFen);
			const int maxInter = 100000;
			if (abs(toFen)>maxInter)
			{
				int toZongFen = toFen/(maxInter/10);
				if(toZongFen>0)toTextFormat = StringUtils::format(";%d:", toZongFen);
				if(toZongFen<0)toTextFormat = StringUtils::format("<%d:", toZongFen);
			}
			pData->_curZongFenText->setString(toTextFormat);
		}
		
		//总盘数目
		{
			int toFen = zongpan;
			std::string toTextFormat = StringUtils::format("%d", toFen);
			pData->_curPanshuText->setString(toTextFormat);
		}
		
		pData->_img_zhuang->setVisible(isZhuang);

		pData->_curIcon->setVisible(true);


		//获取头像
		/*cocos2d::Node* headParentNode = pData->_curIcon->getParent();
		std::string headPic = HeadManager::getHeadImage(pUserInfo->bBoy, pUserInfo->bLogoID);
		pData->_curIcon->loadTexture(headPic);

		std::string homePage = pUserInfo->headUrl;
		VipHeadSprite* sprite = VipHeadSprite::create(pData->_curIcon, pUserInfo->dwUserID, homePage, pUserInfo->iVipTime);
		sprite->setPosition(pData->_curIcon->getPosition());
		headParentNode->addChild(sprite);*/


		
	}

	void MJGameResult::setShowCurPan(int jinpaipan, int huapaipan, int kezipan, int bugangpan, int minggangpai, int angangpan, int zipaipengpan, int dir)
	{
		
		int index = dir;
		CPlayerData* pData = m_PlayerDataVec[index];
		auto tempNode = pData->_xiangxiLayer;
		int Statpos = 156;
		int diffpos = 110;

		
		//金牌盘
		if (jinpaipan > 0)
		{
			auto sp = Sprite::create(SPRITE_PATH + "xiangxi_jin.png");
			sp->setPosition(Vec2(Statpos, 0));
			tempNode->addChild(sp);
			sp->setVisible(true);
			 Statpos += diffpos;
			 Text *pannum = cocos2d::ui::Text::create("TextBMFont", "fonts/Marker Felt.ttf", 24);
			 std::string str = StringUtils::format("%d", jinpaipan);
			 pannum->setString(str);
			 pannum->setAnchorPoint(Vec2(0.5, 0.5));
			 pannum->setColor(Color3B(119, 59, 13));
			 pannum->setVisible(true);
			 sp->addChild(pannum);
			 pannum->setPosition(Vec2(65.81, 13));
			 auto sppan = Sprite::create(SPRITE_PATH + "xiangxipan.png");
			 sppan->setVisible(true);
			 sppan->setPosition(Vec2(86.67, 12));
			 sp->addChild(sppan);
		}

		//花牌盘
		if (huapaipan > 0)
		{
			auto sp = Sprite::create(SPRITE_PATH + "xiangxi_hua.png");
			sp->setPosition(Vec2(Statpos, 0));
			tempNode->addChild(sp);
			sp->setVisible(true);
			Statpos += diffpos;
			Text *pannum = cocos2d::ui::Text::create("TextBMFont", "fonts/Marker Felt.ttf", 24);
			std::string str = StringUtils::format("%d", huapaipan);
			pannum->setString(str);
			pannum->setAnchorPoint(Vec2(0.5, 0.5));
			pannum->setColor(Color3B(119, 59, 13));
			pannum->setVisible(true);
			sp->addChild(pannum);
			pannum->setPosition(Vec2(65.81, 13));
			auto sppan = Sprite::create(SPRITE_PATH + "xiangxipan.png");
			sppan->setVisible(true);
			sppan->setPosition(Vec2(86.67, 12));
			sp->addChild(sppan);

		}

		//刻子盘
		if (kezipan > 0)
		{
			auto sp = Sprite::create(SPRITE_PATH + "xiangxi_kezi.png");
			sp->setPosition(Vec2(Statpos, 0));
			tempNode->addChild(sp);
			sp->setVisible(true);
			Statpos += diffpos;
			Text *pannum = cocos2d::ui::Text::create("TextBMFont", "fonts/Marker Felt.ttf", 24);
			std::string str = StringUtils::format("%d", kezipan);
			pannum->setString(str);
			pannum->setAnchorPoint(Vec2(0.5, 0.5));
			pannum->setColor(Color3B(119, 59, 13));
			pannum->setVisible(true);
			sp->addChild(pannum);
			pannum->setPosition(Vec2(65.81, 13));
			auto sppan = Sprite::create(SPRITE_PATH + "xiangxipan.png");
			sppan->setVisible(true);
			sppan->setPosition(Vec2(86.67, 12));
			sp->addChild(sppan);

		}


		//补杠盘
		if (bugangpan > 0)
		{
			auto sp = Sprite::create(SPRITE_PATH + "xiangxi_bugang.png");
			sp->setPosition(Vec2(Statpos, 0));
			tempNode->addChild(sp);
			sp->setVisible(true);
			Statpos += diffpos;
			Text *pannum = cocos2d::ui::Text::create("TextBMFont", "fonts/Marker Felt.ttf", 24);
			std::string str = StringUtils::format("%d", bugangpan);
			pannum->setString(str);
			pannum->setAnchorPoint(Vec2(0.5, 0.5));
			pannum->setColor(Color3B(119, 59, 13));
			pannum->setVisible(true);
			sp->addChild(pannum);
			pannum->setPosition(Vec2(65.81, 13));
			auto sppan = Sprite::create(SPRITE_PATH + "xiangxipan.png");
			sppan->setVisible(true);
			sppan->setPosition(Vec2(86.67, 12));
			sp->addChild(sppan);

		}

		//明杠盘
		if (minggangpai > 0)
		{
			auto sp = Sprite::create(SPRITE_PATH + "xiangxi_minggang.png");
			sp->setPosition(Vec2(Statpos, 0));
			tempNode->addChild(sp);
			sp->setVisible(true);
			Statpos += diffpos;
			Text *pannum = cocos2d::ui::Text::create("TextBMFont", "fonts/Marker Felt.ttf", 24);
			std::string str = StringUtils::format("%d", minggangpai);
			pannum->setString(str);
			pannum->setAnchorPoint(Vec2(0.5, 0.5));
			pannum->setColor(Color3B(119, 59, 13));
			pannum->setVisible(true);
			sp->addChild(pannum);
			pannum->setPosition(Vec2(65.81, 13));
			auto sppan = Sprite::create(SPRITE_PATH + "xiangxipan.png");
			sppan->setVisible(true);
			sppan->setPosition(Vec2(86.67, 12));
			sp->addChild(sppan);

		}
		//暗杠盘
		if (angangpan > 0)
		{
			
			auto sp = Sprite::create(SPRITE_PATH + "xiangxi_angang.png");
			sp->setPosition(Vec2(Statpos, 0));
			tempNode->addChild(sp);
			sp->setVisible(true);
			Statpos += diffpos;
			Text *pannum = cocos2d::ui::Text::create("TextBMFont", "fonts/Marker Felt.ttf", 24);
			std::string str = StringUtils::format("%d", angangpan);
			pannum->setString(str);
			pannum->setAnchorPoint(Vec2(0.5, 0.5));
			pannum->setColor(Color3B(119, 59, 13));
			pannum->setVisible(true);
			sp->addChild(pannum);
			pannum->setPosition(Vec2(65.81, 13));
			auto sppan = Sprite::create(SPRITE_PATH + "xiangxipan.png");
			sppan->setVisible(true);
			sppan->setPosition(Vec2(86.67, 12));
			sp->addChild(sppan);

		}

		//字碰牌
		if (zipaipengpan > 0)
		{
			auto sp = Sprite::create(SPRITE_PATH + "xiangxi_peng.png");
			sp->setPosition(Vec2(Statpos, 0));
			tempNode->addChild(sp);
			sp->setVisible(true);
			Statpos += diffpos;
			Text *pannum = cocos2d::ui::Text::create("TextBMFont", "fonts/Marker Felt.ttf", 24);
			std::string str = StringUtils::format("%d", zipaipengpan);
			pannum->setString(str);
			pannum->setAnchorPoint(Vec2(0.5, 0.5));
			pannum->setColor(Color3B(119, 59, 13));
			pannum->setVisible(true);
			sp->addChild(pannum);
			pannum->setPosition(Vec2(65.81, 13));
			auto sppan = Sprite::create(SPRITE_PATH + "xiangxipan.png");
			sppan->setVisible(true);
			sppan->setPosition(Vec2(86.67, 12));
			sp->addChild(sppan);

		}
		
	}



	void MJGameResult::StartAction(char cardId, std::function<void()> func)
	{
		cocos2d::Size winSize = Director::getInstance()->getWinSize();

		m_pZongChengJiNode->setVisible(false);
		m_pCurChengJiNode->setVisible(false);
		if (cardId<0)
		{
			m_pZongChengJiNode->setVisible(false);
			m_pCurChengJiNode->setVisible(true);
			return ;
		}
		MahjongCard* pCard = MahjongCard::create(DI_FRONT_SOUTH,SOUTH_DIR,cardId);
		pCard->setPosition(Vec2(winSize.width/2.0f, winSize.height/2.0f + 200));
		pCard->setVisible(true);
		this->addChild(pCard);
		auto pCallFunc1 = [func]()
		{
			if(func) func();
		};
		auto pCallFunc2 = [&]()
		{
			m_pZongChengJiNode->setVisible(false);
			m_pCurChengJiNode->setVisible(true);
			//this->quicklyShade(200);
		};
		cocos2d::Vec2 toPos = pCard->getPosition();
		pCard->runAction(Sequence::create(DelayTime::create(0.5f),EaseSineIn::create(MoveTo::create(0.3f, Vec2(toPos.x, toPos.y-150))),
			EaseSineOut::create(MoveTo::create(0.5f, Vec2(toPos.x, toPos.y-30))), 
			EaseSineIn::create(MoveTo::create(0.4f, Vec2(toPos.x, toPos.y-150))),
			EaseSineOut::create(MoveTo::create(0.3f, Vec2(toPos.x, toPos.y-60))),
			EaseSineIn::create(MoveTo::create(0.2f, Vec2(toPos.x, toPos.y-150))), 
			EaseSineOut::create(MoveTo::create(0.25f, Vec2(toPos.x, toPos.y-100))),
			EaseSineIn::create(MoveTo::create(0.2f, Vec2(toPos.x, toPos.y-150))), 
			CallFunc::create(pCallFunc1), DelayTime::create(3), CallFunc::create(pCallFunc2), nullptr));
	}

	void MJGameResult::HideAll()
	{
		m_pZongChengJiNode->setVisible(false);
		m_pCurChengJiNode->setVisible(false);
	}

	void MJGameResult::setShowCurCardArray(UserInfoStruct* pUserInfo, const char* pCardArray, char cardCount, 
		const char pActArray[][10], char actCount, char hupaiCardId, int dir)
	{
		int index = dir;
		CPlayerData* pData = m_PlayerDataVec[index];
		cocos2d::Vec2 toBeginPos = pData->_curCardNodePos;
		float toScale = 0.9;
		cocos2d::Size cardSize = Size::ZERO;
		float toWidthDist = 0;
		//绘制动作
		for (char i=0; i<actCount; i++)
		{
			char toCount = pActArray[i][2];
			for (char k=0; k<toCount; k++)
			{
				int cardId = pActArray[i][k+3];
				MahjongCard* pCard = MahjongCard::create(DI_FRONT_SOUTH,SOUTH_DIR,cardId);
				pCard->setScale(0.9);
	
				cardSize = Size(50,70);//pCard->getContentSize()*0.9;
	
				pCard->setPosition(toBeginPos);
				pData->_curCardNode->addChild(pCard);

				if (toWidthDist <= 0)toWidthDist = cardSize.width*toScale - 3;
				toBeginPos.x += toWidthDist;
			}
			toBeginPos.x += (toWidthDist)/3.0f;
		}

		if (cardCount>0)
		{
			int pToCardArray[TOTAL_CARD_MAX_COUNT] = {};
			for (char i=0; i<cardCount; i++) pToCardArray[i+1] = pCardArray[i];
			static_quicksort(pToCardArray, 0, cardCount);

			int toHuPaiCardId = hupaiCardId;
			bool toTemp = false;
			for (char i=0; i<cardCount; i++)
			{
				int cardId = pToCardArray[i+1];
				if (!toTemp && toHuPaiCardId==cardId)
				{
					toTemp =true;
				}
				else
				{
					MahjongCard* pCard = MahjongCard::create(DI_FRONT_SOUTH,SOUTH_DIR,cardId);
					cardSize = Size(53, 66.6);//pCard->getContentSize()*0.9;
					pCard->setScale(0.9);
					pCard->setPosition(toBeginPos);
					pData->_curCardNode->addChild(pCard);
					if (toWidthDist <= 0)toWidthDist = cardSize.width*toScale - 3;
					toBeginPos.x += toWidthDist;
				}
			}
			if (toTemp)
			{
				toBeginPos.x += (toWidthDist)/2.0f;
				MahjongCard* pCard = MahjongCard::create(DI_FRONT_SOUTH,SOUTH_DIR,toHuPaiCardId);
				cardSize = Size(53, 66.6);//pCard->getContentSize()*0.9;
				pCard->setScale(0.9);
				pCard->setPosition(toBeginPos);
				pCard->setHuVisible(true);
				pData->_curCardNode->addChild(pCard);
				toBeginPos.x += toWidthDist;
			}
		}
	}

	void MJGameResult::clearPlayerData()
	{
		for (auto iter = m_PlayerDataVec.begin(); iter != m_PlayerDataVec.end(); iter++)
		{
			delete *iter;
		}
		m_PlayerDataVec.clear();
	}
	void MJGameResult::showAllResultNode(bool isvisible)
	{
		//修改为默认先显示单局结算  确认后显示总结算
		if (isvisible)
		{
			_isGameEnd = true;
			GameManager::getInstance()->setIsAllRoundEnd(true);
		}
		//游戏结束时显示。进入游戏、分享、查看总结算
		m_btnshowtotal->setVisible(true);
		//m_btnshare->setPosition(Vec2(663,60.8));
		m_pZongChengJiNode->setVisible(!isvisible);
		m_pCurChengJiNode->setVisible(isvisible);
	}


}



