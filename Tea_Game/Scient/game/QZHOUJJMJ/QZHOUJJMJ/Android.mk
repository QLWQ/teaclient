LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNNet)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNUI)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNMarket)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLobby)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLogic)

LOCAL_MODULE := QZHOUJJMJ_static

LOCAL_MODULE_FILENAME := libQZHOUJJMJ

LOCAL_SRC_FILES := ../Classes/GameTable/QZHOUJJMJ_GameTableLogic.cpp \
                   ../Classes/GameTable/QZHOUJJMJ_GameManager.cpp \
                   ../Classes/GameTable/QZHOUJJMJ_ResourceLoader.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_CardPool.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_GameTableUI.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_PoolAction.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_HuCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_Card.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_SouthMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_EastMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_WestHandCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_SouthHandCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_EastHandCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_WestMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_MahjongCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_NorthMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_NorthHandCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_ConcealedKong.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_Factory.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_MahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_MeldedKong.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_TouchCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_TouchKong.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_Result.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_TurnTable.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_GMLayer.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_HandCard.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_ActionButtonList.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_SetLayer.cpp \
				   ../Classes/GameTable/QZHOUJJMJ_EatCard.cpp \

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMessage \
                    $(LOCAL_PATH)/../Classes/GameTable \
                    $(LOCAL_PATH)/../../ \
                    $(LOCAL_PATH)/../ 

LOCAL_EXPORT_LDLIBS := -llog \
                       -lz \
                       -landroid     
                   
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_net_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_market_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_lobby_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_logic_static

LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static


include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)
$(call import-module,editor-support/cocostudio)
$(call import-module,extensions)

$(call import-module,LibHNUI)
$(call import-module,LibHNNet)
$(call import-module,LibHNMarket)
$(call import-module,LibHNLogic)
$(call import-module,LibHNLobby)