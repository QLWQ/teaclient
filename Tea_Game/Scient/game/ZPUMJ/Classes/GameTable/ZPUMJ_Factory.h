#ifndef _ZPUMJ_FACTORYCARDPOOL_H_
#define _ZPUMJ_FACTORYCARDPOOL_H_

#include "ZPUMJ_CardPool.h"
#include "ZPUMJ_MahjongCardPool.h"
#include "ZPUMJ_SouthMahjongCardPool.h"
#include "ZPUMJ_WestMahjongCardPool.h"
#include "ZPUMJ_EastMahjongCardPool.h"
#include "ZPUMJ_NorthMahjongCardPool.h"

#include "ZPUMJ_TouchCard.h"
#include "ZPUMJ_MeldedKong.h"
#include "ZPUMJ_ConcealedKong.h"
#include "ZPUMJ_TouchKong.h"
#include "ZPUMJ_HuCard.h"
#include "ZPUMJ_EatCard.h"

namespace ZPUMJ
{

	class Factory
	{
	public:
		static CardPool* createEastPool(INT count);
		static CardPool* createWestPool(INT count);
		static CardPool* createSouthPool(INT count);
		static CardPool* createNorthPool(INT count);

		static PoolAction* createTouchCardAction();
		static PoolAction* createMeldedKongAction();
		static PoolAction* createConcealedKongAction();
		static PoolAction* createTouchKongAction();
		static PoolAction* createTouchEatAction();
		static PoolAction* createHuCardAction();
	};

}

#endif