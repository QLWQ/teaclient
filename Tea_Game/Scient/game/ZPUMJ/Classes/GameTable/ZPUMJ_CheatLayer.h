/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __ZPUMJ_GAMECHEAT_LAYER_H__
#define __ZPUMJ_GAMECHEAT_LAYER_H__

#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNNetExport.h"
#include "ZPUMJ_Protocol.h"
USING_NS_CC;
using namespace cocostudio;
using namespace ui;
namespace ZPUMJ
{
	class ZPUMJCheatLayer : public HNLayer
	{
	public:
		ZPUMJCheatLayer();
		virtual ~ZPUMJCheatLayer();

	public:
		virtual bool init() override;

		void show();

		//发送请求
		void sendRequest(char card, char seatNo);

		//请求剩余的牌
		void requestCardList();

		//消息回调
		bool onCheatMessageCallback(GameProtoS2CSetNextCard* pData);
	private:
		//确定按钮
		void onSureClick();
		//选牌事件
		void onSelectCardEvent(Ref* pRef);

		//刷新牌列表
		void refreshCardList(GameProtoS2CSetNextCard* msg);

		//处理作弊结果
		void dealCheatResult(GameProtoS2CSetNextCard* msg);	
	public:
		CREATE_FUNC(ZPUMJCheatLayer);

	private:
		ListView* m_pListView1 = nullptr;
		ListView* m_pListView2 = nullptr;
		ListView* m_pListView3 = nullptr;
		ListView* m_pListView4 = nullptr;

		char	m_bRequestType;  //请求类型  =0 为请求列表, =1 为请求下一张牌
		char   m_cSelectCardNo;
		std::vector<Button*> m_pButtonList;
	};
}
#endif // __ZPUMJ_GAMECHEAT_LAYER_H__
