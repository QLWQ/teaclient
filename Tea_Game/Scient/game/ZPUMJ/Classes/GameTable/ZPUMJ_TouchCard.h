#ifndef _ZPUMJ_TOUCHCARD_H_
#define _ZPUMJ_TOUCHCARD_H_

#include "ZPUMJ_poolaction.h"

namespace ZPUMJ
{
	class TouchCard :
		public PoolAction
	{
	public:
		TouchCard(void);
		~TouchCard(void);

		virtual void run() override;

		CREATE_FUNC(TouchCard);
	};

}

#endif