
#ifndef _ZPUMJ_WESTMAHJONGCARDPOOL_H_
#define _ZPUMJ_WESTMAHJONGCARDPOOL_H_

#include "ZPUMJ_MahjongCardPool.h"
#include "ZPUMJ_WestHandCard.h"

namespace ZPUMJ
{
	class WestMahjongCardPool :
		public MahjongCardPool
	{
	public:
		WestMahjongCardPool(void);
		~WestMahjongCardPool(void);

		CREATE_COUNT(WestMahjongCardPool);
		virtual bool init(INT count);

		virtual void setHandCardPos(INT catchCard) override;				 // ��������
		virtual int  getZhuaPaiZOrder();
		virtual cocos2d::Vec2 getCatchPos()override;
		virtual  void refreshAllShowCard()override;
		virtual void EmptyAllShowCard()override;
		virtual void RefreshShowCard()override;
		virtual Vec2 getOutToDeskPos()override;


		//���ˣ�����
		virtual void showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> handCards,INT huCard,bool visibleAllCard) override;
		virtual void showHuTips(std::vector<INT> huTip)override;
		virtual void changeCardToHead(std::vector<INT> cards)override;

	};

}

#endif