﻿#ifndef _ZPUMJ_TableLogic_H_
#define _ZPUMJ_TableLogic_H_

#include "ZPUMJ_MessageHead.h"
#include "ZPUMJ_CardPool.h"

namespace ZPUMJ
{
	class GameTableUICallBack;
	class GameTableUI;
	class GameTableLogic : public HNGameLogicBase
	{
	public:
		GameTableLogic(GameTableUICallBack* uiCallback, BYTE deskNo, bool bAutoCreate);
		virtual ~GameTableLogic();


	public:
		virtual void dealGameStartResp(INT bDeskNO) ;
		virtual void dealGameClean() override;
		//更新比赛场进度
		virtual void dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum) override;
		virtual void dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum) override;
		virtual void dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum) override;
		//////////////////////////////////////////////////////////////////////////////
		virtual void dealGameEndResp(INT bDeskNO) ;

		virtual void dealUserAgreeResp(MSG_GR_R_UserAgree* agree) override;

		virtual void dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;
		// 用户站起
		virtual void dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;
		// 游戏状态
		virtual void dealGameStationResp(void* object, INT objectSize) override;	
		//设置回放模式
		virtual void dealGamePlayBack(bool isback) override;
		// 房主消息
		virtual void dealDeskOwnertResp(void* object, INT objectSize);

		// 游戏消息（游戏的主体消息来自这里）
		virtual void dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;
		virtual void dealGameDeskNotFound(void* object, INT objectSize);

		virtual void dealUserCutMessageResp(INT userId, BYTE seatNo)override;
	
	public:				// 消息处理
		virtual void setGameStation(void* pBuffer,int nLen);												///<设置状态/// 断线重连消息
		virtual void onUserAgree(const MSG_GR_R_UserAgree& msg);			
		/// 起立消息    
		void sendStandUp();
	public:
		virtual void dealOnSendAllCardEx(char* pData, int datasize);
		virtual void dealShowCardFlowerEx(char* pData, int datasize);
		virtual void dealGameBegin(char* pData, int datasize);
		virtual void dealShowCardFlowerFinshEx(char* pData, int datasize);
		virtual void dealShowGoldCardEx(char* pData, int datasize);

		virtual void dealOnFollowTingTis(char* pData, int datasize);
		virtual void dealOnOutCardEx(char* pData, int datasize);
		virtual void dealOnZhuaCardEx(char* pData, int datasize);
		virtual void dealOnActionAppearEx(char* pData, int datasize);
		virtual void dealOnActionEatEx(char* pData, int datasize);
		virtual void dealOnGameFinish(char* pData, int datasize);
		virtual void dealOnPlayerHu(char* pData,int datasize);
		virtual void dealOnTingPaiEx(char* pData, int datasize);
		virtual void dealOnFlowZhuang(char*object,int objectSize);
		virtual void dealAuto(char*object, int objectSize);
		virtual void dealSelectChaHua(char*object, int objectSize);
	public:
		virtual void clearDeskUsers() {}
		virtual void dealUserInfoChangeResp(const UserInfoStruct* user) override;
		virtual void dealQueueUserSitMessage(bool success, const std::string& message)override;

		virtual void dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;
		virtual void dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk) override;
		//道具特效聊天
		virtual void dealUserActionMessage(DoNewProp* normalTalk) override;
		//红包活动
		virtual void dealGameActivityResp(MSG_GR_GameNum* pData) override;
		virtual void dealGameActivityReceive(MSG_GR_Set_JiangLi* pData) override;

		//比赛场游戏轮数
		//virtual void dealGameNumRound(MSG_GR_ConTestRoundNum* pData) override;

		//比赛场游戏局数
		//virtual void dealGameInnings(MSG_GR_ConTestRoundNumInOneRound* pData) override;

		//玩家金币不足提示
		virtual void dealUserNotEnoughMoney() override;
	public:			
		void waitAgree();		// 等候同意
		void clearDesk();
		void loadUsers();                                           // 加载用户
	public:
		sitDir getUserDir(const INT& deskStation);
		INT getUserStation(const sitDir& dir);
	private:
		std::vector<CardPool::CGroupCardData> finCardGroup(char	actionCard[ACTION_MAX_COUNT][10],INT actionCount,INT station);

	private:
		GameTableUI*		_callBack;
		UserInfoStruct		_userInfo;					// 玩家信息
		GamePromptLayer*	_GamePrompt = nullptr;		// 待操作界面
		bool				_hasSetGameStation;			// 初次进场，重连时用
		int				_waitTime = 15;
	public:
		static GameTableLogic* getInstance() { return _instance; }
		
	private:
		static GameTableLogic* _instance;
		bool _isHaveAction = false;
	};

	#define GTLogic() GameTableLogic::getInstance()

}


#endif // _HSMJ_TableLogic_H_