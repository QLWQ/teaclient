#ifndef _ZPUMJ_TOUCHKONG_H_
#define _ZPUMJ_TOUCHKONG_H_

#include "ZPUMJ_poolaction.h"

namespace ZPUMJ
{

	class TouchKong :
		public PoolAction
	{
	public:
		TouchKong(void);
		~TouchKong(void);

		virtual void run() override;

		CREATE_FUNC(TouchKong);
	};

}

#endif
