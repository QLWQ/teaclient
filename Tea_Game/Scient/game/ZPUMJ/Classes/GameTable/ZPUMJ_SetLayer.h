/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __ZPUMJ_GAMESET_LAYER_H__
#define __ZPUMJ_GAMESET_LAYER_H__

#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
USING_NS_CC;

using namespace cocostudio;
using namespace ui;

class ZPUMJSetLayer : public HNLayer
{
public:
	ZPUMJSetLayer();
	virtual ~ZPUMJSetLayer();

	typedef std::function<void()> ExitCallBack;
	ExitCallBack	onExitCallBack = nullptr;

	typedef std::function<void()> DisCallBack;
	DisCallBack		onDisCallBack = nullptr;

public:
    virtual bool init() override;

	void showSet(Node* parent, int zorder, int tag = -1);

	void close();

private:
	// 拖动条回调函数
	void sliderCallback(Ref* pSender, Slider::EventType type);

	void CheckBoxCallback(Ref* pSender);

	// 离开按钮
	void onExitClick(Ref* pRef);

	// 解散按钮
	void onDisClick(Ref* pRef);

private:
	//Vector<CheckBox*> _vec_CheckBox;			//复选框
	CheckBox* _Check_yinyue = nullptr;
	CheckBox* _Check_yinxiao = nullptr;
	CheckBox* _Check_jingyin = nullptr;
	Slider* _effectSlider = nullptr;
	Slider* _musicSlider = nullptr;

public:
	CREATE_FUNC(ZPUMJSetLayer);
};

#endif // __GoldenFlower_GAMESET_LAYER_H__
