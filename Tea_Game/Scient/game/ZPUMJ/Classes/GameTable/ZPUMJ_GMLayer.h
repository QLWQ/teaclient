#ifndef ZPUMJ_GMLayer_h__
#define ZPUMJ_GMLayer_h__
#include "HNNetExport.h"

namespace ZPUMJ
{
	class ZPUMJ_GMLayer : public HNLayer
	{
	public:
		ZPUMJ_GMLayer();
		~ZPUMJ_GMLayer();

		CREATE_FUNC(ZPUMJ_GMLayer);

		void setUIData(std::vector<UserInfoStruct> users,std::vector<std::vector<INT>> cards);
	private:
		virtual bool init() override;
		void clickCloseEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		
		bool LeftCardCallBack(HNSocketMessage* socketMessage);

	private:
		cocos2d::Node*	m_pMainNode;
	};
}



#endif // HSMJ_GMLayer_h__
