#ifndef ZPUMJ_ChatTip_h__
#define ZPUMJ_ChatTip_h__

#include <string>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::extension;
using namespace ui;


namespace ZPUMJ
{
	class ZPUMJ_ChatTip : public Sprite
	{
	public:
		ZPUMJ_ChatTip();
		~ZPUMJ_ChatTip();
		CREATE_FUNC(ZPUMJ_ChatTip);
		void setClickCallBack(std::function<void()> clickCallBack );
		void setInfo(std::string name,std::string talkMsg);
		void playNewMsgAction();
		void stopNewMsgAction();
	private:
		virtual bool init();
		void onBtnClick(Ref* pSender,Control::EventType event);

	private:
		Label*					_chatUserName;
		Label*					_chatLabel;
		Sprite*					_chatTips;
		std::function<void()>	_clickCallBack;
	};

	
}
#endif // HSMJ_ChatTip_h__