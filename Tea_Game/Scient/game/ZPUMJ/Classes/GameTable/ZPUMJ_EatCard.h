#ifndef _ZPUMJ_EATACTION_H_
#define _ZPUMJ_EATACTION_H_
#include "ZPUMJ_poolaction.h"

namespace ZPUMJ
{

	class EatCard :
		public PoolAction
	{
	public:
		EatCard(void);
		~EatCard(void);

		virtual void run() override;

		CREATE_FUNC(EatCard);

	};

}
#endif