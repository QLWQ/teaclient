#ifndef _ZPUMJ_MELDEDKONG_H_
#define _ZPUMJ_MELDEDKONG_H_

#include "ZPUMJ_poolaction.h"

namespace ZPUMJ
{

	class MeldedKong :
		public PoolAction
	{
	public:
		MeldedKong(void);
		~MeldedKong(void);

		virtual void run() override;

		CREATE_FUNC(MeldedKong);
	};

}

#endif