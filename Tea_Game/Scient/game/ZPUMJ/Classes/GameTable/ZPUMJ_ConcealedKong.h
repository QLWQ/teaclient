#ifndef _ZPUMJ_CONCEALEDKONG_H_
#define _ZPUMJ_CONCEALEDKONG_H_

#include "ZPUMJ_poolaction.h"

namespace ZPUMJ
{

	class ConcealedKong :
		public PoolAction
	{
	public:
		ConcealedKong(void);
		~ConcealedKong(void);

		virtual void run() override;

		CREATE_FUNC(ConcealedKong);

	};

}
#endif