#include "ZPUMJ_MahjongCard.h"
#include "ZPUMJ_GameManager.h"

namespace ZPUMJ
{

	MahjongCard::MahjongCard(void):
		_zhuanShi(nullptr)
	{
		_sum = -1;
		_number = 0;
		_sitNo = MID_DIR;
		_color = INVALID;
		_switch = false;
	}


	MahjongCard::~MahjongCard(void)
	{
	}

	void  MahjongCard::setWitch(bool swth)
	{
		_switch = swth;
	}

	MahjongCard* MahjongCard::create(mahjongCreateType type, sitDir sitNo, INT number)
	{
		MahjongCard* pCard = new MahjongCard();
		if (pCard && pCard->init(type,sitNo,number))
		{
			pCard->autorelease();
			return pCard;
		}

		CC_SAFE_DELETE(pCard);
		return nullptr;

	}

	MahjongCard* MahjongCard::create(mahjongCreateType type, sitDir sitNo)
	{
		MahjongCard* pCard = new MahjongCard();
		if (pCard && pCard->init(type,sitNo,0))
		{
			pCard->autorelease();
			return pCard;
		}

		CC_SAFE_DELETE(pCard);
		return nullptr;
	}


	bool MahjongCard::init(mahjongCreateType type, sitDir sitNo, INT number)
	{
		if (! Card::init(type, sitNo, number))
		{
			return false;
		}


		_sitNo = sitNo;
		_sum = number;
		_type = type;

		if (number == 0)
		{
			_number = 0;
			_color = INVALID;
		}
		else
		{
			_number = number % 10;
			_color = mahjongColor(number / 10);
		}

		std::string pic = getPicString(type,sitNo,number);
		//SpriteFrame *frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(pic);
		//if (frame != nullptr)
		//{
			
		//	return false;
		//}
		if (type == mahjongCreateType::DI_EAST_STAND 
			|| type == mahjongCreateType::DI_WEST_STAND
			|| type == mahjongCreateType::DI_NORTH_STAND 
			|| type == mahjongCreateType::DI_SOUTH_BACK 
			|| type == mahjongCreateType::DI_EAST_BACK
			|| type == mahjongCreateType::DI_NORTH_BACK
			|| type == mahjongCreateType::DI_WEST_BACK
			|| type == mahjongCreateType::DI_FRONT_ZHENG
			)
		{
			if (FileUtils::getInstance()->isFileExist(pic))
			{
				_uiSp = Sprite::create(pic);
				//_uiSp->setScale(0.9);
				this->setAnchorPoint(Vec2(0.5f, 0.5f));
				//this->setContentSize(_uiSp->getContentSize()*0.9);
				if (type == mahjongCreateType::DI_FRONT_ZHENG)
				{
					//_uiSp->setScale(0.7);
					_uiSp->setPosition(Vec2(0, 0));
				}
				else
				{
					_uiSp->setPosition(_uiSp->getContentSize() / 2);
				}
				this->addChild(_uiSp);
			}
			else
			{
				//HNLOG("%s===nil", pic);
			}
		}
		else
		{

			if (FileUtils::getInstance()->isFileExist(pic))
			{
				std::string bg = getCardBg(type, sitNo, number);
				_uiSp = Sprite::create(bg);
				//_uiSp->setScale(0.9);
				this->setAnchorPoint(Vec2(0.5f, 0.5f));
				//this->setContentSize(_uiSp->getContentSize()*0.9);			
				_uiSp->setPosition(_uiSp->getContentSize() / 2);
				this->addChild(_uiSp);
				auto sp = Sprite::create(pic);
				sp->setPosition(_uiSp->getContentSize() / 2);
				_uiSp->addChild(sp);
			}
			//else HNLOG("%s===nil", pic);
		}

		//assert(_uiSp != nullptr);
		if (_uiSp == nullptr)
		{
			//log("888888888888888888888");
			return false;
		}

		switch (type)
		{
			case mahjongCreateType::DI_SOUTH_STAND:
			{
				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::create("platform/Games/youjinbiaozhi1.png");
					goldCard->setPosition(Vec2(57.29, 72));
					goldCard->setVisible(true);
					//goldCard->setScale(2.0);
					_uiSp->addChild(goldCard);
				}
			}
			break;
			case mahjongCreateType::DI_FRONT_SOUTH:
				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::create("platform/Games/youjinbiaozhi2.png");
					goldCard->setPosition(Vec2(48, 66));
					goldCard->setVisible(true);
					goldCard->setScale(1.5);
					_uiSp->addChild(goldCard);
				}
				break;
			case mahjongCreateType::DI_OUT_CARD_SOUTH:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::create("platform/Games/youjinbiaozhi2.png");
					goldCard->setPosition(Vec2(29, 40));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
			break;
			case mahjongCreateType::DI_OUT_CARD_EAST:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::create("platform/Games/youjinbiaozhi4.png");
					goldCard->setPosition(Vec2(14, 32));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
			break;
			case mahjongCreateType::DI_OUT_CARD_NORTH:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::create("platform/Games/youjinbiaozhi5.png");
					goldCard->setPosition(Vec2(11, 25));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
			break;
			case mahjongCreateType::DI_OUT_CARD_WEST:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::create("platform/Games/youjintubiao3.png");
				goldCard->setPosition(Vec2(33.0, 23));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
		}

		if (_type == mahjongCreateType::DI_SOUTH_STAND)
		{
			Sprite* ting=Sprite::create(SPRITE_PATH+"img_ting.png");
			ting->setAnchorPoint(Vec2(0.0,1.0));
			ting->setPosition(Vec2(3,_uiSp->getContentSize().height - ting->getContentSize().height));
			ting->setName("ting");
			ting->setVisible(false);
			_uiSp->addChild(ting);

			Sprite* tingstate = Sprite::create("platform/Games/sp_ting.png");
			tingstate->setAnchorPoint(Vec2(0.5, 0.0));
			tingstate->setPosition(Vec2(_uiSp->getContentSize().width/2, _uiSp->getContentSize().height - ting->getContentSize().height));
			tingstate->setName("tingstate");
			tingstate->setVisible(false);
			_uiSp->addChild(tingstate);

			Sprite* kou=Sprite::create(SPRITE_PATH+"img_kou.png");
			kou->setAnchorPoint(Vec2(1.0,0.0));
			kou->setPosition(Vec2(_uiSp->getContentSize().width,0));
			kou->setName("kou");
			kou->setVisible(false);
			_uiSp->addChild(kou);

			Sprite* huan=Sprite::create(SPRITE_PATH+"img_huan.png");
			huan->setAnchorPoint(Vec2(0.0,1.0));
			huan->setPosition(Vec2(0,_uiSp->getContentSize().height-huan->getContentSize().height));
			huan->setName("huan");
			huan->setVisible(false);
			_uiSp->addChild(huan);
		}


		Sprite* hu=Sprite::create(SPRITE_PATH+"img_hu_tips.png");
		hu->setAnchorPoint(Vec2(0.0,1.0));
		hu->setPosition(Vec2(12,_uiSp->getContentSize().height - 3));
		hu->setName("hu");
		hu->setVisible(false);
		_uiSp->addChild(hu);
	

		_size = _uiSp->getContentSize();
		initZhuanShi();

		return true;
	}

	std::string MahjongCard::getPicString(mahjongCreateType type, sitDir sitNo, INT number)
	{
		std::string head = "";
		switch (type)
		{
		case ZPUMJ::mahjongCreateType::DI_SOUTH_STAND:
			head = "platform/Games/card/ZhengDa";
			break;
		case ZPUMJ::mahjongCreateType::DI_EAST_STAND:		//站立
			head = "platform/Games/right_hand";
			break;
		case ZPUMJ::mahjongCreateType::DI_WEST_STAND:
			head = "platform/Games/left_hand";
			break;
		case ZPUMJ::mahjongCreateType::DI_NORTH_STAND:
			head = "platform/Games/up_hand";
			break;
		case ZPUMJ::mahjongCreateType::DI_SOUTH_BACK:
			head = "platform/Games/me_tun_back";
			break;
		case ZPUMJ::mahjongCreateType::DI_EAST_BACK:
			head = "platform/Games/right_tun_back";
			break;
		case ZPUMJ::mahjongCreateType::DI_NORTH_BACK:
			head = "platform/Games/up_tun_back";
			break;
		case ZPUMJ::mahjongCreateType::DI_WEST_BACK:
			head = "platform/Games/left_tun_back";
			break;

		case ZPUMJ::mahjongCreateType::DI_FRONT_SOUTH:
			head = "platform/Games/card/DaoDa";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_EAST:
			head = "platform/Games/card/PgkE";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_NORTH:
			head = "platform/Games/card/ChuNorth";				
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_WEST:
			head = "platform/Games/card/PgkW";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_ZHENG:			//金 暂时用自己的牌
			head = "platform/Games/card/ZHENG";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_WEST:
			head = "platform/Games/card/ChuWest";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_EAST:
			head = "platform/Games/card/ChuEast";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_SOUTH:
			head = "platform/Games/card/ChuSouth";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_NORTH:
			head = "platform/Games/card/ChuNorth";
			break;
		default:
			break;
		}


		switch (_color)
		{
		case ZPUMJ::mahjongColor::WAN:
			head += "Wan";
			head += StringUtils::format("%d", _number);
			break;
		case ZPUMJ::mahjongColor::TIAO:
			head += "Tiao";
			head += StringUtils::format("%d", _number);
			break;
		case ZPUMJ::mahjongColor::TONG:
			head += "Tong";
			head += StringUtils::format("%d", _number);
			break;
		case ZPUMJ::mahjongColor::FANG:
			{
				if (number == CMjEnum::MJ_TYPE_FD)
				{
					head += "Dong";
				}
				else if (number == CMjEnum::MJ_TYPE_FN)
				{
					head += "Nan";
				}
				else if (number == CMjEnum::MJ_TYPE_FX)
				{
					head += "Xi";
				}
				else if (number == CMjEnum::MJ_TYPE_FB)
				{
					head += "Bei";
				}
				else if (number == CMjEnum::MJ_TYPE_ZHONG)
				{
					head += "Zhong";
				}
				else if(number==CMjEnum::MJ_TYPE_FA)
				{
					head += "Fa";

				}
				else if(number==CMjEnum::MJ_TYPE_BAI)
				{
					head += "Bai";

				}
			}
		case ZPUMJ::mahjongColor::HUA:
			{
				if (number == CMjEnum::MJ_TYPE_FCHUN)
				{
					head += "Chun";
				}
				else if (number == CMjEnum::MJ_TYPE_FXIA)
				{
					head += "Xia";
				}
				else if (number == CMjEnum::MJ_TYPE_FQIU)
				{
					head += "Qiu";
				}
				else if (number == CMjEnum::MJ_TYPE_FDONG)
				{
					head += "HDong";
				}
				else if (number == CMjEnum::MJ_TYPE_FMEI)
				{
					head += "Mei";
				}
				else if (number == CMjEnum::MJ_TYPE_FLAN)
				{
					head += "Lan";
				}
				else if (number == CMjEnum::MJ_TYPE_FZHU)
				{
					head += "Zhu";
				}
				else if (number == CMjEnum::MJ_TYPE_FJU)
				{
					head += "Ju";
				}
			}
			break;
		default:
			break;
		}

		//if (_color == INVALID && type == ZPUMJ::mahjongCreateType::DI_FRONT_EAST)
		//{
		//	head += "Bai";
		//}

		return head+".png";
	}

	std::string MahjongCard::getCardBg(mahjongCreateType type, sitDir sitNo, INT number)
	{
		std::string head = "";
		switch (type)
		{
		case ZPUMJ::mahjongCreateType::DI_SOUTH_STAND:
			head = "platform/Games/me_hand";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_SOUTH:
			head = "platform/Games/me_act";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_EAST:
			head = "platform/Games/right_act";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_NORTH:			//旋转
			head = "platform/Games/up_out";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_WEST:
			head = "platform/Games/left_act";
			break;
		case ZPUMJ::mahjongCreateType::DI_FRONT_ZHENG:			//金 暂时用自己的牌
			head = "platform/Games/me_hand";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_WEST:
			head = "platform/Games/left_out";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_EAST:			//旋转
			head = "platform/Games/right_out";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_SOUTH:
			head = "platform/Games/me_out";
			break;
		case ZPUMJ::mahjongCreateType::DI_OUT_CARD_NORTH:
			head = "platform/Games/up_out";
			break;
		default:
			break;
		}
		return head + ".png";
	}


	void MahjongCard::setTingVisible(bool visible)
	{
	 	Node* node = this->getSpChildByName("ting");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::setTingStateVisible(bool visible)
	{
		Node* node = this->getSpChildByName("tingstate");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::setKouVisible(bool visible)
	{
		Node* node = this->getSpChildByName("kou");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::setHuanVisible(bool visible)
	{
		Node* node = this->getSpChildByName("huan");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::setHuVisible(bool visible)
	{
		Node* node = this->getSpChildByName("hu");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::initZhuanShi()
	{
		Vec2 pos = Vec2(0,0);
		switch (_sitNo)
		{
		case ZPUMJ::MID_DIR:
			break;
		case ZPUMJ::SOUTH_DIR:
			pos = Vec2(_uiSp->getContentSize().width/2 - 3,_uiSp->getContentSize().height);
			break;
		case ZPUMJ::EAST_DIR:
			pos = Vec2(15.0f,_uiSp->getContentSize().height/2+25.0f);
			break;
		case ZPUMJ::WEST_DIR:
			pos = Vec2(_uiSp->getContentSize().width/2+10.0f,_uiSp->getContentSize().height/2+25.0f);
			break;
		case ZPUMJ::NORTH_DIR:
			pos = Vec2(_uiSp->getContentSize().width/2,50);
			break;
		default:
			break;
		}
		//添加"钻石"精灵
		_zhuanShi=Sprite::create(COCOS_PATH+"Sprite/zuanshi1.png");
		_zhuanShi->setScale(1.5f);
		_zhuanShi->setAnchorPoint(Vec2(0.5f,0.5f));
		_zhuanShi->setPosition(pos);
		_zhuanShi->setVisible(false);
		_uiSp->addChild(_zhuanShi,100);

	}

	void MahjongCard::playZhuanShiAction()
	{
		if (_zhuanShi == nullptr)
		{
			return;
		}

		_zhuanShi->setVisible(true);
		_zhuanShi->stopAllActions();
		Sequence* beat=Sequence::create(MoveBy::create(0.4f,Vec2(0,15)),MoveBy::create(0.6f,Vec2(0,-15)),nullptr);
		RepeatForever* forever = RepeatForever::create(beat);
		_zhuanShi->runAction(forever);

	}

	void MahjongCard::stopZhuanShiAction()
	{
		if (_zhuanShi == nullptr)
		{
			return;
		}

		_zhuanShi->stopAllActions();
		_zhuanShi->setVisible(false);
		
	}

}