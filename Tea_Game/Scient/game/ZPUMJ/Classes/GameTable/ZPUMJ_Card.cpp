#include "ZPUMJ_Card.h"
#include "ZPUMJ_GameManager.h"

namespace ZPUMJ
{

	Card::Card(void)
	{
		_uiSp = nullptr;
		_type = mahjongCreateType::DI_WEST_BACK;   
		_number = -999;
		_sitNo = sitDir::MID_DIR;
		_color = mahjongColor::TIAO;
		_canTouch = false;
		_isSelect = false;
		_isChange = false;
		_canFollow = false;
	}


	Card::~Card(void)
	{
	}


	bool Card::init(mahjongCreateType type, sitDir sitNo, INT number)
	{
		if (!Node::init())
		{
			return false;
		}

		return true;
	}

	Size Card::getCardSize()
	{
		return _size;
	}

	void Card::setCardPos(const Vec2& pos)
	{
		this->setPosition(pos);
	}

	void Card::setCardZorder(const int zorder)
	{
		_uiSp->setLocalZOrder(zorder);
		_cardZorder = zorder;
	}

	void Card::setCardOwner(const sitDir& dir)
	{
		_sitNo = dir;
	}


	INT Card::getCardNumber()
	{
		return _number;
	}
		
	mahjongColor Card::getCardColor()
	{
		return _color;
	}

	void Card::setCardColor(Color3B color)
	{
		_uiSp->setColor(color);
	}

	void Card::setCardNum(const INT& number)
	{
		_number=number;
	}

	void Card::setCardTouchEvent()
	{
		auto listener =EventListenerTouchOneByOne::create();
		listener->setSwallowTouches(true);
		listener->onTouchBegan = CC_CALLBACK_2(Card::touchBegin,this);
		listener->onTouchMoved =  CC_CALLBACK_2(Card::touchMove,this);
		listener->onTouchEnded = CC_CALLBACK_2(Card::touchEnd,this);

		listener->onTouchCancelled = [&](Touch* t, Event* e){};
		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, _uiSp);
	}

	void Card::setCardEnableTouch(bool touch)
	{
		_canTouch = touch;
	}

	INT Card::getCardZorder()
	{
		return _cardZorder;
	}

	cocos2d::Node* Card::getSpChildByName(const std::string& name)
	{
		if (!_uiSp) return nullptr;
		return _uiSp->getChildByName(name);
	}

	void Card::touchEnd(Touch*touch, Event*tevent)
	{
		_canFollow = false;
		if (GameManager::getInstance()->getCurrOperDir()!=SOUTH_DIR)
		{
			return;
		}
		cocos2d::Vec2 toPos = touch->getLocation();	
		bool isGamePlaying=true;
		bool isCatchCardState= GameManager::getInstance()->getIsCatchCard();
		CardPool* pool =  GameManager::getInstance()->getSelfCardPool();
		SouthMahjongCardPool* selfPool = dynamic_cast<SouthMahjongCardPool*>(pool);
		cocos2d::Vec2 touchPos = touch->getLocation();
		if (_isSelect&&_canTouch&&_sitNo == SOUTH_DIR&&isGamePlaying&&!isCatchCardState&&selfPool)
		{
			CardPool* pool =  GameManager::getInstance()->getSelfCardPool();
			SouthMahjongCardPool* selfPool = dynamic_cast<SouthMahjongCardPool*>(pool);

			if (selfPool == nullptr)
			{
				return ;
			}
			static float  globalOutPosY = Director::getInstance()->getWinSize().height*0.2; //出牌上限
			if ( touchPos.y>globalOutPosY)
			{
				selfPool->TouchCard(this);
				//出牌完成整理手牌
				//selfPool->refreshAllShowCard();
			}
			else
			{
				//点击弹起处理(自己出牌的时候点一下弹起)
				if (this->getSelect())
				{
					this->setPosition(Vec2(_startPos.x,40));
				}
				else
				{
					this->setPosition(_startPos);
				}
				/*cocos2d::Vec2 cardPos = this->getParent()->getPosition();
				this->setPosition(_startPos);*/
				//selfPool->TouchCard(this);
			}

		}
	}

	void Card::touchMove(Touch*touch, Event*tevent)
	{
		if (GameManager::getInstance()->getCurrOperDir()!=SOUTH_DIR)
		{
			return;
		}

		cocos2d::Vec2 toPos = touch->getLocation();	
		bool isGamePlaying=true;
		bool isCatchCardState= GameManager::getInstance()->getIsCatchCard();
		CardPool* pool =  GameManager::getInstance()->getSelfCardPool();
		SouthMahjongCardPool* selfPool = dynamic_cast<SouthMahjongCardPool*>(pool);
		if (_isSelect&&_canTouch&&_sitNo == SOUTH_DIR&&isGamePlaying&&!isCatchCardState&&selfPool)
		{
			cocos2d::Vec2 cardPos = this->getParent()->convertToNodeSpace(toPos);
			Size size = _size; //Size(72, 80);
			cardPos = Vec2(cardPos.x - size.width / 2, cardPos.y - size.height / 2);
			if (_canFollow ==  false && cardPos.y > _size.height / 2)
			{
				_canFollow = true;
			}
			if (_canFollow)
				this->setPosition(cardPos);
		}
	}

	bool Card::touchBegin(Touch*touch, Event*tevent)
	{
		cocos2d::Vec2 toPos = _uiSp->convertTouchToNodeSpace(touch);
		cocos2d::Rect box;
		box.origin = Vec2::ZERO;
		box.size = _size;
		if (box.containsPoint(toPos))   // 是否牌范围内
		{
			_startPos = this->getPosition();
			_porePos = _uiSp->convertTouchToNodeSpace(touch);
			if (!_canTouch)            // 是否能触摸
			{
				return false;
			}

			if (_sitNo != SOUTH_DIR)
			{
				return false;
			}
			if (GameManager::getInstance()->getIsCatchCard())
			{
				return false;
			}
			if (GameManager::getInstance()->getIsHasAction() && (!GameManager::getInstance()->getIsHasChiAction()))
			{
				return false;
			}
			if (GameManager::getInstance()->getIsHasChiAction())
			{
				GameManager::getInstance()->setSeletChiCard(this);
				return true;
			}



			CardPool* pool =  GameManager::getInstance()->getSelfCardPool();
			SouthMahjongCardPool* selfPool = dynamic_cast<SouthMahjongCardPool*>(pool);

			if (selfPool == nullptr)
			{
				return false;
			}
			selfPool->TouchCard(this);
			return true;
		}
		return false;
	}

	void Card::setIsChange(bool isChange)
	{
		_isChange = isChange;
	}

	bool Card::getIsChange()
	{
		return _isChange;
	}

	void Card::setChangeColor()
	{
		this->setCardColor(Color3B(214, 112, 112));
	}

}