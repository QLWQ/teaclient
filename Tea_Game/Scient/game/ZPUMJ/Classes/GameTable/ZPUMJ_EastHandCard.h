#ifndef ZPUMJ_EastHandCard_h__
#define ZPUMJ_EastHandCard_h__

#include "cocos2d.h"
#include "ZPUMJ_CardPool.h"
#include "ZPUMJ_HandCard.h"
using namespace cocos2d;

/*
�������ϰ�
*/

namespace ZPUMJ
{
	class ZPUMJ_EastHandCard : public HandCard
	{
	public:
		ZPUMJ_EastHandCard();
		~ZPUMJ_EastHandCard();

		CREATE_FUNC(ZPUMJ_EastHandCard);

	
		virtual Vec2 refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>normalCards,INT catchCard) override;
		virtual Vec2 refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>tingCards) override;
		virtual void setSingleGroupSize(Size groupSize) override;
		virtual void setNormalCardSize(Size normalSize)override;
		virtual void setTingCardSize(Size tingSize)override;
	 	virtual Vec2 getCatchWorldPos() override;
		virtual Vec2 refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,INT huCardId,bool visibleAllCard)override;
		virtual void putChangeCardToHead(std::vector<INT> cards)override;
		virtual void moveChangeCard(sitDir dir)override;

	private:
		virtual bool init() override;
		void refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards);
		Vec2 getNextPosition(Vec2 prePoint, float deltLen);
		void resetData();

	private:
		int		_beginZorder;
		Vec2	_prePoint;
	};
}



#endif // HSMJ_EastHandCard_h__
