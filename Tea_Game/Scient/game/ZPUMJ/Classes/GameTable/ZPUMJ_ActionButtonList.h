#ifndef ZPUMJ_ActionButtonList_h__
#define ZPUMJ_ActionButtonList_h__

#include "cocos2d.h"
#include "ZPUMJ_MessageHead.h"
using namespace cocos2d;

namespace  ZPUMJ
{
	class ZPUMJ_ActionButtonList :public Node
	{
	public:
		ZPUMJ_ActionButtonList();
		~ZPUMJ_ActionButtonList();

		CREATE_FUNC(ZPUMJ_ActionButtonList);

	private:
		enum ACITONTAG
		{
			NOACTION =-1,
			GANG     =0,
			PENG     =1,
			CHI		 =2,
			HU       =3,
			TIANHU	=4,
			YOUJING =5,
			SHUANGYOU =6,
			SANYOU = 7,
			SANJINGDAO =8,
			QIANGJING =9,
			ZIMO =10,
			BAHUAYOU =11,
			GUO =12,
			TING =13
		};

	private:
		virtual bool init();

	private:
		void clickBtEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
	public:
		void showActionBotton(bool hasHu, bool canchi,bool hasPeng, bool hasGang, bool canTing, bool canQiang, bool canDao, bool canyou, bool cantianhu, bool canzimo, char num);
		void showActionByDir(const sitDir& dir, bool hasHu, bool canchi, bool hasPeng, bool hasGang, bool canTing, bool canQiang, bool canDao, bool canyou, bool cantianhu, bool canzimo, char num);
	private:
		void reSetButtonPos();
		void reSetSpPos(const sitDir& dir);
	public:


	private:
		std::vector<Button*> _buttonVec;
		std::vector<Sprite*> _spVec;
		Node*				 _loader;
		Vec2				 _PosOrigin;
	};
}



#endif // ZZMJ_ActionButtonList_h__
