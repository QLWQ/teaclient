
#include "ZPUMJ_ChatTip.h"


namespace ZPUMJ
{
	ZPUMJ_ChatTip::ZPUMJ_ChatTip() :
		_chatUserName(nullptr),
		_chatLabel(nullptr),
		_chatTips(nullptr),
		_clickCallBack(nullptr)
	{

	}

	ZPUMJ_ChatTip::~ZPUMJ_ChatTip()
	{

	}


	void ZPUMJ_ChatTip::setClickCallBack(std::function<void()> clickCallBack)
	{
		_clickCallBack =clickCallBack;
	}


	

	bool ZPUMJ_ChatTip::init()
	{
		if (!Sprite::init())
		{
			return false;
		}

		Sprite* bg = Sprite::create("HNPlatform/GameChat/ChatRes/btn_liaotian1.png");
		auto chatSize = bg->getContentSize();
		this->setContentSize(chatSize);


		auto chatPic1 = ui::Scale9Sprite::create("HNPlatform/GameChat/ChatRes/btn_liaotian1.png");
		auto chatPic2 = ui::Scale9Sprite::create("HNPlatform/GameChat/ChatRes/btn_liaotian1.png");
		ControlButton* bgBtn = ControlButton::create(chatPic1);
		bgBtn->setBackgroundSpriteForState(chatPic2, Control::State::HIGH_LIGHTED);
		bgBtn->setContentSize(chatSize);
		bgBtn->setPreferredSize(chatSize);
		bgBtn->setZoomOnTouchDown(false);
		bgBtn->setAnchorPoint(Vec2(0.0f, 0.5f));
		bgBtn->setPosition(0.0f + 17,chatSize.height/2 + 150);
		
		bgBtn->addTargetWithActionForControlEvents(this, cccontrol_selector(ZPUMJ_ChatTip::onBtnClick), Control::EventType::TOUCH_UP_INSIDE);
		this->addChild(bgBtn);

		

		_chatTips = Sprite::create("HNPlatform/GameChat/ChatRes/chat_tips.png");
		_chatTips->setPosition(20, chatSize.height/2);
		this->addChild(_chatTips);
		_chatTips->setVisible(false);


		return true;
	}


	void ZPUMJ_ChatTip::onBtnClick(Ref* pSender, Control::EventType event)
	{
		if (event == Control::EventType::TOUCH_UP_INSIDE)
		{
			if (_clickCallBack)
			{
				_clickCallBack();
			}
		}
	}


	void ZPUMJ_ChatTip::setInfo(std::string name, std::string talkMsg)
	{
		if (_chatUserName != nullptr)
		{
			_chatUserName->removeFromParent();
			_chatUserName = nullptr;
		}

		if (_chatLabel != nullptr)
		{
			_chatLabel->removeFromParent();
			_chatLabel = nullptr;
		}

		_chatUserName = Label::createWithSystemFont(name, "", 20);
		_chatUserName->setAnchorPoint(Vec2(0, 0.5));
		_chatUserName->setColor(Color3B(222,138,57));
		_chatUserName->setPosition(30, this->getContentSize().height/2);
		this->addChild(_chatUserName);

		float width = this->getContentSize().width-_chatUserName->getContentSize().width-30;
		float posX = _chatUserName->getContentSize().width+30;

		_chatLabel = Label::createWithSystemFont(talkMsg, "", 18, Size(width, this->getContentSize().height/2));
		_chatLabel->setAnchorPoint(Vec2(0, 0.5));
		_chatLabel->setPosition(posX, this->getContentSize().height/2-5);
		this->addChild(_chatLabel);



	}


	void ZPUMJ_ChatTip::playNewMsgAction()
	{
		stopNewMsgAction();
		_chatTips->setVisible(true);
		auto a1 = FadeIn::create(0.5f);
		auto a2 = FadeOut::create(0.5f);
		_chatTips->runAction(Repeat::create(Sequence::create(a1, a2, nullptr), 15));

	}


	void ZPUMJ_ChatTip::stopNewMsgAction()
	{
		_chatTips->stopAllActions();
		_chatTips->setVisible(false);
	}
}


