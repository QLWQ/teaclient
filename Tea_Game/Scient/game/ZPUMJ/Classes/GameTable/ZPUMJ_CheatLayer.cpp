/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "ZPUMJ_CheatLayer.h"
#include "ZPUMJ_MessageHead.h"
#include "ZPUMJ_MahjongCard.h"
#include "ZPUMJ_GameManager.h"

namespace ZPUMJ
{

	static const char* CHEAT_CSB = "platform/createRoomUI/cheatUi/CheatNode.csb";

	static const int CHEAT_LAYER_ZORDER = 100000000 - 1;		// 子节点弹出框层级

	static const int CHEAT_LAYER_TAG = 100000000 - 1;		// 子节点弹出框层级


	ZPUMJCheatLayer::ZPUMJCheatLayer()
	{
		m_pListView1 = nullptr;
		m_pListView2 = nullptr;
		m_pListView3 = nullptr;
		m_pListView4 = nullptr;
		m_bRequestType = 0;
		m_cSelectCardNo = 0;
	}

	ZPUMJCheatLayer::~ZPUMJCheatLayer()
	{

	}

	void ZPUMJCheatLayer::show()
	{
		Node* root = Director::getInstance()->getRunningScene();
		CCAssert(nullptr != root, "root is null");
		if (!getParent())
		{
			setName("ZPUMJCheatLayer");
			open(ACTION_TYPE_LAYER::SCALE, root, Vec2::ZERO, CHEAT_LAYER_ZORDER, CHEAT_LAYER_TAG);
		}

		requestCardList();
	}

	bool ZPUMJCheatLayer::init()
	{
		if (!HNLayer::init()) {
			return false;
		}

		auto node = CSLoader::createNode(CHEAT_CSB);
		CCAssert(node != nullptr, "null");
		node->setPosition(Vec2::ZERO);
		addChild(node);

		auto paneL_mask = dynamic_cast<Layout*>(node->getChildByName("panel_mask"));

		auto img_bg = dynamic_cast<ImageView*>(paneL_mask->getChildByName("Image_bg"));
		img_bg->setScale(1280 / _winSize.width, 720 / _winSize.height);

		// 关闭
		auto btn_close = dynamic_cast<Button*>(img_bg->getChildByName("button_close"));
		btn_close->addClickEventListener([=](Ref*) {
			close();
		});
		paneL_mask->addClickEventListener([=](Ref*) {
			close();
		});

		// 确定
		auto btn_sure = dynamic_cast<Button*>(paneL_mask->getChildByName("button_sure"));
		btn_sure->addClickEventListener([=](Ref*) {
			onSureClick();
		});

		//获取麻将图标列表
		m_pListView1 = dynamic_cast<ListView*>(img_bg->getChildByName("ListView_1"));
		m_pListView2 = dynamic_cast<ListView*>(img_bg->getChildByName("ListView_2"));
		m_pListView3 = dynamic_cast<ListView*>(img_bg->getChildByName("ListView_3"));
		m_pListView4 = dynamic_cast<ListView*>(img_bg->getChildByName("ListView_4"));

		return true;
	}

	// 切换账号
	void ZPUMJCheatLayer::onSureClick()
	{
		if (m_cSelectCardNo <= 0)
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("请先选择一张牌!"));
			return;
		}
		char cardId = m_cSelectCardNo;
		char seatNo = RoomLogic()->loginResult.pUserInfoStruct.bDeskStation;
		sendRequest(cardId, seatNo);
	}

	void ZPUMJCheatLayer::requestCardList()
	{
		//请求剩余的牌
		sendRequest(0, RoomLogic()->loginResult.pUserInfoStruct.bDeskStation);
	}

	//发送请求
	void ZPUMJCheatLayer::sendRequest(char card, char seatNo)
	{
		if (card <= 0)
			m_bRequestType = 0;
		else
			m_bRequestType = 1;
		GameProtoC2SSetNextCard pData;
		pData.card = card;
		pData.seatNo = seatNo;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_SetNextCard, &pData, sizeof(pData));
	}

	bool ZPUMJCheatLayer::onCheatMessageCallback(GameProtoS2CSetNextCard* pData)
	{
		if (m_bRequestType == 0)
		{
			refreshCardList(pData);
		}
		else if (m_bRequestType == 1)
		{
			dealCheatResult(pData);
		}
		return true;
	}

	void ZPUMJCheatLayer::refreshCardList(GameProtoS2CSetNextCard* msg)
	{
		m_pListView1->removeAllItems();
		m_pListView2->removeAllItems();
		m_pListView3->removeAllItems();
		m_pListView4->removeAllItems();
		m_pButtonList.clear();
		for (int i = 0; i < sizeof(msg->leftCardArray) / sizeof(char); i++)
		{
			char count = msg->leftCardArray[i];  //牌剩余的数量
			if (count <= 0)
				continue;
			char number = i % 10;		//牌面值
			mahjongColor color = mahjongColor(i / 10); //牌花色类型
			MahjongCard* pCard = MahjongCard::create(DI_FRONT_SOUTH, SOUTH_DIR, i);  //以自己的方向
			pCard->setCardEnableTouch(false); //设置为不可点击状态
			pCard->setTag(1);
			auto btn =  Button::create();
			btn->setTag(i);   //将牌值设置为tag
			btn->addChild(pCard);
			btn->addClickEventListener(CC_CALLBACK_1(ZPUMJCheatLayer::onSelectCardEvent, this));
			btn->ignoreContentAdaptWithSize(false);
			btn->setContentSize(pCard->getCardSize());

			std::string szCount = StringUtils::format("%d", count);
			Text* textCount = Text::create(szCount.c_str(), "platform/common/RTWSYueRoudGoG0v1-Regular.ttf", 28);
			textCount->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
			textCount->setTag(2);
			textCount->setPosition(Vec2(30,30));
			textCount->setColor(Color3B(255, 255, 255));
			btn->addChild(textCount);
			switch (color)
			{
			case mahjongColor::WAN:
				m_pListView1->pushBackCustomItem(btn);
				break;
			case mahjongColor::TIAO:
				m_pListView2->pushBackCustomItem(btn);
				break;
			case mahjongColor::TONG:
				m_pListView3->pushBackCustomItem(btn);
				break;
			case mahjongColor::FANG:
			case mahjongColor::HUA:
				m_pListView4->pushBackCustomItem(btn);
				break;
			}
			m_pButtonList.push_back(btn);
		}
	}

	void ZPUMJCheatLayer::dealCheatResult(GameProtoS2CSetNextCard* msg)
	{
		if (msg->card <= 0)
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("请求失败,可能没有该牌了"));
			prompt->setCallBack([=]() {
				close();
			});
		}
		else
		{
			//更新麻将界面显示
			GameManager::getInstance()->ShowNextCheatCard(msg->card);
			close();
		}
	}

	void ZPUMJCheatLayer::onSelectCardEvent(Ref* pRef)
	{
		auto  btn = dynamic_cast<Button*>(pRef);
		char tag = btn->getTag();
		if (tag == m_cSelectCardNo)
		{
			m_cSelectCardNo = 0;
			auto pCard = dynamic_cast<MahjongCard*>(btn->getChildByTag(1));
			if (!pCard) return;
			pCard->setCardColor(Color3B(255, 255, 255));
		}
		else
		{
			m_cSelectCardNo = tag;
			for (int i = 0; i < m_pButtonList.size(); i++)
			{
				auto pBtn = m_pButtonList.at(i);
				auto pCard = dynamic_cast<MahjongCard*>(pBtn->getChildByTag(1));
				if (!pCard) continue;

				if (m_cSelectCardNo == pBtn->getTag())
				{
					pCard->setChangeColor();
				}
				else
				{
					pCard->setCardColor(Color3B(255, 255, 255));
				}
			}
		}
	}
}
