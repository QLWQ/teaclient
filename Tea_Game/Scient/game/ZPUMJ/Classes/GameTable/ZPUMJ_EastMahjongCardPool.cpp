#include "ZPUMJ_EastMahjongCardPool.h"
#include "ZPUMJ_GameManager.h"

namespace ZPUMJ
{

	EastMahjongCardPool::EastMahjongCardPool(void)
	{
	}


	EastMahjongCardPool::~EastMahjongCardPool(void)
	{
	}

	bool EastMahjongCardPool::init(INT count)
	{
		if (! MahjongCardPool::init())
		{
			return false;
		}
		
		_dir = sitDir::EAST_DIR;
		m_iMaxZOrder = 500;
		
		_handCard = ZPUMJ_EastHandCard::create();
		//_handCard->setScale(0.9f);
		this->addChild(_handCard);

		return true;
	}

	void EastMahjongCardPool::setHandCardPos(INT catchCard)
	{
		refreshAllShowCard();
	}

	int EastMahjongCardPool::getZhuaPaiZOrder()
	{
		return m_iMaxZOrder - 200;
	}

	void EastMahjongCardPool::refreshAllShowCard()
	{
		std::vector<CGroupCardData> temp;
		for (auto v:m_GroupCardDataVec)
		{
			temp.push_back(*v);
		}
	
		if (_isTingState)
		{
			_handCard->refreshHandCardTing(temp,_handCardList);
		}
		else
		{
			if (_isCatchCard)
			{
				_handCard->refreshHandCard(temp, _handCardList, _handCardList.at(_handCardList.size() - 1));
			}
			else
			{
				_handCard->refreshHandCard(temp, _handCardList, 0);
			}
		}

	}

	//清空切后台前发的牌
	void EastMahjongCardPool::EmptyAllShowCard()
	{
		_handCardList.clear();
	}

	//切回游戏刷新手牌
	void EastMahjongCardPool::RefreshShowCard()
	{

	}

	cocos2d::Vec2 EastMahjongCardPool::getCatchPos()
	{
		return _handCard->getCatchWorldPos();
	}

	cocos2d::Vec2 EastMahjongCardPool::getOutToDeskPos()
	{
		return getCatchPos();
	}


	void EastMahjongCardPool::showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> handCards,INT huCard,bool visibleAllCard)
	{
		_handCardList.clear();
		for (auto v:handCards)
		{
			_handCardList.push_back(v);
		}

		sortCard();
		moveOneCardToLast(huCard);
		_handCard->refreshHandCardHu(group,_handCardList,huCard,visibleAllCard);

		if (_huTips)
		{
			_huTips->removeFromParent();
			_huTips = nullptr;
		}
	}

	void EastMahjongCardPool::showHuTips(std::vector<INT> huTip)
	{
		if (_huTips)
		{
			_huTips->removeFromParent();
		}

		if (huTip.empty())
		{
			return;
		}

		_huTips = Node::create();
		_huTips->setPosition(Vec2(1060-huTip.size()*60.0f,240));
		this->addChild(_huTips);

		Size size = Size((huTip.size())*50+10,80);
		Sprite* hu = Sprite::create(SPRITE_PATH+"img_hux.png");
		hu->setPosition(Vec2(20,size.height/2));
		_huTips->addChild(hu);


		ui::Scale9Sprite* bg = ui::Scale9Sprite::create(SPRITE_PATH+"bg_hux.png");
		bg->setAnchorPoint(Vec2(0.0f,0.5f));
		bg->setContentSize(size);
		bg->setPosition(60,size.height/2);
		_huTips->addChild(bg);


		int index = 0;
		for (auto v:huTip)
		{
			MahjongCard* card = MahjongCard::create(DI_SOUTH_STAND,SOUTH_DIR,v);
			card->setAnchorPoint(Vec2(0.0f,0.5f));
			card->setScale(0.6f);
			card->setPosition(Vec2(5+index*50,size.height/2));
			bg->addChild(card);
			index++;
		}
	}

	void EastMahjongCardPool::changeCardToHead(std::vector<INT> cards)
	{
		for (auto v: cards)
		{
			auto itr = std::find(_handCardList.begin(),_handCardList.end(),v);
			if (itr != _handCardList.end())
			{
				_handCardList.erase(itr);
			}
		}

		std::vector<CardPool::CGroupCardData>groupCards;
		for (auto v:m_GroupCardDataVec)
		{
			groupCards.push_back(*v);
		}

		_handCard->refreshHandCard(groupCards,_handCardList,0);
		//把牌摆在前面
		_handCard->putChangeCardToHead(cards);
	}

}