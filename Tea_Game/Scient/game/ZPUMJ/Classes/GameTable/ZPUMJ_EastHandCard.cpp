#include "ZPUMJ_EastHandCard.h"
#include "ZPUMJ_GameManager.h"

namespace ZPUMJ
{
	static cocos2d::Vec2 beginDirPos = Vec2(1140, 130);
	static cocos2d::Vec2 endDirPos = Vec2(1020, 630);
	
	ZPUMJ_EastHandCard::ZPUMJ_EastHandCard()
	{
		_groupSize = Size(50,31.5);
		_normalSize = Size(50,31.5);
		_tingSize = Size(40,31.5);
		_beginZorder=3000;

	}

	ZPUMJ_EastHandCard::~ZPUMJ_EastHandCard()
	{
	}

	bool ZPUMJ_EastHandCard::init()
	{
		if (!HandCard::init())
		{
			return false;
		}
		return true;
	}


	void ZPUMJ_EastHandCard::refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards)
	{
		//处理碰杠牌，碰杠牌下边,扣放上边
		std::vector<CardPool::CGroupCardData> pengGangList;
		for (auto value:groupCards)
		{
			if ( value._iType==CardPool::CGroupCard_AnGang 
				|| value._iType == CardPool::CGroupCard_MingGang
				|| value._iType == CardPool::CGroupCard_Chi
				|| value._iType ==CardPool::CGroupCard_Peng)
			{
				pengGangList.push_back(value);
			}
		}

		//摆牌碰杠
		for(auto pengGang:pengGangList)
		{
			Vec2 secondPos = Vec2::ZERO;
			for (int i=0; i<pengGang._iCount; i++)
			{
				Card* pCard = nullptr;
				cocos2d::Sprite*ZheZhao = NULL;
				if (pengGang._iType == CardPool::CGroupCard_AnGang && i<4)
				{
					if (GameManager::getInstance()->isToushiMode() || GameManager::getInstance()->getPlayBack())
						pCard = GameManager::getInstance()->createPengGangFront(EAST_DIR, pengGang._iCardId);
					else
						pCard = GameManager::getInstance()->createBeiPai(EAST_DIR);
				}
				else if (pengGang._iType == CardPool::CGroupCard_Chi)
				{
					pCard = GameManager::getInstance()->createPengGangFront(EAST_DIR, pengGang._iChiCardArray[i]);
					if (pengGang._iChiCardArray[i] == pengGang._iCardId)
					{
						ZheZhao = Sprite::create(SPRITE_PATH + "Pgke.png");
					}
				}
				else
				{
					if (pengGang._iType == CardPool::CGroupCard_Peng)
					{
						if (i == 0)
						{
							ZheZhao = Sprite::create(SPRITE_PATH + "Pgke.png");
						}
					}
					pCard = GameManager::getInstance()->createPengGangFront(EAST_DIR, pengGang._iCardId);
				}

				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setCardEnableTouch(false);
				pCard->setVisible(true);
				
				if (ZheZhao != NULL)
				{
					ZheZhao->setScale(0.9);
					ZheZhao->setPosition(Vec2(36,28));
					pCard->addChild(ZheZhao, 1);
					ZheZhao->setVisible(true);
				}
				if (i==3)
				{
					pCard->setLocalZOrder(_beginZorder+3);
					pCard->setPosition(Vec2(secondPos.x+4,secondPos.y+15));
				}
				else
				{
					_prePoint = getNextPosition(_prePoint,_groupSize.height);
					pCard->setLocalZOrder(_beginZorder--);
					pCard->setPosition(_prePoint);
					if (i==1)
					{
						secondPos = _prePoint;
					}
				}
				this->addChild(pCard);
				_groupCardsList.push_back(pCard);
			}

			//每个碰杠留出空格
			_prePoint = getNextPosition(_prePoint,_groupSize.height/3.5f);
		}

	}


	cocos2d::Vec2 ZPUMJ_EastHandCard::getNextPosition(Vec2 prePoint, float deltLen)
	{
		cocos2d::Vec2 nodeDir = endDirPos-beginDirPos;
		nodeDir.normalize();
		nodeDir.scale(deltLen);
		return prePoint+nodeDir;

	}

	Vec2 ZPUMJ_EastHandCard::refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>normalCards, INT catchCard)
	{
		resetData();

		refreshPengGang(groupCards);

		//牌还没出的手牌
		std::vector<cocos2d::Node*> nodelist;
		bool is_playback = GameManager::getInstance()->getPlayBack();
		bool is_toushi = GameManager::getInstance()->isToushiMode();
		for (auto iter = normalCards.begin(); iter != normalCards.end(); iter++)
		{
			
			if (catchCard > 0 && iter == (normalCards.end() - 1))
				_prePoint = getNextPosition(_prePoint, _normalSize.height * 1.4f);
			else
				_prePoint = getNextPosition(_prePoint, _normalSize.height);
			if (is_playback || is_toushi)
			{
				Card* pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_EAST, EAST_DIR, *iter);
				pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
				pCard->setLocalZOrder(_beginZorder--);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(true);
				pCard->setPosition(_prePoint);
				this->addChild(pCard);

				_handCardsList.push_back(pCard);
			}
			else
			{
				Card* pCard = MahjongCard::create(mahjongCreateType::DI_EAST_STAND, EAST_DIR);
				pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
				pCard->setLocalZOrder(_beginZorder--);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(true);
				pCard->setPosition(_prePoint);
				this->addChild(pCard);

				_handCardsList.push_back(pCard);
			}
			
		}
		
	
		return Vec2::ZERO;
	}

	
	Vec2 ZPUMJ_EastHandCard::refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>tingCards)
	{
		resetData();

		refreshPengGang(groupCards);

		//牌还没出的手牌
		int index = 0;
		int size=tingCards.size();//用于记录玩家手牌数量
		int num=0;
		std::vector<cocos2d::Node*> nodelist;
		for (auto iter = tingCards.begin(); iter != tingCards.end(); iter++)
		{
			num++;
			_prePoint = getNextPosition(_prePoint,_groupSize.height);
			Card* pCard= nullptr;
			if(num==size)
			{//玩家听牌后放到一张牌
				pCard= MahjongCard::create(mahjongCreateType::DI_EAST_BACK, EAST_DIR);
				num=0;
			}
			else
			{
				pCard= MahjongCard::create(mahjongCreateType::DI_EAST_STAND, EAST_DIR);
			}
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			pCard->setLocalZOrder(_beginZorder--);
			pCard->setVisible(true);
			pCard->setCardEnableTouch(false);
			pCard->setPosition(_prePoint);
			this->addChild(pCard);

			_handCardsList.push_back(pCard);
		}

	
		return Vec2::ZERO;
	}

	Vec2 ZPUMJ_EastHandCard::refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>handCards, INT huCardId, bool visibleAllCard)
	{
		resetData();
		_groupCards.clear();
		for (auto v:groupCards)
		{
			_groupCards.push_back(v);
		}

		refreshPengGang(groupCards);
		for (auto iter = handCards.begin(); iter != handCards.end(); iter++)
		{
			if (*iter == 0)
			{
				//过滤结算报错（数据为空）
				break;
			}
			_prePoint = getNextPosition(_prePoint,_tingSize.height);
			if (visibleAllCard)
			{
				Card* pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_EAST, EAST_DIR, *iter);
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setLocalZOrder(_beginZorder--);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);

				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard->setHuVisible(true);
				}

			}
			else
			{
				Card* pCard	= nullptr;
				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_EAST, EAST_DIR, *iter);
					pCard->setHuVisible(true);
				}
				else
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_EAST_BACK, EAST_DIR);
				}
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setLocalZOrder(_beginZorder--);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);
			}
		}

		return Vec2::ZERO;
	}

	void ZPUMJ_EastHandCard::putChangeCardToHead(std::vector<INT> cards)
	{
		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}
		_changeCardsInHead.clear();

		float startX = 1176;
		int index = 0;
		int zorder = 500;
		std::vector<Point> v_pos_temp;
		for (auto v:cards)
		{
			auto pCard = MahjongCard::create(DI_EAST_BACK,EAST_DIR);
			pCard->setPosition(Vec2(startX-index*8,360+index*38));
			pCard->setLocalZOrder(zorder--);
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			this->addChild(pCard);
			_changeCardsInHead.push_back(pCard);
			
			MoveBy* moveBy = MoveBy::create(0.5f,Vec2(-120,0));
			pCard->runAction(moveBy);
			v_pos_temp.push_back(pCard->getPosition()+ Vec2(-120, 0));
			index++;
		}
		GameManager::getInstance()->addChangeCardPos(sitDir::EAST_DIR,v_pos_temp);
	}

	void ZPUMJ_EastHandCard::moveChangeCard(sitDir dir)
	{
		int i = 0;
		for (auto v:_changeCardsInHead)
		{
			switch (dir)
			{
			case ZPUMJ::SOUTH_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CW, bezDir::EAST_SOUTH_DIR, sitDir::SOUTH_DIR, i);
				break;
			case ZPUMJ::WEST_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::STRAIGHT, bezDir::BEZ_MID_DIR, sitDir::WEST_DIR, i);
				break;
			case ZPUMJ::NORTH_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CCW, bezDir::EAST_NORTH_DIR, sitDir::NORTH_DIR, i);
				break;
			default:
				break;
			}
			i++;
		}
		_changeCardsInHead.clear();
	}

	void ZPUMJ_EastHandCard::setSingleGroupSize(Size groupSize)
	{
		_groupSize = groupSize;
	}

	void ZPUMJ_EastHandCard::setNormalCardSize(Size normalSize)
	{
		_normalSize = normalSize;
	}

	void ZPUMJ_EastHandCard::setTingCardSize(Size tingSize)
	{
		_tingSize = tingSize;
	}

	cocos2d::Vec2 ZPUMJ_EastHandCard::getCatchWorldPos()
	{
		return convertToWorldSpace(getNextPosition(_prePoint,90));
	}

	void ZPUMJ_EastHandCard::resetData()
	{
		for (auto v:_groupCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_handCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}

		_groupCards.clear();
		_groupCardsList.clear();
		_handCardsList.clear();
		_changeCardsInHead.clear();
		_beginZorder=3000;
		_prePoint = beginDirPos;
	}

}
