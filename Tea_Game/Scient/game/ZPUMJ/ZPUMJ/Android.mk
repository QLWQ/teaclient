LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNNet)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNUI)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNMarket)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLobby)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLogic)

LOCAL_MODULE := ZPUMJ_static

LOCAL_MODULE_FILENAME := libZPUMJ

LOCAL_SRC_FILES := ../Classes/GameTable/ZPUMJ_GameTableLogic.cpp \
                   ../Classes/GameTable/ZPUMJ_GameManager.cpp \
                   ../Classes/GameTable/ZPUMJ_ResourceLoader.cpp \
				   ../Classes/GameTable/ZPUMJ_CardPool.cpp \
				   ../Classes/GameTable/ZPUMJ_GameTableUI.cpp \
				   ../Classes/GameTable/ZPUMJ_PoolAction.cpp \
				   ../Classes/GameTable/ZPUMJ_HuCard.cpp \
				   ../Classes/GameTable/ZPUMJ_Card.cpp \
				   ../Classes/GameTable/ZPUMJ_SouthMahjongCardPool.cpp \
				   ../Classes/GameTable/ZPUMJ_EastMahjongCardPool.cpp \
				   ../Classes/GameTable/ZPUMJ_WestHandCard.cpp \
				   ../Classes/GameTable/ZPUMJ_SouthHandCard.cpp \
				   ../Classes/GameTable/ZPUMJ_EastHandCard.cpp \
				   ../Classes/GameTable/ZPUMJ_WestMahjongCardPool.cpp \
				   ../Classes/GameTable/ZPUMJ_MahjongCard.cpp \
				   ../Classes/GameTable/ZPUMJ_NorthMahjongCardPool.cpp \
				   ../Classes/GameTable/ZPUMJ_NorthHandCard.cpp \
				   ../Classes/GameTable/ZPUMJ_ConcealedKong.cpp \
				   ../Classes/GameTable/ZPUMJ_Factory.cpp \
				   ../Classes/GameTable/ZPUMJ_MahjongCardPool.cpp \
				   ../Classes/GameTable/ZPUMJ_MeldedKong.cpp \
				   ../Classes/GameTable/ZPUMJ_TouchCard.cpp \
				   ../Classes/GameTable/ZPUMJ_TouchKong.cpp \
				   ../Classes/GameTable/ZPUMJ_Result.cpp \
				   ../Classes/GameTable/ZPUMJ_TurnTable.cpp \
				   ../Classes/GameTable/ZPUMJ_GMLayer.cpp \
				   ../Classes/GameTable/ZPUMJ_HandCard.cpp \
				   ../Classes/GameTable/ZPUMJ_ActionButtonList.cpp \
				   ../Classes/GameTable/ZPUMJ_SetLayer.cpp \
				   ../Classes/GameTable/ZPUMJ_EatCard.cpp \
				   ../Classes/GameTable/ZPUMJ_CheatLayer.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMessage \
                    $(LOCAL_PATH)/../Classes/GameTable \
                    $(LOCAL_PATH)/../../ \
                    $(LOCAL_PATH)/../ 

LOCAL_EXPORT_LDLIBS := -llog \
                       -lz \
                       -landroid     
                   
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_net_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_market_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_lobby_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_logic_static

LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static


include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)
$(call import-module,editor-support/cocostudio)
$(call import-module,extensions)

$(call import-module,LibHNUI)
$(call import-module,LibHNNet)
$(call import-module,LibHNMarket)
$(call import-module,LibHNLogic)
$(call import-module,LibHNLobby)