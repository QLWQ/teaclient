/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_SoundLayer_h__
#define __CaiShenThirteenCard_SoundLayer_h__

#include "cocos2d.h"
#include "HNNetExport.h"

namespace CaiShenThirteenCard
{
	class SoundLayer : public HN::HNLayer
	{
		Size		_nodeContentSize;
		ImageView*	_settingBK;
		Slider*		_sliderMusic;
		Slider*		_sliderEffects;
		std::function<void(bool open)> _musicCallback;
		std::function<void(bool open)> _effectsCallback;

	public:
		CREATE_FUNC(SoundLayer);

	public:
		virtual bool init() override;
		void closeSet();

	protected:
		SoundLayer();
		virtual ~SoundLayer();
	
	public:
		void setMusicCallback(const std::function<void(bool open)>& callback);
		void setEffectsCallback(const std::function<void(bool open)>& callback);


	protected:
		void onMusicTouchEventCallBack(Ref* pSender, Widget::TouchEventType type);
		void onEffectsTouchEventCallBack(Ref* pSender, Widget::TouchEventType type);

		void onMusicSliderCallback(Ref* pSender, Slider::EventType eventType);
		void onEffectsSliderCallback(Ref* pSender, Slider::EventType eventType);

	private:
		void wrireConfig();
		void readConfig();
	};
}

#endif // __CaiShenThirteenCard_SoundLayer_h__

