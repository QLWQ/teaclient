/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_ExitLayer_h__
#define __CaiShenThirteenCard_ExitLayer_h__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "HNNetExport.h"

USING_NS_CC;
using namespace ui;

namespace CaiShenThirteenCard
{
	class ExitLayer : public HN::HNLayer
	{
	public:
		CREATE_FUNC(ExitLayer);

	public:
		virtual bool init() override;
		void colseExit();

	public:
		void setSureClickEvent(const std::function<void ()>& callback);

	public:
		void onSureCallBack(Ref* pSender);
		void onCancelCallBack(Ref* pSender);

	protected:
		ExitLayer();
		virtual ~ExitLayer();

	private:
		ImageView*				_exitBG;
		std::function<void()>	_callback;
	};
}

#endif // __CaiShenThirteenCard_ExitLayer_h__
