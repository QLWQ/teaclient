/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_Game_Table_UI_Callback_H__
#define __CaiShenThirteenCard_Game_Table_UI_Callback_H__

#include "HNNetExport.h"
#include "HNLogicExport.h"
#include "CaiShenThirteenCardMessageHead.h"
#include <string>
#include <vector>

namespace CaiShenThirteenCard
{
	/*
	 * game ui callback
	 */

	class IGameTableUICallback : public HN::IHNGameLogicBase
	{
	public:
		// 玩家坐下
		virtual void onAddPlayer(BYTE lSeatNo, LLONG userID, bool self) = 0;
		// 玩家站起
		virtual void onRemovePlayer(BYTE lSeatNo, LLONG userID, bool self) = 0;
		//设置游戏基础倍率
		virtual void onSetGameBasePoint(INT point) = 0;

		virtual void setDeskInfo() = 0;
		virtual void onVipTip(BYTE lSeatNo) = 0;

	public:
		// 玩家准备
		virtual void onGameAgree(BYTE lSeatNo, bool self) = 0;
		// 通知抢庄
		virtual void onGameNoticeRobNt(BYTE lSeatNo, bool self) = 0;
		// 通知抢庄结果
		virtual void onGameRobNtResult(BYTE lSeatNo, bool robResult, bool self) = 0;
		// 确定庄家位置
		virtual void onGameMakeSureNt(BYTE lSeatNo, bool self, int QZSeatNO[]) = 0;
		// 发牌
		virtual void onGameSendCard(const std::vector<BYTE>& cards) = 0;
		//一键搓牌
		virtual void onGameSetAutoRubCardType(BYTE byHeapCard[3][5], int iHeapShape[3]) = 0;
		// 通知开牌
		virtual void onGameNoticeOpenCard() = 0;
		// 开牌结果
		virtual void onGameOpenCardResult(BYTE lSeatNo) = 0;
		// 比牌
		virtual void onGameCompareCard(const BYTE heapCard[PLAY_COUNT][3][5]) = 0;
		// 开火
		virtual void onGameOpenFire(const bool userFireUser[PLAY_COUNT][PLAY_COUNT]) = 0;
		//被打枪音效
		virtual void onGameBeOpenFireEffect() = 0;
		// 全垒打
		virtual void onGameFireAll(BYTE lSeatNo) = 0;
		//全垒打音效
		virtual void onGameFireAllEffect() = 0;
		// 游戏结算
		virtual void onGameResult(const S_C_GameResult* pObject) = 0;
		// 游戏规则
		virtual void showGameRule(bool bHaveCaiShen, bool bSiHua, int point, BYTE playerNum, BYTE playMode, bool tongSha, bool jiaLiangMen) = 0;
		//直接显示自己的摆牌数据
		virtual void showMyCard(const BYTE heapCard[3][5]) = 0;
		virtual void showUserCut(BYTE lseat) = 0;

		//赠送礼物回复
		//virtual void dealGift(TMSG_SENG_GIFT_REQ data) = 0;
	public:
		virtual void onGSFree(const bool userReady[PLAY_COUNT]) = 0;
		virtual void onGSRobNt(BYTE currRobUser) = 0;
		virtual void onGSSendCard(const std::vector<BYTE>& cards) = 0;
		virtual void onGSOpenCard(const std::vector<BYTE>& cards, const BYTE heapCard[3][5]) = 0;
		virtual void onGSCompareCard(const BYTE heapCard[PLAY_COUNT][3][5]) = 0;
		virtual void sendRecordData(S_C_GameRecordResult* data)= 0;
		virtual void sendSSSRecordData(S_C_Game_SSS_RecordResult* data) = 0;
		virtual void onCheatStatus(S_C_CheatStatus* data) = 0;
		//显示小结算榜
		//virtual void showSingleCalculateBoard(GameEndStruct* endData) = 0;
		//显示大结算榜
		virtual void showCalculateBoard(const CalculateBoard* boardData) = 0;
		////////////////////////////////////////////////////////////////////
		//聊天
		////////////////////////////////////////////////////////////////////
		virtual void onChatTextMsg(BYTE seatNo, std::string msg) = 0;									// 文本聊天消息

		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime) = 0;						// 语音聊天消息

		virtual void setPlayerPos()=0;
		virtual void setAllFire(bool isAllFire) = 0;//是否全垒打

		//打枪提示音
		virtual void tipsOpenFireEffect(bool sex) = 0;
		//全垒打提示音
		virtual void tipsAllOpenFireEffect(bool sex) = 0; 
		virtual void	setBottomScore(int Score) = 0;

	};

}



#endif
