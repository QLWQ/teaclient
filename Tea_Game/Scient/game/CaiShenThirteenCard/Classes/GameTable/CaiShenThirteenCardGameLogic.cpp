/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardGameLogic.h"
#include "CaiShenThirteenCardMessageHead.h"

#include <time.h>
#include <algorithm>
#include <stdlib.h>

namespace CaiShenThirteenCard
{
	//构造函数
	CUpGradeGameLogic::CUpGradeGameLogic(void)
	{
		m_bSortCardStyle = 0; //0 牌按大小排序;1 按牌型排序
		m_iCondition = 0;			//无出牌限制条件
		m_bKingCanReplace = true;  //是否有财神牌
		//	m_iStation[4] = 500;
		//	for (int i=0; i<4; i++)
		//		m_iStation[i] = 100*i;
	}
	//析造函数
	CUpGradeGameLogic::~CUpGradeGameLogic()
	{
	}
	//获取扑克花色
	BYTE CUpGradeGameLogic::GetCardHuaKind(BYTE iCard, bool bTrueHua)
	{
		int iHuaKind = (iCard&UG_HUA_MASK) >> 4;
		if (!bTrueHua)
		{
			return iHuaKind = UG_NT_CARD;
		}
		return iHuaKind;
	}

	//获取扑克大小 （2 - 18 ， 15 以上是主牌 ： 2 - 21 ， 15 以上是主）
	int CUpGradeGameLogic::GetCardBulk(BYTE iCard, bool bExtVal)
	{
		//if ((iCard == 0x6E) || (iCard == 0x6F))
		//{
		//	return bExtVal ? (iCard - 14) : (iCard - 62); //大小鬼64+14-62=16	只返回大小猫的值
		//}

		int iCardNum = GetCardNum(iCard);
		int iHuaKind = GetCardHuaKind(iCard, true);

		return ((bExtVal) ? ((iHuaKind >> 4) + (iCardNum * 4)) : (iCardNum));
	}

	//设置排序方式
	void CUpGradeGameLogic::SetSortKind(BYTE bSortCardStyle)
	{
		m_bSortCardStyle = bSortCardStyle;
	}
	//按牌面数字从大到小排列扑克
	bool CUpGradeGameLogic::SortCard(BYTE iCardList[], BYTE bUp[], BYTE iCardCount, bool bSysSort)
	{
		bool bSorted = true, bTempUp;
		int iTemp, iLast = 0, iStationVol[45];
		memset(iStationVol, 0, sizeof(iStationVol));
		if (iCardCount > 45)
		{
			iCardCount = 45;
		}
		iLast = iCardCount - 1;
		//获取位置数值
		for (int i = 0; i<iCardCount; i++)
		{
			iStationVol[i] = GetCardBulk(iCardList[i], true);
			//if (iStationVol[i]>=15) iStationVol[i]+=m_iStation[4];
			//else iStationVol[i]+=m_iStation[GetCardHuaKind(iCardList[i],FALSE)>>4];
		}

		//排序操作(按从大到小排序)
		do
		{
			bSorted = true;
			for (int i = 0; i<iLast; i++)
			{
				if (iStationVol[i]<iStationVol[i + 1])
				{
					//交换位置				//==冒泡排序
					iTemp = iCardList[i];
					iCardList[i] = iCardList[i + 1];
					iCardList[i + 1] = iTemp;

					iTemp = iStationVol[i];
					iStationVol[i] = iStationVol[i + 1];
					iStationVol[i + 1] = iTemp;

					if (bUp != NULL)
					{
						bTempUp = bUp[i];
						bUp[i] = bUp[i + 1];
						bUp[i + 1] = bTempUp;
					}
					bSorted = false;
				}
			}
			iLast--;
		} while (!bSorted);

		//系统序列不考虑花色牌型问题
		if (bSysSort)
		{
			ReverseCard(iCardList, bUp, iCardCount);
			return true;
		}
		if (GetSortCardStyle() == 1) //按牌型排序
			SortCardByStyle(iCardList, iCardCount);

		if (GetSortCardStyle() == 2)
			SortCardByKind(iCardList, iCardCount);


		return true;
	}

	bool CUpGradeGameLogic::ReverseCard(BYTE iCardList[], BYTE bUp[], BYTE iCardCount)
	{
		BYTE iTemp;
		for (int i = 0; i< iCardCount / 2; i++)
		{
			iTemp = iCardList[i];
			iCardList[i] = iCardList[iCardCount - 1 - i];
			iCardList[iCardCount - 1 - i] = iTemp;
		}
		return true;
	}
	//按牌型排序
	bool CUpGradeGameLogic::SortCardByStyle(BYTE iCardList[], BYTE iCardCount)
	{
		//如果排序设置是要求按大小排序
		if (m_bSortCardStyle == 0)
		{
			SortCard(iCardList, NULL, iCardCount);
			return true;
		}
		//下面的代码==按牌形排大小
		int iStationVol[45];
		for (int i = 0; i<iCardCount; i++)
		{
			iStationVol[i] = GetCardBulk(iCardList[i], false);
		}

		int Start = 0;
		int j, step;
		BYTE CardTemp[8];					//用来保存要移位的牌形
		int CardTempVal[8];					//用来保存移位的牌面值
		for (int i = 8; i>1; i--)				//在数组中找一个连续i张相同的值
		{
			for (j = Start; j<iCardCount; j++)
			{
				CardTemp[0] = iCardList[j];			//保存当前i个数组相同的值
				CardTempVal[0] = iStationVol[j];
				for (step = 1; step<i&&j + step<iCardCount;)			//找一个连续i个值相等的数组(并保存于临时数组中)
				{
					if (iStationVol[j] == iStationVol[j + step])
					{
						CardTemp[step] = iCardList[j + step];			//用来保存牌形
						CardTempVal[step] = iStationVol[j + step];		//面值
						step++;
					}
					else
						break;
				}

				if (step >= i)	//找到一个连续i个相等的数组串起始位置为j,结束位置为j+setp-1
				{			//将从Start开始到j个数组后移setp个
					if (j != Start) //排除开始就是有序
					{
						for (; j >= Start; j--) //从Start张至j张后移动i张
						{
							iCardList[j + i - 1] = iCardList[j - 1];
							iStationVol[j + i - 1] = iStationVol[j - 1];
						}
						for (int k = 0; k<i; k++)
						{
							iCardList[Start + k] = CardTemp[k];	//从Start开始设置成CardSave
							iStationVol[Start + k] = CardTempVal[k];
						}
					}
					Start = Start + i;
				}
				j = j + step - 1;
			}
		}
		return true;
	}

	//按花色排序
	bool CUpGradeGameLogic::SortCardByKind(BYTE iCardList[], BYTE iCardCount)
	{
		return true;
	}

	//删除扑克
	int CUpGradeGameLogic::RemoveCard(BYTE iRemoveCard[],   //要删除的牌面
		int iRemoveCount,		//要删除的牌总数
		BYTE iCardList[],		//要处理的数组
		int iCardCount)		//处理数组的上限
	{
		//检验数据
		if (iRemoveCount > iCardCount) return 0;

		int iRecount;
		int iDeleteCount = 0; //把要删除的牌置零

		for (int i = 0; i < iRemoveCount; i++)
		{
			for (int j = 0; j < iCardCount; j++)
			{
				if (iRemoveCard[i] == iCardList[j])
				{
					iDeleteCount++;
					iCardList[j] = 0;
					break;
				}
			}
		}
		iRecount = RemoveNummCard(iCardList, iCardCount); //删除做了标记的牌

		if (iDeleteCount != iRecount)
			return 0;

		return iDeleteCount;
	}

	//增加扑克牌数据
	int	CUpGradeGameLogic::AddCardToList(BYTE byAddCard[], int &iAddCount, BYTE byCardList[], int iCardCount)
	{
		if (iAddCount > iCardCount) return 0;

		int iTmpAddCount = 0; //把要删除的牌置零
		for (int i = 0; i < iAddCount; i++)
		{
			if (0 == byAddCard[i])
			{
				continue;
			}

			for (int j = 0; j < iCardCount; j++)
			{
				if (byCardList[j] == 0)
				{
					byCardList[j] = byAddCard[i];
					byAddCard[i] = 0;
					iTmpAddCount++;
					break;
				}
			}
		}

		iAddCount = 0;


		if (iTmpAddCount != iAddCount)
		{
			return 0;
		}

		return iTmpAddCount;
	}


	//清除 0 位扑克
	int CUpGradeGameLogic::RemoveNummCard(BYTE iCardList[], int iCardCount)
	{
		int iRemoveCount = 0;
		std::vector<BYTE> temp;
		temp.clear();
		for (int i = 0; i<iCardCount; i++)
		{
			if (iCardList[i] != 0)
			{
				temp.push_back(iCardList[i]);
			}
			else
			{
				iRemoveCount++;
			}
		}
		memset(iCardList, 0, sizeof(BYTE)*iCardCount);
		for (int i = 0; i<temp.size(); i++)
		{
			iCardList[i] = temp.at(i);
		}
		return iRemoveCount;
	}

	//比较单张
	bool CUpGradeGameLogic::CompareOnlyOne(BYTE iFirstCard, BYTE iNextCard)
	{
		//第一个表示桌面上最大牌, 第二个表示要出的牌
		return GetCardBulk(iFirstCard) < GetCardBulk(iNextCard);
	}

	//几张牌是否是相同数字
	bool CUpGradeGameLogic::IsSameNumCard(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		int i, temp[18] = { 0 };
		for (i = 0; i < iCardCount; i++)
		{
			temp[GetCardBulk(iCardList[i], false)]++;
		}

		for (i = 0; i < 18; i++)
		{
			if (temp[i] != 0)
				break;
		}
		if (m_bKingCanReplace)
		{
			if (i<16)//王带其他牌
				return (temp[i] + temp[16] + temp[17] == iCardCount);
			//else//只有王
			if (i < 17)
				return (temp[16] + temp[17] == iCardCount);
		}
		else
			return (temp[i] == iCardCount);
		return 0;
	}

	//查找用户手中炸弹数
	BYTE CUpGradeGameLogic::GetBombCount(BYTE iCardList[], int iCardCount, int iNumCount, bool bExtVal)
	{
		int iCount = 0,
			temp[18] = { 0 };
		for (int i = 0; i<iCardCount; i++)
		{
			temp[GetCardBulk(iCardList[i])]++;
		}
		for (int i = 0; i<16; i++)
		{
			if (temp[i] >= iNumCount)
				iCount++;
		}
		return iCount;
	}

	//获取指定张数牌个数
	BYTE CUpGradeGameLogic::GetCountBySpecifyNumCount(BYTE iCardList[], int iCardCount, int Num)
	{
		BYTE temp[18] = { 0 };
		int count = 0;
		for (int i = 0; i < iCardCount; i++)
			temp[GetCardBulk(iCardList[i])]++;

		for (int i = 0; i< 18; i++)
		if (temp[i] == Num)
			count++;

		return count;
	}

	//获取指定牌个数
	BYTE CUpGradeGameLogic::GetCountBySpecifyCard(BYTE iCardList[], int iCardCount, BYTE bCard)
	{
		int count = 0;
		for (int i = 0; i < iCardCount; i++)
		if (iCardList[i] == bCard)
			count++;

		return count;
	}
	//获取指定牌张数牌大小
	BYTE CUpGradeGameLogic::GetBulkBySpecifyCardCount(BYTE iCardList[], int iCardCount, int iCount)
	{
		BYTE temp[18] = { 0 };
		for (int i = 0; i < iCardCount; i++)
			temp[GetCardBulk(iCardList[i])]++;

		for (int i = 17; i> 0; i--)
		if (temp[i] == iCount)
			return i;

		return 0;
	}

	//是否为变种顺子
	bool CUpGradeGameLogic::IsVariationSequence(BYTE iCardList[], int iCardCount, int iCount)
	{
		int iValue = iCardCount / iCount;
		if (iCardCount != iCount *iValue)						 //张数不相配
			return false;

		int iFirstMax = 0, iSecondMax = 0, iThirdMax = 0, iMin = 18;//找出第一大,第二大,第三大的牌,和最小牌
		BYTE temp[18] = { 0 };
		for (int i = 0; i < iCardCount; i++)						//牌多少
		{
			temp[GetCardBulk(iCardList[i])]++;
		}

		for (int i = 0; i<18; i++)
		{
			if (temp[i] != 0 && temp[i] != iCount)	//非找定顺子
				return false;
		}

		for (int i = 0; i < 18; i++)						//最小牌最大可能到A
		{
			if (temp[i] != 0)
				iMin = i;
		}

		for (int i = 17; i>0; i--)
		{
			if (temp[i] != 0)
			{
				iFirstMax = i;						//可能是2也可以是A
				for (int j = i - 1; j>0; j--)
				{
					if (temp[j] != 0)//找到第二大的退出循环(无第三大的)//可能是A也可以非A
					{
						iSecondMax = j;
						for (int k = j - 1; j>0; j--)
						{
							if (temp[k] != 0)//查第第三大的退出循环	//可是存在也可以不存在
							{
								iThirdMax = k;
								break;
							}
						}
						break;
					}
				}
				break;
			}
		}

		if (iFirstMax < 15)	//不存在2的情况,正常情况下
		{
			return (iFirstMax - iMin + 1 == iValue);
		}

		if (iFirstMax == 15)	//存在2,再看是否存在A
		{
			if (iSecondMax == 14)		//存在A
			{
				if (iThirdMax == 0)		//不存在第三大,也只有A2两种牌
					return true;

				return (iThirdMax - iMin + 1 == iValue - 2);		//存在 A2情况包括处理AA2233
			}
			return (iSecondMax - iMin + 1 == iValue - 1);
		}

		return false;
	}
	/*
	@brief:是否為順子
	@param:iCardList,牌数组，iCardCount，牌数量
	@return:是否为顺子
	*/
	//
	bool CUpGradeGameLogic::IsSequence(BYTE iCardList[], int iCardCount)
	{
		BYTE temp[17] = { 0 };
		memset(temp, 0, sizeof(BYTE)* 17);
		for (int i = 0; i < iCardCount; i++)
		{
			temp[GetCardNum(iCardList[i])]++;
		}
		if (temp[14] != 0)//有A这个值
		{
			bool flag = true;
			//扫描连续位置的几张牌是否都为1，如果是则为顺子
			for (int i = 2; i < 6; i++)
			{
				if (temp[i] == 0)
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				return true;
			}
		}

		for (int i = 0; i < 17; i++)
		{
			if (temp[i] != 0)//有值
			{
				//扫描连续位置的几张牌是否都为1，如果是则为顺子
				for (int j = i; j < i + iCardCount; j++)
				{
					if (temp[j] != 1 || j >= 15)
					{
						return false;
					}
				}
				return true;
			}
		}

	}

	//提取指定牌返回找到牌個數
	BYTE  CUpGradeGameLogic::TackOutBySpecifyCard(BYTE iCardList[], int iCardCount, BYTE bCardBuffer[], int &iResultCardCount, BYTE bCard)
	{
		iResultCardCount = 0;
		for (int i = 0; i < iCardCount; i++)
		{
			if (iCardList[i] == bCard)
				bCardBuffer[iResultCardCount++] = iCardList[i];
		}
		return iResultCardCount;
	}


	//提取1,2,3 or 4张相同数字的牌
	int CUpGradeGameLogic::TackOutBySepcifyCardNumCount(BYTE iCardList[], int iCardCount,
		BYTE iDoubleBuffer[], BYTE bCardNum,
		bool bExtVal)
	{
		int iCount = 0, temp[18] = { 0 };
		for (int i = 0; i < iCardCount; i++)
		{
			temp[GetCardNum(iCardList[i])]++;
		}

		BYTE iKingResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iCardList, iCardCount, iKingResultList);

		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iCardList, iCardCount, iHuaResultList);

		if (!m_bKingCanReplace)
		{
			iKingCount = 0;
			iHuaCount = 0;
		}
		for (int i = 14; i>=0; i--)
		{
			if (temp[i] + iKingCount + iHuaCount >= bCardNum && temp[i]>0) //现在要查找的牌型:one?double?three?four_bomb?
			{
				for (int j = 0; j < iCardCount; j++)
				{
					if (iCount<4 && i == GetCardNum(iCardList[j]))
					{
						iDoubleBuffer[iCount++] = iCardList[j];
					}
					if (iCount<4  && j == iCardCount - 1 && iKingCount>0) //用王牌填充
					{
						iDoubleBuffer[iCount++] = iKingResultList[iKingCount - 1];
						iKingCount--;
						if (iCount<4 && iKingCount>0)
						{
							iDoubleBuffer[iCount++] = iKingResultList[iKingCount - 1];
							iKingCount--;
						}
					}
					if (iCount<4 && j == iCardCount - 1 && iHuaCount>0) //用花牌填充
					{
						iDoubleBuffer[iCount++] = iHuaResultList[iHuaCount - 1];
						iHuaCount--;
						if (iCount < 4 && iHuaCount>0)
						{
							iDoubleBuffer[iCount++] = iHuaResultList[iHuaCount - 1];
							iHuaCount--;
							if (iCount < 4 && iHuaCount>0)
							{
								iDoubleBuffer[iCount++] = iHuaResultList[iHuaCount - 1];
								iHuaCount--;
							}
						}
					}
				}
			}
		}
		return iCount;
	}

	//提取指定花色牌
	int CUpGradeGameLogic::TackOutByCardKind(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], BYTE iCardKind)
	{
		int count = 0;

		for (int i = 0; i < iCardCount; i++)
		{
			//TCHAR sz[200];
			//wsprintf(sz,"i=%d,iCardKind = %d %d",i,iCardKind,GetCardHuaKind(iCardList[i]));
			//WriteStr(sz,2,2);
			if (GetCardHuaKind(iCardList[i]) == iCardKind)
			{
				iDoubleBuffer[count++] = iCardList[i];
			}
		}
		return count;
	}

	//拆出(将手中牌多的拆成少的)
	int CUpGradeGameLogic::TackOutMuchToFew(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], int &iBufferCardCount, BYTE iCardMuch, BYTE iCardFew)
	{
		iBufferCardCount = 0;
		int count = 0;
		BYTE iBuffer[54];
		int iCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, iCardMuch);
		if (iCount <= 0)
			return count;
		for (int i = 0; i < iCount; i += iCardMuch)
		{
			::memcpy(&iDoubleBuffer[iBufferCardCount], &iBuffer[i], sizeof(BYTE)*iCardFew);
			iBufferCardCount += iCardFew;
			count++;
		}
		return count;
	}

	//查找大于iCard的单牌所在iCardList中的序号
	BYTE  CUpGradeGameLogic::GetSerialByMoreThanSpecifyCard(BYTE iCardList[], int iCardCount,
		BYTE iCard, BYTE iBaseCardCount,
		bool bExtValue)
	{
		BYTE MaxCard = 0;
		BYTE Serial = 0;
		BYTE MaxCardNum = 255;

		int BaseCardNum = GetCardBulk(iCard);	//当前比较值

		for (BYTE i = 0; i<iCardCount; i += iBaseCardCount)
		{
			int temp = GetCardBulk(iCardList[i]);

			if (temp<MaxCardNum && temp>BaseCardNum)
			{
				MaxCardNum = temp;
				Serial = i; //得到序号
				break;
			}
		}

		return Serial;
	}


	//查找==iCard的单牌所在iCardList中的序号
	int  CUpGradeGameLogic::GetSerialBySpecifyCard(BYTE iCardList[], int iStart, int iCardCount, BYTE iCard)
	{
		for (int i = iStart; i < iCardCount; i++)
		{
			if (iCardList[i] == iCard)
				return i;
		}
		return -1;
	}

	//变种顺子中最大的
	BYTE CUpGradeGameLogic::GetBulkBySpecifyVariationSequence(BYTE iCardList[], int iCardCount, int iSequence)
	{
		int iFirstMax = 0, iSecondMax = 0, iThirdMax = 0;//找出第一大,第二大,第三大的牌,和最小牌
		BYTE temp[18] = { 0 };
		for (int i = 0; i < iCardCount; i++)						//牌多少
		{
			temp[GetCardBulk(iCardList[i])]++;
		}

		for (int i = 17; i>0; i++)
		{
			if (temp[i] == iSequence)
			{
				iFirstMax = i;						//可能是2也可以是A
				for (int j = i - 1; j>0; j--)
				{
					if (temp[j] == iSequence)//找到第二大的退出循环(无第三大的)//可能是A也可以非A
					{
						iSecondMax = j;
						for (int k = j - 1; j>0; j--)
						{
							if (temp[k] == iSequence)//查第第三大的退出循环	//可是存在也可以不存在
							{
								iThirdMax = k;
								break;
							}
						}
						break;
					}
				}
				break;
			}
		}

		if (iFirstMax == 15)	//存在2,再看是否存在A
		{
			if (iSecondMax == 14)		//存在A
			{
				if (iThirdMax == 0)		//不存在第三大,也只有A2两种牌
					return 2;

				return iThirdMax;		//存在 A2情况包括处理AA2233
			}
			return iSecondMax;
		}
		return 0;
	}

	//获取指定顺子中牌点最小值(iSequence 代表顺子的牌数最多为
	BYTE  CUpGradeGameLogic::GetBulkBySpecifySequence(BYTE iCardList[], int iCardCount, int iSequence)
	{
		int temp[18] = { 0 };
		for (int i = 0; i < iCardCount; i++)
		{
			temp[GetCardBulk(iCardList[i])]++;
		}

		for (int k = 0; k < 15; k++)
		{
			if (temp[k] == iSequence)
			{
				return k;
			}
		}
		return 0;
	}

	//找出一个最小或最大的牌
	int  CUpGradeGameLogic::GetBulkBySepcifyMinOrMax(BYTE iCardList[], int iCardCount, int MinOrMax/*1 or 255*/, bool bExtVal)
	{
		int CardNum = GetCardBulk(iCardList[0], false);

		if (MinOrMax == 1) //找最小的
		{
			for (int i = 1; i < iCardCount; i++)
			{
				if (GetCardBulk(iCardList[i], false) < CardNum)
					CardNum = GetCardBulk(iCardList[i], false);
			}
		}
		else if (MinOrMax == 255)
		{
			for (int i = 1; i < iCardCount; i++)
			{
				if (GetCardBulk(iCardList[i], false) > CardNum)
					CardNum = GetCardBulk(iCardList[i], false);
			}
		}

		//返回的是 GetCardBulk() 得到的值
		return CardNum;
	}

	/////////////////////////////////////////////////////////////////////////
	/**
	* @info 获取牌型
	* @param iCardList[] 牌
	* @param iCardCount 牌的数量
	* @param bExlVol ?
	*/
	BYTE CUpGradeGameLogic::GetCardShape(BYTE iCardList[], int iCardCount, bool bExlVol)
	{
		BYTE bIsLuck[5] = { 0 };
		if (IsOnlyOne(iCardList, iCardCount) && (m_iCardShape&(0x01))) return UG_ONLY_ONE; //单牌
		if (IsDouble(iCardList, iCardCount) && (m_iCardShape&(0x01 << 1))) return UG_DOUBLE;	 //对牌
		if (IsThreeX(iCardList, iCardCount, 0) && (m_iCardShape&(0x01 << 2))) return UG_THREE;	 //三张

		if (IsThreeX(iCardList, iCardCount, 1) && (m_iCardShape&(0x01 << 3))) return UG_THREE_ONE; //三带一
		if (IsThreeX(iCardList, iCardCount, 2) && (m_iCardShape&(0x01 << 4))) return UG_THREE_TWO; //三带二
		if (IsThreeX(iCardList, iCardCount, 3) && (m_iCardShape&(0x01 << 5)))	return UG_THREE_DOUBLE;	//三带对


		/* 顺子中包括 同花顺,所以先判断是否同花顺,如果不是，再判断是否是顺子，如果是顺子，就是一般的顺子啦*/
		if (IsStraightFlush(iCardList, iCardCount) && (m_iCardShape&(0x01 << 7))) return UG_STRAIGHT_FLUSH; //同花顺
		if (IsStraight(iCardList, iCardCount) && (m_iCardShape&(0x01 << 6))) return UG_STRAIGHT;            //顺子	
		if (IsDoubleSequence(iCardList, iCardCount) && (m_iCardShape&(0x01 << 8))) return UG_DOUBLE_SEQUENCE;  //连对


		if (IsThreeXSequence(iCardList, iCardCount, 3) && (m_iCardShape&(0x01 << 12))) return UG_THREE_DOUBLE_SEQUENCE; //连的三带对
		if (IsThreeXSequence(iCardList, iCardCount, 2) && (m_iCardShape&(0x01 << 11))) return UG_THREE_TWO_SEQUENCE; //连的三带二
		if (IsThreeXSequence(iCardList, iCardCount, 1) && (m_iCardShape&(0x01 << 10))) return UG_THREE_ONE_SEQUENCE; //连的三带一
		if (IsThreeXSequence(iCardList, iCardCount, 0) && (m_iCardShape&(0x01 << 9))) return UG_THREE_SEQUENCE; //连三

		if (IsFourX(iCardList, iCardCount, 4) && (m_iCardShape&(0x01 << 16))) return UG_FOUR_TWO_DOUBLE;		//四带二对(要求是二对)
		if (IsFourX(iCardList, iCardCount, 3) && (m_iCardShape&(0x01 << 15))) return UG_FOUR_ONE_DOUBLE;		//四带一对(要求成对)
		if (IsFourX(iCardList, iCardCount, 2) && (m_iCardShape&(0x01 << 14))) return UG_FOUR_TWO;			//四带二(不要求成对)
		if (IsFourX(iCardList, iCardCount, 1) && (m_iCardShape&(0x01 << 13))) return UG_FOUR_ONE;			//四带一

		if (IsFourXSequence(iCardList, iCardCount, 4)) return UG_FOUR_TWO_DOUBLE_SEQUENCE;	//四顺带二对
		if (IsFourXSequence(iCardList, iCardCount, 2)) return UG_FOUR_TWO_SEQUENCE;	//四顺带二单张
		if (IsFourXSequence(iCardList, iCardCount, 0)) return UG_FOUR_SEQUENCE;	//四顺


		if (IsKingBomb(iCardList, iCardCount) && (m_iCardShape&(0x01 << 20))) return UG_KING_BOMB;//王炸
		if (IsBomb(iCardList, iCardCount) && (m_iCardShape&(0x01 << 19))) return UG_BOMB; //4张以上同点牌，炸弹

		return UG_ERROR_KIND;
	}

	//对牌
	bool CUpGradeGameLogic::IsDouble(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		if (iCardCount != 2)
			return false;
		return IsSameNumCard(iCardList, iCardCount, bExtVal);
	}

	//3 带 0,1or2,or3
	bool CUpGradeGameLogic::IsThreeX(BYTE iCardList[], int iCardCount, int iX, bool bExtVal)
	{
		INT newCount = 0;
		BYTE newCards[13] = { 0 };
		bool isSameFlower = FindCalabash(iCardList, iCardCount, newCards, newCount);
		return isSameFlower;
	}

	//四带1or2
	bool CUpGradeGameLogic::IsFourX(BYTE iCardList[], int iCardCount, int iX)
	{
		if (iCardCount >8 || iCardCount < 4)
			return false;

		if (GetCountBySpecifyNumCount(iCardList, iCardCount, 4) != 1)//是否有四个牌型
			return false;

		switch (iX)
		{
		case 0:
			return iCardCount == 4;//四张
		case 1:
			return iCardCount == 5;//四带1张
		case 2:
			return iCardCount == 6;//四带2(不要求成对)
		case 3:
			return (iCardCount == 6 && 1 == GetCountBySpecifyNumCount(iCardList, iCardCount, 2));//要求成对
		case 4:
			return (iCardCount == 8 && 2 == GetCountBySpecifyNumCount(iCardList, iCardCount, 2));	//四带2对
		}

		return false;
	}

	//王炸
	bool CUpGradeGameLogic::IsKingBomb(BYTE iCardList[], int iCardCount)			//是否为王炸(抓到所的王)
	{
		if (iCardCount != KING_COUNT)
			return false;

		for (int i = 0; i<iCardCount; i++)
		if (iCardList[i] != 0x6e && iCardList[i] != 0x6f)
			return false;
		return true;
	}

	//4+张牌 炸弹
	bool CUpGradeGameLogic::IsBomb(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		if (iCardCount < 4)
			return false;

		return IsSameNumCard(iCardList, iCardCount, bExtVal); //是否是相同数字
	}

	//同花炸弹
	bool CUpGradeGameLogic::IsBombSameHua(BYTE iCardList[], int iCardCount)
	{
		BYTE bIsLuck[5] = { 0 };
		if (!IsBomb(iCardList, iCardCount)) return false;
		if (!IsSameHuaKind(iCardList, iCardCount)) return false;
		return true;
	}

	//同花(非同花)
	bool CUpGradeGameLogic::IsFlush(BYTE iCardList[], int iCardCount)
	{
		BYTE bIsLuck[5] = { 0 };
		return IsSameHuaKind(iCardList, iCardCount);
	}

	//同花顺 
	bool CUpGradeGameLogic::IsStraightFlush(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		//if (!IsSameHuaKind(iCardList, iCardCount)) return false; //同花？

		//if (!IsStraight(iCardList, iCardCount)) return false; //顺子？
		INT newCount = 0;
		BYTE newCards[13] = { 0 };
		bool isSameFlower = FindSameFlower(iCardList, iCardCount, newCards, newCount);
		if (!isSameFlower)
		{
			return false;
		}
		INT newCount1 = 0;
		BYTE newCards1[13] = { 0 };
		bool isStraight = FindStraight(iCardList, iCardCount, newCards1, newCount1);
		if (!isStraight)
		{
			return false;
		}
		return true;
	}

	//指定张数是否是顺子
	bool CUpGradeGameLogic::IsStraight(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		if (iCardCount < 5)
			return false;
		//return IsSequence(iCardList, iCardCount);
		INT newCount = 0;
		BYTE newCards[13] = { 0 };
		bool isStraight = FindStraight(iCardList, iCardCount, newCards, newCount);
		return isStraight;
	}

	//是否是连对
	bool CUpGradeGameLogic::IsDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		BYTE bIsLuck[5] = { 0 };
		if (iCardCount % 2 != 0 || iCardCount < 6)
			return false;

		return IsSequence(iCardList, iCardCount);
	}

	//三顺带二顺
	bool CUpGradeGameLogic::IsThreeSequenceDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		BYTE bIsLuck[5] = { 0 };
		if (iCardCount < 10)		//三顺至少2二顺也至少二
			return false;

		BYTE iBuffer3[54], iBuffer2[54];
		bool bValue3 = false, bValue2 = false;	//三顺,二顺是否为顺,
		int TackOutCount3 = 0, TackOutCount2 = 0;

		TackOutCount3 = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer3, 3);//三对
		TackOutCount2 = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer2, 2);//二对
		if (TackOutCount3 <= 0 || TackOutCount2 <= 0 || TackOutCount3 + TackOutCount2 != iCardCount || TackOutCount3 / 3 != TackOutCount2 / 2)
			return false;
		bValue3 = IsSequence(iBuffer3, TackOutCount3);
		//	TCHAR sz[200];
		//wsprintf(sz,"%d",bValue3);
		//	WriteStr(sz);
		bValue2 = (IsVariationSequence(iBuffer2, TackOutCount2, 2) || IsSequence(iBuffer2, TackOutCount2));
		//	TCHAR sz[200];
		//	wsprintf(sz,"bValue3=%d,bValue2=%d==变种2顺%d,标准二顺%d",bValue3,bValue2,IsVariationSequence(iBuffer2,TackOutCount2,2),IsSequence(iBuffer2,TackOutCount2,2));
		//	WriteStr(sz);
		return bValue3&&bValue2;
	}

	//连的三带 0,1 or 2
	bool CUpGradeGameLogic::IsThreeXSequence(BYTE iCardList[], int iCardCount, int iSeqX, bool bExtVal)
	{
		BYTE bIsLuck[5] = { 0 };
		if (iCardCount < 6)		//三顺至少2
			return false;

		BYTE iBuffer[54];
		int TackOutCount = 0;
		switch (iSeqX)
		{
		case 0:
			if (iCardCount % 3 != 0)
				return false;
			return IsSequence(iCardList, iCardCount);
			break;
		case 1://带单
			TackOutCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, 3);
			if (TackOutCount>0 && TackOutCount / 3 * 4 == iCardCount)
				return IsSequence(iBuffer, TackOutCount); //2011-6-28 修改333444请允许带55
			//&& (TackOutCount/3==GetCountBySpecifyNumCount(iCardList,iCardCount,1));//沈阳要求333444不能带55;
			break;
		case 2://带二单
			TackOutCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, 3);
			if (TackOutCount>0 && TackOutCount / 3 * 5 == iCardCount)
				return IsSequence(iBuffer, TackOutCount);
		case 3://带对
			TackOutCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, 3);
			if (TackOutCount>0 && TackOutCount / 3 * 5 == iCardCount
				&&GetCountBySpecifyNumCount(iCardList, iCardCount, 2) == TackOutCount / 3)
				return IsSequence(iBuffer, TackOutCount);

			break;
		}
		return false;
	}

	//四顺带　
	bool CUpGradeGameLogic::IsFourXSequence(BYTE iCardList[], int iCardCount, int iSeqX)
	{
		BYTE bIsLuck[5] = { 0 };
		if (iCardCount < 8)		//四顺至少2
			return false;

		BYTE iBuffer[54];
		int TackOutCount = 0;
		switch (iSeqX)
		{
		case 0:
			if (iCardCount % 4 != 0)
				return false;
			return IsSequence(iCardList, iCardCount);
			break;

		case 1://带单张
			TackOutCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, 4);
			if (TackOutCount>0 && TackOutCount / 4 * 5 == iCardCount)
				return IsSequence(iBuffer, TackOutCount);
			break;

		case 2://带二张(可以非对子）
			TackOutCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, 4);
			if (TackOutCount>0 && TackOutCount / 4 * 6 == iCardCount)
				return IsSequence(iBuffer, TackOutCount);

		case 3://带一对
			TackOutCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, 4);
			if (TackOutCount>0 && TackOutCount / 4 * 6 == iCardCount
				&&TackOutCount / 4 == GetBulkBySpecifyCardCount(iCardList, iCardCount, 2))
				return IsSequence(iBuffer, TackOutCount);

		case 4://(带二对）
			TackOutCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iBuffer, 4);
			if (TackOutCount>0 && TackOutCount / 4 * 6 == iCardCount
				&&TackOutCount / 2 == GetBulkBySpecifyCardCount(iCardList, iCardCount, 2))
				return IsSequence(iBuffer, TackOutCount);
			break;
		}
		return false;
	}

	//提取单个的三带0, 1 or 2 到底带的是几,由 iBaseCount-3 来决定
	BYTE CUpGradeGameLogic::TackOutThreeX(BYTE iCardList[], int iCardCount,
		BYTE iBaseCard[], int iBaseCount,
		BYTE iResultCard[], int &iResultCount, int iValue)
	{
		iResultCount = 0;
		if (iCardCount<iBaseCount)
			return false;
		BYTE iTempCard[54];
		int threecard = GetBulkBySpecifyCardCount(iBaseCard, iBaseCount, 3);//桌面牌三张的点数
		//3张牌总个数
		BYTE iCount = TackOutBySepcifyCardNumCount(iCardList, iCardCount, iTempCard, 3);

		if (iCount > 0)//提取大于桌面的三条
		{
			BYTE byCardTemp = 0x00;
			for (int i = 0; i<iBaseCount; ++i)
			{
				if (threecard == GetCardBulk(iBaseCard[i]))
				{
					byCardTemp = iBaseCard[i];
					break;
				}
			}
			if (0x00 == byCardTemp)
			{
				return false;
			}

			BYTE Step = GetSerialByMoreThanSpecifyCard(iTempCard, iCount, byCardTemp, 3, true);//牌面值进去
			//if(Step == 0)
			//	return FALSE;
			::memcpy(iResultCard, &iTempCard[Step], sizeof(BYTE)* 3);
			//TCHAR sz[200];
			//wsprintf(sz,"Step=%d,iBaseCount=%d",Step,iBaseCount);
			//WriteStr(sz,7,7);

			//if(CompareOnlyOne(iBaseCard[0], iResultCard[0]))			//由于传过来的step可能为0得进行一次比较处理
			if (threecard >= GetBulkBySpecifyCardCount(iResultCard, 3, 3))
				return false;
			//iResultCount = 3;
			//else
			//	return FALSE;
		}
		else
			return false;
		//将原值移走
		BYTE Tmp[54];
		int iTempCount = iCardCount;
		::memcpy(Tmp, iCardList, sizeof(BYTE)*iCardCount);
		RemoveCard(iResultCard, 3, Tmp, iTempCount);
		iTempCount -= 3;
		int destCount = iBaseCount - 3;
		//	TCHAR sz[200];
		//	wsprintf(sz,"iValue=%d,destCount=%d",iValue,destCount);
		//	WriteStr(sz,8,8);
		switch (iValue)
		{
		case 1:
		case 2:
		{
				  iCount = TackOutBySepcifyCardNumCount(Tmp, iTempCount, iTempCard, 1);
				  if (iCount >= destCount)//查找到单牌
				  {
					  memcpy(&iResultCard[3], iTempCard, sizeof(BYTE)*destCount);
					  iResultCount = iBaseCount;
					  break;
				  }
				  //拆对来补单牌
				  iCount = TackOutBySepcifyCardNumCount(Tmp, iTempCount, iTempCard, 2);
				  if (iCount >= destCount)
				  {
					  memcpy(&iResultCard[3], iTempCard, sizeof(BYTE)*destCount);
					  iResultCount = iBaseCount;
					  break;
				  }

				  //拆三张来补单牌
				  iCount = TackOutBySepcifyCardNumCount(Tmp, iTempCount, iTempCard, 3);
				  if (iCount < 3)//仅一三张无法拆
					  break;
				  memcpy(&iResultCard[3], iTempCard, sizeof(BYTE)*destCount);
				  iResultCount = iBaseCount;
				  break;
		}
		case 3:
		{
				  iCount = TackOutBySepcifyCardNumCount(Tmp, iTempCount, iTempCard, 2);
				  if (iCount > 0)
				  {
					  memcpy(&iResultCard[3], iTempCard, sizeof(BYTE)*destCount);
					  iResultCount = iBaseCount;
					  break;
				  }
				  //拆三张来补单牌
				  iCount = TackOutBySepcifyCardNumCount(Tmp, iTempCount, iTempCard, 3);
				  if (iCount < 3)//仅一三张无法拆
					  break;
				  memcpy(&iResultCard[3], iTempCard, sizeof(BYTE)*destCount);
				  iResultCount = iBaseCount;
				  break;

		}
		default:
			iResultCount = 0;
			break;
		}
		//		wsprintf(sz,"iResultCount=%d,iBaseCount=%d",iResultCount,iBaseCount);
		//	WriteStr(sz,8,8);
		if (iResultCount == iBaseCount)
			return  true;
		iResultCount = 0;
		return false;
		//return (iResultCount == iBaseCount);
	}
	//提取蝴蝶
	bool  CUpGradeGameLogic::TrackOut3Sequence2Sequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount)
	{
		iResultCardCount = 0;
		if (iCardCount < iBaseCount)	//张数不够
			return false;
		BYTE tmpBaseCard[54];//,destCard[54];
		int tmpbaseCardCount = 0, destCardCount = 0;
		//将桌面牌的三条分离出来
		tmpbaseCardCount = TackOutBySepcifyCardNumCount(iBaseCard, iBaseCount, tmpBaseCard, 3);
		if (tmpbaseCardCount < 6)	//至少六张以上
			return false;
		//先提取比桌面大的三顺
		if (!TackOutSequence(iCardList, iCardCount, tmpBaseCard, tmpbaseCardCount, iResultCard, iResultCardCount, 3))
			return false;
		//将手牌复制一份(移除三顺牌)
		BYTE TMP[54];
		int TmpCount = iCardCount;
		::memcpy(TMP, iCardList, sizeof(BYTE)*iCardCount);
		RemoveCard(iResultCard, iResultCardCount, TMP, TmpCount);
		TmpCount -= iResultCardCount;
		destCardCount = iBaseCount - iResultCardCount;	//补牌数量

		BYTE twoList[54];
		int twoCount;
		//将桌面牌的二顺分离出来
		tmpbaseCardCount = TackOutBySepcifyCardNumCount(iBaseCard, iBaseCount, tmpBaseCard, 2);
		if (!TackOutSequence(TMP, TmpCount, tmpBaseCard, tmpbaseCardCount, twoList, twoCount, 2, true))
			return false;
		//	int TwoSequenceLen = (iBaseCount- tmpbaseCardCount)/2;
		//	tmpbaseCardCount =TackOutBySepcifyCardNumCount(TMP,TmpCount,tmpBaseCard,3);
		::memcpy(&iResultCard[iResultCardCount], twoList, sizeof(BYTE)*twoCount);
		iResultCardCount += twoCount;
		return true;
	}
	//提取指定三条带顺
	bool  CUpGradeGameLogic::TrackOut3XSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int xValue)
	{
		iResultCardCount = 0;
		if (iCardCount < iBaseCount)	//张数不够
			return false;
		BYTE tmpBaseCard[54];//,destCard[54];
		int tmpbaseCardCount = 0, destCardCount = 0;
		//将桌面牌的三条分离出来
		tmpbaseCardCount = TackOutBySepcifyCardNumCount(iBaseCard, iBaseCount, tmpBaseCard, 3);
		if (tmpbaseCardCount < 6)	//至少六张以上
			return false;
		//TCHAR sz[200];
		//wsprintf(sz,"三顺子提取之前%d",iResultCardCount);
		//WriteStr(sz);	
		//先提取比桌面大的三顺
		if (!TackOutSequence(iCardList, iCardCount, tmpBaseCard, tmpbaseCardCount, iResultCard, iResultCardCount, 3))
			return false;
		//TCHAR sz[200];
		//wsprintf(sz,"三顺子提取成功%d",iResultCardCount);
		//WriteStr(sz);
		//将手牌复制一份
		BYTE TMP[54];
		int TmpCount = iCardCount;
		::memcpy(TMP, iCardList, sizeof(BYTE)*iCardCount);
		RemoveCard(iResultCard, iResultCardCount, TMP, TmpCount);
		TmpCount -= iResultCardCount;
		destCardCount = iBaseCount - iResultCardCount;	//补牌数量

		switch (xValue)
		{
		case 1:
		case 2:
		{
				  tmpbaseCardCount = TackOutBySepcifyCardNumCount(TMP, TmpCount, tmpBaseCard, 1);//凑单牌
				  if (tmpbaseCardCount >= destCardCount)
				  {
					  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*destCardCount);//够单
					  iResultCardCount += destCardCount;
				  }
				  else
				  {
					  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*tmpbaseCardCount);
					  iResultCardCount += tmpbaseCardCount;
					  destCardCount -= tmpbaseCardCount;
					  tmpbaseCardCount = TackOutBySepcifyCardNumCount(TMP, TmpCount, tmpBaseCard, 2);//用对牌补
					  if (tmpbaseCardCount >= destCardCount)
					  {
						  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*destCardCount);
						  iResultCardCount += destCardCount;
					  }
					  else
					  {
						  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*tmpbaseCardCount);
						  iResultCardCount += tmpbaseCardCount;
						  destCardCount -= tmpbaseCardCount;
						  tmpbaseCardCount = TackOutBySepcifyCardNumCount(TMP, TmpCount, tmpBaseCard, 3);//用三条补
						  //
						  if (tmpbaseCardCount >= destCardCount)
						  {
							  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*destCardCount);
							  iResultCardCount += destCardCount;
						  }
					  }
				  }
				  break;
		}
		case 3:
		{
				  tmpbaseCardCount = TackOutBySepcifyCardNumCount(TMP, TmpCount, tmpBaseCard, 2);//凑对牌
				  if (tmpbaseCardCount >= destCardCount)
				  {
					  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*destCardCount);
					  iResultCardCount += destCardCount;
				  }
				  else
				  {
					  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*tmpbaseCardCount);
					  iResultCardCount += tmpbaseCardCount;
					  destCardCount -= tmpbaseCardCount;
					  //tmpbaseCardCount =TackOutBySepcifyCardNumCount(TMP,TmpCount,tmpBaseCard,3);//用三条补对
					  TackOutMuchToFew(TMP, TmpCount, tmpBaseCard, tmpbaseCardCount, 3, 2);	//将手中三条拆成对来配
					  if (tmpbaseCardCount >= destCardCount)//三条拆对够补
					  {
						  ::memcpy(&iResultCard[iResultCardCount], tmpBaseCard, sizeof(BYTE)*destCardCount);
						  iResultCardCount += destCardCount;
					  }
				  }
				  break;
		}
		default:
			break;
		}
		//wsprintf(sz,"iResultCardCount=%d,iBaseCount=%d",iResultCardCount,iBaseCount);
		//WriteStr(sz);
		if (iResultCardCount == iBaseCount)
			return true;
		iResultCardCount = 0;
		return false;
		return (iResultCardCount == iBaseCount);
		//	return FALSE;
	}


	//重写提取单张的顺子,连对 or 连三
	bool CUpGradeGameLogic::TackOutSequence(BYTE iCardList[], int iCardCount, //手中的牌
		BYTE iBaseCard[], int iBaseCount,   //桌面上最大的牌, 牌的个数
		BYTE iResultCard[], int &iResultCount, //找到的牌
		int xSequence, bool bNoComp)							//顺子的个数
	{
		iResultCount = 0;
		BYTE iTack[54];
		int iTackCount = iCardCount;
		//复制一份
		::memcpy(iTack, iCardList, sizeof(BYTE)*iCardCount);
		BYTE iBuffer[54];
		int iBufferCount = 0;
		int iBaseStart, iDestStart = 0, iDestEnd = 0;
		int iSequenceLen = iBaseCount;
		int temp[18] = { 0 };
		int num = 0;
		//提取所有炸弹(从手中删除所有炸弹)
		TackOutAllBomb(iTack, iTackCount, iBuffer, iBufferCount);
		RemoveCard(iBuffer, iBufferCount, iTack, iTackCount);
		iTackCount -= iBufferCount;
		//进行一次系统序例化处理(按牌形排序，小->大测试
		SortCard(iTack, NULL, iTackCount, true);
		//用缓冲队例保存
		for (int i = 0; i<iTackCount; i++)
		{
			temp[GetCardBulk(iTack[i])]++;
		}

		switch (xSequence)
		{
			//单顺
		case 1:
			iSequenceLen = iBaseCount;
			if (!bNoComp)
				iBaseStart = GetSequenceStartPostion(iBaseCard, iBaseCount, 1);
			else
				iBaseStart = 2;
			for (int i = iBaseStart + 1; i<15; i++)
			{
				if (temp[i] >= 1)
				{
					if (iDestStart == 0)
						iDestStart = i;
					iDestEnd++;
					if (iDestEnd == iSequenceLen)
						break;
				}
				else
				{
					iDestStart = 0;
					iDestEnd = 0;
				}
			}
			if (iDestEnd != iSequenceLen)
				return false;
			//提取队列
			for (int j = 0; j<iTackCount; j++)
			{
				if (GetCardBulk(iTack[j]) == iDestStart)//找到一张牌
				{
					iResultCard[iResultCount++] = iTack[j];
					iDestStart++;
					iDestEnd--;
					//break;
				}
				//已经找全
				if (iDestEnd == 0)
				{
					return true;
				}
			}
			break;
		case 2:
			iSequenceLen = iBaseCount / 2;
			if (!bNoComp)
				iBaseStart = GetSequenceStartPostion(iBaseCard, iBaseCount, 2);
			else
				iBaseStart = 2;
			for (int i = iBaseStart + 1; i<15; i++)
			{
				if (temp[i] >= 2)
				{
					if (iDestStart == 0)
						iDestStart = i;
					iDestEnd++;
					if (iDestEnd == iSequenceLen)
						break;
				}
				else
				{
					iDestStart = 0;
					iDestEnd = 0;
				}
			}
			if (iDestEnd != iSequenceLen)
				return false;
			num = 0;
			//提取队列
			for (int j = 0; j<iTackCount; j++)
			{
				if (GetCardBulk(iTack[j]) == iDestStart)
				{
					iResultCard[iResultCount++] = iTack[j];
					num++;
				}

				if (num == 2)//一对已经找到
				{
					num = 0;
					iDestStart++;
					iDestEnd--;
					//已经找全
					if (iDestEnd == 0)
						return true;
					//break;
					//i = 0;
					//continue;
				}
			}
			break;
		case 3:
			iSequenceLen = iBaseCount / 3;
			if (!bNoComp)
				iBaseStart = GetSequenceStartPostion(iBaseCard, iBaseCount, 3);
			else
				iBaseStart = 2;
			for (int i = iBaseStart + 1; i<15; i++)
			{
				if (temp[i] >= 3)
				{
					if (iDestStart == 0)
						iDestStart = i;
					iDestEnd++;
					if (iDestEnd == iSequenceLen)
						break;
				}
				else
				{
					iDestStart = 0;
					iDestEnd = 0;
				}
			}
			if (iDestEnd != iSequenceLen)
				return false;
			num = 0;
			//提取队列
			for (int j = 0; j<iTackCount; j++)
			{
				if (GetCardBulk(iTack[j]) == iDestStart)
				{
					iResultCard[iResultCount++] = iTack[j];
					num++;

					if (num == 3)//找到三张
					{
						num = 0;
						iDestStart++;
						iDestEnd--;
						//已经找全
						if (iDestEnd == 0)
							return true;
					}
				}
			}

			break;
		}
		return false;
	}

	//提取同花順
	bool CUpGradeGameLogic::TackOutStraightFlush(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount)
	{
		iResultCardCount = 0;
		if (iCardCount < iBaseCount)
			return false;
		BYTE iBaseMinCard = GetBulkBySepcifyMinOrMax(iBaseCard, iBaseCount, 1);//桌面的顺子中最小的牌
		BYTE iTack[54];
		int iTackCount = iCardCount;
		//复制一份
		::memcpy(iTack, iCardList, sizeof(BYTE)*iCardCount);
		BYTE iBuffer[54];
		int iBufferCount = 0;
		int iDestStart = 0, iDestEnd = 0;
		int iSequenceLen = iBaseCount;
		int temp[18] = { 0 };
		int num = 0;
		//提取所有炸弹(从手中删除所有炸弹)
		TackOutAllBomb(iTack, iTackCount, iBuffer, iBufferCount);
		RemoveCard(iBuffer, iBufferCount, iTack, iTackCount);
		iTackCount -= iBufferCount;

		SortCard(iTack, NULL, iTackCount, true);

		BYTE iTempKind[54];
		int iTempKindCount = 0;
		//TCHAR sz[200];
		//wsprintf(sz,"iTackCount=%d,iBaseCount=%d",iTackCount,iBaseCount);
		//WriteStr(sz);
		//用缓冲队例保存
		for (int kind = 0; kind <= 48; kind += 16)
		{	//提取方块
			iResultCardCount = 0;
			iTempKindCount = TackOutByCardKind(iTack, iTackCount, iTempKind, kind);
			//wsprintf(sz,"kind=%d,iTempKindCount=%d",kind,iTempKindCount);
			//WriteStr(sz);
			if (iTempKindCount >= iBaseCount)					//大于桌面
			{
				for (int i = 0; i < iTempKindCount; i++)
				{
					temp[GetCardBulk(iTempKind[i])]++;
				}

				for (int i = iBaseMinCard + 1; i<15; i++)//对队例进行遍历
				{
					if (temp[i] >= 1)		//某花色有牌
					{
						if (iDestStart == 0)
							iDestStart = i;
						iDestEnd++;
						if (iDestEnd == iSequenceLen)
							break;
					}
					else
					{
						iDestStart = 0;
						iDestEnd = 0;
					}
				}
				//wsprintf(sz,"iDestEnd=%d,iCardCount=%d",iDestEnd,iCardCount);
				//WriteStr(sz);

				if (iDestEnd != iBaseCount)	//某种花色不符合,换另外一种花色
					continue;
				//提取队列
				for (int j = 0; j<iTempKindCount; j++)
				{
					if (GetCardBulk(iTempKind[j]) == iDestStart)
					{
						iResultCard[iResultCardCount++] = iTempKind[j];
						iDestStart++;
						iDestEnd--;
					}
					//已经找全
					if (iDestEnd == 0)
						return true;
				}
			}

		}

		return false;
	}

	//得到顺子的起始位置
	int CUpGradeGameLogic::GetSequenceStartPostion(BYTE iCardList[], int iCardCount, int xSequence)
	{
		BYTE temp[18] = { 0 };
		int Postion = 0;
		for (int i = 0; i<iCardCount; i++)
		{
			temp[GetCardBulk(iCardList[i])]++;
		}

		for (int i = 0; i<18; i++)
		{
			if (temp[i] == xSequence)
				return i;
		}
		return Postion;
	}

	//提取所有炸弹为提反单顺,双顺,三顺做准备
	bool CUpGradeGameLogic::TackOutAllBomb(BYTE iCardList[], int iCardCount,
		BYTE iResultCard[], int &iResultCardCount, int iNumCount)
	{
		iResultCardCount = 0;
		BYTE bCardBuffer[54];
		BYTE bombcount = GetBombCount(iCardList, iCardCount, iNumCount);
		if (bombcount<0)
			return false;
		for (int i = iNumCount; i < 9; i++)
		{
			int count = TackOutBySepcifyCardNumCount(iCardList, iCardCount, bCardBuffer, i);
			if (count > 0)
			{
				::memcpy(&iResultCard[iResultCardCount], bCardBuffer, sizeof(BYTE)*count);
				iResultCardCount += count;
				break;
			}
		}
		return true;
	}

	//提取炸弹
	bool CUpGradeGameLogic::TackOutBomb(BYTE iCardList[], int iCardCount,
		BYTE iResultCard[], int &iResultCardCount, int iNumCount)
	{
		iResultCardCount = 0;
		BYTE bCardBuffer[54];
		//BYTE bombcount = GetBombCount(iCardList, iCardCount, iNumCount);
		//if (bombcount<0)
		//	return false;
		//for (int i = iNumCount; i<9; i++)
		{
			int count = TackOutBySepcifyCardNumCount(iCardList, iCardCount, bCardBuffer, 4);
			if (count > 0)
			{
				::memcpy(iResultCard, bCardBuffer, sizeof(iResultCard));
				iResultCardCount = count;
				//break;
			}
		}
		if (iResultCardCount == 0)
			TackOutKingBomb(iCardList, iCardCount, iResultCard, iResultCardCount);
		return true;
	}

	//提取王炸
	bool CUpGradeGameLogic::TackOutKingBomb(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount)
	{
		iResultCardCount = 0;

		BYTE bCardBuf[8];
		int kingcount = 0;
		int SingKing = KING_COUNT / 2;
		int count = TackOutBySpecifyCard(iCardList, iCardCount, bCardBuf, kingcount, 0x6e);
		if (count != SingKing)
			return false;

		::memcpy(iResultCard, bCardBuf, sizeof(BYTE)*count);

		count = TackOutBySpecifyCard(iCardList, iCardCount, bCardBuf, kingcount, 0x6f);
		if (count != SingKing)
		{
			return false;

		}
		::memcpy(&(iResultCard[SingKing]), bCardBuf, sizeof(BYTE)*count);
		return iResultCardCount == KING_COUNT;
	}

	//找出一个最小或最大的牌
	BYTE CUpGradeGameLogic::GetCardMinOrMax(BYTE iCardList[], int iCardCount, int MinOrMax/*1 or 255*/, bool bExtVal)
	{
		int nIndex = 0;
		int CardNum;

		if (MinOrMax == 1) //找最小的
		{
			CardNum = 65536;
			for (int i = 0; i < iCardCount; i++)
			{
				// 不考虑 2 、王
				if (bExtVal && (2 == GetCardNum(iCardList[i]) || 0x6E == iCardList[i] || 0x6F == iCardList[i]))
					continue;

				if (GetCardBulk(iCardList[i], false) < CardNum)
				{
					CardNum = GetCardBulk(iCardList[i], false);
					nIndex = i;
				}
			}
		}
		else if (MinOrMax == 255)
		{
			CardNum = -1;
			for (int i = 0; i < iCardCount; i++)
			{
				if (bExtVal && (2 == GetCardNum(iCardList[i]) || 0x6E == iCardList[i] || 0x6F == iCardList[i]))
					continue;

				if (GetCardBulk(iCardList[i], false) > CardNum)
				{
					CardNum = GetCardBulk(iCardList[i], false);
					nIndex = i;
				}
			}
		}

		if (bExtVal && (65536 == CardNum || -1 == CardNum))
			return 255;

		return iCardList[nIndex];
	}

	/*
	@brief:获取指定牌中的对子个数
	@param:iCardList，牌数组，iCardCount，牌数组长度,bIsLuck,是否被选为对子标志，bIsKing 王是否可以替代任何牌
	@return:void
	*/
	BYTE CUpGradeGameLogic::GetDoubleNums(BYTE iCardList[], int iCardCount, bool bExtVal)
	{
		BYTE bTempList[18] = { 0 }, bTempDouble = 0;
		memset(bTempList, 0, sizeof(bTempList));
		//获取扑克牌对应的数字(除大小王之外)
		int k = 0;
		for (int i = 0; i<iCardCount; i++)
		{
			if (GetCardIsCaiShen(iCardList[i]) || 0x00 == iCardList[i])
			{
				continue;
			}
			bTempList[GetCardNum(iCardList[i])]++;
		}
		for (int i = 0; i<18; i++)
		{
			if (2 == bTempList[i])
			{
				bTempDouble++;
			}
		}
		return bTempDouble;
	}
	/*
	@brief:获取指定牌中的相同牌的最大个数
	@param:iCardList，牌数组，iCardCount，牌数组长度,bIsLuck 被选中的牌置1，bIsKing，是否有王
	@return:对子个数
	*/
	int CUpGradeGameLogic::GetSameCardMaxNums(BYTE iCardList[], int iCardCount)
	{
		INT newCount = 0;
		BYTE newCards[13] = { 0 };
		bool isFourStrip = FindFourStrip(iCardList, iCardCount, newCards, newCount);
		if (isFourStrip)
		{
			return 4;
		}
		else
		{
			return 1;
		}
	}
	//获得牌型
	int CUpGradeGameLogic::GetCardStyle(BYTE iCardList[], int iCardCount)
	{
		BYTE bTempList[18] = { 0 };
		memcpy(bTempList, iCardList, sizeof(BYTE)*iCardCount);
		INT newCount = 0;
		BYTE newCards[13] = { 0 };
		//bool isSameFlower = FindSameFlower(iCardList, iCardCount, newCards, newCount);
		if (FindFiveStrip(iCardList, iCardCount, newCards, newCount)) return SHAPE_STRIP_FIVE;		//五同
		if (IsStraightFlush(iCardList, iCardCount)) return SHAPE_STRAIGHT_FLUSH;		//同花顺
		if (FindFourStrip(iCardList, iCardCount, newCards, newCount)) return SHAPE_STRIP_SI;		//四条
		if (IsThreeX(iCardList, iCardCount, 3, false))	return SHAPE_THREE_DOUBLE;		//三带对(葫芦)
		if (IsSameHuaKind(iCardList, iCardCount)) return SHAPE_FLOWER;					//同花色	
		if (IsStraight(iCardList, iCardCount, false)) return SHAPE_STRAIGHT;			//顺子
		if (FindThreeStrip(iCardList, iCardCount, newCards, newCount)) return SHAPE_STRIP_SAN;		//三条
		if (2 == GetDoubleNums(iCardList, iCardCount)) return SHAPE_DOUBLE_LIANG;		//两对
		if (FindOneDouble(iCardList, iCardCount, newCards, newCount)) return SHAPE_DOUBLE_YI;			//一对
		return SHAPE_HIGH_CARD;															//高牌
	}
	/*比较两手中牌的大小
	@param:wFirstCard[] 第一手牌集
	@param: iFirstCount  第一手牌张数
	@param: WwSecondCard[] 第二手牌集
	@param: iSecondCount  第二手牌张数
	@return: 是否大过
	*/
	int CUpGradeGameLogic::CompareCard(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		BYTE byFirCard[5], bySecCard[5];
		memset(byFirCard, 0, sizeof(byFirCard));
		memset(bySecCard, 0, sizeof(bySecCard));

		//复制第一手牌
		for (int i = 0x00; i < iFirstCount; i++)
		{
			byFirCard[i] = byFirstCard[i];
		}
		//复制第一手牌
		for (int i = 0x00; i < iSecondCount; i++)
		{
			bySecCard[i] = bySecondCard[i];
		}

		ReSortCard(byFirCard, iFirstCount);			//对牌进行排序(从大到小)
		ReSortCard(bySecCard, iSecondCount);			//对牌进行排序(从大到小)

		int iFirstCardShape = GetCardStyle(byFirCard, iFirstCount);
		int iSecondCardShape = GetCardStyle(bySecCard, iSecondCount);

		if (iFirstCardShape != iSecondCardShape)       //牌型不相同
		{
			return (iFirstCardShape - iSecondCardShape > 0x00) ? 1 : -1;
		}
		else                                          //牌型相同
		{
			switch (iFirstCardShape)
			{
			case SHAPE_STRAIGHT_FLUSH:        //同花顺,花色相同的顺子
			{
			  return CmpStraightFlush(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_STRIP_SI:	         //炸弹
			{
				return CmpBomb(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_THREE_DOUBLE:		//三带二(葫芦)
			{
				return CmpThreeDouble(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_FLOWER:				//同花
			{
				return CmpFlush(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_STRAIGHT:			//顺子
			{
				return CmpStraight(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_STRIP_SAN:			//三张
			{
				return CmpThree(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_DOUBLE_LIANG:		//两对对子
			{
				return CmpTwoDouble(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_DOUBLE_YI:			//对子
			{
				return CmpDouble(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_HIGH_CARD:			//单张
			{
				return CmpOne(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			case SHAPE_STRIP_FIVE:			//五同
			{
				return CmpFive(byFirCard, iFirstCount, bySecCard, iSecondCount);
			}
			default:
				break;
			}
		}

		enum cardShape
		{
			SHAPE_HIGH_CARD = 0,			//高牌
			SHAPE_DOUBLE_YI = 1,			//一对
			SHAPE_DOUBLE_LIANG = 2,			//两对
			SHAPE_STRIP_SAN = 3,			//三条
			SHAPE_STRAIGHT_SMALL = 4,		//最小顺子
			SHAPE_STRAIGHT = 5,				//顺子
			SHAPE_FLOWER = 6,				//同花
			SHAPE_THREE_DOUBLE = 7,			//葫芦
			SHAPE_STRIP_SI = 8,				//炸弹
			SHAPE_STRAIGHT_FLUSH_SMALL = 9,	//最小同花顺
			SHAPE_STRAIGHT_FLUSH = 10		//同花顺
		};

		return -1;
	}
	/*得到手牌中最大的牌
	@param:wCardList[] 牌集
	@param:iCardCount 牌张数
	@return:int最大牌
	*/
	BYTE CUpGradeGameLogic::GetMaxCard(BYTE  wCardList[], int iCardCount)
	{
		BYTE bTempCount[16], bMaxCard = 0, temp = 0;
		//vector<BYTE> bMaxContain;
		//bMaxContain.clear();
		//memset(bTempCount,0,sizeof(bTempCount));
		temp = GetCardNum(wCardList[0]);
		bMaxCard = wCardList[0];
		//按牌值找最大牌
		for (int i = 0; i < iCardCount; i++)
		{
			if (GetCardNum(wCardList[i])>temp)
			{
				bMaxCard = wCardList[i];
				temp = GetCardNum(wCardList[i]);
			}
		}
		return bMaxCard;
		//if(bMaxCard>0)//比较花色
		//{
		//	for (int i = 0; i < iCardCount; i++)
		//	{
		//		temp=GetCardNum(wCardList[i]);
		//		if(bMaxCard==temp)
		//		{
		//			bMaxContain.push_back(wCardList[i]);
		//		}
		//	}
		//	if(bMaxContain.size()>1)
		//	{
		//		bMaxCard=0;
		//		for(int i=0;i<bMaxContain.size();i++)
		//		{
		//			temp=GetCardHuaKind(bMaxContain.at(i));
		//			bMaxCard=(temp>bMaxCard)?temp:bMaxCard;
		//		}
		//		return bMaxCard;
		//	}
		//	else
		//	{
		//		return bMaxCard;
		//	}
		//}
	}

	//查找同花顺
	bool CUpGradeGameLogic::FindSameFlowerFlush(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int &iResultCardCount)
	{
		BYTE bTempList[7];		
		memset(bTempList, 0, sizeof(bTempList));
		
		if (iHandCardCount<5)
		{
			return false;
		}
		//计算同花个数
		for (int i = 0; i<iHandCardCount; i++)
		{
			if (!GetCardIsCaiShen(iHandCard[i]))
			{
				bTempList[GetCardHuaKind(iHandCard[i])]++;
			}
			
		}
		BYTE iCardResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);

		BYTE iHuasultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuasultList);

		int iSameMax[5] = { 0 };//同花最大个数
		int iFlower[5] = { 0 };//选中的花色
		int iFlowerCount = 0;
		bool result = false;
		for (int k = 0; k < iHandCardCount;k++)
		{
			if (!GetCardIsCaiShen(iHandCard[k]))
			{
				auto maxHua = GetCardHuaKind(iHandCard[k]);
				if(bTempList[maxHua] + iKingCount + iHuaCount >= 5) //如果数量大于5
				{
					iFlower[0] = maxHua;
					result = extractSameFlowerFlush(iFlower[0], iHandCard, iHandCardCount, iResultCard, iResultCardCount);
					if (result)
					{
						return true;
					}
				}
			}
		}
		return result;

		if (iFlowerCount == 0)
		{
			//OutputDebugString("lwlog::同花个数太少");
			return false;
		}

		
		for (int i = 0; i < iFlowerCount; i++)
		{
			result = extractSameFlowerFlush(iFlower[i], iHandCard, iHandCardCount, iResultCard, iResultCardCount);
			if (result) break;			
		}
		
		return result;	
	}

	//提取同花顺
	bool CUpGradeGameLogic::extractSameFlowerFlush(int iFlower, BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int &iResultCardCount)
	{
		iResultCardCount = 0;
		std::vector<BYTE> vSameFlowerList;
		vSameFlowerList.clear();
		//提取出选中花色的扑克
		for (int i = 0; i < iHandCardCount; i++)
		{
			if (iFlower == GetCardHuaKind(iHandCard[i]) && !GetCardIsCaiShen(iHandCard[i]))
			{
				vSameFlowerList.push_back(iHandCard[i]);
			}
		}
		BYTE bSameFlowerList[17];
		memset(bSameFlowerList, 0, sizeof(bSameFlowerList));
		for (int i = 0; i < vSameFlowerList.size(); i++)
		{
			bSameFlowerList[GetCardNum(vSameFlowerList.at(i))]++;
		}
		BYTE iCardResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);

		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList);

		//从同花中找顺子
		if (bSameFlowerList[14] != 0 || (m_bKingCanReplace && (iKingCount+iHuaCount>0)))//如果有A
		{
			//1:先判断是否存在 10 J Q K A,有则提取
			bool bFlag = true;
			int num = 0;
			//继续扫描
			for (int j = 10; j < 15; j++)
			{
				if (1 <= bSameFlowerList[j])
				{
					num++;
				}
			}
			bFlag = num >= 5 ? true : false;
			if (m_bKingCanReplace&& iKingCount + iHuaCount>0 && num>1/*四张财神就是同花顺了*/)
			{
				bFlag = num + iKingCount + iHuaCount>= 5 ? true : false;
			}
			if (bFlag)
			{
				for (int k = 14; k >= 10; k--)
				{
					for (int t = 0; t < vSameFlowerList.size(); t++)
					{
						if (k == GetCardNum(vSameFlowerList.at(t)))
						{
							iResultCard[iResultCardCount] = vSameFlowerList.at(t);
							iResultCardCount++;
							break;
						}
					}
				}
				if (m_bKingCanReplace && iKingCount>0 && iResultCardCount<5)
				{
					iResultCard[iResultCardCount] = iCardResultList[iKingCount - 1];
					iKingCount--;
					iResultCardCount++;
					if (iKingCount>0 && iResultCardCount<5)
					{
						iResultCard[iResultCardCount] = iCardResultList[iKingCount - 1];
						iKingCount--;
						iResultCardCount++;
					}
				}
				if (iKingCount <= 0 && iHuaCount>0 && iResultCardCount < 5 && m_bKingCanReplace)
				{
					int oldHuaCount = iHuaCount;
					for (int i = 0; i < oldHuaCount; i++)
					{
						if (iResultCardCount < 5)
						{
							iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
							iHuaCount--;
							iResultCardCount++;
						}
					}
				}
				return true;
			}

			//2:再判断是否存在 A 2 3 4 5,有则提取
			bFlag = true;
			int aToFiveNum = 0;
			//继续扫描
			for (int j = 2; j < 6; j++)
			{
				if (1 <= bSameFlowerList[j])
				{
					aToFiveNum++;
				}
			}
			//是否有A
			int Anum = 0;
			for (int t = 0; t < vSameFlowerList.size(); t++)
			{
				if (14 == GetCardNum(vSameFlowerList.at(t)))
				{
					Anum++;
					break;
				}
			}
			bFlag = aToFiveNum + Anum >= 5 ? true : false;
			if (m_bKingCanReplace && iKingCount + iHuaCount>0 && aToFiveNum + Anum>1/*四张财神就是同花顺了*/)
			{
				bFlag = aToFiveNum + Anum + iKingCount + iHuaCount >= 5 ? true : false;
			}
			if (bFlag)
			{
				for (int t = 0; t < vSameFlowerList.size(); t++)
				{
					if (14 == GetCardNum(vSameFlowerList.at(t)))
					{
						iResultCard[4] = vSameFlowerList.at(t);
						iResultCardCount++;
						break;
					}
					if (t == vSameFlowerList.size()-1 && iKingCount>0 && m_bKingCanReplace)
					{
						iResultCard[4] = iCardResultList[iKingCount - 1];
						iKingCount--;
						iResultCardCount++;
						break;
					}
					if (t == vSameFlowerList.size() - 1 && iHuaCount > 0 && m_bKingCanReplace)
					{
						iResultCard[4] = iHuaResultList[iHuaCount - 1];
						iHuaCount--;
						iResultCardCount++;
						break;
					}
				}

				int index = 0;
				for (int k = 5; k >= 2; k--)
				{
					for (int t = 0; t < vSameFlowerList.size(); t++)
					{
						if (k == GetCardNum(vSameFlowerList.at(t)))
						{
							iResultCard[index] = vSameFlowerList.at(t);
							index++;
							iResultCardCount++;
							break;
						}
					}
				}
				if (m_bKingCanReplace&& iKingCount>0 && iResultCardCount<5)
				{
					iResultCard[index] = iCardResultList[iKingCount - 1];
					index++;
					iResultCardCount++;
					iKingCount--;
					if (iResultCardCount<5 && iKingCount>0)
					{
						iResultCard[index] = iCardResultList[iKingCount - 1];
						index++;
						iResultCardCount++;
						iKingCount--;
					}
				}
				if (m_bKingCanReplace&& iKingCount <= 0 && iResultCardCount < 5 && iHuaCount>0)
				{
					int oldHuaCount = iHuaCount;
					for (int i = 0; i < oldHuaCount;i++)
					{
						if (iResultCardCount<5)
						{
							iResultCard[index] = iHuaResultList[iHuaCount - 1];
							index++;
							iResultCardCount++;
							iHuaCount--;
						}
					}
				}
				return true;
			}
		}
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);
		for (int i = 17; i >= 0; i--)
		{
			if (bSameFlowerList[i] != 0)//有值
			{
				int j = i;
				bool bFlag = true;
				//继续扫描
				int num = 0;
				for (; j < (i + 5); j++)
				{
					if (1 <= bSameFlowerList[j] && j < 15)
					{
						num++;
					}
				}
				bFlag = num >= 5 ? true : false;
				if (m_bKingCanReplace && iKingCount+iHuaCount>0 && num>1/*四张财神就是同花顺了*/)
				{
					bFlag = num + iKingCount + iHuaCount>= 5 ? true : false;
				}
				if (bFlag)
				{
					for (int k = j - 1; k >= i; k--)
					{
						for (int t = 0; t < vSameFlowerList.size(); t++)
						{
							if (k == GetCardNum(vSameFlowerList.at(t)))
							{
								iResultCard[iResultCardCount] = vSameFlowerList.at(t);
								iResultCardCount++;
								break;
							}
						}
					}
					if (m_bKingCanReplace&&iKingCount>0 && iResultCardCount < 5)
					{
						iResultCard[iResultCardCount] = iCardResultList[iKingCount - 1];
						iKingCount--;
						iResultCardCount++;
						if (iResultCardCount<5 && iKingCount>0)
						{
							iResultCard[iResultCardCount] = iCardResultList[iKingCount - 1];
							iResultCardCount++;
							iKingCount--;
						}
					}
					if (m_bKingCanReplace&& iKingCount <= 0 && iResultCardCount < 5 && iHuaCount>0)
					{
						int oldHuaCount = iHuaCount;
						for (int i = 0; i < oldHuaCount; i++)
						{
							if (iResultCardCount < 5)
							{
								iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
								iHuaCount--;
								iResultCardCount++;
							}
						}
					}
					return true;
				}
			}
		}
		//OutputDebugString("lwlog::007");
		return false;
	}


	//查找四条
	bool CUpGradeGameLogic::FindFourStrip(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard)
	{
		if (iHandCardCount<4)
		{
			return false;
		}
		TackOutBomb(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
		if (isNeedFillCard)
		{
			if (4 == iResultCardCount)
			{
				//只有四张牌，需要补一张单牌
				BYTE bTempList[18];
				memset(bTempList, 0, sizeof(bTempList));
				for (int i = 0; i<iHandCardCount; i++)
				{
					bool bSame = false;
					for (int k = 0; k<iResultCardCount; k++)
					{
						if (iResultCard[k] == iHandCard[i])
						{
							bSame = true;
							break;
						}
					}
					if (bSame)
					{
						continue;
					}
					bTempList[GetCardNum(iHandCard[i])]++;
				}
				//找牌型最小的一张牌
				for (int i = 0; i<18; i++)//单牌
				{
					if (1 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
				for (int i = 0; i<18; i++)//对牌
				{
					if (2 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
				for (int i = 0; i<18; i++)//三张一样的牌
				{
					if (3 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
				for (int i = 0; i<18; i++)//四张一样的牌
				{
					if (4 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
			}
			return false;
		}
		else
		{
			return 4 == iResultCardCount ? true : false;
		}
	}

	//查找葫芦
	bool CUpGradeGameLogic::FindCalabash(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount)
	{
		BYTE bTempList[18];
		memset(bTempList, 0, sizeof(bTempList));
		iResultCardCount = 0;
		if (iHandCardCount<5)
		{
			return false;
		}
		if (FindFourStrip(iHandCard,iHandCardCount,iResultCard,iResultCardCount))
		{
			return false;
		}
		memset(bTempList, 0, sizeof(bTempList));
		iResultCardCount = 0;
		for (int i = 0; i<iHandCardCount; i++)
		{
			bTempList[GetCardNum(iHandCard[i])]++;
		}
		int iMaxSum = 0;
		int iSameSum = 0;
		bool findThree = false;
		bool findTwo = false;
		int  threeIndex = 255;

		BYTE iKingResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList);

		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList);
		//查找三张一样的牌		
		for (int i = 14; i >= 0; i--)
		{
			if ((bTempList[i]>=3 || (bTempList[i] + iKingCount + iHuaCount >= 3 && m_bKingCanReplace && bTempList[i]>0)) && findThree == false)
			{
				for (int index = 0; index < iHandCardCount; index++)
				{
					if (i == GetCardNum(iHandCard[index]))
					{
						if (iResultCardCount == 3)
						{
							break;
						}
						iResultCard[iResultCardCount] = iHandCard[index];
						iResultCardCount++;
						findThree = true;
						threeIndex = i;
					}
					if (m_bKingCanReplace && iKingCount > 0 && index == iHandCardCount - 1 && iResultCardCount<3) //用王牌填
					{
						iResultCard[iResultCardCount] = iKingResultList[iKingCount-1];
						iResultCardCount++;
						iKingCount--;
						if (iResultCardCount == 2 && iKingCount > 0)
						{
							iResultCard[iResultCardCount] = iKingResultList[iKingCount - 1];
							iResultCardCount++;
							iKingCount--;
						}
					}
					if (m_bKingCanReplace && iKingCount <= 0 && iHuaCount>0 && index == iHandCardCount - 1 && iResultCardCount < 3) //用花牌填
					{
						iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
						iResultCardCount++;
						iHuaCount--;
						if (iResultCardCount == 2 && iHuaCount > 0)
						{
							iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
							iResultCardCount++;
							iHuaCount--;
						}
					}
				}
				break;
			}
		}

		//查找对牌
		for (int i = 0; i <= 14; i++)
		{
			if ((bTempList[i]>=2 || (bTempList[i] + iKingCount + iHuaCount >= 2 && m_bKingCanReplace)) && bTempList[i] != 0 && threeIndex != i)
			{
				for (int index = 0; index < iHandCardCount; index++)
				{
					if (i == GetCardNum(iHandCard[index]))
					{
						iResultCard[iResultCardCount] = iHandCard[index];
						iResultCardCount++;
						if (iResultCardCount == 5)
						{
							return true;
						}
						findTwo = true;
					}
					if (index == iHandCardCount - 1 && m_bKingCanReplace && iKingCount>0 && iResultCardCount<5)
					{
						iResultCard[iResultCardCount] = iKingResultList[iKingCount-1];
						iResultCardCount++;
					}
					if (index == iHandCardCount - 1 && m_bKingCanReplace && iHuaCount>0 && iResultCardCount<5 && iKingCount <= 0)
					{
						iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
						iResultCardCount++;
					}
				}
				break;
			}
		}		
		
		//没有找到2张的，用有3张的代替
		if (findTwo == false)
		{
			for (int i = 0; i <= 14; i--)
			{
				if (3 <= bTempList[i] && threeIndex != i)
				{
					for (int index = 0; index < iHandCardCount; index++)
					{
						if (i == GetCardNum(iHandCard[index]))
						{
							if (iResultCardCount == 5)
							{
								return true;
							}
							iResultCard[iResultCardCount] = iHandCard[index];
							iResultCardCount++;
							findTwo = true;
						}
					}
					break;
				}
			}
		}
		
		if (5 == iResultCardCount)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//查找同花
	bool CUpGradeGameLogic::FindSameFlower(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount)
	{
		BYTE bTempList[7];
		memset(bTempList, 0, sizeof(bTempList));
		iResultCardCount = 0;
		if (iHandCardCount<5)
		{
			return false;
		}

		//计算同花个数
		for (int i = 0; i<iHandCardCount; i++)
		{
			if (!GetCardIsCaiShen(iHandCard[i]))
			{
				bTempList[GetCardHuaKind(iHandCard[i])]++;
			}
		}
		BYTE iKingResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList);

		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList);

		if (m_bKingCanReplace)
		{
			for (int i = 0; i < 7; i++)
			{
				if (bTempList[i]>0)
				{
					bTempList[i] = bTempList[i] + iKingCount + iHuaCount;
				}
			}
		}
		for (int i = 6; i >= 0; i--)
		{
			if (bTempList[i] >= 5)
			{
				for (int index = 0; index<iHandCardCount; index++)
				{
					if (i == GetCardHuaKind(iHandCard[index]) || (m_bKingCanReplace && (iKingCount+iHuaCount >0)))
					{
						if (GetCardIsCaiShen(iHandCard[index]) && iResultCardCount < 5)
						{
							if (iKingCount>0)
							{
								iResultCard[iResultCardCount] = iKingResultList[iKingCount - 1];
								iResultCardCount++;
								iKingCount--;
							}
							else if (iHuaCount>0 && iResultCardCount < 5)
							{
								iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
								iResultCardCount++;
								iHuaCount--;
							}
							if (5 == iResultCardCount)
							{
								return true;
							}
							continue;
						}
						iResultCard[iResultCardCount] = iHandCard[index];
						iResultCardCount++;
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	//查找顺子
	bool CUpGradeGameLogic::FindStraight(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount)
	{
		BYTE bTempList[18];
		memset(bTempList, 0, sizeof(bTempList));
		iResultCardCount = 0;
		BYTE iKingResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList);
		
		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList);

		if (iHandCardCount<5)
		{
			return false;
		}
		for (int i = 0; i<iHandCardCount; i++)
		{
			bTempList[GetCardNum(iHandCard[i])]++;
		}
		if (bTempList[14] != 0 || (m_bKingCanReplace && (iKingCount + iHuaCount> 0)))//有A这个值，
		{
			//1:先判断是否存在 10 J Q K A,有则提取
			bool bFlag = true; 
			int TenToANum = 0; //10 J Q K A的数量
			//继续扫描
			for (int j = 10; j < 15; j++)
			{
				if (1 <= bTempList[j])
				{
					TenToANum++;
				}
			}
			bFlag = TenToANum >= 5 ? true : false;
			if (m_bKingCanReplace && (iKingCount + iHuaCount>0) && TenToANum >= 1)
			{
				bFlag = TenToANum + iKingCount + iHuaCount >= 5 ? true : false;
			}
			if (bFlag)//找到了10 J Q K A
			{
				for (int k = 14; k >= 10 ; k--)
				{
					for (int t = 0; t < iHandCardCount; t++)
					{
						if (k == GetCardNum(iHandCard[t]) && iResultCardCount<5)
						{
							iResultCard[iResultCardCount] = iHandCard[t];
							iResultCardCount++;
							break;
						}
						if (t == iHandCardCount - 1 && (m_bKingCanReplace && (iKingCount>0)) && iResultCardCount<5)//没有找到 用王代替
						{
							iResultCard[iResultCardCount] = iKingResultList[iKingCount-1];
							iResultCardCount++;
							iKingCount--;
							if (iKingCount > 0 && iResultCardCount < 5)
							{
								iResultCard[iResultCardCount] = iKingResultList[iKingCount - 1];
								iResultCardCount++;
								iKingCount--;
							}
						}
						if (t == iHandCardCount - 1 && (m_bKingCanReplace && (iHuaCount > 0) && (iKingCount <= 0)) && iResultCardCount<5)//没有找到 用花牌代替
						{
							int oldHuaCount = iHuaCount;
							for (int i = 0; i < oldHuaCount; i++)
							{
								if (iResultCardCount < 5 && iHuaCount > 0)
								{
									iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
									iResultCardCount++;
									iHuaCount--;
								}
							}
						}
					}
				}
				return true;
			}

			//2:再判断是否存在 A 2 3 4 5,有则提取
			bFlag = true;
			memset(bTempList, 0, sizeof(bTempList));
			for (int i = 0; i < iHandCardCount; i++)
			{
				bTempList[GetCardNum(iHandCard[i])]++;
			}
			iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList);
			int aToFive = 0;//2 3 4 5数量
			//继续扫描
			for (int j = 2; j < 6; j++)
			{
				if (1 <= bTempList[j])
				{
					aToFive++;
				}
			}
			//有没有A
			int haveA = 0;
			for (int t = 0; t < iHandCardCount; t++)
			{
				if (14 == GetCardNum(iHandCard[t]))
				{
					haveA++;
					break;
				}
			}
			bFlag = aToFive+haveA >= 5 ? true : false;
			if (m_bKingCanReplace && (iKingCount + iHuaCount>0))
			{
				bFlag = aToFive + iKingCount + haveA + iHuaCount >= 5 ? true : false;
			}
			if (bFlag)
			{
				bool findA = false;
				for (int t = 0; t < iHandCardCount; t++)
				{
					if (14 == GetCardNum(iHandCard[t]))
					{
						iResultCard[4] = iHandCard[t];
						iResultCardCount++;
						findA = true;
						break;
					}
					if (t == iHandCardCount - 1 && (m_bKingCanReplace && (iKingCount>0)) && !findA)
					{
						iResultCard[4] = iKingResultList[iKingCount - 1];
						iResultCardCount++;
						iKingCount--;
						findA = true;
					}
					if (t == iHandCardCount - 1 && (m_bKingCanReplace && (iKingCount <= 0) && iHuaCount>0) && !findA)
					{
						iResultCard[4] = iHuaResultList[iHuaCount - 1];
						iResultCardCount++;
						iHuaCount--;
					}
				}
				int index = 0;
				for (int k = 5; k >=2 ; k--)
				{
					for (int t = 0; t < iHandCardCount; t++)
					{
						if (k == GetCardNum(iHandCard[t]) && iResultCardCount<5)
						{
							iResultCard[index] = iHandCard[t];
							index++;
							iResultCardCount++;
							break;
						}
						if (t == iHandCardCount - 1 && (m_bKingCanReplace && (iKingCount > 0)) && iResultCardCount<5)
						{
							iResultCard[index] = iKingResultList[iKingCount - 1];
							index++;
							iResultCardCount++;
							iKingCount--;
						}
						if (t == iHandCardCount - 1 && (m_bKingCanReplace && (iHuaCount > 0) && (iKingCount <= 0)) && iResultCardCount<5)
						{
							int oldHuaCount = iHuaCount;
							for (int i = 0; i < oldHuaCount;i++)
							{
								if (iHuaCount>0 && iResultCardCount<5)
								{
									iResultCard[index] = iHuaResultList[iHuaCount - 1];
									index++;
									iResultCardCount++;
									iHuaCount--;
								}
							}
						}
					}
				}
				return true;
			}
		}
		
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList);
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList);
		//找顺子
		for (int i = 14; i >= 0; i--)
		{
			if (bTempList[i] != 0 || (m_bKingCanReplace && (iKingCount + iHuaCount > 0)))//有值
			{
				int j = i;
				bool bFlag = true;
				//继续扫描
				int shunZiNum = 0; //连续的数字
				for (; j <(i + 5); j++)
				{
					if (1 <= bTempList[j] && j < 15)
					{
						shunZiNum++;
					}
				}
				bFlag = shunZiNum >= 5 ? true : false;
				if (m_bKingCanReplace && iKingCount + iHuaCount >0)
				{
					bFlag = shunZiNum + iKingCount + iHuaCount >= 5 ? true : false;
				}
				if (bFlag)
				{
					for (int k = j - 1; k >= i; k--)
					{
						for (int t = 0; t<iHandCardCount; t++)
						{
							if (k == GetCardNum(iHandCard[t]) && iResultCardCount<5)
							{
								iResultCard[iResultCardCount] = iHandCard[t];
								iResultCardCount++;
								break;
							}
							if (t == iHandCardCount-1 && m_bKingCanReplace && iKingCount>0 && iResultCardCount<5)
							{
								iResultCard[iResultCardCount] = iKingResultList[iKingCount-1];
								iResultCardCount++;
								iKingCount--;
							}
							if (t == iHandCardCount - 1 && m_bKingCanReplace && (iHuaCount > 0) && (iKingCount <= 0) && iResultCardCount<5)
							{
								int oldHuaCount = iHuaCount;
								for (int i = 0; i < oldHuaCount;i++)
								{
									if (iHuaCount>0 && iResultCardCount<5)
									{
										iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
										iResultCardCount++;
										iHuaCount--;
									}
								}
							}
						}
					}
					return true;
				}
			}
		}
		return false;
	}
	//查找三条
	bool CUpGradeGameLogic::FindThreeStrip(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard)
	{
		BYTE bTempList[18];
		memset(bTempList, 0, sizeof(bTempList));
		iResultCardCount = 0;
		if (iHandCardCount<3)
		{
			return false;
		}
		for (int i = 0; i<iHandCardCount; i++)
		{
			bTempList[GetCardNum(iHandCard[i])]++;
		}
		int iThree = 0;//对子个数	
		BYTE iKingResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList);

		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList);

		//查找三张一样的牌
		for (int i = 14; i >= 0; i--)
		{
			if (3 <= bTempList[i] || (m_bKingCanReplace && (iKingCount + iHuaCount + bTempList[i] >= 3) && (bTempList[i]>0 || iHandCardCount == 3)))
			{
				for (int index = 0; index < iHandCardCount; index++)
				{
					if (iResultCardCount>=3)
					{
						break;
					}
					if (i == GetCardNum(iHandCard[index]))
					{
						iResultCard[iResultCardCount] = iHandCard[index];
						iResultCardCount++;
					}
				}
				if (m_bKingCanReplace && iKingCount > 0 && iResultCardCount < 3) //用王牌填
				{
					iResultCard[iResultCardCount] = iKingResultList[iKingCount - 1];
					iResultCardCount++;
					iKingCount--;
					if (iKingCount>0 && iResultCardCount < 3)
					{
						iResultCard[iResultCardCount] = iKingResultList[iKingCount - 1];
						iResultCardCount++;
						iKingCount--;
					}
				}
				if (m_bKingCanReplace && iHuaCount > 0 && iResultCardCount < 3) //用花牌填
				{
					iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
					iResultCardCount++;
					iHuaCount--;
					if (iHuaCount>0 && iResultCardCount < 3)
					{
						iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
						iResultCardCount++;
						iHuaCount--;
					}
					if (iHuaCount>0 && iResultCardCount < 3)
					{
						iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
						iResultCardCount++;
						iHuaCount--;
					}
				}
				iThree++;
				break;
			}
		}
		if (isNeedFillCard)
		{
			if (3 == iResultCardCount)
			{
				//只有三张牌，需要补两张单牌
				memset(bTempList, 0, sizeof(bTempList));
				for (int i = 0; i<iHandCardCount; i++)
				{
					bool bSame = false;
					for (int k = 0; k<iResultCardCount; k++)
					{
						if (iResultCard[k] == iHandCard[i])
						{
							bSame = true;
							break;
						}
					}
					if (bSame)
					{
						continue;
					}
					bTempList[GetCardNum(iHandCard[i])]++;
				}
				//找牌型最小的一张牌
				for (int i = 0; i<18; i++)//单牌
				{
					if (1 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								if (5 == iResultCardCount)
								{
									return true;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
				}
				for (int i = 0; i<18; i++)//对牌
				{
					if (2 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								if (5 == iResultCardCount)
								{
									return true;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
				}
				for (int i = 0; i<18; i++)//三张一样的牌
				{
					if (3 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								if (5 == iResultCardCount)
								{
									return true;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
				}
				for (int i = 0; i<18; i++)//四张一样的牌
				{
					if (4 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								if (5 == iResultCardCount)
								{
									return true;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
				}
			}
			return false;
		} 
		else
		{
			return iThree == 0 ? false : true;
		}
		
		
	}
	//查找两对
	bool CUpGradeGameLogic::FindSecondDouble(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard)
	{
		BYTE bTempList[18];
		memset(bTempList, 0, sizeof(bTempList));
		iResultCardCount = 0;
		if (iHandCardCount<4)
		{
			return false;
		}
		for (int i = 0; i<iHandCardCount; i++)
		{
			bTempList[GetCardNum(iHandCard[i])]++;
		}
		int iDouble = 0;//对子个数
				
		for (int i = 17; i >= 0; i--)
		{
			if (2 == bTempList[i])
			{
				for (int index = 0; index < iHandCardCount; index++)
				{
					if (i == GetCardNum(iHandCard[index]))
					{
						iResultCard[iResultCardCount] = iHandCard[index];
						iResultCardCount++;
					}
				}
				iDouble++;
				if (2 == iDouble)
				{
					break;
				}
			}
		}
		

		if (isNeedFillCard)
		{
			if (2 == iDouble)
			{
				//只有四张牌，补一张牌
				memset(bTempList, 0, sizeof(bTempList));
				for (int i = 0; i<iHandCardCount; i++)
				{
					bool bSame = false;
					for (int k = 0; k<iResultCardCount; k++)
					{
						if (iResultCard[k] == iHandCard[i])
						{
							bSame = true;
							break;
						}
					}
					if (bSame)
					{
						continue;
					}
					bTempList[GetCardNum(iHandCard[i])]++;
				}
				//找牌型最小的一张牌
				for (int i = 0; i<18; i++)//单牌
				{
					if (1 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
				for (int i = 0; i<18; i++)//对牌
				{
					if (2 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
				for (int i = 0; i<18; i++)//三张一样的牌
				{
					if (3 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
				for (int i = 0; i<18; i++)//四张一样的牌
				{
					if (4 == bTempList[i])
					{
						for (int index = 0; index<iHandCardCount; index++)
						{
							if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
							{
								iResultCard[iResultCardCount] = iHandCard[index];
								iResultCardCount++;
								return true;
							}
						}
					}
				}
			}
			return false;
		} 
		else
		{
			return iDouble == 2 ? true : false;
		}		
	}
	
	//查找一对
	bool CUpGradeGameLogic::FindOneDouble(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard)
	{
		BYTE bTempList[18];
		memset(bTempList, 0, sizeof(bTempList));
		
		if (iHandCardCount<3)
		{
			return false;
		}
		for (int i = 0; i<iHandCardCount; i++)
		{
			bTempList[GetCardNum(iHandCard[i])]++;
		}
		int iDouble = 0;//对子个数
		BYTE iKingResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList); //大小王

		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList); //花牌(癞子)

		for (int i = 14; i >= 0; i--)
		{
			if (2 <= bTempList[i] || (m_bKingCanReplace && iKingCount + iHuaCount > 0 && 1 <= bTempList[i]))
			{
				for (int index = 0; index < iHandCardCount; index++) 
				{
					if (iResultCardCount>=2)
					{
						break;
					}
					if (i == GetCardNum(iHandCard[index]))
					{
						iResultCard[iResultCardCount] = iHandCard[index];
						iResultCardCount++;
					}
					if (index == iHandCardCount - 1 && m_bKingCanReplace&&iKingCount>0 && iResultCardCount == 1) //用大小王填
					{
						iResultCard[iResultCardCount] = iKingResultList[iKingCount - 1];
						iResultCardCount++;
					}
					if (index == iHandCardCount - 1 && m_bKingCanReplace&&iHuaCount>0 && iResultCardCount == 1) //用花牌(癞子)填
					{
						iResultCard[iResultCardCount] = iHuaResultList[iHuaCount - 1];
						iResultCardCount++;
					}
				}
				iDouble++;
				break;
			}
		}
		
		if (isNeedFillCard)
		{
			if (1 == iDouble)
			{
				if (3 == iHandCardCount)
				{
					for (int i = 0; i < 18; i++)//查找最后一张单牌
					{
						if (1 == bTempList[i])
						{
							for (int index = 0; index < iHandCardCount; index++)
							{
								if (i == GetCardNum(iHandCard[index]) && iResultCardCount<iHandCardCount && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
								{
									iResultCard[iResultCardCount] = iHandCard[index];
									iResultCardCount++;
								}
							}
						}
					}
					return true;
				}
				else
				{			
					//只有四张牌，补一张牌
					memset(bTempList, 0, sizeof(bTempList));
					for (int i = 0; i < iHandCardCount; i++)
					{
						bool bSame = false;
						for (int k = 0; k < iResultCardCount; k++)
						{
							if (iResultCard[k] == iHandCard[i])
							{
								bSame = true;
								break;
							}
						}
						if (bSame)
						{
							continue;
						}
						bTempList[GetCardNum(iHandCard[i])]++;
					}
					//找牌型最小的一张牌
					for (int i = 0; i < 18; i++)//单牌
					{
						if (1 == bTempList[i])
						{
							for (int index = 0; index < iHandCardCount; index++)
							{
								if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
								{
									iResultCard[iResultCardCount] = iHandCard[index];
									iResultCardCount++;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
					for (int i = 0; i < 18; i++)//对牌
					{
						if (2 == bTempList[i])
						{
							for (int index = 0; index < iHandCardCount; index++)
							{
								if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
								{
									iResultCard[iResultCardCount] = iHandCard[index];
									iResultCardCount++;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
					for (int i = 0; i < 17; i++)//三张一样的牌
					{
						if (3 == bTempList[i])
						{
							for (int index = 0; index < iHandCardCount; index++)
							{
								if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
								{
									iResultCard[iResultCardCount] = iHandCard[index];
									iResultCardCount++;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
					for (int i = 0; i < 18; i++)//四张一样的牌
					{
						if (4 == bTempList[i])
						{
							for (int index = 0; index < iHandCardCount; index++)
							{
								if (i == GetCardNum(iHandCard[index]) && !finOneCard(iResultCard, iResultCardCount, GetCardNum(iHandCard[index])))
								{
									iResultCard[iResultCardCount] = iHandCard[index];
									iResultCardCount++;
								}
							}
						}
						if (5 == iResultCardCount)
						{
							return true;
						}
					}
				}
			}
			return false;
		} 
		else
		{
			return iDouble == 0 ? false : true;
		}

	}
	
	//查找五同
	bool CUpGradeGameLogic::FindFiveStrip(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard)
	{
		iResultCardCount = 0;
		if (iHandCardCount < 5)
		{
			return false;
		}
		int temp[18] = { 0 };
		for (int i = 0; i < iHandCardCount; i++)
		{
			temp[GetCardBulk(iHandCard[i])]++;
		}
		BYTE iKingResultList[2] = {};
		BYTE iKingCount = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iKingResultList);

		BYTE iHuaResultList[6] = {};
		BYTE iHuaCount = 0;
		iHuaCount = GetHuaCard(iHandCard, iHandCardCount, iHuaResultList);
		if (!m_bKingCanReplace)
		{
			iKingCount = 0;
			iHuaCount  = 0;
		}
		for (int i = 14; i >= 0; i--)
		{
			if (temp[i] + iKingCount + iHuaCount >= 5)
			{
				for (int j = 0; j < iHandCardCount; j++)
				{
					if (iResultCardCount>=5)
					{
						break;
					}
					if (i == GetCardBulk(iHandCard[j]))
					{
						iResultCard[iResultCardCount++] = iHandCard[j];
					}
					if (iResultCardCount < 5 && m_bKingCanReplace && (j == (iHandCardCount - 1)) && iKingCount>0)//需要用财神牌填充
					{
						iResultCard[iResultCardCount++] = iKingResultList[iKingCount - 1];
						iKingCount--;
						if (iResultCardCount < 5 && iKingCount>0)
						{
							iResultCard[iResultCardCount++] = iKingResultList[iKingCount - 1];
							iKingCount--;
						}
					}
					if (iResultCardCount < 5 && m_bKingCanReplace && (j == (iHandCardCount - 1)) && iKingCount<=0 && iHuaCount>0)//需要用花牌牌填充
					{
						int oldHuaCount = iHuaCount;
						for (int i = 0; i < oldHuaCount; i++)
						{
							if (iResultCardCount < 5 && iHuaCount>0)
							{
								iResultCard[iResultCardCount++] = iHuaResultList[iHuaCount - 1];
								iHuaCount--;
							}
						}
					}
				}
			}
		}
		return iResultCardCount;
	}

	//查找所有对子
	bool CUpGradeGameLogic::FindAllDouble(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
		iResultCardCount = 0;
		if (HandCardCount < 3)
		{
			return false;
		}
		//把大小王和花牌(癞子)放到最后面
		BYTE iHandCard[13] = { 0 };
		int iHandCardCount = 0;
		sortCardListForFindAllType(HandCard, HandCardCount, iHandCard, iHandCardCount);

		int iDouble = 0;//对子个数
		BYTE iCardResultList[2] = {};
		BYTE iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);
		//遍历查找
		for (int i = 0; i < iHandCardCount; i++)
		{
			if (iHandCard[i] && !GetCardIsCaiShen(iHandCard[i]))//最少有一张
			{
				for (int j = i+1; j < iHandCardCount; j++)
				{
					if (j>=iHandCardCount)
					{
						continue;
					}
					if (iHandCard[j])
					{
						if (GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[j]) ||
							GetCardIsCaiShen(iHandCard[j]))
						{
							allResultCard.push_back(iHandCard[i]);
							allResultCard.push_back(iHandCard[j]);
							iResultCardCount = iResultCardCount + 2;
							iDouble++;
						}
					}
				}
			}
		}
	
		if (iDouble == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	//查找所有两对
	bool CUpGradeGameLogic::FindAllTwoDouble(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
		iResultCardCount = 0;
		if (HandCardCount < 3)
		{
			return false;
		}
		//把大小王和花牌(癞子)放到最后面
		BYTE iHandCard[13] = { 0 };
		int iHandCardCount = 0;
		//sortCardListForFindAllType(HandCard, HandCardCount, iHandCard, iHandCardCount);
		//不要财神牌
		for (int i = 0; i < HandCardCount;i++)
		{
			if (!GetCardIsCaiShen(HandCard[i]))
			{
				iHandCard[iHandCardCount] = HandCard[i];
				iHandCardCount++;
			}
		}
		int iTwoDouble = 0;//两对个数
		//遍历查找
		for (int i = 0; i < iHandCardCount; i++)
		{
			if (iHandCard[i])//最少有一张
			{
				for (int j = i + 1; j < iHandCardCount; j++)
				{
					if (j >= iHandCardCount)
					{
						continue;
					}
					if (iHandCard[j])
					{
						if (GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[j]))
						{
							//到这里找到了一对 去掉这对 找下一对
							BYTE newHandCard[13] = { 0 };
							int newHandCardCount = 0;
							for (int e = 0; e < iHandCardCount; e++)
							{
								if (iHandCard[e] != iHandCard[i] && iHandCard[e] != iHandCard[j])
								{
									newHandCard[newHandCardCount] = iHandCard[e];
									newHandCardCount++;
								}
							}
							for (int f = 0; f < newHandCardCount; f++)
							{
								if (newHandCard[f])//最少有一张
								{
									for (int g = f + 1; g < newHandCardCount; g++)
									{
										if (g >= newHandCardCount)
										{
											continue;
										}
										if (newHandCard[g])
										{
											if (GetCardNum(newHandCard[f]) == GetCardNum(newHandCard[g]))
											{
												
												BYTE tempCard[4] = { 0 };
												tempCard[0] = iHandCard[i];
												tempCard[1] = iHandCard[j];
												tempCard[2] = newHandCard[f];
												tempCard[3] = newHandCard[g];
												BYTE resultCard[4] = { 0 };
												int resultCardNum = 0;
												if (FindFourStrip(tempCard, 4, resultCard, resultCardNum))//不能是炸弹
												{
													continue;
												}
												//只能是从大到小 从小到大会重复写入
												if (GetCardNum(tempCard[3])>GetCardNum(tempCard[0]))
												{
													continue;
												}
												allResultCard.push_back(iHandCard[i]);
												allResultCard.push_back(iHandCard[j]);
												allResultCard.push_back(newHandCard[f]);
												allResultCard.push_back(newHandCard[g]);
												iResultCardCount = iResultCardCount + 4;
												iTwoDouble++;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (iTwoDouble == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	//查找所有三张
	bool CUpGradeGameLogic::FindAllThreeStrip(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
		iResultCardCount = 0;
		if (HandCardCount < 3)
		{
			return false;
		}
		//把大小王和花牌放到最后面
		BYTE iHandCard[13] = { 0 };
		int iHandCardCount = 0;
		sortCardListForFindAllType(HandCard, HandCardCount, iHandCard, iHandCardCount);

		int iDouble = 0;//对子个数
		BYTE iCardResultList[2] = {};
		BYTE iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);
		bool firstNumIsCaiShen = false;
		bool twoNumIsCaiShen = false;
		bool threeNumCaiShen = false;
		//遍历查找
		for (int i = 0; i < iHandCardCount; i++)//第一张
		{
			if (iHandCard[i])
			{
				firstNumIsCaiShen = GetCardIsCaiShen(iHandCard[i]);
				for (int j = i + 1; j < iHandCardCount; j++)//第二张
				{
					if (j >= iHandCardCount)
					{
						continue;
					}
					if (!iHandCard[j])
					{
						continue;
					}
					twoNumIsCaiShen = GetCardIsCaiShen(iHandCard[j]);
					for (int k = j + 1; k < iHandCardCount; k++) //第3张
					{
						if (k >= iHandCardCount)
						{
							continue;
						}
						if (iHandCard[k])
						{
							threeNumCaiShen = GetCardIsCaiShen(iHandCard[k]);
							int caiShenNum = 0;
							if (firstNumIsCaiShen)caiShenNum++;
							if (twoNumIsCaiShen)caiShenNum++;
							if (threeNumCaiShen)caiShenNum++;
							if ((caiShenNum == 2) || (caiShenNum == 3 && HandCardCount == 3) ||
								((GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[j])) && (GetCardNum(iHandCard[j]) == GetCardNum(iHandCard[k]))) ||
								(firstNumIsCaiShen && (caiShenNum == 1) && (GetCardNum(iHandCard[j]) == GetCardNum(iHandCard[k]))) ||
								(twoNumIsCaiShen && (caiShenNum == 1) && (GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[k]))) ||
								(threeNumCaiShen && (caiShenNum == 1) && (GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[j])))
								)
							{
								allResultCard.push_back(iHandCard[i]);
								allResultCard.push_back(iHandCard[j]);
								allResultCard.push_back(iHandCard[k]);
								iResultCardCount = iResultCardCount + 3;
								iDouble++;
							}
						}
					}
				}
			}
		}

		if (iDouble == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}


    //查找所有葫芦
	bool CUpGradeGameLogic::FindAllCalabash(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
		iResultCardCount = 0;
		if (HandCardCount < 5)
		{
			return false;
		}
		//把大小王放到最后面
		BYTE iHandCard[13] = { 0 };
		int iHandCardCount = 0;
		sortCardListForFindAllType(HandCard, HandCardCount, iHandCard, iHandCardCount);
		int iDouble = 0;//葫芦个数
		bool firstNumIsCaiShen = false;
		bool twoNumIsCaiShen = false;
		bool threeNumCaiShen = false;
		//遍历查找
		for (int i = 0; i < iHandCardCount; i++)//第一张
		{
			if (iHandCard[i] && !GetCardIsCaiShen(iHandCard[i]))//最少有一张
			{
				firstNumIsCaiShen = GetCardIsCaiShen(iHandCard[i]);
				for (int j = i + 1; j < iHandCardCount; j++)//第二张
				{
					if (j >= iHandCardCount){continue;}
					if (!iHandCard[j]){continue;}
					twoNumIsCaiShen = GetCardIsCaiShen(iHandCard[j]);
					for (int k = j + 1; k < iHandCardCount; k++) //第3张
					{
						if (k >= iHandCardCount){continue;}
						if (iHandCard[k])
						{
							threeNumCaiShen = GetCardIsCaiShen(iHandCard[k]);
							int caiShenNum = 0;
							if (firstNumIsCaiShen)caiShenNum++;
							if (twoNumIsCaiShen)caiShenNum++;
							if (threeNumCaiShen)caiShenNum++;
							if (((GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[j])) && (GetCardNum(iHandCard[j]) == GetCardNum(iHandCard[k]))) ||
								(caiShenNum == 2) ||
								((twoNumIsCaiShen) && (!threeNumCaiShen) && (GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[k]))) ||
								((!twoNumIsCaiShen) && (threeNumCaiShen) && (GetCardNum(iHandCard[i]) == GetCardNum(iHandCard[j])))
								)
							{
								//到这里已经找到了3张 去掉这3张 找2张就可以了
								BYTE newHandCard[13] = { 0 };
								int newHandCardCount = 0;
								bool newFirstNumIsCaiShen = false;
								for (int e = 0; e < iHandCardCount; e++)
								{
									if (iHandCard[e] != iHandCard[i] && iHandCard[e] != iHandCard[j] && iHandCard[e] != iHandCard[k])
									{
										newHandCard[newHandCardCount] = iHandCard[e];
										newHandCardCount++;
									}
								}
								for (int f = 0; f < newHandCardCount; f++)
								{
									if (newHandCard[f] && !GetCardIsCaiShen(newHandCard[f]))//最少有一张
									{
										newFirstNumIsCaiShen = GetCardIsCaiShen(newHandCard[f]);
										for (int g = f + 1; g < newHandCardCount; g++)
										{
											if (g >= newHandCardCount)
											{
												continue;
											}
											if (newHandCard[g])
											{
												if ((GetCardNum(newHandCard[f]) == GetCardNum(newHandCard[g])) ||
													 GetCardIsKing(newHandCard[g]))
												{
													BYTE tempCard[5] = { 0 };
													tempCard[0] = iHandCard[i];
													tempCard[1] = iHandCard[j];
													tempCard[2] = iHandCard[k];
													tempCard[3] = newHandCard[f];
													tempCard[4] = newHandCard[g];
													BYTE resultCard[5] = {0};
													int resultCardNum = 0;
													if (FindFourStrip(tempCard, 5, resultCard, resultCardNum))//不能是炸弹
													{
														continue;
													}
													allResultCard.push_back(iHandCard[i]);
													allResultCard.push_back(iHandCard[j]);
													allResultCard.push_back(iHandCard[k]);
													allResultCard.push_back(newHandCard[f]);
													allResultCard.push_back(newHandCard[g]);
													iResultCardCount = iResultCardCount + 5;
													iDouble++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (iDouble == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	//查找所有四张
	bool CUpGradeGameLogic::FindAllFourStrip(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
			iResultCardCount = 0;
			if (HandCardCount < 5)
			{
				return false;
			}
			//把大小王放到最后面
			BYTE iHandCard[13] = { 0 };
			int iHandCardCount = 0;
			sortCardListForFindAllType(HandCard, HandCardCount, iHandCard, iHandCardCount);
			int iDouble = 0;//对子个数
			BYTE iCardResultList[2] = {};
			BYTE iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);
			//遍历查找
			for (int i = 0; i < iHandCardCount; i++)//第一张
			{
				BYTE tempCard[4] = { 0 };
				if (iHandCard[i] && !GetCardIsCaiShen(iHandCard[i]))//最少有一张
				{
					for (int j = i + 1; j < iHandCardCount; j++)//第二张
					{
						if (j >= iHandCardCount){continue;}
						if (!iHandCard[j]){continue;}
						for (int k = j + 1; k < iHandCardCount; k++) //第3张
						{
							if (k >= iHandCardCount){continue;}
							if (iHandCard[k])
							{
								for (int e = k + 1; e < iHandCardCount; e++) //第四张
								{
									if (e >= iHandCardCount){continue;}
									if (iHandCard[e])
									{
										tempCard[0] = iHandCard[i];
										tempCard[1] = iHandCard[j];
										tempCard[2] = iHandCard[k];
										tempCard[3] = iHandCard[e];
										bool findSame = true;//是不是相同的 
										bool haveKing = false;
										for (int f = 0; f < 4;f++)
										{
											if (!GetCardIsCaiShen(tempCard[f]))
											{
												for (int g = f + 1; g < 4;g++)
												{
													if (g >= 4)
													{
														continue;
													}
													if (!GetCardIsCaiShen(tempCard[g]))
													{
														if (GetCardNum(tempCard[f]) != GetCardNum(tempCard[g]))
														{
															findSame = false;
														}
													}
													else
													{
														haveKing = true;
													}
												}
											}
											else
											{
												haveKing = true;
											}
										}
										if ((!haveKing && !m_bKingCanReplace && findSame) || (m_bKingCanReplace && findSame))
										{
											allResultCard.push_back(iHandCard[i]);
											allResultCard.push_back(iHandCard[j]);
											allResultCard.push_back(iHandCard[k]);
											allResultCard.push_back(iHandCard[e]);
											iResultCardCount = iResultCardCount + 4;
											iDouble++;
										}
									}
								}
							}
						}
					}
				}
			}

			if (iDouble == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
	}

	//查找所有同花
	bool CUpGradeGameLogic::FindAllSameFlower(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
			iResultCardCount = 0;
			if (HandCardCount < 5)
			{
				return false;
			}
			//把大小王和花牌放到最后面
			BYTE iHandCard[13] = { 0 };
			int iHandCardCount = 0;
			sortCardListForFindAllType(HandCard, HandCardCount, iHandCard, iHandCardCount);
			int iDouble = 0;//对子个数
			//遍历查找
			for (int i = 0; i < iHandCardCount; i++)//第一张
			{
				BYTE tempCard[5] = { 0 };
				if (iHandCard[i] && !GetCardIsCaiShen(iHandCard[i]))//最少有一张
				{
					for (int j = i + 1; j < iHandCardCount; j++)//第二张
					{
						if (j >= iHandCardCount)
						{
							continue;
						}
						if (!iHandCard[j])
						{
							continue;
						}
						for (int k = j + 1; k < iHandCardCount; k++) //第3张
						{
							if (k >= iHandCardCount)
							{
								continue;
							}
							if (iHandCard[k])
							{
								for (int e = k + 1; e < iHandCardCount; e++) //第四张
								{
									if (e >= iHandCardCount)
									{
										continue;
									}
									if (iHandCard[e])
									{
										for (int h = e + 1; h < iHandCardCount; h++) //第5张
										{
											if (h >= iHandCardCount)
											{
												continue;
											}
											if (iHandCard[h])
											{
												tempCard[0] = iHandCard[i];
												tempCard[1] = iHandCard[j];
												tempCard[2] = iHandCard[k];
												tempCard[3] = iHandCard[e];
												tempCard[4] = iHandCard[h];
												bool findSame = true;//是不是相同的 
												bool haveCaiShen = false;
												for (int f = 0; f < 5; f++)
												{
													if (!GetCardIsCaiShen(tempCard[f]))
													{
														for (int g = f + 1; g < 5; g++)
														{
															if (g >= 5)
															{
																continue;
															}
															if (!GetCardIsCaiShen(tempCard[g]))
															{
																if (GetCardHuaKind(tempCard[f]) != GetCardHuaKind(tempCard[g]))
																{
																	findSame = false;
																}
															}
															else
															{
																haveCaiShen = true;
															}
														}
													}
													else
													{
														haveCaiShen = true;
													}
												}
												if ((!haveCaiShen && !m_bKingCanReplace && findSame) || (m_bKingCanReplace && findSame))
												{
													allResultCard.push_back(iHandCard[i]);
													allResultCard.push_back(iHandCard[j]);
													allResultCard.push_back(iHandCard[k]);
													allResultCard.push_back(iHandCard[e]);
													allResultCard.push_back(iHandCard[h]);
													iResultCardCount = iResultCardCount + 5;
													iDouble++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

			if (iDouble == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
	}

	//查找所有顺子
	bool CUpGradeGameLogic::FindAllStraight(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount, bool needSameFlower)
	{
		//把大小王放到最后面
		BYTE newHandCard[13] = { 0 };
		int newHandCardCount = 0;
		sortCardListForFindAllType(iHandCard, iHandCardCount, newHandCard, newHandCardCount);
		BYTE findStraight[5] = { 0 };
		int iDouble = 0;
		//先找到最大的10JQKA
		for (int i = 0; i < newHandCardCount; i++) //第一张
		{
			if ((GetCardNum(newHandCard[i]) >= 10 && GetCardNum(newHandCard[i]) <= 14) || (GetCardIsCaiShen(newHandCard[i])))
			{
				findStraight[0] = newHandCard[i];
				for (int j = i + 1; j < newHandCardCount; j++)//第2张
				{
					if (j >= newHandCardCount)
					{
						break;
					}
					if ((GetCardNum(newHandCard[j]) >= 10 && GetCardNum(newHandCard[j]) <= 14) || (GetCardIsCaiShen(newHandCard[j])))
					{
						findStraight[1] = newHandCard[j];
						for (int k = j + 1; j < newHandCardCount; k++)//第3张
						{
							if (k >= newHandCardCount)
							{
								break;
							}
							if ((GetCardNum(newHandCard[k]) >= 10 && GetCardNum(newHandCard[k]) <= 14) || (GetCardIsCaiShen(newHandCard[k])))
							{
								findStraight[2] = newHandCard[k];
								for (int g = k + 1; g < newHandCardCount; g++)//第4张
								{
									if (g >= newHandCardCount)
									{
										break;
									}
									if ((GetCardNum(newHandCard[g]) >= 10 && GetCardNum(newHandCard[g]) <= 14) || (GetCardIsCaiShen(newHandCard[g])))
									{
										findStraight[3] = newHandCard[g];
										for (int f = g + 1; f < newHandCardCount; f++)//第5张
										{
											if (f >= newHandCardCount)
											{
												break;
											}
											if ((GetCardNum(newHandCard[f]) >= 10 && GetCardNum(newHandCard[f]) <= 14) || (GetCardIsCaiShen(newHandCard[f])))
											{
												findStraight[4] = newHandCard[f];

												//找到5张牌看能不能成为顺子
												INT newCount1 = 0;
												BYTE newCards1[500] = { 0 };

												bool isSame = false;
												if (needSameFlower)
												{
													isSame = FindSameFlower(findStraight, 5, newCards1, newCount1);
													//同花顺也可能组成五同
													auto caiShenNum = GetHuaCount(findStraight, 5) + GetKingCount(findStraight, 5);
													if (!isSame || caiShenNum>3)
													{
														continue;
													}
												}
												if (FindStraight(findStraight, 5, newCards1, newCount1))
												{
													allResultCard.push_back(findStraight[0]);
													allResultCard.push_back(findStraight[1]);
													allResultCard.push_back(findStraight[2]);
													allResultCard.push_back(findStraight[3]);
													allResultCard.push_back(findStraight[4]);
													iResultCardCount = iResultCardCount + 5;
													iDouble++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//再找A2345
		for (int i = 0; i < newHandCardCount; i++) //第一张
		{
			if (GetCardNum(newHandCard[i]) == 14 || (GetCardNum(newHandCard[i]) >= 2 && GetCardNum(newHandCard[i]) <= 5) || (GetCardIsCaiShen(newHandCard[i])))
			{
				findStraight[0] = newHandCard[i];
				for (int j = i + 1; j < newHandCardCount; j++)//第2张
				{
					if (j >= newHandCardCount)
					{
						break;
					}
					if (GetCardNum(newHandCard[j]) == 14 || (GetCardNum(newHandCard[j]) >= 2 && GetCardNum(newHandCard[j]) <= 5) || (GetCardIsCaiShen(newHandCard[j])))
					{
						findStraight[1] = newHandCard[j];
						for (int k = j + 1; j < newHandCardCount; k++)//第3张
						{
							if (k >= newHandCardCount)
							{
								break;
							}
							if (GetCardNum(newHandCard[k]) == 14 || (GetCardNum(newHandCard[k]) >= 2 && GetCardNum(newHandCard[k]) <= 5) || (GetCardIsCaiShen(newHandCard[k])))
							{
								findStraight[2] = newHandCard[k];
								for (int g = k + 1; g < newHandCardCount; g++)//第4张
								{
									if (g >= newHandCardCount)
									{
										break;
									}
									if (GetCardNum(newHandCard[g]) == 14 || (GetCardNum(newHandCard[g]) >= 2 && GetCardNum(newHandCard[g]) <= 5) || (GetCardIsCaiShen(newHandCard[g])))
									{
										findStraight[3] = newHandCard[g];
										for (int f = g + 1; f < newHandCardCount; f++)//第5张
										{
											if (f >= newHandCardCount)
											{
												break;
											}
											if (GetCardNum(newHandCard[f]) == 14 || (GetCardNum(newHandCard[f]) >= 2 && GetCardNum(newHandCard[f]) <= 5) || (GetCardIsCaiShen(newHandCard[f])))
											{
												findStraight[4] = newHandCard[f];

												//找到5张牌看能不能成为顺子
												INT newCount1 = 0;
												BYTE newCards1[500] = { 0 };

												bool isSame = false;
												if (needSameFlower)
												{
													isSame = FindSameFlower(findStraight, 5, newCards1, newCount1);
													//同花顺也可能组成五同
													auto caiShenNum = GetHuaCount(findStraight, 5) + GetKingCount(findStraight, 5);
													if (!isSame || caiShenNum>3)
													{
														continue;
													}
												}

												if (FindStraight(findStraight, 5, newCards1, newCount1))
												{
													allResultCard.push_back(findStraight[0]);
													allResultCard.push_back(findStraight[1]);
													allResultCard.push_back(findStraight[2]);
													allResultCard.push_back(findStraight[3]);
													allResultCard.push_back(findStraight[4]);
													iResultCardCount = iResultCardCount + 5;
													iDouble++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//再找普通的顺子
		for (int i = 0; i < newHandCardCount; i++) //第一张
		{
			if (newHandCard[i] || (GetCardIsCaiShen(newHandCard[i])))
			{
				findStraight[0] = newHandCard[i];
				for (int j = i + 1; j < newHandCardCount; j++)//第2张
				{
					if (j >= newHandCardCount)
					{
						break;
					}
					if (newHandCard[j] || (GetCardIsCaiShen(newHandCard[j])))
					{
						findStraight[1] = newHandCard[j];
						for (int k = j + 1; j < newHandCardCount; k++)//第3张
						{
							if (k >= newHandCardCount)
							{
								break;
							}
							if (newHandCard[k] || (GetCardIsCaiShen(newHandCard[k])))
							{
								findStraight[2] = newHandCard[k];
								for (int g = k + 1; g < newHandCardCount; g++)//第4张
								{
									if (g >= newHandCardCount)
									{
										break;
									}
									if (newHandCard[g] || (GetCardIsCaiShen(newHandCard[g])))
									{
										findStraight[3] = newHandCard[g];
										for (int f = g + 1; f < newHandCardCount; f++)//第5张
										{
											if (f >= newHandCardCount)
											{
												break;
											}
											if (newHandCard[f] || (GetCardIsCaiShen(newHandCard[f])))
											{
												findStraight[4] = newHandCard[f];
												//找到5张牌看能不能成为顺子
												INT newCount1 = 0;
												BYTE newCards1[500] = { 0 };
												//如果能组成10JQKA或者A2345
												BYTE bTempList[18];
												memset(bTempList, 0, sizeof(bTempList));
												BYTE kingResult[2];
												BYTE nowKingCount = GetKingCard(findStraight, 5, kingResult);
												BYTE huaResult[6];
												BYTE nowHuaCount = GetHuaCard(findStraight, 5, huaResult);
												for (int i = 0; i < 5; i++)
												{
													bTempList[GetCardNum(findStraight[i])]++;
												}
												//有10JQKA?
												bool bFlag = true;
												int TenToANum = 0;
												//继续扫描
												for (int u = 10; u < 15; u++)
												{
													if (1 <= bTempList[u])
													{
														TenToANum++;
													}
												}
												bFlag = TenToANum >= 5 ? true : false;
												if (m_bKingCanReplace && (nowKingCount + nowHuaCount>0))
												{
													bFlag = TenToANum + nowKingCount +nowHuaCount >= 5 ? true : false;
												}
												if (bFlag)
												{
													continue; //有10JQKA
												}
												//有A2345?
												bFlag = true;
												int Anum = 0;
												for (int u = 2; u < 6; u++)
												{
													if (1 <= bTempList[u])
													{
														Anum++;
													}
												}
												bFlag = Anum + bTempList[14] >= 5 ? true : false;
												if (m_bKingCanReplace && (nowKingCount + nowHuaCount>0))
												{
													bFlag = Anum + bTempList[14] + nowKingCount + nowHuaCount >= 5 ? true : false;
												}
												if (bFlag)
												{
													continue;//有A2345
												}

												bool isSame = false;
												if (needSameFlower)
												{
													isSame = FindSameFlower(findStraight, 5, newCards1, newCount1);
													//同花顺也可能组成五同
													auto caiShenNum = GetHuaCount(findStraight, 5) + GetKingCount(findStraight, 5);
													if (!isSame || caiShenNum>3)
													{
														continue;
													}
												}

												if (FindStraight(findStraight, 5, newCards1, newCount1))
												{
													allResultCard.push_back(findStraight[0]);
													allResultCard.push_back(findStraight[1]);
													allResultCard.push_back(findStraight[2]);
													allResultCard.push_back(findStraight[3]);
													allResultCard.push_back(findStraight[4]);
													iResultCardCount = iResultCardCount + 5;
													iDouble++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (iDouble == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	bool CUpGradeGameLogic::FindAllSameFlowerFlush(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
		return FindAllStraight(iHandCard, iHandCardCount, allResultCard, iResultCardCount, true);
	}

	//查找所有五同
	bool CUpGradeGameLogic::FindAllFiveStrip(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount)
	{
		iResultCardCount = 0;
		if (HandCardCount < 5)
		{
			return false;
		}
		//把大小王放到最后面
		BYTE iHandCard[13] = { 0 };
		int iHandCardCount = 0;
		sortCardListForFindAllType(HandCard, HandCardCount, iHandCard, iHandCardCount);
		int iDouble = 0;//对子个数
		BYTE iCardResultList[2] = {};
		BYTE iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);
		//遍历查找
		for (int i = 0; i < iHandCardCount; i++)//第一张
		{
			BYTE tempCard[5] = { 0 };
			if (iHandCard[i])//最少有一张
			{
				for (int j = i + 1; j < iHandCardCount; j++)//第二张
				{
					if (j >= iHandCardCount)
					{
						continue;
					}
					if (!iHandCard[j])
					{
						continue;
					}
					for (int k = j + 1; k < iHandCardCount; k++) //第3张
					{
						if (k >= iHandCardCount)
						{
							continue;
						}
						if (iHandCard[k])
						{
							for (int e = k + 1; e < iHandCardCount; e++) //第四张
							{
								if (e >= iHandCardCount)
								{
									continue;
								}
								if (iHandCard[e])
								{
									for (int h = e + 1; h < iHandCardCount; h++)
									{
										if (h >= iHandCardCount)
										{
											continue;
										}
										if (iHandCard[h])
										{
											tempCard[0] = iHandCard[i];
											tempCard[1] = iHandCard[j];
											tempCard[2] = iHandCard[k];
											tempCard[3] = iHandCard[e];
											tempCard[4] = iHandCard[h];
											bool findSame = true;//是不是相同的 
											bool haveKing = false;
											for (int f = 0; f < 5; f++)
											{
												if (!GetCardIsCaiShen(tempCard[f]))
												{
													for (int g = f + 1; g < 5; g++)
													{
														if (g >= 5)
														{
															continue;
														}
														if (!GetCardIsCaiShen(tempCard[g]))
														{
															if (GetCardNum(tempCard[f]) != GetCardNum(tempCard[g]))
															{
																findSame = false;
															}
														}
														else
														{
															haveKing = true;
														}
													}
												}
												else
												{
													haveKing = true;
												}
											}
											if ((!haveKing && !m_bKingCanReplace && findSame) || (m_bKingCanReplace && findSame))
											{
												allResultCard.push_back(iHandCard[i]);
												allResultCard.push_back(iHandCard[j]);
												allResultCard.push_back(iHandCard[k]);
												allResultCard.push_back(iHandCard[e]);
												allResultCard.push_back(iHandCard[h]);
												iResultCardCount = iResultCardCount + 5;
												iDouble++;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		if (iDouble == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	//查找牌值最大的高牌
	bool CUpGradeGameLogic::FindMaxHighCard(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount)
	{
		int iMaxSum = 0;

		BYTE bRemainCard[USER_CARD_COUNT];
		memset(bRemainCard, 0, sizeof(bRemainCard));
		memcpy(bRemainCard, iHandCard, sizeof(BYTE)*iHandCardCount);
		if (3 == iHandCardCount)//提三张牌
		{
			iResultCardCount = iHandCardCount;
			memcpy(iResultCard, iHandCard, sizeof(BYTE)*iHandCardCount);
			return true;
		}
		else//提五张牌
		{
			for (int i = 0; i<iHandCardCount; i++)
			{
				if (GetCardNum(iHandCard[i])>iMaxSum)
				{
					iMaxSum = GetCardNum(iHandCard[i]);
					iResultCard[0] = iHandCard[i];
					iResultCardCount = 1;
				}
			}
			DeleteACard(bRemainCard, USER_CARD_COUNT, iResultCard[0]);//删除提取的一张最大牌
			for (int i = 0; i<USER_CARD_COUNT; i++)
			{
				if (TestData(bRemainCard[i], 1))
				{
					iResultCard[iResultCardCount] = bRemainCard[i];
					iResultCardCount++;
					DeleteACard(bRemainCard, USER_CARD_COUNT, bRemainCard[i]);//删除提取的一张牌
					if (5 == iResultCardCount)
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	//从指定数组中删除一张牌
	void CUpGradeGameLogic::DeleteACard(BYTE bCardList[], int iCount, BYTE bDeleteCard)
	{
		for (int i = 0; i<iCount; i++)
		{
			if (bDeleteCard == bCardList[i])
			{
				bCardList[i] = 0;
				break;
			}
		}
	}
	/*
	@brief:当前牌数组是否有效
	@param:iData:牌数据,iStyle,判断类型
	@@return:BOOL
	*/
	//验证游戏中数据是否有效
	bool CUpGradeGameLogic::TestData(int iData, int iStyle)
	{
		if (0 == iStyle)//验证玩家座位号是否有效
		{
			if (iData >= 0 && iData < PLAY_COUNT)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (1 == iStyle)//验证牌数据是否有效
		{
			if (iData>0 && iData<112)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	//计算有效牌个数
	int CUpGradeGameLogic::CountUserCard(BYTE bCardList[], int iCount)
	{
		int iTemp = 0;
		for (int i = 0; i<iCount; i++)
		{
			if (TestData(bCardList[i], 1))
			{
				iTemp++;
			}
		}
		return iTemp;
	}

	//获取一对中最大的单牌
	BYTE CUpGradeGameLogic::GetMaxSingleCards(BYTE iHandCard[], int iHandCardCount)
	{
		BYTE bTempList[18];
		memset(bTempList, 0, sizeof(bTempList));

		if (iHandCardCount < 3)
		{
			return 0;
		}

		for (int i = 0; i < iHandCardCount; i++)
		{
			bTempList[GetCardNum(iHandCard[i])]++;
		}

		BYTE byMax = 0;
		for (int i = 0; i<18; i++)
		{
			if (1 == bTempList[i])
			{
				if (0 == byMax)
				{
					byMax = i;
				}
				else
				{
					if (i > byMax)
					{
						byMax = i;
					}
				}
			}
		}
		return byMax;
	}

	//排序
	bool CUpGradeGameLogic::SortCardData(BYTE byCardList[], int iCardCount)
	{
		bool bSorted = true, bTempUp;
		int iTemp, iLast = 0, iStationVol[45];
		memset(iStationVol, 0, sizeof(iStationVol));
		if (iCardCount > 45)
		{
			iCardCount = 45;
		}
		iLast = iCardCount - 0x01;
		//获取位置数值
		for (int i = 0; i < iCardCount; i++)
		{
			iStationVol[i] = GetCardBulk(byCardList[i], true);
		}

		//排序操作(按从大到小排序)
		do
		{
			bSorted = true;
			for (int i = 0; i < iLast; i++)
			{
				if (iStationVol[i] < iStationVol[i + 1])
				{
					//交换位置				//==冒泡排序
					iTemp = byCardList[i];
					byCardList[i] = byCardList[i + 1];
					byCardList[i + 1] = iTemp;

					iTemp = iStationVol[i];
					iStationVol[i] = iStationVol[i + 1];
					iStationVol[i + 1] = iTemp;
					bSorted = false;
				}
			}
			iLast--;
		} while (!bSorted);

		return true;
	}

	//同花色	
	bool CUpGradeGameLogic::IsSameHuaKind(BYTE byCardList[], int iCardCount)
	{
		if (iCardCount != 0x05)
		{
			return false;
		}

		//BYTE iFirstHua = 0x00;

		//iFirstHua = GetCardHuaKind(byCardList[0]); //取得第一张牌的花色

		////判断是否同一花色
		//for (int i = 0x00; i < iCardCount; i++)
		//{
		//	if (GetCardHuaKind(byCardList[i]) != iFirstHua)
		//	{
		//		return false;
		//	}
		//}
		INT newCount = 0;
		BYTE newCards[13] = { 0 };
		bool isSameFlower = FindSameFlower(byCardList, iCardCount, newCards, newCount);
		return isSameFlower;
	}

	//最小顺子
	bool CUpGradeGameLogic::IsSmallStraight(BYTE byCardList[], int iCardCount)
	{
		if (0x05 != iCardCount)
		{
			return false;
		}
		BYTE byCardData[5];
		memcpy(byCardData, byCardList, sizeof(byCardData));
		bool bCheckAFlag = true;
		for (int i = 0x00; i < 0x05; i++)
		{
			if (0x0E == GetCardNum(byCardData[i]))
			{
				byCardData[i] = byCardData[i] & 0xf0;			//将A变成最小牌
				bCheckAFlag = false;
				break;
			}
		}
		if (true == bCheckAFlag)			//如果不含有牌A,则不会是最小顺子
		{
			return false;
		}
		return IsSequence(byCardData, iCardCount);
	}

	//最小同花顺
	bool CUpGradeGameLogic::IsStraightSmallFlush(BYTE byCardList[], int iCardCount)
	{
		//同花？
		if (!IsSameHuaKind(byCardList, iCardCount))
		{
			return false;
		}
		//顺子？
		if (!IsSmallStraight(byCardList, iCardCount))
		{
			return false;
		}
		return true;
	}

	//同花顺,花色相同的顺子
	int CUpGradeGameLogic::CmpStraightFlush(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		if (iFirstCount != 0x05 || iSecondCount != 0x05)
		{
			return -1;
		}

		BYTE iFrstCardData = GetSingleMinData(byFirstCard, iFirstCount);
		BYTE iSecondCardData = GetSingleMinData(bySecondCard, iSecondCount);

		BYTE byFirstStraightValue = GetStraightCardValue(byFirstCard, iFirstCount);
		BYTE bySecondStraightValue = GetStraightCardValue(bySecondCard, iSecondCount);
		BYTE iCardResultList1[2] = {};
		BYTE iCardResultList2[2] = {};
		if (byFirstStraightValue == bySecondStraightValue)//点数相同
		{
			BYTE iCardCommon1 = 255;
			BYTE iCardCaiShen1 = 255;
			for (int i = 0; i < iFirstCount; i++)
			{
				if (!GetCardIsCaiShen(byFirstCard[i]))
				{
					if (iCardCommon1 == 255)
					{
						iCardCommon1 = byFirstCard[i];
					}
					else if (GetCardNum(byFirstCard[i])>GetCardNum(iCardCommon1))
					{
						iCardCommon1 = byFirstCard[i];
					}
				}
				else
				{
					if (iCardCaiShen1 == 255)
					{
						iCardCaiShen1 = byFirstCard[i];
					}
					else if (GetCardHuaKind(byFirstCard[i]) > GetCardHuaKind(iCardCaiShen1) ||
						GetCardNum(byFirstCard[i]) > GetCardNum(iCardCaiShen1))
					{
						iCardCaiShen1 = byFirstCard[i];
					}
				}
			}
			BYTE iCardCommon2 = 255;
			BYTE iCardCaiShen2 = 255;
			for (int i = 0; i < iSecondCount; i++)
			{
				if (!GetCardIsCaiShen(bySecondCard[i]))
				{
					if (iCardCommon2 == 255)
					{
						iCardCommon2 = bySecondCard[i];
					}
					else if (GetCardNum(bySecondCard[i])>GetCardNum(iCardCommon2))
					{
						iCardCommon2 = bySecondCard[i];
					}
				}
				else
				{
					if (iCardCaiShen2 == 255)
					{
						iCardCaiShen2 = bySecondCard[i];
					}
					else if (GetCardHuaKind(bySecondCard[i]) > GetCardHuaKind(iCardCaiShen2) ||
						GetCardNum(bySecondCard[i]) > GetCardNum(iCardCaiShen2))
					{
						iCardCaiShen2 = bySecondCard[i];
					}
				}
			}

			{
				//同花顺点数相同 直接比较花色
				return GetCardHuaKind(iCardCommon1) > GetCardHuaKind(iCardCommon2);
			}
		}
		else
		{
			return byFirstStraightValue - bySecondStraightValue > 0 ? 1 : -1;
		}
	}

	//取得牌最大的牌值(牌已经排序)
	BYTE CUpGradeGameLogic::GetMaxCardValue(BYTE byCardList[], int iCardCount)
	{
		if (iCardCount <= 0x00)
		{
			return 0xff;
		}
		return 0x0f & byCardList[0];
	}

	//取得最小单牌的牌
	BYTE CUpGradeGameLogic::GetSingleMinData(BYTE byCardList[], int iCardCount)
	{
		if (iCardCount <= 0x00)
		{
			return 0xff;
		}

		BYTE temp[17];
		int iCardValue = 0xff;
		memset(temp, 0x00, sizeof(temp));

		for (int i = 0x00; i < iCardCount; i++)
		{
			if (0xff != byCardList[i])
			{
				temp[GetCardNum(byCardList[i])]++;
			}
		}
		for (int i = 0x00; i < 17; i++)
		{
			if (temp[i] == 0x01)
			{
				iCardValue = i;
				break;
			}
		}

		for (int i = 0x00; i < iCardCount; i++)
		{
			if (GetCardNum(byCardList[i]) == iCardValue)
			{
				return byCardList[i];
			}
		}

		return 0xff;
	}

	//取得最大单牌的牌
	BYTE CUpGradeGameLogic::GetSingleMaxData(BYTE byCardList[], int iCardCount)
	{
		if (iCardCount <= 0x00)
		{
			return 0xff;
		}

		BYTE temp[17];
		int iCardValue = 0xff;
		memset(temp, 0x00, sizeof(temp));

		for (int i = 0x00; i < iCardCount; i++)
		{
			if (0xff != byCardList[i])
			{
				temp[GetCardNum(byCardList[i])]++;
			}
		}
		for (int i = 16; i >= 0x00; i--)
		{
			if (temp[i] == 0x01)
			{
				iCardValue = i;
				break;
			}
		}

		for (int i = 0x00; i < iCardCount; i++)
		{
			if (GetCardNum(byCardList[i]) == iCardValue)
			{
				return byCardList[i];
			}
		}

		return 0xff;
	}

	//同花最小顺子
	int CUpGradeGameLogic::CmpSmallStraightFlush(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		if (iFirstCount != 0x05 || iSecondCount != 0x05)
		{
			return -1;
		}

		if ((byFirstCard[0] & 0xf0) > (bySecondCard[0] & 0xf0))
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	//炸弹
	int CUpGradeGameLogic::CmpBomb(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		INT newCount1 = 0;
		BYTE newCards1[13] = { 0 };
		BYTE iCard1 = 255;
		BYTE iCardCaiShen1 = 255;
		FindFourStrip(byFirstCard, iFirstCount, newCards1, newCount1);
		for (int i = 0; i < newCount1; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(newCards1[i]))
			{
				if (iCard1 == 255)
				{
					iCard1 = newCards1[i];
				}
				else if (GetCardHuaKind(newCards1[i])>GetCardHuaKind(iCard1))
				{
					iCard1 = newCards1[i];
				}
			}
			else
			{
				if (iCardCaiShen1 == 255)
				{
					iCardCaiShen1 = newCards1[i];
				}
				else if (GetCardNum(newCards1[i]) >GetCardNum(iCardCaiShen1)||
					GetCardHuaKind(newCards1[i]) >GetCardHuaKind(iCardCaiShen1))
				{
					iCardCaiShen1 = newCards1[i];
				}
			}
		}

		INT newCount2 = 0;
		BYTE newCards2[13] = { 0 };
		BYTE iCard2 = 255;
		BYTE iCardCaiShen2 = 255;
		FindFourStrip(bySecondCard, iSecondCount, newCards2, newCount2);
		for (int i = 0; i < newCount2; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(newCards2[i]))
			{
				if (iCard2 == 255)
				{
					iCard2 = newCards2[i];
				}
				else if (GetCardHuaKind(newCards2[i])>GetCardHuaKind(iCard2))
				{
					iCard2 = newCards2[i];
				}
			}
			else
			{
				if (iCardCaiShen2 == 255)
				{
					iCardCaiShen2 = newCards2[i];
				}
				else if (GetCardNum(newCards2[i]) > GetCardNum(iCardCaiShen2) ||
					GetCardHuaKind(newCards2[i]) > GetCardHuaKind(iCardCaiShen2))
				{
					iCardCaiShen2 = newCards2[i];
				}
			}
		}
		if (GetCardNum(iCard1)!=GetCardNum(iCard2)) //大小不同 直接返回
		{
			return GetCardNum(iCard1) > GetCardNum(iCard2) ? true : false;
		}
		else//大小相同
		{
			//比较剩下的那单张牌
			int iFirstDanCard = 255;
			int iTwoDanCard = 255;
			for (int i = 0; i < 5; i++)
			{
				if (GetCardNum(iCard1) != GetCardNum(byFirstCard[i]) && !GetCardIsCaiShen(byFirstCard[i]))
				{
					iFirstDanCard = byFirstCard[i];
				}
				if (GetCardNum(iCard2) != GetCardNum(bySecondCard[i]) && !GetCardIsCaiShen(bySecondCard[i]))
				{
					iTwoDanCard = bySecondCard[i];
				}
			}
			if (GetCardNum(iFirstDanCard) != GetCardNum(iTwoDanCard))
			{
				return GetCardNum(iFirstDanCard) > GetCardNum(iTwoDanCard);
			}
			else
			{
				return GetCardHuaKind(iFirstDanCard) > GetCardHuaKind(iTwoDanCard);
			}
			return false;
			/*if (iCardCaiShen1 == 255 && iCardCaiShen2 == 255)
			{
				return GetCardHuaKind(iCard1) > GetCardHuaKind(iCard2);
			}
			else if (iCardCaiShen1 != 255 && iCardCaiShen2 != 255)
			{
				if (GetCardNum(iCardCaiShen1) == GetCardNum(iCardCaiShen2))
				{
					return GetCardHuaKind(iCardCaiShen1) > GetCardHuaKind(iCardCaiShen2);
				}
				else
				{
					return GetCardNum(iCardCaiShen1) > GetCardNum(iCardCaiShen2);
				}
			}
			else if (iCardCaiShen1 != 255)
			{
				return true;
			}
			else
			{
				return false;
			}*/
		}
		return false;
	}

	//比五同
	int CUpGradeGameLogic:: CmpFive(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		INT newCount1 = 0;
		BYTE newCards1[13] = { 0 };
		BYTE iCard1 = 255;
		BYTE iCardCaiShen1 = 255;
		FindFiveStrip(byFirstCard, iFirstCount, newCards1, newCount1);
		for (int i = 0; i < newCount1; i++)//找到花色最大的那张非财神
		{
			if (!GetCardIsCaiShen(newCards1[i]))
			{
				if (iCard1 == 255)
				{
					iCard1 = newCards1[i];
				}
				else if (GetCardHuaKind(newCards1[i])>GetCardHuaKind(iCard1))
				{
					iCard1 = newCards1[i];
				}
			}
			else
			{
				if (iCardCaiShen1 == 255)
				{
					iCardCaiShen1 = newCards1[i];
				}
				else if (GetCardNum(newCards1[i]) > GetCardNum(iCardCaiShen1) ||
					GetCardHuaKind(newCards1[i]) > GetCardHuaKind(iCardCaiShen1))
				{
					iCardCaiShen1 = newCards1[i];
				}
			}
		}

		INT newCount2 = 0;
		BYTE newCards2[13] = { 0 };
		BYTE iCard2 = 255;
		BYTE iCardCaiShen2 = 255;
		FindFiveStrip(bySecondCard, iSecondCount, newCards2, newCount2);
		for (int i = 0; i < newCount2; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(newCards2[i]))
			{
				if (iCard2 == 255)
				{
					iCard2 = newCards2[i];
				}
				else if (GetCardHuaKind(newCards2[i])>GetCardHuaKind(iCard2))
				{
					iCard2 = newCards2[i];
				}
			}
			else
			{
				if (iCardCaiShen2 == 255)
				{
					iCardCaiShen2 = newCards2[i];
				}
				else if (GetCardNum(newCards2[i]) > GetCardNum(iCardCaiShen2) ||
					GetCardHuaKind(newCards2[i]) > GetCardHuaKind(iCardCaiShen2))
				{
					iCardCaiShen2 = newCards2[i];
				}
			}
		}

		if (GetCardNum(iCard1) != GetCardNum(iCard2))
		{
			return GetCardNum(iCard1) > GetCardNum(iCard2) ? true : false;
		}
		else
		{
			if (iCardCaiShen1 == 255 && iCardCaiShen2 == 255)
			{
				return GetCardHuaKind(iCard1) > GetCardHuaKind(iCard2);
			}
			else if (iCardCaiShen1 != 255 && iCardCaiShen2 != 255)
			{
				if (GetCardNum(iCardCaiShen1) == GetCardNum(iCardCaiShen2))
				{
					return GetCardHuaKind(iCardCaiShen1) > GetCardHuaKind(iCardCaiShen2);
				}
				else
				{
					return GetCardNum(iCardCaiShen1) > GetCardNum(iCardCaiShen2);
				}
			}
			else if (iCardCaiShen1 != 255)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	//三带二
	int CUpGradeGameLogic::CmpThreeDouble(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		//找到那3张进行比较
		INT iFirCardNewCount = 0;
		BYTE iFirCardNewCards[13] = { 0 };
		BYTE iCardCommon1 = 255;
		BYTE iCardCaiShen1 = 255;
		FindThreeStrip(byFirstCard, iFirstCount, iFirCardNewCards, iFirCardNewCount);
		for (int i = 0; i < iFirCardNewCount; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(iFirCardNewCards[i]))
			{
				if (iCardCommon1 == 255)
				{
					iCardCommon1 = iFirCardNewCards[i];
				}
				else if (GetCardHuaKind(iFirCardNewCards[i])>GetCardHuaKind(iCardCommon1))
				{
					iCardCommon1 = iFirCardNewCards[i];
				}
			}
			else
			{
				if (iCardCaiShen1 == 255)
				{
					iCardCaiShen1 = iFirCardNewCards[i];
				}
				else if (GetCardHuaKind(iFirCardNewCards[i]) > GetCardHuaKind(iCardCaiShen1) ||
					GetCardNum(iFirCardNewCards[i]) > GetCardNum(iCardCaiShen1))
				{
					iCardCaiShen1 = iFirCardNewCards[i];
				}
			}
		}

		INT iSecCardNewCount = 0;
		BYTE iSecCardNewCards[13] = { 0 };
		BYTE iCardCommon2 = 255;
		BYTE iCardCaiShen2 = 255;
		FindThreeStrip(bySecondCard, iSecondCount, iSecCardNewCards, iSecCardNewCount);
		for (int i = 0; i < iSecCardNewCount; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(iSecCardNewCards[i]))
			{
				if (iCardCommon2 == 255)
				{
					iCardCommon2 = iSecCardNewCards[i];
				}
				else if (GetCardHuaKind(iSecCardNewCards[i])>GetCardHuaKind(iCardCommon2))
				{
					iCardCommon2 = iSecCardNewCards[i];
				}
			}
			else
			{
				if (iCardCaiShen2 == 255)
				{
					iCardCaiShen2 = iSecCardNewCards[i];
				}
				else if (GetCardHuaKind(iSecCardNewCards[i]) > GetCardHuaKind(iCardCaiShen2) ||
					GetCardNum(iSecCardNewCards[i]) > GetCardNum(iCardCaiShen2))
				{
					iCardCaiShen2 = iSecCardNewCards[i];
				}
			}
		}
		//大小不相同下 直接比较非财神的大小了
		if (GetCardNum(iCardCommon1) != GetCardNum(iCardCommon2))
		{
			return GetCardNum(iCardCommon1) > GetCardNum(iCardCommon2);
		}
		else//大小相同
		{
			//比较对子
			BYTE iFirCardTwoCard[2] = { 0 };
			BYTE iFirCardTwoCardCount = 0;
			for (int i = 0; i < 5; i++)
			{
				bool findSame = false;
				for (int j = 0; j < 3; j++)
				{
					if (byFirstCard[i] == iFirCardNewCards[j])
					{
						findSame = true;
					}
				}
				if (!findSame)
				{
					iFirCardTwoCard[iFirCardTwoCardCount++] = byFirstCard[i];
				}
			}
			BYTE iSecCardTwoCard[2] = { 0 };
			BYTE iSecCardTwoCardCount = 0;
			for (int i = 0; i < 5; i++)
			{
				bool findSame = false;
				for (int j = 0; j < 3; j++)
				{
					if (bySecondCard[i] == iSecCardNewCards[j])
					{
						findSame = true;
					}
				}
				if (!findSame)
				{
					iSecCardTwoCard[iSecCardTwoCardCount++] = bySecondCard[i];
				}
			}
			if (GetCardNum(iFirCardTwoCard[0]) != GetCardNum(iSecCardTwoCard[0])) //两张的大小不同
			{
				return	GetCardNum(iFirCardTwoCard[0]) > GetCardNum(iSecCardTwoCard[0]);
			}
			else
			{
				if (iCardCaiShen1 == 255 && iCardCaiShen2 == 255)
				{
					return GetCardHuaKind(iCardCommon1) > GetCardHuaKind(iCardCommon2);
				}
				else if (iCardCaiShen1 != 255 && iCardCaiShen2 != 255)
				{
					if (GetCardNum(iCardCaiShen1) == GetCardNum(iCardCaiShen2))
					{
						return GetCardHuaKind(iCardCaiShen1) > GetCardHuaKind(iCardCaiShen2);
					}
					else
					{
						return GetCardNum(iCardCaiShen1) > GetCardNum(iCardCaiShen2);
					}
				}
				else if (iCardCaiShen1 != 255)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		return false;
	}

	//同花
	int CUpGradeGameLogic::CmpFlush(BYTE iFirCard[], int iFirstCount, BYTE iSecCard[], int iSecondCount)
	{
		//同花
		BYTE iNewFirCard[5] = { 0 };
		BYTE iNewSecCard[5] = { 0 };
		int cardCount = 0;
		BYTE iCardCaiShen1 = 255;
		
		for (int i = 0; i < 5; i++)
		{
			if (!GetCardIsCaiShen(iFirCard[i]))
			{
				iNewFirCard[cardCount] = iFirCard[i];
				cardCount++;
			}
			else
			{
				if (iCardCaiShen1 == 255)
				{
					iCardCaiShen1 = iFirCard[i];
				}
				else if (GetCardNum(iFirCard[i]) > GetCardNum(iCardCaiShen1) ||
					GetCardHuaKind(iFirCard[i]) > GetCardHuaKind(iCardCaiShen1))
				{
					iCardCaiShen1 = iFirCard[i];
				}
			}
		}

		cardCount = 0;
		BYTE iCardCaiShen2 = 255;
		for (int i = 0; i < 5; i++)
		{
			if (!GetCardIsCaiShen(iSecCard[i]))
			{
				iNewSecCard[cardCount] = iSecCard[i];
				cardCount++;
			}
			else
			{
				if (iCardCaiShen2 == 255)
				{
					iCardCaiShen2 = iSecCard[i];
				}
				else if (GetCardNum(iSecCard[i]) > GetCardNum(iCardCaiShen2) ||
					GetCardHuaKind(iSecCard[i]) > GetCardHuaKind(iCardCaiShen2))
				{
					iCardCaiShen2 = iSecCard[i];
				}
			}
		}

		//把财神牌换成 A
		for (int i = 0; i < 5; i++)
		{
			if (GetCardIsCaiShen(iFirCard[i]))
			{
				iFirCard[i] = 0X3D;
			}
			if (GetCardIsCaiShen(iSecCard[i]))
			{
				iSecCard[i] = 0X3D;
			}
		}
		SortCardData(iFirCard, iFirstCount);
		SortCardData(iSecCard, iSecondCount);


		for (int i = 0; i < 5; i++)
		{
			if (iFirCard[i] && iSecCard[i])
			{
				if (GetCardNum(iFirCard[i]) != GetCardNum(iSecCard[i]))
				{
					return GetCardNum(iFirCard[i]) > GetCardNum(iSecCard[i]);
				}
				else
				{
					continue;
				}
			}
		}
		return GetCardHuaKind(iNewFirCard[0]) > GetCardHuaKind(iNewSecCard[0]);

	}

	//顺子
	int CUpGradeGameLogic::CmpStraight(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		if (iFirstCount != 0x05 || iSecondCount != 0x05)
		{
			return -1;
		}

		BYTE iFrstCardData = GetSingleMinData(byFirstCard, iFirstCount);
		BYTE iSecondCardData = GetSingleMinData(bySecondCard, iSecondCount);

		BYTE byFirstStraightValue = GetStraightCardValue(byFirstCard, iFirstCount);
		BYTE bySecondStraightValue = GetStraightCardValue(bySecondCard, iSecondCount);
		BYTE iCardResultList1[2] = {};
		BYTE iCardResultList2[2] = {};
		if (byFirstStraightValue == bySecondStraightValue)//点数相同
		{
			BYTE iCardCommon1 = 255;
			BYTE iCardCaiShen1 = 255;
			for (int i = 0; i < iFirstCount; i++)
			{
				if (!GetCardIsCaiShen(byFirstCard[i]))
				{
					if (iCardCommon1 == 255)
					{
						iCardCommon1 = byFirstCard[i];
					}
					else if (GetCardNum(byFirstCard[i])>GetCardNum(iCardCommon1))
					{
						iCardCommon1 = byFirstCard[i];
					}
				}
				else
				{
					if (iCardCaiShen1 == 255)
					{
						iCardCaiShen1 = byFirstCard[i];
					}
					else if (GetCardHuaKind(byFirstCard[i]) > GetCardHuaKind(iCardCaiShen1) ||
						GetCardNum(byFirstCard[i]) > GetCardNum(iCardCaiShen1))
					{
						iCardCaiShen1 = byFirstCard[i];
					}
				}
			}
			BYTE iCardCommon2 = 255;
			BYTE iCardCaiShen2 = 255;
			for (int i = 0; i < iSecondCount; i++)
			{
				if (!GetCardIsCaiShen(bySecondCard[i]))
				{
					if (iCardCommon2 == 255)
					{
						iCardCommon2 = bySecondCard[i];
					}
					else if (GetCardNum(bySecondCard[i])>GetCardNum(iCardCommon2))
					{
						iCardCommon2 = bySecondCard[i];
					}
				}
				else
				{
					if (iCardCaiShen2 == 255)
					{
						iCardCaiShen2 = bySecondCard[i];
					}
					else if (GetCardHuaKind(bySecondCard[i]) > GetCardHuaKind(iCardCaiShen2) ||
						GetCardNum(bySecondCard[i]) > GetCardNum(iCardCaiShen2))
					{
						iCardCaiShen2 = bySecondCard[i];
					}
				}
			}

			{
				if (iCardCaiShen1 == 255 && iCardCaiShen2 == 255)
				{
					if (GetCardNum(iCardCommon1) == GetCardNum(iCardCommon2))
					{
						auto hua1 = GetCardHuaKind(iCardCommon1);
						auto hua2 = GetCardHuaKind(iCardCommon2);
						return  hua1 > hua2;
					}
					return GetCardNum(iCardCommon1) > GetCardNum(iCardCommon2);
				}
				else
				{
					auto indexNum = (byFirstStraightValue == 80 || byFirstStraightValue == 100) ? 14 : GetCardNum(iFrstCardData) + 4;
					
					if (finOneCard(byFirstCard, iFirstCount, indexNum) && finOneCard(bySecondCard, iSecondCount, indexNum))
					{
						return GetCardHuaKind(iCardCommon1) > GetCardHuaKind(iCardCommon2);
					}
					else if (!finOneCard(byFirstCard, iFirstCount, indexNum) && !finOneCard(bySecondCard, iSecondCount, indexNum))
					{
						if (GetCardNum(iCardCaiShen1) != GetCardNum(iCardCaiShen2))
						{
							return GetCardNum(iCardCaiShen1) > GetCardNum(iCardCaiShen2);
						}
						else
						{
							return GetCardHuaKind(iCardCaiShen1) > GetCardHuaKind(iCardCaiShen2);
						}
					}
					else if (finOneCard(byFirstCard, iFirstCount, indexNum) && finOneCard(bySecondCard, iSecondCount, indexNum))
					{
						return GetCardHuaKind(iCardCommon1) > GetCardHuaKind(iCardCommon1);
					}
					else if (finOneCard(byFirstCard, iFirstCount, indexNum))
					{
						return false;
					}
					else
					{
						return true;
					}
				}
			}
		}
		else
		{
			return byFirstStraightValue - bySecondStraightValue > 0 ? 1 : -1;
		}
	}


	//最小顺子
	int CUpGradeGameLogic::CmpSmallStraight(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		if (iFirstCount != 0x05 || iSecondCount != 0x05)
		{
			return -1;
		}

		BYTE byFirstHandlTemp[5];
		BYTE bySecondHandlTemp[5];

		for (int i = 0x00; i < iFirstCount; i++)
		{
			if ((byFirstCard[i] & 0x0f) == 0x0D)
			{
				byFirstHandlTemp[i] = byFirstCard[i] & 0xf0;
			}
			else
			{
				byFirstHandlTemp[i] = byFirstCard[i];
			}

			if ((bySecondCard[i] & 0x0f) == 0x0D)
			{
				bySecondHandlTemp[i] = bySecondCard[i] & 0xf0;
			}
			else
			{
				bySecondHandlTemp[i] = bySecondCard[i];
			}
		}

		int iFrstCardData = GetSingleMaxData(byFirstHandlTemp, iFirstCount);
		int iSecondCardData = GetSingleMaxData(bySecondHandlTemp, iSecondCount);

		if ((iFrstCardData & 0x0f) > (iSecondCardData & 0x0f))
		{
			return 1;
		}
		else if ((iFrstCardData & 0x0f) < (iSecondCardData & 0x0f))
		{
			return -1;
		}
		else
		{
			if ((iFrstCardData & 0xf0) > (iSecondCardData & 0xf0))
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
		return -1;
	}

	//三张
	int CUpGradeGameLogic::CmpThree(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount)
	{
		//找到那3张进行比较
		INT iFirCardNewCount = 0;
		BYTE iFirCardNewCards[13] = { 0 };
		BYTE iCardCommon1 = 255;
		BYTE iCardCaiShen1 = 255;
		FindThreeStrip(byFirstCard, iFirstCount, iFirCardNewCards, iFirCardNewCount);
		for (int i = 0; i < iFirCardNewCount; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(iFirCardNewCards[i]))
			{
				if (iCardCommon1 == 255)
				{
					iCardCommon1 = iFirCardNewCards[i];
				}
				else if (GetCardHuaKind(iFirCardNewCards[i])>GetCardHuaKind(iCardCommon1))
				{
					iCardCommon1 = iFirCardNewCards[i];
				}
			}
			else
			{
				if (iCardCaiShen1 == 255)
				{
					iCardCaiShen1 = iFirCardNewCards[i];
				}
				else if (GetCardHuaKind(iFirCardNewCards[i])>GetCardHuaKind(iCardCaiShen1) || 
					GetCardNum(iFirCardNewCards[i])>GetCardNum(iCardCaiShen1))
				{
					iCardCaiShen1 = iFirCardNewCards[i];
				}
			}
		}

		INT iSecCardNewCount = 0;
		BYTE iSecCardNewCards[13] = { 0 };
		BYTE iCardCommon2 = 255;
		BYTE iCardCaiShen2 = 255;
		FindThreeStrip(bySecondCard, iSecondCount, iSecCardNewCards, iSecCardNewCount);
		for (int i = 0; i < iSecCardNewCount; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(iSecCardNewCards[i]))
			{
				if (iCardCommon2 == 255)
				{
					iCardCommon2 = iSecCardNewCards[i];
				}
				else if (GetCardHuaKind(iSecCardNewCards[i])>GetCardHuaKind(iCardCommon2))
				{
					iCardCommon2 = iSecCardNewCards[i];
				}
			}
			else
			{
				if (iCardCaiShen2 == 255)
				{
					iCardCaiShen2 = iSecCardNewCards[i];
				}
				else if (GetCardHuaKind(iSecCardNewCards[i]) > GetCardHuaKind(iCardCaiShen2) ||
					GetCardNum(iSecCardNewCards[i]) > GetCardNum(iCardCaiShen2))
				{
					iCardCaiShen2 = iSecCardNewCards[i];
				}
			}
		}
		if (GetCardIsCaiShen(iFirCardNewCards[0]) && GetCardIsCaiShen(iFirCardNewCards[1])
			&& GetCardIsCaiShen(iFirCardNewCards[2]))//头道3个财神组成的3条
		{
			if (finOneCard(iSecCardNewCards, 3, 14) && iCardCaiShen2 != 255)//中道有A
			{
				//比最大的那张财神牌
				if (GetCardNum(iCardCaiShen1) != GetCardNum(iCardCaiShen2))
				{
					return GetCardNum(iCardCaiShen1) > GetCardNum(iCardCaiShen2);
				}
				else
				{
					return GetCardHuaKind(iCardCaiShen1) > GetCardHuaKind(iCardCaiShen2);
				}
			}
			else
			{
				return true;
			}
		}
		//大小不相同下 直接比较非财神的大小了
		if (GetCardNum(iCardCommon1) != GetCardNum(iCardCommon2))
		{
			return GetCardNum(iCardCommon1) > GetCardNum(iCardCommon2);
		}
		else//大小相同
		{
			if (iFirstCount == 3) //头道只有三张了 比不赢了
			{
				return false;
			}
			else
			{
				int iFirstDanCard = 255;
				int iTwoDanCard = 255;
				for (int i = 0; i < 5; i++)
				{
					if (GetCardNum(iCardCommon1) != GetCardNum(byFirstCard[i]) && !GetCardIsCaiShen(byFirstCard[i]))
					{
						if (iFirstDanCard == 255)
						{
							iFirstDanCard = byFirstCard[i];
						}
						else if (iFirstDanCard != 255 && byFirstCard[i] != 255
							&& GetCardNum(byFirstCard[i] > GetCardNum(iFirstDanCard)))
						{
							iFirstDanCard = byFirstCard[i];
						}
					}
					if (GetCardNum(iCardCommon2) != GetCardNum(bySecondCard[i]) && !GetCardIsCaiShen(bySecondCard[i]))
					{
						if (iTwoDanCard == 255)
						{
							iTwoDanCard = bySecondCard[i];
						}
						else if (iTwoDanCard != 255 && bySecondCard[i] != 255
							&& GetCardNum(bySecondCard[i] > GetCardNum(iTwoDanCard)))
						{
							iTwoDanCard = bySecondCard[i];
						}
					}
				}
				if (GetCardNum(iFirstDanCard) != GetCardNum(iTwoDanCard))
				{
					return GetCardNum(iFirstDanCard) > GetCardNum(iTwoDanCard);
				}
				else
				{
					return GetCardHuaKind(iFirstDanCard) > GetCardHuaKind(iTwoDanCard);
				}
			}
			return false;
			//if (iCardCaiShen1 == 255 && iCardCaiShen2 == 255)//都没财神
			//{
			//	return GetCardHuaKind(iCardCommon1) > GetCardHuaKind(iCardCommon2);
			//}
			//else if (iCardCaiShen1 != 255 && iCardCaiShen2 != 255)//都有财神
			//{
			//	if (GetCardNum(iCardCaiShen1) == GetCardNum(iCardCaiShen2))
			//	{
			//		return GetCardHuaKind(iCardCaiShen1) > GetCardHuaKind(iCardCaiShen2);
			//	}
			//	else
			//	{
			//		return GetCardNum(iCardCaiShen1) > GetCardNum(iCardCaiShen2);
			//	}
			//}
			//else if (iCardCaiShen1 != 255)
			//{
			//	return true;
			//}
			//else
			//{
			//	return false;
			//}
		}
	}

	//两对
	int CUpGradeGameLogic::CmpTwoDouble(BYTE iFirCard[], int iFirstCount, BYTE iSecCard[], int iSecondCount)
	{
		// 第一对比较
		int pd1 = GetCardNum(iFirCard[0]),
			pd2 = GetCardNum(iSecCard[0]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		// 第二对比较
		pd1 = GetCardNum(iFirCard[2]);
		pd2 = GetCardNum(iSecCard[2]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		// 比第5张牌
		pd1 = GetCardNum(iFirCard[4]);
		pd2 = GetCardNum(iSecCard[4]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		pd1 = GetCardHuaKind(GetSingleMaxData(iFirCard, iFirstCount));
		pd2 = GetCardHuaKind(GetSingleMaxData(iSecCard, iSecondCount));
		return (pd1 - pd2 > 0 ? 1 : -1);
	}

	//一对
	int CUpGradeGameLogic::CmpDouble(BYTE iFirCard[], int iFirstCount, BYTE iSecCard[], int iSecondCount)
	{
		//找到那一对进行比较
		INT iFirCardNewCount = 0;
		BYTE iFirCardNewCards[13] = { 0 };
		BYTE iCardCommon1 = 255;
		BYTE iCardCaiShen1 = 255;
		FindOneDouble(iFirCard, iFirstCount, iFirCardNewCards, iFirCardNewCount);
		for (int i = 0; i < iFirCardNewCount; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(iFirCardNewCards[i]))
			{
				if (iCardCommon1 == 255)
				{
					iCardCommon1 = iFirCardNewCards[i];
				}
				else if (GetCardHuaKind(iFirCardNewCards[i])>GetCardHuaKind(iCardCommon1))
				{
					iCardCommon1 = iFirCardNewCards[i];
				}
			}
			else
			{
				iCardCaiShen1 = iFirCardNewCards[i];
			}
		}

		INT iSecCardNewCount = 0;
		BYTE iSecCardNewCards[13] = { 0 };
		BYTE iCardCommon2 = 255;
		BYTE iCardCaiShen2 = 255;
		FindOneDouble(iSecCard, iSecondCount, iSecCardNewCards, iSecCardNewCount);
		for (int i = 0; i < iSecCardNewCount; i++)//找到花色最大的那张非王牌
		{
			if (!GetCardIsCaiShen(iSecCardNewCards[i]))
			{
				if (iCardCommon2 == 255)
				{
					iCardCommon2 = iSecCardNewCards[i];
				}
				else if (GetCardHuaKind(iSecCardNewCards[i])>GetCardHuaKind(iCardCommon2))
				{
					iCardCommon2 = iSecCardNewCards[i];
				}
			}
			else
			{
				iCardCaiShen2 = iSecCardNewCards[i];
			}
		}
		//大小不相同下 直接比较非财神的大小了
		if (GetCardNum(iCardCommon1) != GetCardNum(iCardCommon2))
		{
			return GetCardNum(iCardCommon1) > GetCardNum(iCardCommon2);
		}
		else//大小相同
		{
			int iFirstDanCard = 255;
			int iTwoDanCard = 255;
			for (int i = 0; i < 5; i++)
			{
				if (GetCardNum(iCardCommon1) != GetCardNum(iFirCard[i]) && !GetCardIsCaiShen(iFirCard[i]))
				{
					if (iFirstDanCard == 255)
					{
						iFirstDanCard = iFirCard[i];
					}
					else if (iFirstDanCard != 255 && iFirCard[i] != 255
						&& GetCardNum(iFirCard[i] > GetCardNum(iFirstDanCard)))
					{
						iFirstDanCard = iFirCard[i];
					}
				}
				if (GetCardNum(iCardCommon2) != GetCardNum(iSecCard[i]) && !GetCardIsCaiShen(iSecCard[i]))
				{
					if (iTwoDanCard == 255)
					{
						iTwoDanCard = iSecCard[i];
					}
					else if (iTwoDanCard != 255 && iSecCard[i] != 255
						&& GetCardNum(iSecCard[i] > GetCardNum(iTwoDanCard)))
					{
						iTwoDanCard = iSecCard[i];
					}
				}
			}
			if (GetCardNum(iFirstDanCard) != GetCardNum(iTwoDanCard))
			{
				return GetCardNum(iFirstDanCard) > GetCardNum(iTwoDanCard);
			}
			else
			{
				return GetCardHuaKind(iFirstDanCard) > GetCardHuaKind(iTwoDanCard);
			}
			/*if (iCardCaiShen1 == 255 && iCardCaiShen2 == 255)
			{
			return GetCardHuaKind(iCardCommon1) > GetCardHuaKind(iCardCommon2);
			}
			else if (iCardCaiShen1 != 255 && iCardCaiShen2 != 255)
			{
			if (GetCardNum(iCardCaiShen1) == GetCardNum(iCardCaiShen2))
			{
			return GetCardHuaKind(iCardCaiShen1) > GetCardHuaKind(iCardCaiShen2);
			}
			else
			{
			return GetCardNum(iCardCaiShen1) > GetCardNum(iCardCaiShen2);
			}
			}
			else if (iCardCaiShen1 != 255)
			{
			return true;
			}
			else
			{
			return false;
			}*/
		}
	}

	//比单张
	int CUpGradeGameLogic::CmpOne(BYTE iFirCard[], int iFirstCount, BYTE iSecCard[], int iSecondCount)
	{
		/*SortCard(iFirCard,NULL,iFirstCount);
		SortCard(iSecCard,NULL,iSecondCount);*/

		// 比第1张牌
		int pd1 = GetCardNum(iFirCard[0]),
			pd2 = GetCardNum(iSecCard[0]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		// 比第2张牌
		pd1 = GetCardNum(iFirCard[1]);
		pd2 = GetCardNum(iSecCard[1]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		// 比第3张牌
		pd1 = GetCardNum(iFirCard[2]);
		pd2 = GetCardNum(iSecCard[2]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		if (iFirstCount == 3)
		{
			pd1 = GetCardHuaKind(iFirCard[0]);
			pd2 = GetCardHuaKind(iSecCard[0]);
			return (pd1 - pd2 > 0 ? 1 : -1);
		}
		// 比第4张牌
		pd1 = GetCardNum(iFirCard[3]);
		pd2 = GetCardNum(iSecCard[3]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		// 比第5张牌
		pd1 = GetCardNum(iFirCard[4]);
		pd2 = GetCardNum(iSecCard[4]);
		if (pd1 != pd2)
		{
			return (pd1 - pd2 > 0 ? 1 : -1);
		}

		pd1 = GetCardHuaKind(iFirCard[0]);
		pd2 = GetCardHuaKind(iSecCard[0]);
		return (pd1 - pd2 > 0 ? 1 : -1);
	}

	//取得几张相同牌的牌值
	int  CUpGradeGameLogic::GetSameCardValue(BYTE byCardList[], int iCardCount, int iSameCardNum)
	{
		BYTE temp[17];
		memset(temp, 0x00, sizeof(temp));

		for (int i = 0x00; i < iCardCount; i++)
		{
			if (0xff != byCardList[i])
			{
				temp[GetCardNum(byCardList[i])]++;
			}
		}
		for (int i = 0x00; i < 17; i++)
		{
			if (iSameCardNum == temp[i])
			{
				return i;
			}
		}
		return 0xff;
	}

	//取得几张相同的牌值同时把牌删了
	int  CUpGradeGameLogic::GetSameCardValueAndClearThisCardData(BYTE byCardList[], int iCardCount, int iSameCardNum)
	{
		int iCardValue = 0xff;
		BYTE temp[17];
		memset(temp, 0x00, sizeof(temp));

		for (int i = 0x00; i < iCardCount; i++)
		{
			if (0xff != byCardList[i])
			{
				temp[GetCardNum(byCardList[i])]++;
			}
		}
		for (int i = 0x00; i < 17; i++)
		{
			if (iSameCardNum == temp[i])
			{
				iCardValue = i;
				break;
			}
		}
		for (int i = 0x00; i < iCardCount; i++)
		{
			if (iCardValue == GetCardNum(byCardList[i]))
			{
				byCardList[i] = 0xff;
			}
		}

		return iCardValue;
	}

	//三条
	bool CUpGradeGameLogic::IsSameThree(BYTE byCardList[], int iCardCount)
	{
		BYTE temp[17];
		memset(temp, 0x00, sizeof(temp));

		//含三张标志
		bool bThreeFlage = false;

		//含两张标志
		bool bTwoFlag = false;

		for (int i = 0x00; i < iCardCount; i++)
		{
			temp[GetCardNum(byCardList[i])]++;
		}
		for (int i = 0x00; i < 17; i++)
		{
			if (0x03 == temp[i])
			{
				return true;
			}
		}
		return false;
	}

	//两对
	bool CUpGradeGameLogic::IsTwoDoubleNums(BYTE byCardList[], int iCardCount)
	{
		if (0x05 != iCardCount)
		{
			return false;
		}
		BYTE temp[17];
		memset(temp, 0x00, sizeof(temp));

		int iDoubleNum = 0x00;
		for (int i = 0x00; i < iCardCount; i++)
		{
			temp[GetCardNum(byCardList[i])]++;
		}
		for (int i = 0x00; i < 17; i++)
		{
			if (0x02 == temp[i])
			{
				iDoubleNum++;
			}
		}

		if (iDoubleNum == 0x02)
		{
			return true;
		}

		return false;
	}

	//一对
	bool CUpGradeGameLogic::IsDoubleNums(BYTE byCardList[], int iCardCount)
	{
		BYTE temp[17];
		memset(temp, 0x00, sizeof(temp));

		int iDoubleNum = 0x00;
		for (int i = 0x00; i < iCardCount; i++)
		{
			temp[GetCardNum(byCardList[i])]++;
		}
		for (int i = 0x00; i < 17; i++)
		{
			if (0x02 == temp[i])
			{
				return true;
			}
		}
		return false;
	}

	//重新排序
	bool CUpGradeGameLogic::ReSortCard(BYTE iCardList[], int iCardCount)
	{
		SortCard(iCardList, NULL, iCardCount);
		// 按牌形排大小
		int iStationVol[45];
		for (int i = 0; i < iCardCount; i++)
		{
			iStationVol[i] = GetCardBulk(iCardList[i], false);
		}

		int Start = 0;
		int j, step;
		BYTE CardTemp[8];			// 用来保存要移位的牌形
		int CardTempVal[8];			// 用来保存移位的牌面值
		for (int i = 8; i > 1; i--)	// 在数组中找一个连续i张相同的值
		{
			for (j = Start; j < iCardCount; j++)
			{
				CardTemp[0] = iCardList[j];								// 保存当前i个数组相同的值
				CardTempVal[0] = iStationVol[j];
				for (step = 1; step < i && j + step < iCardCount;)	// 找一个连续i个值相等的数组(并保存于临时数组中)
				{
					if (iStationVol[j] == iStationVol[j + step])
					{
						CardTemp[step] = iCardList[j + step];		// 用来保存牌形
						CardTempVal[step] = iStationVol[j + step];	// 面值
						step++;
					}
					else
						break;
				}

				if (step >= i)						// 找到一个连续i个相等的数组串起始位置为j,结束位置为j+setp-1
				{									// 将从Start开始到j个数组后移setp个
					if (j != Start)					// 排除开始就是有序
					{
						for (; j >= Start; j--)		// 从Start张至j张后移动i张
						{
							iCardList[j + i - 1] = iCardList[j - 1];
							iStationVol[j + i - 1] = iStationVol[j - 1];
						}
						for (int k = 0; k < i; k++)
						{
							iCardList[Start + k] = CardTemp[k];	// 从Start开始设置成CardSave
							iStationVol[Start + k] = CardTempVal[k];
						}
					}
					Start = Start + i;
				}
				j = j + step - 1;
			}
		}
		return true;
	}

	//获取指定数组中王牌数量
	BYTE CUpGradeGameLogic::GetKingCard(BYTE iCardList[], int iCardCount, BYTE iCardResultList[])
	{
		int iCount = 0;
		for (int i = 0; i < iCardCount; ++i)
		{
			if (iCardList[i] == 0x6E)
			{
				iCardResultList[iCount] = 0x6E;
				iCount++;
			}
			if (iCardList[i] == 0x6F)
			{
				iCardResultList[iCount] = 0x6F;
				iCount++;
			}
		}
		return iCount;
	}

	//获取牌是不是王牌
	bool CUpGradeGameLogic::GetCardIsKing(BYTE iCard)
	{
		if (iCard == 0x6E || iCard == 0x6F)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//获取指定数组中花牌(癞子)数量
	BYTE CUpGradeGameLogic::GetHuaCard(BYTE iCardList[], int iCardCount, BYTE iCardResultList[])
	{
		int iCount = 0;
		for (int i = 0; i < iCardCount; ++i)
		{
			if (iCardList[i] == 0x5E || iCardList[i] == 0x4E || iCardList[i] == 0x3E || iCardList[i] == 0x2E || iCardList[i] == 0x1E || iCardList[i] == 0x0E)
			{
				iCardResultList[iCount] = iCardList[i];
				iCount++;
			}
		}
		return iCount;
	}

	//获取牌是不是花牌(癞子)
	bool CUpGradeGameLogic::GetCardIsHua(BYTE iCard)
	{
		if (iCard == 0x0E || iCard == 0x1E || iCard == 0x2E || iCard == 0x3E || iCard == 0x4E || iCard == 0x5E)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//获取这张牌能不能当财神用
	bool CUpGradeGameLogic::GetCardIsCaiShen(BYTE iCard)
	{
		if ((GetCardIsHua(iCard) || GetCardIsKing(iCard) ) &&  m_bKingCanReplace)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//把财神牌王和花牌(癞子)移动到最后面
	void CUpGradeGameLogic::sortCardListForFindAllType(BYTE cardList[],int cardCount, BYTE sortResult[], int &sortCount)
	{
		for (int i = 0; i < cardCount; i++)
		{
			if (!GetCardIsKing(cardList[i]) && !GetCardIsHua(cardList[i]))
			{
				sortResult[sortCount] = cardList[i];
				sortCount++;
			}
		}
		BYTE kingResult[2] = { 0 };
		BYTE kingCount = 0;
		kingCount = GetKingCard(cardList, cardCount, kingResult);
		for (int i = 0; i < kingCount; i++)
		{
			sortResult[sortCount] = kingResult[i];
			sortCount++;
		}

		BYTE huaResult[6] = { 0 };
		BYTE huaCount = 0;
		huaCount = GetHuaCard(cardList, cardCount, huaResult);
		for (int i = 0; i < huaCount; i++)
		{
			sortResult[sortCount] = huaResult[i];
			sortCount++;
		}
	}

	//获取顺子的值
	BYTE CUpGradeGameLogic::GetStraightCardValue(BYTE iHandCard[], int iHandCardCount)
	{
		BYTE temp[17] = { 0 };
		BYTE iCardResultList[2] = {};
		int iSum = 0;
		memset(temp, 0, sizeof(BYTE)* 17);
		for (int i = 0; i < iHandCardCount; i++)
		{
			if (GetCardNum(iHandCard[i]) == 14)
				temp[1]++;

			temp[GetCardNum(iHandCard[i])]++;
			iSum += GetCardNum(iHandCard[i]);
		}

		int iKingCount = 0, iKingCountTmp = 0;
		iKingCount = GetKingCard(iHandCard, iHandCardCount, iCardResultList);
		int iHuaCount = GetHuaCount(iHandCard,iHandCardCount);

		if (temp[14] != 0 || (m_bKingCanReplace && (iKingCount + iHuaCount > 0)))//有10 j q k a这个值，
		{
			bool bFlag = true;
			int TenToANum = 0;
			for (int j = 10; j < 15; j++)
			{
				if (1 <= temp[j])
				{
					TenToANum++;
				}
			}
			bFlag = TenToANum >= 5 ? true : false;
			if (m_bKingCanReplace && (iKingCount + iHuaCount>0) && TenToANum >= 1)
			{
				bFlag = TenToANum + iKingCount + iHuaCount >= 5 ? true : false;
			}
			if (bFlag)
			{
				return 100;
			}
		}
		if (temp[14] != 0 || (m_bKingCanReplace && (iKingCount + iHuaCount > 0)))//有a2345这个值，
		{
			bool bFlag = true;
			int TenToANum = 0;
			for (int j = 2; j < 6; j++)
			{
				if (1 <= temp[j])
				{
					TenToANum++;
				}
			}
			//有没有A
			int haveA = 0;
			for (int t = 0; t < iHandCardCount; t++)
			{
				if (14 == GetCardNum(iHandCard[t]))
				{
					haveA++;
					break;
				}
			}
			bFlag = TenToANum + haveA >= 5 ? true : false;
			if (m_bKingCanReplace && (iKingCount + iHuaCount>0))
			{
				bFlag = TenToANum + iKingCount + haveA + iHuaCount >= 5 ? true : false;
			}
			if (bFlag)
			{
				return 80;
			}
		}
		BYTE bKingValue = 0, bStraightSumValue = 0;
		for (int i = 0; i < 17; i++)
		{
			iKingCountTmp = iKingCount;
			bStraightSumValue = 0;
			if (temp[i] != 0)//有值 todo
			{
				//if (i == 1 && (iSum >= 28 && iSum <= 42))//A2345 或带王的A2345
				//{
				//	return 20 - 1;
				//}
				//else if (i == 1 && (iSum >= 60 && iSum <= 66))//10JQKA
				//{
				//	return 60;
				//}
				//else
					return i * 5 + 10;
			}
		}
		return 0;
	}

	int CUpGradeGameLogic::CompareCardInEqualValueByHaveKing(BYTE wFirstCard[], int iFirstCount, BYTE wSecondCard[], int iSecondCount)
	{
		/*
		处理过程
		1 都无财神返回0
		2 一有一无 直接返回 真(无财神)最大
		3 都有财神
		3.1 都为普通王 已处理
		3.2 都为花牌 花牌少的大
		3.3 一人仅有花牌，一人仅有王牌  王的手牌大
		3.4 两个人 都有王牌花牌 带王牌多的获胜
		*/
		int iFirstKingCount = GetKingCount(wFirstCard, iFirstCount);
		int iSecondKingCount = GetKingCount(wSecondCard, iSecondCount);
		int iFirstColorCount = GetHuaCount(wFirstCard, iFirstCount);
		int iSecondColorCount = GetHuaCount(wSecondCard, iSecondCount);
		int iFirstMammonCount = iFirstKingCount + iFirstColorCount;
		int iSecondMammonCount = iSecondKingCount + iSecondColorCount;

		if (iFirstMammonCount == 0 || iSecondMammonCount == 0)//1 2 有一个没有财神
		{
			return iFirstMammonCount == iSecondMammonCount ? 0 : iSecondMammonCount - iFirstMammonCount;
		}

		if (iFirstColorCount + iSecondColorCount == 0)//3.1都没有花牌
		{
			return iFirstKingCount == iSecondKingCount ? 0 : iSecondKingCount - iFirstKingCount;
		}
		else if (iFirstKingCount + iSecondKingCount == 0)//3.2都没有王
		{
			return  iFirstColorCount == iSecondColorCount ? 0 : iSecondColorCount - iFirstColorCount;
		}
		else if (iSecondKingCount == 0 && iFirstColorCount == 0)//3.3 2有花  1有王
		{
			return 1;
		}

		else if (iFirstKingCount == 0 && iSecondColorCount == 0)//3.3 1有花 2有王
		{
			return -1;
		}
		else if (iFirstKingCount&&iFirstColorCount&&iSecondKingCount&&iSecondColorCount)
		{
			if (iFirstKingCount == iSecondKingCount)
			{
				return 0;//比单牌？
			}
		}
		else
		{
			return 0;
		}
	}

	BYTE CUpGradeGameLogic::GetKingCount(BYTE iCardList[], int iCardCount)
	{
		int bCount = 0;
		for (int i = 0; i < iCardCount; ++i)
		{
			if (iCardList[i] == 0x6E || iCardList[i] == 0x6F)
			{
				bCount++;
			}
		}
		return bCount;
	}

	BYTE CUpGradeGameLogic::GetHuaCount(BYTE iCardList[], int iCardCount)
	{
		int bCount = 0;
		for (int i = 0; i < iCardCount; ++i)
		{
			if (GetCardIsHua(iCardList[i]))
			{
				bCount++;
			}
		}
		return bCount;
	}

	bool CUpGradeGameLogic::finOneCard(BYTE iCardList[], int iCardCount, int icard)
	{
		for (int i = 0; i < iCardCount;i++)
		{
			if (GetCardNum(iCardList[i]) == icard)
			{
				return true;
			}
		}
		return false;
	}



}


//////////////////////////////////////////////////////////////////////////
