/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardSettlementLayer.h"
#include "CaiShenThirteenCardWrapper.h"

USING_NS_CC;
using namespace ui;

namespace CaiShenThirteenCard
{
#define WIN_SIZE		Director::getInstance()->getWinSize()
#define PlayerCount	6

	static const char* USER_MEN_HEAD = "platform/head/men_head.png";
	static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";
	
	//////////////////////////////////////////////////////////////////////////


	SettlementLayer::SettlementLayer()
		: _callback(nullptr)
		, _clock(nullptr)
		, _btnWinOrLose(nullptr)
		, _settlementBG(nullptr)
		, _countdownTime(5)
	{
		/*_userHead = nullptr;
		_spriteHead = nullptr;*/
		memset(_results, 0x0, sizeof(_results));
		memset(_nickNames, 0x0, sizeof(_nickNames));
		memset(_textAtlasScores, 0x0, sizeof(_textAtlasScores));
		memset(_head_bg, 0x0, sizeof(_head_bg));
		memset(_userHead, 0x0, sizeof(_userHead));
	}

	SettlementLayer::~SettlementLayer()
	{

	}

	void SettlementLayer::closeSettlement()
	{
		unschedule(CC_SCHEDULE_SELECTOR(SettlementLayer::scheduleCountdownTimer));
		_settlementBG->runAction(Sequence::create(ScaleTo::create(0.1f, 0.3f), CallFunc::create([&]()
		{
			if (_callback) _callback();
			this->removeFromParent();
		}), nullptr));
	}

	bool SettlementLayer::init()
	{
		if (!HNLayer::init())
		{
			return false;
		}
		this->setIgnoreAnchorPointForPosition(false);

		auto settlementNode = CSLoader::createNode("CaiShenThirteenCard/GameSettlementUi/Node_Settlement.csb");
		settlementNode->setPosition(Vec2(WIN_SIZE.width / 2, WIN_SIZE.height / 2));

		settlementNode->runAction(Sequence::create(DelayTime::create(5.0f), CallFunc::create([=](){
			closeSettlement();
		}), nullptr));
		this->addChild(settlementNode, 100);
		auto settlementPanel = dynamic_cast<Layout*>(settlementNode->getChildByName("Panel_Settlement"));
		settlementPanel->addClickEventListener([=](Ref*){
			//closeSettlement();
		});

		settlementPanel->setScale(_winSize.width / 1280, _winSize.height / 720);

		float scalex = 1280 / _winSize.width;
		float scaley = 720 / _winSize.height;
		_settlementBG = dynamic_cast<ImageView*>(settlementPanel->getChildByName("Image_BG"));
		_settlementBG->setScale(scalex, scaley);
		//长屏适配
		if (_winSize.width / _winSize.height >= 1.8)
		{
			_settlementBG->setScale(scalex / scaley, 1);
		}
		char buf[64];

		for (size_t i = 0; i < 6; i++)
		{
			sprintf(buf, "Panel_user%d", i);
			_head_bg[i] = dynamic_cast<Layout*>(_settlementBG->getChildByName(buf));
			auto img_frame = dynamic_cast<ImageView*>(_head_bg[i]->getChildByName("Image_frame"));
			auto img_head = dynamic_cast<ImageView*>(_head_bg[i]->getChildByName("Image_head"));
			
			_userHead[i] = GameUserHead::create(img_head, img_frame);
			_userHead[i]->show();
			_nickNames[i] = dynamic_cast<Text*>(img_frame->getChildByName("Text_NickName"));
			_textAtlasScores[i] = dynamic_cast<TextBMFont*>(img_frame->getChildByName("BitmapFontLabel_Score"));
			
			//牌
			for (int j = 0; j < 13;j++)
			{
				sprintf(buf, "ImageCard_%d", j);
				_ImageCard[i][j] = dynamic_cast<ImageView*>(_head_bg[i]->getChildByName(buf));
			}
			
			//每道的得分情况
			for (int j = 0; j < 4; j++)
			{
				sprintf(buf, "text_score%d", j);
				_textAtlasComepareResult[i][j] = dynamic_cast<TextBMFont*>(_head_bg[i]->getChildByName(buf));
			}

			for (int j = 0; j < 4; j++)
			{
				sprintf(buf, "Image_%d", j);
				_ImageDaoShu[i][j] = dynamic_cast<ImageView*>(_head_bg[i]->getChildByName(buf));
			}

			//房主标识
			_ownerImage[i] = dynamic_cast<ImageView*>(_head_bg[i]->getChildByName("Image_fangzhu"));
			_ownerImage[i]->setLocalZOrder(10);
			//庄家标识
			_ownerZhuang[i] = dynamic_cast<ImageView*>(_head_bg[i]->getChildByName("Image_zhuang"));
			_ownerZhuang[i]->setLocalZOrder(10);
		} 
		
		_btnWinOrLose = dynamic_cast<Button*>(_settlementBG->getChildByName("Button_WinOrLose"));
		_btnWinOrLose->setVisible(false);

		
		auto btnClose = dynamic_cast<Button*>(_settlementBG->getChildByName("Button_Close"));

		btnClose->addClickEventListener([=](Ref* ref){ closeSettlement(); });		
		_clock = dynamic_cast<TextAtlas*>(btnClose->getChildByName("AtlasLabel_Clock"));

		return true;
	}

	void SettlementLayer::loadParameter(const std::vector<SettlementParameter>& parameters, bool isNowCompare)
	{
		int iParametersSize = parameters.size();
		for (size_t i = 0; i < iParametersSize; i++)
		{
			if (parameters[i].self)
			{
				_btnWinOrLose->setVisible(true);
				if (parameters[i].score > 0)
				{
					_btnWinOrLose->setBright(true);
					if (!isNowCompare)
					{
						HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/win.mp3"); 
					}
					else
					{
						runAction(Sequence::create(DelayTime::create(3.0f), CCCallFunc::create([=](){
							HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/win.mp3");
						}), nullptr));
					}
				}
				else
				{
					_btnWinOrLose->setBright(false);
					if (!isNowCompare)
					{
						HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/lose.mp3");
					}
					else
					{
						runAction(Sequence::create(DelayTime::create(3.0f), CCCallFunc::create([=](){
							HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/lose.mp3");
						}), nullptr));
					}
				}
			}

			_nickNames[i]->setString(GBKToUtf8(parameters[i].nickName.c_str()));
			
			// 显示玩家头像
			_userHead[i]->loadTexture(parameters[i].isBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);
			_userHead[i]->loadTextureWithUrl(parameters[i].sHeadUrl);
			auto user = UserInfoModule()->findUser(parameters[i].iUserID);
			_userHead[i]->setVIPHead("", user->iVipLevel);
			if (user->iVipLevel > 0)
				_nickNames[i]->setColor(Color3B(255,0,0));
			else
				_nickNames[i]->setColor(Color3B(255, 255, 255));

			//房主标识
			_ownerImage[i]->setVisible(parameters[i].iUserID == HNPlatformConfig()->getMasterID());
			char buf[64];

			//庄家标识
			_ownerZhuang[i]->setVisible(parameters[i].isZhuang);

			if (parameters[i].score > 0)
			{
				_textAtlasScores[i]->setColor(Color3B::RED);
				sprintf(buf, "+%lld", parameters[i].score);
			}
			else if (parameters[i].score < 0)
			{
				_textAtlasScores[i]->setColor(Color3B::GREEN);
				sprintf(buf, "%lld", parameters[i].score);
			}
			else
			{
				_textAtlasScores[i]->setColor(Color3B::WHITE);
				sprintf(buf, "%lld", parameters[i].score);
			}
			_textAtlasScores[i]->setString(buf);

			//显示玩家的摆牌数据
			BYTE cardValue[13] = {};
			int cardNum = 0;
			for (int k = 0; k < 3; k++)
			{
				cardValue[cardNum++] = parameters[i].byHeapCard[0][k];
			}
			for (int k = 0; k < 5; k++)
			{
				cardValue[cardNum++] = parameters[i].byHeapCard[1][k];
			}
			for (int k = 0; k < 5; k++)
			{
				cardValue[cardNum++] = parameters[i].byHeapCard[2][k];
			}
			for (int b = 0; b < 13; b++)
			{
				char filename[128];
				sprintf(filename, "Games/CaiShenThirteenCard/GamePlist/0x%02X.png", cardValue[b]);
				_ImageCard[i][b]->loadTexture(filename, Widget::TextureResType::PLIST);
			}
			//显示玩家的每道得分
			for (int k = 0; k < 3;k++)
			{
				if (parameters[i].ComepareResult[k]>0 )
				{
					_textAtlasComepareResult[i][k]->setString(StringUtils::format("+%d", parameters[i].ComepareResult[k]));
					_textAtlasComepareResult[i][k]->setColor(Color3B::RED);
				}
				else
				{
					_textAtlasComepareResult[i][k]->setString(StringUtils::format("%d", parameters[i].ComepareResult[k]));
					_textAtlasComepareResult[i][k]->setColor(Color3B::GREEN);
					if (parameters[i].ComepareResult[k] == 0)
					{
						_textAtlasComepareResult[i][k]->setString(StringUtils::format("+%d", parameters[i].ComepareResult[k]));
						_textAtlasComepareResult[i][k]->setColor(Color3B::WHITE);
					}
				}
				
			}
			if (parameters[i].iExtraScore > 0)
			{
				_textAtlasComepareResult[i][3]->setString(StringUtils::format("+%d", parameters[i].iExtraScore));
				_textAtlasComepareResult[i][3]->setColor(Color3B::RED);
			}
			else
			{
				_textAtlasComepareResult[i][3]->setString(StringUtils::format("%d", parameters[i].iExtraScore));
				_textAtlasComepareResult[i][3]->setColor(Color3B::GREEN);
				if (parameters[i].iExtraScore == 0)
				{
					_textAtlasComepareResult[i][3]->setString(StringUtils::format("+%d", parameters[i].iExtraScore));
					_textAtlasComepareResult[i][3]->setColor(Color3B::WHITE);
				}
			}
		}

		for (int i = iParametersSize; i < PlayerCount; i++)
		{
			_head_bg[i]->setVisible(false);
		}
		if (1 == parameters[0].palyMode)
		{
			for (int i = 0; i < 6; i++)
			{
				_textAtlasComepareResult[i][0]->setPositionY(_textAtlasComepareResult[i][0]->getPositionY() - 15);
				_textAtlasComepareResult[i][1]->setPositionY(_textAtlasComepareResult[i][1]->getPositionY() - 15);
				_textAtlasComepareResult[i][2]->setPositionY(_textAtlasComepareResult[i][2]->getPositionY() - 15);
				_ImageDaoShu[i][0]->setPositionY(_ImageDaoShu[i][0]->getPositionY() - 15);
				_ImageDaoShu[i][1]->setPositionY(_ImageDaoShu[i][1]->getPositionY() - 15);
				_ImageDaoShu[i][2]->setPositionY(_ImageDaoShu[i][2]->getPositionY() - 15);
				_ImageDaoShu[i][3]->setVisible(false);
				_textAtlasComepareResult[i][3]->setVisible(false);
			}
		}
		
	}

	void SettlementLayer::startTimer(int time)
	{
		_countdownTime = time;
		this->unschedule(CC_SCHEDULE_SELECTOR(SettlementLayer::scheduleCountdownTimer));
		this->schedule(CC_SCHEDULE_SELECTOR(SettlementLayer::scheduleCountdownTimer), 1.0f);
	}

	void SettlementLayer::setCloseCallback(const ccSettlementTimer& callback)
	{
		_callback = callback;
	}

	void SettlementLayer::scheduleCountdownTimer(float dt)
	{
		if (_countdownTime < 0)
		{
			closeSettlement();
		}
		else
		{
			char buf[64];
			sprintf(buf, "%d", _countdownTime);		
			_clock->setString(buf);

			if (!_clock->isVisible()) _clock->setVisible(true);
		}
		_countdownTime--;
	}
}
