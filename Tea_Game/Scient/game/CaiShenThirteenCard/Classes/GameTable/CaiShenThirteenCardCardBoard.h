/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_Action_Board_H__
#define __CaiShenThirteenCard_Action_Board_H__


#include "cocos2d.h"
#include <string>
#include <queue>
#include "HNNetExport.h"

USING_NS_CC;

namespace CaiShenThirteenCard
{
	class CardSprite;

	class CardBoard: public HN::HNLayer
	{
		typedef std::function<void()> MoveCardCallBack;

	public:
		MoveCardCallBack		_moveCardCallBack;

		static CardBoard* create(bool self);

	public:
		virtual bool init();

	public:
		void sendCardOneByOne(const std::vector<BYTE>& vtcards, float interval);
		void sendCard(const std::vector<BYTE>& cards);
		void removeCard(const std::vector<BYTE>& cards);
		void setMultSelected(bool multiSelect);
		void removeAllCard();
	public:
		std::vector<BYTE> getUpCards();
		std::vector<BYTE> getCards();
		unsigned int getCardSize() const;
		bool empty() const;
		void clear();
		void enableCardTouch(bool enableTouch);

		void downCards();
		void upCards(const std::vector<BYTE>& cards);

		void sortUpCards( std::vector<BYTE>& cards);

		void sortStylorCardEvent();
		void ReturnsortCardEvent();

		void changeUpDown();

		void upAllCards();
	protected:
		CardBoard(bool self);
		virtual ~CardBoard();

	private:
		bool onTouchBegan(Touch* touch, Event *event);
		void onTouchMoved(Touch* touch, Event *event);
		void onTouchEnded(Touch* touch, Event *event);

		void scheduleSendCardStart(float dt);

	private:
		void addCard(BYTE cardValue);
		void removeCard(BYTE cardValue);
		void resizeCardBoard();
		void touchCheck(const Vec2& pos, bool isEnd = false);
		void sortCard();
		void sortStylorCard();//����ɫ����_
		void upCard(CardSprite* card, float delay = 0.0f);
		void downCard(CardSprite* card, float delay = 0.0f);
		void swtichUpDown();

	private:
		std::vector<BYTE> searchCards(const std::string& cond);

	private:
		std::queue<BYTE> _tmpCardValues;
		std::vector<CardSprite*> _pokers;
		bool _self;
		bool _multiSelect;
		bool _enableCardTouch;

	};
}




#endif