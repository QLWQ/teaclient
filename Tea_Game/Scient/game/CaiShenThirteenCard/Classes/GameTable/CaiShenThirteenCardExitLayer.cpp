/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardExitLayer.h"
#include "CaiShenThirteenCardWrapper.h"

#define WIN_SIZE		Director::getInstance()->getWinSize()

namespace CaiShenThirteenCard
{
	//////////////////////////////////////////////////////////////////////////

	ExitLayer::ExitLayer()
		: _callback(nullptr)
	{

	}

	ExitLayer::~ExitLayer()
	{

	}

	void ExitLayer::colseExit()
	{
		_exitBG->runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create([=](){
			this->removeFromParent();
		}), nullptr));
	}

	bool ExitLayer::init()
	{
		if (!HNLayer::init()) return false;
		this->setIgnoreAnchorPointForPosition(false);

		auto exitNode = CSLoader::createNode("CaiShenThirteenCard/GameExitUi/Node_ExitUi.csb");
		exitNode->setPosition(Vec2(WIN_SIZE.width / 2, WIN_SIZE.height / 2));
		this->addChild(exitNode, 10);

		auto panel = dynamic_cast<Layout*>(exitNode->getChildByName("Panel_Exit"));
		_exitBG = dynamic_cast<ImageView*>(panel->getChildByName("Image_ExitBG"));

		auto Button_Sure = dynamic_cast<Button*>(_exitBG->getChildByName("Button_Sure"));
		auto Button_Cancel = dynamic_cast<Button*>(_exitBG->getChildByName("Button_Cancel"));

		Button_Sure->addClickEventListener(CC_CALLBACK_1(ExitLayer::onSureCallBack, this));
		Button_Cancel->addClickEventListener(CC_CALLBACK_1(ExitLayer::onCancelCallBack, this));

		_exitBG->runAction(Sequence::create(
			Hide::create(), 
			ScaleTo::create(0.0f, 0.2f),
			Show::create(),
			ScaleTo::create(0.1f, 1.0f), nullptr));

		return true;
	}

	void ExitLayer::setSureClickEvent(const std::function<void()>& callback)
	{
		_callback = callback;
	}

	void ExitLayer::onSureCallBack(Ref* pSender)
	{
		if (nullptr != _callback)
		{
			_callback();
		}
	}

	void ExitLayer::onCancelCallBack(Ref* pSender)
	{
		this->runAction(Sequence::create(DelayTime::create(0.1f), RemoveSelf::create(), nullptr));
	}
}
