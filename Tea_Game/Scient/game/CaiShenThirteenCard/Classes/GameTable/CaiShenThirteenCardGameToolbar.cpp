/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardGameToolbar.h"

using namespace ui;

#define WIN_SIZE		Director::getInstance()->getWinSize()

namespace CaiShenThirteenCard
{
	const char* Name_Exit_Menu = "Button_leavingRoom";
	const char* Name_Set_Menu = "Button_Setting";
	
	gameToolbar::gameToolbar()
		: _isOut(false)
		, _onSet(nullptr)
		, _onExit(nullptr)
	{
		
	}

	gameToolbar::~gameToolbar()
	{
		_toolBarBG = nullptr;
	}

	bool gameToolbar::doLoad(Node* toolBar)
	{
		_toolBarBG = toolBar;

		if (_toolBarBG == nullptr) return false;
		float scY = 720 / WIN_SIZE.height;
		_toolBarBG->setScaleY(scY);

		//auto btnExit = dynamic_cast<Button*>(toolBar->getChildByName(Name_Exit_Menu));
		//btnExit->setVisible(!(RoomLogic()->getRoomRule() & GRR_GAME_BUY));
		//auto btnSetting = dynamic_cast<Button*>(toolBar->getChildByName(Name_Set_Menu));

		//btnExit->addTouchEventListener(CC_CALLBACK_2(gameToolbar::menuClickCallback, this));
		//btnSetting->addTouchEventListener(CC_CALLBACK_2(gameToolbar::menuClickCallback, this));

		return true;
	}

	void gameToolbar::menuClickCallback(cocos2d::Ref* pSender, ui::Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	return;
		Button* pMenu = dynamic_cast<Button*>(pSender);

		std::string name = pMenu->getName();

		if (name.compare(Name_Exit_Menu) == 0)
		{
			if (_onExit) _onExit();
		}
		else if (name.compare(Name_Set_Menu) == 0)
		{
			if (_onSet) _onSet();
		}
	}
}

