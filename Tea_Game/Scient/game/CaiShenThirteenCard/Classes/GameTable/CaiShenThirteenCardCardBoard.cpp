/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardCardBoard.h"
#include "CaiShenThirteenCardCardSprite.h"

#include <algorithm>


using namespace HN;

/*
 * 玩家出牌操作面板
 * Tag用来记录牌是否被点击
 * Name用来记录牌是否被提起
 */

#define UP_OFFSET				20.0f		//40.0f
#define SELF_CARD_MIN_INTERVAL  40.0f		// 两张牌间最小间隔  45.0f
#define SELF_CARD_MAX_INTERVAL  70.0f		// 两张牌间最大间隔  70.0f
#define OTHER_CARD_INTERVAL     15.0f

#define UP        "up"
#define DOWN      "down"

#define TAG_UNSELECTED  0
#define TAG_SELECTED    1
#define TAG_MOVEING		2

#define ZORDER_CARD_BEGAN		1

static float send_card_time		=	0.15f;

namespace CaiShenThirteenCard
{
	CardBoard* CardBoard::create(bool self)
	{
		CardBoard* board = new CardBoard(self);
		if (board->init())
		{
			board->autorelease();
			return board;
		}
		CC_SAFE_DELETE(board);
		return nullptr;
	}

	CardBoard::CardBoard(bool self)
		: _self(self)
		, _enableCardTouch(true)
		, _multiSelect(false)
		, _moveCardCallBack(nullptr)
	{

	}

	CardBoard::~CardBoard()
	{

	}

	bool CardBoard::init()
	{
		if(!HNLayer::init())
		{
			return false;
		}

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0.5f, 0.5f));
		this->setContentSize(Size::ZERO);

		if(_self)
		{
			auto listener = EventListenerTouchOneByOne::create();
			listener->onTouchBegan = CC_CALLBACK_2(CardBoard::onTouchBegan, this);
			listener->onTouchMoved = CC_CALLBACK_2(CardBoard::onTouchMoved, this);
			listener->onTouchEnded = CC_CALLBACK_2(CardBoard::onTouchEnded, this);
			listener->setSwallowTouches(true);
			this->_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
		}

		return true;
	}

	void CardBoard::sendCardOneByOne(const std::vector<BYTE>& cards, float interval)
	{
		if (cards.empty()) return;

		unschedule(schedule_selector(CardBoard::scheduleSendCardStart));
		{
			std::queue<BYTE>().swap(_tmpCardValues);
		}
		removeCard(cards);

		for (auto& card : cards)
		{
			_tmpCardValues.push(card);
		}
		if (interval < send_card_time)
		{
			interval = send_card_time;
		}
		schedule(schedule_selector(CardBoard::scheduleSendCardStart), interval);
	}

	void CardBoard::sendCard(const std::vector<BYTE>& cards)
	{
		if (cards.empty()) return;

		for (BYTE card : cards)
		{
			addCard(card);
		}
		sortCard();
		resizeCardBoard();
	}

	std::vector<BYTE> CardBoard::getUpCards()
	{		
		std::vector<BYTE> cards = searchCards(std::string(UP));
		return cards;	
	}
	
	std::vector<BYTE> CardBoard::getCards()
	{
		std::vector<BYTE> cards = searchCards(std::string(""));
		return cards;
	}

	unsigned int CardBoard::getCardSize() const
	{
		return _pokers.size();
	}

	bool CardBoard::empty() const
	{
		return _pokers.empty();
	}

	void CardBoard::scheduleSendCardStart(float dt)
	{
		do
		{
			if (_tmpCardValues.empty())
			{
				unschedule(schedule_selector(CardBoard::scheduleSendCardStart)); break;
			}
			else
			{

			}

			HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/sendcard.mp3");

			addCard(_tmpCardValues.front());
			sortCard();
			resizeCardBoard();
			_tmpCardValues.pop();
		} while (0);
	}

	bool CardBoard::onTouchBegan(Touch* touch, Event *event)
	{
		if (!_enableCardTouch) return false;

		Vec2 pos = this->convertToNodeSpace(touch->getLocation());
		touchCheck(pos);
		Rect rect(0,0, this->getContentSize().width, this->getContentSize().height);
		if(rect.containsPoint(pos))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void CardBoard::onTouchMoved(Touch* touch, Event *event)
	{
		if (!_enableCardTouch) return;
		Vec2 pos = this->convertToNodeSpace(touch->getLocation());
		touchCheck(pos);
	}

	void CardBoard::onTouchEnded(Touch* touch, Event *event)
	{
		if (!_enableCardTouch) return;

		Vec2 pos = this->convertToNodeSpace(touch->getLocation());

		Rect rect(0, 0, this->getContentSize().width, this->getContentSize().height);
		if (!rect.containsPoint(pos))
		{
			touchCheck(pos,true);
		}	

		for (size_t i = 0; i < _pokers.size(); i++)
		{
			if (_pokers[i] != nullptr)
			{
				_pokers[i]->setColor(Color3B::WHITE);
			}
		}
		changeUpDown();
	}

	void CardBoard::touchCheck(const Vec2& pos, bool isEnd)
	{
		for (size_t i = 0; i < _pokers.size(); i++)
		{
			int index = _pokers.size() - i - 1;
			Rect rect = _pokers[index]->getBoundingBox();
			if (rect.containsPoint(pos))
			{
				if (isEnd && _pokers[index]->getTag() == TAG_SELECTED)
				{
					_pokers[index]->setTag(TAG_UNSELECTED);
					_pokers[index]->setColor(Color3B::WHITE);
				}
				else if (!isEnd && _pokers[index]->getTag() == TAG_UNSELECTED)
				{
					_pokers[index]->setTag(TAG_SELECTED);
					_pokers[index]->setColor(Color3B::GRAY);
				}
				return;
			}
		}

		/*if (_multiSelect)
		{
			for (size_t i = 0; i < _pokers.size(); i++)
			{
				int index = _pokers.size() - i - 1;
				Rect rect = _pokers[index]->getBoundingBox();
				if (rect.containsPoint(pos))
				{
					if (_pokers[index]->getTag() == TAG_SELECTED)
					{
						_pokers[index]->setTag(TAG_UNSELECTED);
						_pokers[index]->setName(DOWN);
						downCard(_pokers[index]);
					}
					else if (_pokers[index]->getTag() == TAG_UNSELECTED)
					{
						_pokers[index]->setTag(TAG_SELECTED);
						_pokers[index]->setName(UP);
						upCard(_pokers[index]);
					}
					return;
				}
			}
		}
		else
		{
			int selectedIndex = 0;
			for (size_t i = 0; i < _pokers.size(); i++)
			{
				int index = _pokers.size() - i - 1;

				Rect rect = _pokers[index]->getBoundingBox();
				if (rect.containsPoint(pos))
				{
					if (_pokers[index]->getTag() == TAG_SELECTED)
					{
						_pokers[index]->setTag(TAG_UNSELECTED);
						_pokers[index]->setName(DOWN);
						downCard(_pokers[index]);
					}
					else if (_pokers[index]->getTag() == TAG_UNSELECTED)
					{
						_pokers[index]->setTag(TAG_SELECTED);
						_pokers[index]->setName(UP);
						upCard(_pokers[index]);
					}
					selectedIndex = index;
					break;
				}
			}

			for (size_t j = 0; j < _pokers.size(); j++)
			{
				int index = _pokers.size() - j - 1;
				if (index == selectedIndex) continue;

				_pokers[index]->setTag(TAG_UNSELECTED);
				_pokers[index]->setName(DOWN);
				downCard(_pokers[index]);
			}
		}*/
	}


	void CardBoard::enableCardTouch(bool enableTouch)
	{
		_enableCardTouch = enableTouch;
	}

	void CardBoard::clear()
	{
		unschedule(schedule_selector(CardBoard::scheduleSendCardStart));
		
		{
			std::queue<BYTE>().swap(_tmpCardValues);
		}

		for(auto poker : _pokers)
		{
			if (poker != nullptr) poker->removeFromParent();
		}
		_pokers.clear();
	}

	void CardBoard::addCard(BYTE cardValue)
	{
		CardSprite* card = CardSprite::create(cardValue);
		card->setScale(_self ? 1.12f : 0.72f);
		card->setName(DOWN);
		card->setTag(TAG_UNSELECTED);
		this->addChild(card);
		_pokers.push_back(card);
	}

	void CardBoard::removeAllCard()
	{
		this->removeAllChildren();
		_pokers.clear();
	}

	void CardBoard::removeCard(BYTE cardValue)
	{
		auto iterCard = find_if(_pokers.begin(), _pokers.end(), [cardValue](CardSprite* card)
			{
				return (card->getValue() == cardValue);
			});
		if (iterCard != _pokers.end())
		{
			CardSprite* card = (*iterCard);
			card->removeFromParent();
			_pokers.erase(iterCard);
		}
	}

	void CardBoard::setMultSelected(bool multiSelect)
	{
		_multiSelect = multiSelect;
	}

	void CardBoard::removeCard(const std::vector<BYTE>& cards)
	{
		if (cards.empty()) return;

		for (BYTE card : cards)
		{
			removeCard(card);
		}
		resizeCardBoard();
	}

	void CardBoard::sortCard()
	{
		std::sort(_pokers.begin(), _pokers.end(), [] (CardSprite* left, CardSprite* right) -> bool
			{
				BYTE lCard = left->getValue();
				BYTE rCard = right->getValue();

				BYTE lColor = lCard  & 0xf0;
				BYTE rColor = rCard & 0xf0;
				BYTE lValue = lCard  & 0x0f;
				BYTE rValue = rCard & 0x0f;

				////2
				//if(lValue == 0x01)
				//{
				//	lValue = 0x0e;
				//}

				//if(rValue == 0x1)
				//{
				//	rValue = 0x0e;
				//}

				return ((rValue < lValue) || (rValue == lValue && rColor < lColor));
			});
	}

	void CardBoard::sortUpCards( std::vector<BYTE>& cards)
	{
		std::sort(cards.begin(), cards.end(), [](BYTE lCard, BYTE rCard) -> bool
		{			
			BYTE lColor = lCard & 0xf0;
			BYTE rColor = rCard & 0xf0;
			BYTE lValue = lCard & 0x0f;
			BYTE rValue = rCard & 0x0f;

			return ((rValue < lValue) || (rValue == lValue && rColor < lColor));
		});
	}

	void CardBoard::sortStylorCardEvent()
	{
		sortStylorCard();
		resizeCardBoard();
	}

	void CardBoard::sortStylorCard()
	{
		std::sort(_pokers.begin(), _pokers.end(), [](CardSprite* left, CardSprite* right) -> bool
		{
			BYTE lCard = left->getValue();
			BYTE rCard = right->getValue();

			BYTE lColor = lCard & 0xf0;
			BYTE rColor = rCard & 0xf0;
			BYTE lValue = lCard & 0x0f;
			BYTE rValue = rCard & 0x0f;

			////2
			//if (lValue == 0x01)
			//{
			//	lValue = 0x0e;
			//}

			//if (rValue == 0x1)
			//{
			//	rValue = 0x0e;
			//}
			
			return ((rColor < lColor) || (rColor == lColor && rValue < lValue));
		});
	}

	std::vector<BYTE> CardBoard::searchCards(const std::string& cond)
	{
		std::vector<BYTE> cards;
		if (cond.empty())
		{
			for (auto poker : _pokers)
			{
				cards.push_back(poker->getValue());
			}
		}
		else
		{
			for (auto poker : _pokers)
			{
				if (poker->getName().compare(cond) == 0)
				{
					cards.push_back(poker->getValue());
				}
			}
		}
		sortUpCards(cards);
		return cards;
	}

	void CardBoard::swtichUpDown()
	{
		for (auto card : _pokers)
		{
			if (card->getTag() == TAG_UNSELECTED) continue;

			if (card->getName().compare(UP) == 0)
			{
				downCard(card);
			}
			else
			{
				upCard(card);
			}
			card->setTag(TAG_UNSELECTED);
		}
	}

	void CardBoard::downCards()
	{
		for(auto& card : _pokers)
		{
			card->setTag(TAG_UNSELECTED);
			downCard(card);
		}
	}

	void CardBoard::upCards(const std::vector<BYTE>& cards)
	{
		/*//花色和牌值相同的都会起立
		for(auto poker : _pokers)
		{
			auto iter = find_if(cards.begin(), cards.end(), [&](BYTE val)
			{
				return poker->getValue() == val;
			});
			if (iter != cards.end())
			{
				upCard(poker);
			}
			else
			{
				downCard(poker);
			}
		}
		*/
		std::vector<CardSprite*> upPokers; //已经起立的扑克牌
		for (auto card : cards)
		{
			auto iter = find_if(_pokers.begin(), _pokers.end(), [&](CardSprite* val)
			{
				return find(upPokers.begin(), upPokers.end(), val) == upPokers.end() && card == val->getValue();
			});
			if (iter != _pokers.end())
			{
				upCard(*iter);
				upPokers.push_back(*iter);
			}
		}
		//其余的牌都要坐下
		for (auto poker : _pokers)
		{
			auto iter = find(upPokers.begin(), upPokers.end(), poker);
			if (iter == upPokers.end())
			{
				downCard(poker);
			}
		}
	}

	void CardBoard::upCard(CardSprite* card, float delay)
	{
		if (card->getName().compare(DOWN) == 0)
		{
			card->setName(UP);
			card->stopAllActions();
			card->runAction(Sequence::create(DelayTime::create(delay), MoveBy::create(0.1f, Vec2(0, UP_OFFSET)), nullptr));
			if (_moveCardCallBack) _moveCardCallBack();
		}
	}

	void CardBoard::downCard(CardSprite* card, float delay)
	{
		if (card->getName().compare(UP) == 0)
		{
			card->setName(DOWN);
			card->stopAllActions();
			card->runAction(Sequence::create(DelayTime::create(delay), MoveBy::create(0.1f, Vec2(0, -UP_OFFSET)), nullptr));
			if (_moveCardCallBack) _moveCardCallBack();
		}
	}

	void CardBoard::resizeCardBoard()
	{
		if(_pokers.empty()) return;
	
		Size cardSize = _pokers[0]->getContentSize() * _pokers[0]->getScale();

		float interval = OTHER_CARD_INTERVAL;
		if(_self)
		{
			//70 * 94
			float maxLength = cardSize.width + (15 - 1) * SELF_CARD_MIN_INTERVAL;
			interval = maxLength / _pokers.size();
			if(interval > SELF_CARD_MAX_INTERVAL)
			{
				interval = SELF_CARD_MAX_INTERVAL;
			}
		}

		float width  = cardSize.width + (_pokers.size() - 1) * interval;
		float height = cardSize.height + UP_OFFSET;
	
		this->setContentSize(Size(width, height));

		for(size_t i = 0; i < _pokers.size(); i++)
		{
			_pokers[i]->setAnchorPoint(Vec2(0,0));
			_pokers[i]->setPosition(interval * i, _pokers[i]->getPositionY());
			_pokers[i]->setLocalZOrder(ZORDER_CARD_BEGAN + i);
			log("x:%f,y:%f", interval * i, _pokers[i]->getPositionY());
		}

		if(!_self)
		{
			downCards();
		}
	}

	void CardBoard::changeUpDown()
	{
		for (size_t i = 0; i < _pokers.size(); i++)
		{
			if (_pokers[i]->getTag() == TAG_UNSELECTED)	continue;

			if (_pokers[i]->getName().compare(UP) == 0)
			{
				downCard(_pokers[i], 0);
			}
			else
			{
				upCard(_pokers[i]);
			}
			_pokers[i]->setTag(TAG_UNSELECTED);
		}
	}

	void CardBoard::upAllCards()
	{
		for (size_t i = 0; i < _pokers.size(); i++)
		{
			upCard(_pokers[i]);
		}
	}

	void CardBoard::ReturnsortCardEvent()
	{
		sortCard();
		resizeCardBoard();
	}

}
