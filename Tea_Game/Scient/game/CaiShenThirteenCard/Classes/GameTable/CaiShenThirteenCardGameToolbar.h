/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __HN_CaiShenThirteenCard_TOOLBAR_H__
#define __HN_CaiShenThirteenCard_TOOLBAR_H__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

namespace CaiShenThirteenCard
{
	class gameToolbar
	{
	public:
		bool doLoad(Node* toolBar);
		void doRestore();

		gameToolbar();
		virtual ~gameToolbar();

	public:
		void menuClickCallback(cocos2d::Ref* pSender, ui::Widget::TouchEventType touchtype);


	private:
		bool				_isOut;
		Node*			_toolBarBG;

		typedef std::function<void()> ExitCallBack;
		typedef std::function<void()> ChatCallBack;
		typedef std::function<void()> SetCallBack;

		typedef std::function<void()> CancelRoomCallBack;
		typedef std::function<void()> LeavingRoomCallBack;
		typedef std::function<void()> ApplyCancelRoomCallBack;

	public:
		ExitCallBack				_onExit;//退出回调
		SetCallBack					_onSet;//设置回调
	};
}



#endif

