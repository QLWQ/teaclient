/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardGameTableUI.h"
#include "CaiShenThirteenCardGameTableLogic.h"
#include "CaiShenThirteenCardControllerLayer.h"
#include "CaiShenThirteenCardWrapper.h"
#include "CaiShenThirteenCardExitLayer.h"
#include "CaiShenThirteenCardSoundLayer.h"
#include "HNNetExport.h"
#include "HNLobbyExport.h"
#include "CaiShenThirteenCardGameToolbar.h"
#include "CaiShenThirteenCardGameLoading.h"
#include "CaiShenThirteenCardCardSuite.h"
#include "CaiShenThirteenCardSetLayer.h"
using namespace HN;

////////////////////////////////////////////////////////////////////////////////////////////

#define WIN_SIZE		Director::getInstance()->getWinSize()
#define VISIBLE_SIZE	Director::getInstance()->getVisibleSize()

#define CHILD_LAYER_ZORDER				1000000

#define	LOCAL_ZORDER_CONTROL_LAYER		10000
#define	TAG_CONTROL_LAYER	CHILD_LAYER_ZORDER

#define	LOCAL_ZORDER_EXIT_LAYER			10001
#define	TAG_EXIT_LAYER		CHILD_LAYER_ZORDER

#define	LOCAL_ZORDER_SET_LAYER			20007
#define	TAG_SET_LAYER		CHILD_LAYER_ZORDER

#define	LOCAL_ZORDER_SETTLEMENT_LAYER	20000
#define	TAG_SETTLEMENT_LAYER	CHILD_LAYER_ZORDER


/*** 作弊部分  ***/
#define		CHEAT_SSS_BUTTON_COUNT		2			//作弊按钮数
static const std::string _sss_cb_names[CHEAT_SSS_BUTTON_COUNT] = { "cheat_none", "cheat_win" };
static const Vec2 _sss_cb_points[CHEAT_SSS_BUTTON_COUNT] = { Vec2(80, 660), Vec2(245, 660) };
static const std::string _sss_txt_titles[CHEAT_SSS_BUTTON_COUNT] = { GBKToUtf8("不作弊"), GBKToUtf8("高胜率") };
static const int _sss_cheat_commond[CHEAT_SSS_BUTTON_COUNT] = { 0, 2 };   //发送的作弊指令


namespace CaiShenThirteenCard
{
	static const char* GameShoot_Json = "CaiShenThirteenCard/GameTableRes/gameShoot/gameShoot.json";
	static const char* GameShoot_Atlas = "CaiShenThirteenCard/GameTableRes/gameShoot/gameShoot.atlas";

	static const char* GameShoot_Fire_Json = "CaiShenThirteenCard/GameTableRes/skeleton/skeleton.json";
	static const char* GameShoot_Fire_Atlas = "CaiShenThirteenCard/GameTableRes/skeleton/skeleton.atlas";

	static const char* GAME_FACE_SOUND_CHAT = "platform/Chat/chatAudio/face%d.mp3";       //点击表情出来语音
	static const char* Be_OpenFire_Effect = "platform/Chat/chatAudio/face4.mp3";       //"漆黑漆黑"
	static const char* Fire_All_Effect = "platform/Chat/chatAudio/face3.mp3";       //"浪起来"

	HNGameUIBase* GameTableUI::create(BYTE bDeskIndex, bool bAutoCreate)
	{
		GameTableUI* tableUI = new GameTableUI();
		if (tableUI->init(bDeskIndex, bAutoCreate))
		{
			tableUI->autorelease();
			return tableUI;
		}
		else
		{
			CC_SAFE_DELETE(tableUI);
			return nullptr;
		}
	}

	GameTableUI::GameTableUI()
		: _btnReady(nullptr)
		, _suite(nullptr)
		, _btn_qiangzhaung(nullptr)
		, _btn_buqiang(nullptr)
	{

	}

	GameTableUI::~GameTableUI()
	{
		HN_SAFE_DELETE(_logic);
	}

	bool GameTableUI::init(BYTE bDeskIndex, bool bAutoCreate)
	{
		if (!HNGameUIBase::init()) return false;
		//FileUtils::getInstance()->addSearchPath("Games/CaiShenThirteenCard/GamePlist");
		m_bCheatShow = false;
		//异步加载资源
		auto loadingLayer = NewThirteenCardGameLoading::create();
		addChild(loadingLayer);
		loadingLayer->onCloseCallBack = [=](){
			initGameUI(bDeskIndex, bAutoCreate);
		};
		//initGameUI(bDeskIndex, bAutoCreate);
		if (PlatformLogic()->loginResult.iVipLevel > 0)
		{
			
			std::string str = "欢迎尊贵的Vip";
			str.append(StringUtils::toString(PlatformLogic()->loginResult.iVipLevel));
			str.append("玩家：");
			str.append(PlatformLogic()->loginResult.nickName);
			str.append(",进入房间！");
			GameNotice::getInstance()->postMessage(GBKToUtf8(str.c_str()),0);
		}

		_cheatStatus = 0;
		return true;
	}

	void GameTableUI::initGameUI(BYTE bDeskIndex, bool bAutoCreate)
	{
		_deskNo = bDeskIndex;
		doLoad();

		_logic = new GameTableLogic(this, bDeskIndex, bAutoCreate);

		_logic->enterGame();


		//	初始化VIP房间控制器
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			roomController = VipRoomController::create(bDeskIndex);
			roomController->setName("roomController");
			roomController->setRoomNumPos(Vec2(_winSize.width * 0.02f, _winSize.height * 10.84f));
			roomController->setPlayCountPos(Vec2(_winSize.width * 0.02f, _winSize.height * 10.81f));
			roomController->setDismissBtnPos(Vec2(_winSize.width * 0.9f, _winSize.height * 10.933f));
			roomController->setRoomResultBtnPos(Vec2(_winSize.width * 0.9f-100, _winSize.height * 0.928f));
			roomController->setReturnBtnPos(Vec2(_winSize.width * 0.84f, _winSize.height * 10.933f));
			roomController->setReturnBtnTexture("CaiShenThirteenCard/GameTableUi/res/leavingRoom1.png");
			roomController->setDismissBtnTexture("CaiShenThirteenCard/GameTableUi/res/cancelRoom.png");
			//roomController->setKLGInvitationBtnTexture("CaiShenThirteenCard/GameTableUi/res/klgyaoqing.png");
			roomController->setInvitationBtnTexture("CaiShenThirteenCard/GameTableUi/res/wxyaoqing.png");

			roomController->setRoomNumPos(Vec2(10, 700));

			roomController->setPlayCountPos(Vec2(150, 700));

			roomController->setDifenPos(Vec2(260, 700));

			roomController->onSetVipInfoCallBack = [this](){

				// 更新房主标志显示
				setDeskInfo();
			};
			addChild(roomController, 20005);

			roomController->onDissmissCallBack = [this]() {

				roomController->onDissmissCallBack = [this]() {

					if (!_allRoundsEnd1)
					{
						auto prompt = GamePromptLayer::create();

						prompt->showPrompt(GBKToUtf8("桌子已解散"));
						prompt->setCallBack([=]() {
							RoomLogic()->close();
							GamePlatform::createPlatform();
						});
					}
				};
			};
		}

		//初始化比赛场控制器
		if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
		{
			GameMatchController* matchController = GameMatchController::create(bDeskIndex);
			addChild(matchController, 20010);
		}

		//显示聊天界面
		gameChatLayer();

		for (int i = 0; i < 6;i++)
		{
			_isfire[i] = false;
		}
		//模拟数据
		/*	 2    3    4    5    6    7    8    9   10    J    Q    K    A
			0x51 0x52 0x53 0x54 0x55 0x56 0x57 0x58 0x59 0x5A 0x5B 0x5C 0x5D   黑桃 0x5E
			81   82    83  84   85   86   87   88   89   90   91   92   93           94

			2    3    4    5    6    7    8    9   10    J    Q    K    A
			0x41 0x42 0x43 0x44 0x45 0x46 0x47 0x48 0x49 0x4A 0x4B 0x4C 0x4D   红心 0x4E
			65   66    67  68   69   70   71   72   73   74   75   76   77           78

			2    3    4    5    6    7    8    9   10    J    Q    K    A
			0x31 0x32 0x33 0x34 0x35 0x36 0x37 0x38 0x39 0x3A 0x3B 0x3C 0x3D   梅花 0x3E
			49   50    51  52   53   54   55   56   57   58   59   60   61           62

			2    3    4    5    6    7    8    9   10    J   Q    K    A
			0x21 0x22 0x23 0x24 0x25 0x26 0x27 0x28 0x29 0x2A 0x2B 0x2C 0x2D   方块 0x2E
			33   34   35   36   37   38   39   40   41   42   43   44   45          46

			2    3    4    5    6    7    8    9   10    J   Q    K    A
			0x11 0x12 0x13 0x14 0x15 0x16 0x17 0x18 0x19 0x1A 0x1B 0x1C 0x1D   黑方 0x1E
			17   18   19   20   21   22   23   24   25   26   27   28   29          30

			2    3    4     5    6    7    8    9   10    J    Q    K    A
			0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0A 0x0B 0x0C 0x0D   红圆 0x0E
			1   2    3    4     5    6    7    8    9   10   11   12   13          14

			小鬼 大鬼
			0x6E 0x6F
			110   111*/
		//BYTE icard[USER_CARD_COUNT] = {86,84,77,72,69,57,49,42,22,21,20,18,8};
		//BYTE icard[USER_CARD_COUNT] = { 1, 2, 3, 4, 5, 17, 18, 29, 20, 21, 6, 22, 46 };
		//BYTE icard[USER_CARD_COUNT] = { 42, 10, 54, 25, 9, 53, 37, 50, 40, 23, 22, 5, 36 };
		//BYTE icard[USER_CARD_COUNT] = { 78, 30, 14, 12, 42, 23, 36, 20, 54, 38, 22, 5, 17 };
		//BYTE icard[USER_CARD_COUNT] = { 13, 12, 11, 10, 9, 29, 28, 27, 26, 25, 78, 79, 14 };
		//BYTE icard[USER_CARD_COUNT] = { 62, 46, 30, 14, 13, 29, 28, 27, 26, 25, 7, 35, 36 };
		// 玩家牌数据
		//memcpy(_logic->TableData.m_byUserCards[0], icard, sizeof(icard));
		//memcpy(_logic->TableData.m_byUserCards[1], icard, sizeof(icard));
		//memcpy(_logic->TableData.m_byUserCards[2], icard, sizeof(icard));
		//std::vector<BYTE> cards;
		//for (size_t i = 0; i < USER_CARD_COUNT; i++) cards.push_back(icard[i]);
		//_logic->TableData.m_EnUserState[0] = STATE_NORMAL;
		//_logic->TableData.m_EnUserState[1] = STATE_NORMAL;
		//_logic->TableData.m_EnUserState[2] = STATE_NORMAL;
		//_logic->TableData.m_EnUserState[3] = STATE_NORMAL;
		//onGameSendCard(cards);
		//auto sp = Sprite::create();
		//addChild(sp);
		//sp->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=](){
		//	//显示操作容器
		//	_btnReady->setVisible(false);
		//	_controller.loadCards(cards);
		//	_controller.cardBoardTouch(true);
		//	onGameNoticeOpenCard();
		//}), nullptr));
	}

	void GameTableUI::onEnter()
	{
		HNGameUIBase::onEnter();
		NotificationCenter::getInstance()->addObserver(this, callfuncO_selector(GameTableUI::sendCardActionFinsh), "SendCardEventFinish", nullptr);
		HNAudioEngine::getInstance()->playBackgroundMusic("CaiShenThirteenCard/sound/bg.mp3");
	}

	void GameTableUI::onExit()
	{
		SpriteFrameCache* cache = SpriteFrameCache::getInstance();
		cache->removeSpriteFramesFromFile("CaiShenThirteenCard/GamePlist/cards.plist");
		cache->removeSpriteFramesFromFile("CaiShenThirteenCard/GamePlist/gameType.plist");
		cache->removeSpriteFramesFromFile("CaiShenThirteenCard/GamePlist/h_cartoon.plist");

		NotificationCenter::getInstance()->removeObserver(this, "SendCardEventFinish");
		HNGameUIBase::onExit();
	}

	void GameTableUI::doLoad()
	{

		//玩家
		auto rootNode = CSLoader::createNode("CaiShenThirteenCard/GameTableUi/Node_TableUi.csb");
		rootNode->setPosition(Vec2(WIN_SIZE.width / 2, WIN_SIZE.height / 2));
		this->addChild(rootNode);

		

		_tableUi = dynamic_cast<Layout*>(rootNode->getChildByName("Panel_TableUi"));
		float scaleY = WIN_SIZE.height / _tableUi->getContentSize().height;
		_tableUi->setScaleY(scaleY);

		_BottonScore = Sprite::create("platform/common/df_frame.png");
		_BottonScore->setPosition(Vec2(1280 / 2 - 300, 720 / 2 - 330));
		_tableUi->addChild(_BottonScore);

		auto BottonLabel = Label::createWithSystemFont("", "", 50);
		//auto BottonLabel = CCLabelAtlas::create("", "CaiShenDDZ/tableUI/res/difenshuzi.png", 32, 41, '.');
		BottonLabel->setPosition(Vec2(87, 22));
		BottonLabel->setColor(Color3B(255,255,0));
		BottonLabel->setScale(0.6);
		BottonLabel->setName("TextDi");
		_BottonScore->addChild(BottonLabel);


		char buf[128];
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			_players[i].doLoad(i, _tableUi);
			_players[i].showOrHideUser(false);
			/*_players[i].userMessageCallBack = [=](){
				BYTE lSeat = _logic->viewToLogicSeatNo(i);
				if (lSeat != INVALID_DESKSTATION)
				{
				auto userMessage = UserMessage::create(_deskNo, lSeat);
				userMessage->setPosition(_winSize / 2);
				addChild(userMessage, CHILD_LAYER_ZORDER);
				}
				};*/
		}

		//手牌
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			sprintf(buf, "HandCard_%d", i);
			_handCard[i].doLoad(i, _tableUi);
			_handCard[i].setIsSelf(0 == i);
		}
		//准备
		_btnReady = dynamic_cast<Button*>(_tableUi->getChildByName("Button_Agree"));
		_btnReady->addClickEventListener(CC_CALLBACK_1(GameTableUI::onReadyClick, this));
		_btnReady->setScaleY(720 / WIN_SIZE.height);

		auto panel_State = dynamic_cast<Layout*>(_tableUi->getChildByName("Panel_State"));
		_suite = CardSuite::create(0.8);
		_suite->setPosition(Vec2(WIN_SIZE.width / 2, WIN_SIZE.height / 2));
		panel_State->addChild(_suite);
		_suite->setScaleY(1 / scaleY);
		//长屏适配
		if (WIN_SIZE.width / WIN_SIZE.height >= 1.8)
		{
			_suite->setScale(scaleY, 1);
		}
		//设置
		if (roomController == nullptr)
		{
			roomController = VipRoomController::create(_deskNo);
		}
		//if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		//{
			roomController->onSetVipInfoCallBack = [this](){
				// 更新房主标志显示
				setDeskInfo();
			};
			roomController->onDissmissCallBack = [this]() {

				if (!_allRoundsEnd1)
				{
					auto prompt = GamePromptLayer::create();

					prompt->showPrompt(GBKToUtf8("桌子已解散"));
					prompt->setCallBack([=]() {
						RoomLogic()->close();
						GamePlatform::createPlatform();
					});
				}
			};

			addChild(roomController, 2999999);
			//roomController->getCardValue();
			// 修改房间号位置
			roomController->setRoomNumPos(Vec2(-15.0f, _winSize.height ));
			// 修改局数位置
			roomController->setPlayCountPos(Vec2(-15.0f, _winSize.height ));
			roomController->setInvitationBtnTexture("NN8/table/yaoqinghaoyou.png");
			// 			// 修改邀请好友位置
			//roomController->setInvitationBtnPos(Vec2(_winSize.width / 2, _winSize.height * 0.15f));
			// 修改返回位置 影藏
			roomController->setReturnBtnPos(Vec2(_winSize.width + 500.0f, 0));
			// 修改解散位置 影藏
			roomController->setDismissBtnPos(Vec2(_winSize.width + 500.0f, 0));
			//修改查看房间信息按钮位置
			roomController->setRoomResultBtnPos(Vec2(_winSize.width, _winSize.height+500));
		//}
		//auto Btn_set = dynamic_cast<Button*>(_tableUi->getChildByName("Button_Set"));
		auto Btn_set = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_tableUi, "Button_Set"));
		Btn_set->addClickEventListener([=](Ref* ref) {

			auto setLayer = GameSetLayer::create();
			setLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 20006);
			setLayer->setName("setLayer");
			setLayer->onExitCallBack = [=]() {
				if (roomController)
				{
					roomController->returnBtnCallBack(ref);
				}
				else
				{
					_logic->sendleave();
				}
				removeSetLayer();
			};
			setLayer->updateCheatStatus(m_bCheatShow);
			setLayer->onDisCallBack = [=]() {

				if (roomController) roomController->dismissBtnCallBack(ref);
				removeSetLayer();
			};
			setLayer->onShowMJCheatCallBack = [=](bool bShow){
				if (bShow)
					createCheatSwitchs();
				else
					clearCheatSwitchs();
			};
			if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
			{
				//设置游戏状态按钮
				setLayer->setGameStateView();
			}
			else
			{
				//设置金币场退出
				setLayer->setGameGlodExit();
			}
		});
		Btn_set->setPosition(Vec2(Btn_set->getPosition().x-10, Btn_set->getPosition().y - 6));
		//Btn_set->setPosition(Vec2(_winSize.width+50, _winSize.height + 500));
		
		//文字
		_btn_gameChat = dynamic_cast<Button*>(_tableUi->getChildByName("Button_Chat"));
		_btn_gameChat->addClickEventListener(CC_CALLBACK_1(GameTableUI::onChatClick, this));

		//语音	
		_btn_gameVoice = dynamic_cast<Button*>(_tableUi->getChildByName("Button_voice"));
		_btn_gameVoice->getTitleRenderer()->setVisible(false);
		_btn_gameVoice->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
			_chatLayer->voiceChatUiButtonCallBack(pSender, type);
		});

		_btn_gameChat->setScaleY(720 / WIN_SIZE.height);
		//_btn_gameVoice->setScale(0.8f, 0.8f * 720 / WIN_SIZE.height);

		//操作面板
		_controller.doLoad(_tableUi);
		_controller.setOutCardClickEvent(CC_CALLBACK_1(GameTableUI::onOutCardClick, this));
		_controller.doRestore();
		//顶部工具栏
		//initToolBarLayer();
		//战绩按钮
		auto btn_Record = dynamic_cast<Button*>(_tableUi->getChildByName("Button_Record"));
		btn_Record->setPositionY(Btn_set->getPositionY());
		//btn_Record->setPosition(Vec2(_winSize.width + 100, _winSize.height + 500));
		btn_Record->addClickEventListener([=](Ref* ref) {
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, S_C_GAME_RECORD, 0, NULL);

		});


	}

	void GameTableUI::CheckBoxCheatEvent(Ref* pSender, CheckBox::EventType type)
	{
		CheckBox* pCheckBox = dynamic_cast<CheckBox*>(pSender);
		if (pCheckBox)
		{
			std::string select_name = pCheckBox->getName();
			for (int i = 0; i < CHEAT_SSS_BUTTON_COUNT; i++)
			{
				auto pCurCheckBox = dynamic_cast<CheckBox*>(getChildByName(_sss_cb_names[i]));
				if (_sss_cb_names[i] == select_name)
				{//选中当前项,发送命令给服务端
					C_S_CheatStatus pData;
					pData.CheatStatus = _sss_cheat_commond[i];
					RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, C_S_CHEAT_STATUS, &pData, sizeof(pData));
				}
				else
				{
					pCurCheckBox->setSelected(false);
				}
			}
		}
	}

	void GameTableUI::createCheatSwitchs()
	{
		for (int i = 0; i < CHEAT_SSS_BUTTON_COUNT; i++)
		{
			auto pCheckBox = CheckBox::create("platform/createRoomUI/createUi/res/new/fx_dt.png", "platform/createRoomUI/createUi/res/new/gou_tb.png");
			pCheckBox->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			pCheckBox->setPosition(_sss_cb_points[i]);
			pCheckBox->setName(_sss_cb_names[i].c_str());
			pCheckBox->setSelected(i == 0 ? true : false);
			pCheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(GameTableUI::CheckBoxCheatEvent, this)));
			addChild(pCheckBox);

			Text* pText = Text::create(_sss_txt_titles[i].c_str(), "platform/common/RTWSYueRoudGoG0v1-Regular.ttf", 28);
			pText->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
			pText->setPosition(Vec2(55, pCheckBox->getContentSize().height / 2));
			pText->setColor(Color3B(255, 0, 0));
			pCheckBox->addChild(pText);
		}
		m_bCheatShow = true;
	}

	void GameTableUI::clearCheatSwitchs()
	{
		for (int i = 0; i < CHEAT_SSS_BUTTON_COUNT; i++)
		{
			auto pCheckBox = dynamic_cast<CheckBox*>(getChildByName(_sss_cb_names[i].c_str()));
			if (pCheckBox)
				pCheckBox->removeFromParent();
		}
		m_bCheatShow = false;
	}

	void GameTableUI::setPlayerPos()
	{
		float tableHeight = _tableUi->getContentSize().height;
		auto player1 = _tableUi->getChildByName("Panel_Player1");
		player1->setPositionY(tableHeight / 2);
		auto player3 = _tableUi->getChildByName("Panel_Player3");
		player3->setPositionY(tableHeight / 2);

		auto handcard1 = _tableUi->getChildByName("Panel_HandCard1");
		handcard1->setPositionY(tableHeight / 2);
		auto handcard3 = _tableUi->getChildByName("Panel_HandCard3");
		handcard3->setPositionY(tableHeight / 2);

		auto panel_State = dynamic_cast<Layout*>(_tableUi->getChildByName("Panel_State"));
		float stateHeght = panel_State->getContentSize().height;
		auto state1 = panel_State->getChildByName("Image_State1");
		state1->setPositionY(stateHeght / 2);
		auto state3 = panel_State->getChildByName("Image_State3");
		state3->setPositionY(stateHeght / 2);

		auto Panel_LightCard = dynamic_cast<Layout*>(_tableUi->getChildByName("Panel_LightCard"));
		float lightCardHeght = Panel_LightCard->getContentSize().height;
		auto lightcard1 = Panel_LightCard->getChildByName("Panel_User1");
		lightcard1->setPositionY(lightCardHeght / 2);
		auto lightcard3 = Panel_LightCard->getChildByName("Panel_User3");
		lightcard3->setPositionY(lightCardHeght / 2);
	}

	void GameTableUI::initToolBarLayer()
	{
		auto toolBarBG = dynamic_cast<Node*>(_tableUi->getChildByName("node_ToolsBar"));

		_toolBar.doLoad(toolBarBG);

		_toolBar._onExit = [this](){
			if (_logic->getGameStatus() != GS_WAIT_SETGAME &&
				_logic->getGameStatus() != GS_WAIT_AGREE &&
				_logic->getGameStatus() != GS_WAIT_NEXT &&
				_logic->TableData.m_EnUserState[_logic->getMySeatNo()] != STATE_ERR)
			{
				auto tip = Sprite::create("CaiShenThirteenCard/GameTableUi/res/game_exitTip.png");
				tip->setPosition(Vec2(WIN_SIZE / 2));
				_tableUi->addChild(tip, CHILD_LAYER_ZORDER);
				tip->setScale(0.1f);
				tip->runAction(Sequence::create(ScaleTo::create(0.2f, 1.0f),
					DelayTime::create(1.0f), ScaleTo::create(0.1f, 0.0f), RemoveSelf::create(true), nullptr));
				return;
			}

			auto exitFunc = [](bool force)
			{
				if (force) GamePlatform::returnPlatform(ROOMLIST);
				else GamePlatform::returnPlatform(DESKLIST);
			};

			HelperWrapper search(this);
			auto exitPrompt = search.find<ExitLayer>("exitLayer");
			if (nullptr == exitPrompt)
			{
				exitPrompt = ExitLayer::create();
				exitPrompt->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
				exitPrompt->setPosition(WIN_SIZE.width / 2, WIN_SIZE.height / 2);
				exitPrompt->setName("exitLayer");
				exitPrompt->setSureClickEvent([=]()
				{
					//正常退出
					_logic->sendUserUp(exitFunc);
					//强行退出
					//_logic->sendForceQuit(exitFunc);
				});
				addChild(exitPrompt, LOCAL_ZORDER_EXIT_LAYER);
			}
		};

		_toolBar._onSet = [this](){
			HelperWrapper search(this);
			auto sound = search.find<SoundLayer>("setLayer");
			if (nullptr == sound)
			{
				sound = SoundLayer::create();
				sound->setName("setLayer");
				addChild(sound, LOCAL_ZORDER_SET_LAYER);
				sound->setMusicCallback([&](bool open) {
					if (open)
					{
						HNAudioEngine::getInstance()->playBackgroundMusic("CaiShenThirteenCard/sound/bg.mp3");
					}
				});

			}
		};

		//文字
		//_btn_gameChat = dynamic_cast<Button*>(_tableUi->getChildByName("Button_Chat"));
		//_btn_gameChat->addClickEventListener(CC_CALLBACK_1(GameTableUI::onChatClick, this));

		////语音	
		//_btn_gameVoice = dynamic_cast<Button*>(_tableUi->getChildByName("Button_voice"));
		//_btn_gameVoice->getTitleRenderer()->setVisible(false);
		//_btn_gameVoice->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
		//	_chatLayer->voiceChatUiButtonCallBack(pSender, type);
		//});

		//_btn_gameChat->setScaleY(720 / WIN_SIZE.height);
		//_btn_gameVoice->setScale(0.8f, 0.8f * 720 / WIN_SIZE.height);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	void GameTableUI::onOutCardClick(ControllerLayer* controller)
	{
		INT heapCount[3] = { 3, 5, 5 };
		BYTE heapCard[3][5] = { 0 };
		controller->getCards(0, heapCard[0]);
		controller->getCards(1, heapCard[1]);
		controller->getCards(2, heapCard[2]);

		_logic->sendOpenCard(heapCount, heapCard);
	}

	void GameTableUI::restoreChildUI()
	{
		HelperWrapper search(this);
		{
			auto sound = search.find<SoundLayer>("setLayer");
			if (nullptr != sound) sound->closeSet();

			auto exit = search.find<ExitLayer>("exitLayer");
			if (nullptr != exit) exit->colseExit();
		}
	}

	void GameTableUI::onReadyClick(cocos2d::Ref *pSender)
	{
		_logic->sendAgreeGame();

		_btnReady->setVisible(false);

		BYTE lMySeatNo = _logic->getMySeatNo();
		BYTE vMySeatNo = _logic->logicToViewSeatNo(lMySeatNo);
		_players[vMySeatNo].showTimer(0, false);
	}

	void GameTableUI::onSettlementTimer()
	{
		//_btnReady->setVisible(true);
		_settlement = nullptr;
		BYTE lMySeatNo = _logic->getMySeatNo();
		BYTE vMySeatNo = _logic->logicToViewSeatNo(lMySeatNo);

		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			BYTE v = _logic->logicToViewSeatNo(i);
			if (!_players[v].isValid()) continue;

			//_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
			//_handCard[v].doRestore();
			_players[v].showTimer(0, false);
			_players[v].setUserIsNT(false);
		}

		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			if (!_logic->_allRoundsEnd)
			{
				onReadyClick(nullptr);
			}
			else
			{
				if (_calculateBoard)
				{
					_calculateBoard->setVisible(true);
				}
			}
		}
		else
		{
			_btnReady->setVisible(true);
			_players[vMySeatNo].showTimer(_logic->TableData.m_byBeginTime, true, [=](){
				RoomLogic()->close();
				GamePlatform::createPlatform();
			});
		}
	}

	void GameTableUI::sendGift(BYTE senderSeatNo, BYTE receiverSeatNo, int giftNo)
	{
		auto SSeatNo = _logic->logicToViewSeatNo(senderSeatNo);
		auto ESeatNo = _logic->logicToViewSeatNo(receiverSeatNo);
		if (_players[SSeatNo].isValid() && _players[ESeatNo].isValid())
		{
			_players[SSeatNo].playGiftAni(_players[SSeatNo].getPlayerPos(), _players[ESeatNo].getPlayerPos(), giftNo);
		}
	}

	//赠送礼物回复
	//void GameTableUI::dealGift(TMSG_SENG_GIFT_REQ data)
	//{
	//	int giftNo = data.iGiftNo;
	//	if (data.isSendAllPlayers)
	//	{
	//		for (int i = 0; i < PLAY_COUNT; i++)
	//		{
	//			auto userInfo = UserInfoModule()->findUser(_deskNo, i);
	//			if (userInfo && (i != data.bSenderSeatNo))
	//			{
	//				//移动礼物并播放动画
	//				sendGift(data.bSenderSeatNo, i, giftNo);
	//			}
	//		}
	//	}
	//	else
	//	{
	//		//移动礼物并播放动画
	//		sendGift(data.bSenderSeatNo, data.bReceiverSeatNo, giftNo);
	//	}
	//}

	////////////////////////////////////////////////////////////////////////////////////////////////
	void GameTableUI::onGSFree(const bool userReady[PLAY_COUNT])
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			BYTE v = _logic->logicToViewSeatNo(i);

			_handCard[v].doRestore();
			if (i == _logic->getMySeatNo()) _controller.doRestore();
			_players[v].showTimer(0, false);
			if (userReady[i])
			{
				_players[v].setStateLabel(GamePlayer::STATE_LABEL::READY);
			}
			else
			{
				_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
				if (i == _logic->getMySeatNo())
				{
					if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
					{
						_btnReady->setVisible(true);
					}
					else
					{
						_btnReady->setVisible(true);
						_players[v].showTimer(_logic->TableData.m_byBeginTime, true, [=](){
							RoomLogic()->close();
							GamePlatform::createPlatform();
						});
					}
				}
			}
		}
	}

	void GameTableUI::onGSRobNt(BYTE currRobUser)
	{
		// 显示谁不抢
		//for (size_t i = 0; i < PLAY_COUNT; i++)
		//{
		//	BYTE v = _logic->logicToViewSeatNo(i);

		//	_players[v].showTimer(0, false);

		//	// 没有轮到抢庄
		//	if (STATE_NORMAL == _logic->TableData.m_EnUserState[i])
		//	{
		//		_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
		//	}

		//	// 已经抢过庄，不抢
		//	if (STATE_HAVE_ROB == _logic->TableData.m_EnUserState[i])
		//	{
		//		_players[v].setStateLabel(GamePlayer::STATE_LABEL::UNCALL);
		//	}
		//}
		BYTE v = _logic->logicToViewSeatNo(currRobUser);
		if (v == 0)
		{
			if (_btn_qiangzhaung == nullptr)
			{
				_btn_qiangzhaung = Button::create();
				_btn_qiangzhaung->loadTextureNormal("CaiShenThirteenCard/GameTableUi/res/qiangzhuang.png", ui::Widget::TextureResType::LOCAL);
				_btn_qiangzhaung->addClickEventListener(CC_CALLBACK_1(GameTableUI::onQiangzhuangClick, this));
				_btn_qiangzhaung->setPosition(Vec2(_btnReady->getPosition().x - 150, _btnReady->getPosition().y));
				this->addChild(_btn_qiangzhaung, 299);
			}
			else
			{
				_btn_qiangzhaung->setVisible(true);
			}
			if (_btn_buqiang == nullptr)
			{
				_btn_buqiang = Button::create();
				_btn_buqiang->loadTextureNormal("CaiShenThirteenCard/GameTableUi/res/buqiang.png", ui::Widget::TextureResType::LOCAL);
				_btn_buqiang->addClickEventListener(CC_CALLBACK_1(GameTableUI::onBuqiangClick, this));
				_btn_buqiang->setPosition(Vec2(_btnReady->getPosition().x + 150, _btnReady->getPosition().y));
				this->addChild(_btn_buqiang, 299);
			}
			else
			{
				_btn_buqiang->setVisible(true);
			}
		}
		//	_players[v].showTimer(_logic->TableData.m_byRemaindTime, true);
	}

	void GameTableUI::onGSSendCard(const std::vector<BYTE>& cards)
	{
		//设置庄家
		if (_logic->TableData.m_byNowNtStation != 255)
		{
			BYTE vBanker = _logic->logicToViewSeatNo(_logic->TableData.m_byNowNtStation);
			_players[vBanker].setUserIsNT(true);
		}

		//显示各玩家的牌
		BYTE lMySeatNo = _logic->getMySeatNo();

		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			_players[i].showTimer(0, false);

			if (STATE_ERR == _logic->TableData.m_EnUserState[i]) continue;
			if (i == lMySeatNo) continue;

			BYTE v = _logic->logicToViewSeatNo(i);
			//_players[v].showTimer(_logic->TableData.m_byRemaindTime, true);
			_handCard[v].sendCard(0.0f);
		}

		if (STATE_ERR != _logic->TableData.m_EnUserState[lMySeatNo])
		{
			restoreChildUI();
			_controller.loadCards(cards);
		}
		else
		{
			_controller.doRestore();
		}
	}

	void GameTableUI::onGSOpenCard(const std::vector<BYTE>& cards, const BYTE heapCard[3][5])
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			BYTE v = _logic->logicToViewSeatNo(i);
			_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
			_players[v].showTimer(0, false);
			if (i == _logic->getMySeatNo()) _controller.doRestore();
		}
		if (_logic->TableData.m_byNowNtStation != 255)
		{
			BYTE vBanker = _logic->logicToViewSeatNo(_logic->TableData.m_byNowNtStation);
			_players[vBanker].setUserIsNT(true);
		}

		//显示倒计时（把自己位置的计时器作为公共计时器）
		BYTE lMySeatNo = _logic->getMySeatNo();
		BYTE vSeatNo = _logic->logicToViewSeatNo(lMySeatNo);

		//自己不是中途加入
		if (STATE_ERR != _logic->TableData.m_EnUserState[lMySeatNo])
		{
			BYTE vSeatNo = _logic->logicToViewSeatNo(lMySeatNo);

			//已经摆牌了
			if (STATE_OPEN_CARD == _logic->TableData.m_EnUserState[lMySeatNo])
			{
				_handCard[vSeatNo].setLightCardVisible(true);
				showMyCard(heapCard);
				_players[vSeatNo].setStateLabel(GamePlayer::STATE_LABEL::ZPDONE);
			}
			else
			{
				//显示操作容器
				_controller.loadCards(cards);
				_controller.cardBoardTouch(true);
			}
		}
		else
		{
			HNLOGEX_INFO("%s", "gamestation_KaiPai_myState_ERROR");
		}
		//显示其他各玩家的牌
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (STATE_ERR == _logic->TableData.m_EnUserState[i]) continue;

			if (i == lMySeatNo) continue;

			BYTE v = _logic->logicToViewSeatNo(i);

			//如果已经摆牌了 那么就显示摆牌数据
			if (STATE_OPEN_CARD == _logic->TableData.m_EnUserState[i])
			{
				//	_handCard[v].setLightCardVisible(true);
				_players[v].setStateLabel(GamePlayer::STATE_LABEL::ZPDONE);
			}
			else
			{
				_handCard[v].setLightCardVisible(false);

				_handCard[v].sendCard(0.0f);

				_players[v].setStateLabel(GamePlayer::STATE_LABEL::SORT);
			}
		}
		_controller.checkCardTypeBtnStatue();
	}

	void GameTableUI::onGSCompareCard(const BYTE heapCard[PLAY_COUNT][3][5])
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			BYTE v = _logic->logicToViewSeatNo(i);
			_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
			_players[v].showTimer(0, false);
			if (i == _logic->getMySeatNo()) _controller.doRestore();
		}

		//设置庄家
		BYTE v = _logic->logicToViewSeatNo(_logic->TableData.m_byNowNtStation);
		_players[v].setUserIsNT(true);

		doCompareCard(heapCard, _logic->TableData.m_byRemaindTime);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	void GameTableUI::onAddPlayer(BYTE lSeatNo, LLONG userID, bool self)
	{
		BYTE v = _logic->logicToViewSeatNo(lSeatNo);
		UserInfoStruct* user = _logic->getUserBySeatNo(lSeatNo);
		_players[v].setLogicSeatNo(lSeatNo);
		_players[v].setViewSeatNo(v);
		_players[v].setUserID(userID);
		_players[v].setName(user->nickName);
		//_players[v].setGold(user->i64Money);
		_players[v].setGold(0);
		_players[v].setHead(user->bBoy, user->dwUserID, user->bLogoID);
		_players[v].setMySelf(self);
		_players[v].showOrHideUser(true);
		
		
	}
	void GameTableUI::onVipTip(BYTE lSeatNo)
	{
		UserInfoStruct* user = _logic->getUserBySeatNo(lSeatNo);
		if (user->iVipLevel > 0)
		{

			std::string str = "欢迎尊贵的Vip";
			str.append(StringUtils::toString(user->iVipLevel));
			str.append("玩家：");
			str.append(user->nickName);
			str.append(",进入房间！");
			GameNotice::getInstance()->postMessage(GBKToUtf8(str.c_str()),0);
		}
	}

	void GameTableUI::onRemovePlayer(BYTE vSeatNo, LLONG userID, bool self)
	{
		_players[vSeatNo].doRestore();
		_handCard[vSeatNo].doRestore();
	}

	void GameTableUI::onSetGameBasePoint(INT point)
	{
		Text* txtDiZhu = (Text*)_tableUi->getChildByName("Text_diZhu");
		if (nullptr != txtDiZhu) {
			txtDiZhu->setString(StringUtils::format(GBKToUtf8("底注：%d"), point));
		}
	}

	void GameTableUI::onGameAgree(BYTE lSeatNo, bool self)
	{
		BYTE v = _logic->logicToViewSeatNo(lSeatNo);
		_players[v].setStateLabel(GamePlayer::STATE_LABEL::READY);
		//_handCard[v].doRestore();

		HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/ready.mp3");

		if (self)
		{
			_btnReady->setVisible(false);
			_players[v].showTimer(0, false);
			//if (_isOwner)
			//{
			////	_btn_beginGame->setVisible(false);
			//	_btnReady->setVisible(false);
			//}

		}
	}

	void GameTableUI::onGameNoticeRobNt(BYTE lSeatNo, bool self)
	{
		//视图位置
		_btnReady->setVisible(false);

		//for (size_t i = 0; i < PLAY_COUNT; i++)
		//{
		//	BYTE v = _logic->logicToViewSeatNo(i);
		//	_handCard[v].doRestore();

		//	// 没有轮到抢庄
		//	if (STATE_NORMAL == _logic->TableData.m_EnUserState[i])
		//	{
		//		_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
		//	}

		//	// 已经抢过庄，不抢
		//	if (STATE_HAVE_ROB == _logic->TableData.m_EnUserState[i])
		//	{
		//		_players[v].setStateLabel(GamePlayer::STATE_LABEL::UNCALL);
		//	}
		//}

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			auto userInfo = _logic->getUserBySeatNo(i);
			if (userInfo)
			{
				_handCard[_logic->logicToViewSeatNo(i)].doRestore();
			}
		}
		BYTE v = _logic->logicToViewSeatNo(lSeatNo);
		//if (v==0)
		{
			if (_btn_qiangzhaung==nullptr)
			{
				_btn_qiangzhaung = Button::create();
				_btn_qiangzhaung->loadTextureNormal("CaiShenThirteenCard/GameTableUi/res/qiangzhuang.png",ui::Widget::TextureResType::LOCAL);
				_btn_qiangzhaung->addClickEventListener(CC_CALLBACK_1(GameTableUI::onQiangzhuangClick, this));
				_btn_qiangzhaung->setPosition(Vec2(_btnReady->getPosition().x-150,_btnReady->getPosition().y));
				this->addChild(_btn_qiangzhaung, 299);
			}
			else
			{
				_btn_qiangzhaung->setVisible(true);
			}
			if (_btn_buqiang == nullptr)
			{
				_btn_buqiang = Button::create();
				_btn_buqiang->loadTextureNormal("CaiShenThirteenCard/GameTableUi/res/buqiang.png", ui::Widget::TextureResType::LOCAL);
				_btn_buqiang->addClickEventListener(CC_CALLBACK_1(GameTableUI::onBuqiangClick, this));
				_btn_buqiang->setPosition(Vec2(_btnReady->getPosition().x + 150, _btnReady->getPosition().y));
				this->addChild(_btn_buqiang, 299);
			}
			else
			{
				_btn_buqiang->setVisible(true);
			}
		}
		//	_players[v].showTimer(_logic->TableData.m_byRobTime, true);
		_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
	}

	void GameTableUI::onGameRobNtResult(BYTE lSeatNo, bool robResult, bool self)
	{
		if (STATE_ERR == _logic->TableData.m_EnUserState[lSeatNo]) return;

		BYTE v = _logic->logicToViewSeatNo(lSeatNo);
		_players[v].showTimer(0, false);

		if (robResult)
		{
			for (size_t i = 0; i < PLAY_COUNT; i++)
			{
				_players[v].showTimer(0, false);
				_players[i].setStateLabel(GamePlayer::STATE_LABEL::NONE);
			}

			_players[v].setUserIsNT(robResult);
		}
		else
		{
			_players[v].setStateLabel(GamePlayer::STATE_LABEL::UNCALL);
		}
	}

	void GameTableUI::onGameMakeSureNt(BYTE lSeatNo, bool self, int QZSeatNO[])
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			_players[i].showTimer(0, false);
			_players[i].setStateLabel(GamePlayer::STATE_LABEL::NONE);
		}

		// 设置庄家
		BYTE v = _logic->logicToViewSeatNo(lSeatNo);
		if (self)
		{

		}
		else
		{

		}
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			BYTE seatNo = _logic->logicToViewSeatNo(i);
			if (1 == QZSeatNO[i] && i != lSeatNo)
			{
				_players[seatNo].showKuangBtnAction(true, false);
			}
			else if (i == lSeatNo)
			{
				_players[seatNo].showKuangBtnAction(true, true);
			}
		}
		//_players[v].showKuangBtnAction(true, true);
		//_players[v].setUserIsNT(self);
	}

	void GameTableUI::onGameSendCard(const std::vector<BYTE>& cards)
	{
		_btnReady->setVisible(false);

		float dt = (float)_logic->TableData.m_bySendCardTime / 13.0f;

		BYTE l = _logic->getMySeatNo();

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			auto userInfo = _logic->getUserBySeatNo(i);
			if (userInfo)
			{
				_handCard[_logic->logicToViewSeatNo(i)].doRestore();
			}
		}
		
		
		//设置庄家
		if (_logic->TableData.m_byNowNtStation!=255)
		{
			BYTE v = _logic->logicToViewSeatNo(_logic->TableData.m_byNowNtStation);
			_players[v].setUserIsNT(true);
		}

		if (STATE_ERR == _logic->TableData.m_EnUserState[l]) return;

		restoreChildUI();

		_controller.loadCards(cards);//cards, dt
		_controller.cardBoardTouch(false);
		_controller.setCardBoardVisible(false);
		
		sendCardAction(cards);
		//摆牌60S，摆不完帮你摆下面是倒计时
		//_players[0].showTimer(60, true);
	}

	void GameTableUI::onGameSetAutoRubCardType(BYTE byHeapCard[3][5], int iHeapShape[3])
	{
		_controller.setAutoRubCardType(byHeapCard, iHeapShape);
	}

	void GameTableUI::sendCardAction(const std::vector<BYTE>& cards)
	{
		///////////////////////////////////////
		//设置马牌
		//if (roomController->getCardValue() != 0)
		{
// 			std::string str = StringUtils::format("Games/CaiShenThirteenCard/GamePlist/0x%02X.png", roomController->getCardValue());
// 			_Sp_mapaiCard->setTexture(str);
		}
		// animate & data
		_isNowSendCard = true;//正在发牌
		_suite->shuffle(0.01f);
		Sequence* s = nullptr;

		BYTE station = 0;
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			auto masterInfo = UserInfoModule()->findUser(HNPlatformConfig()->getMasterID());
			if (masterInfo)
			{
				station = masterInfo->bDeskStation;
			}
		}

		for (BYTE handindex = 0; handindex < USER_CARD_COUNT; handindex++)
		{
			for (BYTE seatindex = 0; seatindex < PLAY_COUNT; seatindex++)
			{
				BYTE logiSeat = (seatindex + station) % PLAY_COUNT;

				BYTE viewSeat = _logic->logicToViewSeatNo(logiSeat);

				if (_players[viewSeat].isValid() && _logic->TableData.m_EnUserState[logiSeat] != STATE_ERR)
				{
					_suite->restoreCardSize(0.8);
					_handCard[viewSeat]._panel_handCard->setVisible(true);
					auto receiver = _handCard[viewSeat]._cardList[handindex];

					auto tarpos = receiver->getParent()->convertToWorldSpace(receiver->getPosition());
					BYTE value = ((logiSeat == _logic->getMySeatNo()) ? cards[handindex] : 0);


					auto func = CallFunc::create([=](){
						_handCard[viewSeat]._cardList[handindex]->setVisible(true);
						if (viewSeat == 0)
						{
							_handCard[viewSeat].loadCardsTexture(_handCard[viewSeat]._cardList[handindex], value);
							//auto z =roomController->getCardValue();
							//if (roomController->getCardValue() == value)
							//{
							//	//如果马牌在手中，显示
							//	int x = 0;
							//}
						}
						_handCard[viewSeat].isLastCard(seatindex, handindex);
					});

					auto handaction = TargetedAction::create(receiver, func);

					float dsAngle = (viewSeat == 0 || viewSeat == 2) ? 180 : 90;

					float fScale = (viewSeat == 0) ? 0.8f : 0.3f;

					auto suiteaction = _suite->dealCard(0.02f, tarpos, dsAngle, fScale);

					if (s == nullptr)
					{
						s = Sequence::create(suiteaction, handaction, CallFunc::create([=](){
						}), nullptr);
					}
					else
					{
						s = Sequence::create(s, suiteaction, handaction, CallFunc::create([=](){
						}), nullptr);
					}
				}
			}
		}
		_suite->runActionPair(this, s);
		runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=](){
			_isNowSendCard = false;//发牌完成
		}), nullptr));
		_suite->recycle(0.01f);

	}


	void GameTableUI::onGameNoticeOpenCard()
	{
		BYTE l = _logic->getMySeatNo();
		BYTE v = _logic->logicToViewSeatNo(l);
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			_players[v].showTimer(_logic->TableData.m_byThinkTime, true);
		}

		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			BYTE v = _logic->logicToViewSeatNo(i);
			if (!_players[v].isValid()) continue;
			if (STATE_ERR == _logic->TableData.m_EnUserState[i]) continue;
			//跳过自己
			if (i == l) continue;

			_players[v].setStateLabel(GamePlayer::STATE_LABEL::SORT);
		}

		if (STATE_ERR == _logic->TableData.m_EnUserState[l])
		{
			HNLOGEX_INFO("%s", "gamemessageKaiPai_myState_ERROR");
			return;
		}

		if (!_isNowSendCard)//不是发牌中
		{
			HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/ding.mp3");
			_controller.cardBoardTouch(true);
			_controller.checkCardTypeBtnStatue();
		}
		else
		{
			runAction(Sequence::create(DelayTime::create(0.5f), CallFunc::create([=](){
				HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/ding.mp3");
				_controller.cardBoardTouch(true);
				_controller.checkCardTypeBtnStatue();
			}), nullptr));
		}

	}

	void GameTableUI::onGameOpenCardResult(BYTE lSeatNo)
	{
		BYTE v = _logic->logicToViewSeatNo(lSeatNo);
		_players[v].showTimer(0, false);

		if (STATE_ERR == _logic->TableData.m_EnUserState[lSeatNo]) return;
		_players[v].setStateLabel(GamePlayer::STATE_LABEL::ZPDONE);
		_handCard[v].sethandCardVisible(false);

		if (lSeatNo == _logic->getMySeatNo()) _controller.doRestore();

		HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/bipaiyingfen.mp3");
	}

	void GameTableUI::onGameCompareCard(const BYTE heapCard[PLAY_COUNT][3][5])
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			BYTE v = _logic->logicToViewSeatNo(i);
			_players[v].showTimer(0, false);
			if (STATE_ERR == _logic->TableData.m_EnUserState[i]) continue;
			if (!_players[v].isValid()) continue;

			_players[v].setStateLabel(GamePlayer::STATE_LABEL::NONE);
			_handCard[v].sethandCardVisible(false);
		}
		doCompareCard(heapCard, _logic->TableData.m_byCompareTime);
	}

	void GameTableUI::onGameOpenFire(const bool userFireUser[PLAY_COUNT][PLAY_COUNT])
	{
		//二维数组，一维下标打二维下标
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			for (int j = 0; j < PLAY_COUNT; j++)
			{
				if (userFireUser[i][j])
				{
					BYTE fireViewSeatNo = _logic->logicToViewSeatNo(i);
					BYTE beFireViewSeatNo = _logic->logicToViewSeatNo(j);

					_fireSeatNo.clear();
					_beFireSeatNo.clear();
					_fireSeatNo.push_back(fireViewSeatNo);
					_beFireSeatNo.push_back(beFireViewSeatNo);
					playFireAnimation();
					_isfire[i] = true;
				}
			}
		}
		
	}

	//被打枪音效		
	void GameTableUI::onGameBeOpenFireEffect()
	{
		HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/daqiang.mp3");
	}

	void GameTableUI::onGameFireAll(BYTE lSeatNo)
	{
		//if (_colorLayer == nullptr)
		//{
		//	_colorLayer = LayerColor::create(Color4B(0, 0, 0, 200));
		//	this->addChild(_colorLayer, 500);
		//}

		//BYTE fireViewSeatNo = _logic->logicToViewSeatNo(lSeatNo);
		//auto fireUserhead = _players[fireViewSeatNo].getChatParent();
		//Vec2 pos0 = fireUserhead->convertToWorldSpace(fireUserhead->getPosition());
		//Vec2 pos1 = this->convertToNodeSpace(pos0);
		//Vec2 pos = _colorLayer->convertToNodeSpace(pos1);

		////打枪或全垒打骨骼动画
		//auto gameShoot = SkeletonAnimation::createWithFile(GameShoot_Json, GameShoot_Atlas);
		//auto ani = gameShoot->setAnimation(0, "gameShoot", false);
		//_colorLayer->addChild(gameShoot);
		//gameShoot->setPosition(Director::getInstance()->getWinSize() / 2);

		//auto head = _players[fireViewSeatNo].getUserHeadImg();

		//Sprite* firehead = Sprite::create(head->getRenderFile().file);
		//firehead->setPosition(pos);
		//_colorLayer->addChild(firehead);

		//gameShoot->setTrackEndListener(ani, [&](int trackIndex){
		//	runAction(Sequence::create(DelayTime::create(0.02f), CallFunc::create([=](){
		//		_colorLayer->removeFromParent();
		//		_colorLayer = nullptr;
		//	}), nullptr));
		//});
		if (0 == _logic->logicToViewSeatNo(_logic->_Zhuang))
		{
			tipsAllOpenFireEffect(_players[_logic->_Zhuang].getUserSex());
		}
		this->runAction(Sequence::create(DelayTime::create(1.f), CallFunc::create([=]()
		{
			Size size = Director::getInstance()->getWinSize();
			cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("CaiShenThirteenCard/animation/quanleida0.png", "CaiShenThirteenCard/animation/quanleida0.plist"
				, "CaiShenThirteenCard/animation/quanleida.ExportJson");
			auto  armature = cocostudio::Armature::create("quanleida");
			armature->getAnimation()->play("quanleida");
			armature->setScale(0.5f);
			armature->setPosition(size.width / 2, size.height / 2 + 12);
			armature->setVisible(true);
			this->addChild(armature, 100);

			HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/quanleida.mp3");

			std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)
			{
				if (type == MovementEventType::COMPLETE)
				{
					armature->getAnimation()->stop();
					armature->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature)), NULL));
				}
			};
			armature->getAnimation()->setMovementEventCallFunc(armatureFun);
		}), nullptr));

	}

	void GameTableUI::onGameFireAllEffect()
	{
		HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/quanleida.mp3");
	}

	void GameTableUI::playFireAnimation()
	{
		/*if (_fireSeatNo.empty() || _beFireSeatNo.empty())
		{
			if (_colorLayer != nullptr)
			{
				_colorLayer->removeFromParent();
				_colorLayer = nullptr;
			}
			return;
		}
		BYTE fireViewSeatNo = _fireSeatNo.front();
		BYTE beFireViewSeatNo = _beFireSeatNo.front();

		if (_colorLayer == nullptr)
		{
			_colorLayer = LayerColor::create(Color4B(0, 0, 0, 200));
			this->addChild(_colorLayer, 500);
		}

		Size winSize = Director::getInstance()->getWinSize();
		auto fireUserhead = _players[fireViewSeatNo].getChatParent();
		Vec2 pos0_0 = fireUserhead->convertToWorldSpace(fireUserhead->getPosition());
		Vec2 pos1_0 = this->convertToNodeSpace(pos0_0);
		Vec2 pos_0 = _colorLayer->convertToNodeSpace(pos1_0);

		auto beFireUserhead = _players[beFireViewSeatNo].getChatParent();
		Vec2 pos0_1 = beFireUserhead->convertToWorldSpace(beFireUserhead->getPosition());
		Vec2 pos1_1 = this->convertToNodeSpace(pos0_1);
		Vec2 pos_1 = _colorLayer->convertToNodeSpace(pos1_1);

		//打枪或全垒打骨骼动画
		auto gameShoot = SkeletonAnimation::createWithFile(GameShoot_Fire_Json, GameShoot_Fire_Atlas);
		auto ani = gameShoot->setAnimation(0, "qiangji", false);
		_colorLayer->addChild(gameShoot);
		gameShoot->setPosition(winSize / 2);

		auto fire = _players[fireViewSeatNo].getUserHeadImg();
		Sprite* firehead = Sprite::create(fire->getRenderFile().file);
		firehead->setPosition(Vec2(winSize.width / 4, winSize.height / 2));
		_colorLayer->addChild(firehead);

		auto befire = _players[beFireViewSeatNo].getUserHeadImg();
		Sprite* beFirehead = Sprite::create(befire->getRenderFile().file);
		beFirehead->setPosition(Vec2(winSize.width * 3 / 4, winSize.height / 2));
		_colorLayer->addChild(beFirehead, 100);

		gameShoot->setTrackEndListener(ani, [=](int trackIndex){
			runAction(Sequence::create(DelayTime::create(0.02f), CCCallFunc::create([=](){

				beFirehead->runAction(Sequence::create(Spawn::create(RotateBy::create(0.5, 90), MoveBy::create(0.5, Vec2(200, 0)), nullptr)
					, Spawn::create(RotateBy::create(0.5, 90), MoveBy::create(0.5, Vec2(200, 0)), nullptr)
					, CCCallFunc::create([=](){
					playFireAnimation();
				})
					, nullptr));

			}), nullptr));

		});

		_fireSeatNo.pop_front();
		_beFireSeatNo.pop_front();*/
	/*	if (_fireSeatNo.empty() || _beFireSeatNo.empty())
		{
			if (_colorLayer != nullptr)
			{
				_colorLayer->removeFromParent();
				_colorLayer = nullptr;
			}
			return;
		}*/
		BYTE fireViewSeatNo = _fireSeatNo.front();
		BYTE beFireViewSeatNo = _beFireSeatNo.front();

		Size winSize = Director::getInstance()->getWinSize();
		auto fireUserhead = _players[fireViewSeatNo].getChatParent();
		Vec2 pos0_0 = fireUserhead->convertToWorldSpace(fireUserhead->getPosition());
		Vec2 pos1_0 = this->convertToNodeSpace(pos0_0);
		//Vec2 pos_0 = _colorLayer->convertToNodeSpace(pos1_0);

		auto beFireUserhead = _players[beFireViewSeatNo].getChatParent();
		Vec2 pos0_1 = beFireUserhead->convertToWorldSpace(beFireUserhead->getPosition());
		Vec2 pos1_1 = this->convertToNodeSpace(pos0_1);
		//Vec2 pos_1 = _colorLayer->convertToNodeSpace(pos1_1);
		if (pos0_0 == pos0_1)
		{
			return;
		}
		//打枪动画
		Size size = Director::getInstance()->getWinSize();
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("CaiShenThirteenCard/animation/daqiang0.png", "CaiShenThirteenCard/animation/daqiang0.plist"
			, "CaiShenThirteenCard/animation/daqiang.ExportJson");

		if (!_isfire[_logic->viewToLogicSeatNo(fireViewSeatNo)])
		{
			tipsOpenFireEffect(_players[_logic->viewToLogicSeatNo(fireViewSeatNo)].getUserSex());
		auto  armature = cocostudio::Armature::create("daqiang");
			armature->getAnimation()->play("daqiangdonghua");
			armature->setScale(0.55f);
			//armature->setPosition(size.width / 2, size.height / 2 + 12);
			armature->setPosition(pos0_0.x + 50,pos0_0.y + 40 );
			armature->setVisible(true);
			this->addChild(armature, 100);
			if (fireViewSeatNo == 0 || fireViewSeatNo == 1||fireViewSeatNo == 4)
			{
				armature->setPosition(pos0_0.x + 100, pos0_0.y + 40);
			}
			else if (fireViewSeatNo == 5 || fireViewSeatNo == 3)
			{
				armature->setPosition(pos0_0.x , pos0_0.y + 40);
				armature->setScaleX(-0.55f);
			}
			else if ((pos0_0.x - pos0_1.x)>0)
			{
				armature->setScaleX(-0.55f);
			}
			std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)
	
			{
				if (type == MovementEventType::COMPLETE)
				{
					armature->getAnimation()->stop();
					armature->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature)), NULL));
				}
			};
			armature->getAnimation()->setMovementEventCallFunc(armatureFun);
	}

		auto  armature1 = cocostudio::Armature::create("daqiang");
		armature1->getAnimation()->play("qiangdong");
		armature1->setScale(0.729f);
		//armature->setPosition(size.width / 2, size.height / 2 + 12);
		armature1->setPosition(pos0_1.x + 30, pos0_1.y + 30);
		armature1->setVisible(true);
		this->addChild(armature1, 100);
		std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun1 = [=](Armature* armature1, MovementEventType type, const std::string& id)

		{
			if (type == MovementEventType::COMPLETE)
			{
				armature1->getAnimation()->stop();
				armature1->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature1)), NULL));
				runAction(Sequence::create(DelayTime::create(1.0f), CCCallFunc::create([=](){
					if (_isAllFire)
					{
						onGameFireAll(fireViewSeatNo);
						_isAllFire = false;
					}
				}), nullptr));
			}
		};
		armature1->getAnimation()->setMovementEventCallFunc(armatureFun1);

	}

	void GameTableUI::onGameResult(const S_C_GameResult* pObject)
	{
		
		_settlement = SettlementLayer::create();
		_settlement->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		_settlement->setPosition(WIN_SIZE.width / 2, WIN_SIZE.height / 2);
		Director::getInstance()->getRunningScene()->addChild(_settlement, LOCAL_ZORDER_SETTLEMENT_LAYER, TAG_SETTLEMENT_LAYER);
		_settlement->setCloseCallback(CC_CALLBACK_0(GameTableUI::onSettlementTimer, this));

		BYTE lMySeatNo = _logic->getMySeatNo();
		std::vector<SettlementParameter> pragmas;
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			if (STATE_ERR == _logic->TableData.m_EnUserState[i]) continue;
			BYTE v = _logic->logicToViewSeatNo(i);
			_players[v].setGold(_players[v].getGold() + pObject->i64UserMoney[i]);

			UserInfoStruct* user = _logic->getUserBySeatNo(i);

			SettlementParameter pragma;
			pragma.score = pObject->i64UserMoney[i];
			pragma.tax = pObject->i64UserFen[i] - pObject->i64UserMoney[i];
			pragma.nickName = user->nickName;
			pragma.self = (i == lMySeatNo);
			pragma.fireScore = pObject->i64UserFireScore[i];
			pragma.allHitScore = pObject->i64UserAllHitScore[i];
			pragma.isBoy = user->bBoy;
			pragma.sHeadUrl = user->headUrl;
			pragma.iUserID = user->dwUserID;
			memcpy(pragma.byHeapCard, pObject->byHeapCard[i], sizeof(pObject->byHeapCard[i])); //摆牌的数据
			memcpy(pragma.ComepareResult, pObject->iComepareResult[i], sizeof(pObject->iComepareResult[i])); //每道的得分
			pragma.iExtraScore = pObject->iExtraScore[i];                  //额外的得分
			pragma.palyMode = _logic->_gameMode;
			//是否为庄
			pragma.isZhuang = false;
			if (i == (_logic->TableData.m_byNowNtStation))
			{
				pragma.isZhuang = true;
			}
			pragmas.push_back(pragma);
		}
		_settlement->loadParameter(pragmas, _isNowCompareCard);
		if (_isNowCompareCard) //正在比牌 延迟显示小结算界面
		{
			_settlement->setVisible(false);
			_settlement->runAction(Sequence::create(DelayTime::create(1.0f), Show::create(), nullptr));
		}
		for (int i = 0; i < 6;i++)
		{
			_isfire[i] = false;
		}
		_isAllFire = false;
	}

	void GameTableUI::doCompareCard(const BYTE heapCard[PLAY_COUNT][3][5], BYTE time)
	{
		float dt = 0.0f;
		_isNowCompareCard = true;
		if (time >= 3)
		{
			//dt = (float)(time - 3) / 3;
			dt = (float)time / 3.0f;
		}

		if (time >= 3)
		{
			for (size_t i = 0; i < PLAY_COUNT; i++)
			{
				if (i == _logic->getMySeatNo()) continue;
				BYTE v = _logic->logicToViewSeatNo(i);
				if (!_players[v].isValid()) continue;
				_players[v].setStateLabel(GamePlayer::STATE_LABEL::ZPDONE);
			}
			this->runAction(Sequence::create(
				DelayTime::create(1.0f),
				CallFunc::create([=](){
				for (size_t i = 0; i < PLAY_COUNT; i++) _players[i].setStateLabel(GamePlayer::STATE_LABEL::NONE);
				compareCard(heapCard, dt);
			}), nullptr));
		}
		else
		{
			compareCard(heapCard, dt);
		}
	}

	void GameTableUI::compareCard(const BYTE heapCard[PLAY_COUNT][3][5], float litghtCardTime)
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			if (STATE_ERR == _logic->TableData.m_EnUserState[i])
			{
				if (i == _logic->getMySeatNo())
				{
					HNLOGEX_INFO("%s", "BiPai_myState_ERROR");
				}
				continue;
			}

			BYTE v = _logic->logicToViewSeatNo(i);
			if (!_players[v].isValid()) continue;

			std::vector<BYTE> cards;
			for (size_t j = 0; j < 3; j++)
			{
				for (size_t k = 0; k < 5; k++)
				{
					BYTE val = heapCard[i][j][k];
					//HNLOG_INFO("i = %d, v = %d, c = %02X", i, v, val);
					if (0 == val) continue;
					if (0xFF == val) continue;
					cards.push_back(val);
					//挂马牌
				/*	if (roomController->getCardValue() ==val)
					{
						int x = 0;
					}*/
				}
			}

			if (cards.empty())
			{
				_handCard[v].setLightCardVisible(false);
			}
			else
			{
				_handCard[v].loadCards(cards);
				_handCard[v].setLightCardVisible(true);
			}
		}

		if (litghtCardTime >= 1)
		{
			if (_logic->_gameMode == 1)//温州玩法
			{
				this->runAction(Sequence::create(
					DelayTime::create(0.3f), CallFunc::create([&](){ doLightCard(0); }),			//亮牌
					DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(0); }),			//显示结果
					DelayTime::create(litghtCardTime), CallFunc::create([&](){ doLightCard(1); }),	//亮牌
					DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(1); }),			//显示结果
					DelayTime::create(litghtCardTime), CallFunc::create([&](){ doLightCard(2); }),	//亮牌
					DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(2); }),			//显示结果
					//DelayTime::create(0.5f), CallFunc::create([&](){ doShowScore(3); }),	//显示结果
					CallFunc::create([=](){
					_isNowCompareCard = false; }),
						nullptr));
			}
			else //福建玩法
			{
				this->runAction(Sequence::create(
					DelayTime::create(0.3f), CallFunc::create([&](){ doLightCard(0); }),			//亮牌
					DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(0); }),			//显示结果
					DelayTime::create(litghtCardTime), CallFunc::create([&](){ doLightCard(1); }),	//亮牌
					DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(1); }),			//显示结果
					DelayTime::create(litghtCardTime), CallFunc::create([&](){ doLightCard(2); }),	//亮牌
					DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(2); }),			//显示结果
					DelayTime::create(0.5f), CallFunc::create([&](){ doShowScore(3); }),	//显示结果
					CallFunc::create([=](){
					_isNowCompareCard = false; }),
						nullptr));
			}

		}
		else
		{
			this->runAction(Sequence::create(
				DelayTime::create(0.1f), CallFunc::create([&](){ doLightCard(0); }),			//亮牌
				DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(0); }),			//显示结果
				DelayTime::create(0.1f), CallFunc::create([&](){ doLightCard(1); }),			//亮牌
				DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(1); }),			//显示结果
				DelayTime::create(0.1f), CallFunc::create([&](){ doLightCard(2); }),			//亮牌
				DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(2); }),			//显示结果
				DelayTime::create(0.1f), CallFunc::create([&](){ doShowScore(3); }),
				CallFunc::create([=](){ _isNowCompareCard = false; }),
				nullptr));
		}
	}

	void GameTableUI::doShowScore(int index)
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			if (STATE_ERR == _logic->TableData.m_EnUserState[i]) continue;

			BYTE v = _logic->logicToViewSeatNo(i);
			if (!_players[v].isValid()) continue;

			if (index != 3)
			{
				LLONG score = _logic->TableData.m_iComepareResult[i][index];
				BYTE cardType = _logic->TableData.m_iHeapShape[i][index];

				_handCard[v].showScore(index, _players[v].getUserSex(), cardType, score);
			}
			else
			{
				LLONG score = _logic->TableData.m_iExtraScore[i];
				_handCard[v].setScore(score, index);
			}
		}
	}
	//应该要改下面的代码
	void GameTableUI::doLightCard(int index)
	{
		for (size_t i = 0; i < PLAY_COUNT; i++)
		{
			if (STATE_ERR == _logic->TableData.m_EnUserState[i]) continue;

			BYTE v = _logic->logicToViewSeatNo(i);
			if (!_players[v].isValid()) continue;

			_handCard[v].lightCard(index);
		}
		HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/showcard.mp3");
	}

	void GameTableUI::sendCardActionFinsh(Ref* pSender)
	{
		_controller.setCardBoardVisible(true);
		int myViewSeat = _logic->logicToViewSeatNo(_logic->getMySeatNo());
		_handCard[myViewSeat]._panel_handCard->setVisible(false);
	}

	bool GameTableUI::isValidSeat(BYTE seatNo)
	{
		return (seatNo < PLAY_COUNT && seatNo >= 0);
	}

	void GameTableUI::showCalculateBoard(const CalculateBoard* boardData)
	{
		_allRoundsEnd1 = true;
		_calculateBoard = CalculateBoardAll::create();
		_calculateBoard->showAndUpdateBoard(Director::getInstance()->getRunningScene(), boardData, HNPlatformConfig()->getVipRoomNum(), HNPlatformConfig()->getMasterID());
		if (_settlement && HNPlatformConfig()->getPlayCount() == 0)
		{
			_calculateBoard->setVisible(false);
		}
		else
		{
			_calculateBoard->setVisible(true);
			if (_settlement)
			{
				_settlement->setVisible(false);
			}
		}
	}

	void GameTableUI::returnButtonClickCallBack()
	{
		_logic->stop();
		RoomLogic()->close();
		GamePlatform::createPlatform();
	}

	void GameTableUI::beginButtonClickCallBack(Ref* ref, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::ENDED != type) return;

		_logic->sendBeginGame();
	}

	void GameTableUI::onChatClick(Ref* pSender)
	{
		if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
			return;
		}
		if (_chatLayer == nullptr)
		{
			//显示聊天界面
			gameChatLayer();
		}
		_chatLayer->showChatLayer();
	}

	//显示文本聊天
	void GameTableUI::onChatTextMsg(BYTE seatNo, std::string msg)
	{
		auto userInfo = UserInfoModule()->findUser(_logic->getMyDeskNo(), seatNo);
		auto viewSeatNo = _logic->logicToViewSeatNo(seatNo);
		auto chatParent = _players[viewSeatNo].getChatParent();
		auto frameSize = chatParent->getContentSize();

		if (userInfo == nullptr) return;
		Vec2 bubblePos = chatParent->getPosition();
		bool isFilpped = false;

		if (viewSeatNo == 0 || viewSeatNo == 1 || viewSeatNo == 4)
		{
			bubblePos.x = bubblePos.x + frameSize.width / 2 + 65;
		}
		else
		{
			isFilpped = true;
			bubblePos.x = bubblePos.x + 20;
		}
		_chatLayer->onHandleTextMessage(chatParent, bubblePos, msg, userInfo->nickName, userInfo->bBoy, isFilpped, viewSeatNo);
	}

	//显示语音聊天
	void GameTableUI::onChatVoiceMsg(int userID, int voiceID, int voiceTime)
	{
		auto userInfo = UserInfoModule()->findUser(userID);
		auto viewSeatNo = _logic->logicToViewSeatNo(userInfo->bDeskStation);
		auto chatParent = _players[viewSeatNo].getChatParent();
		auto frameSize = chatParent->getContentSize();
		if (userInfo == nullptr) return;
		Vec2 bubblePos = chatParent->getPosition();
		bool isFilpped = false;

		if (viewSeatNo == 0 || viewSeatNo == 1 || viewSeatNo == 4)
		{
			bubblePos.x = bubblePos.x + frameSize.width / 2 + 65;
		}
		else
		{
			isFilpped = true;
			bubblePos.x = bubblePos.x + 20;
		}
		_chatLayer->onHandleVocieMessage(chatParent, bubblePos, voiceID, userInfo->nickName, isFilpped, viewSeatNo, voiceTime);
	}
	//聊天界面
	void GameTableUI::gameChatLayer()
	{
		_chatLayer = GameChatLayer::create();
		addChild(_chatLayer, 20006);
		_chatLayer->setPosition(_winSize / 2);
		_chatLayer->onSendTextCallBack = [=](const std::string msg) {
			if (!msg.empty())
			{
				_logic->sendChatMsg(msg);
			}
		};
	}

	void GameTableUI::setDeskInfo()
	{
		//设置房主标识显示
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			auto info = _logic->getUserBySeatNo(i);
			if (info)
			{
				_players[_logic->logicToViewSeatNo(i)].showOwnerImage(HNPlatformConfig()->getMasterID() == info->dwUserID);
			}
		}
	}

	void GameTableUI::showGameRule(bool bShuangWang, bool bSiHua, int point, BYTE playerNum, BYTE playMode, bool tongSha, bool jiaLiangMen)
	{
		std::string str("");
		Text* txtRule = (Text*)_tableUi->getChildByName("Text_gameRule");
		Text* txtPlayerNum = (Text*)_tableUi->getChildByName("Text_gamePlayerNum");
		Text* txtPlayMode = (Text*)_tableUi->getChildByName("Text_gamePlayMode");
		if (playMode == 1)
		{
			txtPlayMode->setString(StringUtils::format("%s", GBKToUtf8("玩法：温州玩法")));
			str += "温州玩法";
		}
		else
		{
			txtPlayMode->setString(StringUtils::format("%s", GBKToUtf8("玩法：福建玩法")));
			str += "福建玩法";
		}
		txtPlayerNum->setString(StringUtils::format("%s%d%s", GBKToUtf8("人数："), playerNum, GBKToUtf8("人")));
		std::string rule = "";
		if (!(bShuangWang || bSiHua))
		{
			rule = "无财神";
			str += " | 无财神";
		}
		else if (bShuangWang && bSiHua)
		{
			rule = "双王,花牌";
			str += " | 双王+花牌";
		}
		else
		{
			rule = bShuangWang ? "双王" : "花牌";
			str += bShuangWang ? " | 双王" : " | 花牌";
		}

		if (tongSha)
		{
			rule += ",全垒打";
			str += " | 全垒打";
		}
		if (jiaLiangMen)
		{
			rule += ",加两门";
			str += " | 加两门";
		}

		//保存有消息玩法
		//HNPlatformConfig()->setPlayMethod(str);

		if (txtRule)
		{
			txtRule->setVisible(false);
			txtRule->setString(StringUtils::format(GBKToUtf8("规则：%s"), GBKToUtf8(rule)));
		}
		std::string shareRule = StringUtils::format(("%d%s | %s | %d%s"), playerNum, GBKToUtf8("人"), GBKToUtf8(str), point, GBKToUtf8("底分"));
		VipRoomController* controller = (VipRoomController*)getChildByName("roomController");
		if (controller)
		{
			controller->setShareRule(shareRule);
		}
	}

	void GameTableUI::showMyCard(const BYTE heapCard[3][5])
	{
		std::vector<BYTE> cards;
		for (size_t j = 0; j < 3; j++)
		{
			for (size_t k = 0; k < 5; k++)
			{
				BYTE val = heapCard[j][k];
				//HNLOG_INFO("i = %d, v = %d, c = %02X", i, v, val);
				if (0 == val) continue;
				if (0xFF == val) continue;
				cards.push_back(val);
			}
		}

		if (cards.empty())
		{
			_handCard[0].setLightCardVisible(false);
		}
		else
		{
			_handCard[0].loadCards(cards);
			_handCard[0].setLightCardVisible(true);
			_handCard[0].lightCard(0);
			_handCard[0].lightCard(1);
			_handCard[0].lightCard(2);
			_handCard[0].hideAllScore();
		}
	}

	void GameTableUI::showUserCut(BYTE lseat)
	{
		auto viewSeat = _logic->logicToViewSeatNo(lseat);
		_players[viewSeat].setStateLabel(GamePlayer::STATE_LABEL::NONE);
	}

	void GameTableUI::removeSetLayer()
	{
		auto setLayout = this->getChildByName("setLayer");
		if (setLayout)
		{
			setLayout->removeFromParent();
		}
	}

	void GameTableUI::onQiangzhuangClick(cocos2d::Ref *pSender)
	{
		_logic->sendUserRobNt(true);

		_btn_qiangzhaung->setVisible(false);
		_btn_buqiang->setVisible(false);

		BYTE lMySeatNo = _logic->getMySeatNo();
		BYTE vMySeatNo = _logic->logicToViewSeatNo(lMySeatNo);
		_players[vMySeatNo].showTimer(0, false);
	}

	void GameTableUI::onBuqiangClick(cocos2d::Ref *pSender)
	{
		_logic->sendUserRobNt(false);

		_btn_qiangzhaung->setVisible(false);
		_btn_buqiang->setVisible(false);

		BYTE lMySeatNo = _logic->getMySeatNo();
		BYTE vMySeatNo = _logic->logicToViewSeatNo(lMySeatNo);
		_players[vMySeatNo].showTimer(0, false);
	}

	void GameTableUI::sendRecordData(S_C_GameRecordResult* data)
	{
		int PlayerCount = 0;
		CHAR* szName[10];
		for (int j = 0; j < 10;j++)
		{
			szName[j] = "1";
		}
		int index = 0;
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			UserInfoStruct* pUser = _logic->getUserBySeatNo(i);
			if (pUser)
			{
				PlayerCount++;
				szName[index] = pUser->nickName;
				index++;
			}
		}

		auto gameRecord = GameRecord::createWithData(szName, data->JuCount, data->GameCount, PlayerCount);
		addChild(gameRecord, 100000000);
	}

	void GameTableUI::sendSSSRecordData(S_C_Game_SSS_RecordResult* data)
	{
		int PlayerCount = 0;
		CHAR* szName[10];
		for (int j = 0; j < 10; j++)
		{
			szName[j] = "1";
		}
		int index = 0;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* pUser = _logic->getUserBySeatNo(i);
			if (pUser)
			{
				PlayerCount++;
				szName[index] = pUser->nickName;
				index++;
			}
		}

		auto gameRecord = GameRecord::createsssWithData(szName, data->JuCount, data->GameCount, PlayerCount);
		addChild(gameRecord, 100000000);
	}

	void GameTableUI::onCheatStatus(S_C_CheatStatus* data)
	{
		_cheatStatus = data->CheatStatus;
		auto setLayer = dynamic_cast<GameSetLayer*>(getChildByName("setLayer"));
		if (setLayer)
		{
			setLayer->updateCheatStatus(data->CheatStatus);
		}
	}

	void GameTableUI::setAllFire(bool isAllFire)
	{
		_isAllFire = isAllFire;
	}

	void GameTableUI::tipsOpenFireEffect(bool sex)
	{
		if (sex)
		{
			HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/boy/daqiang_boy.mp3");
		}
		else
		{
			HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/girl/daqiang_girl.mp3");
		}
	}

	void GameTableUI::tipsAllOpenFireEffect(bool sex)
	{
		if (sex)
		{
			HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/boy/quanleida_boy.mp3");
		}
		else
		{
			HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/girl/quanleida_girl.mp3");
		}
	}

	void	GameTableUI::setBottomScore(int Score)
	{
		if (Score > 0)
		{
			
			auto text = (Label*)_BottonScore->getChildByName("TextDi");
			text->setString(StringUtils::toString(Score));
		}
		else
			_BottonScore->setVisible(false);


	}

}
