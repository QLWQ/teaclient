/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardCardSprite.h"

namespace CaiShenThirteenCard
{
	CardSprite* CardSprite::create(BYTE cardValue)
	{
		// out of range
		if (!(
			(cardValue >= 0x00 && cardValue <= 0x0E) ||//��Բ
			(cardValue >= 0x11 && cardValue <= 0x1E) ||//�ڷ�
			(cardValue >= 0x21 && cardValue <= 0x2E) ||//����
			(cardValue >= 0x31 && cardValue <= 0x3E) ||//÷��
			(cardValue >= 0x41 && cardValue <= 0x4E) ||//����
			(cardValue >= 0x51 && cardValue <= 0x5E) ||//����
			(cardValue >= 0x6E && cardValue <= 0x6F)   //��С��
			))
		{
			return nullptr;
		}

		CardSprite* card = new CardSprite();
		if (card && card->initWithCardValue(cardValue))
		{
			card->autorelease();
			return card;
		}
		CC_SAFE_DELETE(card);
		return nullptr;
	}

	CardSprite::CardSprite()
	{
		
	}

	CardSprite::~CardSprite()
	{

	}

	bool CardSprite::initWithCardValue(BYTE cardValue)
	{
		if (!Sprite::initWithSpriteFrameName(getCardTextureFileName(cardValue)))
		{
			return false;
		}
		this->setIgnoreAnchorPointForPosition(false);
		auto listener = EventListenerTouchOneByOne::create();
		listener->onTouchBegan = CC_CALLBACK_2(CardSprite::onTouchBegan, this);
		listener->onTouchMoved = CC_CALLBACK_2(CardSprite::onTouchMoved, this);
		listener->onTouchEnded = CC_CALLBACK_2(CardSprite::onTouchEnded, this);
		listener->setSwallowTouches(true);
		this->_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

		_cardValue = cardValue;
		
		return true;
	}

	bool CardSprite::onTouchBegan(Touch* touch, Event *event)
	{
		return false;
	}

	void CardSprite::onTouchMoved(Touch* touch, Event *event)
	{

	}

	void CardSprite::onTouchEnded(Touch* touch, Event *event)
	{

	}

	void CardSprite::setValue(BYTE cardValue)
	{
		do
		{
			if (cardValue < 0x00) break;

			if (cardValue > 0x3D)
			{
				if (cardValue != 0x4E && cardValue != 0x4F) break;
			}

			if (_cardValue != cardValue)
			{
				_cardValue = cardValue;
				this->setSpriteFrame(getCardTextureFileName(_cardValue));
			}

		} while (0);
	}

	BYTE CardSprite::getValue() const
	{
		return _cardValue;
	}

	std::string CardSprite::getCardTextureFileName(BYTE cardValue)
	{
		char filename[128];
		sprintf(filename, "Games/CaiShenThirteenCard/GamePlist/0x%02X.png", cardValue);
		return filename;
	}

	void CardSprite::setGray(bool gray)
	{
		if (gray)
		{
			Sprite* sprite_chess = Sprite::createWithTexture(this->getTexture());
			sprite_chess->setPosition(sprite_chess->getContentSize().width / 2, sprite_chess->getContentSize().height / 2);

			RenderTexture *render = RenderTexture::create(sprite_chess->getContentSize().width, sprite_chess->getContentSize().height, Texture2D::PixelFormat::RGBA8888);
			render->beginWithClear(0.0f, 0.0f, 0.0f, 0.0f);
			sprite_chess->visit();
			render->end();
			Director::getInstance()->getRenderer()->render();

			Image *finalImage = render->newImage();

			unsigned char *pData = finalImage->getData();

			int iIndex = 0;

			for (int i = 0; i < finalImage->getHeight(); i++)
			{
				for (int j = 0; j < finalImage->getWidth(); j++)
				{
					// gray
					int iBPos = iIndex;

					unsigned int iB = pData[iIndex];
					iIndex++;

					unsigned int iG = pData[iIndex];
					iIndex++;

					unsigned int iR = pData[iIndex];
					iIndex++;
					iIndex++;

					unsigned int iGray = 0.3 * iR + 0.6 * iG + 0.1 * iB;

					pData[iBPos] = pData[iBPos + 1] = pData[iBPos + 2] = (unsigned char)iGray;
				}
			}

			Texture2D *texture = new Texture2D;
			texture->initWithImage(finalImage);
			this->setTexture(texture);

			delete finalImage;
			texture->release();
		}
		else
		{
			this->setSpriteFrame(getCardTextureFileName(_cardValue));
		}
	}
}

