/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardGamePlayer.h"

//////////////////////////////////////////////////////////////////////////

#define READY_LABEL			"CaiShenThirteenCard/GameState/game_ready.png"
#define READY_LABEL_RIGHT	"CaiShenThirteenCard/GameState/game_ready_right.png"
#define CALL_LABEL			"CaiShenThirteenCard/GameState/game_call.png"
#define CALL_LABEL_RIGHT	"CaiShenThirteenCard/GameState/game_call_right.png"
#define UNCALL_LABEL		"CaiShenThirteenCard/GameState/game_uncall.png"
#define UNCALL_LABEL_RIGHT	"CaiShenThirteenCard/GameState/game_uncall_right.png"
#define PEIPAI_LABEL		"CaiShenThirteenCard/GameState/game_peipai.png"
#define PEIPAI_LABEL_RIGHT	"CaiShenThirteenCard/GameState/game_peipai_right.png"
#define ZPDONE_LABEL		"CaiShenThirteenCard/GameState/game_zpdone.png"
#define ZPDONE_LABEL_RIGHT	"CaiShenThirteenCard/GameState/game_zpdone_right.png"

const static char* Player_Empty = "CaiShenThirteenCard/GameTableUi/res/room_default_none.png";
const static char* Player_Normal_W = "CaiShenThirteenCard/GameTableUi/res/women_head.png";
const static char* Player_Normal_M = "CaiShenThirteenCard/GameTableUi/res/men_head.png";
const static int giftImgNum[6] = { 19, 28, 21, 29, 19, 28 };


#define PLAYER_STATE_TAG	100

namespace CaiShenThirteenCard
{
	GamePlayer::GamePlayer()
		: _textName(nullptr)
		, _taGold(nullptr)
		, _stateLabel(STATE_LABEL::NONE)
		, _userID(INVALID_USER_ID)
		, _gold(0)
		, _self(false)
		, _panel_Player(nullptr)
		, _ivNT(nullptr)
		, _iMaster(nullptr)
		, _clock(nullptr)
		, _logicSeatNo(INVALID_DESKSTATION)
		, _viewSeatNo(INVALID_DESKSTATION)
		, _userHead(nullptr)
	{
	}

	GamePlayer::~GamePlayer()
	{
		_panel_Player = nullptr;
		_clock = nullptr;
		_stateImage = nullptr;
		_ivNT = nullptr;
		_iMaster = nullptr;
		_nameBox = nullptr;
		_textName = nullptr;
		_taGold = nullptr;
	}

	void GamePlayer::doUnload()
	{
		_panel_Player = nullptr;
		_clock = nullptr;
		_stateImage = nullptr;
		_ivNT = nullptr;
		_iMaster = nullptr;
		_nameBox = nullptr;
		_textName = nullptr;
		_taGold = nullptr;
	}

	void GamePlayer::doLoad(BYTE seatNo, Layout* Panel_Table)
	{
		_viewSeatNo = seatNo;
		char str[24];

		//玩家头像
		sprintf(str, "Panel_Player%d", seatNo);
		_panel_Player = dynamic_cast<Layout*>(Panel_Table->getChildByName(str));

		//倒计时时钟
		sprintf(str, "Image_Clock%d", seatNo);
		auto panel_Timer = dynamic_cast<Layout*>(Panel_Table->getChildByName("Panel_Timer"));
		_clock			= dynamic_cast<ImageView*>(panel_Timer->getChildByName(str));

		//玩家状态气泡
		sprintf(str, "Image_State%d", seatNo);
		auto panel_State = dynamic_cast<Layout*>(Panel_Table->getChildByName("Panel_State"));
		_stateImage		= dynamic_cast<ImageView*>(panel_State->getChildByName(str));

		Size winsize = Director::getInstance()->getWinSize();

		_panel_Player->setScaleY(720 / winsize.height);
		//长屏适配
		if (winsize.width / winsize.height >= 1.8)
		{
			_panel_Player->setScale(winsize.height / 720,1);
		}

		_ivNT = dynamic_cast<ImageView*>(_panel_Player->getChildByName("Image_NT"));
		_ivNT->setVisible(false);
		_iMaster = dynamic_cast<ImageView*>(_panel_Player->getChildByName("Image_owner"));
		_iMaster->setLocalZOrder(10);
		_textName = dynamic_cast<Text*>(_panel_Player->getChildByName("Text_NickName"));
		_textName->setFontSize(22);
		_textName->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
		//_textName->setPosition(Vec2(_textName->getPosition().x, _textName->getPosition().y - 5));
		
		_taGold = dynamic_cast<Text*>(_panel_Player->getChildByName("Text_Money"));

		auto img_frame = dynamic_cast<ImageView*>(_panel_Player->getChildByName("Image_frame"));
		img_frame->setScale(0.9);
		img_frame->setVisible(false);
		auto img_head = dynamic_cast<ImageView*>(_panel_Player->getChildByName("Image_head"));
		img_head->setScale(0.85);
		auto img_box = ImageView::create("CaiShenThirteenCard/GameTableUi/res/men_head.png");
		_userHead = GameUserHead::create(img_head, img_box);
		_userHead->show(Player_Empty);
		_userHead->addClickEventListener([=](Ref* pSender){
			if (userMessageCallBack)
			{
				userMessageCallBack();
			}
		});
		
		//显示玩家信息的按钮
		_userMessageBtn = dynamic_cast<Button*>(_panel_Player->getChildByName("Button_userMessage"));
		//庄家动画按钮 
		_BtnKuang = dynamic_cast<Button*>(_panel_Player->getChildByName("Button_kuang"));
		_BtnKuang->setVisible(false);
	}

	void GamePlayer::doRestore()
	{
		setUserID(INVALID_USER_ID);
		setLogicSeatNo(INVALID_DESKSTATION);
		setViewSeatNo(INVALID_DESKSTATION);
		setName("");
		setGold(0);
		setUserIsNT(false);
		showOrHideUser(false);
		showTimer(0, false);
		setStateLabel(STATE_LABEL::NONE);
	}

	Vec2 GamePlayer::getPlayerPos()
	{
		return _panel_Player->getPosition();
	}

	void GamePlayer::setName(const std::string& name)
	{
		if (name.compare(_name) == 0)
		{
			return;
		}
		_name = name;
		if (_name.empty())
		{
			_textName->setString(GBKToUtf8("未知"));
		}
		else
		{
			_textName->setString(GBKToUtf8(_name.c_str()));
		}
	}

	void GamePlayer::setGold(LLONG gold)
	{
		if (_gold != gold)
		{
			_gold = gold;

			if (_taGold != nullptr) {
				_taGold->setString(to_string(_gold));
			}			

			//if (_gold > 0)
			//{
			//	_taGold->setString(to_string(_gold));
			//}
			//else
			//{
			//	_taGold->setString("0");
			//}
		}
	}

	void GamePlayer::setHead(bool isBoy, int iUserID, int iUserHeadID)
	{
		if (iUserHeadID == 0) iUserHeadID = 1;
		std::string name = StringUtils::format("platform/common/Head/Head%d.jpg", iUserHeadID);//isBoy ? Player_Normal_M : Player_Normal_W;
		bool isExist = FileUtils::getInstance()->isFileExist(name);
		if (isExist)
			_userHead->loadTexture(name);
		else
			_userHead->loadTexture(isBoy ? Player_Normal_M : Player_Normal_W);
		auto user = UserInfoModule()->findUser(iUserID);
		if (user)
		{
			_userHead->loadTextureWithUrl(user->headUrl);
		}	
		_userHead->setVIPHead("", user->iVipLevel);
		if (user->iVipLevel > 0)  _textName->setColor(Color3B(255, 0, 0));
	}

	void GamePlayer::showOrHideUser(bool visible)
	{
		_panel_Player->setVisible(visible);
	}

	void GamePlayer::setStateLabel(STATE_LABEL state)
	{
		switch (state)
		{
		case CaiShenThirteenCard::GamePlayer::STATE_LABEL::NONE:
			{
				_stateImage->setVisible(false);
			}break;
		case CaiShenThirteenCard::GamePlayer::STATE_LABEL::READY:
			{
				if (_viewSeatNo == 0 || _viewSeatNo == 1||_viewSeatNo == 4) _stateImage->loadTexture(READY_LABEL);
				else _stateImage->loadTexture(READY_LABEL_RIGHT);
				_stateImage->setVisible(true);
			}break;
		case CaiShenThirteenCard::GamePlayer::STATE_LABEL::CALL:
			{
				if (_viewSeatNo == 0 || _viewSeatNo == 1 || _viewSeatNo == 4) _stateImage->loadTexture(CALL_LABEL);
				else _stateImage->loadTexture(CALL_LABEL_RIGHT);
				_stateImage->setVisible(true);
			}break;
		case CaiShenThirteenCard::GamePlayer::STATE_LABEL::UNCALL:
			{
				if (_viewSeatNo == 0 || _viewSeatNo == 1 || _viewSeatNo == 4) _stateImage->loadTexture(UNCALL_LABEL);
				else _stateImage->loadTexture(UNCALL_LABEL_RIGHT);
				_stateImage->setVisible(true);
			}break;
		case CaiShenThirteenCard::GamePlayer::STATE_LABEL::SORT:
			{
				if (_viewSeatNo == 0 || _viewSeatNo == 1 || _viewSeatNo == 4) _stateImage->loadTexture(PEIPAI_LABEL);
				else _stateImage->loadTexture(PEIPAI_LABEL_RIGHT);
				_stateImage->setVisible(true);
			}break;
		case CaiShenThirteenCard::GamePlayer::STATE_LABEL::ZPDONE:
			{
				if (_viewSeatNo == 0 || _viewSeatNo == 1 || _viewSeatNo == 4) _stateImage->loadTexture(ZPDONE_LABEL);
				else _stateImage->loadTexture(ZPDONE_LABEL_RIGHT);
				_stateImage->setVisible(true);
			}break;
			default:
				break;
		}
	}

	void GamePlayer::setUserIsNT(bool isNT)
	{
		_ivNT->setVisible(isNT);//isNT
	}

	void GamePlayer::playGiftAni(Vec2 startPos, Vec2 endPos, int giftNo)
	{
		if (giftNo < 0 || giftNo >= 6)
		{
			return;
		}

		auto giftSp = Sprite::create();
		_panel_Player->getParent()->addChild(giftSp, 10);
		giftSp->setTexture(StringUtils::format("platform/userMessage/res/item_img_%d.png", giftNo));
		giftSp->setPosition(startPos);
		giftSp->runAction(Sequence::create(MoveTo::create(0.5f, endPos), CallFunc::create([=](){
			//播放动画
			giftSp->setVisible(false);
			std::string strFile = StringUtils::format("giftAni_%d", giftNo);
			std::string strImg = StringUtils::format("item_%d", giftNo);
			createAni(strFile, strImg, giftImgNum[giftNo], giftSp);
		}), DelayTime::create((giftNo == 0) ? 0 : 0.5f), CallFunc::create([=](){
			std::string str = StringUtils::format("platform/userMessage/sound/item%d.mp3", giftNo);
			HNAudioEngine::getInstance()->playEffect(str.c_str());
		}), DelayTime::create(3.0f), RemoveSelf::create(), nullptr));
	}

	void GamePlayer::createAni(std::string fileName, std::string imgName, int imgCount, Node* playNode, bool isNt)
	{
		std::string fileStr = StringUtils::format("platform/userMessage/animations/%s.plist", fileName.c_str());
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile(fileStr);
		Vector<SpriteFrame *> frameVec;
		std::string str;
		for (int i = 0; i < imgCount; i++)
		{
			str = StringUtils::format("%s_%d.png", imgName.c_str(), i);
			SpriteFrame * frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			frameVec.pushBack(frame);
		}

		Animation * ani = Animation::createWithSpriteFrames(frameVec, 0.1f, 1);
		Animate * ac = Animate::create(ani);

		auto actionSp = Sprite::create();
		SpriteFrame* frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(StringUtils::format("%s_0.png", imgName.c_str()));
		actionSp->setSpriteFrame(frame);
		actionSp->setPosition(playNode->getPosition());
		playNode->getParent()->addChild(actionSp, 4);
		actionSp->runAction(Sequence::create(ac, CallFunc::create([=](){
			if (playNode)
			{
				playNode->setVisible(isNt);
			}
		}), DelayTime::create(0.5f), RemoveSelf::create(), nullptr));
	}

	//创建人物帧动画
	Animation* GamePlayer::addSpriteData(HeadType type)
	{
		char str[32];
		Animation *bUser = Animation::create();
		bUser->setDelayPerUnit(0.15f);
		bUser->setLoops(-1);
		for (int i = 1; i <= 12; i++)
		{
			sprintf(str, "playercartoon_%d_%d.png", type, i);
			bUser->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(str));
		}
		sprintf(str, "Player_%d", type);
		AnimationCache::getInstance()->addAnimation(bUser, str);
		return bUser;
	}

	void GamePlayer::showTimer(int dt, bool bShow, std::function<void()> callFunc)
	{
		auto timer = dynamic_cast<TextAtlas*>(_clock->getChildByName("AtlasLabel_Timer"));
		_clock->setVisible(bShow);
		timer->setVisible(bShow);

		if (bShow)
		{
			timing(timer, dt, callFunc);
		}
	}

	//倒计时处理,function回调为传进来的倒计时结束要做的处理
	void GamePlayer::timing(Node* node, int dt, std::function<void()> callFunc)
	{
		if (nullptr == node || !node->isVisible()) return;

		char str[12];
		sprintf(str, "%d", dt);
		dt--;
		if (dt < 0)
		{
			if (callFunc) callFunc();
			return;
		}

		TextAtlas* text = (TextAtlas*)node;
		text->setString(str);
		node->runAction(Sequence::create(DelayTime::create(1.0f),
			CallFuncN::create(CC_CALLBACK_1(GamePlayer::timing, this, dt, callFunc)), nullptr));
	}

	Node* GamePlayer::getChatParent()
	{
		return _userHead;
	}

	ImageView* GamePlayer::getUserHeadImg()
	{
		return _userHead;
	}

	//显示房主标识
	void GamePlayer::showOwnerImage(bool show)
	{
		if (_iMaster)
		{
			_iMaster->setVisible(show);
		}
	}

	void GamePlayer::showKuangBtnAction(bool isPlay,bool isZhuang)
	{
		if (isPlay == true)
		{
			Sprite* sp = Sprite::create("CaiShenThirteenCard/GameTableUi/res/box_light1.png");
			sp->setAnchorPoint(Vec2(0.5, 0.5));
			sp->setPosition(_BtnKuang->getPosition());
			sp->setZOrder(20000);
			_panel_Player->addChild(sp);
			
			Sequence* seqAct1 = Sequence::create(FadeIn::create(0.1f), FadeOut::create(0.1f),nullptr);

			Sequence* seqAct = Sequence::create(Repeat::create(seqAct1, 5),
				RemoveSelf::create(true), nullptr);
			//sp->runAction(seqAct);
			if (isZhuang)
			{
				sp->runAction(Sequence::create(DelayTime::create(0.1f), seqAct, CallFunc::create([=](){
					setUserIsNT(true);
				}),nullptr));
			}
			else
			{
				sp->runAction(Sequence::create(DelayTime::create(0.1f), seqAct, nullptr));
			}
			
		}
	}

}
