/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardHandCard.h"
#include "CaiShenThirteenCardGameLogic.h"

#define WIN_SIZE		Director::getInstance()->getWinSize()

namespace CaiShenThirteenCard
{
#define HandCardCount 13
	NewThirteenCardHandCard::NewThirteenCardHandCard()
		: _panel_handCard(nullptr)
		, _LightCardPanel(nullptr)
	{
		_playerCount = 0;
		_iLastCard = 0;
	}

	NewThirteenCardHandCard::~NewThirteenCardHandCard()
	{
		_panel_handCard = nullptr;
		_LightCardPanel = nullptr;
		_cardList.clear();
		_cardScore.clear();
		_cardType.clear();
		_first.clear();
		_second.clear();
		_third.clear();
	}

	void NewThirteenCardHandCard::doRestore()
	{
		_invalid = false;
		for (auto card : _cardList) card->setVisible(false);
		for (auto image : _cardType) image->setVisible(false);

		for (auto text : _cardScore)
		{
			text->setVisible(false);
			text->setColor(Color3B(255, 255, 255));
		}

		_LightCardPanel->setVisible(false);

		for (auto card : _first)
		{
			card->setTag(0);
			loadCardsTexture(card, 0);
		}

		for (auto card : _second)
		{
			card->setTag(0);
			loadCardsTexture(card, 0);
		}

		for (auto card : _third)
		{
			card->setTag(0);
			loadCardsTexture(card, 0);
		}	
	}

	void NewThirteenCardHandCard::doLoad(BYTE seatNo, Layout* Panel_Table)
	{
		_viewSeatNo = seatNo;

		char str[128];
		sprintf(str, "Panel_HandCard%d", seatNo);
		auto winsize = Director::getInstance()->getWinSize();
		_panel_handCard = dynamic_cast<Layout*>(Panel_Table->getChildByName(str));
		_panel_handCard->setScaleY(720 / winsize.height);
		//长屏适配
		if (winsize.width / winsize.height >= 1.8)
		{
			_panel_handCard->setScale(winsize.height / 720, 1);
		}
		//if (0 != seatNo)//别人的手牌（只用来做发牌效果） //0 != seatNo
		//{
			for (int i = 0; i < 13; i++)
			{
				sprintf(str, "Image_Card%d", i);
				auto card = dynamic_cast<ImageView*>(_panel_handCard->getChildByName(str));
				card->setVisible(false);//false
				_cardList.push_back(card);
			}
		//}

		//用来比牌的手牌
		sprintf(str, "Panel_User%d", seatNo);
		auto panel = dynamic_cast<Layout*>(Panel_Table->getChildByName("Panel_LightCard"));
		panel->setVisible(true);
		

		_LightCardPanel = dynamic_cast<Layout*>(panel->getChildByName(str));
		_LightCardPanel->setScaleY(720 / winsize.height);
		//长屏适配
		if (winsize.width / winsize.height >= 1.8)
		{
			_LightCardPanel->setScale(winsize.height / 720, 1);
		}


		for (size_t i = 0; i < 13; i++)
		{
			sprintf(str, "Image_Card%d", i);
			auto imageCard = dynamic_cast<ImageView*>(_LightCardPanel->getChildByName(str));
			loadCardsTexture(imageCard, 0x0);
			if (i < 3) _first.push_back(imageCard);
			else if (i >= 3 && i < 8) _second.push_back(imageCard);
			else _third.push_back(imageCard);
		}

		//牌型和分数展示
		for (size_t i = 0; i < 4; i++)
		{
			sprintf(str, "BitmapFontLabel_Score%d", i);
			auto cardScore = dynamic_cast<TextBMFont*>(_LightCardPanel->getChildByName(str));
			_cardScore.push_back(cardScore);
		}
		for (size_t i = 0; i < 3; i++)
		{
			sprintf(str, "Image_CardType%d", i);
			auto cardType = dynamic_cast<ImageView*>(_LightCardPanel->getChildByName(str));
			cardType->ignoreContentAdaptWithSize(true);
			_cardType.push_back(cardType);
		}
	}

	void NewThirteenCardHandCard::sendCard(float interval)
	{
		if (_self) return;

		if (interval > 0.1f)
		{
			sendCardOneByOne(_panel_handCard, interval, 0);
		}
		else for (auto card : _cardList) card->setVisible(true);
	}

	void NewThirteenCardHandCard::sendCardOneByOne(Node* node, float interval, int idx)
	{
		if (idx >= 13) return;

		_cardList[idx]->setVisible(true);
		idx++;
		node->runAction(Sequence::create(DelayTime::create(interval),
			CallFuncN::create(CC_CALLBACK_1(NewThirteenCardHandCard::sendCardOneByOne, this, interval, idx)), nullptr));
	}

	void NewThirteenCardHandCard::loadCards(const std::vector<BYTE>& cards)
	{		
		for (size_t i = 0; i < 3; i++) _first[i]->setTag(cards[i]);
		for (size_t i = 3, j = 0; i < 8; i++, j++) _second[j]->setTag(cards[i]);
		for (size_t i = 8, j = 0; i < cards.size(); i++, j++) _third[j]->setTag(cards[i]);

		_invalid = true;
	}

	void NewThirteenCardHandCard::loadCardsTexture(ImageView* card, BYTE cardValue)
	{
		if (!isValid(cardValue)) return;
		char filename[128];
		sprintf(filename, "Games/CaiShenThirteenCard/GamePlist/0x%02X.png", cardValue);		
		card->loadTexture(filename, Widget::TextureResType::PLIST);
	}

	bool NewThirteenCardHandCard::isValid(BYTE value) const
	{
		return value > 0x00 || value < 0x3D;
	}

	std::pair<std::string, std::string> NewThirteenCardHandCard::getCardTypeResource(bool sex, BYTE cardType)
	{
		std::pair<std::string, std::string> cardTypeResource;
		switch (cardType)
		{
		case SHAPE_HIGH_CARD:		//高牌
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type1.png", sex ? "CaiShenThirteenCard/sound/boy/10.mp3" : "CaiShenThirteenCard/sound/girl/10.mp3");
			break;
		case SHAPE_DOUBLE_YI:		//一对
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type2.png", sex ? "CaiShenThirteenCard/sound/boy/1.mp3" : "CaiShenThirteenCard/sound/girl/1.mp3");
			break;
		case SHAPE_DOUBLE_LIANG:	//两对
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type3.png", sex ? "CaiShenThirteenCard/sound/boy/2.mp3" : "CaiShenThirteenCard/sound/girl/2.mp3");
			break;
		case SHAPE_STRIP_SAN:		//三条
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type4.png", sex ? "CaiShenThirteenCard/sound/boy/3.mp3" : "CaiShenThirteenCard/sound/girl/3.mp3");
			break;
		case SHAPE_STRAIGHT:		//顺子
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type5.png", sex ? "CaiShenThirteenCard/sound/boy/4.mp3" : "CaiShenThirteenCard/sound/girl/4.mp3");
			break;
		case SHAPE_FLOWER:			//同花
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type6.png", sex ? "CaiShenThirteenCard/sound/boy/5.mp3" : "CaiShenThirteenCard/sound/girl/5.mp3");
			break;
		case SHAPE_THREE_DOUBLE:	//葫芦
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type7.png", sex ? "CaiShenThirteenCard/sound/boy/6.mp3" : "CaiShenThirteenCard/sound/girl/6.mp3");
			break;
		case SHAPE_STRIP_SI:		//四条
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type8.png", sex ? "CaiShenThirteenCard/sound/boy/7.mp3" : "CaiShenThirteenCard/sound/girl/7.mp3");
			break;
		case SHAPE_STRAIGHT_FLUSH:	//同花顺
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type9.png", sex ? "CaiShenThirteenCard/sound/boy/8.mp3" : "CaiShenThirteenCard/sound/girl/8.mp3");
			break;
		case SHAPE_STRIP_FIVE:	//五同
			return std::make_pair("Games/CaiShenThirteenCard/GamePlist/game_type10.png", sex ? "CaiShenThirteenCard/sound/boy/9.mp3" : "CaiShenThirteenCard/sound/girl/9.mp3");
			break;
		default:
			break;
		}
		return cardTypeResource;
	}

	void NewThirteenCardHandCard::showScore(int index, bool sex, BYTE cardType, LLONG score)
	{
		if (index > 2) return;
		if (!_invalid) return;

		std::pair<std::string, std::string> cardTypeResource = getCardTypeResource(sex, cardType);
		setCardType(cardTypeResource.first, index);
		setScore(score, index);
		if (_self)
		{
			if (!cardTypeResource.second.empty())
			{
				HNAudioEngine::getInstance()->playEffect(cardTypeResource.second.c_str());
			}
		}
	}

	void NewThirteenCardHandCard::sethandCardVisible(bool bShow)
	{
		if (_self) return;
		for (auto card : _cardList) card->setVisible(bShow);
	}

	void NewThirteenCardHandCard::setLightCardVisible(bool bShow)
	{
		_LightCardPanel->setVisible(bShow);
		if (bShow)
		{
			_LightCardPanel->setScale(0.2f);
			_LightCardPanel->runAction(Sequence::create(ScaleTo::create(0.1f, 1.1f), ScaleTo::create(0.1f, 1.0f), nullptr));
		}
	}

	void NewThirteenCardHandCard::lightCard(int index)
	{
		if (!_invalid) return;
		switch (index)
		{
		case 0: for (auto& card : _first) loadCardsTexture(card, card->getTag()); break;
		case 1: for (auto& card : _second) loadCardsTexture(card, card->getTag()); break;
		case 2: for (auto& card : _third) loadCardsTexture(card, card->getTag()); break;
		default:
			break;
		}
	}

	void NewThirteenCardHandCard::lightFirst()
	{
		if (!_invalid) return;

		for (auto& card : _first) loadCardsTexture(card, card->getTag());
	}

	void NewThirteenCardHandCard::lightSecond()
	{
		if (!_invalid) return;

		for (auto& card : _second) loadCardsTexture(card, card->getTag());
	}

	void NewThirteenCardHandCard::lightThird()
	{
		if (!_invalid) return;

		for (auto& card : _third) loadCardsTexture(card, card->getTag());
	}

	void NewThirteenCardHandCard::setCardType(const std::string& cardType, int idx)
	{
		_cardType[idx]->loadTexture(cardType, Widget::TextureResType::PLIST);
		_cardType[idx]->setScale(0.5f);		
		bool isVisible = (idx == 2) ? true : true;
		_cardType[idx]->setVisible(isVisible);
		_cardType[idx]->runAction(Sequence::create(ScaleTo::create(0.2f, 1.3f), 
			DelayTime::create(0.1f), ScaleTo::create(0.1f, 1.0f), nullptr));
	}

	void NewThirteenCardHandCard::setScore(LLONG score, int idx)
	{
		char str[32];

		if (score >= 0)
		{
			sprintf(str, "+%lld", score);
			_cardScore[idx]->setColor(Color3B::YELLOW);
		}
		else
		{
			sprintf(str, "%lld", score);
			_cardScore[idx]->setColor(Color3B::GREEN);
		}

		_cardScore[idx]->setString(str);
		//通杀不要了 要的话把判断去了
		if (idx != 3)
		{
			_cardScore[idx]->setVisible(true);
		}
	}

	void NewThirteenCardHandCard::isLastCard(int playerSeat, int cardNo)
	{
		log("zhifu isLastCard: playerSeat:%d, cardNo:%d", playerSeat, cardNo);

		if (cardNo == 0)//根据发来的第一张牌次数判断当前有几人_
		{
			_playerCount++;
		}

		if (cardNo == (HandCardCount - 1))
		{
			_iLastCard++;
			if (_playerCount == _iLastCard)//最后一个人拿到最后一张牌_
			{
				_playerCount = 0;
				_iLastCard = 0;

				NotificationCenter::getInstance()->postNotification("SendCardEventFinish");
			}
		}
	}

	void NewThirteenCardHandCard::hideAllScore()
	{
		for (int i = 0; i < _cardScore.size();i++)
		{
			if (_cardScore.at(i))
			{
				_cardScore.at(i)->setVisible(false);
			}
		}
	}
}
