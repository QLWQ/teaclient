/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_ControllerLayer_H__
#define __CaiShenThirteenCard_ControllerLayer_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "HNUIExport.h"
#include "HNNetExport.h"
#include <string>
#include <memory>
#include <array>

using namespace ui;

namespace CaiShenThirteenCard
{
	class CardBoard;
	class CardFrameWrapper;
	class ControllerLayer;

	typedef std::function<void(ControllerLayer* controller)> ccControllerLayerClick;

	class ControllerLayer : public HNLayer
	{
		static const unsigned int CARD_FRAME_COUNT = 13;
		static const unsigned int CARD_TYPE = 9;//牌型选择_
		static const unsigned int COMFORM_TY = 3;//牌的组合形式_
		static const unsigned int COMFORM_CARD_TYPE = 3;//牌组合形式下的牌型_
	public:
		//加载ui
		void doLoad(Layout* Panel_Table);
		void doRestore();

	public:
		ControllerLayer();
		virtual ~ControllerLayer();

	public:
		void loadCards(const std::vector<BYTE>& cards, float interval = 0.0f);
	
	public:
		void getCards(int index, BYTE cards[5]);
		std::vector<BYTE> getCards();

		void cardBoardTouch(bool enableTouch);

	public:
		void setOutCardClickEvent(const ccControllerLayerClick& callback);
		//检测牌型按钮可点击状态
		void checkCardTypeBtnStatue();

		void showCombineFormState(bool isVisible);
		void setCardBoardVisible(bool isVisible);
		//设置一键搓牌对应的牌数据
		void setAutoRubCardType(BYTE byHeapCard[3][5], int iHeapShape[3]);
		//获取牌型名称
		std::string getCardShapeName(int iHeapShape);
	protected:
		//出牌操作
		void onOutCardClick(cocos2d::Ref *pSender);
		//回收操作
		void onRecoveryCardClick(cocos2d::Ref *pSender);
	
	protected:
		void onCardFrameClick(CardFrameWrapper* cardFrame);
		
		void onControlClick(cocos2d::Ref *pSender);
		void onHelpClick(cocos2d::Ref *pSender);
		void onStylorSort(cocos2d::Ref *pSender);
		void onCardTypeTipClick(cocos2d::Ref *pSender);//牌型点击回调
		void onCombiningFormClick(cocos2d::Ref *pSender);//牌组合点击回调
		//判断中墩尾墩排列是否合法
		bool checkCardsTypeValid();
		//检测摆墩按钮可点击状态
		void checkBtnStatue();
		void clearNowIndex(INT type);
	
	protected:
		void setCardsValueAndloadTexture(ImageView* card, BYTE cardValue);
		std::vector<BYTE> restoreCardFrame(int start, int end);
		int getCardFrameValidCount(int start, int end);
		bool scanningCardFrame(int start, int end, const std::function<void(ImageView* card)>& callback);
		void restoreDunBtn(bool bEnabled);
		void findCardType(int type);
		std::string findFirstCardType(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);//第一种牌型组合_ 
		std::string findSecondCardType(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);//第二种牌型组合_ 
		std::string findThirdCardType(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);//第三种牌型组合_ 
		void compareCardType(); //牌型组合比较_
		
	private:
		Button* _btnOutputCard;
		Button* _btnRecoveryCard;
		Button* _btnHead;
		Button* _btnMid;
		Button* _btnLast;
		Button*	_btnAutoRubcard;//一键搓牌_
		Button*	_btnStylor;//花色排序_
		Button* _btnmanualRubcard;//手动搓牌_
		Node*	_node_cardType; //牌型提示
		Node*	_nodecombiningForm;//组合提示
		Node*	_nodeCombinePos[3];//用于定位
		Text*	_tetComFormTy[COMFORM_TY][COMFORM_CARD_TYPE];
		Button*	_comFormTy[COMFORM_TY];//组合形式_
		Button*	_btnCardType[CARD_TYPE];//牌型：0对子,1三条,2顺子,3同花,4葫芦,5炸弹,6同花顺
		BYTE _comFormCardValue[COMFORM_TY][14]; //3种组合 14,最后一位留一个结束符_
	private:
		Layout*		_controUi;
		ImageView*	_paidunBG;
		CardBoard*	_cardBoard;
		ImageView*	_arrCardFrame[CARD_FRAME_COUNT];
		ccControllerLayerClick	_outCardCallback;

		enum  eCombiningForm
		{
			eFirstType = 0,
			eSecondType,
			eThirdType
		};
	public:
		enum CardType
		{
			DOUBLE = 0, //对子
			TWO_DOUBLE =1,//两对
			THREE = 2, //三条
			SHUNZI = 3, //顺子
			SAMEHUA = 4, //同花
			HULU = 5, //葫芦
			FOUR = 6, //炸弹
			SAMEHUA_SHUNZI = 7, //同花顺
			WUTONG = 8, //五同

			ALL_TYPE,
		};
		std::vector<BYTE> _hintCardList[ALL_TYPE];
		INT  _hintCardNum[ALL_TYPE];
		INT  _nowIndex[ALL_TYPE];
		bool _isClickF = false;
	};
}

#endif // !__CaiShenThirteenCard_ControllerLayer_H__
