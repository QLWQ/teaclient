/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_SettlementLayer_h__
#define __CaiShenThirteenCard_SettlementLayer_h__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "HNNetExport.h"
#include "HNLobbyExport.h"
namespace CaiShenThirteenCard
{
	typedef std::function<void()> ccSettlementTimer;

	typedef struct _SettlementParameter
	{
		std::string nickName;
		LLONG score;
		LLONG tax;
		LLONG fireScore;
		LLONG allHitScore;
		bool self;
		bool isBoy;
		int iUserID;
		std::string sHeadUrl;
		int ComepareResult[3];         //每道得分
		BYTE byHeapCard[3][5];          //摆牌数据
		int  iExtraScore;   //额外得分
		int  palyMode;      //玩法 1温州 2 福建
		bool isZhuang;
	} SettlementParameter;

	class SettlementLayer : public HN::HNLayer
	{
		ccSettlementTimer _callback;
		int _countdownTime;

	private:
		ImageView*		_settlementBG;
		Button*			_btnWinOrLose;
		ui::TextAtlas*	_clock;
		ui::Text*		_nickNames[6];
		ui::Text*		_results[6];
		ui::TextBMFont* _textAtlasScores[6];
		Layout*			_head_bg[6];
		GameUserHead*	_userHead[6];
		ImageView*      _ImageCard[6][13];
		ui::TextBMFont* _textAtlasComepareResult[6][4];
		ImageView*      _ImageDaoShu[6][4];
		ImageView*      _ownerImage[6];
		ImageView*      _ownerZhuang[6];
	public:
		CREATE_FUNC(SettlementLayer);

	public:
		virtual bool init() override;
		void closeSettlement();

	public:
		void loadParameter(const std::vector<SettlementParameter>& parameters, bool isNowCompare = false);
		void startTimer(int time);
		void setCloseCallback(const ccSettlementTimer& callback);

	protected:
		void scheduleCountdownTimer(float dt);

	protected:
		SettlementLayer();
		virtual ~SettlementLayer();
	};
}

#endif // __CaiShenThirteenCard_SettlementLayer_h__
