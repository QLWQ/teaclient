/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardSoundLayer.h"
#include "CaiShenThirteenCardWrapper.h"
#include "HNLobbyExport.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace ui;

namespace CaiShenThirteenCard
{
	//////////////////////////////////////////////////////////////////////////

	SoundLayer::SoundLayer()
		: _sliderMusic(nullptr)
		, _sliderEffects(nullptr)
		, _musicCallback(nullptr)
		, _effectsCallback(nullptr)
	{

	}

	SoundLayer::~SoundLayer()
	{
	}

	void SoundLayer::closeSet()
	{
		wrireConfig();

		_settingBK->runAction(Sequence::create(ScaleTo::create(0.1f, 0.3f), CallFunc::create([&]()
		{
			this->removeFromParent();
		}), nullptr));
	}

	bool SoundLayer::init()
	{
		if (!HNLayer::init()) return false;

		auto settingNode = CSLoader::createNode("CaiShenThirteenCard/GameSettingUi/Node_SettingUi.csb");
		settingNode->setPosition(_winSize / 2);
		this->addChild(settingNode, 100);

		auto settingPanel = dynamic_cast<Layout*>(settingNode->getChildByName("Panel_BG"));
		settingPanel->addClickEventListener([=](Ref*){
			closeSet();
		});

		_settingBK = dynamic_cast<ImageView*>(settingPanel->getChildByName("Image_SettingBG"));

		{
			HelperWrapper search(_settingBK);

			_nodeContentSize = _settingBK->getContentSize();

			auto btnClose = search.find<Button>("Button_Close");
			btnClose->addClickEventListener([=](Ref* ref){
				closeSet();
			});

			//����/��Ч ������
			_sliderMusic = search.find<Slider>("Slider_Music");
			_sliderEffects = search.find<Slider>("Slider_Effects");
			_sliderMusic->addEventListener(CC_CALLBACK_2(SoundLayer::onMusicSliderCallback, this));
			_sliderEffects->addEventListener(CC_CALLBACK_2(SoundLayer::onEffectsSliderCallback, this));

			//��Ч��ť
			auto btnEffectOpen = search.find<Button>("Button_EffectOpen");
			btnEffectOpen->addTouchEventListener(CC_CALLBACK_2(SoundLayer::onEffectsTouchEventCallBack, this));
			auto btnEffectClose = search.find<Button>("Button_EffectClose");
			btnEffectClose->addTouchEventListener(CC_CALLBACK_2(SoundLayer::onEffectsTouchEventCallBack, this));

			//���ְ�ť
			auto btnMusicOpen = search.find<Button>("Button_MusicOpen");
			btnMusicOpen->addTouchEventListener(CC_CALLBACK_2(SoundLayer::onMusicTouchEventCallBack, this));
			auto btnMusicClose = search.find<Button>("Button_MusicClose");
			btnMusicClose->addTouchEventListener(CC_CALLBACK_2(SoundLayer::onMusicTouchEventCallBack, this));
		}

		readConfig();	

		_settingBK->runAction(Sequence::create(
			Hide::create(), ScaleTo::create(0.0f, 0.2f),
			Show::create(), ScaleTo::create(0.1f, 1.0f),
			nullptr));
		//
		//
		return true;
	}

	void SoundLayer::setMusicCallback(const std::function<void(bool open)>& callback)
	{
		_musicCallback = callback;
	}

	void SoundLayer::setEffectsCallback(const std::function<void(bool open)>& callback)
	{
		_effectsCallback = callback;
	}

	void SoundLayer::onMusicTouchEventCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::ENDED != type) return;
		auto btn = (Button*)pSender;
		std::string name = btn->getName();

		if (name.compare("Button_MusicOpen") == 0)
		{
			_musicCallback(true);
			_sliderMusic->setPercent(50);

			HNAudioEngine::getInstance()->setSwitchOfMusic(true);
			HNAudioEngine::getInstance()->resumeBackgroundMusic();
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(0.5f);
		}

		if (name.compare("Button_MusicClose") == 0)
		{
			_musicCallback(false);
			_sliderMusic->setPercent(0);

			HNAudioEngine::getInstance()->setSwitchOfMusic(false);
			HNAudioEngine::getInstance()->pauseBackgroundMusic();
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
		}
	}

	void SoundLayer::onEffectsTouchEventCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::ENDED != type) return;
		auto btn = (Button*)pSender;
		std::string name = btn->getName();

		if (name.compare("Button_EffectOpen") == 0)
		{
			_sliderEffects->setPercent(50);

			HNAudioEngine::getInstance()->setSwitcjofEffect(true);
			HNAudioEngine::getInstance()->setEffectsVolume(0.5f);
		}

		if (name.compare("Button_EffectClose") == 0)
		{
			_sliderEffects->setPercent(0);

			HNAudioEngine::getInstance()->setSwitcjofEffect(false);
			HNAudioEngine::getInstance()->setEffectsVolume(0.0f);
		}
	}

	void SoundLayer::onMusicSliderCallback(Ref* pSender, Slider::EventType eventType)
	{
		Slider* slider = (Slider*)pSender;
		float l = slider->getPercent() / 100.f;
		switch (eventType)
		{
		case cocos2d::ui::Slider::EventType::ON_PERCENTAGE_CHANGED:
			
			if (slider->getPercent() == 0)
			{
				HNAudioEngine::getInstance()->setSwitchOfMusic(false);
				HNAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (!HNAudioEngine::getInstance()->getSwitchOfMusic())
			{
				HNAudioEngine::getInstance()->setSwitchOfMusic(true);
				HNAudioEngine::getInstance()->resumeBackgroundMusic();
			}
			else
			{

			}
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(l);
			break;
		default:
			break;
		}
	}

	void SoundLayer::onEffectsSliderCallback(Ref* pSender, Slider::EventType eventType)
	{
		Slider* slider = (Slider*)pSender;
		float l = slider->getPercent() / 100.f;
		switch (eventType)
		{
		case cocos2d::ui::Slider::EventType::ON_PERCENTAGE_CHANGED:
			if (slider->getPercent() == 0)
			{
				HNAudioEngine::getInstance()->setSwitcjofEffect(false);
			}
			else
			{
				HNAudioEngine::getInstance()->setSwitcjofEffect(true);
			}
			HNAudioEngine::getInstance()->setEffectsVolume(l);
			break;
		default:
			break;
		}
	}

	void SoundLayer::wrireConfig()
	{
		UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, _sliderEffects->getPercent());
		UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, _sliderMusic->getPercent());
		UserDefault::getInstance()->flush();
	}

	void SoundLayer::readConfig()
	{
		_sliderMusic->setPercent(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT));
		_sliderEffects->setPercent(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT));
	}

}
