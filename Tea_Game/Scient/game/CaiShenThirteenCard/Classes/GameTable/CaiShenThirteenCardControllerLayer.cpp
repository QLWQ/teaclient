/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardControllerLayer.h"
#include "CaiShenThirteenCardWrapper.h"
#include "CaiShenThirteenCardCardBoard.h"
#include "CaiShenThirteenCardFrameWrapper.h"
#include "CaiShenThirteenCardGameLogic.h"

//////////////////////////////////////////////////////////////////////////
#define WIN_SIZE  Director::getInstance()->getWinSize()

#define CARD_BOARD_TAG		1000
#define CARD_BOARD_ZORDER	CARD_BOARD_TAG

namespace CaiShenThirteenCard
{
	const char* _gCardShapeName[] =
	{
		"未知",
		"散牌",
		"一对",
		"两对",
		"三条",
		"顺子",
		"同花",
		"葫芦",
		"四条",
		"同花顺",
		"五同",
	};
	/////////////////////////////////////////////////////////////////////////////////////////

	ControllerLayer::ControllerLayer()
		: _outCardCallback(nullptr)
		, _controUi(nullptr)
		, _cardBoard(nullptr)
		, _btnRecoveryCard(nullptr)
		, _btnOutputCard(nullptr)
		, _btnHead(nullptr)
		, _btnMid(nullptr)
		, _btnLast(nullptr)
		, _btnAutoRubcard(nullptr)
		, _paidunBG(nullptr)
		, _node_cardType(nullptr)
		, _btnStylor(nullptr)
		, _nodecombiningForm(nullptr)
		, _btnmanualRubcard(nullptr)
	{	
		
		memset(_nodeCombinePos, 0, sizeof(_nodeCombinePos));
		memset(_btnCardType, 0, sizeof(_btnCardType));
		memset(_comFormTy, 0, sizeof(_comFormTy));
		memset(_comFormCardValue, 0, sizeof(_comFormCardValue));
		memset(_tetComFormTy, 0, sizeof(_tetComFormTy));
		
	}

	ControllerLayer::~ControllerLayer()
	{
		_controUi = nullptr;
		_paidunBG = nullptr;
		_btnHead = nullptr;
		_btnMid = nullptr;
		_btnLast = nullptr;
		_btnOutputCard = nullptr;
		_btnRecoveryCard = nullptr;
		_btnAutoRubcard = nullptr;
		_cardBoard = nullptr;
		_btnStylor = nullptr;
		_btnmanualRubcard = nullptr;
		for (size_t i = 0; i < CARD_FRAME_COUNT; i++) _arrCardFrame[i] = nullptr;
	}

	void ControllerLayer::doLoad(Layout* Panel_Table)
	{
		_controUi = dynamic_cast<Layout*>(Panel_Table->getChildByName("Panel_Controller"));
		auto winSize = Director::getInstance()->getWinSize();
		float scalex = 1280 / winSize.width;
		float scaley = 720 / winSize.height;
		for (auto child : _controUi->getChildren())
		{
			child->setScale(scalex, scaley);
		}

		//长屏适配
		if (winSize.width / winSize.height >= 1.8)
		{
			for (auto child : _controUi->getChildren())
			{
				child->setScale(scalex / scaley, 1);
			}
		}
		_paidunBG = dynamic_cast<ImageView*>(_controUi->getChildByName("Image_PaidunBG"));
		_paidunBG->setScale(0.7);

		_btnHead = dynamic_cast<Button*>(_paidunBG->getChildByName("Button_Head"));
		_btnMid = dynamic_cast<Button*>(_paidunBG->getChildByName("Button_Middle"));
		_btnLast = dynamic_cast<Button*>(_paidunBG->getChildByName("Button_Tail"));

		for (size_t i = 0; i < CARD_FRAME_COUNT; i++)
		{
			_arrCardFrame[i] = dynamic_cast<ImageView*>(_paidunBG->getChildByName(StringUtils::format("Image_Card%d", i)));
			setCardsValueAndloadTexture(_arrCardFrame[i], 0x0);
		}

		_btnHead->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onControlClick, this));
		_btnMid->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onControlClick, this));
		_btnLast->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onControlClick, this));

		_btnOutputCard = dynamic_cast<Button*>(_controUi->getChildByName("Button_True"));
		_btnOutputCard->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onOutCardClick, this));

		_btnRecoveryCard = dynamic_cast<Button*>(_controUi->getChildByName("Button_Cancel"));
		_btnRecoveryCard->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onRecoveryCardClick, this));

		_btnAutoRubcard = dynamic_cast<Button*>(_controUi->getChildByName("Button_autoRubcard"));
		_btnAutoRubcard->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onHelpClick, this));

		_btnmanualRubcard = dynamic_cast<Button*>(_controUi->getChildByName("Button_manualRubcard"));
		_btnmanualRubcard->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onHelpClick, this));

		_btnStylor = dynamic_cast<Button*>(_controUi->getChildByName("Button_Stylor"));
		_btnStylor->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onStylorSort, this));
		
		_node_cardType = dynamic_cast<Node*>(_controUi->getChildByName("node_cardType"));
		_node_cardType->setVisible(false);

		_nodecombiningForm = dynamic_cast<Node*>(_controUi->getChildByName("node_combiningForm"));
		_nodecombiningForm->setVisible(false);
				

		char cardType[64];	
		for (int i = 0; i < 3; i++)
		{
			sprintf(cardType, "node_combinePos%d", i + 1);
			_nodeCombinePos[i] = dynamic_cast<Node*>(_controUi->getChildByName(cardType));
		}

		for (int i = 0; i < CARD_TYPE; i++)
		{
			sprintf(cardType, "btn_cardType%d", i + 1);
			_btnCardType[i] = dynamic_cast<Button*>(_node_cardType->getChildByName(cardType));
			_btnCardType[i]->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onCardTypeTipClick, this));
			_btnCardType[i]->setEnabled(false);
		}

		for (int i = 0; i < COMFORM_TY; i++)
		{
			sprintf(cardType, "Button_comFormTy%d", i);
			_comFormTy[i] = dynamic_cast<Button*>(_nodecombiningForm->getChildByName(cardType));
			_comFormTy[i]->addClickEventListener(CC_CALLBACK_1(ControllerLayer::onCombiningFormClick, this));
		}
		

		for (int i = 0; i < COMFORM_TY; i++)
		{
			for (int j = 0; j < COMFORM_CARD_TYPE; j++)
			{
				sprintf(cardType, "Text_Type%d", j);
				_tetComFormTy[i][j] = dynamic_cast<Text*>(_comFormTy[i]->getChildByName(cardType));
			}			
		}


		float scY = 720 / WIN_SIZE.height;
		if (WIN_SIZE.width / WIN_SIZE.height <= 1.7f)
		{
			_paidunBG->setScaleY(scY);
			_btnAutoRubcard->setScaleY(scY);
			_btnOutputCard->setScaleY(scY);
			_btnRecoveryCard->setScaleY(scY);
		}
	
	}

	void ControllerLayer::doRestore()
	{
		for (size_t i = 0; i < CARD_FRAME_COUNT; i++) setCardsValueAndloadTexture(_arrCardFrame[i], 0x0);
		if (_cardBoard != nullptr)
		{
			_cardBoard->removeFromParent();
			_cardBoard = nullptr;
		}
		_paidunBG->setVisible(false);
		_btnAutoRubcard->setVisible(false);
		_btnmanualRubcard->setVisible(false);
		_btnStylor->setVisible(false);
		_node_cardType->setVisible(false);
		_btnOutputCard->setVisible(false);
		_btnRecoveryCard->setVisible(false);
		_nodecombiningForm->setVisible(false);
		_nodecombiningForm->setPosition(_nodeCombinePos[2]->getPosition()); //位置恢复
		restoreDunBtn(false);
	}

	void ControllerLayer::loadCards(const std::vector<BYTE>& cards, float interval)
	{
		if (_cardBoard)
		{
			_cardBoard->removeFromParent();
			_cardBoard = nullptr;
		}
		_cardBoard = CardBoard::create(true);
		_cardBoard->setPosition(WIN_SIZE.width / 2, 165.f);
		_controUi->addChild(_cardBoard, CARD_BOARD_ZORDER, CARD_BOARD_TAG);
		_cardBoard->setMultSelected(true);

		float scY = 720 / WIN_SIZE.height;
		_cardBoard->setScaleY(scY);
		//长屏适配
		if (WIN_SIZE.width / WIN_SIZE.height >= 1.8)
		{
			_cardBoard->setScale(1 / scY, 1);
		}

		if (interval > 0.1f)
		{
			_cardBoard->sendCardOneByOne(cards, interval);
		}
		else
		{
			//防止断线重连回来原先的牌没被移除
			_cardBoard->removeCard(cards);
			_cardBoard->sendCard(cards);
		}

		_cardBoard->_moveCardCallBack = [=](){
			checkBtnStatue();
		};
	}

	void ControllerLayer::checkBtnStatue()
	{
		std::vector<BYTE> cards = _cardBoard->getUpCards();

		int headCount = getCardFrameValidCount(0, 3);
		int midCount = getCardFrameValidCount(3, 8);
		int lastCount = getCardFrameValidCount(8, 13);

		if (headCount == 0) _btnHead->setEnabled(cards.size() == 3);
		if (midCount == 0) _btnMid->setEnabled(cards.size() == 5);
		if (lastCount == 0) _btnLast->setEnabled(cards.size() == 5);
	}

	void ControllerLayer::cardBoardTouch(bool enableTouch)
	{
		_paidunBG->setVisible(enableTouch);
		if (_cardBoard) _cardBoard->enableCardTouch(enableTouch);
		_btnAutoRubcard->setVisible(enableTouch);
		_node_cardType->setVisible(enableTouch);
		_btnStylor->setVisible(enableTouch);
	}

	void ControllerLayer::setCardsValueAndloadTexture(ImageView* card, BYTE cardValue)
	{
		char filename[128];
		sprintf(filename, "Games/CaiShenThirteenCard/GamePlist/0x%02X.png", cardValue);
		card->loadTexture(filename, Widget::TextureResType::PLIST);
		card->setTag(cardValue);
		card->setVisible(0x0 != cardValue);
	}

	void ControllerLayer::getCards(int index, BYTE cards[5])
	{
		if (index < 0) return;
		if (index > 2) return;
		
		switch (index)
		{
		case 0: for (size_t i = 0; i < 3; i++) cards[i] = _arrCardFrame[i]->getTag(); break;
		case 1: for (size_t i = 0; i < 5; i++) cards[i] = _arrCardFrame[i + 3]->getTag(); break;
		case 2: for (size_t i = 0; i < 5; i++) cards[i] = _arrCardFrame[i + 8]->getTag(); break;
		default:
			break;
		}
	}

	std::vector<BYTE> ControllerLayer::getCards()
	{
		std::vector<BYTE> cards;
		for (size_t i = 0; i < CARD_FRAME_COUNT; i++)
		{
			cards.push_back(_arrCardFrame[i]->getTag());
		}
		return cards;
	}

	bool ControllerLayer::scanningCardFrame(int start, int end, const std::function<void(ImageView* card)>& callback)
	{
		if (start > end) return false;
		if (start < 0) return false;
		if (end > CARD_FRAME_COUNT) return false;

		for (int i = start; i < end; i++) callback(_arrCardFrame[i]);

		return true;
	}

	std::vector<BYTE> ControllerLayer::restoreCardFrame(int start, int end)
	{
		std::vector<BYTE> cards;
		scanningCardFrame(start, end, [&](ImageView* card)
		{
			BYTE val = card->getTag();
			if (val != 0x0)
			{
				cards.push_back(val);
				setCardsValueAndloadTexture(card, 0x0);
			}
		});

		return cards;
	}

	int ControllerLayer::getCardFrameValidCount(int start, int end)
	{
		int ret = 0;
		scanningCardFrame(start, end, [&ret](ImageView* card)
		{
			BYTE val = card->getTag();
			if (val != 0x0) ret++;
		});

		return ret;
	}

	void ControllerLayer::restoreDunBtn(bool bEnabled)
	{
		_btnHead->setEnabled(bEnabled);
		_btnMid->setEnabled(bEnabled);
		_btnLast->setEnabled(bEnabled);
	}

	void ControllerLayer::setOutCardClickEvent(const ccControllerLayerClick& callback)
	{
		_outCardCallback = callback;
	}

	void ControllerLayer::onRecoveryCardClick(cocos2d::Ref *pSender)
	{		
		_btnOutputCard->setVisible(false);
		_btnRecoveryCard->setVisible(false);

		if (_btnmanualRubcard->isVisible())
		{
			_node_cardType->setVisible(false);
			_nodecombiningForm->setVisible(true);
		} 
		else if (_btnAutoRubcard->isVisible())
		{
			_node_cardType->setVisible(true);
			_nodecombiningForm->setVisible(false);
		}
		
		

		std::vector<BYTE> cards = restoreCardFrame(0, 13);

		if (!cards.empty())
		{
			if (_cardBoard) _cardBoard->sendCard(cards);
		}

		// 更新头墩， 中墩， 尾墩按钮
		restoreDunBtn(false);

		checkCardTypeBtnStatue();	
	}

	void ControllerLayer::onOutCardClick(cocos2d::Ref *pSender)
	{
		//cardBoardTouch(false);
		//_btnOutputCard->setVisible(false);
		//_btnRecoveryCard->setVisible(false);
		_isClickF = false;
		if (nullptr != _outCardCallback)
		{
			_outCardCallback(this);
		}
	}

	void ControllerLayer::onCardFrameClick(CardFrameWrapper* cardFrame)
	{
		// 上-次选的牌盒和当前选的进行交换
		/*auto switchCard = [&](CardFrameWrapper* cardFrame)
		{
			for (size_t i = 0; i < CARD_FRAME_COUNT; i++)
			{
				if (_arrCardFrame[i] == cardFrame) continue;

				if (_arrCardFrame[i]->isSelected())
				{
					BYTE val_0 = _arrCardFrame[i]->getTag();
					BYTE val_1 = cardFrame->getValue();

					cardFrame->setValue(val_0);
					_arrCardFrame[i]->setValue(val_1);
					break;
				}
			}
		};
		// 1. 当前牌盒如果有牌，则交换。
		// 2. 当前牌盒没有牌,则判断牌面板有没有选中的牌，如果有，则添加到牌盒。
		std::vector<BYTE> cards = _cardBoard->getUpCards();
		if (cards.empty())
		{
			switchCard(cardFrame);
		}
		else
		{
			// 如果当前选中的牌盒没有牌的时候添加
			if (cardFrame->getValue() == 0xFF)
			{
				if (cards.size() > 1 && cards.size() <= 5)
				{
					int row = cardFrame->getGroup();
					bool success = false;
					switch (row)
					{
					case 1:
					{
						if (cards.size() == 3)
						{
							for (size_t i = 0; i < 3; i++)
							{
								_arrCardFrame[i]->setValue(cards[i]);
							}
							success = true;
						}

					} break;
					case 2:
					{
						if (cards.size() == 5)
						{
							for (size_t i = 0; i < 5; i++)
							{
								_arrCardFrame[i + 3]->setValue(cards[i]);
							}
							success = true;
						}
					} break;
					case 3:
					{
						if (cards.size() == 5)
						{
							for (size_t i = 0; i < 5; i++)
							{
								_arrCardFrame[i + 8]->setValue(cards[i]);
							}
							success = true;
						}
					} break;
					default:
						break;
					}

					if (success)
					{
						_cardBoard->removeCard(cards);
					}
				}
				else
				{
					for (auto& card : cards)
					{
						cardFrame->setValue(card);
					}
					_cardBoard->removeCard(cards);
				}				
			}	
		}

		// 反选牌盒
		for (size_t i = 0; i < CARD_FRAME_COUNT; i++)
		{
			if (_arrCardFrame[i] == cardFrame) continue;
			_arrCardFrame[i]->setSelected(false);
		}

		// 更新头墩， 中墩， 尾墩按钮
		restoreDunBtn();

		// 出牌按钮更新
		_btnOutputCard->setVisible(_cardBoard->empty());
		_btnRecoveryCard->setVisible(_cardBoard->empty());
		*/
	}

	bool ControllerLayer::checkCardsTypeValid()
	{
		int headCount = getCardFrameValidCount(0, 3);
		int midCount = getCardFrameValidCount(3, 8);
		int lastCount = getCardFrameValidCount(8, 13);

		BYTE headCard[5] = { 0 };
		BYTE midCard[5] = { 0 };
		BYTE lastCard[5] = { 0 };

		getCards(0, headCard);
		getCards(1, midCard);
		getCards(2, lastCard);

		CUpGradeGameLogic gameLogic;

		bool isValid = true;
		if (headCount == 3 && midCount == 5)
		{
			if (1 == gameLogic.CompareCard(headCard, 3, midCard, 5)) isValid = false;
		}

		if (headCount == 3 && lastCount == 5)
		{
			if (1 == gameLogic.CompareCard(headCard, 3, lastCard, 5)) isValid = false;
		}

		if (midCount == 5 && lastCount == 5)
		{
			if (1 == gameLogic.CompareCard(midCard, 5, lastCard, 5)) isValid = false;
		}
		return isValid;
	}
	void  ControllerLayer::onStylorSort(cocos2d::Ref *pSender)
	{
		if (!_isClickF)
		{
			_cardBoard->sortStylorCardEvent();
			_isClickF = true;
		}
		else
		{
			_cardBoard->ReturnsortCardEvent();
			_isClickF = false;
		}
	}
	void ControllerLayer::onHelpClick(cocos2d::Ref *pSender)
	{
		Button * control = dynamic_cast<Button*>(pSender);
		std::string name = control->getName();
				
		if (name.compare("Button_autoRubcard") == 0) //一键搓牌
		{
		//	log("zhifu 11111");
			//所有牌回收
			onRecoveryCardClick(nullptr);
			if (_cardBoard != nullptr)
			{
				_cardBoard->downCards();
			}
			_node_cardType->setVisible(false);
			_nodecombiningForm->setVisible(true);
			_btnmanualRubcard->setVisible(true);
			_btnAutoRubcard->setVisible(false);
	//		log("zhifu 22222");
			for (int i = 0; i < COMFORM_TY; i++)
			{
				_comFormTy[i]->setVisible(true);
			}
	//		log("zhifu 33333");
			/* //使用服务端的一键搓牌数据
			for (int i = 0; i < COMFORM_TY; i++)
			{
				findCardType(i);
			}
			*/
	//		log("zhifu 44444");
			compareCardType();
	//		log("zhifu 55555");
		}
		else if (name.compare("Button_manualRubcard") == 0)//手动搓牌
		{
			checkCardTypeBtnStatue();
			_node_cardType->setVisible(true);
			_nodecombiningForm->setVisible(false);

			_btnmanualRubcard->setVisible(false);
			_btnAutoRubcard->setVisible(true);
			//
			onRecoveryCardClick(NULL);
			_cardBoard->downCards();
		}
	}

	void ControllerLayer::compareCardType()
	{	
		/*
		1:a = b  = c
		2:a = b != c (_nodeCombinePos1 去a)
		3:a = c != b (_nodeCombinePos1 去a)
		4:b = c != a (_nodeCombinePos2 去c)
		5:a != b != c
		*/

		 int iRlt1 = strcmp((char*)_comFormCardValue[eCombiningForm::eFirstType], (char*)_comFormCardValue[eCombiningForm::eSecondType]);//a b
		 int iRlt2 = strcmp((char*)_comFormCardValue[eCombiningForm::eFirstType], (char*)_comFormCardValue[eCombiningForm::eThirdType]);//a c
		 int iRlt3 = strcmp((char*)_comFormCardValue[eCombiningForm::eSecondType], (char*)_comFormCardValue[eCombiningForm::eThirdType]);//b c
		 
		if (iRlt1 == 0 && iRlt3 == 0 && iRlt2 == 0)// a == b && b == c && a == c
		{
			_comFormTy[eCombiningForm::eSecondType]->setVisible(true);
			_comFormTy[eCombiningForm::eFirstType]->setVisible(false);
			_comFormTy[eCombiningForm::eThirdType]->setVisible(false);	
		}		
		else if ((iRlt1 == 0 && iRlt2 != 0) || (iRlt2 == 0 && iRlt1 != 0))//(a == b && a != c) || (a == c && a != b)
		{
			if (_tetComFormTy[0][2]->getString() == _tetComFormTy[1][2]->getString()) //值和牌型完全一样
			{
				_comFormTy[eCombiningForm::eFirstType]->setVisible(false);
				_nodecombiningForm->setPosition(_nodeCombinePos[0]->getPosition());
			}
		}	
		else if ((iRlt3 == 0 && iRlt1 != 0))//(b == c && b != a)
		{
			_comFormTy[eCombiningForm::eThirdType]->setVisible(false);

			_nodecombiningForm->setPosition(_nodeCombinePos[1]->getPosition());
		}
		else if (iRlt1 != 0 && iRlt3 != 0 && iRlt2 != 0)//a != b && a != c && b != c
		{
			_comFormTy[eCombiningForm::eSecondType]->setVisible(true);
			_comFormTy[eCombiningForm::eFirstType]->setVisible(true);
			_comFormTy[eCombiningForm::eThirdType]->setVisible(true);
		}
	}

	std::string ControllerLayer::getCardShapeName(int iHeapShape)
	{
		return _gCardShapeName[iHeapShape];
	}
	void ControllerLayer::setAutoRubCardType(BYTE byHeapCard[3][5], int iHeapShape[3])
	{
		//这里保存了三份一样的数据。
		for (int type = 0; type < COMFORM_TY; type++)
		{
			for (int i = 0; i < 3; i++)
			{
				_tetComFormTy[type][i]->setString(GBKToUtf8(getCardShapeName(iHeapShape[i])));
			}
			for (int i = 0; i < 3; i++)
			{
				_comFormCardValue[type][i] = byHeapCard[0][i];
			}
			for (int i = 3; i < 8; i++)
			{ 
				_comFormCardValue[type][i] = byHeapCard[1][i - 3];
			}
			for (int i = 8; i < 13; i++)
			{
				_comFormCardValue[type][i] = byHeapCard[2][i - 8];
			}
		}
	}


	void ControllerLayer::findCardType(int type)
	{	
		std::vector<BYTE> cards = _cardBoard->getCards();

		if (cards.empty()) return;

		BYTE tempCards[13] = { 0 };
		std::string sContent[3] = {"散牌","散牌","散牌"};

		std::string str0 = "";
		std::string str1 = "";
		std::string str2 = "";
		//尾道牌值
		copy(cards.begin(), cards.end(), std::begin(tempCards));
		INT tailCardCount = 0;
		BYTE tailCardCards[5] = { 0 };
		switch (type)
		{
		case eFirstType:
			str2 = findFirstCardType(tempCards, cards.size(), tailCardCards, tailCardCount);
			break;
		case eSecondType:
			str2 = findSecondCardType(tempCards, cards.size(), tailCardCards, tailCardCount);
			break;
		case eThirdType:
			str2 = findThirdCardType(tempCards, cards.size(), tailCardCards, tailCardCount);
			break;
		default:
			break;
		}
		for (int i = 0; i < 5; i++)
		{
			auto iterCard = find(cards.begin(), cards.end(), tailCardCards[i]);
			cards.erase(iterCard);
		}

		//中道牌值_
		copy(cards.begin(), cards.end(), std::begin(tempCards));
		INT middleCardCount = 0;
		BYTE middleCardCards[5] = { 0 };
		switch (type)
		{
		case eFirstType:
			str1 = findFirstCardType(tempCards, cards.size(), middleCardCards, middleCardCount);
			break;
		case eSecondType:
			str1 = findSecondCardType(tempCards, cards.size(), middleCardCards, middleCardCount);
			break;
		case eThirdType:
			str1 = findThirdCardType(tempCards, cards.size(), middleCardCards, middleCardCount);
			break;
		default:
			break;
		}
		for (int i = 0; i < 5; i++)
		{
			auto iterCard = find(cards.begin(), cards.end(), middleCardCards[i]);
			cards.erase(iterCard);
		}
		CUpGradeGameLogic gameLogic;
		if (gameLogic.CompareCard(middleCardCards, 5, tailCardCards, 5) == 1)//如果中道大于尾道，则互换
		{
			BYTE tempCardCards[5] = { 0 };
			memcpy(tempCardCards, middleCardCards, sizeof(middleCardCards));
			memcpy(middleCardCards, tailCardCards, sizeof(middleCardCards));
			memcpy(tailCardCards, tempCardCards, sizeof(tempCardCards));

			std::string tempContent = "";
			tempContent = str2;
			str2 = str1;
			str1 = tempContent;
		}



		//头道牌值
		copy(cards.begin(), cards.end(), std::begin(tempCards));
		INT headCardCount = 0;
		BYTE headCardCards[3] = { 0 };
		int cardSize = cards.size();

		//查找三条
		bool result = false;
		result = gameLogic.FindThreeStrip(tempCards, cardSize, headCardCards, headCardCount);
		str0 = ("三条");
		if (!result)
		{
			//查找一对
			result = gameLogic.FindOneDouble(tempCards, cardSize, headCardCards, headCardCount, true);
			str0 = ("一对");
			if (!result)
			{
				//查找散牌
				result = gameLogic.FindMaxHighCard(tempCards, cardSize, headCardCards, headCardCount);
				str0 = ("散牌");
			}
		}
		

		//设置文字
		//for (int i = 0; i < COMFORM_TY; i++)
		{
			_tetComFormTy[type][0]->setString(GBKToUtf8(str0));
			_tetComFormTy[type][1]->setString(GBKToUtf8(str1));
			_tetComFormTy[type][2]->setString(GBKToUtf8(str2));
		}

		for (int i = 8; i < 13; i++)
		{
			_comFormCardValue[type][i] = tailCardCards[i - 8];
		}
		//各道牌值保存_
		for (int i = 0; i < 3; i++)
		{
			_comFormCardValue[type][i] = headCardCards[i]; 
		}

		for (int i = 3; i < 8; i++)
		{
			_comFormCardValue[type][i] = middleCardCards[i - 3];
		}		
	}

	std::string ControllerLayer::findFirstCardType(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount)
	{
		//1:同花顺>炸弹>葫芦>同花>顺子>三条>两对>一对>单张
		std::string sContent = "";
		bool result = false;
		CUpGradeGameLogic gameLogic;
		//查找五同
		result = gameLogic.FindFiveStrip(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
		sContent = "五同";
		if (!result)
		{
			//查找同花顺
			result = gameLogic.FindSameFlowerFlush(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
			sContent = "同花顺";
			if (!result)
			{
				//查找四条
				result = gameLogic.FindFourStrip(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
				sContent = "炸弹";
				if (!result)
				{
					//查找葫芦
					result = gameLogic.FindCalabash(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
					sContent = "葫芦";
					if (!result)
					{
						//查找同花
						result = gameLogic.FindSameFlower(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
						sContent = "同花";
						if (!result)
						{
							//查找顺子
							result = gameLogic.FindStraight(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
							sContent = "顺子";
							if (!result)
							{
								//查找三条
								result = gameLogic.FindThreeStrip(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
								sContent = "三条";
								if (!result)
								{
									//查找两对
									result = gameLogic.FindSecondDouble(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
									sContent = "两对";
									if (!result)
									{
										//查找一对
										result = gameLogic.FindOneDouble(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
										sContent = "一对";
										if (!result)
										{
											//查找散牌
											result = gameLogic.FindMaxHighCard(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
											sContent = "散牌";
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return (sContent);
	}


	std::string ControllerLayer::findSecondCardType(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount)//第二种牌型组合_ 
	{
		//1:同花顺>炸弹>葫芦>同花>顺子>三条>两对>一对>单张
		std::string sContent = "";
		bool result = false;
		CUpGradeGameLogic gameLogic;
			//查找同花顺
			result = gameLogic.FindSameFlowerFlush(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
			sContent = "同花顺";
			if (!result)
			{
				//查找四条
				result = gameLogic.FindFourStrip(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
				sContent = "炸弹";
				if (!result)
				{
					//查找葫芦
					result = gameLogic.FindCalabash(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
					sContent = "葫芦";
					if (!result)
					{
						//查找同花
						result = gameLogic.FindSameFlower(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
						sContent = "同花";
						if (!result)
						{
							//查找顺子
							result = gameLogic.FindStraight(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
							sContent = "顺子";
							if (!result)
							{
								//查找三条
								result = gameLogic.FindThreeStrip(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
								sContent = "三条";
								if (!result)
								{
									//查找两对
									result = gameLogic.FindSecondDouble(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
									sContent = "两对";
									if (!result)
									{
										//查找一对
										result = gameLogic.FindOneDouble(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
										sContent = "一对";
										if (!result)
										{
											//查找散牌
											result = gameLogic.FindMaxHighCard(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
											sContent = "散牌";
										}
									}
								}
							}
						}
					}
				}
			}
		return (sContent);
	}

	std::string ControllerLayer::findThirdCardType(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount)//第三种牌型组合_ 
	{
		//1:同花顺>炸弹>葫芦>同花>顺子>三条>两对>一对>单张
		std::string sContent = "";
		bool result = false;
		CUpGradeGameLogic gameLogic;
				////查找四条
				result = gameLogic.FindFourStrip(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
				sContent = "炸弹";
				if (!result)
				{
				//查找同花顺
				result = gameLogic.FindSameFlowerFlush(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
				sContent = "同花顺";
				
					if (!result)
					{
						//查找葫芦
						result = gameLogic.FindCalabash(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
						sContent = "葫芦";
						if (!result)
						{
							//查找同花
							result = gameLogic.FindSameFlower(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
							sContent = "同花";
							if (!result)
							{
								//查找顺子
								result = gameLogic.FindStraight(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
								sContent = "顺子";
								if (!result)
								{
									//查找三条
									result = gameLogic.FindThreeStrip(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
									sContent = "三条";
									if (!result)
									{
										//查找两对
										result = gameLogic.FindSecondDouble(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
										sContent = "两对";
										if (!result)
										{
											//查找一对
											result = gameLogic.FindOneDouble(iHandCard, iHandCardCount, iResultCard, iResultCardCount, true);
											sContent = "一对";
											if (!result)
											{
												//查找散牌
												result = gameLogic.FindMaxHighCard(iHandCard, iHandCardCount, iResultCard, iResultCardCount);
												sContent = "散牌";
											}
										}
									}
								}
							}
						}
					}
				}
				
		return (sContent);
	}

	void ControllerLayer::onCardTypeTipClick(cocos2d::Ref *pSender)
	{
		Button * control = dynamic_cast<Button*>(pSender);
		std::string name = control->getName();
	
		std::vector<BYTE> cards = _cardBoard->getCards();

		if (cards.empty()) return;

		BYTE tempCards[13] = { 0 };
		INT newCount = 0;
		BYTE newCards[13] = { 0 };

		copy(cards.begin(), cards.end(), std::begin(tempCards));
		cards.clear();
		CUpGradeGameLogic gameLogic;
		bool result = false;
		//牌型
		if (name.compare("btn_cardType1") == 0)//牌型：0对子
		{
			clearNowIndex(DOUBLE);
			if (_nowIndex[DOUBLE]>=_hintCardNum[DOUBLE]-1)
			{
				_nowIndex[DOUBLE] = 0;
			}
			for (int i = 0; i < 2;i++)
			{
				cards.push_back(_hintCardList[DOUBLE].at(_nowIndex[DOUBLE]));
				_nowIndex[DOUBLE]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
		else if (name.compare("btn_cardType2") == 0)//两对
		{
			clearNowIndex(TWO_DOUBLE);
			if (_nowIndex[TWO_DOUBLE] >= _hintCardNum[TWO_DOUBLE] - 1)
			{
				_nowIndex[TWO_DOUBLE] = 0;
			}
			for (int i = 0; i < 4; i++)
			{
				cards.push_back(_hintCardList[TWO_DOUBLE].at(_nowIndex[TWO_DOUBLE]));
				_nowIndex[TWO_DOUBLE]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
		else if (name.compare("btn_cardType3") == 0)//三条
		{
			clearNowIndex(THREE);
			if (_nowIndex[THREE] >= _hintCardNum[THREE] - 1)
			{
				_nowIndex[THREE] = 0;
			}
			for (int i = 0; i < 3; i++)
			{
				cards.push_back(_hintCardList[THREE].at(_nowIndex[THREE]));
				_nowIndex[THREE]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
		else if (name.compare("btn_cardType4") == 0)//顺子
		{
			clearNowIndex(SHUNZI);
			if (_nowIndex[SHUNZI] >= _hintCardNum[SHUNZI] - 1)
			{
				_nowIndex[SHUNZI] = 0;
			}
			for (int i = 0; i < 5; i++)
			{
				cards.push_back(_hintCardList[SHUNZI].at(_nowIndex[SHUNZI]));
				_nowIndex[SHUNZI]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
		else if (name.compare("btn_cardType5") == 0)//同花
		{
			clearNowIndex(SAMEHUA);
			if (_nowIndex[SAMEHUA] >= _hintCardNum[SAMEHUA] - 1)
			{
				_nowIndex[SAMEHUA] = 0;
			}
			for (int i = 0; i < 5; i++)
			{
				cards.push_back(_hintCardList[SAMEHUA].at(_nowIndex[SAMEHUA]));
				_nowIndex[SAMEHUA]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
		else if (name.compare("btn_cardType6") == 0)// 葫芦
		{
			clearNowIndex(HULU);
			if (_nowIndex[HULU] >= _hintCardNum[HULU] - 1)
			{
				_nowIndex[HULU] = 0;
			}
			for (int i = 0; i < 5; i++)
			{
				cards.push_back(_hintCardList[HULU].at(_nowIndex[HULU]));
				_nowIndex[HULU]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
		else if (name.compare("btn_cardType7") == 0)//炸弹
		{
			clearNowIndex(FOUR);
			if (_nowIndex[FOUR] >= _hintCardNum[FOUR] - 1)
			{
				_nowIndex[FOUR] = 0;
			}
			for (int i = 0; i < 4; i++)
			{
				cards.push_back(_hintCardList[FOUR].at(_nowIndex[FOUR]));
				_nowIndex[FOUR]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
		else if (name.compare("btn_cardType8") == 0)//同花顺
		{
			clearNowIndex(SAMEHUA_SHUNZI);
			if (_nowIndex[SAMEHUA_SHUNZI] >= _hintCardNum[SAMEHUA_SHUNZI] - 1)
			{
				_nowIndex[SAMEHUA_SHUNZI] = 0;
			}
			for (int i = 0; i < 5; i++)
			{
				cards.push_back(_hintCardList[SAMEHUA_SHUNZI].at(_nowIndex[SAMEHUA_SHUNZI]));
				_nowIndex[SAMEHUA_SHUNZI]++;
			}
			_cardBoard->upCards(cards);
			return;
		}	
		else if (name.compare("btn_cardType9") == 0)//五同
		{
			clearNowIndex(WUTONG);
			if (_nowIndex[WUTONG] >= _hintCardNum[WUTONG] - 1)
			{
				_nowIndex[WUTONG] = 0;
			}
			for (int i = 0; i < 5; i++)
			{
				cards.push_back(_hintCardList[WUTONG].at(_nowIndex[WUTONG]));
				_nowIndex[WUTONG]++;
			}
			_cardBoard->upCards(cards);
			return;
		}
	}


	void ControllerLayer::checkCardTypeBtnStatue()
	{
		if (!_cardBoard) return;
		std::vector<BYTE> cards = _cardBoard->getCards();

		if (cards.empty()) return;

		if (cards.size() > 13) return;

		memset(_hintCardList, 0, sizeof(_hintCardList));
		memset(_hintCardNum, 0, sizeof(_hintCardNum));
		memset(_nowIndex, 0, sizeof(_nowIndex));

		BYTE tempCards[13] = { 0 };
		INT newCount = 0;
		//BYTE newCards[500] = { 0 };
		//std::vector<BYTE> newCards;
		copy(cards.begin(), cards.end(), std::begin(tempCards));

		CUpGradeGameLogic gameLogic;
		
		_btnCardType[0]->setEnabled(gameLogic.FindAllDouble(tempCards, cards.size(), _hintCardList[DOUBLE], newCount));//对子
		_hintCardNum[DOUBLE] = newCount;
		newCount = 0;
		
		_btnCardType[1]->setEnabled(gameLogic.FindAllTwoDouble(tempCards, cards.size(), _hintCardList[TWO_DOUBLE], newCount));//两对子
		_hintCardNum[TWO_DOUBLE] = newCount;
		newCount = 0;

		_btnCardType[2]->setEnabled(gameLogic.FindAllThreeStrip(tempCards, cards.size(), _hintCardList[THREE], newCount));//三条
		_hintCardNum[THREE] = newCount;
		newCount = 0;

		_btnCardType[3]->setEnabled(gameLogic.FindAllStraight(tempCards, cards.size(), _hintCardList[SHUNZI], newCount));//顺子
		_hintCardNum[SHUNZI] = newCount;
		newCount = 0;

		_btnCardType[4]->setEnabled(gameLogic.FindAllSameFlower(tempCards, cards.size(), _hintCardList[SAMEHUA], newCount));//同花
		_hintCardNum[SAMEHUA] = newCount;
		newCount = 0;

		_btnCardType[5]->setEnabled(gameLogic.FindAllCalabash(tempCards, cards.size(), _hintCardList[HULU], newCount));//葫芦
		_hintCardNum[HULU] = newCount;
		newCount = 0;

		_btnCardType[6]->setEnabled(gameLogic.FindAllFourStrip(tempCards, cards.size(), _hintCardList[FOUR], newCount));//炸弹
		_hintCardNum[FOUR] = newCount;
		newCount = 0;

		_btnCardType[7]->setEnabled(gameLogic.FindAllSameFlowerFlush(tempCards, cards.size(), _hintCardList[SAMEHUA_SHUNZI], newCount));//同花顺
		_hintCardNum[SAMEHUA_SHUNZI] = newCount;
		newCount = 0;

		_btnCardType[8]->setEnabled(gameLogic.FindAllFiveStrip(tempCards, cards.size(), _hintCardList[WUTONG], newCount));//五同
		_hintCardNum[WUTONG] = newCount;
		newCount = 0;
	}


	void ControllerLayer::showCombineFormState(bool isVisible)
	{
		if (_nodecombiningForm)
		{
			_nodecombiningForm->setVisible(isVisible);
		}		
	}

	void ControllerLayer::setCardBoardVisible(bool isVisible)
	{
		if (_cardBoard)
		{
			_cardBoard->setVisible(isVisible);
		}
	}

	void ControllerLayer::onCombiningFormClick(cocos2d::Ref *pSender)
	{
		_nodecombiningForm->setVisible(false);
		Button * control = dynamic_cast<Button*>(pSender);
		std::string name = control->getName();

		int iCombineType = 0;
		if (name.compare("Button_comFormTy0") == 0)//组合：1:同花顺>炸弹>葫芦>同花>顺子>三条>两对>一对>单张
		{
			iCombineType = 0;
		}
		else if (name.compare("Button_comFormTy1") == 0)//组合：2:炸弹>葫芦>三条>两对>一对>同花顺>同花>顺子>单张 
		{
			iCombineType = 1;
		}	
		else if (name.compare("Button_comFormTy2") == 0)//组合：3:同花顺>同花>顺子>炸弹>葫芦>三条>两对>一对>单张
		{
			iCombineType = 2;
		}
				
		for (int i = 0; i < CARD_FRAME_COUNT; i++)
		{
			setCardsValueAndloadTexture(_arrCardFrame[i], _comFormCardValue[iCombineType][i]);
		}
		
		if (_cardBoard)
		{
			_cardBoard->removeAllCard();
			// 出牌按钮更新
			_btnOutputCard->setVisible(_cardBoard->empty());
			_btnRecoveryCard->setVisible(_cardBoard->empty());

			restoreDunBtn(false);

			if (_cardBoard->empty())
			{
				_node_cardType->setVisible(false);
				_nodecombiningForm->setVisible(false);
			}
		}
	}

	void ControllerLayer::onControlClick(cocos2d::Ref *pSender)
	{
		Button * control = dynamic_cast<Button*>(pSender);
		std::string name = control->getName();

		if (!_cardBoard) return;
		
		HNAudioEngine::getInstance()->playEffect("CaiShenThirteenCard/sound/putcard.mp3");

		std::vector<BYTE> upCards = _cardBoard->getUpCards();
	
		if (upCards.size() == 5)
		{
			int byCard[5] = { 13, 4, 3, 2, 1 };
			bool isSpecial = true; //是否是A 2 3 4 5的顺子

			int cardValue = 0;
			for (int i = 0; i < 5; i++)
			{
				cardValue = upCards.at(i) & UG_VALUE_MASK;
				if (cardValue != byCard[i])
				{
					isSpecial = false;
					break;
				}
			}
			if (isSpecial)
			{
				upCards.push_back(upCards.at(0));
				upCards.erase(upCards.begin());
			}
		}
		else
		{
			_cardBoard->sortUpCards(upCards);
		}
		
		bool success = false;

		if (name.compare("Button_Head") == 0)
		{
			std::vector<BYTE> cards;
			cards = restoreCardFrame(0, 3);
			if (!cards.empty())
			{
				_cardBoard->sendCard(cards);
				checkCardTypeBtnStatue();
			}
			else
			{
				if (upCards.size() == 3)
				{
					for (size_t i = 0; i < 3; i++) setCardsValueAndloadTexture(_arrCardFrame[i], upCards[i]);
					success = true;
				}
			}
		}

		if (name.compare("Button_Middle") == 0)
		{
			std::vector<BYTE> cards;
			cards = restoreCardFrame(3, 8);
			if (!cards.empty())
			{
				_cardBoard->sendCard(cards);
				checkCardTypeBtnStatue();
			}
			else
			{
				if (upCards.size() == 5)
				{
					for (size_t i = 0; i < 5; i++) setCardsValueAndloadTexture(_arrCardFrame[i + 3], upCards[i]);
					success = true;
				}
			}
		}

		if (name.compare("Button_Tail") == 0)
		{
			std::vector<BYTE> cards;
			cards = restoreCardFrame(8, 13);
			if (!cards.empty())
			{
				_cardBoard->sendCard(cards);
				checkCardTypeBtnStatue();
			}
			else
			{
				if (upCards.size() == 5)
				{
					for (size_t i = 0; i < 5; i++) setCardsValueAndloadTexture(_arrCardFrame[i + 8], upCards[i]);
					success = true;
				}
			}
		}

		if (success)
		{
		
			//如果牌型顺序不合法（尾墩>中墩>头墩）
			if (!checkCardsTypeValid())
			{
				auto tip = Sprite::create("CaiShenThirteenCard/GameTableUi/res/game_tip.png");
				tip->setPosition(Vec2(WIN_SIZE / 2));
				_controUi->addChild(tip, CARD_BOARD_ZORDER + 1);
				tip->setScale(0.1f);
				tip->runAction(Sequence::create(ScaleTo::create(0.2f, 1.0f), 
					DelayTime::create(1.0f), ScaleTo::create(0.1f, 0.0f), RemoveSelf::create(true), nullptr));

				//所有牌回收
				onRecoveryCardClick(nullptr);			
			}

			if (_cardBoard)
			{
				_cardBoard->removeCard(upCards);
				// 出牌按钮更新
				_btnOutputCard->setVisible(_cardBoard->empty());
				_btnRecoveryCard->setVisible(_cardBoard->empty());

				if (_cardBoard->empty())
				{
					_node_cardType->setVisible(false);
					_nodecombiningForm->setVisible(false);
				}
				checkCardTypeBtnStatue();
			}

			if (checkCardsTypeValid())//牌型合法，剩余5张或者3张自动摆上牌
			{
				int a = _cardBoard->getCardSize();
				if ((a == 3) || (a == 5))
				{
					auto sp = Sprite::create();
					this->addChild(sp);
					_cardBoard->upAllCards();
					if (getCardFrameValidCount(0, 3) == 0)
					{
						onControlClick(_btnHead);
					}
					if (getCardFrameValidCount(3, 8) == 0)
					{
						onControlClick(_btnMid);
					}
					if (getCardFrameValidCount(8, 13) == 0)
					{
						onControlClick(_btnLast);
					}
				}
			}
		}
		else
		{
			//回收
			_btnOutputCard->setVisible(false);
			_btnRecoveryCard->setVisible(false);

			if (_btnmanualRubcard->isVisible())
			{
				_node_cardType->setVisible(false);
				_nodecombiningForm->setVisible(true);
			}
			else if (_btnAutoRubcard->isVisible())
			{
				_node_cardType->setVisible(true);
				_nodecombiningForm->setVisible(false);
			}


			checkCardTypeBtnStatue();
		}
		checkBtnStatue();
	}

	void ControllerLayer::clearNowIndex(INT type)
	{
		for (int i = 0; i < ALL_TYPE;i++)
		{
			if (type != i)
			{
				_nowIndex[i] = 0;
			}
		}
	}
}
