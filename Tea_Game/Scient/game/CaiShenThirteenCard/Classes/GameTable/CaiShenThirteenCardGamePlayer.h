/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_GAME_PLAYER_H__
#define __CaiShenThirteenCard_GAME_PLAYER_H__

#include "cocos2d.h"
#include "HNNetExport.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include "CaiShenThirteenCardWrapper.h"
#include "HNLobbyExport.h"
namespace CaiShenThirteenCard
{
	class GamePlayer
	{

	public:
		enum class STATE_LABEL : BYTE
		{
			NONE,
			READY,
			CALL,
			UNCALL,
			SORT,
			ZPDONE
		};

		enum HeadType
		{
			HEAD1,
			HEAD2,
			HEAD3,
			HEAD4, 

			NONE
		};

	public:
		GamePlayer();
		virtual ~GamePlayer();

		//加载ui
		void doLoad(BYTE seatNo, Layout* Panel_Table);
		void doUnload();
		//玩家信息重置
		void doRestore();

		//设置帧动画图片信息
		void onRender();
		//创建人物帧动画
		Animation* addSpriteData(HeadType type);

		Node* getChatParent();

		ImageView* getUserHeadImg();

	public:
		//有效玩家
		bool isValid() const { return _userID != INVALID_USER_ID; }

		//属性接口
	public:
		void setName(const std::string& name);

		std::string& getName() { return _name; }

		void setGold(LLONG gold);

		LLONG getGold() const { return _gold; }

		void setHead(bool isBoy, int iUserID, int iUserHeadID);

		// 隐藏或者显示玩家
		void showOrHideUser(bool visible);

		// 设置玩家状态气泡
		void setStateLabel(STATE_LABEL state);

		// 显示庄家信息
		void setUserIsNT(bool isNT);

		// 显示房主信息
		void setUserIsMaster(bool isMaster);

		// 显示倒计时
		void showTimer(int dt, bool bShow, std::function<void()> callFunc = nullptr);

		//显示房主标识
		void showOwnerImage(bool show);

		std::function<void()> userMessageCallBack = nullptr;
		void playGiftAni(Vec2 startPos, Vec2 endPos, int giftNo); //播放礼物动画
		void createAni(std::string fileName, std::string imgName, int imgCount, Node* playNode, bool isNt = false); //创建动画
		void showKuangBtnAction(bool isPlay,bool isZhuang);//抢庄动画
		Vec2 getPlayerPos();
		//私有UI事件接口
	private:
		//计时器
		void timing(Node* node, int dt, std::function<void()> callFunc);

	private:
		Layout*		_panel_Player = nullptr;
		ImageView*	_ivNT         = nullptr; //庄家标志
		ImageView*	_iMaster      = nullptr; //房主志
		ImageView*	_nameBox      = nullptr; //名称币框
		Text*		_textName     = nullptr; //家昵称
		Text*		_taGold       = nullptr; //玩家币
		ImageView*	_stateImage   = nullptr; //玩家状态图标
		ImageView*	_clock        = nullptr; //时钟
		STATE_LABEL _stateLabel   = STATE_LABEL::NONE;
		GameUserHead* _userHead   = nullptr;
		Button* _userMessageBtn = nullptr;
		Button*		_BtnKuang = nullptr;	//庄家动画按钮
	private:
		LLONG _gold;
		std::string _name;
		std::string _head;
		int _headId;

	private:
		CC_SYNTHESIZE(LLONG, _userID, UserID);						//玩家ID
		CC_SYNTHESIZE(bool, _sex, UserSex);							//玩家性别
		CC_SYNTHESIZE(bool, _self, MySelf);							//是否自己
		CC_SYNTHESIZE(INT, _logicSeatNo, LogicSeatNo);				//逻辑座位号
		CC_SYNTHESIZE(INT, _viewSeatNo, ViewSeatNo);				//视图座位号
	};
}



#endif // !_GAME_PLAYER_
