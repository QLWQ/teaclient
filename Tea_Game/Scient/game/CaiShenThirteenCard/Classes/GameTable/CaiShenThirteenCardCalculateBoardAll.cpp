/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardCalculateBoardAll.h"
#include <string>
#include "HNLogicExport.h"
#include "HNOpenExport.h"
#include "HNLobby/GameChildLayer/GameShareLayer.h"


namespace CaiShenThirteenCard
{
	static const char* calculateBoardcsbFilePath = "Games/CaiShenThirteenCard/allBoard/allBoard.csb";
	static const char* USER_MEN_HEAD = "platform/head/men_head.png";
	static const char* USER_WOMEN_HEAD = "platform/head/women_head.png";

	CalculateBoardAll::CalculateBoardAll()
	{
		_deskPassword = "";
		/*_userHead = nullptr;
		_spriteHead = nullptr;*/
	}

	CalculateBoardAll::~CalculateBoardAll()
	{

	}

	bool CalculateBoardAll::init()
	{
		if (!HNCalculateBoard::init()) return false;

		Size winSize = Director::getInstance()->getWinSize();

		_calculateBoardNode = CSLoader::createNode(calculateBoardcsbFilePath);
		_calculateBoardNode->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		_calculateBoardNode->setScale(0);
		addChild(_calculateBoardNode, 1231233);
		_calculateBoardNode->runAction(ScaleTo::create(0.2f, 1.0f));
		_calculateBoardLayout = (Layout*)_calculateBoardNode->getChildByName("Panel_1");

		float scaleX = winSize.width / 1280;
		float scaleY = winSize.height / 720;
		_calculateBoardLayout->setScaleX(scaleX);
		_calculateBoardLayout->setScaleY(scaleY);

		//分享按钮 恐龙谷
		_btn_shareKongLong = (Button*)_calculateBoardLayout->getChildByName("btn_shareKongLong");
		if (_btn_shareKongLong)
		{
			_btn_shareKongLong->addTouchEventListener(CC_CALLBACK_2(HNCalculateBoard::shareBtnCallBack, this));
		}

		//分享按钮 微信
		_btn_shareWeiXin = (Button*)_calculateBoardLayout->getChildByName("btn_shareWeiXin");
		if (_btn_shareWeiXin)
		{
			_btn_shareWeiXin->addTouchEventListener(CC_CALLBACK_2(HNCalculateBoard::shareBtnCallBack, this));
		}

		//普通房间隐藏分享按钮
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			_btn_shareKongLong->setVisible(false);
			_btn_shareWeiXin->setVisible(false);
		}

		//返回按钮
		_btn_back = (Button*)_calculateBoardLayout->getChildByName("btn_back");
		if (_btn_back)
		{
			_btn_back->addTouchEventListener(CC_CALLBACK_2(HNCalculateBoard::backBtnCallBack, this));
		}

		this->setVisible(false);

		return true;
	}

	void CalculateBoardAll::backBtnCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::ENDED != type) return;
		
		//暂离房间,key为用户ID,value为密码
		char keyLeavingRoom[64] = { 0 };
		sprintf(keyLeavingRoom, "%d", PlatformLogic()->loginResult.dwUserID);
		UserDefault::getInstance()->setStringForKey(keyLeavingRoom, "");//此时可以设为暂离房间状态_
		UserDefault::getInstance()->flush();
		this->runAction(Sequence::create(DelayTime::create(0.5), CallFunc::create([](){
			RoomLogic()->close();
			GamePlatform::createPlatform();
		}), nullptr));
		
	}

	void CalculateBoardAll::shareBtnCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		
		std::string gameName = "王牌十三张";	
		Button* pShare = dynamic_cast<Button*>(pSender);
		std::string name = pShare->getName();
		if (!_isTouch) return;
		_isTouch = false;
		this->runAction(Sequence::create(DelayTime::create(3.f), CallFunc::create([=]()
		{
			_isTouch = true;
		}), nullptr));

		auto shareLayer = GameShareLayer::create();
		shareLayer->setName("shareLayer");
		shareLayer->SetShareInfo("", "", "");
		shareLayer->show();
		// 恐龙谷分享
		//UMengSocial::getInstance()->doShare("", "", "");
		//if (name.compare("btn_shareKongLong") == 0)
		//{
		//	//if (Operator::requestChannel("sysmodule", "isAppInstalled", "KLG").compare("true") == 0) 
		//	//{
		//	//	DinosaurSocial::getInstance()->inviteFriends(gameName, HNPlatformConfig()->getVipRoomNum(), true);
		//	//
		//	//	DinosaurSocial::getInstance()->onShareCallBack = [=](bool success, const std::string& errorMsg) {
		//	//		_isTouch = true;
		//	//	};
		//	//}
		//	//else {
		//	//	_isTouch = true;
		//	//	GamePromptLayer::create()->showPrompt(GBKToUtf8("请先安装恐龙谷"));
		//	//}			
		//}

		//// 微信分享
		//else if (name.compare("btn_shareWeiXin") == 0)
		//{
		//	if (Operator::requestChannel("sysmodule", "isAppInstalled", to_string(Platform::WEIXIN)).compare("true") == 0) 
		//	{
		//		UMengSocial::getInstance()->inviteFriends(gameName, HNPlatformConfig()->getVipRoomNum(), true, Platform::WEIXIN);

		//		UMengSocial::getInstance()->onShareCallBack = [=](bool success, int platform, const std::string& errorMsg) {
		//			_isTouch = true;
		//		};
		//	}
		//	else {
		//		_isTouch = true;
		//		GamePromptLayer::create()->showPrompt(GBKToUtf8("请先安装微信"));
		//	}			
		//}
		//UMengSocial::getInstance()->doShare("", "", "");
	}

	void CalculateBoardAll::showAndUpdateBoard(Node* parent, const CalculateBoard* boardData, const std::string& roomNumber, int ownerUserID)
	{
		_deskPassword = roomNumber;

		//显示结算
		showBoard(parent, 30000);

		char buffer[32] = { 0 };

		auto currentDeskNo = RoomLogic()->loginResult.pUserInfoStruct.bDeskNO;

		//更新房间号
		auto txt_roomNumber = dynamic_cast<Text*>(_calculateBoardLayout->getChildByName("txt_roomNumber"));
	//	sprintf(buffer, "房号：%s", roomNumber.c_str());
		std::string roomID = "";
		roomID.append("房号：");
		roomID.append(roomNumber);
		txt_roomNumber->setString(GBKToUtf8(roomID));

		int length = 0;
		int iShow[PLAY_COUNT] = { 0 };

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			sprintf(buffer, "layout_score%d", i);
			auto subLayout = dynamic_cast<Layout*>(_calculateBoardLayout->getChildByName(buffer));
			subLayout->setVisible(false);
			auto userInfo = UserInfoModule()->findUser(currentDeskNo, i);
			//判断玩家是否存在
			if (userInfo)
			{
				iShow[length] = i;
				length++;
			}
		}
		
		//根据玩家人数调整位置
		movePlayerLayout(length);

		for (int showIndex = 0; showIndex < length; ++showIndex)
		{
			auto userInfo = UserInfoModule()->findUser(currentDeskNo, iShow[showIndex]);
			sprintf(buffer, "layout_score%d", showIndex);
			auto subLayout = dynamic_cast<Layout*>(_calculateBoardLayout->getChildByName(buffer));
			subLayout->setVisible(true);

			if (!userInfo) continue;
			
			//更新玩家昵称
			auto txt_name = dynamic_cast<Text*>(subLayout->getChildByName("txt_name"));
			txt_name->setPositionX(230);
			txt_name->setSize(Size(210,40));
			txt_name->setString(GBKToUtf8(userInfo->nickName));
			if (userInfo->iVipLevel>0)			txt_name->setColor(Color3B(255,0,0));
			else  txt_name->setColor(Color3B(255, 255, 255));
			//更新玩家ID
			//auto txt_Id = dynamic_cast<Text*>(subLayout->getChildByName("txt_userID"));
			//txt_Id->setString(StringUtils::format("ID:%d", userInfo->dwUserID));
			// 更新房主图标
			auto image_fangzhu = dynamic_cast<ImageView*>(subLayout->getChildByName("image_fangzhu"));
			image_fangzhu->setLocalZOrder(10);
			bool isOwner = userInfo->dwUserID == ownerUserID;
			image_fangzhu->setVisible(isOwner);//isOwner
			
			//更新总成绩
			auto txt_allScore = dynamic_cast<Text*>(subLayout->getChildByName("txt_allScore"));
			txt_allScore->setString(to_string(boardData[iShow[showIndex]].i64WinMoney));
			txt_allScore->setPositionX(txt_allScore->getPositionX() + 20);

			//更新大赢家
			auto image_win = dynamic_cast<ImageView*>(subLayout->getChildByName("Image_dayingjia"));
			image_win->setVisible(boardData[iShow[showIndex]].bWinner);
			image_win->setLocalZOrder(image_win->getLocalZOrder() + 100);
			image_win->setVisible(false);

			//更新胜局
			auto txt_shengju = dynamic_cast<Text*>(subLayout->getChildByName("txt_shengju"));
			auto txt_shengju_score = dynamic_cast<Text*>(txt_shengju->getChildByName("txt_shengju_score"));
			txt_shengju_score->setString(to_string(boardData[iShow[showIndex]].iWinCount));

			//平局
			auto txt_pingju = dynamic_cast<Text*>(subLayout->getChildByName("txt_pingju"));
			auto txt_pingju_score = dynamic_cast<Text*>(txt_pingju->getChildByName("txt_pingju_score"));
			txt_pingju_score->setString(to_string(boardData[iShow[showIndex]].iDrawCount));

			//输局
			auto txt_shuju = dynamic_cast<Text*>(subLayout->getChildByName("txt_shuju"));
			auto txt_shuju_score = dynamic_cast<Text*>(txt_shuju->getChildByName("txt_shuju_score"));
			txt_shuju_score->setString(to_string(boardData[iShow[showIndex]].iLoseCount));

			//更新用户结算榜头像			
			auto img_frame = dynamic_cast<ImageView*>(subLayout->getChildByName("Image_frame"));			
			auto img_head = dynamic_cast<ImageView*>(subLayout->getChildByName("Image_head"));

			auto userHead = GameUserHead::create(img_head, img_frame);
			userHead->show();
			userHead->loadTexture(userInfo->bBoy ? USER_MEN_HEAD : USER_WOMEN_HEAD);
			userHead->loadTextureWithUrl(userInfo->headUrl);
		}
	}

	//根据玩家人数调整位置
	void CalculateBoardAll::movePlayerLayout(int palyNum)
	{
		char buffer[32] = { 0 };
		Layout* subLayout[6] = {};
		auto winSize = Director::getInstance()->getWinSize();
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			sprintf(buffer, "layout_score%d", i);
			subLayout[i] = dynamic_cast<Layout*>(_calculateBoardLayout->getChildByName(buffer));
		}
		if (palyNum == 2)
		{
			/*subLayout[0]->setPosition(subLayout[2]->getPosition());
			subLayout[1]->setPosition(subLayout[3]->getPosition());*/
		}
		if (palyNum == 3)
		{
		/*	subLayout[0]->setPositionX(subLayout[0]->getPositionX() + winSize.width*0.25f);
			subLayout[1]->setPositionX(subLayout[1]->getPositionX() + winSize.width*0.25f);
			subLayout[2]->setPositionX(subLayout[2]->getPositionX() + winSize.width*0.25f);*/
		}
		if (palyNum == 4)
		{
		/*	subLayout[0]->setPositionX(subLayout[0]->getPositionX() + winSize.width*0.166f);
			subLayout[1]->setPositionX(subLayout[1]->getPositionX() + winSize.width*0.166f);
			subLayout[2]->setPositionX(subLayout[2]->getPositionX() + winSize.width*0.166f);
			subLayout[3]->setPositionX(subLayout[3]->getPositionX() + winSize.width*0.166f);*/
		}
		if (palyNum == 5)
		{
		/*	subLayout[0]->setPositionX(subLayout[0]->getPositionX() + winSize.width*0.08f);
			subLayout[1]->setPositionX(subLayout[1]->getPositionX() + winSize.width*0.08f);
			subLayout[2]->setPositionX(subLayout[2]->getPositionX() + winSize.width*0.08f);
			subLayout[3]->setPositionX(subLayout[3]->getPositionX() + winSize.width*0.08f);
			subLayout[4]->setPositionX(subLayout[4]->getPositionX() + winSize.width*0.08f);*/
		}
	}
}

