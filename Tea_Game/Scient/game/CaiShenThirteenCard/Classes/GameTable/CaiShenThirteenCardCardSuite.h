/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_CARDSUITE_H__
#define __CaiShenThirteenCard_CARDSUITE_H__

#include "CaiShenThirteenCardMessageHead.h"
#include "CaiShenThirteenCardCardSprite.h"


namespace CaiShenThirteenCard
{
	class CardSuite : public Layer
	{
	public:
		static CardSuite* create(float scale);

		void shuffle(float duration, float radius = 350.0f, float alpha = 30.0f, float offsetY = 350.0f);
		void dealCard(float duration, Vec2 target, std::function<void()> callback);
		TargetedAction* dealCard(float duration, Vec2 target, float dstAngle = 180, float fScale = 1.0);
		void restoreCardSize(float fScale);
		void recycle(float duration);
		void runActionPair(Node* node, Action* action);

	private:
		enum
		{
			TOTAL_CARDS = 78
		};

		typedef struct ActionPair
		{
			Node*	node;
			Action*	action;
		}ActionPair;

		bool init(float scale);
		void run(float delta);

		CardSuite(void);
		~CardSuite(void);

	private:
		CardSprite*	_suite[TOTAL_CARDS];
		bool		_empty;
		int			_totalCards;
		float		_scale;
		BYTE		_sent;
		std::list<ActionPair> _queue;

	};

}

#endif