/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardWrapper.h"

namespace CaiShenThirteenCard
{
	bool IWrapper::doLoad(Node* component)
	{
		CCAssert(nullptr != component, "nullptr == _component");
		if (nullptr == component) return false;

		_component = component;

		load();

		return true;
	}

	void IWrapper::doUnload()
	{
		unload();
		_component = nullptr;
	}

	void IWrapper::doRestore()
	{
		restore();
	}

	void IWrapper::setVisible(bool visible)
	{
		if (_visible != visible)
		{
			_visible = visible;
			_component->setVisible(_visible);
		}
	}

	bool IWrapper::isVisible() const
	{
		return _visible;
	}

	int IWrapper::getLocalZOrder() const
	{
		return _component->getLocalZOrder();
	}

	const Size& IWrapper::getContentSize() const
	{
		return _component->getContentSize();
	}

	void IWrapper::setContentSize(const Size& contentSize)
	{
		_component->setContentSize(contentSize);
	}

	const Vec2& IWrapper::getPosition() const
	{
		return _component->getPosition();
	}

	void IWrapper::getPosition(float* x, float* y) const
	{
		_component->getPosition(x, y);
	}

	void IWrapper::setPosition(float x, float y)
	{
		_component->setPosition(x, y);
	}
		
	void IWrapper::setPosition(const Vec2 &position)
	{
		_component->setPosition(position);
	}

	void IWrapper::setTag(int tag)
	{
		_component->setTag(tag);
	}
	
	int IWrapper::getTag() const
	{
		return _component->getTag();
	}

	void IWrapper::setName(const std::string& name)
	{
		_component->setName(name);
	}

	std::string IWrapper::getName()
	{
		return _component->getName();
	}

	void IWrapper::addChild(Node * child)
	{
		_component->addChild(child);
	}

	void IWrapper::addChild(Node * child, int localZOrder)
	{
		_component->addChild(child, localZOrder);
	}

	void IWrapper::addChild(Node* child, int localZOrder, int tag)
	{
		_component->addChild(child, localZOrder, tag);
	}

	void IWrapper::addChild(Node* child, int localZOrder, const std::string &name)
	{
		_component->addChild(child, localZOrder, name);
	}

	void IWrapper::removeChild(Node* child, bool cleanup)
	{
		_component->removeChild(child, cleanup);
	}

	void IWrapper::removeChild(int tag, bool cleanup)
	{
		_component->removeChildByTag(tag, cleanup);
	}

	void IWrapper::removeChild(const std::string &name, bool cleanup)
	{
		_component->removeChildByName(name, cleanup);
	}
}
