/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardGameLoading.h"
#include "CaiShenThirteenCardGameTableUI.h"

namespace CaiShenThirteenCard
{
	static const char*	LAYER_BG				=	"CaiShenThirteenCard/GameTableUi/res/h_startpage_bk.png";
	static const char*	CARDS_PLIST_PATH		=	"CaiShenThirteenCard/GamePlist/cards.plist";
	static const char*	CARDS_IMAGE_PATH		=	"CaiShenThirteenCard/GamePlist/cards.png";
	static const char*	GAMETYPE_PLIST_PATH		=	"CaiShenThirteenCard/GamePlist/gameType.plist";
	static const char*	GAMETYPE_IMAGE_PATH		=	"CaiShenThirteenCard/GamePlist/gameType.png";
	static const char*	CARTOON_PLIST_PATH		=	"CaiShenThirteenCard/GamePlist/h_cartoon.plist";
	static const char*	CARTOON_IMAGE_PATH		=	"CaiShenThirteenCard/GamePlist/h_cartoon.png";
	static const char*	LOADING_PLIST_PATH		=	"CaiShenThirteenCard/GamePlist/loading.plist";
	static const char*	LOADING_IMAGE_PATH		=	"CaiShenThirteenCard/GamePlist/loading.png";
	static const char*	SETTING_PLIST_PATH		=	"CaiShenThirteenCard/GamePlist/Plist_SettingUi.plist";
	static const char*	SETTING_IMAGE_PATH		=	"CaiShenThirteenCard/GamePlist/Plist_SettingUi.png";
	static const char*	TABLE_PLIST_PATH		=	"CaiShenThirteenCard/GamePlist/Plist_TableUi.plist";
	static const char*	TABLE_IMAGE_PATH		=	"CaiShenThirteenCard/GamePlist/Plist_TableUi.png";


	NewThirteenCardGameLoading::NewThirteenCardGameLoading()
		: fileNum (0)
	{
	}

	NewThirteenCardGameLoading::~NewThirteenCardGameLoading()
	{
	}

	NewThirteenCardGameLoading* NewThirteenCardGameLoading::create()
	{
		NewThirteenCardGameLoading* pRet = new NewThirteenCardGameLoading();
		if (nullptr != pRet && pRet->init())
		{
			pRet->autorelease();
			return pRet;
		}
		CC_SAFE_DELETE(pRet);
		return nullptr;
	}

	bool NewThirteenCardGameLoading::init()
	{
		if (!HNLayer::init()) return false;

		setBackGroundImage(LAYER_BG);
		auto winSize = Director::getInstance()->getWinSize();

		SpriteFrameCache::getInstance()->addSpriteFramesWithFile(LOADING_PLIST_PATH);

		//loading¶¯»­
		char name[32];

		auto processAnimation = Animation::create();
		processAnimation->setDelayPerUnit(0.2f);
		processAnimation->setLoops(-1);
		for (int i = 1; i <= 4; i++)
		{
			sprintf(name, "h_startpage_process_%d.png", i);
			processAnimation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(name));
		}
		auto  animate2 = Animate::create(processAnimation);
		auto processSp = Sprite::createWithSpriteFrameName(name);
		processSp->setPosition(Vec2(winSize.width / 2, winSize.height * 0.5f));
		processSp->runAction(animate2);
		addChild(processSp);

		Director::getInstance()->getTextureCache()->addImageAsync(CARDS_IMAGE_PATH, CC_CALLBACK_1(NewThirteenCardGameLoading::loadingTextureCallback, this, CARDS_PLIST_PATH));
		Director::getInstance()->getTextureCache()->addImageAsync(GAMETYPE_IMAGE_PATH, CC_CALLBACK_1(NewThirteenCardGameLoading::loadingTextureCallback, this, GAMETYPE_PLIST_PATH));
		Director::getInstance()->getTextureCache()->addImageAsync(CARTOON_IMAGE_PATH, CC_CALLBACK_1(NewThirteenCardGameLoading::loadingTextureCallback, this, CARTOON_PLIST_PATH));
		Director::getInstance()->getTextureCache()->addImageAsync(SETTING_IMAGE_PATH, CC_CALLBACK_1(NewThirteenCardGameLoading::loadingTextureCallback, this, SETTING_PLIST_PATH));
		Director::getInstance()->getTextureCache()->addImageAsync(TABLE_IMAGE_PATH, CC_CALLBACK_1(NewThirteenCardGameLoading::loadingTextureCallback, this, TABLE_PLIST_PATH));
		//onCloseCallBack();
		return true;
	}

	void NewThirteenCardGameLoading::loadingTextureCallback(Texture2D* textureData, std::string plist)
	{
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plist , textureData);

		loadingCallback(0.01f);
	}

	void NewThirteenCardGameLoading::loadingCallback(float dt)
	{
		fileNum++;
		if (fileNum >= 0.1)
		{
			this->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=](){
				if (nullptr != onCloseCallBack)
				{
					onCloseCallBack();
				}
				this->removeFromParent();
			}), nullptr));
		}
	}	
}