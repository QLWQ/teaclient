/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_LOADING_LAYER_H__
#define __CaiShenThirteenCard_LOADING_LAYER_H__

#include "HNNetExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"

USING_NS_CC;

namespace CaiShenThirteenCard
{
	class NewThirteenCardGameLoading : public HNLayer
	{
		BYTE  fileNum;

	public:
		typedef std::function<void ()> CloseCallBack;
		CloseCallBack  onCloseCallBack;
	public:
		static NewThirteenCardGameLoading* create();
		void loadingTextureCallback(Texture2D* textureData, std::string plist);
		void loadingCallback(float dt);
		
	protected:
		NewThirteenCardGameLoading();
		virtual ~NewThirteenCardGameLoading();

	private:
		bool init();
		
	};
}
#endif // __CaiShenThirteenCard_LOADING_LAYER_H__
