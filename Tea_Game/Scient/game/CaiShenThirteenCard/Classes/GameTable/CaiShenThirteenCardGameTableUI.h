/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_Game_Table_UI_H__
#define __CaiShenThirteenCard_Game_Table_UI_H__

#include <array>
#include <memory>
#include "cocos2d.h"
#include "HNLobbyExport.h"
#include "HNNetExport.h"
#include "CaiShenThirteenCardGameTableUICallback.h"
#include "CaiShenThirteenCardGamePlayer.h"
#include "CaiShenThirteenCardHandCard.h"
#include "CaiShenThirteenCardGameToolbar.h"
#include "CaiShenThirteenCardControllerLayer.h"
#include "CaiShenThirteenCardSettlementLayer.h"
#include "CaiShenThirteenCardCalculateBoardAll.h"
#include <spine/spine-cocos2dx.h>
#include "spine/spine.h"
#include <deque>
using namespace spine;
USING_NS_CC;

namespace CaiShenThirteenCard
{
	class GameTableLogic;
	class CardSuite;
	class GameTableUI : public HN::HNGameUIBase, public IGameTableUICallback
	{
	private:
		GameTableLogic*			_logic;
		Layout*					_tableUi;
		gameToolbar				_toolBar;
		ControllerLayer			_controller;

		GamePlayer				_players[PLAY_COUNT];
		NewThirteenCardHandCard	_handCard[PLAY_COUNT];

		ui::Button* _btnReady;
		ui::Button* _btnChangeTable;
		CardSuite*	_suite;

	public:
		static HNGameUIBase* create(BYTE bDeskIndex, bool bAutoCreate);

	protected:
		GameTableUI();
		virtual ~GameTableUI();

	public:
		bool init(BYTE bDeskIndex, bool bAutoCreate);
		virtual void onEnter() override;
		virtual void onExit() override;
	
		void initGameUI(BYTE bDeskIndex, bool bAutoCreate);
		void initToolBarLayer();
		void removeSetLayer();
		virtual void	setBottomScore(int Score);
		virtual void onVipTip(BYTE lSeatNo) ;

	protected:
		void onOutCardClick(ControllerLayer* controller);

		void onReadyClick(cocos2d::Ref *pSender);
		void onQiangzhuangClick(cocos2d::Ref *pSender);
		void onBuqiangClick(cocos2d::Ref *pSender);

		void onChatClick(Ref* pSender);

	protected:
		void onSettlementTimer();

		// 逻辑接口
	protected:
		virtual void onAddPlayer(BYTE lSeatNo, LLONG userID, bool self) override;
		virtual void onRemovePlayer(BYTE lSeatNo, LLONG userID, bool self) override;
		//设置游戏基础倍率
		virtual void onSetGameBasePoint(INT point) override;

	private:
		bool isValidSeat(BYTE seatNo);
		
	protected:
		//空闲阶段
		virtual void onGSFree(const bool userReady[PLAY_COUNT]) override;
		//叫庄阶段
		virtual void onGSRobNt(BYTE currRobUser) override;
		//发牌阶段
		virtual void onGSSendCard(const std::vector<BYTE>& cards) override;
		//开牌阶段
		virtual void onGSOpenCard(const std::vector<BYTE>& cards, const BYTE heapCard[3][5]) override;
		//比牌阶段
		virtual void onGSCompareCard(const BYTE heapCard[PLAY_COUNT][3][5]) override;

	protected:
		// 玩家准备
		virtual void onGameAgree(BYTE lSeatNo, bool self) override;
		// 通知抢庄
		virtual void onGameNoticeRobNt(BYTE lSeatNo, bool self) override;
		// 通知抢庄结果
		virtual void onGameRobNtResult(BYTE lSeatNo, bool robResult, bool self) override;
		// 确定庄家位置
		virtual void onGameMakeSureNt(BYTE lSeatNo, bool self, int QZSeatNO[]) override;
		// 发牌
		virtual void onGameSendCard(const std::vector<BYTE>& cards) override;
		// 一键搓牌
		virtual void onGameSetAutoRubCardType(BYTE byHeapCard[3][5], int iHeapShape[3]) override;
		// 通知开牌
		virtual void onGameNoticeOpenCard() override;
		// 开牌结果
		virtual void onGameOpenCardResult(BYTE lSeatNo) override;
		// 比牌
		virtual void onGameCompareCard(const BYTE heapCard[PLAY_COUNT][3][5]) override;
		// 开火
		virtual void onGameOpenFire(const bool userFireUser[PLAY_COUNT][PLAY_COUNT]) override;
		//被打枪音效		
		virtual void onGameBeOpenFireEffect() override;
		// 全垒打
		virtual void onGameFireAll(BYTE lSeatNo) override;
		//全垒打音效
		virtual void onGameFireAllEffect() override;
		//打枪提示音
		virtual void tipsOpenFireEffect(bool sex) override;
		//全垒打提示音
		virtual void tipsAllOpenFireEffect(bool sex)override;
		// 游戏结算
		virtual void onGameResult(const S_C_GameResult* pObject) override;

		virtual void showGameRule(bool bHaveCaiShen, bool bSiHua, int point, BYTE playerNum, BYTE playMode, bool tongSha, bool jiaLiangMen) override;

		virtual void showMyCard(const BYTE heapCard[3][5])override;

		virtual void showUserCut(BYTE lseat)override;

		virtual void setPlayerPos() override;

		virtual void sendRecordData(S_C_GameRecordResult* data);
		virtual void sendSSSRecordData(S_C_Game_SSS_RecordResult* data);
		virtual void onCheatStatus(S_C_CheatStatus* data);
		//赠送礼物回复
		//virtual void dealGift(TMSG_SENG_GIFT_REQ data)override;
		virtual void setAllFire(bool isAllFire);//是否全垒打
		// 加载UI
	protected:
		void doLoad();
		void doCompareCard(const BYTE heapCard[PLAY_COUNT][3][5], BYTE time);
		void compareCard(const BYTE heapCard[PLAY_COUNT][3][5], float litghtCardTime);
		void doShowScore(int index);
		void doLightCard(int index);
		void playFireAnimation();

		void createCheatSwitchs();
		void clearCheatSwitchs();
		void CheckBoxCheatEvent(Ref* pSender, CheckBox::EventType type);
		bool m_bCheatShow; //是否已经打开作弊开关
	private:
		void restoreChildUI();
		void sendCardAction(const std::vector<BYTE>& cards);
		void sendCardActionFinsh(Ref* pSender);//发牌动作完毕
		void sendGift(BYTE senderSeatNo, BYTE receiverSeatNo, int giftNo);  //赠送礼物
	
	public:
		GameChatLayer*		_chatLayer = nullptr;

	private:
		virtual void setDeskInfo();

		//聊天界面
		void gameChatLayer();

		virtual void onChatTextMsg(BYTE seatNo, std::string msg);										//处理文本聊天消息

		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime);							//处理语音聊天消息
		//显示小结算榜
		//virtual void showSingleCalculateBoard(GameEndStruct* endData) override;
		//显示大结算榜
		virtual void showCalculateBoard(const CalculateBoard* boardData) override;
		

		void returnButtonClickCallBack();
		void beginButtonClickCallBack(Ref* ref, Widget::TouchEventType type);

		bool	_isTouchEnabled = true;									// 按钮是否可点击
		bool	_exitGame = false;										// 退出游戏
		bool	_isNowSendCard = false;									// 是否正在发牌
		bool	_isNowCompareCard = false;								// 是否正在比牌

		Button* _btn_gameVoice;											// 语音按钮
		Button* _btn_gameChat;											// 文字聊天按钮Button_Chat
		Button* _btn_toolBarExit;										// 下拉退出按钮
		Button* _btn_qiangzhaung;										// 抢庄按钮
		Button* _btn_buqiang;											// 不抢按钮
		Button* _btn_record;											//战绩按钮
		Button* _btn_close;												//关闭战绩
		SkeletonAnimation* _gameShoot = nullptr;						//打枪或全垒打骨骼动画
		LayerColor* _colorLayer = nullptr;
		deque<BYTE> _fireSeatNo;
		deque<BYTE> _beFireSeatNo;
		SettlementLayer*   _settlement = nullptr;
		CalculateBoardAll* _calculateBoard = nullptr;
		VipRoomController* roomController = nullptr;
		BYTE               _deskNo = 255;
		bool				_allRoundsEnd1 = false;
		bool				_isfire[6];		//是否打过枪
		bool				_isAllFire = false;		//是否全垒打
		Sprite*         _BottonScore;

		int _index = 0;

		int _cheatStatus;  //作弊选项
	};
}


#endif
