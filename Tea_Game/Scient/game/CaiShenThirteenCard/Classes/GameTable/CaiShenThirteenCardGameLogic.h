/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_GameLogic_h__
#define __CaiShenThirteenCard_GameLogic_h__

#include "HNNetExport.h"

#include <vector>

namespace CaiShenThirteenCard
{

	enum cardShape
	{
		SHAPE_HIGH_CARD = 1,			//高牌
		SHAPE_DOUBLE_YI = 2,			//一对
		SHAPE_DOUBLE_LIANG = 3,			//两对
		SHAPE_STRIP_SAN = 4,			//三条
		SHAPE_STRAIGHT = 5,				//顺子
		SHAPE_FLOWER = 6,				//同花
		SHAPE_THREE_DOUBLE = 7,			//葫芦
		SHAPE_STRIP_SI = 8,				//炸弹
		SHAPE_STRAIGHT_FLUSH = 9,		//同花顺
		SHAPE_STRIP_FIVE = 10           //五同
	};

	//操作掩码
#define	UG_HUA_MASK					0xF0			//1111 0000
#define	UG_VALUE_MASK				0x0F			//0000 1111

	//扑克花色
#define UG_FANG_KUAI				0x00			//方块	0000 0000
#define UG_MEI_HUA					0x10			//梅花	0001 0000
#define UG_HONG_TAO					0x20			//红桃	0010 0000
#define UG_HEI_TAO					0x30			//黑桃	0011 0000
#define UG_NT_CARD					0x40			//主牌	0100 0000
#define UG_ERROR_HUA				0xF0			//错误  1111 0000

	//扑克出牌类型
#define UG_ERROR_KIND							0				//错误

#define UG_ONLY_ONE								1				//单张
#define UG_DOUBLE								2				//对牌

#define UG_VARIATION_STRAIGHT					3				//变种顺子(A2345)顺子中最小
#define UG_STRAIGHT								4               //顺子,5+张连续牌
#define UG_FLUSH								5				//同花(非连)
#define UG_STRAIGHT_FLUSH						6               //同花顺,花色相同的顺子

#define UG_THREE								7				//三张
#define UG_THREE_ONE							8               //3 带 1
#define UG_THREE_TWO							9               //3 带 2
#define UG_THREE_DOUBLE							10				//3 带1对

#define UG_VARIATION_DOUBLE_SEQUENCE			11				//变种双顺(AA22)最小
#define UG_DOUBLE_SEQUENCE						12				//连对,2+个连续的对子

#define UG_VARIATION_THREE_SEQUENCE				13				//变种三顺(AAA222最小)
#define UG_THREE_SEQUENCE						14				//连三张，2+个连续的三张

#define UG_VARIATION_THREE_ONE_SEQUENCE			15				//变种三顺带一
#define UG_THREE_ONE_SEQUENCE					16              //2+个连续的三带一

#define UG_VARIATION_THREE_TWO_SEQUENCE			17				//变种三顺带二
#define UG_THREE_TWO_SEQUENCE					18				//2+个连续的三带二

#define UG_VARIATION_THREE_DOUBLE_SEQUENCE		19				//变种三连张带对
#define UG_THREE_DOUBLE_SEQUENCE				20				//三连张带对

#define UG_VARIATION_THREE_SEQUENCE_DOUBLE_SEQUENCE		21		//变种蝴蝶(三顺带二顺)
#define UG_THREE_SEQUENCE_DOUBLE_SEQUENCE		22				//蝴蝶(三顺带二顺)

#define UG_FOUR_ONE								23				//四带一
#define UG_FOUR_TWO								24				//四带二张
#define UG_FOUR_ONE_DOUBLE						25				//四带一对
#define UG_FOUR_TWO_DOUBLE						26				//四带二对

#define UG_VARIATION_FOUR_SEQUENCE				27				//四顺
#define UG_FOUR_SEQUENCE						28				//四顺

#define UG_VARIATION_FOUR_ONE_SEQUENCE			29				//四带一顺
#define UG_FOUR_ONE_SEQUENCE					30				//四带一顺

#define UG_VARIATION_FOUR_TWO_SEQUENCE			31				//四带二顺
#define UG_FOUR_TWO_SEQUENCE					32				//四带二顺

#define UG_VARIATION_FOUR_ONE_DOUBLE_SEQUENCE	33				//四带对顺
#define UG_FOUR_ONE_DOUBLE_SEQUENCE				34				//四带对顺

#define UG_VARIATION_FOUR_TWO_DOUBLE_SEQUENCE	35				//四带二对顺
#define UG_FOUR_TWO_DOUBLE_SEQUENCE				36				//四带二对顺


#define UG_SLAVE_510K							37              //510K炸弹,花色不同
#define UG_MASTER_510K							38              //510K同花炸弹

#define UG_BOMB									39              //炸弹>=4張
#define UG_BOMB_SAME_HUA						40				//同花炸弹(在四副或以上的牌中出现)
#define UG_KING_BOMB							41				//王炸(最大炸弹)


#define KING_COUNT								2				//所有王的个数

	//510K逻辑类 支持 2 副扑克）
	class CUpGradeGameLogic
	{
		//变量定义
	private:
		//	int				m_iStation[5];	   //相对位置（方块，梅花，红桃，黑桃，主牌）
		BYTE			m_bSortCardStyle;  //排序方式
		int				m_iCondition;			//限制条件
		bool			m_bKingCanReplace;		//王是否可当

		UINT           m_iCardShape;	//支持牌型
		//函数定义
	public:
		//构造函数		
		CUpGradeGameLogic(void);
		//析构函数
		virtual ~CUpGradeGameLogic();

		//功能函数（公共函数）
	public:
		//[设置相关]
		//获取扑克数字
		inline int GetCardNum(BYTE iCard) { return (iCard&UG_VALUE_MASK) + 1; }
		//获取扑克花色(默认为真实花色)
		BYTE GetCardHuaKind(BYTE iCard, bool bTrueHua = true);
		//获取扑克相对大小(默认为牌大小,非排序大小)
		int GetCardBulk(BYTE iCard, bool bExtVal = false);
		//設置王可以當牌
		void SetKingCanReplace(bool bKingCanReplace = false){ m_bKingCanReplace = bKingCanReplace; }
		//獵取王是否可以當牌
		bool GetKingCanReplace(){ return m_bKingCanReplace; }
		//设置排序方式
		void SetSortCardStyle(BYTE SortCardStyle){ m_bSortCardStyle = SortCardStyle; }
		//获取排序方式
		BYTE GetSortCardStyle(){ return m_bSortCardStyle; }

		//[排序]
	public:
		//排列扑克,按大小(保留系统序例)
		bool SortCard(BYTE iCardList[], BYTE bUp[], BYTE iCardCount, bool bSysSort = false);
		//反转牌顺(从低->高)
		bool ReverseCard(BYTE iCardList[], BYTE bUp[], BYTE iCardCount);
		//按牌型排序
		bool SortCardByStyle(BYTE iCardList[], BYTE iCardCount);
		//按花色排序
		bool SortCardByKind(BYTE iCardList[], BYTE iCardCount);
		//设置排序方式
		void SetSortKind(BYTE bSortCardStyle);
	public:
		//删除扑克
		int RemoveCard(BYTE iRemoveCard[], int iRemoveCount, BYTE iCardList[], int iCardCount);
		//增加扑克牌数据
		int	AddCardToList(BYTE byAddCard[], int &iAddCount, BYTE iCardList[], int iCardCount);
	private:
		//清除 0 位扑克
		int RemoveNummCard(BYTE iCardList[], int iCardCount);

	public://[辅助函数]
		//对比单牌
		bool CompareOnlyOne(BYTE iFirstCard, BYTE iNextCard);
		//是否为同一数字牌
		bool IsSameNumCard(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//查找 >=4 炸弹的数量炸弹基数
		BYTE GetBombCount(BYTE iCardList[], int iCardCount, int iNumCount = 4, bool bExtVal = false);
		//获取指定大小牌个数
		BYTE GetCountBySpecifyNumCount(BYTE iCardList[], int iCardCount, int Num);
		//获取指定牌个数
		BYTE GetCountBySpecifyCard(BYTE iCardList[], int iCardCount, BYTE bCard);
		//获取指定牌张数牌大小(队例中只能够有一种牌的张数为iCount,不然传出去的将是第一个指定张数的值)
		BYTE GetBulkBySpecifyCardCount(BYTE iCardList[], int iCardCount, int iCount);
		//是否为某指定的顺子(变种顺子)
		bool IsVariationSequence(BYTE iCardList[], int iCardCount, int iCount);
		//是否为某指定的顺子
		bool IsSequence(BYTE iCardList[], int iCardCount);
		//提取指定的牌
		BYTE TackOutBySpecifyCard(BYTE iCardList[], int iCardCount, BYTE bCardBuffer[], int &iResultCardCount, BYTE bCard);
		//提取所有符合条件的牌,单张,对牌,三张,4炸弹牌型
		int TackOutBySepcifyCardNumCount(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], BYTE bCardNum, bool bExtVal = false);
		//提取指定花色牌
		int TackOutByCardKind(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], BYTE iCardKind);
		//拆出(将手中牌多的拆成少的)
		int TackOutMuchToFew(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], int &iBufferCardCount, BYTE iCardMuch, BYTE iCardFew);
		//查找大于iCard的单牌所在iCardList中的序号
		BYTE GetSerialByMoreThanSpecifyCard(BYTE iCardList[], int iCardCount, BYTE iCard, BYTE iBaseCardCount, bool bExtValue = false);
		//查找==iCard的单牌所在iCardList中的序号(起始位置,到終點位置)
		int GetSerialBySpecifyCard(BYTE iCardList[], int iStart, int iCardCount, BYTE iCard);
		//获取指定顺子中牌点最小值(iSequence 代表顺子的牌数最多为
		BYTE GetBulkBySpecifySequence(BYTE iCardList[], int iCardCount, int iSequence = 3);
		//获取指定顺子中牌点最大值变种顺子
		BYTE GetBulkBySpecifyVariationSequence(BYTE iCardList[], int iCardCount, int iSequence = 3);
		//查找最小 (1) or 最大 (255) 牌
		int	GetBulkBySepcifyMinOrMax(BYTE iCardList[], int iCardCount, int MinOrMax, bool bExtVal = false);
		////////////////////////////////////////////////////////////////////////////////////////////////////
		//[牌型相关]
	public:
		//获取牌型
		BYTE GetCardShape(BYTE iCardList[], int iCardCount, bool bExlVal = false);
		//是否单牌
		inline bool IsOnlyOne(BYTE iCardList[], int iCardCount) { return iCardCount == 1; };
		//是否对牌
		bool IsDouble(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//3 带 1or2(带一对带二单张或带一单张
		bool IsThreeX(BYTE iCardList[], int iCardCount, int iX/*1or2*/, bool bExtVal = false);
		//王炸
		bool IsKingBomb(BYTE iCardList[], int iCardCount);
		//4+张牌 炸弹
		bool IsBomb(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//同花炸弹
		bool IsBombSameHua(BYTE iCardList[], int iCardCount);
		//同花(非顺子)
		bool IsFlush(BYTE iCardList[], int iCardCount);
		//是否是同花顺
		bool IsStraightFlush(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//单甩
		bool IsStraight(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//对甩 //连对?
		bool IsDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal = false);

		//是否是连续的三带X(0,1,2,3)
		bool IsThreeXSequence(BYTE iCardList[], int iCardCount, int iSeqX/*0,1or2*/, bool bExtVal = false);
		//是否三顺带二顺(蝴蝶)
		bool IsThreeSequenceDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//四带一或者四带二
		bool IsFourX(BYTE iCardList[], int iCardCount, int iX/*1or 2*/);//单张1,任意张2,一对子,2对4
		//四带一或者四带二的顺子
		bool IsFourXSequence(BYTE iCardList[], int iCardCount, int iSeqX);
		//[出牌相关]
public:
		//提取单个的三带1 or 2or 3(单,一对,或二单张)
		BYTE TackOutThreeX(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int  xValue);
		//提取2个以上连续的三带1,2
		bool TrackOut3XSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int xValue);
		//提取2个以上连续的三带1,2
		bool TrackOut3Sequence2Sequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount);
		//提取2个以上连续的三带1,2
		//BYTE TrackOut3XSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, bool bExtVal=false);
		//提取单张的顺子,连对 or 连三
		//BYTE TackOutSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount);
		//获取顺子中最小位置值(xSequence表示默认单顺)
		int GetSequenceStartPostion(BYTE iCardList[], int iCardCount, int xSequence = 1);
		//提取单张的顺子,连对顺子,连三顺子
		bool TackOutSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int xSequence, bool bNoComp = false);
		//提取同花顺
		bool TackOutStraightFlush(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount);
		//BYTE TackOutStraightFlush(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int bExtVal=false);
		//提取所的炸弹
		bool TackOutAllBomb(BYTE iCardList[], int iCardCount,
			BYTE iResultCard[], int &iResultCardCount, int iNumCount = 4);
		//提取炸弹(张数默认为4)
		bool TackOutBomb(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, int iNumCount = 4);
		//提取王炸
		bool TackOutKingBomb(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount);
	public:

		//查找牌型中有几个三张的牌
		//int SearchCountOfThree(BYTE iCardList[],int iCardCount,bool bExtVal=false);
		//查找三张的牌点
		//int SearchThreeCard(BYTE iCardList[],int iCardCount,bool bExtVal=false);
		//查找三连牌中最小的那个三张的牌点
		int SearchMinThreeSeq(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//重置限制条件
		void ResetCondition();

		///设置游戏牌型
		void SetCardShape(UINT iCardShape){ m_iCardShape = iCardShape; }

		///查找最小 (1) or 最大 (255) 牌值
		///
		/// [@param in bExtVal] 真，不考虑2、王
		BYTE GetCardMinOrMax(BYTE iCardList[], int iCardCount, int MinOrMax, bool bExtVal = false);
	public:
		//获取指定牌中的对子个数
		BYTE GetDoubleNums(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//获取指定牌中的相同牌的最大个数
		int GetSameCardMaxNums(BYTE iCardList[], int iCardCount);
		//获得牌型
		int GetCardStyle(BYTE iCardList[], int iCardCount);
		///比较两手中牌的大小
		int CompareCard(BYTE wFirstCard[], int iFirstCount, BYTE wSecondCard[], int iSecondCount);
		///得到手牌中最大的牌(含花色)
		BYTE GetMaxCard(BYTE  wCardList[], int iCardCount);
		//验证游戏中数据是否有效
		bool TestData(int iData, int iStyle);
		//计算有效牌个数
		int CountUserCard(BYTE bCardList[], int iCount);
		//从指定数组中删除一张牌
		void DeleteACard(BYTE bCardList[], int iCount, BYTE bDeleteCard);
	public:
		//查找同花顺
		bool FindSameFlowerFlush(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);
		//查找四条
		bool FindFourStrip(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard = false);
		//查找葫芦
		bool FindCalabash(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);
		//查找同花
		bool FindSameFlower(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);
		//查找顺子
		bool FindStraight(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);
		//查找三条
		bool FindThreeStrip(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard = false);
		//查找两对
		bool FindSecondDouble(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard = false);
		//查找一对
		bool FindOneDouble(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard = false);
		//查找五同
		bool FindFiveStrip(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount, bool isNeedFillCard = false);

		//查找所有对子
		bool FindAllDouble(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找所有两对
		bool FindAllTwoDouble(BYTE HandCard[], int HandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找所有三张
		bool FindAllThreeStrip(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找所有葫芦
		bool FindAllCalabash(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找所有四张
		bool FindAllFourStrip(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找所有同花
		bool FindAllSameFlower(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找所有顺子
		bool FindAllStraight(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount, bool needSameFlower = false);
		//查找所有同花顺
		bool FindAllSameFlowerFlush(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找所有五同
		bool FindAllFiveStrip(BYTE iHandCard[], int iHandCardCount, std::vector<BYTE>&allResultCard, int & iResultCardCount);
		//查找牌值最大的高牌
		bool FindMaxHighCard(BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int & iResultCardCount);

	public:
		//获取一对中最大的单牌
		BYTE GetMaxSingleCards(BYTE iHandCard[], int iHandCardCount);
		//排序
		bool SortCardData(BYTE byCard[], int iCardCount);
		//同花色	
		bool IsSameHuaKind(BYTE byCardList[], int iCardCount);
		//最小顺子
		bool IsSmallStraight(BYTE byCardList[], int iCardCount);
		//最小同花顺
		bool IsStraightSmallFlush(BYTE byCardList[], int iCardCount);
		//同花顺,花色相同的顺子
		int CmpStraightFlush(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//取得牌最大的牌值(牌已经排序)
		BYTE GetMaxCardValue(BYTE byCardList[], int iCardCount);
		//取得最小单牌的牌
		BYTE GetSingleMinData(BYTE byCardList[], int iCardCount);
		//取得最大单牌的牌
		BYTE GetSingleMaxData(BYTE byCardList[], int iCardCount);
		//同花最小顺子
		int CmpSmallStraightFlush(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//炸弹
		int CmpBomb(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//三带二
		int CmpThreeDouble(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//同花
		int CmpFlush(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//顺子
		int CmpStraight(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//最小顺子
		int CmpSmallStraight(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//三张
		int CmpThree(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//两对
		int CmpTwoDouble(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//一对
		int CmpDouble(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//比单张
		int CmpOne(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//比五同
		int CmpFive(BYTE byFirstCard[], int iFirstCount, BYTE bySecondCard[], int iSecondCount);
		//取得几张相同牌的牌值
		int  GetSameCardValue(BYTE byCardList[], int iCardCount, int iSameCardNum);
		//取得几张相同的牌值同时把牌删了
		int  GetSameCardValueAndClearThisCardData(BYTE byCardList[], int iCardCount, int iSameCardNum);
		//三条
		bool IsSameThree(BYTE byCardList[], int iCardCount);
		//两对
		bool IsTwoDoubleNums(BYTE byCardList[], int iCardCount);
		//一对
		bool IsDoubleNums(BYTE byCardList[], int iCardCount);
		//重新排序
		bool ReSortCard(BYTE iCardList[], int iCardCount);
		//获取指定数组中王牌数量
		BYTE GetKingCard(BYTE iCardList[], int iCardCount, BYTE iCardResultList[]);
		//获取牌是不是王牌
		bool GetCardIsKing(BYTE iCard);
		//获取指定数组中花牌(癞子)数量
		BYTE GetHuaCard(BYTE iCardList[], int iCardCount, BYTE iCardResultList[]);
		//获取牌是不是花牌(癞子)
		bool GetCardIsHua(BYTE iCard);
		//获取这张牌能不能当财神用
		bool GetCardIsCaiShen(BYTE iCard);
		//把财神牌王和花牌(癞子)移动到最后面
		void sortCardListForFindAllType(BYTE cardList[], int cardCount, BYTE sortResult[], int &sortCount);
		//提取同花顺
		bool extractSameFlowerFlush(int iFlower, BYTE iHandCard[], int iHandCardCount, BYTE iResultCard[], int &iResultCardCount);
		//获取顺子的值
		BYTE GetStraightCardValue(BYTE iHandCard[], int iHandCardCount);
		int CompareCardInEqualValueByHaveKing(BYTE wFirstCard[], int iFirstCount, BYTE wSecondCard[], int iSecondCount);
		BYTE GetKingCount(BYTE iCardList[], int iCardCount);
		BYTE GetHuaCount(BYTE iCardList[], int iCardCount);
		bool finOneCard(BYTE iCardList[], int iCardCount, int icard);
    };

}



//////////////////////////////////////////////////////////////////////////

#endif // GameLogic_h__
