/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardFrameWrapper.h"

namespace CaiShenThirteenCard
{
	#define CARD_SPRITE_TAG	100

	CardFrameWrapper::CardFrameWrapper()
		: _callback(nullptr)
		, _mask(nullptr)
		, _cardValue(0xFF)
		, _selected(false)
	{
		
	}

	CardFrameWrapper::~CardFrameWrapper()
	{

	}

	bool CardFrameWrapper::load()
	{	
		getComponent<ImageView>()->addClickEventListener(CC_CALLBACK_1(CardFrameWrapper::onCardFrameClick, this));

		_mask = Sprite::createWithSpriteFrameName("CaiShenThirteenCard/game/control/card_frame_mask.png");
		_mask->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		_mask->setPosition(getContentSize().width / 2, getContentSize().height / 2);
		addChild(_mask, 2);

		_mask->setVisible(false);

		return true;
	}

	void CardFrameWrapper::unload()
	{

	}

	void CardFrameWrapper::restore()
	{ 
		setValue(0xFF); 
		setSelected(false);
	}

	void CardFrameWrapper::setValue(BYTE cardValue)
	{
		if (_cardValue != cardValue)
		{
			removeChild(CARD_SPRITE_TAG);
			_cardValue = cardValue;

			if (_cardValue != 0xFF)
			{
				auto cardSprite = Sprite::create();
				cardSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
				cardSprite->setPosition(getContentSize().width / 2, getContentSize().height / 2);
				cardSprite->setContentSize(getContentSize());
				cardSprite->setScale(0.9f);
				addChild(cardSprite, 1, CARD_SPRITE_TAG);

				cardSprite->setSpriteFrame(getCardTextureFileName(_cardValue));
			}
		}
	}

	std::string CardFrameWrapper::getCardTextureFileName(BYTE cardValue)
	{
		char filename[128];
		sprintf(filename, "Games/CaiShenThirteenCard/GamePlist/0x%02X.png", cardValue);
		return filename;
	}

	void CardFrameWrapper::setCardFrameClickEvent(const ccCardFrameClick& callback)
	{
		_callback = callback;
	}

	void CardFrameWrapper::setSelected(bool selected)
	{ 
		if (_selected != selected)
		{
			_selected = selected;
			_mask->setVisible(_selected);
		}
	}

	void CardFrameWrapper::setGroup(int group)
	{
		if (_group != group)
		{
			_group = group;
		}
	}

	int CardFrameWrapper::getGroup() const
	{
		return _group;
	}

	void CardFrameWrapper::onCardFrameClick(cocos2d::Ref *pSender)
	{
		_selected = true;

// 		if (_selected)
// 		{
// 			_mask->setVisible(true);
// 		}

		if (nullptr != _callback)
		{
			_callback(this);
		}
	}
}

