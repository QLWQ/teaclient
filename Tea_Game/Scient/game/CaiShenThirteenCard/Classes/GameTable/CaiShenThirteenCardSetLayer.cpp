/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardSetLayer.h"
#include "CaiShenThirteenCardMessageHead.h"
#include "json/rapidjson.h"
#include "json/document.h"
using namespace CaiShenThirteenCard;

static const char* SETTING_PATH = "platform/setting/roomSettingNode.csb";
#define		CHEAT_BUTTON_COUNT		3			//作弊按钮数
static const std::string _names[CHEAT_BUTTON_COUNT] = { "cheat_none", "cheat_last", "cheat_win" };
static const Vec2 _points[CHEAT_BUTTON_COUNT] = { Vec2(-270, -140), Vec2(-60, -140), Vec2(260, -140) };
static const std::string _titles[CHEAT_BUTTON_COUNT] = { GBKToUtf8("不作弊"), GBKToUtf8("大概率尾道好牌"), GBKToUtf8("必赢") };

CaiShenThirteenCardSetLayer::CaiShenThirteenCardSetLayer()
{

}

CaiShenThirteenCardSetLayer::~CaiShenThirteenCardSetLayer()
{

}

void CaiShenThirteenCardSetLayer::showSet(Node* parent, int zorder, int tag)
{
	CCAssert(nullptr != parent, "parent is nullptr");
	if (0 != tag) {
		parent->addChild(this, zorder, tag);
	}
	else {
		parent->addChild(this, zorder);
	}

	setPosition(Director::getInstance()->getWinSize() / 2);
}

void CaiShenThirteenCardSetLayer::close()
{
	this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){

		UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, _effectSlider->getPercent());
		UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, _musicSlider->getPercent());
		UserDefault::getInstance()->flush();

		this->removeFromParent();
	}), nullptr));
}

bool CaiShenThirteenCardSetLayer::init()
{
	if (!HNLayer::init()) {
		return false;
	}

	auto node = CSLoader::createNode(SETTING_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(Vec2::ZERO);
	addChild(node);

	auto layout_bg = dynamic_cast<Layout*>(node->getChildByName("Panel_setting"));
	layout_bg->setScale(_winSize.width / 1280, _winSize.height / 720);

	auto img_bg = dynamic_cast<ImageView*>(layout_bg->getChildByName("Image_bg"));
	img_bg->setScale(1280 / _winSize.width, 720 / _winSize.height);

	// 关闭
	auto btn_guanBi = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btn_guanBi->addClickEventListener([=](Ref*) {
		close();
	});

	//音效滑动条
	_effectSlider = (Slider*)img_bg->getChildByName("Slider_effect");
	_effectSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT));
	_effectSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(CaiShenThirteenCardSetLayer::sliderCallback, this)));

	//音乐滑动条
	_musicSlider = (Slider*)img_bg->getChildByName("Slider_music");
	_musicSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT));
	_musicSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(CaiShenThirteenCardSetLayer::sliderCallback, this)));

	// 解散房间
	auto btn_dis = (Button*)img_bg->getChildByName("Button_dismiss");
	btn_dis->addClickEventListener(CC_CALLBACK_1(CaiShenThirteenCardSetLayer::onDisClick, this));
	btn_dis->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);

	// 离开游戏
	auto btn_exit = (Button*)img_bg->getChildByName("Button_return");
	btn_exit->addClickEventListener(CC_CALLBACK_1(CaiShenThirteenCardSetLayer::onExitClick, this));
	if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
	{
		btn_exit->setPositionX(img_bg->getContentSize().width / 2);
	}

	/*
	//这里请求是否能有特殊权限
	std::string url = HNPlatformConfig()->buildHttp(getPHPServerUrl(), StringUtils::format("/hyapp/getUserInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID));
	//std::string url = StringUtils::format("http://192.168.0.33:8060/hyapp/getUserInfo.php?UserID=%d", PlatformLogic()->loginResult.dwUserID);
	HNHttpRequest::getInstance()->addObserver(this);
	HNHttpRequest::getInstance()->request("requestCheat", cocos2d::network::HttpRequest::Type::GET, url);
	*/
	//测试代码
	createCheatSwitch();
	return true;
}
void CaiShenThirteenCardSetLayer::onHttpResponse(const std::string& requestName, bool isSucceed, const std::string &responseData)
{
	if (!isSucceed) return;
	rapidjson::Document doc;
	doc.Parse<rapidjson::kParseDefaultFlags>(responseData.c_str());

	if (doc.HasParseError() || !doc.IsObject())
	{
		return;
	}
	if (requestName.compare("requestCheat") == 0)
	{
		if (doc.HasMember("Cheated") && doc.HasMember("Code"))  //Code = 1标识通讯正常
		{
			int cheated = doc["Cheated"].GetInt();
			int code = doc["Code"].GetInt();
			if (cheated == 1 && code == 1)
			{
				//创建作弊按钮开关
				createCheatSwitch();
			}
		}
	}
}


void CaiShenThirteenCardSetLayer::createCheatSwitch()
{	
	for (int i = 0; i < CHEAT_BUTTON_COUNT; i++)
	{
		auto pCheckBox = CheckBox::create("platform/common/fangxing1.png", "platform/common/fangxing2.png");
		pCheckBox->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		pCheckBox->setPosition(_points[i]);
		pCheckBox->setName(_names[i].c_str());
		pCheckBox->setSelected(i == 0 ? true : false);
		pCheckBox->addEventListener(CheckBox::ccCheckBoxCallback(CC_CALLBACK_2(CaiShenThirteenCardSetLayer::CheckBoxCheatEvent, this)));
		addChild(pCheckBox);

		Text* pText = Text::create(_titles[i].c_str(), "platform/common/RTWSYueRoudGoG0v1-Regular.ttf", 32);
		pText->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
		pText->setPosition(Vec2(50, pCheckBox->getContentSize().height / 2));
		pText->setColor(Color3B(255, 0, 0));
		pCheckBox->addChild(pText);
	}
}

void CaiShenThirteenCardSetLayer::updateCheatStatus(int status)
{
	for (int i = 0; i < CHEAT_BUTTON_COUNT; i++)
	{
		auto pCheckBox = dynamic_cast<CheckBox*>(getChildByName(_names[i]));
		if (pCheckBox == nullptr) continue;
		pCheckBox->setSelected(status == i ? true : false);
	}
}

void CaiShenThirteenCardSetLayer::CheckBoxCheatEvent(Ref* pSender, CheckBox::EventType type)
{
	CheckBox* pSelectCheckBox = dynamic_cast<CheckBox*>(pSender);
	if (pSelectCheckBox)
	{
		std::string select_name = pSelectCheckBox->getName();
		for (int i = 0; i < CHEAT_BUTTON_COUNT;i++)
		{
			auto pCheckBox = dynamic_cast<CheckBox*>(getChildByName(_names[i]));
			if (_names[i] == select_name)
			{//选中当前项,发送命令给服务端
				C_S_CheatStatus pData;
				pData.CheatStatus = i;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, C_S_CHEAT_STATUS, &pData, sizeof(pData));
			}
			else
			{
				pCheckBox->setSelected(false);
			}
		}
	}
}

void CaiShenThirteenCardSetLayer::onExitClick(Ref* pRef)
{
	if (nullptr != onExitCallBack) {
		onExitCallBack();
	}
}

// 切换账号
void CaiShenThirteenCardSetLayer::onDisClick(Ref* pRef)
{
	if (nullptr != onDisCallBack) {
		onDisCallBack();
	}
}

void CaiShenThirteenCardSetLayer::sliderCallback(Ref* pSender, Slider::EventType type)
{
	Slider* pSlider = static_cast<Slider*>(pSender);
	auto name = pSlider->getName();

	float l = pSlider->getPercent() / 100.f;

	if (name.compare("Slider_music") == 0)
	{
		if (pSlider->getPercent() == 0)
		{
			HNAudioEngine::getInstance()->setSwitchOfMusic(false);
			HNAudioEngine::getInstance()->pauseBackgroundMusic();
		}
		else if (!HNAudioEngine::getInstance()->getSwitchOfMusic())
		{
			HNAudioEngine::getInstance()->setSwitchOfMusic(true);
			HNAudioEngine::getInstance()->resumeBackgroundMusic();
		}
		else
		{

		}
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(l);
	}

	if (name.compare("Slider_effect") == 0)
	{
		if (pSlider->getPercent() == 0)
		{
			HNAudioEngine::getInstance()->setSwitcjofEffect(false);
		}
		else
		{
			HNAudioEngine::getInstance()->setSwitcjofEffect(true);
		}
		HNAudioEngine::getInstance()->setEffectsVolume(l);
	}
}