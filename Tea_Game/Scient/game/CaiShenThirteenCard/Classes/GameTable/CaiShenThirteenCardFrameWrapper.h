/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_CardFrameWrapper_H__
#define __CaiShenThirteenCard_CardFrameWrapper_H__

#include "cocos2d.h"
#include <string>
#include "CaiShenThirteenCardWrapper.h"
#include "HNNetExport.h"

USING_NS_CC;

namespace CaiShenThirteenCard
{
	class CardFrameWrapper;

	typedef std::function<void(CardFrameWrapper* cardFrame)> ccCardFrameClick;

	class CardFrameWrapper : public IWrapper
	{
		int _group;
		bool _selected;
		BYTE _cardValue;
		Sprite*	_mask;
		ccCardFrameClick _callback;

	public:
		CardFrameWrapper();
		virtual ~CardFrameWrapper();

	public:
		virtual bool load() override;
		virtual void unload() override;
		virtual void restore() override;

	public:
		void setValue(BYTE cardValue);
		BYTE getValue() const { return _cardValue;}

		bool isValid() const { return 0x00 != 0xFF; }				
		
		void setSelected(bool selected);
		bool isSelected() const { return _selected; }

		void setGroup(int group);
		int getGroup() const;

	public:
		void setCardFrameClickEvent(const ccCardFrameClick& callback);

	public:
		static std::string getCardTextureFileName(BYTE cardValue);

	protected:
		void onCardFrameClick(cocos2d::Ref *pSender);
	};
}

#endif // !__CaiShenThirteenCard_CardFrameWrapper_H__
