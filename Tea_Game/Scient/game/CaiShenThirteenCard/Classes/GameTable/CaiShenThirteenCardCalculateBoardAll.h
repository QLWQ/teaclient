/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef CaiShenThirteenCard_CalculateBoard_All_h__
#define CaiShenThirteenCard_CalculateBoard_All_h__

#include "HNUIExport.h"
#include "CaiShenThirteenCardMessageHead.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "HNLobbyExport.h"
using namespace ui;
using namespace cocostudio;
using namespace HN;

namespace CaiShenThirteenCard
{
	class CalculateBoardAll : public HNCalculateBoard
	{	
	public:
		CREATE_FUNC(CalculateBoardAll);

		CalculateBoardAll();
		virtual ~CalculateBoardAll();

		virtual bool init() override;

		//显示结算数据
		void showAndUpdateBoard(Node* parent, const CalculateBoard* boardData, const std::string& roomNumber, int ownerUserID);

		//根据玩家人数调整位置
		void movePlayerLayout(int palyNum);
	private:

		//分享按钮回调
		virtual void shareBtnCallBack(Ref* pSender, Widget::TouchEventType type) override;

		//返回按钮回调
		virtual void backBtnCallBack(Ref* pSender, Widget::TouchEventType type) override;

		std::string _deskPassword;
		/*GameUserHead* _userHead;
		Sprite* _spriteHead;*/
		bool	_isTouch = true;				// 按钮是否可以点击
	public:
		Button* _btn_shareKongLong;
		Button* _btn_shareWeiXin;
	};
}


#endif //__CaiShenThirteenCard_All_h__
