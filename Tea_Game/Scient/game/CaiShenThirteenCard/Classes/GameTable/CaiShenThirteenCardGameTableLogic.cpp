/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenThirteenCardGameTableLogic.h"
#include "CaiShenThirteenCardGameTableUI.h"
#include "HNNetExport.h"
#include "HNLobbyExport.h"
#include "HNUIExport.h"

using namespace HN;

namespace CaiShenThirteenCard
{
	const char* gCardShapeName[] =
	{
		"未知",
		"高牌",
		"一对",
		"两对",
		"三条",
		"顺子",
		"同花",
		"葫芦",
		"四条",
		"同花顺",
		"五同",
	};

	GameTableLogic::GameTableLogic(IGameTableUICallback* uiCallback, BYTE deskNo, bool bAutoCreate)
		: HNGameLogicBase(deskNo, PLAY_COUNT, bAutoCreate, uiCallback)
		, _uiCallback(uiCallback)
		, _bSuper(false)
		, _gameStart(false)
		, _superStation(-1)
		, _allRoundsEnd(false)
	{
		_isVipRoom = RoomLogic()->getRoomRule() & GRR_GAME_BUY;
		_gameStatus = 255;
	}

	GameTableLogic::~GameTableLogic()
	{

	}

	void GameTableLogic::enterGame()
	{
		if (_autoCreate)
		{
			sendGameInfo();
		}
	}

	void GameTableLogic::loadDeskUsersUI()
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_uiCallback->onRemovePlayer(i, 0, 0);
		}
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (i >= _existPlayer.size())
			{
				continue;
			}
			const UserInfoStruct* pUser = getUserBySeatNo(i);
			if (_existPlayer[i] && pUser != nullptr)
			{
				bool isMe = (pUser->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
				_uiCallback->onAddPlayer(pUser->bDeskStation, pUser->dwUserID, isMe);
			
			}
		}
	}

	void GameTableLogic::clearDesk()
	{

	}

	void GameTableLogic::clearDeskUsers()
	{

	}

	void GameTableLogic::initParams()
	{
		

	}

	void GameTableLogic::dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		if (!_gameStart) return;

		switch(messageHead->bAssistantID)
		{
		case S_C_NOTICE_ROB: //通知抢庄
		{
			CHECK_SOCKET_DATA(S_C_TNoticeRobNt, sizeof(S_C_TNoticeRobNt), "S_C_TNoticeRobNt size error!");
			S_C_TNoticeRobNt* pTNoticeRobNt = (S_C_TNoticeRobNt*)(object);
				
			//当前抢庄玩家
			TableData.m_byCurrRobUser = pTNoticeRobNt->byCurrRobDesk;		
			//玩家状态
			memcpy(TableData.m_EnUserState, pTNoticeRobNt->iUserState, sizeof(TableData.m_EnUserState));
			//游戏状态
			_gameStatus = GS_ROB_NT;

			bool self = (_mySeatNo == pTNoticeRobNt->byCurrRobDesk);
			_uiCallback->onGameNoticeRobNt(pTNoticeRobNt->byCurrRobDesk, self);

		} break;
		case S_C_USER_ROB_RESULT://玩家抢庄结果
		{
			CHECK_SOCKET_DATA(S_C_UserRobNt_Result, sizeof(S_C_UserRobNt_Result), "S_C_UserRobNt_Result size error!");
			S_C_UserRobNt_Result* pRobNtResult = (S_C_UserRobNt_Result*)(object);		

			//游戏状态
			_gameStatus = GS_ROB_NT;

			bool self = (_mySeatNo == pRobNtResult->byDeskStation);
			_uiCallback->onGameRobNtResult(pRobNtResult->byDeskStation, pRobNtResult->byRobResult, self);

		} break;
		case S_C_MAKE_SUER_NT: //确定庄家
		{
			CHECK_SOCKET_DATA(S_C_MakeSure_NtStation, sizeof(S_C_MakeSure_NtStation), "S_C_MakeSure_NtStation size error!");
			S_C_MakeSure_NtStation* pNtStation = (S_C_MakeSure_NtStation*)(object);
			
			//游戏状态
			_gameStatus = GS_ROB_NT;

			TableData.m_byNowNtStation = pNtStation->byNowNtStation;

			bool self = (_mySeatNo == pNtStation->byNowNtStation);
			_uiCallback->onGameMakeSureNt(TableData.m_byNowNtStation, self,pNtStation->iUserRobNt );
		} break;
		case S_C_SEND_CARD: //发牌消息
		{
			CHECK_SOCKET_DATA(S_C_SendCard, sizeof(S_C_SendCard), "S_C_SendCard size error!");
			S_C_SendCard* pSendCard = (S_C_SendCard*)(object);
			
			//游戏状态
			_gameStatus = GS_SEND_CARD;

			//当前庄家位置
			TableData.m_byNowNtStation = pSendCard->byNowNtStation;

			
			//玩家牌数据
			memcpy(TableData.m_byUserCards[_mySeatNo], pSendCard->byUserCards, sizeof(TableData.m_byUserCards[_mySeatNo]));

			//玩家状态
			memcpy(TableData.m_EnUserState, pSendCard->iUserState, sizeof(TableData.m_EnUserState));

			std::vector<BYTE> cards;
			for (size_t i = 0; i < USER_CARD_COUNT; i++) cards.push_back(pSendCard->byUserCards[i]);

			_uiCallback->onGameSendCard(cards);
		} break;
		case S_C_NOTICE_OPEN_CARD://通知开牌消息
		{
			CHECK_SOCKET_DATA(S_C_NoticeOpenCard, sizeof(S_C_NoticeOpenCard), "S_C_NoticeOpenCard size error!");
			S_C_NoticeOpenCard* pNoticeOpenCard = (S_C_NoticeOpenCard*)(object);
			//游戏状态
			_gameStatus = GS_OPEN_CARD;
			//玩家状态
			memcpy(TableData.m_EnUserState, pNoticeOpenCard->iUserState, sizeof(TableData.m_EnUserState));
			//排序牌数据
			CUpGradeGameLogic gameLogic;
			gameLogic.SortCard(TableData.m_byUserCards[_mySeatNo], NULL, USER_CARD_COUNT);

			_uiCallback->onGameNoticeOpenCard();
		} break;
		case S_C_USER_OPEN_CARD_RESULT://玩家开牌结果
		{
			CHECK_SOCKET_DATA(C_S_UserOpenCard_Result, sizeof(C_S_UserOpenCard_Result), "C_S_UserOpenCard_Result size error!");
			C_S_UserOpenCard_Result* pOpenCardResult = (C_S_UserOpenCard_Result*)(object);
			
			//玩家位置
			BYTE seatNo = pOpenCardResult->byDeskStation;
			//玩家状态
			memcpy(TableData.m_EnUserState, pOpenCardResult->iUserState, sizeof(TableData.m_EnUserState));
			//每一堆牌数据
			memcpy(TableData.m_byHeapCard[seatNo], pOpenCardResult->byHeapCard, sizeof(TableData.m_byHeapCard[seatNo]));
			
			_uiCallback->onGameOpenCardResult(seatNo);
			if (seatNo == _mySeatNo)
			{
				_uiCallback->showMyCard(pOpenCardResult->byHeapCard);
			}

		} break; 
		case S_C_COMPARE_CARD://比牌消息
		{
			CHECK_SOCKET_DATA(S_C_CompareCard, sizeof(S_C_CompareCard), "S_C_CompareCard size error!");
			S_C_CompareCard* pCompareCard = (S_C_CompareCard*)(object);
		
			_gameStatus = GS_COMPARE_CARD;

			//每一堆牌数据
			memcpy(TableData.m_byHeapCard, pCompareCard->byHeapCard, sizeof(TableData.m_byHeapCard));
			//牌型
			memcpy(TableData.m_iHeapShape, pCompareCard->iHeapShape, sizeof(TableData.m_iHeapShape));
			//比牌结果
			memcpy(TableData.m_iComepareResult, pCompareCard->iComepareResult, sizeof(TableData.m_iComepareResult));
			memcpy(TableData.m_iExtraScore, pCompareCard->iExtraScore, sizeof(TableData.m_iExtraScore));
			//玩家状态
			memcpy(TableData.m_EnUserState, pCompareCard->iUserState, sizeof(TableData.m_EnUserState));
			_uiCallback->onGameCompareCard(TableData.m_byHeapCard);

		} break;
		case S_C_OPEN_FIRE://打枪消息
		{
			CHECK_SOCKET_DATA(S_C_OpenFire, sizeof(S_C_OpenFire), "S_C_OpenFire size error!");
			S_C_OpenFire* pOpenFire = (S_C_OpenFire*)(object);

			bool userFireUser[PLAY_COUNT][PLAY_COUNT] = {false};

			memcpy(userFireUser, pOpenFire->bUserFireUser, sizeof(userFireUser));

			//二维数组，一维下标打二维下标
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				for (int j = 0; j < PLAY_COUNT; j++)
				{
					if (userFireUser[i][j] && j == _mySeatNo)//打枪,且是我的三道都被比下去
					{
						_uiCallback->onGameBeOpenFireEffect();
						break;
					}
				}
			}

			_uiCallback->onGameOpenFire(pOpenFire->bUserFireUser);

		} break;
		case ASS_SEND_ALL_HIT:
		{
			CHECK_SOCKET_DATA(S_C_FireAll, sizeof(S_C_FireAll), "S_C_FireAll size error!");
			S_C_FireAll* pFireAll = (S_C_FireAll*)(object);

			////只有庄家才有全垒打，不是庄家就不处理此消息

			//if (TableData.m_byNowNtStation == _mySeatNo)
				//_uiCallback->onGameFireAllEffect();
				//_uiCallback->onGameFireAll(pFireAll->bDeskStation);
			_uiCallback->setAllFire(true);
			_Zhuang = pFireAll->bDeskStation;
			
		} break;
		case S_C_GAME_END://结算消息
		{
			CHECK_SOCKET_DATA(S_C_GameResult, sizeof(S_C_GameResult), "S_C_GameResult size error!");
			S_C_GameResult* pGameResult = (S_C_GameResult*)(object);

			_gameStatus = GS_WAIT_NEXT;
			//保存数据
			memcpy(TableData.m_i64UserFen, pGameResult->i64UserFen, sizeof(TableData.m_i64UserFen));
			memcpy(TableData.m_i64UserMoney, pGameResult->i64UserMoney, sizeof(TableData.m_i64UserMoney));

			_uiCallback->onGameResult(pGameResult);
		} break;
		case S_C_IS_SUPER_USER:		//超端验证消息
		{
			CHECK_SOCKET_DATA(S_C_SuperUser, sizeof(S_C_SuperUser), "S_C_SuperUser size error!");
			S_C_SuperUser* pSuperUser = (S_C_SuperUser*)(object);

			_bSuper = pSuperUser->bIsSuper;
			_superStation = pSuperUser->byDeskStation;

			break;
		}
		case ASS_UPDATE_CALCULATE_BOARD_SIG:		//大结算
		{
			assert(sizeof(CalculateBoard)* 6 == objectSize);

			dealOnCalculateBoard((CalculateBoard*)object);
			
			break;
		}
		case S_C_GAME_RECORD_RESULT:			//房卡游戏战绩
		{
			CHECK_SOCKET_DATA(S_C_Game_SSS_RecordResult, sizeof(S_C_Game_SSS_RecordResult), "S_C_Game_SSS_RecordResult size error!");
			S_C_Game_SSS_RecordResult* pSuperUser = (S_C_Game_SSS_RecordResult*)(object);
			_uiCallback->sendSSSRecordData(pSuperUser);
			break;
		}
		case S_C_GAME_JINGBI_RECORD_RESULT:			//金币游戏战绩
		{
			CHECK_SOCKET_DATA(S_C_GameRecordResult, sizeof(S_C_GameRecordResult), "S_C_GameRecordResult size error!");
			S_C_GameRecordResult* pSuperUser = (S_C_GameRecordResult*)(object);
			_uiCallback->sendRecordData(pSuperUser);
			break;
		}
		case S_C_CHEAT_STATUS:		//作弊消息返回
		{
			CHECK_SOCKET_DATA(S_C_CheatStatus, sizeof(S_C_CheatStatus), "S_C_CheatStatus size error!");
			S_C_CheatStatus* pData = (S_C_CheatStatus*)(object);
			_uiCallback->onCheatStatus(pData);
			break;
		}
		case S_C_HEAP_CARD:   //一键搓牌消息
		{
			CHECK_SOCKET_DATA(S_C_HeapCard, sizeof(S_C_HeapCard), "S_C_HeapCard size error!");
			S_C_HeapCard* pData = (S_C_HeapCard*)(object);
			_uiCallback->onGameSetAutoRubCardType(pData->byHeapCard, pData->iHeapShape);
			break;
		}
		default:
			HNLOG_ERROR("unknow game command.");
			break;
		}
	}

	void GameTableLogic::dealOnCalculateBoard(const CalculateBoard* boardData)
	{
		log("dealOnCalculateBoard \n");

		_allRoundsEnd = true;

		_uiCallback->showCalculateBoard(boardData);

		//大结算时把场景置为大厅 防止走房间重连
		HNPlatformConfig()->setSceneState(PlatformConfig::SCENE_STATE::OTHER);
	}

	void GameTableLogic::dealGameStartResp(BYTE bDeskNO)
	{

	}

	void GameTableLogic::dealGameEndResp(BYTE deskNo)
	{

	}
	void GameTableLogic::dealGameClean()
	{

	}
	void GameTableLogic::dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum)
	{

	}
	void GameTableLogic::dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum)
	{

	}
	void GameTableLogic::dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum)
	{

	}

	void GameTableLogic::dealUserAgreeResp(MSG_GR_R_UserAgree* agree)
	{	
		//根据游戏状态处理准备消息
		if (_gameStatus == GS_WAIT_SETGAME || _gameStatus == GS_WAIT_AGREE || _gameStatus == GS_WAIT_NEXT)
		{
			bool self = (_mySeatNo == agree->bDeskStation);
			_uiCallback->onGameAgree(agree->bDeskStation, self);
		}
	}

	void GameTableLogic::dealQueueUserSitMessage(bool success, const std::string& message)
	{
		HNGameLogicBase::dealQueueUserSitMessage(success, message);

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		if (success)
		{
			loadDeskUsersUI();
			enterGame();
		}
		else
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(message);
			prompt->setCallBack([=](){
				stop();
				RoomLogic()->close();
				GamePlatform::returnPlatform(ROOMLIST);
			});
		}
	}

	void GameTableLogic::dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		bool isMe = (user->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
		if (isMe)
		{
			loadDeskUsersUI();

		}
		else
		{
			_uiCallback->onAddPlayer(userSit->bDeskStation, userSit->dwUserID, isMe);
			_uiCallback->onVipTip(userSit->bDeskStation);
		}
		_uiCallback->setDeskInfo();
		log("zhifu dealUserSitResp  userSit->dwUserID :%d", userSit->dwUserID);
	}

	void GameTableLogic::dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		bool self = (userSit->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
		if (self)
		{
			stop();
			RoomLogic()->close();
			GamePlatform::createPlatform();
		}
		else
		{
			_uiCallback->onRemovePlayer(logicToViewSeatNo(userSit->bDeskStation),0, 0);
		}
	}

	void GameTableLogic::dealUserCutMessageResp(INT userId, BYTE bDeskNO)
	{
		//根据游戏状态处理掉线消息
		if (_gameStatus == GS_WAIT_SETGAME || _gameStatus == GS_WAIT_AGREE || _gameStatus == GS_WAIT_NEXT)
		{
			_uiCallback->showUserCut(bDeskNO);
		}
	}

	void GameTableLogic::dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo)
	{
		HNGameLogicBase::dealGameInfoResp(pGameInfo);

	}

	void GameTableLogic::dealUserInfoChangeResp(const UserInfoStruct* user)
	{

	}

	void GameTableLogic::dealGameStationResp(void* object, INT objectSize)
	{
		_gameStart = true;
		
		//是否启用财神牌玩法
		S_C_TGameStation_Base* pGameBase = (S_C_TGameStation_Base*)(object);
		_gameLogic.SetKingCanReplace(pGameBase->bShuangWang || pGameBase->bSiHua);
		_uiCallback->showGameRule(pGameBase->bShuangWang, pGameBase->bSiHua, pGameBase->iBaseFen, pGameBase->iPlayerNum, pGameBase->palyMode, pGameBase->bTongSha,pGameBase->bJiaLiangMen);
		//_maxPlayers = pGameBase->iPlayerNum; //设置人数
		//保存游戏人数
		//HNPlatformConfig()->setPlayerNum(_maxPlayers);
		_uiCallback->setBottomScore(pGameBase->iBaseFen);
		if (_maxPlayers <= 4)
		{
			_uiCallback->setPlayerPos();
		}
		_gameMode = pGameBase->palyMode;
		loadDeskUsersUI();
		switch(pGameBase->byGameStation)
		{
		case GS_WAIT_SETGAME: //等待东家设置状态				
		case GS_WAIT_AGREE:	//等待同意设置
		case GS_WAIT_NEXT:	//等待下一盘开始
		{
			CHECK_SOCKET_DATA(S_C_TGameStation_Free, sizeof(S_C_TGameStation_Free), "S_C_TGameStation_Free size error!");
			S_C_TGameStation_Free* pGameStation = (S_C_TGameStation_Free*)(object);

			TableData.m_byBeginTime = pGameStation->byBeginTime;
			TableData.m_byRobTime = pGameStation->byRobTime;
			TableData.m_bySendCardTime = pGameStation->bySendCardTime;
			TableData.m_byThinkTime = pGameStation->byThinkTime;
			TableData.m_byCompareTime = pGameStation->byCompareTime;
			TableData.m_byCountTime = pGameStation->byCountTime;
			TableData.m_iBaseFen = pGameStation->iBaseFen;
			
			for (size_t i = 0; i < PLAY_COUNT; i++)
			{
				bool ready = pGameStation->bUserReady[i];
				HNLOG_INFO("GS_WAIT_SETGAME || GS_WAIT_AGREE || GS_WAIT_NEXT UserReady = %d", ready);
			}

			_uiCallback->onGSFree(pGameStation->bUserReady);
			_uiCallback->onSetGameBasePoint(pGameStation->iBaseFen);
			HNLOG_ERROR("GS_WAIT_SETGAME || GS_WAIT_AGREE || GS_WAIT_NEXT");
		} break;
		case GS_ROB_NT:		//抢庄状态
		{
			CHECK_SOCKET_DATA(S_C_GameStation_RobNt, sizeof(S_C_GameStation_RobNt), "S_C_GameStation_RobNt size error!");
			S_C_GameStation_RobNt* pGameStation = (S_C_GameStation_RobNt*)(object);

			TableData.m_byBeginTime = pGameStation->byBeginTime;
			TableData.m_byRobTime = pGameStation->byRobTime;
			TableData.m_bySendCardTime = pGameStation->bySendCardTime;
			TableData.m_byThinkTime = pGameStation->byThinkTime;
			TableData.m_byCompareTime = pGameStation->byCompareTime;
			TableData.m_byCountTime = pGameStation->byCountTime;
			TableData.m_iBaseFen = pGameStation->iBaseFen;
			
			//剩余时间
			TableData.m_byRemaindTime = pGameStation->byRemaindTime;
			//当前操作玩家
			TableData.m_byCurrRobUser = pGameStation->byCurrRobUser;
			//庄家位置
			TableData.m_byNowNtStation = pGameStation->byNowNtStation;
			//游戏状态
			_gameStatus = GS_ROB_NT;

			// 玩家状态
			memcpy(TableData.m_EnUserState, pGameStation->iUserState, sizeof(TableData.m_EnUserState));

			_uiCallback->onGSRobNt(pGameStation->byCurrRobUser);
			_uiCallback->onSetGameBasePoint(pGameStation->iBaseFen);
			HNLOG_DEBUG("GS_ROB_NT");
		} break;
		case GS_SEND_CARD:	//发牌状态
		{
			CHECK_SOCKET_DATA(S_C_GameStation_SendCard, sizeof(S_C_GameStation_SendCard), "S_C_GameStation_SendCard size error!");
			S_C_GameStation_SendCard* pGameStation = (S_C_GameStation_SendCard*)(object);

			TableData.m_byBeginTime = pGameStation->byBeginTime;
			TableData.m_byRobTime = pGameStation->byRobTime;
			TableData.m_bySendCardTime = pGameStation->bySendCardTime;
			TableData.m_byThinkTime = pGameStation->byThinkTime;
			TableData.m_byCompareTime = pGameStation->byCompareTime;
			TableData.m_byCountTime = pGameStation->byCountTime;
			TableData.m_iBaseFen = pGameStation->iBaseFen;

			//庄家位置
			TableData.m_byNowNtStation = pGameStation->byNowNtStation;
			//剩余时间
			TableData.m_byRemaindTime = pGameStation->byRemaindTime;
			//游戏状态
			_gameStatus = GS_SEND_CARD;

			//玩家状态
			memcpy(TableData.m_EnUserState, pGameStation->iUserState, sizeof(TableData.m_EnUserState));

			//正常的玩家就显示操作容器 中途加入的不能显示
			if (TableData.m_EnUserState[_mySeatNo] != STATE_ERR)
			{
				CUpGradeGameLogic gameLogic;
				gameLogic.SortCard(pGameStation->byUserCards, NULL, USER_CARD_COUNT);
			}

			//玩家的牌数据
			memcpy(TableData.m_byUserCards[_mySeatNo], pGameStation->byUserCards, sizeof(TableData.m_byUserCards[_mySeatNo]));

			std::vector<BYTE> cards;
			for (size_t i = 0; i < USER_CARD_COUNT; i++)
			{
				BYTE val = pGameStation->byUserCards[i];
				if (0 == val) continue;
				if (0xFF == val) continue;
				cards.push_back(val);
			}

			_uiCallback->onGSSendCard(cards);
			_uiCallback->onSetGameBasePoint(pGameStation->iBaseFen);
			HNLOG_DEBUG("GS_SEND_CARD");
		} break;
		case GS_OPEN_CARD:	//摆牌状态
		{
			CHECK_SOCKET_DATA(S_C_GameStation_OpenCard, sizeof(S_C_GameStation_OpenCard), "S_C_GameStation_OpenCard size error!");
			S_C_GameStation_OpenCard* pGameStation = (S_C_GameStation_OpenCard*)(object);

			TableData.m_byBeginTime = pGameStation->byBeginTime;
			TableData.m_byRobTime = pGameStation->byRobTime;
			TableData.m_bySendCardTime = pGameStation->bySendCardTime;
			TableData.m_byThinkTime = pGameStation->byThinkTime;
			TableData.m_byCompareTime = pGameStation->byCompareTime;
			TableData.m_byCountTime = pGameStation->byCountTime;
			TableData.m_iBaseFen = pGameStation->iBaseFen;

			//庄家位置
			TableData.m_byNowNtStation = pGameStation->byNowNtStation;
			//剩余时间
			TableData.m_byRemaindTime = pGameStation->byRemaindTime;
			//游戏状态
			_gameStatus = GS_OPEN_CARD;
			
			//玩家状态
			memcpy(TableData.m_EnUserState, pGameStation->iUserState, sizeof(TableData.m_EnUserState));
			//玩家的牌数据
			memcpy(TableData.m_byUserCards[_mySeatNo], pGameStation->byUserCards, sizeof(TableData.m_byUserCards[_mySeatNo]));
			//玩家摆牌数据
			memcpy(TableData.m_byHeapCard[_mySeatNo], pGameStation->byMyHeapCard, sizeof(TableData.m_byHeapCard[_mySeatNo]));

			std::vector<BYTE> cards;
			for (size_t i = 0; i < USER_CARD_COUNT; i++)
			{
				BYTE val = pGameStation->byUserCards[i];
				if (0 == val) continue;
				if (0xFF == val) continue;
				cards.push_back(val);
			}

			_uiCallback->onGSOpenCard(cards, pGameStation->byMyHeapCard);
			_uiCallback->onSetGameBasePoint(pGameStation->iBaseFen);
			HNLOG_DEBUG("GS_OPEN_CARD");
		} break;
		case GS_COMPARE_CARD://比牌阶段
		{
			CHECK_SOCKET_DATA(S_C_GameStation_CompareCard, sizeof(S_C_GameStation_CompareCard), "S_C_GameStation_CompareCard size error!");
			S_C_GameStation_CompareCard* pGameStation = (S_C_GameStation_CompareCard*)(object);

			TableData.m_byBeginTime = pGameStation->byBeginTime;
			TableData.m_byRobTime = pGameStation->byRobTime;
			TableData.m_bySendCardTime = pGameStation->bySendCardTime;
			TableData.m_byThinkTime = pGameStation->byThinkTime;
			TableData.m_byCompareTime = pGameStation->byCompareTime;
			TableData.m_byCountTime = pGameStation->byCountTime;
			TableData.m_iBaseFen = pGameStation->iBaseFen;

			//庄家位置
			TableData.m_byNowNtStation = pGameStation->byNowNtStation;
			//剩余时间
			TableData.m_byRemaindTime = pGameStation->byRemaindTime;
			//游戏状态
			_gameStatus = GS_COMPARE_CARD;

			//玩家状态
			memcpy(TableData.m_EnUserState, pGameStation->iUserState, sizeof(TableData.m_EnUserState));
			//玩家摆牌数据
			memcpy(TableData.m_byHeapCard, pGameStation->byHeapCard, sizeof(TableData.m_byHeapCard));
			//牌型
			memcpy(TableData.m_iHeapShape, pGameStation->iHeapShape, sizeof(TableData.m_iHeapShape));
			//比牌结果
			memcpy(TableData.m_iComepareResult, pGameStation->iComepareResult, sizeof(TableData.m_iComepareResult));
			memcpy(TableData.m_iExtraScore, pGameStation->iExtraScore, sizeof(TableData.m_iExtraScore));

			_uiCallback->onGSCompareCard(TableData.m_byHeapCard);
			_uiCallback->onSetGameBasePoint(pGameStation->iBaseFen);
			HNLOG_DEBUG("GS_COMPARE_CARD");
		} break;
		default: 
			HNLOG_ERROR("unknow GS.");
			break;
		}
	}

	//设置回放模式
	void GameTableLogic::dealGamePlayBack(bool isback)
	{
		//GameManager::getInstance()->setPlayBack(isback);
	}

	void GameTableLogic::sendForceQuit(const ccExitCallback& callback)
	{
		bool connected = RoomLogic()->isConnect();
		if (connected)
		{
			HNGameLogicBase::sendForceQuit();
		}
		callback(connected);
	}

	void GameTableLogic::sendUserUp(const ccExitCallback& callback)
	{
		_exitCB = callback;

		bool connected = RoomLogic()->isConnect();
		if (connected)
		{
			HNGameLogicBase::sendUserUp();
		}
		else
		{
			_exitCB(connected);
		}
	}

	const char* GameTableLogic::getCardTypeName(BYTE type)
	{
		return gCardShapeName[type];
	}

	void GameTableLogic::sendUserRobNt(bool rob)
	{
		C_S_UserRobNt UserRobNt;
		UserRobNt.byDeskStation = _mySeatNo;
		UserRobNt.byRob = rob;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, C_S_USER_ROB, &UserRobNt, sizeof(UserRobNt));
	}

	void GameTableLogic::sendOpenCard(INT heapCount[3], BYTE heapCard[3][5])
	{
		C_S_UserOpenCard UserOpenCard;
		UserOpenCard.byDeskStation = _mySeatNo;
		memcpy(UserOpenCard.iHeapCount, heapCount, sizeof(UserOpenCard.iHeapCount));
		memcpy(UserOpenCard.byHeapCard, heapCard, sizeof(UserOpenCard.byHeapCard));
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, C_S_USER_OPEN_CARD, &UserOpenCard, sizeof(UserOpenCard));
	}

	void GameTableLogic::sendBeginGame()
	{
	
		//RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, C_S_USER_ROB, &UserRobNt, sizeof(UserRobNt));
	}

	void GameTableLogic::dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk)
	{
		auto pUser = UserInfoModule()->findUser(normalTalk->dwSendID);
		if (pUser != nullptr)
		{
			_uiCallback->onChatTextMsg(pUser->bDeskStation, normalTalk->szMessage);
		}
	}

	void GameTableLogic::dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		//_uiCallback->onChatVoiceMsg(msg->uUserID, msg->uVoiceID, msg->iVoiceTime);
		switch (messageHead->bAssistantID)
		{
		case ASS_GR_VOIEC:
		{
			//效验数据
			CCAssert(sizeof(VoiceInfo) == objectSize, "VoiceInfo size is error.");
			VoiceInfo* voiceInfo = (VoiceInfo*)object;
			auto userInfo = getUserByUserID(voiceInfo->uUserID);
			if (!userInfo) return;
			if (userInfo->bDeskNO == _deskNo)
			{
				_uiCallback->onChatVoiceMsg(voiceInfo->uUserID, voiceInfo->uVoiceID, voiceInfo->iVoiceTime);
			}
		}
		break;
		default:
			break;
		}
	}
	//void GameTableLogic::dealGiftMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	//{
	//	switch (messageHead->bAssistantID)
	//	{
	//	//case ASS_GR_GIFT:
	//	//{
	//	//					//效验数据
	//	//					CCAssert(sizeof (TMSG_SENG_GIFT_REQ) == objectSize, "TMSG_SENG_GIFT_REQ size is error.");
	//	//					TMSG_SENG_GIFT_REQ* data = (TMSG_SENG_GIFT_REQ*)object;
	//	//					_uiCallback->dealGift(*data);
	//	//}
	//		break;
	//	default:
	//		break;
	//	}
	//}

	BYTE GameTableLogic::viewToLogicSeatNo(BYTE vSeatNO)
	{
		if (_maxPlayers == 2)
		{
			if (vSeatNO == 1 || vSeatNO == 3)
				return INVALID_DESKSTATION;
			if (vSeatNO == 2)
				vSeatNO--;
		}
		if (_maxPlayers == 3)
		{
			if (vSeatNO == 3)
			{
				vSeatNO--;
			}
			else if (vSeatNO == 2)
			{
				return INVALID_DESKSTATION;
			}
		}
		if (_maxPlayers == 5)
		{
			if (vSeatNO == 5)
			{
				vSeatNO = 2;
			}
			else if (vSeatNO == 2)
			{
				return INVALID_DESKSTATION;
			}
		}
		return (vSeatNO - _seatOffset + _maxPlayers) % _maxPlayers;
	}

	BYTE GameTableLogic::logicToViewSeatNo(BYTE lSeatNO)
	{
		auto seat = (lSeatNO + _seatOffset + _maxPlayers) % _maxPlayers;
		if (_maxPlayers == 2)
		{
			if (lSeatNO > 1) //2,3
			{
				if (lSeatNO == 2)
				{
					seat = 1;
				}
				else
				{
					seat = 3;
				}
				return seat;
			}
			if (seat != 0)
			{
				seat = 2;
			}
		}
		else if (_maxPlayers == 3)
		{
			if (lSeatNO == 3)
			{
				seat = 2;
				return seat;
			}
			if (seat == 2)
			{
				seat = 3;
			}
		}
		else if (_maxPlayers == 5)
		{
			if (seat == 2)
			{
				seat = 5;
				return seat;
			}
		}
		return seat;
	}

	void GameTableLogic::sendleave()
	{
		do
		{
			// 			if (!RoomLogic()->isConnect())
			// 			{
			// 				_gametableUi->leaveDesk(false);
			// 				break;
			// 			}
			// 
			// 			if (INVALID_DESKNO == _mySeatNo)
			// 			{
			// 				_gametableUi->leaveDesk(false);
			// 				break;
			// 			}
			// 
			// 			UserInfoStruct* myInfo = getUserBySeatNo(_mySeatNo);
			// 			if (myInfo != nullptr && (_gameStation == GS_PLAY_GAME || _gameStation == GS_SEND_CARD) && (_UserState[_mySeatNo] != STATE_ERR))
			// 			{
			// 				GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏中，不能离开"));
			// 				break;
			// 			}
			HNGameLogicBase::sendUserUp();
		} while (0);
	}

	

}
