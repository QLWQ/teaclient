/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_Game_Table_Logic_H__
#define __CaiShenThirteenCard_Game_Table_Logic_H__

#include "HNLogicExport.h"
#include "HNNetExport.h"
#include "CaiShenThirteenCardGameLogic.h"
#include "CaiShenThirteenCardMessageHead.h"
#include <functional>

namespace CaiShenThirteenCard
{
	class IGameTableUICallback;
	class GameLogic;

	typedef std::function<void(bool force)> ccExitCallback;

	class GameTableLogic: public HN::HNGameLogicBase
	{
		IGameTableUICallback* _uiCallback;
		ccExitCallback _exitCB;

		bool	_bSuper;
		bool	_gameStart;
		BYTE	_superStation;

	public:
		TGameBaseData TableData;

	public:
		GameTableLogic(IGameTableUICallback* uiCallback, BYTE deskNo, bool bAutoCreate);
		virtual ~GameTableLogic();

	public:
		// 是超端
		bool isSuper() const { return _bSuper; }
		const char* getCardTypeName(BYTE type);

	public:
		// 抢庄
		void sendUserRobNt(bool rob);
		// 开牌
		void sendOpenCard(INT heapCount[3], BYTE heapCard[3][5]);
		// 开始游戏（仅限房主）
		void sendBeginGame();
	public:
		// 获取游戏状态
		BYTE getGameStatus() { return _gameStatus; };

		/*
		* @func: framework message function.
		* @info: they will be called when frame message come from base class.
		*/
	protected:
		virtual void dealGameClean() override;
		virtual void dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum) override;
		virtual void dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum) override;
		virtual void dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum) override;
		virtual void dealUserAgreeResp(MSG_GR_R_UserAgree* agree) override;
		virtual void dealGameStartResp(BYTE bDeskNO) override;
		virtual void dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;
		virtual void dealGameEndResp(BYTE deskNo) override;
		virtual void dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;
		virtual void dealQueueUserSitMessage(bool success, const std::string& message) override;
		virtual void dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;
		virtual void dealUserInfoChangeResp(const UserInfoStruct* user) override;
		virtual void dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo) override;
		virtual void dealGameStationResp(void* object, INT objectSize) override;
		virtual void dealGamePlayBack(bool isback) override;
		virtual void dealUserCutMessageResp(INT userId, BYTE bDeskNO) override;

		virtual void dealOnCalculateBoard(const CalculateBoard* boardData);											/// 流局结算榜

		virtual void dealUserNotEnoughMoney() {};
		//virtual void dealUserSitOrUpError(const std::string& message);

		////////////////////////////////////////////////////////////////////
		////聊天接口
	private:
		// 文字聊天消息
		virtual void dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk) override;

		// 语音聊天消息
		virtual void dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;

		// 动作表情包消息
		virtual void dealUserActionMessage(DoNewProp* normalAction) {};

		// 赠礼消息
		//virtual void dealGiftMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;

	public:
		virtual void sendUserUp(const ccExitCallback& callback);
		virtual void sendForceQuit(const ccExitCallback& callback);
		
	public:
		void enterGame();
		//离开房间
		void sendStandUp();

		void sendleave();

		virtual void loadDeskUsersUI();

		virtual void clearDesk() override;

		virtual void clearDeskUsers() override;

		BYTE getMySeatNo() { return _mySeatNo; }

		BYTE getMyDeskNo() { return _deskNo; }

	protected:
		virtual void initParams() override;

	public:
		bool _allRoundsEnd;
		bool _isVipRoom;
		CUpGradeGameLogic _gameLogic;

	public:
		BYTE logicToViewSeatNo(BYTE lSeatNO); //重写视图转换方法
		BYTE viewToLogicSeatNo(BYTE vSeatNO);
	public:
		int _gameMode = 0;
		BYTE _Zhuang = 255;
	};
}


#endif