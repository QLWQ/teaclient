/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_GAME_HANDCARD_H__
#define __CaiShenThirteenCard_GAME_HANDCARD_H__

#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include <utility>

namespace CaiShenThirteenCard
{

	class NewThirteenCardHandCard
	{
		
		Layout*					_LightCardPanel;	//亮牌的手牌容器

		std::vector<ImageView*> _first;				//头墩
		std::vector<ImageView*> _second;			//中墩
		std::vector<ImageView*> _third;				//尾墩

		std::vector<ImageView*> _cardType;			//牌型
		std::vector<TextBMFont*> _cardScore;		//得分

	

		bool _invalid;


	public:
		NewThirteenCardHandCard();
		virtual ~NewThirteenCardHandCard();

		Layout*					_panel_handCard;	//发牌的手牌容器
		std::vector<ImageView*> _cardList;			//发牌手牌	

		//加载ui
		void doLoad(BYTE seatNo, Layout* Panel_Table);
		void doRestore();

		////////////////////////////////发牌区域////////////////////////////////////
		void sendCard(float interval);
		void sendCardOneByOne(Node* node, float interval, int idx);
		
		void isLastCard(int playerSeat, int cardNo);
		void hideAllScore();
		////////////////////////////////比牌区域////////////////////////////////////
	public:
		void sethandCardVisible(bool bShow);
		void setLightCardVisible(bool bShow);
		void lightCard(int index);
		void lightFirst();
		void lightSecond();
		void lightThird();

		void showScore(int index, bool sex, BYTE cardType, LLONG score);
		void showFirstScore(bool sex, BYTE cardType, LLONG score);
		void showSecondScore(bool sex, BYTE cardType, LLONG score);
		void showThirdScore(bool sex, BYTE cardType, LLONG score);

	public:
		//给用来比牌的图片赋值
		void loadCards(const std::vector<BYTE>& cards);
		void loadCardsTexture(ImageView* card, BYTE cardValue);
		bool isValid(BYTE value) const;

		//显示牌型
		void setCardType(const std::string& cardType, int idx);
		//显示得分
		void setScore(LLONG score, int idx);		

	private:
		std::pair<std::string, std::string> getCardTypeResource(bool sex, BYTE cardType);

	private:
		CC_SYNTHESIZE(INT, _viewSeatNo, ViewSeatNo);				//视图座位号
		CC_SYNTHESIZE(bool, _self, IsSelf);							//是否是自己
		
		int _playerCount;//当前游戏人数
		int _iLastCard;	 //给各玩家发最后一张牌
	};

}

#endif