﻿/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com
/Users/redbird/工程/liuniannian/RedBird_ChessRoom_Cpp/XingQiuYX/game/MixProject/Classes
All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenThirteenCard_MessageHead_H__
#define __CaiShenThirteenCard_MessageHead_H__

#include "HNNetExport.h"

namespace CaiShenThirteenCard
{
	enum CaiShenThirteenCard_CONFIG
	{
		GAME_NAME_ID = 12100004,	//名字ID
		PLAY_COUNT = 6,				//游戏人数
		USER_CARD_COUNT = 13,		//每个人所发扑克张数
	};

	//游戏状态定义
	enum CaiShenThirteenCard_GS
	{
		GS_WAIT_SETGAME = 0,	//等待东家设置状态
		GS_WAIT_AGREE = 1,		//等待同意设置
		GS_ROB_NT = 19,			//叫庄状态
		GS_SEND_CARD = 20,		//发牌状态
		GS_OPEN_CARD = 21,		//开牌阶段
		GS_COMPARE_CARD = 22,	//比牌阶段
		GS_WAIT_NEXT = 23,		//等待下一盘开始 
	};

	//游戏消息
	enum CaiShenThirteenCard_ASS
	{
		ASS_OUT_CARD = 50,		//出牌
		ASS_WASHCARD = 51,		//洗牌
		ASS_SEND = 52,			//发牌
		ASS_PREPARE = 53,				//准备完成
		ASS_NORMALFINISH = 54,			//游戏正常结束消息
		ASS_OUT_CARD_RESULT = 55,		//出牌结果
		ASS_SUPER = 56,		//超端验证
		ASS_COMPARE = 57,	//比牌
		ASS_FIRE = 58,		//打枪
		ASS_AUTO = 59,		//托管
		ASS_AUTO_RESULT = 60,			//托管结果
		ASS_SUPER_CONTROL = 61,			//超端控制
		ASS_SUPER_CONTROLRES = 62,		//超端控制结果

		ASS_UPDATE_CALCULATE_BOARD_SIG = 114,		//大结算
		ASS_UPDATE_REMAIN_JUSHU_SIG = 115,			//局数
		ASS_SEND_ALL_HIT = 116						//全垒打
	};
	
	//游戏指令
	enum CaiShenThirteenCard_COMMAND
	{
		S_C_IS_SUPER_USER = 10,		//通知是超端
		C_S_AUTO = 20,				//托管消息(客户端没有做托管 这个直线机器人发送过来 系统代理开牌)

		S_C_NOTICE_ROB = 50,		//通知抢庄
		C_S_USER_ROB = 51,			//玩家抢庄
		S_C_USER_ROB_RESULT = 52,	//玩家抢庄结果
		S_C_MAKE_SUER_NT = 55,		//确定庄家消息	DWJ
		S_C_SEND_CARD = 60,			//发牌消息
		S_C_NOTICE_OPEN_CARD = 70,		//通知开牌消息
		C_S_USER_OPEN_CARD = 71,		//玩家开牌消息
		S_C_USER_OPEN_CARD_RESULT = 72,	//玩家开牌结果
		S_C_COMPARE_CARD = 80,			//比牌消息
		S_C_OPEN_FIRE = 90,		//打枪消息
		S_C_GAME_END = 100,		//结算消息

		S_C_GAME_RECORD = 61,			//战绩请求消息
		S_C_GAME_RECORD_RESULT =162,		//房卡场返回战绩
		S_C_GAME_JINGBI_RECORD_RESULT = 163,  //金币房返回战绩

		C_S_CHEAT_STATUS = 130,	// 用户作弊状态
		S_C_CHEAT_STATUS = 131,	// 此协议如果不需要，可以忽略
		S_C_HEAP_CARD	 = 132,  //服务端一键搓牌消息
	};

	enum En_User_State
	{
		STATE_ERR = 0,			//错误状态  --中途加入游戏状态
		STATE_NORMAL = 1,		//正常状态
		STATE_HAVE_ROB = 2,		//已经抢庄了--玩家处于这个状态表示已经抢庄了
		STATE_OPEN_CARD = 3,	//摆牌完成等待开牌
	};


	#pragma pack(1)

	//游戏数据包客户端和服务端共有的数据
	struct	TGameBaseData
	{
		BYTE			m_byBeginTime;			//准备时间
		BYTE			m_byRobTime;			//抢庄时间
		BYTE			m_bySendCardTime;		//发牌时间
		BYTE			m_byThinkTime;			//开牌时间
		BYTE			m_byCompareTime;		//比牌时间
		BYTE			m_byCountTime;			//结算时间--结算框显示时间
		BYTE			m_byRemaindTime;		//阶段剩余时间
		int				m_iBaseFen;				//基础分

		En_User_State	m_EnUserState[PLAY_COUNT];	//玩家状态

		BYTE			m_byNowNtStation;		//当前庄家位置

		BYTE			m_byCurrRobUser;		//当前抢庄玩家

		BYTE			m_byUserCards[PLAY_COUNT][USER_CARD_COUNT];		//当局所洗的牌

		int				m_iHeapCount[PLAY_COUNT][3];	//玩家每一堆牌的数量
		BYTE			m_byHeapCard[PLAY_COUNT][3][5];	//玩家每一堆牌的数据	
		int				m_iHeapShape[PLAY_COUNT][3];	//玩家对应的牌型

		int				m_iComepareResult[PLAY_COUNT][3];//比牌结果

		int				m_iExtraScore[PLAY_COUNT];				//额外得分

		bool			m_bUserFireUser[PLAY_COUNT][PLAY_COUNT];	//打枪数据 false-标识不打抢  true-标识打枪


		LLONG			m_i64UserFen[PLAY_COUNT];		//结算得分(扣税前)
		LLONG			m_i64UserMoney[PLAY_COUNT];		//结算得分(扣税后)

		TGameBaseData()
		{
			m_byThinkTime = 255;			//游戏思考时间
			m_byRobTime = 255;			//抢庄时间
			m_byBeginTime = 255;			//游戏开始时间
			m_byRemaindTime = 255;
			m_iBaseFen = 0;
			memset(m_EnUserState, STATE_ERR, sizeof(m_EnUserState));
			m_byNowNtStation = 255;
			m_byCurrRobUser = 255;

			memset(m_byUserCards, 0, sizeof(m_byUserCards));
			memset(m_iHeapCount, 0, sizeof(m_iHeapCount));

			memset(m_byHeapCard, 255, sizeof(m_byHeapCard));
			memset(m_iHeapShape, 0, sizeof(m_iHeapShape));
			memset(m_iComepareResult, 0, sizeof(m_iComepareResult));
			memset(m_iExtraScore, 0, sizeof(m_iExtraScore));
			memset(m_bUserFireUser, false, sizeof(m_bUserFireUser));

			memset(m_i64UserFen, false, sizeof(m_i64UserFen));
			memset(m_i64UserMoney, false, sizeof(m_i64UserMoney));

		}

		//初始所有数据
		void	InitAllData()
		{
			m_byThinkTime = 255;			//游戏思考时间
			m_byRobTime = 255;			//抢庄时间
			m_byBeginTime = 255;			//游戏开始时间
			m_byRemaindTime = 255;
			m_iBaseFen = 0;
			memset(m_EnUserState, STATE_ERR, sizeof(m_EnUserState));
			m_byNowNtStation = 255;
			m_byCurrRobUser = 255;

			memset(m_byUserCards, 0, sizeof(m_byUserCards));
			memset(m_iHeapCount, 0, sizeof(m_iHeapCount));

			memset(m_byHeapCard, 255, sizeof(m_byHeapCard));
			memset(m_iHeapShape, 0, sizeof(m_iHeapShape));
			memset(m_iComepareResult, 0, sizeof(m_iComepareResult));
			memset(m_iExtraScore, 0, sizeof(m_iExtraScore));
			memset(m_bUserFireUser, false, sizeof(m_bUserFireUser));

			memset(m_i64UserFen, false, sizeof(m_i64UserFen));
			memset(m_i64UserMoney, false, sizeof(m_i64UserMoney));
		}

		//初始化部分数据
		void	InitPartData()
		{
			memset(m_EnUserState, STATE_ERR, sizeof(m_EnUserState));
			m_byNowNtStation = 255;
			m_byCurrRobUser = 255;

			memset(m_byUserCards, 0, sizeof(m_byUserCards));
			memset(m_iHeapCount, 0, sizeof(m_iHeapCount));
			memset(m_byHeapCard, 255, sizeof(m_byHeapCard));
			memset(m_iHeapShape, 0, sizeof(m_iHeapShape));
			memset(m_iComepareResult, 0, sizeof(m_iComepareResult));
			memset(m_iExtraScore, 0, sizeof(m_iExtraScore));
			memset(m_bUserFireUser, false, sizeof(m_bUserFireUser));

			memset(m_i64UserFen, false, sizeof(m_i64UserFen));
			memset(m_i64UserMoney, false, sizeof(m_i64UserMoney));
		}
	};
	/*---------------------------------------------------------------------------*/
	//超端验证消息包
	struct	S_C_SuperUser
	{
		BYTE byDeskStation;      //超端位置
		bool bIsSuper;           // 是否开通

		S_C_SuperUser()
		{
			memset(this, 0, sizeof(S_C_SuperUser));
		}
	};

	/*---------------------------------------------------------------------------*/
	//大结算消息包
	struct	CalculateBoard
	{
		bool  bWinner;				//大赢家
		int   iWinCount;			//赢的次数
		int   iDrawCount;			//平的次数
		int   iLoseCount;			//输的次数
		LLONG i64WinMoney;			//总分
		
		CalculateBoard()
		{
			memset(this, 0, sizeof(CalculateBoard));
		}
	};

	/*---------------------------------------------------------------------------*/
	//局数消息包
	struct	UpdateJuShuData
	{
		BYTE m_byRunNum;				//当前局数
		BYTE m_byZongJuShu;				//总局数

		UpdateJuShuData()
		{
			memset(this, 0, sizeof(UpdateJuShuData));
		}
	};

	/*---------------------------------------------------------------------------*/
	//游戏状态基础数据包
	struct	S_C_TGameStation_Base
	{
		BYTE			byGameStation;			//游戏状态

		BYTE			byBeginTime;		//准备时间   
		BYTE			byRobTime;			//抢庄时间
		BYTE			bySendCardTime;		//发牌时间
		BYTE			byThinkTime;		//开牌时间
		BYTE			byCompareTime;		//比牌时间
		BYTE			byCountTime;		//结算时间--结算框显示时间

		int				iBaseFen;				//基础分
		bool            bShuangWang;            //是否是双王
		bool            bSiHua;                 //是否是4花
		BYTE            iPlayerNum;             //购买桌子的人数
		BYTE            palyMode;               //玩法 1温州 2福建
		bool            bTongSha;
		bool            bJiaLiangMen;
		BYTE				DeskConfig[512];

		S_C_TGameStation_Base()
		{
			memset(this, 0, sizeof(S_C_TGameStation_Base));
		}
	};

	/*---------------------------------------------------------------------------*/
	//空闲阶段
	struct	S_C_TGameStation_Free : public S_C_TGameStation_Base
	{
		bool            bUserReady[PLAY_COUNT];			//准备状态
		S_C_TGameStation_Free()
		{
			memset(this, 0, sizeof(S_C_TGameStation_Free));
		}
	};
	/*---------------------------------------------------------------------------*/
	//抢庄阶段
	struct	S_C_GameStation_RobNt : public S_C_TGameStation_Base
	{
		BYTE	byNowNtStation;			//当前庄家位置
		int		iUserState[PLAY_COUNT];	//玩家状态
		BYTE	byRemaindTime;			//阶段剩余时间
		BYTE	byCurrRobUser;			//当前操作玩家
		S_C_GameStation_RobNt()
		{
			memset(this, 0, sizeof(S_C_GameStation_RobNt));
			byNowNtStation = 255;
		}
	};
	/*---------------------------------------------------------------------------*/
	//发牌阶段
	struct	S_C_GameStation_SendCard : public S_C_TGameStation_Base
	{
		BYTE	byNowNtStation;			//当前庄家位置
		int		iUserState[PLAY_COUNT];	//玩家状态
		BYTE	byRemaindTime;			//阶段剩余时间

		BYTE	byUserCards[USER_CARD_COUNT];		//玩家自己的牌数据 只需自己的牌数据即可 

		S_C_GameStation_SendCard()
		{
			memset(this, 0, sizeof(S_C_GameStation_SendCard));
			byNowNtStation = 255;
		}
	};
	/*---------------------------------------------------------------------------*/
	//开牌阶段
	struct	S_C_GameStation_OpenCard : public S_C_TGameStation_Base
	{
		BYTE	byNowNtStation;			//当前庄家位置
		int		iUserState[PLAY_COUNT];	//玩家状态
		BYTE	byRemaindTime;			//阶段剩余时间


		BYTE	byUserCards[USER_CARD_COUNT];		//玩家自己的牌数据 只需自己的牌数据即可 

		BYTE	byMyHeapCard[3][5];		//玩家摆牌每一堆牌的数据		DWJ
		S_C_GameStation_OpenCard()
		{
			memset(this, 0, sizeof(S_C_GameStation_OpenCard));
			byNowNtStation = 255;
		}
	};
	/*---------------------------------------------------------------------------*/
	//比牌阶段
	struct	S_C_GameStation_CompareCard : public S_C_TGameStation_Base
	{
		BYTE	byNowNtStation;			//当前庄家位置
		int		iUserState[PLAY_COUNT];	//玩家状态
		BYTE	byRemaindTime;			//阶段剩余时间

		BYTE	byHeapCard[PLAY_COUNT][3][5];	//玩家摆牌每一堆牌的数据	
		int		iHeapShape[PLAY_COUNT][3];		//玩家对应的牌型
		int		iComepareResult[PLAY_COUNT][3];	//比牌结果
		int		iExtraScore[PLAY_COUNT];				//额外得分
		S_C_GameStation_CompareCard()
		{
			memset(this, 0, sizeof(S_C_GameStation_CompareCard));
			byNowNtStation = 255;
		}
	};
	/*---------------------------------------------------------------------------*/
	//通知抢庄消息
	struct	S_C_TNoticeRobNt
	{
		BYTE			byCurrRobDesk;			//当前抢庄的玩家
		int				iUserState[PLAY_COUNT];	//玩家状态

		S_C_TNoticeRobNt()
		{
			memset(this, 0, sizeof(S_C_TNoticeRobNt));
			byCurrRobDesk = 255;
		}
	};
	//玩家抢庄消息
	struct	C_S_UserRobNt
	{
		BYTE			byDeskStation;		//抢庄的玩家
		bool			byRob;				//是否抢庄	true-抢庄 false-不抢

		C_S_UserRobNt()
		{
			memset(this, 0, sizeof(C_S_UserRobNt));
			byDeskStation = 255;
		}
	};

	struct	S_C_UserRobNt_Result
	{
		BYTE			byDeskStation;		//抢庄的玩家
		bool			byRobResult;		//抢庄结果	true-抢庄 false-不抢
		BYTE			byNowNtStation;		//当前庄家	DWJ
		S_C_UserRobNt_Result()
		{
			memset(this, 0, sizeof(S_C_UserRobNt_Result));
			byDeskStation = 255;
			byNowNtStation = 255;
		}
	};
	//轮庄确定庄家  DWJ
	struct	S_C_MakeSure_NtStation
	{
		BYTE			byNowNtStation;		//当前庄家位置 
		int				iUserState[PLAY_COUNT];	//玩家状态
		int				iUserRobNt[PLAY_COUNT];//玩家是否抢庄
		S_C_MakeSure_NtStation()
		{
			memset(this, 255, sizeof(S_C_MakeSure_NtStation));
		}
	};
	/*---------------------------------------------------------------------------*/
	//一键搓牌消息结构
	struct S_C_HeapCard
	{
		BYTE		byHeapCard[3][5];								//玩家每一堆牌的数据
		int			iHeapShape[3];									//对应的牌堆牌型
		S_C_HeapCard()
		{
			memset(this, 0, sizeof(S_C_HeapCard));
		}
	};

	struct	S_C_SendCard
	{
		BYTE		byNowNtStation;									//当前庄家的位置
		BYTE		byUserCards[USER_CARD_COUNT];					//玩家的牌数据--只发送自己的牌数据 防止看牌外挂  
		BYTE		bySendTurn[PLAY_COUNT*USER_CARD_COUNT];			//发牌顺序
		int			iUserState[PLAY_COUNT];							//玩家状态
		S_C_SendCard()
		{
			memset(this, 255, sizeof(S_C_SendCard));
			memset(iUserState, 0, sizeof(iUserState));
			memset(byUserCards, 0, sizeof(byUserCards));
		}
	};
	/*---------------------------------------------------------------------------*/
	//通知开牌消息
	struct	S_C_NoticeOpenCard
	{
		int			iUserState[PLAY_COUNT];							//玩家状态
		S_C_NoticeOpenCard()
		{
			memset(this, 0, sizeof(S_C_NoticeOpenCard));
		}
	};

	//玩家开牌
	struct	C_S_UserOpenCard
	{
		BYTE		byDeskStation;		//玩家位置
		int			iHeapCount[3];		//玩家每一堆牌的数量	
		BYTE		byHeapCard[3][5];	//玩家每一堆牌的数据	
		C_S_UserOpenCard()
		{
			memset(this, 0, sizeof(C_S_UserOpenCard));
			byDeskStation = 255;
		}
	};

	//玩家开牌结果
	struct	C_S_UserOpenCard_Result
	{

		BYTE		byDeskStation;					//玩家位置
		BYTE		byHeapCard[3][5];				//玩家自己的摆牌数据
		int			iUserState[PLAY_COUNT];			//玩家状态

		C_S_UserOpenCard_Result()
		{
			memset(this, 0, sizeof(C_S_UserOpenCard_Result));
			byDeskStation = 255;
		}
	};
	/*---------------------------------------------------------------------------*/
	//通知比牌消息
	struct	S_C_CompareCard
	{
		BYTE		byHeapCard[PLAY_COUNT][3][5];			//玩家每一堆牌的数据
		int			iHeapShape[PLAY_COUNT][3];				//对应的牌堆牌型
		int			iUserState[PLAY_COUNT];					//玩家状态
		int			iComepareResult[PLAY_COUNT][3];			//比牌结果 1-表示赢了1倍  -1标识输了1倍  2-表示赢了2倍  -2表示输了两倍 以此类推
		int			iExtraScore[PLAY_COUNT];				//额外得分
		S_C_CompareCard()
		{
			memset(this, 0, sizeof(S_C_CompareCard));
		}
	};
	/*---------------------------------------------------------------------------*/
	//打枪
	struct	S_C_OpenFire
	{
		bool	bUserFireUser[PLAY_COUNT][PLAY_COUNT];	//打枪数据 false-标识不打抢  true-标识打枪
		S_C_OpenFire()
		{
			memset(this, 0, sizeof(S_C_OpenFire));
		}
	};
	//全垒打
	struct	S_C_FireAll
	{
		BYTE bDeskStation;
		S_C_FireAll()
		{
			bDeskStation = 255;
		}
	};
	/*--------------------------------------------------------------------*/
	struct	S_C_GameResult
	{
		LLONG		i64UserFen[PLAY_COUNT];			     //得分(扣税前)
		LLONG		i64UserMoney[PLAY_COUNT];		     //改变的金币(扣税后)
		LLONG		i64UserFireScore[PLAY_COUNT];		 //打枪得分
		LLONG		i64UserAllHitScore[PLAY_COUNT];		 //全垒打得分
		BYTE        byHeapCard[PLAY_COUNT][3][5];        //摆牌数据
		int         iComepareResult[PLAY_COUNT][3];      //每道得分
		int         iExtraScore[PLAY_COUNT];             //额外得分
		S_C_GameResult()
		{
			memset(this, 0, sizeof(S_C_GameResult));
		}
	};
	//金币场当前局数之前每局的游戏输赢数据
	struct S_C_GameRecordResult
	{
		int JuCount[8][10];	//每局的玩家输赢分数
		int GameCount;

		S_C_GameRecordResult()
		{
			memset(this, 0, sizeof(S_C_GameRecordResult));
		}
	};
	//房卡场场当前局数之前每局的游戏输赢数据
	struct S_C_Game_SSS_RecordResult
	{
		int JuCount[8][24];	//每局的玩家输赢分数
		int GameCount;

		S_C_Game_SSS_RecordResult()
		{
			memset(this, 0, sizeof(S_C_Game_SSS_RecordResult));
		}
	};
	/*---------------------------------------------------------------------------*/
	//托管消息包
	struct	C_S_UserAuto
	{
		BYTE byDeskStation;      //玩家位置 
		bool bAuto;				// 是否托管

		C_S_UserAuto()
		{
			memset(this, 0, sizeof(C_S_UserAuto));
		}
	};

	/*****   作弊相关消息结构体   ****/
	struct C_S_CheatStatus
	{
		int CheatStatus;	// 0.不作弊  1.大概率尾道好牌  2.必赢
		C_S_CheatStatus()
		{
			CheatStatus = 0;
		}
	};
	struct S_C_CheatStatus
	{
		int CheatStatus;
		S_C_CheatStatus()
		{
			CheatStatus = 0;
		}
	};

	#pragma pack()
}


/********************************************************************************/
#endif
