#include "ZZHOUMJ_MahjongCard.h"
#include "ZZHOUMJ_GameManager.h"

namespace ZZHOUMJ
{

	MahjongCard::MahjongCard(void):
		_zhuanShi(nullptr)
	{
		_sum = -1;
		_number = 0;
		_sitNo = MID_DIR;
		_color = INVALID;
		_switch = false;
	}


	MahjongCard::~MahjongCard(void)
	{
	}

	void  MahjongCard::setWitch(bool swth)
	{
		_switch = swth;
	}

	MahjongCard* MahjongCard::create(mahjongCreateType type, sitDir sitNo, INT number)
	{
		MahjongCard* pCard = new MahjongCard();
		if (pCard && pCard->init(type,sitNo,number))
		{
			pCard->autorelease();
			return pCard;
		}

		CC_SAFE_DELETE(pCard);
		return nullptr;

	}

	MahjongCard* MahjongCard::create(mahjongCreateType type, sitDir sitNo)
	{
		MahjongCard* pCard = new MahjongCard();
		if (pCard && pCard->init(type,sitNo,0))
		{
			pCard->autorelease();
			return pCard;
		}

		CC_SAFE_DELETE(pCard);
		return nullptr;
	}


	bool MahjongCard::init(mahjongCreateType type, sitDir sitNo, INT number)
	{
		if (! Card::init(type, sitNo, number))
		{
			return false;
		}


		_sitNo = sitNo;
		_sum = number;
		_type = type;

		if (number == 0)
		{
			_number = 0;
			_color = INVALID;
		}
		else
		{
			_number = number % 10;
			_color = mahjongColor(number / 10);
		}

		std::string pic = getPicString(type,sitNo,number);
		_uiSp = Sprite::createWithSpriteFrameName(pic);
		_uiSp->setScale(0.9);
		this->setAnchorPoint(Vec2(0.5f,0.5f));
		this->setContentSize(_uiSp->getContentSize()*0.9);
		_uiSp->setPosition(_uiSp->getContentSize()/2);
		this->addChild(_uiSp);

		

		switch (type)
		{
			case mahjongCreateType::DI_SOUTH_STAND:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::createWithSpriteFrameName("Gold.png");
					goldCard->setPosition(Vec2(63.29, 80.64));
					goldCard->setVisible(true);
					goldCard->setScale(2.0);
					_uiSp->addChild(goldCard);
				}

			}
			break;
			case mahjongCreateType::DI_FRONT_SOUTH:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::createWithSpriteFrameName("Gold_nan.png");
					goldCard->setPosition(Vec2(38, 61));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
			break;
			case mahjongCreateType::DI_FRONT_EAST:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::createWithSpriteFrameName("Gold_dong.png");
					goldCard->setPosition(Vec2(19.83, 43.48));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
			break;
			case mahjongCreateType::DI_FRONT_NORTH:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::createWithSpriteFrameName("Gold_bei.png");
					goldCard->setPosition(Vec2(14.43, 30.60));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
			break;
			case mahjongCreateType::DI_FRONT_WEST:
			{

				if (number == GameManager::getInstance()->GetGoldCard())
				{
					Sprite *goldCard = Sprite::createWithSpriteFrameName("Gold_xi.png");
					goldCard->setPosition(Vec2(42.56, 30.15));
					goldCard->setVisible(true);
					_uiSp->addChild(goldCard);
				}
			}
		}


	

		if (_type == mahjongCreateType::DI_SOUTH_STAND)
		{
			Sprite* ting=Sprite::create(SPRITE_PATH+"img_ting.png");
			ting->setAnchorPoint(Vec2(0.0,1.0));
			ting->setPosition(Vec2(0,_uiSp->getContentSize().height-ting->getContentSize().height));
			ting->setName("ting");
			ting->setVisible(false);
			_uiSp->addChild(ting);

			Sprite* kou=Sprite::create(SPRITE_PATH+"img_kou.png");
			kou->setAnchorPoint(Vec2(1.0,0.0));
			kou->setPosition(Vec2(_uiSp->getContentSize().width,0));
			kou->setName("kou");
			kou->setVisible(false);
			_uiSp->addChild(kou);

			Sprite* huan=Sprite::create(SPRITE_PATH+"img_huan.png");
			huan->setAnchorPoint(Vec2(0.0,1.0));
			huan->setPosition(Vec2(0,_uiSp->getContentSize().height-huan->getContentSize().height));
			huan->setName("huan");
			huan->setVisible(false);
			_uiSp->addChild(huan);
		}


		Sprite* hu=Sprite::create(SPRITE_PATH+"img_hu_tips.png");
		hu->setAnchorPoint(Vec2(0.0,1.0));
		hu->setPosition(Vec2(0,_uiSp->getContentSize().height));
		hu->setName("hu");
		hu->setVisible(false);
		_uiSp->addChild(hu);
	

		_size = _uiSp->getContentSize();
		initZhuanShi();

		return true;
	}

	std::string MahjongCard::getPicString(mahjongCreateType type, sitDir sitNo, INT number)
	{
		std::string head = "";
		switch (type)
		{
		case ZZHOUMJ::mahjongCreateType::DI_SOUTH_STAND:
			head = "ZhengDa";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_EAST_STAND:
			head = "liYou";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_WEST_STAND:
			head = "liZuo";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_NORTH_STAND:
			head = "liNorthxiao";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_SOUTH_BACK:
			head = "DaoBeiS";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_EAST_BACK:
			head = "DaoBeiE";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_NORTH_BACK:
			head = "DaoBeiN";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_WEST_BACK:
			head = "DaoBeiW";
			break;

		case ZZHOUMJ::mahjongCreateType::DI_FRONT_SOUTH:
			head = "ChuSouth";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_FRONT_EAST:
			head = "PgkE";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_FRONT_NORTH:
			head = "ChuNorth";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_FRONT_WEST:
			head = "PgkW";
			break;
		case ZZHOUMJ::mahjongCreateType::DI_FRONT_ZHENG:
			head = "ZHENG";
			break;
		default:
			break;
		}


		switch (_color)
		{
		case ZZHOUMJ::mahjongColor::WAN:
			head += "Wan";
			head += StringUtils::format("%d", _number);
			break;
		case ZZHOUMJ::mahjongColor::TIAO:
			head += "Tiao";
			head += StringUtils::format("%d", _number);
			break;
		case ZZHOUMJ::mahjongColor::TONG:
			head += "Tong";
			head += StringUtils::format("%d", _number);
			break;
		case ZZHOUMJ::mahjongColor::FANG:
			{
				if (number == CMjEnum::MJ_TYPE_FD)
				{
					head += "Dong";
				}
				else if (number == CMjEnum::MJ_TYPE_FN)
				{
					head += "Nan";
				}
				else if (number == CMjEnum::MJ_TYPE_FX)
				{
					head += "Xi";
				}
				else if (number == CMjEnum::MJ_TYPE_FB)
				{
					head += "Bei";
				}
				else if (number == CMjEnum::MJ_TYPE_ZHONG)
				{
					head += "Zhong";
				}
				else if(number==CMjEnum::MJ_TYPE_FA)
				{
					head += "Fa";

				}
				else if(number==CMjEnum::MJ_TYPE_BAI)
				{
					head += "Bai";

				}
			}
		case ZZHOUMJ::mahjongColor::HUA:
			{
				if (number == CMjEnum::MJ_TYPE_FCHUN)
				{
					head += "Chun";
				}
				else if (number == CMjEnum::MJ_TYPE_FXIA)
				{
					head += "Xia";
				}
				else if (number == CMjEnum::MJ_TYPE_FQIU)
				{
					head += "Qiu";
				}
				else if (number == CMjEnum::MJ_TYPE_FDONG)
				{
					head += "HDong";
				}
				else if (number == CMjEnum::MJ_TYPE_FMEI)
				{
					head += "Mei";
				}
				else if (number == CMjEnum::MJ_TYPE_FLAN)
				{
					head += "Lan";
				}
				else if (number == CMjEnum::MJ_TYPE_FZHU)
				{
					head += "Zhu";
				}
				else if (number == CMjEnum::MJ_TYPE_FJU)
				{
					head += "Ju";
				}
			}
			break;
		default:
			break;
		}

		return head+".png";
	}


	void MahjongCard::setTingVisible(bool visible)
	{
	 	Node* node = this->getSpChildByName("ting");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::setKouVisible(bool visible)
	{
		Node* node = this->getSpChildByName("kou");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::setHuanVisible(bool visible)
	{
		Node* node = this->getSpChildByName("huan");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::setHuVisible(bool visible)
	{
		Node* node = this->getSpChildByName("hu");
		if (node)
		{
			node->setVisible(visible);
		}
	}

	void MahjongCard::initZhuanShi()
	{
		Vec2 pos = Vec2(0,0);
		switch (_sitNo)
		{
		case ZZHOUMJ::MID_DIR:
			break;
		case ZZHOUMJ::SOUTH_DIR:
			pos = Vec2(_uiSp->getContentSize().width/2,_uiSp->getContentSize().height);
			break;
		case ZZHOUMJ::EAST_DIR:
			pos = Vec2(15.0f,_uiSp->getContentSize().height/2+25.0f);
			break;
		case ZZHOUMJ::WEST_DIR:
			pos = Vec2(_uiSp->getContentSize().width/2+15.0f,_uiSp->getContentSize().height/2+25.0f);
			break;
		case ZZHOUMJ::NORTH_DIR:
			pos = Vec2(_uiSp->getContentSize().width/2,25);
			break;
		default:
			break;
		}
		//����"��ʯ"����
		_zhuanShi=Sprite::create(COCOS_PATH+"Sprite/zuanshi1.png");
		_zhuanShi->setAnchorPoint(Vec2(0.5f,0.5f));
		_zhuanShi->setPosition(pos);
		_zhuanShi->setVisible(false);
		_uiSp->addChild(_zhuanShi,100);

	}

	void MahjongCard::playZhuanShiAction()
	{
		if (_zhuanShi == nullptr)
		{
			return;
		}

		_zhuanShi->setVisible(true);
		_zhuanShi->stopAllActions();
		Sequence* beat=Sequence::create(MoveBy::create(0.4f,Vec2(0,15)),MoveBy::create(0.6f,Vec2(0,-15)),nullptr);
		RepeatForever* forever = RepeatForever::create(beat);
		_zhuanShi->runAction(forever);

	}

	void MahjongCard::stopZhuanShiAction()
	{
		if (_zhuanShi == nullptr)
		{
			return;
		}

		_zhuanShi->stopAllActions();
		_zhuanShi->setVisible(false);
		
	}

}