#ifndef _ZZHOUMJ_MELDEDKONG_H_
#define _ZZHOUMJ_MELDEDKONG_H_

#include "ZZHOUMJ_poolaction.h"

namespace ZZHOUMJ
{

	class MeldedKong :
		public PoolAction
	{
	public:
		MeldedKong(void);
		~MeldedKong(void);

		virtual void run() override;

		CREATE_FUNC(MeldedKong);
	};

}

#endif