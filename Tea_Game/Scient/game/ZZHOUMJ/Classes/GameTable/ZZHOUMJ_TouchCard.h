#ifndef _ZZHOUMJ_TOUCHCARD_H_
#define _ZZHOUMJ_TOUCHCARD_H_

#include "ZZHOUMJ_poolaction.h"

namespace ZZHOUMJ
{
	class TouchCard :
		public PoolAction
	{
	public:
		TouchCard(void);
		~TouchCard(void);

		virtual void run() override;

		CREATE_FUNC(TouchCard);
	};

}

#endif