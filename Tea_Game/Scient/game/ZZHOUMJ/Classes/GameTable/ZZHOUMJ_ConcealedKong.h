#ifndef _ZZHOUMJ_CONCEALEDKONG_H_
#define _ZZHOUMJ_CONCEALEDKONG_H_

#include "ZZHOUMJ_poolaction.h"

namespace ZZHOUMJ
{

	class ConcealedKong :
		public PoolAction
	{
	public:
		ConcealedKong(void);
		~ConcealedKong(void);

		virtual void run() override;

		CREATE_FUNC(ConcealedKong);

	};

}
#endif