#ifndef ZZHOUMJ_ActionButtonList_h__
#define ZZHOUMJ_ActionButtonList_h__

#include "cocos2d.h"
#include "ZZHOUMJ_MessageHead.h"
using namespace cocos2d;

namespace  ZZHOUMJ
{
	class ZZHOUMJ_ActionButtonList :public Node
	{
	public:
		ZZHOUMJ_ActionButtonList();
		~ZZHOUMJ_ActionButtonList();

		CREATE_FUNC(ZZHOUMJ_ActionButtonList);

	private:
		enum ACITONTAG
		{
			NOACTION =-1,
			GANG     =0,
			PENG     =1,
			CHI		 =2,
			HU       =3,
			TIANHU	=4,
			YOUJING =5,
			SHUANGYOU =6,
			SANYOU = 7,
			SANJINGDAO =8,
			QIANGJING =9,
			ZIMO =10,
			GUO =11,
			TING =12
		};

	private:
		virtual bool init();

	private:
		void clickBtEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
	public:
		void showActionBotton(bool hasHu, bool canchi,bool hasPeng, bool hasGang, bool canTing, bool canQiang, bool canDao, bool canyou, bool cantianhu, bool canzimo, char num);
	private:
		void reSetButtonPos();
	public:


	private:
		std::vector<Button*> _buttonVec;
		Node*				 _loader;
		Vec2				 _PosOrigin;
	};
}



#endif // ZZMJ_ActionButtonList_h__
