#ifndef _ZZHOUMJ_MAHJONGCARD_H_
#define _ZZHOUMJ_MAHJONGCARD_H_

#include "ZZHOUMJ_card.h"


namespace ZZHOUMJ
{
	class MahjongCard :
		public Card
	{
	public:
		MahjongCard(void);
		~MahjongCard(void);
		
		/*
		*
		* @param number				牌值，0表示牌背
		* @return					是否可以出
		*/
		static MahjongCard* create(mahjongCreateType type, sitDir sitNo, INT number);
		static MahjongCard* create(mahjongCreateType type, sitDir sitNo);

		virtual bool init(mahjongCreateType type, sitDir sitNo, INT number) override;
		virtual void setTingVisible(bool visible)override;
		virtual void setKouVisible(bool visible)override;
		virtual void setHuanVisible(bool visible)override;
		virtual void setHuVisible(bool visible)override;

		virtual void playZhuanShiAction() override;
		virtual void stopZhuanShiAction() override;
		virtual void setWitch(bool swth)override;
	private:
		std::string getPicString(mahjongCreateType type, sitDir sitNo, INT number);
		void initZhuanShi();
	private:
		cocos2d::Sprite* _zhuanShi;
		bool             _switch;
	};


}

#endif	// _HSMJ_MAHJONGCARD_H_