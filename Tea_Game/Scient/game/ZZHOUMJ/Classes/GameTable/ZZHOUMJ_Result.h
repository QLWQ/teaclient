#ifndef ZZHOUMJ_Result_h__
#define ZZHOUMJ_Result_h__
#include "ZZHOUMJ_MessageHead.h"

namespace ZZHOUMJ
{
	class MJGameResult:public HNLayer
	{
	private:
		struct CPlayerData
		{
			cocos2d::Node*			_mainNode;
			cocos2d::ui::ImageView*	_headImage;
			cocos2d::ui::Text*		_nameText;
			cocos2d::ui::Text*		_idText;
			cocos2d::Node*			_fangZhuTip;
			cocos2d::ui::Text*		_mingGangText;
			cocos2d::ui::Text*		_anGangText;
			cocos2d::ui::Text*		_zimoText;
			cocos2d::ui::Text*		_dianpaoText;
			cocos2d::ui::Text*		_jiepaoText;
			cocos2d::ui::Text*		_allRoomScoreText;
			cocos2d::Node*			_curMainNode;
			cocos2d::Node*			_curCardNode;
			cocos2d::ui::Text*		_curNameText;
			cocos2d::ui::Text*		_curTipText;
			cocos2d::ui::TextAtlas*	_curGangFenText;
			cocos2d::ui::TextAtlas*	_curZongFenText;
			cocos2d::ui::TextAtlas*	_curHuanFenText;
			cocos2d::ui::TextAtlas*	_curHuaFenText;
			cocos2d::Vec2			_curCardNodePos;
			cocos2d::Node*			_xiangxiLayer;
			cocos2d::ui::ImageView*	 _curIcon;
			cocos2d::ui::ImageView*	 _img_title;
			cocos2d::ui::TextAtlas*	_lab_num;
			cocos2d::ui::ImageView*	_img_zhuang;
			float					_curCardScale;
		};
	public:
		MJGameResult();
		~MJGameResult();

		virtual bool init();
		CREATE_FUNC(MJGameResult);
		
		void setShowUserInfo(UserInfoStruct* pUserInfo, bool isfangzhu, int allRoomScore,sitDir dir);
		void setShowBeiLvInfo(UserInfoStruct* pUserInfo, int zimoNum, int dianpaoNum, int jiepaoNum, int mingGangNum, int anGangNum,sitDir dir);
		void setShowCurCardArray(UserInfoStruct* pUserInfo, const char* pCardArray, char cardCount, const char pActArray[][10], char actCount, char hupaiCardId,sitDir dir);
		void setShowCurFen(UserInfoStruct* pUserInfo, bool isZimo, bool isTianhu, bool IsSanjindao, bool isQiangjin, int huanFen, int gangFen, int zongFen, sitDir dir, bool isYoijin[3], int huafen, char toHuPaiCardId, bool isZhuang);
		void ShowReSoultTitle(char hutype);

		void StartAction(char cardId, std::function<void()> func);
		void HideAll();
		void onChickjieCallBack(Ref* pSender, Widget::TouchEventType type);
		void showAllResultNode(bool isvisible);
	private:   
		void clearPlayerData();
		
	private:
		cocos2d::Node*				m_pZongChengJiNode;
		cocos2d::Node*				m_pCurChengJiNode;
		std::vector<CPlayerData*>	m_PlayerDataVec;

		std::vector<Node*>	_resultList;
		Sprite*				_bgSprite;
		bool				_IsShowCard;
		bool				_isGameEnd = false;
	};

}



#endif //ZZHOUMJs_Result_h__



