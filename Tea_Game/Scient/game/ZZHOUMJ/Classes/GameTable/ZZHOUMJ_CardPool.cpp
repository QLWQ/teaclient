#include "ZZHOUMJ_CardPool.h"
#include "ZZHOUMJ_PoolAction.h"
#include "ZZHOUMJ_GameManager.h"

namespace ZZHOUMJ
{

	/////////////////各个位置初始化位置////////////////////////////


	 //摆放已经出了的牌位置
	 Vec2 CardPool::SounthDirOutCardPos = Vec2(540, 320);
	 Vec2 CardPool::WestDirOutCardPos = Vec2(480, 500);
	 Vec2 CardPool::EastDirOutCardPos = Vec2(821, 379);
	 Vec2 CardPool::NorthDirOutCardPos = Vec2(780, 600);

	
	 Vec2 CardPool:: SounthPGActionCardPos	= Vec2(640,240);
	 Vec2 CardPool:: WestPGActionCardPos	= Vec2(370,400);
	 Vec2 CardPool:: EastPGActionCardPos	= Vec2(920,400);
	 Vec2 CardPool:: NorthPGActionCardPos	= Vec2(640,560);

	//////////////////////////////////////////////////////////////////////////

	 static bool sortCardValue_Static(INT a, INT b)
	 {
		if (a == 37)
		{
			a = GameManager::getInstance()->GetGoldCard();
		}
		if (b == 37)
		{
			b = GameManager::getInstance()->GetGoldCard();
		}
		INT number1 = a % 10;
		INT number2 = b % 10;

		if (number1<number2)
		{
			return true;
		}
		else
		{
			return false;
		}

	
	 }


	CardPool::CardPool(void):
		_handCard(nullptr)
	{
		_isInChangeState = false;
	}


	CardPool::~CardPool(void)
	{
		ClearGroupData();
	}

	void CardPool::addHandCard(INT cardValue)
	{
		sortCard();

		//最后捉的手牌不排序，让他放在最后
		_handCardList.push_back(cardValue);
		setHandCardPos(cardValue);

	}

	void CardPool::sortCard()
	{
		//查找出相同花色牌
		std::vector<INT> gold;
		std::vector<INT> tiao;
		std::vector<INT> tong;
		std::vector<INT> feng;
		std::vector<INT> wan;
		std::vector<INT> other;

		for (auto v:_handCardList)
		{
			mahjongColor color = mahjongColor(v / 10);
			if (v != GameManager::getInstance()->GetGoldCard() && v != 37)
			{
				switch (color)
				{
				case mahjongColor::WAN:
					wan.push_back(v);
					break;
				case mahjongColor::TIAO:
					tiao.push_back(v);
					break;
				case mahjongColor::TONG:
					tong.push_back(v);
					break;
				case mahjongColor::FANG:
					feng.push_back(v);
					break;
				default:
					other.push_back(v);
				}
			}
			else if (v == 37)
			{
				mahjongColor goldColor = mahjongColor(GameManager::getInstance()->GetGoldCard() / 10);
				switch (goldColor)
				{
				case mahjongColor::WAN:
					wan.push_back(v);
					break;
				case mahjongColor::TIAO:
					tiao.push_back(v);
					break;
				case mahjongColor::TONG:
					tong.push_back(v);
					break;
				case mahjongColor::FANG:
					feng.push_back(v);
					break;
				default:
					other.push_back(v);
				}
			}
			else
			{
				gold.push_back(v);
			}
			
		}

		std::sort(gold.begin(), gold.end(), sortCardValue_Static);
		std::sort(wan.begin(),wan.end(),sortCardValue_Static);
		std::sort(tiao.begin(),tiao.end(),sortCardValue_Static);
		std::sort(tong.begin(),tong.end(),sortCardValue_Static);
		std::sort(feng.begin(),feng.end(),sortCardValue_Static);		
		std::sort(other.begin(),other.end(),sortCardValue_Static);


		//整合所有组
		_handCardList.clear();
		for (auto v:gold)
		{
			_handCardList.push_back(v);
		}
		for (auto v:wan)
		{
			_handCardList.push_back(v);
		}
		for (auto v:tiao)
		{
			_handCardList.push_back(v);
		}

		for (auto v:tong)
		{
			_handCardList.push_back(v);
		}

		for (auto v:feng)
		{
			_handCardList.push_back(v);
		}
		for (auto v:other)
		{
			_handCardList.push_back(v);
		}


	}


	void CardPool::moveOneCardToLast(int card)
	{
		std::vector<int>::iterator itr = std::find(_handCardList.begin(),_handCardList.end(),card);
		if (itr != _handCardList.end())
		{
			_handCardList.erase(itr);
			_handCardList.push_back(card);
		}
	}

	void CardPool::sendSomeCard(const std::vector<INT> vec)
	{
		for (auto vv: vec)
		{
			_handCardList.push_back(vv);
		}
		sortCard();
		refreshAllShowCard();
	}


	bool CardPool::init()
	{
		if (!Layer::init())
		{
			return false;
		}
		ClearGroupData();
		
		m_iMaxZOrder =  100;
		
		_dir = sitDir::MID_DIR;
		_sendCardCount = 0;
		_isTingState= false;
		return true;
	}


	const sitDir& CardPool::getSitDir()
	{
		return _dir;
	}

	void CardPool::addSomeOutCards(const INT& count, const INT& number, CGroupCardType cgtype, bool hideLastOutCard, char chiCardArray[CHICARDARRAY])
	{
	
		if (hideLastOutCard)  // 隐藏出的牌
		{
			auto card = GameManager::getInstance()->getLastOutCard();
			
			if (card != nullptr)
			{
				GameManager::getInstance()->setMinusOutCount(card->getCardOwner());
			}
			GameManager::getInstance()->removeLastOutCard();
		}
		addGoupGangPeng(count, number, cgtype, chiCardArray);
	}


	void CardPool::finishGame()
	{
		reSetData();
		m_GroupCardDataVec.clear();
		_changeCardList.clear();

		_handCard->setLeftChangeCard(_changeCardList);
	}


	void CardPool::reSetData()
	{
		_sendCardCount = 0;
		
		_isTingState= false;
		_handCardList.clear();
		
		ClearGroupData();
		refreshAllShowCard();
	}

	bool CardPool::addGoupGangPeng(int count, int cardId, CGroupCardType cgtype, char chiCardArray[CHICARDARRAY])
	{
		bool temp = refreshGoupCardCount(cardId, cgtype, count, chiCardArray);
		if (temp)
		{
			refreshAllShowCard();
		}
		return temp;
	}


	bool CardPool::refreshGoupCardCount(int cardId, CGroupCardType cgtype, int setnum, char chiCardArray[CHICARDARRAY])
	{
		if(setnum<=0) return false;
		for (auto iter = m_GroupCardDataVec.begin(); iter != m_GroupCardDataVec.end(); iter++)
		{
			CGroupCardData* pData = *iter;
			if (pData->_iCardId==cardId && pData->_iType==cgtype)
			{
				if (pData->_iCount>=setnum) return false;
				pData->_iCount = setnum;
				return true;
			}
		}

		for (auto iter = m_GroupCardDataVec.begin(); iter != m_GroupCardDataVec.end(); )
		{
			CGroupCardData* pData = *iter;
			if (pData->_iCardId == cardId &&pData->_iType == CGroupCard_Peng&&cgtype == CGroupCard_MingGang&&CGroupCard_Chi != cgtype)
			{
				delete pData;
				iter=m_GroupCardDataVec.erase(iter);
			}
			else
			{
				iter++;
			}
		}
		CGroupCardData* pData = new CGroupCardData();
		pData->_iType = cgtype;
		pData->_iCardId = cardId;
		pData->_iCount = setnum;

		if (CGroupCard_Chi == cgtype)
		{
			for (int i = 0; i < CHICARDARRAY; i++)
			{
				pData->_iChiCardArray[i] = chiCardArray[i];
			}
		}

		m_GroupCardDataVec.push_back(pData);
		return true;
	}

	void CardPool::ClearGroupData()
	{
		for (auto iter = m_GroupCardDataVec.begin(); iter != m_GroupCardDataVec.end(); iter++)
		{
			delete *iter;
		}
		m_GroupCardDataVec.clear();
	}


	void CardPool::refreshHandCardValue(std::vector<INT> v)
	{
		
		_handCardList.clear();
		for (auto temp:v)
		{
			_handCardList.push_back(temp);
		}
		sortCard();
		refreshAllShowCard();
	}


	int CardPool::getZhuaPaiZOrder()
	{
		return m_iMaxZOrder;
	}

	std::vector<INT> CardPool::getHandCardList()
	{
		return _handCardList;
	}

	void CardPool::moveChangeCard(sitDir dir)
	{
		_handCard->moveChangeCard(dir);
	}

	void CardPool::enterChangeCards()
	{
		this->setIsInChangeState(true);
	}


	void CardPool::exitChangeCards(int changeSezi,std::vector<INT>outCard,std::vector<INT> inCard,std::vector<INT> handCardList)
	{
		this->setIsInChangeState(false);

		sitDir index = _dir;
		//计算移动
		switch (changeSezi)
		{
		case 1:
		case 5://顺时针交换
			{
				index = sitDir((_dir + 3) % PLAYER_COUNT);
				break;
			}
		case 2:
		case 6://对家交换
			{
				index = sitDir((_dir + 2) % PLAYER_COUNT);
				break;
			}
		case 3:
		case 4://逆时针交换
			{
				index = sitDir((_dir + 1) % PLAYER_COUNT);
				break;
			}
		default:
			break;
		}

		_handCardList.clear();
		for (auto v:handCardList)
		{
			_handCardList.push_back(v);
		}
		
		for (auto v:inCard)
		{
			_changeCardList.push_back(v);
		}

		_handCard->setLeftChangeCard(inCard);

		sortCard();
		this->moveChangeCard(index);
		//refreshAllShowCard();
		DelayTime* dealy = DelayTime::create(3.0f);
		auto callBack = CallFunc::create(CC_CALLBACK_0(CardPool::refreshAllShowCard,this));
		this->runAction(Sequence::create(dealy,callBack,nullptr));


	}

	void CardPool::changeCardToHead(std::vector<INT> cards)
	{

	}

}