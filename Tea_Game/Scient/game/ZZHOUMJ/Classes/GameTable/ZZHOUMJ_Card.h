#ifndef _ZZHOUMJ_CARD_H_
#define _ZZHOUMJ_CARD_H_

#include "cocos2d.h"
#include "ZZHOUMJ_MessageHead.h"
#include "ZZHOUMJ_ResourceLoader.h"

namespace ZZHOUMJ
{

	class Card :
		public cocos2d::Node
	{
	public:
		Card(void);
		~Card(void);

		virtual bool init(mahjongCreateType _type, sitDir sitNo, INT number) = 0;         // 初始化卡牌的属性
	
		// set 属性
		void setCardPos(const Vec2& pos);																	// 设置卡牌的位置
		void setCardZorder(const int zorder);																// 设置卡牌的localZorder
		void setCardOwner(const sitDir& dir);																// 设置卡牌的使用者
		void setCardEnableTouch(bool touch = true);															// 设置卡牌的触摸开关
		void setCardTouchEvent();                                                                           // 设置卡牌的触摸属性
		void setCardColor(Color3B color);																	//设置牌颜色
		void setCardNum(const INT&  number);																
		// get 属性
		Size getCardSize();																				// 获取卡牌的尺寸
		Vec2 getCardPos() { return this->getPosition(); }												// 获取卡牌的位置
		sitDir getCardOwner() { return _sitNo; };														// 获取卡牌的使用者
		INT getCardNumber();																			// 获取卡牌的点数
		mahjongColor getCardColor();																	// 获取卡牌的花色
		INT getCardZorder();																			// 获取卡牌的localZoder值
		INT getCardSumNumber() { return _sum; }															// 获取卡牌的编码值
		void setIsChange(bool isChange);
		bool getIsChange();
		void setChangeColor();
	public:
		cocos2d::Node* getSpChildByName(const std::string& name);
		virtual void setTingVisible(bool visible){};
		virtual void setKouVisible(bool visible){};
		virtual void setHuanVisible(bool visible){};
		virtual void setHuVisible(bool visible){};

		virtual void playZhuanShiAction() {};
		virtual void stopZhuanShiAction() {};
		virtual void  setWitch(bool swth){};
		bool touchBegin(Touch*touch, Event*tevent);
		void touchMove(Touch*touch, Event*tevent);
		void touchEnd(Touch*touch, Event*tevent);
	protected:
		cocos2d::Sprite*	_uiSp;																									// 界面显示
		mahjongCreateType	_type;																									// 创建类型
		INT					_number;																								// 点数
		sitDir				_sitNo;																									// 使用者
		mahjongColor		_color;																									// 花色    
		Size				_size;																									// 尺寸   
		bool				_canTouch;																								// 触摸
		bool				_isChange;																								// 是否是交换的牌
		INT					_cardZorder;																							// localZoder
		INT					_sum;                                                                                                   // 编码值
		CC_SYNTHESIZE(bool, _isSelect, Select);	//被选中
		Vec2                _porePos;
		
	};

}
#endif // _CARD_H_