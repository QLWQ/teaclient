#ifndef _ZZHOUMJ_FACTORYCARDPOOL_H_
#define _ZZHOUMJ_FACTORYCARDPOOL_H_

#include "ZZHOUMJ_CardPool.h"
#include "ZZHOUMJ_MahjongCardPool.h"
#include "ZZHOUMJ_SouthMahjongCardPool.h"
#include "ZZHOUMJ_WestMahjongCardPool.h"
#include "ZZHOUMJ_EastMahjongCardPool.h"
#include "ZZHOUMJ_NorthMahjongCardPool.h"

#include "ZZHOUMJ_TouchCard.h"
#include "ZZHOUMJ_MeldedKong.h"
#include "ZZHOUMJ_ConcealedKong.h"
#include "ZZHOUMJ_TouchKong.h"
#include "ZZHOUMJ_HuCard.h"
#include "ZZHOUMJ_EatCard.h"

namespace ZZHOUMJ
{

	class Factory
	{
	public:
		static CardPool* createEastPool(INT count);
		static CardPool* createWestPool(INT count);
		static CardPool* createSouthPool(INT count);
		static CardPool* createNorthPool(INT count);

		static PoolAction* createTouchCardAction();
		static PoolAction* createMeldedKongAction();
		static PoolAction* createConcealedKongAction();
		static PoolAction* createTouchKongAction();
		static PoolAction* createTouchEatAction();
		static PoolAction* createHuCardAction();
	};

}

#endif