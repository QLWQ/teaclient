#include "ZZHOUMJ_GameManager.h"
#include "SimpleAudioEngine.h"
#include "ZZHOUMJ_GMLayer.h"
#include "ZZHOUMJ_ActionButtonList.h"

namespace ZZHOUMJ
{
	Point GameManager::g_bezControl[bezDir::BEZ_DIR_MAX][2] = {
		Vec2(856,536),Vec2(1085,473), 
		Vec2(1014,169),Vec2(851,102),
		Vec2(451,140),Vec2(334,192),
		Vec2(281,451),Vec2(378,540)
	};
	GameManager* GameManager::_instance = nullptr;

	GameManager::GameManager(void):
		_turnTable(nullptr),
		_PlayerWay(nullptr),
		_HuaSwitch(true),
		_chiTip(nullptr)
	{
		_instance = this;
		_isHasAction = false;
		_leftCardCount = 78;
		_isCatchCard=false;
		buhutip = NULL;
		_ways="";
		_isHasChiAction = false;
		memset(_vecUser,0,sizeof(_vecUser));
		memset(_ziMoOrHu,0,sizeof(_ziMoOrHu));
		memset(_actionPoint,0,sizeof(_actionPoint));
		isShow = false;
		isSHowFlowe = false;
		_chiPaiCard = 0;
		_IsPutongHua = false;
	}

	GameManager::~GameManager(void)
	{
		ResourceLoader::clearAll();																							
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();							
		SpriteFrameCache::getInstance()->removeSpriteFramesFromFile(PLIST_PATH + "resource.plist");
	}

	bool GameManager::init()
	{
		if (!Layer::init())
		{
			return false;
		}
		// 资源初始化
		setGlobalScale();
		this->setScale(_globalScale * 1.5f);
		loadResource();

		_PlayerWay = Label::createWithTTF("","platform/common/RTWSYueRoudGoG0v1-Regular.ttf",23);
		_PlayerWay->setPosition(Vec2(150, 670));
		_PlayerWay->setColor(Color3B(0,255,255));
		addChild(_PlayerWay,100);

		_tuoguanBtn = Button::create("Games/ZZHOUMJ/sprite/btn_trusteeship.png", "Games/ZZHOUMJ/sprite/btn_trusteeship.png", "");
		_tuoguanBtn->addTouchEventListener(CC_CALLBACK_2(GameManager::clickTuoguanEventCallBack, this));
		_tuoguanBtn->setPosition(Vec2(1310, 670));
		_tuoguanBtn->setVisible(true);
		_tuoguanBtn->setTouchEnabled(false);
		addChild(_tuoguanBtn, 1000);

		_tingpaiTipBtn = Button::create("Games/ZZHOUMJ/sprite/btn_ting.png", "Games/ZZHOUMJ/sprite/btn_ting.png", "");
		_tingpaiTipBtn->addTouchEventListener(CC_CALLBACK_2(GameManager::clickTingPaiTipEvenCallBack, this));
		_tingpaiTipBtn->setPosition(Vec2(1230, 330));
		_tingpaiTipBtn->setVisible(false);
		addChild(_tingpaiTipBtn, 101);

		_tingAction = CSLoader::createNode(COCOS_PATH + "showTingItem.csb");
		_tingAction->setVisible(false);
		_tingAction->setPosition(Vec2(640,145));
		auto ation = CSLoader::createTimeline(COCOS_PATH + "showTingItem.csb");
		_tingAction->runAction(ation);
		ation->gotoFrameAndPlay(0, true);
		addChild(_tingAction, 1002);

		return true;
	}

	bool GameManager::SetOrGetHuaSwitch(bool type,bool swit)
	{
		if(!type)
		{
			_HuaSwitch = swit;
		}
        return _HuaSwitch;
	}
	
	GameManager* GameManager::getInstance()
	{
		if (_instance == nullptr)
		{
			_instance = GameManager::create();
		}
		return _instance;
	}

	void GameManager::loadResource()
	{
	
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile(PLIST_PATH + "resource.plist");
		initData();
		addLoader();
		initButton();
		initUI();

		this->schedule(schedule_selector(GameManager::playBgTimer), 5.0f);
	}

	void GameManager::initData()
	{
		_startDir = sitDir::MID_DIR;    // 庄家
		_curSendDir = sitDir::MID_DIR;    // 发牌方向
		_vecTingPaiState.assign(PLAY_COUNT,false);
// 		_buhuaPos[0] = 129;
// 		_buhuaPos[1] = 170;
// 		_buhuaPos[2] = -42;
// 		_buhuaPos[3] = -30;
		memset(_buhuaPos, 0, sizeof(_buhuaPos));
	}

	void GameManager::initUser(const sitDir& dir,const UserInfoStruct* user)
	{
		if (user==nullptr)
		{
			return;
		}

		COCOS_NODE(Sprite,StringUtils::format("player%d",dir))->setVisible(true);
		COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", dir))->setVisible(false);
		COCOS_NODE(Text, StringUtils::format("name%d", dir))->setString(GBKToUtf8(user->nickName));
		if (user->bUserState == USER_CUT_GAME )
		{
			COCOS_NODE(Text, StringUtils::format("lixian%d", dir))->setZOrder(20000);
			COCOS_NODE(Text, StringUtils::format("lixian%d", dir))->setVisible(true);
		}
		else
		{
			COCOS_NODE(Text, StringUtils::format("lixian%d", dir))->setVisible(false);
		}
		refreshUserMoney(dir,*user);

	}

	
	void GameManager::restartSetData()
	{
		//memset(_actionPoint,0,sizeof(_actionPoint));
		_sendCardCount = 0;
		_goldCard = 0;
		_vvStartHandCard.clear();
		_vecHasOutCardCount.assign(PLAY_COUNT, 0);
		
		for (auto i = 0; i < PLAY_COUNT; i++)
		{
			
			COCOS_NODE(Sprite, StringUtils::format("ready%d", i))->setVisible(false);
			COCOS_NODE(Sprite, StringUtils::format("Mai%d", i))->setVisible(false);
			COCOS_NODE(Node, StringUtils::format("hua_tip%d", i))->removeAllChildren();
		}
		COCOS_NODE(Sprite, "waiting")->setVisible(false);
		isSHowFlowe = true;
		COCOS_NODE(ImageView, "jingdikuang")->setVisible(false);
		HNAudioEngine::getInstance()->playBackgroundMusic(("Games/ZZHOUMJ/Music/background/waiting.mp3"));
		_leftCardCount = 78;
		_isHasChiAction = false;
		_HuaSwitch = true;
		
		if (_chiTip)
		{
			_chiTip->setVisible(false);
			_chiTip->removeFromParent();
			_chiTip = nullptr;
		}
		_tingpaiTipBtn->setVisible(false);
		_tingAction->setVisible(false);
		if (_tingLayer)
		{
			_tingLayer->setVisible(false);
			_tingLayer->removeFromParent();
			_tingLayer = nullptr;
		}
		_turnTable->setLeftCardVisble(false);
		isPlayerGame = false;
		loadUsers();
		initData();
	}

	void GameManager::addLoader()
	{
		// 创建资源管理器
		_cLoader = cocosResourceLoader::create(COCOS_PATH + "MainScene.csb");
		this->addChild(_cLoader);
		auto cancleNode = CSLoader::createNode(COCOS_PATH + "cancelTuoguanNode.csb");
		_canclePanle = (Layout*)cancleNode->getChildByName("Panel_1");
		_canclePanle->setVisible(false);
		this->addChild(cancleNode, 2003);
	}


	void GameManager::startButtonClickCallBack(Ref* ref,Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			{
				GTLogic()->sendAgreeGame();
				resetInit();
				agreeGame();
				_turnTable->setLeftCardVisble(false);
				_turnTable->setCardCount(136);
				isPlayerGame = false;
				break;
			}
		default:
			break;
		}
	}


	void GameManager::pengButtonClickCallBack(Ref* ref,Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			{
				resetDiKuang();
				GameProtoC2SActionEat_t toProtData;
				toProtData.actionType = 2;
				memset(toProtData.chiCardArray, 0, sizeof(toProtData.chiCardArray));
				toProtData.chiCardCount = 0;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ActionEat, &toProtData, sizeof(toProtData));
				break;
			}
		default:
			break;
		}
	}

	void GameManager::gangButtonClickCallBack(Ref* ref,Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			{
				resetDiKuang();
				
				GameProtoC2SActionEat_t toProtData;
				toProtData.actionType = 3;
				memset(toProtData.chiCardArray, 0, sizeof(toProtData.chiCardArray));
				toProtData.chiCardCount = 0;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ActionEat, &toProtData, sizeof(toProtData));
			}
		default:
			break;
		}
	}

	void GameManager::huButtonClickCallBack(Ref* ref,Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			{
				resetDiKuang();

				GameProtoC2SHuPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_HuPai, &toProtData, sizeof(toProtData));
				break;
			}
		default:
			break;
		}
	}

	int GameManager::getRd_A_N(int a, int n)
	{
		srand(time(nullptr));
		auto x = int(rand() % (n+1));
		if (x < a)
		{
			x = a;
		}
		return x;
	}

	void GameManager::playBgTimer(float dt)
	{
		if (/*GTLogic()->isGamePlaying() && */!CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
		{
			auto i = getRd_A_N(1, 5);
			HNAudioEngine::getInstance()->playBackgroundMusic((MUSIC_BG_PATH + StringUtils::format("bg%d.mp3", 1)).c_str());
		}
	}

	void GameManager::initButton()
	{
		// 设置可视可用
		resetDiKuang();

		// 设置回调函数
		COCOS_NODE(Button, "start")->addTouchEventListener(CC_CALLBACK_2(GameManager::startButtonClickCallBack, this));
		COCOS_NODE(Button, "peng")->addTouchEventListener(CC_CALLBACK_2(GameManager::pengButtonClickCallBack, this));
		COCOS_NODE(Button, "gang")->addTouchEventListener(CC_CALLBACK_2(GameManager::gangButtonClickCallBack, this));
		COCOS_NODE(Button, "hu")->addTouchEventListener(CC_CALLBACK_2(GameManager::huButtonClickCallBack, this));
		COCOS_NODE(Button, "qi")->addTouchEventListener(CC_CALLBACK_2(GameManager::qiButtonClickCallBack, this));
		COCOS_NODE(Button, "ting")->addTouchEventListener(CC_CALLBACK_2(GameManager::tingButtonClickCallBack, this));
		auto cancleBtn = (Button*)_canclePanle->getChildByName("Button_cancelAuto");
		cancleBtn->addTouchEventListener(CC_CALLBACK_2(GameManager::quxiaotuoguanClickCallBack, this));
	}

	void GameManager::tingButtonClickCallBack(Ref* ref,Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
			{
				resetDiKuang();
				GameProtoC2STingPai_t toProtData;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_TingPai, &toProtData, sizeof(toProtData));
				break;
			}
		default:
			break;
		}
	}

	void GameManager::quxiaotuoguanClickCallBack(Ref* ref, Widget::TouchEventType type)
	{
		if (type != Widget::TouchEventType::ENDED)
		{
			return;
		}
		userAuto(false, false, _isHasAction);
	}

	void GameManager::initUI()
	{
		// 麻将
		this->setScale(_globalScale);
		_vecHasOutCardCount.assign(PLAY_COUNT, 0);

		reParent("dikuang");
		
		auto pos = COCOS_NODE(Button, "start")->getPosition();
		COCOS_NODE(Button, "start")->removeFromParent();
		this->addChild(COCOS_NODE(Button, "start"));
		COCOS_NODE(Button, "start")->setLocalZOrder(99998);
		COCOS_NODE(Button, "start")->setPosition(pos);

		_turnTable = ZZHOUMJ_TurnTable::create();
		_turnTable->setTurnInitDir(MID_DIR);
		this->addChild(_turnTable,99);

		//showMeAction(true, true, true, true, true, true, true, true, true, true, 1);
		auto southPool	= Factory::createSouthPool(1);
	    auto eastPool	= Factory::createEastPool(1);
		auto northPool	= Factory::createNorthPool(1);
		auto westPool	= Factory::createWestPool(1);
		
		// 添加4个方向的牌池给底牌,顺序和sitDir保持一致
		addCardPool(southPool);
		addCardPool(eastPool);
		addCardPool(northPool);
		addCardPool(westPool);

		// 添加打牌的动作
		_touchCard = Factory::createTouchCardAction();
		this->addChild(_touchCard,topZorder);
		_meldedKong = Factory::createMeldedKongAction();
		this->addChild(_meldedKong,topZorder);
		_concealedKong = Factory::createConcealedKongAction();
		this->addChild(_concealedKong,topZorder);
		_touchKong = Factory::createTouchKongAction();
		this->addChild(_touchKong,topZorder);
		_touchEat = Factory::createTouchEatAction();
		this->addChild(_touchEat, topZorder);
		_huCard = Factory::createHuCardAction();
		this->addChild(_huCard,topZorder);

	}

	void GameManager::reParent(std::string name)
	{
		auto pos = COCOS_NODE(Sprite, name)->getPosition();
		COCOS_NODE(Sprite, name)->removeFromParentAndCleanup(false);
		this->addChild(COCOS_NODE(Sprite, name));
		COCOS_NODE(Sprite, name)->setLocalZOrder(topZorder);
		COCOS_NODE(Sprite, name)->setPosition(pos);
	}


	void GameManager::resetDiKuang()
	{
		if (this->getChildByName("buttonNode"))
		{
			this->removeChildByName("buttonNode");
		}
		if (_chiTip)
		{
			_chiTip->setVisible(false);
			_chiTip->removeFromParent();
			_chiTip = nullptr;
		}
		setIsHasAction(false);
	}

	void GameManager::addUser(const sitDir& dir,UserInfoStruct* user)
	{
		int index = int(dir);
		if (index>=PLAY_COUNT)
		{
			return;
		}
		_vecUser[index] = user;
		initUser(dir,user);
		COCOS_NODE(Sprite, StringUtils::format("player%d", dir))->setVisible(true);
		COCOS_NODE(Sprite, StringUtils::format("zhuang%d", dir))->setVisible(false);
		COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", dir))->setVisible(false);
		if (user->bUserState == USER_ARGEE /*&& !GTLogic()->isGamePlaying()*/)
		{
			COCOS_NODE(Sprite, StringUtils::format("ready%d", dir))->setVisible(true);
		}
		else
		{
			COCOS_NODE(Sprite, StringUtils::format("ready%d", dir))->setVisible(false);
		}

		if(nullptr == COCOS_NODE(ImageView,StringUtils::format("head%d", dir))) return;
// 		int headId = user->bLogoID;
// 		std::string headPic = HeadManager::getHeadImage(user->bBoy, headId);
// 		COCOS_NODE(ImageView, StringUtils::format("head%d", dir))->loadTexture(headPic);
// 		std::string homePage = user->headUrl;
// 		VipHeadSprite* sprite = VipHeadSprite::create(COCOS_NODE(ImageView, StringUtils::format("head%d", dir)), user->dwUserID, homePage, user->iVipTime);
// 		sprite->setPosition(COCOS_NODE(ImageView, StringUtils::format("head%d", dir))->getPosition());
// 		this->addChild(sprite);
		GameUserHead*	userHead = nullptr;
		userHead = GameUserHead::create(COCOS_NODE(ImageView, StringUtils::format("head%d", dir)));
		userHead->show();
		std::string name = user->bBoy ? Player_Normal_M : Player_Normal_W;
		userHead->loadTexture(name);
		if (user)
		{
			userHead->loadTextureWithUrl(user->headUrl);
		}
		refreshUserMoney(dir,*user);
	}


	void GameManager::afterScorePoint(const sitDir& dir, int money)
	{
		INT station = GTLogic()->getUserStation(dir);
		if (station>=PLAY_COUNT)
		{
			return;
		}

		_actionPoint[station]+=money;
		UserInfoStruct* userinfo=_vecUser[dir];
		if (userinfo == nullptr)
		{
			return;
		}

		refreshUserMoney(dir,*userinfo);

	}

	void GameManager::userLeave(const sitDir& dir)
	{
	
		int index = int(dir);
		if (index>=PLAY_COUNT)
		{
			return;
		}

		_vecUser[index] = nullptr;
		COCOS_NODE(Text, StringUtils::format("name%d", dir))->setString("");
		COCOS_NODE(Text, StringUtils::format("money%d", dir))->setString("");
		COCOS_NODE(Sprite,StringUtils::format("player%d",dir))->setVisible(false);
		COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", dir))->setVisible(false);
	}

	

	void GameManager::userAgree(const sitDir& dir)
	{
		if (dir == sitDir::SOUTH_DIR)
		{
			/*if (GTLogic()->isGamePlaying())
			{
				return ;
			}*/
			
			agreeGame();
		
		}

		COCOS_NODE(Sprite, StringUtils::format("ready%d", dir))->setVisible(true);

	}

	void GameManager::startGame()
	{
		COCOS_NODE(Sprite, "waiting")->setVisible(false);
		COCOS_NODE(Button, "start")->setVisible(false);
		COCOS_NODE(ImageView, "jingdikuang")->setVisible(false);

		for (auto i = 0; i < PLAY_COUNT; i++)
		{
			COCOS_NODE(Sprite, StringUtils::format("ready%d", i))->setVisible(false);
		}
		_tuoguanBtn->setVisible(true);
		_tuoguanBtn->setTouchEnabled(true);
	}

	void GameManager::playGame()
	{

		COCOS_NODE(Sprite, "waiting")->setVisible(false);
		startOutCard(_startDir);												// 第一次出牌时

		if (_turnTable)
		{
			_turnTable->setLeftCardVisble(true);
			_turnTable->setCardCount(_leftCardCount);
		}
		isPlayerGame = true;
		showTimeCountByAction(true,10);
	}

	UserInfoStruct** GameManager::getVecUser() 
	{ 
		return _vecUser; 
	}

	void GameManager::catchCard(const sitDir& dir, const INT& number)
	{
		/*if (!GTLogic()->isGamePlaying())
		{
			return;
		}*/
		
		this->turnTableDir(dir);

		setCurrOperDir(dir);
		commonCatchCard(dir, number);
		
		_leftCardCount--;

		if (_turnTable)
		{
			_turnTable->setCardCount(MAX(0,_leftCardCount));
		}
	}

	void GameManager::outCard(const sitDir& dir, const INT& number)
	{
		int index =int(dir);

		setCurrOperDir(dir);
		if (dir == SOUTH_DIR)
		{
			_isHasChiAction = false;
		}
	
		INT zorder = 0;
		Vec2 outPos = _vecCardPool.at(index)->getOutToDeskPos();
		Vec2 setPos = getOutCardDeskPos(dir,zorder);
		auto card =  getFrontCard(dir, number);

		card->setPosition(outPos);
		this->addChild(card);
		//5. 移动动画
		card->runAction(Sequence::create(
			MoveTo::create(0.1f, setPos),
			/*CallFunc::create(CC_CALLBACK_0(Card::setLocalZOrder, card, zorder)),*/
			CallFunc::create(CC_CALLBACK_0(Card::playZhuanShiAction, card)),
			nullptr
			));

		card->setLocalZOrder(zorder);

		Card* lastestOutCard = getLastOutCard();
		for (auto v:_allOutCardList)
		{
			v->stopZhuanShiAction();
		}
		
		_allOutCardList.push_back(card);
		// 6. 整理手牌
		afterOutCard(dir);
		
		
	}

	void GameManager::passAction()
	{
		resetDiKuang();
		GameProtoC2SPassAction_t toProtData;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_PassAction, &toProtData, sizeof(toProtData));
	}
	  


	void GameManager::loadUsers()
	{
		auto userInfo = RoomLogic()->loginResult.pUserInfoStruct;
		// 显示玩家
		std::vector<UserInfoStruct *> vec;
		UserInfoModule()->findDeskUsers(userInfo.bDeskNO, vec);
		for (auto &v : vec)
		{
			addUser(GTLogic()->getUserDir(v->bDeskStation), v);
		}
	}

	void GameManager::refreshUserMoney(const sitDir& dir, const UserInfoStruct& user)
	{
		LLONG money =0+_actionPoint[user.bDeskStation];
		COCOS_NODE(Text, StringUtils::format("money%d", dir))->setString(StringUtils::format("%lld", money));
	}


	Animation* GameManager::getAni(std::string name)
	{
		auto cache = AnimationCache::getInstance();
		Animation* animation = nullptr;
		Vector<SpriteFrame *> spFrame;
		animation = cache->getAnimation(name);
		if (animation == nullptr)
		{
			for (auto i = 1; i < 50; i++)
			{
				auto fullName = StringUtils::format("%s%d.png", name.c_str(), i);
				auto spf = SpriteFrameCache::getInstance()->getSpriteFrameByName(fullName.c_str());
				if (spf != nullptr)
				{
					spFrame.pushBack(spf);
				}
				else
				{
					break;
				}
			}
			animation = Animation::createWithSpriteFrames(spFrame);
			animation->setDelayPerUnit(0.05f);
			animation->setLoops(1);
			cache->addAnimation(animation, name);
		}
		else
		{
			auto animationCopy = animation->clone();
			auto copyName = StringUtils::format("%sCopy", name.c_str());
			cache->addAnimation(animationCopy, copyName);
			return animationCopy;
		}
		return animation;
	}

	void GameManager::setNt(const sitDir& dir, int bankCount)
	{
		_currOperDir = _curSendDir = _startDir = dir;

		for (auto i = 0; i < PLAY_COUNT; i++)
		{
			COCOS_NODE(Sprite, StringUtils::format("zhuang%d", i))->setVisible(false);
		}
		COCOS_NODE(Sprite, StringUtils::format("zhuang%d", _startDir))->setVisible(true);
		if (bankCount > 0)
		{
			std::string str = StringUtils::format("/%d", bankCount);
			COCOS_NODE(TextAtlas, StringUtils::format("zhangcount%d", _startDir))->setVisible(true);
			COCOS_NODE(TextAtlas, StringUtils::format("zhangcount%d", _startDir))->setString(str);
		}
		if (_turnTable)
		{
			_turnTable->setTurnInitDir(_startDir);
			this->turnTableDir(_startDir);
		}
		
		for (auto i = 0; i < PLAY_COUNT; i++)
		{
			COCOS_NODE(Sprite, StringUtils::format("ready%d", i))->setVisible(false);
		}
		COCOS_NODE(Sprite, "waiting")->setVisible(false);
	}

	void GameManager::setCatchDir(const sitDir& dir)
	{
		_startCatchDir = dir;
	}

	void GameManager::finishGame(const std::vector<std::vector<int>>& vvNum)
	{
	
	}

	void GameManager::resetInit()
	{
		this->unschedule(schedule_selector(GameManager::sendCardTimer));

		for (auto &v : _vecCardPool)
		{
			v->finishGame();
			v->ClearGroupData();
		}

		
		for (auto vvv : _allOutCardList)
		{
			vvv->stopAllActions();
			vvv->removeFromParentAndCleanup(true);
		}
		_allOutCardList.clear();

		for (auto vv:_ziMoOrHu)
		{
			if (vv ==nullptr)
			{
				continue;
			}
			vv->removeFromParent();
		}

		memset(_ziMoOrHu,0,sizeof(_ziMoOrHu));

		restartSetData();
	}


	void GameManager::agreeGame()
	{
		COCOS_NODE(Button, "start")->setVisible(false);
		COCOS_NODE(Sprite, "waiting")->setVisible(true);
		playCommonSound("Ready");
	}

	void GameManager::initAllHandCard(std::vector<std::vector<INT>> vvSouthCard)
	{
		for (auto &v : _vecCardPool)
		{
			v->finishGame();
			v->ClearGroupData();
		}

		_vvStartHandCard = vvSouthCard;
		memset(_currSendIndex,0,sizeof(_currSendIndex));
		// 开始发牌
		sendCard(_startCatchDir);

	}

	Card* GameManager::commonCatchCard(const sitDir& dir, const INT& mahjongNumber)
	{
		
		// 插入手牌
		Card* giveCard = getCard(dir, mahjongNumber);
		giveCard->setPosition(640,360);
		this->addChild(giveCard,100);
		if (SOUTH_DIR==dir)
		{
			setIsCatchCard(true);
		}
		CardPool* pCardPool= _vecCardPool.at(INT(dir));
		giveCard->setGlobalZOrder(pCardPool->getZhuaPaiZOrder());
		giveCard->runAction(
			Sequence::create(
			EaseSineOut::create(MoveTo::create(0.1f, pCardPool->getCatchPos()))
			,CallFunc::create(CC_CALLBACK_0(CardPool::addHandCard, pCardPool, mahjongNumber))
			,CallFunc::create([this](){setIsCatchCard(false);})
			,RemoveSelf::create()
			,nullptr
			));

		return giveCard;
	}



	void GameManager::addCardPool(CardPool * pool)
	{
		_vecCardPool.push_back(pool);
		this->addChild(pool,300);
	}


	std::vector<CardPool *>& GameManager::getUserCardPool()
	{ 
		return _vecCardPool; 
	}

	CardPool* GameManager::getSelfCardPool()
	{
		if(_vecCardPool.empty()) return nullptr;
		return _vecCardPool.at(0);
	}

	
	Card* GameManager::getCard(const sitDir& dir, const INT& mahjongNumber)
	{
		Card* giveCard = nullptr;
		auto pool = _vecCardPool.at(INT(dir));
		switch (dir)
		{
		case sitDir::SOUTH_DIR:
			{
				giveCard = getZhengLiCard(mahjongNumber);
				giveCard->setCardOwner(sitDir::SOUTH_DIR);
				break;
			}
		case sitDir::EAST_DIR:
			{
				giveCard = getYouLiCard();
				giveCard->setCardOwner(sitDir::EAST_DIR);
				break;
			}
		case sitDir::NORTH_DIR:
			{
				giveCard = getHouLiCard();
				giveCard->setCardOwner(sitDir::NORTH_DIR);
				break;
			}
		case sitDir::WEST_DIR:
			{
				giveCard = getZuoLiCard();
				giveCard->setCardOwner(sitDir::WEST_DIR);
				break;
			}
	
		default:
			break;
		}
		giveCard->setCardZorder(500);
		giveCard->setCardPos(Vec2(-1000, -1000));
		
		return giveCard;
	}


	Card* GameManager::getFrontCard(const sitDir& dir, const INT& mahjongNumber)
	{
		MahjongCard*  card = nullptr;
		switch (dir)
		{
		case SOUTH_DIR:
			card = MahjongCard::create(ZZHOUMJ::mahjongCreateType::DI_FRONT_SOUTH, dir, mahjongNumber);
			break;
		case EAST_DIR:
			card = MahjongCard::create(ZZHOUMJ::mahjongCreateType::DI_FRONT_EAST, dir, mahjongNumber);
			break;
		case WEST_DIR:
			card = MahjongCard::create(ZZHOUMJ::mahjongCreateType::DI_FRONT_WEST, dir, mahjongNumber);
			break;
		case NORTH_DIR:
			card = MahjongCard::create(ZZHOUMJ::mahjongCreateType::DI_FRONT_NORTH, dir, mahjongNumber);
			break;
		default:
			break;
		}
		
		return card;
	}

	

	Card* GameManager::getZhengLiCard(const INT& mahjongNumber)
	{
		Card*  card = MahjongCard::create(mahjongCreateType::DI_SOUTH_STAND, sitDir::SOUTH_DIR,mahjongNumber);
		card->setCardTouchEvent();
		return card;
	}

	Card* GameManager::getZuoLiCard()
	{
		Card* card = MahjongCard::create(mahjongCreateType::DI_WEST_STAND, sitDir::WEST_DIR);
		return card;
	}

	Card* GameManager::getYouLiCard()
	{
		auto card = MahjongCard::create(mahjongCreateType::DI_EAST_STAND, sitDir::EAST_DIR);
		return card;
	}

	Card* GameManager::getHouLiCard()
	{
		Card* card = MahjongCard::create(mahjongCreateType::DI_NORTH_STAND, sitDir::NORTH_DIR);
		return card;
	}

	Vec2 GameManager::getOutCardDeskPos(const sitDir& dir,INT& zOrder)
	{
		
		auto startPos = Vec2::ZERO;
		Size outRect;
		auto index = INT(dir);
		auto count = _vecHasOutCardCount.at(index);
		++_vecHasOutCardCount.at(index);
		Vec2 returnPos = startPos;

		int curLine = count/5;
		int lineIndex = count%5;
		

		switch (dir)
		{
		case sitDir::SOUTH_DIR:
			{
				curLine = count / 10;
				lineIndex = count % 10;
				outRect = Size(40.5,50);
				startPos =  CardPool::SounthDirOutCardPos;
				returnPos = startPos +Vec2(-20,-22)+ Vec2(lineIndex* outRect.width, -curLine * outRect.height);
				zOrder = 500+curLine;
				break;
			}
		case sitDir::EAST_DIR:
			{
				curLine = count / 8;
				lineIndex = count % 8;
				outRect = Size(53,34*0.9);
				startPos = CardPool::EastDirOutCardPos;
				returnPos = startPos + Vec2(40,-20) + Vec2(curLine*outRect.width-8*lineIndex, lineIndex*outRect.height);
				if (curLine == 0)
					zOrder = 500 - lineIndex;
				else
					zOrder = 500- (lineIndex*curLine+ curLine);
				break;
			}
		case sitDir::NORTH_DIR:
			{
				curLine = count / 10;
				lineIndex = count % 10;
				outRect = Size(38*0.9,36);
				startPos = CardPool::NorthDirOutCardPos;
				returnPos = startPos +Vec2(-outRect.width,5) + Vec2(-lineIndex*outRect.width,-curLine*outRect.height);
				zOrder = 500+curLine;
				break;
			}
		case sitDir::WEST_DIR:
			{
				curLine = count / 8;
				lineIndex = count % 8;
				outRect = Size(53,33*0.9);//53,33
				startPos = CardPool::WestDirOutCardPos;//5,-30,,,-8*lineIndex
				returnPos = startPos+Vec2(5,+10) + Vec2(-curLine*outRect.width-8*lineIndex, -lineIndex*outRect.height);
				zOrder = 500-curLine;
				break;
			}
		default:
			break;
		}

		return returnPos;
	}

	void GameManager::afterOutCard(const sitDir& dir)
	{
		auto poolIndex = INT(dir);
		_vecCardPool.at(poolIndex)->setOutCardCount(_vecCardPool.at(poolIndex)->getOutCardCount()+1);
	}

	void GameManager::startOutCard(const sitDir& dir)
	{
		auto index = int(dir);
		auto pool = _vecCardPool.at(index);
		pool->setHandCardPos(_vvStartHandCard.at(index).at(0));
	}

	void GameManager::buhuaCard(const sitDir& dir, INT buhuaCard)
	{
		if (!isPlayerGame)
		{
			return;
		}
		auto index = int(dir);
		auto pool = _vecCardPool.at(index);
		pool->setHandCardPos(buhuaCard);
	}

	void GameManager::setMinusOutCount(const sitDir& dir)
	{
		INT index = INT(dir);
		_vecHasOutCardCount.at(index) = _vecHasOutCardCount.at(index) - 1;
	}


	void GameManager::sendCard(const sitDir& dir)
	{
		_sendCardCount = 0;
		this->schedule(schedule_selector(GameManager::sendCardTimer), 0.16f);
	}

	

	void GameManager::sendCardToPool(const INT& cardCount)
	{
		if (_curSendDir>=DIR_MAX || _curSendDir<= MID_DIR)
		{
			return;
		}

		playCommonSound("zhuapai");
		std::vector<INT> vecTmpCard;										// 需要发的牌
		auto startIndex = _currSendIndex[_curSendDir];						// 需要发的牌起始位置
		auto endIndex = startIndex + cardCount - 1;							// 需要发的牌结束位置
		for (auto i = startIndex; i <= endIndex; i++)						// 填充需要发的牌
		{
			auto num = _vvStartHandCard.at(INT(_curSendDir)).at(i);
			vecTmpCard.push_back(num);										// 填充要发的牌
		}

		_currSendIndex[_curSendDir] = endIndex+1;							// 设置新的起始点

		// 往该方向发牌
		if (_curSendDir>MID_DIR && _curSendDir<DIR_MAX)
		{
			_vecCardPool.at(_curSendDir)->sendSomeCard(vecTmpCard);
		}
		else
		{
			return;
		}

		// 往下一个方向发
		if (_curSendDir==DIR_MAX-1)
		{
			_curSendDir = SOUTH_DIR;    
		}
		else
		{
			_curSendDir = sitDir(_curSendDir+1);
		}
		
	}

	void GameManager::sendCardTimer(float dt)
	{
		if (_sendCardCount < 16)  // 小于9次, 每次发4张
		{
			sendCardToPool(4);
		}
		else if (_sendCardCount < 20)   // 发一张
		{
			if (_curSendDir == _startDir) //庄家最后发两张
			{
				sendCardToPool(1);
			}
		}
		else
		{
			this->unschedule(schedule_selector(GameManager::sendCardTimer));
			_sendCardCount = 0;
			return;
		}
		++_sendCardCount;
	}

	

	void GameManager::setUserTingPaiState(bool isTingPai, INT index)	
	{
		_vecTingPaiState.at(index)=isTingPai;
		_vecCardPool[index]->setIsTingState(isTingPai);	
	}

	void GameManager::setGlobalScale()
	{
		// 适配
		auto visibleSZ = Director::getInstance()->getVisibleSize();
		auto visibleOrigin = Director::getInstance()->getVisibleOrigin();

		// 背景层
		auto bg = Sprite::create(PIC_PATH + "bg.png");
		this->addChild(bg);
		bg->setAnchorPoint(Vec2::ZERO);
		bg->setPosition(visibleOrigin);
		bg->setLocalZOrder(-111);

		auto newOrigin = visibleOrigin;
		auto uiSZ = Size(1280, 720);
		float scale = 1.0f;

		if (visibleSZ.width >= uiSZ.width && visibleSZ.height >= uiSZ.height)		// 1 . 全包含
		{
			newOrigin += Vec2((visibleSZ.width - uiSZ.width)/2.0f, (visibleSZ.height - uiSZ.height)/2.0f);
		}
		else if (visibleSZ.width >= uiSZ.width && visibleSZ.height < uiSZ.height)
		{
			scale = (visibleSZ.height / uiSZ.height);
			newOrigin.x += (visibleSZ.width - uiSZ.width * scale)/2.0f;
		}
		else if (visibleSZ.width < uiSZ.width && visibleSZ.height >= uiSZ.height)
		{
			scale = (visibleSZ.width / uiSZ.width);
			newOrigin.y += (visibleSZ.height - uiSZ.height * scale) / 2.0f;
		}
		else if (visibleSZ.width < uiSZ.width && visibleSZ.height < uiSZ.height)
		{
			auto scaleA = visibleSZ.width/uiSZ.width;
			auto scaleB = visibleSZ.height/uiSZ.height;
			scale = scaleA < scaleB ? scaleA : scaleB;
			newOrigin.x += (visibleSZ.width - uiSZ.width * scale) / 2.0f;
			newOrigin.y += (visibleSZ.height - uiSZ.height * scale) / 2.0f;
		}
		_globalScale = scale;
		
	}
	

	void GameManager::playCommonSound(std::string soundName)
	{
		// head
		auto fullName = MUSIC_COMMON_PATH;
		fullName += soundName;
		fullName += ".mp3";
		HNAudioEngine::getInstance()->playEffect(fullName.c_str());
	}

	bool GameManager::isMan(int index)
	{
		if (index>=PLAY_COUNT)
		{
			return false;
		}

		if (_vecUser[index] == nullptr)
		{
			return false;
		}


		return _vecUser[index]->bBoy;
	}

	void GameManager::showMeAction(bool canPeng, bool canchi,bool canGang, bool canHu, bool canTing, bool canQiang, bool canDao, bool canyou, bool cantianhu, bool canzimo, char num)
	{

		if (this->getChildByName("buttonNode"))
		{
			this->removeChildByName("buttonNode");
		}
		Size winSzie=Director::getInstance()->getWinSize();
		ZZHOUMJ_ActionButtonList* buttonNode = ZZHOUMJ_ActionButtonList::create();
		buttonNode->setName("buttonNode");
		buttonNode->setPosition(winSzie/2.0);
		buttonNode->showActionBotton(canHu,canchi,canPeng,canGang,canTing,canQiang,canDao,canyou,cantianhu,canzimo,num); //牌的响应
		//buttonNode->showActionBotton(true, true, true, true, true, true, true, true, true, true, 1);
		this->addChild(buttonNode,topZorder);
	}

	void GameManager::seChiPaiList(std::vector<std::vector<INT>> cradArray)
	{
		_sChiList = cradArray;
	}

	void GameManager::setCurrOperDir(const sitDir& dir)
	{
		_currOperDir = dir;
		
	}
	
	const sitDir& GameManager::getCurrOperDir()
	{
		return _currOperDir;
	}


	void GameManager::showScoreAnimation(const sitDir& dir, int money)
	{
		if (money==0)
		{
			return;
		}

	    auto prarent=COCOS_NODE(Text, StringUtils::format("money%d", dir))->getParent();
		Vec2 pos=COCOS_NODE(Text, StringUtils::format("money%d", dir))->getPosition();
		if (dir == SOUTH_DIR)
		{
			Size size = Director::getInstance()->getWinSize();
			pos = Vec2(size.width/2,180);
		}

		LabelAtlas* moneyText=nullptr;
		if (money>0)
		{
			 std::string myMoney = StringUtils::format(">%d", money);
		     moneyText = CCLabelAtlas::create(myMoney, FNT_PATH+"font_num1.png", 19, 25, '0');
		}
		else
		{
		    std::string myMoney = StringUtils::format("<%d", money);
		     moneyText = CCLabelAtlas::create(myMoney, FNT_PATH+"font_num2.png", 19, 25, '0');
		}
		moneyText->setAnchorPoint(Vec2(0.5, 0.5));
		moneyText->setScale(2.0);
		moneyText->setPosition(pos);
		prarent->addChild(moneyText);
		moneyText->runAction(Sequence::create(DelayTime::create(0.8f), MoveBy::create(1.5f, Point(0, 50)), RemoveSelf::create(), nullptr));

		afterScorePoint(dir, money);
			
	 }

	bool GameManager::getUserTingPaiState(INT index)
	{
		return  _vecTingPaiState.at(index);
	}

	
	void GameManager::setUserDefine(std::vector<int> define)
	{		
	 //  if (define.size()==0)
	 //  {
		//	return;
	 //  }
	 //  if(_ways.size())  _ways="";
  //     if (define.at(0)==1)
	 //  {
		// std::string way = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_hutype14");	
		// _ways+=way;
		// _ways+=",";
	 //  }	
	 //  //if (define.at(0)==0)
	 //  //{
		//  // std::string way = PromptDictionary::getInstance().findPromptByKey("Word_HSMJ_GameTableUI_hutype19");	
		//  // _ways+=way;
		//  // _ways+=",";
	 //  //}
  //     if (define.at(1)==1)
	 //  {
		//std::string way = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_hutype15");
		//_ways+=way;
		//_ways+=",";
	 //  }	
	 //  //if (define.at(1)==0)
	 //  //{
		//  // std::string way = PromptDictionary::getInstance().findPromptByKey("Word_HSMJ_GameTableUI_hutype20");
		//  // _ways+=way;
		//  // _ways+=",";
	 //  //}
	 //  if (define.size()<=3)
	 //  {
		//   return;
	 //  }
	 //  if (define.at(2)==0)
	 //  {
		//   std::string way = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_hutype21");				
		//   _ways+=way;
	 //  }
	 //   if (define.at(2)==2)
	 //  {
		//std::string way = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_hutype16");				
	 //  	_ways+=way;
		//}	
	 //  if (define.at(2)==5)
		//{
		//std::string way = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_hutype17");				
		//_ways+=way;
	 //  }
	 //  if (define.at(2)==8)
	 //  {
		//std::string way = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_hutype18");				
	 //   _ways+=way;
	 //  }
		//_PlayerWay->setString(_ways);
	}

	Card* GameManager::createZhengPai(const sitDir& dir, int cardId, bool baddChild)
	{
		Card* pCard = nullptr;
		switch (dir)
		{
		case sitDir::SOUTH_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_SOUTH_STAND, dir, cardId);				
			}
			break;
		case sitDir::WEST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_WEST_STAND, dir);
			}
			break;
		case sitDir::EAST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_EAST_STAND, dir);
			}
			break;
		default:
			break;
		}
		if(baddChild)this->addChild(pCard);
		return pCard;
	}

	Card* GameManager::createBeiPai(const sitDir& dir)
	{
		Card* pCard = nullptr;
		switch (dir)
		{
		case sitDir::SOUTH_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_SOUTH_BACK, dir);
			}
			break;
		case sitDir::EAST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_EAST_BACK, dir);
			}
			break;
		case sitDir::WEST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_WEST_BACK, dir);
			}
			break;
		case sitDir::NORTH_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_NORTH_BACK, dir);
			}
			break;
		default:
			break;
		}
		return pCard;
	}

	Card* GameManager::createPengGangFront(const sitDir& dir, int cardId)
	{
		Card* pCard = nullptr;
		switch (dir)
		{
		case sitDir::SOUTH_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_SOUTH, dir, cardId);
			}
			break;
		case sitDir::EAST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_EAST, dir, cardId);
			}
			break;
		case sitDir::WEST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_WEST, dir, cardId);
			}
			break;
		case sitDir::NORTH_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_NORTH, dir, cardId);
			}
			break;

		default:
			break;
		}
		return pCard;
	}


	Card* GameManager::createTingPai(const sitDir& dir, int cardId)
	{
		Card* pCard = nullptr;
		switch (dir)
		{
		case sitDir::SOUTH_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_SOUTH, dir, cardId);				
			}
			break;
		case sitDir::EAST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_EAST, dir, cardId);
			}
			break;
		case sitDir::WEST_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_WEST, dir, cardId);
			}
			break;
		case sitDir::NORTH_DIR:
			{
				pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_NORTH, dir, cardId);
			}
			break;

		default:
			break;
		}
		
		return pCard;
	}

	void GameManager::testOutCard()
	{
	
		for (int dir=0;dir<4;dir++)
		{
			for (int i =0;i<30;i++)
			{
				outCard((sitDir)dir,11);
				//int zorder =0;
				//Vec2 outPos = Vec2(0,0);
				//Vec2 setPos = getOutCardDeskPos((sitDir)dir,zorder);
				//auto card =  getFrontCard((sitDir)dir, 11);
				//_allOutCardList.push_back(card);
				//card->setCardPos(outPos);
				//this->addChild(card);
				//card->playZhuanShiAction();
				//card->runAction(Sequence::create(
				//	EaseSineOut::create(MoveTo::create(0.5f, setPos)),
				//	CallFunc::create(CC_CALLBACK_0(Card::setLocalZOrder, card, zorder)),
				//	nullptr
				//	) );
			}
			
		}
		
	}

	void GameManager::refreshHandCardValue(const sitDir& dir,std::vector<INT> cards)
	{
		auto poolIndex =int(dir);
		_vecCardPool.at(poolIndex)->refreshHandCardValue(cards);
	}


	void GameManager::showHandCardHu(sitDir dir, std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>handCards, bool isZiMo, INT huCard, bool visibleAllCard,bool IsTianhu,bool isQiangjin,bool isanjindao,bool isYoujin[3])
	{
		_tuoguanBtn->setVisible(true);
		_tuoguanBtn->setTouchEnabled(false);
		_tingpaiTipBtn->setVisible(false);
		_tingAction->setVisible(false);
		//吃胡去掉胡的牌
		if (isZiMo == false)
		{
			if (_allOutCardList.empty() == false)
			{
				auto card = _allOutCardList.crbegin();
				if ((*card)->getCardSumNumber() == huCard)
				{
					(*card)->removeFromParent();
					_allOutCardList.pop_back();
				}
			}
		}

		//胡牌还是自摸提示
		Sprite* sprite = nullptr;
		if (isZiMo)
		{
			sprite = Sprite::create(SPRITE_PATH+"img_zimo.png");
		}
		if (IsTianhu)
		{
			sprite = Sprite::create(SPRITE_PATH + "img_tianhu.png");
		}
		if (isQiangjin)
		{
			sprite = Sprite::create(SPRITE_PATH + "img_qiangjin.png");
		}
		if (isanjindao)
		{
			sprite = Sprite::create(SPRITE_PATH + "img_sanjindao.png");
		}
		if (isYoujin[0])
		{
			sprite = Sprite::create(SPRITE_PATH + "img_youjin.png");
		}
		if (isYoujin[1])
		{
			sprite = Sprite::create(SPRITE_PATH + "img_shuangyou.png");
		}
		if (isYoujin[2])
		{
			sprite = Sprite::create(SPRITE_PATH + "img_sanyou.png");
		}
		if (!isZiMo&&!IsTianhu&&!isQiangjin&&!isanjindao&&!isYoujin[0] && !isYoujin[1]&&!isYoujin[2]&&huCard>0)
		{
			sprite = Sprite::create(SPRITE_PATH+"img_hupai.png");
		}
		
		Size size = Director::getInstance()->getWinSize();
		Vec2 pos;
		switch (dir)
		{
		case ZZHOUMJ::SOUTH_DIR:
			pos =Vec2(size.width/2,180);
			break;
		case ZZHOUMJ::EAST_DIR:
			pos =Vec2(1050,size.height/2+40);
			break;
		case ZZHOUMJ::NORTH_DIR:
			pos =Vec2(size.width/2,590);
			break;
		case ZZHOUMJ::WEST_DIR:
			pos =Vec2(300,size.height/2);
			break;
		default:
			break;
		}

		if (sprite)
		{
			sprite->setPosition(pos);
			sprite->setRotation(dir*-90);
			sprite->setLocalZOrder(1000);
			this->addChild(sprite);
			if (_ziMoOrHu[dir])
			{
				_ziMoOrHu[dir]->removeFromParent();
			}
			_ziMoOrHu[dir]  = sprite;
		}

		auto poolIndex =int(dir);
		_vecCardPool.at(poolIndex)->showHandCardHu(groupCards,handCards,huCard,visibleAllCard);
	}


	void  GameManager::ShowHu(sitDir dir, int cardID)
	{
		PoolAction * action = nullptr;
		action = _huCard;
		assert(action != nullptr);

		action->setInfo(dir, cardID);



	}
	//播放闪电动画
	void  GameManager::ShowShanDian(sitDir dir)
	{
		

		Size size = Director::getInstance()->getWinSize();
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo(ANM4_PATH + "shandian0.png", ANM4_PATH + "shandian0.plist", ANM4_PATH + "shandian.ExportJson");
		Vec2 pos;
		int jiaodu = 0;
			
		switch (dir)
		{
		case ZZHOUMJ::SOUTH_DIR:
			pos = Vec2(size.width / 2, 200);
			jiaodu = 0;
			break;
		case ZZHOUMJ::EAST_DIR:
			pos = Vec2(1150, size.height / 2 + 40);
			jiaodu = 75;
			break;
		case ZZHOUMJ::NORTH_DIR:
			pos = Vec2(size.width / 2, 670);
			jiaodu = 0;
			break;
		case ZZHOUMJ::WEST_DIR:
			pos = Vec2(210, size.height / 2);
			jiaodu = -78.7;
			break;
		default:
			break;
		}


		auto  armature = cocostudio::Armature::create("shandian");
		if (dir != ZZHOUMJ::SOUTH_DIR)
		{
			armature->getAnimation()->play("shandian1");
		}
		else
		{
			armature->getAnimation()->play("shandian2");
		}
		armature->setRotation(jiaodu);
		armature->setScale(1.0);
		armature->setPosition(pos);
		armature->setVisible(true);
		this->addChild(armature, 1000);
		std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)

		{
			if (type == MovementEventType::COMPLETE)
			{
				armature->getAnimation()->stop();
				armature->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature)), NULL));
			}
		};
		armature->getAnimation()->setMovementEventCallFunc(armatureFun);




	
	
	
	
	
	
	}

	void GameManager::playThingAction(int thingID, sitDir dir, int cardID, bool isMan,char chiCardArray[CHICARDCOUNT])
	{
		INT index = INT(dir);
		if (_vecCardPool.size()<=index)
		{
			return;
		}
		PoolAction * action = nullptr;
		if (thingID==QiangGang)    // 补杠/碰杠
		{
			action = _meldedKong;
			_vecCardPool.at(index)->addSomeOutCards(4, cardID, CardPool::CGroupCard_MingGang, true, chiCardArray);
			action->playSexActionSound(isMan, "gang");	
		}
		else if (thingID==AnGang)  // 暗杠
		{
			action = _concealedKong;
			_vecCardPool.at(index)->addSomeOutCards(4, cardID, CardPool::CGroupCard_AnGang, false, chiCardArray);
			action->playSexActionSound(isMan, "gang");	
		}
		else if (thingID==MingGang)  // 明杠
		{
			action = _touchKong;
			_vecCardPool.at(index)->addSomeOutCards(4, cardID, CardPool::CGroupCard_MingGang, true, chiCardArray);
			action->playSexActionSound(isMan, "gang");	
			
		}
		else if (thingID==PengPai)//碰
		{
			_vecCardPool.at(index)->addSomeOutCards(3, cardID, CardPool::CGroupCard_Peng, true, chiCardArray);
			action = _touchCard;
			action->playSexActionSound(isMan, "peng");	
		}
		else if (thingID == ChiPai)//吃
		{
			_vecCardPool.at(index)->addSomeOutCards(3, cardID, CardPool::CGroupCard_Chi, true, chiCardArray);
			action = _touchEat;
			action->playSexActionSound(isMan, "chi");
		}

		assert(action != nullptr);
		
		action->setInfo(dir, cardID);
		_vecCardPool.at(index)->setHandCardPos(cardID);
		_isHasChiAction = false;
		COCOS_NODE(Sprite, "dikuang")->setVisible(false);
	
	}

	//播放金牌动画
	void GameManager::SHowGoldCardAnimation()
	{
		Size size = Director::getInstance()->getWinSize();
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo(ANM_PATH + "goldcard0.png", ANM_PATH + "goldcard0.plist", ANM_PATH + "goldcard.ExportJson");
		auto  armature = cocostudio::Armature::create("goldcard");
		armature->getAnimation()->play("Animation1");
		armature->setScale(1.5);
		armature->setPosition(size.width/2, size.height/2);
		armature->setVisible(true);
		this->addChild(armature,1000);
		auto kaijing = Sprite::create(PIC_PATH + "kaijin.png");
		kaijing->setScale(1.5);
		kaijing->setPosition(size.width / 2, size.height / 2);
		kaijing->setVisible(true);
		this->addChild(kaijing, 100);

		std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)

		{
			if (type == MovementEventType::COMPLETE)
			{
				armature->getAnimation()->stop();
				kaijing->removeFromParent();
				armature->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature)), NULL));
				ShowGoldCardAction(sitDir::MID_DIR, GetGoldCard());
			}
		};
		armature->getAnimation()->setMovementEventCallFunc(armatureFun);


	}


	//播放分饼动画
	void GameManager::SHowFengbinAnimation()
	{
		Size size = Director::getInstance()->getWinSize();
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo(ANM1_PATH + "fenbing0.png", ANM1_PATH + "fenbing0.plist", ANM1_PATH + "fenbing.ExportJson");
		auto  armature = cocostudio::Armature::create("fenbing");
		armature->getAnimation()->play("Animation1");
		armature->setScale(0.185);
		armature->setPosition(size.width / 2, size.height / 2+12);
		armature->setVisible(true);
		this->addChild(armature, 1000);
		std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)

		{
			if (type == MovementEventType::COMPLETE)
			{
				armature->getAnimation()->stop();
				armature->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature)), NULL));
			}
		};
		armature->getAnimation()->setMovementEventCallFunc(armatureFun);


	}

	//播放牌运动的动画
	void GameManager::ShowGoldCardAction(sitDir turnDir, int GoldCard)
	{
		Size size = Director::getInstance()->getWinSize();
		Vec2 PosZJ = COCOS_NODE(ImageView,"jingdikuang")->getPosition();
		Card* pCard = nullptr;
		pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_ZHENG, turnDir, GoldCard);
		pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
		pCard->setGlobalZOrder(1000);
		pCard->setCardEnableTouch(false);
		pCard->setPosition(size / 2);
		pCard->setName("zhengjing");
		pCard->setVisible(false);
		CallFunc* act1 = CallFunc::create([&]()
		{
			this->getChildByName("zhengjing")->setVisible(true);
		});
		this->addChild(pCard,1000);
		pCard->runAction(Sequence::create(DelayTime::create(1.5f), act1, DelayTime::create(0.5f), Spawn::create(MoveTo::create(0.5f, Vec2(PosZJ.x + 10.0f, PosZJ.y)),
			nullptr), CCCallFuncN::create(this, callfuncN_selector(GameManager::ShowGoldCard)), nullptr));

	}

	//显示金牌
	void  GameManager::ShowGoldCard(Node* sender)
	{
		sender->removeFromParent();
		auto jing_bg = COCOS_NODE(ImageView, "jingdikuang");
		jing_bg->setVisible(true);
		jing_bg->setAnchorPoint(Vec2(0.5f, 0.5f));
		Card* pCard = nullptr;
		auto turnDir = sitDir::MID_DIR;
		pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_ZHENG, turnDir, GetGoldCard());
		pCard->setVisible(true);
		pCard->setZOrder(1000);
		pCard->setScale(1.0);
		pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
		pCard->setPosition(Vec2(44,32));
		jing_bg->addChild(pCard);
		refreshHandCardGold();
	}

	void GameManager::refreshHandCardGold()
	{
		std::vector<INT> hCard = _vecCardPool.at(0)->getHandCardList();
		_vecCardPool.at(0)->refreshHandCardValue(hCard);
	
	}

	//显示补花提示
	void GameManager::showBuhuaTip(bool show)
	{

		if (show == false)
		{
			return;
		}
		auto buhutip = Sprite::create(PIC_PATH + "buhua.png");
		buhutip->setVisible(true);
		buhutip->setZOrder(1000);
		buhutip->setScale(1.5);
		buhutip->setAnchorPoint(Vec2(0.5f, 0.5f));
		buhutip->setPosition(Vec2(640, 360));
		this->addChild(buhutip);
		buhutip->runAction(Sequence::create(DelayTime::create(2.0f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, buhutip)), NULL));

	}

	//显示补花的牌
	void GameManager::showBuhuaCard(sitDir turnDir, int CardNum)
	{
		_leftCardCount--;
		//显示补花的牌
		switch (turnDir)
		{
		case ZZHOUMJ::SOUTH_DIR:
		{
			auto hua_tip = createHuaTip(CardNum);
			hua_tip->setVisible(true);
			hua_tip->setZOrder(1000);
			hua_tip->setAnchorPoint(Vec2::ZERO);
			hua_tip->setPosition(Vec2(0,_buhuaPos[turnDir]));
			_buhuaPos[0] = _buhuaPos[0] + 45;
			COCOS_NODE(Node, StringUtils::format("hua_tip%d", turnDir))->addChild(hua_tip);
		}
			break;
		case ZZHOUMJ::EAST_DIR:
		{
		
			auto hua_tip = createHuaTip(CardNum);
			hua_tip->setVisible(true);
			hua_tip->setZOrder(1000);
			hua_tip->setAnchorPoint(Vec2::ZERO);
			hua_tip->setPosition(Vec2(0, _buhuaPos[turnDir]));
			_buhuaPos[1] = _buhuaPos[1] + 45;
			COCOS_NODE(Node, StringUtils::format("hua_tip%d", turnDir))->addChild(hua_tip);
		
		}
			break;
		case ZZHOUMJ::NORTH_DIR:
		{
			auto hua_tip = createHuaTip(CardNum);
			hua_tip->setVisible(true);
			hua_tip->setZOrder(1000);
			hua_tip->setAnchorPoint(Vec2(0,1));
			hua_tip->setPosition(Vec2(0, _buhuaPos[2]));
			_buhuaPos[2] = _buhuaPos[2] - 45;
			COCOS_NODE(Node, StringUtils::format("hua_tip%d", turnDir))->addChild(hua_tip);
		
		}
			break;
		case ZZHOUMJ::WEST_DIR:
		{
	
			auto hua_tip = createHuaTip(CardNum);
			hua_tip->setVisible(true);
			hua_tip->setZOrder(1000);
			hua_tip->setAnchorPoint(Vec2(0,1));
			hua_tip->setPosition(Vec2(0, _buhuaPos[turnDir]));
			_buhuaPos[3] = _buhuaPos[3] - 45;
			COCOS_NODE(Node, StringUtils::format("hua_tip%d", turnDir))->addChild(hua_tip);
		}
			break;
		default:
			break;
		}


	
	}
	Sprite *GameManager::createHuaTip(int Cardnum)
	{
		std::string str = StringUtils::format("%d.png", Cardnum);
		auto sp = Sprite::create(PIC_PATH + str);
		return sp;
	}

	void  GameManager::playAnimation()
	{
		if (isShow)
		{
			return;
		}
		Size size = Director::getInstance()->getWinSize();
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo(ANM2_PATH + "leida0.png", ANM2_PATH + "leida0.plist", ANM2_PATH + "leida.ExportJson");
		auto  armature1= cocostudio::Armature::create("leida");
		armature1->getAnimation()->play("leida");
		armature1->setPosition(size.width / 2, size.height / 2 + 12);
		armature1->setVisible(true);
		armature1->setScale(0.5);
		this->addChild(armature1, 1000);
		std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)

		{
			if (type == MovementEventType::COMPLETE)
			{
				armature1->getAnimation()->stop();
				isShow = true;
				armature1->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature1)), NULL));
				
			}
		};
		armature1->getAnimation()->setMovementEventCallFunc(armatureFun);
	
	}






	void GameManager::turnTableDir(sitDir dir)
	{
		if (_turnTable)
		{
			_turnTable->turnTableDir(dir);
		}
		for (int i = 0; i < 4; ++i)
		{
			if (i == dir)
			{
				COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", i))->setVisible(true);
				COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", i))->stopAllActions();
				Blink* bl = Blink::create(2.0, 2);
				RepeatForever* reAction = RepeatForever::create(bl);
				COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", i))->runAction(reAction);
			}
			else
			{
				COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", i))->stopAllActions();
				COCOS_NODE(ImageView, StringUtils::format("Image_turn%d", i))->setVisible(false);
			}

		}
	}

	void GameManager::enterTingHandle(std::vector<INT>handcards,std::vector<INT>canOutCard, 
		std::vector<INT>canKouCards)
	{
		if (_currOperDir != SOUTH_DIR)
		{
			return;
		}

		SouthMahjongCardPool* pool = dynamic_cast<SouthMahjongCardPool*>(_vecCardPool.at(0));
		pool->enterTingHandle(handcards,canOutCard,canKouCards);

	}

	void GameManager::initHandCard(char cardArray[PLAY_COUNT][17],char cardArrayCout[PLAY_COUNT])
	{
		//初始化手牌
		for (int i=0;i<PLAY_COUNT;++i)
		{
			sitDir dir = GTLogic()->getUserDir(i);
			std::vector<INT> handCards;
			for (int j=0;j<cardArrayCout[i];j++)
			{
				handCards.push_back(cardArray[i][j]);
			}
			this->refreshHandCardValue(dir,handCards);
		}
	}

	void GameManager::resetBeginStageUI(char bankerNo,sitDir bankerDir,sitDir catchDir, char bySeziBanker,int bankCount)
	{
		setNt(bankerDir, bankCount);
		
		GameManager::getInstance()->setCatchDir(catchDir);
		runShaiZiAction(bySeziBanker);

	}

	void GameManager::resetSendCardStageUI(sitDir bankerDir,GameStatusSendCard_t* data)
	{
		startGame();
		setNt(bankerDir, data->bankcount);
		this->unschedule(schedule_selector(GameManager::sendCardTimer));
		initHandCard(data->cardArray,data->countArray);
		
	}

	void GameManager::resetOutCatchCardStageUI(sitDir bankerDir, GameStatusOutCard_t* data, bool isOutStage)
	{
		startGame();
		setNt(bankerDir, data->bankcount);
		COCOS_NODE(Button, "start")->setVisible(false);
		COCOS_NODE(Sprite, "waiting")->setVisible(false);

		this->unschedule(schedule_selector(GameManager::sendCardTimer));
		sitDir turnDir = GTLogic()->getUserDir(data->turnSeatNo);
		turnTableDir(turnDir);
		setCurrOperDir(turnDir);

		_leftCardCount = data->leftCardCount;
		_goldCard = data->toGoldCard;
		auto jing_bg = COCOS_NODE(ImageView, "jingdikuang");
		jing_bg->setVisible(true);
		jing_bg->setAnchorPoint(Vec2(0.5f, 0.5f));
		Card* pCard = nullptr;
		auto turnDir1 = sitDir::MID_DIR;
		pCard = MahjongCard::create(mahjongCreateType::DI_FRONT_ZHENG, turnDir1, GetGoldCard());
		pCard->setVisible(true);
		pCard->setZOrder(1000);
		pCard->setScale(1.0);
		pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
		pCard->setPosition(Vec2(44, 32));
		jing_bg->addChild(pCard);

		_turnTable->setCardCount(_leftCardCount);
		_turnTable->setLeftCardVisble(true);
		isPlayerGame = true;
		//是否是听,听状态在刷新手牌前面
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			bool isFinishTing = data->isTingArray[i];
			sitDir dir = GTLogic()->getUserDir(i);
			int index = int(dir);
			setUserTingPaiState(isFinishTing, index);
		}

		//恢复手牌组
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			sitDir dir = GTLogic()->getUserDir(i);
			int index = int(dir);
			std::vector<CardPool*> cardPools = getUserCardPool();
			if (cardPools.size() < index)
			{
				continue;
			}
			std::vector<CardPool::CGroupCardData> groupCards;
			for (int j = 0; j < data->actionCountArray[i]; j++)
			{
				int count = data->actionCardArray[i][j][2];
				CardPool::CGroupCardData cardData;
				char chiCardArray[3] = { 0 };
				std::vector<INT> cards;
				for (int z = 3; z < 3 + count; z++)
				{
					cards.push_back(data->actionCardArray[i][j][z]);
				}
				if (data->actionCardArray[i][j][0] == ChiPai)
				{
					for (int k = 0; k < CHICARDCOUNT; k++)
					{
						chiCardArray[k] = cards.at(k);
					}
				}

				switch (data->actionCardArray[i][j][0])
				{
				case ChiPai:
					cardPools.at(index)->addGoupGangPeng(3, cards.at(0), CardPool::CGroupCard_Chi, chiCardArray);
					cardData._iCardId = cards.at(0);
					cardData._iCount = cards.size();
					cardData._iType = CardPool::CGroupCard_Chi;
					groupCards.push_back(cardData);
					break;
				case PengPai:
					cardPools.at(index)->addGoupGangPeng(3, cards.at(0), CardPool::CGroupCard_Peng);
					cardData._iCardId = cards.at(0);
					cardData._iCount = cards.size();
					cardData._iType = CardPool::CGroupCard_Peng;
					groupCards.push_back(cardData);

					break;
				case MingGang:
				case QiangGang:
					cardPools.at(index)->addGoupGangPeng(4, cards.at(0), CardPool::CGroupCard_MingGang);
					cardData._iCardId = cards.at(0);
					cardData._iCount = cards.size();
					cardData._iType = CardPool::CGroupCard_MingGang;
					groupCards.push_back(cardData);

					break;
				case AnGang:
					cardPools.at(index)->addGoupGangPeng(4, cards.at(0), CardPool::CGroupCard_AnGang);
					cardData._iCardId = cards.at(0);
					cardData._iCount = cards.size();
					cardData._iType = CardPool::CGroupCard_AnGang;
					groupCards.push_back(cardData);
					break;
				default:
					break;
				}
			}


			std::vector<INT> handCards;
			for (int j = 0; j < data->countArray[i]; j++)
			{
				handCards.push_back(data->cardArray[i][j]);
			}

			if (data->bIsHu[i])
			{
				bool isYoujin[3] = { false, false, false };
				this->showHandCardHu(dir, groupCards, handCards, false, data->huCardId[i], dir == SOUTH_DIR, false, false, false, isYoujin);
			}
			else
			{
				this->refreshHandCardValue(dir, handCards);
			}
		}

		//恢复历史出牌
		for (auto v : _allOutCardList)
		{
			v->removeFromParent();
		}
		_allOutCardList.clear();
		Card* zhuangshiCard = nullptr;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			sitDir dir = GTLogic()->getUserDir(i);
			for (int cardCount = 0; cardCount < data->allOutCountArray[i]; cardCount++)
			{

				int cardNumber = data->allOutCardArray[i][cardCount];
				if (cardNumber >= 41)
				{
					showBuhuaCard(dir, cardNumber);
					continue;
				}
				int zorder = 0;
				Vec2 setPos = getOutCardDeskPos(dir, zorder);
				auto card = getFrontCard(dir, cardNumber);
				card->setLocalZOrder(zorder);
				card->setPosition(setPos);
				this->addChild(card);

				//最后一张显示砖石
				if (i == data->seatNo && cardCount == data->allOutCountArray[i] - 1)
				{
					card->playZhuanShiAction();
					zhuangshiCard = card;
				}
				else
				{
					_allOutCardList.push_back(card);

				}
			}
		}
		if (zhuangshiCard != nullptr)
		{
			_allOutCardList.push_back(zhuangshiCard);
		}




		if (data->isHavePeng || data->isHaveGang || data->isHaveHu || data->isHaveTing || data->isHaveChi || data->isHaveDao || data->ishaveTianhu || data->ishavezimo)
		{
			//显示当前动作
			showMeAction(data->isHavePeng,data->isHaveChi,data->isHaveGang,data->isHaveHu,data->isHaveTing,data->isHaveQiang,data->isHaveDao,data->isHaveYou,data->ishaveTianhu,data->ishavezimo,data->num);
			GameManager::getInstance()->setIsHasAction(true);
		}
		if (data->bAction)
		{
			GameManager::getInstance()->setCurrOperDir(sitDir(-1));
		}

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			sitDir dir = GTLogic()->getUserDir(i);
			if (dir == 0 && data->bTuoGuanArray[i] && _isHasAction)
			{
				passAction();
			}
		}


	}

	void GameManager::resetChangeCardStageUI(sitDir bankerDir,GameStatusChangeCard_t* data)
	{
		startGame();
		setNt(bankerDir, data->bankcount);
		initHandCard(data->cardArray,data->countArray);
		
		GameManager::getInstance()->changeCardBegin();
		
		if (data->isAllApply)
		{
			GameProtoS2CChangeCardFinish finish;
			finish.changeSezi = data->changeCardSezi;
			memcpy(finish.cardArray,data->cardArray,sizeof(data->cardArray));
			memcpy(finish.countArray,data->countArray,sizeof(data->countArray));
			memcpy(finish.changeCard,data->changeCardArray,sizeof(data->changeCardArray));
			memcpy(finish.changeScore,data->changeScore,sizeof(data->changeScore));
			memcpy(finish.getCard,data->afterchangeCardArray,sizeof(data->afterchangeCardArray));
			
			GameManager::getInstance()->changeCardEnd(&finish);
		}
		else
		{
			//把交换牌摆出去
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				auto dir = GTLogic()->getUserDir(i);
				bool isHas = true;
				for (int j=0;j<data->changeCardCount[i];++j)
				{
					if (data->changeCardArray[i][j] == 0)
					{
						isHas = false;
						break;
					}
				}
				if (isHas)
				{
					GameManager::getInstance()->changeCardToHead(dir,data->changeCardArray[i],data->changeCardCount[i]);
				}
			}
		}
	}

	void GameManager::removeLastOutCard()
	{
		Card* last = getLastOutCard();
		if (last)
		{
			_allOutCardList.pop_back();
			last->removeFromParent();
		}
		
	}

	Card* GameManager::getLastOutCard()
	{
		if (_allOutCardList.empty())
		{
			return nullptr;
		}

		return *(_allOutCardList.rbegin());
	}



	void GameManager::updateActionScore(INT score[PLAY_COUNT])
	{
		for (int i =0;i<PLAY_COUNT;++i)
		{
			sitDir dir = GTLogic()->getUserDir(i);
			_actionPoint[i] = score[i];
			UserInfoStruct* userinfo=_vecUser[dir];
			if (userinfo == nullptr)
			{
				continue;
			}
			refreshUserMoney(dir,*userinfo);
		}
	}

	void GameManager::showGMButton()
	{
// 
// 		COCOS_NODE(Button, "btn_GM")->setVisible(true);
// 		COCOS_NODE(Button, "btn_GM")->addTouchEventListener(CC_CALLBACK_2(GameManager::onGMClickCallBack, this));

	}

	void GameManager::onGMClickCallBack(Ref* ref,Widget::TouchEventType type)
	{
		if (type!= cocos2d::ui::Widget::TouchEventType::BEGAN)return;

		std::vector<UserInfoStruct> userList;
		std::vector<std::vector<INT>> handCards;
		for (int i =0;i<PLAY_COUNT;++i)
		{
			if (_vecUser[i] == nullptr)
			{
				continue;
			}

			INT userId = PlatformLogic()->loginResult.dwUserID;
			if (_vecUser[i]->dwUserID != userId)
			{
				userList.push_back(*_vecUser[i]);

				auto dir =GTLogic()->getUserDir(_vecUser[i]->bDeskStation);
				int poolIndex = int(dir);
				std::vector<INT> hCard = _vecCardPool.at(poolIndex)->getHandCardList();
				handCards.push_back(hCard);
			}
		}
		ZZHOUMJ_GMLayer* layer = ZZHOUMJ_GMLayer::create();
		layer->setUIData(userList,handCards);
		this->addChild(layer,1000);
	}

	void GameManager::showHuTips(const sitDir& dir,std::vector<INT> cards)
	{
		auto poolIndex =int(dir);
		_vecCardPool.at(poolIndex)->showHuTips(cards);
	}

	void GameManager::changeCardBegin()
	{
		for (int i=MID_DIR+1;i<DIR_MAX;++i)
		{
			_vecCardPool.at(i)->enterChangeCards();
		}
	}

	void GameManager::changeCardEnd(GameProtoS2CChangeCardFinish* changeCardFinish)
	{
		//打筛子
		runShaiZiAction(changeCardFinish->changeSezi);

		Node* node = Node::create();
		this->addChild(node);

		GameProtoS2CChangeCardFinish* pData = new GameProtoS2CChangeCardFinish();
		memcpy(pData,changeCardFinish,sizeof(GameProtoS2CChangeCardFinish));

		DelayTime* delay = DelayTime::create(3.0f);
		auto fun = CallFunc::create([=](){
			for (int i=0;i<PLAY_COUNT;i++)
			{
				std::vector<INT> outCards;
				std::vector<INT> inCards;
				sitDir dir = GTLogic()->getUserDir(i);
				mahjongColor cradColor= mahjongColor(pData->changeCard[i][1] / 10);
				for (int j=0;j<3;++j)
				{
					outCards.push_back(pData->changeCard[i][j]);
					inCards.push_back(pData->getCard[i][j]);
					
				
				}

				for (int k = 0; k < PLAY_COUNT-1; k++)
				{
					mahjongColor color = mahjongColor(pData->changeCard[i][k] / 10);
					if (cradColor!=color)
					{
						COCOS_NODE(Sprite, StringUtils::format("Mai%d",dir))->setVisible(true);
						break;
					}
				}
			
				std::vector<INT> handCard;
				for (int j=0;j<pData->countArray[i];++j)
				{
					handCard.push_back(pData->cardArray[i][j]);
				}

				_vecCardPool.at(dir)->exitChangeCards(pData->changeSezi,outCards,inCards,handCard);

				GameManager::getInstance()->showScoreAnimation(dir,pData->changeScore[i]);
			}

			//分数动画待加
			GameManager::getInstance()->updateActionScore(pData->changeScore);
			delete pData;
		}
		
		);
		
		node->runAction(Sequence::create(delay,fun,RemoveSelf::create(true),nullptr));

	}

	void GameManager::changeCardToHead(sitDir dir,char card[3],char cardCount)
	{
		std::vector<INT> temp;
		for (int i=0;i<3;i++)
		{
			temp.push_back(card[i]);
		}

		_vecCardPool.at(dir)->changeCardToHead(temp);
	}
	void GameManager::addChangeCardPos(const sitDir& dir,const std::vector<Point>& vPos)
	{
		if (_sitDirPosChangeCardMap.find(dir) != _sitDirPosChangeCardMap.end())
			return;
		_sitDirPosChangeCardMap.insert(std::pair<sitDir, std::vector<Point>>(dir, vPos));
	}
	void GameManager::runChangeCardAction(Card* pNode, const changeActType& act_type, const bezDir& bez_dir, const sitDir& sit_dir, int index)
	{
		if (pNode == nullptr || _sitDirPosChangeCardMap.find(sit_dir) == _sitDirPosChangeCardMap.end() || _sitDirPosChangeCardMap[sit_dir].empty())
			return;
		sitDir origin_dir = pNode->getCardOwner();
		float ftime = 0.6f;
		switch (act_type)
		{
		case changeActType::STRAIGHT: 
		{
			Vec2 posTemp = Vec2::ZERO;
			switch (sit_dir)
			{
			case sitDir::EAST_DIR:
				posTemp.x = 750;
				break;
			case sitDir::WEST_DIR:
				posTemp.x = -750;
				break;
			case sitDir::SOUTH_DIR:
				posTemp.y = -400;
				break;
			case sitDir::NORTH_DIR:
				posTemp.y = 400;
				break;
			default:
				break;
			}
			
			MoveBy* by = MoveBy::create(ftime, posTemp);
			Sequence* seq = Sequence::create(by,
				CallFunc::create([=]() {pNode->setPosition(_sitDirPosChangeCardMap[origin_dir][index]); }),
				CCDelayTime::create(1.0f),
				RemoveSelf::create(true), nullptr,
				nullptr);
			pNode->runAction(seq);

		}
		break;
		case changeActType::CW: 
		{
			ccBezierConfig bez;
			Vec2 posTemp = _sitDirPosChangeCardMap[sit_dir][index];
			switch (sit_dir)
			{
			case sitDir::EAST_DIR:
				posTemp.x += 0;
				break;
			case sitDir::WEST_DIR:
				posTemp.x -= 150;
				break;
			default:
				break;
			}
			bez.endPosition = posTemp;
			bez.controlPoint_1 = Vec2(g_bezControl[bez_dir][1].x, 720 - g_bezControl[bez_dir][1].y);
			bez.controlPoint_2 = Vec2(g_bezControl[bez_dir][0].x, 720 - g_bezControl[bez_dir][0].y);
			BezierTo* bzby = BezierTo::create(ftime, bez);

			Spawn* spawn = Spawn::create(
				RotateBy::create(ftime, 90),
				bzby, nullptr);

			Sequence* seq = Sequence::create(spawn,
				CallFunc::create([=]() {pNode->setPosition(_sitDirPosChangeCardMap[origin_dir][index]); }),
				RotateBy::create(0, -90),
				DelayTime::create(1.0f),
				RemoveSelf::create(true), nullptr);
			pNode->runAction(seq);
			
		}		
		break;
		case changeActType::CCW:
		{
			
			ccBezierConfig bez;
			Vec2 posTemp = _sitDirPosChangeCardMap[sit_dir][index];
			switch (sit_dir)
			{
			case sitDir::EAST_DIR:
				posTemp.x -= 180;
				break;
			default:
				break;
			}
			bez.endPosition = posTemp;
			bez.controlPoint_1 = Vec2(g_bezControl[bez_dir][0].x,720- g_bezControl[bez_dir][0].y);
			bez.controlPoint_2 = Vec2(g_bezControl[bez_dir][1].x,720- g_bezControl[bez_dir][1].y);
			BezierTo* bzby = BezierTo::create(ftime, bez);

			Spawn* spawn = Spawn::create(
				CCRotateBy::create(ftime, 90),
				bzby, 
				nullptr);

			Sequence* seq = Sequence::create(spawn,
				CallFunc::create([=]() {pNode->setPosition(_sitDirPosChangeCardMap[origin_dir][index]); }),
				RotateBy::create(0, -90),
				DelayTime::create(1.0f),
				RemoveSelf::create(true), nullptr);
			pNode->runAction(seq);
		}
		break;
		default:
			break;
		}
	}

	void GameManager::runShaiZiAction(int shaiValue)
	{
		auto directSZ =Director::getInstance()->getWinSize();
		auto startPos = Vec2(1280/2,360);
		Sprite* hand = Sprite::create("ZZHOUMJ/sprite/hand.png");
		hand->setPosition(Vec2(directSZ.width/2+100,directSZ.height/2-150));
		hand->setGlobalZOrder(1000);
		this->addChild(hand);
		Sequence* seq = Sequence::create(DelayTime::create(0.8f),RemoveSelf::create(true),nullptr);
		hand->runAction(seq);

		playCommonSound("Sezi");
		auto touziA = Sprite::createWithSpriteFrameName(StringUtils::format("touzi%d.png", shaiValue));
		this->addChild(touziA);

		touziA->setVisible(true);
		touziA->setGlobalZOrder(9);
		touziA->setScale(0.8f);
		touziA->setPosition(startPos);

		auto animationA = getAni("touziAction");
		animationA->setDelayPerUnit(0.01f);
		animationA->setLoops(25);
		animationA->setRestoreOriginalFrame(true);
		auto animateA = Animate::create(animationA);
		touziA->runAction(Sequence::create(
			Spawn::create(
			EaseSineOut::create(MoveTo::create(0.5f, Vec2(directSZ.width * 0.48, directSZ.height*0.63f))),
			animateA,
			nullptr),
			DelayTime::create(3.0f),
			CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, touziA)),
			nullptr));

		int shaiValueB = 0;
		int num = rand() % 3;
		if (shaiValue > 3)
		{
			shaiValueB = shaiValue - num;
		}
		else
		{
			shaiValueB = shaiValue + num;
		}
		auto animationB = getAni("touziAction");
		animationB->setDelayPerUnit(0.01f);
		animationB->setLoops(25);
		animationB->setRestoreOriginalFrame(true);
		auto animateB = Animate::create(animationB);
		auto touziB = Sprite::createWithSpriteFrameName(StringUtils::format("touzi%d.png", shaiValueB));
		this->addChild(touziB);
		touziB->setVisible(true);
		touziB->setGlobalZOrder(9);
		touziB->setScale(0.8f);
		touziB->setPosition(startPos);
		touziB->runAction(Sequence::create(
			Spawn::create(
			EaseSineOut::create(MoveTo::create(0.5f, Vec2(directSZ.width * 0.52, directSZ.height*0.63f))),
			animateB,
			nullptr),
			DelayTime::create(3.0f),
			CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, touziB)),
			nullptr));

	}

	void GameManager::userOffLine(const sitDir& dir)
	{
		COCOS_NODE(Text, StringUtils::format("lixian%d", dir))->setZOrder(20000);
		COCOS_NODE(Text, StringUtils::format("lixian%d", dir))->setVisible(true);
	}


	//显示
	void GameManager::ShowChiTip()
	{
		

		if (_sChiList.empty())
		{
			return;
		}

		Size winSzie = Director::getInstance()->getWinSize();
		int num = _sChiList.size();
		Vec2 posTips;
		if (num == 1)
		{
			std::vector<INT> chiCardArray;
			for (int i = 0; i < _sChiList.at(0).size(); i++)
			{
				chiCardArray.push_back(_sChiList.at(0).at(i));
			}
			sendChiCard(chiCardArray);
			return;
		}
		else if (num >= 3)
		{
			posTips = Vec2(winSzie.width / 2.0f - 240, 150);
		}
		else
		{
			posTips = Vec2(winSzie.width / 2.0f, 150);
		}
		_chiTip = Node::create();
		_chiTip->setPosition(posTips);
		this->addChild(_chiTip, topZorder);

		Button* cancle = Button::create(COCOS_PATH + "Sprite/btn_cancel1.png", COCOS_PATH + "Sprite/btn_cancel2.png");
		cancle->addTouchEventListener(CC_CALLBACK_2(GameManager::qiButtonClickCallBack, this));
		cancle->setPosition(Vec2(10, 140));
		_chiTip->addChild(cancle);
		if (_chiCardList.size() > 0)
		{
			_chiCardList.clear();
		}
		int tag = 0;
		for (size_t i = 0; i < _sChiList.size(); i++)
		{
			Size size = Size((_sChiList.at(i).size()) * 70, 110);
			ui::Scale9Sprite* bg = ui::Scale9Sprite::create(SPRITE_PATH + "bg_hu.png");
			bg->setAnchorPoint(Vec2(0.5f, 0.5f));
			bg->setPosition(Vec2(0 + 270 * i, size.height / 2));
			bg->setContentSize(size);
			_chiTip->addChild(bg);

			int index = 0;
			for (auto v : _sChiList.at(i))
			{
				MahjongCard* card = MahjongCard::create(DI_SOUTH_STAND, SOUTH_DIR, v);
			
				card->setAnchorPoint(Vec2(0.0f, 0.5f));
				card->setScale(0.8f);
				card->setCardEnableTouch(true);
				card->setCardTouchEvent();
				card->setTag(tag);
				card->setPosition(Vec2(3 + index * 70, size.height / 2));
				_chiCardList.push_back(card);
				if (v == _chiPaiCard)
				{
					auto sp = Sprite::create(SPRITE_PATH + "zhengda.png");
					sp->setScale(0.9);
					card->addChild(sp);
					sp->setAnchorPoint(Vec2(0.0f, 0.5f));
					sp->setPosition(Vec2(5,55));

				}
				bg->addChild(card);
				index++;
			}
			tag++;
		}
	
	
	}

	//取消按钮
	void GameManager::qiButtonClickCallBack(Ref* ref, Widget::TouchEventType type)
	{
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::BEGAN:
		{
			setIsHasChiAction(false);
			passAction();
			if (_chiTip)
			{
				_chiTip->setVisible(false);
				_chiTip->removeFromParent();
				_chiTip = nullptr;
			}
			break;
		}
		default:
			break;
		}
	}
	void GameManager::setSeletChiCard(Card* selectCard)
	{
		if (_chiCardList.size() <= 0)
		{
			return;
		}
		std::vector<INT> chiCardArray;
		if (selectCard->getSelect())
		{
			for (Card* card : _chiCardList)
			{
				if (card->getSelect() && card->getTag() == selectCard->getTag())
				{
					chiCardArray.push_back(card->getCardSumNumber());
				}
			}
			sendChiCard(chiCardArray);
		}
		else if (_sChiList.size() == 1)
		{
			for (Card* card : _chiCardList)
			{

				chiCardArray.push_back(card->getCardSumNumber());
			}
			sendChiCard(chiCardArray);
		}
		for (Card* card : _chiCardList)
		{
			if (card->getTag() != selectCard->getTag())
			{
				card->setSelect(false);
				card->setCardColor(Color3B(255, 255, 255));
			}
			else
			{
				card->setSelect(true);
				card->setCardColor(Color3B(255, 255, 195));
			}
		}
	}

	void GameManager::sendChiCard(std::vector<INT> cradArray)
	{
		if (cradArray.size() <= 0)
		{
			return;
		}

		GameProtoC2SActionEat_t toProtData;
		memset(&toProtData, 0, sizeof(toProtData));
		toProtData.actionType = 1;
		for (size_t i = 0; i < cradArray.size(); i++)
		{
			toProtData.chiCardArray[i] = cradArray.at(i);
		}
		toProtData.chiCardCount = 0;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_ActionEat, &toProtData, sizeof(toProtData));
		if (_chiTip)
		{
			_chiTip->setVisible(false);
			_chiTip->removeFromParent();
			_chiTip = nullptr;
		}
		_isHasChiAction = false;
		_chiCardList.clear();
		_sChiList.clear();
		setIsHasChiAction(false);
	}

	void GameManager::setIsAllRoundEnd(bool isEnd)
	{
		_isAllRoundEnd = isEnd;
	}

	void GameManager::userAuto(bool bAuto, bool isend, bool isAction)
	{
		if (!_canclePanle)
		{
			return;
		}
		_canclePanle->setVisible(bAuto);
		if (isAction)
		{
			passAction();
		}
		if (!isend)
		{
			float delayTime = 0.0f;
			if (isAction)
			{
				delayTime = 1.0f;
			}
			else
			{
				delayTime = 0.1f;
			}
			this->runAction(Sequence::createWithTwoActions(DelayTime::create(delayTime), CallFunc::create([=](){
				GameProtoC2STuoGuan_t tuoguanData;
				tuoguanData.bTuoGuan = bAuto;
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_C2S_TuoGuan, (void*)&tuoguanData, sizeof(GameProtoC2STuoGuan_t));
			})));
		}		
	}

	
	void GameManager::clickTuoguanEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)
		{
			return;
		}
		userAuto(true, false, _isHasAction);
	}


	void GameManager::setAutoImgeVisibal(const sitDir& dir, const bool isvisible)
	{
		COCOS_NODE(Sprite, StringUtils::format("tuoguang%d", dir))->setVisible(isvisible);
		COCOS_NODE(Sprite, StringUtils::format("tuoguang%d", dir))->setZOrder(20000);
		if (dir == sitDir::SOUTH_DIR)
		{
			_canclePanle->setVisible(isvisible);
		}
	}



	void GameManager::clickTingPaiTipEvenCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)
		{
			return;
		}
		if (_tingLayer)
		{
			if (_tingLayer->isVisible())//可见的时候收回去
			{
				auto fadeout = FadeOut::create(0.5f);
				auto seq = Sequence::createWithTwoActions(fadeout, CallFunc::create([=](){
					_tingLayer->setVisible(false);
				}));
				_tingLayer->runAction(seq);
			}
			else
			{			
				_tingLayer->setVisible(true);
				auto fadein = FadeIn::create(0.5f);
				_tingLayer->runAction(fadein);
			}
		}
	}


	void GameManager::setTingLayer(const char pActArray[USERCARD_MAX_COUNT], char actCount)
	{
		_tingpaiTipBtn->setVisible(actCount > 0 ? true : false);
		_tingAction->setVisible(actCount > 0 ? true : false);
		if (_tingLayer)
		{
			_tingLayer->setVisible(false);
			_tingLayer->removeFromParent();
			_tingLayer = nullptr;
		}
		if (actCount <= 0)
		{
			return;
		}
		_tingLayer = CSLoader::createNode(COCOS_PATH + "tingNode.csb");
		this->addChild(_tingLayer, 2002);
		_tingLayer->setPosition(Vec2(_tingpaiTipBtn->getPosition().x - 50, _tingpaiTipBtn->getPosition().y - 50));
		_tingLayer->setVisible(false);
		auto bgimg = (ImageView*)_tingLayer->getChildByName("Image_1");
		Size bgSize = bgimg->getContentSize();
		bgimg->setContentSize(Size(bgSize.width + (78 * (actCount - 2)), bgSize.height));
		ListView* tinglist = (ListView*)_tingLayer->getChildByName("ListView_hu");
		tinglist->setContentSize(Size(78 * actCount, 100));
		tinglist->setTouchEnabled(false);
		for (int i = 0; i < actCount; i++)
		{
			ImageView* diSp = ImageView::create("Games/ZZHOUMJ/sprite/img_kuang0.png");
			diSp->setContentSize(Size(40, 57));
			int huCardId = (int)pActArray[i];
			MahjongCard* pCard = MahjongCard::create(DI_FRONT_SOUTH, SOUTH_DIR, huCardId);
			diSp->addChild(pCard);
			pCard->setScale(1.2f);
			Size dispSize = diSp->getContentSize() / 2;
			pCard->setPosition(Size(dispSize.width - 5, dispSize.height - 5));
			tinglist->pushBackCustomItem(diSp);
		}
	}

	void GameManager::setPingbiTing(bool isting)
	{
		
	}

	void GameManager::setAutoBtnEnable(bool benable)
	{
		//_tuoguanBtn->setTouchEnabled(benable);
	}

	void GameManager::showTimeCountByAction(bool isHaveAction, int time)
	{
		if (!_turnTable)
		{
			return;
		}		
		_turnTable->showTimeCountAction(isHaveAction, time);
	}










}
