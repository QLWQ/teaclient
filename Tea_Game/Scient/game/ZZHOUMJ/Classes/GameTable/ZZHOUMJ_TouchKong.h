#ifndef _ZZHOUMJ_TOUCHKONG_H_
#define _ZZHOUMJ_TOUCHKONG_H_

#include "ZZHOUMJ_poolaction.h"

namespace ZZHOUMJ
{

	class TouchKong :
		public PoolAction
	{
	public:
		TouchKong(void);
		~TouchKong(void);

		virtual void run() override;

		CREATE_FUNC(TouchKong);
	};

}

#endif
