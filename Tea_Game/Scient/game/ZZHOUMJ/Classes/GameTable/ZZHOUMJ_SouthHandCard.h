#ifndef ZZHOUMJ_SouthHandCard_h__
#define ZZHOUMJ_SouthHandCard_h__

#include "cocos2d.h"
#include "ZZHOUMJ_CardPool.h"
#include "ZZHOUMJ_HandCard.h"
using namespace cocos2d;

namespace ZZHOUMJ
{
	class ZZHOUMJ_SouthHandCard : public HandCard
	{
	public:
		ZZHOUMJ_SouthHandCard();
		~ZZHOUMJ_SouthHandCard();

		CREATE_FUNC(ZZHOUMJ_SouthHandCard);


		virtual Vec2 refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>normalCards,INT catchCard)override;
		virtual Vec2 refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>tingCards)override;
		virtual Vec2 refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,INT huCardId,bool visibleAllCard)override;
		virtual void setSingleGroupSize(Size groupSize)override;
		virtual void setNormalCardSize(Size normalSize)override;
		virtual void setTingCardSize(Size tingSize)override;
		virtual Vec2 getCatchWorldPos()override;
		virtual void enterChangeCards()override;
		virtual void exitChangeCards()override;
		virtual void putChangeCardToHead(std::vector<INT> cards)override;
		virtual void moveChangeCard(sitDir dir)override;
		
		/*
		*设置点中牌,返回是否能出牌
		*/
		bool setTouchCardIsCanOut(Card* tCard);

		/*
		*选中扣牌
		*/
		void selectKouCard(Card* tCard);

		/*
		* 选中交换的牌
		*/
		void selectChangeCards(Card* tCard);

		
		/*
		* 处理听牌
		*
		* @param handcards		手牌
		* @param canOutCard		能出的牌
		* @param canKouCards	能扣的牌
		* return 返回牌摆到的x坐标
		*/
		float handleTing(std::vector<INT>handcards,std::vector<INT>canOutCard,
			std::vector<INT>canKouCards,std::vector<CardPool::CGroupCardData>groupCards);

	
		/*
		* 获取需要交换的牌
		* return 返回交换的牌
		*/
		std::vector<INT> getChangeCards();

		
		void setHandCardTouch(bool isTouch);

	private:
		Vec2 getNextPosition(Vec2 prePoint, float deltLen);
		virtual bool init() override;
		void refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards);
		
		/*
		处理手牌，可以出的牌
		*/
		void refreshOutCard(std::vector<INT>handcards,std::vector<INT>canOutCard,INT catchCard);

		void resetData();

	private:
		Vec2									_prePoint;
		std::vector<Card*>						_selectChangeCards;
		
	};
}



#endif // HSMJ_SouthHandCard_h__
