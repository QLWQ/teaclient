#ifndef _ZZHOUMJ_GAMEMANAGER_H_
#define _ZZHOUMJ_GAMEMANAGER_H_

#include "ZZHOUMJ_ResourceLoader.h"
#include "ZZHOUMJ_GameTableLogic.h"
#include "ZZHOUMJ_MessageHead.h"
#include "ZZHOUMJ_Factory.h"
#include "ZZHOUMJ_TurnTable.h"


namespace ZZHOUMJ
{
	class GameManager :
		public cocos2d::Layer
	{
	public:
		GameManager(void);
		~GameManager(void);

	public:
		static GameManager* getInstance();
		virtual bool init() override;		
		CREATE_FUNC(GameManager);
		virtual void loadResource();// 加载资源
		cocosResourceLoader* getCR() { return _cLoader; }

	private:
		virtual void initData();						// 初始化数据
		void restartSetData();
		void addLoader();								// 添加资源管理者
		void initButton();								// 设置按钮
		void initUI();									// 配置UI层
		void reParent(std::string name);				// 重置cocos_node节点父节点
	
		//按钮回调函数
		void startButtonClickCallBack(Ref* ref,Widget::TouchEventType type);
		void pengButtonClickCallBack(Ref* ref,Widget::TouchEventType type);
		void gangButtonClickCallBack(Ref* ref,Widget::TouchEventType type);
		void huButtonClickCallBack(Ref* ref,Widget::TouchEventType type);
		void qiButtonClickCallBack(Ref* ref,Widget::TouchEventType type);
		void onGMClickCallBack(Ref* ref,Widget::TouchEventType type);
		void tingButtonClickCallBack(Ref* ref,Widget::TouchEventType type);
		void quxiaotuoguanClickCallBack(Ref* ref, Widget::TouchEventType type);
		void clickTuoguanEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);//托管按钮
		void clickTingPaiTipEvenCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);//点击听牌提示按钮
	private:																																		
		int getRd_A_N(int a, int n);
		void playBgTimer(float dt);	// 播放背景音乐																				

	private:
		static GameManager* _instance;
		
	public:
		void testOutCard();
		void setAutoImgeVisibal(const sitDir& dir, const bool isvisible);
		void showTimeCountByAction(bool isHaveAction,int time);
	private:
		cocosResourceLoader* _cLoader;																					// cocostudio资源
		
	private:
		void initUser(const sitDir& dir,const UserInfoStruct* user);
		void initHandCard(char cardArray[PLAY_COUNT][17],char cardArrayCout[PLAY_COUNT]);
	public:
		virtual void addUser(const sitDir& dir,UserInfoStruct* user);													//  增添玩家
		virtual void userLeave(const sitDir& dir);																		//  玩家离开
		virtual void userAgree(const sitDir& dir);																		//  玩家准备
		virtual void startGame();																						//	所有玩家准备完毕
		virtual void playGame();																						//  开始打牌
		virtual void catchCard(const sitDir& dir, const INT& number);													//  抓牌
	
		virtual void userAuto(bool bAuto,bool isend,bool isAction);//托管,是否托管，游戏是否结束

		virtual void outCard(const sitDir& dir, const INT& number);														//  出牌
		void passAction();																								//  过牌
		void loadUsers();
		void refreshUserMoney(const sitDir& dir, const UserInfoStruct& user);
		void seChiPaiList(std::vector<std::vector<INT>> cradArray);

		void userOffLine(const sitDir& dir);

		Animation* getAni(std::string name);																			// 取帧动画
		void setNt(const sitDir& dir, int bankCount);																					// 设置庄家
		void setCatchDir(const sitDir& dir);																			// 设置抓牌方向
		void finishGame(const std::vector<std::vector<int>>& vvNum);													// 结束游戏
		void resetInit();																								// 初始化
		void agreeGame();
		void showGMButton();
		UserInfoStruct** getVecUser();
		void initAllHandCard(std::vector<std::vector<INT>> vvSouthCard);												// 启动发牌入口
		Card* commonCatchCard(const sitDir& dir, const INT& mahjongNumber);												// 正前抓单张牌
		void addCardPool(CardPool * pool);																				// 增加方位卡片池	
		void resetDiKuang();																							// 重置底框按钮和状态
		std::vector<CardPool *>& getUserCardPool();																		// 获取卡片池
		CardPool* getSelfCardPool();
		Card* createZhengPai(const sitDir& dir, int cardId, bool baddChild = true);
		Card* createBeiPai(const sitDir& dir);
		Card* createPengGangFront(const sitDir& dir, int cardId);
		Card* createTingPai(const sitDir& dir, int cardId);

		void refreshHandCardValue(const sitDir& dir,std::vector<INT> cards);
		
		void refreshKouPai(const sitDir dir,std::vector<INT> kouCards);

		void showHandCardHu(sitDir dir,std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,bool isZiMo,INT huCard,bool visibleAllCard,bool isTianhu,bool isQiangjin,bool isSanjindao,bool isYoujin[3]);
		
		//播放事件动画
		void playThingAction(int thingID, sitDir dir, int cardID, bool isMan, char chiCardArray[CHICARDCOUNT]);
		//播放金动画
		void SHowGoldCardAnimation();
		//播放分饼动画
		void SHowFengbinAnimation();

		//播放麻将牌运动的动画
		void ShowGoldCardAction(sitDir turnDir,int GoldCard);
		//显示金牌
		void ShowGoldCard(Node* sender);
		//刷新金牌之后的牌
		void refreshHandCardGold();
		void showBuhuaTip(bool show);
		void showBuhuaCard(sitDir turnDir, int CardNum);
		Sprite*createHuaTip(int CardNum);
		void playAnimation();


		//指针指向方向
		void turnTableDir(sitDir dir);

		/*
		* 进入听牌操作
		*
		* @param handcards			手牌
		* @param canOutCard			能够出的牌，出一张后就听
		* @param canKouCards		能扣的牌组合,每张牌代表3张 
		* @param kouGroupArray		能组合的扣牌
		* @return					
		*/
		void enterTingHandle(std::vector<INT>handcards,std::vector<INT>canOutCard,
			std::vector<INT>canKouCards);

		/*
		* 重置游戏开始阶段ui
		*/
		void resetBeginStageUI(char bankerNo,sitDir bankerDir,sitDir catchDir,char bySeziBanker,int bankCount);

		/*
		* 重置游戏发牌阶段ui
		*/
		void resetSendCardStageUI(sitDir bankerDir,GameStatusSendCard_t* data);

		/*
		* 出牌和摸牌阶段
		*/
		void resetOutCatchCardStageUI(sitDir bankerDir,GameStatusOutCard_t* data,bool isOutStage);

		/*
		*交换牌阶段
		*/
		void resetChangeCardStageUI(sitDir bankerDir,GameStatusChangeCard_t* data);

		void changeCardBegin();

		
		void changeCardEnd(GameProtoS2CChangeCardFinish* changeCardFinish);

		void changeCardToHead(sitDir dir,char card[3],char cardCount);


		void updateActionScore(INT score[PLAY_COUNT]);

		void showHuTips(const sitDir& dir,std::vector<INT> cards);
		//显示闪电
		void ShowShanDian(sitDir dir);

		/*
		*筛子动画
		*/
		void runShaiZiAction(int shaiValue);
		//显示当前听的牌的提示
		void setTingLayer(const char pActArray[USERCARD_MAX_COUNT], char actCount);
		void setPingbiTing(bool isting);
	public:
		Card* getCard(const sitDir& dir, const INT& mahjongNumber);														// 取正立牌
		Card* getFrontCard(const sitDir& dir, const INT& mahjongNumber);												// 正面牌
		Card* getZhengLiCard(const INT& mahjongNumber);																	// 正立牌
		Card* getZuoLiCard();																							// 左立牌
		Card* getYouLiCard();																							// 右立牌
		Card* getHouLiCard();																							// 后立牌

	public:
		cocos2d::Vec2 getOutCardDeskPos(const sitDir& dir,INT& zOrder);													// 出牌桌面摆放位置
		void afterOutCard(const sitDir& dir);																			// 出牌后
		void startOutCard(const sitDir& dir);																			// 庄家第一次开始出牌
		void setMinusOutCount(const sitDir& dir);																		// 减一次打出的牌数目
		void removeLastOutCard();																						// 移除最后出的牌
		Card* getLastOutCard();
		void buhuaCard(const sitDir& dir, INT buhuaCard);
	private:
		void sendCard(const sitDir& dir/*起始方位*/);																	// 开局发牌
		void sendCardToPool(const INT& cardCount);																		// 发送底牌给方向池, 计时器调用
		void sendCardTimer(float dt);																					// 发牌计时器

	
	private:
		UserInfoStruct*					_vecUser[PLAY_COUNT];															// 游戏人数				
		sitDir							_startDir;																		// 庄家位置
		sitDir							_curSendDir;																	// 当前发牌位置
		sitDir							_startCatchDir;																	// 开始抓牌方向
		INT								_sendCardCount;																	// 发牌的次数
		std::vector<CardPool *>			_vecCardPool;																	// 3方卡片池
		std::vector<std::vector<INT>>	_vvStartHandCard;																// 起始手牌
		INT								_currSendIndex[PLAY_COUNT];														// 正在发的牌最新的位置
		std::vector<INT>				_vecHasOutCardCount;															// 已经出牌的数目
		std::vector<bool>				_vecTingPaiState;																// 玩家是否听牌状态
		std::vector<Card *>				_allOutCardList;																// 所有已经出的牌
		INT								_leftCardCount;																	// 剩余多少张牌没有摸
		Sprite*							_ziMoOrHu[PLAY_COUNT];															// 自摸还是胡牌
		INT								_goldCard;																		//金牌
		// 多分辨率适应
	public:
		void setGlobalScale();																							// 设置全局缩放
	
		// 动作牌
	public:
		PoolAction* _touchCard;																							// 碰牌
		PoolAction* _meldedKong;																						// 明杠
		PoolAction* _concealedKong;																						// 暗杠
		PoolAction* _touchKong;																							// 补杠
		PoolAction* _huCard;																							// 胡牌
		PoolAction*_touchEat;																							//吃牌

		void playCommonSound(std::string soundName);

		// 玩家信息
		bool isMan(int index);																							// 是男人还是女人

	public:  
		void showMeAction(bool canPeng, bool canchi,bool canGang, bool canHu, bool canTing, bool canQiang,bool canDao,bool canyou,bool cantianhu,bool canzimo,char num);
		void setCurrOperNumber(const INT& number);
		const INT& getCurrOperNumber();
		void setCurrOperDir(const sitDir& dir);
		const sitDir& getCurrOperDir();
		void setUserTingPaiState(bool isTingPai,INT index);
		void afterScorePoint(const sitDir& dir, int money);
		void showScoreAnimation(const sitDir& dir, int money);
		bool getUserTingPaiState(INT index);
		void setUserDefine(std::vector<int> define);//解析玩法
		bool SetOrGetHuaSwitch(bool type,bool swit=true);
		void SetGoldCard(INT GoldCard) { _goldCard = GoldCard; };	//设置金牌
		INT GetGoldCard(){ return _goldCard; };	//获得金牌
		void setAutoBtnEnable(bool benable);
	private:
		float			_globalScale;		// 全局缩放
		sitDir			_currOperDir;
		Label*			_PlayerWay;		//只能自模胡
		std::string     _ways;          //玩法集结
		INT				_actionPoint[PLAY_COUNT];//动作分
		ZZHOUMJ_TurnTable*	_turnTable;
		CC_SYNTHESIZE(bool,_isHasAction,IsHasAction);// 是否有动作
		CC_SYNTHESIZE(bool,_isCatchCard,IsCatchCard);// 是否抓牌中
		CC_SYNTHESIZE(bool, _isHasChiAction, IsHasChiAction);// 是否有吃动作
		bool            _HuaSwitch;   //用于处理出牌滑动BUG

		std::map<sitDir, std::vector<Point>>          _sitDirPosChangeCardMap; //保存需要转换手牌的位置
		static Point g_bezControl[bezDir::BEZ_DIR_MAX][2];                                       //贝塞尔曲线控制点
	public:
		void addChangeCardPos(const sitDir& dir, const std::vector<Point>& vPos);//保存所换的牌的位置信息
		/** Creates the runChangeCardAction.
		* @param pNode 动作节点.
		* @param act_type 动作类型.
		* @param bez_dir 贝塞尔曲线的控制方向.
		* @param sit_dir 目标点方向.
		* @param index 位置id.
		*/
		void runChangeCardAction(Card* pNode,const changeActType& act_type,const bezDir& bez_dir,const sitDir& sit_dir,int index);
		/**重新设置牌的位置**/
		void resetChangeCard();
		void  ShowHu(sitDir dir, int cardID);
		void ShowChiTip();
		void setSeletChiCard(Card* selectCard);
		void sendChiCard(std::vector<INT> cradArray);

		void setIsAllRoundEnd(bool isEnd);
		bool getIsAllRoundEnd(){ return _isAllRoundEnd; }

		Node*			_chiTip;		//吃提示
		std::vector<Card*>	_chiCardList;	//吃牌容器
		std::vector<std::vector<INT>>	_sChiList;	        //可以吃的牌
		Sprite *buhutip;
		int _buhuaPos[PLAY_COUNT];
		bool isShow;
		bool isSHowFlowe;
		int _chiPaiCard;
		bool _IsPutongHua;
		bool _isAllRoundEnd = false;
		Button* _tuoguanBtn = nullptr;
		Button* _tingpaiTipBtn = nullptr;
		Node* _tingLayer = nullptr;
		Layout* _canclePanle = nullptr;
		Node*  _tingAction = nullptr;
		bool isPlayerGame = false;
	};

}

#endif // _GAMEMANAGER_H_