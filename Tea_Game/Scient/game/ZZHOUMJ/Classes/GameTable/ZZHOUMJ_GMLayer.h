#ifndef ZZHOUMJ_GMLayer_h__
#define ZZHOUMJ_GMLayer_h__
#include "HNNetExport.h"

namespace ZZHOUMJ
{
	class ZZHOUMJ_GMLayer : public HNLayer
	{
	public:
		ZZHOUMJ_GMLayer();
		~ZZHOUMJ_GMLayer();

		CREATE_FUNC(ZZHOUMJ_GMLayer);

		void setUIData(std::vector<UserInfoStruct> users,std::vector<std::vector<INT>> cards);
	private:
		virtual bool init() override;
		void clickCloseEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		
		bool LeftCardCallBack(HNSocketMessage* socketMessage);

	private:
		cocos2d::Node*	m_pMainNode;
	};
}



#endif // HSMJ_GMLayer_h__
