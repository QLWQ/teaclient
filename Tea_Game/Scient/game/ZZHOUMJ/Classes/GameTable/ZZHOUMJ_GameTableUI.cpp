﻿#include "ZZHOUMJ_GameTableUI.h"
#include "ZZHOUMJ_ChatTip.h"
#include "ZZHOUMJ_Result.h"
#include "ZZHOUMJ_SetLayer.h"

using namespace std;
namespace ZZHOUMJ
{
	/**************************************************************************/

	const int maxOrder = 200;
	const int tipOrder = 99;
	const int Max_Zorder = 100;
	GameTableUI::GameTableUI()
		:
		_labekaCount(nullptr),
		_backBtn(nullptr)
	{
		_isGameEnd = false;
		_isFangzhuFufei = false;
		_isShowAllBtn = false;
	}

	GameTableUI::~GameTableUI()
	{
		delete _tableLogic;
		_tableLogic = nullptr;
	}

	GameTableUI* GameTableUI::create(INT deskNo, bool autoCreate)
	{
		auto ui = new GameTableUI;
		if (ui && ui->init(deskNo, autoCreate))
		{
			ui->autorelease();
			return ui;
		}
		CC_SAFE_DELETE(ui);
		return nullptr;
	}

	bool GameTableUI::init(INT deskNo, bool bAutoCreate)
	{
		if (!HNGameUIBase::init())
		{
			return false;
		}
		//创建按钮框
		_BtnLayout = Layout::create();
		_BtnLayout->setPosition(Vec2(1460, 670));
		//_BtnLayout->setAnchorPoint(Vec2::ZERO);
		_BtnLayout->setSize(Size(400, 50));
		_BtnLayout->setZOrder(Max_Zorder + 201);
		this->addChild(_BtnLayout);
		//HNPreLoadResource::getInstance()->setLoadResPath("Games/ZZHOUMJ/Music/sound.xml");
		_deskNo = deskNo;
		Size winSize = Director::getInstance()->getWinSize();
		//Social::SocialSysManager::getInstance()->addObserver(this);
		_mahjongManager = GameManager::create();
		_mahjongManager->setName("_mahjongManager");
		this->addChild(_mahjongManager,10);

		int roomID = 0;
		ComRoomInfo* pInfo = RoomLogic()->getInstance()->getSelectedRoom();
		if (pInfo)
		{
			roomID =  pInfo->uRoomID;
		}

		_tableLogic = new GameTableLogic(this, deskNo,bAutoCreate);
		_tableLogic->sendGameInfo();

		/*_chatTip = QZHOUMJ_ChatTip::create();
		_chatTip->setPosition(Vec2(winSize.width-60, 50));
		_chatTip->setClickCallBack(CC_CALLBACK_0(GameTableUI::openChatDialog,this));
		this->addChild(_chatTip,maxOrder);*/

		//std::string str1 = PromptDictionary::getInstance().findPromptByKey("Word_QZHOUMJ_GameTableUI_tips1");
		//Label* label1 = Label::createWithTTF(str1,"platform/common/RTWSYueRoudGoG0v1-Regular.ttf",23);
		//label1->setPosition(Vec2(48,700));
		//label1->setColor(Color3B::GREEN);
		//addChild(label1,maxOrder);

		////std::string str2 = PromptDictionary::getInstance().findPromptByKey("Word_QZHOUMJ_GameTableUI_tips2");
		//char num[50]={0};
		//sprintf(num, "%d", deskNo);
		//str2.append(num);
		//Label* label2 = Label::createWithTTF(str2,"platform/common/RTWSYueRoudGoG0v1-Regular.ttf",23);
		//label2->setString(str2);
		//label2->setPosition(Vec2(180,700));
		//label2->setColor(Color3B::YELLOW);
		//addChild(label2,maxOrder);


		//std::string str3 = PromptDictionary::getInstance().findPromptByKey("HNRoom_HNRoomLogic_SystemMes27");
		//_labekaCount = Label::createWithTTF("","platform/common/RTWSYueRoudGoG0v1-Regular.ttf",23);
		//_labekaCount->setString(str3);
		//_labekaCount->setVisible(false);
		//_labekaCount->setPosition(Vec2(300,700));
		//_labekaCount->setColor(Color3B::YELLOW);
		//addChild(_labekaCount,maxOrder);


// 		_inviteBtn = Button::create("QZHOUMJ/cocos/Button/yaoqinghaoyou.png","QZHOUMJ/cocos/Button/yaoqinghaoyou.png","");
// 		_inviteBtn->addTouchEventListener(CC_CALLBACK_2(GameTableUI::clickWebchatEventCallback, this));
// 		_inviteBtn->setPosition(Vec2(winSize.width/2,winSize.height/2-47));
// 		this->addChild(_inviteBtn,maxOrder);

// 		_backBtn = Button::create("Games/QZHOUMJ/sprite/btn_fhdt1.png","Games/QZHOUMJ/sprite/btn_fhdt2.png","");
// 		_backBtn->addTouchEventListener(CC_CALLBACK_2(GameTableUI::clickBackEventCallBack,this));
// 		_backBtn->setPosition(Vec2(1200, winSize.height - 50));
// 		this->addChild(_backBtn,maxOrder);

		//GameVoiceView* pVoiceView = GameVoiceView::create();
		//pVoiceView->ShowVoiceBtn((char*)this, Vec2(winSize.width-40, 150), true);
		//this->addChild(pVoiceView,maxOrder);

		auto set = Button::create("Games/ZZHOUMJ/cocos/Button/btn_shezhi1.png","Games/ZZHOUMJ/cocos/Button/btn_shezhi2.png","");
		set->addTouchEventListener(CC_CALLBACK_2(GameTableUI::clickSetEventCallBack,this));
		set->setPosition(Vec2(90,0));
		_BtnLayout->addChild(set, maxOrder);

		initDissolveRoomOpervation();
		//初始化VIP房间控制器
		
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			roomController = VipRoomController::create(deskNo);
			roomController->onSetVipInfoCallBack = [this](){
				// 更新房主标志显示
				//setDeskInfo();
			};
			roomController->onDissmissCallBack = [this]() {
				_allRoundsEnd = _mahjongManager->getIsAllRoundEnd();
				if (!_allRoundsEnd)
				{
					auto prompt = GamePromptLayer::create();
					prompt->showPrompt("");
					//prompt->setTextJiesanTipVisible(true);
					prompt->setCallBack([=]() {

						RoomLogic()->close();
						GamePlatform::createPlatform();
					});
				}
				else
				{
					if (m_pResultUI)
					{
						m_pResultUI->showAllResultNode(true);
					}
					
				}
			};
			addChild(roomController, Max_Zorder + 200);
			//邀请好友按钮
			roomController->setInvitationBtnTexture("ZZHOUMJ/cocos/Button/yaoqinghaoyou.png");
			// 修改邀请好友位置
			roomController->setInvitationBtnPos(Vec2(_winSize.width / 2, 300));
			// 修改返回位置 隐藏
			roomController->setReturnBtnPos(Vec2(_winSize.width + 500.0f, 0));
			// 修改解散位置 隐藏
			roomController->setDismissBtnPos(Vec2(_winSize.width + 500.0f, 0));
			//修改查看房间信息按钮位置
			roomController->setRoomResultBtnPos(Vec2(_winSize.width + 190, _winSize.height - 50));
			//修改房间号位置
			roomController->setRoomNumPos(Vec2(5, _winSize.height * 0.98f));
			//修改局数位置
			roomController->setPlayCountPos(Vec2(5, _winSize.height * 0.93f));
			//修改底分位置
			roomController->setDifenPos(Vec2(5, _winSize.height * 0.88f));
		}
		gameChatLayer();
		Button* voice = Button::create("ZZHOUMJ/cocos/Button/voice.png", "", "ZZHOUMJ/cocos/Button/voice.png");
		voice->setPosition(Vec2(winSize.width - 50, 170));
		this->addChild(voice, maxOrder);
		Button* chat = Button::create("ZZHOUMJ/cocos/Button/message.png", "", "ZZHOUMJ/cocos/Button/message.png");
		chat->setPosition(Vec2(winSize.width - 50, 250));
		this->addChild(chat, maxOrder);
		voice->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
			if (_chatLayer == nullptr)
			{
				//显示聊天界面
				gameChatLayer();
			}
			_chatLayer->voiceChatUiButtonCallBack(pSender, type);
		});
		chat->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
			if (_chatLayer == nullptr)
			{
				//显示聊天界面
				gameChatLayer();
			}
			_chatLayer->showChatLayer();
		});
		Button* record = Button::create("ZZHOUMJ/cocos/Button/btn_record.png", "", "ZZHOUMJ/cocos/Button/btn_record.png");
		record->setPosition(Vec2(-70, 0));
		_BtnLayout->addChild(record, maxOrder);
		record->addClickEventListener([=](Ref* ref) {
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, S_C_GAME_RECORD, 0, NULL);

		});
		Button* btnDrop = Button::create("ZZHOUMJ/cocos/Button/btn_drop.png", "", "ZZHOUMJ/cocos/Button/btn_drop.png");
		btnDrop->setPosition(Vec2(1230, 670));
		this->addChild(btnDrop, maxOrder + 201);
		btnDrop->addClickEventListener([=](Ref* ref) {
			if (!_isShowAllBtn)
			{
				btnDrop->runAction(Sequence::create(MoveBy::create(0.5f, Vec2(-320, 0)), ScaleTo::create(0.1f, -1), NULL));
				roomController->getRoomInfoBtn()->runAction(MoveBy::create(0.5f, Vec2(-320, 0)));
				_mahjongManager->_tuoguanBtn->runAction(MoveBy::create(0.5f, Vec2(-320, 0)));
				_BtnLayout->runAction(MoveBy::create(0.5f, Vec2(-320, 0)));
				_isShowAllBtn = true;
			}
			else if (_isShowAllBtn)
			{
				btnDrop->runAction(Sequence::create(MoveBy::create(0.5f, Vec2(320, 0)), ScaleTo::create(0.1f, 1), NULL));
				roomController->getRoomInfoBtn()->runAction(MoveBy::create(0.5f, Vec2(320, 0)));
				_mahjongManager->_tuoguanBtn->runAction(MoveBy::create(0.5f, Vec2(320, 0)));
				_BtnLayout->runAction(MoveBy::create(0.5f, Vec2(320, 0)));
				_isShowAllBtn = false;

			}
		});
		return true;
	}



	void GameTableUI::showBackBtn(bool visible)
	{
		//_backBtn->setVisible(visible);
	}

	void GameTableUI::dealLeaveDesk()
	{
		HNAudioEngine::getInstance()->resumeBackgroundMusic();
		if (_isGameEnd)
		{
			return;
		}
		GamePlatform::returnPlatform(LayerType::PLATFORM);
	}

	void GameTableUI::agreeGame(const sitDir& dir)
	{
		_mahjongManager->userAgree(dir);
	}



	/*****************************************************************************************/

	void GameTableUI::outCard(const sitDir& dir, const INT& number)
	{
		_mahjongManager->outCard(dir, number);

	}

	void GameTableUI::catchCard(const sitDir& dir, const INT& number)
	{
		_mahjongManager->catchCard(dir, number);
	}



	void GameTableUI::setDissloveBtState()
	{	
		
	}

	void GameTableUI::setGamecount(int counts)
	{
		/*if (_labekaCount)
		{
		std::string str3 = PromptDictionary::getInstance().findPromptByKey("HNRoom_HNRoomLogic_SystemMes27");
		_labekaCount->setString(StringUtils::format(str3.c_str(),counts));
		_labekaCount->setVisible(true);
		}

		if (counts<=0)
		{
		GamePromptLayerHN* tipLayer=GamePromptLayerHN::create();
		tipLayer->setCallBack([&]()
		{
		GamePlatform::returnPlatform();
		});
		std::string str = PromptDictionary::getInstance().findPromptByKey("HNRoom_HNRoomLogic_SystemMes29");
		tipLayer->setPrompt(str);
		this->addChild(tipLayer,tipOrder);

		}*/

	}

	void GameTableUI::showWechat(bool visible)
	{
		//_inviteBtn->setVisible(visible);
	}


	//设置是否是房主付费
	void GameTableUI::setFuFeiRule(bool isFangzhu)
	{

		_isFangzhuFufei = isFangzhu;


	}

	/*****************************************************************************************/
	void GameTableUI::addUser(const sitDir& dir, UserInfoStruct *user)
	{
		_mahjongManager->addUser(dir, user);

	}

	void GameTableUI::removeUser(const sitDir& dir)
	{
		_mahjongManager->userLeave(dir);
	}

	void GameTableUI::openChatDialog()
	{
		
	}

	void GameTableUI::onSocketMessage(UINT MainID, UINT AssistantID,const rapidjson::Document& doc)
	{
		

	}
	void GameTableUI::clickWebchatEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		

		//std::string str = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_tips3");
		//if (!_isFangzhuFufei)
		//{
		//	str = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_tips5");
		//}
		//char num[200]={0};
		//sprintf(num,str.c_str(),_deskNo);
		//std::string web = GameConfig::getInstance()->getShareIOPic();
		//std::string pic = GameConfig::getInstance()->getSharePic();
		////std::string imagePath = FileUtils::getInstance()->fullPathForFilename(pic);
		////ThridShare::getInstance()->shareWeChat(web.c_str(),num,"",imagePath.c_str());
		//ThridLogin::getInstance()->share("", num, web, pic);

		
		/*
		//GameManager::getInstance()->testOutCard();
		static int test = 0;
		if (test == 0)
		{
			//////////////////////////////////////////////////////////////////////////
			std::vector<CardPool*> pool = GameManager::getInstance()->getUserCardPool();
			CardPool* pSouthPool = pool.at(0);
			std::vector<INT> cards;
			cards.push_back(11);
			cards.push_back(11);
			cards.push_back(11);


			pSouthPool->addHandCard(11);
			pSouthPool->addHandCard(13);
			pSouthPool->addHandCard(21);
			pSouthPool->addHandCard(37);
			pSouthPool->addHandCard(11);
			pSouthPool->addHandCard(13);
			pSouthPool->addHandCard(21);
			pSouthPool->addHandCard(37);
			pSouthPool->addGoupGangPeng(4,12,CardPool::CGroupCard_MingGang);
			pSouthPool->addGoupGangPeng(3,14,CardPool::CGroupCard_Peng);
			pSouthPool->changeCardToHead(cards);

			std::vector<INT> handcards;
			handcards.push_back(13);
			handcards.push_back(13);
			handcards.push_back(13);
			handcards.push_back(13);
			handcards.push_back(13);
			handcards.push_back(13);
			handcards.push_back(13);
			handcards.push_back(11);
			std::vector<CardPool::CGroupCardData>group;
			GameManager::getInstance()->refreshHandCardValue(SOUTH_DIR,handcards);

			CardPool* pEastPool = pool.at(1);
			pEastPool->addHandCard(11);
			pEastPool->addHandCard(13);
			pEastPool->addHandCard(21);
			pEastPool->addHandCard(37);
			pEastPool->addHandCard(11);
			pEastPool->addHandCard(13);
			pEastPool->addHandCard(21);
			pEastPool->addHandCard(37);
			pEastPool->addGoupGangPeng(4,12,CardPool::CGroupCard_MingGang);
			pEastPool->addGoupGangPeng(3,14,CardPool::CGroupCard_Peng);
			pEastPool->changeCardToHead(cards);


			CardPool* pNorthPool = pool.at(2);
			pNorthPool->addHandCard(11);
			pNorthPool->addHandCard(13);
			pNorthPool->addHandCard(21);
			pNorthPool->addHandCard(37);
			pNorthPool->addHandCard(11);
			pNorthPool->addHandCard(13);
			pNorthPool->addHandCard(21);
			pNorthPool->addHandCard(37);
			pNorthPool->addGoupGangPeng(4,12,CardPool::CGroupCard_MingGang);
			pNorthPool->addGoupGangPeng(3,14,CardPool::CGroupCard_Peng);
			pNorthPool->changeCardToHead(cards);


			CardPool* pWestPool = pool.at(3);
			pWestPool->addHandCard(11);
			pWestPool->addHandCard(13);
			pWestPool->addHandCard(21);
			pWestPool->addHandCard(37);
			pWestPool->addHandCard(11);
			pWestPool->addHandCard(13);
			pWestPool->addHandCard(21);
			pWestPool->addHandCard(37);
			pWestPool->addGoupGangPeng(4,12,CardPool::CGroupCard_MingGang);
			pWestPool->addGoupGangPeng(3,14,CardPool::CGroupCard_Peng);
			pWestPool->changeCardToHead(cards);

			std::vector<CardPool::CGroupCardData>groupCards;
			for (int i=0;i<PLAY_COUNT;++i)
			{
				//GameManager::getInstance()->showHandCardHu((sitDir)i,groupCards,cards,true,11,true);
			}

		}
		else
		{
			GameProtoS2CChangeCardFinish pProtData;
			memset(&pProtData,0,sizeof(pProtData));
			memset(pProtData.changeCard,3,sizeof(pProtData.changeCard));
			memset(pProtData.getCard,3,sizeof(pProtData.getCard));
			memset(pProtData.cardArray,3,sizeof(pProtData.cardArray));
			memset(pProtData.countArray,6,sizeof(pProtData.countArray));
			pProtData.changeSezi=3;
			GameManager::getInstance()->changeCardEnd(&pProtData);
		}

		test++;
		
		*/

	}

	void GameTableUI::clickBackEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		GamePlatform::returnPlatform(LayerType::PLATFORM);
	}

	void GameTableUI::clickSetEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}
		auto setLayer = ZZHOUMJSetLayer::create();
		setLayer->showSet(this, 301);
		setLayer->setName("setLayer");
		setLayer->onExitCallBack = [=]() {
			if (roomController)
			{
				roomController->returnBtnCallBack(pSender);
			}
			else
			{
				_tableLogic->sendStandUp();
			}
			removeSetLayer();
		};

		setLayer->onDisCallBack = [=]() {

			if (roomController) roomController->dismissBtnCallBack(pSender);
			removeSetLayer();
		};
	}

	void GameTableUI::initDissolveRoomOpervation()
	{
		//Size winsize=Director::getInstance()->getWinSize();
		//_dissloveRoom =DissolveRoom::create(_deskNo,PlatformLogic()->loginResult.dwUserID);
		//_dissloveRoom->setAnchorPoint(Vec2(0.5,0.5));
		//_dissloveRoom->setPosition(Vec2(winsize.width+10,730));		
		//_dissloveRoom->setGameLogic(_tableLogic);
		//_dissloveRoom->onFangZhuCallBack(true);
		//_dissloveRoom->setVisible(true);
		//this->addChild(_dissloveRoom,maxOrder);

	}


	void GameTableUI::calSameIpUser(std::vector<UserInfoStruct*>& same)
	{
		std::vector<UserInfoStruct*> users;
		UserInfoModule()->findDeskUsers(_deskNo, users);
		if (PLAYER_COUNT != users.size())
		{
			return;
		}
		
		for (size_t i = 0; i < users.size(); i++)
		{
			std::vector<UserInfoStruct*> findSameIp;
			findSameIp.push_back(users.at(i));
			for (size_t j = i+1;j < users.size(); j++)
			{
				if (users.at(i)->dwUserIP == users.at(j)->dwUserIP)
				{
					findSameIp.push_back(users.at(j));
				}
			}
			
			if (findSameIp.size()>=2)
			{
				for (auto v:findSameIp)
				{
					same.push_back(v);
				}
				return;
			}
		}
	}

	
	Node* GameTableUI::createGameFinishNode()
	{
		if (m_pResultUI)
		{
			m_pResultUI = NULL;
		}
		_mahjongManager->setIsAllRoundEnd(true);
		//_mahjongManager->setPingbiTing(false);
		m_pResultUI = MJGameResult::create();
		this->addChild(m_pResultUI, 1000);
		return m_pResultUI;
	}

	void GameTableUI::checkIpSameTips()
	{
		//
		std::vector<UserInfoStruct*> same;
		calSameIpUser(same);

	/*	if (same.empty()==false)
		{
			std::string tips;
			for (size_t i =0;i<same.size();++i)
			{
				tips+=GBKToUtf8(same.at(i)->nickName);
				if (i!=same.size()-1)
				{
					tips+=PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_he");
				}
				tips+="\n";
			}
			tips+=PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_XiangTongIp");

			GameTips* test = GameTips::getInstance();
			test->setLocalZOrder(maxOrder);
			test->setDissolveRoom(_dissloveRoom);
			test->setTipsText(tips);


		}*/

	}

	void GameTableUI::showGameDeskNotFound()
	{/*
		std::string text = PromptDictionary::getInstance().findPromptByKey("GameTableLogic_tips1");
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(text);
		prompt->setCallBack([this](){
			GamePlatform::returnPlatform();
		});*/
	}


	void GameTableUI::onGameDisconnect(bool isReconnect)
	{
		
	}

	void GameTableUI::onChatTextMsg(BYTE seatNo, CHAR msg[])
	{
		auto userInfo = _tableLogic->getUserBySeatNo(seatNo);
		auto viewSeatNo = _tableLogic->logicToViewSeatNo(seatNo);

		auto herd = COCOS_NODE(ImageView, StringUtils::format("head%d", viewSeatNo));
		Vec2 bubblePostion = herd->getPosition();

		bool isFilpped = true;

		if (viewSeatNo == 0 || viewSeatNo == 3)
		{
			isFilpped = false;
		}
		else
		{
			isFilpped = true;
		}

		_chatLayer->onHandleTextMessage(herd->getParent(), bubblePostion, msg, userInfo->nickName, userInfo->bBoy, isFilpped, viewSeatNo);
	}

	void GameTableUI::onChatVoiceMsg(int userID, int voiceID, int voiceTime)
	{
		if (nullptr == _chatLayer) return;

		UserInfoStruct* userInfo = _tableLogic->getUserByUserID(userID);

		if (nullptr == userInfo) return;
		auto viewSeatNo = _tableLogic->logicToViewSeatNo(userInfo->bDeskStation);
		auto herd = COCOS_NODE(ImageView, StringUtils::format("head%d", viewSeatNo));

		Vec2 bubblePostion = herd->getParent()->convertToWorldSpace(herd->getPosition());

		Size headSize = herd->getContentSize();

		bool isFilpped = true;

		if (viewSeatNo == 0 || viewSeatNo == 3)
		{
			isFilpped = false;
		}
		else
		{
			isFilpped = true;
		}
		_chatLayer->onHandleVocieMessage(herd->getParent(), bubblePostion, voiceID, userInfo->nickName, isFilpped, viewSeatNo, voiceTime);
	}

	void GameTableUI::setDeskInfo()
	{
		auto info = _tableLogic->getUserByUserID(HNPlatformConfig()->getMasterID());
		if (info)
		{
			auto viewSeatNo = _tableLogic->logicToViewSeatNo(info->bDeskStation);
			COCOS_NODE(Sprite, StringUtils::format("zhuang%d", viewSeatNo))->setVisible(true);
		}

// 		if (HNPlatformConfig()->getNowCount() > 0)
// 		{
// 			Layout* tableLayout = (Layout*)(_tableWidget->getChildByName("gold_Poker"));
// 			Layout* layout_buttons = (Layout*)(tableLayout->getChildByName("layout_middle"));
// 			Button* btn_start = (Button*)(layout_buttons->getChildByName("btn_start"));
// 			btn_start->setPositionX(_winSize.width / 2);
// 		}
	}

	void GameTableUI::removeSetLayer()
	{
		auto setLayout = this->getChildByName("setLayer");
		if (setLayout)
		{
			setLayout->removeFromParent();
		}
	}

	void GameTableUI::gameChatLayer()
	{
		_chatLayer = GameChatLayer::create();
		addChild(_chatLayer, Max_Zorder + 500);
		_chatLayer->setPosition(_winSize / 2);
		_chatLayer->onSendTextCallBack = [=](const std::string msg) {
			if (!msg.empty())
			{
				_tableLogic->sendChatMsg(msg);
			}
		};
	}

	void GameTableUI::sendRecordData(S_C_GameRecordResult* data)
	{
		int PlayerCount = 0;
		CHAR* szName[10];
		for (int j = 0; j < 10; j++)
		{
			szName[j] = "";
		}
		int index = 0;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* pUser = _tableLogic->getUserBySeatNo(i);
			if (pUser)
			{
				PlayerCount++;
				szName[index] = pUser->nickName;
				index++;
			}
		}

		auto gameRecord = GameRecord::createWithData(szName, data->JuCount, data->GameCount, PlayerCount);
		addChild(gameRecord, 100000000);
	}


};

