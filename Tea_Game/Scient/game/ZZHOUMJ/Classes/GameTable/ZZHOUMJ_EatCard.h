#ifndef _ZZHOUMJ_EATACTION_H_
#define _ZZHOUMJ_EATACTION_H_
#include "ZZHOUMJ_poolaction.h"

namespace ZZHOUMJ
{

	class EatCard :
		public PoolAction
	{
	public:
		EatCard(void);
		~EatCard(void);

		virtual void run() override;

		CREATE_FUNC(EatCard);

	};

}
#endif