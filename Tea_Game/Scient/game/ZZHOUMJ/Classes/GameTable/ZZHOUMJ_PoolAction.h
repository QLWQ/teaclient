#ifndef _ZZHOUMJ_POOLACTION_H_
#define _ZZHOUMJ_POOLACTION_H_

#include "cocos2d.h"
#include "ZZHOUMJ_CardPool.h"

namespace ZZHOUMJ
{
	class PoolAction :
		public cocos2d::Node
	{
	public:
		PoolAction(void);
		~PoolAction(void);

		void setInfo(sitDir dir, const INT& cardNumber);
		void uiAction(std::string name);
		virtual void run() = 0;
		static void playSexActionSound(bool isMan, std::string actionName);
		static void playSexNumberSound(bool isMan, INT cardNumber);
	private:
		void  getAni(std::string name,Vec2 pos);
		
	protected:
		sitDir _dir;
		INT _cardNumber;
		Vector<SpriteFrame *> _vSpFrame;
		Sprite* _runSp;
	};
}

#endif