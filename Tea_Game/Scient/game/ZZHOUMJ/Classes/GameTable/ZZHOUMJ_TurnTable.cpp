#include "ZZHOUMJ_TurnTable.h"
#include "ZZHOUMJ_GameManager.h"
#include "cocostudio/CocoStudio.h"

namespace ZZHOUMJ
{
	const static std::string SPRITEPATH= GAME_SOURCE_PAHT+"cocos/Sprite/";

	ZZHOUMJ_TurnTable::ZZHOUMJ_TurnTable() :
		_loader(nullptr),
		_qianHouSiTips(nullptr),
		_qianSiHouSiText(nullptr)
	{

	}

	ZZHOUMJ_TurnTable::~ZZHOUMJ_TurnTable()
	{

	}

	void ZZHOUMJ_TurnTable::turnTableDir(sitDir dir)
	{
		for (int i =1;i<=4;++i)
		{
			char text[100] ={0};
			sprintf(text,"pointer%d",i);

			Node* node =  _loader->getChildByName(text);
			if (node ==  nullptr)
			{
				continue;
			}

			if (i==dir+1)
			{
				node->stopAllActions();
				setAllinit(dir);
				Blink* bl=Blink::create(2.0,1.0);
				RepeatForever* reAction=RepeatForever::create(bl);
				node->runAction(reAction);
			}
			else
			{
				node->stopAllActions();
				node->setVisible(false);
			}

		}
		
	}

	void ZZHOUMJ_TurnTable::setTurnInitDir(sitDir dir)
	{
		auto temp = _loader->getChildByName("direction");
		if (temp == nullptr)
		{
			return;
		}

		switch (dir)
		{
		case ZZHOUMJ::SOUTH_DIR:
			{
				Sprite* zhuobu = (Sprite*)temp->getChildByName("direction_bg");
				zhuobu->setVisible(true);
				zhuobu->setTexture(SPRITEPATH + "zhuobu_dong.png");

				Sprite* spDir1=(Sprite*)temp->getChildByName("SP_Dir1");
				spDir1->setVisible(true);
				spDir1->setTexture(SPRITEPATH+"dong_1.png");

				Sprite* spDir2=(Sprite*)temp->getChildByName("SP_Dir2");
				spDir2->setVisible(true);
				spDir2->setTexture(SPRITEPATH+"nan_2.png");
				//spDir2->setRotation(-90.0f);

				Sprite* spDir3=(Sprite*)temp->getChildByName("SP_Dir3");
				spDir3->setVisible(true);
				spDir3->setTexture(SPRITEPATH+"xi_3.png");


				Sprite* spDir4=(Sprite*)temp->getChildByName("SP_Dir4");
				spDir4->setVisible(true);
				spDir4->setTexture(SPRITEPATH+"bei_4.png");
				//spDir4->setRotation(90.0f);


			}
			break;
		case ZZHOUMJ::WEST_DIR:
			{

				Sprite* zhuobu = (Sprite*)temp->getChildByName("direction_bg");
				zhuobu->setVisible(true); 
				zhuobu->setTexture(SPRITEPATH + "zhuobu_nan.png");

				Sprite* spDir1=(Sprite*)temp->getChildByName("SP_Dir1");
				spDir1->setTexture(SPRITEPATH+"nan_1.png");
				spDir1->setVisible(true);

				Sprite* spDir2=(Sprite*)temp->getChildByName("SP_Dir2");
				spDir2->setTexture(SPRITEPATH+"xi_2.png");
				spDir2->setVisible(true);
				//spDir2->setRotation(-90.0f);


				Sprite* spDir3=(Sprite*)temp->getChildByName("SP_Dir3");
				spDir3->setVisible(true);
				spDir3->setTexture(SPRITEPATH+"bei_bei.png");


				Sprite* spDir4=(Sprite*)temp->getChildByName("SP_Dir4");
				spDir4->setVisible(true);
				spDir4->setTexture(SPRITEPATH+"dong_4.png");
				//spDir4->setRotation(90.0f);

			}
			break;
		case ZZHOUMJ::EAST_DIR:
			{

				Sprite* zhuobu = (Sprite*)temp->getChildByName("direction_bg");
				zhuobu->setVisible(true);
				zhuobu->setTexture(SPRITEPATH + "zhuobu_bei.png");

				Sprite* spDir1=(Sprite*)temp->getChildByName("SP_Dir1");
				spDir1->setTexture(SPRITEPATH+"bei_1.png");
				spDir1->setVisible(true);

				Sprite* spDir2=(Sprite*)temp->getChildByName("SP_Dir2");
				spDir2->setTexture(SPRITEPATH+"dong_2.png");
				spDir2->setVisible(true);
				//spDir2->setRotation(-90.0f);

				Sprite* spDir3=(Sprite*)temp->getChildByName("SP_Dir3");
				spDir3->setVisible(true);
				spDir3->setTexture(SPRITEPATH+"nan_3.png");


				Sprite* spDir4=(Sprite*)temp->getChildByName("SP_Dir4");
				spDir4->setVisible(true);
				spDir4->setTexture(SPRITEPATH+"xi_4.png");
				//spDir4->setRotation(90.0f);
			}
			break;
		case ZZHOUMJ::NORTH_DIR:
			{
				Sprite* zhuobu = (Sprite*)temp->getChildByName("direction_bg");
				zhuobu->setVisible(true);
				zhuobu->setTexture(SPRITEPATH + "zhuobu_xi.png");

				Sprite* spDir1=(Sprite*)temp->getChildByName("SP_Dir1");
				spDir1->setTexture(SPRITEPATH+"xi_1.png");
				spDir1->setVisible(true);

				Sprite* spDir2=(Sprite*)temp->getChildByName("SP_Dir2");
				spDir2->setTexture(SPRITEPATH+"bei_2.png");
				spDir2->setVisible(true);
				//spDir2->setRotation(-90.0f);

				Sprite* spDir3=(Sprite*)temp->getChildByName("SP_Dir3");
				spDir3->setVisible(true);
				spDir3->setTexture(SPRITEPATH+"dong_3.png");


				Sprite* spDir4=(Sprite*)temp->getChildByName("SP_Dir4");
				spDir4->setVisible(true);
				spDir4->setTexture(SPRITEPATH+"nan_4.png");
				//spDir4->setRotation(90.0f);
			}
			break;
		default:
			break;
		}
		setAllinit(ZZHOUMJ::MID_DIR);

	}

	void ZZHOUMJ_TurnTable::setAllinit(sitDir dir)
	{
	
		auto temp = _loader->getChildByName("direction");
		if (temp == nullptr)
		{
			return;
		}

		Sprite* spDir1 = (Sprite*)temp->getChildByName("SP_Dir1");
	
		spDir1->setVisible(false);

		Sprite* spDir2 = (Sprite*)temp->getChildByName("SP_Dir2");
		spDir2->setVisible(false);
	
		Sprite* spDir3 = (Sprite*)temp->getChildByName("SP_Dir3");
		spDir3->setVisible(false);

		Sprite* spDir4 = (Sprite*)temp->getChildByName("SP_Dir4");
		spDir4->setVisible(false);
		if (dir >= 0)
		{
			char text[100] = { 0 };
			sprintf(text, "SP_Dir%d", dir+1);
			Sprite* spDir = (Sprite*)temp->getChildByName(text);
			spDir->setVisible(true);
		}

	}

	void ZZHOUMJ_TurnTable::setCardCount(int count)
	{
		Text* crdCount = (Text*)_loader->getChildByName("leftCount");
		std::string str = StringUtils::format("%s%d%s", GBKToUtf8("ʣ"), count, GBKToUtf8("��"));
		crdCount->setString(str);
		/*
		Text* crdCount = (Text*) _loader->getChildByName("leftCount");
		std::string str = PromptDictionary::getInstance().findPromptByKey("Word_ZZHOUMJ_GameTableUI_tips4");
		char num[200]={0};
		sprintf(num,str.c_str(),count);
		crdCount->setString(num);*/

		//if (count==55)
		//{
		//	std::string str = PromptDictionary::getInstance().findPromptByKey("Word_HSMJ_GameTableUI_qianSiKaiShi");
		//	_qianHouSiTips->setVisible(true);
		//	_qianSiHouSiText->setString(str);
		//}
		//else if (count==51)
		//{
		//	std::string str = PromptDictionary::getInstance().findPromptByKey("Word_HSMJ_GameTableUI_qianSiKaiJieShu");
		//	_qianHouSiTips->setVisible(true);
		//	_qianSiHouSiText->setString(str);
		//}
		//else if(count==4)
		//{
		//	std::string str = PromptDictionary::getInstance().findPromptByKey("Word_HSMJ_GameTableUI_houSiKaiShi");
		//	_qianHouSiTips->setVisible(true);
		//	_qianSiHouSiText->setString(str);
		//}
		//else if (count == 0)
		//{
		//	std::string str = PromptDictionary::getInstance().findPromptByKey("Word_HSMJ_GameTableUI_houSiJieShu");
		//	_qianHouSiTips->setVisible(true);
		//	_qianSiHouSiText->setString(str);
		//}
		//else
		//{
		//	_qianHouSiTips->setVisible(false);
		//	_qianSiHouSiText->setString("");
		//}
	}

	void ZZHOUMJ_TurnTable::setLeftCardVisble(bool isVisble)
	{
		Text* crdCount = (Text*) _loader->getChildByName("leftCount");
		crdCount->setVisible(isVisble);
	}

	bool ZZHOUMJ_TurnTable::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		_loader = CSLoader::createNode(COCOS_PATH+"turnTable.csb"); 
		Size winSize = Director::getInstance()->getWinSize();
		_loader->setPosition(winSize.width/2,winSize.height*0.60f);
		this->addChild(_loader);

		_qianHouSiTips = Sprite::create(SPRITE_PATH+"bg_q4js.png");
		_qianHouSiTips->setPosition(winSize.width/2,winSize.height*0.45f);
		_qianHouSiTips->setVisible(false);
		this->addChild(_qianHouSiTips);

		_qianSiHouSiText = Text::create();
		_qianSiHouSiText->setFontSize(30);
		_qianSiHouSiText->setPosition(_qianHouSiTips->getContentSize()/2);
		_qianHouSiTips->addChild(_qianSiHouSiText);

		_timeCount = (TextAtlas*)_loader->getChildByName("AtlasLabel_time");
		return true;
	}


	void ZZHOUMJ_TurnTable::updateTimeCount(float dt)
	{
		_nowTime--;
		_timeCount->setString(StringUtils::format("%d", _nowTime));
		if (_nowTime <= 3 && _nowTime > 0)
		{
			HNAudioEngine::getInstance()->playEffect("ZZHOUMJ/Music/common/CountDown.mp3");
		}
		if (_nowTime <= 0)
		{
			_nowTime = 0;
			GameManager::getInstance()->setIsHasChiAction(false);
			GameManager::getInstance()->resetDiKuang();
			_timeCount->setVisible(false);
			unschedule(schedule_selector(ZZHOUMJ_TurnTable::updateTimeCount));
		}
	}

	void ZZHOUMJ_TurnTable::showTimeCountAction(bool isVisble, int time)
	{
		_timeCount->setVisible(isVisble);
		_timeCount->setString(StringUtils::format("%d", time));
		
		if (time > 0)
		{
			_nowTime = time;
			schedule(schedule_selector(ZZHOUMJ_TurnTable::updateTimeCount), 1.0f);
		}
		else
		{
			_nowTime = 0;
			GameManager::getInstance()->setIsHasChiAction(false);
			GameManager::getInstance()->resetDiKuang();
			unschedule(schedule_selector(ZZHOUMJ_TurnTable::updateTimeCount));
		}
	}

}
