#ifndef _ZZHOUMJ_EASTMAHJONGCARDPOOL_H_
#define _ZZHOUMJ_EASTMAHJONGCARDPOOL_H_


#include "ZZHOUMJ_EastHandCard.h"
#include "ZZHOUMJ_MahjongCardPool.h"

namespace ZZHOUMJ
{
	class EastMahjongCardPool :
		public MahjongCardPool
	{
	public:
		EastMahjongCardPool(void);
		~EastMahjongCardPool(void);

		CREATE_COUNT(EastMahjongCardPool);
		virtual bool init(INT count);

		virtual void setHandCardPos(INT catchCard) override;					 // ��������
		virtual int  getZhuaPaiZOrder();
		virtual void refreshAllShowCard()override;
		virtual cocos2d::Vec2 getCatchPos()override;
		virtual Vec2 getOutToDeskPos()override;

		virtual void showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> allHandCards,INT huCard,bool visibleAllCard) override;
		virtual void showHuTips(std::vector<INT> huTip)override;
		virtual void changeCardToHead(std::vector<INT> cards)override;
	};

}

#endif