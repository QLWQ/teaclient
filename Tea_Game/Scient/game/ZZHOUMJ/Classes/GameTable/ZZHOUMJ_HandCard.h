#ifndef ZZHOUMJ_HandCard_h__
#define ZZHOUMJ_HandCard_h__

#include "cocos2d.h"
#include "ZZHOUMJ_CardPool.h"
using namespace cocos2d;

namespace ZZHOUMJ
{
	class HandCard: public Layer
	{
	public:
		HandCard();
		~HandCard();

		CREATE_FUNC(HandCard);

		virtual bool init()override;

		/*
		* 刷新手牌
		*
		* @param groupCards		碰杠牌列表
		* @param normalCards	普通牌
		* @param catchCard		刚刚捉到的手牌（0：不是刚刚捉到牌）
		* return 返回牌摆到的坐标
		*/
		virtual Vec2 refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>normalCards,INT catchCard);

		/*
		* 刷新手牌(听牌)
		*
		* @param groupCards		碰杠扣牌列表
		* @param tingCards		听牌
		* return 返回牌摆到的坐标
		*/
		virtual Vec2 refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>tingCards);

		/*
		* 刷新胡
		*
		* @param groupCards		碰杠扣牌列表
		* @param handCards		手牌
		* return 返回牌摆到的坐标
		*/
		virtual Vec2 refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,INT huCardId,bool visibleAllCard);

		virtual void setSingleGroupSize(Size groupSize){};
		virtual void setNormalCardSize(Size normalSize){};
		virtual void setTingCardSize(Size tingSize){};


		/*
		获取摸牌位置，世界坐标
		*/
		virtual Vec2 getCatchWorldPos();

		/*
		*进入交换牌
		*/
		virtual void enterChangeCards();

		/*
		*退出交换牌
		*/
		virtual void exitChangeCards();

		/*
		*交换牌放前面
		*/
		virtual void putChangeCardToHead(std::vector<INT> cards);
		
		/*
		*交换牌到指定的位置
		*/
		virtual void moveChangeCard(sitDir dir){};

		void setLeftChangeCard(std::vector<INT> cards);

		void removeChangeCard(INT card);
		void addDrawChangeCard(INT card);

		bool isCanDrawChangeCard(INT card);

	protected:
		Size									_groupSize;
		Size									_normalSize;
		Size									_tingSize;
		bool									_isInChange;
		std::vector<CardPool::CGroupCardData>	_groupCards;
		std::list<Card*>						_handCardsList;	//手牌，不含已经出了的杠碰
		std::list<Card*>						_groupCardsList;//组牌（杠碰）
		std::vector<Card*>						_changeCardsInHead;
		std::vector<INT>						_leftChangeCard;
		std::vector<INT>						_drawChangeCard;
		
	};

}


#endif // HSMJ_HandCard_h__
