#include "ZZHOUMJ_NorthHandCard.h"
#include "ZZHOUMJ_GameManager.h"

namespace ZZHOUMJ
{
	static float heightY = 670;
	static cocos2d::Vec2 beginDirPos = Vec2(930, 670);
	static cocos2d::Vec2 endDirPos =  Vec2(330, 670);

	ZZHOUMJ_NorthHandCard::ZZHOUMJ_NorthHandCard()
	{
		_groupSize = Size(36,50);
		_normalSize = Size(36,50);
		_tingSize = Size(36,50);
	}

	ZZHOUMJ_NorthHandCard::~ZZHOUMJ_NorthHandCard()
	{
	}

	bool ZZHOUMJ_NorthHandCard::init()
	{
		if (!HandCard::init())
		{
			return false;
		}
		return true;
	}


	void ZZHOUMJ_NorthHandCard::refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards)
	{
		//处理碰杠牌，碰杠牌放左边,扣放右边
		std::vector<CardPool::CGroupCardData> pengGangList;
		for (auto value:groupCards)
		{
			if ( value._iType==CardPool::CGroupCard_AnGang 
				|| value._iType == CardPool::CGroupCard_MingGang 
				|| value._iType == CardPool::CGroupCard_Chi
				|| value._iType ==CardPool::CGroupCard_Peng)
			{
				pengGangList.push_back(value);
			}
		}

		//摆牌碰杠
		for(auto pengGang:pengGangList)
		{
			for (int i=0; i<pengGang._iCount; i++)
			{
				Card* pCard = nullptr;
				cocos2d::Sprite*ZheZhao = NULL;
				if (pengGang._iType == CardPool::CGroupCard_AnGang && i<3)
				{
					pCard = GameManager::getInstance()->createBeiPai(NORTH_DIR);
				}
				else if (pengGang._iType == CardPool::CGroupCard_Chi)
				{
					pCard = GameManager::getInstance()->createPengGangFront(NORTH_DIR, pengGang._iChiCardArray[i]);
					if (pengGang._iChiCardArray[i] == pengGang._iCardId)
					{
						ZheZhao = Sprite::create(SPRITE_PATH + "chunorth.png");
					}
				}
				else
				{
					if (pengGang._iType == CardPool::CGroupCard_Peng)
					{
						if (i == 0)
						{
							ZheZhao = Sprite::create(SPRITE_PATH + "chunorth.png");
						}
					}
					pCard = GameManager::getInstance()->createPengGangFront(NORTH_DIR, pengGang._iCardId);
				}

				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setCardEnableTouch(false);
				pCard->setVisible(true);
				pCard->setGlobalZOrder(100 - i);
				if (ZheZhao != NULL)
				{
					ZheZhao->setScale(0.9);
					ZheZhao->setPosition(23,30);
					pCard->addChild(ZheZhao, 1);
					ZheZhao->setVisible(true);
				}


				if (i==3)
				{
					Vec2 pos = getNextPosition(_prePoint,-_groupSize.width);
					pCard->setGlobalZOrder(100 + i);
					pCard->setPosition(Vec2(pos.x,pos.y+10));
				}
				else
				{
					_prePoint = getNextPosition(_prePoint,_groupSize.width);
					pCard->setGlobalZOrder(100 + i);
					pCard->setPosition(_prePoint);

				}
				this->addChild(pCard);
				_groupCardsList.push_back(pCard);
			}
			_prePoint = getNextPosition(_prePoint,_groupSize.width/3.5f);
		}

	}

	void ZZHOUMJ_NorthHandCard::refreshOutCard(std::vector<INT>handcards)
	{
		//牌还没出的手牌
		int index = 0;
		for (auto iter = handcards.begin(); iter != handcards.end(); iter++)
		{
			_prePoint = getNextPosition(_prePoint,_normalSize.width);
			Card* pCard= MahjongCard::create(mahjongCreateType::DI_NORTH_STAND, NORTH_DIR,0);
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			pCard->setGlobalZOrder(100 - index++);
			pCard->setVisible(true);
			this->addChild(pCard);
			pCard->setPosition(_prePoint);
			_handCardsList.push_back(pCard);
			//不可以点击
			pCard->setCardEnableTouch(false);
		
		}

		if (handcards.size()>0)
		{
			_prePoint = getNextPosition(_prePoint,_normalSize.width/2);
		}
	}

	

	void ZZHOUMJ_NorthHandCard::resetData()
	{
		
		for (auto v:_groupCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_handCardsList)
		{
			v->removeFromParent();
		}

		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}

		_groupCards.clear();
		_groupCardsList.clear();
		_handCardsList.clear();
		_changeCardsInHead.clear();
		_prePoint=beginDirPos;

	}


	Vec2 ZZHOUMJ_NorthHandCard::refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>normalCards, INT catchCard)
	{
		resetData();
		_groupCards.clear();
		for (auto v:groupCards)
		{
			_groupCards.push_back(v);
		}

		refreshPengGang(groupCards);

		refreshOutCard(normalCards);
		
		return Vec2::ZERO;
	}

	
	Vec2 ZZHOUMJ_NorthHandCard::refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>tingCards)
	{
		resetData();
		refreshPengGang(groupCards);

		//牌还没出的手牌
		int index = 0;
		int size = tingCards.size();//用于记录玩家手牌数量
		int num=0;
		for (auto iter = tingCards.begin(); iter != tingCards.end(); iter++)
		{
			num++;
			_prePoint = getNextPosition(_prePoint,_tingSize.width);
			Card*pCard=nullptr;
			if(size==num)
			{
				pCard= MahjongCard::create(mahjongCreateType::DI_NORTH_BACK, NORTH_DIR);
				num=0;
			}
			else
			{
				pCard= MahjongCard::create(mahjongCreateType::DI_NORTH_STAND, NORTH_DIR,0);
			}
			pCard->setAnchorPoint(Vec2(0.5f,0.5f));
			pCard->setGlobalZOrder(100 - index++);
			pCard->setVisible(true);
			pCard->setCardEnableTouch(false);
			this->addChild(pCard);
			pCard->setPosition(_prePoint);
			_handCardsList.push_back(pCard);
		}

		if (tingCards.size()>0)
		{
			_prePoint = getNextPosition(_prePoint,_tingSize.width/2);
		}

	
		return Vec2::ZERO;
	}


	Vec2 ZZHOUMJ_NorthHandCard::refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>handCards, INT huCardId, bool visibleAllCard)
	{
		resetData();
		_groupCards.clear();
		for (auto v:groupCards)
		{
			_groupCards.push_back(v);
		}

		refreshPengGang(groupCards);
		
		bool  isFind = false;
		for (auto iter = handCards.begin(); iter != handCards.end(); iter++)
		{
			_prePoint = getNextPosition(_prePoint,_tingSize.width);
			if (visibleAllCard)
			{
				Card* pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_NORTH, NORTH_DIR,*iter);
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setGlobalZOrder(100);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);

				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard->setHuVisible(true);
				}

			}
			else
			{
				Card* pCard	= nullptr;
				if (iter+1 == handCards.end()  && (*iter) == huCardId)
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_FRONT_NORTH, NORTH_DIR, *iter);
					pCard->setHuVisible(true);
				}
				else
				{
					pCard= MahjongCard::create(mahjongCreateType::DI_NORTH_BACK, NORTH_DIR);
				}
				pCard->setAnchorPoint(Vec2(0.5f,0.5f));
				pCard->setGlobalZOrder(100);
				pCard->setVisible(true);
				pCard->setCardEnableTouch(false);
				this->addChild(pCard);
				pCard->setPosition(_prePoint);
				_handCardsList.push_back(pCard);
			}
		}

		return Vec2::ZERO;
	}

	void ZZHOUMJ_NorthHandCard::setSingleGroupSize(Size groupSize)
	{
		_groupSize = groupSize;
	}

	void ZZHOUMJ_NorthHandCard::setNormalCardSize(Size normalSize)
	{
		_normalSize = normalSize;
	}

	void ZZHOUMJ_NorthHandCard::setTingCardSize(Size tingSize)
	{
		_tingSize = tingSize;
	}

	
	cocos2d::Vec2 ZZHOUMJ_NorthHandCard::getCatchWorldPos()
	{
		return  getNextPosition(_prePoint,_normalSize.width);
	}

	void ZZHOUMJ_NorthHandCard::putChangeCardToHead(std::vector<INT> cards)
	{
		for (auto v:_changeCardsInHead)
		{
			v->removeFromParent();
		}
		_changeCardsInHead.clear();

		float startX = 580;
		int index = 0;
		std::vector<Point> v_pos_temp;
		for (auto v:cards)
		{
			auto pCard = MahjongCard::create(DI_NORTH_BACK,NORTH_DIR);
			pCard->setPosition(Vec2(startX+index*35,680));
			pCard->setAnchorPoint(Vec2(0.5f, 0.5f));
			this->addChild(pCard);
			_changeCardsInHead.push_back(pCard);
			MoveBy* moveBy = MoveBy::create(0.5f,Vec2(0,-80));
			pCard->runAction(moveBy);
			v_pos_temp.push_back(pCard->getPosition()+Vec2(0, -80));
			
			index++;
		}
		GameManager::getInstance()->addChangeCardPos(sitDir::NORTH_DIR,v_pos_temp);
	}



	void ZZHOUMJ_NorthHandCard::moveChangeCard(sitDir dir)
	{
		int i = 0;
		for (auto v:_changeCardsInHead)
		{
			switch (dir)
			{
			case ZZHOUMJ::SOUTH_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::STRAIGHT, bezDir::BEZ_MID_DIR, sitDir::SOUTH_DIR, i);
				break;
			case ZZHOUMJ::EAST_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CW, bezDir::EAST_NORTH_DIR, sitDir::EAST_DIR, i);
				break;
			case ZZHOUMJ::WEST_DIR:
				GameManager::getInstance()->runChangeCardAction(v, changeActType::CCW, bezDir::WEST_NORTH_DIR, sitDir::WEST_DIR, i);
				break;
			default:
				break;
			}
			i++;
		}
		_changeCardsInHead.clear();
	}

	cocos2d::Vec2 ZZHOUMJ_NorthHandCard::getNextPosition(Vec2 prePoint, float deltLen)
	{
		cocos2d::Vec2 nodeDir = endDirPos-beginDirPos;
		nodeDir.normalize();
		nodeDir.scale(deltLen);
		return prePoint+nodeDir;
	}

}
