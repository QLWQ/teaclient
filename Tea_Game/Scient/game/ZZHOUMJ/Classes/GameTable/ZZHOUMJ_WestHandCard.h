#ifndef ZZHOUMJ_WestHandCard_h__
#define ZZHOUMJ_WestHandCard_h__

#include "cocos2d.h"
#include "ZZHOUMJ_CardPool.h"
#include "ZZHOUMJ_HandCard.h"
using namespace cocos2d;

/*
�������°�
*/

namespace ZZHOUMJ
{
	class ZZHOUMJ_WestHandCard : public HandCard
	{
	public:
		ZZHOUMJ_WestHandCard();
		~ZZHOUMJ_WestHandCard();

		CREATE_FUNC(ZZHOUMJ_WestHandCard);

		virtual Vec2 refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>normalCards,INT catchCard)override;
		virtual Vec2 refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>tingCards)override;
		virtual Vec2 refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,INT huCardId,bool visibleAllCard)override;
		virtual void setSingleGroupSize(Size groupSize)override;
		virtual void setNormalCardSize(Size normalSize)override;
		virtual void setTingCardSize(Size tingSize)override;
		virtual	Vec2 getCatchWorldPos()override;
		virtual void putChangeCardToHead(std::vector<INT> cards)override;
		virtual void moveChangeCard(sitDir dir)override;

		void resetData();

	private:
		virtual bool init() override;
		void refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards);
		Vec2 getNextPosition(Vec2 prePoint, float deltLen);

	private:
		int		_beginZorder;
		Vec2	_prePoint;
	
	};
}



#endif // HSMJ_WestHandCard_h__
