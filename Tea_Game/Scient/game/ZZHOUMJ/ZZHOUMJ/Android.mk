LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNNet)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNUI)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNMarket)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLobby)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLogic)

LOCAL_MODULE := ZZHOUMJ_static

LOCAL_MODULE_FILENAME := libZZHOUMJ

LOCAL_SRC_FILES := ../Classes/GameTable/ZZHOUMJ_GameTableLogic.cpp \
                   ../Classes/GameTable/ZZHOUMJ_GameManager.cpp \
                   ../Classes/GameTable/ZZHOUMJ_ResourceLoader.cpp \
				   ../Classes/GameTable/ZZHOUMJ_CardPool.cpp \
				   ../Classes/GameTable/ZZHOUMJ_GameTableUI.cpp \
				   ../Classes/GameTable/ZZHOUMJ_PoolAction.cpp \
				   ../Classes/GameTable/ZZHOUMJ_HuCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_Card.cpp \
				   ../Classes/GameTable/ZZHOUMJ_SouthMahjongCardPool.cpp \
				   ../Classes/GameTable/ZZHOUMJ_EastMahjongCardPool.cpp \
				   ../Classes/GameTable/ZZHOUMJ_WestHandCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_SouthHandCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_EastHandCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_WestMahjongCardPool.cpp \
				   ../Classes/GameTable/ZZHOUMJ_MahjongCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_NorthMahjongCardPool.cpp \
				   ../Classes/GameTable/ZZHOUMJ_NorthHandCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_ConcealedKong.cpp \
				   ../Classes/GameTable/ZZHOUMJ_Factory.cpp \
				   ../Classes/GameTable/ZZHOUMJ_MahjongCardPool.cpp \
				   ../Classes/GameTable/ZZHOUMJ_MeldedKong.cpp \
				   ../Classes/GameTable/ZZHOUMJ_TouchCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_TouchKong.cpp \
				   ../Classes/GameTable/ZZHOUMJ_ChatTip.cpp \
				   ../Classes/GameTable/ZZHOUMJ_Result.cpp \
				   ../Classes/GameTable/ZZHOUMJ_TurnTable.cpp \
				   ../Classes/GameTable/ZZHOUMJ_GMLayer.cpp \
				   ../Classes/GameTable/ZZHOUMJ_HandCard.cpp \
				   ../Classes/GameTable/ZZHOUMJ_ActionButtonList.cpp \
				   ../Classes/GameTable/ZZHOUMJ_SetLayer.cpp \
				   ../Classes/GameTable/ZZHOUMJ_EatCard.cpp \

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMessage \
                    $(LOCAL_PATH)/../Classes/GameTable \
                    $(LOCAL_PATH)/../../ \
                    $(LOCAL_PATH)/../ 

LOCAL_EXPORT_LDLIBS := -llog \
                       -lz \
                       -landroid     
                   
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_net_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_market_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_lobby_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_logic_static

LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static


include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)
$(call import-module,editor-support/cocostudio)
$(call import-module,extensions)

$(call import-module,LibHNUI)
$(call import-module,LibHNNet)
$(call import-module,LibHNMarket)
$(call import-module,LibHNLogic)
$(call import-module,LibHNLobby)