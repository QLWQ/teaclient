/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_MessageHead_H__
#define __CaiShenDDZ_MessageHead_H__

#include "HNNetExport.h"

namespace CaiShenDDZ
{

	const std::string Player_Normal_M = "platform/common/boy.png";
	const std::string Player_Normal_W = "platform/common/girl.png";

	enum Button_Type
	{
		BUTTON_CALL_LORD	= 0x01,// 叫地主
		BUTTON_ROB_LORD		= 0x02,// 抢地主
		BUTTON_DOUBLE		= 0x04,// 加倍
		BUTTON_SHOW_CARD	= 0x08,// 明牌
		BUTTON_READY		= 0x10,// 准备
		BUTTON_OUTCARD		= 0x20,// 出牌
		BUTTON_HIDE			= 0x40,
		BUTTON_CALL_SCORE	= 0x80,// 叫分a
	};

	enum Hint_Type
	{
		STATE_READY,		// 准备
		STATE_CALL_LORD,	// 叫地主
		STATE_CALL_SCORE1,	// 叫1分a
		STATE_CALL_SCORE2,	// 叫2分a
		STATE_CALL_SCORE3,	// 叫3分a
		STATE_NO_CALL_LORD,	// 不叫
		STATE_ROB_LORD,		// 抢地主
		STATE_NO_ROB_LORD,	// 不抢
		STATE_DOUBLE,		// 加倍
		STATE_NO_DOUBLE,	// 不加倍
		STATE_SHOW_CARD,	// 明牌
		STATE_NO_OUTCARD,	// 不出

		STATE_HIDE,
	};

	enum Tips_Type
	{
		TIPS_INVALIDOUT,	// 错误出牌牌型
		TIPS_CANNOTOUT,		// 没有比上家大的牌
		TIPS_HIDE,
	};

	enum EBackArrayType
	{
		BACK_ARRAY_ERROR = 0,
		BACK_ARRAY_ONEKING = 1,	//单王
		BACK_ARRAY_SHUNZI = 2,	//顺子
		BACK_ARRAY_TONGHUA = 3,	//同花
		BACK_ARRAY_TWOKING = 4,	//双王
		BACK_ARRAY_SANZHANG = 5,//三张

	};

	enum GAME_RULE
	{
		JINGDIAN = 1,					//经典玩法
		JINGDIAN_NO_SHUFFLE = 2,		//经典玩法,不洗牌
		JINGDIAN_LAIZI = 3,				//经典癞子斗地主
		JINGDIAN_LAIZI_NOSHUFFLE = 4,	//经典癞子斗地主，不洗牌
		TIANDI_LAIZI = 5,				//天地癞子斗地主
		TIANDI_LAIZI_NOSHUFFLE	= 6,	//天地癞子斗地主 不洗牌
	};

	enum DouDiZhu_COMMAND
	{
		GAME_NAME_ID = 10100003, // 名字 ID
        PLAY_COUNT	         =	3,        //游戏人数
        
        //游戏状态定义
        GS_WAIT_SETGAME		 = 0,		  //等待东家设置状态
		GS_WAIT_ARGEE		 = 1,		  //等待同意设置
		GS_CALL_SCORE		 = 21,		  //叫分
		GS_ROB_NT			 = 22,		  //抢地主
		GS_ADD_DOUBLE        = 23,		  //加倍
		GS_SHOW_CARD         = 24,		  //明牌
        GS_PLAY_GAME		 = 25,		  //游戏中状态
        GS_WAIT_NEXT		 = 26,		  //等待下一盘开始 
	};

	enum EArrayType
	{
		ARRAY_Type_ERROR = 0,
		ARRAY_Type_SINGLE,			//单张
		ARRAY_Type_DOUBLE ,			//对子
		ARRAY_Type_3W_,				//3不带
		ARRAY_Type_3W1_ONE,			//3带单张
		ARRAY_Type_3W1_DOUBLE,		//3带对子
		ARRAY_Type_PLANE_,			//飞机不带翅膀
		ARRAY_Type_PLANE_ONE,		//飞机带单张
		ARRAY_Type_PLANE_DOUBLE,    //飞机带对子
		ARRAY_Type_STRAIGHT_ONE,    //单顺子
		ARRAY_Type_STRAIGHT_DOUBLE, //连对
		ARRAY_Type_4W2_ONE,			//4带2张单张
		ARRAY_Type_4W2_DOUBLE,		//4带2张对子
		ARRAY_Type_SBOMB,			//软炸
		ARRAY_Type_HBOMB,			//硬炸
		ARRAY_Type_WBOMB,			//王炸
		ARRAY_Type_4L,				//4癞子
		ARRAY_Type_LBOMB_S,		//5张以上炸弹 软炸
		ARRAY_Type_LBOMB_L,			//5张以上炸弹 纯癞子炸
	};

	const  INT  BACK_CARD_COUNT = 3;				//底牌张数
	const  INT  ONE_HAND_CARD_COUNT = 20;			//手牌张数
	const  INT  CARD_PAIRS      = 1;				//几副牌
	const  INT  MAX_CARD_COUNT = 54 * CARD_PAIRS;   //一副牌张数

	#pragma pack(1)
	////////////////游戏数据包/////////////////////////////////////
	//明牌准备消息（C->S）   ASS_GM_AGREE_GAME				1
	struct  T_C2S_MINGPAI_RES
	{
		bool    bStart;        //是否为开始
		bool    bMing;         //玩家是否明牌
		BYTE    bDeskStaion;   //玩家的位置
		T_C2S_MINGPAI_RES()
		{
			memset(this, 0, sizeof(T_C2S_MINGPAI_RES));
		}
	};

	//游戏开始  ASS_GAME_BEGIN
	struct	T_S2C_GAMEBEGIN_NOTIFY
	{
		bool    bIsMingPai[PLAY_COUNT];     //玩家是否明牌

		T_S2C_GAMEBEGIN_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_GAMEBEGIN_NOTIFY));
		}
	};

	//发牌   ASS_SEND_ALL_CARD
	struct	T_S2C_SENDALLCARD_NOTIFY
	{
		UINT	iUserCardCount[PLAY_COUNT];					 //用户手上扑克数目
		BYTE	iUserCard[PLAY_COUNT][ONE_HAND_CARD_COUNT];	 //用户手上的扑克

		T_S2C_SENDALLCARD_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_SENDALLCARD_NOTIFY));
		}
	};

	//叫分通知  ASS_CALL_SCORE
	struct T_S2C_CALLSCORE_NOTIFY
	{
		BYTE	 bDeskStation;		//当前叫分者
		UINT     iMinCallScore;     //最小叫分
		T_S2C_CALLSCORE_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_CALLSCORE_NOTIFY));
		}
	};

	//叫分请求和回复  ASS_CALL_SCORE_RESULT
	struct T_S2C_CALLSCORE_RES
	{
		BYTE	 bDeskStation;		//当前叫分者
		INT		 iValue;			//叫分值 0:不叫，1-3分
		T_S2C_CALLSCORE_RES()
		{
			memset(this, 0, sizeof(T_S2C_CALLSCORE_RES));
		}
	};
	typedef T_S2C_CALLSCORE_RES  T_C2S_CALLSCORE_REQ;

	//通知抢地主  ASS_ROB_NT
	struct T_S2C_ROBNT_NOTIFY
	{
		BYTE bSeatNo;
		UINT iValue;       //抢地主情况（1-叫地主， 2-抢地主）
		T_S2C_ROBNT_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_ROBNT_NOTIFY));
		}
	};

	//抢地主请求和回复  ASS_ROB_NT_RESULT
	struct T_S2C_ROBNT_RES
	{
		BYTE bSeatNo;
		UINT iValue;       //抢地主情况（0-不叫，1-叫地主，2-不抢，3-抢地主）
		T_S2C_ROBNT_RES()
		{
			memset(this, 0, sizeof(T_S2C_ROBNT_RES));
		}
	};
	typedef T_S2C_ROBNT_RES  T_C2S_ROBNT_REQ;

	//癞子牌通知  ASS_LAIZI_CARD_NOTIFY
	struct	T_S2C_LAIZICARD_NOTIFY
	{
		bool   bIsTianLai;									//是否是天癞子
		BYTE   bLaiZi;										//癞子牌
		BYTE   byUserCards[PLAY_COUNT][ONE_HAND_CARD_COUNT];//玩家手牌
		UINT   iCardCount[PLAY_COUNT];						//手牌数量
		T_S2C_LAIZICARD_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_LAIZICARD_NOTIFY));
		}
	};

	//定庄消息  ASS_DINGZHUANG
	struct	T_S2C_DINGZHUANG_NOTIFY
	{
		BYTE   bNtSeatNo;									//庄家座位号
		UINT   iCallScoreNumber;							//底分
		BYTE   byBackCard[BACK_CARD_COUNT];					//底牌
		BYTE   byUserCards[PLAY_COUNT][ONE_HAND_CARD_COUNT];//玩家手牌
		UINT   iCardCount[PLAY_COUNT];						//手牌数量
		T_S2C_DINGZHUANG_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_DINGZHUANG_NOTIFY));
		}
	};

	//加倍通知  ASS_ADD_DOUBLE
	struct T_S2C_ADDDOUBLE_NOTIFY
	{
		BYTE bDeskStation;			//加倍位置
		T_S2C_ADDDOUBLE_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_ADDDOUBLE_NOTIFY));
		}
	};

	//加倍请求和回复  ASS_ADD_DOUBLE_RESULT
	struct T_S2C_AddDouble_RES
	{
		BYTE bDeskStation;			//加倍位置
		INT iValue;					//加倍数 0：不加倍 1:加倍
		T_S2C_AddDouble_RES()
		{
			memset(this, 0, sizeof(T_S2C_AddDouble_RES));
		}
	};
	typedef T_S2C_AddDouble_RES  T_C2S_AddDouble_RES;

	//通知明牌  ASS_SHOW_CARD
	struct T_S2C_SHOWCARD_NOTIFY
	{
		BYTE bSeatNo;
		T_S2C_SHOWCARD_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_SHOWCARD_NOTIFY));
		}
	};

	//明牌请求  ASS_SHOW_CARD_REQ
	struct T_C2S_SHOWCARD_REQ
	{
		BYTE bSeatNo;    //明牌座位号
		INT  iRate;      //明牌倍数
		T_C2S_SHOWCARD_REQ()
		{
			memset(this, 0, sizeof(T_C2S_SHOWCARD_REQ));
		}
	};

	//明牌回复  ASS_SHOW_CARD_RESULT
	struct T_S2C_SHOWCARD_RES
	{
		BYTE bSeatNo;							//明牌座位号
		BYTE iCardList[ONE_HAND_CARD_COUNT];	//扑克信息
		UINT iCardCount;			            //扑克数目
		INT  iRate;								//明牌倍数
		T_S2C_SHOWCARD_RES()
		{
			memset(this, 0, sizeof(T_S2C_SHOWCARD_RES));
		}
	};

	//新一轮出牌消息  ASS_NEW_TURN
	struct T_S2C_NEWTURN_NOTIFY
	{
		BYTE				iOutDeskStation;				//出牌的位置
		T_S2C_NEWTURN_NOTIFY()
		{
			iOutDeskStation = INVALID_DESKNO;
		}
	};

	//出牌请求  ASS_OUT_CARD
	struct T_C2S_PLAY_CARD_REQ
	{
		EArrayType  eArrayType;								//牌类型
		BYTE		bCardList[ONE_HAND_CARD_COUNT];			//目标牌型数据，癞子牌转成的牌
		BYTE		bActualCardList[ONE_HAND_CARD_COUNT];	//实际手牌，有癞子牌
		INT			iCardCount;								//扑克数目

		void clear()
		{
			memset(this, 0, sizeof(T_C2S_PLAY_CARD_REQ));
		}

		T_C2S_PLAY_CARD_REQ()
		{
			clear();
		}
	};

	//出牌结果  ASS_OUT_CARD_RESULT
	struct T_S2C_PLAY_CARD_RES
	{
		BYTE				iNextDeskStation;				//下一出牌者
		bool				bCanOutCard;					//下一个玩家操作，要不起false
		BYTE				bDeskStation;					//当前出牌者	
		BYTE				bHandCard[ONE_HAND_CARD_COUNT];	//出牌后的手牌
		UINT				bHandCardCount;                 //牌数量
		T_C2S_PLAY_CARD_REQ tPlayCards;						//出牌数据
		T_S2C_PLAY_CARD_RES()
		{
			memset(this, 0, sizeof(T_S2C_PLAY_CARD_RES));
		}
	};

	//癞子组合选择通知  ASS_LAIZI_SELECT
	struct T_S2C_SELECT_RESULT_RES
	{
		T_C2S_PLAY_CARD_REQ sCard[ONE_HAND_CARD_COUNT];   //癞子组合成多种牌型
		UINT  iCardCount;                                 //组合牌个数
		T_S2C_SELECT_RESULT_RES()
		{
			memset(this, 0, sizeof(T_S2C_SELECT_RESULT_RES));
		}
	};

	//过牌请求  ASS_PASS
	struct T_C2S_PASS_REQ
	{
		bool bPass;
		T_C2S_PASS_REQ()
		{
			memset(this, 0, sizeof(T_C2S_PASS_REQ));
		}
	};

	//过牌结果  ASS_PASS_RESULT
	struct T_S2C_PASS_RES
	{
		BYTE bSeatNo;
		BYTE bNextDeskStation;   //下一个出牌者
		bool bCanOutCard;		//下一个玩家操作，要不起false
		bool bNewTurn;			//是否是新一轮
		T_S2C_PASS_RES()
		{
			memset(this, 0, sizeof(T_S2C_PASS_RES));
		}
	};

	//提示请求  ASS_PROMPT
	struct T_C2S_PROMPT_CARD_REQ
	{
		BYTE bSeatNo;
		BYTE bPromptType;//0:按牌型排序，  1：按打出后，剩余的手牌和牌值排序
		T_C2S_PROMPT_CARD_REQ()
		{
			memset(this, INVALID_DESKNO, sizeof(T_C2S_PROMPT_CARD_REQ));
		}
	};

	//提示结果  ASS_PROMPT_RESULT
	struct T_S2C_PROMPT_CARD_RES
	{
		T_C2S_PLAY_CARD_REQ sCards[ONE_HAND_CARD_COUNT]; //牌数据
		UINT iCardCount;								 //组合牌个数

		void clear()
		{
			memset(this, 0, sizeof(T_S2C_PROMPT_CARD_RES));
		}

		T_S2C_PROMPT_CARD_RES()
		{
			clear();
		}
	};

	//小结算  ASS_CONTINUE_END
	struct T_S2C_RESULT_NOTIFY
	{
		bool        bIsSpring;                                   //是否春天
		BYTE		iUpGradeStation;							 //庄家位置
		UINT		iUserCardCount[PLAY_COUNT];					 //用户手上扑克数目
		BYTE		iUserCard[PLAY_COUNT][ONE_HAND_CARD_COUNT];	 //用户手上的扑克
		LLONG  		iMoney[PLAY_COUNT];			                 //输赢得分
		int			initialMultiple;								//初始倍数
		int			mingMultiple;									//明牌倍数
		int			robMultiple;									//抢地主倍数
		int			cardMultiple;									//底牌倍数
		int			bombMultiple;									//炸弹倍数
		int			springMultiple;									//春天倍数
		int			publicMultiple;									//公共倍数
		int			landlordMultiple;								//地主加倍
		int			peasantryMultiple[PLAY_COUNT];					//农民加倍
		int			totalMultiple[PLAY_COUNT];						//总加倍
		T_S2C_RESULT_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_RESULT_NOTIFY));
		}
	};

	//大结算  ASS_CALCULATE_BOARD
	struct T_S2C_TOTAL_RESULT
	{
		bool	bWinner[PLAY_COUNT];			//大赢家
		int		iBombCount[PLAY_COUNT];			//炸弹次数
		LLONG	iMaxWinMoney[PLAY_COUNT];		//最多赢钱数目
		int		iMaxContinueCount[PLAY_COUNT];	//最高连胜
		int		iWinCount[PLAY_COUNT];			//胜局
		LLONG	i64WinMoney[PLAY_COUNT];		//输赢金币
		T_S2C_TOTAL_RESULT()
		{
			memset(this, 0, sizeof(T_S2C_TOTAL_RESULT));
		}
	};

	//托管  ASS_AUTO
	struct T_S2C_TRUSTEE_RES
	{
		BYTE bDeskStation;
		bool bAuto;
		T_S2C_TRUSTEE_RES()
		{
			memset(this, 0, sizeof(T_S2C_TRUSTEE_RES));
		}
	};

	//倍数更新通知  ASS_RATEUPDATE
	struct T_S2C_RATEUPDATE_NOTIFY
	{
		UINT iUserRate[PLAY_COUNT]; //所有玩家的倍数
		T_S2C_RATEUPDATE_NOTIFY()
		{
			memset(this, 0, sizeof(T_S2C_RATEUPDATE_NOTIFY));
		}
	};

	///游戏状态（基础包）
	struct GameStation_Base
	{
		BYTE				bGameStation;                   //当前的游戏状态
		BYTE				iBegINTime;						//开始准备时间
		BYTE				iThinkTime;						//出牌思考时间
		BYTE                iRobNTTime;						//抢地主时间
		BYTE				iCallScoreTime;					//叫分计时
		BYTE				iAddDoubleTime;					//加倍时间
		BYTE				iMingPaiTime;					//明牌时间
		INT				 iBasePoint;						//底分
	    INT               iBaseMul;                       //倍数
		INT               iGameRule;                      //游戏玩法 参考GAME_RULE
		LLONG		     iuserMoney[PLAY_COUNT];
		GameStation_Base()
		{
			memset(this, 0, sizeof(GameStation_Base));
		}
	};

	//游戏状态（ 等待阶段 ）
	struct GameStation_2 : public GameStation_Base
	{
		bool                bUserReady[PLAY_COUNT];         //玩家是否已准备
		GameStation_2()
		{
			memset(this, 0, sizeof(GameStation_2));
		}
	};

	//游戏状态（等待定庄阶段）
	struct GameStation_3 : public GameStation_Base
	{
		BYTE				iUpGradePeople;					//庄家位置
		BYTE                iCurOperator;					//当前操作的人
		bool				bAuto[PLAY_COUNT];				//托管情况 
		BYTE				bCallScore[PLAY_COUNT];			//几家叫分情况(255,未操作，0：不抢，1~3：抢)
		BYTE 				bRobNT[PLAY_COUNT];				//抢地主情况(255,未操作，0：不叫，1：叫地主，2：不抢，3：抢地主)
		BYTE                bUserDoubleValue[PLAY_COUNT];	//玩家加倍情况(255,未操作，0：不加，1：加)
		UINT			    iUserCardCount[PLAY_COUNT];		//用户手上扑克数目
		BYTE			    bUserCard[PLAY_COUNT][ONE_HAND_CARD_COUNT];	 //用户手上的扑克
		BYTE                bGameBackCard[BACK_CARD_COUNT];	//底牌
		UINT 				iBackCardCount;					//底牌数量
		bool				bIsMingPai[PLAY_COUNT];         //玩家是否明牌
		BYTE				bTianLaiZi;						//天癞子牌
		BYTE				bDiLaiZi;						//地癞子牌
		GameStation_3()
		{
			memset(this, 0, sizeof(GameStation_3));
		}
	};

	//游戏状态（ 游戏中状态 ）
	struct GameStation_4 : public GameStation_Base
	{
		BYTE				iUpGradePeople;						//庄家位置
		BYTE				iLastOutCardPeople;					//上一个出牌用户
		BYTE				iOutCardPeople;						//现在出牌用户
		bool				bCanOutCard;						//当前操作玩家要不起false
		bool				bAuto[PLAY_COUNT];					//托管情况
		UINT			    iUserCardCount[PLAY_COUNT];			//用户手上扑克数目
		BYTE			    iUserCard[PLAY_COUNT][ONE_HAND_CARD_COUNT];	 //用户手上的扑克
		T_C2S_PLAY_CARD_REQ iDeskCardList[PLAY_COUNT];//上一次出牌的扑克
		bool                bPass[PLAY_COUNT];					//不出
		BYTE                iGameBackCard[BACK_CARD_COUNT];		//底牌
		UINT 				iBackCardCount;						//底牌数量
		bool                bIsMingPai[PLAY_COUNT];             //玩家是否明牌
		BYTE				bTianLaiZi;						    //天癞子牌
		BYTE				bDiLaiZi;						    //地癞子牌
		GameStation_4()
		{
			memset(this, 0, sizeof(GameStation_4));
		}
	};

	// 游戏流程结构
	struct GameTableInfo
	{
		BYTE				iGameStatus;						//游戏状态
		BYTE				iBeginTime;							//开始准备时间
		BYTE				iThinkTime;							//出牌思考时间
		BYTE				iCallScoreTime;						//叫分时间
		BYTE                iRobNTTime;							//抢地主时间
		BYTE				iAddDoubleTime;						//加倍时间
		BYTE				iMingPaiTime;						//明牌时间
		BYTE				iLordSeatNo;						//庄家位置
		BYTE				iOutCardSeatNo;						//当前出牌位置
		bool                bUserReady[PLAY_COUNT];				//玩家准备
		bool				bUserAuto[PLAY_COUNT];				//玩家托管
		T_C2S_PLAY_CARD_REQ	iLastOutCardList;//最后出的牌
		BYTE				iLastOutCardCount;					//最后出牌的张数
		BYTE			    iUserCard[PLAY_COUNT][ONE_HAND_CARD_COUNT];		        //玩家手牌
		INT					iUserCardCount[PLAY_COUNT];			//玩家牌张数
		BYTE                bTianLaiziCard;						//天癞子牌值 a
		BYTE				bDiLaiziCard;						//地癞子值
		bool				bIsMingPai[PLAY_COUNT];				//玩家是否明牌
		T_S2C_PROMPT_CARD_RES promptCards;                      //提示牌型
		INT                 promptIndex;                        //提示牌型的下标
		INT                iBaseMul;                       //倍数

		GameTableInfo()
		{
			Clear();
		}

		void Clear()
		{
			memset(this, 0, sizeof(GameTableInfo));
			iLordSeatNo = INVALID_DESKSTATION;
			iOutCardSeatNo = INVALID_DESKSTATION;
		}
	};
	//战绩信息
	struct S_C_GameRecordResult
	{
		int JuCount[8][10];	//每局的玩家输赢分数
		int GameCount;

		S_C_GameRecordResult()
		{
			memset(this, 0, sizeof(S_C_GameRecordResult));
		}
	};
#pragma pack()

	// 数据包处理辅助标识
	enum Net_Cmd
	{
		ASS_GAME_BEGIN				=	51,				//游戏开始
		ASS_SEND_ALL_CARD			=	52,				//发牌消息
		ASS_CALL_SCORE				=	53,				//通知叫分
		ASS_CALL_SCORE_RESULT		=	54,				//叫分结果/请求叫分
		ASS_ROB_NT					=	55,				//通知抢地主
		ASS_ROB_NT_RESULT			=   56,				//抢地主结果/请求抢地主
		ASS_DINGZHUANG				=   57,				//定庄消息
		ASS_ADD_DOUBLE				=	58,				//通知加倍
		ASS_ADD_DOUBLE_RESULT		=	59,				//加倍结果/请求加倍
		ASS_SHOW_CARD				=   60,				//通知明牌
		ASS_SHOW_CARD_REQ		    =   61,				//明牌请求
		ASS_SHOW_CARD_RESULT		=	62,				//明牌结果
		ASS_LAIZI_CARD_NOTIFY		=	63,				//癞子牌通知
		ASS_NEW_TURN				=   64,				//开始出牌
		ASS_OUT_CARD				=	65,				//出牌请求
		ASS_OUT_CARD_RESULT			=   66,				//出牌结果
		ASS_LAIZI_SELECT            =   67,				//癞子组合通知
		ASS_PASS					=	68,				//过牌请求
		ASS_PASS_RESULT			    =   69,				//过牌结果
		ASS_PROMPT					=   70,				//提示请求
		ASS_PROMPT_RESULT			=   71,				//提示结果
		ASS_ERROR_CARDTYPE			=   72,				//错误牌型通知（空消息）
		ASS_CONTINUE_END			=	73,				//小结算
		ASS_CALCULATE_BOARD		    =   74,				//大结算
		ASS_AUTO					=   75,				//托管
		ASS_RATEUPDATE				=   76,				//更新倍数
		ASS_ALL_CARD_INFO			=	77,

		S_C_GAME_RECORD				=	161,			//战绩请求消息
		S_C_GAME_RECORD_RESULT		=	162,			//返回战绩
	};
}
#endif