/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_Game_Table_UI_H__
#define __CaiShenDDZ_Game_Table_UI_H__

#include "CaiShenDDZGameTableUICallback.h"
#include "CaiShenDDZGamePlayer.h"
#include "CaiShenDDZPokerCard.h"
#include "CaiShenDDZCalculateBoardSingle.h"
#include "HNLobbyExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include <vector>

USING_NS_CC_EXT;

namespace CaiShenDDZ
{
	class GameTableLogic;
	class CardOutList;
	class CardListBoard;

	class GameTableUI : public HN::HNGameUIBase, public GameTableUICallback
	{
	public:
		static HNGameUIBase* create(BYTE bDeskIndex, bool bAutoCreate);
		virtual bool init(BYTE bDeskIndex, bool bAutoCreate);
		virtual void onExit();
		virtual void onEnterTransitionDidFinish() override;

		// 逻辑接口虚方法
	public:
		virtual void addUser(BYTE seatNo) override;
		virtual void removeUser(BYTE seatNo) override;
		virtual void setUserName(BYTE seatNo,const std::string& name) override;					//显示玩家昵称
		virtual void setUserMoney(BYTE seatNo, LLONG money);									//显示玩家金币
		virtual void setUserScore(LLONG userScore[PLAY_COUNT]);
		
		virtual void setUserAuto(BYTE seatNo, bool isAuto) override;							//显示玩家托管
		virtual void setUserHead(BYTE seatNo) override;											//显示玩家头像
		virtual void showUserHandCard(BYTE seatNo, const std::vector<BYTE>& values, BYTE tianLaiziCard, BYTE diLaiziCard, bool isUp, bool isOnce, bool isFront) override;//显示玩家手牌
		virtual void showUserHandCardCount(BYTE seatNo, INT count) override;					//显示手牌张数
		virtual void showUserHandCardOut(BYTE seatNo, const std::vector<BYTE> &Actualcards, const std::vector<BYTE> &cards) override;	//显示出牌
		virtual void showUserOutCardType(BYTE logicSeatNo, BYTE cardValue, BYTE type) override;						//显示出牌类型

		virtual void downUserCards(BYTE seatNo) override;										//收起弹出的牌
		virtual void setUpCardList(BYTE seatNo, const std::vector<BYTE>& upCards) override;		//设置对应的手牌弹出
		virtual void getUpCardList(BYTE seatNo, std::vector<BYTE>* upCards) override;			//获取弹出的手牌

		virtual void showLandlordCard(const std::vector<BYTE>& cards) override;					//显示三张地主牌
		virtual void showBackCardTypeAndMutiple(bool isShow, BYTE cardType = 255, BYTE cardMutiple = 255) override;//显示地主牌类型奖励
		virtual void showLandlord(BYTE seatNo, bool visible) override;							//显示地主标志
		virtual void showNotice(const std::string &message);									//显示公告信息
		virtual void showVipEnterTip(BYTE seatNo);                               //显示VIP进入提示         
		virtual void setBasePoint(int point) override;											//显示底分
		virtual void setMultiple(int multiple, bool isAll = false) override;					//显示倍数

		virtual void showActionButton(BYTE flag, bool firstOut = false,bool needShowTimer = true) override;				//显示操作按钮
		virtual void showUserOperationHints(BYTE seatNo, Hint_Type type = STATE_HIDE) override;	//显示玩家操作提示
		virtual void setAutoAgree(int delayTime) override;										//倒计时自动准备
		virtual void showTimer(BYTE seatNo, int delayTime, bool visible) override;				//显示玩家定时器
		virtual void showGameTips(Tips_Type type) override;										//游戏提示
		virtual void showMingPaiMulImg(int mul) override;										//明牌倍数
		virtual void setAutoBtnVisible(bool visible) override;									//托管按钮是否显示

		virtual void showSingleCalculateBoard(T_S2C_RESULT_NOTIFY endData) override;			//小结算
		virtual void showCalculateBoard(T_S2C_TOTAL_RESULT boardData) override;			//大结算

		virtual void onChatTextMsg(BYTE seatNo, std::string msg);								//处理文本聊天消息
		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime);					//处理语音聊天消息

		virtual void onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index)override;			//处理玩家之间的动作特效

		virtual void afterScoreByDeskStation(int Station, int money) override;		//发特效动作扣金币

		//设置癞子牌是否显示
		virtual void setLaiziCard(bool isShow, BYTE cardValue,bool bTianLaizi = true) override;

		//显示癞子组合牌型选择界面
		virtual	void showLaiziCardType(T_S2C_SELECT_RESULT_RES data) override;
		virtual	void showLaiziCardTypeLayer(bool isShow) override;


		//房间说明
		virtual void showGameRule(int palyMode)override;

		//请求红包任务状态
		virtual void CheckActivity()override;

		//请求红包奖励
		virtual void CheckReceive()override;

		//更新游戏的进度
		virtual void	setActivityNum(int Current, int toal, int CanTake)override;
		//更新游戏红包中奖
		virtual void setReceiveState(int ReceiveType, int AwardNum)override;

		virtual void UpDateUserLocation(int index) override;			//更新经纬度地理位置

		//赠送礼物回复
	//	virtual void dealGift(TMSG_SENG_GIFT_REQ data)override;
		void sendGift(BYTE senderSeatNo, BYTE receiverSeatNo, int giftNo);  //赠送礼物

		virtual void clearDesk();
		virtual void leaveDesk(bool isForceQuit);

		virtual void setRecordEnable(bool visible);//记牌器按钮可用
		virtual void setCardCountResult(BYTE CardList[PLAY_COUNT][54]);
		virtual void setLordCard(bool visible);
		virtual void setRecordVis(bool visible);
		virtual void setCardCountVis(BYTE seatNo ,bool visible);//明牌是否显示牌数

		//发送战绩数据
		virtual void sendRecordData(S_C_GameRecordResult* data);

	private:
		void autoOutCallFunction(cocos2d::Node* pNode);

		void menuClickCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);

		void updateUserMoney(BYTE seatNo, int money);

		//弹窗用户信息
		void getUserInfo(Ref* pSender, Widget::TouchEventType type); //玩家头像回调

	protected:
		void loadLayout(BYTE bDeskIndex);
		bool isValidSeat(BYTE seatNo);
		void initParams();

		void addOutCardList(BYTE seatNo);
		void addHandCardList(BYTE seatNo);

		void setDeskInfo();

		// 聊天界面
		void gameChatLayer();
		void removeSetLayer();
		void chatBtnClickCallBack(Ref* ref, Widget::TouchEventType type);

		// 显示房主标志
		void showMaster(BYTE seatNo, bool bShow) override;

		// 显示离线标志
		virtual void showUserCut(BYTE seatNo, bool bShow) override;

		// 回放载入完毕
		void gameRecordReady();

		// 显示手机状态信息
		void showPhoneState();

		//创建癞子牌组合子项
		Button* getLaiziTypeItem(T_C2S_PLAY_CARD_REQ data);

		//开启明牌倒计时
		void startTimer(TextAtlas* txtTimer, int timer);
		//关闭明牌倒计时
		void stopTimer(TextAtlas* txtTimer);

		
	protected:
		GameTableUI();

		virtual ~GameTableUI();

	protected:
		GameTableLogic*		_tableLogic = nullptr;
		Layout*				_tableWidget = nullptr;
		GameChatLayer*		_chatLayer = nullptr;
		PlayerUI*			_players[PLAY_COUNT];
		Button*			_TouchPlayer[PLAY_COUNT];
		CardListBoard*		_cardListBoard[PLAY_COUNT];
		CardOutList*		_cardOutList[PLAY_COUNT];
		Text*				_text_time = nullptr;
		bool				_isGameEnd = false;
		BYTE                _tianLaiziCardValue = 0;
		BYTE                _diLaiziCardValue = 0;
		// 初始化计时器标记
		bool _timerflag = false;
		TextAtlas* _showCardTimer = nullptr;
		//玩法说明
		Text*	_text_wanfa = nullptr;

		ImageView* _imgCard[3];
		//记牌器
		ImageView* _imgRecord = nullptr;
		Button* _cRecord = nullptr;
		Button* _btn_showCard = nullptr;
		Button* _btn_showCanel = nullptr;
		Text* _textRecord[15];
		BYTE  _deskNo = 255;
		BYTE					_countResult[15];//存储剩余牌数数据数组
		VipRoomController* roomController = nullptr;
		CalculateBoardSingle* _boarSingle = nullptr;
		bool _allRoundsEnd = false;
		Sprite* _BottonScore;

		string					_vecUserGeographic[PLAY_COUNT];			//地理位置以及距离存储
		float					_Array_Distance[PLAY_COUNT][3];

		INT				_actionPoint[PLAY_COUNT];//动作分
		INT				_ActionMoney[PLAY_COUNT];//临时用户金币
		//用户数据
		UserInfoStruct*					_vecUser[PLAY_COUNT];
		
		LLONG		_scoreArray[PLAY_COUNT];
	};
}


#endif