/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZGameTableLogic.h"
#include "CaiShenDDZGameTableUI.h"
#include "HNNetExport.h"
#include "CaiShenDDZGameAudio.h"

using namespace HN;

namespace CaiShenDDZ
{

#define ENABLE_MING_PAI

/************************line*********************************/
	GameTableLogic::GameTableLogic(GameTableUICallback* uiCallback, BYTE deskNo, bool bAutoCreate)
	: _uiCallback(uiCallback)
	, HNGameLogicBase(deskNo, PLAY_COUNT, bAutoCreate, uiCallback)
{
	initParams();
}

GameTableLogic::~GameTableLogic()
{

}
/************************line*********************************/
void GameTableLogic::dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize)
{
	switch(messageHead->bAssistantID)
	{
	case ASS_GAME_BEGIN:
		HNLOG_INFO("dealGameBeginResp");
		dealGameBeginResp(object, objectSize);
		break;
	case ASS_SEND_ALL_CARD:
		HNLOG_INFO("dealSendAllCardResp");
		dealSendAllCardResp(object, objectSize);
		break;
	case ASS_CALL_SCORE:
		HNLOG_INFO("dealCallScoreResp");
		dealCallScoreResp(object, objectSize);
		break;
	case ASS_CALL_SCORE_RESULT:
		HNLOG_INFO("dealCallScoreResultResp");
		dealCallScoreResultResp(object, objectSize);
		break;
	case ASS_ROB_NT:
		HNLOG_INFO("dealRobNTResp");
		dealRobNTResp(object, objectSize);
		break;
	case ASS_ROB_NT_RESULT:
		HNLOG_INFO("dealRobNTResultResp");
		dealRobNTResultResp(object, objectSize);
		break;
	case ASS_DINGZHUANG:
		HNLOG_INFO("dealDingZhuang");
		dealDingZhuang(object, objectSize);
		break;
	case ASS_LAIZI_CARD_NOTIFY:
		HNLOG("dealLaiZiCardNotity");
		dealLaiZiNotify(object, objectSize);
		break;
	case ASS_ADD_DOUBLE:
		HNLOG_INFO("dealAddDoubleResp");
		dealAddDoubleResp(object, objectSize);
		break;
	case ASS_ADD_DOUBLE_RESULT:
		HNLOG_INFO("dealAddDoubleResultResp");
		dealAddDoubleResultResp(object, objectSize);
		break;
	case ASS_SHOW_CARD:
		HNLOG_INFO("dealShowCardResp");
		dealShowCardResp(object, objectSize);
		break;
	case ASS_SHOW_CARD_RESULT:
		HNLOG_INFO("dealShowCardResultResp");
		dealShowCardResultResp(object, objectSize);
		break;
	case ASS_NEW_TURN:
		HNLOG_INFO("dealNewTurnResp");
		dealNewTurnResp(object, objectSize);
		break;
	case ASS_PROMPT_RESULT:
		HNLOG_INFO("dealPromptResult");
		dealPromptResult(object, objectSize);
		break;
	case ASS_PASS_RESULT:
		HNLOG_INFO("dealPassResult");
		dealPassResult(object, objectSize);
		break;
	case ASS_ERROR_CARDTYPE:
		HNLOG_INFO("dealTypeError");
		dealTypeError(object, objectSize);
		break;
	case ASS_LAIZI_SELECT:
		HNLOG_INFO("dealLaiziSelect");
		dealLaiziSelect(object, objectSize);
		break;
	case ASS_OUT_CARD_RESULT:
		HNLOG_INFO("dealOutCardResultResp");
		dealOutCardResultResp(object, objectSize);
		break;
	case ASS_CONTINUE_END:
		HNLOG_INFO("dealContinueEndResp");
		dealContinueEndResp(object, objectSize);
		break;
	case ASS_CALCULATE_BOARD:
		HNLOG_INFO("dealOnCalculateBoard");
		dealOnCalculateBoard(object, objectSize);
		break;
	case ASS_AUTO:
		HNLOG_INFO("dealAutoResp");
		dealAutoResp(object, objectSize);
		break;
	case ASS_RATEUPDATE:
		HNLOG_INFO("dealUpdateRate");
		dealUpdateRate(object, objectSize);
		break;
	case ASS_ALL_CARD_INFO:
		HNLOG_INFO("dealRecord");
		dealRecord(object, objectSize);
		break;
	case S_C_GAME_RECORD_RESULT:
		HNLOG_INFO("sendRecord");
		{
			CHECK_SOCKET_DATA(S_C_GameRecordResult, sizeof(S_C_GameRecordResult), "S_C_GameRecordResult size error!");
			S_C_GameRecordResult* pSuperUser = (S_C_GameRecordResult*)(object);
			_uiCallback->sendRecordData(pSuperUser);
		}
		break;
	default:
		HNLOG_INFO("unknow game command.");
		break;
	}
}

void GameTableLogic::dealOnCalculateBoard(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_TOTAL_RESULT, objectSize, "T_S2C_TOTAL_RESULT size of error!");
	T_S2C_TOTAL_RESULT* pData = (T_S2C_TOTAL_RESULT*)object;
	_uiCallback->showCalculateBoard(*pData);
}

void GameTableLogic::dealGameBeginResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_GAMEBEGIN_NOTIFY, objectSize, "T_S2C_GAMEBEGIN_NOTIFY size of error!");
	T_S2C_GAMEBEGIN_NOTIFY* pData = (T_S2C_GAMEBEGIN_NOTIFY*)object;

	clearDesk();

	_gameStatus = GS_PLAY_GAME;
	_uiCallback->setRecordEnable(false);
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		_tableInfo.bIsMingPai[i] = pData->bIsMingPai[i];
	}
}

void GameTableLogic::dealSendAllCardResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_SENDALLCARD_NOTIFY, objectSize, "T_S2C_SENDALLCARD_NOTIFY size of error!");
	T_S2C_SENDALLCARD_NOTIFY* pData = (T_S2C_SENDALLCARD_NOTIFY*)object;

	_gameStatus = GS_PLAY_GAME;
	_uiCallback->setRecordEnable(false);
	//屏蔽操作按钮
	_uiCallback->showActionButton(BUTTON_HIDE);
	HNLOG_INFO("_tableInfo.bTianLaiziCard = %d,_tableInfo.bDiLaiziCard = %d", _tableInfo.bTianLaiziCard, _tableInfo.bDiLaiziCard);

	for (int i = 0; i < PLAY_COUNT; i++)
	{
		_userCardCount[i] = pData->iUserCardCount[i];
		for (int j = 0; j < _userCardCount[i]; j++)
		{
			_userCardList[i][j] = pData->iUserCard[i][j];
		}
	}
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		BYTE seatNo = logicToViewSeatNo(i);
		_uiCallback->showUserOperationHints(i, STATE_HIDE);
		_tableInfo.iUserCardCount[i] = pData->iUserCardCount[i];
		memcpy(_tableInfo.iUserCard[i], pData->iUserCard[i], sizeof(pData->iUserCard[i]));

		std::vector<BYTE> cards(pData->iUserCardCount[i]);
		std::copy(pData->iUserCard[i], pData->iUserCard[i] + pData->iUserCardCount[i], cards.begin());
		// 是否显示明牌
		_uiCallback->showUserHandCard(seatNo, cards, _tableInfo.bTianLaiziCard,_tableInfo.bDiLaiziCard, false, false, (_tableInfo.bIsMingPai[i] || (i == _mySeatNo)));
		if (_tableInfo.bIsMingPai[i] || (i == _mySeatNo))
		{
			_uiCallback->setCardCountVis(seatNo, false);
		}

		std::vector<BYTE> CardsCount;
		for (int j = 0; j < pData->iUserCardCount[i]; j++)
		{
			CardsCount.push_back(_userCardList[i][j]);
		}
		// 显示其他玩家牌张数
		if (0 != seatNo)
		{
			_uiCallback->showUserHandCardCount(seatNo, pData->iUserCardCount[i]);
		}
		_uiCallback->setCardCountResult(_userCardList);
	}
}

void GameTableLogic::dealCallScoreResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_CALLSCORE_NOTIFY, objectSize, "T_S2C_CALLSCORE_NOTIFY size of error!");
	T_S2C_CALLSCORE_NOTIFY* pData = (T_S2C_CALLSCORE_NOTIFY*)object;
	
	if (pData->bDeskStation == _mySeatNo)
	{
		//显示叫分按钮
		_uiCallback->showActionButton(BUTTON_CALL_SCORE);
	}
	_uiCallback->showTimer(logicToViewSeatNo(pData->bDeskStation), _tableInfo.iCallScoreTime, true);
}

void GameTableLogic::dealCallScoreResultResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_CALLSCORE_RES, objectSize, "T_S2C_CALLSCORE_RES size of error!");
	T_S2C_CALLSCORE_RES* pData = (T_S2C_CALLSCORE_RES*)object;
	//显示叫分
	BYTE seatNo = logicToViewSeatNo(pData->bDeskStation);
	switch (pData->iValue)
	{
	case 0:
		_uiCallback->showUserOperationHints(seatNo, STATE_NO_CALL_LORD);
		break;
	case 1:
		_uiCallback->showUserOperationHints(seatNo, STATE_CALL_SCORE1);
		break;
	case 2:
		_uiCallback->showUserOperationHints(seatNo, STATE_CALL_SCORE2);
		break;
	case 3:
		_uiCallback->showUserOperationHints(seatNo, STATE_CALL_SCORE3);
		break;
	default:
		break;
	}
	if (_mySeatNo == pData->bDeskStation)
	{
		_uiCallback->showActionButton(BUTTON_HIDE);
	}
	_uiCallback->showTimer(seatNo, 0, false);
}

void GameTableLogic::dealRobNTResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_ROBNT_NOTIFY, objectSize, "T_S2C_ROBNT_NOTIFY size of error!");
	T_S2C_ROBNT_NOTIFY* pData = (T_S2C_ROBNT_NOTIFY*)object;

	//屏蔽操作按钮
	_uiCallback->showActionButton(BUTTON_HIDE);
	BYTE seatNo = logicToViewSeatNo(pData->bSeatNo);

	_uiCallback->showUserOperationHints(seatNo);
	_uiCallback->showTimer(seatNo, _tableInfo.iRobNTTime, true);
	if (pData->bSeatNo == _mySeatNo)
	{
		if(1 == pData->iValue)
		{
			_uiCallback->showActionButton(BUTTON_CALL_LORD);
		}
		else if(2 == pData->iValue)
		{
			_uiCallback->showActionButton(BUTTON_ROB_LORD);
		}
	}
	
	_uiCallback->showTimer(logicToViewSeatNo(pData->bSeatNo), _tableInfo.iRobNTTime, true);
}

void GameTableLogic::dealRobNTResultResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_ROBNT_RES, objectSize, "T_S2C_ROBNT_RES size of error!");
	T_S2C_ROBNT_RES* pData = (T_S2C_ROBNT_RES*)object;

	_uiCallback->showActionButton(BUTTON_HIDE);
	BYTE seatNo = logicToViewSeatNo(pData->bSeatNo);
	switch (pData->iValue)
	{
	case 0:
		_uiCallback->showUserOperationHints(seatNo, STATE_NO_CALL_LORD);
		break;
	case 1:
		_uiCallback->showUserOperationHints(seatNo, STATE_CALL_LORD);
		break;
	case 2:
		_uiCallback->showUserOperationHints(seatNo, STATE_NO_ROB_LORD);
		break;
	case 3:
		_uiCallback->showUserOperationHints(seatNo, STATE_ROB_LORD);
		break;
	default:
		break;
	}
	_uiCallback->showTimer(seatNo, 0, false);
	
}

void GameTableLogic::dealDingZhuang(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_DINGZHUANG_NOTIFY, objectSize, "T_S2C_DINGZHUANG_NOTIFY size of error!");
	T_S2C_DINGZHUANG_NOTIFY* pData = (T_S2C_DINGZHUANG_NOTIFY*)object;

	std::copy(pData->byBackCard, pData->byBackCard + BACK_CARD_COUNT,
		_userCardList[pData->bNtSeatNo] + _userCardCount[pData->bNtSeatNo]);

	_userCardCount[pData->bNtSeatNo] = pData->iCardCount[pData->bNtSeatNo];

	_uiCallback->showActionButton(BUTTON_HIDE);
	//保存庄家座位号
	_tableInfo.iLordSeatNo = pData->bNtSeatNo;

	//显示底分
	_uiCallback->setBasePoint(pData->iCallScoreNumber);

	//显示底牌
	std::vector<BYTE> cards(BACK_CARD_COUNT);
	std::copy(pData->byBackCard, pData->byBackCard + BACK_CARD_COUNT, cards.begin());


	auto iuser = pData->bNtSeatNo;


	std::vector<BYTE> vtShowmingPai(_userCardCount[iuser]);
	std::copy(_userCardList[iuser], _userCardList[iuser] + _userCardCount[iuser], vtShowmingPai.begin());

	//for (int i = 0; i < PLAY_COUNT; i++)
	//{
	//	_userCardCount[i] = pData->iCardCount[i];
	//	for (int j = 0; j < _userCardCount[i]; j++)
	//	{
	//		_userCardList[i][j] = pData->byUserCards[i][j];
	//	}
	//}
	//for (int  i = 0; i <PLAY_COUNT; i++)
	//{
	//	std::vector<BYTE> CardsCount;
	//	for (int j = 0; j < pData->iCardCount[i]; j++)
	//	{
	//		CardsCount.push_back(_userCardList[i][j]);
	//	}

	_uiCallback->showLandlordCard(cards);
	
	//显示地主
	if (pData->bNtSeatNo != INVALID_DESKSTATION)
	{
		_uiCallback->showLandlord(logicToViewSeatNo(pData->bNtSeatNo), true);
	}

	//刷新手牌
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		BYTE seatNo = logicToViewSeatNo(i);
		_uiCallback->showUserOperationHints(i, STATE_HIDE);
		_tableInfo.iUserCardCount[i] = pData->iCardCount[i];
		memcpy(_tableInfo.iUserCard[i], pData->byUserCards[i], sizeof(pData->byUserCards[i]));

		std::vector<BYTE> cards(pData->iCardCount[i]);
		std::copy(pData->byUserCards[i], pData->byUserCards[i] + pData->iCardCount[i], cards.begin());
		_uiCallback->showUserHandCard(seatNo, cards, _tableInfo.bTianLaiziCard,_tableInfo.bDiLaiziCard, false, true, (_tableInfo.bIsMingPai[i] || (i == _mySeatNo)));

		// 显示其他玩家牌张数
		if (0 != seatNo)
		{
			_uiCallback->showUserHandCardCount(seatNo, pData->iCardCount[i]);
		}
	}
	_uiCallback->setLordCard(true);
	_uiCallback->setRecordEnable(true);
	_uiCallback->setCardCountResult(_userCardList);
}

void GameTableLogic::dealLaiZiNotify(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_LAIZICARD_NOTIFY, objectSize, "T_S2C_DINGZHUANG_NOTIFY size of error!");
	T_S2C_LAIZICARD_NOTIFY* pData = (T_S2C_LAIZICARD_NOTIFY*)object;
	//保存癞子牌
	pData->bIsTianLai ? _tableInfo.bTianLaiziCard = pData->bLaiZi : _tableInfo.bDiLaiziCard = pData->bLaiZi;
	//显示癞子牌
	if (pData->bIsTianLai)
	{
		_uiCallback->setLaiziCard(true, pData->bLaiZi);
	}
	else
	{
		_uiCallback->setLaiziCard(true, pData->bLaiZi,false);
	}
	HNLOG_INFO("pData->bIsTianLai = %d,pData->bLaiZi = %d", pData->bIsTianLai, pData->bLaiZi);
	//天癞子发牌前通知
	if ((getGameRule() == TIANDI_LAIZI) && pData->bIsTianLai) return;

	//刷新玩家手牌,显示癞子标志
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		BYTE seatNo = logicToViewSeatNo(i);
		_tableInfo.iUserCardCount[i] = pData->iCardCount[i];
		memcpy(_tableInfo.iUserCard[i], pData->byUserCards[i], sizeof(pData->byUserCards[i]));

		std::vector<BYTE> cards(pData->iCardCount[i]);
		std::copy(pData->byUserCards[i], pData->byUserCards[i] + pData->iCardCount[i], cards.begin());
		_uiCallback->showUserHandCard(seatNo, cards, _tableInfo.bTianLaiziCard, _tableInfo.bDiLaiziCard, false, true, (_tableInfo.bIsMingPai[i] || (i == _mySeatNo)));

		// 显示其他玩家牌张数
		if (0 != seatNo)
		{
			_uiCallback->showUserHandCardCount(seatNo, pData->iCardCount[i]);
		}
	}
}

void GameTableLogic::dealAddDoubleResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_ADDDOUBLE_NOTIFY, objectSize, "T_S2C_ADDDOUBLE_NOTIFY size of error!");
	T_S2C_ADDDOUBLE_NOTIFY* pData = (T_S2C_ADDDOUBLE_NOTIFY*)object;

	//屏蔽操作按钮
	_uiCallback->showActionButton(BUTTON_HIDE);
	// 所有人都可以进行加倍操作
	if (pData->bDeskStation == _mySeatNo)
	{
		BYTE seatNo = logicToViewSeatNo(_mySeatNo);
		_uiCallback->showUserOperationHints(seatNo);
		_uiCallback->showActionButton(BUTTON_DOUBLE);
	}
	_uiCallback->showTimer(logicToViewSeatNo(pData->bDeskStation), _tableInfo.iAddDoubleTime, true);
}

void GameTableLogic::dealAddDoubleResultResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_AddDouble_RES, objectSize, "T_S2C_AddDouble_RES size of error!");
	T_S2C_AddDouble_RES* pData = (T_S2C_AddDouble_RES*)object;

	BYTE seatNo = logicToViewSeatNo(pData->bDeskStation);
	if (pData->iValue > 0)
	{
		_uiCallback->showUserOperationHints(seatNo, STATE_DOUBLE);
	}
	else
	{
		_uiCallback->showUserOperationHints(seatNo, STATE_NO_DOUBLE);
	}
	
	_uiCallback->showTimer(seatNo, 0, false);
	if (_mySeatNo == pData->bDeskStation)
	{
		_uiCallback->showActionButton(BUTTON_HIDE);
	}
}

void GameTableLogic::dealShowCardResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_SHOWCARD_NOTIFY, objectSize, "T_S2C_SHOWCARD_NOTIFY size of error!");
	T_S2C_SHOWCARD_NOTIFY* pData = (T_S2C_SHOWCARD_NOTIFY*)object;

	//屏蔽操作按钮
	_uiCallback->showActionButton(BUTTON_HIDE);

	for(int i = 0; i < PLAY_COUNT; i++)
	{
		BYTE seatNo = logicToViewSeatNo(i);
		_uiCallback->showUserOperationHints(seatNo);
	}

	HNLOG_INFO("_mySeatNo = %d, showCard seat = %d,_tableInfo.bIsMingPai[_mySeatNo] = %d", _mySeatNo, pData->bSeatNo, _tableInfo.bIsMingPai[_mySeatNo]);
	if ((pData->bSeatNo == _mySeatNo) && !_tableInfo.bIsMingPai[_mySeatNo])
	{
		_uiCallback->showActionButton(BUTTON_SHOW_CARD,false,_tableInfo.iLordSeatNo == INVALID_DESKSTATION);
		//_uiCallback->showTimer(logicToViewSeatNo(pData->bSeatNo), _tableInfo.iMingPaiTime, true);
	}
}

void GameTableLogic::dealShowCardResultResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_SHOWCARD_RES, objectSize, "T_S2C_SHOWCARD_RES size of error!");
	T_S2C_SHOWCARD_RES* pData = (T_S2C_SHOWCARD_RES*)object;

	if (pData->iRate > 0)
	{
		BYTE seatNo = logicToViewSeatNo(pData->bSeatNo);
		_tableInfo.bIsMingPai[pData->bSeatNo] = true;
		memcpy(_tableInfo.iUserCard[pData->bSeatNo], pData->iCardList, sizeof(pData->iCardList));

		std::vector<BYTE> cards(pData->iCardCount);
		std::copy(pData->iCardList, pData->iCardList + pData->iCardCount, cards.begin());
		_uiCallback->showUserHandCard(seatNo, cards, _tableInfo.bTianLaiziCard,_tableInfo.bDiLaiziCard, false, true, 
			(_tableInfo.bIsMingPai[pData->bSeatNo] || (pData->bSeatNo == _mySeatNo)));

		_uiCallback->showMingPaiMulImg(pData->iRate);
	}
	if (pData->bSeatNo == getMySeatNo())
	{
		_uiCallback->showActionButton(BUTTON_HIDE);
	}
}

void GameTableLogic::dealNewTurnResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_NEWTURN_NOTIFY, objectSize, "T_S2C_NEWTURN_NOTIFY size of error!");
	T_S2C_NEWTURN_NOTIFY* pData = (T_S2C_NEWTURN_NOTIFY*)object;

	_tableInfo.iOutCardSeatNo = pData->iOutDeskStation;
	_tableInfo.iLastOutCardCount = 0;
	_tableInfo.iLastOutCardList.clear();
	_tableInfo.promptIndex = 0;

	_uiCallback->showTimer(logicToViewSeatNo(pData->iOutDeskStation), _tableInfo.iThinkTime, true);
	//屏蔽操作按钮
	_uiCallback->showActionButton(BUTTON_HIDE);
	//是自己出牌
	if (pData->iOutDeskStation == _mySeatNo)
	{
		_uiCallback->downUserCards(logicToViewSeatNo(pData->iOutDeskStation));
	}
	GameAudio::playBackground(BK_TYPE::Normal);
	playerOut(pData->iOutDeskStation);

	_tableInfo.promptCards.clear();

	// 金币场显示托管按钮

	_uiCallback->setAutoBtnVisible(true);

}

void GameTableLogic::dealPromptResult(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_PROMPT_CARD_RES, objectSize, "T_S2C_PROMPT_CARD_RES size of error!");
	T_S2C_PROMPT_CARD_RES* pData = (T_S2C_PROMPT_CARD_RES*)object;
	if (pData->iCardCount > 0)
	{
		_tableInfo.promptCards = *pData;

		std::vector<BYTE> cards(_tableInfo.promptCards.sCards[0].iCardCount);
		std::copy(_tableInfo.promptCards.sCards[0].bActualCardList,
			_tableInfo.promptCards.sCards[0].bActualCardList + _tableInfo.promptCards.sCards[0].iCardCount, cards.begin());
		_uiCallback->setUpCardList(0, cards);
		((++_tableInfo.promptIndex) >= _tableInfo.promptCards.iCardCount) ? 0 : _tableInfo.promptIndex;
	}
	else
	{
		//不出
		sendNoOut();
		//_uiCallback->showGameTips(TIPS_CANNOTOUT);
	}
}
T_C2S_PLAY_CARD_REQ GameTableLogic::getReadyOutData()
{
	int index = _tableInfo.promptIndex > 0 ? _tableInfo.promptIndex - 1 : ((_tableInfo.promptCards.iCardCount > 0) ? (_tableInfo.promptCards.iCardCount - 1) : 0);
	return _tableInfo.promptCards.sCards[index];
}

void GameTableLogic::dealPassResult(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_PASS_RES, objectSize, "T_S2C_PASS_RES size of error!");
	T_S2C_PASS_RES* pData = (T_S2C_PASS_RES*)object;

	auto seatNo = logicToViewSeatNo(pData->bSeatNo);
	_uiCallback->showTimer(seatNo, 0, false);

	if (seatNo == 0)
	{
		_uiCallback->showActionButton(BUTTON_HIDE);
		_uiCallback->downUserCards(0);
	}
	//新一轮清除上家出牌数（解决玩家都不出时，新一轮按钮会先出现不出再隐藏的尴尬）
	if (pData->bNewTurn)
		_tableInfo.iLastOutCardCount = 0;
	//下一个出牌者显示出牌按钮
	playerOut(pData->bNextDeskStation);
	if (pData->bNextDeskStation == _mySeatNo)
	{
		_uiCallback->downUserCards(logicToViewSeatNo(pData->bNextDeskStation));
		if (!pData->bCanOutCard)
		{
			_uiCallback->showGameTips(TIPS_CANNOTOUT);
		}
	}

	//显示不出
	_uiCallback->showUserOperationHints(seatNo, STATE_NO_OUTCARD);
}

void GameTableLogic::dealTypeError(void* object, INT objectSize)
{
	//牌型错误
	_uiCallback->showGameTips(TIPS_INVALIDOUT);
}

void GameTableLogic::dealLaiziSelect(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_SELECT_RESULT_RES, objectSize, "T_S2C_SELECT_RESULT_RES size of error!");
	T_S2C_SELECT_RESULT_RES* pData = (T_S2C_SELECT_RESULT_RES*)object;

	//显示癞子组合牌型界面
	_uiCallback->showLaiziCardType(*pData);
	_uiCallback->showLaiziCardTypeLayer(true);
}

void GameTableLogic::dealOutCardResultResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_PLAY_CARD_RES, objectSize, "T_S2C_PLAY_CARD_RES size of error!");
	T_S2C_PLAY_CARD_RES* pData = (T_S2C_PLAY_CARD_RES*)object;

	_tableInfo.iLastOutCardCount = pData->tPlayCards.iCardCount;
	_tableInfo.promptIndex = 0;
	_uiCallback->showLaiziCardTypeLayer(false);

	if (pData->tPlayCards.iCardCount > 0)
	{

		int removeCount = _gameLogic.RemoveCard(pData->tPlayCards.bActualCardList, pData->tPlayCards.iCardCount, _userCardList[pData->bDeskStation], _userCardCount[pData->bDeskStation]);
		_userCardCount[pData->bDeskStation] -= removeCount;

		_tableInfo.iUserCardCount[pData->bDeskStation] = pData->bHandCardCount;
		std::copy(pData->bHandCard, pData->bHandCard + pData->tPlayCards.iCardCount, _tableInfo.iUserCard[pData->bDeskStation]);
	}	

	BYTE seatNo = logicToViewSeatNo(pData->bDeskStation);
	_uiCallback->showTimer(seatNo, 0, false);

	std::vector<BYTE> Actualcards(pData->tPlayCards.iCardCount);
	std::copy(pData->tPlayCards.bActualCardList, pData->tPlayCards.bActualCardList + pData->tPlayCards.iCardCount, Actualcards.begin());

	std::vector<BYTE> cards(pData->tPlayCards.iCardCount);
	std::copy(pData->tPlayCards.bActualCardList, pData->tPlayCards.bActualCardList + pData->tPlayCards.iCardCount, cards.begin());

	_uiCallback->showUserHandCardOut(seatNo, Actualcards, cards);

	_uiCallback->showUserOutCardType(pData->bDeskStation, (pData->tPlayCards.bCardList[0]%16), pData->tPlayCards.eArrayType);
	_uiCallback->showGameTips(TIPS_HIDE);

	if(pData->bDeskStation == _mySeatNo)
	{
		_uiCallback->showActionButton(BUTTON_HIDE);
	}
	else
	{
		_uiCallback->showUserHandCardCount(seatNo, pData->bHandCardCount);
	}

	std::vector<BYTE> handcards(pData->bHandCardCount);
	std::copy(pData->bHandCard, pData->bHandCard + pData->bHandCardCount, handcards.begin());
	_uiCallback->showUserHandCard(seatNo, handcards, _tableInfo.bTianLaiziCard,_tableInfo.bDiLaiziCard, false, true,
		(_tableInfo.bIsMingPai[pData->bDeskStation] || (pData->bDeskStation == _mySeatNo)));

	// 报单/报双
	if (pData->bHandCardCount == 1 || pData->bHandCardCount == 2)
	{
		GameAudio::playCardLast(getUserIsBoy(seatNo), pData->bHandCardCount);
	}

	playerOut(pData->iNextDeskStation);
	if (pData->iNextDeskStation == _mySeatNo)
	{
		_uiCallback->downUserCards(logicToViewSeatNo(pData->iNextDeskStation));
		if (!pData->bCanOutCard)
		{
			_uiCallback->showGameTips(TIPS_CANNOTOUT);
		}
	}
	_tableInfo.promptCards.clear();
	_uiCallback->setCardCountResult(_userCardList);
	_uiCallback->showTimer(logicToViewSeatNo(pData->iNextDeskStation), _tableInfo.iThinkTime, true);
}

void GameTableLogic::dealContinueEndResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_RESULT_NOTIFY, objectSize, "T_S2C_RESULT_NOTIFY size of error!");
	T_S2C_RESULT_NOTIFY* pData = (T_S2C_RESULT_NOTIFY*)object;
	_uiCallback->setRecordEnable(false);
	GameAudio::stopBackground();

	if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
	{
		//更新红包任务状态
		_uiCallback->CheckActivity();
	}

	_gameStatus = GS_WAIT_NEXT;

	std::vector<BYTE> cards;
	for(int i = 0; i < PLAY_COUNT; i++)
	{
		_uiCallback->showTimer(i, 0, false);
		BYTE seatNo = logicToViewSeatNo(i);
		cards.clear();
		if(pData->iUserCardCount[i] > 0)
		{
			cards.resize(pData->iUserCardCount[i]);
			std::copy(pData->iUserCard[i], pData->iUserCard[i] + pData->iUserCardCount[i], cards.begin());
		}
			
		_uiCallback->showUserHandCard(seatNo, cards, _tableInfo.bTianLaiziCard,_tableInfo.bDiLaiziCard, false, true, true);
		_uiCallback->showUserHandCardCount(seatNo, 0);
	}
	//取消托管
	_uiCallback->setAutoBtnVisible(false);
	for (int i = 0; i < PLAY_COUNT;i++)
	{
		_uiCallback->setUserAuto(i, false);
	}

	_uiCallback->setRecordEnable(false);
	_uiCallback->setRecordVis(false);
	_uiCallback->setLordCard(false);
	_uiCallback->showSingleCalculateBoard(*pData); //显示小结结算榜
}

void GameTableLogic::dealAutoResp(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_TRUSTEE_RES, objectSize, "T_S2C_TRUSTEE_RES size of error!");
	T_S2C_TRUSTEE_RES* pData = (T_S2C_TRUSTEE_RES*)object;

	_tableInfo.bUserAuto[pData->bDeskStation] = pData->bAuto;
	_uiCallback->setUserAuto(logicToViewSeatNo(pData->bDeskStation), pData->bAuto);
}

void GameTableLogic::dealUpdateRate(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_RATEUPDATE_NOTIFY, objectSize, "T_S2C_RATEUPDATE_NOTIFY size of error!");
	T_S2C_RATEUPDATE_NOTIFY* pData = (T_S2C_RATEUPDATE_NOTIFY*)object;

	//更新自己界面上显示的倍数
	_uiCallback->setMultiple(pData->iUserRate[_mySeatNo]);
}

void GameTableLogic::dealUserAgreeResp(MSG_GR_R_UserAgree* agree)
{
	HNLOG_INFO("dealUserAgreeResp");
	_tableInfo.bUserReady[agree->bDeskStation] = (agree->bAgreeGame == 1);
	if (agree->bDeskStation == _mySeatNo)
	{
		if (!isInGame())
		{
			clearDesk();
			_uiCallback->showActionButton(BUTTON_HIDE);
		}
	}

	if (1 == agree->bAgreeGame)
	{
		BYTE seatNo = logicToViewSeatNo(agree->bDeskStation);
		if(agree->bDeskStation != INVALID_DESKSTATION)
		{
			if (!isInGame())
			{
				 _uiCallback->showUserOperationHints(seatNo, STATE_READY);
			}
			_uiCallback->showTimer(seatNo, 0, false);
		}
	}
}

void GameTableLogic::dealGameActivityResp(MSG_GR_GameNum* pData)
{
	int GameNum = pData->iGameNum;
	int dangqianjieduan = pData->idangqianjieduan;
	int CanTakeNum = pData->iCanTakeNum;
	//更新红包活动数据
	_uiCallback->setActivityNum(GameNum, dangqianjieduan, CanTakeNum);
}

void GameTableLogic::dealGameActivityReceive(MSG_GR_Set_JiangLi* pData) 
{
	//领取奖励类型
	int ReceiveType = pData->iRandnum;
	int Index = pData->iJieDuan;
	if (Index == 1)				//阶段一奖励类型为金币，房卡（0-3金币，4-5房卡）
	{
		if (ReceiveType <= 3)
		{
			ReceiveType = 1;
		}
		else
		{
			ReceiveType = 2;
		}
	}
	int AwardNum = pData->iAwardNum;
	//显示中奖处理
	_uiCallback->setReceiveState(ReceiveType, AwardNum);
}
  
void GameTableLogic::dealGameStartResp(BYTE bDeskNO)
{
}

void GameTableLogic::dealGameClean()
{
	//GameManager::getInstance()->onGameStartClean();
}

void GameTableLogic::dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum)
{

}
void GameTableLogic::dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum)
{

}
void GameTableLogic::dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum)
{

}

void GameTableLogic::dealGameEndResp(BYTE deskNo)
{
}

void GameTableLogic::dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
{
	auto deskUser = getUserByUserID(user->dwUserID);

	bool isMe = (user->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
	if (isMe)
	{
		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_uiCallback->removeUser(i);
		}
		loadUsers();
	}
	else
	{
		BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
		_uiCallback->addUser(seatNo);

	
		_uiCallback->showVipEnterTip(seatNo);
		_uiCallback->showUserHandCardCount(logicToViewSeatNo(seatNo), _tableInfo.iUserCardCount[seatNo]);
		// 显示房主标志
		if (userSit->bDeskMaster) _uiCallback->showMaster(seatNo, true);
	}
	//_uiCallback->UpDateUserLocation(0);
}

void GameTableLogic::dealQueueUserSitMessage(bool success, const std::string& message)
{
	HNGameLogicBase::dealQueueUserSitMessage(success, message);

	LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

	if (success)
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_uiCallback->removeUser(i);
		}
		loadUsers();
		sendGameInfo();
	}
	else
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(message);
		prompt->setCallBack([=](){
			stop();
			RoomLogic()->close();
			GamePlatform::returnPlatform(ROOMLIST);
		});
	}
}

void GameTableLogic::dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
{
	HNGameLogicBase::dealUserUpResp(userSit, user);

	if (nullptr == user) return;
	BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
	_uiCallback->removeUser(seatNo);

	if (userSit->bDeskMaster) _uiCallback->showMaster(seatNo, false);
	
	if (INVALID_DESKSTATION == _mySeatNo)
	{
		stop();
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY
			|| RoomLogic()->getRoomRule() & GRR_QUEUE_GAME
			|| RoomLogic()->getRoomRule() & GRR_CONTEST
			|| RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
		{
			RoomLogic()->close();
			GamePlatform::createPlatform();
		}
		else
		{
			_uiCallback->leaveDesk(false);
		}
	}
}

void GameTableLogic::dealUserInfoChangeResp(const UserInfoStruct* user)
{
	auto deskUser = getUserByUserID(user->dwUserID);
	if (deskUser)
	{
		BYTE seatNo = logicToViewSeatNo(deskUser->bDeskStation);
		if (!RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			_uiCallback->setUserMoney(seatNo, user->i64Money);
		}
		
	}	
}

void GameTableLogic::dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo)
{
	_uiCallback->showNotice(pGameInfo->szMessage);
}

void GameTableLogic::dealUserCutMessageResp(INT userId, BYTE seatNo)
{
	_uiCallback->showUserCut(logicToViewSeatNo(seatNo), true);
}

void GameTableLogic::dealGameStationResp(void* object, INT objectSize)
{
	LLONG pScoreArray[PLAY_COUNT] = { 0 };
	HNLOG("dealGameStationResp");
	
	GameStation_Base* base = (GameStation_Base *)object;
	_gameStatus = base->bGameStation;
	_tableInfo.iGameStatus = base->bGameStation;
	_tableInfo.iBeginTime = base->iBegINTime;
	_tableInfo.iThinkTime = base->iThinkTime;
	_tableInfo.iCallScoreTime = base->iCallScoreTime;
	_tableInfo.iRobNTTime = base->iRobNTTime;
	_tableInfo.iAddDoubleTime = base->iAddDoubleTime;
	_tableInfo.iMingPaiTime = base->iMingPaiTime;
	_tableInfo.iBaseMul = base->iBaseMul;
	//显示底分
	_uiCallback->setBasePoint(base->iBasePoint);
	memcpy(pScoreArray, base->iuserMoney, sizeof(pScoreArray));
	//保存游戏人数
//	HNPlatformConfig()->setPlayerNum(_maxPlayers);
	setGameRule(base->iGameRule);
	_uiCallback->showGameRule(base->iGameRule);
	clearDesk();
	
	if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
	{
		//更新红包任务状态
		_uiCallback->CheckActivity();
	}

	switch (_gameStatus)
	{
	case GS_WAIT_SETGAME:
	case GS_WAIT_ARGEE:
	case GS_WAIT_NEXT:
		dealWaitStation(object, objectSize);
		//_uiCallback->setRecordEnable(true);
		_uiCallback->setRecordVis(false);
		break;
	case GS_CALL_SCORE:
	case GS_ROB_NT:
	case GS_ADD_DOUBLE:
	case GS_SHOW_CARD:
		dealBackCardStation(object, objectSize);
		break;
	case GS_PLAY_GAME:
		dealPlayStation(object, objectSize);
		_uiCallback->setLordCard(true);
		_uiCallback->setRecordEnable(true);
		_uiCallback->setRecordVis(false);
		
		break;
	break;
	default:
		break;
	}
	//_uiCallback->setUserScore(pScoreArray);
}

//设置回放模式
void GameTableLogic::dealGamePlayBack(bool isback)
{
	//GameManager::getInstance()->setPlayBack(isback);
}

//等待阶段
void GameTableLogic::dealWaitStation(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(GameStation_2, objectSize, "GameStation_2 size is error.");
	GameStation_2* data = (GameStation_2*)object;

	for (int i = 0; i < PLAY_COUNT; i++)
	{
		_tableInfo.bUserReady[i] = data->bUserReady[i];
		BYTE seatNo = logicToViewSeatNo(i);
		if (data->bUserReady[i])
		{
			_uiCallback->showUserOperationHints(seatNo, STATE_READY);
		}
	}

	if (_mySeatNo != INVALID_DESKSTATION && !_tableInfo.bUserReady[_mySeatNo])
	{
		_uiCallback->showTimer(logicToViewSeatNo(_mySeatNo), _tableInfo.iBeginTime, true);
		if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME)
		{
			_uiCallback->showActionButton(BUTTON_SHOW_CARD);
		}
		else
		{
			_uiCallback->showActionButton(BUTTON_READY);
		}
	}
	_uiCallback->setMultiple(_tableInfo.iBaseMul);
	_uiCallback->setRecordVis(false);
}

//扣压底牌阶段
void GameTableLogic::dealBackCardStation(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(GameStation_3, objectSize, "GameStation_3 size is error.");
	GameStation_3* data = (GameStation_3*)object;

	//保存庄家位置
	_tableInfo.iLordSeatNo = data->iUpGradePeople;
	if (data->iUpGradePeople != INVALID_DESKSTATION)
	{
		_uiCallback->showLandlord(logicToViewSeatNo(data->iUpGradePeople), true);
	}
	//保存癞子牌
	_tableInfo.bTianLaiziCard = data->bTianLaiZi;
	_tableInfo.bDiLaiziCard = data->bDiLaiZi;
	//显示癞子牌
	_uiCallback->setLaiziCard(true, data->bTianLaiZi);
	_uiCallback->setLaiziCard(true, data->bDiLaiZi,false);
	HNLOG_INFO("data->bTianLaiZi = %d,data->bDiLaiZi = %d", data->bTianLaiZi, data->bDiLaiZi);

	for (int i = 0; i < PLAY_COUNT; i++)
	{
		_uiCallback->setUserAuto(logicToViewSeatNo(i), data->bAuto[i]);
		_uiCallback->showUserOperationHints(i, STATE_HIDE);
		_tableInfo.iUserCardCount[i] = data->iUserCardCount[i];
		memcpy(_tableInfo.iUserCard[i], data->bUserCard[i], sizeof(data->bUserCard[i]));

		if (data->iUserCardCount[i] > 0)
		{
			BYTE seatNo = logicToViewSeatNo(i);
			std::vector<BYTE> cards(data->iUserCardCount[i]);
			std::copy(data->bUserCard[i], data->bUserCard[i] + data->iUserCardCount[i], cards.begin());

			_uiCallback->showUserHandCard(seatNo, cards, _tableInfo.bTianLaiziCard,_tableInfo.bDiLaiziCard, false, true, (data->bIsMingPai[i] || (i == _mySeatNo)));
			if (0 != seatNo)
			{
				_uiCallback->showUserHandCardCount(seatNo, data->iUserCardCount[i]);
			}
		}
	}

	//显示底牌
	std::vector<BYTE> cards(BACK_CARD_COUNT);
	std::copy(data->bGameBackCard, data->bGameBackCard + data->iBackCardCount, cards.begin());
	_uiCallback->showLandlordCard(cards);

	BYTE seatNo = logicToViewSeatNo(data->iCurOperator);
	switch (_gameStatus)
	{
	case GS_CALL_SCORE:
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			BYTE seatNo = logicToViewSeatNo(i);
			switch (data->bCallScore[i])
			{
			case 0:
				_uiCallback->showUserOperationHints(seatNo, STATE_NO_CALL_LORD);
				break;
			case 1:
				_uiCallback->showUserOperationHints(seatNo, STATE_CALL_SCORE1);
				break;
			case 2:
				_uiCallback->showUserOperationHints(seatNo, STATE_CALL_SCORE2);
				break;
			case 3:
				_uiCallback->showUserOperationHints(seatNo, STATE_CALL_SCORE3);
				break;
			default:
				break;
			}			
		}

		if ((data->iCurOperator == _mySeatNo) && data->bCallScore[_mySeatNo] == INVALID_DESKSTATION)
		{
			_uiCallback->showActionButton(BUTTON_CALL_SCORE);
		}
		_uiCallback->showTimer(seatNo, _tableInfo.iCallScoreTime, true);
	}
		break;
	case GS_ROB_NT:
	{
		bool isFirstRob = true;   //是否是第一个叫地主的人
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			BYTE seatNo = logicToViewSeatNo(i);
			switch (data->bRobNT[i])
			{
			case 0:
				isFirstRob = false;
				_uiCallback->showUserOperationHints(seatNo, STATE_NO_CALL_LORD);
				break;
			case 1:
				isFirstRob = false;
				_uiCallback->showUserOperationHints(seatNo, STATE_CALL_LORD);
				break;
			case 2:
				isFirstRob = false;
				_uiCallback->showUserOperationHints(seatNo, STATE_NO_ROB_LORD);
				break;
			case 3:
				isFirstRob = false;
				_uiCallback->showUserOperationHints(seatNo, STATE_ROB_LORD);
				break;
			default:
				break;
			}
		}
		if ((data->iCurOperator == _mySeatNo) && data->bRobNT[_mySeatNo] == INVALID_DESKSTATION)
		{
			if (isFirstRob)
			{
				_uiCallback->showActionButton(BUTTON_CALL_LORD);
			}
			else
			{
				_uiCallback->showActionButton(BUTTON_ROB_LORD);
			}
		}
		_uiCallback->showTimer(seatNo, _tableInfo.iCallScoreTime, true);
	}
		break;
	case GS_ADD_DOUBLE:
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			BYTE seatNo = logicToViewSeatNo(i);
			switch (data->bUserDoubleValue[i])
			{
			case 0:
				_uiCallback->showUserOperationHints(seatNo, STATE_NO_DOUBLE);
				break;
			case 1:
				_uiCallback->showUserOperationHints(seatNo, STATE_DOUBLE);
				break;
			default:
				break;
			}
		}
		if (data->bUserDoubleValue[_mySeatNo] == INVALID_DESKSTATION)
		{
			_uiCallback->showActionButton(BUTTON_DOUBLE);
		}
		_uiCallback->showTimer(seatNo, _tableInfo.iAddDoubleTime, true);
	}
		break;
	case GS_SHOW_CARD:
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (data->bIsMingPai[i] == 0)
			{
				if (i == _mySeatNo)
				{
					_uiCallback->showActionButton(BUTTON_SHOW_CARD);
				}
				_uiCallback->showTimer(seatNo, _tableInfo.iMingPaiTime, true);
			}
			else
			{
				_uiCallback->showUserOperationHints(seatNo, STATE_SHOW_CARD);
			}
		}
	}
		break;
	default:
		break;
	}
}

//游戏中阶段
void GameTableLogic::dealPlayStation(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(GameStation_4, objectSize, "GameStation_4 size is error.");
	GameStation_4* data = (GameStation_4*)object;

	//保存庄家位置
	_tableInfo.iLordSeatNo = data->iUpGradePeople;
	_tableInfo.iOutCardSeatNo = data->iOutCardPeople;

	//保存癞子牌
	_tableInfo.bTianLaiziCard = data->bTianLaiZi;
	_tableInfo.bDiLaiziCard = data->bDiLaiZi;
	//显示癞子牌
	_uiCallback->setLaiziCard(true, data->bTianLaiZi);
	_uiCallback->setLaiziCard(true, data->bDiLaiZi,false);
	HNLOG_INFO("data->bTianLaiZi = %d,data->bDiLaiZi = %d", data->bTianLaiZi, data->bDiLaiZi);

	if (data->iLastOutCardPeople != INVALID_DESKSTATION)
	{
		_tableInfo.iLastOutCardCount = data->iDeskCardList[data->iLastOutCardPeople].iCardCount;
		_tableInfo.iLastOutCardList = data->iDeskCardList[data->iLastOutCardPeople];
	}

	for (int i = 0; i < PLAY_COUNT; i++)
	{
		BYTE seatNo = logicToViewSeatNo(i);
		_uiCallback->setUserAuto(seatNo, data->bAuto[i]);
		_uiCallback->showUserOperationHints(i, STATE_HIDE);
		_tableInfo.iUserCardCount[i] = data->iUserCardCount[i];
		_userCardCount[i] = data->iUserCardCount[i];
		
		memcpy(_tableInfo.iUserCard[i], data->iUserCard[i], sizeof(data->iUserCard[i]));
		
		if (data->iDeskCardList[i].iCardCount > 0 && !data->bPass[i] && data->iOutCardPeople != i)
		{
			std::vector<BYTE> Actualcards(data->iDeskCardList[i].iCardCount);
			std::copy(data->iDeskCardList[i].bActualCardList, data->iDeskCardList[i].bActualCardList + data->iDeskCardList[i].iCardCount,
				Actualcards.begin());

			std::vector<BYTE> cards(data->iDeskCardList[i].iCardCount);
			std::copy(data->iDeskCardList[i].bCardList, data->iDeskCardList[i].bCardList + data->iDeskCardList[i].iCardCount, 
				cards.begin());

			_uiCallback->showUserHandCardOut(seatNo, Actualcards, cards);
		}
		
		

		//显示不出标识
		if (data->bPass[i])
		{
			_uiCallback->showUserOperationHints(seatNo, STATE_NO_OUTCARD);
		}

		if (data->iUserCardCount[i] > 0)
		{
			std::vector<BYTE> cards(data->iUserCardCount[i]);
			std::copy(data->iUserCard[i], data->iUserCard[i] + data->iUserCardCount[i], cards.begin());

			_uiCallback->showUserHandCard(seatNo, cards, _tableInfo.bTianLaiziCard,_tableInfo.bDiLaiziCard, false, true, (data->bIsMingPai[i] || (i == _mySeatNo)));
			if (0 != seatNo)
			{
				_uiCallback->showUserHandCardCount(seatNo, data->iUserCardCount[i]);
			}
		}	
		for (int j = 0; j < _tableInfo.iUserCardCount[i]; j++)
		{
			_userCardList[i][j] = data->iUserCard[i][j];
		}
	}

	//显示底牌
	std::vector<BYTE> cards(BACK_CARD_COUNT);
	std::copy(data->iGameBackCard, data->iGameBackCard + data->iBackCardCount, cards.begin());
	_uiCallback->showLandlordCard(cards);

	if (data->iOutCardPeople == _mySeatNo)
	{
		BYTE seatNo = logicToViewSeatNo(data->iOutCardPeople);
		if (!data->bCanOutCard)
		{
			_uiCallback->showGameTips(TIPS_CANNOTOUT);
		}
		_uiCallback->showTimer(seatNo, _tableInfo.iThinkTime, true);
		_uiCallback->showActionButton(BUTTON_OUTCARD, 0 == _tableInfo.iLastOutCardCount);
	}
	if (data->iUpGradePeople != INVALID_DESKSTATION)
	{
		_uiCallback->showLandlord(logicToViewSeatNo(data->iUpGradePeople), true);
	}

	// 金币场显示托管按钮

	
	_uiCallback->setAutoBtnVisible(true);


	_uiCallback->setCardCountResult(_userCardList);
}

void GameTableLogic::sendForceQuit()
{
	HNLOG_INFO("sendForceQuit");
	if (RoomLogic()->isConnect())
	{
		HNGameLogicBase::sendForceQuit();
	}
	_uiCallback->leaveDesk(true);
}

void GameTableLogic::sendUserUp()
{
	HNLOG_INFO("sendUserUp");
	if (RoomLogic()->isConnect())
	{
		HNGameLogicBase::sendUserUp();
	}
	else
	{
		_uiCallback->leaveDesk(false);
	}
}

void GameTableLogic::sendShowStart(bool bMing)
{
	if (bMing)
	{
		HNLOG_INFO("sendShowStart");
	}
	else
	{
		HNLOG_INFO("sendNotShowStart");
	}
	T_C2S_MINGPAI_RES data;
	data.bDeskStaion = _mySeatNo;
	data.bMing = bMing;
	data.bStart = true;
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_GM_AGREE_GAME, (void*)&data, sizeof(data));
}

void GameTableLogic::sendOutCard(const std::vector<BYTE> Actualcards, const std::vector<BYTE> cards, EArrayType type)
{
	HNLOG_INFO("sendOutCard");
	T_C2S_PLAY_CARD_REQ data;
	data.eArrayType = type;
	data.iCardCount = cards.size();
	for(size_t i = 0; i < cards.size(); i++)
	{
		data.bActualCardList[i] = Actualcards[i];
		data.bCardList[i] = cards[i];
	}
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_OUT_CARD, (void*)&data, sizeof(data));
}

void GameTableLogic::sendNoOut()
{
	HNLOG_INFO("sendNoOut");
	T_C2S_PASS_REQ data;
	data.bPass = true;
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_PASS, (void*)&data, sizeof(data));
}

void GameTableLogic::sendPrompt()
{
	if (_tableInfo.promptCards.iCardCount > 0)
	{
		std::vector<BYTE> cards(_tableInfo.promptCards.sCards[_tableInfo.promptIndex].iCardCount);
		std::copy(_tableInfo.promptCards.sCards[_tableInfo.promptIndex].bActualCardList,
			_tableInfo.promptCards.sCards[_tableInfo.promptIndex].bActualCardList + _tableInfo.promptCards.sCards[_tableInfo.promptIndex].iCardCount, cards.begin());
		_uiCallback->setUpCardList(0, cards);

		++_tableInfo.promptIndex;

		if (_tableInfo.promptIndex >= _tableInfo.promptCards.iCardCount)
		{
			_tableInfo.promptIndex = 0;
		}
	}
	else
	{
		HNLOG_INFO("sendPrompt");
		T_C2S_PROMPT_CARD_REQ data;
		data.bSeatNo = _mySeatNo;
		data.bPromptType = 2;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_PROMPT, &data, sizeof(data));
	}
}

void GameTableLogic::sendRobNT(int value)
{
	HNLOG_INFO("sendRobNT");
	T_C2S_ROBNT_REQ data;
	memset(&data, 0, sizeof(data));
	data.bSeatNo = _mySeatNo;
	data.iValue = value;
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_ROB_NT_RESULT, &data, sizeof(data));
}

void GameTableLogic::sendCallScore(int value)
{
	HNLOG_INFO("sendCallScore");
	T_C2S_CALLSCORE_REQ data;
	memset(&data, 0, sizeof(data));
	data.bDeskStation = _mySeatNo;
	data.iValue = value;
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_CALL_SCORE_RESULT, &data, sizeof(data));
}

void GameTableLogic::sendDouble(int value)
{
	HNLOG_INFO("sendDouble");
	T_C2S_AddDouble_RES data;
	memset(&data, 0, sizeof(data));
	data.bDeskStation = _mySeatNo;
	data.iValue = value;
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_ADD_DOUBLE_RESULT, &data, sizeof(data));
}

void GameTableLogic::sendShowCard(int value)
{
	HNLOG_INFO("sendShowCard value = %d",value);
	T_C2S_SHOWCARD_REQ data;
	memset(&data, 0, sizeof(data));
	data.bSeatNo = _mySeatNo;
	data.iRate = value;
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_SHOW_CARD_REQ, &data, sizeof(data));
}

void GameTableLogic::sendShowCanel()
{
	HNLOG_INFO("sendShowCanel");
	T_C2S_SHOWCARD_REQ data;
	memset(&data, 0, sizeof(data));
	data.bSeatNo = 255;
	data.iRate = 0;
	//RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_SHOW_CARD_REQ, &data,sizeof(data));
}

bool GameTableLogic::sendAuto(bool bAuto)
{
	HNLOG_INFO("sendAuto");
	T_S2C_TRUSTEE_RES TrustData;
	TrustData.bAuto = bAuto ; 
	TrustData.bDeskStation = _mySeatNo; 
	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_AUTO, &TrustData, sizeof(T_S2C_TRUSTEE_RES));
	return true;
}

void GameTableLogic::enterGame()
{
	if (_autoCreate)
	{
		loadUsers();
		sendGameInfo();
	}
}

void GameTableLogic::loadUsers()
{
	for(int i = 0; i < PLAY_COUNT; i++)
	{
		const UserInfoStruct* pUser = getUserBySeatNo(i);
		if(_existPlayer[i] && pUser != nullptr)
		{
			BYTE seatNo = logicToViewSeatNo(i);
			_uiCallback->addUser(seatNo);
		}
	}
	//_uiCallback->UpDateUserLocation(0);
}

void GameTableLogic::clearDesk()
{
	HNGameLogicBase::clearDesk();
	_uiCallback->clearDesk();
	_uiCallback->setMultiple(_tableInfo.iBaseMul);
	_tableInfo.promptCards.clear();
	_tableInfo.bTianLaiziCard = 0;
	_tableInfo.bDiLaiziCard = 0;
	_tableInfo.iLordSeatNo = INVALID_DESKSTATION;
}

void GameTableLogic::autoOutCheck(BYTE vSeatNo, const std::vector<BYTE>& cards)
{
	BYTE lSeatNo = viewToLogicSeatNo(vSeatNo);

	int  upCardCount = cards.size();
	BYTE upCardList[MAX_CARD_COUNT] = {0};

	std::copy(cards.begin(), cards.end(), upCardList);
	int  resultCardCount = 0;
	BYTE resultCardList[MAX_CARD_COUNT] = { 0 };

	if(upCardCount >= 5 && 0 == _tableInfo.iLastOutCardCount)
	{
		_gameLogic.DragCardAutoSetValidCard(upCardList, upCardCount, resultCardList, resultCardCount ,_tableInfo.bTianLaiziCard, _tableInfo.bDiLaiziCard);
		if(resultCardCount >= 5)
		{
			std::vector<BYTE> cards1(resultCardCount);
			std::copy(resultCardList, resultCardList + resultCardCount, cards1.begin());
			_uiCallback->setUpCardList(vSeatNo, cards1);
		}
		else
		{
			_uiCallback->setUpCardList(vSeatNo, cards);
		}
	}
	else
	{
		if (1 == upCardCount)
		{
			_gameLogic.AITrackOutCard(_tableInfo.iUserCard[lSeatNo], _tableInfo.iUserCardCount[lSeatNo], upCardList, upCardCount,
				_tableInfo.iLastOutCardList.bCardList, _tableInfo.iLastOutCardCount, resultCardList, resultCardCount);

			if (resultCardCount > 0 && _gameLogic.CanOutCard(resultCardList, resultCardCount, _tableInfo.iLastOutCardList.bCardList,
				_tableInfo.iLastOutCardCount, _tableInfo.iUserCard[lSeatNo], _tableInfo.iUserCardCount[lSeatNo], 0 == _tableInfo.iLastOutCardCount))
			{
				std::vector<BYTE> card2(resultCardCount);
				std::copy(resultCardList, resultCardList + resultCardCount, card2.begin());
				_uiCallback->setUpCardList(vSeatNo, card2);
			}
			else
			{
				_uiCallback->setUpCardList(vSeatNo, cards);
			}
		}
		else
		{
			_uiCallback->setUpCardList(vSeatNo, cards);
		}
	}
}

void GameTableLogic::playerOut(BYTE seatNo)
{
	if (seatNo >= 0 && seatNo < PLAY_COUNT)
	{
		BYTE vSeatNo = logicToViewSeatNo(seatNo);
		// 清理桌面上轮出的牌
		std::vector<BYTE> cards;
		_uiCallback->showUserHandCardOut(vSeatNo, cards, cards);

		_uiCallback->showGameTips(TIPS_HIDE);

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			// 隐藏所有状态提示
			_uiCallback->showUserOperationHints(i);
		}

		// 显示出牌倒计时
		_uiCallback->showTimer(vSeatNo, _tableInfo.iThinkTime, true);
		if (seatNo == _mySeatNo)
		{
			// 显示出牌操作按钮（第一个出牌不显示不出按钮）
			_uiCallback->showActionButton(BUTTON_OUTCARD, 0 == _tableInfo.iLastOutCardCount);
		}
	}
}

bool GameTableLogic::isInGame()
{
	if (_gameStatus == GS_WAIT_SETGAME || _gameStatus == GS_WAIT_ARGEE || _gameStatus == GS_WAIT_NEXT)
	{
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////
////聊天接口
////////////////////////////////////////////////////////////////////
void GameTableLogic::dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk)
{
	auto pUser = UserInfoModule()->findUser(normalTalk->dwSendID);
	if (pUser != nullptr)
	{
		_uiCallback->onChatTextMsg(pUser->bDeskStation, normalTalk->szMessage);
	}
}

//特效道具聊天
void GameTableLogic::dealUserActionMessage(DoNewProp* normalAction)
{
	auto pUser = UserInfoModule()->findUser(normalAction->dwUserID);
	if (pUser != nullptr)
	{
		auto SendUser = UserInfoModule()->findUser(normalAction->dwUserID);
		auto TargetUser = UserInfoModule()->findUser(normalAction->dwDestID);
		_uiCallback->onActionChatMsg(SendUser->bDeskStation, TargetUser->bDeskStation, normalAction->iPropNum);
		//发送特效道具扣除用户金币
		_uiCallback->afterScoreByDeskStation(SendUser->bDeskStation, -50);
	}
}

void GameTableLogic::dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
{
	//_uiCallback->onChatVoiceMsg(msg->uUserID, msg->uVoiceID, msg->iVoiceTime);
	switch (messageHead->bAssistantID)
	{
	case ASS_GR_VOIEC:
	{
		//效验数据
		CCAssert(sizeof(VoiceInfo) == objectSize, "VoiceInfo size is error.");
		VoiceInfo* voiceInfo = (VoiceInfo*)object;
		auto userInfo = getUserByUserID(voiceInfo->uUserID);
		if (!userInfo) return;
		if (userInfo->bDeskNO == _deskNo)
		{
			_uiCallback->onChatVoiceMsg(voiceInfo->uUserID, voiceInfo->uVoiceID, voiceInfo->iVoiceTime);
		}
	}
	break;
	default:
		break;
	}
}

//void GameTableLogic::dealGiftMessage(NetMessageHead* messageHead, void* object, INT objectSize)
//{
//	switch (messageHead->bAssistantID)
//	{
//	//case ASS_GR_GIFT:
//	//{
//	//					//效验数据
//	//					CCAssert(sizeof (TMSG_SENG_GIFT_REQ) == objectSize, "TMSG_SENG_GIFT_REQ size is error.");
//	//					TMSG_SENG_GIFT_REQ* data = (TMSG_SENG_GIFT_REQ*)object;
//	//					_uiCallback->dealGift(*data);
//	//}
//		break;
//	default:
//		break;
//	}
//}

void GameTableLogic::clearDeskUsers()
{
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		BYTE seatNo = logicToViewSeatNo(i);
		const UserInfoStruct* UserInfo = getUserBySeatNo(i);

		if (seatNo == 0) continue;

		if (!UserInfo) _uiCallback->removeUser(seatNo);
	}
}

void GameTableLogic::initParams()
{
	_tableInfo.Clear();

	for (int i = 0; i < PLAY_COUNT; i++)
	{
		memset(_userCardList[i], 0, sizeof(_userCardList[i]));
	}
	memset(_userCardCount, 0, sizeof(_userCardCount));
}

void GameTableLogic::dealRecord(void* object, INT objectSize)
{
	CHECK_SOCKET_DATA(T_S2C_SENDALLCARD_NOTIFY, objectSize, "T_S2C_SENDALLCARD_NOTIFY size of error!");
	T_S2C_SENDALLCARD_NOTIFY* pData = (T_S2C_SENDALLCARD_NOTIFY*)object;

}



}