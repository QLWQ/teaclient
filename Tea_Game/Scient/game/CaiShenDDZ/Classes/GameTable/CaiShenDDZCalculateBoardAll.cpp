/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZCalculateBoardAll.h"
#include <string>
#include "HNLogicExport.h"
#include "HNLobbyExport.h"
#include "HNOpenExport.h"
#include "HNLobby/GameChildLayer/GameShareLayer.h"

using namespace CaiShenDDZ;

static const char* calculateBoardcsbFilePath = "CaiShenDDZ/calculateBoard/allBoard/allBoard.csb";

static const char* Player_Normal_M = "CaiShenDDZ/tableUI/res/men_head.png";
static const char* Player_Normal_W = "CaiShenDDZ/tableUI/res/women_head.png";

CalculateBoardAll::CalculateBoardAll() 
{
	
}

CalculateBoardAll::~CalculateBoardAll()
{

}

bool CalculateBoardAll::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(calculateBoardcsbFilePath);
	node->setPosition(_winSize / 2);
	addChild(node, 2);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_board"));

	_img_bg = dynamic_cast<ImageView*>(layout->getChildByName("Image_bg"));

	layout->setScale(_winSize.width / 1280, _winSize.height / 720);

	float scalex = 1280 / _winSize.width;
	float scaley = 720 / _winSize.height;
	_img_bg->setScale(scalex, scaley);

	// 恐龙谷分享
// 	auto btn_klg = (Button*)_img_bg->getChildByName("btn_klg");
// 	if (btn_klg) {
// 		btn_klg->addTouchEventListener(CC_CALLBACK_2(CalculateBoardAll::shareBtnCallBack, this));
// 	}

	// 微信分享
	auto btn_wechat = (Button*)_img_bg->getChildByName("btn_wechat");
	if (btn_wechat) {
		btn_wechat->addTouchEventListener(CC_CALLBACK_2(CalculateBoardAll::shareBtnCallBack, this));
	}

	//返回按钮
	auto btn_back = (Button*)_img_bg->getChildByName("btn_back");
	if (btn_back)
	{
		btn_back->addTouchEventListener(CC_CALLBACK_2(CalculateBoardAll::backBtnCallBack, this));
	}

	return true;
}

void CalculateBoardAll::backBtnCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (Widget::TouchEventType::ENDED != type) return;

	RoomLogic()->close();
	GamePlatform::createPlatform();
}

void CalculateBoardAll::shareBtnCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (Widget::TouchEventType::ENDED != type) {
		return;
	}

	std::string gameName = "斗地主";
	Button* pShare = (Button*)pSender;
	std::string name = pShare->getName();
	this->runAction(Sequence::create(DelayTime::create(3.f), CallFunc::create([=]()
	{
		_isTouch = true;
	}), nullptr));

	auto shareLayer = GameShareLayer::create();
	shareLayer->setName("shareLayer");
	shareLayer->SetShareInfo("", "", "");
	shareLayer->show();
}

void CalculateBoardAll::showAndUpdateBoard(Node* parent, T_S2C_TOTAL_RESULT boardData)
{
	//显示结算
	parent->addChild(this, 100000009);

	//更新房间号
	auto txt_roomNumber = dynamic_cast<Text*>(_img_bg->getChildByName("txt_roomNumber"));
	txt_roomNumber->setString(StringUtils::format(GBKToUtf8("房号：%s"), HNPlatformConfig()->getVipRoomNum().c_str()));

	for (int i = 0; i < PLAY_COUNT; ++i)
	{
		auto subLayout = dynamic_cast<Layout*>(_img_bg->getChildByName(StringUtils::format("layout_score%d", i)));

		// 更新总成绩
		auto txt_allScore = dynamic_cast<Text*>(subLayout->getChildByName("txt_allScore"));
		txt_allScore->setString(to_string(boardData.i64WinMoney[i]));

		// 更新大赢家
		auto image_win = dynamic_cast<ImageView*>(subLayout->getChildByName("image_winner"));
		image_win->setVisible(boardData.bWinner[i]);

		// 更新炸弹次数
		auto txt_zadan_score = dynamic_cast<Text*>(subLayout->getChildByName("txt_zadan_score"));
		txt_zadan_score->setString(to_string(boardData.iBombCount[i]));

		// 更新最高赢点
		auto txt_zuigaoyindian_score = dynamic_cast<Text*>(subLayout->getChildByName("txt_zuigaoyindian_score"));
		std::string moneyStr("");
		if (boardData.iMaxWinMoney[i] <= -10000 || boardData.iMaxWinMoney[i] >= 10000)
		{
			//boardData.iMaxWinMoney[i] = boardData.iMaxWinMoney[i] / 10000;
			moneyStr = StringUtils::format("%lld", boardData.iMaxWinMoney[i]);
		}
		else 
		{
			moneyStr = StringUtils::format("%lld", boardData.iMaxWinMoney[i]);
		}
		txt_zuigaoyindian_score->setString(moneyStr);

		// 更新最高连胜
		auto txt_zuigaoliansheng_score = dynamic_cast<Text*>(subLayout->getChildByName("txt_zuigaoliansheng_score"));
		txt_zuigaoliansheng_score->setString(to_string(boardData.iMaxContinueCount[i]));

		// 更新胜局
		auto txt_shengju_score = dynamic_cast<Text*>(subLayout->getChildByName("txt_shengju_score"));
		txt_shengju_score->setString(to_string(boardData.iWinCount[i]));

		// 更新玩家昵称
		auto txt_name = dynamic_cast<Text*>(subLayout->getChildByName("txt_name"));
		txt_name->setString(GBKToUtf8("未知"));

		// 更新房主图标
		auto image_fangzhu = dynamic_cast<ImageView*>(subLayout->getChildByName("image_fangzhu"));
		image_fangzhu->setVisible(false);

		auto user = UserInfoModule()->findUser(RoomLogic()->loginResult.pUserInfoStruct.bDeskNO, i);
		if (!user) continue;

		txt_name->setString(GBKToUtf8(user->nickName));
		
		// 更新玩家ID
		auto txt_Id = dynamic_cast<Text*>(subLayout->getChildByName("txt_userID"));
		txt_Id->setString(to_string(user->dwUserID));
		
		image_fangzhu->setVisible(user->dwUserID == HNPlatformConfig()->getMasterID());

		auto img_head = dynamic_cast<ImageView*>(subLayout->getChildByName("Image_head"));
		auto img_frame = dynamic_cast<ImageView*>(subLayout->getChildByName("Image_headFrame"));

		// 更新用户结算榜头像
		auto userHead = GameUserHead::create(img_head, img_frame);
		userHead->show();
		userHead->loadTexture(user->bBoy ? Player_Normal_M : Player_Normal_W);
		userHead->setHeadByFaceID(user->bLogoID);
		userHead->loadTextureWithUrl(user->headUrl);
	}	
}