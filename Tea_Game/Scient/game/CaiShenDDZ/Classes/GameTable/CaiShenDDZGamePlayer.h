/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_GAME_PLAYER_H__
#define __CaiShenDDZ_GAME_PLAYER_H__

#include "cocos2d.h"
#include "HNNetExport.h"
#include "HNLobbyExport.h"
#include "CaiShenDDZMessageHead.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>



const std::string GAME_SOURCE_PAHT = "Games/QZHOUJJMJ/";
const std::string MUSIC_PATH = GAME_SOURCE_PAHT + "Music/";
const std::string MUSIC_SEX_PATH = MUSIC_PATH + "putong/";
const std::string MUSIC_BG_PATH = MUSIC_PATH + "background/";
const std::string MUSIC_COMMON_PATH = MUSIC_PATH + "common/";

const std::string COCOS_PATH = GAME_SOURCE_PAHT + "cocos/";

using namespace ui;
using namespace std;

namespace CaiShenDDZ
{
	class PlayerUI : public HNLayer
	{
	public:
		static PlayerUI* create(ImageView* user, int index);
		virtual bool init(ImageView* user, int index);

	public:
		void loadUser(UserInfoStruct* info);

	public:
		// 玩家是否显示
		void setVisible(bool visible);

		// 设置玩家性别
		void setUserSex(bool isBoy);

		// 设置显示玩家ID
		void setUserId(INT userId);

		//设置玩家IP
		//void setUserIP(INT userId);

		// 设置显示玩家昵称
		void setUserName(const std::string& name);

		//??è?í??ò?e±ò

		LLONG ffmoney;

		LLONG getUserMoney();

		// éè????ê?í??ò?e??
		void setUserMoney(LLONG money);

		// 设置显示玩家牌张
		void setUserCardCount(int count = 0);

		void setCardCountVis(bool visible);

		// 设置显示玩家托管状态
		void setAutoHead(bool isAuto);

		// 设置显示玩家头像
		void setHead(int FaceID);

		// 设置显示玩家游戏形象
		void setGameHead(bool isBoy, bool isLord);

		//设置玩家信息6.4
		void getUserInfo(Ref* pSender, Widget::TouchEventType type);

		//关闭玩家信息6.5
		void closeLayer(Ref* pSender, Widget::TouchEventType type);

		// 清理玩家头像
		void setEmptyHead();

		// 设置玩家庄家状态哎
		void setBanker(bool isBanker);

		// 设置玩家房主状态
		void setOwner(bool isOwner);

		// 设置玩家离线状态
		void setOffline(bool isOffline);

		// 设置显示玩家状态
		void setUserOperationHints(Hint_Type type = STATE_HIDE);

		// 显示闹钟（倒计时结束带回调）
		void showTimer(int delayTime, std::function<void()> func);

		// 显示闹钟（可以中途关闭闹钟）
		void showTimer(int delayTime, bool visible);

		Vec2 getHeadPosition();

		Size getHeadSize();

		Node* getChatParent();

		std::function<void()> userMessageCallBack = nullptr;
		void playGiftAni(Vec2 startPos, Vec2 endPos, int giftNo); //播放礼物动画
		void createAni(std::string fileName, std::string imgName, int imgCount, Node* playNode, bool isNt = false); //创建动画

	protected:
		PlayerUI();
		virtual ~PlayerUI();

	private:
		bool			_isBoy = true;
		UINT			_userId = INVALID_USER_ID;
		UINT			m_UserIP = INVALID_USER_ID;
		ui::Text*		_textName = nullptr;
		TextAtlas*		_textMoney = nullptr;
		TextAtlas*		_iCardCount = nullptr;
		ImageView*		_iOutTip = nullptr;
		ImageView*		_iOffLine = nullptr;
		ImageView*		_iAuto = nullptr;
		ImageView*		_iOwner = nullptr;
		ImageView*		_iBanker = nullptr;
		ImageView*		_iClock = nullptr;
		GameUserHead*	_userHead = nullptr;
		Sprite*			_iState = nullptr;
		ImageView*		_img_player = nullptr;
		int               _PlayerIndex = -1;
		cocos2d::Node*	m_pUerInfo; //玩家信息
		cocos2d::Node*	m_pUserLayer; //弹出的玩家信息
		ui::Text*		m_pName = nullptr;
		ui::Text*		m_pIP = nullptr;
		ui::Text*		m_pID = nullptr;
		LLONG			m_i64Money;							//??°ü?e±ò
	};
}



#endif // __DouDiZhu_GAME_PLAYER_H__
