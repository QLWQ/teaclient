/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZCalculateBoardSingle.h"
#include <string>
#include "HNLogicExport.h"
#include "HNLobbyExport.h"
#include "CaiShenDDZGameAudio.h"

using namespace CaiShenDDZ;

static const char* calculateBoardcsbFilePath = "CaiShenDDZ/calculateBoard/singleBoard/singleBoard.csb";
static const char* moreBeishuCsb = "CaiShenDDZ/calculateBoard/singleBoard/beishu.csb";
static const char* Player_Normal = "CaiShenDDZ/calculateBoard/singleBoard/res/xiaojiesuantouxiangchicun.png";
static const char* Player_Normal_M = "CaiShenDDZ/tableUI/res/men_head.png";
static const char* Player_Normal_W = "CaiShenDDZ/tableUI/res/women_head.png";

CalculateBoardSingle::CalculateBoardSingle()
{
	
}

CalculateBoardSingle::~CalculateBoardSingle()
{

}

void CalculateBoardSingle::close()
{
	_img_bg->runAction(Sequence::create(ScaleTo::create(0.1f, 0.3f), CallFunc::create([&]()
	{
		this->removeFromParent();
	}), nullptr));
}

bool CalculateBoardSingle::init()
{
	if (!HNLayer::init()) return false;

	auto node = CSLoader::createNode(calculateBoardcsbFilePath);
	node->setPosition(_winSize / 2);
	addChild(node, 2);

	auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_board"));

	_img_bg = dynamic_cast<ImageView*>(layout->getChildByName("Image_bg"));
	_img_bg->setScale(0);
	_img_bg->runAction(ScaleTo::create(0.2f, 1.0f));

	//确定按钮
	auto btn_confirm = (Button*)_img_bg->getChildByName("btn_confirm");
	if (btn_confirm)
	{
		btn_confirm->addTouchEventListener(CC_CALLBACK_2(CalculateBoardSingle::confirmBtnCallBack, this));
	}

	return true;
}

void CalculateBoardSingle::confirmBtnCallBack(Ref* pSender, Widget::TouchEventType type)
{
	if (Widget::TouchEventType::ENDED != type) return;

	if (onCloseCallBack) onCloseCallBack();	

	this->close();
}

//倍数
std::string CalculateBoardSingle::numToString(int imultiple)
{
	std::string str;
	if (imultiple>0)
	{
		str =  StringUtils::format("x%d", imultiple);
	}
	else
	{
		str =  "---";
	}	
	return str;
}

//倍数详情回调
void CalculateBoardSingle::moreBeishuBtnCallBack(Ref* pSender,T_S2C_RESULT_NOTIFY boardData)
{
	auto btnMore = (Button*)pSender;
	auto layer = Layer::create();
	addChild(layer, 3);
	auto infoNode = CSLoader::createNode(moreBeishuCsb);
	auto rootLayout = (Layout*)infoNode->getChildByName("Panel_1");
	//初始倍数
	auto textInitial = (Text*)rootLayout->getChildByName("Text_chushi");
	textInitial->setString(numToString(boardData.initialMultiple));
	//明牌倍数
	auto textMing = (Text*)rootLayout->getChildByName("Text_mingpai");
	textMing->setString(numToString(boardData.mingMultiple));
	//抢地主倍数
	auto textRob = (Text*)rootLayout->getChildByName("Text_qiangdizhu");
	textRob->setString(numToString(boardData.robMultiple));
	//底牌倍数
	auto textCard = (Text*)rootLayout->getChildByName("Text_dipai");
	textCard->setString(numToString(boardData.cardMultiple));
	//炸弹倍数
	auto textBomb = (Text*)rootLayout->getChildByName("Text_zhadan");
	textBomb->setString(numToString(boardData.bombMultiple));
	//春天倍数
	auto textSpring = (Text*)rootLayout->getChildByName("Text_chuntian");
	textSpring->setString(numToString(boardData.springMultiple));

	//公共倍数
	auto textPublic = (Text*)rootLayout->getChildByName("Text_gonggongbeishu");
	textPublic->setString(StringUtils::format("%d", boardData.publicMultiple));
	//地主倍数
	auto textLandlord = (Text*)rootLayout->getChildByName("Text_dizhujiabei");
	textLandlord->setString(numToString(boardData.landlordMultiple));
	UserInfoStruct myInfo = RoomLogic()->loginResult.pUserInfoStruct;
	//农民倍数
	auto textPeasantry = (Text*)rootLayout->getChildByName("Text_nongmingbeishu");
	textPeasantry->setString(numToString(boardData.peasantryMultiple[myInfo.bDeskStation]));
	//总倍数
	auto textTotal = (Text*)rootLayout->getChildByName("Text_zongbeishu");
	std::string totalStr("");
	if (boardData.totalMultiple[myInfo.bDeskStation] <= -10000 || boardData.totalMultiple[myInfo.bDeskStation] >= 10000)
	{
		//boardData.totalMultiple[myInfo.bDeskStation] = boardData.totalMultiple[myInfo.bDeskStation] / 10000;
		totalStr = StringUtils::format("%d", boardData.totalMultiple[myInfo.bDeskStation]/*, GBKToUtf8("万")*/);
	}
	else
	{
		totalStr = StringUtils::format("%d", boardData.totalMultiple[myInfo.bDeskStation]);
	}
	textTotal->setString(totalStr);
	layer->addChild(infoNode);
	Vec2 pos = layer->convertToNodeSpace(btnMore->getParent()->convertToWorldSpace(btnMore->getPosition()));
	infoNode->setPosition(pos);
	infoNode->setScale(0);
	auto spawn = Spawn::create(ScaleTo::create(0.2f, 1.0f), EaseIn::create(MoveTo::create(0.2f, Vec2(pos.x, pos.y + 20)), 1.5f), nullptr);
	infoNode->runAction(spawn);

	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);//阻止触摸向下传递
	listener->onTouchBegan = [=](Touch* touch, Event* event)
	{
		Rect rect = rootLayout->getBoundingBox();
		if (!rect.containsPoint(touch->getLocation()))//判断触摸点是否在目标的范围内
		{
			auto sequence = Sequence::create(
				Spawn::create(ScaleTo::create(0.2f, 0), EaseOut::create(MoveTo::create(0.2f, pos), 1.0f), nullptr),
				CCCallFunc::create([&]()
			{
				layer->removeFromParent();

			}), nullptr);
			infoNode->runAction(sequence);

			return true;
		}
		else
			return false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, layer);
}

void CalculateBoardSingle::showAndUpdateBoard(Node* parent, T_S2C_RESULT_NOTIFY boardData, string point, string muil)
{
	//显示结算
	parent->addChild(this, 7);

	UserInfoStruct myInfo = RoomLogic()->loginResult.pUserInfoStruct;

	auto img_title = dynamic_cast<ImageView*>(_img_bg->getChildByName("Image_title"));
	auto img_titleName = dynamic_cast<ImageView*>(img_title->getChildByName("Image_titleName"));
	auto img_role = dynamic_cast<ImageView*>(_img_bg->getChildByName("Image_role"));
	if (boardData.iMoney[myInfo.bDeskStation] > 0)
	{
		GameAudio::playWin();
		img_title->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/win_bg.png");
		img_titleName->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/you_win.png");
		if (myInfo.bDeskStation == boardData.iUpGradeStation)
		{
			img_role->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/lord_win.png");
		}
		else
		{
			img_role->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/peasant_win.png");
		}
		
	}
	else
	{
		GameAudio::playLose();
		img_title->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/lose_bg.png");
		img_titleName->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/you_lose.png");
		if (myInfo.bDeskStation == boardData.iUpGradeStation)
		{
			img_role->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/lord_lose.png");
		}
		else
		{
			img_role->loadTexture("CaiShenDDZ/calculateBoard/singleBoard/res/peasant_lose.png");
		}
	}



	auto layout_score = dynamic_cast<Layout*>(_img_bg->getChildByName("layout_score"));
	auto img_head = dynamic_cast<ImageView*>(layout_score->getChildByName("Image_head"));
	auto img_frame = dynamic_cast<ImageView*>(layout_score->getChildByName("Image_frame"));

	// 更新用户结算榜头像
	auto userHead = GameUserHead::create(img_head, img_frame);
	userHead->show(Player_Normal);
	userHead->loadTexture(myInfo.bBoy ? Player_Normal_M : Player_Normal_W);
	userHead->setHeadByFaceID(myInfo.bLogoID);
	userHead->loadTextureWithUrl(myInfo.headUrl);
	userHead->setVisible(false);


	//倍数详情
	auto btn_more = dynamic_cast<Button*>(layout_score->getChildByName("Button_more"));
	btn_more->addClickEventListener(CC_CALLBACK_1(CalculateBoardSingle::moreBeishuBtnCallBack, this,boardData));

	for (int i = 0; i < PLAY_COUNT; ++i)
	{	
		BYTE seatNo = logicToViewSeatNo(i);

		//更新玩家昵称
		auto txt_name = dynamic_cast<Text*>(layout_score->getChildByName(StringUtils::format("Text_name%d", seatNo)));
		txt_name->setString(GBKToUtf8("未知"));

		//更新倍数
		auto txt_beishu = dynamic_cast<Text*>(layout_score->getChildByName(StringUtils::format("Text_beishu%d", seatNo)));
		std::string mulStr("");
		if (boardData.totalMultiple[i] <= -10000 || boardData.totalMultiple[i] >= 10000)
		{
			//boardData.totalMultiple[i] = boardData.totalMultiple[i] / 10000;
			mulStr = StringUtils::format("%d", boardData.totalMultiple[i]);
		}
		else
		{
			mulStr = StringUtils::format("%d", boardData.totalMultiple[i]);
		}
		txt_beishu->setString(mulStr);

		//底分
		auto txt_difen = dynamic_cast<Text*>(layout_score->getChildByName(StringUtils::format("Text_difen%d", seatNo)));
		if(txt_difen)
		{
			txt_difen->setString(point);
		}

		//更新得分
		auto txt_allScore = dynamic_cast<Text*>(layout_score->getChildByName(StringUtils::format("Text_score%d", seatNo)));
		std::string scoreStr("");
		if (boardData.iMoney[i] <= -10000 || boardData.iMoney[i] >= 10000)
		{
			//boardData.iMoney[i] = boardData.iMoney[i] / 10000;
			scoreStr = StringUtils::format("%lld",boardData.iMoney[i]);
		}
		else
		{
			scoreStr = StringUtils::format("%lld",boardData.iMoney[i]);
		}
		txt_allScore->setString(scoreStr);

		auto userInfo = UserInfoModule()->findUser(myInfo.bDeskNO, i);
		if (!userInfo) continue;

		txt_name->setString(GBKToUtf8(userInfo->nickName));
		if (userInfo->iVipLevel > 0) txt_name->setColor(Color3B(255,0,0));
		char name[50] = { 0 };
		sprintf(name, "Sprite_Landlord%d", seatNo );
		Sprite* landName = dynamic_cast<Sprite*>(layout_score->getChildByName(name));
		if (landName != nullptr)
		{
			//更新庄家标志
			if (i == boardData.iUpGradeStation)
			{
				landName->setTexture("CaiShenDDZ/calculateBoard/singleBoard/res/lord_icon.png");
				landName->setVisible(true);
			}
			else
			{
				landName->setTexture(nullptr);
				landName->setVisible(false);
			}
		}
	}
}

BYTE CalculateBoardSingle::logicToViewSeatNo(BYTE lSeatNO)
{
	BYTE mySeatNo = RoomLogic()->loginResult.pUserInfoStruct.bDeskStation;

	return (lSeatNO - mySeatNo + PLAY_COUNT) % PLAY_COUNT;
}