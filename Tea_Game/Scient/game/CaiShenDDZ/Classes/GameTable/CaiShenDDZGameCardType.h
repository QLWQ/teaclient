/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef _CaiShenDDZ_GAME_CARD_TYPE_
#define _CaiShenDDZ_GAME_CARD_TYPE_

#include "cocos2d.h"
#include "HNNetExport.h"

USING_NS_CC;

namespace CaiShenDDZ
{
	class GameCardType : public HNLayer
	{
	public:
		GameCardType();
		~GameCardType();
	public:
		static GameCardType* create();
		virtual bool onTouchBegan(Touch *touch, Event *unused_event);
	private:
		bool init();
	};
}



#endif // !_GAME_CARD_TYPE_
