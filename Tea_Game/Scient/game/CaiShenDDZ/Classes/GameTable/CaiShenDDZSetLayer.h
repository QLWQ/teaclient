/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_GAMESET_LAYER_H__
#define __CaiShenDDZ_GAMESET_LAYER_H__

#include "HNLobbyExport.h"
#include "HNUIExport.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio;
using namespace ui;

namespace CaiShenDDZ
{

	class DouDiZhuSetLayer : public HNLayer
	{
	public:
		DouDiZhuSetLayer();
		virtual ~DouDiZhuSetLayer();

		typedef std::function<void()> ExitCallBack;
		ExitCallBack	onExitCallBack = nullptr;

		typedef std::function<void()> DisCallBack;
		DisCallBack		onDisCallBack = nullptr;

	public:
		virtual bool init() override;

		void showSet(Node* parent, int zorder, int tag = -1);

		void setRoomController(VipRoomController* roomController) { _roomController = roomController; }

		void close();

	private:
		// 拖动条回调函数
		void sliderCallback(Ref* pSender, Slider::EventType type);

		// 离开按钮
		void onExitClick(Ref* pRef);

		// 解散按钮
		void onDisClick(Ref* pRef);

	private:
		Slider* _effectSlider = nullptr;
		Slider* _musicSlider = nullptr;
		ImageView* _imgaeBg = nullptr;
		VipRoomController* _roomController = nullptr;

	public:
		CREATE_FUNC(DouDiZhuSetLayer);
	};
}

#endif // __DouDiZhu_GAMESET_LAYER_H__
