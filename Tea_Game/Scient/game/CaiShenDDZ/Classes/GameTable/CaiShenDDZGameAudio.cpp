/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZGameAudio.h"
#include "HNUIExport.h"
#include "cocos2d.h"

namespace CaiShenDDZ
{

	static const char* GAME_AUDIO_ROOT = "CaiShenDDZ/audio/effect/";

	void GameAudio::playBackground(BK_TYPE bkNum)
	{
		switch (bkNum)
		{
		case BK_TYPE::Welcome:
			HNAudioEngine::getInstance()->playBackgroundMusic("CaiShenDDZ/audio/music/MusicEx_Welcome.mp3", true);
			break;
		case BK_TYPE::Normal:
			HNAudioEngine::getInstance()->playBackgroundMusic("CaiShenDDZ/audio/music/MusicEx_Normal.mp3", true);
			break;
		case BK_TYPE::Fast:
		{
			if (Director::getInstance()->getRunningScene()->getActionByTag(67)) break;

			HNAudioEngine::getInstance()->playBackgroundMusic("CaiShenDDZ/audio/music/MusicEx_Normal2.mp3", false);
			auto action = Sequence::create(DelayTime::create(34.f), CallFunc::create([=]() {
				playBackground(Normal);
			}), nullptr);
			action->setTag(67);
			Director::getInstance()->getRunningScene()->runAction(action);
		} break;
		default:
			break;
		}
	}

	void GameAudio::stopBackground()
	{
		HNAudioEngine::getInstance()->stopBackgroundMusic();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void GameAudio::playCallNT(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append("CallLandlord_JDZ_M.mp3") : filename.append("CallLandlord_JDZ_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playNotCallNT(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append("CallLandlord_BJDZ_M.mp3") : filename.append("CallLandlord_BJDZ_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playCallSocreNT(bool isBoy, int score)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append(StringUtils::format("CallLandlord_Score%d_M.mp3", score)) : filename.append(StringUtils::format("CallLandlord_Score%d_W.mp3", score));
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playRobNT(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append("CallLandlord_QDZ_M.mp3") : filename.append("CallLandlord_QDZ_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playNotRobNT(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append("CallLandlord_BQDZ_M.mp3") : filename.append("CallLandlord_BQDZ_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playDouble(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append("CALL_DOUBLE_M.mp3") : filename.append("CALL_DOUBLE_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playNotDouble(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append("CALL_NO_DOUBLE_M.mp3") : filename.append("CALL_NO_DOUBLE_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playPublish(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		isBoy ? filename.append("CARDS_VISIBLE_M.mp3") : filename.append("CARDS_VISIBLE_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playNotOut(bool isBoy)
	{
		int value = rand() % 3 + 1;
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Pass");
		filename.append(std::to_string(value));
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void GameAudio::playSingle(const std::string &value, bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Single_");
		filename.append(value);
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameAudio::playDouble(const std::string &value, bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Double_");
		filename.append(value);
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void GameAudio::playThree(int value, bool isBoy)
	{
		char str[10] = {0};
		sprintf(str, "%d", value);
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Three_");
		filename.append(str);
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameAudio::playStraight(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Straight");
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameAudio::playDoubleSequence(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_DoubleLine");
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameAudio::playPlan(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Plane");
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameAudio::playFourTake(bool isDouble, bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		if(isDouble)
		{
			filename.append("Audio_Card_Four_Take_2Double");
		}
		else
		{
			filename.append("Audio_Card_Four_Take_2");
		}
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playRocket(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Rocket");
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());

		playBackground(Fast);
	}

	void GameAudio::playBomb(bool isBoy)
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Bomb");
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());

		playBackground(Fast);
	}

	void GameAudio::playCardLast(bool isBoy, int value)
	{
		char str[10] = {0};
		sprintf(str, "%d", value);
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Audio_Card_Last_");
		filename.append(str);
		isBoy ? filename.append("_M.mp3") : filename.append("_W.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameAudio::playPlanEffect()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("CardType_Aircraft.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playBombEffect()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("CardType_Bomb.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playDoubleSequenceEffect()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("CardType_Double_Line.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playRocketEffect()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("CardType_Missile.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playStraightEffect()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("CardType_One_Line.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playSpringEffect()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("CardType_One_Line.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void GameAudio::playWin()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("MusicEx_Win.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playLose()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("MusicEx_Lose.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

	void GameAudio::playDispathCard()
	{
		std::string filename(GAME_AUDIO_ROOT);
		filename.append("Special_Dispatch.mp3");
		HNAudioEngine::getInstance()->playEffect(filename.c_str());
	}

}
