/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef CaiShenDDZ_CalculateBoard_All_h__
#define CaiShenDDZ_CalculateBoard_All_h__

#include "HNUIExport.h"
#include "CaiShenDDZMessageHead.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

using namespace ui;
using namespace cocostudio;
using namespace HN;

namespace CaiShenDDZ
{
	class CalculateBoardAll : public HNLayer
	{	
	public:
		CREATE_FUNC(CalculateBoardAll);

		CalculateBoardAll();
		virtual ~CalculateBoardAll();

		virtual bool init() override;

		//显示结算数据
		void showAndUpdateBoard(Node* parent, T_S2C_TOTAL_RESULT boardData);

	private:

		//分享按钮回调
		void shareBtnCallBack(Ref* pSender, Widget::TouchEventType type);

		//返回按钮回调
		void backBtnCallBack(Ref* pSender, Widget::TouchEventType type);

	protected:
		ImageView* _img_bg = nullptr;

		bool	_isTouch = true;				// 按钮是否可以点击
	};
}


#endif // CaiShenDDZ_CalculateBoard_All_h__
