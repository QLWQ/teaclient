/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZGamePlayer.h"
#include "CaiShenDDZPokerCard.h"
#include "CaiShenDDZGameTableUI.h"
#include "CaiShenDDZGameTableUI.h"

#include "HNCommon/HNConverCode.h"
#include "HNUIExport.h"
#include "CaiShenDDZGameAudio.h"
#include "Tool/Tools.h"
namespace CaiShenDDZ
{
	const char* Player_Empty = "CaiShenDDZ/tableUI/res/touxiangchicun.png";
	//const char* Player_Normal_M = "CaiShenDDZ/tableUI/res/men_head.png";
	//const char* Player_Normal_W = "CaiShenDDZ/tableUI/res/women_head.png";
	const char* Player_Farmer_W = "CaiShenDDZ/tableUI/res/room_farmer_f.png";
	const char* Player_Farmer_M = "CaiShenDDZ/tableUI/res/room_farmer_m.png";
	const char* Player_Lord_W = "CaiShenDDZ/tableUI/res/room_lord_f.png";
	const char* Player_Lord_M = "CaiShenDDZ/tableUI/res/room_lord_m.png";

	//////////////////////////////////////////////////////////////////////////
	const char* File_Win_Num = "CaiShenDDZ/game/fonts/win_number.fnt";
	const char* File_Lose_Num = "CaiShenDDZ/game/fonts/lose_number.fnt";

	const static int giftImgNum[6] = { 19, 28, 21, 29, 19, 28 };

	//////////////////////////////////////////////////////////////////////////
	PlayerUI* PlayerUI::create(ImageView* user, int index)
	{
		PlayerUI* player = new PlayerUI();
		if (player->init(user, index))
		{
			player->autorelease();
			return player;
		}
		CC_SAFE_DELETE(player);
		return nullptr;
	}

	PlayerUI::PlayerUI()
	{
		
	}

	PlayerUI::~PlayerUI()
	{

	}

	bool PlayerUI::init(ImageView* user,int index)
	{
		if (!HNLayer::init() || !user) return false;

		_img_player = user;
		_PlayerIndex = index;
		auto img_Head = dynamic_cast<ImageView*>(user->getChildByName("Image_head"));
		img_Head->setScale(0.9);
		auto img_Frame = dynamic_cast<ImageView*>(user->getChildByName("Image_frame"));
		img_Frame->setVisible(false);

		_userHead = GameUserHead::create(img_Head, img_Frame);
		_userHead->show(Player_Empty);
		_userHead->addClickEventListener([=](Ref*) {
			if (userMessageCallBack)
			{
				userMessageCallBack();
			}
		});
		
		img_Head->setLocalZOrder(10);
		img_Frame->setLocalZOrder(10);
		_userHead->setLocalZOrder(800);
		_textName = dynamic_cast<Text*>(user->getChildByName("Text_name"));
		
		_textMoney = dynamic_cast<TextAtlas*>(user->getChildByName("AtlasLabel_money"));
		_textMoney->setPosition(Vec2(_textMoney->getPositionX(), _textMoney->getPositionY() - 2));
		_iCardCount = dynamic_cast<TextAtlas*>(user->getChildByName("AtlasLabel_count"));
		_iCardCount->setVisible(false);
		_iCardCount->setLocalZOrder(10);
		_iOffLine = dynamic_cast<ImageView*>(user->getChildByName("Image_offline"));
		_iOffLine->setLocalZOrder(3);
		_iOffLine->setVisible(false);
		_iAuto = dynamic_cast<ImageView*>(user->getChildByName("Image_auto"));
		_iAuto->setLocalZOrder(3);
		_iAuto->setVisible(false);
		_iOwner = dynamic_cast<ImageView*>(user->getChildByName("Image_owner"));
		_iOwner->setLocalZOrder(4);
		_iOwner->setVisible(false);
		_iBanker = dynamic_cast<ImageView*>(user->getChildByName("Image_banker"));
		_iBanker->setVisible(false);
		_iBanker->setLocalZOrder(4);
		_iClock = dynamic_cast<ImageView*>(user->getChildByName("Image_clock"));
		_iClock->setLocalZOrder(2);
		_iClock->setVisible(false);

		_iState = dynamic_cast<Sprite*>(user->getChildByName("Sprite_state"));
		Vec2 pos = user->convertToWorldSpace(_iState->getPosition());
		pos = _img_player->getParent()->getParent()->convertToNodeSpace(pos);
		_iState->removeFromParentAndCleanup(false);
		_img_player->getParent()->getParent()->addChild(_iState, 2);
		_iState->setPosition(pos);
		_iState->setScale(1280 / _winSize.width, 720 / _winSize.height);

		return true;
	}

	void PlayerUI::loadUser(UserInfoStruct* info)
	{
		setUserId(info->dwUserID);
		//setUserIP(info->dwUserIP); //gjd_6.4
		setHead(info->bLogoID);
		setUserSex(info->bBoy);
		Tools::TrimSpace(info->nickName);
		string strName = info->nickName;
		if (strName.length() > 12)
		{
			strName.erase(13, strName.length());
			strName.append("...");
		}
		setUserName(GBKToUtf8(strName));
		
		m_i64Money = info->i64Money;
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)//6.29
		{
			setUserMoney(0);
		}
		else
		{
			setUserMoney(info->i64Money);
		}
		setUserCardCount(0);
		//setAutoHead(false);
		setOwner(info->dwUserID == HNPlatformConfig()->getMasterID());
		//setBanker(false);
		setOffline(info->bUserState == USER_CUT_GAME);
		setVisible(true);
	}

	void PlayerUI::setVisible(bool visible)
	{
		if (_img_player)
		{
			_img_player->setVisible(visible);

			if (!visible)
			{
				_iState->setVisible(visible);
				_iClock->setVisible(visible);
			}
		}
	}

	void PlayerUI::setUserSex(bool isBoy)
	{
		_isBoy = isBoy;
	}

	void PlayerUI::setUserId(INT userId)
	{
		_userId = userId;
	}
	LLONG PlayerUI::getUserMoney()
	{
		return m_i64Money;
	}

	void PlayerUI::setUserName(const std::string& name)
	{
		CCAssert(nullptr != _textName, "nullptr == _textName");
		if (nullptr == _textName) return;
		_textName->setString(name);
	}

	void PlayerUI::setUserMoney(LLONG money)
	{
		ffmoney = money;

		CCAssert(nullptr != _textMoney, "nullptr == _textMoney");
		if (nullptr == _textMoney) return;
		std::string moneyStr("");
		LLONG a = money;
		LLONG b = a;
// 		if (money <= -10000)
// 		{
// 			money = money / 10000;
// 			moneyStr = StringUtils::format("%lld", money);
// 		}
// 		else if (money >-10000 && money < 0)
// 		{
// 			moneyStr = StringUtils::format("%lld", money);
// 		}
// 		else if (money >= 0 && money < 10000 )
// 		{
// 			moneyStr = StringUtils::format("%lld", money);
// 		}
// 		else
// 		{
// 			money = money / 10000;
// 			
// 		}
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)//6.29
		{
			if (money >= 0)
			{
				moneyStr = StringUtils::format("%lld", money);
			}
			else
			{
				moneyStr = StringUtils::format("/%lld", money);
			}
			_textMoney->setString(moneyStr.c_str());
		}
		else
		{
			if (money >= 0)
			{
				moneyStr = StringUtils::format("%lld", money);
			}
			else
			{
				moneyStr = StringUtils::format("/%lld", money);
			}
			moneyStr = HNToWanDian(money);
			_textMoney->setString(moneyStr.c_str());
			_textMoney->setScale(0.5);
		}
	}

	void PlayerUI::setUserCardCount(int count)
	{
		CCAssert(nullptr != _iCardCount, "nullptr == _iCardCount");
		if (nullptr == _iCardCount) return;

		_iCardCount->setVisible(count > 0);

		if (count >= 0)
		{
			_iCardCount->setString(to_string(count));
			_iCardCount->setLocalZOrder(200);
		}
	}

	void PlayerUI::setAutoHead(bool isAuto)
	{
		CCAssert(nullptr != _iAuto, "nullptr == _iAuto");
		if (nullptr == _iAuto) return;

		_iAuto->setVisible(isAuto);
	}

	void PlayerUI::setHead(int FaceID)
	{
		CCAssert(nullptr != _userHead, "nullptr == _userHead");
		if (nullptr == _userHead) return;
		srand(time(nullptr) / (_PlayerIndex+1));
		//int ranNumber = rand() % 1924;
		//if (ranNumber == 0) ranNumber = 1;
		if (FaceID == 0) FaceID = 1;
		std::string name = StringUtils::format("platform/common/Head/Head%d.jpg", FaceID);//isBoy ? Player_Normal_M : Player_Normal_W;
		bool isExist = FileUtils::getInstance()->isFileExist(name);
		if (isExist)
			_userHead->loadTexture(name);
		else
		{
			GamePromptLayer::create()->showPrompt(StringUtils::format("%d", FaceID));
		}
		auto userInfo = UserInfoModule()->findUser(_userId);
		if (userInfo) _userHead->loadTextureWithUrl(userInfo->headUrl);
		if (userInfo->iVipLevel > 0)_textName->setColor(Color3B(255, 0, 0));
		_userHead->setVIPHead("", userInfo->iVipLevel);
		_userHead->setTag(_PlayerIndex);
		//string strIP = Tools::parseIPAdddress(userInfo->dwUserIP);
		////auto BottonLabel = Label::createWithSystemFont("", "Arial", 50);
		//auto BottonLabel = CCLabelAtlas::create("", "CaiShenDDZ/tableUI/res/difenshuzi.png", 32, 41, '.');
		//BottonLabel->setPosition(Vec2(100, 100));
		//BottonLabel->setColor(Color3B(255, 255, 255));
		//BottonLabel->setName(strIP);
		//_userHead->getParent()->addChild(BottonLabel,9999999999);
		
		//_userHead->addTouchEventListener(CC_CALLBACK_2(PlayerUI::getUserInfo, this)); // 6.3
	}

	void PlayerUI::getUserInfo(Ref* pSender, Widget::TouchEventType type){
		//斗地主显示IP6.4
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		int curUser = _PlayerIndex;
		auto userInfo = UserInfoModule()->findUser(_userId);
		string strIP = Tools::parseIPAdddress(userInfo->dwUserIP);
		string strName = userInfo->nickName;
		string strID = StringUtils::toString(userInfo->dwUserID);
		//个人信息界面
		cocos2d::Size winSize = Director::getInstance()->getWinSize();
		m_pUerInfo = CSLoader::createNode("platform/Chat/UserInfoNode.csb");
		m_pUerInfo->setPosition(cocos2d::Vec2(winSize.width / 2.0f, winSize.height / 2.0f));
		this->addChild(m_pUerInfo, 999);

		Button* ting = (Button*)m_pUerInfo->getChildByName("btn_close");
		ting->addTouchEventListener(CC_CALLBACK_2(PlayerUI::closeLayer, this));
		m_pName = (Text*)m_pUerInfo->getChildByName("userName_0");
		m_pIP = (Text*)m_pUerInfo->getChildByName("userIP_0");
		m_pID = (Text*)m_pUerInfo->getChildByName("userID_0");

		auto img_head = dynamic_cast<ImageView*>(m_pUerInfo->getChildByName("userHead"));
		//auto img_frame = dynamic_cast<ImageView*>(m_pUerInfo->getChildByName("userFrame"));
		string strFrame = "platform/Chat/Chat/chatRes/userbg.jpg";

		// 更新用户结算榜头像
		auto userHead = GameUserHead::create(img_head);
		userHead->show(strFrame);
		userHead->loadTexture(userInfo->bBoy ? Player_Normal_M : Player_Normal_W);
		if (userInfo->headUrl != ""){
			userHead->loadTextureWithUrl(userInfo->headUrl);
		}

		m_pName->setString(GBKToUtf8(strName));
		m_pIP->setString(strIP);
		m_pID->setString((strID));
	}

	void PlayerUI::closeLayer(Ref* pSender, Widget::TouchEventType type)
	{
		m_pUerInfo->removeFromParent();
	}

	void PlayerUI::setGameHead(bool isBoy, bool isLord)
	{
		return; //屏蔽这个方法

		CCAssert(nullptr != _userHead, "nullptr == _userHead");
		if (nullptr == _userHead) return;

		std::string filename = isLord ? (isBoy ? Player_Lord_M : Player_Lord_W) : (isBoy ? Player_Farmer_M : Player_Farmer_W);
		_userHead->loadTexture(filename);
	}

	void PlayerUI::setEmptyHead()
	{
		CCAssert(nullptr != _userHead, "nullptr == _userHead");
		if (nullptr == _userHead) return;

		_userHead->loadTexture(Player_Empty);
	}

	void PlayerUI::setBanker(bool isBanker)
	{
		CCAssert(nullptr != _iBanker, "nullptr == _iBanker");
		if (nullptr == _iBanker) return;

		_iBanker->setVisible(isBanker);
	}

	void PlayerUI::setOwner(bool isOwner)
	{
		CCAssert(nullptr != _iOwner, "nullptr == _iOwner");
		if (nullptr == _iOwner) return;

		_iOwner->setVisible(isOwner);
	}

	void PlayerUI::setOffline(bool isOffline)
	{
		CCAssert(nullptr != _iOffLine, "nullptr == _iOffLine");
		if (nullptr == _iOffLine) return;

		_iOffLine->setVisible(isOffline);
	}

	void PlayerUI::setUserOperationHints(Hint_Type type/* = NONE*/)
	{
		CCAssert(nullptr != _iState, "nullptr == _iState");
		if (nullptr == _iState) return;

		_iState->setVisible(type != STATE_HIDE);

		if (type == STATE_HIDE) return;

		_iState->setTexture(StringUtils::format("CaiShenDDZ/tableUI/res/userState/hint_Type%d.png", type));

		switch (type)
		{
		case CaiShenDDZ::STATE_READY:
			break;
		case CaiShenDDZ::STATE_CALL_LORD:
			GameAudio::playCallNT(_isBoy);
			break;
		case CaiShenDDZ::STATE_CALL_SCORE1:
			GameAudio::playCallSocreNT(_isBoy,1);
			break;
		case CaiShenDDZ::STATE_CALL_SCORE2:
			GameAudio::playCallSocreNT(_isBoy, 2);
			break;
		case CaiShenDDZ::STATE_CALL_SCORE3:
			GameAudio::playCallSocreNT(_isBoy, 3);
			break;
		case CaiShenDDZ::STATE_NO_CALL_LORD:
			GameAudio::playNotCallNT(_isBoy);
			break;
		case CaiShenDDZ::STATE_ROB_LORD:
			GameAudio::playRobNT(_isBoy);
			break;
		case CaiShenDDZ::STATE_NO_ROB_LORD:
			GameAudio::playNotRobNT(_isBoy);
			break;
		case CaiShenDDZ::STATE_DOUBLE:
			GameAudio::playDouble(_isBoy);
			break;
		case CaiShenDDZ::STATE_NO_DOUBLE:
			GameAudio::playNotDouble(_isBoy);
			break;
		case CaiShenDDZ::STATE_SHOW_CARD:
			GameAudio::playPublish(_isBoy);
			break;
		case CaiShenDDZ::STATE_NO_OUTCARD:
			GameAudio::playNotOut(_isBoy);
			break;
		case CaiShenDDZ::STATE_HIDE:
			break;
		default:
			break;
		}
	}

	void PlayerUI::showTimer(int delayTime, std::function<void()> func)
	{
		auto text_timer = dynamic_cast<TextAtlas*>(_iClock->getChildByName("AtlasLabel_time"));

		int time = --delayTime;
		if (time <= 0)
		{
			text_timer->stopAllActions();
			_iClock->setVisible(false);
			if (func) func();
			return;
		}

		_iClock->setVisible(true);

		text_timer->setString(to_string(time));
		text_timer->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=]() {

			showTimer(time, func);
		}), nullptr));
	}

	void PlayerUI::showTimer(int delayTime, bool visible)
	{
		auto text_timer = dynamic_cast<TextAtlas*>(_iClock->getChildByName("AtlasLabel_time"));

		int time = --delayTime;
		text_timer->stopAllActions();
		_iClock->setVisible(false);
		if (!visible || time <= 0)
			return;
		_iClock->setVisible(true);

		text_timer->setString(to_string(time));
		text_timer->runAction(Sequence::create(DelayTime::create(1.0f), CallFunc::create([=]() {

			showTimer(time, time > 0);
		}), nullptr));
	}

	Vec2 PlayerUI::getHeadPosition()
	{
		auto img_Frame = dynamic_cast<ImageView*>(_img_player->getChildByName("Image_frame"));

		if (img_Frame)
		{
			return img_Frame->getParent()->convertToWorldSpace(img_Frame->getPosition());
		}

		return Vec2::ZERO;
	}

	Size PlayerUI::getHeadSize()
	{
		auto img_Frame = dynamic_cast<ImageView*>(_img_player->getChildByName("Image_frame"));

		if (img_Frame)
		{
			return img_Frame->getContentSize();
		}

		return Size::ZERO;
	}

	Node* PlayerUI::getChatParent()
	{
		auto img_Frame = dynamic_cast<ImageView*>(_img_player->getChildByName("Image_frame"));

		return img_Frame;
	}

	void PlayerUI::playGiftAni(Vec2 startPos, Vec2 endPos, int giftNo)
	{
		if (giftNo < 0 || giftNo >= 6)
		{
			return;
		}

		auto giftSp = Sprite::create();
		////
		_img_player->getParent()->getParent()->addChild(giftSp, 10);
		giftSp->setTexture(StringUtils::format("platform/userMessage/res/item_img_%d.png", giftNo));
		giftSp->setPosition(startPos);
		giftSp->runAction(Sequence::create(MoveTo::create(0.5f, endPos), CallFunc::create([=](){
			//播放动画
			giftSp->setVisible(false);
			std::string strFile = StringUtils::format("giftAni_%d", giftNo);
			std::string strImg = StringUtils::format("item_%d", giftNo);
			createAni(strFile, strImg, giftImgNum[giftNo], giftSp);
		}), DelayTime::create((giftNo == 0) ? 0 : 0.5f), CallFunc::create([=](){
			std::string str = StringUtils::format("platform/userMessage/sound/item%d.mp3", giftNo);
			HNAudioEngine::getInstance()->playEffect(str.c_str());
		}), DelayTime::create(3.0f), RemoveSelf::create(), nullptr));
	}

	void PlayerUI::createAni(std::string fileName, std::string imgName, int imgCount, Node* playNode, bool isNt)
	{
		std::string fileStr = StringUtils::format("platform/userMessage/animations/%s.plist", fileName.c_str());
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile(fileStr);
		Vector<SpriteFrame *> frameVec;
		std::string str;
		for (int i = 0; i < imgCount; i++)
		{
			str = StringUtils::format("%s_%d.png", imgName.c_str(), i);
			SpriteFrame * frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(str);
			frameVec.pushBack(frame);
		}

		Animation * ani = Animation::createWithSpriteFrames(frameVec, 0.1f, 1);
		Animate * ac = Animate::create(ani);

		auto actionSp = Sprite::create();
		SpriteFrame* frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(StringUtils::format("%s_0.png", imgName.c_str()));
		actionSp->setSpriteFrame(frame);
		actionSp->setPosition(playNode->getPosition());
		playNode->getParent()->addChild(actionSp, 4);
		actionSp->runAction(Sequence::create(ac, CallFunc::create([=](){
			if (playNode)
			{
				playNode->setVisible(isNt);
			}
		}), DelayTime::create(0.5f), RemoveSelf::create(), nullptr));
	}

	void PlayerUI::setCardCountVis(bool visible)
	{
		_iCardCount->setVisible(visible);
	}

}