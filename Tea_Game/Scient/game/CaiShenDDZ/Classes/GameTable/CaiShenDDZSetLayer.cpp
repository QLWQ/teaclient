/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZSetLayer.h"

//static const char* SETTING_PATH = "CaiShenDDZ/tableUI/Node_setting.csb";
static const char* SETTING_PATH = "platform/setting/roomSettingNode.csb";
namespace CaiShenDDZ
{
	DouDiZhuSetLayer::DouDiZhuSetLayer()
	{

	}

	DouDiZhuSetLayer::~DouDiZhuSetLayer()
	{

	}

	void DouDiZhuSetLayer::showSet(Node* parent, int zorder, int tag)
	{
		CCAssert(nullptr != parent, "parent is nullptr");
		if (0 != tag) {
			parent->addChild(this, zorder, tag);
		}
		else {
			parent->addChild(this, zorder);
		}
	/*	if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			bool isFangZhu = HNPlatformConfig()->getMasterID() == PlatformLogic()->loginResult.dwUserID;
			bool isGameStart = _roomController->isVipRoomGameStarted();
			auto btn_dis = (Button*)_imgaeBg->getChildByName("Button_dismiss");
			auto btn_exit = (Button*)_imgaeBg->getChildByName("Button_return");
			Size s = _imgaeBg->getContentSize();
			if (isGameStart)
			{
				btn_exit->setVisible(false);
				btn_dis->setPositionX(s.width / 2);
			}
			else
			{
				if (!isFangZhu)
				{
					btn_dis->setVisible(false);
					btn_exit->setPositionX(s.width / 2);
				}

			}
		}*/
		setPosition(Director::getInstance()->getWinSize() / 2);
	}

	void DouDiZhuSetLayer::close()
	{
		this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&]() {

			UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, _effectSlider->getPercent());
			UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, _musicSlider->getPercent());
			UserDefault::getInstance()->flush();

			this->removeFromParent();
		}), nullptr));
	}

	bool DouDiZhuSetLayer::init()
	{
		if (!HNLayer::init()) {
			return false;
		}

		//auto node = CSLoader::createNode(SETTING_PATH);
		//CCAssert(node != nullptr, "null");
		//node->setPosition(Vec2::ZERO);
		//addChild(node);
		//auto layout_bg = dynamic_cast<Layout*>(node->getChildByName("Panel_setting"));
		//layout_bg->setScale(_winSize.width / 1280, _winSize.height / 720);
		//auto img_bg = dynamic_cast<ImageView*>(layout_bg->getChildByName("Image_bg"));
		//_imgaeBg = img_bg;
		//img_bg->setScale(1280 / _winSize.width, 720 / _winSize.height);
		//// 关闭
		//auto btn_guanBi = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
		//btn_guanBi->addClickEventListener([=](Ref*) {
		//	close();
		//});
		////音效滑动条
		//_effectSlider = (Slider*)img_bg->getChildByName("Slider_effect");
		//_effectSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT));
		//_effectSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(DouDiZhuSetLayer::sliderCallback, this)));
		////音乐滑动条
		//_musicSlider = (Slider*)img_bg->getChildByName("Slider_music");
		//_musicSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT));
		//_musicSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(DouDiZhuSetLayer::sliderCallback, this)));
		//// 解散房间
		//auto btn_dis = (Button*)img_bg->getChildByName("Button_dismiss");
		//btn_dis->addClickEventListener(CC_CALLBACK_1(DouDiZhuSetLayer::onDisClick, this));
		//btn_dis->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);
		//// 离开游戏
		//auto btn_exit = (Button*)img_bg->getChildByName("Button_return");
		//btn_exit->addClickEventListener(CC_CALLBACK_1(DouDiZhuSetLayer::onExitClick, this));
		//if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		//{
		//	btn_exit->setPositionX(img_bg->getContentSize().width / 2);
		//}


		auto node = CSLoader::createNode(SETTING_PATH);
		CCAssert(node != nullptr, "null");
		node->setPosition(Vec2::ZERO);
		addChild(node);

		auto layout_bg = dynamic_cast<Layout*>(node->getChildByName("Panel_setting"));
		layout_bg->setScale(_winSize.width / 1280, _winSize.height / 720);

		auto img_bg = dynamic_cast<ImageView*>(layout_bg->getChildByName("Image_bg"));
		img_bg->setScale(1280 / _winSize.width, 720 / _winSize.height);

		// 关闭
		auto btn_guanBi = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
		btn_guanBi->addClickEventListener([=](Ref*) {
			close();
		});

		//音效滑动条
		_effectSlider = (Slider*)img_bg->getChildByName("Slider_effect");
		_effectSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT));
		_effectSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(DouDiZhuSetLayer::sliderCallback, this)));

		//音乐滑动条
		_musicSlider = (Slider*)img_bg->getChildByName("Slider_music");
		_musicSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT));
		_musicSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(DouDiZhuSetLayer::sliderCallback, this)));

		// 解散房间
		auto btn_dis = (Button*)img_bg->getChildByName("Button_dismiss");
		btn_dis->addClickEventListener(CC_CALLBACK_1(DouDiZhuSetLayer::onDisClick, this));
		btn_dis->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);

		// 离开游戏
		auto btn_exit = (Button*)img_bg->getChildByName("Button_return");
		btn_exit->addClickEventListener(CC_CALLBACK_1(DouDiZhuSetLayer::onExitClick, this));
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			btn_exit->setPositionX(img_bg->getContentSize().width / 2);
		}

		return true;
		
	}

	void DouDiZhuSetLayer::onExitClick(Ref* pRef)
	{
		if (nullptr != onExitCallBack) {
			onExitCallBack();
		}
	}

	// 切换账号
	void DouDiZhuSetLayer::onDisClick(Ref* pRef)
	{
		if (nullptr != onDisCallBack) {
			onDisCallBack();
		}
	}

	void DouDiZhuSetLayer::sliderCallback(Ref* pSender, Slider::EventType type)
	{
		Slider* pSlider = static_cast<Slider*>(pSender);
		auto name = pSlider->getName();

		float l = pSlider->getPercent() / 100.f;

		if (name.compare("Slider_music") == 0)
		{
			if (pSlider->getPercent() == 0)
			{
				HNAudioEngine::getInstance()->setSwitchOfMusic(false);
				HNAudioEngine::getInstance()->pauseBackgroundMusic();
			}
			else if (!HNAudioEngine::getInstance()->getSwitchOfMusic())
			{
				HNAudioEngine::getInstance()->setSwitchOfMusic(true);
				HNAudioEngine::getInstance()->resumeBackgroundMusic();
			}
			else
			{

			}
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(l);
		}

		if (name.compare("Slider_effect") == 0)
		{
			if (pSlider->getPercent() == 0)
			{
				HNAudioEngine::getInstance()->setSwitcjofEffect(false);
			}
			else
			{
				HNAudioEngine::getInstance()->setSwitcjofEffect(true);
			}
			HNAudioEngine::getInstance()->setEffectsVolume(l);
		}
	}
}