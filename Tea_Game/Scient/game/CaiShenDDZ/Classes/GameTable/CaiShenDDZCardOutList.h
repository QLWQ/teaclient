/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_CARD_OUT_LIST_H__
#define __CaiShenDDZ_CARD_OUT_LIST_H__

#include "HNNetExport.h"
#include "CaiShenDDZMessageHead.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

namespace CaiShenDDZ
{
	class GameTableUI;

	//////////////////////////////////////////////////////////////////////////
	class CardOutList: public HN::HNLayer
	{
	public:
		static CardOutList* create();
		virtual bool init();
		void outCard(const std::vector<BYTE> &Actualcards, const std::vector<BYTE> &cards, BYTE tianLaiziCard, BYTE diLaiziCard);
	protected:
		CardOutList();
		virtual ~CardOutList();
	};

}

#endif