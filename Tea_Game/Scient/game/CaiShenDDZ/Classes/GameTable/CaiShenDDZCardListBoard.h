/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_Action_Board_H__
#define __CaiShenDDZ_Action_Board_H__

#include "CaiShenDDZPokerCard.h"

#include "HNNetExport.h"

namespace CaiShenDDZ
{
	class CardListBoard : public HN::HNLayer
	{
	public:
		static CardListBoard* create(bool isMySelf, BYTE seatNo, ImageView* iCard);
		virtual bool init(bool isMySelf, BYTE seatNo, ImageView* iCard);
		//add new.
		void addCardOneByOne(const std::vector<BYTE>& values, bool isFront, bool isNoShffule, BYTE tianLaiziCard, BYTE diLaiziCard);
		void addCardAllOnce(const std::vector<BYTE>& values, bool isUp, bool isFront, BYTE tianLaiziCard, BYTE diLaiziCard);
		void removeCardAllOnce(const std::vector<BYTE>& values);

		//sort card value, record in card list.
		void setCallFunction(std::function<void(cocos2d::Node*)> callfunc);
		void getUpCards(std::vector<BYTE>* upCards);
		void getTouchedCards(std::vector<BYTE>* outCards);
		BYTE getSeatNo() const { return _seatNo;}
		void clear();
		void downCards();
		void upCards(const std::vector<BYTE>& cards);
		int getSendCardNum();                //获取已经下发的牌张数，明牌加倍时用

	protected:
		CardListBoard();
		virtual ~CardListBoard();

		//add new.
		void addCard(BYTE value, bool isUp, BYTE tianLaiziCard, BYTE diLaiziCard);
		void removeCard(BYTE value);
		void resizeCardList();
		bool touchCheck(const Vec2& pos, bool isAhead = true);
		void reorderCard();
		void removeTouchedTag();
		void upCard(PokerCard* card);
		void downCard(PokerCard* card, float delay);
		void changeUpDown();
		bool onTouchBegan(Touch* touch, Event *event);
		void onTouchMoved(Touch* touch, Event *event);
		void onTouchEnded(Touch* touch, Event *event);
		void scheduleRun(float delta);

	private:
		//未处理列表
		std::vector<BYTE> _undoList;
		std::vector<PokerCard*> _cardsPtr;
		bool _isMySelf = false;
		BYTE _seatNo = INVALID_DESKSTATION;
		ImageView* _backCard = nullptr;
		std::function<void (cocos2d::Node*)> _callfunc;
		Vec2	_startMovePoint;
		Vec2	_midMovePoint;
		Vec2	_currentMovePoint;
		BYTE    _tianLaiziCardValue = 0;
		BYTE    _diLaiziCardValue = 0;
		int		_sendCount = 0;	//发牌次数
		bool	_isNoShuffle = false;	//是否不洗牌
	};
}




#endif