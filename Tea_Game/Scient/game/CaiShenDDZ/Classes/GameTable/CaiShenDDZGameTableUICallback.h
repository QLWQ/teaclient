/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_Game_Table_UI_Callback_H__
#define __CaiShenDDZ_Game_Table_UI_Callback_H__

#include "HNNetExport.h"
#include "HNLogicExport.h"
#include "CaiShenDDZMessageHead.h"


namespace CaiShenDDZ
{
	/*
 * game ui callback
 */

class GameTableUICallback : public HN::IHNGameLogicBase
{
public:
	virtual void addUser(BYTE seatNo) = 0;																	//添加玩家
	virtual void removeUser(BYTE seatNo) = 0;																//移除玩家
	virtual void setUserName(BYTE seatNo, const std::string& name) = 0;										//显示玩家昵称
	virtual void setUserMoney(BYTE seatNo, LLONG money) = 0;												//显示玩家金币	
	virtual void setUserAuto(BYTE seatNo, bool isAuto) = 0;													//显示玩家托管标志
	virtual void setUserHead(BYTE seatNo) = 0;																//显示玩家头像
	virtual void showLandlord(BYTE seatNo, bool isLord) = 0;												//显示地主标志
	virtual void showUserCut(BYTE seatNo, bool bShow) = 0;													//显示玩家掉线
	virtual void setUserScore(LLONG userScore[PLAY_COUNT]) = 0;
	virtual void showUserHandCard(BYTE seatNo, const std::vector<BYTE>& values, BYTE tianLaiziCard, BYTE diLaiziCard, bool isUp, bool isOnce, bool isFront) = 0;//显示玩家手牌
	virtual void showUserHandCardCount(BYTE seatNo, INT count) = 0;											//显示玩家手牌张数
	virtual void showUserHandCardOut(BYTE seatNo, const std::vector<BYTE> &Actualcards, const std::vector<BYTE> &cards) = 0;	//显示玩家出牌
	virtual void showUserOutCardType(BYTE logicSeatNo, BYTE cardValue, BYTE type) = 0;											//显示玩家出牌牌型

	virtual void showLandlordCard(const std::vector<BYTE>& cards) = 0;										//显示地主牌
	virtual void showBackCardTypeAndMutiple(bool isShow, BYTE cardType = 255, BYTE cardMutiple = 255) = 0;	//显示地主牌信息
	virtual void setBasePoint(int point) = 0;																//显示底分
	virtual void setMultiple(int multiple, bool isAll = false) = 0;											//显示倍数

	virtual void setCardCountVis(BYTE seatNo, bool visible) = 0;											//明牌是否显示牌数

	virtual void setUpCardList(BYTE seatNo, const std::vector<BYTE>& upCards) = 0;							//设置对应的手牌弹出
	virtual void getUpCardList(BYTE seatNo, std::vector<BYTE>* upCards) = 0;								//获取弹出的手牌
	virtual void downUserCards(BYTE seatNo) = 0;															//收回弹起的手牌

	virtual void showActionButton(BYTE flag, bool firstOut = false,bool needShowTimer = true) = 0;									//显示对应操作按钮
	virtual void showUserOperationHints(BYTE seatNo, Hint_Type type = STATE_HIDE) = 0;						//显示玩家操作提示
	virtual void showNotice(const std::string &message) = 0;												//显示公告信息
	virtual void showVipEnterTip(BYTE seatNo) = 0;                               //显示VIP进入提示   
	virtual void showMingPaiMulImg(int mul) = 0;
	virtual void setAutoAgree(int delayTime) = 0;															//自动准备
	virtual void showTimer(BYTE seatNo, int delayTime, bool visible) = 0;									//启动玩家定时器

	virtual void setAutoBtnVisible(bool visible) = 0;														//托管按钮是否显示

	virtual void showGameTips(Tips_Type type) = 0;
	//显示癞子组合牌型选择界面
	virtual	void showLaiziCardType(T_S2C_SELECT_RESULT_RES data) = 0;
	virtual	void showLaiziCardTypeLayer(bool isShow) = 0;
	/*
	 * something about desk.
	 */
	virtual void clearDesk() = 0;
	virtual void leaveDesk(bool isForceQuit) = 0;

	//显示小结结算榜
	virtual void showSingleCalculateBoard(T_S2C_RESULT_NOTIFY endData) = 0;
	virtual void showCalculateBoard(T_S2C_TOTAL_RESULT boardData) = 0;						//显示结算榜

	// 显示房主标志
	virtual void showMaster(BYTE viewSeatNo, bool bShow) = 0;
	virtual void onChatTextMsg(BYTE seatNo, std::string msg) = 0;									// 文本聊天消息
	virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime) = 0;						// 语音聊天消息

	virtual void onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index) = 0;	//处理玩家之间的动作特效

	//扣除玩家发送特效金币
	virtual void afterScoreByDeskStation(int Station, int money) = 0;

	//设置癞子牌是否显示
	virtual void setLaiziCard(bool isShow, BYTE cardValue,bool bTianLaizi = true) = 0;

	//更新玩家的经纬度
	virtual void UpDateUserLocation(int Index) = 0;

	//房间说明
	virtual void showGameRule(int palyMode) = 0;
	//记牌器
	virtual void setRecordEnable(bool visible) = 0;
	virtual void setLordCard(bool visible) = 0;
	virtual void setRecordVis(bool visible) = 0;
	virtual void setCardCountResult(BYTE CardList[PLAY_COUNT][54]) = 0;

	virtual void sendRecordData(S_C_GameRecordResult* data) = 0;
	//赠送礼物回复
	//virtual void dealGift(TMSG_SENG_GIFT_REQ data) = 0;

	//发送红包任务状态
	virtual void CheckActivity() = 0;
	//发送红包奖励
	virtual void CheckReceive() = 0;

	//更新红包任务状态
	virtual void setActivityNum(int Current, int toal, int CanTake) = 0;
	virtual void setReceiveState(int ReceiveType, int AwardNum) = 0;
};
}
#endif