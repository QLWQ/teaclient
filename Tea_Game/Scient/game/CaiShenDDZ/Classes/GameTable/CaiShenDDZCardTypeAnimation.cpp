/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZCardTypeAnimation.h"
#include "CaiShenDDZGameAudio.h"
#include <spine/spine-cocos2dx.h>
#include "spine/spine.h"

USING_NS_CC;
using namespace spine;

namespace CaiShenDDZ
{
	#define WIN_SIZE  Director::getInstance()->getWinSize()

	void CardTypeAnimation::playRocket(cocos2d::Node* parent, int zorder)
	{
		auto Rocket = SkeletonAnimation::createWithFile("CaiShenDDZ/skeleton/Rocket/default.json", "CaiShenDDZ/skeleton/Rocket/default.atlas");
		Rocket->setPosition(WIN_SIZE / 2);
		parent->addChild(Rocket, zorder);
		auto ani = Rocket->setAnimation(0, "animation", false);

		Rocket->setTrackEndListener(ani, [=](int trackIndex) {
			Rocket->runAction(Sequence::create(DelayTime::create(0.02f), RemoveSelf::create(true), nullptr));
		});

		GameAudio::playRocketEffect();
	}

	void CardTypeAnimation::playBomb(cocos2d::Node* parent, int zorder)
	{
		auto Bomb = SkeletonAnimation::createWithFile("CaiShenDDZ/skeleton/Bomb/default.json", "CaiShenDDZ/skeleton/Bomb/default.atlas");
		Bomb->setPosition(WIN_SIZE / 2);
		parent->addChild(Bomb, zorder);
		auto ani = Bomb->setAnimation(0, "animation", false);

		Bomb->setTrackEndListener(ani, [=](int trackIndex) {
			Bomb->runAction(Sequence::create(DelayTime::create(0.02f), CallFunc::create([=](){
				Bomb->removeFromParentAndCleanup(true);
			}), nullptr));
		});

		GameAudio::playBombEffect();
	}

	void CardTypeAnimation::playStraight(cocos2d::Node* parent, int zorder)
	{
		auto Straight = SkeletonAnimation::createWithFile("CaiShenDDZ/skeleton/Straight/default.json", "CaiShenDDZ/skeleton/Straight/default.atlas");
		Straight->setPosition(WIN_SIZE / 2);
		parent->addChild(Straight, zorder);
		auto ani = Straight->setAnimation(0, "animation", false);

		Straight->setTrackEndListener(ani, [=](int trackIndex) {
			Straight->runAction(Sequence::create(DelayTime::create(0.02f), RemoveSelf::create(true), nullptr));
		});

		GameAudio::playStraightEffect();
	}

	void CardTypeAnimation::playDoubleStraight(cocos2d::Node* parent, int zorder)
	{
		auto DoubleStraight = SkeletonAnimation::createWithFile("CaiShenDDZ/skeleton/DoubleStraight/default.json", "CaiShenDDZ/skeleton/DoubleStraight/default.atlas");
		DoubleStraight->setPosition(WIN_SIZE / 2);
		parent->addChild(DoubleStraight, zorder);
		auto ani = DoubleStraight->setAnimation(0, "animation", false);

		DoubleStraight->setTrackEndListener(ani, [=](int trackIndex) {
			DoubleStraight->runAction(Sequence::create(DelayTime::create(0.02f), RemoveSelf::create(true), nullptr));
		});
		
		GameAudio::playDoubleSequenceEffect();
	}

	void CardTypeAnimation::playPlane(cocos2d::Node* parent, int zorder)
	{
		auto Plane = SkeletonAnimation::createWithFile("CaiShenDDZ/skeleton/Plane/default.json", "CaiShenDDZ/skeleton/Plane/default.atlas");
		Plane->setPosition(WIN_SIZE / 2);
		parent->addChild(Plane, zorder);
		auto ani = Plane->setAnimation(0, "animation", false);

		Plane->setTrackEndListener(ani, [=](int trackIndex) {
			Plane->runAction(Sequence::create(DelayTime::create(0.02f), RemoveSelf::create(true), nullptr));
		});
		
		GameAudio::playPlanEffect();
	}

	void CardTypeAnimation::playSpring(cocos2d::Node* parent, int zorder)
	{
		auto Spring = SkeletonAnimation::createWithFile("CaiShenDDZ/skeleton/Spring/default.json", "CaiShenDDZ/skeleton/Spring/default.atlas");
		Spring->setPosition(WIN_SIZE / 2);
		parent->addChild(Spring, zorder);
		auto ani = Spring->setAnimation(0, "animation", false);

		Spring->setTrackEndListener(ani, [=](int trackIndex) {
			Spring->runAction(Sequence::create(DelayTime::create(0.02f), RemoveSelf::create(true), nullptr));
		});

		//GameAudio::playPlanEffect();
	}
}


