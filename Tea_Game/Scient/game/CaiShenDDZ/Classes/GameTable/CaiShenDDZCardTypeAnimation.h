/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_CARD_TYPE_ANIMATION_H__
#define __CaiShenDDZ_CARD_TYPE_ANIMATION_H__

#include "cocos2d.h"

namespace CaiShenDDZ
{
	class CardTypeAnimation
	{
	public:
		static void playRocket(cocos2d::Node* parent, int zorder);
		static void playBomb(cocos2d::Node* parent, int zorder);
		static void playStraight(cocos2d::Node* parent, int zorder);
		static void playDoubleStraight(cocos2d::Node* parent, int zorder);
		static void playPlane(cocos2d::Node* parent, int zorder);
		static void playSpring(cocos2d::Node* parent, int zorder);
	};
}



#endif