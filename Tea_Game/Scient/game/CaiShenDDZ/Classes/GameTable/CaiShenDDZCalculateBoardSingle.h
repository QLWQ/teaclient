/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef CaiShenDDZ_CalculateBoard_Single_h__
#define CaiShenDDZ_CalculateBoard_Single_h__

#include "HNUIExport.h"
#include "CaiShenDDZMessageHead.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

using namespace ui;
using namespace cocostudio;
using namespace HN;

namespace CaiShenDDZ
{
	class CalculateBoardSingle : public HNLayer
	{
	public:
		typedef std::function<void()> CloseCallBack;
		CloseCallBack onCloseCallBack = nullptr;

		CREATE_FUNC(CalculateBoardSingle);

		CalculateBoardSingle();
		virtual ~CalculateBoardSingle();

		virtual bool init();

		void close();

		//显示结算数据
		void showAndUpdateBoard(Node* parent, T_S2C_RESULT_NOTIFY boardData, string point, string mulStr);

	private:
		//确定按钮回调
		void confirmBtnCallBack(Ref* pSender, Widget::TouchEventType type);

		//倍数详情回调
		void moreBeishuBtnCallBack(Ref* pSender, T_S2C_RESULT_NOTIFY boardData);

		BYTE logicToViewSeatNo(BYTE lSeatNO);

		//倍数
		std::string numToString(int imultiple);

	protected:
		ImageView* _img_bg = nullptr;
	};
}


#endif // DouDiZhu_CalculateBoard_Single_h__
