/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __CaiShenDDZ_Game_Table_Logic_H__
#define __CaiShenDDZ_Game_Table_Logic_H__

#include "HNLogicExport.h"
#include "HNNetExport.h"
#include "CaiShenDDZMessageHead.h"
#include "CaiShenDDZGameLogic.h"

namespace CaiShenDDZ
{
	
	class GameTableUICallback;
	class GameLogic;

	class GameTableLogic: public HN::HNGameLogicBase
	{
	public:
		GameTableLogic(GameTableUICallback* uiCallback, BYTE deskNo, bool bAutoCreate);
		virtual ~GameTableLogic();	
		virtual void dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize);

	public:
		virtual void sendUserUp() override;
		virtual void sendForceQuit() override;
		virtual void clearDesk() override;
		virtual void clearDeskUsers() override;

	public:
		/*
		* @func: interface for others.
		* @info: they will support table ui to get information, or send some message to server.
		*/
		void sendShowStart(bool bMing);
		void sendOutCard(const std::vector<BYTE> Actualcards, const std::vector<BYTE> cards, EArrayType type);
		void sendNoOut();
		void sendPrompt();
		void sendRobNT(int value);
		void sendCallScore(int value);
		void sendDouble(int value);
		void sendShowCard(int value);
		void sendShowCanel();
		bool sendAuto(bool bAuto);
		void enterGame();
		void loadUsers();
		BYTE getMyDeskNo() { return _deskNo; }
		BYTE getGameStatus() { return _gameStatus; };
		void autoOutCheck(BYTE vSeatNo, const std::vector<BYTE>& cards);
		void playerOut(BYTE seatNo);
		bool isInGame();
		CC_SYNTHESIZE_PASS_BY_REF(INT, _gameRule, GameRule); //获取游戏规则
		T_C2S_PLAY_CARD_REQ getReadyOutData();
	protected:
		/*
		 * @func: game message function.
		 * @info: they will be called when game message come, message dispatch from base class.
		 */
		void dealGameBeginResp(void* object, INT objectSize);			//游戏开始
		void dealSendAllCardResp(void* object, INT objectSize);			//发牌

		void dealCallScoreResp(void* object, INT objectSize);			//叫分
		void dealCallScoreResultResp(void* object, INT objectSize);		//叫分结果
		void dealRobNTResp(void* object, INT objectSize);				//叫地主/抢地主
		void dealRobNTResultResp(void* object, INT objectSize);			//叫地主/抢地主 结果
		void dealDingZhuang(void* object, INT objectSize);		     	//定庄消息
		void dealLaiZiNotify(void* object, INT objectSize);		     	//癞子通知消息
		void dealAddDoubleResp(void* object, INT objectSize);			//开始加倍操作
		void dealAddDoubleResultResp(void* object, INT objectSize);		//加倍操作结果
		void dealShowCardResp(void* object, INT objectSize);			//地主开始选择是否明牌操作
		void dealShowCardResultResp(void* object, INT objectSize);		//明牌操作结果
		void dealOutCardResultResp(void* object, INT objectSize);		//出牌结果
		void dealOneTurnOverResp(void* object, INT objectSize);			//一轮出牌结束
		void dealNewTurnResp(void* object, INT objectSize);				//新一轮开始
		void dealPromptResult(void* object, INT objectSize);			//提示结果
		void dealPassResult(void* object, INT objectSize);				//过牌结果
		void dealLaiziSelect(void* object, INT objectSize);				//癞子组合牌型
		void dealTypeError(void* object, INT objectSize);				//牌型错误通知
		void dealContinueEndResp(void* object, INT objectSize);			//一局结束
		void dealAutoResp(void* object, INT objectSize);				//有人托管消息
		void dealOnCalculateBoard(void* object, INT objectSize);		//大结算消息
		void dealUpdateRate(void* object, INT objectSize);				//更新倍数
		void dealRecord(void* object, INT objectSize);

		//重连阶段////////////
		//等待阶段
		void dealWaitStation(void* object, INT objectSize);
		//扣压底牌阶段
		void dealBackCardStation(void* object, INT objectSize);
		//游戏中阶段
		void dealPlayStation(void* object, INT objectSize);

		/*
		 * @func: framework message function.
		 * @info: they will be called when frame message come from base class.
		 */
		virtual void dealUserAgreeResp(MSG_GR_R_UserAgree* agree) override;
		virtual void dealGameStartResp(BYTE bDeskNO) override;
		virtual void dealGameClean() override;
		//更新比赛场进度
		virtual void dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum) override;
		virtual void dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum) override;
		virtual void dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum) override;
		////////////////////////////////////////////////////////////////////////////////
		virtual void dealGameEndResp(BYTE deskNo) override;
		virtual void dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;
		virtual void dealQueueUserSitMessage(bool success, const std::string& message) override;
		virtual void dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;
		virtual void dealUserInfoChangeResp(const UserInfoStruct* user) override;
		virtual void dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo) override;
		virtual void dealGameStationResp(void* object, INT objectSize) override;
		virtual void dealGamePlayBack(bool isback) override;
		virtual void dealUserCutMessageResp(INT userId, BYTE seatNo) override;
		virtual void dealGameActivityResp(MSG_GR_GameNum* pData) override;
		virtual void dealGameActivityReceive(MSG_GR_Set_JiangLi* pData) override;

		//比赛场游戏轮数
		//virtual void dealGameNumRound(MSG_GR_ConTestRoundNum* pData) override;

		//比赛场游戏局数
		//virtual void dealGameInnings(MSG_GR_ConTestRoundNumInOneRound* pData) override;

		////////////////////////////////////////////////////////////////////
		////聊天接口
		////////////////////////////////////////////////////////////////////
		virtual void dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk) override;
		//道具特效聊天
		virtual void dealUserActionMessage(DoNewProp* normalTalk) override;
		virtual void dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;
		// 赠礼消息
		//virtual void dealGiftMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;

	protected:
		virtual void initParams();

	protected:
		GameTableUICallback*	_uiCallback;
		GameLogic				_gameLogic;
		GameTableInfo			_tableInfo;
		UINT                    _gameAllMutiple = 0;
		BYTE					_userCardList[PLAY_COUNT][54];
		BYTE _userCardCount[PLAY_COUNT];
	};
}
#endif