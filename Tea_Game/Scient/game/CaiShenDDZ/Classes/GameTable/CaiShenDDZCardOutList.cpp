/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZCardOutList.h"
#include "CaiShenDDZPokerCard.h"
#include "CaiShenDDZGameTableUI.h"

namespace CaiShenDDZ
{

#define X_OUT_OFFSET 30.0f

#define File_Cancel_Normal   "CaiShenDDZ/game/btn/not_trusteeship_n.png"
#define File_Cancel_Touched  "CaiShenDDZ/game/btn/not_trusteeship_d.png"

//////////////////////////////////////////////////////////////////////////

	CardOutList* CardOutList::create()
	{
		CardOutList* cardList = new CardOutList();
		if(cardList->init())
		{
			cardList->autorelease();
			return cardList;
		}
		CC_SAFE_DELETE(cardList);
		return nullptr;
	}

	bool CardOutList::init()
	{
		if(!HNLayer::init())
		{
			return false;
		}	
		this->setIgnoreAnchorPointForPosition(false);

		return true;
	}

	void CardOutList::outCard(const std::vector<BYTE> &Actualcards, const std::vector<BYTE> &cards, BYTE tianLaiziCard, BYTE diLaiziCard)
	{
		this->removeAllChildren();

		std::vector<Node*> cardList;
		for(size_t i = 0; i < cards.size(); i++)
		{
			BYTE laiziValue = 0;
			//������ӱ�־
			if ((Actualcards[i] % 16) == tianLaiziCard || (Actualcards[i] % 16) == diLaiziCard)
			{
				laiziValue = cards[i] % 16;
			}
			PokerCard* card = PokerCard::create(cards[i], laiziValue,0);
			card->setScale(0.55f);
			this->addChild(card, i);
			cardList.push_back(card);
		}

		if (!cardList.empty())
		{
			Size size = cardList[0]->getContentSize() * cardList[0]->getScale();
			this->setContentSize(Size(size.width + (cardList.size() - 1) * X_OUT_OFFSET, size.height));

			float startX = size.width  / 2;
			float startY = size.height / 2;

			for(size_t i = 0 ; i < cardList.size(); i++)
			{
				cardList[i]->setAnchorPoint(Vec2(0.5f, 0.5f));
				cardList[i]->setPosition(startX + i * X_OUT_OFFSET, startY);
				//cardList[i]->runAction(MoveTo::create(0.2f, Vec2(startX + i * X_OUT_OFFSET, startY)));
			}
		}
	}

	CardOutList::CardOutList()
	{

	}

	CardOutList::~CardOutList()
	{

	}

}
