/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "CaiShenDDZGameTableUI.h"
#include "CaiShenDDZGameTableLogic.h"
#include "CaiShenDDZCardOutList.h"
#include "CaiShenDDZCardListBoard.h"
#include "CaiShenDDZCardTypeAnimation.h"
#include "CaiShenDDZGameAudio.h"
#include "CaiShenDDZSetLayer.h"
#include "CaiShenDDZCalculateBoardAll.h"
#include "CaiShenDDZCalculateBoardSingle.h"
#include "CaiShenDDZSetLayer.h"
#include "HNUIExport.h"
#include <ctime>
#include "HNLobby/GameChildLayer/GameUserPopupInfoLayer.h"
	
#define Max_Zorder              100
#define Table_Zorder            1
#define Animation_Zorder        3

namespace CaiShenDDZ
{

HNGameUIBase* GameTableUI::create(BYTE bDeskIndex, bool bAutoCreate)
{
	GameTableUI* tableUI = new GameTableUI();
	if (tableUI->init(bDeskIndex, bAutoCreate))
	{
		tableUI->autorelease();
		return tableUI;
	}
	else
	{
		CC_SAFE_DELETE(tableUI);
		return nullptr;
	}
}

bool GameTableUI::init(BYTE bDeskIndex, bool bAutoCreate)
{
	if (!HNGameUIBase::init()) return false;

	_deskNo = bDeskIndex;

	// 加载UI
	loadLayout(bDeskIndex);

	


	
	// 初始化逻辑
	_tableLogic = new GameTableLogic(this, bDeskIndex, bAutoCreate);
	_tableLogic->enterGame();
	
	return true;
}

void GameTableUI::loadLayout(BYTE bDeskIndex)
{
	//add image cache.
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("CaiShenDDZ/plist/cards.plist");

	auto node = CSLoader::createNode("CaiShenDDZ/tableUI/Node_table.csb"); //斗地主场景
	_tableWidget = dynamic_cast<Layout*>(node->getChildByName("Panel_table"));
	_tableWidget->setScale(_winSize.width / 1280, _winSize.height / 720);
	_tableWidget->setPosition(_winSize / 2);
	addChild(node);

	auto Image_table = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_table"));
	Image_table->loadTexture("platform/Games/ddz/ddz_dt.png");

	//自己用户信息背景条
	auto user_infobg = Sprite::create("platform/Games/ddz/xl.png");
	user_infobg->setPosition(Vec2(1280 / 2,50 / 2));
	Image_table->addChild(user_infobg);

	_BottonScore = Sprite::create("platform/common/df_frame.png");
	_BottonScore->setPosition(Vec2(1280 / 2 + 200, 720 / 2 - 335));
	node->addChild(_BottonScore);
	
	//auto BottonLabel = Label::createWithSystemFont("", "", 30);
	auto BottonLabel = CCLabelAtlas::create("","CaiShenDDZ/tableUI/res/difenshuzi.png",32,41,'.');
	BottonLabel->setPosition(Vec2(67,10));
	BottonLabel->setScale(0.6);
	BottonLabel->setName("TextDi");
	_BottonScore->addChild(BottonLabel);
	if (PlatformLogic()->loginResult.iVipLevel > 0)
	{

		std::string str = "欢迎尊贵的Vip";
		str.append(StringUtils::toString(PlatformLogic()->loginResult.iVipLevel));
		str.append("玩家：");
		str.append(PlatformLogic()->loginResult.nickName);
		str.append(",进入房间！");
		GameNotice::getInstance()->postMessage(GBKToUtf8(str.c_str()),0);
	}
	

	float scalex = 1280 / _winSize.width;
	float scaley = 720 / _winSize.height;

	 	{
	 		//初始化VIP房间控制器
	 		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
	 		{
	 			roomController = VipRoomController::create(bDeskIndex, false);
	 			roomController->setRoomNumPos(Vec2(_winSize.width * 0.01f, _winSize.height * 0.91f));
	 			roomController->setPlayCountPos(Vec2(_winSize.width * 0.01f, _winSize.height * 0.88f));
	 			roomController->onSetVipInfoCallBack = [this]() {
	 
	 				// 更新房主标志显示
	 				setDeskInfo();
	 			};
	 
	 			roomController->onDissmissCallBack = [this]() {
	 
	 				if (!_isGameEnd)
	 				{						RoomLogic()->close();
						GamePlatform::createPlatform();

	 					/*auto prompt = GamePromptLayer::create();
	 					prompt->showPrompt(GBKToUtf8("桌子已解散"));
	 					prompt->setCallBack([=]() {
	 
	 						RoomLogic()->close();
	 						GamePlatform::createPlatform();
	 					});*/
	 				}
	 			};
	 			addChild(roomController, 5);
	 			roomController->setName("roomController");
				roomController->setVisible(false);
	 		}
	 
	 		//房间玩法
	 		_text_wanfa = Text::create();
	 		_text_wanfa->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
	 		_text_wanfa->setFontSize(20);
	 		_text_wanfa->setPosition(Vec2(_winSize.width * 0.01f, _winSize.height * 0.94f));
	 		_text_wanfa->setString("");
	 		_text_wanfa->setColor(Color3B(0, 255, 0));
	 		_tableWidget->addChild(_text_wanfa, 5);
	 
	 		//初始化比赛场控制器
	 		if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
	 		{
	 			GameMatchController* matchController = GameMatchController::create(bDeskIndex);
	 			matchController->setRankingTipPos(Vec2(_winSize.width * 0.78f, _winSize.height * 0.95f), Color4B::WHITE, 22);
	 			addChild(matchController, 10);
	 		}
	 
	 	}
	{
		roomController = VipRoomController::create(bDeskIndex);
		//if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			
			roomController->onSetVipInfoCallBack = [this](){
				// 更新房主标志显示
				setDeskInfo();
			};
			roomController->onDissmissCallBack = [this]() {

				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("房间已解散!"));

				prompt->setCallBack([=]() {

					RoomLogic()->close();
					GamePlatform::createPlatform();
				});
			};
			addChild(roomController, 20);

			

			// 修改房间号位置
			roomController->setRoomNumPos(Vec2(15.0f, _winSize.height - 25.0f));
			// 修改局数位置
			roomController->setPlayCountPos(Vec2(15.0f, _winSize.height - 50.0f));

			roomController->setRoomResultBtnPos(Vec2(_winSize.width - 150, _winSize.height - 47));


			//邀请好友按钮
			roomController->setInvitationBtnTexture("platform/Games/ddz/yqhy.png");

			roomController->setInvitationBtnPos(Vec2(_winSize.width / 2, _winSize.height / 2 + 50));

			roomController->setRoomNumPos(Vec2(10, 700));

			roomController->setPlayCountPos(Vec2(150, 700));

			roomController->setDifenPos(Vec2(260, 700));
			//调整红包位置
			roomController->setActivityPosition(0, 50.0f);

			// 			// 修改邀请好友位置
			//roomController->setInvitationBtnPos(_canvas.reday->getPosition() - Vec2(190, 0));
			// 修改返回位置 影藏
			roomController->setReturnBtnPos(Vec2(_winSize.width + 500.0f, 0));
			// 修改解散位置 影藏
			roomController->setDismissBtnPos(Vec2(_winSize.width + 500.0f, 0));
			//添加红包任务回调处理
			roomController->onClickButtonCallBack = [this](int Index){
				//发送领取红包
				if (Index < 0 || Index > 5)
				{
					return;
				}
				else
				{
					//发送
					CheckReceive();
				}
			};
			roomController->onReceiveCallBack = [this](){
				CheckActivity();
			};
			
			auto chatBtn = Button::create("platform/Games/dz.png", "", "platform/Games/dz.png");
				chatBtn->setPosition(Vec2(_winSize.width - 60, 300));
				roomController->addChild(chatBtn, 105);
				auto voiceBtn = Button::create("platform/Games/yy.png", "", "platform/Games/yy.png");
				voiceBtn->setPosition(Vec2(_winSize.width - 60, 200));
				roomController->addChild(voiceBtn, 105);
				auto setBtn = Button::create("platform/Games/sz.png", "", "platform/Games/sz.png");
				setBtn->setPosition(Vec2(_winSize.width - 60, _winSize.height - 60));
				roomController->addChild(setBtn, 105);
				setBtn->addClickEventListener([=](Ref* ref) {
					auto setLayer = GameSetLayer::create();
					setLayer->setName("setLayer");
					setLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 50);
					if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
					{
						//设置游戏状态按钮
						setLayer->setGameStateView();
					}
					else
					{
						//设置金币场退出
						setLayer->setGameGlodExit();
					}
					setLayer->onExitCallBack = [=]() {
						if (roomController)
						{
							roomController->returnBtnCallBack(ref);
						}
						else
						{
							_tableLogic->sendUserUp();
						}
						removeSetLayer();
					};
					setLayer->onDisCallBack = [=]() {

						if (roomController) roomController->dismissBtnCallBack(ref);
						removeSetLayer();
					};
					/*auto setLayer = DouDiZhuSetLayer::create();
					setLayer->showSet(this, 301);
					setLayer->setName("setLayer");
					setLayer->onExitCallBack = [=]() {
						if (roomController)
						{
							roomController->returnBtnCallBack(ref);
						}
						else
						{
							_tableLogic->sendUserUp();
						}
						removeSetLayer();
					};

					setLayer->onDisCallBack = [=]() {

						if (roomController) roomController->dismissBtnCallBack(ref);
						removeSetLayer();
					};*/

				});
				chatBtn->addTouchEventListener(CC_CALLBACK_2(GameTableUI::chatBtnClickCallBack, this));
				voiceBtn->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
					// 			if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
					// 			{
					// 				auto prompt = GamePromptLayer::create();
					// 				prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
					// 				return;
					// 			}
					if (_chatLayer == nullptr)
					{
						//显示聊天界面
						gameChatLayer();
					}
					_chatLayer->voiceChatUiButtonCallBack(pSender, type);
				});
			

		}

	}
	
	{
		// 顶部UI
		auto img_top = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_top"));
		for (auto child : img_top->getChildren())
		{
			child->setScale(scalex, scaley);
		}

		// 时间显示
		auto panel_time = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_timeBG"));
		for (auto child : panel_time->getChildren())
		{
			child->setScale(scalex, scaley);
		}
	}

	{
		// 手牌层
		auto layout_handcard = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_handCard"));
		for (auto child : layout_handcard->getChildren())
		{
			child->setScale(scalex, scaley);
		}
		//长屏适配
		if (_winSize.width / _winSize.height >= 1.8)
		{
			for (auto child : layout_handcard->getChildren())
			{
				child->setScale(scalex / scaley, 1);
			}
		}

		// 出牌层
		auto layout_outcard = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_outCard"));
		for (auto child : layout_outcard->getChildren())
		{
			child->setScale(scalex, scaley);
		}
	}

	{
		// 玩家操作按钮
		auto layout_operat = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_operat"));
		layout_operat->setLocalZOrder(1);
		for (auto child : layout_operat->getChildren())
		{
			child->setScale(scalex, scaley);
		}

		for (int i = 0; i < 17; i++)
		{
			auto btn_operat = dynamic_cast<Button*>(layout_operat->getChildByTag(i));
			if (!btn_operat) continue;
			btn_operat->addTouchEventListener(CC_CALLBACK_2(GameTableUI::menuClickCallback, this));
			btn_operat->setVisible(false);
		}
	}

	{
		// 加载玩家UI
		auto layout_player = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_player"));
		for (auto child : layout_player->getChildren())
		{
			child->setScale(scalex, scaley);
		}

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			auto img_user = dynamic_cast<ImageView*>(layout_player->getChildByName(StringUtils::format("Image_player%d", i)));
			auto player = PlayerUI::create(img_user,i);
			player->userMessageCallBack = [=](){
				BYTE lSeat = _tableLogic->viewToLogicSeatNo(i);
				if (isValidSeat(lSeat))
				{
					//auto userMessage = UserMessage::create(_deskNo, lSeat);
					//userMessage->setPosition(_winSize / 2);
					//addChild(userMessage, Animation_Zorder);
				}
			};
			addChild(player);
			player->setVisible(false);

			auto img_Head = dynamic_cast<ImageView*>(img_user->getChildByName("Image_head"));

			//头像点击
			_TouchPlayer[i] = Button::create("platform/Games/xianchu.png", "", "platform/Games/xianchu.png");
			Vec2 pos = img_user->convertToWorldSpace(img_Head->getPosition());
			_TouchPlayer[i]->setPosition(pos);
			_TouchPlayer[i]->setTag(i);
			_TouchPlayer[i]->setName("popUserInfo");
			_TouchPlayer[i]->setOpacity(0);
			_TouchPlayer[i]->setVisible(false);
			layout_player->addChild(_TouchPlayer[i]);
			_TouchPlayer[i]->addTouchEventListener(CC_CALLBACK_2(GameTableUI::getUserInfo, this));
			
			_players[i] = player;
			_players[i]->setLocalZOrder(200);		
			
		}
	}

	{
		// 加载托管层
		auto layout_auto = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_auto"));
		layout_auto->setLocalZOrder(101);
		layout_auto->setVisible(false);
		for (auto child : layout_auto->getChildren())
		{
			child->setScale(scalex, scaley);
		}

		// 取消托管按钮
		auto btn_cancle = dynamic_cast<Button*>(layout_auto->getChildByName("Button_qxtg"));
		btn_cancle->addClickEventListener([=](Ref*) {
			_tableLogic->sendAuto(false);
		});
	}

	{
		// 加载按钮层
		auto layout_btn = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_btn"));
		layout_btn->setLocalZOrder(1);

		for (auto child : layout_btn->getChildren())
		{
			child->setScale(scalex, scaley);
		}

		// 托管按钮
		auto btn_auto = dynamic_cast<Button*>(layout_btn->getChildByName("Button_ai"));
		btn_auto->setVisible(false);
		btn_auto->addClickEventListener([=](Ref*) {
			_tableLogic->sendAuto(true);
		});

		// 设置按钮
		auto btn_set = dynamic_cast<Button*>(layout_btn->getChildByName("Button_set"));
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			
			btn_set->setPosition(Vec2(_winSize.width - 50, _winSize.height - 47));
			btn_set->setVisible(false);
			btn_set->addClickEventListener([=](Ref* ref) {
				auto setLayer = GameSetLayer::create();
				setLayer->setName("setLayer");
				setLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 50);
				setLayer->onExitCallBack = [=]() {

					if (roomController)
					{
						roomController->returnBtnCallBack(ref);
					}
					else
					{
						if (_tableLogic->isInGame())
						{
							GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏中不能退出！"));
							return;
						}
						auto prompt = GamePromptLayer::create(true);
						prompt->setCallBack([=](){
							//正常退出
							_tableLogic->sendUserUp();
						});
						prompt->showPrompt(GBKToUtf8("是否退出房间？"));


					}
				};

				setLayer->onDisCallBack = [=]() {

					if (roomController) roomController->dismissBtnCallBack(ref);
				};
				if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
				{
					//设置游戏状态按钮
					setLayer->setGameStateView();
				}
				else
				{
					//设置金币场退出
					setLayer->setGameGlodExit();
				}
				/*
				auto setLayer = DouDiZhuSetLayer::create();
				setLayer->setRoomController(roomController);
				setLayer->showSet(this, 4);
				setLayer->onExitCallBack = [=]() {

					if (roomController)
					{
						roomController->returnBtnCallBack(ref);
					}
					else
					{
						if (_tableLogic->isInGame())
						{
							GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏中不能退出！"));
							return;
						}
						auto prompt = GamePromptLayer::create(true);
						prompt->setCallBack([=](){
							//正常退出
							_tableLogic->sendUserUp();
						});
						prompt->showPrompt(GBKToUtf8("是否退出房间？"));
					
						
					}
				};

				setLayer->onDisCallBack = [=]() {

					if (roomController) roomController->dismissBtnCallBack(ref);
				};
				*/
			});
			
		}

		// 聊天按钮
		auto btn_chat = dynamic_cast<Button*>(layout_btn->getChildByName("Button_chat"));
		btn_chat->addClickEventListener([=](Ref*) {

			if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
				return;
			}
			if (_chatLayer == nullptr)
			{
				//显示聊天界面
				gameChatLayer();
			}
			_chatLayer->showChatLayer();
		});

		// 语音按钮
		auto btn_voice = dynamic_cast<Button*>(layout_btn->getChildByName("Button_voice"));
		btn_voice->getTitleRenderer()->setVisible(false);
		//btn_voice->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);
		btn_voice->setVisible(false);
		btn_voice->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type) {

			if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
				return;
			}
			if (_chatLayer == nullptr)
			{
				//显示聊天界面
				gameChatLayer();
			}
			_chatLayer->voiceChatUiButtonCallBack(pSender, type);
		});

		// 微信邀请按钮
		auto Button_weixin = dynamic_cast<Button*>(layout_btn->getChildByName("Button_weixin"));
		//Button_weixin->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);
		Button_weixin->setVisible(false);
		Button_weixin->addClickEventListener([=](Ref* ref) {

			roomController->invitationBtnCallBack(ref);
		});
		// 恐龙谷邀请按钮
		auto Button_klg = dynamic_cast<Button*>(layout_btn->getChildByName("Button_klg"));
		//Button_klg->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);
		Button_klg->setVisible(false);
		Button_klg->addClickEventListener([=](Ref* ref) {

			roomController->invitationBtnCallBack(ref);
		});
		// 复制房号
		auto btn_copyRoomNum = dynamic_cast<Button*>(layout_btn->getChildByName("Button_copyRoomNum"));
		//btn_copyRoomNum->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);
		btn_copyRoomNum->setVisible(false);
		btn_copyRoomNum->addClickEventListener([=](Ref* ref) {

			//roomController->copyRoomNumBtnCallBack(ref);
		});
		_imgRecord = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_record"));
		_imgRecord->setVisible(false);
		_cRecord = (Button*)layout_btn->getChildByName("Button_record");
		_cRecord->setEnabled(false);
		char textRecord[64] = { 0 };
		for (int i = 0; i < 15; i++)
		{
			sprintf(textRecord, "Text_%d", i + 1);
			_textRecord[i] = (Text*)_imgRecord->getChildByName(textRecord);
			_textRecord[i]->setString("");
		}
		// 显示聊天界面
		gameChatLayer();

		// 显示手机信息
		showPhoneState();
		////战绩按钮
		//auto btn_Record = dynamic_cast<Button*>(layout_btn->getChildByName("Button_record1"));
		//btn_Record->setPosition(Vec2(btn_set->getPosition().x, btn_set->getPosition().y-75));
		//btn_Record->addClickEventListener([=](Ref* ref) {
		//	RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, S_C_GAME_RECORD, 0, NULL);

		//});
		//btn_Record->setVisible(false);

	}
}

void GameTableUI::onExit()
{
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("CaiShenDDZ/plist/cards.plist");
	HNGameUIBase::onExit();
}

void GameTableUI::onEnterTransitionDidFinish()
{
	HNGameUIBase::onEnterTransitionDidFinish();
	GameAudio::playBackground(BK_TYPE::Welcome);
}

void GameTableUI::autoOutCallFunction(cocos2d::Node* pNode)
{
	if(pNode == nullptr) return;
	CardListBoard* outboard = (CardListBoard*)pNode;
	std::vector<BYTE> outCards;
	outboard->getTouchedCards(&outCards);
	_tableLogic->autoOutCheck(outboard->getSeatNo(), outCards);
}

void GameTableUI::menuClickCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
{
	if (Widget::TouchEventType::ENDED != touchtype)	return;

	std::vector<BYTE> cards;

	Button* ptr = (Button*)pSender;
	std::string btnName = ptr->getName();

	if (btnName.compare("Button_ready") == 0)//准备
	{
		if ((_tableLogic->getGameStatus() == GS_WAIT_NEXT)
			&& (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME))
		{
			// 一局结束，换桌
			auto loding = LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在换桌，请稍后。。。"), 22);
			loding->setCancelCallBack([=]() {

				RoomLogic()->close();
				GamePlatform::createPlatform();
			});
			// 进入排队游戏
			_tableLogic->sendChangeDesk();
			_tableLogic->clearDeskUsers();
		}
		else
		{
			_tableLogic->sendShowStart(false);
			showActionButton(BUTTON_HIDE);
		}
			
	}
	else if (btnName.compare("Button_showStart") == 0)//明牌准备
	{
		if ((_tableLogic->getGameStatus() == GS_WAIT_NEXT)
			&& (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME))
		{
			// 一局结束，换桌
			auto loding = LoadingLayer::createLoading(Director::getInstance()->getRunningScene(), GBKToUtf8("正在换桌，请稍后。。。"), 22);
			loding->setCancelCallBack([=]() {

				RoomLogic()->close();
				GamePlatform::createPlatform();
			});
			// 进入排队游戏
			_tableLogic->sendChangeDesk();
			_tableLogic->clearDeskUsers();
		}
		else
		{
			_tableLogic->sendShowStart(true);
			showActionButton(BUTTON_HIDE);
		}		
	}
	else if (btnName.compare("Button_notOut") == 0)//不出
	{
		_tableLogic->sendNoOut();
		_cardListBoard[0]->downCards();
		showGameTips(TIPS_HIDE);
	}
	else if (btnName.compare("Button_tip") == 0)//提示
	{
		_cardListBoard[0]->downCards();
		_tableLogic->sendPrompt();
		showGameTips(TIPS_HIDE);
	}
	else if (btnName.compare("Button_outCard") == 0)//出牌
	{
		showGameTips(TIPS_HIDE);
		//发送出牌请求
		T_C2S_PLAY_CARD_REQ data = _tableLogic->getReadyOutData();
		_cardListBoard[0]->getUpCards(&cards);
		std::vector<BYTE> Actualcards(data.iCardCount);
		std::copy(data.bActualCardList, data.bActualCardList + data.iCardCount, Actualcards.begin());
		std::vector<BYTE> Dstcards(data.iCardCount);
		std::copy(data.bCardList, data.bCardList + data.iCardCount, Dstcards.begin());
		bool flag = true;
		if (cards.size() != Actualcards.size())
			flag = false;
		else
		{
			for (int i = 0; i < cards.size(); i++)
			{
				if (cards[i] != Actualcards[i])
				{
					flag = false;
					break;
				}
			}
		}	
		for (int i = 0; i < cards.size(); i++)
		{
			log(" out cards[%d] = %d", i, cards[i]);
		}
		if (flag)
			_tableLogic->sendOutCard(Actualcards, Dstcards, data.eArrayType);
		else
			_tableLogic->sendOutCard(cards, cards, EArrayType::ARRAY_Type_ERROR);
	}
	else if (btnName.compare("Button_noCallLord") == 0)//不叫
	{
		_tableLogic->sendRobNT(0);
	}
	else if (btnName.compare("Button_callLord") == 0)//叫地主
	{
		_tableLogic->sendRobNT(1);
	}
	else if (btnName.compare("Button_noRobLord") == 0)//不抢
	{
		_tableLogic->sendRobNT(2);
	}
	else if (btnName.compare("Button_robLord") == 0)//抢地主
	{
		_tableLogic->sendRobNT(3);
	}
	else if (btnName.compare("Button_showCard") == 0)//明牌
	{
		auto text_showCard = dynamic_cast<TextAtlas*>(ptr->getChildByName("AtlasLabel_timer"));
		if (text_showCard != nullptr)
		{
			stopTimer(text_showCard);
			int iMutiple = text_showCard->getTag();
			_tableLogic->sendShowCard(iMutiple);
		}
	}
	else if (btnName.compare("Button_showCanel")==0)//取消明牌
	{
		_btn_showCard->setVisible(false);
		_btn_showCanel->setVisible(false);
		_tableLogic->sendShowCanel();
		
	}
	else if (btnName.compare("Button_noDouble") == 0)//不加倍
	{
		_tableLogic->sendDouble(0);
	}
	else if (btnName.compare("Button_double") == 0)//加倍
	{
		_tableLogic->sendDouble(1);
	}
	else if (btnName.compare("Button_callLord_0") == 0)//不叫
	{
		_tableLogic->sendCallScore(0);
	}
	else if (btnName.compare("Button_callLord_1") == 0)//叫1分
	{
		_tableLogic->sendCallScore(1);
	}
	else if (btnName.compare("Button_callLord_2") == 0)//叫2分
	{
		_tableLogic->sendCallScore(2);
	}
	else if (btnName.compare("Button_callLord_3") == 0)//叫3分
	{
		_tableLogic->sendCallScore(3);
	}
	else
	{

	}
}

void GameTableUI::initParams()
{
	memset(_players, 0, sizeof(_players));
	memset(_cardListBoard, 0 , sizeof(_cardListBoard));
	memset(_cardOutList, 0, sizeof(_cardOutList));
	memset(_countResult, 0, sizeof(_countResult));
	memset(_imgCard, 0, sizeof(_imgCard));
	memset(_scoreArray, 0, sizeof(_scoreArray));
	memset(_vecUser, 0, sizeof(_vecUser));
	memset(_actionPoint, 0, sizeof(_actionPoint));
	memset(_ActionMoney, 0, sizeof(_ActionMoney));
	memset(_Array_Distance, 0, sizeof(_Array_Distance));
}

void GameTableUI::addOutCardList(BYTE seatNo)
{
	auto layout_outCard = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_outCard"));
	auto img_outCard = dynamic_cast<ImageView*>(layout_outCard->getChildByName(StringUtils::format("Image_outCard%d", seatNo)));
	if (!img_outCard) return;

	_cardOutList[seatNo] = CardOutList::create();
	_cardOutList[seatNo]->setScale(1.1f);
	_cardOutList[seatNo]->setAnchorPoint(img_outCard->getAnchorPoint());
	img_outCard->getParent()->addChild(_cardOutList[seatNo], img_outCard->getLocalZOrder() + 1);
	_cardOutList[seatNo]->setPosition(img_outCard->getPosition());
}

void GameTableUI::addHandCardList(BYTE seatNo)
{
	auto layout_handCard = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_handCard"));
	auto img_handCard = dynamic_cast<ImageView*>(layout_handCard->getChildByName(StringUtils::format("Image_handCard%d", seatNo)));
	if (!img_handCard) return;

	_cardListBoard[seatNo] = CardListBoard::create(seatNo == 0, seatNo, img_handCard);
	_cardListBoard[seatNo]->setLocalZOrder(150);
	_cardListBoard[seatNo]->setScale(0.7f);
	float scY = 720 / _winSize.height;
	//_cardListBoard[seatNo]->setScaleY(scY);
	//长屏适配
	//if (_winSize.width / _winSize.height >= 1.8)
	//{
	//	_cardListBoard[seatNo]->setScale(1 / scY, 1);
	//}
	_cardListBoard[seatNo]->setPosition(img_handCard->getPosition());
	_cardListBoard[seatNo]->setAnchorPoint(img_handCard->getAnchorPoint());
	_cardListBoard[seatNo]->setCallFunction(CC_CALLBACK_1(GameTableUI::autoOutCallFunction, this));
	//_cardListBoard[seatNo]->setZOrder(15);
	img_handCard->getParent()->addChild(_cardListBoard[seatNo], img_handCard->getLocalZOrder());
	//img_handCard->setZOrder(16);
}

GameTableUI::GameTableUI()
{
	
	initParams();
}

GameTableUI::~GameTableUI()
{
	_tableLogic->stop();
	HN_SAFE_DELETE(_tableLogic);
	SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("CaiShenDDZ/plist/cards.plist");
}

/**************************************************************/

void GameTableUI::showBackCardTypeAndMutiple(bool isShow, BYTE cardType/* = 255*/, BYTE cardMutiple/* = 255*/)
{
	auto img_top = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_top"));
	// 奖励牌型
	auto sprite_type = dynamic_cast<Sprite*>(img_top->getChildByName("Sprite_cardType"));
	sprite_type->setVisible(isShow);
	// 奖励分数
	auto sprite_score = dynamic_cast<Sprite*>(img_top->getChildByName("Sprite_cardScore"));
	sprite_score->setVisible(isShow);

	if (isShow)
	{
		sprite_type->setTexture(StringUtils::format("CaiShenDDZ/tableUI/res/addImage/paixin_%02d.png", cardType - 9));
		sprite_score->setTexture(StringUtils::format("CaiShenDDZ/tableUI/res/addImage/peishu_%02d.png", cardMutiple - 1));
	}
}

void GameTableUI::showLandlordCard(const std::vector<BYTE>& cards)
{
	auto img_top = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_top"));

	if (cards.size() > 0)
	{
		for (int i = 0; i < 3; i++)
		{
			_imgCard[i] = dynamic_cast<ImageView*>(img_top->getChildByName(StringUtils::format("Image_card%d", i)));
			if (_imgCard[i])
			{
				// out of range
				if (cards[i] < 0x00 || cards[i] > 0x3D)
				{
					if (cards[i] != 0x4E && cards[i] != 0x4F)
					{
						continue;;
					}
				}

				_imgCard[i]->loadTexture(StringUtils::format("Games/CaiShenDDZ/plist/0x%02X.png", cards[i]), TextureResType::PLIST);
			}
		}
	}
	else
	{
		for (int i = 0; i < 3; i++)
		{
			_imgCard[i] = dynamic_cast<ImageView*>(img_top->getChildByName(StringUtils::format("Image_card%d", i)));
			if (_imgCard[i]) _imgCard[i]->loadTexture(StringUtils::format("Games/CaiShenDDZ/plist/0x%02X.png", 0), TextureResType::PLIST);
		}
	}

	// 激活回放控制器
	gameRecordReady();
}

/************************************************************************/
void GameTableUI::addUser(BYTE seatNo)
{
	if (!isValidSeat(seatNo))	return;

	BYTE logicSeatNo = _tableLogic->viewToLogicSeatNo(seatNo);
	UserInfoStruct* info = _tableLogic->getUserBySeatNo(logicSeatNo);
	_players[seatNo]->loadUser(info);
	_vecUser[seatNo] = info;

	for (int i = 0; i < PLAY_COUNT; i++)
	{
		auto seatNo = _tableLogic->logicToViewSeatNo(i);
		UserInfoStruct* info = _tableLogic->getUserBySeatNo(seatNo);
		if (info == nullptr)
		{
			continue;
		}
		_scoreArray[i] = _players[seatNo]->ffmoney;
	}
	if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
	{
		_actionPoint[info->bDeskStation] = info->i64Money;
	}
	else
	{
		_ActionMoney[info->bDeskStation] = info->i64Money;
	}
	if (_TouchPlayer[seatNo])
	{
		_TouchPlayer[seatNo]->setVisible(true);
	}
	/*setUserMoney(seatNo, info->i64Money);
	_players[seatNo]->setZOrder(1000);*/
}

void GameTableUI::removeUser(BYTE seatNo)
{
	if (!isValidSeat(seatNo))	return;
	_players[seatNo]->setVisible(false);
	_vecUser[seatNo] = nullptr;
	_vecUserGeographic[seatNo] = "";
	for (int i = 0; i < 3; i++)
	{
		_Array_Distance[seatNo][i] = NULL;
	}
	if (_TouchPlayer[seatNo])
	{
		_TouchPlayer[seatNo]->setVisible(false);
	}
}

void GameTableUI::downUserCards(BYTE seatNo)
{
	_cardListBoard[seatNo]->downCards();
}

void GameTableUI::setUserName(BYTE seatNo, const std::string& name)
{
	if (!isValidSeat(seatNo))	return;
	_players[seatNo]->setUserName(name);
}

void GameTableUI::setUserMoney(BYTE seatNo, LLONG money)
{

	if (!isValidSeat(seatNo))	return;
	_players[seatNo]->setUserMoney(money);
	if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
	{
		int pso = _tableLogic->viewToLogicSeatNo(seatNo);
		_actionPoint[pso] = money;
	}
	else
	{
		//房卡场金币数不受结算影响
	}
}

void GameTableUI::updateUserMoney(BYTE seatNo, int money)
{
	BYTE viewSeat = _tableLogic->logicToViewSeatNo(seatNo);
	_scoreArray[seatNo] = money;
	setUserMoney(seatNo, _scoreArray[seatNo]);
}

void GameTableUI::setUserScore(LLONG userScore[PLAY_COUNT])
{
	memcpy(_scoreArray, userScore, sizeof(_scoreArray));
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		auto seatNo = _tableLogic->logicToViewSeatNo(i);
		setUserMoney(seatNo, _scoreArray[i]);
	}
}

void GameTableUI::setUserAuto(BYTE seatNo, bool isAuto)
{
	if(!isValidSeat(seatNo))	return;
	_players[seatNo]->setAutoHead(isAuto);

	// 自己托管时显示托管屏蔽层
	if (0 == seatNo)
	{
		auto layout_auto = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_auto"));
		layout_auto->setVisible(isAuto);
	}
}

void GameTableUI::setUserHead(BYTE seatNo)
{
	if(!isValidSeat(seatNo)) return;

	BYTE logicSeatNo = _tableLogic->viewToLogicSeatNo(seatNo);
	_players[seatNo]->setHead(_tableLogic->getUserIsBoy(logicSeatNo));
}

void GameTableUI::showUserHandCardCount(BYTE seatNo, INT count)
{
	if (!isValidSeat(seatNo)) return;
	_players[seatNo]->setUserCardCount(count);
}

void GameTableUI::showUserHandCardOut(BYTE seatNo, const std::vector<BYTE> &Actualcards, const std::vector<BYTE> &cards)
{
	if(!isValidSeat(seatNo)) return;
	
	if (_cardListBoard[seatNo])
	{
		_cardListBoard[seatNo]->removeCardAllOnce(Actualcards);
	}

	if (!_cardOutList[seatNo])
	{
		addOutCardList(seatNo);
	}
	
	_cardOutList[seatNo]->outCard(Actualcards, cards, _tianLaiziCardValue,_diLaiziCardValue);
}

void GameTableUI::showUserOutCardType(BYTE logicSeatNo, BYTE cardValue, BYTE type)
{
	bool isBoy = _tableLogic->getUserIsBoy(logicSeatNo);
	switch (type)
	{
	case ARRAY_Type_SINGLE:
		HNLOG_INFO("############# out value = %d", cardValue);
		GameAudio::playSingle(to_string(cardValue), isBoy);
		break;
	case ARRAY_Type_DOUBLE:
		GameAudio::playDouble(to_string(cardValue), isBoy);
		break;
	case ARRAY_Type_3W_:
		GameAudio::playThree(0, isBoy);
		break;
	case ARRAY_Type_3W1_ONE:
		GameAudio::playThree(1, isBoy);
		break;
	case ARRAY_Type_3W1_DOUBLE:
		GameAudio::playThree(2, isBoy);
		break;
	case ARRAY_Type_STRAIGHT_ONE:
		CardTypeAnimation::playStraight(this, Animation_Zorder);
		GameAudio::playStraight(isBoy);
		break;
	case ARRAY_Type_STRAIGHT_DOUBLE:
		CardTypeAnimation::playDoubleStraight(this, Animation_Zorder);
		GameAudio::playDoubleSequence(isBoy);
		break;
	case ARRAY_Type_PLANE_:
	case ARRAY_Type_PLANE_ONE:
	case ARRAY_Type_PLANE_DOUBLE:
		CardTypeAnimation::playPlane(this, Animation_Zorder);
		GameAudio::playPlan(isBoy);
		break;
	case ARRAY_Type_4W2_DOUBLE:
		GameAudio::playFourTake(true, isBoy);
		break;
	case ARRAY_Type_4W2_ONE:
		GameAudio::playFourTake(false, isBoy);
		break;
	case ARRAY_Type_WBOMB:
		CardTypeAnimation::playRocket(this, Animation_Zorder);
		GameAudio::playRocket(isBoy);
		break;
	case ARRAY_Type_SBOMB:
	case ARRAY_Type_HBOMB:
	case ARRAY_Type_4L:
	case ARRAY_Type_LBOMB_S:
	case ARRAY_Type_LBOMB_L:
		CardTypeAnimation::playBomb(this, Animation_Zorder);
		GameAudio::playBomb(isBoy);
	default:
		break;
	}
}

void GameTableUI::setUpCardList(BYTE seatNo, const std::vector<BYTE>& upCards)
{
	if(!isValidSeat(seatNo) || _cardListBoard[seatNo] == nullptr) return;
	_cardListBoard[seatNo]->upCards(upCards);
}

void GameTableUI::getUpCardList(BYTE seatNo, std::vector<BYTE>* upCards)
{
	if(!isValidSeat(seatNo) || _cardListBoard[seatNo] == nullptr) return;
	_cardListBoard[seatNo]->getTouchedCards(upCards);
}

void GameTableUI::showLandlord(BYTE seatNo, bool visible)
{
	if(isValidSeat(seatNo))
	{
		_players[seatNo]->setBanker(visible);
	}
}

void GameTableUI::showActionButton(BYTE flag, bool firstOut/* = false*/,bool needShowTimer)
{
	auto layout_operat = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_operat"));
	// 准备按钮
	auto btn_ready = dynamic_cast<Button*>(layout_operat->getChildByName("Button_ready"));
	btn_ready->setVisible((flag & BUTTON_READY) != 0);

	// 明牌准备按钮
	auto btn_showStart = dynamic_cast<Button*>(layout_operat->getChildByName("Button_showStart"));
	btn_showStart->setVisible((flag & BUTTON_READY) != 0);

	// 叫地主按钮
	auto btn_callLord = dynamic_cast<Button*>(layout_operat->getChildByName("Button_callLord"));
	btn_callLord->setVisible((flag & BUTTON_CALL_LORD) != 0);

	// 不叫按钮
	auto btn_noCallLord = dynamic_cast<Button*>(layout_operat->getChildByName("Button_noCallLord"));
	btn_noCallLord->setVisible((flag & BUTTON_CALL_LORD) != 0);

	// 抢地主按钮
	auto btn_robLord = dynamic_cast<Button*>(layout_operat->getChildByName("Button_robLord"));
	btn_robLord->setVisible((flag & BUTTON_ROB_LORD) != 0);

	// 不抢按钮
	auto btn_noRobLord = dynamic_cast<Button*>(layout_operat->getChildByName("Button_noRobLord"));
	btn_noRobLord->setVisible((flag & BUTTON_ROB_LORD) != 0);

	// 明牌按钮
	_btn_showCard = dynamic_cast<Button*>(layout_operat->getChildByName("Button_showCard"));
	_btn_showCard->setVisible((flag & BUTTON_SHOW_CARD) != 0);
	_btn_showCanel = dynamic_cast<Button*>(layout_operat->getChildByName("Button_showCanel"));
	_btn_showCanel->setVisible((flag & BUTTON_SHOW_CARD) != 0);
	if ((flag & BUTTON_SHOW_CARD) != 0)
	{
		auto text_showCard = dynamic_cast<TextAtlas*>(_btn_showCard->getChildByName("AtlasLabel_timer"));
		if (!needShowTimer || ((_tableLogic->getGameRule() != JINGDIAN_LAIZI) && (_tableLogic->getGameRule() != TIANDI_LAIZI)))
		{
			text_showCard->setString("/2");
			text_showCard->setTag(2);
		}
		else
		{
			startTimer(text_showCard, 4);
		}
	}
	// 加倍按钮
	auto btn_double = dynamic_cast<Button*>(layout_operat->getChildByName("Button_double"));
	btn_double->setVisible((flag & BUTTON_DOUBLE) != 0);

	// 不加倍按钮
	auto btn_noDouble = dynamic_cast<Button*>(layout_operat->getChildByName("Button_noDouble"));
	btn_noDouble->setVisible((flag & BUTTON_DOUBLE) != 0);

	// 不叫分按钮
	auto btn_noCallScore = dynamic_cast<Button*>(layout_operat->getChildByName("Button_callLord_0"));
	btn_noCallScore->setVisible((flag & BUTTON_CALL_SCORE) != 0);

	// 叫1分按钮
	auto btn_callScore1 = dynamic_cast<Button*>(layout_operat->getChildByName("Button_callLord_1"));
	btn_callScore1->setVisible((flag & BUTTON_CALL_SCORE) != 0);

	// 叫2分按钮
	auto btn_callScore2 = dynamic_cast<Button*>(layout_operat->getChildByName("Button_callLord_2"));
	btn_callScore2->setVisible((flag & BUTTON_CALL_SCORE) != 0);

	// 叫3分按钮
	auto btn_callScore3 = dynamic_cast<Button*>(layout_operat->getChildByName("Button_callLord_3"));
	btn_callScore3->setVisible((flag & BUTTON_CALL_SCORE) != 0);

	/*
	* 第一手出牌，必须要出牌，显示出牌、提示
	* 已经有人出过牌，显示出牌、提示不出
	*/
	// 不出按钮
	auto btn_noOutCard = dynamic_cast<Button*>(layout_operat->getChildByName("Button_notOut"));
	btn_noOutCard->setVisible(((flag & BUTTON_OUTCARD) != 0) && !firstOut);

	// 提示按钮
	auto btn_tip = dynamic_cast<Button*>(layout_operat->getChildByName("Button_tip"));
	btn_tip->setVisible((flag & BUTTON_OUTCARD) != 0);

	// 出牌按钮
	auto btn_outCard = dynamic_cast<Button*>(layout_operat->getChildByName("Button_outCard"));
	btn_outCard->setVisible((flag & BUTTON_OUTCARD) != 0);

	if (firstOut)
	{
		btn_tip->setPositionX(_winSize.width * 0.4f);
		btn_outCard->setPositionX(_winSize.width * 0.6f);
	}
	else
	{
		btn_tip->setPositionX(_winSize.width * 0.5f);
		btn_outCard->setPositionX(_winSize.width * 0.7f);
	}
}

//开启明牌倒计时
void GameTableUI::startTimer(TextAtlas* txtTimer, int timer)
{
	stopTimer(txtTimer);

	txtTimer->setString("x2");
	txtTimer->setTag(2);
	//_timerflag = false;
	//std::time_t now = std::time(nullptr);
	//int timeTag = now + timer;
	//txtTimer->runAction(RepeatForever::create(Sequence::create(
	//	CallFunc::create([=]()
	//{
	//	int nowtag = std::time(nullptr);

	//	if (timeTag > nowtag)
	//	{
	//		// 更新计时器数字
	//		std::stringstream buff;
	//		buff << "/" << timeTag - nowtag;
	//		txtTimer->setString(buff.str());
	//		txtTimer->setTag(timeTag - nowtag);
	//	}

	//	if (timeTag <= nowtag && !_timerflag)
	//	{
	//		_timerflag = true;
	//		txtTimer->stopAllActions();
	//	}
	//}),
	//	DelayTime::create(1.0f),
	//	nullptr)));
}

//关闭明牌倒计时
void GameTableUI::stopTimer(TextAtlas* txtTimer)
{
	txtTimer->stopAllActions();
}
void GameTableUI::showUserOperationHints(BYTE seatNo, Hint_Type type/* = NONE*/)
{
	if (!isValidSeat(seatNo)) return;

	_players[seatNo]->setUserOperationHints(type);
}

void GameTableUI::showUserHandCard(BYTE seatNo, const std::vector<BYTE>& values, BYTE tianLaiziCard, BYTE diLaiziCard, bool isUp, bool isOnce, bool isFront)
{
	if (isValidSeat(seatNo) && _players[seatNo] != nullptr)
	{
		// 如果发牌的时候还有结算列表则移除（回放时会出现这样情况）
		if (0 == seatNo && getChildByName("singleBoard"))
		{
			removeChildByName("singleBoard");
		}
		
		if(_cardListBoard[seatNo] == nullptr)
		{
			addHandCardList(seatNo);
		}
		if(!isUp)
		{
			_cardListBoard[seatNo]->clear();
		}
		if (isFront)
		{
			_players[seatNo]->setCardCountVis(false);
		}
		
		isOnce ? _cardListBoard[seatNo]->addCardAllOnce(values, isUp, isFront, tianLaiziCard,diLaiziCard) : _cardListBoard[seatNo]->addCardOneByOne(values, isFront, _tableLogic->getGameRule() == JINGDIAN_NO_SHUFFLE, tianLaiziCard,diLaiziCard);
	}
}

void GameTableUI::clearDesk()
{
	for(int i = 0; i < PLAY_COUNT; i++)
	{		
		if(_cardListBoard[i] != nullptr)
		{
			_cardListBoard[i]->clear();
		}
		
		if(_cardOutList[i] != nullptr)
		{
			_cardOutList[i]->removeAllChildren();
		}

		showLandlord(i, false);
		setUserAuto(i, false);
	}

	setLaiziCard(false, 0);

	if (getChildByName("CalculateBoard"))
	{
		getChildByName("CalculateBoard")->removeFromParent();
	}

	
	// 隐藏桌面提示
	showGameTips(TIPS_HIDE);

	// 隐藏底牌类型和倍数
	showBackCardTypeAndMutiple(false);

	// 恢复地主牌背
	std::vector<BYTE> cards;
	showLandlordCard(cards);
	setLordCard(false);
	// 隐藏所有操作按钮
	showActionButton(BUTTON_HIDE);
}

void GameTableUI::leaveDesk(bool isForceQuit)
{
	/*if (isForceQuit)
	{*/
		_tableLogic->stop();
		RoomLogic()->close();
		GamePlatform::createPlatform();
	/*}
	else
	{
		if (RoomLogic()->getRoomRule() & GRR_QUEUE_GAME || RoomLogic()->getRoomRule() & GRR_NOTCHEAT)
		{
			RoomLogic()->close();
			GamePlatform::returnPlatform(LayerType::ROOMLIST);
		}
		else
		{
			GamePlatform::returnPlatform(LayerType::DESKLIST);
		}
	}*/
}

void GameTableUI::showNotice(const std::string &message)
{
	//GameNotice::getInstance()->postMessage(GBKToUtf8(message.c_str()));
}
void GameTableUI::showVipEnterTip(BYTE seatNo)
{
	BYTE logicSeatNo = _tableLogic->viewToLogicSeatNo(seatNo);
	UserInfoStruct* info = _tableLogic->getUserBySeatNo(logicSeatNo);
	if (info->iVipLevel > 0)
	{
	std::string str = "欢迎尊贵的Vip";
	str.append(StringUtils::toString(info->iVipLevel));
	str.append("玩家：");
	str.append(info->nickName);
	str.append(",进入房间！");
	GameNotice::getInstance()->postMessage(GBKToUtf8(str.c_str()),0);
	}
	
}

void GameTableUI::setBasePoint(int point)
{
	auto img_top = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_top"));
	auto text_point = dynamic_cast<TextAtlas*>(img_top->getChildByName("AtlasLabel_point"));
	if (text_point) 
		text_point->setString(to_string(point));

	if (point > 0)
	{
		
		auto text = (LabelAtlas*)_BottonScore->getChildByName("TextDi");
		text->setString(StringUtils::toString(point));

	}
	else
	{
		_BottonScore->setVisible(false);
	}
}

void GameTableUI::setMultiple(int multiple, bool isAll/* = false*/)
{
	auto img_top = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_top"));
	auto text_rate = dynamic_cast<TextAtlas*>(img_top->getChildByName("AtlasLabel_rate"));
	std::string mulStr("");
	if (multiple >= 0 && multiple < 10000)
	{
		mulStr = StringUtils::format("%d", multiple);
	}
	else
	{
		//multiple = multiple / 10000;
		mulStr = StringUtils::format("%d", multiple);
	}
	if (text_rate) text_rate->setString(mulStr);

	auto img_rate = dynamic_cast<ImageView*>(img_top->getChildByName("Image_rate"));
	if (img_rate)
	{
		if (!isAll)
		{
			//img_rate->loadTexture("CaiShenDDZ/tableUI/res/beishu.png");
		}
		else
		{
			//img_rate->loadTexture("CaiShenDDZ/tableUI/res/zongfen.png");
		}
	}	
}

void GameTableUI::showGameTips(Tips_Type type)
{
	auto img_tips = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_gameTips"));
	if (!img_tips) return;

	img_tips->setVisible(type != TIPS_HIDE);

	switch (type)
	{
	case CaiShenDDZ::TIPS_INVALIDOUT:
		img_tips->loadTexture("CaiShenDDZ/tableUI/res/tips_outerror.png");
		break;
	case CaiShenDDZ::TIPS_CANNOTOUT:
		img_tips->loadTexture("CaiShenDDZ/tableUI/res/tips_onlypass.png");
		break;
	default:
		break;
	}
}

bool GameTableUI::isValidSeat(BYTE seatNo)
{
	return (seatNo < PLAY_COUNT && seatNo >= 0);
}

void GameTableUI::setDeskInfo()
{
	auto info = _tableLogic->getUserByUserID(HNPlatformConfig()->getMasterID());
	if (info) showMaster(_tableLogic->logicToViewSeatNo(info->bDeskStation), true);

	auto layout_btn = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_btn"));
	auto Button_weixin = dynamic_cast<Button*>(layout_btn->getChildByName("Button_weixin"));
	//Button_weixin->setVisible(0 == HNPlatformConfig()->getNowCount());
	Button_weixin->setVisible(false);
	auto Button_klg = dynamic_cast<Button*>(layout_btn->getChildByName("Button_klg"));
	Button_klg->setVisible(false);
	auto btn_copyRoomNum = dynamic_cast<Button*>(layout_btn->getChildByName("Button_copyRoomNum"));
	btn_copyRoomNum->setVisible(false);
}

void GameTableUI::showMaster(BYTE seatNo, bool bShow)
{
	if (!isValidSeat(seatNo)) return;
	_players[seatNo]->setOwner(bShow);
}

void GameTableUI::showUserCut(BYTE seatNo, bool bShow)
{
	if (!isValidSeat(seatNo) || _cardListBoard[seatNo] == nullptr) return;

	_players[seatNo]->setOffline(bShow);

	//设置离线玩家的准备标志隐藏
	_players[seatNo]->setUserOperationHints(STATE_HIDE);
}

void GameTableUI::setAutoAgree(int delayTime)
{
	_players[0]->showTimer(delayTime, [=]() {

		_tableLogic->sendAgreeGame();
		//showReadyBtnVisible(false);
	});
}

void GameTableUI::showTimer(BYTE seatNo, int delayTime, bool visible)
{
	if (!isValidSeat(seatNo))	return;
	_players[seatNo]->showTimer(delayTime, visible);

	//for (int i = 0; i < PLAY_COUNT; i++)
	//{
	//	_players[i]->showTimer(0, false);
	//}
}

void GameTableUI::showCalculateBoard(T_S2C_TOTAL_RESULT boardData)
{
	_isGameEnd = true;
	Node* pRuningScene = Director::getInstance()->getRunningScene();
	auto calculateBoard = CalculateBoardAll::create();
	
	//if (getChildByName("singleBoard")) calculateBoard->setVisible(false);
	calculateBoard->setName("calculateBoardAll");
	//pRuningScene->addChild(calculateBoard, 100000009);
	
	calculateBoard->showAndUpdateBoard(pRuningScene, boardData);

}

void GameTableUI::showSingleCalculateBoard(T_S2C_RESULT_NOTIFY endData)
{
	_isGameEnd = true;
	_boarSingle = CalculateBoardSingle::create();
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		auto seatNo = _tableLogic->logicToViewSeatNo(i);
		_scoreArray[i] += endData.iMoney[i];
		setUserMoney(seatNo, _scoreArray[i]);
	}
	auto img_top = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_top"));
	auto text_point = dynamic_cast<TextAtlas*>(img_top->getChildByName("AtlasLabel_point"));
	auto text_rate = dynamic_cast<TextAtlas*>(img_top->getChildByName("AtlasLabel_rate"));
	//获取底分和倍数
	string point = text_point->getString();
	std::string mulStr = text_rate->getString();

	Node* pRuningScene = Director::getInstance()->getRunningScene();
	_boarSingle->showAndUpdateBoard(pRuningScene, endData, point, mulStr);
	_boarSingle->setName("singleBoard");
	_boarSingle->onCloseCallBack = [=](){
		if (getChildByName("calculateBoardAll"))
		{
			getChildByName("calculateBoardAll")->setVisible(true);
			return;
		}
		showActionButton(BUTTON_READY);
		GameAudio::playBackground(Welcome);
	};

	// 如果是春天，则先展示春天动画然后显示小结算
	float delaytime = 3.0f;
	if (endData.bIsSpring)
	{
		delaytime = 5.0f;
		_boarSingle->setVisible(false);
		CardTypeAnimation::playSpring(this, Animation_Zorder);
		GameAudio::playSpringEffect();
		_boarSingle->runAction(Sequence::create(DelayTime::create(2.0f), Show::create(), nullptr));
	}

	// 比赛场显示3秒小结算然后清理桌面
	if (RoomLogic()->getRoomRule() & GRR_CONTEST || (RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST))
	{
		runAction(Sequence::create(DelayTime::create(delaytime), CallFunc::create([=](){
			if (getChildByName("singleBoard"))
			{
				getChildByName("singleBoard")->removeFromParent();
				_tableLogic->clearDesk();
				_tableLogic->clearDeskUsers();
				GameAudio::playBackground(Welcome);
			}
		}), nullptr));
	}
}

void GameTableUI::showMingPaiMulImg(int mul)
{
	if (mul >= 2 && mul <= 5)
	{
		std::string str = StringUtils::format("CaiShenDDZ/tableUI/res/addImage/mul_%d.png", mul);
		auto mulSp = Sprite::create(str);
		addChild(mulSp, Max_Zorder + 1);
		mulSp->setPosition(_winSize / 2);
		mulSp->setScale(0.8f);
		mulSp->runAction(Sequence::create(DelayTime::create(2.0f), CallFunc::create([=](){
			mulSp->removeFromParent();
		}), nullptr));
	}
}

void GameTableUI::setAutoBtnVisible(bool visible)
{
	auto layout_btn = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_btn"));
	// 托管按钮
	auto btn_auto = dynamic_cast<Button*>(layout_btn->getChildByName("Button_ai"));
	btn_auto->setVisible(visible);
}

//房间说明
void GameTableUI::showGameRule(int playMode)
{
// 	std::string str("");
// 	switch (playMode)
// 	{
// 	case 1:
// 		_text_wanfa->setString(StringUtils::format("%s", GBKToUtf8("玩法：经典")));
// 		str += "经典";
// 		break;
// 	case 2:
// 		_text_wanfa->setString(StringUtils::format("%s", GBKToUtf8("玩法：不洗牌")));
// 		str += "不洗牌";
// 		break;
// 	case 3:
// 		_text_wanfa->setString(StringUtils::format("%s", GBKToUtf8("玩法：癞子")));
// 		str += "癞子";
// 		break;
// 	case 4:
// 		_text_wanfa->setString(StringUtils::format("%s", GBKToUtf8("玩法：癞子,不洗牌")));
// 		str += "癞子,不洗牌";
// 		break;
// 	case 5:
// 		_text_wanfa->setString(StringUtils::format("%s", GBKToUtf8("玩法：天地癞子")));
// 		str += "天地癞子";
// 		break;
// 	case 6:
// 		_text_wanfa->setString(StringUtils::format("%s", GBKToUtf8("玩法：天地癞子,不洗牌")));
// 		str += "天地癞子,不洗牌";
// 		break;
// 	default:
// 		break;
// 	}
	//保存有消息玩法
//	HNPlatformConfig()->setPlayMethod(str);

// 	std::string shareRule = StringUtils::format(("%s | %s"),/* HNPlatformConfig()->getPlayerNum(),*/ GBKToUtf8("人"), GBKToUtf8(str));
// 	VipRoomController* controller = (VipRoomController*)getChildByName("roomController");
// 	if (controller)
// 	{
// 		controller->setShareRule(shareRule);
// 	}
}

//显示文本聊天
void GameTableUI::onChatTextMsg(BYTE seatNo, std::string msg)
{
	auto userInfo = UserInfoModule()->findUser(_tableLogic->getMyDeskNo(), seatNo);
	auto viewSeatNo = _tableLogic->logicToViewSeatNo(seatNo);
	auto chatParent = _players[viewSeatNo]->getChatParent();
	auto frameSize = chatParent->getContentSize();
	
	if (userInfo == nullptr) return;
	Vec2 bubblePos = chatParent->getPosition()+Vec2(0,20);
	bool isFilpped = false;

	if (viewSeatNo >= 0 && viewSeatNo <= 1)
	{
		bubblePos.x = bubblePos.x + frameSize.width / 2 + 10;
	}
	else
	{
		isFilpped = true;
		bubblePos.x = bubblePos.x - frameSize.width / 2;
	}
	_chatLayer->onHandleTextMessage(chatParent->getParent(), bubblePos, msg, userInfo->nickName, userInfo->bBoy, isFilpped, viewSeatNo);
}

//显示语音聊天
void GameTableUI::onChatVoiceMsg(int userID, int voiceID, int voiceTime)
{
	auto userInfo = UserInfoModule()->findUser(userID);
	auto viewSeatNo = _tableLogic->logicToViewSeatNo(userInfo->bDeskStation);
	auto chatParent = _players[viewSeatNo]->getChatParent();
	auto frameSize = chatParent->getContentSize();
	if (userInfo == nullptr) return;
	Vec2 bubblePos = chatParent->getPosition()+Vec2(0,20);
	bool isFilpped = false;

	if (viewSeatNo >= 0 && viewSeatNo <= 1)
	{
		bubblePos.x = bubblePos.x + frameSize.width / 2 + 10;
	}
	else
	{
		isFilpped = true;
		bubblePos.x = bubblePos.x - frameSize.width / 2;
	}
	_chatLayer->onHandleVocieMessage(chatParent->getParent(), bubblePos, voiceID, userInfo->nickName, isFilpped, viewSeatNo, voiceTime);
}

//聊天界面
void GameTableUI::gameChatLayer()
{
	_chatLayer = GameChatLayer::create();
	addChild(_chatLayer, 301);
	_chatLayer->setPosition(_winSize / 2);
	_chatLayer->onSendTextCallBack = [=](const std::string msg) {
		if (!msg.empty())
		{
			_tableLogic->sendChatMsg(msg);
		}
	};
}

//显示动作特效
void GameTableUI::onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index)
{
	auto pLayer = dynamic_cast<GameUserPopupInfoLayer*>(getChildByName(GAME_POPUPINFO_LAYER_NAME));
	if (pLayer == nullptr)
	{
		pLayer = GameUserPopupInfoLayer::create();
		pLayer->setMaxPlayerCount(PLAY_COUNT);
	}
	auto viewSSeatNo = _tableLogic->logicToViewSeatNo(sendSeatNo);
	auto viewTSeatNo = _tableLogic->logicToViewSeatNo(TargetSeatNo);
	auto herd = _TouchPlayer[viewSSeatNo];
	auto herd1 = _TouchPlayer[viewTSeatNo];
	pLayer->doAction(sendSeatNo, TargetSeatNo, index, herd, herd1);
}

void GameTableUI::gameRecordReady()
{
	EventCustom event("gameRecordReady");
	Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
}

void GameTableUI::showPhoneState()
{
	auto img_timeBG = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_timeBG"));
	auto img_signal = dynamic_cast<ImageView*>(img_timeBG->getChildByName("Image_signal"));
	auto img_power = dynamic_cast<ImageView*>(img_timeBG->getChildByName("Image_power"));
	auto loadingBar_power = dynamic_cast<LoadingBar*>(img_timeBG->getChildByName("LoadingBar_power"));
	auto text_power = dynamic_cast<Text*>(img_timeBG->getChildByName("Text_power"));
	auto text_time = dynamic_cast<Text*>(img_timeBG->getChildByName("Text_time"));
	std::string args = Operator::requestChannel("sysmodule", "getBatteryLeave");
	//log("getBatteryLeave:%s", args.c_str());

	loadingBar_power->setPercent(atoi(args.c_str()));
	text_power->setString(args);

	std::string type = Operator::requestChannel("sysmodule", "getNetType");
	//log("getNetType:%s", type.c_str());

	// 网络类型为WIFI，则显示信号强度
	if (type.compare("WIFI") == 0)
	{
		std::string level = Operator::requestChannel("sysmodule", "getNetLevel");
		//log("getNetLevel:%s", level.c_str());

		img_signal->loadTexture(StringUtils::format("CaiShenDDZ/tableUI/res/signal/wf%s.png", level.c_str()));
	}
	else if (type.compare("2G") == 0 || type.compare("3G") == 0 || type.compare("4G") == 0)
	{
		img_signal->loadTexture(StringUtils::format("CaiShenDDZ/tableUI/res/signal/%s.png", type.c_str()));
	}//无法获取信号
	else
	{
		img_signal->setVisible(false);
	}

	time_t tt = time(NULL);
	struct tm* Time = localtime(&tt);
	char showTime[24];
	strftime(showTime, sizeof(showTime), "%H:%M", Time);

	// 时间
	text_time->setString(showTime);

	this->runAction(Sequence::create(DelayTime::create(5.0f), CallFunc::create([=]() {

		showPhoneState();
	}), nullptr));
}

//设置癞子牌是否显示
void GameTableUI::setLaiziCard(bool isShow, BYTE cardValue,bool bTianLaizi)
{
	auto ImageTianLaiziFrame = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_laiziFrame_tian"));
	auto ImageTianLaiziCard = dynamic_cast<ImageView*>(ImageTianLaiziFrame->getChildByName("Image_laiziCard"));
	auto ImageTian = dynamic_cast<ImageView*>(ImageTianLaiziFrame->getChildByName("Image_tian"));
	auto ImageLai = dynamic_cast<ImageView*>(ImageTianLaiziFrame->getChildByName("Image_lai"));

	auto ImageDiLaiziFrame = dynamic_cast<ImageView*>(_tableWidget->getChildByName("Image_laiziFrame_di"));
	auto ImageDiLaiziCard = dynamic_cast<ImageView*>(ImageDiLaiziFrame->getChildByName("Image_laiziCard"));

	ImageTianLaiziFrame->setScale(0.1);
	ImageDiLaiziFrame->setScale(0.1);
	ImageTianLaiziFrame->setPosition(Vec2(640,520));
	ImageDiLaiziFrame->setPosition(Vec2(640, 520));
	if (cardValue == 0 && !isShow)
	{
		_tianLaiziCardValue = cardValue;
		_diLaiziCardValue = cardValue;
		ImageTianLaiziFrame->setVisible(false);
		ImageDiLaiziFrame->setVisible(false);
		return;
	}
	else if (cardValue > 0 && (cardValue <= 0x4f))
	{
		if (bTianLaizi) //天地癞子和普通癞子都为true
		{
			ImageTianLaiziFrame->setVisible(isShow);
			_tianLaiziCardValue = cardValue;

			if (ImageTianLaiziFrame->isVisible())
			{
				auto scaleto = ScaleTo::create(1, 1.25);

				ActionInterval* jumpto = JumpTo::create(1, Vec2(160, 637), 50, 1);
				auto seq = Sequence::create(scaleto, jumpto, NULL);
				ImageTianLaiziFrame->runAction(seq);
			}

			//天癞子牌框
			ImageTianLaiziCard->loadTexture(StringUtils::format("Games/CaiShenDDZ/plist/0x%02X.png", cardValue), TextureResType::PLIST);
			if ((_tableLogic->getGameRule() != TIANDI_LAIZI) && (_tableLogic->getGameRule() != TIANDI_LAIZI_NOSHUFFLE))
			{
				ImageTian->setVisible(false);
				ImageLai->setVisible(true);
			}
			else
			{
				ImageTian->setVisible(true);
				ImageLai->setVisible(false);
			}
		}
		else
		{
			_diLaiziCardValue = cardValue;
			ImageDiLaiziFrame->setVisible(isShow);

			if (ImageDiLaiziFrame->isVisible())
			{
				auto scaleto = ScaleTo::create(1, 1.25);

				ActionInterval* jumpto = JumpTo::create(1, Vec2(66, 637), 50, 1);
				auto seq = Sequence::create(scaleto, jumpto, NULL);
				ImageDiLaiziFrame->runAction(seq);
			}
			if (ImageTianLaiziFrame->isVisible())
			{
				ImageTianLaiziFrame->setPosition(Vec2(160, 637));
				ImageTianLaiziFrame->setScale(1.25f);
			}

			//地癞子牌框
			ImageDiLaiziCard->loadTexture(StringUtils::format("Games/CaiShenDDZ/plist/0x%02X.png", cardValue), TextureResType::PLIST);
		}
	}
}

void GameTableUI::showLaiziCardType(T_S2C_SELECT_RESULT_RES data)
{
	auto layer = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_laiziSelect"));
	layer->setLocalZOrder(2);
	layer->addClickEventListener([this](Ref* pSender){
		showLaiziCardTypeLayer(false);
	});

	auto listview = dynamic_cast<ListView*>(layer->getChildByName("ListView_1"));
	listview->removeAllItems();
	if (data.iCardCount > 4)
	{
		listview->setScrollBarEnabled(true);
		listview->setScrollBarAutoHideEnabled(false);
	}
	else
	{
		listview->setScrollBarEnabled(false);
	}
	for (int i = 0; i < data.iCardCount; i++)
	{
		auto item = getLaiziTypeItem(data.sCard[i]);
		listview->pushBackCustomItem(item);
	}
}

void GameTableUI::showLaiziCardTypeLayer(bool isShow)
{
	auto layer = dynamic_cast<Layout*>(_tableWidget->getChildByName("Panel_laiziSelect"));
	if (layer)
	{
		layer->setVisible(isShow);
	}	
}

//创建癞子牌组合子项
Button* GameTableUI::getLaiziTypeItem(T_C2S_PLAY_CARD_REQ data)
{
	auto node = CSLoader::createNode("CaiShenDDZ/tableUI/Node_laiziCardType.csb");
	auto btn = dynamic_cast<Button*> (node->getChildByName("Button_1"));

	btn->addClickEventListener([=](Ref* pSender){
		//发送出牌请求
		std::vector<BYTE> Actualcards(data.iCardCount);
		std::copy(data.bActualCardList, data.bActualCardList + data.iCardCount, Actualcards.begin());
		std::vector<BYTE> cards(data.iCardCount);
		std::copy(data.bCardList, data.bCardList + data.iCardCount, cards.begin());
		_tableLogic->sendOutCard(Actualcards, cards, data.eArrayType);
	});

	//设置牌型
	auto textCardType = dynamic_cast<Text*> (btn->getChildByName("Text_cardType"));
	switch (data.eArrayType)
	{
	case ARRAY_Type_SINGLE:
		textCardType->setString(GBKToUtf8("单张"));
		break;
	case ARRAY_Type_DOUBLE:
		textCardType->setString(GBKToUtf8("对子"));
		break;
	case ARRAY_Type_3W_:
		textCardType->setString(GBKToUtf8("三张"));
		break;
	case ARRAY_Type_3W1_ONE:
		textCardType->setString(GBKToUtf8("三带一"));
		break;
	case ARRAY_Type_3W1_DOUBLE:		//3带对子
		textCardType->setString(GBKToUtf8("三带对子"));
		break;
	case ARRAY_Type_PLANE_:
	case ARRAY_Type_PLANE_ONE:
	case ARRAY_Type_PLANE_DOUBLE:
		textCardType->setString(GBKToUtf8("飞机"));
		break;
	case ARRAY_Type_STRAIGHT_ONE:
		textCardType->setString(GBKToUtf8("顺子"));
		break;
	case ARRAY_Type_STRAIGHT_DOUBLE:
		textCardType->setString(GBKToUtf8("连对"));
		break;
	case ARRAY_Type_4W2_ONE:
		textCardType->setString(GBKToUtf8("4带2"));
		break;
	case ARRAY_Type_4W2_DOUBLE:
		textCardType->setString(GBKToUtf8("4带2对"));
		break;
	case ARRAY_Type_SBOMB:
	case ARRAY_Type_HBOMB:			//硬炸
	case ARRAY_Type_WBOMB:
	case ARRAY_Type_4L:
	case ARRAY_Type_LBOMB_S:
	case ARRAY_Type_LBOMB_L:
		textCardType->setString(GBKToUtf8("炸弹"));
		break;
	default:
		break;
	}

	//设置牌
	auto scrollview = dynamic_cast<ui::ScrollView*> (btn->getChildByName("ScrollView_1"));
	scrollview->removeAllChildrenWithCleanup(true);
	scrollview->setScrollBarEnabled(false);
	for (int i = 0; i < data.iCardCount; i++)
	{
		BYTE laiziValue = 0;
		//设置癞子标志
		if ((data.bActualCardList[i] % 16) == _tianLaiziCardValue || (data.bActualCardList[i] % 16) == _diLaiziCardValue)
		{
			laiziValue = data.bCardList[i] % 16;
		}
		auto card = PokerCard::create(data.bCardList[i], laiziValue,0);
		scrollview->addChild(card, 1);
		card->setPosition(Vec2(card->getContentSize().width / 2 * 0.5 * (i + 1), scrollview->getContentSize().height / 2));
		card->setScale(0.4f); //癞子出牌显示调整6.30
	}

	btn->removeFromParentAndCleanup(true);
	return btn;
}

void GameTableUI::sendGift(BYTE senderSeatNo, BYTE receiverSeatNo, int giftNo)
{
	auto SSeatNo = _tableLogic->logicToViewSeatNo(senderSeatNo);
	auto ESeatNo = _tableLogic->logicToViewSeatNo(receiverSeatNo);
	if ((isValidSeat(SSeatNo) && _players[SSeatNo] != nullptr && (isValidSeat(ESeatNo) && _players[ESeatNo] != nullptr)))
	{
		Vec2 startPostion = _tableWidget->convertToNodeSpace(_players[SSeatNo]->getHeadPosition());
		Vec2 endPosition = _tableWidget->convertToNodeSpace(_players[ESeatNo]->getHeadPosition());
		_players[SSeatNo]->playGiftAni(startPostion, endPosition, giftNo);
	}
}

void GameTableUI::setCardCountResult(BYTE CardList[PLAY_COUNT][54])
{
	memset(_countResult, 0, sizeof(_countResult));
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		for (int j = 0; j < 54; j++)
		{
			switch (CardList[i][j])
			{
			case 0x01:
			case 0x11:
			case 0x21:
			case 0x31:
			{
						 _countResult[0]++;
						 break;
			}
			case 0x02:
			case 0x12:
			case 0x22:
			case 0x32:
			{
						 _countResult[1]++;
						 break;
			}
			case 0x03:
			case 0x13:
			case 0x23:
			case 0x33:


			{
						 _countResult[2]++;
						 break;
			}
			case 0x04:
			case 0x14:
			case 0x24:
			case 0x34:

			{
						 _countResult[3]++;
						 break;
			}
			case 0x05:
			case 0x15:
			case 0x25:
			case 0x35:

			{
						 _countResult[4]++;
						 break;
			}
			case 0x06:
			case 0x16:
			case 0x26:
			case 0x36:

			{
						 _countResult[5]++;
						 break;
			}
			case 0x07:
			case 0x17:
			case 0x27:
			case 0x37:

			{
						 _countResult[6]++;
						 break;
			}
			case 0x08:
			case 0x18:
			case 0x28:
			case 0x38:

			{
						 _countResult[7]++;
						 break;
			}
			case 0x09:
			case 0x19:
			case 0x29:
			case 0x39:

			{
						 _countResult[8]++;
						 break;
			}
			case 0x0A:
			case 0x1A:
			case 0x2A:
			case 0x3A:

			{
						 _countResult[9]++;
						 break;
			}
			case 0x0B:
			case 0x1B:
			case 0x2B:
			case 0x3B:

			{
						 _countResult[10]++;
						 break;
			}
			case 0x0C:
			case 0x1C:
			case 0x2C:
			case 0x3C:

			{
						 _countResult[11]++;
						 break;
			}
			case 0x0D:
			case 0x1D:
			case 0x2D:
			case 0x3D:

			{
						 _countResult[12]++;
						 break;
			}
			case 0x4E:
			{
						 _countResult[13]++;
						 break;
			}
			case 0x4F:
			{
						 _countResult[14]++;
						 break;
			}
			default:
				break;
			}
		}
	}
	for (int i = 0; i < 15; i++)
	{
		_textRecord[i]->setString(StringUtils::format("%d", _countResult[i]));
	}
}

void GameTableUI::setRecordEnable(bool visible)
{
	_cRecord->setEnabled(visible);
	_cRecord->addClickEventListener([=](Ref*){
		for (int i = 0; i < 3; i++)
		{
			if (_imgCard[i]->isVisible())
			{
				_imgCard[i]->setVisible(false);
				_imgRecord->setVisible(true);
			}
			else
			{
				_imgRecord->setVisible(false);
				_imgCard[i]->setVisible(true);

			}
		}
	});
}

void GameTableUI::setLordCard(bool visible)
{
	for (int i = 0; i < 3; i++)
	{
		_imgCard[i]->setVisible(visible);
	}
	
}

void GameTableUI::setRecordVis(bool visible)
{
	_imgRecord->setVisible(visible);
}

void GameTableUI::removeSetLayer()
{
	auto setLayout = this->getChildByName("setLayer");
	if (setLayout)
	{
		setLayout->removeFromParent();
	}
}

void GameTableUI::chatBtnClickCallBack(Ref* ref, Widget::TouchEventType type)
{
	if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
	{
		auto prompt = GamePromptLayer::create();
		prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
		return;
	}
	if (_chatLayer == nullptr)
	{
		//显示聊天界面
		gameChatLayer();
	}
	if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
	{
		_chatLayer->setCustomchat();
	}
	_chatLayer->showChatLayer();
}

void GameTableUI::setCardCountVis(BYTE seatNo,bool visible)
{
	_players[seatNo]->setCardCountVis(visible);
}

void GameTableUI::sendRecordData(S_C_GameRecordResult* data)
{
	int PlayerCount = 0;
	CHAR* szName[10];
	for (int j = 0; j < 10; j++)
	{
		szName[j] = "";
	}
	int index = 0;
	for (int i = 0; i < PLAY_COUNT; i++)
	{
		UserInfoStruct* pUser = _tableLogic->getUserBySeatNo(i);
		if (pUser)
		{
			PlayerCount++;
			szName[index] = pUser->nickName;
			index++;
		}
	}

	auto gameRecord = GameRecord::createWithData(szName, data->JuCount, data->GameCount, PlayerCount);
	addChild(gameRecord, 100000000);
}

void GameTableUI::CheckActivity()
{
	MSG_GR_I_GetDeskUserInfo DataInfo;
	DataInfo.iUserID = PlatformLogic()->loginResult.dwUserID;
	RoomLogic()->sendData(MDM_GM_GAME_FRAME, ASS_GM_GET_GAMENUM, &DataInfo, sizeof(DataInfo));
}

void GameTableUI::CheckReceive()
{
	MSG_GR_Get_RandNum DataInfo;
	DataInfo.iUserID = PlatformLogic()->loginResult.dwUserID;
	RoomLogic()->sendData(MDM_GM_GAME_FRAME, ASS_GM_GET_RANDNUM, &DataInfo, sizeof(DataInfo));
}

void GameTableUI::setActivityNum(int current, int toal, int CanTake)
{
	if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
	{
		//VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
		if (roomController)
		{
			roomController->setOpenActivity(true);
			roomController->setCurrentTask(current, toal);
			if (CanTake > 0)
			{
				roomController->setIsStandard(true);
				roomController->setCanTakeNum(CanTake);
			}
			else
			{
				roomController->setIsStandard(false);
			}
		}
	}
}

void GameTableUI::setReceiveState(int ReceiveType, int AwardNum)
{
	if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
	{
		//VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
		if (roomController)
		{
			roomController->setAnimationForTake(ReceiveType, AwardNum);
		}
	}
}

//赠送礼物回复
//void GameTableUI::dealGift(TMSG_SENG_GIFT_REQ data)
//{
//	int giftNo = data.iGiftNo;
//	if (data.isSendAllPlayers)
//	{
//		for (int i = 0; i < PLAY_COUNT; i++)
//		{
//			auto userInfo = UserInfoModule()->findUser(_deskNo, i);
//			if (userInfo && (i != data.bSenderSeatNo))
//			{
//				//移动礼物并播放动画
//				sendGift(data.bSenderSeatNo, i, giftNo);
//			}
//		}
//	}
//	else
//	{
//		//移动礼物并播放动画
//		sendGift(data.bSenderSeatNo, data.bReceiverSeatNo, giftNo);
//	}
//}

	//点击头像
	void GameTableUI::getUserInfo(Ref* pSender, Widget::TouchEventType type){
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		int curDir = ((GameUserHead*)pSender)->getTag();
		auto pLayer = GameUserPopupInfoLayer::create();
		pLayer->setCurDir(curDir);
		pLayer->setMaxPlayerCount(PLAY_COUNT);
		pLayer->setUserInfo(_vecUser, PLAY_COUNT);
		pLayer->setLogicBase(_tableLogic);
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				pLayer->setUserMoney(_ActionMoney, PLAY_COUNT);
			}
			else
			{
				pLayer->setUserMoney(_actionPoint, PLAY_COUNT);
			}
		}
		else
		{
			pLayer->setUserMoney(_ActionMoney, PLAY_COUNT);
		}
		pLayer->open(HNLayer::ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, 99999);
	}


	void GameTableUI::afterScoreByDeskStation(int Station, int money)
	{
		if (Station >= PLAY_COUNT)
		{
			return;
		}
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			_actionPoint[Station] += money;
			//更新自己的金币数目
			//int Seatno = _logic->logicToViewSeatNo(Station);
			//_players[Seatno].setUserMoney(_actionPoint[Station]);
		}
		else
		{
			_ActionMoney[Station] += money;
		}
	}

	void GameTableUI::UpDateUserLocation(int index)
	{
		//屏蔽地理位置请求
		if (true)
		{
			return;
		}
		if (_vecUser[index] == nullptr)
		{
			UpDateUserLocation(index + 1);
			return;
		}
		if (index > 2)
		{
			return;
		}
		//根据用户IP获取经纬度
		//*****************************************//
		float X = _vecUser[index]->fLat;
		float Y = _vecUser[index]->fLnt;
		if (X != 0 || Y != 0)
		{
			if (!_vecUserGeographic[index].empty())
			{
				UpDateUserLocation(index + 1);
				return;
			}
		}
		GameLocation::getInstance()->onGetLocationIP = [=](bool success, float latitude, float longtitude, const std::string& addr) {
			if (success)
			{
				float y = latitude;
				float x = longtitude;
				_vecUser[index]->fLat = longtitude;
				_vecUser[index]->fLnt = latitude;
				//straddName = addr;
				GameLocation::getInstance()->onGetAddrByLocation = [=](bool success, string& addr, string& province, string& city){
					if (success)
					{
						_vecUserGeographic[index] = addr;
					}
					UpDateUserLocation(index + 1);
				};
				GameLocation::getInstance()->getAddrByLocation(x, y);
			}
		};
		string strIP = Tools::parseIPAdddress(_vecUser[index]->dwUserIP);
		if (X == 0 || Y == 0)
		{
			GameLocation::getInstance()->getLocation(strIP);
		}
		else
		{
			if (_vecUserGeographic[index].empty())
			{
				GameLocation::getInstance()->getAddrByLocation(X, Y);
			}
		}
		//*****************************************//
	}
}

