/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "AppDelegate.h"

#include "HNLobbyExport.h"
#include "HNNetExport.h"
#include "HNUIExport.h"

#include "json/rapidjson.h"
#include "json/document.h"
#include "HNNetProtocol/HNBaseCommand.h"

/*******************************添加游戏****************************************/
//#include "GoldenToadMessageHead.h"
//#include "GoldenToadGameTableUI.h"

// #include "LandlordMessageHead.h"
// #include "LandlordGameTableUI.h"

// #include "GoldenFlowerMessageHead.h"
// #include "GoldenFlowerGameTableUI.h"

#include "CaiShenDDZMessageHead.h"
#include "CaiShenDDZGameTableUI.h"

#include "NN8MessageHead.h"
#include "NN8GameTableUI.h"

#include "CaiShenThirteenCardMessageHead.h"
#include "CaiShenThirteenCardGameTableUI.h"

#include "QZHOUMJ_MessageHead.h"
#include "QZHOUMJ_GameTableUI.h"

#include "QZHOUJJMJ_MessageHead.h"
#include "QZHOUJJMJ_GameTableUI.h"

#include "RunCrazyMessage.h"
#include "RunCrazyTableUI.h"

#include "ZPUMJ_MessageHead.h"
#include "ZPUMJ_GameTableUI.h"

//#include "NewThirteenCardMessageHead.h"
//#include "NewThirteenCardGameTableUI.h"

#include "ZZHOUMJ_MessageHead.h"
#include "ZZHOUMJ_GameTableUI.h"

#include "XMENMJ_MessageHead.h"
#include "XMENMJ_GameTableUI.h"
/******************************************************************************/

USING_NS_CC;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "dia2dump/dia2dump.h"
using namespace PDB;
#endif

static const char* GameLogoPath = "platform/game_logo.png";

// 游戏设计尺寸
static cocos2d::Size designResolutionSize = cocos2d::Size(1280, 720);

// 更新授权码
static std::string APP_INFO_KEY("ChessRoom-Lite");

// 游戏选项
struct GameItem
{
	// 游戏名称ID
	UINT nameId;
	// 游戏类型（普通，百人，单人）
	HNGameCreator::GAMETYPE type;
	// 游戏入口回调
	GAME_CREATE_SELECTOR create;
	// 游戏资源文件存放地址
	std::string storagePath;
};

/*******************************添加游戏****************************************/
static GameItem GameList[] =
{
	{ CaiShenDDZ::GAME_NAME_ID, HNGameCreator::NORMAL, CaiShenDDZ::GameTableUI::create, "Games/CaiShenDDZ/" },
 	{ NN8::NAME_ID, HNGameCreator::NORMAL, NN8::GameTableUI::create, "Games/NN8/" },
	{ CaiShenThirteenCard::GAME_NAME_ID, HNGameCreator::NORMAL, CaiShenThirteenCard::GameTableUI::create, "Games/CaiShenThirteenCard/" },
 	{ QZHOUMJ::NAME_ID, HNGameCreator::NORMAL, QZHOUMJ::GameTableUI::create, "Games/QZHOUMJ/" },
	{ QZHOUMJ::YJNAME_ID, HNGameCreator::NORMAL, QZHOUMJ::GameTableUI::create, "Games/QZHOUMJ/" },
	{ QZHOUJJMJ::NAME_ID, HNGameCreator::NORMAL, QZHOUJJMJ::GameTableUI::create, "Games/QZHOUJJMJ/" },
	{ RunCrazy::GAME_NAME_ID, HNGameCreator::NORMAL, RunCrazy::GameTableUI::create, "Games/RunCrazy/" },
	{ RunCrazy::NEW_GAME_ID, HNGameCreator::NORMAL, RunCrazy::GameTableUI::create, "Games/RunCrazy/" },
	{ RunCrazy::NEW_OUTGAME_ID, HNGameCreator::NORMAL, RunCrazy::GameTableUI::create, "Games/RunCrazy/" },
	{ ZPUMJ::NAME_ID, HNGameCreator::NORMAL, ZPUMJ::GameTableUI::create, "Games/ZPUMJ/" },
 	{ ZZHOUMJ::NAME_ID, HNGameCreator::NORMAL, ZZHOUMJ::GameTableUI::create, "Games/ZZHOUMJ/" },
 	{ XMENMJ::NAME_ID, HNGameCreator::NORMAL, XMENMJ::GameTableUI::create, "Games/XMENMJ/" },
};
/******************************************************************************/

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	Cleanup();
#endif
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
	//set OpenGL context attributions,now can only set six attributions:
	//red,green,blue,alpha,depth,stencil
	GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };

	GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
	return 0; //flag for packages manager
}

static bool isShowConsole()
{
	bool bShow = true;
	do
	{
		std::string configfile("config.json");
		if (cocos2d::FileUtils::getInstance()->isFileExist(configfile))
		{
			std::string json = cocos2d::FileUtils::getInstance()->getStringFromFile(configfile);
			rapidjson::Document doc;
			doc.Parse<rapidjson::kParseDefaultFlags>(json.c_str());
			if (doc.HasParseError() || !doc.IsObject())
			{
				break;
			}

			std::string select_server = "server";
			if (doc.HasMember("select_server"))
			{
				select_server = doc["select_server"].GetString();
			}
			if (doc.HasMember(select_server.c_str()))
			{
				if (doc[select_server.c_str()].HasMember("isConsole"))
				{
					bShow = doc[select_server.c_str()]["isConsole"].GetBool();
				}
			}
		}
	} while (0);
	return bShow;
}

bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if (!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
		Size size = getConfigSize();
		glview = GLViewImpl::createWithRect("Redbird", Rect(0, 0, size.width, size.height));
#else
		glview = GLViewImpl::create("Redbird");
#endif
		director->setOpenGLView(glview);
	}

	// turn on display FPS
	//director->setDisplayStats(true);

	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0f / 60);

	// Set the design resolution
	glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::SHOW_ALL);
	Size frameSize = glview->getFrameSize();
	//glview->setFrameZoomFactor(0.5f);
	register_all_packages();

	//加载符号
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

	char exeFullPath[MAX_PATH];
	memset(exeFullPath, 0, MAX_PATH);

	GetModuleFileNameA(NULL, exeFullPath, MAX_PATH);
	/*
	char *exe_name = strrchr(exeFullPath, '\\');
	char *directory = strchr(exeFullPath, '\\');
	*/
	std::wstring path = utf8_to_unicode(exeFullPath);
	std::wstring::size_type   pos(0);
	if ((pos = path.find(L".exe")) != std::wstring::npos)
	{
		path.replace(pos, 4, L".pdb");	
	}
	Init((wchar_t*)path.c_str());
#endif

	//添加控制台输出
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	if (isShowConsole())
	{
		// create console window
		AllocConsole();
		HWND hwndConsole = GetConsoleWindow();
		if (hwndConsole != NULL)
		{
			ShowWindow(hwndConsole, SW_SHOW);
			BringWindowToTop(hwndConsole);
			freopen("CONOUT$", "wt", stdout);
			freopen("CONOUT$", "wt", stderr);

			HMENU hmenu = GetSystemMenu(hwndConsole, FALSE);
			if (hmenu != NULL)
			{
				DeleteMenu(hmenu, SC_CLOSE, MF_BYCOMMAND);
			}
		}
	}
#endif

	initLogSystem();

	initGameConfig();

	HNAudioEngine::initAudio();
	Scene * scene = GameInitial::createScene();
	director->runWithScene(scene);

	// create a scene. it's an autorelease object
	/*auto scene = HelloWorld::createScene();
	director->runWithScene(scene);*/

	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	//Director::getInstance()->pause();
	Director::getInstance()->stopAnimation();
	//HNAudioEngine::getInstance()->pauseBackgroundMusic();
	HNAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);

	Configuration::getInstance()->setValue("bBackground", Value(true));

	// 切后台保存当前系统时间
	UINT nowTime = time(NULL);
	Configuration::getInstance()->setValue("bkTime", Value(nowTime));
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
	//Director::getInstance()->resume();
	Director::getInstance()->startAnimation();
	//HNAudioEngine::getInstance()->resumeBackgroundMusic();
	int value = UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT, 0);
	HNAudioEngine::getInstance()->setBackgroundMusicVolume(value);

	Configuration::getInstance()->setValue("bBackground", Value(false));

	// 切回前台派发通知
	EventCustom event(FOCEGROUND);
	Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
}

Size AppDelegate::getConfigSize()
{
	Size size = designResolutionSize;
	do
	{
		std::string filename("config.json");
		if (FileUtils::getInstance()->isFileExist(filename))
		{
			std::string json = FileUtils::getInstance()->getStringFromFile(filename);
			rapidjson::Document doc;
			doc.Parse<rapidjson::kParseDefaultFlags>(json.c_str());
			if (doc.HasParseError() || !doc.IsObject())
			{
				break;
			}

			std::string select_server = "server";
			if (doc.HasMember("select_server"))
			{
				select_server = doc["select_server"].GetString();
			}
			if (doc.HasMember(select_server.c_str()))
			{
				if (doc[select_server.c_str()].HasMember("width"))
				{
					size.width = doc[select_server.c_str()]["width"].GetInt();
				}
				if (doc[select_server.c_str()].HasMember("height"))
				{
					size.height = doc[select_server.c_str()]["height"].GetInt();
				}
			}		
		}
	} while (0);

	return size;
}

void AppDelegate::initGameConfig()
{
	FileUtils::getInstance()->addSearchPath(FileUtils::getInstance()->getWritablePath(), true);
	FileUtils::getInstance()->addSearchPath(FileUtils::getInstance()->getWritablePath() + "Games", true);
	FileUtils::getInstance()->addSearchPath("Games");
	FileUtils::getInstance()->setXXTeaKey(getXXTEA_KEY());
	//PlatformConfig::getInstance()->setGameLogo(GameLogoPath);

	initConfigUrl();

	// 添加游戏
	int gameCount = sizeof(GameList) / sizeof(GameItem);
	for (int i = 0; i < gameCount; i++)
	{
		// 添加游戏列表
		GameCreator()->addGame(GameList[i].nameId, GameList[i].type, GameList[i].create);

		// 添加更新列表
		UpdateInfoModule()->addGame(GameList[i].nameId, GameList[i].storagePath);
	}

	// 设置游戏授权码
	HNPlatformConfig()->setAppKey(APP_INFO_KEY);

	// 是否开启苹果内购
	HNPlatformConfig()->setIsIAP(false);

	// 平台设计尺寸
	HNPlatformConfig()->setPlatformDesignSize(designResolutionSize);

	// 游戏设计尺寸
	HNPlatformConfig()->setGameDesignSize(designResolutionSize);

	// 获取商品列表
	//ProductManger::getInstance()->addProducts("mixproject");

	// 初始化bugly
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	HNLog::initBugly("4faa8e6b77", false, CrashReport::Error);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	HNLog::initBugly("addb5247fc", false, CrashReport::Error);
#endif
}

void AppDelegate::initLogSystem()
{
	// 启动日志
	std::string path = HN::Operator::requestChannel("sysmodule", "getSdcardPath");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
	path += "HNLog";
#endif
	HNLog::logDebug("initLogSystem:%s", path.c_str());
	log("getSdcardPath:%s", path.c_str());
	HNLOGEX_START(path);

	// bugly日志测试
	//HNLOG_BUGLY_LOG("getSdcardPath:%s", path.c_str());

	//HNLOG_EXCEPTION("getSdcardPath:%s", path.c_str());
}
