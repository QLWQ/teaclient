LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNNet)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNUI)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNMarket)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLobby)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLogic)

LOCAL_MODULE := QZHOUMJ_static

LOCAL_MODULE_FILENAME := libQZHOUMJ

LOCAL_SRC_FILES := ../Classes/GameTable/QZHOUMJ_GameTableLogic.cpp \
                   ../Classes/GameTable/QZHOUMJ_GameManager.cpp \
                   ../Classes/GameTable/QZHOUMJ_ResourceLoader.cpp \
				   ../Classes/GameTable/QZHOUMJ_CardPool.cpp \
				   ../Classes/GameTable/QZHOUMJ_GameTableUI.cpp \
				   ../Classes/GameTable/QZHOUMJ_PoolAction.cpp \
				   ../Classes/GameTable/QZHOUMJ_HuCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_Card.cpp \
				   ../Classes/GameTable/QZHOUMJ_SouthMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUMJ_EastMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUMJ_WestHandCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_SouthHandCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_EastHandCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_WestMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUMJ_MahjongCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_NorthMahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUMJ_NorthHandCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_ConcealedKong.cpp \
				   ../Classes/GameTable/QZHOUMJ_Factory.cpp \
				   ../Classes/GameTable/QZHOUMJ_MahjongCardPool.cpp \
				   ../Classes/GameTable/QZHOUMJ_MeldedKong.cpp \
				   ../Classes/GameTable/QZHOUMJ_TouchCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_TouchKong.cpp \
				   ../Classes/GameTable/QZHOUMJ_Result.cpp \
				   ../Classes/GameTable/QZHOUMJ_TurnTable.cpp \
				   ../Classes/GameTable/QZHOUMJ_GMLayer.cpp \
				   ../Classes/GameTable/QZHOUMJ_HandCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_ActionButtonList.cpp \
				   ../Classes/GameTable/QZHOUMJ_SetLayer.cpp \
				   ../Classes/GameTable/QZHOUMJ_EatCard.cpp \
				   ../Classes/GameTable/QZHOUMJ_CheatLayer.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMessage \
                    $(LOCAL_PATH)/../Classes/GameTable \
                    $(LOCAL_PATH)/../../ \
                    $(LOCAL_PATH)/../ 

LOCAL_EXPORT_LDLIBS := -llog \
                       -lz \
                       -landroid     
                   
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_net_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_market_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_lobby_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_logic_static

LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static


include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)
$(call import-module,editor-support/cocostudio)
$(call import-module,extensions)

$(call import-module,LibHNUI)
$(call import-module,LibHNNet)
$(call import-module,LibHNMarket)
$(call import-module,LibHNLogic)
$(call import-module,LibHNLobby)