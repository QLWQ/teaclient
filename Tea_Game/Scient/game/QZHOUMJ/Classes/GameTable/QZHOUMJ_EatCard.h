#ifndef _QZHOUMJ_EATACTION_H_
#define _QZHOUMJ_EATACTION_H_
#include "QZHOUMJ_poolaction.h"

namespace QZHOUMJ
{

	class EatCard :
		public PoolAction
	{
	public:
		EatCard(void);
		~EatCard(void);

		virtual void run() override;

		CREATE_FUNC(EatCard);

	};

}
#endif