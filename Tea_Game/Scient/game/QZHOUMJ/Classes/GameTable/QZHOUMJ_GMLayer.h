#ifndef QZHOUMJ_GMLayer_h__
#define QZHOUMJ_GMLayer_h__
#include "HNNetExport.h"

namespace QZHOUMJ
{
	class QZHOUMJ_GMLayer : public HNLayer
	{
	public:
		QZHOUMJ_GMLayer();
		~QZHOUMJ_GMLayer();

		CREATE_FUNC(QZHOUMJ_GMLayer);

		void setUIData(std::vector<UserInfoStruct> users,std::vector<std::vector<INT>> cards);
	private:
		virtual bool init() override;
		void clickCloseEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		
		bool LeftCardCallBack(HNSocketMessage* socketMessage);

	private:
		cocos2d::Node*	m_pMainNode;
	};
}



#endif // HSMJ_GMLayer_h__
