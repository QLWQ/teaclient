#include "QZHOUMJ_PoolAction.h"
#include "QZHOUMJ_GameManager.h"
namespace QZHOUMJ
{

	PoolAction::PoolAction(void)
	{
		_runSp = nullptr;
	}

	PoolAction::~PoolAction(void)
	{
		_vSpFrame.clear();
	}

	void PoolAction::setInfo(sitDir dir, const INT& cardNumber)
	{
		_cardNumber = cardNumber;
		_dir = dir;
		run();
	}

	void PoolAction::uiAction(std::string name)
	{
		Vec2 pos;
		switch (_dir)
		{
		case QZHOUMJ::SOUTH_DIR:
			pos = CardPool::SounthPGActionCardPos;
			break;
		case QZHOUMJ::WEST_DIR:
			pos = CardPool::WestPGActionCardPos;
			break;
		case QZHOUMJ::EAST_DIR:
			pos = CardPool::EastPGActionCardPos;
			break;
		case QZHOUMJ::NORTH_DIR:
			pos = CardPool::NorthPGActionCardPos;
			break;
		default:
			break;
		}
		
		//播放动画
		 getAni(name,pos);
		///*if (name)
		//if (!_runSp->isVisible())
		//{
		//	_runSp->setPosition(pos);
		//	_runSp->runAction(
		//		Sequence::create(
		//		CallFunc::create(CC_CALLBACK_0(Node::setVisible, _runSp, true)),
		//		Animate::create(animation),
		//		CallFunc::create(CC_CALLBACK_0(Node::setVisible, _runSp, false)),
		//		nullptr));
		//}
		//else
		//{
		//	auto sp = Sprite::createWithSpriteFrameName("huAction1.png");
		//	this->addChild(sp);
		//	sp->setVisible(false);
		//	sp->setPosition(pos);
		//	sp->runAction(
		//		Sequence::create(
		//		CallFunc::create(CC_CALLBACK_0(Node::setVisible, _runSp, true)),
		//		Animate::create(animation),
		//		CallFunc::create(CC_CALLBACK_0(Node::setVisible, _runSp, false)),
		//		CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, sp)),
		//		nullptr));
		//}*/







	}
	
	void  PoolAction::getAni(std::string name,Vec2 pos)
	{
		

		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo(ANM3_PATH + "gang0.png", ANM3_PATH + "gang0.plist", ANM3_PATH + "gang.ExportJson");
		auto  armature = cocostudio::Armature::create("gang");
		armature->getAnimation()->play(name);
		armature->setScale(1.0);
		armature->setPosition(pos);
		armature->setVisible(true);
		this->addChild(armature, 1000);
		std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)

		{
			if (type == MovementEventType::COMPLETE)
			{
				armature->getAnimation()->stop();
				armature->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature)), NULL));
			}
		};
		armature->getAnimation()->setMovementEventCallFunc(armatureFun);
	}

	void PoolAction::playSexActionSound(bool isMan, std::string actionName)
	{
		// head
		auto fullName = MUSIC_SEX_PATH;
		if (isMan)
		{
			if (GameManager::getInstance()->_IsPutongHua)
			{
				fullName += "Man/";
			}
			else
			{

				fullName += "M_Man/";
			}
		}
		else
		{
			if (GameManager::getInstance()->_IsPutongHua)
			{
				fullName += "Wom/";
			}
			else
			{

				fullName += "M_Wom/";
			}
		}

		fullName += actionName;

		fullName += ".mp3";

		HNAudioEngine::getInstance()->playEffect(fullName.c_str());
	}

	void PoolAction::playSexNumberSound(bool isMan, INT cardNumber)
	{
		// head
		auto fullName = MUSIC_SEX_PATH;
		if (isMan)
		{
			if (GameManager::getInstance()->_IsPutongHua)
			{
				fullName += "Man/";
			}
			else
			{

				fullName += "M_Man/";
			}

		}
		else
		{
			if (GameManager::getInstance()->_IsPutongHua)
			{
				fullName += "Wom/";
			}
			else
			{

				fullName += "M_Wom/";
			}
		}

		// value init
		int maxNum = 9;
		std::string fullFileName[99];
		// 万
		auto begin = CMjEnum::MJ_TYPE_W1 %10;
		for (auto i = begin; i < begin + 9; i++)
		{
			fullFileName[CMjEnum::MJ_TYPE_W1 + i - 1] = StringUtils::format("%dwan", i);
		}
		// 条
		begin = CMjEnum::MJ_TYPE_T1 %10;
		for (auto i = begin; i < begin + 9; i++)
		{
			fullFileName[CMjEnum::MJ_TYPE_T1 + i - 1] = StringUtils::format("%dtiao", i);
		}
		// 筒
		begin = CMjEnum::MJ_TYPE_B1 %10;
		for (auto i = begin; i < begin + 9; i++)
		{
			fullFileName[CMjEnum::MJ_TYPE_B1 + i - 1] = StringUtils::format("%dtong", i);
		}
		fullFileName[CMjEnum::MJ_TYPE_FD] = "dongfeng";
		fullFileName[CMjEnum::MJ_TYPE_FN] = "nanfeng";
		fullFileName[CMjEnum::MJ_TYPE_FX] = "xifeng";
		fullFileName[CMjEnum::MJ_TYPE_FB] = "beifeng";
		// 红中
		fullFileName[CMjEnum::MJ_TYPE_ZHONG] = "zhong";
		fullFileName[CMjEnum::MJ_TYPE_FA] = "fa";
		fullFileName[CMjEnum::MJ_TYPE_BAI] = "bai";
		fullFileName[98] = "buhua";
		// tail
		fullName +=  fullFileName[cardNumber];
		fullName += ".mp3";
		HNAudioEngine::getInstance()->playEffect(fullName.c_str());
	}

}