#include "QZHOUMJ_HandCard.h"

namespace QZHOUMJ
{
	HandCard::HandCard()
	{
	}

	HandCard::~HandCard()
	{
	}

	bool HandCard::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		return true;
	}

	Vec2 HandCard::refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>normalCards,INT catchCard)
	{
		return Vec2::ZERO;
	}

	Vec2 HandCard::refreshHandAllCard(std::vector<CardPool::CGroupCardData>groupCards, std::vector<INT>normalCards, INT catchCard)
	{
		return Vec2::ZERO;
	}

	Vec2 HandCard::refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>tingCards)
	{
		return Vec2::ZERO;
	}

	Vec2 HandCard::refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,INT huCardId,bool visibleAllCard)
	{
		return Vec2::ZERO;
	}

	cocos2d::Vec2 HandCard::getCatchWorldPos()
	{
		return Vec2::ZERO;
	}

	void HandCard::enterChangeCards()
	{

	}

	void HandCard::exitChangeCards()
	{

	}

	void HandCard::putChangeCardToHead(std::vector<INT> cards)
	{

	}

	void HandCard::setLeftChangeCard(std::vector<INT> cards)
	{
		_leftChangeCard.clear();
		for (auto v:cards)
		{
			_leftChangeCard.push_back(v);
		}
	}

	void HandCard::removeChangeCard(INT card)
	{
		std::vector<INT>::iterator itr = std::find(_leftChangeCard.begin(),_leftChangeCard.end(),card);
		if (itr != _leftChangeCard.end())
		{
			_leftChangeCard.erase(itr);
		}

	}

	void HandCard::addDrawChangeCard(INT card)
	{
		_drawChangeCard.push_back(card);
	}

	bool HandCard::isCanDrawChangeCard(INT card)
	{
		std::vector<INT>::iterator itr = std::find(_leftChangeCard.begin(),_leftChangeCard.end(),card);
		if (itr ==_leftChangeCard.end())
		{
			return false;
		}

		if (_drawChangeCard.size()>=_leftChangeCard.size())
		{
			return false;
		}

		//查找是否还有没有绘制的交换牌
		std::vector<INT> temp;
		for (auto v:_leftChangeCard)
		{
			temp.push_back(v);
		}

		for (auto v:_drawChangeCard)
		{
			std::vector<INT>::iterator itr = std::find(temp.begin(),temp.end(),v);
			if (itr != temp.end())
			{
				temp.erase(itr);
			}
		}

		itr = std::find(temp.begin(),temp.end(),card);
		return itr != temp.end();

	}

}
