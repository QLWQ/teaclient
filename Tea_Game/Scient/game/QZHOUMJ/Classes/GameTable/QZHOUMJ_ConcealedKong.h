#ifndef _QZHOUMJ_CONCEALEDKONG_H_
#define _QZHOUMJ_CONCEALEDKONG_H_

#include "QZHOUMJ_poolaction.h"

namespace QZHOUMJ
{

	class ConcealedKong :
		public PoolAction
	{
	public:
		ConcealedKong(void);
		~ConcealedKong(void);

		virtual void run() override;

		CREATE_FUNC(ConcealedKong);

	};

}
#endif