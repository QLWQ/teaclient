﻿#ifndef _QZHOUMJ_GAMETABLE_H
#define _QZHOUMJ_GAMETABLE_H

/*****************************************************/
// hn.h
#include "QZHOUMJ_MessageHead.h"
#include "QZHOUMJ_GameTableUICallback.h"
#include "QZHOUMJ_GameTableLogic.h"
#include "QZHOUMJ_GameManager.h"
#include "QZHOUMJ_Result.h"

/*****************************************************/

namespace QZHOUMJ
{
	// namespace
	USING_NS_CC;
	using namespace HN;
	using namespace ui;
	using namespace std;

	class GameTableUI : public HNGameUIBase, GameTableUICallBack
	{
	public: // create
		GameTableUI();
		virtual ~GameTableUI();
		static GameTableUI* create(INT deskNo, bool bAutoCreate);
		bool init(INT deskNo, bool autoCreate);
		void showBackBtn(bool visible);
		void setIsKeEnd(bool Isend);
		void setFuFeiRule(bool isFangzhu);
	public: 
		// 游戏状态
		virtual void dealLeaveDesk() override;    // 离开桌子
		virtual void addUser(const sitDir& dir,  UserInfoStruct * user);     // 进入桌子
		virtual void UpDateUserLocation(int index) override;
		virtual void removeUser(const sitDir& dir);    // 离开桌子
		virtual void agreeGame(const sitDir& dir) override;
		
		virtual void outCard(const sitDir& dir, const INT& number) override;
		virtual void catchCard(const sitDir& dir, const INT& number) override;
		virtual void setDissloveBtState() override;
		virtual void setGamecount(int counts) override;
		virtual void showWechat(bool visible) override;
		

		virtual Node* createGameFinishNode() override;
		virtual void checkIpSameTips() override;
		virtual void showGameDeskNotFound()override;

		virtual void setDeskInfo() override;
		void onCloseResult();
		virtual void onChatTextMsg(BYTE seatNo, CHAR msg[]);											// 处理文本聊天消息
		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime);	                        // 处理语音聊天消息	
		// 动作聊天信息
		virtual void onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index);
		void gameChatLayer();                                                                           // 聊天界面
		void removeSetLayer();

		virtual void sendRecordData(S_C_GameRecordResult* data);										//战绩数据
		virtual void	setBottomScore(int Score);

		//更新游戏的进度
		virtual void	setActivityNum(int Current, int toal, int CanTake);
		//更新游戏红包中奖
		virtual void setReceiveState(int ReceiveType, int AwardNum);
	private:
		virtual void openChatDialog();
		virtual void onSocketMessage(UINT MainID, UINT AssistantID,const rapidjson::Document& doc);
		virtual void onGameDisconnect(bool isReconnect);
	private:   
		void clickWebchatEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void clickBackEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void clickSetEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);//设置按钮
		
		void initDissolveRoomOpervation(); //添加解散房间功能
		void calSameIpUser(std::vector<UserInfoStruct*>& same);
	private:						
		GameTableLogic*			_tableLogic;				// 逻辑层
		GameManager*			_mahjongManager;
		Label*					_labekaCount;				//当前房间剩余局数
		Button*					_inviteBtn;                 //微信邀请按钮
		sitDir					_startCatchDir;
		int						_deskNo;
		Button*                 _backBtn;                 //返回大厅按钮
		bool					_isGameEnd;				//是否会是当前轮结束
		bool					_isFangzhuFufei;		//付费规则
		GameChatLayer* _chatLayer = nullptr;
		VipRoomController* roomController = nullptr;
		VipRoomLogic* _logic = nullptr;
		bool _isKe = false;//判断是否是课
		bool	_allRoundsEnd = false;
		MJGameResult* m_pResultUI = nullptr;//结算页面
		Layout*					_BtnLayout = nullptr;
		bool					_isShowAllBtn;
		Label*         _BottonScore;
		
	};

}
#endif
