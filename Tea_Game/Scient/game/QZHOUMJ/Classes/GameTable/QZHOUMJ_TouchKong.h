#ifndef _QZHOUMJ_TOUCHKONG_H_
#define _QZHOUMJ_TOUCHKONG_H_

#include "QZHOUMJ_poolaction.h"

namespace QZHOUMJ
{

	class TouchKong :
		public PoolAction
	{
	public:
		TouchKong(void);
		~TouchKong(void);

		virtual void run() override;

		CREATE_FUNC(TouchKong);
	};

}

#endif
