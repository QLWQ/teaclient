#ifndef _QZHOUMJ_TOUCHCARD_H_
#define _QZHOUMJ_TOUCHCARD_H_

#include "QZHOUMJ_poolaction.h"

namespace QZHOUMJ
{
	class TouchCard :
		public PoolAction
	{
	public:
		TouchCard(void);
		~TouchCard(void);

		virtual void run() override;

		CREATE_FUNC(TouchCard);
	};

}

#endif