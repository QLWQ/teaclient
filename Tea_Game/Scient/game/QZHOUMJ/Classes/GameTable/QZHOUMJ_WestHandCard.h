#ifndef QZHOUMJ_WestHandCard_h__
#define QZHOUMJ_WestHandCard_h__

#include "cocos2d.h"
#include "QZHOUMJ_CardPool.h"
#include "QZHOUMJ_HandCard.h"
using namespace cocos2d;

/*
�������°�
*/

namespace QZHOUMJ
{
	class QZHOUMJ_WestHandCard : public HandCard
	{
	public:
		QZHOUMJ_WestHandCard();
		~QZHOUMJ_WestHandCard();

		CREATE_FUNC(QZHOUMJ_WestHandCard);

		virtual Vec2 refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>normalCards,INT catchCard)override;
		virtual Vec2 refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>tingCards)override;
		virtual Vec2 refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,INT huCardId,bool visibleAllCard)override;
		virtual void setSingleGroupSize(Size groupSize)override;
		virtual void setNormalCardSize(Size normalSize)override;
		virtual void setTingCardSize(Size tingSize)override;
		virtual	Vec2 getCatchWorldPos()override;
		virtual void putChangeCardToHead(std::vector<INT> cards)override;
		virtual void moveChangeCard(sitDir dir)override;

		void resetData();

	private:
		virtual bool init() override;
		void refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards);
		Vec2 getNextPosition(Vec2 prePoint, float deltLen);

	private:
		int		_beginZorder;
		Vec2	_prePoint;
	
	};
}



#endif // HSMJ_WestHandCard_h__
