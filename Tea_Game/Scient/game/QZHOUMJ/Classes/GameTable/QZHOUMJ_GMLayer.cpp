#include "QZHOUMJ_GMLayer.h"
#include "QZHOUMJ_MessageHead.h"
#include "QZHOUMJ_MahjongCard.h"

namespace QZHOUMJ
{
	QZHOUMJ_GMLayer::QZHOUMJ_GMLayer()
	{
	}

	QZHOUMJ_GMLayer::~QZHOUMJ_GMLayer()
	{
		RoomLogic()->removeEventSelector(MDM_GM_GAME_NOTIFY,GAME_PROTOCOL_S2C_RequestLeftCard);
	}

	
	bool QZHOUMJ_GMLayer::init()
	{
		if (!HNLayer::init())
		{
			return false;
		}


		//
		Size size = Director::getInstance()->getWinSize();
		m_pMainNode = CSLoader::createNode(COCOS_PATH+"HSZ_gm_layer.csb");
		m_pMainNode->setPosition(cocos2d::Vec2(size.width/2.0f, size.height/2.0f));
		this->addChild(m_pMainNode, 100);

		auto pBgNode = m_pMainNode->getChildByName("bg");
		Button* closeBtn = dynamic_cast<Button*>(m_pMainNode->getChildByName("closeButton"));
		closeBtn->addTouchEventListener(CC_CALLBACK_2(QZHOUMJ_GMLayer::clickCloseEventCallback, this));

		GameProtoC2SRequestLeftCard_t temp;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY,GAME_PROTOCOL_C2S_RequestLeftCard,&temp,sizeof(temp));
		RoomLogic()->addEventSelector(MDM_GM_GAME_NOTIFY, GAME_PROTOCOL_S2C_RequestLeftCard, HN_SOCKET_CALLBACK(QZHOUMJ_GMLayer::LeftCardCallBack, this));
		
		auto pText = m_pMainNode->getChildByName("text");
		pText->setVisible(false);


		return true;
	}

	void QZHOUMJ_GMLayer::setUIData(std::vector<UserInfoStruct> users, std::vector<std::vector<INT>> cards)
	{
		auto pBgNode = m_pMainNode->getChildByName("bg");
		for (int index = 0; index < 3; ++ index) //(auto v:users)
		{
			auto node = m_pMainNode->getChildByName(StringUtils::format("player%d", index));
			auto text = dynamic_cast<ui::Text*>(node->getChildByName("name"));
			if (index < users.size()) 
			{
				auto user = users[index];
				node->setVisible(true);
				text->setString(user.nickName);

				GameUserHead*	userHead = nullptr;
				userHead = GameUserHead::create(dynamic_cast<ImageView*>(node->getChildByName("head")));
				userHead->show();
				userHead->setTag(index);
				std::string name = user.bBoy ? Player_Normal_M : Player_Normal_W;
				userHead->loadTexture(name);
				userHead->setHeadByFaceID(user.bLogoID);
				userHead->loadTextureWithUrl(user.headUrl);
				userHead->setVIPHead("", user.iVipLevel);
				userHead->setPosition(CCPoint());
			}
			else
			{
				node->setVisible(false);
			}
		}

		int line = 0;
		//��
		for (auto v: cards)
		{
			int cardIndex = 0;
			for (auto vv: v)
			{
				MahjongCard* pCard = MahjongCard::create(DI_SOUTH_STAND,SOUTH_DIR,vv);
				pCard->setScale(0.45f);
				auto cardSize = pCard->getCardSize() * pCard->getScale();
				pCard->setPosition(Vec2(90 + cardSize.width * cardIndex, (355 - cardSize.height - 10) - line * 100));
				pBgNode->addChild(pCard);
				cardIndex++;
			}

			line++;
		}
	}


	void QZHOUMJ_GMLayer::clickCloseEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype)	
		{
			return;
		}

		this->removeFromParentAndCleanup(true);

	}

	bool QZHOUMJ_GMLayer::LeftCardCallBack(HNSocketMessage* socketMessage)
	{
		GameProtoS2CRequestLeftCard_t* card = (GameProtoS2CRequestLeftCard_t*)(socketMessage->object);
		if (socketMessage->objectSize != sizeof(GameProtoS2CRequestLeftCard_t))
		{
			return true;
		}

		auto pBgNode = m_pMainNode->getChildByName("bg");

		auto pText = m_pMainNode->getChildByName("text");

		//ȡ�������
		//int max = MIN(card->leftCount,6);
		const int AllShowCardNum = 9;
		int beginIndex = card->leftCount - AllShowCardNum;
		int endIndex = card->leftCount;
		if(beginIndex<0) beginIndex = 0;

		pText->setVisible(endIndex > beginIndex);

		for (int i = 0; (i + beginIndex) < endIndex; ++i)
		{
			MahjongCard* pCard = MahjongCard::create(DI_SOUTH_STAND,SOUTH_DIR,card->leftCardArray[i]);
			pCard->setScale(0.45f);
			auto cardSize = pCard->getCardSize() * pCard->getScale();
			pCard->setPosition(Vec2(90 + cardSize.width * i, 10));
			pBgNode->addChild(pCard);
		}

		return true;
	}

}

