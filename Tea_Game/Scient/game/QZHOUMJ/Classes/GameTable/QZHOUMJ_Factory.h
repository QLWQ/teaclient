#ifndef _QZHOUMJ_FACTORYCARDPOOL_H_
#define _QZHOUMJ_FACTORYCARDPOOL_H_

#include "QZHOUMJ_CardPool.h"
#include "QZHOUMJ_MahjongCardPool.h"
#include "QZHOUMJ_SouthMahjongCardPool.h"
#include "QZHOUMJ_WestMahjongCardPool.h"
#include "QZHOUMJ_EastMahjongCardPool.h"
#include "QZHOUMJ_NorthMahjongCardPool.h"

#include "QZHOUMJ_TouchCard.h"
#include "QZHOUMJ_MeldedKong.h"
#include "QZHOUMJ_ConcealedKong.h"
#include "QZHOUMJ_TouchKong.h"
#include "QZHOUMJ_HuCard.h"
#include "QZHOUMJ_EatCard.h"

namespace QZHOUMJ
{

	class Factory
	{
	public:
		static CardPool* createEastPool(INT count);
		static CardPool* createWestPool(INT count);
		static CardPool* createSouthPool(INT count);
		static CardPool* createNorthPool(INT count);

		static PoolAction* createTouchCardAction();
		static PoolAction* createMeldedKongAction();
		static PoolAction* createConcealedKongAction();
		static PoolAction* createTouchKongAction();
		static PoolAction* createTouchEatAction();
		static PoolAction* createHuCardAction();
	};

}

#endif