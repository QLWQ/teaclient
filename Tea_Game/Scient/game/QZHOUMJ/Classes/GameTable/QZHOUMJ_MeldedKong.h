#ifndef _QZHOUMJ_MELDEDKONG_H_
#define _QZHOUMJ_MELDEDKONG_H_

#include "QZHOUMJ_poolaction.h"

namespace QZHOUMJ
{

	class MeldedKong :
		public PoolAction
	{
	public:
		MeldedKong(void);
		~MeldedKong(void);

		virtual void run() override;

		CREATE_FUNC(MeldedKong);
	};

}

#endif