#ifndef _QZHOUMJ_MAHJONGCARDPOOL_H_
#define _QZHOUMJ_MAHJONGCARDPOOL_H_

#include "QZHOUMJ_CardPool.h"
#include "QZHOUMJ_MahjongCard.h"

namespace QZHOUMJ
{
#define CREATE_COUNT(_className) \
	static _className * create(INT count) \
	{ \
		auto p = new _className; \
		if (p && p->init(count)) \
		{ \
			p->autorelease(); \
			return p; \
		} \
		delete p; \
		return nullptr; \
	} 

	class MahjongCardPool :
		public CardPool
	{
	public:
		MahjongCardPool(void);
		~MahjongCardPool(void);

		
		virtual bool init() override;
		virtual void TouchCard(Card* tcard){};//������е���
		
	protected:
		Node*				_huTips;
	};

}

#endif