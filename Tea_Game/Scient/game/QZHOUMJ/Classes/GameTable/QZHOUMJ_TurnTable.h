#ifndef QZHOUMJ_TurnTable_h__
#define QZHOUMJ_TurnTable_h__

#include "cocos2d.h"
#include "QZHOUMJ_MessageHead.h"
using namespace cocos2d;

namespace  QZHOUMJ
{
	class QZHOUMJ_TurnTable :public Layer
	{
	public:
		QZHOUMJ_TurnTable();
		~QZHOUMJ_TurnTable();

		CREATE_FUNC(QZHOUMJ_TurnTable);

		void turnTableDir(sitDir dir);
		void setTurnInitDir(sitDir dir);
		void setCardCount(int count);
		void setLeftCardVisble(bool isVisble);
		void setAllinit(sitDir dir);
		void showTimeCountAction(bool isVisble,int time);
		void updateTimeCount(float dt);
	private:
		virtual bool init();

	private:
		Node*	_loader;
		Sprite*	_qianHouSiTips;
		Text*	_qianSiHouSiText;
		TextAtlas* _timeCount;
		TextBMFont* _text_CardNum;
		int _nowTime;
	};
}



#endif // HSMJ_TurnTable_h__
