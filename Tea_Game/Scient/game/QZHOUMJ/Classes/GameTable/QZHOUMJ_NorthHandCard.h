#ifndef QZHOUMJ_NorthHandCard_h__
#define QZHOUMJ_NorthHandCard_h__

#include "cocos2d.h"
#include "QZHOUMJ_CardPool.h"
#include "QZHOUMJ_HandCard.h"

using namespace cocos2d;

namespace QZHOUMJ
{
	class QZHOUMJ_NorthHandCard : public HandCard
	{
	public:
		QZHOUMJ_NorthHandCard();
		~QZHOUMJ_NorthHandCard();

		CREATE_FUNC(QZHOUMJ_NorthHandCard);

		virtual Vec2 refreshHandCard(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>normalCards,INT catchCard)override;
		virtual Vec2 refreshHandCardTing(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>tingCards)override;
		virtual Vec2 refreshHandCardHu(std::vector<CardPool::CGroupCardData>groupCards,std::vector<INT>handCards,INT huCardId,bool visibleAllCard)override;
		virtual void setSingleGroupSize(Size groupSize)override;
		virtual void setNormalCardSize(Size normalSize)override;
		virtual void setTingCardSize(Size tingSize)override;
		virtual Vec2 getCatchWorldPos()override;
		virtual void putChangeCardToHead(std::vector<INT> cards)override;
		virtual void moveChangeCard(sitDir dir)override;

	private:
		virtual bool init() override;
		void refreshPengGang(std::vector<CardPool::CGroupCardData>groupCards);
		
		
		/*
		处理手牌，可以出的牌
		*/
		void refreshOutCard(std::vector<INT>handcards, INT catchCard);

		void resetData();

		Vec2 getNextPosition(Vec2 prePoint, float deltLen);

	private:
		int		_beginZorder;
		Vec2	_prePoint;
		
	};
}



#endif // HSMJ_NorthHandCard_h__
