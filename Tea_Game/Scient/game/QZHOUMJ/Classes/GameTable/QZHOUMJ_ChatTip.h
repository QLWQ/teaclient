#ifndef QZHOUMJ_ChatTip_h__
#define QZHOUMJ_ChatTip_h__

#include <string>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::extension;
using namespace ui;


namespace QZHOUMJ
{
	class QZHOUMJ_ChatTip : public Sprite
	{
	public:
		QZHOUMJ_ChatTip();
		~QZHOUMJ_ChatTip();
		CREATE_FUNC(QZHOUMJ_ChatTip);
		void setClickCallBack(std::function<void()> clickCallBack );
		void setInfo(std::string name,std::string talkMsg);
		void playNewMsgAction();
		void stopNewMsgAction();
	private:
		virtual bool init();
		void onBtnClick(Ref* pSender,Control::EventType event);

	private:
		Label*					_chatUserName;
		Label*					_chatLabel;
		Sprite*					_chatTips;
		std::function<void()>	_clickCallBack;
	};

	
}
#endif // HSMJ_ChatTip_h__