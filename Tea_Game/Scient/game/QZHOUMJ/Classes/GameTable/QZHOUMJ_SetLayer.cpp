/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "QZHOUMJ_SetLayer.h"
#include "HNNetExport.h"

static const char* SETTING_PATH = "platform/setting/roomSettingNode.csb";

QZHOUMJSetLayer::QZHOUMJSetLayer()
{

}

QZHOUMJSetLayer::~QZHOUMJSetLayer()
{

}

void QZHOUMJSetLayer::showSet(Node* parent, int zorder, int tag)
{
	CCAssert(nullptr != parent, "parent is nullptr");
	if (0 != tag) {
		parent->addChild(this, zorder, tag);
	}
	else {
		parent->addChild(this, zorder);
	}

	setPosition(Director::getInstance()->getWinSize() / 2);//setPosition(Vec2(508, 292));//Director::getInstance()->getWinSize() / 2);
}

void QZHOUMJSetLayer::close()
{
	this->runAction(Sequence::create(FadeOut::create(0.3f), CallFunc::create([&](){
		//UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, _vec_CheckBox[1]->isSelected() ? 0.0 : 1.0);
		//UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, _musicSlider->getPercent());
		//UserDefault::getInstance()->flush();

		this->removeFromParent();
	}), nullptr));
}

bool QZHOUMJSetLayer::init()
{
	if (!HNLayer::init()) {
		return false;
	}

	auto node = CSLoader::createNode(SETTING_PATH);
	CCAssert(node != nullptr, "null");
	node->setPosition(Vec2::ZERO);
	addChild(node);

	/*auto layout_bg = dynamic_cast<Layout*>(node->getChildByName("Panel_setting"));
	layout_bg->setScale(_winSize.width / 1280, _winSize.height / 720);*/

	auto img_bg = dynamic_cast<ImageView*>(node->getChildByName("Image_bg"));
	img_bg->setScale(1280 / _winSize.width, 720 / _winSize.height);

	// 关闭
	auto btn_guanBi = dynamic_cast<Button*>(img_bg->getChildByName("Button_close"));
	btn_guanBi->addClickEventListener([=](Ref*) {
		close();
	});


	//复选框按钮
	//_vec_CheckBox.clear();
	_Check_yinxiao = dynamic_cast<CheckBox*>(img_bg->getChildByName("CheckBox_yinxiao"));
	_Check_yinxiao->setTag(2);
	_Check_yinxiao->setSelected(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0);
	_Check_yinxiao->addClickEventListener(CC_CALLBACK_1(QZHOUMJSetLayer::CheckBoxCallback, this));
	_Check_yinyue = dynamic_cast<CheckBox*>(img_bg->getChildByName("CheckBox_yinyue"));
	_Check_yinyue->setTag(1);
	_Check_yinyue->setSelected(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0);
	_Check_yinyue->addClickEventListener(CC_CALLBACK_1(QZHOUMJSetLayer::CheckBoxCallback, this));
	//_vec_CheckBox.pushBack(btn_yinyue);
	//_vec_CheckBox.pushBack(btn_yinxiao);
	_Check_jingyin = dynamic_cast<CheckBox*>(img_bg->getChildByName("CheckBox_jingyin"));
	_Check_jingyin->setTag(3);
	_Check_jingyin->setSelected(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0 && UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0);
	_Check_jingyin->addClickEventListener(CC_CALLBACK_1(QZHOUMJSetLayer::CheckBoxCallback, this));
	//_vec_CheckBox.pushBack(CheckBox_jingyin);
 //   //音效滑动条
	//_effectSlider = (Slider*)img_bg->getChildByName("Slider_effect");
	//_effectSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT));
	//_effectSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(QZHOUJJMJSetLayer::sliderCallback, this)));

 //   //音乐滑动条
	//_musicSlider = (Slider*)img_bg->getChildByName("Slider_music");
	//_musicSlider->setPercent(UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT));
	//_musicSlider->addEventListener(Slider::ccSliderCallback(CC_CALLBACK_2(QZHOUJJMJSetLayer::sliderCallback, this)));

	// 解散房间
	auto btn_dis = (Button*)img_bg->getChildByName("Button_qieHuan");
	btn_dis->loadTextures("platform/setting/res/jsfj.png", "", "platform/setting/res/jsfj.png");
	btn_dis->addClickEventListener(CC_CALLBACK_1(QZHOUMJSetLayer::onDisClick, this));
	btn_dis->setVisible(RoomLogic()->getRoomRule() & GRR_GAME_BUY);

	// 离开游戏
	auto btn_exit = (Button*)img_bg->getChildByName("Button_liKai");
	btn_exit->addClickEventListener(CC_CALLBACK_1(QZHOUMJSetLayer::onExitClick, this));
	if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
	{
		Size consize = img_bg->getContentSize();
		btn_exit->setPosition(Vec2(consize.width / 2 + 150, 291.5));
	}

	return true;
}

void QZHOUMJSetLayer::onExitClick(Ref* pRef)
{
	if (nullptr != onExitCallBack) {
		if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
		{
			auto PopLayer = GamePromptLayer::create(true);
			PopLayer->showPrompt(GBKToUtf8("当前比赛未结束,若退出游戏后无法加入其他房间!"));
			PopLayer->setCallBack([=](){
				//点击退出按钮
				onExitCallBack();
			});
			PopLayer->setCancelCallBack([=](){
				//点击关闭
				close();
			});
		}
		else
		{
			onExitCallBack();
		}
	}
}

// 切换账号
void QZHOUMJSetLayer::onDisClick(Ref* pRef)
{
	if (nullptr != onDisCallBack) {
		onDisCallBack();
	}
}

void QZHOUMJSetLayer::CheckBoxCallback(Ref* pSender)
{
	CheckBox* btn = static_cast<CheckBox*>(pSender);
	if (btn->isSelected())
	{
		//判断勾选状态按钮（静音需要特殊处理）
		//默认没有勾选，关闭为勾选
		if (btn->getTag() == 1)
		{
			HNAudioEngine::getInstance()->pauseBackgroundMusic();
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(0.0);
			UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 0);
			//是否全部静音
			if (UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0 && UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0)
			{
				_Check_jingyin->setSelected(true);
			}
		}
		else if (btn->getTag() == 2)
		{
			//HNAudioEngine::getInstance()->setSwitcjofEffect(false);
			HNAudioEngine::getInstance()->setEffectsVolume(0.0);
			UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 0);
			//是否全部静音
			if (UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT) == 0 && UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT) == 0)
			{
				_Check_jingyin->setSelected(true);
			}
		}
		else if (btn->getTag() == 3)
		{
			_Check_yinxiao->setSelected(true);
			_Check_yinyue->setSelected(true);
			UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 0);
			UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 0);
			HNAudioEngine::getInstance()->pauseBackgroundMusic();
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(0.0);
			HNAudioEngine::getInstance()->setEffectsVolume(0.0);
		}
	}
	else
	{
		//判断勾选状态按钮（静音需要特殊处理）
		if (btn->getTag() == 1)
		{
			HNAudioEngine::getInstance()->resumeBackgroundMusic();
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(1.0);
			UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 100);
			if (_Check_jingyin->isSelected())
			{
				_Check_jingyin->setSelected(false);
			}
		}
		else if (btn->getTag() == 2)
		{
			//HNAudioEngine::getInstance()->setSwitcjofEffect(false);
			HNAudioEngine::getInstance()->setEffectsVolume(1.0);
			UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 100);
			if (_Check_jingyin->isSelected())
			{
				_Check_jingyin->setSelected(false);
			}
		}
		else if (btn->getTag() == 3)
		{
			_Check_yinxiao->setSelected(false);
			_Check_yinyue->setSelected(false);
			UserDefault::getInstance()->setIntegerForKey(MUSIC_VALUE_TEXT, 100);
			UserDefault::getInstance()->setIntegerForKey(SOUND_VALUE_TEXT, 100);
			HNAudioEngine::getInstance()->resumeBackgroundMusic();
			HNAudioEngine::getInstance()->setBackgroundMusicVolume(1.0);
			HNAudioEngine::getInstance()->setEffectsVolume(1.0);
		}
	}
	UserDefault::getInstance()->flush();
}

void QZHOUMJSetLayer::sliderCallback(Ref* pSender, Slider::EventType type)
{
	Slider* pSlider = static_cast<Slider*>(pSender);
	auto name = pSlider->getName();

	float l = pSlider->getPercent() / 100.f;

	if (name.compare("Slider_music") == 0)
	{
		if (pSlider->getPercent() == 0)
		{
			HNAudioEngine::getInstance()->setSwitchOfMusic(false);
			HNAudioEngine::getInstance()->pauseBackgroundMusic();
		}
		else if (!HNAudioEngine::getInstance()->getSwitchOfMusic())
		{
			HNAudioEngine::getInstance()->setSwitchOfMusic(true);
			HNAudioEngine::getInstance()->resumeBackgroundMusic();
		}
		else
		{

		}
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(l);
	}

	if (name.compare("Slider_effect") == 0)
	{
		if (pSlider->getPercent() == 0)
		{
			HNAudioEngine::getInstance()->setSwitcjofEffect(false);
		}
		else
		{
			HNAudioEngine::getInstance()->setSwitcjofEffect(true);
		}
		HNAudioEngine::getInstance()->setEffectsVolume(l);
	}
}