#include "QZHOUMJ_NorthMahjongCardPool.h"
#include "QZHOUMJ_GameManager.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
#include <algorithm>

namespace QZHOUMJ
{

	NorthMahjongCardPool::NorthMahjongCardPool(void)
	{

	}


	NorthMahjongCardPool::~NorthMahjongCardPool(void)
	{

	}

	bool NorthMahjongCardPool::init(INT count)
	{
		if (! MahjongCardPool::init())
		{
			return false;
		}
	
		_dir = sitDir::NORTH_DIR;
		m_iMaxZOrder = 1000;

		_handCard = QZHOUMJ_NorthHandCard::create();
		this->addChild(_handCard);
		return true;
	}

	void NorthMahjongCardPool::setHandCardPos(INT catchCard)
	{
		refreshAllShowCard();
	}


	int NorthMahjongCardPool::getZhuaPaiZOrder()
	{
		return m_iMaxZOrder + 50;
	}

	void NorthMahjongCardPool::refreshAllShowCard()
	{
		std::vector<CGroupCardData> temp;
		for (auto v:m_GroupCardDataVec)
		{
			temp.push_back(*v);
		}

		_handCard->setTingCardSize(Size(28,40));
		_handCard->setSingleGroupSize(Size(30,40));
		_handCard->setNormalCardSize(Size(32, 50));

		if (_isTingState)
		{
			 _handCard->refreshHandCardTing(temp,_handCardList);
		}
		else
		{
			if (_isCatchCard)
			{
				_handCard->refreshHandCard(temp, _handCardList, _handCardList.at(_handCardList.size() - 1));
			}
			else
			{
				_handCard->refreshHandCard(temp, _handCardList, 0);
			}
		}
		
	}

	//清空切后台前发的牌
	void NorthMahjongCardPool::EmptyAllShowCard()
	{
		_handCardList.clear();
	}

	//切回游戏刷新手牌
	void NorthMahjongCardPool::RefreshShowCard()
	{

	}


	cocos2d::Vec2 NorthMahjongCardPool::getOutToDeskPos()
	{	
		return getCatchPos();
	}


	void NorthMahjongCardPool::showHandCardHu(std::vector<CGroupCardData>group,std::vector<INT> handCards,INT huCard,bool visibleAllCard)
	{
		_handCardList.clear();
		for (auto v:handCards)
		{
			_handCardList.push_back(v);
		}

		sortCard();
		moveOneCardToLast(huCard);
		_handCard->refreshHandCardHu(group,_handCardList,huCard,visibleAllCard);

		if (_huTips)
		{
			_huTips->removeFromParent();
			_huTips = nullptr;
		}
	}


	cocos2d::Vec2 NorthMahjongCardPool::getCatchPos()
	{
	
		return _handCard->getCatchWorldPos();
	}

	void NorthMahjongCardPool::showHuTips(std::vector<INT> huTip)
	{
		if (_huTips)
		{
			_huTips->removeFromParent();
		}
		
		if (huTip.empty())
		{
			return;
		}

		Size size = Size((huTip.size())*50+10,80);
		_huTips = Node::create();
		_huTips->setPosition(Vec2(900-huTip.size()*50.0f,590));
		_huTips = Node::create();
		this->addChild(_huTips);
		
		Sprite* hu = Sprite::create(SPRITE_PATH+"img_hux.png");
		hu->setPosition(Vec2(20,size.height/2));
		_huTips->addChild(hu);
		
		ui::Scale9Sprite* bg = ui::Scale9Sprite::create(SPRITE_PATH+"bg_hux.png");
		bg->setAnchorPoint(Vec2(0.0f,0.5f));
		bg->setContentSize(size);
		bg->setPosition(80,size.height/2);
		_huTips->addChild(bg);

		
		int index = 0;
		for (auto v:huTip)
		{
			MahjongCard* card = MahjongCard::create(DI_SOUTH_STAND,SOUTH_DIR,v);
			card->setAnchorPoint(Vec2(0.0f,0.5f));
			card->setScale(0.8f);
			card->setPosition(Vec2(3+index*70,size.height/2));
			bg->addChild(card);
			index++;
		}
	}


	void NorthMahjongCardPool::changeCardToHead(std::vector<INT> cards)
	{
		for (auto v: cards)
		{
			auto itr = std::find(_handCardList.begin(),_handCardList.end(),v);
			if (itr != _handCardList.end())
			{
				_handCardList.erase(itr);
			}
		}

		std::vector<CardPool::CGroupCardData>groupCards;
		for (auto v:m_GroupCardDataVec)
		{
			groupCards.push_back(*v);
		}
		_handCard->refreshHandCard(groupCards,_handCardList,0);
		//把牌摆在前面
		_handCard->putChangeCardToHead(cards);
	}

}