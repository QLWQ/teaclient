/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __RunCrazyMessage_h__
#define __RunCrazyMessage_h__

#include "HNBaseType.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
using namespace cocos2d;

namespace RunCrazy
{
#pragma pack(1)
	// 常用常量定义
	enum Const_Value
	{
		PLAY_COUNT = 3, // 玩家人数
		GAME_NAME_ID = 12345678, //游戏id
		CARD_COUNT= 48,
		HAND_CARD_COUNT = 48,	 //玩家手牌数量
		CARD_WIDTH = 74,
		CARD_HEIGHT = 109,
		HAND_CARD_COUNT_MODEFY = 16,
		NICNAME_SIZE = 61,//玩家昵称字符串长度
		//HAND_CARD_COUNT_HISTORY = 45,
		KING_COUNT = 2,				//所有王的个数
		NEW_GAME_ID = 20200310,		//算牌跑得快（16张）
		NEW_OUTGAME_ID = 20200311,		//上游跑得快（15张）
	};

	const std::string Player_Normal_M = "platform/common/boy.png";
	const std::string Player_Normal_W = "platform/common/girl.png";

	// 辅助消息id定义
	enum Cmd_Assit_Id
	{
		ASS_TEST = 1, // 仅供测试
		CARD_TYPE = 99,
	};

	//游戏状态定义
	enum GameStation_Value
	{
		GS_WAIT_SETGAME = 0,				//等待东家设置状态
		GS_WAIT_ARGEE = 1,				//等待同意设置
		GS_SEND_CARD = 20,				//发牌状态
		GS_PLAY_GAME = 21,				//游戏中状态
		GS_WAIT_NEXT = 22,				//等待下一盘开始 
	};

	//服务端到客户端的消息
	enum S_C_Game_MSG
	{
		S_C_GAME_BEGIN = 51,	 //游戏开始
		S_C_GAME_PREPARE = 52,	 //准备工作
		S_C_SEND_CARD = 53,		 //发牌信息
		S_C_SEND_CARD_MSG = 54,  //发牌过程中处理消息
		S_C_SEND_ALL_CARD = 55,  //发送所有牌(一下发放全部)
		S_C_SEND_FINISH = 56,	 //发牌完成
		S_C_GAME_PLAY	= 76,	 //开始游戏
		S_C_OUT_CARD_RESULT = 78,//出牌结果
		ASS_SUPER_USER = 79,     //超端消息
		S_C_NEW_TURN = 82,		 //新一轮开始
		S_C_CONTINUE_END = 84,	 //游戏结束
		S_C_NO_CONTINUE_END	= 85,//游戏结束
		ASS_AUTO			=91, //用户托管

		S_C_SETTLEMENTLIST = 98,
		S_C_OUTCardError = 99,
		S_C_CANNT_COUT_CARD=100,
		ASS_REQ_SET_CARD = 101,  //设置牌型
		S_C_BOMB_NUM = 103,		//倍数
		S_C_CHUNTIAN  = 104,
	};

	//客户端发向服务端的消息
	enum C_S_Game_MSG
	{
		C_S_OUT_CARD = 77,
	};

	//游戏状态数据包	（ 等待东家设置状态 ）
	struct GameStation_base
	{
		UINT				gameNameId;						// 当前游戏ID

		//游戏版本
		BYTE				iVersion;						//游戏版本号
		BYTE				iVersion2;						//游戏版本号

		BYTE				iBeginTime;						//开始准备时间
		BYTE				iThinkTime;						//出牌思考时间
		BYTE				iLeftTime;						//剩余时间
		UINT				iCardShape;						//牌型设置

		UINT				iDeskBasePoint;					//桌面基础分
		UINT				iRoomBasePoint;					//房间倍数
		BYTE                iWanfa;
		BYTE                iDifen;
		BYTE                ithreeaaa;
		BYTE                iBeishu;
		LLONG           ilScoreTEMP[PLAY_COUNT];				//
		LLONG				iRunPublish;					//逃跑扣分
		int                 iGuiZe;                         //规则
		bool                bMastOutBlackThree;					   //是否要出黑桃3
		BYTE				DeskConfig[512];

		//游戏信息
		GameStation_base()
		{
			memset(this, 0, sizeof(GameStation_base));
		}
	};

	//游戏状态数据包	（ 等待其他玩家开始 ）
	struct S_C_GameStation_Wait :public GameStation_base
	{

		bool                bUserReady[PLAY_COUNT];        ///玩家是否已准备

		S_C_GameStation_Wait()
		{
			memset(this, 0, sizeof(S_C_GameStation_Wait));
		}
	};

	//游戏状态数据包	（ 游戏中状态 ）
	struct S_C_GameStation_Play :public GameStation_base
	{
		bool				bIsLastCard;							//是否有上轮数据
		BYTE				bIsPass;								//是否不出
		int					iBase;									//当前炸弹个数
		int					iOutCardPeople;							//现在出牌用户
		int					iFirstOutPeople;						//一轮先出牌的用户
		int					iBigOutPeople;							//上次出牌的用户

		bool				bAuto[PLAY_COUNT];						//托管情况
		bool				bCanleave[PLAY_COUNT];					//能否点退出
		BYTE				iUserCardCount[PLAY_COUNT];				//用户手上扑克数目  
		BYTE				iUserCardList[CARD_COUNT];				//用户手上的扑克

		BYTE				iBaseOutCount;							//出牌的数目 上次出的牌
		BYTE                iBaseCardList[CARD_COUNT];				//桌面上的牌

		BYTE				iDeskCardCount[PLAY_COUNT];				//桌面扑克的数目  所有玩家的牌
		BYTE                iDeskCardList[PLAY_COUNT][CARD_COUNT];  //桌面上的牌

		BYTE				iLastCardCount[PLAY_COUNT];				//上轮扑克的数目  // 用不上
		BYTE				iLastOutCard[PLAY_COUNT][CARD_COUNT];	//上轮的扑克	  // 用不上

		bool                bPass[PLAY_COUNT];						//不出
		bool                bLastTurnPass[PLAY_COUNT];				//上一轮不出

		S_C_GameStation_Play()
		{
			memset(this, 0, sizeof(S_C_GameStation_Play));
		}

	};

	//游戏状态数据包	（ 等待下盘开始状态  ）
	struct S_C_GameStation_Next :public GameStation_base
	{
		bool                bUserReady[PLAY_COUNT];     ///玩家准备
		S_C_GameStation_Next()
		{
			memset(this, 0, sizeof(S_C_GameStation_Next));
		}
	};

	//发牌结束
	struct	S_C_SendCardFinishStruct
	{
		BYTE		bReserve;
		BYTE		byUserCardCount[PLAY_COUNT];				//用户手上扑克数目
		BYTE		byUserCard[PLAY_COUNT][HAND_CARD_COUNT];	//用户手上的扑克
		S_C_SendCardFinishStruct()
		{
			memset(this, 0, sizeof(S_C_SendCardFinishStruct));
		}
	};

	//倍数
	struct	S_C_OutBombCard
	{
		int	BombNum;
		S_C_OutBombCard()
		{
			memset(this, 0, sizeof(S_C_OutBombCard));
		}
	};

	//發送所有牌數據
	struct	S_C_SendAllStruct
	{
		BYTE             iUserCardCount[PLAY_COUNT];		//发牌数量
		BYTE             iUserCardList[HAND_CARD_COUNT];				//发牌队例

		S_C_SendAllStruct()
		{
			memset(this, 0, sizeof(S_C_SendAllStruct));
		}
	};


	//游戏开始数据包
	struct S_C_BeginPlayStruct
	{
		BYTE				iOutDeskStation;				//出牌的位置
		BYTE                iFirstNecessaryCard;			//需要先出的牌是哪张
		bool                bMastOutBlackThree;             //是否要出黑桃3
	};
    
    struct S_C_GameBeginStruct
    {
        BYTE iPlayLimit;	// 游戏总局数
        BYTE iBeenPlayGame; // 已经玩了多少局
        UINT iCardShape;	// 牌型设置
		int  iNUm;			//玩法
    };

	//用户出牌数据包 （发向服务器）
	struct C_S_OutCardStruct
	{
		int					iCardCount;						//扑克数目
		BYTE				iCardList[HAND_CARD_COUNT];		//扑克信息
		C_S_OutCardStruct()
		{
			memset(this, 0, sizeof(C_S_OutCardStruct));
		}
	};


	//用户出牌结果数据包 （发向客户端）
	struct S_C_OutCardMsg
	{
		int					iNextDeskStation;				//下一出牌者
		BYTE				iCardCount;						//扑克数目
		BYTE				bDeskStation;					//当前出牌者	
		BYTE				iCardList[CARD_COUNT];			//扑克信息
		bool                bShowNotic;
		S_C_OutCardMsg()
		{
			memset(this, 0, sizeof(S_C_OutCardMsg));
		}
	};

	//新一轮
	struct S_C_NewTurnStruct
	{
		BYTE				bDeskStation;					//坐号
		bool				bEnableNotic;						//保留
	};

	//游戏结束统计数据包
	struct S_C_GameEndStruct
	{
		BYTE			    iUserCardCount[PLAY_COUNT];	    //用户手上扑克数目
		BYTE			    iUserCard[PLAY_COUNT][CARD_COUNT];		//用户手上的扑克

		LLONG  			    iTurePoint[PLAY_COUNT];			//玩家得分
		LLONG				iAllTurePoint[PLAY_COUNT];			//玩家总得分
		bool                iFanBeiFlag[PLAY_COUNT];			//是否春天
		LLONG				iChangeMoney[8];			//玩家金币，此参数无用
		BYTE				byBomb[PLAY_COUNT];					// 倍数
		char				szName[PLAY_COUNT][NICNAME_SIZE];			///昵称
		int					iBaopeiUser;					//包赔玩家座位号
		bool				iDaXiaoTou;
		S_C_GameEndStruct()
		{
			memset(this, 0, sizeof(S_C_GameEndStruct));
		}

	};


	//托管数据结构
	struct AutoStruct
	{
		BYTE bDeskStation;
		bool bAuto;
	};

	// 结算榜
	struct S_C_SettlementList
	{
		int iSingleMaxScore[PLAY_COUNT];  // 单局最高分数
		int iOutBoomNumber[PLAY_COUNT];  // 炸弹数
		int iWinNumber[PLAY_COUNT];   // 胜利局数
		int iLostNumber[PLAY_COUNT];  // 失败局数
		int iWinChunTian[PLAY_COUNT];					//关牌局数
		int iLostChunTian[PLAY_COUNT];				//被关牌局数
		LLONG llscore[PLAY_COUNT];   // 最高分数
		bool iDaXiaoTou;
		int iDaTou[PLAY_COUNT];				//大头局数
		int iXiaoTou[PLAY_COUNT];					//小头局数
		S_C_SettlementList()
		{
			memset(this, 0, sizeof(S_C_SettlementList));
		}

	};

	struct S_C_OutCardError
	{
		int iError;
		S_C_OutCardError()
		{
			iError = -1;         // 1 当前玩家不是出牌玩家 不能出牌  2 当前玩家出牌不符合基本牌型  3. 第一局 必须出黑3  4:3带1 中途不能出 5 炸弹不能拆 6 当前出牌不合法
		}

	};
	struct S_C_CannotOutCard
	{
		BYTE byDeskStation;
		S_C_CannotOutCard()
		{
			byDeskStation = 255;         // 1 当前玩家不是出牌玩家 不能出牌  2 当前玩家出牌不符合基本牌型  3. 第一局 必须出黑3  4:3带1 中途不能出 5 炸弹不能拆 6 当前出牌不合法
		}

	};

	//超端消息结构体
	struct SuperUserMsg
	{
		BYTE byDeskStation;
		bool bIsSuper;
		SuperUserMsg()
		{
			memset(this, 0, sizeof(SuperUserMsg));
		}
	};

	//设置是否发好牌
	struct SuperSetCardMsg
	{
		bool isMake;
		SuperSetCardMsg()
		{
			memset(this, 0, sizeof(SuperSetCardMsg));
		}
	};
}
#pragma pack()
#endif