/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RunCrazyTableLogic.h"
#include "RunCrazyTableUICallback.h"
#include "RunCrazySoundManager.h"
#include "cocos2d.h"
#include "HNLobbyExport.h"


namespace RunCrazy
{

	TableLogic::TableLogic(TableUICallback* uiCallback, BYTE deskId, bool autoCreate) :
		_uiCallback(uiCallback), HNGameLogicBase(deskId, PLAY_COUNT, autoCreate, uiCallback)
	{
		_HandCard.push_back(std::vector<BYTE>());
		_HandCard.push_back(std::vector<BYTE>());
		_HandCard.push_back(std::vector<BYTE>());
		_FirstNecessaryCard = 0;
		firstoutType = 0;
		memset(_bIsSuper, 0, sizeof(_bIsSuper));
	}

	TableLogic::~TableLogic()
	{

	}

	/************************分割线*********************************/
	void TableLogic::dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		switch (messageHead->bAssistantID)//通知消息
		{
		case S_C_GAME_BEGIN:	//游戏开始消息	
		{
			CCAssert(sizeof(S_C_GameBeginStruct) == objectSize, "S_C_GameBeginStruct size error!");
			auto data = (S_C_GameBeginStruct*)object;
			// 允许托管(比赛场不显示托管，显示排行榜)
			if (!(RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST))
			{
				_uiCallback->showTuoguanBtn(true);
			}
			// 播放开始动画
			_uiCallback->playGameStartAnimi();
			setGameStatus(S_C_GAME_BEGIN);
		}
			break;
		case S_C_SEND_ALL_CARD:	//发牌结束消息
			//默认发牌开始关闭结算界面（比赛场）
			//_uiCallback->GameStationPlaying(nullptr);
			_uiCallback->ResetUI();
			_uiCallback->ShowSettlement(NULL, false);
			DealGameSendAllCard(object, objectSize);
			break;
		case S_C_SEND_FINISH:	//发牌结束消息	
			DealGameSendCardFinish();
			break;
		case S_C_GAME_PLAY:
			DealGameBeginPlay(object, objectSize);
			break;
		case C_S_OUT_CARD:
			DealUserOutCardResult(object, objectSize);
			break;
		case S_C_OUT_CARD_RESULT:
			DealUserOutCardResult(object, objectSize);
			break;
		case S_C_BOMB_NUM:
			DealUserBombNum(object, objectSize);
			break;
		case S_C_CHUNTIAN:
			DealUserFuji(object, objectSize);
			break;
		case S_C_CANNT_COUT_CARD:
			DelCanCelCard(object, objectSize);
			break;
		case  S_C_NEW_TURN://新一轮打牌
			DealNewTurnOutCard(object, objectSize);
			break;

		case S_C_NO_CONTINUE_END:
		case S_C_CONTINUE_END:
			if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
			{
				//更新红包任务状态
				//_uiCallback->CheckActivity();
			}
			DealGameEnd(object, objectSize);
			setGameStatus(S_C_CONTINUE_END);

			break;

		case ASS_AUTO:
		{
			CCAssert(objectSize == sizeof(AutoStruct), "ASS_AUTO objectsize error!");

			AutoStruct *pAutoData = (AutoStruct *)object;
			BYTE byDeskView = logicToViewSeatNo(pAutoData->bDeskStation);
			_uiCallback->ShowTuoGuanLogo(byDeskView, pAutoData->bAuto);
			break;
		}
		case S_C_SETTLEMENTLIST:
		{
			HNLOGEX_ERROR("S_C_SettlementList");
			CCAssert(objectSize == sizeof(S_C_SettlementList), "S_C_SettlementList objectsize error!");

			S_C_SettlementList *pAutoData = (S_C_SettlementList *)object;
			_uiCallback->GameOverInfo(pAutoData);
			break;
		}
		case ASS_SUPER_USER:
		{
			CCAssert(objectSize == sizeof(SuperUserMsg), "ASS_SUPER_USER objectsize error!");
			SuperUserMsg *pAutoData = (SuperUserMsg *)object;
			memset(_bIsSuper, 0, sizeof(_bIsSuper));
			_bIsSuper[pAutoData->byDeskStation] = pAutoData->bIsSuper;
			break;
		}
		default:
			HNLOG("INVALID_ASSID:%d", messageHead->bAssistantID);
			break;
		}
	}

	void TableLogic::dealQueueUserSitMessage(bool success, const std::string& message)
	{
		HNGameLogicBase::dealQueueUserSitMessage(success, message);

		LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

		if (success)
		{
			UpdateAllUserInfo();
			sendGameInfo();
		}
		else
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(message);
			prompt->setCallBack([=](){
				stop();
				RoomLogic()->close();
				GamePlatform::returnPlatform(ROOMLIST);
			});
		}
	}

	void TableLogic::dealUserCutMessageResp(INT userId, BYTE bDeskNO)
	{
		auto user = this->getUserByUserID(userId);
		if (!user) return;
		_uiCallback->ShowUserOutLine(logicToViewSeatNo(user->bDeskStation), true);
	}

	void TableLogic::dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo)
	{

	}

	void TableLogic::dealUserInfoChangeResp(const UserInfoStruct* user)
	{
		//_uiCallback->UpdateUserInfo(logicToViewSeatNo(user->bDeskStation), user);
	}

	void TableLogic::dealGameStationResp(void* object, INT objectSize)
	{

		//int _Rule = 0;
		//_Rule = (_Rule | (1 << 0)); //炸弹可拆
		//_Rule = (_Rule | (1 << 1));
		//_upGradeGameLogic.setCondition(_Rule);

		_uiCallback->ResetUI();
		//获取所有玩家的信息
		UpdateAllUserInfo();

		GameStation_base * pGameStationBase = (GameStation_base *)object;
		memcpy(&GameInfo, pGameStationBase, sizeof(GameStation_base));
		//设置房间信息
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			//更新红包任务状态
			//_uiCallback->CheckActivity();
			//设置游戏规则
			_uiCallback->setGamePlayRules(GameInfo.iWanfa, GameInfo.iDifen, GameInfo.iBeishu);
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				//场景消息恢复用户分数
				BYTE byDeskView = logicToViewSeatNo(i);
				_uiCallback->SetUserMoney(GameInfo.ilScoreTEMP[i], byDeskView);
				//DealUserFuji(nullptr,1);
			}
		}
		_uiCallback->setDeskInfo(GameInfo.gameNameId);

		//设置特定规则在背景桌面上
		string str = "";

		//三代二带2
		_upGradeGameLogic.setThree_Tape(pGameStationBase->DeskConfig[7]);
		if (pGameStationBase->DeskConfig[7] - 48 == 0)
		{
			str.append("三带二不可带2");
		}
		else
		{
			str.append("三带二可带2");
		}

		//是否显示剩余牌
		if (pGameStationBase->DeskConfig[8] - 48 == 0)
		{
			_isShowResidue = false;
		}
		else if (pGameStationBase->DeskConfig[8] - 48 == 1)
		{
			_isShowResidue = true;
		}

		//剩余张数报警
		if (pGameStationBase->DeskConfig[9] - 48 == 0)
		{
			_isAlarmNum = 3;
		}
		else if (pGameStationBase->DeskConfig[9] - 48 == 1)
		{
			_isAlarmNum = 1;
		}

		if (GameInfo.gameNameId == NEW_GAME_ID)
		{
			//炸弹是否可拆（DeskConfig[11]可拆为0，不可拆为1）
			_upGradeGameLogic.setDisarmBomb(pGameStationBase->DeskConfig[11]);
			if (pGameStationBase->DeskConfig[11] - 48 == 0)
			{
				str.append(" 炸弹可拆");
			}
			else
			{
				str.append(" 炸弹不可拆");
			}

			//是不是可以四带三（四带二为0，四带三为1，不可带为2）
			_upGradeGameLogic.setFour_Tape(pGameStationBase->DeskConfig[12]);
			if (pGameStationBase->DeskConfig[12] - 48 == 0)
			{
				str.append(" 四带二");
			}
			else if (pGameStationBase->DeskConfig[12] - 48 == 1)
			{
				str.append(" 四带三");
			}
			else if (pGameStationBase->DeskConfig[12] - 48 == 2)
			{
				str.append(" 四张不可带");
			}
		}

		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY && GameInfo.gameNameId != GAME_NAME_ID)
		{
			_uiCallback->setDeskStr(str);
		}

		//设置3A最大或者最小
		_upGradeGameLogic.setLimit3AMax(GameInfo.ithreeaaa);
		HNLOGEX_ERROR("TableLogic::dealGameStationResp %d", _gameStatus);
		//获取所有玩家的信息
		switch (_gameStatus)
		{
		case GS_WAIT_SETGAME:
		case GS_WAIT_ARGEE:
		case GS_WAIT_NEXT:
		{
			CCAssert(sizeof(S_C_GameStation_Wait) == objectSize, "GS_WAIT_NEXT object size error!");
			S_C_GameStation_Wait * pGameStation = (S_C_GameStation_Wait *)object;
			if (!(RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST))
			{
				_uiCallback->GameStationWaitAgree();
			}
			if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				// 比赛开赛消息使用消息中心通知出去，比赛控制中心接收显示开赛动画
				EventCustom event(RECONNECTION);
				Director::getInstance()->getEventDispatcher()->dispatchEvent(&event);
			}
			//玩家是否托管
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				BYTE byDeskView = logicToViewSeatNo(i);
				_uiCallback->ShowReadyImage(byDeskView, pGameStation->bUserReady[i]);
			}
			_uiCallback->ShowTime_PDK(GameInfo.iBeginTime, logicToViewSeatNo(_mySeatNo));
			break;
		}
		case GS_SEND_CARD:
		case GS_PLAY_GAME:
		{
			CCAssert(sizeof(S_C_GameStation_Play) == objectSize, "GS_PLAY_GAME object size error!");
			S_C_GameStation_Play * pGameStationPlay = (S_C_GameStation_Play *)object;

			memcpy(&GameInfo, pGameStationPlay, sizeof(S_C_GameStation_Play));
			//玩家是否托管
			BYTE byDeskView = 0;
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				byDeskView = logicToViewSeatNo(i);
				_uiCallback->ShowReadyImage(byDeskView, false);
				_uiCallback->ShowTuoGuanLogo(byDeskView, GameInfo.bAuto[i]);
			}
			_uiCallback->GameStationPlaying(pGameStationPlay);

			if (GameInfo.iBaseOutCount != 0)
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(GameInfo.iBaseOutCount);
				std::copy(GameInfo.iBaseCardList, GameInfo.iBaseCardList + GameInfo.iBaseOutCount, _byTiShiCardResult.begin());
			}

			int cardcount = 0;
			//显示手牌信息
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				_HandCard[i].clear();
				byDeskView = logicToViewSeatNo(i);
				// 玩家手牌
				{
					if (GameInfo.iUserCardCount[i] != 0)
					{
						_HandCard[i].resize(GameInfo.iUserCardCount[i]);
						std::copy(&GameInfo.iUserCardList[cardcount], &GameInfo.iUserCardList[cardcount] + GameInfo.iUserCardCount[i], _HandCard[i].begin());
						_uiCallback->SetUserCard(byDeskView, &_HandCard[i][0], _HandCard[i].size());
						auto info = getUserBySeatNo(byDeskView);
						if (info)
						{
							if (_isShowResidue)
							{
								_uiCallback->setOtherUserPokerCount(byDeskView, _HandCard[i].size());
							}
						}
						else
						{
							//_uiCallback->setOtherUserPokerCount(byDeskView, HAND_CARD_COUNT_MODEFY);
						}
					}
					cardcount += GameInfo.iUserCardCount[i];
				}

				// 出的牌
				{
					_uiCallback->SetUserOutCard(byDeskView, GameInfo.iDeskCardList[i], GameInfo.iDeskCardCount[i]);
				}
								 
			}
			//显示出牌定时器
			ShowOutCardBtnAndTimer(GameInfo.iOutCardPeople, GameInfo.iLeftTime);
			// 允许托管
			if (!(RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST))
			{
				_uiCallback->showTuoguanBtn(true);
			}
			_uiCallback->setCardCountResult(_HandCard);
							 
			break;
		}
		default:
			break;
		}

		//播放背景音乐
		SoundManager::PlayBgSound();


		/*	memset(GameInfo.iUserCardList, 2, sizeof(GameInfo.iUserCardList));
			memset(GameInfo.iUserCardCount, 16, sizeof(GameInfo.iUserCardCount));*/

		//GameInfo.iUserCardList[0] = 0x03;
		//GameInfo.iUserCardList[1] = 0x13;
		//GameInfo.iUserCardList[2] = 0x04;
		//GameInfo.iUserCardList[3] = 0x25;
		//GameInfo.iUserCardList[4] = 0x36;
		//GameInfo.iUserCardList[5] = 0x06;
		//GameInfo.iUserCardList[6] = 0x16;
		//GameInfo.iUserCardList[7] = 0x08;
		//GameInfo.iUserCardList[8] = 0x18;
		//GameInfo.iUserCardList[9] = 0x28;
		//GameInfo.iUserCardList[10] = 0x38;
		//GameInfo.iUserCardList[11] = 0x2c;
		//GameInfo.iUserCardList[12] = 0x0c;
		//GameInfo.iUserCardList[13] = 0x1d;
		//GameInfo.iUserCardList[14] = 0x2d;
		//GameInfo.iUserCardList[15] = 0x01;
		//_uiCallback->SetUserCard(0, &GameInfo.iUserCardList[0], 16);



		//BYTE card[12] = {};
		//memset(card, 6, sizeof(card));
		//for (size_t i = 0; i < 3; i++)
		//{
		//	_uiCallback->SetUserOutCard(i, card, 12);
		//	_uiCallback->ShowBaoJing(i,true);
		//}
	}

	//设置回放模式
	void TableLogic::dealGamePlayBack(bool isback)
	{
		//GameManager::getInstance()->setPlayBack(isback);
	}

	void TableLogic::dealUserAgreeResp(MSG_GR_R_UserAgree* agree)
	{
		if (getGameStatus() == S_C_GAME_BEGIN) return;  // 游戏开始不处理游戏开始消息
		_uiCallback->ShowReadyImage(logicToViewSeatNo(agree->bDeskStation), agree->bAgreeGame);

	}

	void TableLogic::dealGameClean()
	{
		_uiCallback->ResetUI();
	}

	void TableLogic::dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum)
	{

	}
	void TableLogic::dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum)
	{

	}
	void TableLogic::dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum)
	{

	}

	void TableLogic::dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		HNGameLogicBase::dealUserSitResp(userSit, user);
		auto deskUser = getUserByUserID(user->dwUserID);
		bool isMe = (user->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);
		if (isMe)
		{
			_uiCallback->UpdateUserInfo(0, nullptr);
		}
		else
		{
			BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
			_uiCallback->UpdateUserInfo(seatNo, deskUser);
		}
		//_uiCallback->UpDateUserLocation(0);
		//UpdateAllUserInfo();
	}

	void TableLogic::dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		//TODO 
		if (userSit->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID)
		{
			_uiCallback->userUp(0);

		}
		else
		{

			BYTE byViewStation = logicToViewSeatNo(userSit->bDeskStation);
			_uiCallback->userUp(byViewStation);
		}
	}

	//更新所有玩家信息
	void TableLogic::UpdateAllUserInfo()
	{
		//获取所有玩家的信息
		std::vector<UserInfoStruct *> vecAllDeskUser;
		UserInfoModule()->findDeskUsers(_deskNo, vecAllDeskUser);
		for (int i = 0; i < vecAllDeskUser.size(); i++)
		{
			BYTE byViewStation = logicToViewSeatNo(vecAllDeskUser[i]->bDeskStation);
			if (byViewStation < 0 || byViewStation >= PLAY_COUNT)
			{
				continue;
			}
			if (_uiCallback != nullptr)
			{
				//加用户数据（弹窗要用）
				_uiCallback->UpdateUserInfo(byViewStation, vecAllDeskUser[i]);
			}

		}
		//_uiCallback->UpDateUserLocation(0);
	}
	//显示玩家所有牌
	void TableLogic::DealGameSendAllCard(void* object, INT objectSize)
	{
		//检测数据结构大小
		CCAssert(sizeof(S_C_SendAllStruct) == objectSize, "S_C_SEND_ALL_CARD objectsize error");

		S_C_SendAllStruct * pSendAllCard = (S_C_SendAllStruct *)object;
		if (nullptr == pSendAllCard)
		{
			return;
		}

		//保存手牌信息
		memcpy(GameInfo.iUserCardCount, pSendAllCard->iUserCardCount, sizeof(pSendAllCard->iUserCardCount));
		memcpy(GameInfo.iUserCardList, pSendAllCard->iUserCardList, sizeof(pSendAllCard->iUserCardList));

		int cardcount = 0;
		//显示手牌信息
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_HandCard[i].clear();
			// 玩家手牌
			{
				_HandCard[i].resize(GameInfo.iUserCardCount[i]);
				std::copy(&GameInfo.iUserCardList[cardcount], &GameInfo.iUserCardList[cardcount] + GameInfo.iUserCardCount[i], _HandCard[i].begin());
				cardcount += GameInfo.iUserCardCount[i];
			}
			std::sort(_HandCard[i].begin(), _HandCard[i].end(), [this](BYTE a, BYTE b){ return _upGradeGameLogic.GetCardBulk(a) > _upGradeGameLogic.GetCardBulk(b); });
		}

		_uiCallback->PlaySendCardAnimation();
		_uiCallback->setCardCountResult(_HandCard);
	}

	void TableLogic::DealGameSendCardFinish()
	{
		_uiCallback->SendCardFinish();
		//显示手牌信息
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			// 玩家手牌
			if (_HandCard[i].size() == 0)
			{
				//默认发牌时3个人16张
				//_uiCallback->setOtherUserPokerCount(logicToViewSeatNo(i), HAND_CARD_COUNT_MODEFY);
				continue;
			}
			_uiCallback->SetUserCard(logicToViewSeatNo(i), &_HandCard[i][0], _HandCard[i].size());
			if (_isShowResidue)
			{
				_uiCallback->setOtherUserPokerCount(logicToViewSeatNo(i), _HandCard[i].size());
			}
		}
	}


	//开始游戏
	void TableLogic::DealGameBeginPlay(void* object, INT objectSize)
	{
		CCAssert(sizeof(S_C_BeginPlayStruct) == objectSize, "S_C_GAME_PLAY objectsize error!");

		S_C_BeginPlayStruct * PBegin = (S_C_BeginPlayStruct *)object;
		if (nullptr == PBegin)
		{
			return;
		}

		//获取当前出牌玩家
		GameInfo.iOutCardPeople = PBegin->iOutDeskStation;
		GameInfo.iFirstOutPeople = PBegin->iOutDeskStation;
		_FirstNecessaryCard = PBegin->iFirstNecessaryCard;
		GameInfo.bMastOutBlackThree = PBegin->bMastOutBlackThree;
		//设置第一轮先出牌玩家以及应该先出的牌型
		if (GameInfo.bMastOutBlackThree)
		{
			BYTE byDeskView = logicToViewSeatNo(GameInfo.iFirstOutPeople);
			_uiCallback->setNewTurnOutCard(false);
			_uiCallback->ShowFirstOutCard(byDeskView, _FirstNecessaryCard);
		}

		firstoutType = eRunCrazy::UG_ONLY_ONE;
		_byTiShiCardResult.clear();
		ShowOutCardBtnAndTimer(GameInfo.iOutCardPeople, GameInfo.iThinkTime);
	}


	//当前出牌玩家显示按钮，计时器
	void TableLogic::ShowOutCardBtnAndTimer(BYTE byNowOutCarduser, BYTE byTime)
	{
		//获取当前出牌玩家
		BYTE byDeskView = logicToViewSeatNo(byNowOutCarduser);
		_uiCallback->ShowTime_PDK(byTime, byDeskView);

		//显示出牌按钮
		if (GameInfo.iOutCardPeople == _mySeatNo)
		{
			_uiCallback->ShowRefferOutCardBtn(byDeskView);
		}
	}

	//处理出牌结果
	void TableLogic::DealUserOutCardResult(void* object, INT objectSize)
	{
		//检测出牌数据结构
		CCAssert(sizeof(S_C_OutCardMsg) == objectSize, "C_S_OUT_CARD, objectsize error!");
		S_C_OutCardMsg * pOutCardResult = (S_C_OutCardMsg *)object;


		int iOutCardCount = pOutCardResult->iCardCount;
		if (iOutCardCount != 0)
		{
			GameInfo.iBigOutPeople = pOutCardResult->bDeskStation;//已经出牌玩家
		}
		_uiCallback->showActionBtn(false);
		//出牌后去掉首发出牌显示
		_uiCallback->removeFirstOutCard();
		GameInfo.iOutCardPeople = pOutCardResult->iNextDeskStation;//当前可以出牌玩家
		BYTE byDeskView = logicToViewSeatNo(pOutCardResult->bDeskStation);
		if (iOutCardCount == 0)
		{
			if (pOutCardResult->bShowNotic)
			{
				_uiCallback->setNewTurnOutCard(false);
				ShowOutCardBtnAndTimer(GameInfo.iOutCardPeople, GameInfo.iThinkTime); // 提示出牌
			}
			else
			{
				if (GameInfo.iBigOutPeople != GameInfo.iOutCardPeople)
				{
					_uiCallback->setNewTurnOutCard(true);
				}
				bool gender = false;
				auto info = getUserBySeatNo(pOutCardResult->bDeskStation);
				if (info) gender = info->bBoy;
				//没有更大的牌
				_uiCallback->ShowTime_PDK(0, byDeskView);
				SoundManager::PlayOutCardSound(PASS, 0, gender);
			}
			return;
		}
		else
		{
			GameInfo.bMastOutBlackThree = false;
			GameInfo.iFirstOutPeople = 255;
			// 保存出牌信息
			GameInfo.iBaseOutCount = pOutCardResult->iCardCount;
			memset(GameInfo.iBaseCardList, 255, sizeof(GameInfo.iBaseCardList));
			memcpy(GameInfo.iBaseCardList, pOutCardResult->iCardList, GameInfo.iBaseOutCount);

			_byTiShiCardResult.clear();
			_byTiShiCardResult.resize(pOutCardResult->iCardCount);
			std::copy(pOutCardResult->iCardList, pOutCardResult->iCardList + pOutCardResult->iCardCount, _byTiShiCardResult.begin());
		}

		// 显示的出牌
		byDeskView = logicToViewSeatNo(GameInfo.iBigOutPeople);
		_uiCallback->SetUserOutCard(byDeskView, pOutCardResult->iCardList, pOutCardResult->iCardCount);
		if (_isShowResidue)
		{
			_uiCallback->setOtherUserPokerCount(byDeskView, pOutCardResult->iCardCount);
		}

		//删除玩家出的牌
		bool bisshow = (_HandCard[GameInfo.iBigOutPeople].size() > 0 && (_HandCard[GameInfo.iBigOutPeople][0] > 0 && _HandCard[GameInfo.iBigOutPeople][0] < 80)); // 是否明牌显示 （针对回放）
		if (GameInfo.iBigOutPeople == _mySeatNo || bisshow)
		{
			for (int i = 0; i < pOutCardResult->iCardCount; i++)
			{
				for (auto it = _HandCard[GameInfo.iBigOutPeople].begin(); it != _HandCard[GameInfo.iBigOutPeople].end(); it++)
				{
					if (*it == pOutCardResult->iCardList[i])
					{
						_HandCard[GameInfo.iBigOutPeople].erase(it);
						break;
					}
				}
			}
			//_uiCallback->setNewTurnOutCard(false);
		}
		else
		{
			if (_HandCard[GameInfo.iBigOutPeople].size() >= pOutCardResult->iCardCount)
				_HandCard[GameInfo.iBigOutPeople].erase(_HandCard[GameInfo.iBigOutPeople].begin(), _HandCard[GameInfo.iBigOutPeople].begin() + pOutCardResult->iCardCount);
		}

		//显示玩家手牌
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			// 重连服务端有时候出牌消息会在gamestation 之前
			_uiCallback->SetUserCard(logicToViewSeatNo(i), _HandCard[i].size() >0 ? &_HandCard[i][0] : nullptr, _HandCard[i].size());
			auto info = getUserBySeatNo(i);
			if (info)
			{
				if (_isShowResidue)
				{
					_uiCallback->setOtherUserPokerCount(logicToViewSeatNo(i), _HandCard[i].size());
				}
			}
			else
			{
				//_uiCallback->setOtherUserPokerCount(logicToViewSeatNo(i), HAND_CARD_COUNT_MODEFY);
			}
			if (_HandCard[i].size() <= _isAlarmNum && _HandCard[i].size() != 0)
			{
				_uiCallback->ShowBaoJing(logicToViewSeatNo(i), true);
			}
		}	

		int iCardType = _upGradeGameLogic.GetCardShape(pOutCardResult->iCardList, pOutCardResult->iCardCount);
		int iCardValue = _upGradeGameLogic.GetCardNum(GameInfo.iBaseCardList[0]);

		_uiCallback->playEffect(iCardType);

		// 音效
		bool gender = false;
		auto info = getUserBySeatNo(pOutCardResult->bDeskStation);
		if (info) gender = info->bBoy;
		SoundManager::PlayOutCardSound(iCardType, iCardValue, gender);

		if (_HandCard[GameInfo.iBigOutPeople].size() == 1)
		{
			SoundManager::PlayOutCardSound(LEFT_ONE_CARD, iCardValue, gender);
		}

		//隐藏所有不出图pain
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_uiCallback->ShowDoNotOutCard(i, false);
		}
		_uiCallback->setCardCountResult(_HandCard);
	}

	//收到倍数变化
	void TableLogic::DealUserBombNum(void* object, INT objectSize)
	{
		//检测数据结构
		CCAssert(sizeof(S_C_OutBombCard) == objectSize, "C_S_OUT_BombCard, objectsize error!");
		S_C_OutBombCard * pOutBombResult = (S_C_OutBombCard *)object;

		//设置显示倍数
		_uiCallback->setGameMultiple(pOutBombResult->BombNum);
	}

	void TableLogic::DealUserFuji(void* object, INT objectSize)
	{
		//设置伏击动画
		_uiCallback->playEffect(CARD_TYPE);
	}

	//发送游戏消息
	void TableLogic::SendGameData(int iMsgID, void* object, INT objectSize)
	{
		//向服务端发向
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, iMsgID, object, objectSize);
	}

	//出牌是否合乎规则 是则返回true 否则返回false
	bool TableLogic::CanOutCard(BYTE * byOutCard, int iOutCardCount)
	{
		int iOutCardResultCount = 0;//获取别的玩家出牌的数量
		int iHandCardCount = 0;		//获取自己手牌的数量
		int iResult = 0;

		BYTE nextPlayer = GetNextDeskStation(_mySeatNo);

		//下家手上只剩一张牌的时候
		if ((GameInfo.iUserCardCount[nextPlayer] == 1) && (iOutCardCount == 1))
		{
			//不是出手上最大一张单牌就返回false
			if (byOutCard[0] != _HandCard[_mySeatNo][0])
			{
				return false;
			}
		}

		if (_mySeatNo == GameInfo.iFirstOutPeople)
		{

			return true;
		}
		else
		{
			iResult = _upGradeGameLogic.CanOutCard(byOutCard, iOutCardCount, GameInfo.iBaseCardList, GameInfo.iBaseOutCount, &_HandCard[_mySeatNo][0], _HandCard[_mySeatNo].size(), false);
			if (iResult >= 1)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

		return false;
	}

	//新一轮出牌
	void TableLogic::DealNewTurnOutCard(void* object, INT objectSize)
	{
		//检测出牌数据结构
		CCAssert(sizeof(S_C_NewTurnStruct) == objectSize, "S_C_NEW_TURN objectsize error!");

		S_C_NewTurnStruct * pNewTurn = (S_C_NewTurnStruct *)object;
		if (nullptr == pNewTurn)
		{
			return;
		}
		BYTE nextPlayer = GetNextDeskStation(_mySeatNo);
		//下家手上只剩一张牌的时候
		if (_HandCard[nextPlayer].size() == 1)
		{
			firstoutType = eRunCrazy::UG_BOMB;
		}
		else
		{
			firstoutType = eRunCrazy::UG_ONLY_ONE;
		}

		_byTiShiCardResult.clear();
		GameInfo.iOutCardPeople = pNewTurn->bDeskStation;//当前可以出牌玩家
		GameInfo.iFirstOutPeople = pNewTurn->bDeskStation;
		ShowOutCardBtnAndTimer(GameInfo.iOutCardPeople, GameInfo.iThinkTime);
		BYTE byDeskView = logicToViewSeatNo(GameInfo.iOutCardPeople);
		BYTE byOutCard[CARD_COUNT] = { 0 };
		memset(byOutCard, 255, sizeof(byOutCard));
		_uiCallback->SetUserOutCard(byDeskView, byOutCard, 0);
		//设置新一轮出牌
		//_uiCallback->setNewTurnOutCard(true);
	}
	void TableLogic::DelCanCelCard(void* object, INT objectSize)
	{
		HNLOG("pdkfpjs *** DealGameEnd");
		//检测结算数据结构
		CCAssert(sizeof(S_C_CannotOutCard) == objectSize, "YAo BU QI objectsize error!");
		S_C_CannotOutCard * msg = (S_C_CannotOutCard*)object;
		_uiCallback->ShowDoNotOutCard(this->logicToViewSeatNo(msg->byDeskStation), true);
		//玩家要不起设置倒计时停止
		_uiCallback->ShowTime_PDK(0, this->logicToViewSeatNo(msg->byDeskStation));

	}

	//处理游戏结束消息
	void TableLogic::DealGameEnd(void* object, INT objectSize)
	{
		HNLOG("pdkfpjs *** DealGameEnd");
		//检测结算数据结构
		CCAssert(sizeof(S_C_GameEndStruct) == objectSize, "S_C_CONTINUE_END objectsize error!");

		S_C_GameEndStruct * pNewTurn = (S_C_GameEndStruct *)object;
		if (nullptr == pNewTurn)
		{
			return;
		}

		_FirstNecessaryCard = 0;

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			BYTE byDeskStation = logicToViewSeatNo(i);
			_uiCallback->SetUserCard(byDeskStation, pNewTurn->iUserCard[i], pNewTurn->iUserCardCount[i]);
			if (_isShowResidue)
			{
				_uiCallback->setOtherUserPokerCount(logicToViewSeatNo(i), _HandCard[i].size());
			}
			if (i == _mySeatNo)
			{
				SoundManager::playWinLose(pNewTurn->iTurePoint[i]>0);
			}
		}

		_uiCallback->ShowSettlement(pNewTurn, true);
		_uiCallback->ShowTime_PDK(GameInfo.iBeginTime, 0);
		_uiCallback->ShowTimeLeaveGame(GameInfo.iBeginTime);

		// 隐藏托管按钮
		_uiCallback->showTuoguanBtn(false);

	}

	//提示出牌
	void TableLogic::GetAutoOutCard(BYTE iResultCard[], int & iResultCardCount)
	{
		GetAutoOutCard(_HandCard[_mySeatNo], iResultCard, iResultCardCount, false);
	}

	// 滑动出牌
	void TableLogic::GetAutoOutCard(std::vector<BYTE> iCardlist, BYTE iResultCard[], int & iResultCardCount, bool bSelect)
	{
		if (GameInfo.iOutCardPeople != _mySeatNo) return;
		BYTE byFirstOut = 0;
		if (GameInfo.iFirstOutPeople == _mySeatNo)
		{
			byFirstOut = 1;
		}
		else
		{
			byFirstOut = 0;
		}

		std::sort(iCardlist.begin(), iCardlist.end(), [this](BYTE a, BYTE b){ return _upGradeGameLogic.GetCardBulk(a) > _upGradeGameLogic.GetCardBulk(b); });
		BYTE nextPlayer = GetNextDeskStation(_mySeatNo);

		//下家手上只剩一张牌的时候
		if (_HandCard[nextPlayer].size() == 1)
		{
			//首出
			if (byFirstOut)
			{
				//抽手上最大的一张牌出去
				// 提示顺序 ： 炸弹-》飞机-》连队-》三带二 -》顺子-》三张-》对子-》单张-》炸弹  此为循环提示 
				while (true)
				{
					if (firstoutType == eRunCrazy::UG_BOMB)
					{
						if (getBomb(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, eRunCrazy::UG_THREE_SEQUENCE)) return;
					}
					else if (firstoutType == eRunCrazy::UG_FOUR_THREE_DOUBLE)
					{
						if (getFourX(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 6, eRunCrazy::UG_BOMB)) return;
					}
					else if (firstoutType == eRunCrazy::UG_FOUR_THREE)
					{
						if (getFourX(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 3, eRunCrazy::UG_BOMB)) return;
					}
					else if (firstoutType == eRunCrazy::UG_THREE_SEQUENCE)
					{
						if (getSequence(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 3, eRunCrazy::UG_DOUBLE_SEQUENCE)) return;
					}

					else if (firstoutType == eRunCrazy::UG_DOUBLE_SEQUENCE)
					{
						if (getSequence(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 2, eRunCrazy::UG_THREE_TWO)) return;
					}
					else if (firstoutType == eRunCrazy::UG_THREE_TWO)
					{
						if (getThreeAndTwo(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, eRunCrazy::UG_STRAIGHT)) return;
					}
					else if (firstoutType == eRunCrazy::UG_STRAIGHT)
					{
						if (getSequence(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 1, eRunCrazy::UG_THREE)) return;
					}
					else if (firstoutType == eRunCrazy::UG_DOUBLE || firstoutType == eRunCrazy::UG_THREE)
					{
						int icount = firstoutType == eRunCrazy::UG_THREE ? 3 : 2;
						if (getSingleCard(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, icount)) return;
						else
						{
							if (firstoutType == eRunCrazy::UG_THREE) firstoutType = eRunCrazy::UG_DOUBLE;
							else if (firstoutType == eRunCrazy::UG_DOUBLE)  firstoutType = eRunCrazy::UG_ONLY_ONE;
						}
					}
					else if (firstoutType == eRunCrazy::UG_ONLY_ONE)
					{
						_upGradeGameLogic.TackOutCardBySpecifyCardNum(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, iCardlist[0]); //获取最大的单牌
						iResultCardCount = 1;
						firstoutType = eRunCrazy::UG_BOMB;
						return;
					}
				}
			}
			else if (GameInfo.iBaseOutCount == 1)
			{
				//有炸弹就出炸弹
				BYTE iCount = _upGradeGameLogic.TackOutBySepcifyCardNumCount(&iCardlist[0], iCardlist.size(), iResultCard, 4);

				if (iCount >= 4)
				{
					iResultCardCount = 4;
				}
				else
				{
					//抽手上最大的一张牌出去
					_upGradeGameLogic.TackOutCardBySpecifyCardNum(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, iCardlist[0]);
					iResultCardCount = 1;
					if (!_upGradeGameLogic.CompareOnlyOne(GameInfo.iBaseCardList[0], iResultCard[0]))
					{
						iResultCardCount = 0;
					}
				}
			}
			else
			{
				_upGradeGameLogic.AutoOutCard(&iCardlist[0], iCardlist.size(), GameInfo.iBaseCardList, GameInfo.iBaseOutCount, iResultCard, iResultCardCount, byFirstOut);
			}
		}
		else
		{
			if (_upGradeGameLogic.GetCardShape(&iCardlist[0], iCardlist.size()) != UG_ERROR_KIND && bSelect)   // 滑动选择是一种牌型就直接返回
			{
				iResultCardCount = iCardlist.size();
				std::copy(iCardlist.begin(), iCardlist.end(), iResultCard);
				return;
			}

			if (byFirstOut) // 首出
			{
				if (GameInfo.bMastOutBlackThree) // 首出3
				{
					//首出牌设置为首轮必须先出的牌
					_upGradeGameLogic.TackOutCardBySpecifyCard(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, &iCardlist[0], 0, _FirstNecessaryCard);
				}
				else
				{
					if (_byTiShiCardResult.empty())
					{
						firstoutType = eRunCrazy::UG_ONLY_ONE;
					}
					else
					{
						if (!firstoutType)
						{
							firstoutType = eRunCrazy::UG_ONLY_ONE;
						}
					}
					// 提示顺序 ： 单张-》对子-》三张-》顺子 -》三带二-》连队-》飞机-》四代 2 -》四代3->炸弹-》单张  此为循环提示 
					while (true)
					{
						if (firstoutType == eRunCrazy::UG_ONLY_ONE || firstoutType == eRunCrazy::UG_DOUBLE || firstoutType == eRunCrazy::UG_THREE)
						{
							int icount = firstoutType == eRunCrazy::UG_ONLY_ONE ? 1 : (firstoutType == eRunCrazy::UG_DOUBLE ? 2 : 3);
							if (getSingleCard(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, icount)) return;

							else {
								if (firstoutType == eRunCrazy::UG_THREE) firstoutType = eRunCrazy::UG_STRAIGHT;
								else if (firstoutType == eRunCrazy::UG_DOUBLE)  firstoutType = eRunCrazy::UG_THREE;
								else if (firstoutType == eRunCrazy::UG_ONLY_ONE)  firstoutType = eRunCrazy::UG_DOUBLE;
							}
						}
						else if (firstoutType == eRunCrazy::UG_STRAIGHT)
						{
							if (getSequence(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 1, eRunCrazy::UG_THREE_TWO)) return;
						}
						else if (firstoutType == eRunCrazy::UG_THREE_TWO)
						{
							if (getThreeAndTwo(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, eRunCrazy::UG_DOUBLE_SEQUENCE)) return;
						}
						else if (firstoutType == eRunCrazy::UG_DOUBLE_SEQUENCE)
						{
							if (getSequence(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 2, eRunCrazy::UG_THREE_SEQUENCE)) return;
						}
						else if (firstoutType == eRunCrazy::UG_THREE_SEQUENCE)
						{
							if (getSequence(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 3, eRunCrazy::UG_BOMB)) return;
						}
						//else if (firstoutType == eRunCrazy::UG_FOUR_TWO)
						//{
						//	if (getFourX(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 2, eRunCrazy::UG_FOUR_THREE)) return;
						//}
						else if (firstoutType == eRunCrazy::UG_FOUR_THREE)
						{
							if (getFourX(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 3, eRunCrazy::UG_BOMB)) return;
						}
						else if (firstoutType == eRunCrazy::UG_FOUR_THREE_DOUBLE)
						{
							if (getFourX(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, 6, eRunCrazy::UG_BOMB)) return;
						}
						else if (firstoutType == eRunCrazy::UG_BOMB)
						{
							if (getBomb(&iCardlist[0], iCardlist.size(), iResultCard, iResultCardCount, eRunCrazy::UG_ONLY_ONE)) return;
						}
					}
				}
			}
			else
			{
				int iSearchCount = 0;
				BYTE SearchCard[CARD_COUNT] = { 0 };
				if (_upGradeGameLogic.AutoOutCard(&iCardlist[0], iCardlist.size(), &_byTiShiCardResult[0], _byTiShiCardResult.size(), iResultCard, iResultCardCount, byFirstOut) && iResultCardCount)
				{
					_byTiShiCardResult.resize(iResultCardCount);
					std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());

				}
				else
				{
					memset(iResultCard, 0, sizeof(iResultCard));
					if (_upGradeGameLogic.AutoOutCard(&iCardlist[0], iCardlist.size(), GameInfo.iBaseCardList, GameInfo.iBaseOutCount, iResultCard, iResultCardCount, byFirstOut) && iResultCardCount)
					{
						_byTiShiCardResult.resize(iResultCardCount);
						std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
					}
				}
			}
		}
	}

	//自动出牌
	void TableLogic::AutoOutCard()
	{
		if (GameInfo.bAuto[_mySeatNo])
		{
			BYTE byOutCard[HAND_CARD_COUNT];
			int iResultCardCount = 0;
			memset(byOutCard, 255, sizeof(byOutCard));
			GetAutoOutCard(byOutCard, iResultCardCount);

			//玩家出牌
			C_S_OutCardStruct stOutCard;
			//获取要出的牌
			memcpy(stOutCard.iCardList, byOutCard, HAND_CARD_COUNT);
			//获取出牌的数量
			stOutCard.iCardCount = 0;
			for (int i = 0; i < HAND_CARD_COUNT; i++)
			{
				if (byOutCard[i] != 255 && byOutCard[i] != 0)
				{
					stOutCard.iCardCount++;
				}
			}
			//发送出牌消息
			SendGameData(C_S_OUT_CARD, &stOutCard, sizeof(stOutCard));
		}

	}
	BYTE TableLogic::logicToViewSeatNo(BYTE lSeatNO)
	{
		return (lSeatNO - _mySeatNo + PLAY_COUNT) % PLAY_COUNT;
	}

	//获取房间底分
	int TableLogic::GetBasePoint()
	{
		return GameInfo.iDeskBasePoint;
	}

	BYTE TableLogic::GetNextDeskStation(BYTE bDeskStation)
	{
		return (_mySeatNo + (PLAY_COUNT - 1)) % PLAY_COUNT;
	}

	// 托管
	void TableLogic::sendTuoguan(bool dotuoguan)
	{
		AutoStruct data;
		data.bAuto = dotuoguan;
		data.bDeskStation = _mySeatNo;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_AUTO, &data, sizeof(data));
	}


	bool TableLogic::isSuperUser()
	{
		return _bIsSuper[_mySeatNo];
	}

	void TableLogic::clearDesk()
	{
		_uiCallback->ResetUI();
	}

	void TableLogic::clearDeskUsers()
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_uiCallback->UpdateUserInfo(i, nullptr);
		}
	}

	void TableLogic::dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk)
	{
		_uiCallback->onChatTextMsg(normalTalk->dwSendID, normalTalk->szMessage);
	}

	//特效道具聊天
	void TableLogic::dealUserActionMessage(DoNewProp* normalAction)
	{
		auto pUser = UserInfoModule()->findUser(normalAction->dwUserID);
		if (pUser != nullptr)
		{
			auto SendUser = UserInfoModule()->findUser(normalAction->dwUserID);
			auto TargetUser = UserInfoModule()->findUser(normalAction->dwDestID);
			_uiCallback->onActionChatMsg(SendUser->bDeskStation, TargetUser->bDeskStation, normalAction->iPropNum);
			//发送特效道具扣除用户金币
			_uiCallback->afterScoreByDeskStation(SendUser->bDeskStation, -50);
		}
	}

	void TableLogic::dealGameActivityResp(MSG_GR_GameNum* pData)
	{
		int GameNum = pData->iGameNum;
		int dangqianjieduan = pData->idangqianjieduan;
		int CanTakeNum = pData->iCanTakeNum;
		//更新红包活动数据
		_uiCallback->setActivityNum(GameNum, dangqianjieduan, CanTakeNum);
	}

	void TableLogic::dealGameActivityReceive(MSG_GR_Set_JiangLi* pData)
	{
		//领取奖励类型
		int ReceiveType = pData->iRandnum;
		int Index = pData->iJieDuan;
		if (Index == 1)				//阶段一奖励类型为金币，房卡（0-3金币，4-5房卡）
		{
			if (ReceiveType <= 3)
			{
				ReceiveType = 1;
			}
			else
			{
				ReceiveType = 2;
			}
		}
		int AwardNum = pData->iAwardNum;
		//显示中奖处理
		_uiCallback->setReceiveState(ReceiveType, AwardNum);
	}

// 	void TableLogic::dealVoiceMessage(const VoiceInfo* msg)
// 	{
// 		_uiCallback->onChatVoiceMsg(msg->uUserID, msg->uVoiceID, msg->iVoiceTime);
// 
// 	}
	void TableLogic::dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		switch (messageHead->bAssistantID)
		{
		case ASS_GR_VOIEC:
		{
			//效验数据
			CCAssert(sizeof(VoiceInfo) == objectSize, "VoiceInfo size is error.");
			VoiceInfo* voiceInfo = (VoiceInfo*)object;
			auto userInfo = getUserByUserID(voiceInfo->uUserID);
			if (!userInfo) return;
			if (userInfo->bDeskNO == _deskNo)
			{
				_uiCallback->onChatVoiceMsg(voiceInfo->uUserID, voiceInfo->uVoiceID, voiceInfo->iVoiceTime);
			}
		}
		break;
		default:
			break;
		}
	}
	/************************************************************************************************************/

	bool TableLogic::getBomb(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int& iResultCardCount, eRunCrazy NextType) // 获取炸弹
	{
		iResultCardCount = 0;
		bool bsuccess = false;
		if (_byTiShiCardResult.empty())
		{
			if (_upGradeGameLogic.TackOutBomb(iCardlist, cardSize, iResultCard, iResultCardCount, 4) && iResultCardCount) //就直接提取相应的牌型
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}
		else
		{
			//如果上次提示有找到 就直接提取比上次大的牌
			if (_upGradeGameLogic.AutoOutCard(iCardlist, cardSize, &_byTiShiCardResult[0], _byTiShiCardResult.size(), iResultCard, iResultCardCount, false) && iResultCardCount)
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}

		if (!bsuccess)
		{
			_byTiShiCardResult.clear();
			firstoutType = NextType;
			bsuccess = false;

		}
		return bsuccess;
	}

	bool TableLogic::getSequence(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount, int type, eRunCrazy NextType) // 获取飞机
	{
		iResultCardCount = 0;
		bool bsuccess = false;
		if (_byTiShiCardResult.empty())
		{
			_byTiShiCardResult.push_back(255);
			int cardnum = (type == 3 ? 6 : (type == 2 ? 4 : 5)); // 飞机6张 连队4张 单顺5张
			if (_upGradeGameLogic.TackOutSequence(iCardlist, cardSize, &_byTiShiCardResult[0], cardnum, iResultCard, iResultCardCount, type, true)) //如果上次提示没有找到 就直接提取相应的牌型
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}
		else
		{
			//如果上次提示有找到 就直接提取比上次大的牌
			if (_upGradeGameLogic.AutoOutCard(iCardlist, cardSize, &_byTiShiCardResult[0], _byTiShiCardResult.size(), iResultCard, iResultCardCount, false) && iResultCardCount)
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}

		if (!bsuccess || _upGradeGameLogic.GetCardShape(iResultCard, iResultCardCount) == eRunCrazy::UG_BOMB)
		{
			_byTiShiCardResult.clear();
			firstoutType = NextType;
			bsuccess = false;

		}
		return bsuccess;
	}

	bool TableLogic::getThreeAndTwo(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int& iResultCardCount, eRunCrazy NextType) // 获取三代
	{
		iResultCardCount = 0;
		bool bsuccess = false;
		if (_byTiShiCardResult.empty())
		{
			_byTiShiCardResult.push_back(255);
			if (_upGradeGameLogic.TackOutThreeX(iCardlist, cardSize, &_byTiShiCardResult[0], 2, iResultCard, iResultCardCount, 2, true))//如果上次提示没有找到 就直接提取相应的牌型
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}
		else
		{
			//如果上次提示有找到 就直接提取比上次大的牌
			if (_upGradeGameLogic.AutoOutCard(iCardlist, cardSize, &_byTiShiCardResult[0], _byTiShiCardResult.size(), iResultCard, iResultCardCount, false) && iResultCardCount)
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}
		if (!bsuccess || _upGradeGameLogic.GetCardShape(iResultCard, iResultCardCount) == eRunCrazy::UG_BOMB)
		{
			_byTiShiCardResult.clear();
			firstoutType = NextType;
			bsuccess = false;

		}
		return bsuccess;
	}

	/*
	@type 可取1 2 3 表示四代N
	**/
	bool TableLogic::getFourX(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount, int type, eRunCrazy NextType)
	{
		iResultCardCount = 0;
		bool bsuccess = false;
		if (_byTiShiCardResult.empty())
		{
			_byTiShiCardResult.push_back(255);
			if (_upGradeGameLogic.TackOutForX(iCardlist, cardSize, &_byTiShiCardResult[0], 0, iResultCard, iResultCardCount, type))//如果上次提示没有找到 就直接提取相应的牌型
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}
		else
		{
			//如果上次提示有找到 就直接提取比上次大的牌
			if (_upGradeGameLogic.AutoOutCard(iCardlist, cardSize, &_byTiShiCardResult[0], _byTiShiCardResult.size(), iResultCard, iResultCardCount, false) && iResultCardCount)
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}
		if (!bsuccess || _upGradeGameLogic.GetCardShape(iResultCard, iResultCardCount) == eRunCrazy::UG_BOMB)
		{
			_byTiShiCardResult.clear();
			firstoutType = NextType;
			bsuccess = false;

		}
		return bsuccess;
	}

	bool TableLogic::getSingleCard(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount, int type) // 获取单张 对子 三张
	{
		bool bsuccess = false;
		if (_byTiShiCardResult.empty())
		{
			if (_upGradeGameLogic.TackOutBySepcifyCardNumCount(iCardlist, cardSize, iResultCard, type))  //如果上次提示没有找到 就直接提取相应的牌型
			{
				iResultCardCount = type;
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}

		}
		else
		{
			//如果上次提示有找到 就直接提取比上次大的牌
			if (_upGradeGameLogic.AutoOutCard(iCardlist, cardSize, &_byTiShiCardResult[0], _byTiShiCardResult.size(), iResultCard, iResultCardCount, false) && iResultCardCount)
			{
				_byTiShiCardResult.clear();
				_byTiShiCardResult.resize(sizeof(BYTE)*iResultCardCount);
				std::copy(iResultCard, iResultCard + iResultCardCount, _byTiShiCardResult.begin());
				bsuccess = true;
			}
		}
		if (!bsuccess || _upGradeGameLogic.GetCardShape(iResultCard, iResultCardCount) == eRunCrazy::UG_BOMB)
		{
			_byTiShiCardResult.clear();
			bsuccess = false;
		}
		return bsuccess;
	}

	bool TableLogic::getOutCard(BYTE * iCardlist, int cardSize, BYTE * iOutlist, int OutSize, BYTE *iResultCard, int &iResultCardCount, bool firstout)
	{
		_upGradeGameLogic.AutoOutCard(iCardlist, cardSize, iOutlist, OutSize, iResultCard, iResultCardCount, firstout);
		return true;
	}
	/************************************************************************************************************/
}
