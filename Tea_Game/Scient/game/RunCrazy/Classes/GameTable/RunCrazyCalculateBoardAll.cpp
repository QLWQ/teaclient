/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RunCrazyCalculateBoardAll.h"
#include <string>
#include "HNLogicExport.h"
#include "HNLobbyExport.h"
#include "HNOpenExport.h"
#include "HNLobby/GameChildLayer/GameShareLayer.h"

namespace RunCrazy
{

	CalculateBoardAll::CalculateBoardAll()
	{

	}

	CalculateBoardAll::~CalculateBoardAll()
	{

	}

	bool CalculateBoardAll::init()
	{
		if (!HNLayer::init()) return false;

		auto node = CSLoader::createNode("platform/Games/RunCrazy/Result/allBoard.csb");
		node->setPosition(_winSize / 2);
		addChild(node, 2);

		auto layout = dynamic_cast<Layout*>(node->getChildByName("Panel_back"));
		layout->setScale(_winSize.width / 1280, _winSize.height / 720);

		_img_bg = dynamic_cast<ImageView*>(layout->getChildByName("Image_back"));
		_img_bg->setScale(1280 / _winSize.width, 720 / _winSize.height);

		//分享按钮
		auto btn_share = (Button*)_img_bg->getChildByName("Button_invite");
		if (btn_share)
		{
			btn_share->addTouchEventListener(CC_CALLBACK_2(CalculateBoardAll::shareBtnCallBack, this));
		}

		//返回按钮
		auto btn_back = (Button*)_img_bg->getChildByName("Button_return");
		if (btn_back)
		{
			btn_back->addTouchEventListener(CC_CALLBACK_2(CalculateBoardAll::backBtnCallBack, this));
		}

		return true;
	}

	void CalculateBoardAll::backBtnCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::ENDED != type) return;

		RoomLogic()->close();
		GamePlatform::createPlatform();
	}

	void CalculateBoardAll::shareBtnCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (Widget::TouchEventType::ENDED != type) return;

		auto shareLayer = GameShareLayer::create();
		shareLayer->setName("shareLayer");
		shareLayer->SetInviteInfo("跑得快", HNPlatformConfig()->getVipRoomNum(), true);
		shareLayer->show();
	}

	void CalculateBoardAll::showAndUpdateBoard(Node* parent, const S_C_SettlementList* boardData)
	{
		//显示结算
		parent->addChild(this, 7);

		//更新房间号
		auto txt_roomNumber = dynamic_cast<Text*>(_img_bg->getChildByName("Text_room"));
		txt_roomNumber->setString(HNPlatformConfig()->getVipRoomNum());
		int bestwiner = 0;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (boardData->llscore[i] != 0 && bestwiner < boardData->llscore[i])
			{
				bestwiner = boardData->llscore[i];
			}
		}

		// 时间戳
		stringstream strbuffer;
		strbuffer << GBKToUtf8("跑得快")<<"\n";
		time_t t = time(nullptr);
		char timebuff[100] = { 0 };
		strftime(timebuff, 100, "%y%m%d %H:%M:%S", localtime(&t));
		strbuffer << timebuff;
		auto txt_time = dynamic_cast<Text*>(_img_bg->getChildByName("Text_info"));
		txt_time->setString(strbuffer.str());

		int PlayerNum = 0;
		for (int i = 0; i < PLAY_COUNT; ++i)
		{
			auto user = UserInfoModule()->findUser(RoomLogic()->loginResult.pUserInfoStruct.bDeskNO, i);
			if (user) PlayerNum++;
		}

		auto subLayout = dynamic_cast<ListView*>(_img_bg->getChildByName("ListView_score"));
		subLayout->setItemModel(subLayout->getItem(0));
		subLayout->removeAllItems();
		if (PlayerNum == 3)
		{
			subLayout->setContentSize(Size(820, 408));
		}
		else
		{
			subLayout->setContentSize(Size(540, 408));
		}
		for (int showIndex = 0; showIndex < PLAY_COUNT; ++showIndex)
		{
			auto user = UserInfoModule()->findUser(RoomLogic()->loginResult.pUserInfoStruct.bDeskNO, showIndex);
			if (!user) continue;
			subLayout->pushBackDefaultItem();
			auto nodeItem = subLayout->getItem(subLayout->getItems().size()-1);

			//根据游戏规则切换背景
			if (boardData->iDaXiaoTou)
			{
				auto Image_bg = dynamic_cast<ImageView*>(nodeItem->getChildByName("Image_bg"));
				Image_bg->loadTexture("platform/Games/RunCrazy/Result/daxiaotou.png");
			}

			// 更新总成绩
			auto txt_allScore = dynamic_cast<Text*>(nodeItem->getChildByName("txt_allScore"));
			txt_allScore->setString("0");
			if (boardData->llscore[showIndex] > 0)
			{
				txt_allScore->setColor(Color3B(155, 37, 37));
				string str = "+";
				str.append(to_string(boardData->llscore[showIndex]));
				txt_allScore->setString(str);
			}
			else if (boardData->llscore[showIndex] < 0)
			{
				txt_allScore->setColor(Color3B(42, 53, 107));
				txt_allScore->setString(to_string(boardData->llscore[showIndex]));
			}

			// 更新大赢家
			auto image_win = dynamic_cast<ImageView*>(nodeItem->getChildByName("image_winner"));
			image_win->setVisible((boardData->llscore[showIndex] == bestwiner) && bestwiner != 0);
			image_win->setLocalZOrder(5);

			// 单局最高分
			/*auto txt_zadan_score = dynamic_cast<Text*>(nodeItem->getChildByName("txt_hightScore"));
			txt_zadan_score->setString(to_string(boardData->iSingleMaxScore[showIndex]));*/

			// 总结算伏击次数
			auto txt_fuji_count = dynamic_cast<Text*>(nodeItem->getChildByName("txt_fuji_count"));
			txt_fuji_count->setString(to_string(boardData->iWinChunTian[showIndex]));

			// 被伏击次数
			auto txt_beifuji_count = dynamic_cast<Text*>(nodeItem->getChildByName("txt_beifuji_count"));
			txt_beifuji_count->setString(to_string(boardData->iLostChunTian[showIndex]));

			//大小头
			if (boardData->iDaXiaoTou)
			{
				auto txt_daxiaotou_count = dynamic_cast<Text*>(nodeItem->getChildByName("txt_daxiaotou_count"));
				txt_daxiaotou_count->setVisible(true);
				txt_daxiaotou_count->setString(GBKToUtf8(StringUtils::format("%d / %d", boardData->iDaTou[showIndex], boardData->iXiaoTou[showIndex])));
			}

			// 炸弹数
			/*auto txt_zuigaoyindian_score = dynamic_cast<Text*>(nodeItem->getChildByName("txt_bumnum_score"));
			txt_zuigaoyindian_score->setString(to_string(boardData->iOutBoomNumber[showIndex]));*/

			// 胜/负局
			auto txt_zuigaoliansheng_score = dynamic_cast<Text*>(nodeItem->getChildByName("txt_shengju_score"));
			txt_zuigaoliansheng_score->setString(GBKToUtf8(StringUtils::format("%d / %d ", boardData->iWinNumber[showIndex], boardData->iLostNumber[showIndex])));


			// 更新玩家昵称
			auto txt_name = dynamic_cast<Text*>(nodeItem->getChildByName("Panel_name")->getChildByName("Text_name"));
			txt_name->setString("");
			auto txt_Id = dynamic_cast<Text*>(nodeItem->getChildByName("Panel_name")->getChildByName("Text_ID"));
			txt_Id->setString("");

			// 更新房主图标
			auto image_fangzhu = dynamic_cast<ImageView*>(nodeItem->getChildByName("Image_host"));
			image_fangzhu->setVisible(false);

			string str = user->nickName;
			if (str.length() > 10)
			{
				str.erase(11, str.length());
				str.append("...");
				txt_name->setString(GBKToUtf8(str));
			}
			else
			{
				txt_name->setString(GBKToUtf8(str));
			}
			//txt_name->setString(GBKToUtf8(user->nickName));

			// 更新玩家ID
			//txt_Id->setString(to_string(user->dwUserID));

			image_fangzhu->setVisible(user->dwUserID == HNPlatformConfig()->getMasterID());

			auto img_head = dynamic_cast<ImageView*>(nodeItem->getChildByName("Image_head"));
			auto img_frame = dynamic_cast<ImageView*>(nodeItem->getChildByName("Image_frame"));

			// 更新用户结算榜头像
			img_head->loadTexture(user->bBoy ? "platform/head/men_head.png" : "platform/head/women_head.png");
			auto userHead = GameUserHead::create(img_head, img_frame);
			userHead->show("platform/head/men_head.png");
			userHead->setHeadByFaceID(user->bLogoID);
			if (strlen(user->headUrl)>10)
			{
				//userHead->getParent()->setLocalZOrder(-1);
				userHead->loadTextureWithUrl(user->headUrl);
				//userHead->loadTextureWithUrl("https://wx.qlogo.cn/mmopen/vi_32/FakRicR9okQjMfx7tXq8xb3T8hdibicAzVUntl6KuIiayXjCBF0SQtraqz7usAia8opqGQsrhCqVicbswPTL1sq0ibjTg/0");
			}
		}
	}
}