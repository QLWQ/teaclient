#ifndef __RunCrazy_CARDSUITE_H__
#define __RunCrazy_CARDSUITE_H__

#include "HNUIExport.h"
#include "RunCrazyPoker.h"

namespace RunCrazy
{
	class CardSuite : public HNLayer
	{
	public:
		static CardSuite* create(float scale);
		void shuffle(float duration, float radius = 350.0f, float alpha = 30.0f, float offsetY = 350.0f);
		void dealCard(float duration, Vec2 target, std::function<void()> callback);
		TargetedAction* dealCard(float duration, Vec2 target, float dstAngle = 180, float fScale = 1.0);
		void restoreCardSize(float fScale);
		void recycle(float duration);
		void runActionPair(Node* node, Action* action);

	private:
		enum
		{
			TOTAL_CARDS = 48,
		};

		typedef struct ActionPair
		{
			Node*	node;
			Action*	action;
		}ActionPair;

		bool init(float scale);
		void run(float delta);

		CardSuite(void);
		~CardSuite(void);

	private:
		Poker*	_suite[TOTAL_CARDS];
		bool		_empty;
		int			_totalCards;
		float		_scale;
		BYTE		_sent;
		std::list<ActionPair> _queue;

	};

}

#endif