/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef DouDiZhu_CalculateBoard_All_h__
#define DouDiZhu_CalculateBoard_All_h__

#include "HNUIExport.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "RunCrazyMessage.h"

using namespace ui;
using namespace cocostudio;
using namespace HN;

namespace RunCrazy
{
	class CalculateBoardAll : public HNLayer
	{	
	public:
		CREATE_FUNC(CalculateBoardAll);

		CalculateBoardAll();
		virtual ~CalculateBoardAll();

		virtual bool init() override;

		//显示结算数据
		void showAndUpdateBoard(Node* parent, const S_C_SettlementList* boardData);

	private:

		//分享按钮回调
		void shareBtnCallBack(Ref* pSender, Widget::TouchEventType type);

		//返回按钮回调
		void backBtnCallBack(Ref* pSender, Widget::TouchEventType type);

	protected:
		ImageView* _img_bg = nullptr;
	};
}


#endif // DouDiZhu_CalculateBoard_All_h__
