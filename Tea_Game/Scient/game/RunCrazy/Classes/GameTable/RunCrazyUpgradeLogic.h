/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __RunCrazyupgradelogic_h__
#define __RunCrazyupgradelogic_h__

#include "HNBaseType.h"
#include "RunCrazyMessage.h"
#include <vector>

namespace RunCrazy
{
    enum eRunCrazy
    {
        //扑克花色
        UG_FANG_KUAI = 0x00,		//方块	0000 0000
        UG_MEI_HUA = 0x10,			//梅花	0001 0000
        UG_HONG_TAO = 0x20,			//红桃	0010 0000
        UG_HEI_TAO = 0x30,			//黑桃	0011 0000
        UG_NT_CARD = 0x40,			//主牌	0100 0000
        UG_ERROR_HUA = 0xF0,		//错误  1111 0000

        //扑克出牌类型
        UG_ERROR_KIND = 0,				//错误

        UG_ONLY_ONE = 1,				//单张
        UG_DOUBLE = 2,				//对牌

        UG_VARIATION_STRAIGHT = 3,				//变种顺子(A2345)顺子中最小
        UG_STRAIGHT = 4,               //顺子,5+张连续牌
        UG_FLUSH = 5,				//同花(非连)
        UG_STRAIGHT_FLUSH = 6,               //同花顺,花色相同的顺子

        UG_THREE = 7,				//三张
        UG_THREE_ONE = 8,               //3 带 1
        UG_THREE_TWO = 9,               //3 带 2
        UG_THREE_DOUBLE = 10,				//3 带1对

        UG_VARIATION_DOUBLE_SEQUENCE = 11,				//变种双顺(AA22)最小
        UG_DOUBLE_SEQUENCE = 12,				//连对,2+个连续的对子

        UG_VARIATION_THREE_SEQUENCE = 13,				//变种三顺(AAA222最小)
        UG_THREE_SEQUENCE = 14,				//连三张，2+个连续的三张

        UG_VARIATION_THREE_ONE_SEQUENCE = 15,				//变种三顺带一
        UG_THREE_ONE_SEQUENCE = 16,              //2+个连续的三带一

        UG_VARIATION_THREE_TWO_SEQUENCE = 17,				//变种三顺带二
        UG_THREE_TWO_SEQUENCE = 18,				//2+个连续的三带二

        UG_VARIATION_THREE_DOUBLE_SEQUENCE = 19,				//变种三连张带对
        UG_THREE_DOUBLE_SEQUENCE = 20,						//三连张带对

        UG_VARIATION_THREE_SEQUENCE_DOUBLE_SEQUENCE = 21,		//变种蝴蝶(三顺带二顺)
        UG_THREE_SEQUENCE_DOUBLE_SEQUENCE = 22,				//蝴蝶(三顺带二顺)

        UG_FOUR_SEQUENCE						= 28,				//四顺
		UG_FOUR_ONE								= 29,				//四顺带1
		UG_FOUR_TWO								= 30,				//四顺带2
		UG_FOUR_THREE							= 31,				//四顺带3
		UG_FOUR_THREE_DOUBLE						= 32,			//四顺带3对


        UG_SLAVE_510K = 37,              //510K炸弹,花色不同
        UG_MASTER_510K = 38,              //510K同花炸弹

        UG_BOMB = 39,				    //炸弹>=4張
		UG_3ABOMB = 40,				    //炸弹
        UG_BOMB_SAME_HUA = 41,			//同花炸弹(在四副或以上的牌中出现)
		UG_KING_BOMB = 42,				//王炸(最大炸弹)

		UG_THREEA_TWO = 58,               //3A 带 2
		UG_THREEA_THREE = 59,              //3A 带 3
		UG_THREEA_THREEDOUBLE = 60,			//3A 带3对

    };

	//操作掩码
	enum 
	{
		UG_HUA_MASK					=0xF0,			//1111 0000
		UG_VALUE_MASK				=0x0F,			//0000 1111
	};


    //510K逻辑类 支持 2 副扑克）
    class CUpGradeGameLogic
    {
        //变量定义
    private:

        BYTE				m_bSortCardStyle;		//排序方式
        int					m_iCondition;			//限制条件
		BYTE				m_bThreeaaa;			//是否3A最大（0最大  1最小）
		BYTE				m_bThree_Tape = 1;			//三带二带“2”(0不可带“2”，1可带“2”)
		BYTE				m_bDisarmBomb = 0;			//是否可拆炸弹（0可拆  1不可拆）
		BYTE				m_bFour_Tape = 1;			//四带几（0四带二，1四带三，2不可带）
        bool				m_bKingCanReplace;		//王是否可当
        unsigned int		m_iCardShape;			//支持牌型
        //函数定义
    public:
        //构造函数		
        CUpGradeGameLogic(void);
        //析构函数
        virtual ~CUpGradeGameLogic();

        //功能函数（公共函数）
    public:
        //[设置相关]
        //获取扑克数字
        inline int GetCardNum(BYTE iCard) { return (iCard&UG_VALUE_MASK) + 1; }
        //获取扑克花色(默认为真实花色)
        BYTE GetCardHuaKind(BYTE iCard, bool bTrueHua = true);
        //获取扑克相对大小(默认为牌大小,非排序大小)
        int GetCardBulk(BYTE iCard, bool bExtVal = false);
        //获取扑克牌通过相对大小
        BYTE GetCardByValue(int iCardValue);
        //設置王可以當牌
        void SetKingCanReplace(bool bKingCanReplace = false){ m_bKingCanReplace = bKingCanReplace; }
        //獵取王是否可以當牌
        bool GetKingCanReplace(){ return m_bKingCanReplace; }
        //设置排序方式
        void SetSortCardStyle(BYTE SortCardStyle){ m_bSortCardStyle = SortCardStyle; }
        //获取排序方式
        BYTE GetSortCardStyle(){ return m_bSortCardStyle; }

        //[排序]
    public:
        //排列扑克,按大小(保留系统序例)
        bool SortCard(BYTE iCardList[], BYTE bUp[], BYTE iCardCount, bool bSysSort = false);
        //反转牌顺(从低->高)
        bool ReverseCard(BYTE iCardList[], BYTE bUp[], BYTE iCardCount);
        //按牌型排序
        bool SortCardByStyle(BYTE iCardList[], BYTE iCardCount);
        //按花色排序
        bool SortCardByKind(BYTE iCardList[], BYTE iCardCount);
    public:
        //混乱扑克
        BYTE RandCard(BYTE iCard[], int iCardCount, int iRoomId, bool bHaveKing = false);		//bHaveKing表示是否有大小猫,false无,ture有
        //删除扑克
        int RemoveCard(BYTE iRemoveCard[], int iRemoveCount, BYTE iCardList[], int iCardCount);

        int GetCardCount(BYTE iCard[], int iCardCount);

        bool IsLegalCard(BYTE iCard);
    private:
        //清除 0 位扑克
        int RemoveNummCard(BYTE iCardList[], int iCardCount);

    public://[辅助函数]
        //对比单牌
        bool CompareOnlyOne(BYTE iFirstCard, BYTE iNextCard);
        //查找分数
        int FindPoint(BYTE iCardList[], int iCardCount);
        //是否为同一数字牌
        bool IsSameNumCard(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //是否为同一花色
        bool IsSameHuaKind(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //查找 >=4 炸弹的数量炸弹基数
        BYTE GetBombCount(BYTE iCardList[], int iCardCount, int iNumCount = 4, bool bExtVal = false);
        //获取指定大小牌个数
        BYTE GetCountBySpecifyNumCount(BYTE iCardList[], int iCardCount, int Num);
        //获取指定牌个数 //bIngoneColor 是否忽略颜色
        BYTE GetCountBySpecifyCard(BYTE iCardList[], int iCardCount, BYTE bCard,bool bIngoneColor = false);
        //获取指定牌张数牌大小(队例中只能够有一种牌的张数为iCount,不然传出去的将是第一个指定张数的值)
        BYTE GetBulkBySpecifyCardCount(BYTE iCardList[], int iCardCount, int iCount);
        //是否为某指定的顺子(变种顺子)
        bool IsVariationSequence(BYTE iCardList[], int iCardCount, int iCount);
        //是否为某指定的顺子
        bool IsSequence(BYTE iCardList[], int iCardCount, int iCount);
        //提取指定的牌
        BYTE TackOutBySpecifyCard(BYTE iCardList[], int iCardCount, BYTE bCardBuffer[], int &iResultCardCount, BYTE bCard);
        //提取某张指定数字的牌
        bool TackOutCardBySpecifyCardNum(BYTE iCardList[], int iCardCount, BYTE iBuffer[], int &iBufferCardCount, BYTE iCard, bool bExtVal = false);
        //提取所有符合条件的牌,单张,对牌,三张,4炸弹牌型
        int TackOutBySepcifyCardNumCount(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], BYTE bCardNum, bool bExtVal = false);
		//辅助查找（AAA）
		int Tack3AOutBySepcifyCardNumCount(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], BYTE bCardNum,bool bExtVal = false);
		//获取3A张数
		BYTE Get3ACountBySpecifyNumCount(BYTE iCardList[], int iCardCount, int Num);

		BYTE TackOut3AX(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int  xValue);

        //提取指定花色牌
        int TackOutByCardKind(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], BYTE iCardKind);
        //拆出(将手中牌多的拆成少的)
        int TackOutMuchToFew(BYTE iCardList[], int iCardCount, BYTE iDoubleBuffer[], int &iBufferCardCount, BYTE iCardMuch, BYTE iCardFew);
        //查找大于iCard的单牌所在iCardList中的序号
        BYTE GetSerialByMoreThanSpecifyCard(BYTE iCardList[], int iCardCount, BYTE iCard, BYTE iBaseCardCount, bool bExtValue = false);
        //查找==iCard的单牌所在iCardList中的序号(起始位置,到終點位置)
        int GetSerialBySpecifyCard(BYTE iCardList[], int iStart, int iCardCount, BYTE iCard);
        //获取指定顺子中牌点最小值(iSequence 代表顺子的牌数最多为
        BYTE GetBulkBySpecifySequence(BYTE iCardList[], int iCardCount, int iSequence = 3);
        //获取指定顺子中牌点最大值变种顺子
        BYTE GetBulkBySpecifyVariationSequence(BYTE iCardList[], int iCardCount, int iSequence = 3);
        //查找最小 (1) or 最大 (255) 牌
        int	GetBulkBySepcifyMinOrMax(BYTE iCardList[], int iCardCount, int MinOrMax, bool bExtVal = false);
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //[牌型相关]
    public:
        //获取牌型
        BYTE GetCardShape(BYTE iCardList[], int iCardCount, bool bExlVal = false);
        //是否单牌
        inline bool IsOnlyOne(BYTE iCardList[], int iCardCount) { return iCardCount == 1; };
        //是否对牌
        bool IsDouble(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //3 带 1or2(带一对带二单张或带一单张
        bool IsThreeX(BYTE iCardList[], int iCardCount, int iX/*1or2*/, bool bExtVal = false);
        //王炸
        bool IsKingBomb(BYTE iCardList[], int iCardCount);
        //4+张牌 炸弹
        bool IsBomb(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//3A炸弹
		bool Is3ABomb(BYTE iCardList[], int iCardCount, bool bExtVal = false);
		//3A 带 1or2(带一对带二单张或带一单张
		bool Is3AThreeX(BYTE iCardList[], int iCardCount, int iX/*1or2*/, bool bExtVal = false);
        //同花炸弹
        bool IsBombSameHua(BYTE iCardList[], int iCardCount);
        //同花(非顺子)
        bool IsFlush(BYTE iCardList[], int iCardCount);
        //是否是同花顺
        bool IsStraightFlush(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //变种单甩
        bool IsVariationStraight(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //单甩
        bool IsStraight(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //变种对甩
        bool IsVariationDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //对甩 //连对?
        bool IsDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal = false);

        //是否变种是连续的三带X(0,1,2,3)
        bool IsVariationThreeXSequence(BYTE iCardList[], int iCardCount, int iSeqX/*0,1or2*/, bool bExtVal = false);
        //是否是连续的三带X(0,1,2,3)
        bool IsThreeXSequence(BYTE iCardList[], int iCardCount, int iSeqX/*0,1or2*/, bool bExtVal = false);
        //是否三顺带二顺(蝴蝶)
        bool IsThreeSequenceDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //是否三顺带二顺(蝴蝶)
        bool IsVariationThreeSequenceDoubleSequence(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //510K 炸弹
        bool IsSlave510K(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //510K 同花炸弹
        bool IsMaster510K(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //四带一或者四带二
        bool IsFourX(BYTE iCardList[], int iCardCount, int iX/*1or 2*/);//单张1,任意张2,一对子,2对4
        //是否变种四带X顺
        bool IsVariationFourXSequence(BYTE iCardList[], int iCardCount, int iSeqX);
        //四带一或者四带二的顺子
        bool IsFourXSequence(BYTE iCardList[], int iCardCount, int iSeqX);
        //[出牌相关]
    public:
        //自动出牌函数
        bool AutoOutCard(BYTE iHandCard[], int iHandCardCount, BYTE iBaseCard[], int iBaseCardCount, BYTE iResultCard[], int & iResultCardCount, bool bFirstOut);
        //是否可以出牌
        bool CanOutCard(BYTE iOutCard[], int iOutCount, BYTE iBaseCard[], int iBaseCount, BYTE iHandCard[], int iHandCount, bool bFirstOut = false);
        //查找比当前出牌大的
        bool TackOutCardMoreThanLast(BYTE iHandCard[], int iHandCardCount, BYTE iBaseCard[], int iBaseCardCount,
            BYTE iResultCard[], int & iResultCardCount, bool bExtVal = false);

        //[提取牌]
    public:
        //提取单个的三带1 or 2or 3(单,一对,或二单张)
		BYTE TackOutThreeX(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int  xValue, bool bNoComp = false);
        //提取2个以上连续的三带1,2
        bool TrackOut3XSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int xValue);
		//提取单个的四带1 or 2or 3 (单,二单张,3张)
		bool TackOutForX(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int  xValue);
		//提取四带3对子
		bool TackOutForSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int  xValue);
        //提取2个以上连续的三带1,2
		bool TrackOut3Sequence2Sequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, bool bNoComp = false);
        //获取顺子中最小位置值(xSequence表示默认单顺)
        int GetSequenceStartPostion(BYTE iCardList[], int iCardCount, int xSequence = 1);

		// 获取与指定牌值相同的牌数
		int GetSameCardValueCount(BYTE* pCards, int count, BYTE cardNum);

        //提取单张的顺子,连对顺子,连三顺子
        bool TackOutSequence(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount, int xSequence, bool bNoComp = false);
        //提取同花顺
        bool TackOutStraightFlush(BYTE iCardList[], int iCardCount, BYTE iBaseCard[], int iBaseCount, BYTE iResultCard[], int &iResultCardCount);
        //提取所的炸弹
        bool TackOutAllBomb(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, int iNumCount = 4);
        //提取炸弹(张数默认为4)
        bool TackOutBomb(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, int iNumCount = 4);
		//提取炸弹(AAA)
		bool Tack3AOutBomb(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, int iNumCount = 3);
        //提取王炸
        bool TackOutKingBomb(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount);
        //提取510K
        bool TrackOut510K(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, bool bExtVal = false);
        //测试510K
        bool Test510K(BYTE iCardList[], int iCardCount, bool bExtVal = false);
        //拷背
        bool Copy510K(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount);
        //拆大桌面牌
        bool TackOutCardByNoSameShape(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, BYTE iBaseCard[], int iBaseCardCount);
        ///提取指定的牌
        bool TackOutCardBySpecifyCard(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, BYTE iBaseCard[], int iBaseCardCount, BYTE iSepcifyCard);
        //用大的牌牌大桌面上的牌
        bool TackOutMoreThanLastShape(BYTE iCardList[], int iCardCount, BYTE iResultCard[], int &iResultCardCount, BYTE iBaseCard[], int iBaseCardCount);

    public:

		//设置限制条件
		void setCondition(unsigned int iCardShape){ m_iCondition = iCardShape; }
		
		//设置3A是否为最大
		void setLimit3AMax(BYTE ithreeaaa){ m_bThreeaaa = ithreeaaa; }

		//设置三带是否能带2
		void setThree_Tape(BYTE Three_Tape){ m_bThree_Tape = Three_Tape - 48; }

		//设置拆弹是否可拆
		void setDisarmBomb(BYTE DisarmBomb){ m_bDisarmBomb = DisarmBomb - 48; }

		//设置四带几
		void setFour_Tape(BYTE Four_Tape){ m_bFour_Tape = Four_Tape - 48; }

        //用户点击牌之后智能提取牌
        void AITrackOutCard(BYTE iCardList[], int iCardCount, BYTE iUpCardList[], int iUpCardCount, BYTE iBaseCardList[], int iBaseCardCount, BYTE bResult[], int & bResultCount);
        //查找最小 (1) or 最大 (255) 牌值
    
        /// [@param in bExtVal] 真，不考虑2、王
        BYTE GetCardMinOrMax(BYTE iCardList[], int iCardCount, int MinOrMax, bool bExtVal = false);


    };
}

#endif // !__upgradelogic_h__