/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RunCrazySoundManager.h"
#include "RunCrazyUpgradeLogic.h"
#include "HNNetExport.h"
#include "../LibHNUI/HNUIExport.h"

namespace RunCrazy
{
    const static char* OUT_CARD_SOUND_PATH = "RunCrazy/audio/effect/";
    const static char* EFFECT_WIN = "RunCrazy/audio/effect/Audio_You_Win.mp3";
    const static char* EFFECT_LOSS = "RunCrazy/audio/effect/Audio_You_Lose.mp3";
    const static char* EFFECT_GAME_START = "RunCrazy/audio/effect/Audio_Open_Chest.mp3";
    const static char* EFFECT_OUT_CARD = "RunCrazy/audio/effect/Audio_Out_Card.mp3";

    SoundManager::SoundManager()
    {
    }


    SoundManager::~SoundManager()
    {
    }


    //播放背景音乐
    void SoundManager::PlayBgSound()
    {
        //加载音乐和音效文件
        HNAudioEngine::getInstance()->playBackgroundMusic("RunCrazy/audio/music/gameback.mp3", true);

		float l = UserDefault::getInstance()->getIntegerForKey(MUSIC_VALUE_TEXT, 50) / 100.f;
		HNAudioEngine::getInstance()->setBackgroundMusicVolume(l);
		if (l == 0.0f)
		{
			HNAudioEngine::getInstance()->pauseBackgroundMusic();
		}

		float f = UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT, 50) / 100.f;
		HNAudioEngine::getInstance()->setEffectsVolume(f);
    }

    //播放出牌音效
    //iCardType 牌类型
    //iCardValue 牌值
    void SoundManager::PlayOutCardSound(int iCardType, int iCardValue, bool bBoyOrGirl)
    {
        std::string filename(OUT_CARD_SOUND_PATH);
        std::string soundName;
        switch (iCardType)
        {
        case UG_ONLY_ONE: //单张
        {
            if (iCardValue <= 0 || iCardValue > 0x0f)
            {
                return;
            }
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Single_%d_M.mp3", iCardValue);
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Single_%d_W.mp3", iCardValue);
            }

            break;
        }
        case UG_DOUBLE:   //对牌
        {

            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Double_%d_M.mp3", iCardValue);
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Double_%d_W.mp3", iCardValue);
            }

            break;
        }
        case UG_THREE:    //三张
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Three_0_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Three_0_W.mp3");
            }
            break;
        }
		case UG_3ABOMB:
        case UG_BOMB:	//四张 炸弹
        {

            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Bomb_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Bomb_W.mp3");
            }
            break;
        }
        case UG_THREE_ONE: //三带一
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Three_1_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Three_1_W.mp3");
            }
            break;
        }
        case UG_THREE_TWO: //三带二张
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Three_2_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Three_2_W.mp3");
            }
            break;
            break;
        }
        case UG_THREE_DOUBLE:	//三带一对
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Three_2_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Three_2_W.mp3");
            }
            break;
        }
        case UG_STRAIGHT: //顺子
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Straight_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Straight_W.mp3");
            }
            break;
        }
        case UG_DOUBLE_SEQUENCE:
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_DoubleLine_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_DoubleLine_W.mp3");
            }
            break;
        }
		case UG_VARIATION_THREE_SEQUENCE: //飞机
		case UG_THREE_SEQUENCE:
		case UG_VARIATION_THREE_TWO_SEQUENCE:
		case UG_THREE_TWO_SEQUENCE:
		case UG_VARIATION_THREE_DOUBLE_SEQUENCE:
		case UG_THREE_DOUBLE_SEQUENCE:
		case UG_VARIATION_THREE_SEQUENCE_DOUBLE_SEQUENCE:
		case UG_THREE_SEQUENCE_DOUBLE_SEQUENCE:
		{
			if (bBoyOrGirl)
			{
				soundName = StringUtils::format("Audio_Card_Plane_M.mp3");
			}
			else
			{
				soundName = StringUtils::format("Audio_Card_Plane_W.mp3");
			}
			break;
		}
		case UG_FOUR_THREE:
		case UG_FOUR_THREE_DOUBLE:
		{
			if (bBoyOrGirl)
			{
				soundName = StringUtils::format("Audio_Card_Four_Take_3_M.mp3");
			}
			else
			{
				soundName = StringUtils::format("Audio_Card_Four_Take_3_W.mp3");
			}
			break;
		}
        //case UG_FOUR_ONE: //四带1
        //{
        //	if (bBoyOrGirl)
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_1_M.mp3");
        //	}
        //	else
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_1_W.mp3");
        //	}
        //	break;
        //}
        //case UG_FOUR_TWO: //四带2
        //{
        //	if (bBoyOrGirl)
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_2_M.mp3");
        //	}
        //	else
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_2_W.mp3");
        //	}
        //	break;
        //}
        //case UG_FOUR_ONE_DOUBLE: //四带1对
        //{
        //	if (bBoyOrGirl)
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_1_M.mp3");
        //	}
        //	else
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_1_W.mp3");
        //	}
        //	break;
        //}
        //case UG_FOUR_THREE: //四带2对
        //{
        //	if (bBoyOrGirl)
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_2Double_M.mp3");
        //	}
        //	else
        //	{
        //		soundName = StringUtils::format("Audio_Card_Four_Take_2Double_M.mp3");
        //	}
        //	break;
        //}
        case PASS: //四带2对
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Pass2_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Pass2_W.mp3");
            }
            break;
        }
        case LEFT_ONE_CARD: //四带2对
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Last_1_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Last_1_W.mp3");
            }
            break;
        }
        case LEFT_TWO_CARD: //四带2对
        {
            if (bBoyOrGirl)
            {
                soundName = StringUtils::format("Audio_Card_Last_2_M.mp3");
            }
            else
            {
                soundName = StringUtils::format("Audio_Card_Last_2_W.mp3");
            }
            break;
        }
		case CARD_TYPE:
		{
			if (bBoyOrGirl)
			{
				soundName = StringUtils::format("Bomb_fuji.mp3");
			}
			break;
		}
        default:
            break;
        }
        filename.append(soundName);
        ////播放音乐
        //HNAudioEngine::getInstance()->preloadEffect(filename.c_str());
        //HNAudioEngine::getInstance()->setEffectsVolume(0.9);
        //播放音效
        HNAudioEngine::getInstance()->playEffect(filename.c_str());


    }

    //按钮声音
    void SoundManager::PlayOtherSound(int iSoundType)
    {
        std::string filename(OUT_CARD_SOUND_PATH);
        std::string soundName;
        switch (iSoundType)
        {
        case SOUND_MOUSE_MOVE: //单张
        {
            soundName = StringUtils::format("Audio_Card_Click.mp3");
            break;
        }
        case SOUND_MOUSE_DOWN:   //对牌
        {
            soundName = StringUtils::format("Audio_Card_Click.mp3");
            break;
        }
        default:
            break;
        }
        filename.append(soundName);
        //播放音乐
		float f = UserDefault::getInstance()->getIntegerForKey(SOUND_VALUE_TEXT, 50) / 100.f;
		HNAudioEngine::getInstance()->setEffectsVolume(f);
        //HNAudioEngine::getInstance()->setEffectsVolume(0.9);
        //播放音效
        HNAudioEngine::getInstance()->playEffect(filename.c_str());
    }

    //设置背景声音大小
    void SoundManager::SetBgValue(float fBgValue)
    {
        HNAudioEngine::getInstance()->setBackgroundMusicVolume(fBgValue);
    }

    //设置音效声音大小
    void SoundManager::SetEffectValue(float fEffectValue)
    {
        HNAudioEngine::getInstance()->setEffectsVolume(fEffectValue);
    }

    void SoundManager::playWinLose(bool win)
    {
        HNAudioEngine::getInstance()->playEffect(win ? EFFECT_WIN : EFFECT_LOSS);
    }

    void SoundManager::playGameStart()
    {
        HNAudioEngine::getInstance()->playEffect(EFFECT_GAME_START);
    }

    void SoundManager::playOutCard()
    {
        HNAudioEngine::getInstance()->playEffect(EFFECT_OUT_CARD);
    }
}