/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __RunCrazyTableUICallback_h__
#define __RunCrazyTableUICallback_h__

#include "HNNetExport.h"
#include "HNLogicExport.h"
#include "RunCrazyMessage.h"

namespace RunCrazy
{
	class TableUICallback  : public HN::IHNGameLogicBase
	{
	public:
		//更新用户信息
		//userNicName 玩家的昵称
		//i64UserMoney 玩家的金币
		//byDeskStation 玩家的视图座位号
		virtual void UpdateUserInfo(const int Seatno, const UserInfoStruct* user) = 0;

		//更新玩家的金币
		//_i64UserMoney 玩家金币
		//byDeskStation 玩家的视图座位号
		virtual void SetUserMoney(LLONG _i64UserMoney, BYTE byDeskStation) = 0;

		//更新玩家的经纬度
		virtual void UpDateUserLocation(int Index) = 0;

		//扣除玩家发送特效金币
		virtual void afterScoreByDeskStation(int Station, int money) = 0;

		//等待同意状态界面
		virtual void GameStationWaitAgree() = 0;

		//游戏中界面状态
		virtual void GameStationPlaying(S_C_GameStation_Play * pPlaying) = 0;

		//显示定时器
		//iTimeCount 倒计时显示的总时间
		//byDeskViewStation 玩家的视图座位号
		//bShowOrHide 0 表示隐藏 1 表示显示
		//bIsReadyOrOut 0 时间到关闭客户端 1 时间到自动出牌
		virtual void ShowTime_PDK(int iTimeCount, BYTE byDeskViewStation) = 0;

		virtual void ShowFirstOutCard(BYTE byDeskView, BYTE FirstNeetOutCard) = 0;
		virtual void removeFirstOutCard() = 0;

		//点击开始，同意后的准备图片的显示
		//byDeskViewStation 玩家的视图座位号
		//bIsShowOrHide 0 表示隐藏 1 表示显示
		virtual void ShowReadyImage(BYTE byDeskViewStation, bool bIsShowOrHide) = 0;

		virtual void ShowUserOutLine(BYTE byDeskViewStation, bool bIsShowOrHide) = 0;

		//显示出牌、提示按钮
		virtual void ShowRefferOutCardBtn(BYTE byDeskViewStation) = 0;

		//设置手牌
		//byDeskViewStation 玩家的视图座位号
		//byCard 玩家收牌数组
		//byCardCount 玩家手牌的总数
		virtual void SetUserCard(BYTE byDeskViewStation, BYTE * byCard, BYTE byCardCount) = 0;

		
        //设置出牌
		//byDeskViewStation 玩家的视图座位号
		//byCard 玩家出牌数组
		//byCardCount 玩家出牌的总数
		virtual void SetUserOutCard(BYTE byDeskViewStation, BYTE * byCard, BYTE byCardCount) = 0;
		virtual void setOtherUserPokerCount(BYTE SeatNo, BYTE byCardCount) = 0;
		virtual void setGamePlayRules(BYTE Wanfa, BYTE Difen,BYTE beishu) = 0;
		//virtual void setGame3AMax(BYTE ithreeaaa) = 0;
		virtual void setGameMultiple(int Multiple) = 0;

		//设置游戏信息
		virtual void setDeskInfo(UINT GameID) = 0;

		//显示游戏规则文字
		virtual void setDeskStr(string str) = 0;

		//获取游戏规则信息
		virtual void setDeskConfig(int index) = 0;

		//结算界面显示
		//pGameEnd 结算数据结构
		virtual void ShowSettlement(S_C_GameEndStruct * pGameEnd, bool bShowOrHide) = 0;

		virtual void GameOverInfo(S_C_SettlementList *) = 0;
		//显示不出图片
		virtual void ShowDoNotOutCard(BYTE byDeskViewNo, bool bShowOrHide) = 0;

		//显示发牌动画
		virtual void PlaySendCardAnimation() = 0;
		virtual void SendCardFinish() = 0;

		virtual void setNewTurnOutCard(bool isNewTurn) = 0;

		//显示托管标识
		virtual void ShowTuoGuanLogo(BYTE byDeskViewStation, bool bShowOrHide) = 0;
		//显示报警标识
		virtual void ShowBaoJing(BYTE byDeskViewStation, bool bShowOrHide) = 0;

		virtual void dealLeaveDesk() = 0;

        virtual void userUp(BYTE viewseat) = 0;

        // 托管按钮
		virtual void showTuoguanBtn(bool visible) = 0;
		virtual void showActionBtn(bool visible) = 0;

        //播放音效
        virtual void playEffect(BYTE value) = 0;

        // 游戏开始动画特效
        virtual void playGameStartAnimi() = 0;
		virtual void ResetUI() = 0;


		virtual void onChatTextMsg(int userID, std::string msg) = 0;									// 文本聊天消息

		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime) = 0;						// 语音聊天消息

		virtual void onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index) = 0;	//处理玩家之间的动作特效

		virtual void setCardCountResult(std::vector<std::vector<BYTE>> card) = 0;

		virtual void ShowTimeLeaveGame(int iTimeCount) = 0;

		//发送红包任务状态
		virtual void CheckActivity() = 0;
		//发送红包奖励
		virtual void CheckReceive() = 0;

		//更新红包任务状态
		virtual void setActivityNum(int Current, int toal, int CanTake) = 0;
		virtual void setReceiveState(int ReceiveType, int AwardNum) = 0;
	};

}

#endif
