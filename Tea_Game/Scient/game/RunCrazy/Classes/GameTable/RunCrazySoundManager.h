/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __RunCrazySoundManager_h__
#define __RunCrazySoundManager_h__

const int SOUND_MOUSE_MOVE = 103;
const int SOUND_MOUSE_DOWN = 104;
const int PASS = 100;
const int LEFT_ONE_CARD = 101;
const int LEFT_TWO_CARD = 102;
namespace RunCrazy
{
    class SoundManager
    {
    public:
        SoundManager();
        ~SoundManager();

        //播放背景音乐
        static void PlayBgSound();

        //播放出牌音效
        //iSoundType 音效类型
        static void PlayOutCardSound(int iSoundType, int iCardValue, bool bBoyOrGirl);

        //按钮声音
        static void PlayOtherSound(int iSoundType);

        //设置背景声音大小
        static void SetBgValue(float fBgValue);

        //设置音效声音大小
        static void SetEffectValue(float fEffectValue);

        static void playWinLose(bool win);

        static void playGameStart();

        static void playOutCard();

        ////设置性别
        //void SetSex(bool bBoyOrGirl);

        ////设置性别
        //static void ClearSound();  
    };

}

#endif //__RunCrazySoundManager_h__