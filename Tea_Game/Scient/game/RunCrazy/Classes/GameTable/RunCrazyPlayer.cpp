/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RunCrazyPlayer.h"
#include <spine/spine-cocos2dx.h>
#include "spine/spine.h"
#include "UMeng/UMengSocial.h"

using namespace spine;

namespace RunCrazy
{
	static const char* USER_HEAD_MASK = "platform/common/touxiangdi.png";
	Player::Player()
	{
		userID = 0;
		currenttime = 0;
	}
	void Player::LoadRes(Node * parent)
	{
		_PlayerUI.Root = parent;
		auto plan = parent->getChildByName("Panel_user");
		auto img = plan->getChildByName("Image_back");

		_PlayerUI.OutLine = dynamic_cast<ImageView*>(img->getChildByName("Image_outline"));
		_PlayerUI.HostPlayer = dynamic_cast<ImageView*>(img->getChildByName("Image_host"));
		//string str = parent->getName();
		//设置用户头像上的房主位置
		if (parent->getName().compare("user0") == 0)
		{
			_PlayerUI.HostPlayer->setPosition(Vec2(150, 235));
		}
		else if (parent->getName().compare("user1") == 0)
		{
			_PlayerUI.HostPlayer->setPosition(Vec2(150, 213));
		}
		else if (parent->getName().compare("user2") == 0)
		{
			_PlayerUI.HostPlayer->setPosition(Vec2(150, 213));
		}
		
		_PlayerUI.Name = dynamic_cast<Text*>(img->getChildByName("Text_name"));
		_PlayerUI.Money = dynamic_cast<TextBMFont*>(img->getChildByName("Text_money"));
		_PlayerUI.Image_icon = dynamic_cast<ImageView*>(img->getChildByName("Image_icon"));
		if (dynamic_cast<ImageView*>(img->getChildByName("Image_difen")))
		{
			_PlayerUI.Image_difen = dynamic_cast<ImageView*>(img->getChildByName("Image_difen"));
		}
		if (dynamic_cast<ImageView*>(img->getChildByName("Image_beishu")))
		{
			_PlayerUI.Image_beishu = dynamic_cast<ImageView*>(img->getChildByName("Image_beishu"));
		}
		if (dynamic_cast<ImageView*>(img->getChildByName("Image_fen")))
		{
			_PlayerUI.Image_fen = dynamic_cast<ImageView*>(img->getChildByName("Image_fen"));
		}
		if (dynamic_cast<TextBMFont*>(img->getChildByName("Text_difen")))
		{
			_PlayerUI.Points = dynamic_cast<TextBMFont*>(img->getChildByName("Text_difen"));
		}
		if (dynamic_cast<TextBMFont*>(img->getChildByName("Text_beishu")))
		{
			_PlayerUI.Multiple = dynamic_cast<TextBMFont*>(img->getChildByName("Text_beishu"));
		}
		if (dynamic_cast<TextBMFont*>(img->getChildByName("Text_fen")))
		{
			_PlayerUI.Integral = dynamic_cast<TextBMFont*>(img->getChildByName("Text_fen"));
			_PlayerUI.Integral->setString("0");
		}
		_PlayerUI.Auto = dynamic_cast<ImageView*>(parent->getChildByName("Image_auto"));
		_PlayerUI.VipImg = dynamic_cast<ImageView*>(img->getChildByName("Image_vip"));
		_PlayerUI.VipText = dynamic_cast<TextAtlas*>(img->getChildByName("AtlasLabel_vip"));
		_PlayerUI.Root->setVisible(false);
		_PlayerUI.OutLine->setLocalZOrder(2);
		_PlayerUI.HostPlayer->setLocalZOrder(2);
		_PlayerUI.Name->setLocalZOrder(2);
		_PlayerUI.VipImg->setLocalZOrder(3);
		_PlayerUI.VipImg->setVisible(false);
		_PlayerUI.VipText->setLocalZOrder(3);
		_PlayerUI.StateNode = parent->getChildByName("Player_state");
		auto a = dynamic_cast<ImageView*>(_PlayerUI.StateNode->getChildByName("Image_1"));
		if (a)
		{
			_PlayerUI.StateImg = dynamic_cast<ImageView*>(a->getChildByName("Image_2"));
			_PlayerUI.StateImg->loadTexture("platform/Games/yizhunbei.png");
		}

		

		/* 创建扇形计时器 */
		/*_PlayerUI.timerProgress = ProgressTimer::create(Sprite::createWithSpriteFrameName("Games/RunCrazy/player/kuang.png"));
		_PlayerUI.timerProgress->setNormalizedPosition(Vec2::ANCHOR_MIDDLE);
		_PlayerUI.timerProgress->setReverseProgress(true);
		_PlayerUI.timerProgress->setType(ProgressTimer::Type::RADIAL);
		_PlayerUI.timerProgress->setPosition(_PlayerUI.StateNode->getPosition());
		parent->addChild(_PlayerUI.timerProgress);*/

		//倒计时闹钟
		_PlayerUI.Clock = ImageView::create("platform/Games/clock.png");
		_PlayerUI.Clock->setPosition(_PlayerUI.StateNode->getPosition());
		_PlayerUI.Clock->setScale(1.3);
		_PlayerUI.Clock->setVisible(false);
		parent->addChild(_PlayerUI.Clock);

		//倒计时数字
		_PlayerUI.Clock_Text = TextAtlas::create("", "platform/Games/clock_text.png", 18, 24, "0");
		Size size = _PlayerUI.Clock->getContentSize();
		_PlayerUI.Clock_Text->setPosition(Vec2(size.width/2,size.height/2 - 3));
		_PlayerUI.Clock->addChild(_PlayerUI.Clock_Text);


		// 头像
		_PlayerUI.FrameImg = dynamic_cast<ImageView*> (img->getChildByName("Image_frame"));
		_PlayerUI.headImg = dynamic_cast<ImageView*> (img->getChildByName("Image_head"));
		_PlayerUI.HeadImg = GameUserHead::create(_PlayerUI.headImg, _PlayerUI.FrameImg);
		_PlayerUI.HeadImg->setZOrder(1);
		_PlayerUI.HeadImg->show(USER_HEAD_MASK);

		
	}


	void Player::ResetPlayerUI()
	{
		_PlayerUI.StateNode->setVisible(false);
		_PlayerUI.Auto->setVisible(false);
		_PlayerUI.OutLine->setVisible(false);

	}
	void Player::showReady(bool show)
	{
		_PlayerUI.StateNode->setVisible(show);
		_PlayerUI.StateImg->loadTexture("platform/Games/yizhunbei.png");
	}
	void Player::showAuto(bool show)
	{
		_PlayerUI.Auto->setVisible(show);
	}
	void Player::showHost(bool show)
	{
		_PlayerUI.HostPlayer->setVisible(show);
	}
	void Player::showOutLine(bool show)
	{
		_PlayerUI.OutLine->setVisible(show);
		if (show)
		{
			_PlayerUI.StateNode->setVisible(false);
		}
	}
	void Player::setUserID(int ID)
	{
		userID = ID;
		if (userID==0)
		{
			_PlayerUI.Root->setVisible(false);
		}
		else
		{
			_PlayerUI.Root->setVisible(true);
		}
	}

	void Player::setUserMoney(LLONG money)
	{
		if (_PlayerUI.Image_fen)
		{
			_PlayerUI.Image_fen->setVisible(false);
			_PlayerUI.Integral->setVisible(false);
		}
		_PlayerUI.Money->setString(StringUtils::format("%d",money));//StringUtils::format("%.2f", (double)money * 0.01));
		auto size = _PlayerUI.Money->getContentSize();
		if (size.width > 124)
		{
			auto scaleX = (float)124 / size.width;
			_PlayerUI.Money->setScaleX(scaleX);
		}	
	}

	void Player::setUserIntegral(LLONG Integral)
	{
		_PlayerUI.Money->setVisible(false);
		_PlayerUI.Image_icon->setVisible(false);
		if (_PlayerUI.Image_fen)
		{
			_PlayerUI.Image_fen->setVisible(true);
			_PlayerUI.Integral->setVisible(true);
			_PlayerUI.Integral->setString(StringUtils::format("%d", Integral));
		}
	}

	void Player::setUserPoints(BYTE difen)
	{
		if (_PlayerUI.Points)
		{
			_PlayerUI.Points->setVisible(true);
			_PlayerUI.Points->setString(StringUtils::format("%d", difen));
		}
	}

	void Player::setUserMultiple(int Multiple)
	{
		if (_PlayerUI.Multiple)
		{
			//_PlayerUI.Multiple->setVisible(true);
			_PlayerUI.Multiple->setString(StringUtils::format("%d", Multiple));
		}
		_Multiple = Multiple;
	}

	void Player::setUserMultiplebShow(bool isShow)
	{
		if (_PlayerUI.Image_beishu)
		{
			_PlayerUI.Image_beishu->setVisible(isShow);
			_PlayerUI.Multiple->setVisible(isShow);
		}
	}

	int Player::getUserMultiple()
	{
		return _Multiple;
	}

	void Player::setUserName(std::string name)
	{
		_PlayerUI.Name->setString(GBKToUtf8(name));
	}
	void Player::setUserbBoy(bool bBoy)
	{
		//_PlayerUI.HeadImg->loadTexture(bBoy ? "platform/head/men_head.png" : "platform/head/women_head.png");

	}
	void Player::setHeadUrl(std::string url)
	{

		_PlayerUI.HeadImg->loadTexture(url);
	}
	void Player::setTextureWithUrl(std::string url)
	{
		_PlayerUI.HeadImg->loadTextureWithUrl(url);
	}
	void Player::setVIPHead(std::string url, INT VipLevel)
	{
		_PlayerUI.VipText->setVisible(false);
		_PlayerUI.FrameImg->setVisible(false);
		_PlayerUI.HeadImg->setVIPHead(url, VipLevel);
	}
	void Player::setHeadByFaceID(int faceID)
	{
		_PlayerUI.HeadImg->setHeadByFaceID(faceID);
	}
	void Player::setFrameUrl(std::string url)
	{
		_PlayerUI.FrameImg->loadTexture(url);
		_PlayerUI.FrameImg->setContentSize(_PlayerUI.HeadImg->getContentSize()*1.05);
	}
	void Player::setVipText(int rank)
	{
		_PlayerUI.VipText->setString(StringUtils::format("%d", rank));
	}
	// 打不起
	void Player::showNotout(bool show)
	{
		_PlayerUI.StateNode->setVisible(show);
		_PlayerUI.StateImg->loadTexture("platform/Games/yaobuqi.png");
	}
	void Player::showtime(bool show, int timecount)
	{
		stopTimer = currenttime = timecount;

		if (show && currenttime>0.0f)
		{
			_PlayerUI.Clock_Text->setString(StringUtils::toString(currenttime));
			_PlayerUI.Root->schedule(CC_CALLBACK_1(Player::UpdateTimer,this),1.0f,"UpdateTimer");
			if (_PlayerUI.Clock)
			{
				_PlayerUI.Clock->setVisible(true);
			}
			
		}
		else
		{
			if (_PlayerUI.Clock)
			{
				_PlayerUI.Clock->setVisible(false);
			}
			_PlayerUI.Root->unschedule("UpdateTimer");
		}
	}
	

	Vec2 Player::getChatPos()const
	{
		return  _PlayerUI.HeadImg->getParent()->convertToWorldSpace(_PlayerUI.HeadImg->getPosition());
	}


	void Player::UpdateTimer(float dt)
	{
		currenttime -= 1.0;
		if (currenttime >= 1.0f)
		{
			_PlayerUI.Clock_Text->setString(StringUtils::toString(currenttime));
		}
		else
		{
			if (_PlayerUI.Clock)
			{
				_PlayerUI.Clock->setVisible(false);
			}
			_PlayerUI.Root->unschedule("UpdateTimer");
		}		
	}



	

}