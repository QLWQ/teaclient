/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "HNUIExport.h"
#include "HNLobbyExport.h"
#include "RunCrazyTableUI.h"
#include "RunCrazySoundManager.h"
#include "RunCrazyPoker.h"
#include "HNLobby/CreateRoom/CreateRoom/VipRoomController.h"
#include "HNLobby/GameChildLayer/GameShareLayer.h"

#include "json/rapidjson.h"
#include "json/document.h"
#include <spine/spine-cocos2dx.h>
#include "spine/spine.h"
#include "UMeng/UMengSocial.h"
#include "HNLobby/GameChildLayer/GameUserPopupInfoLayer.h"

using namespace spine;

namespace RunCrazy
{
	const int CONTROL = 0;

	// 0000 0001 第一位表示是否选中
	// 0000 0010 第二位表示是否触碰
	const static int POKER_SELECTED = 1;
	const static int POKER_UNSELECTED = 0;
	const static int POKER_TOUCHING = 2;

	// 0010 or 0100
	static int TAG_TOUCH_BEGIN = 2;
	inline void TouchBegin()
	{
		TAG_TOUCH_BEGIN += 2;
		if (TAG_TOUCH_BEGIN > 4)
			TAG_TOUCH_BEGIN = 2;
	}

	// 手牌是否被触摸中
	inline bool IsPokerTouching(BYTE value)
	{
		value &= 0xFE;
		return value == TAG_TOUCH_BEGIN;
	}
	inline int SetPokerTouching(BYTE value)
	{
		value &= 0x01;
		value |= TAG_TOUCH_BEGIN;
		return value;
	}
	// 手牌是否被选中
	inline bool IsPokerSelected(BYTE value)
	{
		return value & 0x01;
	}
	inline BYTE SetPokerSelect(BYTE value)
	{
		return value | 0x01;
	}
	inline BYTE SetPokerUnselect(BYTE value)
	{
		return value & 0xFE;
	}
	inline BYTE SetPokerReverseSel(BYTE value)
	{
		BYTE tmp = value & 0xFE;
		value |= 0xFE;
		value = ~value;
		value = value | tmp;
		return value;
	}

	HNGameUIBase* GameTableUI::create(BYTE deskId, bool autoCreate)
	{
		GameTableUI* tableUI = new GameTableUI();
		if (tableUI->init(deskId, autoCreate))
		{
			tableUI->autorelease();
			return tableUI;
		}
		else
		{
			CC_SAFE_DELETE(tableUI);
			return nullptr;
		}
	}

	bool GameTableUI::init(BYTE deskId, bool autoCreate)
	{
		if (!HNGameUIBase::init())
		{
			return false;
		}


		_isVIPRoom = (RoomLogic()->getRoomRule() & GRR_GAME_BUY) != 0x00;

		AddloadUI();


		//初始化VIP房间控制器
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			auto roomController = VipRoomController::create(deskId);
			roomController->setName("VipRoomController");
			roomController->setRoomNumPos(Vec2(_winSize.width * 0.03, _winSize.height * 0.98f));
			roomController->setPlayCountPos(Vec2(_winSize.width * 0.03, _winSize.height * 0.94f));
			roomController->setDifenPos(Vec2(_winSize.width * 0.03, _winSize.height * 0.9f));
			roomController->setDismissBtnPos(-Vec2(_winSize.width * 0.05f, _winSize.height * 0.6f));
			roomController->setReturnBtnPos(-Vec2(_winSize.width * 0.05f, _winSize.height * 0.5f));
			roomController->setInvitationBtnPos(-Vec2(_winSize.width * 0.5f, _winSize.height * 0.35f));
			//调整红包任务位置
			roomController->setActivityPosition(211, 125.0f);
			roomController->onSetVipInfoCallBack = [this](){
				// 更新房主标志显示
				setDeskInfo();

			};
			roomController->onDissmissCallBack = [this](){

				HNLOGEX_ERROR("onDissmissCallBack");
				if (_GameEndLayer)
				{
					if (!_TableUI.SettlementNode->isVisible())
					{
						_GameEndLayer->setVisible(true);
					}
				}
			};
			roomController->onClickButtonCallBack = [this](int Index){
				//发送领取红包
				if (Index < 0 || Index > 5)
				{
					return;
				}
				else
				{
					//发送
					CheckReceive();
				}
			};
			roomController->onReceiveCallBack = [this](){
				CheckActivity();
			};
			// 
			addChild(roomController, 5);


		}

		//初始化比赛场控制器
		if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
		{
			GameMatchController* matchController = GameMatchController::create(deskId);
			matchController->setRankingTipPos(Vec2(_winSize.width * 0.6, _winSize.height * 0.97f));
			matchController->setName("GameMatchController");
			//matchController->setMatchControllerBG("platform/Games/bg.png");
			addChild(matchController, 10);
		}

		//牌桌逻辑类
		_logic = new TableLogic(this, deskId, autoCreate);
		_logic->sendGameInfo();

		return true;
	}

	GameTableUI::GameTableUI()
		: _logic(nullptr)
	{
		memset(_vecUser, 0, sizeof(_vecUser));
		memset(_actionPoint, 0, sizeof(_actionPoint));
		memset(_ActionMoney, 0, sizeof(_ActionMoney));
		memset(_Array_Distance, 0, sizeof(_Array_Distance));
		_playersPos[0] = Vec2(160, 275);
		_playersPos[1] = Vec2(245, 575);
		_playersPos[2] = Vec2(1040, 575);
		_IP_baojing[0] = Vec2(71.5, 72.8);
		_IP_baojing[1] = Vec2(78, 544);
		_IP_baojing[2] = Vec2(1206, 544);
		_ClockPos[0] = Vec2(74, 195.5);
		_ClockPos[1] = Vec2(167.6, 561.5);
		_ClockPos[2] = Vec2(1116.83, 587);
	}

	GameTableUI::~GameTableUI()
	{
		_logic->stop();
		HN_SAFE_DELETE(_logic);
		SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("platform/Games/res/baojindeng.plist");
		SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("platform/Chat/light.plist");
		NotificationCenter::getInstance()->removeObserver(this, RECONNECTION);
	}

	void GameTableUI::AddloadUI()
	{
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("platform/Games/res/baojindeng.plist");
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("platform/Chat/light.plist");
		Widget* pBg = (Widget*)CSLoader::createNode("Games/RunCrazy/gameCsb/table.csb");  //"platform/Games/res/table.csb");
		this->addChild(pBg);
		{
			pBg->setScale(_winSize.width / 1280, _winSize.height / 720);
			float scalex = 1280 / _winSize.width;
			float scaley = 720 / _winSize.height;
			for (auto i : pBg->getChildren())
			{
				i->setScale(scalex, scaley);
			}

			ImageView* backgr = dynamic_cast<ImageView*>(pBg->getChildByName("Image_back"));
			backgr->loadTexture("platform/Games/bg.jpg");
			backgr->setScale(1);
			auto down_tip = Sprite::create("platform/Games/img_hengtiao.png");
			down_tip->setPosition(Vec2(_winSize.width / 2, 35));
			backgr->addChild(down_tip);

			_TableUI.Image_DiFen = ImageView::create("");
			_TableUI.Image_DiFen->setPosition(Vec2(_winSize.width / 2 + 5, _winSize.height - 395));
			backgr->addChild(_TableUI.Image_DiFen);
			_TableUI.Image_BG = backgr;

			//设置游戏背景图上的游戏名字
			_Text_GameName = Text::create(GBKToUtf8("跑得快"), "platform/common/hkljhtjw8.ttc", 44);
			//_Text_GameName->enableOutline(Color4B(79, 153, 160, 255), 1);
			_Text_GameName->setPosition(Vec2(648, 365));
			_Text_GameName->setTextColor(Color4B(39, 129, 158, 255));
			_TableUI.Image_BG->addChild(_Text_GameName);

			_FNTtext_MatchGame = Text::create(GBKToUtf8(""), "platform/common/hkljhtjw8.ttc", 28);
			_FNTtext_MatchGame->setTextColor(Color4B(118, 85, 128, 255));
			_FNTtext_MatchGame->setPosition(Vec2(645, 405));
			_TableUI.Image_BG->addChild(_FNTtext_MatchGame);

			Layout* Panel = dynamic_cast<Layout*>(pBg->getChildByName("Panel_cancelauto"));
			Panel->setScale(1);
			for (auto i : Panel->getChildren())
			{
				i->setScale(scalex, scaley);
			}
			//原来的结算界面不用
			Node* Panel_game = dynamic_cast<Node*>(pBg->getChildByName("Panel_gamesid"));
			Panel_game->setVisible(false);
			//结算界面
			_TableUI.SettlementNode = (Node*)CSLoader::createNode("platform/Games/RunCrazy/Result/singleBoard.csb");//pBg->getChildByName("Panel_gamesid");
			pBg->addChild(_TableUI.SettlementNode);
			_TableUI.SettlementNode->setPosition(Vec2(640, 360));
			//_TableUI.SettlementNode->setF
			_TableUI.SettlementNode->setVisible(false);
			_TableUI.SettlementNode->setScale(1);
			for (auto i : _TableUI.SettlementNode->getChildren())
			{
				i->setScale(scalex, scaley);
			}

		}

		//倒计时闹钟
		_TableUI.Clock = ImageView::create("platform/Games/clock.png");
		_TableUI.Clock->setPosition(_ClockPos[0]);
		_TableUI.Clock->setScale(1.3);
		_TableUI.Clock->setVisible(false);
		pBg->addChild(_TableUI.Clock);

		//倒计时数字
		_TableUI.Clock_Text = TextAtlas::create("", "platform/Games/clock_text.png", 18, 24, "0");
		Size size = _TableUI.Clock->getContentSize();
		_TableUI.Clock_Text->setPosition(Vec2(size.width / 2, size.height / 2 - 3));
		_TableUI.Clock->addChild(_TableUI.Clock_Text);

		//显示聊天界面
		gameChatLayer();
		_TableUI.Chat = Button::create("platform/Games/dz.png", "", "platform/Games/dz.png"); //聊天
		_TableUI.Chat->setPosition(Vec2(_winSize.width - 60, 300));
		_TableUI.Chat->addClickEventListener([this](Ref *){

			if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
				return;
			}
			if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
			{
				_TableUI.chatLayer->setCustomchat();
			}
			_TableUI.chatLayer->showChatLayer();
		});
		pBg->addChild(_TableUI.Chat);

		_TableUI.voice = Button::create("platform/Games/yy.png", "", "platform/Games/yy.png");
		//_TableUI.voice->getTitleRenderer()->setVisible(false);
		_TableUI.voice->setPosition(Vec2(_winSize.width - 60, 200));
		pBg->addChild(_TableUI.voice);
		_TableUI.voice->addTouchEventListener(CC_CALLBACK_2(GameChatLayer::voiceChatUiButtonCallBack, _TableUI.chatLayer));

		{
			char buff[32] = { 0 };
			for (BYTE viewseat = 0; viewseat < PLAY_COUNT; viewseat++)
			{
				{
					// 玩家
					sprintf(buff, "user%u", viewseat);
					_players[viewseat].LoadRes(pBg->getChildByName(buff));
					//头像点击
					_TouchPlayer[viewseat] = Button::create("platform/Games/xianchu.png", "", "platform/Games/xianchu.png");
					_TouchPlayer[viewseat]->setPosition(Vec2(pBg->getChildByName(buff)->getPositionX(), pBg->getChildByName(buff)->getPositionY() + 50));
					_TouchPlayer[viewseat]->setTag(viewseat);
					_TouchPlayer[viewseat]->setName("popUserInfo");
					_TouchPlayer[viewseat]->setOpacity(0);
					_TouchPlayer[viewseat]->setVisible(false);
					pBg->addChild(_TouchPlayer[viewseat]);
					_TouchPlayer[viewseat]->addTouchEventListener(CC_CALLBACK_2(GameTableUI::getUserInfo, this));

					//相同IP报警
					Sprite* sp_IPbaojing = Sprite::createWithSpriteFrameName(StringUtils::format("ligt_%d.png", 1));
					sp_IPbaojing->setPosition(_IP_baojing[viewseat]);
					sp_IPbaojing->setName(StringUtils::format("IPbaojing_%d", viewseat));
					sp_IPbaojing->setVisible(false);
					_TableUI.Image_BG->addChild(sp_IPbaojing);

					// 小于3张报警器
					char baojingname[50] = { 0 };
					sprintf(baojingname, "Image_baojing%d", viewseat);
					_TableUI.baojingqiImg[viewseat] = dynamic_cast<ImageView*>(pBg->getChildByName(baojingname));
					_TableUI.baojingqiImg[viewseat]->setVisible(false);

					//小于3张报警动画节点
					auto sp = Sprite::createWithSpriteFrameName(StringUtils::format("ligt_%d.png", 1));
					sp->setPosition(_TableUI.baojingqiImg[viewseat]->getPosition());
					_TableUI.Sp_baojingqi[viewseat] = sp;
					pBg->addChild(sp);
					sp->setVisible(false);

					//记牌器
					char cardcountname[50] = { 0 };
					sprintf(cardcountname, "Image_cardCount%d", viewseat);
					_TableUI.pokerCountImg[viewseat] = dynamic_cast<ImageView*>(pBg->getChildByName(cardcountname));
					_TableUI.pokerCountImg[viewseat]->setVisible(false);
					_TableUI.pokerCountText[viewseat] = (Text*)_TableUI.pokerCountImg[viewseat]->getChildByName("Text_count");
				}

				// 牌池
				{
					sprintf(buff, "outcard%u", viewseat);
					_TableUI.poolcards[viewseat] = (ListView*)(pBg->getChildByName(buff)->getChildByName("ListView_hand"));
					_TableUI.poolcards[viewseat]->removeAllChildren();
					_TableUI.poolcards[viewseat]->setScrollBarOpacity(0);
					auto poker = Poker::create(0);
					poker->ignoreContentAdaptWithSize(false);
					poker->setContentSize(Size(79, 105.5));
					_TableUI.poolcards[viewseat]->setItemModel(poker);
				}
				{
					// 手牌
					sprintf(buff, "handcard%u", viewseat);
					_TableUI.HandList[viewseat] = dynamic_cast<ListView*>(pBg->getChildByName(buff)->getChildByName("ListView_hand"));
					_TableUI.HandList[viewseat]->removeAllChildren();
					_TableUI.HandList[viewseat]->setScrollBarOpacity(0);
					_TableUI.HandList[viewseat]->setScrollBarEnabled(false);
					_TableUI.HandList[viewseat]->setSwallowTouches(true);
				}
				if (viewseat == 0)
				{
					auto poker = Poker::create(0);
					poker->ignoreContentAdaptWithSize(false);
					poker->setContentSize(Size(158, 211));
					_TableUI.HandList[viewseat]->setItemModel(poker);
				}
				else
				{
					auto poker = Poker::create(0);
					poker->ignoreContentAdaptWithSize(false);
					poker->setContentSize(Size(79, 105.5));
					_TableUI.HandList[viewseat]->setItemModel(poker);
				}
				if (viewseat == 0)
				{
					_TableUI.HandList[viewseat]->setTouchEnabled(true);
					_TableUI.HandList[viewseat]->addTouchEventListener([this](Ref* sender, Widget::TouchEventType touch)
					{
						auto spos = _TableUI.HandList[0]->getTouchBeganPosition();
						auto mpos = _TableUI.HandList[0]->getTouchMovePosition();
						auto epos = _TableUI.HandList[0]->getTouchEndPosition();
						Node* poker = nullptr;
						switch (touch) {
						case Widget::TouchEventType::BEGAN:
							TouchBegin();
							poker = getCardByPos(_TableUI.HandList[0]->convertToNodeSpace(spos));
							break;
						case Widget::TouchEventType::MOVED:
							poker = getCardByPos(_TableUI.HandList[0]->convertToNodeSpace(mpos));
							break;
						case Widget::TouchEventType::CANCELED:
						case Widget::TouchEventType::ENDED:
							if (abs(epos.x - spos.x) >= 5.0f) // 单击一张牌不进行划牌提示
							{
								HuaDongSurggest();
							}
							resetTouchTag();
							break;
						default:
							break;
						}

						if (poker)
						{
							int tag = poker->getTag();
							if (!IsPokerTouching(tag))
							{
								// 设置为被触摸中
								tag = SetPokerTouching(tag);

								// 位置切换
								auto pos = poker->getPosition();
								pos += IsPokerSelected(tag) ? Vec2(0, -30) : Vec2(0, 30);
								poker->setPosition(pos);

								// TAG 切换
								tag = SetPokerTouching(tag);
								poker->setTag(SetPokerReverseSel(tag));
							}
						}

					});
				}

			}
		}

		//ShowBaoJing(0, true);

		//加载准备按钮
		_TableUI.btnReady = (Button *)pBg->getChildByName("Button_ready");
		_TableUI.btnReady->setVisible(false);
		_TableUI.btnReady->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		if (!_isVIPRoom)
		{
			_TableUI.btnReady->setPositionX(_winSize.width / 2);
		}


		_TableUI.Invaite = (Button *)pBg->getChildByName("Button_invite");
		_TableUI.Invaite->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		_TableUI.Invaite->setVisible(false);

		//加载出牌按钮
		_TableUI.btnOurCard = (Button *)pBg->getChildByName("Button_outcard");
		_TableUI.btnOurCard->setVisible(false);
		_TableUI.btnOurCard->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));

		//加载提示按钮
		_TableUI.btnSurggest = (Button *)pBg->getChildByName("Button_tishi");
		_TableUI.btnSurggest->setVisible(false);
		_TableUI.btnSurggest->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));

		//等待出牌
		_TableUI.wait = (ImageView *)pBg->getChildByName("Image_tip");
		_TableUI.wait->setVisible(false);

		//托管按钮
		_TableUI.btnTuoGuan = (Button  *)pBg->getChildByName("Button_auto");
		_TableUI.btnTuoGuan->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));

		//取消托管
		_TableUI.CancelPlane = (Layout  *)pBg->getChildByName("Panel_cancelauto");
		_TableUI.CancelPlane->setVisible(false);
		_TableUI.btnCancelTuoGuan = (Button  *)_TableUI.CancelPlane->getChildByName("Button_cancel");
		_TableUI.btnCancelTuoGuan->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		//界面退出
		_TableUI.btnExit = dynamic_cast<Button*>(pBg->getChildByName("Button_return"));
		_TableUI.btnExit->loadTextureNormal("platform/Games/fanhui.png");
		_TableUI.btnExit->setContentSize(Size(60, 66));
		_TableUI.btnExit->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			//房卡模式不显示一键退出
			_TableUI.btnExit->setVisible(false);
		}

		//设置
		_TableUI.btnSet = dynamic_cast<Button*>(pBg->getChildByName("Button_set"));
		_TableUI.btnSet->loadTextureNormal("platform/Games/sz.png");
		_TableUI.btnSet->setContentSize(Size(87, 86));
		_TableUI.btnSet->setPosition(Vec2(_winSize.width - 60, _winSize.height - 60));
		_TableUI.btnSet->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		//规则

		_TableUI.btnHelp = dynamic_cast<Button*>(pBg->getChildByName("Button_help"));
		_TableUI.btnHelp->loadTextureNormal("platform/Games/bztb.png");
		_TableUI.btnHelp->setVisible(false);
		_TableUI.btnHelp->setContentSize(Size(55, 65));
		_TableUI.btnHelp->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		//记牌器
		_TableUI.btnCardCount = dynamic_cast<Button*>(pBg->getChildByName("Button_CardCount"));
		_TableUI.btnCardCount->setVisible(false);
		_TableUI.btnCardCount->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		//记牌器图片
		_TableUI.CardCountImg = dynamic_cast<ImageView*>(pBg->getChildByName("Image_CardCount"));
		_TableUI.CardCountImg->setVisible(false);
		//解散
		_TableUI.jiesan = dynamic_cast<Button*>(pBg->getChildByName("Button_jiesan"));
		_TableUI.jiesan->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		_TableUI.jiesan->setVisible(_isVIPRoom);

		//游戏未开始时隐藏托管和记牌器按钮
		showTuoguanBtn(false);

		auto layerend = _TableUI.SettlementNode->getChildByName("Panel_back")->getChildByName("Image_back");
		//离开
		_TableUI.btnLeft = (Button *)layerend->getChildByName("Button_left");
		_TableUI.btnLeft->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		_TableUI.LeftText = (TextBMFont*)_TableUI.btnLeft->getChildByName("BitmapFontLabel_1");
		//继续
		_TableUI.btnContinue = (Button *)layerend->getChildByName("Button_continue");
		_TableUI.btnContinue->addTouchEventListener(CC_CALLBACK_2(GameTableUI::OnTouch, this));
		if (_isVIPRoom)
		{
			_TableUI.btnLeft->setVisible(false);
			_TableUI.btnContinue->setPositionX(_TableUI.btnLeft->getParent()->getContentSize().width / 2);
		}
		if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
		{
			showRestBtn(false);
		}

		//列表框
		_TableUI.SettlementInfo = (ListView *)layerend->getChildByName("ListView_score");
		_TableUI.SettlementInfo->setItemModel((Layout*)_TableUI.SettlementInfo->getChildByName("item"));
		_TableUI.SettlementInfo->removeAllChildren();

		//PlaySendCardAnimation();
		//用户IP相同报警灯


		//playEffect(UG_STRAIGHT, false);
		//playEffect(UG_VARIATION_DOUBLE_SEQUENCE, false);
		//playEffect(UG_THREE_SEQUENCE, false);
		//playEffect(UG_BOMB, false);
	}
	void GameTableUI::showRestBtn(bool bShow)
	{
		_TableUI.btnLeft->setVisible(bShow);
		_TableUI.btnContinue->setVisible(bShow);
	}
	void GameTableUI::adjustSeatCardList(int seatno)
	{
		if (seatno == 2)
		{
			float itemMargin = _TableUI.HandList[2]->getItemsMargin();  // 2号位的listview 间隔是-50；所以直接加
			int	  itemCount = _TableUI.HandList[2]->getItems().size();
			Size  itemSize = itemCount > 0 ? _TableUI.HandList[2]->getItem(0)->getContentSize() : Size(0, 0);
			_TableUI.HandList[2]->setContentSize(Size((itemSize.width + (itemSize.width + itemMargin)*(itemCount - 1)), _TableUI.HandList[2]->getContentSize().height));
			_TableUI.HandList[2]->doLayout();

			itemCount = _TableUI.poolcards[2]->getItems().size();
			itemSize = itemCount > 0 ? _TableUI.poolcards[2]->getItem(0)->getContentSize() : Size(0, 0);
			_TableUI.poolcards[2]->setContentSize(Size((itemSize.width + (itemSize.width + itemMargin)*(itemCount - 1)), _TableUI.poolcards[2]->getContentSize().height));
			_TableUI.poolcards[2]->doLayout();
		}
		else if (seatno == 0)
		{
			float itemMargin = _TableUI.HandList[0]->getItemsMargin();  // 0号位的listview 间隔是-80；所以直接加
			int	  itemCount = _TableUI.HandList[0]->getItems().size();
			Size  itemSize = itemCount > 0 ? _TableUI.HandList[0]->getItem(0)->getContentSize() : Size(0, 0);
			/*Size  size = Size((itemSize.width + (itemSize.width + itemMargin)*(itemCount - 1)), _TableUI.HandList[0]->getContentSize().height);*/
			_TableUI.HandList[0]->setContentSize(Size((itemSize.width + (itemSize.width + itemMargin)*(itemCount - 1)), _TableUI.HandList[0]->getContentSize().height));
			_TableUI.HandList[0]->doLayout();

			//出牌相对自己居中处理
			itemMargin = _TableUI.poolcards[0]->getItemsMargin();
			itemCount = _TableUI.poolcards[0]->getItems().size();
			itemSize = itemCount > 0 ? _TableUI.poolcards[0]->getItem(0)->getContentSize() : Size(0, 0);
			/*size = Size((itemSize.width + (itemSize.width + itemMargin)*(itemCount - 1)), _TableUI.poolcards[0]->getContentSize().height);*/
			_TableUI.poolcards[0]->setContentSize(Size((itemSize.width + (itemSize.width + itemMargin)*(itemCount - 1)), _TableUI.poolcards[0]->getContentSize().height));
			_TableUI.poolcards[0]->doLayout();
		}
	}

	void GameTableUI::ResetUI()
	{
		for (int viewseat = 0; viewseat < PLAY_COUNT; viewseat++)
		{
			_players[viewseat].showAuto(false);
		}
		hideAllAlert();
		clearAllPoolCard();
		clearAllHandcard();
		showActionBtn(false);
		_TableUI.HandList[0]->setContentSize(Size(1100, 192));
		setGameMultiple(1);
		//_TableUI.HandList[1]->setContentSize(Size(403, 96));
		//_TableUI.HandList[2]->setContentSize(Size(403, 96));
	}

	//更新用户信息
	void GameTableUI::UpdateUserInfo(const int Seatno, const UserInfoStruct* user)
	{

		if (!user)
		{
			_players[Seatno].setUserID(0);
			_players[Seatno].ResetPlayerUI();
			_vecUser[Seatno] = nullptr;
			_vecUserGeographic[Seatno] = "";
			for (int i = 0; i < 3; i++)
			{
				_Array_Distance[Seatno][i] = NULL;
			}
			m_isUserLeave = true;
			if (_TouchPlayer[Seatno])
			{
				_TouchPlayer[Seatno]->setVisible(false);
			}
		}
		else
		{
			//保存用户部分数据（弹窗）
			//setUserInfo(Seatno,(UserInfoStruct*)user);
			_vecUser[Seatno] = (UserInfoStruct*)user;
			_TableUI._vecUser[Seatno] = user;
			if (_TouchPlayer[Seatno])
			{
				_TouchPlayer[Seatno]->setVisible(true);
			}

			_players[Seatno].setUserID(user->dwUserID);
			if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
			{
				_actionPoint[user->bDeskStation] = user->i64Money;
				if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
				{
					_ActionMoney[user->bDeskStation] = user->i64ContestScore;
					_players[Seatno].setUserIntegral(user->i64ContestScore);
				}
				else
				{
					_players[Seatno].setUserMoney(user->i64Money);
				}
				/*_players[Seatno].setUserMoney((RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST) ? user->i64ContestScore : user->i64Money);*/
			}
			else
			{
				_ActionMoney[user->bDeskStation] = user->i64Money;
				//_players[Seatno].setUserIntegral(user->i64ContestScore);
			}
			//加点击弹窗
			//_players[Seatno]->addTouchEventListener()
			Tools::TrimSpace((char*)user->nickName);
			string strName = user->nickName;
			if (strName.length() > 12)
			{
				strName.erase(13, strName.length());
				strName.append("...");
			}
			_players[Seatno].setUserName(strName);
			_players[Seatno].setUserbBoy(user->bBoy);
			_players[Seatno].showAuto(false);
			_players[Seatno].showOutLine(false);
			//_players[Seatno].setHeadUrl("https://wx.qlogo.cn/mmopen/vi_32/FakRicR9okQjMfx7tXq8xb3T8hdibicAzVUntl6KuIiayXjCBF0SQtraqz7usAia8opqGQsrhCqVicbswPTL1sq0ibjTg/0");
			/*if (strlen(user->headUrl) > 10)
			{
			_players[Seatno].setHeadUrl(user->headUrl);
			}*/
			/*int headID = user->bLogoID;
			if (headID > 5)
			{
			headID = 5;
			}*/
			std::string name = user->bBoy ? Player_Normal_M : Player_Normal_W;
			_players[Seatno].setHeadUrl(name);
			_players[Seatno].setHeadByFaceID(user->bLogoID);
			_players[Seatno].setTextureWithUrl(user->headUrl);
			
			_players[Seatno].setVIPHead("", user->iVipLevel);
			//int headFrameID = user->bLogoKuangID;
			//std::string headFrameUrl = StringUtils::format("platform/userData/res/frame/frame_%d.png", headFrameID);
			//_players[Seatno].setFrameUrl(headFrameUrl);
			//_players[Seatno].setVipText(user->iVipLevel);
			// 显示房主标志
			if (_isVIPRoom)
			{
				_players[Seatno].showHost(user->dwUserID == HNPlatformConfig()->getMasterID());
			}
			else
			{
				_players[Seatno].showHost(false);
			}
		}

		//刷新相同IP警报（回放不可用）
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			refreshUserIPlight();
		}
	}

	//点击头像
	void GameTableUI::getUserInfo(Ref* pSender, Widget::TouchEventType type){
		if (Widget::TouchEventType::ENDED != type) {
			return;
		}
		int curDir = ((GameUserHead*)pSender)->getTag();
		auto pLayer = GameUserPopupInfoLayer::create();
		pLayer->setCurDir(curDir);
		pLayer->setMaxPlayerCount(PLAY_COUNT);
		pLayer->setUserInfo(_vecUser, PLAY_COUNT);
		pLayer->setLogicBase(_logic);
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				pLayer->setUserMoney(_actionPoint, PLAY_COUNT);
			}
			else
			{
				pLayer->setUserMoney(_actionPoint, PLAY_COUNT);
			}
		}
		else
		{
			pLayer->setUserMoney(_ActionMoney, PLAY_COUNT);
		}
		pLayer->open(HNLayer::ACTION_TYPE_LAYER::SCALE, this, Vec2::ZERO, 99999);
	}

	//更新相同IP判断
	void GameTableUI::refreshUserIPlight()
	{
		auto node = _TableUI.Image_BG;
		//如果有人退出之后报警需要更新处理
		if (m_isUserLeave)
		{
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				auto AnimationNode = node->getChildByName(StringUtils::format("light%d", i));
				Sprite* pLight = dynamic_cast<Sprite*>(node->getChildByName(StringUtils::format("IPbaojing_%d", i)));
				pLight->setVisible(false);
				if (AnimationNode)
				{
					AnimationNode->setVisible(false);
				}
			}
		}
		for (int i = 0; i < PLAY_COUNT; i++){
			int pso = _logic->viewToLogicSeatNo(i);
			auto User = UserInfoModule()->findUser(RoomLogic()->loginResult.pUserInfoStruct.bDeskNO, pso);
			if (User)
			{
				//第二轮用户信息被清理，重新获取信息
				_TableUI._vecUser[i] = User;
			}
			if (!User)
			{
				continue;
			}
			for (int j = 0; j < PLAY_COUNT; j++)
			{
				int pso1 = _logic->viewToLogicSeatNo(j);
				auto User1 = UserInfoModule()->findUser(RoomLogic()->loginResult.pUserInfoStruct.bDeskNO, pso1);
				if (User1)
				{
					//第二轮用户信息被清理，重新获取信息
					_TableUI._vecUser[j] = User1;
				}
				if (!User1 || i == j)
				{
					continue;
				}
				if (_TableUI._vecUser[i]->dwUserID != _TableUI._vecUser[j]->dwUserID)
				{
					if (_TableUI._vecUser[i]->dwUserIP == _TableUI._vecUser[j]->dwUserIP &&
						_TableUI._vecUser[i]->dwUserID != RoomLogic()->loginResult.pUserInfoStruct.dwUserID &&
						_TableUI._vecUser[j]->dwUserID != RoomLogic()->loginResult.pUserInfoStruct.dwUserID)
					{
						//播放警报动画gjd6.15
						Sprite* pLight = dynamic_cast<Sprite*>(node->getChildByName(StringUtils::format("IPbaojing_%d", i)));
						pLight->setVisible(true);
						Sprite* pLight1 = dynamic_cast<Sprite*>(node->getChildByName(StringUtils::format("IPbaojing_%d", j)));
						pLight1->setVisible(true);
						auto AnimationNode = node->getChildByName(StringUtils::format("light%d", i));
						if (AnimationNode == nullptr)
						{
							auto touziA = Sprite::createWithSpriteFrameName(StringUtils::format("jsd_%d.png", 1));
							node->addChild(touziA);
							touziA->setName(StringUtils::format("light%d", i));
							touziA->setVisible(true);
							touziA->setGlobalZOrder(9);
							touziA->setPosition(pLight->getPosition());

							auto animationA = getAni("jsd_");
							animationA->setDelayPerUnit(0.3f);
							//animation->setDelayPerUnit(float);// 延迟时间（秒）每一帧播放的间隔
							//animationA->setLoops(25);
							//animationA->setRestoreOriginalFrame(true);
							auto animateA = Animate::create(animationA);
							touziA->runAction(CCRepeatForever::create(animateA));
						}
						else
						{
							AnimationNode->setVisible(true);
						}

						auto AnimationNode1 = node->getChildByName(StringUtils::format("light%d", j));
						if (AnimationNode1 == nullptr)
						{
							auto touziB = Sprite::createWithSpriteFrameName(StringUtils::format("jsd_%d.png", 1));
							node->addChild(touziB);
							touziB->setName(StringUtils::format("light%d", j));
							touziB->setVisible(true);
							touziB->setGlobalZOrder(9);
							touziB->setPosition(pLight1->getPosition());

							auto animationA = getAni("jsd_");
							animationA->setDelayPerUnit(0.3f);
							auto animateA = Animate::create(animationA);
							touziB->runAction(CCRepeatForever::create(animateA));
						}
						else
						{
							AnimationNode1->setVisible(true);
						}
					}
				}
			}
		}
	}

	//更新玩家的金币
	//_i64UserMoney 玩家金币
	//byDeskStation 玩家的视图座位号
	void GameTableUI::SetUserMoney(LLONG i64UserMoney, BYTE byDeskViewStation)
	{
		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)
		{
			return;
		}
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				_ActionMoney[byDeskViewStation] = i64UserMoney;
				_players[byDeskViewStation].setUserIntegral(i64UserMoney);
			}
			else
			{
				_players[byDeskViewStation].setUserMoney(i64UserMoney);
			}
			int pso = _logic->viewToLogicSeatNo(byDeskViewStation);
			_actionPoint[pso] = i64UserMoney;
		}
		else
		{
			//房卡场金币数不受结算影响
			_players[byDeskViewStation].setUserIntegral(i64UserMoney);
			//int pso = _logic->viewToLogicSeatNo(byDeskViewStation);
			//_ActionMoney[pso] = i64UserMoney;
		}
	}


	//显示不出、出牌、提示按钮
	//bDoNotOut 手否显示不出按钮 0 表示隐藏 1 表示显示
	//bCanOut 显示出牌按钮
	//bSurggest 显示提示按钮
	void GameTableUI::ShowRefferOutCardBtn(BYTE byDeskViewStation)
	{
		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)
		{
			return;
		}

		showActionBtn(byDeskViewStation == 0);
		if (byDeskViewStation == 0)
		{
			if (!m_isNewTurn)
			{
				AskSurggest();
			}
			clearPoolCard(0);
		}

	}

	void GameTableUI::setNewTurnOutCard(bool isNewTurn)
	{
		m_isNewTurn = isNewTurn;
	}

	//显示定时器
	void GameTableUI::ShowTime_PDK(int iTimeCount, BYTE byDeskViewStation)
	{
		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)
		{
			return;
		}

		//每个玩家倒计时改统一处理，超时自动选择托管
		/*for (BYTE vseat = 0; vseat < PLAY_COUNT; vseat++)
		{
		_players[vseat].showtime(vseat == byDeskViewStation, vseat == byDeskViewStation ? iTimeCount : 0);
		}*/
		showtime(iTimeCount, byDeskViewStation);
	}

	void GameTableUI::showtime(int ShowTime, BYTE ShowDeskView)
	{
		_ShowTime = ShowTime;
		_ShowDeskView = ShowDeskView;
		if (ShowTime > 0)
		{
			if (_TableUI.Clock)
			{
				_TableUI.Clock_Text->setString(StringUtils::toString(_ShowTime));
				_TableUI.Clock->schedule(CC_CALLBACK_1(GameTableUI::UpdateTimer, this), 1.0f, "UpdateTimer");
				_TableUI.Clock->setPosition(_ClockPos[_ShowDeskView]);
				_TableUI.Clock->setVisible(true);
			}
		}
		else
		{
			if (_TableUI.Clock)
			{
				_TableUI.Clock->setVisible(false);
				_TableUI.Clock->unschedule("UpdateTimer");
			}
		}
	}

	void GameTableUI::UpdateTimer(float dt)
	{
		_ShowTime -= 1;
		if (_ShowTime >= 1)
		{
			_TableUI.Clock_Text->setString(StringUtils::toString(_ShowTime));
		}
		else
		{
			if (_TableUI.Clock)
			{
				_TableUI.Clock->setVisible(false);
				_TableUI.Clock->unschedule("UpdateTimer");
			}
			//发送托管
			if (_ShowDeskView == 0)
			{
				if (_logic->getGameStatus() == S_C_GAME_BEGIN || _logic->getGameStatus() == GS_PLAY_GAME)
				{
					//_logic->sendTuoguan(true);
				}
			}
		}
	}

	//显示首发出牌用户以及应该先出的牌
	void GameTableUI::ShowFirstOutCard(BYTE byDeskView, BYTE FirstNeetOutCard)
	{
		if (_TableUI.Image_BG->getChildByName("FirstCard"))
		{
			_TableUI.Image_BG->getChildByName("FirstCard")->removeFromParent();
		}
		_TableUI.Poker_First = Poker::create(FirstNeetOutCard == 255 ? 0 : FirstNeetOutCard);
		_TableUI.Poker_First->ignoreContentAdaptWithSize(false);
		_TableUI.Poker_First->setContentSize(Size(72, 96));//byDeskView == 0 ? Size(144, 192) : Size(72, 96));
		Size size = _TableUI.Poker_First->getContentSize();
		Sprite* sp = Sprite::create("platform/Games/xianchu.png");
		sp->setPosition(Vec2(size.width / 2 + 7, size.height / 2 + 20));
		sp->setScale(0.6);
		_TableUI.Poker_First->addChild(sp);
		_TableUI.Poker_First->setName("FirstCard");
		_TableUI.Poker_First->setPosition(_playersPos[byDeskView]);
		_TableUI.Image_BG->addChild(_TableUI.Poker_First);
		/*_TableUI.HandList[byDeskView]->pushBackCustomItem(poker);
		_TableUI.HandList[byDeskView]->doLayout();*/
	}

	void GameTableUI::removeFirstOutCard()
	{
		if (_TableUI.Image_BG->getChildByName("FirstCard"))
		{
			_TableUI.Poker_First->removeFromParent();
			_TableUI.Poker_First = nullptr;
		}
		//setNewTurnOutCard(false);
	}

	//离开游戏
	void GameTableUI::ExitGame()
	{
		if (_logic->isConnected())
		{
			_logic->sendUserUp();
			//RoomLogic()->close();
			//GamePlatform::createPlatform();
		}
		else
		{
			_logic->stop();
			RoomLogic()->close();
			GamePlatform::createPlatform();
		}
	}

	void GameTableUI::dealLeaveDesk()
	{
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			_logic->stop();
			RoomLogic()->close();
			GamePlatform::createPlatform();
		}
		else
		{
			_logic->stop();
			RoomLogic()->close();
			GamePlatform::createPlatform();
		}
	}

	void GameTableUI::userUp(BYTE viewseat)
	{
		if (viewseat < 0 || viewseat >= PLAY_COUNT)
		{
			dealLeaveDesk();
		}

		if (viewseat == 0)
		{
			dealLeaveDesk();
		}
		else
		{
			UpdateUserInfo(viewseat, nullptr);
			ShowReadyImage(viewseat, false);
		}
	}


	//设置手牌
	void GameTableUI::SetUserCard(BYTE byDeskViewStation, BYTE * byCard, BYTE byCardCount)
	{
		// 		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)
		// 		{
		// 			return;
		// 		}

		if (byDeskViewStation == 0 || _logic->isSuperUser())
		{
			_TableUI.HandList[byDeskViewStation]->removeAllChildren();
			for (int i = 0; i < byCardCount; i++)
			{
				auto poker = Poker::create(byCard[i] == 255 ? 0 : byCard[i]);
				poker->ignoreContentAdaptWithSize(false);
				poker->setContentSize(byDeskViewStation == 0 ? Size(158, 211) : Size(79, 105.5));
				poker->setTag(POKER_UNSELECTED);
				auto pHandList = _TableUI.HandList[byDeskViewStation];
				pHandList->pushBackCustomItem(poker);
				pHandList->doLayout();
				if (byDeskViewStation != 0 && _logic->isSuperUser())
				{
					auto size = _TableUI.pokerCountImg[byDeskViewStation]->getContentSize();
					auto point = _TableUI.pokerCountImg[byDeskViewStation]->getPosition();
					if (byDeskViewStation == 1)
						pHandList->setPositionX(size.width);
					else if (byDeskViewStation == 2)
						pHandList->setPositionX(- size.width);
				}
			}

			adjustSeatCardList(byDeskViewStation);
		}
	}


	//设置出牌
	void GameTableUI::SetUserOutCard(BYTE byDeskViewStation, BYTE * byCard, BYTE byCardCount)
	{
		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)
		{
			return;
		}
		//显示用户出牌
		_TableUI.poolcards[byDeskViewStation]->removeAllChildren();
		for (int i = 0; i < byCardCount; i++)
		{
			if (byCard[i] != 255 && byCard[i] != 0)
			{
				auto poker = Poker::create(byCard[i] == 255 ? 0 : byCard[i]);
				poker->ignoreContentAdaptWithSize(false);
				poker->setContentSize(Size(79, 105.5));
				poker->setTag(POKER_UNSELECTED);
				_TableUI.poolcards[byDeskViewStation]->pushBackCustomItem(poker);
				_TableUI.poolcards[byDeskViewStation]->doLayout();
			}
		}
		adjustSeatCardList(byDeskViewStation);
	}

	//显示不出图片
	void GameTableUI::ShowDoNotOutCard(BYTE byDeskViewNo, bool bShowOrHide)
	{
		if (byDeskViewNo < 0 || byDeskViewNo >= PLAY_COUNT)
		{
			return;
		}

		_players[byDeskViewNo].showNotout(bShowOrHide);

		if (bShowOrHide)
		{
			clearPoolCard(byDeskViewNo);
		}

	}

	//等待同意状态界面
	void GameTableUI::GameStationWaitAgree()
	{
		_TableUI.btnReady->setVisible(true);
	}

	//游戏中界面状态
	void GameTableUI::GameStationPlaying(S_C_GameStation_Play * pPlaying)
	{
		_TableUI.Invaite->setVisible(false);
		_TableUI.btnReady->setVisible(false);
	}


	//点击开始，同意后的准备图片的显示
	void GameTableUI::ShowReadyImage(BYTE byDeskViewStation, bool bIsShowOrHide)
	{
		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)
		{
			return;
		}
		_players[byDeskViewStation].showReady(bIsShowOrHide);

		if (byDeskViewStation == 0)
		{
			if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				_TableUI.btnReady->setVisible(false);
			}
			else
			{
				_TableUI.btnReady->setVisible(!bIsShowOrHide);
			}
			_TableUI.Invaite->setPositionX(bIsShowOrHide ? _winSize.width / 2 : _winSize.width*0.375);
		}
	}
	void GameTableUI::ShowUserOutLine(BYTE byDeskViewStation, bool bIsShowOrHide)
	{
		_players[byDeskViewStation].showOutLine(bIsShowOrHide);
	}

	//请求出牌
	void GameTableUI::AskOutCard()
	{
		//获取选中的牌
		auto cards = getSelectedCardsValue();
		//玩家出牌
		C_S_OutCardStruct data;
		std::copy(cards.begin(), cards.end(), data.iCardList);
		data.iCardCount = cards.size();

		if (_logic->CanOutCard(data.iCardList, data.iCardCount))
		{
			//发送出牌消息
			_logic->SendGameData(C_S_OUT_CARD, &data, sizeof(data));
		}

	}

	//提示
	void GameTableUI::AskSurggest()
	{
		//获取能够出的牌和出牌数目
		BYTE byOutCardResult[HAND_CARD_COUNT];//能够出的牌
		int iOutCardCount = 0; //能够出牌的数目
		memset(byOutCardResult, 255, sizeof(byOutCardResult));

		_logic->GetAutoOutCard(byOutCardResult, iOutCardCount);

		resetSelectedCards();

		std::vector<BYTE> cards(byOutCardResult, byOutCardResult + iOutCardCount);
		setSelectedCards(cards);
	}
	//滑动提示
	void GameTableUI::HuaDongSurggest()
	{

		//获取选中的牌
		auto selectCards = getSelectedCardsValue();
		if (selectCards.size() < 2)
		{
			return;
		}
		BYTE byOutCardResult[HAND_CARD_COUNT];//能够出的牌
		int iOutCardCount = 0; //能够出牌的数目
		memset(byOutCardResult, 255, sizeof(byOutCardResult));
		memcpy(byOutCardResult, &selectCards[0], sizeof(selectCards));
		//_logic->GetAutoOutCard(selectCards, byOutCardResult, iOutCardCount,true);

		//resetSelectedCards();

		std::vector<BYTE> cards(byOutCardResult, byOutCardResult + iOutCardCount);
		setSelectedCards(cards);

	}

	//显示结算界面
	void GameTableUI::ShowSettlement(S_C_GameEndStruct* pGameEnd, bool bShowOrHide)
	{
		_TableUI.SettlementNode->setVisible(bShowOrHide);
		_TableUI.btnReady->setPositionX(_winSize.width / 2);
		_TableUI.CancelPlane->setVisible(false);
		if (!bShowOrHide)
		{
			return;
		}

		// 获胜玩家
		//SkeletonAnimation* action = nullptr;
		auto layerend = _TableUI.SettlementNode->getChildByName("Panel_back")->getChildByName("Image_back");
		if (pGameEnd->iTurePoint[_logic->getMySeatNo()] >= 0)
		{
			//action = SkeletonAnimation::createWithFile("RunCrazy/singleBoard/animation/win.json", "RunCrazy/singleBoard/animation/win.atlas");
			auto winImg = (ImageView*)_TableUI.SettlementNode->getChildByName("Panel_back")->getChildByName("Image_win");
			winImg->setVisible(true);
			auto loseImg = (ImageView*)_TableUI.SettlementNode->getChildByName("Panel_back")->getChildByName("Image_lose");
			loseImg->setVisible(false);
		}
		else
		{
			auto winImg = (ImageView*)_TableUI.SettlementNode->getChildByName("Panel_back")->getChildByName("Image_win");
			winImg->setVisible(false);
			auto loseImg = (ImageView*)_TableUI.SettlementNode->getChildByName("Panel_back")->getChildByName("Image_lose");
			loseImg->setVisible(true);
			//action = SkeletonAnimation::createWithFile("RunCrazy/singleBoard/animation/lose.json", "RunCrazy/singleBoard/animation/lose.atlas");
		}
		//action->addAnimation(0, "animation", true);
		//action->setName("SkeletonAnimation");
		//action->setPosition(Vec2(layerend->getContentSize().width / 2, layerend->getContentSize().height*0.6));
		//layerend->removeChildByName("SkeletonAnimation");
		//layerend->addChild(action);
		//大小头统计
		int BigCard = -1;
		int BigIndex = -1;
		int BigIndex1 = -1;
		int WinIndex = -1;
		int SmallIndex = -1;
		if (pGameEnd->iDaXiaoTou)
		{
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				if (pGameEnd->iUserCardCount[i] > BigCard)
				{
					BigCard = pGameEnd->iUserCardCount[i];
					BigIndex = i;
				}
				else if (pGameEnd->iUserCardCount[i] == BigCard)
				{
					//如果两个大头的情况下
					BigCard = pGameEnd->iUserCardCount[i];
					BigIndex1 = i;
				}
				if (pGameEnd->iTurePoint[i] > 0)
				{
					WinIndex = i;
				}
			}
		}
		//只有一个大头判断小头
		if (BigIndex != -1 && BigIndex1 == -1)
		{
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				if (BigIndex != i && WinIndex != i)
				{
					SmallIndex = i;
				}
			}
		}

		//赢牌玩家是否关牌其他人
		bool isWinFanBei = false;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (pGameEnd->iFanBeiFlag[i])
			{
				isWinFanBei = true;
				break;
			}
		}

		_TableUI.SettlementInfo->removeAllItems();
		//玩家得分
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			int pso = _logic->logicToViewSeatNo(i);
			auto User = _logic->getUserBySeatNo(i);
			if (!User)
			{
				continue;
			}
			_TableUI.SettlementInfo->pushBackDefaultItem();
			auto item = _TableUI.SettlementInfo->getItem(_TableUI.SettlementInfo->getItems().size() - 1);
			//玩家昵称
			auto username = (Text*)item->getChildByName("Panel_name")->getChildByName("Text_name");
			auto last = (Text*)item->getChildByName("Text_last");
			auto boom = (Text*)item->getChildByName("Text_boom");
			auto guan = (Text*)item->getChildByName("Text_guan");
			auto winscore = (Text*)item->getChildByName("Text_winscore");
			auto BigOrS = Sprite::create("platform/Games/da.png");//(Sprite*)item->getChildByName("sp_BigOrS");
			BigOrS->setPosition(Vec2(262.50, 39));
			BigOrS->setVisible(false);
			item->addChild(BigOrS);
			if (BigIndex != -1 && BigIndex == i)
			{
				if (BigOrS)
				{
					BigOrS->setTexture("platform/Games/da.png");
					BigOrS->setVisible(true);
				}
			}
			else if (SmallIndex != -1 && SmallIndex == i)
			{
				if (BigOrS)
				{
					BigOrS->setTexture("platform/Games/xiao.png");
					BigOrS->setVisible(true);
				}
			}
			if (WinIndex != -1 && WinIndex == i)
			{
				if (BigOrS)
				{
					//BigOrS->setTexture("platform/Games/xiao.png");
					BigOrS->setVisible(false);
				}
			}
			if (BigIndex1 != -1 && BigIndex1 == i)
			{
				if (BigOrS)
				{
					BigOrS->setTexture("platform/Games/da.png");
					BigOrS->setVisible(true);
				}
			}
			username->setString(GBKToUtf8(pGameEnd->szName[i]));
			//玩家剩余牌
			last->setString(StringUtils::format("%d", pGameEnd->iUserCardCount[i]));
			//玩家炸弹
			boom->setString(StringUtils::format("%d", pGameEnd->byBomb[i]));

			//玩家关牌
			guan->setString(pGameEnd->iFanBeiFlag[i] ? GBKToUtf8("是") : GBKToUtf8("否"));
			winscore->setString(StringUtils::format("%d", pGameEnd->iTurePoint[i]));
		}
		//自己被关牌
		if (pGameEnd->iFanBeiFlag[_logic->getMySeatNo()])
		{
			IsSpring();
		}
		//特殊情况（自己把别人关牌了，也要现实翻倍）
		else if (pGameEnd->iTurePoint[_logic->getMySeatNo()] > 0 && isWinFanBei)
		{
			IsSpring();
		}
		//auto GameEndDate = pGameEnd;

		//this->runAction(Sequence::create(DelayTime::create(1), CallFunc::create([this](){
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			int pso = _logic->logicToViewSeatNo(i);
			auto User = _logic->getUserBySeatNo(i);
			if (User)
			{
				if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
				{
					if (RoomLogic()->getRoomRule() & GRR_CONTEST || RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
					{
						_ActionMoney[pso] = User->i64Money;
						_players[pso].setUserIntegral(User->i64Money);
					}
					else
					{
						_players[pso].setUserMoney(User->i64Money);
					}
					_actionPoint[i] = User->i64Money;
				}
				else
				{
					_players[pso].setUserIntegral(pGameEnd->iAllTurePoint[i]);
					//_ActionMoney[i] = pGameEnd->iAllTurePoint[i];
				}
			}
		}
		//}), 0));

		//隐藏出牌打牌提示按钮
		showActionBtn(false);
	}


	//显示发牌动画
	void GameTableUI::PlaySendCardAnimation()
	{
		clearAllHandcard();
		_TableUI.HandList[0]->setContentSize(Size(1325, 192));
		_suite = CardSuite::create(0.75);
		_suite->setPosition(Vec2(_winSize.width / 2, _winSize.height / 2));
		this->addChild(_suite);
		_suite->shuffle(0.05f);
		Sequence* s = nullptr;
		for (BYTE handindex = 0; handindex < 16; handindex++)
		{
			for (BYTE seatindex = 0; seatindex < PLAY_COUNT; seatindex++)
			{
				_suite->restoreCardSize(0.75);

				Vec2 pos;
				if (seatindex == 0)
				{
					// 手牌框 初始大小1100*192
					pos = Vec2(_TableUI.HandList[seatindex]->getPosition().x - 550 + handindex * 64, _TableUI.HandList[seatindex]->getPosition().y);
				}
				else if (seatindex == 1)
				{
					pos = Vec2(_TableUI.HandList[seatindex]->getPosition().x + handindex * 28, _TableUI.HandList[seatindex]->getPosition().y);
				}
				else
				{
					pos = Vec2(_TableUI.HandList[seatindex]->getPosition().x - handindex * 28, _TableUI.HandList[seatindex]->getPosition().y);
				}
				auto tarpos = _TableUI.HandList[seatindex]->getParent()->convertToWorldSpace(pos);

				auto func = CallFunc::create([=](){
					_TableUI.HandList[seatindex]->pushBackDefaultItem();
					if (seatindex == 2)
					{
						adjustSeatCardList(seatindex);
					}
				});

				auto handaction = TargetedAction::create(_TableUI.HandList[seatindex], func);

				float fScale = (seatindex == 0) ? 0.7f : 0.3f;

				auto suiteaction = _suite->dealCard(0.05f, tarpos, 180, fScale);

				if (s == nullptr)
				{
					s = Sequence::create(suiteaction, handaction, nullptr);
				}
				else
				{
					s = Sequence::create(s, suiteaction, handaction, nullptr);
				}
			}
		}
		_suite->runActionPair(_TableUI.HandList[0]->getParent(), s);
		_suite->recycle(0.05f);
	}

	void GameTableUI::SendCardFinish()
	{
		_TableUI.HandList[0]->getParent()->stopAllActions();
		if (_suite)
			_suite->removeFromParent();
		_suite = nullptr;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_TableUI.HandList[i]->removeAllChildren();
		}
	}


	//显示托管标识
	void GameTableUI::ShowTuoGuanLogo(BYTE byDeskViewStation, bool bShowOrHide)
	{
		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)
		{
			return;
		}
		_players[byDeskViewStation].showAuto(bShowOrHide);
		if (byDeskViewStation == 0)
		{
			_TableUI.CancelPlane->setVisible(bShowOrHide);
		}

		if (_isVIPRoom) _TableUI.btnTuoGuan->setVisible(false); //创建房间不需要托管按钮

	}

	//显示报警标识
	void GameTableUI::ShowBaoJing(BYTE byDeskViewStation, bool bShowOrHide)
	{
		if (byDeskViewStation < 0 || byDeskViewStation >= PLAY_COUNT)  // 自己显示报警
		{
			return;
		}

		if (bShowOrHide)
		{
			//SkeletonAnimation* action = SkeletonAnimation::createWithFile("RunCrazy/player/baojin/baojin.json", "RunCrazy/player/baojin/baojin.atlas");
			//action->addAnimation(0, "animation", true);
			//action->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
			//action->setNormalizedPosition(Vec2::ANCHOR_MIDDLE);
			//_TableUI.HandList[byDeskViewStation]->getItem(0)->addChild(action);
			//_TableUI.baojingqiImg[byDeskViewStation]->stopAllActions();
			if (_TableUI.Sp_baojingqi[byDeskViewStation]->getChildByName("light1"))
			{
				_TableUI.Sp_baojingqi[byDeskViewStation]->getChildByName("light1")->removeFromParent();
			}
			_TableUI.Sp_baojingqi[byDeskViewStation]->setVisible(true);
			auto touziA = Sprite::createWithSpriteFrameName(StringUtils::format("ligt_%d.png", 1));
			_TableUI.Sp_baojingqi[byDeskViewStation]->addChild(touziA);
			touziA->setPosition(Vec2(_TableUI.Sp_baojingqi[byDeskViewStation]->getContentSize().width / 2, _TableUI.Sp_baojingqi[byDeskViewStation]->getContentSize().height / 2));
			touziA->setName(StringUtils::format("light%d", 1));
			touziA->setVisible(true);

			auto animationA = getAni("ligt_");
			animationA->setDelayPerUnit(0.3f);
			auto animateA = Animate::create(animationA);
			touziA->runAction(CCRepeatForever::create(animateA));
			/*Blink* bl = Blink::create(2.0, 2);
			RepeatForever* reAction = RepeatForever::create(bl);
			_TableUI.baojingqiImg[byDeskViewStation]->runAction(reAction);*/
		}
	}

	Animation* GameTableUI::getAni(std::string name)
	{
		auto cache = AnimationCache::getInstance();
		Animation* animation = nullptr;
		Vector<SpriteFrame *> spFrame;
		animation = cache->getAnimation(name);
		if (animation == nullptr)
		{
			for (auto i = 1; i < 50; i++)
			{
				auto fullName = StringUtils::format("%s%d.png", name.c_str(), i);
				auto spf = SpriteFrameCache::getInstance()->getSpriteFrameByName(fullName.c_str());
				if (spf != nullptr)
				{
					spFrame.pushBack(spf);
				}
				else
				{
					break;
				}
			}
			animation = Animation::createWithSpriteFrames(spFrame);
			animation->setDelayPerUnit(0.05f);
			animation->setLoops(1);
			cache->addAnimation(animation, name);
		}
		else
		{
			auto animationCopy = animation->clone();
			auto copyName = StringUtils::format("%sCopy", name.c_str());
			cache->addAnimation(animationCopy, copyName);
			return animationCopy;
		}
		return animation;
	}

	void GameTableUI::OnTouch(Ref *pSender, Widget::TouchEventType type)
	{
		//获取点击目标
		Button * target = static_cast<Button*>(pSender);

		//获取点击目标的名称
		std::string strField = target->getName();
		//按下事件
		if (type == Widget::TouchEventType::ENDED)
		{
			if (strField == "Button_ready")
			{
				//ShowBaoJing(1, true);
				//playEffect(CARD_TYPE);
				_logic->sendAgreeGame();
				target->setVisible(false);
				hideAllAlert();
				SoundManager::PlayOtherSound(SOUND_MOUSE_DOWN);
			}
			else if (strField == "Button_outcard")//出牌
			{
				AskOutCard();
				SoundManager::PlayOtherSound(SOUND_MOUSE_DOWN);
			}
			else if (strField == "Button_tishi")//提示
			{
				AskSurggest();
				SoundManager::PlayOtherSound(SOUND_MOUSE_DOWN);
			}
			else if (strField == "Button_left")//离开
			{
				if (!_isVIPRoom)
				{
					ExitGame();
				}
				else
				{
					auto shareLayer = GameShareLayer::create();
					shareLayer->setName("shareLayer");
					shareLayer->SetInviteInfo("跑得快", HNPlatformConfig()->getVipRoomNum(), false);
					shareLayer->show();
				}
			}
			else if (strField == "Button_invite")
			{
				VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
				if (roomController)
				{
					roomController->invitationBtnCallBack(pSender);
				}
			}
			else if (strField == "Button_continue")//继续
			{
				_TableUI.SettlementNode->setVisible(false);
				if (_GameEndLayer)
				{
					_GameEndLayer->setVisible(true);
					return;
				}
				_logic->sendAgreeGame();
				ResetUI();
				ShowSettlement(NULL, false);
				SoundManager::PlayOtherSound(SOUND_MOUSE_DOWN);
			}

			else if (strField == "Button_auto")
			{
				_logic->sendTuoguan(true);
			}
			else if (strField == "Button_cancel")
			{
				_logic->sendTuoguan(false);
			}
			else if (strField == "Button_jiesan")
			{
				VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
				if (roomController)
				{
					roomController->dismissBtnCallBack(pSender);
				}
			}

			else if (strField == "Button_return")
			{
				if (_logic->getGameStatus() == S_C_GAME_BEGIN || _logic->getGameStatus() == GS_PLAY_GAME)
				{
					GamePromptLayer::create()->showPrompt(GBKToUtf8("游戏中不允许退出！"));
				}
				else
				{
					VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
					if (roomController)
					{
						roomController->returnBtnCallBack(pSender);
					}
					else
					{
						ExitGame();
					}
				}
			}
			else if (strField == "Button_set")
			{
				auto setLayer = GameSetLayer::create();
				setLayer->setName("setLayer");
				setLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 50);
				VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
				if (roomController)
				{
					//设置游戏状态按钮
					setLayer->setGameStateView();
				}
				setLayer->onExitCallBack = [=]() {
					if (roomController)
					{
						roomController->returnBtnCallBack(pSender);
					}
					else
					{
						_logic->sendUserUp();
					}
					removeSetLayer();
				};

				setLayer->onDisCallBack = [=]() {

					if (roomController) roomController->dismissBtnCallBack(pSender);
					removeSetLayer();
				};
			}
			else if (strField == "Button_help")
			{
				auto rule_Node = CSLoader::createNode("RunCrazy/gameCsb/helpNode.csb");
				rule_Node->setPosition(Vec2(640, 360));
				addChild(rule_Node, 2018);
				auto closeBt = (Button*)rule_Node->getChildByName("Panel_1")->getChildByName("Button_close");
				closeBt->addClickEventListener([=](Ref*){
					rule_Node->removeFromParent();
				});
			}
			else if (strField == "Button_CardCount")
			{
				if (_TableUI.CardCountImg->isVisible())
				{
					_TableUI.CardCountImg->setVisible(false);
				}
				else
				{
					_TableUI.CardCountImg->setVisible(true);
				}
			}
		}
	}

	void GameTableUI::removeSetLayer()
	{
		auto setLayout = this->getChildByName("setLayer");
		if (setLayout)
		{
			setLayout->removeFromParent();
		}
	}

	void GameTableUI::hideAllAlert()
	{
		//     for (BYTE vseat = 0; vseat < PLAY_COUNT; vseat++)
		//     {
		////_players[vseat].showtime(false,0);
		//showtime(0, vseat);
		//     }
		showtime(0, 0);
	}

	void GameTableUI::clearAllPoolCard()
	{
		for (auto p : _TableUI.poolcards)
			p->removeAllChildren();
	}

	void GameTableUI::clearPoolCard(BYTE viewseat)
	{
		if (viewseat >= 0 && viewseat < PLAY_COUNT)
		{
			_TableUI.poolcards[viewseat]->removeAllChildren();
		}
	}

	void GameTableUI::clearAllHandcard()
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_TableUI.HandList[i]->removeAllChildren();
		}
	}
	void GameTableUI::resetTouchTag()
	{
		for (auto it : _TableUI.HandList[0]->getChildren())
		{
			auto tag = it->getTag();
			it->setTag(SetPokerTouching(tag));
		}
	}

	Node* GameTableUI::getCardByPos(Vec2 localpos)
	{
		int MaxCardIndex = _TableUI.HandList[0]->getChildrenCount();
		int Index = 0;
		for (auto it : _TableUI.HandList[0]->getChildren())
		{
			Index++;
			auto pos = it->getPosition();
			auto size = it->getContentSize();
			if (Index != MaxCardIndex)
			{
				if (localpos.x <= pos.x && localpos.x > (pos.x - size.width / 2))
				{
					return it;
				}
			}
			else
			{
				//最后一张点击判断（最后一张的触碰区域跟前面的不大一样）
				if (localpos.x <= pos.x || localpos.x > pos.x)
				{
					return it;
				}
			}
		}
		return nullptr;
	}

	Poker* GameTableUI::getCardByValue(BYTE cardvalue)
	{
		for (auto it : _TableUI.HandList[0]->getChildren())
		{
			auto poker = dynamic_cast<Poker*>(it);
			if (poker && poker->getValue() == cardvalue)
			{
				return poker;
			}
		}

		return nullptr;
	}

	void GameTableUI::setSelected(BYTE cardvalue, bool selected)
	{
		for (auto it : _TableUI.HandList[0]->getChildren())
		{
			auto poker = dynamic_cast<Poker*>(it);
			if (poker && poker->getValue() == cardvalue)
			{
				int tag = poker->getTag();
				if (IsPokerSelected(tag) ^ selected)
				{
					auto offset = IsPokerSelected(tag) ? Vec2(0, -30) : Vec2(0, 30);
					poker->setTag(SetPokerReverseSel(tag));
					poker->runAction(CallFunc::create([poker, offset]()
					{
						auto pos = poker->getPosition();
						poker->setPosition(pos + offset);
					}));
				}
				break;
			}
		}
	}

	void GameTableUI::setSelectedCards(std::vector<BYTE> cardvalues)
	{
		auto cards = _TableUI.HandList[0]->getChildren();
		for (auto valit = cardvalues.begin(); valit != cardvalues.end(); valit++)
		{
			setSelected(*valit, true);
		}
	}

	void GameTableUI::resetSelectedCards()
	{
		auto cards = _TableUI.HandList[0]->getChildren();
		for (auto itr = cards.begin(); itr != cards.end(); itr++)
		{
			auto poker = (Poker*)(*itr);
			setSelected(poker->getValue(), false);
		}
	}

	std::vector<Poker*> GameTableUI::getSelectedCards()
	{
		auto cards = _TableUI.HandList[0]->getChildren();
		std::vector<Poker*> selectedcards;
		for (auto itr = cards.begin(); itr != cards.end(); itr++)
		{
			auto card = (Poker*)(*itr);
			if (IsPokerSelected(card->getTag()))
			{
				selectedcards.push_back(card);
			}
		}
		return selectedcards;
	}

	std::vector<BYTE> GameTableUI::getSelectedCardsValue()
	{
		auto cards = getSelectedCards();
		std::vector<BYTE> cardvalues;
		for (auto itr = cards.begin(); itr != cards.end(); itr++)
		{
			cardvalues.push_back((*itr)->getValue());
		}
		return cardvalues;
	}

	//出牌阶段
	void GameTableUI::showActionBtn(bool visible)
	{
		_TableUI.btnOurCard->setVisible(visible);
		_TableUI.btnSurggest->setVisible(visible);
	}

	// 托管按钮
	void GameTableUI::showTuoguanBtn(bool visible)
	{
		_TableUI.btnTuoGuan->setVisible(visible);
		if (_isVIPRoom) _TableUI.btnTuoGuan->setVisible(false); //创建房间不需要托管按钮
		//_TableUI.btnCardCount->setVisible(visible);
		//_TableUI.CardCountImg->setVisible(visible);
		if (!visible)
		{
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				_TableUI.baojingqiImg[i]->setVisible(false);
				_TableUI.baojingqiImg[i]->stopAllActions();
				_TableUI.Sp_baojingqi[i]->setVisible(false);
				_TableUI.pokerCountImg[i]->setVisible(false);
			}
		}
	}

	// 合成播放三张音效
	void GameTableUI::playEffect(BYTE value)
	{
		SkeletonAnimation* action = nullptr;
		bool isAirplane = false;
		bool isCSBAnimation = false;
		std::string CSBName;
		bool isFuji = false;

		switch (value)
		{

		case RunCrazy::UG_VARIATION_STRAIGHT:
		case RunCrazy::UG_STRAIGHT:
		case RunCrazy::UG_STRAIGHT_FLUSH:  // 顺子
			action = SkeletonAnimation::createWithFile("RunCrazy/skeleton/Straight/default.json", "RunCrazy/skeleton/Straight/default.atlas");
			break;
		case RunCrazy::UG_THREE:
			break;
		case RunCrazy::UG_THREE_TWO:   // 三代二
		case RunCrazy::UG_THREE_DOUBLE:
			//action = SkeletonAnimation::createWithFile("RunCrazy/skeleton/Straight/default.json", "RunCrazy/skeleton/Straight/default.atlas");
			isCSBAnimation = true;
			CSBName = "platform/Games/res/sandaier.csb";
			break;
		case RunCrazy::UG_VARIATION_DOUBLE_SEQUENCE: //双顺
		case RunCrazy::UG_DOUBLE_SEQUENCE:
			action = SkeletonAnimation::createWithFile("RunCrazy/skeleton/DoubleStraight/default.json", "RunCrazy/skeleton/DoubleStraight/default.atlas");
			break;
		case RunCrazy::UG_FOUR_THREE:
			isCSBAnimation = true;
			CSBName = "platform/Games/res/sidaisan.csb";
			break;
		case RunCrazy::UG_FOUR_THREE_DOUBLE:
			isCSBAnimation = true;
			CSBName = "platform/Games/res/sidaisandui.csb";
			break;
		case RunCrazy::UG_VARIATION_THREE_SEQUENCE: //飞机
		case RunCrazy::UG_THREE_SEQUENCE:
		case RunCrazy::UG_VARIATION_THREE_TWO_SEQUENCE:
		case RunCrazy::UG_THREE_TWO_SEQUENCE:
		case RunCrazy::UG_VARIATION_THREE_DOUBLE_SEQUENCE:
		case RunCrazy::UG_THREE_DOUBLE_SEQUENCE:
		case RunCrazy::UG_VARIATION_THREE_SEQUENCE_DOUBLE_SEQUENCE:
		case RunCrazy::UG_THREE_SEQUENCE_DOUBLE_SEQUENCE:
			//设置飞机动画
			isAirplane = true;
			/*action = SkeletonAnimation::createWithFile("RunCrazy/skeleton/Plane/default.json", "RunCrazy/skeleton/Plane/default.atlas");*/
			break;
		case RunCrazy::UG_3ABOMB:
		case RunCrazy::UG_BOMB: // 炸弹
			action = SkeletonAnimation::createWithFile("RunCrazy/skeleton/Bomb/default.json", "RunCrazy/skeleton/Bomb/default.atlas");
			break;
		case CARD_TYPE:
			isFuji = true;
			CSBName = "platform/Games/res/fuji.csb";
			break;
		default:
			break;
		}

		if (action)
		{
			action->addAnimation(0, "animation", false);
			action->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
			action->setNormalizedPosition(Vec2::ANCHOR_MIDDLE);
			action->runAction(Sequence::create(DelayTime::create(2.0f), RemoveSelf::create(), 0));
			addChild(action);
		}

		if (isAirplane)
		{
			//CSB场景读取
			auto node = CSLoader::createNode("platform/Games/res/Airplane.csb");
			node->setPosition(Vec2(_winSize.width * 0.7f, _winSize.height * 0.5f));
			node->setScale(0.8);

			//CSB内置动画播放
			auto ation = CSLoader::createTimeline("platform/Games/res/Airplane.csb");
			node->runAction(ation);
			ation->gotoFrameAndPlay(0, false);
			ation->setTimeSpeed(2.0);
			node->runAction(Sequence::create(DelayTime::create(1.5f), RemoveSelf::create(), 0));
			addChild(node);
		}

		if (isFuji)
		{
			//CSB场景读取
			auto node = CSLoader::createNode(CSBName);
			node->setPosition(Vec2(_winSize.width * 0.5f, _winSize.height * 0.6f));
			node->setScale(2.0);

			//CSB内置动画播放
			auto ation = CSLoader::createTimeline(CSBName);
			node->runAction(ation);
			ation->gotoFrameAndPlay(0, false);
			//ation->setTimeSpeed(2.0);
			node->runAction(Sequence::create(DelayTime::create(3.0f), RemoveSelf::create(), 0));
			addChild(node);
			//伏击播放音效
			SoundManager::PlayOutCardSound(CARD_TYPE, 0, true);
		}

		if (isCSBAnimation)
		{
			//CSB场景读取
			auto node = CSLoader::createNode(CSBName);
			node->setPosition(Vec2(_winSize.width * 0.5f, _winSize.height * 0.6f));
			node->setScale(1.5);

			//CSB内置动画播放
			auto ation = CSLoader::createTimeline(CSBName);
			node->runAction(ation);
			ation->gotoFrameAndPlay(0, false);
			ation->setTimeSpeed(2.0);
			node->runAction(Sequence::create(DelayTime::create(1.5f), RemoveSelf::create(), 0));
			addChild(node);
		}

	}

	// 游戏开始动画特效
	void GameTableUI::playGameStartAnimi()
	{
		_TableUI.Invaite->setVisible(false);
		// 音效
		SoundManager::playGameStart();
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_players[i].showReady(false);
		}
		_TableUI.wait->setVisible(false);
	}

	void GameTableUI::GameOverInfo(S_C_SettlementList * PDate)
	{

		_GameEndLayer = CalculateBoardAll::create();
		_GameEndLayer->showAndUpdateBoard(this, PDate);
		//_GameEndLayer->setVisible(!_TableUI.SettlementNode->isVisible());
		_GameEndLayer->setVisible(false);
	}

	//显示文本聊天
	void GameTableUI::onChatTextMsg(int userID, std::string msg)
	{
		auto userInfo = UserInfoModule()->findUser(userID);
		if (!userInfo) return;
		auto viewSeatNo = _logic->logicToViewSeatNo(userInfo->bDeskStation);

		Vec2 bubblePos = _players[viewSeatNo].getChatPos();
		bool isFilpped = false;

		if (viewSeatNo >= 0 && viewSeatNo <= 1)
		{
			bubblePos.x += 40;

		}
		else
		{
			bubblePos.x -= 20;

			isFilpped = true;
		}
		_TableUI.chatLayer->onHandleTextMessage(this, bubblePos, msg, userInfo->nickName, userInfo->bBoy, isFilpped, viewSeatNo);
	}

	//显示语音聊天
	void GameTableUI::onChatVoiceMsg(int userID, int voiceID, int voiceTime)
	{
		auto userInfo = UserInfoModule()->findUser(userID);
		if (userInfo == nullptr) return;
		auto viewSeatNo = _logic->logicToViewSeatNo(userInfo->bDeskStation);
		Vec2 bubblePos = _players[viewSeatNo].getChatPos();
		bool isFilpped = false;

		if (viewSeatNo >= 0 && viewSeatNo <= 1)
		{
			bubblePos.x += 40;
		}
		else
		{
			bubblePos.x -= 20;
			isFilpped = true;
		}
		_TableUI.chatLayer->onHandleVocieMessage(this, bubblePos, voiceID, userInfo->nickName, isFilpped, viewSeatNo, voiceTime);
	}

	//显示动作特效
	void GameTableUI::onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index)
	{
		auto pLayer = dynamic_cast<GameUserPopupInfoLayer*>(getChildByName(GAME_POPUPINFO_LAYER_NAME));
		if (pLayer == nullptr)
		{
			pLayer = GameUserPopupInfoLayer::create();
			pLayer->setMaxPlayerCount(PLAY_COUNT);
		}
		auto viewSSeatNo = _logic->logicToViewSeatNo(sendSeatNo);
		auto viewTSeatNo = _logic->logicToViewSeatNo(TargetSeatNo);
		auto herd = _TouchPlayer[viewSSeatNo];
		auto herd1 = _TouchPlayer[viewTSeatNo];
		pLayer->doAction(sendSeatNo, TargetSeatNo, index, herd, herd1);
	}

	//聊天界面
	void GameTableUI::gameChatLayer()
	{
		_TableUI.chatLayer = GameChatLayer::create();
		addChild(_TableUI.chatLayer, 100);
		_TableUI.chatLayer->setPosition(_winSize / 2);
		_TableUI.chatLayer->onSendTextCallBack = [=](const std::string msg) {
			if (!msg.empty())
			{
				_logic->sendChatMsg(msg);
			}
		};
	}

	void GameTableUI::setDeskInfo()
	{
		auto user = UserInfoModule()->findUser(HNPlatformConfig()->getMasterID());
		if (user)
		{
			BYTE viewseat = _logic->logicToViewSeatNo(user->bDeskStation);
			_players[viewseat].showHost(user->dwUserID == HNPlatformConfig()->getMasterID());
		}
		auto roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
		if (!roomController->isVipRoomGameStarted()) // 游戏已经开始过了 
		{
			_TableUI.Invaite->setVisible(true);
		}
		else
		{
			_TableUI.btnReady->setPositionX(_winSize.width / 2);
		}
	}

	void GameTableUI::setDeskInfo(UINT GameID)
	{
		string str = "";
		if (GameID == NEW_OUTGAME_ID)
		{
			str = GBKToUtf8("上游跑得快");
			//隐藏底分
			_TableUI.Image_DiFen->setVisible(false);
			//隐藏倍数
			_players[0].setUserMultiplebShow(false);
		}
		else if (GameID == GAME_NAME_ID)
		{
			str = GBKToUtf8("跑得快");
			_TableUI.Image_DiFen->setVisible(true);
			_players[0].setUserMultiplebShow(true);
		}
		else
		{
			str = GBKToUtf8("算牌跑得快");
			_TableUI.Image_DiFen->setVisible(true);
			_players[0].setUserMultiplebShow(true);
		}
		_Text_GameName->setString(str);
	}

	void GameTableUI::setDeskStr(string str)
	{
		if (!str.empty())
		{
			_FNTtext_MatchGame->setString(GBKToUtf8(str));
		}
	}

	void GameTableUI::setDeskConfig(int index)
	{
		VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
		if (roomController)
		{
			BYTE config = roomController->getDeskConfigByIndex(index);
			log("sssssssssss%d=====%d", index, config);
		}
	}

	void GameTableUI::setCardCountResult(std::vector<std::vector<BYTE>> card)
	{
		//记牌器发牌的时候进行牌数据记录。。。（两个人模式下这种计算方式等于作弊。。。）
		Text*	cardCountText[13];//记牌器上的牌数计算
		char countName[64] = { 0 };
		for (int i = 0; i < 13; i++)
		{
			sprintf(countName, "Text_%d", i);
			cardCountText[i] = (Text*)_TableUI.CardCountImg->getChildByName(countName);
		}
		BYTE					countResult[13];//存储剩余牌数数据数组
		memset(countResult, 0, sizeof(countResult));
		for (int i = 0; i < card.size(); i++)
		{
			for (int j = 0; j < card[i].size(); j++)
			{
				switch (card[i].at(j))
				{
				case 0x01:
				case 0x11:
				case 0x21:
				case 0x31:
				{
					countResult[0]++;
					break;
				}
				case 0x02:
				case 0x12:
				case 0x22:
				case 0x32:
				{
					countResult[1]++;
					break;
				}
				case 0x03:
				case 0x13:
				case 0x23:
				case 0x33:
				{
					countResult[2]++;
					break;
				}
				case 0x04:
				case 0x14:
				case 0x24:
				case 0x34:
				{
					countResult[3]++;
					break;
				}
				case 0x05:
				case 0x15:
				case 0x25:
				case 0x35:
				{
					countResult[4]++;
					break;
				}
				case 0x06:
				case 0x16:
				case 0x26:
				case 0x36:
				{
					countResult[5]++;
					break;
				}
				case 0x07:
				case 0x17:
				case 0x27:
				case 0x37:
				{
					countResult[6]++;
					break;
				}
				case 0x08:
				case 0x18:
				case 0x28:
				case 0x38:
				{
					countResult[7]++;
					break;
				}
				case 0x09:
				case 0x19:
				case 0x29:
				case 0x39:
				{
					countResult[8]++;
					break;
				}
				case 0x0A:
				case 0x1A:
				case 0x2A:
				case 0x3A:
				{
					countResult[9]++;
					break;
				}
				case 0x0B:
				case 0x1B:
				case 0x2B:
				case 0x3B:
				{
					countResult[10]++;
					break;
				}
				case 0x0C:
				case 0x1C:
				case 0x2C:
				case 0x3C:
				{
					countResult[11]++;
					break;
				}
				case 0x0D:
				case 0x1D:
				case 0x2D:
				case 0x3D:
				{
					countResult[12]++;
					break;
				}
				default:
					break;
				}
			}
		}
		for (int i = 0; i < 13; i++)
		{
			cardCountText[i]->setString(StringUtils::format("%d", countResult[i]));
		}
	}

	void GameTableUI::ShowTimeLeaveGame(int iTimeCount)
	{
		_TableUI.LeftText->setString(StringUtils::format("%d", iTimeCount));
		_nowTime = iTimeCount;
		schedule(schedule_selector(GameTableUI::UpdateLeaveTimer), 1.0f);
	}

	void GameTableUI::UpdateLeaveTimer(float dt)
	{
		_nowTime--;
		if (_nowTime > 0 && _nowTime <= 5)
		{
			HNAudioEngine::getInstance()->playEffect("RunCrazy/audio/music/s_countdown.mp3");
		}
		_TableUI.LeftText->setString(StringUtils::format("%d", _nowTime));
		if (_nowTime <= 0)
		{
			_nowTime = 0;
			unschedule(schedule_selector(GameTableUI::UpdateLeaveTimer));
		}

	}

	void GameTableUI::setOtherUserPokerCount(BYTE SeatNo, BYTE byCardCount)
	{
		_TableUI.pokerCountImg[SeatNo]->setVisible(true);
		_TableUI.pokerCountText[SeatNo]->setString(StringUtils::format("%d", byCardCount));
// 		if (_logic->isSuperUser()) 
// 		{
// 			float y = _TableUI.HandList[SeatNo]->getPositionY();
// 			_TableUI.pokerCountImg[SeatNo]->setPositionY(387)
// 		}
	}

	void GameTableUI::setGamePlayRules(BYTE wanfa, BYTE difen, BYTE beishu)
	{
		setRules(wanfa);
		setDifen(difen);
		int Beishu = (int)beishu;
		_players[0].setUserMultiple(Beishu);
		_players[0].setUserPoints(difen);
	}

	//void GameTableUI::setGame3AMax(BYTE ithreeaaa)
	//{

	//}

	void GameTableUI::setGameMultiple(int Multiple)
	{
		_players[0].setUserMultiple(Multiple);
	}

	void GameTableUI::IsSpring()
	{
		int Multiple = _players[0].getUserMultiple();
		_players[0].setUserMultiple(Multiple * 2);
	}

	void GameTableUI::setRules(BYTE wanfa)
	{
		VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
		if (roomController)
		{
			string str = "";
			if (wanfa == 1)
			{
				str = "首局黑桃3先出";
			}
			else
			{
				str = "每局黑桃3先出";
			}
			roomController->setDifenString(GBKToUtf8(str));
		}
	}

	void GameTableUI::setDifen(BYTE difen)
	{
		//设置游戏背景图上的底分显示
		string filename = StringUtils::format("platform/Games/%ddifen.png", difen);
		if (FileUtils::getInstance()->isFileExist(filename))
		{
			_TableUI.Image_DiFen->loadTexture(filename);
		}
	}

	void GameTableUI::CheckActivity()
	{
		MSG_GR_I_GetDeskUserInfo DataInfo;
		DataInfo.iUserID = PlatformLogic()->loginResult.dwUserID;
		RoomLogic()->sendData(MDM_GM_GAME_FRAME, ASS_GM_GET_GAMENUM, &DataInfo, sizeof(DataInfo));
	}

	void GameTableUI::CheckReceive()
	{
		MSG_GR_Get_RandNum DataInfo;
		DataInfo.iUserID = PlatformLogic()->loginResult.dwUserID;
		RoomLogic()->sendData(MDM_GM_GAME_FRAME, ASS_GM_GET_RANDNUM, &DataInfo, sizeof(DataInfo));
	}

	void GameTableUI::setActivityNum(int current, int toal, int CanTake)
	{
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
			if (roomController)
			{
				roomController->setOpenActivity(true);
				roomController->setCurrentTask(current, toal);
				if (CanTake > 0)
				{
					roomController->setIsStandard(true);
					roomController->setCanTakeNum(CanTake);
				}
				else
				{
					roomController->setIsStandard(false);
				}
			}
		}
	}

	void GameTableUI::setReceiveState(int ReceiveType, int AwardNum)
	{
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			VipRoomController* roomController = dynamic_cast<VipRoomController*>(this->getChildByName("VipRoomController"));
			if (roomController)
			{
				roomController->setAnimationForTake(ReceiveType, AwardNum);
			}
		}
	}

	void GameTableUI::UpDateUserLocation(int index)
	{
		//屏蔽地理位置请求
		if (true)
		{
			return;
		}
		if (_vecUser[index] == nullptr)
		{
			UpDateUserLocation(index + 1);
			return;
		}
		if (index > 3)
		{
			return;
		}
		//根据用户IP获取经纬度
		//*****************************************//
		float X = _vecUser[index]->fLat;
		float Y = _vecUser[index]->fLnt;
		if (X != 0 || Y != 0)
		{
			if (!_vecUserGeographic[index].empty())
			{
				UpDateUserLocation(index + 1);
				return;
			}
		}
		GameLocation::getInstance()->onGetLocationIP = [=](bool success, float latitude, float longtitude, const std::string& addr) {
			if (success)
			{
				float y = latitude;
				float x = longtitude;
				_vecUser[index]->fLat = longtitude;
				_vecUser[index]->fLnt = latitude;
				//straddName = addr;
				GameLocation::getInstance()->onGetAddrByLocation = [=](bool success, string& addr, string& province, string& city){
					if (success)
					{
						_vecUserGeographic[index] = addr;
					}
					UpDateUserLocation(index + 1);
				};
				GameLocation::getInstance()->getAddrByLocation(x, y);
			}
		};
		string strIP = Tools::parseIPAdddress(_vecUser[index]->dwUserIP);
		if (X == 0 || Y == 0)
		{
			GameLocation::getInstance()->getLocation(strIP);
		}
		else
		{
			if (_vecUserGeographic[index].empty())
			{
				GameLocation::getInstance()->getAddrByLocation(X, Y);
			}
		}
		//*****************************************//
	}

	void GameTableUI::afterScoreByDeskStation(int Station, int money)
	{
		if (Station >= PLAY_COUNT)
		{
			return;
		}
		if (!(RoomLogic()->getRoomRule() & GRR_GAME_BUY))
		{
			_actionPoint[Station] += money;
			//更新自己的金币数目
			int Seatno = _logic->logicToViewSeatNo(Station);
			_players[Seatno].setUserMoney(_actionPoint[Station]);
		}
		else
		{
			_ActionMoney[Station] += money;
		}
	}
}
