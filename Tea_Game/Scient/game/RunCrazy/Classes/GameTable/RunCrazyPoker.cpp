/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#include "RunCrazyPoker.h"

namespace RunCrazy
{

	Poker::Poker()
	{
		_ignoreAnchorPointForPosition = false;
	}

	Poker::~Poker()
	{
	}

	Poker* Poker::create(BYTE cardValue)
	{
		Poker* card = new Poker();
		if (card && card->initWithCardValue(cardValue))
		{
			card->autorelease();
			return card;
		}
		CC_SAFE_DELETE(card);
		return nullptr;
	}

	bool Poker::initWithCardValue(BYTE cardValue)
	{
        if (!ImageView::init())
		{
			return false;
		}

        // out of range
        if (!(
            (cardValue >= 0x00 && cardValue <= 0x0D) ||
            (cardValue >= 0x11 && cardValue <= 0x1D) ||
            (cardValue >= 0x21 && cardValue <= 0x2D) ||
            (cardValue >= 0x31 && cardValue <= 0x3D) ||
            (cardValue >= 0x4E && cardValue <= 0x4F)))
        {
            return false;
        }



        return setValue(cardValue);
	}

    bool Poker::setValue(BYTE cardValue)
	{

		/*
		2     3     4     5     6     7     8     9    10     J     Q     K     A
		0x01, 0x02 ,0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D		//���� 2 - A
		0x11, 0x12 ,0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,		//÷�� 2 - A
		0x21, 0x22 ,0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,		//���� 2 - A
		0x31, 0x32 ,0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D,		//���� 2 - A
		0x4E, 0x4F
		С�� С��

		*/
		
        std::string filename = getCardTextureFileName(cardValue);
        auto sf = SpriteFrameCache::getInstance()->getSpriteFrameByName(filename);
        if (!sf)
        {
            return false;
        }

		_Value = cardValue;
        loadTexture(filename, Widget::TextureResType::PLIST);
        return true;
	}

	BYTE Poker::getValue()
	{
		return _Value;
	}

	string Poker::getCardTextureFileName(BYTE cardValue)
	{
		char filename[64];
		sprintf(filename, "Games/RunCrazy/card/Poker_0x%02X.png", cardValue);

		return filename;
	}
}