/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __RunCrazyTableUIPC_h__
#define __RunCrazyTableUIPC_h__

#include "RunCrazyTableUICallback.h"
#include "RunCrazyTableLogic.h"
#include "RunCrazyCalculateBoardAll.h"
#include "RunCrazyCardCardSuite.h"
#include "RunCrazyPlayer.h"
#include "HNLobbyExport.h"
#include "HNNetExport.h"
#include "cocos2d.h"

namespace RunCrazy
{
	class TableLogic;
	class GameTableUI : public HNGameUIBase, public TableUICallback
	{
	public:
		/*
		 * 功能：创建桌子
		 * 参数：deskId-桌子编号，autoCreate-是否自动坐下
		 */
		static HNGameUIBase* create(BYTE deskId, bool autoCreate);

		/*
		 * 功能：初始化
		 * 参数：deskId-桌子编号，autoCreate-是否自动坐下
		 */
		bool init(BYTE deskId, bool autoCreate);

		// 析构函数
		GameTableUI();

		// 析构函数
		virtual ~GameTableUI();

		// 加载界面
		 void AddloadUI() ;

	protected:
		// 游戏逻辑
		TableLogic*	_logic;
		bool _isVIPRoom=false;

	private:
		struct 
		{
			//准备按钮
			Button * btnReady;
			Button * Invaite;

			ImageView* Image_DiFen;
			Poker* Poker_First;
			ImageView* Image_BG;

			Button * Chat;
			Button * voice;
			GameChatLayer * chatLayer = nullptr;

			ImageView* Clock;
			TextAtlas* Clock_Text;

			Sprite* Sp_baojingqi[PLAY_COUNT];

			//出牌阶段
			Button * btnOurCard;		//出牌 按钮
			Button * btnSurggest;		//提示按钮
			Button * btnExit;			//退出
			Button * jiesan;			//解散
			Button * btnSet;			//设置
			Button * btnHelp;			//规则
			Button * btnCardCount;		//记牌器
			ImageView *wait;  //等待开始
			ImageView * CardCountImg;//记牌器图片
			//托管按钮
			Button * btnTuoGuan;		//托管按钮
			Layout * CancelPlane; //取消托管
			Button * btnCancelTuoGuan; //取消托管

			// 出牌列表
			ListView* poolcards[PLAY_COUNT];
			// 手牌
			ListView* HandList[PLAY_COUNT];
		
			// 结算
			Node * SettlementNode;
			ListView*  SettlementInfo;

			//离开
			Button * btnLeft;
			TextBMFont* LeftText;
			//继续
			Button * btnContinue;
			//报警器
			ImageView*	baojingqiImg[PLAY_COUNT];
			//剩余牌数
			ImageView* pokerCountImg[PLAY_COUNT];
			Text*		pokerCountText[PLAY_COUNT];

			//用户数据
			const UserInfoStruct*					_vecUser[PLAY_COUNT];

		}_TableUI;

		CardSuite *					_suite = nullptr;
		Player					   _players[PLAY_COUNT];
		Button*					_TouchPlayer[PLAY_COUNT];
		UserInfoStruct*					_vecUser[PLAY_COUNT];
		CalculateBoardAll *        _GameEndLayer = nullptr;
		Vec2						   _playersPos[PLAY_COUNT];
		Vec2						   _ClockPos[PLAY_COUNT];
		Vec2						_IP_baojing[PLAY_COUNT];
		string					_vecUserGeographic[PLAY_COUNT];			//地理位置以及距离存储
		float					_Array_Distance[PLAY_COUNT][3];

		INT				_actionPoint[PLAY_COUNT];//动作分
		INT				_ActionMoney[PLAY_COUNT];//临时用户金币
		
		
		int _ShowTime = 0;
		BYTE _ShowDeskView = 0;
		int _nowTime = 0;

		bool			m_isUserLeave = false;
		bool			m_isNewTurn = false;

		//游戏名字显示在桌布上
		Text*		_Text_GameName = nullptr;
		Text*		_FNTtext_MatchGame = nullptr;
		void hideAllAlert();

		//弹窗用户信息
		void getUserInfo(Ref* pSender, Widget::TouchEventType type); //玩家头像回调
		void refreshUserIPlight();
		//void afterScoreByDeskStation(int Station, int money);

		// 牌池
		void clearAllPoolCard();
		void clearPoolCard(BYTE viewseat);

		void clearAllHandcard();
		Node* getCardByPos(Vec2 localpos);

		// 选中的手牌
		Poker* getCardByValue(BYTE cardvalue);
		void setSelected(BYTE cardvalue, bool selected);
		void setSelectedCards(std::vector<BYTE> cardvalues);
		void resetSelectedCards();
		void resetTouchTag();

		void removeSetLayer();

		void showtime(int showtime, BYTE viewseat);
		void UpdateTimer(float dt);

		std::vector<Poker*> getSelectedCards();
		std::vector<BYTE> getSelectedCardsValue();

	public:
		//更新用户信息
		//userNicName 玩家的昵称
		//i64UserMoney 玩家的金币
		//byDeskViewStation 玩家的视图座位号
		virtual void UpdateUserInfo(const int Seatno, const UserInfoStruct* user) override;

		//更新玩家的金币
		//_i64UserMoney 玩家金币
		//byDeskViewStation 玩家的视图座位号
		virtual void SetUserMoney(LLONG i64UserMoney, BYTE byDeskViewStation) override;

		virtual void UpDateUserLocation(int index) override;			//更新经纬度地理位置

		virtual void afterScoreByDeskStation(int Station, int money) override;

		//显示不出、出牌、提示按钮
		//bDoNotOut 手否显示不出按钮 0 表示隐藏 1 表示显示
		//bCanOut 显示出牌按钮
		//bSurggest 显示提示按钮
		virtual void ShowRefferOutCardBtn(BYTE byDeskViewStation) override;

		//显示定时器
		//iTimeCount 倒计时显示的总时间
		//byDeskViewStation 玩家的视图座位号
		//bShowOrHide 0 表示隐藏 1 表示显示
		//bIsReadyOrOut 0 时间到关闭客户端 1 时间到自动出牌
		virtual void ShowTime_PDK(int iTimeCount, BYTE byDeskViewStation) override;

		virtual void ShowFirstOutCard(BYTE byDeskView, BYTE FirstNeetOutCard) override;
		virtual void setGameMultiple(int Multiple) override;
		virtual void removeFirstOutCard() override;

		//游戏结束之后倒计时显示是否离开游戏
		virtual void ShowTimeLeaveGame(int iTimeCount);
		void UpdateLeaveTimer(float dt);
		//点击开始，同意后的准备图片的显示
		//byDeskViewStation 玩家的视图座位号
		//bIsShowOrHide 0 表示隐藏 1 表示显示
		virtual void ShowReadyImage(BYTE byDeskViewStation, bool bIsShowOrHide) override;
		virtual void ShowUserOutLine(BYTE byDeskViewStation, bool bIsShowOrHide) override;

		//设置手牌
		virtual void SetUserCard(BYTE byDeskViewStation, BYTE * byCard, BYTE byCardCount) override;

		//设置出牌
		virtual void SetUserOutCard(BYTE byDeskViewStation, BYTE * byCard, BYTE byCardCount) override;

		//设置其他玩家牌的剩余张数
		virtual void setOtherUserPokerCount(BYTE SeatNo, BYTE byCardCount);

		//设置游戏规则
		virtual void setGamePlayRules(BYTE Wanfa, BYTE Difen, BYTE beishu);

		//设置3A最大最小规则
		//virtual void setGame3AMax(BYTE ithreeaaa);

		//显示不出图片
		virtual void ShowDoNotOutCard(BYTE byDeskViewNo, bool bShowOrHide) override;

		//断线重连状态界面

		//等待同意状态界面
		virtual void GameStationWaitAgree() override;

		//游戏中界面状态
		virtual void GameStationPlaying(S_C_GameStation_Play * pPlaying) override;

		//设置游戏名字,是否显示桌面底分,是否显示倍数
		virtual void setDeskInfo(UINT GameID);

		//显示游戏规则文字
		virtual void setDeskStr(string str);

		//获取游戏规则信息
		virtual void setDeskConfig(int index);

		//结算界面显示
		//pGameEnd 结算数据结构
		virtual void ShowSettlement(S_C_GameEndStruct * pGameEnd, bool bShowOrHide) override;

		//显示托管标识
		virtual void ShowTuoGuanLogo(BYTE byDeskViewStation, bool bShowOrHide) override;
		//显示报警标识
		virtual void ShowBaoJing(BYTE byDeskViewStation, bool bShowOrHide) override;
		virtual void GameOverInfo(S_C_SettlementList *) override;

		//显示发牌动画
		virtual void PlaySendCardAnimation() override;
		virtual void SendCardFinish()override;


		//重置UI
		virtual void ResetUI() override;

		virtual void dealLeaveDesk() override;
		virtual void userUp(BYTE viewseat) override;

		// 托管按钮
		virtual void showTuoguanBtn(bool visible) override;

		// 合成播放三张音效
		virtual void playEffect(BYTE value) override;

		// 游戏开始动画特效
		virtual void playGameStartAnimi() override;

		virtual void onChatTextMsg(int userID, std::string msg)override;										//处理文本聊天消息

		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime)override;							//处理语音聊天消息

		virtual void onActionChatMsg(BYTE sendSeatNo, BYTE TargetSeatNo, int index)override;			//处理玩家之间的动作特效

		virtual void setCardCountResult(std::vector<std::vector<BYTE>> card);

		//设置新一轮出牌处理（新一轮出牌不进行提示出牌）
		virtual void setNewTurnOutCard(bool isNewTurn)override;

		//请求红包任务状态
		virtual void CheckActivity()override;

		//请求红包奖励
		virtual void CheckReceive()override;

		//更新游戏的进度
		virtual void	setActivityNum(int Current, int toal, int CanTake)override;
		//更新游戏红包中奖
		virtual void setReceiveState(int ReceiveType, int AwardNum)override;

	private:

		// 操作按钮
		void showActionBtn(bool visible);

		void adjustSeatCardList(int seatno);

		//结算界面操作按钮是否显示（离开，继续）
		void showRestBtn(bool visible);

		// touch事件
		void OnTouch(Ref *pSender, Widget::TouchEventType type);

		//请求出牌
		void AskOutCard();

		//是否被关牌
		void IsSpring();

		//取帧动画
		Animation* getAni(std::string name);

		//提示
		void AskSurggest();
		//滑动提示
		void HuaDongSurggest();

		//离开游戏
		void ExitGame();

		void setDeskInfo();

		//聊天界面
		void gameChatLayer();

		//设置游戏规则
		void setRules(BYTE wanfa);

		//设置游戏底分
		void setDifen(BYTE difen);
	};
}


#endif // __GModelTableUIPC_h__
