/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/


/************************************************************************/
/*   字符串/数字格式化类                                                 */
/************************************************************************/
#ifndef __RunCrazyDataFormat_h__
#define __RunCrazyDataFormat_h__

#include "HNLobbyExport.h"
#include "HNNetExport.h"

namespace RunCrazy
{
    class Player
    {
    public:

		Player();

		void LoadRes(Node *);
		void ResetPlayerUI();

		void showReady(bool);
		void showAuto(bool);
		void showHost(bool);
		void showOutLine(bool);
		void showtime( bool show,int timecount);

		void setUserIntegral(LLONG integral);//设置游戏积分
		void setUserPoints(BYTE difen);		//设置游戏底分
		void setUserMultiple(int Multiple);
		void setUserMultiplebShow(bool isShow);
		void setUserMoney(LLONG money);
		void setUserbBoy(bool );
		void setUserName(std::string name);
		void setUserID(int ID);
		void setHeadUrl(std::string url);
		void setFrameUrl(std::string url);
		void setTextureWithUrl(std::string url);
		void setVIPHead(std::string url, INT VipLevel);
		void setHeadByFaceID(int faceid);
		void setVipText(int rank);
		Vec2 getChatPos()const;
		int getUserMultiple();

		// 打不起
		void showNotout(bool);

	private:

		void UpdateTimer(float dt);

		struct
		{
			Node *      Root;
			GameUserHead*  HeadImg;
			ImageView*  OutLine;
			ImageView*  Auto;
			ImageView*  HostPlayer;
			ImageView*  FrameImg;//头像框
			ImageView*	VipImg;//vip底框图片
			TextAtlas*	VipText;//vip文本
			ImageView*  headImg;//头像

			Text * Name;
			TextBMFont * Money;
			ImageView* Image_icon;
			TextBMFont * Integral = nullptr;			//积分
			ImageView* Image_fen;
			TextBMFont * Points = nullptr;			//底分
			ImageView* Image_difen;
			TextBMFont * Multiple = nullptr;			//倍数
			ImageView* Image_beishu;

			// 时间
			//ProgressTimer* timerProgress;
			ImageView* Clock;
			TextAtlas* Clock_Text;

			// 打不起
			Node* StateNode;
			ImageView* StateImg;

		} _PlayerUI;

		int userID;
		float currenttime;
		float stopTimer;
		int _Multiple;
		
    };

}

#endif // __RunCrazyDataFormat_h__