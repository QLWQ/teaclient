#include "RunCrazyCardCardSuite.h"

namespace RunCrazy
{

	CardSuite::CardSuite(void) :
		_empty(true),
		_totalCards(TOTAL_CARDS),
		_scale(1),
		_sent(0)
	{
		memset(_suite, 0, sizeof(_suite));
	}


	CardSuite::~CardSuite(void)
	{
	}


	CardSuite* CardSuite::create(float scale)
	{
		CardSuite* suite = new CardSuite();
		if (suite->init(scale))
		{
			suite->autorelease();
			return suite;
		}

		CC_SAFE_DELETE(suite);
		return nullptr;
	}

	bool CardSuite::init(float scale)
	{
		if (!Layer::init())
		{
			return false;
		}

		_scale = scale;
		for (int i = 0; i < _totalCards; i++)
		{
			_suite[i] = Poker::create(0x00);
			_suite[i]->setScale(_scale);
			_suite[i]->setVisible(false);
			this->addChild(_suite[i], i);
		}

		return true;
	}

	void CardSuite::runActionPair(Node* node, Action* action)
	{
		if (node == nullptr || action == nullptr)
		{
			return;
		}

		ActionPair pair;
		pair.node = node;
		pair.action = Sequence::create((FiniteTimeAction*)action,
			CallFunc::create([=](){
			ActionPair pair = _queue.front();
			pair.action->release();
			_queue.pop_front();
			_empty = _queue.empty();
			run(0.0f);
		}),
			nullptr);
		pair.action->retain();
		_queue.push_back(pair);

		if (_empty)
		{
			_empty = false;
			run(0.0f);
		}

	}

	void CardSuite::run(float delta)
	{
		if (!_queue.empty())
		{
			ActionPair pair = _queue.front();
			pair.node->runAction(pair.action);
		}
	}

	//float radius = 350.0f, float alpha = 30.0f, float offsetY = 350.0f
	//float radius , float alpha, float offsetY
	void CardSuite::shuffle(float duration, float radius, float alpha, float offsetY)
	{
		float beta = (180 - alpha) / 2;

		// Angle value to Radians value
		auto r_alpha = alpha * M_PI / 180;
		auto r_beta = beta * M_PI / 180;
		auto r_alphaSplit = r_alpha / (TOTAL_CARDS - 1);

		Sequence* s = nullptr;
		for (int i = 0; i < TOTAL_CARDS; i++)
		{
			auto x_offset = radius*cos(r_alphaSplit*i + r_beta);
			auto y_offset = (-1)*radius*sin(r_alphaSplit*i + r_beta) + offsetY;
			auto rotateAngle = (r_alphaSplit*i + r_beta) * 180 / M_PI - 90;

			_suite[i]->setPosition(Vec2(x_offset, y_offset));
			_suite[i]->setScale(0.8f);
			auto action = Sequence::create(
				RotateBy::create(duration / TOTAL_CARDS, rotateAngle),
				CallFunc::create([=](){
				_suite[i]->setVisible(true);
			})
				, nullptr);

			auto t = TargetedAction::create(_suite[i], action);
			if (s == nullptr)
				s = Sequence::create(t, nullptr);
			else
				s = Sequence::create(s, t, nullptr);
		}

		this->runActionPair(this, s);
		_sent = TOTAL_CARDS - 1;
	}

	void CardSuite::recycle(float duration)
	{
		Sequence* s = nullptr;
		for (int i = TOTAL_CARDS - 1; i >= 0; i--)
		{
			auto action = Sequence::create(
				DelayTime::create(duration / TOTAL_CARDS),
				CallFunc::create([=](){
				_suite[i]->setVisible(false);
				_suite[i]->setRotation(0);
			}),
				nullptr);

			auto t = TargetedAction::create(_suite[i], action);
			if (s == nullptr)
				s = Sequence::create(t, nullptr);
			else
				s = Sequence::create(s, t, nullptr);
		}
		this->runActionPair(this, s);
	}

	void CardSuite::dealCard(float duration, Vec2 target, std::function<void()> callback)
	{
		if (_suite[_sent] != nullptr)
		{
			float duration1;
			float duration2;
			if (duration > 0.25f)
				duration1 = 0.1f;
			else
				duration1 = duration / 2;

			duration2 = duration - duration1;

			auto card = _suite[_sent--];
			card->setScale(_scale);
			auto tarpos = card->getParent()->convertToNodeSpace(target);

			auto rotate1 = RotateTo::create(duration1, 0);
			auto rotate2 = RotateTo::create(duration2, 180);
			auto move = MoveTo::create(duration2, tarpos);
			auto spaw = Spawn::create(move, rotate2, nullptr);

			auto action = Sequence::create(
				rotate1,
				spaw,
				CallFunc::create([=](){
				card->setVisible(false);
				callback();
			}),
				nullptr);

			this->runActionPair(card, action);
		}

	}

	void CardSuite::restoreCardSize(float fScale)
	{
		if ((int)_sent <= 0 || (int)_sent > TOTAL_CARDS)
		{
			return;
		}
		if (_suite[_sent - 1] != nullptr)
		{
			_suite[_sent - 1 ]->setScale(fScale);
		}
	}

	TargetedAction* CardSuite::dealCard(float duration, Vec2 target, float dstAngle, float fScale)
	{
		if ((int)_sent <= 0 || (int)_sent > TOTAL_CARDS - 1)
		{
			return nullptr;
		}
		if (_suite[_sent] != nullptr)
		{
			float duration1;
			float duration2;
			if (duration > 0.25f)
				duration1 = 0.1f;
			else
				duration1 = duration / 2;

			duration2 = duration - duration1;
			auto card = _suite[_sent--];
			card->setScale(_scale);
			auto tarpos = card->getParent()->convertToNodeSpace(target);

			auto rotate1 = RotateTo::create(duration1, 0);
			auto rotate2 = RotateTo::create(duration2, dstAngle);
			auto scale1 = ScaleTo::create(duration2, fScale);
			auto move = MoveTo::create(duration2, tarpos);
			auto spaw = Spawn::create(scale1, move, rotate2, nullptr);

			auto action = Sequence::create(
				rotate1,
				spaw,
				CallFunc::create([=](){
				card->setVisible(false);
			}),
				nullptr);
			
			return TargetedAction::create(_suite[_sent], action);
		}
		else
		{
			return nullptr;
		}
	}

}