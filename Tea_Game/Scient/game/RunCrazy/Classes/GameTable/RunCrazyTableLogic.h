/****************************************************************************
Copyright (c) 2014-2016 ShenZhen Red Bird Network Technology Co.,Ltd

http://www.hotniao.com

All of the content of the software, including code, pictures,
resources, are original. For unauthorized users, the company
reserves the right to pursue its legal liability.
****************************************************************************/

#ifndef __RunCrazyTableLogic_h__
#define __RunCrazyTableLogic_h__

#include "HNNetExport.h"
#include "HNLogicExport.h"
#include "RunCrazyUpgradeLogic.h"

namespace RunCrazy
{
	class TableUICallback;

	class TableLogic : public HN::HNGameLogicBase
	{
	public:
		/*
		 * 功能：构造函数
		 * 参数：uiCallback-界面回调指针，deskId-桌子号，autoCreate-是否自动坐下
		 */
		TableLogic(TableUICallback* uiCallback, BYTE deskId, bool autoCreate);

		// 析构函数
		virtual ~TableLogic();

	public:
		virtual void clearDesk() override;

		virtual void clearDeskUsers() override;

	public:
		//发送游戏消息
		void SendGameData(int iMsgID, void* object, INT objectSize);

		//出牌是否合乎规则
		bool CanOutCard(BYTE * byOutCard, int iOutCardCount);

		//当前出牌玩家显示按钮，计时器
		void ShowOutCardBtnAndTimer(BYTE byNowOutCarduser, BYTE byTime);

		//提示出牌
		//iResultCard 出牌结果
		//iResultCardCount 出牌数量
		void GetAutoOutCard(BYTE iResultCard[], int & iResultCardCount);

		// 滑动出牌
		void GetAutoOutCard(std::vector<BYTE> iCardlist, BYTE iResultCard[], int & iResultCardCount ,bool bSelect);

		BYTE GetNextDeskStation(BYTE bDeskStation);

		//自动出牌
		void AutoOutCard();

		//逻辑转试图
		BYTE logicToViewSeatNo(BYTE lSeatNO);

		//获取房间底分
		int GetBasePoint();
		bool isConnected() { return RoomLogic()->isConnect(); };
		// 托管
		void sendTuoguan(bool dotuoguan);

		//void setIs3AMax(BYTE ithreeaaa);
		//是否超端用户
		bool isSuperUser();

	private:
		/* 非游戏消息 */
		// 玩家同意
		virtual void dealUserAgreeResp(MSG_GR_R_UserAgree* agree) override;

		// 玩家坐下
		virtual void dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;

		// 排队坐下
		virtual void dealQueueUserSitMessage(bool success, const std::string& message) override;

		// 玩家站起
		virtual void dealUserUpResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;

		// 游戏信息
		virtual void dealGameInfoResp(MSG_GM_S_GameInfo* pGameInfo) override;

		// 玩家信息变化
		virtual void dealUserInfoChangeResp(const UserInfoStruct* user) override;

		// 游戏状态（重连）
		virtual void dealGameStationResp(void* object, INT objectSize) override;

		//设置回放模式
		virtual void dealGamePlayBack(bool isback) override;

		/* 游戏消息 */
		virtual void dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize);

		// 玩家掉线
		virtual void dealUserCutMessageResp(INT userId, BYTE bDeskNO) override;

		// 清理游戏
		virtual void dealGameClean() override;

		//更新比赛场进度
		virtual void dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum) override;
		virtual void dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum) override;
		virtual void dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum) override;
	private:
		// 文字聊天消息
		virtual void dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk) override;

		//道具特效聊天
		virtual void dealUserActionMessage(DoNewProp* normalTalk) override;

		//游戏红包活动
		virtual void dealGameActivityResp(MSG_GR_GameNum* pData) override;

		//游戏内领取红包
		virtual void dealGameActivityReceive(MSG_GR_Set_JiangLi* pData) override;

		//比赛场游戏轮数
		//virtual void dealGameNumRound(MSG_GR_ConTestRoundNum* pData) override;

		//比赛场游戏局数
		//virtual void dealGameInnings(MSG_GR_ConTestRoundNumInOneRound* pData) override;

		// 语音聊天消息
		virtual void dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;

		// ***下面添加游戏处理函数，参照上面的-dealTestResp
		//更新所有玩家信息
		void UpdateAllUserInfo();

		//显示玩家所有牌
		void DealGameSendAllCard(void* object, INT objectSize);
		void DealGameSendCardFinish();

		//开始游戏
		void DealGameBeginPlay(void* object, INT objectSize);

		//处理出牌结果
		void DealUserOutCardResult(void* object, INT objectSize);

		//炸弹倍数通知
		void DealUserBombNum(void* object, INT objectSize);

		//伏击通知
		void DealUserFuji(void* object, INT objectSize);

		//新一轮出牌
		void DealNewTurnOutCard(void* object, INT objectSize);

		//要不起
		void DelCanCelCard(void* object, INT objectSize);

		//处理游戏结束消息
		void DealGameEnd(void* object, INT objectSize);


	private:
		// 选择正确的牌
		/*
		@iCardlist 提取牌列表
		@iCardlist 提取牌数量
		@iCardlist 提取牌数量
		@iResultCard 提取牌结果
		@iResultCard 提取牌结果张数
		@NextType 下一种牌型
		return 是否成功
		**/
		bool getBomb(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount, eRunCrazy NextType); // 获取炸弹

		/*
		@type 可取1 2 3 表示单顺 连对 飞机
		**/
		bool getSequence(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount, int type, eRunCrazy NextType); // 获取顺子

		bool getThreeAndTwo(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount,eRunCrazy NextType);  	// 获取三代2

		/*
		@type 可取1 2 3 表示四代N
		**/
		bool getFourX(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount, int type, eRunCrazy NextType);  	// 获取四代N

		/*
		@type 可取1 2 3 表示单张 对子 三张
		**/
		bool getSingleCard(BYTE * iCardlist, int cardSize, BYTE *iResultCard, int &iResultCardCount, int type); // 获取单张 对子 三张

		/*
		@type 获取比桌面上大的牌
		**/
		bool getOutCard(BYTE * iCardlist, int cardSize, BYTE * iOutlist, int OutSize, BYTE *iResultCard, int &iResultCardCount, bool firstout);

	protected:
		TableUICallback*					_uiCallback;//UI界面接口

		std::vector<std::vector<BYTE>>		_HandCard;				// 玩家手牌信息 
		std::vector<BYTE>					_byTiShiCardResult;     // 上局提示信息 
		int									firstoutType; // 当前首出的牌型
		BYTE							_FirstNecessaryCard;
		BYTE							_is3AMax;
		bool							_isThreeGTwo2 = false;		//	是否选三带二（含2）
		bool							_isShowResidue = false;		//	是否显示剩余
		int							_isAlarmNum = 0;		//	剩几张报警
		
		bool			_bIsSuper[PLAY_COUNT]; //超端用户

		//逻辑接口类
		CUpGradeGameLogic					_upGradeGameLogic;
		S_C_GameStation_Play				GameInfo;
	};


}

#endif // __GModelTableLogic_h__