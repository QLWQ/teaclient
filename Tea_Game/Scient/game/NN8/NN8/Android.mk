LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNNet)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNUI)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNMarket)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLobby)
$(call import-add-path,$(LOCAL_PATH)/../../../common/LibHNLogic)

LOCAL_MODULE := NN8_static

LOCAL_MODULE_FILENAME := libNN8

LOCAL_SRC_FILES := ../Classes/GameTable/NN8CardSuite.cpp \
				   ../Classes/GameTable/NN8GameNotice.cpp \
				   ../Classes/GameTable/NN8GameTableLogic.cpp \
			       ../Classes/GameTable/NN8GameTableUI.cpp \
				   ../Classes/GameTable/NN8GameUserMessageBox.cpp \
				   ../Classes/GameTable/NN8NodeList.cpp \
				   ../Classes/GameTable/NN8PlayerUI.cpp \
				   ../Classes/GameTable/NN8Poker.cpp \
				   ../Classes/GameTable/NN8PokerList.cpp \
				   ../Classes/GameTable/NN8ChatTip.cpp \
				   ../Classes/GameTable/NN8ResultShowLayer.cpp \
				   ../Classes/GameTable/NN8AllResultShowLayer.cpp \
				   ../Classes/GameTable/NN8ChatCommon.cpp \
				   ../Classes/GameTable/NN8CalculateNode.cpp \
				   ../Classes/GameTable/NN8SetLayer.cpp \
				   ../Classes/GameTable/NN8SuperSet.cpp \
				   
             
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameMessage \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../Classes/GameTable \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../ \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../ \
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../common/CommonHead \

LOCAL_EXPORT_LDLIBS := -llog \
                       -lz \
                       -landroid     
                   
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

LOCAL_WHOLE_STATIC_LIBRARIES += hn_ui_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_net_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_market_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_lobby_static
LOCAL_WHOLE_STATIC_LIBRARIES += hn_logic_static

include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)
$(call import-module,editor-support/cocostudio)
$(call import-module,extensions)

$(call import-module,LibHNUI)
$(call import-module,LibHNNet)
$(call import-module,LibHNMarket)
$(call import-module,LibHNLogic)
$(call import-module,LibHNLobby)