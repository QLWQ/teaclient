#ifndef __NN8_AFC_SERVER_SHOWHAND_MESSAGE_HEAD_FILE_H__
#define __NN8_AFC_SERVER_SHOWHAND_MESSAGE_HEAD_FILE_H__

#include <memory.h>
#include <string>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"
USING_NS_CC;
#include "HNNetExport.h"
namespace NN8
{

	enum gameState
	{
		FAPAI=1,
		QIANGZHUANG,
		XIAZHU,
		XIKAO

	};

	#pragma pack(1)

// 	#ifdef MAX_PATH
// 	#undef MAX_PATH
// 	#endif

	
	enum Game_ID
	{
		NAME_ID							= 20170708,
		KIND_ID							= 1,
		
		//MAX_PATH						= 260,
		
		PLAY_COUNT						= 8,		// 游戏人数
		HANDCARD_COUNT					= 5,		// 手牌数
	};

	
	enum GAME_STATUS
	{
		GS_WAIT_SETGAME					= 0,			//等待东家设置状态
		GS_WAIT_ARGEE					= 1,			//等待同意设置
		GS_ROB_NT						= 20,			//抢庄状态
		GS_NOTE                         = 21,			//下底注状态
		GS_SEND_CARD					= 22,			//发牌状态
		GS_OPEN_CARD					= 23,			//摆牛状态
		GS_WAIT_NEXT					= 24			//等待下一盘开始
	};
	
	/////////////////////////////////////
	// socket message
	enum NN8_MESSAGE
	{
	
		ASS_CALL_SCORE					= 51,				//通知叫分		DWJ
		ASS_USER_SCORE					= 52,				//玩家叫分		DWJ
		ASS_CALL_SCORE_RESULT			= 53,				//通知叫分结果	DWJ
		ASS_CALL_SCORE_FINISH			= 54,				//通知叫分结束	DWJ
		ASS_CALL_NOTE					= 55,				//通知下注		DWJ
		ASS_USER_NOTE					= 56,				//玩家下注		DWJ
		ASS_CALL_NOTE_RESULT			= 57,				//通知下注结果	DWJ
		ASS_CALL_SEND_CARD				= 58,				//通知发牌信息	DWJ
		ASS_CALL_SEND_FINISH			= 59,				//发牌完成		DWJ
		ASS_CALL_OPEN					= 60,				//通知开牌摆牛	DWJ
		ASS_USER_OPEN					= 61,				//玩家摆牛		DWJ
		ASS_CALL_OPEN_RESULT			= 62,				//玩家摆牛结果	DWJ
		ASS_CALL_ALLOPEN_FINISH			= 63,				//所有玩家开牌结束	DWJ
		ASS_USER_TANPAI					= 64,				//玩家摊牌
		ASS_CALL_WAIT					= 99,				//玩家等待
		ASS_CALL_END					=100,				//解散房间

		ASS_CONTINUE_END				= 65,				//游戏结束
		ASS_NO_CONTINUE_END				= 66,				//游戏结束
		ASS_CUT_END						= 67,				//用户强行离开
		ASS_SALE_END					= 68,				//游戏安全结束
		ASS_GAME_RESULT                 = 69,               //游戏结束服务器发的结构体

		S_C_GAME_RECORD					= 161,			//战绩请求消息
		S_C_GAME_RECORD_RESULT			= 162,			//返回战绩

		ASS_SUPER_USER_SET				= 80,				//超端用户设置消息
		ASS_SUPER_USER_SET_RESULT	    = 81				//超端用户设置结果消息
		//ASS_SUPER_USER					= 79,				//超端用户消息
		
		
	};


    #define  TYPE_GIVE_UP					0x00			//放弃
    #define	TYPE_RESET                      0x01			//要牌
	#define	TYPE_OPEN                       0x02			//开牌
	#define	TYPE_BULL						0x03			//牛
	#define	TYPE_PLAY_GAME                  0x04			//是玩家
	#define	TYPE_NOTE						0x06			//下注

	//游戏状态数据包	（ 等待东家设置状态 ）
	struct GameStation_Base
	{
		//游戏版本
		BYTE				iVersion;						//游戏版本号
		BYTE				iVersion2;						//游戏版本号

		BYTE				byGameStation;					//游戏状态

		//游戏信息
		BYTE				iThinkTime;						//摊牌时间
		BYTE				iBeginTime;						//准备时间
		BYTE				iSendCardTime;					//发牌时间
		BYTE				iCallScoreTime;					//叫庄时间
		BYTE				iXiaZhuTime;					//下注时间

		BYTE				iAllCardCount;					//扑克数目

		int					iRoomBasePoint;					//房间倍数
		INT					iBaseNote;						//底注

		UINT				iCardShape;						//牌型设置
		int					scoreArray[PLAY_COUNT];			//玩家分数 
		bool				superUser[PLAY_COUNT];			//超级玩家


		GameStation_Base()
		{
			memset(this,0,sizeof(GameStation_Base));
			memset(superUser,false,sizeof(superUser));

		}

	};

	//游戏状态数据包	（ 等待其他玩家开始 ）
	struct GameStation_WaiteAgree : public GameStation_Base
	{
		bool                bUserReady[PLAY_COUNT] ;        //玩家是否已准备

		GameStation_WaiteAgree()
		{
			memset(this,0,sizeof(GameStation_WaiteAgree));
		}
	};

	//游戏状态数据包	（ 叫庄状态 ）
	struct GameStation_RobNt : public GameStation_Base
	{
		BYTE			byCurrentCallScore;					//当前抢庄者 
		BYTE			byUserCallStation[PLAY_COUNT];		//各玩家抢庄情况	-1-表示还没操作 0-表示不抢 1-表示已经抢了
		BYTE			iUserStation[PLAY_COUNT];			//各玩家状态 标记是否中途加入的
		BYTE			iUserCardCount[PLAY_COUNT];				//用户手上扑克数目
		BYTE			iUserCard[PLAY_COUNT][HANDCARD_COUNT];	//用户手上的扑克
		LLONG			i64PerJuTotalNote[PLAY_COUNT];		    //用户每局压总注
		BYTE			byGameModel;							//游戏模式 0 明牌抢庄模式  1 经典模式 2 通比模式

		GameStation_RobNt()
		{
			memset(this,0,sizeof(GameStation_RobNt));
			memset(byUserCallStation,255,sizeof(byUserCallStation));
		}
	};
	
	//游戏状态数据包	（ 下注状态 ）
	struct GameStation_Note : public GameStation_Base
	{
		BYTE			byNtStation;							//庄家位置
		int				iUserStation[PLAY_COUNT];				//各玩家下注状态 -1-表示还没操作 0-表示不抢 1-表示已经抢了
		LLONG			i64PerJuTotalNote[PLAY_COUNT];			//用户每局压总注
		LLONG			iLimitNote[PLAY_COUNT];					//最大注数
		LLONG			i64UserNoteLimite[PLAY_COUNT][6];		//玩家6个下注数字
	
		GameStation_Note()
		{
			memset(this,0,sizeof(GameStation_Note));
			memset(iUserStation,-1,sizeof(iUserStation));
		}
	};
	
	//游戏状态数据包	（ 发牌状态 ）
	struct GameStation_SendCard : public GameStation_Base
	{
		BYTE			byNtStation;							//庄家位置
		BYTE			iUserCardCount[PLAY_COUNT];				//用户手上扑克数目
		BYTE			iUserCard[PLAY_COUNT][HANDCARD_COUNT];	//用户手上的扑克
		int				iUserStation[PLAY_COUNT];				//各玩家状态 标记是否中途加入的
		LLONG			i64PerJuTotalNote[PLAY_COUNT];		    //用户每局压总注
		INT             bReadyOpenCard[PLAY_COUNT];//已经摊牌的玩家
	
		GameStation_SendCard()
		{
			memset(this,0,sizeof(GameStation_SendCard));
		}
	};
	
	//游戏状态数据包	（ 发牌状态 ）
	struct GameStation_OpenCard : public GameStation_Base
	{
		BYTE			byNtStation;							//庄家位置
		int				iUserStation[PLAY_COUNT];				//各玩家下注状态 -1-表示还没操作 0-表示不抢 1-表示已经抢了
		BYTE			iUserCardCount[PLAY_COUNT];				//用户手上扑克数目
		BYTE			iUserCard[PLAY_COUNT][HANDCARD_COUNT];	//用户手上的扑克

		BYTE			byOpenUnderCount[PLAY_COUNT];			//底牌张数
		BYTE			byOpenUnderCard[PLAY_COUNT][3];			//底牌的三张牌
		BYTE			byOpenUpCard[PLAY_COUNT][2];			//升起来的2张牌
		int				iOpenShape[PLAY_COUNT];					//摆牛牌型

		LLONG			i64PerJuTotalNote[PLAY_COUNT];			//用户每局压总注
	
		GameStation_OpenCard()
		{
			memset(this,0,sizeof(GameStation_OpenCard));
		}
	};

	//游戏开始
	struct BeginUpgradeStruct
	{
		int				i64UserNoteLimite[PLAY_COUNT][5];	//玩家下注数字
	};


	/// 发牌数据包，一次将扑克全部发给客户端
	struct SendAllCardStruct
	{
		BYTE      iStartPos;									//发牌起始位置
		BYTE      iUserCard[PLAY_COUNT][HANDCARD_COUNT];		//用户扑克
		BYTE      iUserCardCount[PLAY_COUNT];

		SendAllCardStruct()
		{
			memset(iUserCard,0,sizeof(iUserCard));
			memset(iUserCardCount,0,sizeof(iUserCardCount));
			iStartPos = 255;
		}
	};

	//发牌结束
	struct SendCardFinishStruct
	{
		LLONG			i64PerJuTotalNote[PLAY_COUNT];			//用户每局压总注


		SendCardFinishStruct()
		{
			memset(i64PerJuTotalNote, 0, sizeof(i64PerJuTotalNote));

		}
	};

	//游戏开始数据包
	struct BeginPlayStruct
	{
		BYTE				iOutDeskStation;					//出牌的位置
	};


	//游戏结束统计数据包
	struct GameEndStruct
	{
		int					iUserState[PLAY_COUNT];				//四家状态(提前放弃,还是梭)

		BYTE				iCardShape[PLAY_COUNT];
		LLONG				iTurePoint[PLAY_COUNT];				//庄家得分
		LLONG				iChangeMoney[8];
		BYTE				iUpGradeStation;					//庄家位置
		BYTE                iUpBullCard[PLAY_COUNT][3];			//升起的牌
		BYTE				iCardList[PLAY_COUNT][10];			//扑克信息
	};

	//游戏结束统计数据包
	struct GameCutStruct
	{
		int					bDeskStation;						//退出位置
		LLONG					iTurePoint[PLAY_COUNT];			//庄家得分
		LLONG				iChangeMoney[8];
		BYTE                iQuitType;							//退出类型
	};

	//叫分数据包
	struct CallScoreStruct
	{	

		BYTE				bDeskStation;							//当前叫分者
		int					iValue;									//叫分类型（1叫分,0不叫分）
		bool 				bCallScoreflag;							//叫分标记		
		bool				bPlayer;
		BYTE				byUserState[PLAY_COUNT];				//各玩家的状态(标记是否中途进入的 还是一直在游戏当中的玩家)
		BYTE				byUserCallStation[PLAY_COUNT];		//各玩家抢庄情况	255-表示还没操作 0-表示不抢 1-表示已经抢了
		CallScoreStruct()
		{
			memset(this,0,sizeof(CallScoreStruct));
			memset(byUserState,255,sizeof(byUserState));
			memset(byUserCallStation, 255, sizeof(byUserCallStation));
		}

	};
	//游戏结束后发送的数据包
	struct GameEndInfoStruct
	{
		int     AllCount[PLAY_COUNT];                   //1总成绩..
		BYTE	winAllTimes[PLAY_COUNT];				//通杀次数
		BYTE	loseAllTimes[PLAY_COUNT];				//通赔次数
		BYTE	nnTimes[PLAY_COUNT];				    //牛牛次数
		BYTE	winTimes[PLAY_COUNT];				    //胜利次数
		GameEndInfoStruct()
		{

			memset(AllCount, 0, sizeof(AllCount));
			memset(winAllTimes, 0, sizeof(winAllTimes));
			memset(loseAllTimes, 0, sizeof(loseAllTimes));
			memset(nnTimes, 0, sizeof(nnTimes));
			memset(winTimes, 0, sizeof(winTimes));
		};
	};
	
	//玩家摊牌结构体
	struct UserTanPai
	{
		BYTE	byDeskStation;		//摊牌玩家的位置
		INT     bReadyOpenCard[PLAY_COUNT];//已经摊牌的玩家
		BYTE	byUnderCount;		//底牌张数
		BYTE	byUnderCard[3];		//底牌的三张牌
		BYTE	byUpCard[2];		//升起来的2张牌
		int		iShape;				//摆牛牌型

		UserTanPai()
		{
			memset(this,0,sizeof(UserTanPai));
			byDeskStation = 255;
		}
	};

	//用户处理信息
	struct tagUserProcess
	{
		INT  bReadyOpenCard[PLAY_COUNT];//已经摊牌的玩家
		BYTE iVrebType;					//所处理的按钮
		BYTE bUpCard[3];
		int  iNoteType;					//下注数
		tagUserProcess()
		{
			memset(this,0,sizeof(tagUserProcess));
		}
	};

	//发送用户押注
	struct NoteResult
	{
		BYTE bAddStyle;					//下注类型
		BYTE iOutPeople;				//下注者
		int iCurNote;					//当前玩家下注数
	};

	struct GameFinishNotify
	{
		CHAR name[PLAY_COUNT][21];
		int	iBasePoint;
		BYTE iStyle;					//游戏类型是否为强退,还是正常结束
		LLONG	iWardPoint[PLAY_COUNT];
		BYTE iCardShape[PLAY_COUNT];
		LLONG iMoney[PLAY_COUNT];
	};


	//超端控制
	struct	C_S_Super_Control
	{
		int winUserID;
		int loseUserID;


		C_S_Super_Control()
		{
			memset(this, 0, sizeof(C_S_Super_Control));

		}
	};

	//超端控制
	struct	S_C_Super_Control_Result
	{
		bool isSuccese;

		S_C_Super_Control_Result()
		{
			memset(this, 0, sizeof(S_C_Super_Control_Result));
			isSuccese = false;

		}

	};
	//用户等待发送模式信息
	struct gameModel
	{
		BYTE gameModelData; //0 明牌 1经典 2通比
	};

	
	typedef struct SuperUserSetData
	{
		BYTE	byDeskStation;      /**< 玩家位置 */
		bool	bSetSuccese;		//是否设置成功了
		BYTE	byMaxDesk;			//设置最大玩家的位置
		BYTE	byMinDesk;			//设置最小玩家的位置
		SuperUserSetData()
		{
			memset(this,255,sizeof(SuperUserSetData));
			bSetSuccese = false;
		}
	};
	//战绩信息
	struct S_C_GameRecordResult
	{
		int JuCount[8][10];	//每局的玩家输赢分数
		int GameCount;

		S_C_GameRecordResult()
		{
			memset(this, 0, sizeof(S_C_GameRecordResult));
		}
	};


	/////////////////////////////////////
	// audio effect

	const static char* AUDIO_LOSE						= "NN8/audio/table/snglose.mp3";
	const static char* AUDIO_WIN						= "NN8/audio/table/sngwin.mp3";

	const static char* AUDIO_RUNCRAD					= "NN8/audio/table/runcard.mp3";
	const static char* AUDIO_CLICK					    = "NN8/audio/table/dianjipai.mp3";


	

    #pragma pack()
}
#endif
