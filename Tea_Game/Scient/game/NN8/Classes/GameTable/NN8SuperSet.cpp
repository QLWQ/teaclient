#include "NN8SuperSet.h"
#include "HNLobbyExport.h"


#define SuperSet_csb  "Games/NN8/GameSuperSet/superSet.csb"
namespace NN8
{

	GameSuperSet::GameSuperSet()
		:_tableLogic(nullptr)
		
	{
		memset(_player,0,sizeof(_player));
	}
	GameSuperSet::~GameSuperSet()
	{

	}
	GameSuperSet* GameSuperSet::create(GameTableLogic* tableLogic)
	{
		GameSuperSet* show=new GameSuperSet();
		if(show->init(tableLogic))
		{
			show->autorelease();
			return show;
		}
		else
		{
			CC_SAFE_DELETE(show);
			return nullptr;
		}
	}

	bool GameSuperSet::init(GameTableLogic* tableLogic)
	{
		if (!HNLayer::init())
		{
			return false;
		}
		_tableLogic=tableLogic;
		Size winSize=Director::getInstance()->getWinSize();
		auto node=CSLoader::createNode(SuperSet_csb);
		ImageView* pUserImage=(ImageView*)node->getChildByName("superSet");
		node->setPosition(winSize/2);
		this->addChild(node,10);
		Button* buttonOK=(Button*)pUserImage->getChildByName("Button_OK");
		buttonOK->addTouchEventListener(CC_CALLBACK_2(GameSuperSet::btClikeBtBack,this));
		Button* buttonCancel=(Button*)pUserImage->getChildByName("Button_cancle");
		buttonCancel->addTouchEventListener(CC_CALLBACK_2(GameSuperSet::btClikeBtBack,this));
		for (int i=0;i<PLAY_COUNT;i++)
		{
			_player[i]=(ImageView*)pUserImage->getChildByName(StringUtils::format("player%d",i+1));
			CheckBox* selectWin=(CheckBox*)_player[i]->getChildByName("winSelect");
			selectWin->setTag(SelectKind::WIN);
			CheckBox* selectLose=(CheckBox*)_player[i]->getChildByName("loseSelect");
			selectLose->setTag(SelectKind::LOSE);
			selectWin->addEventListener(CC_CALLBACK_1(GameSuperSet::onClikCheckBox,this));
			selectLose->addEventListener(CC_CALLBACK_1(GameSuperSet::onClikCheckBox,this));

		}
		initOnlineUser();
		return true;
	}

	void GameSuperSet::initOnlineUser()
	{
		if (!_tableLogic)
		{
			return;
		}
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* pUserInfo=_tableLogic->getUserBySeatNo(i);
			if (pUserInfo==nullptr)
			{
				continue;
			}

			_player[i]->setVisible(true);
			Text* userName =(Text*) _player[i]->getChildByName("nickeName");
			userName->setString(GBKToUtf8(pUserInfo->nickName));
			Text* userId =(Text*) _player[i]->getChildByName("userID");
			userId->setString(StringUtils::format("%d",pUserInfo->dwUserID));

		}
	}

	void GameSuperSet::onClikCheckBox(Ref* pSelsct)
	{
		CheckBox* selectCheck=(CheckBox*)pSelsct;
		int pSelectTag=selectCheck->getTag();
		Node* prerentNode=selectCheck->getParent();
		selectKindByNode(prerentNode,pSelectTag);
	}

	void GameSuperSet::btClikeBtBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (touchtype != Widget::TouchEventType::ENDED)return;
		Button* pSelcectBt=(Button*)pSender;
		if ("Button_OK"==pSelcectBt->getName())
		{
			sendControl();
			this->removeFromParent();
		}
		if ("Button_cancle"==pSelcectBt->getName())
		{
			this->removeFromParent();
		}

	}

	void GameSuperSet::selectKindByNode(Node* pRootNode,int pTag)
	{
		for (int i=0;i<PLAY_COUNT;i++)
		{
			if (_player[i]==nullptr)
			{
				continue;
			}
			if (pRootNode==_player[i])
			{
				CheckBox* selectWin=(CheckBox*)_player[i]->getChildByName("winSelect");
				if (selectWin->isSelected())
				{
					selectWin->setSelected(pTag==SelectKind::WIN);
				}
			
				

				CheckBox* selectLose=(CheckBox*)_player[i]->getChildByName("loseSelect");
				if (selectLose->isSelected())
				{
					selectLose->setSelected(pTag==SelectKind::LOSE);
				}
			

			}
			else
			{
				CheckBox* selectWin=(CheckBox*)_player[i]->getChildByName("winSelect");
				if (pTag==SelectKind::WIN&&selectWin->isSelected())
				{
					selectWin->setSelected(false);
					
				}
				CheckBox* selectLose=(CheckBox*)_player[i]->getChildByName("loseSelect");
				if (pTag==SelectKind::LOSE&&selectLose->isSelected())
				{
					selectLose->setSelected(false);
					

				}
			}
		}
	}

	

	void GameSuperSet::sendControl()
	{
		if (_tableLogic==nullptr)
		{
			return;
		}
		BYTE pWinSeat=255;
		BYTE pLoseSeat=255;
		for (int i=0;i<PLAY_COUNT;i++)
		{

			if (_player[i]==nullptr)
			{
				continue;
			}

			CheckBox* selectWin=(CheckBox*)_player[i]->getChildByName("winSelect");
			CheckBox* selectLose=(CheckBox*)_player[i]->getChildByName("loseSelect");
			UserInfoStruct* userInfo=_tableLogic->getUserBySeatNo(i);
			if (selectWin->isSelected()&&userInfo)
			{
				 pWinSeat=userInfo->bDeskStation;

			}
			if (selectLose->isSelected()&&userInfo)
			{
				 pLoseSeat=userInfo->bDeskStation;

			}
			
		}
		if (pWinSeat==255&&pLoseSeat==255)
		{
			return;
		}
		_tableLogic->sendSuperControl(pWinSeat,pLoseSeat);

	}

}
