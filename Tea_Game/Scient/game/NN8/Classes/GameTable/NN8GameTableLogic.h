#ifndef __NN8_Game_Table_Logic_H__
#define __NN8_Game_Table_Logic_H__

#include "HNNetExport.h"
#include "NN8MessageHead.h"
#include "HNLogicExport.h"

namespace NN8
{
	class GameTableUICallback;

	/*
	 * NN8
	 * game table logic deal unit.
	 */
	enum BetType
	{
		GUDINGBET =0,
		FREEBET=1

	};
	class GameTableLogic : public HN::HNGameLogicBase
	{
	public:
		// construct & release
		GameTableLogic(GameTableUICallback* uiCallback, BYTE deskNo, bool autoCreate);
		virtual ~GameTableLogic();

		//void sendUserSit(BYTE seatId);
		void enterDesk();

		UserTanPai getBull();
		UserTanPai getAllBull(BYTE seatId);
		UserTanPai getBull(BYTE cardlist[]);
		BYTE getGameStatus();
		bool isPlaying(BYTE seat);

		// send message
		virtual void sendUserUp() override;
		virtual BYTE getGameType();			//»ñµÃÓÎÏ·ÀàÐÍ
		void sendNote(BYTE notetype);
		void sendGiveUp();
		void sendCallScore(bool call);
		void sendShowhand(UserTanPai show);

        // override
        INT getUserId(BYTE lSeatNo);
        UserInfoStruct* getUserByDeskStation(BYTE lSeatNo);
        bool getUserIsBoy(BYTE lSeatNo);
		void sendSuperControl(BYTE bWinSaat,BYTE bLoseSeat);

		void clearDesk();                                           // ÇåÀíÅÆ×À		
		void sendStandUp();

		UserInfoStruct* getUserInfo(int seat);
	protected:
		/////////////////////////////////////
		// game logic
		BYTE getCardPoint(BYTE card);
		inline BYTE getCardNum(BYTE iCard) { return(iCard & 0x0F) + 1; }
		void leaveDesk();
		/////////////////////////////////////
		// game message
		void deal_ASS_CALL_SCORE(void* object, INT objectSize);		// 51
		void dealCallScoreResult(void* object, INT objectSize);		// 53
		void dealCallScoreFinish(void* object, INT objectSize);		// 54
		void dealCallNote(void* object, INT objectSize);			// 55
		void dealCallNoteResult(void* object, INT objectSize);		// 57
		void dealCallSendResult(void* object, INT objectSize);		// 58
		void dealCallSendFinish(void* object, INT objectSize);		// 59
		void dealCallOpen(void* object, INT objectSize);			// 60
		void dealCallOpenResult(void* object, INT objectSize);		// 62
		void dealContinueEnd(void* object, INT objectSize);			// 65
		void dealCutEnd(void* object, INT objectSize);				// 67
		void dealSaleEnd(void* object, INT objectSize);				// 68
		void dealGameResult(void* object, INT objectSize);          //69
		void dealSuperSetResult(void* object, INT objectSize);         
		void dealOpenCardFinish(void* object, INT objectSize);
		void dealUserTanpai(void* object,INT objectSize);
		void dealUserWait(void* object, INT objectSize);


		/////////////////////////////////////
		// platform message
		virtual void dealGameClean() override;
		virtual void dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum) override;
		virtual void dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum) override;
		virtual void dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum) override;
		virtual void dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;
		virtual void dealUserAgreeResp(MSG_GR_R_UserAgree* agree) override;
		virtual void dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user) override;
		virtual void dealUserUpResp(MSG_GR_R_UserUp * userSit,  const UserInfoStruct* user)override;
		virtual void dealGameStartResp(INT deskId) ;
		virtual void dealGamePointResp(void* object, INT objectSize);
		virtual void dealGameEndResp(INT bDeskNO);
		
	
		virtual void dealUserCutMessageResp(INT userId, BYTE seatNo)override;


		virtual void clearDeskUsers() {}
		virtual void dealQueueUserSitMessage(bool success, const std::string& message){}
		virtual void dealUserInfoChangeResp(const UserInfoStruct* user) override;


		/////////////////////////////////////
		// game station
		virtual void dealGameStationResp(void* object, INT objectSize) override;
		//设置回放模式
		virtual void dealGamePlayBack(bool isback) override;
		void gameStation_GS_WAIT_ARGEE(void* object, INT objectSize);
		void gameStation_GS_ROB_NT(void* object, INT objectSize);
		void gameStation_GS_NOTE(void* object, INT objectSize);
		void gameStation_GS_SEND_CARD(void* object, INT objectSize);
		void gameStation_GS_OPEN_CARD(void* object, INT objectSize);

		void saveOpenCardData(UserTanPai* pTanPai);

		virtual void dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize) override;
		virtual void dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk) override;
		
	protected:
		GameTableUICallback* _gametableUi;
		GameStation_Base _gameStation;
		SendAllCardStruct _sendCard;
		std::vector<UserTanPai>  _openResultVec;
				 
	private:
		int		_moshi;
		bool    _isLast;
		BetType _pBetType;
	public:
		int  _iUserStation[PLAY_COUNT];
		bool _playerSitted[PLAY_COUNT];
		BYTE _iGameType;

	};
}


#endif