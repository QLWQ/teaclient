#ifndef NN8CalculateNode_h__
#define NN8CalculateNode_h__


#include "NN8MessageHead.h"
namespace NN8
{
	class CalculateNode: public Sprite
	{
	public:
		CalculateNode();
		~CalculateNode();
		CREATE_FUNC(CalculateNode);
	public:
		bool calculateNN(BYTE cardValue,bool isAddCardValue);
		void clear();
		bool isHaveNiu();
		bool isCalculateFinish();



	private:
		virtual bool init();
		BYTE getCardNum(BYTE value);
		int  getCalValueSize();
		bool addCalValue(int cardValue );
	private:
		int		_sumValue;
		ImageView*			 _jisuanBg;
		cocos2d::ui::Text*		_pValueAllSumText;
		int		_pCalValueArray[10];
		std::vector<ui::Text*>	_ValueTextVec;
	};
}
#endif // NN8CalculateNode_h__