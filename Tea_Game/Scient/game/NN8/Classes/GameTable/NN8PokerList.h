#ifndef __NN8_POKERLIST_H__
#define __NN8_POKERLIST_H__

#include "NN8MessageHead.h"
#include "NN8NodeList.h"
#include "NN8Poker.h"
#include "NN8GameTableUI.h"
#include "NN8CalculateNode.h"

namespace NN8
{
	class PokerList : public NodeList
	{
	protected:
		// override basic set & get
		virtual void setNodeValue(Node* node, BYTE value) override;
		virtual Node* getNodeFromValue(BYTE value) override;

		// construct & release
		PokerList();
		~PokerList();
	};
	///////////////////////////////////
	/////////////////////////////////////

	class NNList : public PokerList
	{
	public:
		static NNList* create(Vec2 offset = Vec2(25, 30));
		void setValueList(BYTE values[], BYTE count = HANDCARD_COUNT);
		virtual void setOffset(Vec2 offset) override;
		void resetAlign();
		virtual void clear() override;

		UserTanPai readBull();
		void showBull(UserTanPai bull,float pScle);
		void showFirstViewBull(UserTanPai bull);
		void clearBull();
        void playBullAudio(BYTE nn, bool isMan);
		void setTabelUI(GameTableUI* table);

		void showBullTips(UserTanPai bull);



	protected:
		// init
		bool initWithValue(Vec2 offset = Vec2(25, 30));
		virtual Vec2 getIndexPosition(int index) override;
		// touch
		virtual void onTouchMoved(Touch *touch, Event *unused_event) override;
		virtual void onTouchEnded(Touch* touch, Event* unused_event) override;
		

		// construct & release
		NNList();
		~NNList();

	protected:
		Sprite* _bull;
		GameTableUI* _table;
		CalculateNode*  _calculate;
	};

}

#endif