#include "NN8CalculateNode.h"
#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
namespace NN8
{
	static int ALL_IN_VALUE_MAX = 3;
	CalculateNode::CalculateNode()
	{

	}

	CalculateNode::~CalculateNode()
	{

	}


	bool CalculateNode::init()
	{
		if (!Sprite::init())
		{
			return false;
		}
		_sumValue = 0;
		_ValueTextVec.clear();
		memset(_pCalValueArray, 0, sizeof(_pCalValueArray));
		auto jisuanBgNode = CSLoader::createNode("NN8/table/JisuanBgNode.csb");
		jisuanBgNode->setAnchorPoint(Vec2(0.5,0.5));
		_jisuanBg= (ImageView*) jisuanBgNode->getChildByName("Image_jsnn");
		Size winSzie=Director::getInstance()->getWinSize();
		this->addChild(jisuanBgNode);

		int index = 1;
		for (int i=0; i<3; i++)
		{
			std::string toKeyStr = StringUtils::format("Text_cardId%d",index++);
			ui::Text* pText =dynamic_cast<ui::Text*>(_jisuanBg->getChildByName(toKeyStr));
			pText->setVisible(true);
			pText->setString("");
			_ValueTextVec.push_back(pText);
		}
		_pValueAllSumText = (Text*)_jisuanBg->getChildByName(StringUtils::format("Text_cardId%d",index++));
		_pValueAllSumText->setVisible(true);
		_pValueAllSumText->setString("");
		return true;
	}


	BYTE CalculateNode::getCardNum(BYTE value)
	{
		int num=(value&0x0F);
		if (num==14)
		{
			num=1;
		}
		if (num>10)
		{
			num=10;
		}
		return num;
	}


	int CalculateNode::getCalValueSize()
	{
		int num = 0;
		for (int i=0; i<ALL_IN_VALUE_MAX; i++)
		{
			if (_pCalValueArray[i]>0) num++;
		}
		return num;
	}


	bool CalculateNode::addCalValue(int cardValue)
	{
		bool isInArray = false;
		for (int i=0; i<ALL_IN_VALUE_MAX; i++)
		{
			if (_pCalValueArray[i] == cardValue)
			{
				isInArray = true;
				break;
			}
		}
		if(isInArray) return false;
		for (int i=0; i<ALL_IN_VALUE_MAX; i++)
		{
			if (_pCalValueArray[i] <= 0)
			{
				_pCalValueArray[i] = cardValue;
				return true;
			}
		}
		return false;
	}

	bool CalculateNode::calculateNN(BYTE cardValue,bool isAddCardValue)
	{
		int curValueSize = getCalValueSize();
		if(isAddCardValue && curValueSize>=ALL_IN_VALUE_MAX) return false;
		_pValueAllSumText->setString("");
		_sumValue = 0;
		if (isAddCardValue)
		{
			if(!addCalValue(cardValue)) return false;
			int sumValue = 0;
			for (int i=0; i<ALL_IN_VALUE_MAX; i++)
			{
				int toValue = _pCalValueArray[i];
				if (toValue>0)
				{
					toValue = getCardNum(toValue);
					sumValue += toValue;
					_ValueTextVec[i]->setString(StringUtils::format("%d", toValue));
				}
				else
				{
					_ValueTextVec[i]->setString("");
				}
			}
			if(getCalValueSize()>=ALL_IN_VALUE_MAX)
			{
				_sumValue = sumValue;
				_pValueAllSumText->setString(StringUtils::format("%d", sumValue));
			}
		}
		else
		{
			for (int i=0; i<3; i++)
			{
				int toValue = _pCalValueArray[i];
				if (cardValue == toValue)
				{
					_ValueTextVec[i]->setString("");
					_pCalValueArray[i] = 0;
					break;
				}
			}
		}

		return true;
	}
	
	void CalculateNode::clear()
	{
		memset(_pCalValueArray, 0, sizeof(_pCalValueArray));
		for (int i=0; i<(int)_ValueTextVec.size(); i++)
		{
			_ValueTextVec[i]->setString("");
		}
		_pValueAllSumText->setString("");
		_sumValue = 0;
	}


	bool CalculateNode::isHaveNiu()
	{
		if (_sumValue>0 && getCalValueSize()>=ALL_IN_VALUE_MAX)
		{
			return _sumValue%10 == 0;
		}
		return false;
	}


	bool CalculateNode::isCalculateFinish()
	{
		return _sumValue>0 && getCalValueSize()>=ALL_IN_VALUE_MAX;
	}

}


