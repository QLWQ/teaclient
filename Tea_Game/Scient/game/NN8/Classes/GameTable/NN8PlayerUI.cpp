                                                      
#include "HNUIExport.h"
#include "NN8GameUserMessageBox.h"
#include "NN8GameTableUI.h"
#include "NN8PlayerUi.h"
#include "HNLobbyExport.h"
#include <cstdio>


#define GAME_SRC_ROOT "NN8"

namespace NN8
{
	const char* Player_Normal_M = "NN8/table/men_head.png";
	const char* Player_Normal_W = "NN8/table/women_head.png";
	PlayerUi::PlayerUi()
	
	{
		_listenner=nullptr;
		memset(&_canvas, 0, sizeof(_canvas));
		memset(&_userInfo, 0, sizeof(UserInfoStruct));
		this->ignoreAnchorPointForPosition(true);
	}

	PlayerUi::~PlayerUi()
	{
	}

	PlayerUi* PlayerUi::create(UserInfoStruct &info, Node* pRootNode,int index)
	{
		PlayerUi* player = new PlayerUi();
		
		if (player->initWithValue(info, pRootNode, index))
		{
			player->autorelease();
			return player;
		}
		else
		{
			CC_SAFE_DELETE(player);
			return nullptr;
		}

	}

	bool PlayerUi::initWithValue(UserInfoStruct &info,Node* pRootNode,int index)
	{

		if (!Node::init() || info.dwUserID == INVALID_USER_ID||pRootNode==nullptr)
		{
			return false;
		}
		_PlayerIndex = index;
		_canvas.userNode = pRootNode;
		_canvas.userNode ->setVisible(true);
		_canvas.zhuang =(ImageView*) _canvas.userNode->getChildByName("userzhuang");
		_canvas.zhuang->setZOrder(10);
		_canvas.userHead =(ImageView*) _canvas.userNode->getChildByName("userHead");
		_canvas.userHead->setScale(0.9);
		_canvas.offLine =(ImageView*) _canvas.userNode->getChildByName("userOffLine");
		_canvas.userBetSore =(ImageView*) _canvas.userNode->getChildByName("batScore");
		_canvas.userBetSore->loadTexture("NN8/player/tag_1.png");
		_canvas.userBetSore->setZOrder(11);
		_canvas.userScore =(Text*) _canvas.userNode->getChildByName("Text_jifen");
		_canvas.userScores = (Text*)_canvas.userNode->getChildByName("Text_jf");
		_canvas.nikeName =(Text*) _canvas.userNode->getChildByName("Text_NikeName");
		_canvas.nikeName->setFontName("platform/common/RTWSYueRoudGoG0v1-Regular.ttf");
		_canvas.batText =(Text*) _canvas.userNode->getChildByName("Text_BetNode");
		_canvas.vip = (TextAtlas*)_canvas.userNode->getChildByName("AtlasLabel_vip");
		_canvas.money = (TextBMFont*)_canvas.userNode->getChildByName("AtlasLabel_money");
		//float Ysize = _canvas.money->getContentSize().width;
		//_canvas.money->setScale(70 / Ysize);
		_canvas.userHeadKuang = (ImageView*)_canvas.userNode->getChildByName("userHeadKuang");
		_canvas.winFont = (TextBMFont*)_canvas.userNode->getChildByName("BitmapFontLabel_win");
		_canvas.loseFont = (TextBMFont*)_canvas.userNode->getChildByName("BitmapFontLabel_lose");
		_canvas.winFont->setZOrder(200);
		_canvas.loseFont->setZOrder(200);
		_canvas.ready = (ImageView*)_canvas.userNode->getChildByName("ready");
		_canvas.tanpai = (ImageView*)_canvas.userNode->getChildByName("img_tanpai");
		_canvas.SignState = (ImageView*)_canvas.userNode->getChildByName("img_state");

		_canvas.framePart = (ParticleSystemQuad*)_canvas.userNode->getChildByName("Particle_frame");
		_canvas.starPart = (ParticleSystemQuad*)_canvas.userNode->getChildByName("Particle_star");

		_listenner =EventListenerTouchOneByOne::create();
		_listenner->setSwallowTouches(false);
		_listenner->onTouchBegan = CC_CALLBACK_2(PlayerUi::touchBegin,this);
		_listenner->onTouchCancelled = [&](Touch* t, Event* e){};
		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_listenner, _canvas.userHead);

		_iOwner = (ImageView*)_canvas.userNode->getChildByName("owner");

		// load user
		loadUser(info);
		return true;
	}

	void PlayerUi::loadUser(UserInfoStruct &info)
	{
		if (info.dwUserID != INVALID_USER_ID&&_canvas.userNode!=nullptr)
		{
			_userInfo = info;

			setOwner(info.dwUserID == HNPlatformConfig()->getMasterID());
		
			if (_canvas.userHead)
			{
				int FaceID = info.bLogoID == 0 ? 1 : info.bLogoID;
				std::string headUrl = StringUtils::format("platform/userData/res/head/icon_%d.png", FaceID);
				_canvas.userHead->loadTexture(headUrl);
				auto msgHead = GameUserHead::create(_canvas.userHead);
				msgHead->show();
				srand(time(nullptr) / (_PlayerIndex + 1));
				//int ranNumber = rand() % 1924;
				//if (ranNumber == 0) ranNumber = 1;
				std::string name = StringUtils::format("platform/common/Head/Head%d.jpg", FaceID);//isBoy ? Player_Normal_M : Player_Normal_W;
				bool isExist = FileUtils::getInstance()->isFileExist(name);
				if (isExist)
					msgHead->loadTexture(name);
				else
				{
					msgHead->loadTexture("platform/common/Head/Head1.jpg");
				}
				msgHead->loadTextureWithUrl(info.headUrl);
				msgHead->setLocalZOrder(-1);
				msgHead->setVIPHead("", info.iVipLevel);


			}

			if (_canvas.nikeName)
			{
				_canvas.nikeName->setString(GBKToUtf8( _userInfo.nickName));
				if (_userInfo.iVipLevel > 0)
				{
					_canvas.nikeName->setColor(Color3B(255,0,0));
				}
				else
				{
					_canvas.nikeName->setColor(Color3B(255, 255, 255));
				}
			}

			if (_canvas.userScore)
			{
				_canvas.userScore->setString("0");
			}

			if (_canvas.offLine)
			{
				_canvas.offLine->setVisible(false);

			}
			upDateUserCoin(info.i64Money);
			if (_canvas.userHeadKuang)
			{
				_canvas.userHeadKuang->setScale(1.0f);
				//std::string headKuangUrl = StringUtils::format("platform/userData/res/frame/frame_%d.png", info.bLogoKuangID);
// 				if (info.bLogoKuangID == 1 || info.bLogoKuangID == 0)
// 				{
// 					_canvas.userHeadKuang->setScale(0.9f);
// 				}
				//_canvas.userHeadKuang->loadTexture(headKuangUrl);
			}
			if (_canvas.vip)
			{
				//_canvas.vip->setString(StringUtils::format("%d",info.VipLevel));
			}
			if (_canvas.money)
			{
				
				//_canvas.money->setString("0");
			}
	
		}

	}



	void PlayerUi::setBetMoneyVisibel(bool isVisibel)
	{
			_canvas.userBetSore->setVisible(isVisibel);
	}

	void PlayerUi::showCallMark(int tag)
	{
		if (tag<=0)
		{
			_canvas.userBetSore->setVisible(false);
			return;
				
		}
		_canvas.userBetSore->setVisible(true);
		//_canvas.batText->setVisible(true);
		//_canvas.batText->setString(StringUtils::format("X%d",tag));

		//grab_multi_1.png
		//std::string path=GAME_SRC_ROOT"/player/nn_img_x%d.png";
		std::string path = GAME_SRC_ROOT"/player/tag_%d.png";
		if (tag == 1)
		{
			_canvas.userBetSore->setSize(Size(59, 39));
			_canvas.userBetSore->loadTexture(StringUtils::format(path.c_str(), tag));
		}

		else if (tag == 5)
		{
			_canvas.userBetSore->setSize(Size(59,39));
			_canvas.userBetSore->loadTexture(StringUtils::format(path.c_str(),tag));
		}
		else if(tag == 10)
		{
			_canvas.userBetSore->setSize(Size(83,40));
			_canvas.userBetSore->loadTexture(StringUtils::format(path.c_str(), tag));
		}
		else if (tag == 15)
		{
			_canvas.userBetSore->setSize(Size(82,39));
			_canvas.userBetSore->loadTexture(StringUtils::format(path.c_str(), tag));
		}
		else if (tag == 20)
		{
			_canvas.userBetSore->setSize(Size(90,40));
			_canvas.userBetSore->loadTexture(StringUtils::format(path.c_str(), tag));
		}

	}

	void PlayerUi::showOffLine()
	{
		_canvas.offLine->setVisible(true);
		
	}

	void PlayerUi::upDateUserMoney(int money)//更新输赢多少
	{
		if (_canvas.userScore == nullptr || money == 0)
		{
			return;
		}
		//_canvas.money->setString(StringUtils::format("%d",money));
		if (money > 0)
		{
			playPartStar();

			_canvas.winFont->setString("+" + StringUtils::format("%d", money));
			_canvas.winFont->setVisible(true);
			_canvas.winFont->setScale(0.2f);

			_canvas.winFont->runAction(Sequence::create(ScaleTo::create(1.5f,1.2f), DelayTime::create(2.0f),
				CallFunc::create([this](){
				_canvas.winFont->setVisible(false);
			}), nullptr));
		}
		else
		{
			_canvas.loseFont->setString(StringUtils::format("%d", money));
			_canvas.loseFont->setVisible(true);
			_canvas.loseFont->setScale(0.2f);

			_canvas.loseFont->runAction(Sequence::create(ScaleTo::create(1.5f, 1.2f), DelayTime::create(2.0f), 
				CallFunc::create([this](){
				_canvas.loseFont->setVisible(false);
			}), nullptr));
		}
	}

	UserInfoStruct PlayerUi::getUserInfo()
	{
		return _userInfo;
	}

	void PlayerUi::showMark(bool show)
	{
		_canvas.zhuang->setVisible(show);
	}



	void PlayerUi::setUIVisble(bool isvisble)
	{
		if (_canvas.userNode)
		{
			_canvas.userNode->setVisible(isvisble);
		}

	}



	void PlayerUi::removeListener()
	{
		if (_listenner)
		{
			Director::getInstance()->getEventDispatcher()->removeEventListener(_listenner);

		}
		
	}

	bool PlayerUi::touchBegin(Touch*touch, Event*tevent)
	{
		cocos2d::Vec2 toPos = _canvas.userHead->convertTouchToNodeSpace(touch);
		cocos2d::Rect box;
		box.origin = Vec2::ZERO;
		box.size =  _canvas.userHead->getContentSize();
		if (box.containsPoint(toPos))   // 是否牌范围内
		{		
		/*	JXShowUserCard* pUserData=JXShowUserCard::create();
			pUserData->updateUI(_userInfo.bBoy,_userInfo.bLogoID,_userInfo.dwUserID,_userInfo.iVipTime,_userInfo.nickName,_userInfo.headUrl,_userInfo.dwUserIP);
			Director::getInstance()->getRunningScene()->addChild(pUserData,1000);*/
			return true;
		}
	

		return false;
	}

	void PlayerUi::hideReady()
	{
		_canvas.ready->setVisible(false);
	}

	void PlayerUi::upDateUserCoin(LLONG money)
	{
		
		_canvas.money->setString(StringUtils::format("%lld", money) );
	}

	void PlayerUi::showSign(bool isSign)
	{
		_canvas.tanpai->setVisible(isSign);
	}

	void PlayerUi::playPartFrame()
	{
		_canvas.framePart->setScale(1.2f);
		_canvas.framePart->setVisible(true);
		_canvas.framePart->start();
		_canvas.framePart->runAction(Sequence::create(DelayTime::create(2.5f), CallFunc::create([this](){
			_canvas.framePart->setVisible(false);
		}), nullptr));
	}

	void PlayerUi::playPartStar()
	{
		_canvas.starPart->setVisible(true);
		_canvas.starPart->start();
		_canvas.starPart->runAction(Sequence::create(DelayTime::create(2.5f), CallFunc::create([this](){
			_canvas.starPart->setVisible(false);
		}), nullptr));
	}

	void PlayerUi::showGameState(std::string str, bool isShow)
	{
		if (isShow)
		{
			_canvas.SignState->setVisible(isShow);
			_canvas.SignState->loadTexture(str);
		}
		else
		{
			_canvas.SignState->setVisible(isShow);
		}
	}


	Size PlayerUi::getHeadSize()
	{
		auto img_Frame = dynamic_cast<ImageView*>(_canvas.userNode->getChildByName("userHead"));

		if (img_Frame)
		{
			return img_Frame->getContentSize();
		}

		return Size::ZERO;
	}

	void PlayerUi::setOwner(bool isOwner)
	{
		CCAssert(nullptr != _iOwner, "nullptr == _iOwner");
		if (nullptr == _iOwner) return;

		_iOwner->setVisible(isOwner);
	}

	bool PlayerUi::isClickBet()
	{
		bool isBetBtn = _canvas.userBetSore->isVisible();
		return isBetBtn;
	}


}
