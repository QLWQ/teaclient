#ifndef __NN8_CARDSUITE_H__
#define __NN8_CARDSUITE_H__

#include "NN8MessageHead.h"
#include "NN8Poker.h"


namespace NN8
{
	class CardSuite : public Layer
	{
	public:
		static CardSuite* create(float scale);

		void shuffle(float duration, float radius = 350.0f, float alpha = 30.0f, float offsetY = 350.0f);
		void dealCard(float duration, Vec2 target, std::function<void()> callback);
		TargetedAction* dealCard(float duration, Vec2 target);
		void recycle(float duration);
		void runActionPair(Node* node, Action* action);
		void setEmptyState(bool state);

	private:
		enum
		{
			TOTAL_CARDS = 52
		};

		typedef struct ActionPair
		{
			Node*	node;
			Action*	action;
		}ActionPair;

		bool init(float scale);
		void run(float delta);

		CardSuite(void);
		~CardSuite(void);

	private:
		Poker*	_suite[TOTAL_CARDS];
		bool		_empty;
		int			_totalCards;
		float		_scale;
		BYTE		_sent;
		std::list<ActionPair> _queue;

	};

}

#endif