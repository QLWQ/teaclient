#ifndef __NN8_NODELIST_H__
#define __NN8_NODELIST_H__

#include "HNNetExport.h"
#include "cocos2d.h"
using namespace cocos2d;

namespace NN8
{
	class NodeList : public Node
	{
	public:
		virtual bool init() override;

		// get
		virtual Rect getListBoundingBox();
		virtual Node* getNodeAt(int index);
		virtual BYTE getValueAt(int index);
		virtual BYTE getNodeListSize();
		virtual Vec2 getIndexPosition(int index);
		virtual int containsValue(BYTE value);

		// set
		virtual void updateValueList(const std::vector<BYTE> &valuelist);
		virtual void setValueList(const std::vector<BYTE> &valuelist);
		virtual void setValue(int index, BYTE value);
		virtual void setOffset(Vec2 offset);
		virtual void setIncreasedZorder(bool increased);
		virtual void setNodeVisible(int index, bool visible);

		virtual bool add(BYTE value);
		virtual void remove(int index);
		virtual void clear();

		// touch
		virtual void setTouchable(bool touchable);
		virtual void setTouchCallback(std::function<void(int index, BYTE value)> callback);
		virtual Node* getTouchedNode();
		virtual int getTouchedIndex();
		void   setCardScle(float scle);
	protected:
		// set
		virtual void setNodeValue(Node* node, BYTE value) = 0;

		// get
		virtual Node* getNodeFromValue(BYTE value) = 0;
		virtual int getIndexZorder(int index);

		// touch
		virtual bool onTouchBegan(Touch *touch, Event *unused_event);
		virtual void onTouchMoved(Touch *touch, Event *unused_event);
		virtual void onTouchEnded(Touch *touch, Event *unused_event);
		virtual bool touchCheck(Vec2 pos);

		// construct & release
		NodeList(void);
		~NodeList(void);

	protected:
		std::vector<Node*> _nodeList;
		std::vector<BYTE> _valueList;
		Vec2 _offset;
		float _scale;
		bool _increasedZorder;

        bool _touchEnabled;
		Node* _touchedNode;
		int _touchIndex;
		std::function<void(int index, BYTE value)> _touchCallback;
	};

}


#endif
