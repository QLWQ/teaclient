#ifndef __NN8ResultShowLayer_H__
#define __NN8ResultShowLayer_H__

#include"cocos2d.h"
#include "HNNetExport.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include "NN8MessageHead.h"
#include "NN8GameTableLogic.h"
using namespace ui;
using namespace std;
using namespace HN;

namespace NN8
{
	class ResultShow:public HNLayer
	{
	public:
		ResultShow();
		~ResultShow();
		static ResultShow * createResultShow(Size size);
		bool InitResultShow(Size size);
		void showResult(GameEndStruct* result,BYTE zhuangSeat,GameTableLogic* logic);
	private:
		void CloseCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
	private:
		ImageView* _listBg;
		ImageView* _player[PLAY_COUNT];
		ImageView* _bright;
	protected:
		UserInfoStruct _userInfo;
	};
}
#endif
