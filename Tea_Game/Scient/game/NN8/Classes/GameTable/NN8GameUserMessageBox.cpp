#include <string>
#include "NN8GameUserMessageBox.h"
#include "NN8PlayerUI.h"
#include "HNLobbyExport.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
using namespace ui;

namespace NN8
{
	GameUserMessageBox* GameUserMessageBox::createMessageBox(cocos2d::Node* parent, INT userID)
	{
		auto pRet = new GameUserMessageBox();
		if (pRet && pRet->init(parent, userID))
		{
			pRet->autorelease();
			return pRet;
		}
		delete pRet;
		pRet = nullptr;
		return pRet;
	}

	bool GameUserMessageBox::init(cocos2d::Node* parent, INT userID)
	{
		if ( !HNDialogBase::init(parent)) return false;

		Size winSize = Director::getInstance()->getWinSize();

		//创建一个点击事件
		auto MyListener = EventListenerTouchOneByOne::create();
		MyListener->setSwallowTouches(true);//阻止触摸向下传递
		MyListener->onTouchBegan = [&](Touch* touch, Event* event)
		{
			Size s = this->getContentSize();
			Rect rect = Rect(0, 0, s.width, s.height);        
			if (rect.containsPoint(touch->getLocation()))//判断触摸点是否在目标的范围内
			{
				//if (_userDataBoxRect.containsPoint(touch->getLocation())) return true;
				hide();
				return true;
			}
			else
				return false;
		};
		//把点击监听添加到监听序列中,因为只创建了一个监听,一个监听只能绑定到一个对象上,所以其他对象要使用监听则克隆
		_eventDispatcher->addEventListenerWithSceneGraphPriority(MyListener, this);

		//玩家信息结构体
		 const UserInfoStruct* userinfo = UserInfoModule()->findUser(userID);
		if (nullptr == userinfo)
		{
			return true;
		}

		auto infoNode = CSLoader::createNode("NN8/player/userinfo.csb");
		auto infoOutline = (Layout*)infoNode->getChildByName("userinfo_panel");
		//auto infoOutline = cocostudio::GUIReader::getInstance()->widgetFromJsonFile("NN8/player/userinfo.json");
		addChild(infoNode);
		_userDataBoxRect = infoOutline->getBoundingBox();
		infoOutline -> setAnchorPoint(Vec2(0, 0));

		auto avata		= dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(infoOutline, "avata_img"));
		auto gender		= dynamic_cast<Button*>(ui::Helper::seekWidgetByName(infoOutline, "gender_btn"));
		auto nickname	= dynamic_cast<Text*>(ui::Helper::seekWidgetByName(infoOutline, "nickname_label"));
		auto alias		= dynamic_cast<Text*>(ui::Helper::seekWidgetByName(infoOutline, "alias_label"));
		auto wallet		= dynamic_cast<Text*>(ui::Helper::seekWidgetByName(infoOutline, "wallet_label"));
		auto bank		= dynamic_cast<Text*>(ui::Helper::seekWidgetByName(infoOutline, "bank_label"));
		auto playcount	= dynamic_cast<Text*>(ui::Helper::seekWidgetByName(infoOutline, "playcount_label"));
		auto winrate	= dynamic_cast<Text*>(ui::Helper::seekWidgetByName(infoOutline, "winrate_label"));

		// avata
		
// 		Sprite *stencil = Sprite::create("headBg.png");
// 		stencil->setAnchorPoint(Vec2(0.5, 0.5));
// 		stencil->setScale(0.5f);
// 
// 
// 		//裁剪node  
// 		ClippingNode *clipper = ClippingNode::create();
// 		clipper->setAnchorPoint(Vec2(0.5, 0.5));
// 		clipper->setPosition(avata->getPosition());
// 		clipper->setAlphaThreshold(0.05);
// 		clipper->setStencil(stencil);
// 		avata->getParent()->addChild(clipper);
// 
// 		//加普通对象进裁剪node，让stencil去裁剪  
// 		Sprite* sp = Sprite::create(headPic);
// 		sp->setScale(stencil->getContentSize().width / sp->getContentSize().width*0.5);
// 		sp->setPosition(Vec2(0, 0));
// 		clipper->addChild(sp);

      
		// gender
		gender->setEnabled(false);
		gender->setBright(userinfo->bBoy);

		string str = "";
		// nickname
		str.append(GBKToUtf8(userinfo->nickName));
		nickname->setString(str);

		// alias
		str ="";
		str.append(userinfo->szName);
		alias->setString(str);

		// wallet
		str ="";
		char number[64];
		sprintf(number, "%lld", userinfo->i64Money);
		str.append(number);
		wallet->setString(str);


		// playcount
		str =  "";
		sprintf(number, "%d", (userinfo->uWinCount + userinfo->uLostCount + userinfo->uMidCount + userinfo->uCutCount));
		str.append(number);
		playcount->setString(str);

		// winrate
		str = "";
		if ((userinfo->uWinCount + userinfo->uLostCount + userinfo->uMidCount + userinfo->uCutCount) > 0)
		{
			sprintf(number, "%.2f%%", 100.0f * userinfo->uWinCount / (userinfo->uWinCount + userinfo->uLostCount + userinfo->uMidCount + userinfo->uCutCount));
			str.append(number);
		}
		winrate->setString(str);

		return true;
	}

}