#include "NN8ResultShowLayer.h"
#include "HNLobbyExport.h"
#include <cstdio>
#include "NN8GameTableUI.h"

#define Result_shoeLayer  "Games/NN8/showResult"
#define GAME_SRC_ROOT "NN8"



namespace NN8
{

 ResultShow::ResultShow()
 : _listBg(nullptr)
 {
	memset(_player,0,sizeof(_player));

 }
 ResultShow::~ResultShow()
 {


 }
 ResultShow * ResultShow::createResultShow(Size size)
  {
       auto  m_resu= new  ResultShow();
		if(m_resu->InitResultShow(size))
		{
			m_resu->autorelease();
			return m_resu;
		}
		else
		{
			CC_SAFE_DELETE(m_resu);
			return nullptr;
		}
  }
 bool ResultShow::InitResultShow(Size size)
 {
	 if (!HNLayer::init())
	 {
		 return false;
	 }
      Node*  madeNode = CSLoader::createNode(Result_shoeLayer"/currentResultNode.csb"); 
	  _listBg=(ImageView*)madeNode->getChildByName("Image_resultBg");
	  _bright=(ImageView*)madeNode->getChildByName("Image_bright");
	  madeNode->setAnchorPoint(Vec2(0.5f,0.5f));
	  madeNode->setPosition(size/2),
	  this->addChild(madeNode,10);


	  Button* sure = (Button*)_listBg->getChildByName("Button_sure");
	  sure->addTouchEventListener(CC_CALLBACK_2(ResultShow::CloseCallBack,this));
// 	  for(int i=0;i<PLAY_COUNT;i++)
// 	  {
// 		  _player[i] = (ImageView*)madeNode->getChildByName(StringUtils::format("Player%d", i + 1));
// 		  _player[i]->setVisible(false);
// 	  }
	  Button* btn_return = (Button*)_listBg->getChildByName("Button_return");
	  btn_return->addClickEventListener([&](Ref*) {
		  HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
		  RoomLogic()->close();
		  GamePlatform::returnPlatform(ROOMLIST);
	  });
	  return true;
 }




 void ResultShow::showResult(GameEndStruct* result,BYTE zhuangSeat,GameTableLogic* logic)
 {
	 UserInfoStruct myInfo=RoomLogic()->loginResult.pUserInfoStruct;
	 for (int i = 0; i < PLAY_COUNT; i++)
	 {
		 UserInfoStruct* userInfo=logic->getUserBySeatNo(i);
		 if (userInfo==nullptr)
		 {
			 continue;
		 }

//		 _player[i]->setVisible(true);
// 		 Text* userName=(Text*)_player[i]->getChildByName("Text_name");
// 		 userName->setString(GBKToUtf8(userInfo->nickName));
		// TextAtlas* winmoney=(TextAtlas*)_player[i]->getChildByName("AtlasLabel_money");
		 //winmoney->setString(StringUtils::format("%lld",result->iTurePoint[i]));
		 auto img_reslut = (ImageView*)_listBg->getChildByName("img_result");
		 auto img_win = (ImageView*)_listBg->getChildByName("Image_win");
		 auto img_lose = (ImageView*)_listBg->getChildByName("Image_lose");



	   if (myInfo.dwUserID==userInfo->dwUserID&&result->iTurePoint[i]<0&&_listBg)
	   {
		 _listBg->loadTexture(Result_shoeLayer"/lose_bg.png");
		 img_reslut->loadTexture(Result_shoeLayer"/you_lose.png");
		 img_win->setVisible(false);
		 img_lose->setVisible(true);
		 _bright->setVisible(false);
		 //_listBg->setPositionY(10);
	   }
	 }
 }

 void ResultShow::CloseCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
  {
	  if (touchtype == Widget::TouchEventType::ENDED)
	  {
		  //RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_GM_AGREE_GAME);
		
		  HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
		 GameTableUI* pGameTableNode= (GameTableUI*) this->getParent();
		 pGameTableNode->initParams();
		 pGameTableNode->ShowReadyBtn();
		 pGameTableNode->closeClock();
		 pGameTableNode->showTanPai(false);

		 if (pGameTableNode)
		 {
			 pGameTableNode->setGameBeginTipsVisible(true);
		 }
		 if (pGameTableNode->isGameFinish())
		 {
			 pGameTableNode->showFinalResult();
		 }
		  this->removeFromParentAndCleanup(true);
		  
	  }
  }
}