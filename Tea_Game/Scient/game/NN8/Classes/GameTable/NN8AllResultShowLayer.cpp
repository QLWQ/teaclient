#include "NN8AllResultShowLayer.h"
#include "HNUIExport.h"
#include "HNLobbyExport.h"
#include "HNLogicExport.h"
#include "HNOpenExport.h"


#define Show_csb  "Games/NN8/showResult/FinalResultNode.csb"
namespace NN8
{
	

	AllResultShow::AllResultShow()
		:_LayerWidget(nullptr)
	{
		memset(_player,0,sizeof(_player));
	}
	AllResultShow::~AllResultShow()
	{

	}
	AllResultShow* AllResultShow::create(Size size)
	{
		AllResultShow* show=new AllResultShow();
		if(show->IntitAllResultShow(size))
		{
			show->autorelease();
			return show;
		}
		else
		{
			CC_SAFE_DELETE(show);
			return nullptr;
		}
	}
	bool AllResultShow::IntitAllResultShow(Size size)
	{
		if (!HNLayer::init())
		{
			return false;
		}
		Size winSize=Director::getInstance()->getWinSize();
		auto node=CSLoader::createNode(Show_csb);
		_LayerWidget=(Layout*)node->getChildByName("Panel_layer");
		node->setPosition(winSize/2);
		this->addChild(node,10);

		for (int i=0;i<PLAY_COUNT;i++)
		{
			_player[i]=(ImageView*)_LayerWidget->getChildByName(StringUtils::format("Player_%d",i+1));
			
		}

		Button* close=(Button*)_LayerWidget->getChildByName("Button_sure");
		close->addTouchEventListener(CC_CALLBACK_2(AllResultShow::btClikeCallBack,this));

		Button* share=(Button*)_LayerWidget->getChildByName("Button_share");
		share->addTouchEventListener(CC_CALLBACK_2(AllResultShow::btClikeCallBack,this));

		return true;
	}



	void AllResultShow::showAllResult(GameEndInfoStruct* reuslt, GameTableLogic* logic)
	{
		if (reuslt==nullptr||logic==nullptr)
		{
			return;
		}
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* userInfo=logic->getUserByDeskStation(i);

			if (userInfo==nullptr)
			{
				continue;
			}
			_player[i]->setVisible(true);
			
			
			
			
// 			//通过精灵形状来录stencil  
// 			Sprite *stencil = Sprite::create("headBg1.png");
// 			stencil->setAnchorPoint(Vec2(0.5, 0.5));
// 			/*stencil->setScale(0.5f);*/
// 			 
// 			 
// 			//裁剪node  
// 			ClippingNode *clipper = ClippingNode::create();
// 			clipper->setAnchorPoint(Vec2(0.5, 0.5));
// 			clipper->setPosition(head->getPosition());
// 			clipper->setAlphaThreshold(0.05);
// 			clipper->setStencil(stencil);
// 			head->getParent()->addChild(clipper);
// 			 
// 			//加普通对象进裁剪node，让stencil去裁剪  
// 			Sprite* sp = Sprite::create(headPic);
// 			sp->setScale(stencil->getContentSize().width / sp->getContentSize().width);
// 			sp->setPosition(Vec2(0, 0));
// 			clipper->addChild(sp);

			Text* userName = (Text*)_player[i]->getChildByName("Text_nikeName");
			userName->setString(GBKToUtf8(userInfo->nickName));
			userName->setFontName("Games/NN8/showAllResult/RTWSYueRoudGoG0v1-Regular.ttf");
			

			/*Text* userId =(Text*) _player[i]->getChildByName("Text_UserId");
			userId->setString(StringUtils::format("%d",userInfo->dwUserID));*/

			Text* zongdefen = (Text*)_player[i]->getChildByName("Text_zj");
			zongdefen->setString(StringUtils::format("%d", reuslt->winTimes[i]));
			zongdefen->setFontName("Games/NN8/showAllResult/RTWSYueRoudGoG0v1-Regular.ttf");
			
			/*Text* tongSha =(Text*) _player[i]->getChildByName("Text_ts");
			tongSha->setString(StringUtils::format("%d",reuslt->winAllTimes[i]));*/

			/*Text* tongpei =(Text*) _player[i]->getChildByName("Text_tp");
			tongpei->setString(StringUtils::format("%d",reuslt->loseAllTimes[i]));*/

			Text* nnCount = (Text*)_player[i]->getChildByName("Text_nn");
			nnCount->setString(StringUtils::format("%d", reuslt->nnTimes[i]));
			nnCount->setFontName("Games/NN8/showAllResult/RTWSYueRoudGoG0v1-Regular.ttf");

			

			Text* winCount = (Text*)_player[i]->getChildByName("Text_wincount");
			winCount->setString(StringUtils::format("%d", reuslt->AllCount[i]));
			winCount->setFontName("Games/NN8/showAllResult/RTWSYueRoudGoG0v1-Regular.ttf");

			auto headImg = ImageView::create("NN8/table/men_head.png");
			//headImg->setPosition(Vec2(_player[i]->getPositionX() - 198, _player[i]->getPositionY() - 480));
			headImg->setPosition(Vec2(_player[i]->getPositionX() - 195, nnCount->getPositionY()));
			_player[i]->addChild(headImg);

			auto msgHead = GameUserHead::create(headImg);
			msgHead->show();
			msgHead->loadTexture(logic->getUserInfo(i)->bBoy ? "NN8/table/men_head.png" : "NN8/table/women_head.png");
			msgHead->loadTextureWithUrl(logic->getUserInfo(i)->headUrl);
			headImg->setScale(1.1);
			
			
				
		}

	}

	void AllResultShow::btClikeCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (touchtype == Widget::TouchEventType::ENDED)
		{
			Button* tempBt=(Button*)pSender;
			if (tempBt->getName()=="Button_sure")
			{
				this->removeFromParentAndCleanup(true);
				RoomLogic()->close();
				GamePlatform::createPlatform();
			}
			else if(tempBt->getName()=="Button_share")
			{
				/*auto pCallFunc = [&]()
				{
					const std::string CUTIMAGENAME = "captureScreen.jpg";
					std::string picFile = FileUtils::getInstance()->getWritablePath()+CUTIMAGENAME;
					if (FileUtils::getInstance()->isFileExist(picFile))
					{
						FileUtils::getInstance()->removeFile(picFile);
					}
					auto pCutScreen = [](bool isSuccess, const std::string& path){
						if (isSuccess)
						{
							
						}
					};				
					utils::captureScreen(pCutScreen, CUTIMAGENAME);
					
				};
				this->runAction(Sequence::create(DelayTime::create(0.1f), CallFunc::create(pCallFunc), nullptr));*/
				//UMengSocial::getInstance()->inviteFriends("牛牛", HNPlatformConfig()->getVipRoomNum(), true);
				UMengSocial::getInstance()->doShare("", "", "");
			}

		}
	}
}
