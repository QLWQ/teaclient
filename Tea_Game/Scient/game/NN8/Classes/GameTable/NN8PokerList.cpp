#include "NN8PokerList.h"


#define GAME_SRC_ROOT "NN8"
namespace NN8
{
	PokerList::PokerList()
	{
	}


	PokerList::~PokerList()
	{
	}

	void PokerList::setNodeValue(Node* node, BYTE value)
	{
		auto poker = (Poker*)node;
		poker->setValue(value);
	}

	Node* PokerList::getNodeFromValue(BYTE value)
	{
		return dynamic_cast<Node*>(Poker::create(value));
	}

	///////////////////////////////////
	/////////////////////////////////////

	NNList::NNList():
		_bull(nullptr)
	{
	}

	NNList::~NNList()
	{
		clear();
	}

	NNList* NNList::create(Vec2 offset)
	{
		NNList* nnlist = new NNList();
		if (nnlist && nnlist->initWithValue(offset))
		{
			nnlist->autorelease();
			return nnlist;
		}
		CC_SAFE_DELETE(nnlist);
		return nullptr;
	}

	bool NNList::initWithValue(Vec2 offset)
	{
		if (!PokerList::init())
		{
			return false;
		}
		setOffset(offset);

		return true;
	}

	void NNList::setOffset(Vec2 offset)
	{
		bool zorder = offset.x > 0 ? true : false;
		setIncreasedZorder(zorder);
		PokerList::setOffset(offset);
	}

	void NNList::resetAlign()
	{
		setOffset(_offset);
		clearBull();
	}

	void NNList::clear()
	{
		PokerList::clear();
		clearBull();
	}

	Vec2 NNList::getIndexPosition(int index)
	{
		Vec2 off = Vec2(_offset.x, 0);
		return off * index;
	}

	void NNList::setValueList(BYTE values[], BYTE count)
	{
		std::vector<BYTE> valuelist(values, values + count);
		PokerList::setValueList(valuelist);
		clearBull();
	}

	void NNList::clearBull()
	{
		if (_bull != nullptr)
		{
			_bull->removeFromParent();
			_bull = nullptr;
		}
	}

	UserTanPai NNList::readBull()
	{
		UserTanPai bull;
		int undercount = 0;
		int upcount = 0;
		for (int i = 0; i < _nodeList.size() && _nodeList.size() == HANDCARD_COUNT; i++)
		{
			if (_nodeList.at(i)->getPosition() == getIndexPosition(i))
			{
				undercount++;
				if (undercount <= 3)
				{
					bull.byUnderCard[undercount - 1] = _valueList.at(i);
				}
				else
				{
					undercount = 5;
					break;
				}
			}
			else
			{
				upcount++;
				if (upcount <= 2)
				{
					bull.byUpCard[upcount - 1] = _valueList.at(i);
				}
				else
				{
					undercount = 5;
					break;
				}				
			}
		}
		if (undercount != 3)
			undercount = HANDCARD_COUNT;

		bull.byUnderCount = undercount;
		return bull;
	}

	void NNList::showBull(UserTanPai bull,float pScle)
	{
		BYTE nn=bull.iShape;
		Vec2 nnpos = getIndexPosition(2);

		// bull texture
		SpriteFrameCache* cache = SpriteFrameCache::getInstance();
		cache->addSpriteFramesWithFile(GAME_SRC_ROOT"/nn/nn.plist");
		char filename[20];
		sprintf(filename, "nn%02d.png", nn);

		if (_bull != nullptr)
		{
			_bull->removeFromParent();
			_bull = nullptr;
		}
		_bull = Sprite::createWithSpriteFrameName(filename);
		_bull->setScale(pScle);
		_bull->setPosition(nnpos+ Vec2(0, -10));
		this->addChild(_bull, 10);

	}

	void NNList::showFirstViewBull(UserTanPai bull)
	{
		// bull texture
		Vec2 nnpos = getIndexPosition(2);
		SpriteFrameCache* cache = SpriteFrameCache::getInstance();
		cache->addSpriteFramesWithFile(GAME_SRC_ROOT"/nn/nn.plist");
		char filename[20];
		BYTE nn=bull.iShape;
		sprintf(filename, "nn%02d.png", nn);

		if (_bull != nullptr)
		{
			_bull->removeFromParent();
			_bull = nullptr;
		}
		_bull = Sprite::createWithSpriteFrameName(filename);
		_bull->setPosition(nnpos + Vec2(0, -10));
		this->addChild(_bull, 10);

	}

	void NNList::onTouchMoved(Touch *touch, Event *unused_event)
	{
	}

	void NNList::onTouchEnded(Touch* touch, Event* unused_event)
	{
		if (_touchedNode != nullptr && _touchIndex >= 0 && _touchIndex < _valueList.size())
		{
			Vec2 indexpos = getIndexPosition(_touchIndex);
			Vec2 nodepos = _touchedNode->getPosition();
			BYTE cardVelue=_valueList.at(_touchIndex);
			HNAudioEngine::getInstance()->playEffect(AUDIO_CLICK);
			if (int(nodepos.x) != int(indexpos.x) || int(nodepos.y) != int(indexpos.y))
			{
				_touchedNode->setPosition(indexpos);
				if (_table)
				{
					_table->calculateNN(cardVelue, false);
				}
				
			}
			else
			{
			
				_touchedNode->setPosition(indexpos + Vec2(0, _offset.y	- 20));
				if (!_table || !_table->calculateNN(cardVelue, true))
				{
					return;
				}
			
			}
			

		
		}
	}

    void NNList::playBullAudio(BYTE nn, bool isMan)
	{
		if (nn >= 0 && nn <= 19)
		{
			string audioStr = "NN8/audio/nn/";
			audioStr += "niu_";

			char s[10];
			sprintf(s, "%d", nn);
			audioStr += s;

			audioStr += ".mp3";
			HNAudioEngine::getInstance()->playEffect(audioStr.c_str());
		}
	}

	void NNList::setTabelUI(GameTableUI* table)
	{
		_table=table;
	}

	void NNList::showBullTips(UserTanPai bull)
	{
		if (bull.iShape<0||bull.iShape>10)
		{
			return;
		}
		if (bull.iShape==0)
		{
			Vec2 nnpos = getIndexPosition(2);
			SpriteFrameCache* cache = SpriteFrameCache::getInstance();
			cache->addSpriteFramesWithFile(GAME_SRC_ROOT"/nn/nn.plist");
			char filename[20];
			BYTE nn=bull.iShape;
			sprintf(filename, "nn%02d.png", nn);


			Sprite* bullSprite = Sprite::createWithSpriteFrameName(filename);
			bullSprite->setPosition(nnpos + Vec2(0, -30));
			this->addChild(bullSprite, 10);
			Sequence* se=Sequence::create(FadeIn::create(0.5),FadeOut::create(0.5),RemoveSelf::create(true),nullptr);
			bullSprite->runAction(se);
			return;
		}

		if (_valueList.size()!=_nodeList.size()&&_nodeList.size()==HANDCARD_COUNT)
		{
			return;
		}
		for (int i = 0; i < _nodeList.size(); i++)
		{
			for (int underindex = 0; underindex < 3 ; underindex++)
			{
				if (_valueList.at(i) == bull.byUnderCard[underindex])
				{
					
					Vec2 underpos = getIndexPosition(i);
					_nodeList.at(i)->setPosition(underpos+ Vec2(0, _offset.y));
				
				}
			}
		}

		
	}

}

