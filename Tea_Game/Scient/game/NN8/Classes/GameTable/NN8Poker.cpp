#include "NN8Poker.h"

namespace NN8
{

	Poker::Poker()
	{

		_ignoreAnchorPointForPosition = false;
	}

	Poker::~Poker()
	{
	}

	Poker* Poker::create(BYTE cardValue)
	{
		Poker* card = new Poker();
		if (card && card->initWithCardValue(cardValue))
		{
			card->setScale(0.8f);
			card->autorelease();
			return card;
		}
		CC_SAFE_DELETE(card);
		return nullptr;
	}

	bool Poker::initWithCardValue(BYTE cardValue)
	{
		if (!Sprite::init())
		{
			return false;
		}

		// out of range
		if (!inRange(cardValue))
		{
			return false;
		}


		_Value = cardValue;
		return this->initWithSpriteFrameName(getCardTextureFileName(cardValue));
	}

	void Poker::setValue(BYTE cardValue)
	{
		// out of range
		if (!inRange(cardValue))
		{
			return;
		}

		this->setSpriteFrame(Poker::getCardTextureFileName(cardValue));
		_Value = cardValue;
	}

	BYTE Poker::getValue()
	{
		return _Value;
	}

	BYTE Poker::getCardNum()
	{
		return (_Value&UG_VALUE_MASK);
	}

	string Poker::getCardTextureFileName(BYTE cardValue)
	{
		char filename[10];
		sprintf(filename, "0x%02X.png", cardValue);

		return filename;
	}

	void Poker::setGray(bool isGray)
	{
		// true - set to gray
		if (isGray == true)
		{
			Sprite* sprite_chess = Sprite::createWithTexture(this->getTexture());
			sprite_chess->setPosition(sprite_chess->getContentSize().width / 2, sprite_chess->getContentSize().height / 2);

			RenderTexture *render = RenderTexture::create(sprite_chess->getContentSize().width, sprite_chess->getContentSize().height, Texture2D::PixelFormat::RGBA8888);
			render->beginWithClear(0.0f, 0.0f, 0.0f, 0.0f);
			sprite_chess->visit();
			render->end();
			Director::getInstance()->getRenderer()->render();

			Image *finalImage = render->newImage();

			unsigned char *pData = finalImage->getData();

			int iIndex = 0;

			for (int i = 0; i < finalImage->getHeight(); i++)
			{
				for (int j = 0; j < finalImage->getWidth(); j++)
				{
					// gray
					int iBPos = iIndex;

					unsigned int iB = pData[iIndex];
					iIndex++;

					unsigned int iG = pData[iIndex];
					iIndex++;

					unsigned int iR = pData[iIndex];
					iIndex++;
					iIndex++;

					unsigned int iGray = 0.3 * iR + 0.6 * iG + 0.1 * iB;

					pData[iBPos] = pData[iBPos + 1] = pData[iBPos + 2] = (unsigned char)iGray;
				}
			}

			Texture2D *texture = new Texture2D;
			texture->initWithImage(finalImage);
			this->setTexture(texture);

			delete finalImage;
			texture->release();
		}
		// false - set to colored
		else
		{
			this->setSpriteFrame(Poker::getCardTextureFileName(_Value));
		}
	}

	bool Poker::inRange(BYTE value)
	{
		if (!(
			(value >= 0x00 && value <= 0x0D) ||
			(value >= 0x11 && value <= 0x1D) ||
			(value >= 0x21 && value <= 0x2D) ||
			(value >= 0x31 && value <= 0x3D) ||
			(value >= 0x4E && value <= 0x4F))
			)
			return false;
		else
			return true;
	}

}