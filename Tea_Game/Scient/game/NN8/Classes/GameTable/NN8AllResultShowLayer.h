#ifndef __NN8AllResultShowLayer_H__
#define __NN8AllResultShowLayer_H__

#include"cocos2d.h"
#include "HNNetExport.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include "NN8MessageHead.h"
#include "NN8GameTableLogic.h"


using namespace ui;
using namespace std;
using namespace HN;
namespace NN8
{
	class  AllResultShow:public HNLayer
	{
	public:
		AllResultShow();
		~AllResultShow();
		static AllResultShow*create(Size size);
		void showAllResult(GameEndInfoStruct* reuslt, GameTableLogic* logic);
	private:
		bool IntitAllResultShow(Size size);
		void btClikeCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
	private:
		ImageView*    _player[PLAY_COUNT];
		Layout *      _LayerWidget;
		
	};
}
#endif

