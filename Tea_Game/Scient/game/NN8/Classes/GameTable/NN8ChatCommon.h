#ifndef NN8ChatCommon_h__
#define NN8ChatCommon_h__

#include "NN8MessageHead.h"


namespace NN8
{
	class NN8ChatCommon:public HNLayer
	{
	public:
		NN8ChatCommon();
		~NN8ChatCommon();
		CREATE_FUNC(NN8ChatCommon);
		virtual bool init();
		static void compareMsg(std::string msg,bool isMen);
		void openMe();
		void closeMe();
	private:
		void onSelectTipsCallBack(Ref* pSender, Widget::TouchEventType type);
		virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);

	public:
		typedef std::function<void (const std::string& text)> SendSpeakCallBack;
		SendSpeakCallBack onSpeakCallBack;

	private:
		int     _oommonChatlimitTime;
		cocos2d::EventListener* _pEventListenr;
		ImageView* chatImag;
	};


}


#endif