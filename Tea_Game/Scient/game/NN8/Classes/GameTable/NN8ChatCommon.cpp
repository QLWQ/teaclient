#include "NN8ChatCommon.h"

namespace NN8
{
	static std::string chat_draft = "";
	static time_t preSpreakTime = 0;
	NN8ChatCommon::NN8ChatCommon():
		_oommonChatlimitTime(3),
		onSpeakCallBack(nullptr),
		_pEventListenr(nullptr),
		chatImag(nullptr)
	{
	}

	NN8ChatCommon::~NN8ChatCommon()
	{
	}

	bool NN8ChatCommon::init()
	{
		if (!HNLayer::init())
		{
			return false;
		}
		Size winSize = Director::getInstance()->getWinSize();
		chatImag = ImageView::create("HNPlatform/GameChat/ChatRes/chat_background1.png");
		chatImag->setPosition(Vec2(winSize/2));
		chatImag->setScale(0.8f);
		this->addChild(chatImag);

		for (int i=0; i<9;i++)
		{

		}



		return true;
	}

	void NN8ChatCommon::compareMsg(std::string msg,bool isMen)
	{
		log("com______Mes%s",msg.c_str());
		for (int i = 0; i < 9; i++)
		{
			std::string tips=StringUtils::format("Word_Common_GameChatDlgLayer_tips%d",i+1);
			std::string str = "";
			log("com______myMes%s",str.c_str());

			bool isFound = false;
			int size1 = msg.size();
			int size2 = str.size();
			if (abs(size1-size2)>4)
			{
				continue;
			}

			if (msg.size()>str.size())
			{
				std::size_t found = msg.find(str);
				if (found!=std::string::npos)
				{
					isFound =true;
				}
			}
			else
			{
				std::size_t found = str.find(msg);
				if (found!=std::string::npos)
				{
					isFound =true;
				}
			}

			if(isFound&&sizeof(msg)==sizeof(str))
			{	
				std::string filename;
				if (isMen)
				{
					filename=StringUtils::format("platform/sound/music/man/v%d.mp3",i+1);	
				}
				else
				{
					filename=StringUtils::format("platform/sound/music/woman/v%d.mp3",i+1);	

				}

				HNAudioEngine::getInstance()->playEffect(filename.c_str());
				return;
			}
		}
	}

	void NN8ChatCommon::openMe()
	{
		if (_pEventListenr)
		{
			Director::getInstance()->getEventDispatcher()->removeEventListener(_pEventListenr);
			_pEventListenr = nullptr;
		}
		auto MyListener = EventListenerTouchOneByOne::create();
		MyListener->setSwallowTouches(true);//��ֹ�������´���
		MyListener->onTouchBegan = CC_CALLBACK_2(NN8ChatCommon::onTouchBegan,this);
		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(MyListener, this);

		_pEventListenr = MyListener;
		this->setVisible(true);
	}

	void NN8ChatCommon::closeMe()
	{
		if (_pEventListenr)
		{
			Director::getInstance()->getEventDispatcher()->removeEventListener(_pEventListenr);
			_pEventListenr = nullptr;
		}

		this->setVisible(false);
	}

	void NN8ChatCommon::onSelectTipsCallBack(Ref* pSender, Widget::TouchEventType type)
	{
		if (type != Widget::TouchEventType::ENDED)
		{
			return;
		}
		auto btn = (Button*)pSender;

		time_t curTime = time(nullptr);
		std::string words =btn->getTitleText();
		if (curTime-preSpreakTime<=_oommonChatlimitTime)
		{
			
			return;
		}
		if (onSpeakCallBack)
		{
			onSpeakCallBack(words);
		}
		closeMe();
		preSpreakTime = curTime;

	}

	bool NN8ChatCommon::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
	{
		Vec2 point = touch->getLocation();
		Rect gameDialogRect =Rect(640,320,chatImag->getContentSize().width,chatImag->getContentSize().height);
		Size s = this->getContentSize();
		Rect rect = Rect(0, 0, s.width, s.height);        
		if (rect.containsPoint(touch->getLocation()))
		{
			if (gameDialogRect.containsPoint(touch->getLocation())) return true;
			closeMe();
			return true;
		}


		return true;
	}

}
