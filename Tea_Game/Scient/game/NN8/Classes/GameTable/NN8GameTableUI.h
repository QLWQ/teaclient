#ifndef __NN8_Game_Table_UI_H__
#define __NN8_Game_Table_UI_H__

#include "cocos2d.h"
#include "NN8GameTableUICallback.h"
#include "NN8GameUserMessageBox.h"
#include "HNNetExport.h"
#include "HNLobbyExport.h"
#include "NN8ResultShowLayer.h"
#include "NN8AllResultShowLayer.h"
#include "NN8ChatCommon.h"
#include "NN8CalculateNode.h"

class VipRoomController;
namespace NN8
{
	/*
	 * game ui unit.
	 */

	class GameTableLogic;
	class PlayerUi;
	class CardSuite;
	class NNList;
	enum
	{
		CLOCK_XIAZHU = 0,
		CLOCK_TANPAI = 1,
		CLOCK_QIANGZHUANG = 2,
		CLOCK_RESULT = 3,
		CLOCK_NONE = 4

	};

	static const int	Max_Zorder = 100;
	static const int	SET_ZORDER = 301;

	class GameTableUI : public HN::HNGameUIBase, public GameTableUICallback, public cocos2d::ui::EditBoxDelegate
	{
	public:
		GameTableUI();
		~GameTableUI();

		static HNGameUIBase* create(INT deskId, bool autoCreate);
		virtual bool init(INT deskId, bool autoCreate);

		virtual void initParams();
		virtual void loadUser(BYTE seat, UserInfoStruct &info);
		virtual void showVipTip(BYTE seat, UserInfoStruct &info);
		virtual void showUserUp(BYTE seatId);

		// allocate dealer
		virtual void render_ASS_CALL_SCORE(bool show) override;
		// note
		virtual void callNote(BeginUpgradeStruct* note);
		virtual void sendCard(SendAllCardStruct send);
		virtual void updateHandcard(BYTE cards[], BYTE seat,bool bOpenCard);
		
		virtual void showBull(UserTanPai bull);
		virtual void deal_ASS_CONTINUE_END(GameEndStruct* end) override;
		virtual void setDissloveBtState() override;
		virtual void setGamecount(int counts,int maxCount) override;
		virtual void setUserDefine(std::vector<int>  define)override;
		virtual void showWechat(bool visible) override;
		virtual void showBatBtVisble(bool isVisble)override;

		virtual void showqiangZhuangTips(BYTE qzStates[])override;
		virtual void hideqiangZhuangTips()override;
		virtual void playQiangzhuangAciton(BYTE qzResult[],BYTE seat,bool visble)override;
		virtual void setZhuangSeat(BYTE seat)override;

		virtual void removeUser(BYTE seatNo)override;



		virtual void setGMButtonVisibel(bool isVisibel)override;

		virtual void showContolSuccese(bool isSuccese)override;

		/////////////////////////////////////

		// update note
		virtual void updateNote(BYTE seatId, LLONG money);
		virtual void playNoteAudio(BYTE seatId, BYTE audioType);
		// show max card mark
		virtual void showDealerMark(BYTE seat, bool visible = false);
		// enable/disable start button
		virtual void setStartBtnVisible(bool visible);
		virtual void editBoxReturn(ui::EditBox* editBox)  {};
		virtual void ShowAllResult(GameEndInfoStruct & cal, bool sh);
		virtual void setGameBeginTipsVisible(bool isvisible);
		virtual void changTipshow(int tag);
		virtual void userRedayShow( int seat, bool isReday) ;
		virtual void setAllUserRedayUnVisble();
		virtual void showOffLine(BYTE seat)override;
		virtual void showCallNode()override;
		virtual void sendCardState(BYTE viewSeat,BYTE cards[])override;
		virtual void reConnect()override;
		virtual void setUserScore(int score[PLAY_COUNT], int Score)override;
		virtual void showGameDeskNotFound()override;
		virtual void updateHandCardWithAction(BYTE cards[], BYTE seat,UserTanPai bull,float delayTime) override;
		virtual void showTanPai(bool isVisible);
		//选中框动画
		virtual void showKuangBtnAction(BYTE seat, bool isPlay);
		virtual void jinYongReturn(bool isPlay);//禁用退出按钮
		virtual void showTanPaiSign(int seat, bool isVisable);//显示摊牌标识
		virtual void playGameStartAni();
		virtual void showSignState(int seat,int state);//游戏状态提示 
		virtual void hideSign(int seat);
		virtual void closeClock();

		virtual void setDeskInfo() override;//创建房间相关
		void showMaster(BYTE seatNo, bool bShow) override;
		//发送战绩数据
		virtual void sendRecordData(S_C_GameRecordResult* data);
	public: 

		void setBeiBtVisible(bool isvisble);		
		void hideBankerTipsAndBetMoneyTips();
		bool isGameFinish();
		void showFinalResult();
		bool seatNoIsOk(BYTE seatNo);

		void showJisuanNode(bool isvible);
		bool calculateNN(BYTE cardValue, bool isAddCardValue);
		void removeSetLayer();

	private:
		void startClickCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void gmBtClickCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);

		void requestDealerCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void buxiangCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void showhandCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void tableMenuCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void noteMenuCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void clickWebchatEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		virtual void openChatDialog();
	    virtual void onSocketMessage(UINT MainID, UINT AssistantID,const rapidjson::Document& doc);
		void initDissolveRoomOpervation();
		void clickShouEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);
		void clickZhanEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);		
		void checkInviAndReadyPos();

		void chatBtnClickCallBack(Ref* ref, Widget::TouchEventType type);//聊天语音回调
 		virtual void onChatTextMsg(BYTE seatNo, CHAR msg[]);											// 处理文本聊天消息
 		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime);	                        // 处理语音聊天消息											
		void gameChatLayer();                                                                           // 聊天界面

		void clickSettingCallBack(cocos2d::Ref* pSender);
	public:
		void ShowReadyBtn();
	private:
		void dengBtPlayAction(float t);//抢庄动画
		void resetDengTipsData();
		void upDateUserMoney();
		inline int GetCardNum(BYTE iCard) { return (iCard & 0x0F) + 1; }
		int GetPoint(int Card);
		void callScoreClock(float dt);//倒计时回调函数
		void showRunTiom(int time,int nGameState);//倒计时
		void leaveDesk(float dt);//退出房间倒计时
		
	private:
		typedef struct TtableUi
		{


			ImageView*  tableBg;
			Text*		fangjianHao;
			Text*		jushu;
			Text*		moshi;
			Text*		yafen;
			Text*		kejin;
			Text*		renshu;
			Button*		menu;
			Button*		setting;
			Button*		exitBt;
			Button*		invite;
			Button*		reday;
			Button*		qiangzhuangBt;
			Button*		buqiangBt;
			Button*		dealer;				// dealer anchor
			ImageView*	gameBegintips;
			ImageView*	gameTips;
			ImageView*	noticBG;
			Button*     btnSuperSet;
			Button*		btnreturn;
			Button*     btnTanPai;
			Button*		kuangBtn[PLAY_COUNT];
		
			Button*		beiBt[5];
			Button*     seats[PLAY_COUNT];				// seats button
			ImageView*	hand[PLAY_COUNT];			// handcard anchor
			ImageView*	niuTips[PLAY_COUNT];			// handcard anchor
			ImageView*	qzDeng[PLAY_COUNT];			// handcard anchor
			ImageView*	zhuangjia[PLAY_COUNT];
			ImageView*	qiangzhuangTip[PLAY_COUNT];	//	抢庄提示
			ImageView*  clock;//闹钟
			TextAtlas*  second;//秒数

			Button* voice;//语音系统按钮
			Button* set;//设置按钮
			Button* chat;//表情聊天


		}TtableUi;

		
	private:
		// ui
		TtableUi		_canvas;
		CardSuite*		_suite;
		PlayerUi*		_players[PLAY_COUNT];
		NNList*			_handcard[PLAY_COUNT];
		CalculateNode*  _calculate;
		

		GameChatLayer* _chatLayer = nullptr;

		VipRoomController* _roomController;

	
		// data
		GameTableLogic* _tableLogic;
		bool		_startflag;
		bool		_customize;
		bool		_pIsGudingYaFen;
		int         _deskNo;
		int			_zhuangSeat;
		ImageView*  _tableBg;
		NN8ChatCommon*       _chatCommon;

	
		std::vector<Button*> _vecBtList;
		int					 _playDengActCount;

		int					_userScore[PLAY_COUNT];

		int					_maxGameCount;
		
		bool				_gameFinish;

		BYTE				_cardMySeat[5];
	
		int					_clockTime;
		int					_gameState;
		BYTE				_state[PLAY_COUNT];
		LLONG				_resultMoney[5];

		int					_calCount;
		bool				_allRoundsEnd = false;
		Sprite*         _BottonScore;
		Label*            _DiFen ;
		int             _MyselfScore = 0;
		int             _dBottonScore = 0;
		string           _OldName = "";
	};
}


#endif
