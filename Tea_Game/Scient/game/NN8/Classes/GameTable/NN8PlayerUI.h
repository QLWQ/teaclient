#ifndef __NN8GAME_PLAYER_H__
#define __NN8GAME_PLAYER_H__

#include "cocos2d.h"
#include "HNNetExport.h"
#include "platform/CCCommon.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include "NN8MessageHead.h"


using namespace ui;
using namespace std;
using namespace HN;

#define GAME_SRC_ROOT "NN8"

namespace NN8
{
	class PlayerUi : public Node
	{
	public:
		static PlayerUi* create(UserInfoStruct &info,Node* pRootNode,int index);		
		void showMark(bool show);
		void loadUser(UserInfoStruct &info);
		void setBetMoneyVisibel(bool isVisibel);
		void showCallMark(int tag);
		void showOffLine();
		void upDateUserMoney(int money);
		void upDateUserCoin(LLONG money);
		UserInfoStruct getUserInfo();
		cocos2d::Node* getHeadNode()		{ return _canvas.userNode; }
		int getUserId()		{ return _userInfo.dwUserID; }
		void setUIVisble(bool isvisble);
	    void removeListener();
		void hideReady();
		void showSign(bool isSign);
		void playPartFrame();
		void playPartStar();
		void showGameState(std::string str,bool isShow);

		Size getHeadSize();
		// 设置玩家房主状态                                                     
		void setOwner(bool isOwner);
		//玩家是否点击下注按钮
		bool isClickBet();
	protected:
		bool initWithValue(UserInfoStruct &info,Node* pRootNode,int index);
		bool touchBegin(Touch*touch, Event*tevent);
		
		PlayerUi();
		~PlayerUi();
	protected:
		typedef struct TPlayerUi
		{
			Node*	    userNode;
			ImageView*	offLine;
			ImageView*	zhuang;
			ImageView*	userHead;
			ImageView*	userBetSore;
			Text*	    nikeName;
			Text*       userScores;
			Text*       userScore;
			Text*       batText;
			TextAtlas*  vip;
			TextBMFont*  money;
			ImageView*  userHeadKuang;
			ImageView*  ready;
			TextBMFont* winFont;
			TextBMFont* loseFont;
			ImageView* tanpai;
			ParticleSystemQuad* framePart;
			ParticleSystemQuad* starPart;
			ImageView* SignState;
		}TPlayerUi;
		ImageView* _iOwner;
	protected:
		TPlayerUi _canvas;
		UserInfoStruct _userInfo;
	private:
		EventListenerTouchOneByOne* _listenner;
		int               _PlayerIndex = -1;
	};

}


#endif // !_GAME_PLAYER_
