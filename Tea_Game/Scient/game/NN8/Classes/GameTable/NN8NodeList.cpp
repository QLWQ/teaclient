#include "NN8NodeList.h"


namespace NN8
{
	bool NodeList::init()
	{
		if (!Node::init())
		{
			return false;
		}

		// touch listener
		auto listener = EventListenerTouchOneByOne::create();
		listener->onTouchBegan = CC_CALLBACK_2(NodeList::onTouchBegan, this);
		listener->onTouchMoved = CC_CALLBACK_2(NodeList::onTouchMoved, this);
		listener->onTouchEnded = CC_CALLBACK_2(NodeList::onTouchEnded, this);
		listener->setSwallowTouches(true);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

		return true;
	}


	NodeList::NodeList(void) :
		_offset(Vec2(0, 0)),
		_increasedZorder(true),
        _touchEnabled(false),
		_touchedNode(nullptr),
		_touchIndex(-1),
		_touchCallback(nullptr),
		_scale(0.8f)
	{
	}

	NodeList::~NodeList(void)
	{
	}

	Rect NodeList::getListBoundingBox()
	{
		auto bound = this->getBoundingBox();
		bound.size = Vec2::ZERO;

		Vec2 max = Vec2::ZERO;
		Vec2 min = Vec2::ZERO;
		for (auto n : _nodeList)
		{
			if (n != nullptr)
			{
				auto boundn = n->getBoundingBox();
				if (boundn.getMinX() < min.x)
					min.x = boundn.getMinX();
				if (boundn.getMinY() < min.y)
					min.y = boundn.getMinY();
				if (boundn.getMaxX() > max.x)
					max.x = boundn.getMaxX();
				if (boundn.getMaxY() > max.y)
					max.y = boundn.getMaxY();
			}
		}

		bound.size.width = max.x - min.x;
		bound.size.height = max.y - min.y;

		return bound;
	}

	Node* NodeList::getNodeAt(int index)
	{
		Node* node = nullptr;
		if (index >= 0 && index< _nodeList.size())
		{
			node = _nodeList.at(index);
		}
		return node;
	}

	BYTE NodeList::getValueAt(int index)
	{
		BYTE value = 0;
		if (index >= 0 && index< _valueList.size())
		{
			value = _valueList.at(index);
		}
		return value;
	}

	BYTE NodeList::getNodeListSize()
	{
		return _nodeList.size();
	}

	void NodeList::setValueList(const std::vector<BYTE> &valuelist)
	{
		clear();
		_valueList.reserve(valuelist.size());
		_nodeList.reserve(valuelist.size());

		for (size_t i = 0; i<valuelist.size(); i++)
		{
			add(valuelist.at(i));
		}
	}

	void NodeList::updateValueList(const std::vector<BYTE> &valuelist)
	{
		std::vector<Node*> nodelist;
		for (size_t i = 0; i < valuelist.size(); i++)
		{
			int index = -1;
			for (size_t j = 0; j<_valueList.size(); j++)
			{
				if (_valueList.at(j) == valuelist.at(i))
					index = j;
			}

			if (index >= 0)
			{
				_nodeList.at(index)->setVisible(true);
				nodelist.push_back(_nodeList.at(index));
				_nodeList.erase(_nodeList.begin() + index);
				_valueList.erase(_valueList.begin() + index);
			}
			else
			{
				Node* newnode = getNodeFromValue(valuelist.at(i));
				if (newnode != nullptr)
				{
					int zorder = getIndexZorder(nodelist.size());
					Vec2 pos = getIndexPosition(nodelist.size());
					nodelist.push_back(newnode);
					this->addChild(newnode);
				}
			}
		}

		for (auto c : _nodeList)
			c->removeFromParent();
		_nodeList.clear();

		_valueList = valuelist;
		_nodeList = nodelist;
		for (size_t i = 0; i < _nodeList.size(); i++)
			_nodeList.at(i)->setPosition(getIndexPosition(i));
	}

	void NodeList::setValue(int index, BYTE value)
	{
		if (index >= 0 && index<_valueList.size() && index<_nodeList.size())
		{
			_valueList.at(index) = value;
			setNodeValue(_nodeList.at(index), value);
		}
	}

	void NodeList::setOffset(Vec2 offset)
	{
		_offset = offset;

		for (size_t i = 0; i< _nodeList.size(); i++)
		{
			_nodeList.at(i)->setPosition(getIndexPosition(i));
		}
	}

	void NodeList::setIncreasedZorder(bool increased)
	{
		_increasedZorder = increased;

		for (size_t i = 0; i< _nodeList.size(); i++)
		{
			_nodeList.at(i)->setLocalZOrder(getIndexZorder(i));
		}
	}

	// start from 0
	int NodeList::getIndexZorder(int index)
	{
		auto flag = _increasedZorder ? 1 : -1;
		return index * flag;
	}

	bool NodeList::add(BYTE value)
	{
		if (value == 0)
		{
			auto card = getNodeFromValue(value);

			if (card != nullptr)
			{

				auto index = _nodeList.size();
				this->addChild(card, getIndexZorder(index));
				//card->setPosition(getIndexPosition(index));
				card->setPosition(getIndexPosition(index));
				card->setScale(_scale);
				_valueList.push_back(value);
				_nodeList.push_back(card);
				return true;
			}
		}
		bool bCopy = false;
		for (int i = 0; i < _valueList.size(); i++)
		{
			if (_valueList[i] == value)
			{
				bCopy = true;
			}
		}
		if (!bCopy && value != 0)
		{
			auto card = getNodeFromValue(value);

			if (card != nullptr)
			{

				auto index = _nodeList.size();
				this->addChild(card, getIndexZorder(index));
				//card->setPosition(getIndexPosition(index));
				card->setPosition(getIndexPosition(index));
				card->setScale(_scale);
				_valueList.push_back(value);
				_nodeList.push_back(card);
				return true;
			}
		}
		/*auto card = getNodeFromValue(value);

		if (card != nullptr)
		{

			auto index = _nodeList.size();
			this->addChild(card, getIndexZorder(index));
			card->setPosition(getIndexPosition(index));
			card->setScale(_scale);
			_valueList.push_back(value);
			_nodeList.push_back(card);

			return true;
		}*/

		return false;
	}

	void NodeList::setNodeVisible(int index, bool visible)
	{
		if (index >= 0 && index< _nodeList.size() && _nodeList.at(index) != nullptr)
		{
			_nodeList.at(index)->setVisible(visible);
		}
	}

	void NodeList::remove(int index)
	{
		// remove from vector
		if (index >= 0 && index< _valueList.size() && index< _nodeList.size())
		{
			_valueList.erase(_valueList.begin() + index);

			if (_nodeList.at(index) != nullptr)
			{
				_nodeList.at(index)->removeFromParent();
				_nodeList.erase(_nodeList.begin() + index);
			}
		}

		// remove from position
		for (size_t i = 0; i< _nodeList.size(); i++)
		{
			if (_nodeList.at(i) != nullptr)
			{
				_nodeList.at(i)->setPosition(getIndexPosition(i));
			}
		}
	}

	// start from 0
	Vec2 NodeList::getIndexPosition(int index)
	{
		return _offset* index;
	}

	int NodeList::containsValue(BYTE value)
	{
		for (size_t i = 0; i < _valueList.size(); i++)
		{
			if (value == getValueAt(i))
			{
				return i;
			}
		}
		return -1;
	}

	void NodeList::clear()
	{
		for (auto n : _nodeList)
		{
			if (n != nullptr)
			{
				n->removeFromParent();
				n = nullptr;
			}
		}
		_nodeList.clear();
		_valueList.clear();
	}

	void NodeList::setTouchable(bool touchable)
	{
		_touchEnabled = touchable;
	}

	void NodeList::setTouchCallback(std::function<void(int index, BYTE value)> callback)
	{
		_touchCallback = callback;
	}

	bool NodeList::touchCheck(Vec2 pos)
	{
		_touchIndex = -1;
		_touchedNode = nullptr;
		int zorder = 0;
		for (size_t i = 0; i < _nodeList.size(); i++)
		{
			auto rect = _nodeList.at(i)->getBoundingBox();
			auto zo = _nodeList.at(i)->getLocalZOrder();
			if (rect.containsPoint(pos))
			{
				if (_touchIndex == -1)
				{
					_touchedNode = _nodeList.at(i);
					_touchIndex = i;
					zorder = _touchedNode->getLocalZOrder();
				}
				else if (_nodeList.at(i)->getLocalZOrder() > zorder)
				{
					_touchedNode = _nodeList.at(i);
					_touchIndex = i;
					zorder = _touchedNode->getLocalZOrder();
				}
			}
		}

		return _touchIndex == -1 ? false : true;
	}


	Node* NodeList::getTouchedNode()
	{
		return _touchedNode;
	}

	int NodeList::getTouchedIndex()
	{
		return _touchIndex;
	}

	void NodeList::setCardScle(float scle)
	{
		_scale=scle;
	}

	bool NodeList::onTouchBegan(Touch *touch, Event *unused_event)
	{
		if (!_touchEnabled)
			return false;

		auto pos = this->convertToNodeSpace(touch->getLocation());
		if (touchCheck(pos))
		{
			return true;
		}

		return false;
	}

	void NodeList::onTouchMoved(Touch *touch, Event *unused_event)
	{
		if (_touchedNode != nullptr)
		{
			_touchedNode->setPosition(this->convertToNodeSpace(touch->getLocation()));
		}
	}

	void NodeList::onTouchEnded(Touch *touch, Event *unused_event)
	{
		if (_touchedNode != nullptr && _touchIndex >= 0 && _touchIndex< _valueList.size())
		{
			auto pos = _touchedNode->getPosition();
			auto bound = _touchedNode->getBoundingBox();

			if (_touchCallback != nullptr && pos.y > bound.size.height)
			{
				_touchedNode->setVisible(false);
				_touchCallback(_touchIndex, _valueList.at(_touchIndex));
			}
			else
			{
				_touchedNode->setPosition(getIndexPosition(_touchIndex));
			}
		}
	}

}
