#ifndef NN8ChatTip_h__
#define NN8ChatTip_h__

#include <string>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::extension;
using namespace ui;


namespace NN8
{
	class NN8ChatTip: public Sprite
	{
	public:
		 NN8ChatTip();
		~NN8ChatTip();
		CREATE_FUNC(NN8ChatTip);
		void setClickCallBack(std::function<void()> clickCallBack );
		void setInfo(std::string name,std::string talkMsg);
		void playNewMsgAction();
		void stopNewMsgAction();
	private:
		virtual bool init();
		void onBtnClick(Ref* pSender,Control::EventType event);
	private:
		Label*					_chatUserName;
		Label*					_chatLabel;
		Sprite*					_chatTips;
		std::function<void()>	_clickCallBack;
	};

	
}
#endif // NN8ChatTip_h__