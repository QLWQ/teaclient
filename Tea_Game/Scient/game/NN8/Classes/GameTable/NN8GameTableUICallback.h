#ifndef __NN8_Game_Table_UI_Callback_H__
#define __NN8_Game_Table_UI_Callback_H__

#include "HNNetExport.h"
#include "NN8MessageHead.h"
#include "HNLogicExport.h"

namespace NN8
{
	class GameTableUICallback : public HN::IHNGameLogicBase
	{
	public:
		virtual void loadUser(BYTE seat, UserInfoStruct &info) = 0;
		virtual void showUserUp(BYTE seatNo) = 0;
	
		virtual void initParams() = 0;
		virtual void render_ASS_CALL_SCORE(bool show) = 0;
		virtual void callNote(BeginUpgradeStruct* note) = 0;
		virtual void sendCard(SendAllCardStruct send) = 0;
		virtual void updateHandcard(BYTE cards[], BYTE seat, bool bOpenCard) = 0;
		virtual void showBull(UserTanPai bull) = 0;
		virtual void deal_ASS_CONTINUE_END(GameEndStruct* end) = 0;

		virtual void updateHandCardWithAction(BYTE cards[], BYTE seat,UserTanPai bull,float delayTime) = 0;
		virtual void removeUser(BYTE seatNo) = 0;

		virtual void showVipTip(BYTE seat, UserInfoStruct &info) = 0;

		virtual void showDealerMark(BYTE seat, bool visible) = 0;
		virtual void updateNote(BYTE seatId, LLONG money) = 0;
		virtual void playNoteAudio(BYTE seatId, BYTE audioType) = 0;
		virtual void setStartBtnVisible(bool visible) = 0;	
		virtual void setDissloveBtState() =0;
		virtual void setGamecount(int counts,int maxCount) =0;
		virtual void setUserDefine(std::vector<int>  define)=0;
		virtual void showWechat(bool visible) = 0;
		virtual void ShowAllResult(GameEndInfoStruct & cal,bool sh) = 0;
		virtual void setGameBeginTipsVisible(bool isvisible) = 0;
		virtual void changTipshow(int tag) = 0;
		virtual void userRedayShow( int seat, bool isReday) = 0;
		virtual void setAllUserRedayUnVisble()=0;
		virtual void showOffLine(BYTE seat)=0;
		virtual void showBatBtVisble(bool isVisble)=0;
		virtual void showqiangZhuangTips(BYTE qzStates[])=0;
		virtual void hideqiangZhuangTips()=0;
		virtual void playQiangzhuangAciton(BYTE qzResult[],BYTE seat,bool visble)=0;
		virtual void setZhuangSeat(BYTE seat)=0;
		virtual void reConnect()=0;
		virtual void showCallNode()=0;
		virtual void sendCardState(BYTE viewSeat,BYTE cards [])=0;
		virtual void setUserScore(int score[PLAY_COUNT],int Score)=0;
		virtual void showGameDeskNotFound()=0;

		virtual void setGMButtonVisibel(bool isVisibel)=0;

		virtual void showContolSuccese(bool isSuccese)=0;
		virtual void showTanPai(bool isVisible) = 0;

		virtual void jinYongReturn(bool isPlay) = 0;
		virtual void showTanPaiSign(int seat,bool isVisable) = 0;
		 
		virtual void playGameStartAni() = 0;
		virtual void showSignState(int seat,int state) = 0;
		virtual void hideSign(int seat) = 0;
		virtual void closeClock() = 0;

		virtual void onChatVoiceMsg(int userID, int voiceID, int voiceTime) = 0;

		// 文本聊天消息
		virtual void onChatTextMsg(BYTE seatNo, CHAR msg[]) = 0;

		// 显示左上角桌子信息
		virtual void setDeskInfo() = 0;
		//显示房卡信息相关
		virtual void showMaster(BYTE viewSeatNo, bool bShow) = 0;
		//发送战绩消息
		virtual void sendRecordData(S_C_GameRecordResult* data) = 0;
		
	};
}

#endif