#ifndef __NN8_SuperSet_H__
#define __NN8_SuperSet_H__

#include"cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <string>
#include "NN8MessageHead.h"
#include "NN8GameTableLogic.h"
using namespace ui;
using namespace std;
using namespace HN;
namespace NN8
{
	class  GameSuperSet:public HNLayer
	{
	public:
		GameSuperSet();
		~GameSuperSet();
		static GameSuperSet*create(GameTableLogic* tableLogic);
	private:
		bool init(GameTableLogic* tableLogic);

	void initOnlineUser();

	private:
		ImageView*    _player[PLAY_COUNT];	

		GameTableLogic* _tableLogic;

	private:
		void onClikCheckBox(Ref* pSelsct);

		void btClikeBtBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype);


		void selectKindByNode(Node* pRootNode,int pTag);//只能选择一个胜利或失败玩家；

		void sendControl();
	private:
		enum SelectKind
		{
			LOSE =0,
			WIN	 =1
		};

	};
}
#endif

