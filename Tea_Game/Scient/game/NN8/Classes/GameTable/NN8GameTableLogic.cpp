#include "NN8MessageHead.h"
#include "NN8GameTableLogic.h"
#include "NN8GameTableUI.h"
#include "HNNetExport.h"
#include "HNLobbyExport.h"
#include "HNUIExport.h"

using namespace HN;

namespace NN8
{
	GameTableLogic::GameTableLogic(GameTableUICallback* uiCallback,BYTE deskNo,bool autoCreate):
		_gametableUi(uiCallback),
		HNGameLogicBase(deskNo,PLAY_COUNT,autoCreate,uiCallback)
	{
		_autoCreate = autoCreate;
		memset(&_gameStation, 0, sizeof(_gameStation));
		memset(&_iUserStation, 0, sizeof(_iUserStation));

		//initParams();
		_moshi=-1;
		_isLast=false;
		_pBetType=FREEBET;
		_iGameType = 0;
	}

	GameTableLogic::~GameTableLogic()
	{
	}


	void GameTableLogic::dealGameMessage(NetMessageHead* messageHead, void* object, INT objectSize)

	{
		if (MDM_GM_GAME_NOTIFY != messageHead->bMainID)
		{
			HNLOG("onGameMessage MainID is error.");
			return;
		}

		switch (messageHead->bAssistantID)
		{
		case ASS_GM_GAME_STATION:
			HNLOG("ASS_GM_GAME_STATION");
		
			break;
		case ASS_GM_AGREE_GAME:
			HNLOG("ASS_GM_AGREE_GAME");
			
			break;

		case ASS_CALL_SCORE:						// 51
			deal_ASS_CALL_SCORE(object, objectSize);
			break;
		case ASS_CALL_WAIT:
			dealUserWait(object, objectSize);
			break;
		case ASS_CALL_SCORE_RESULT:					// 53
			dealCallScoreResult(object, objectSize);
			break;
		case ASS_CALL_SCORE_FINISH:					// 54
			dealCallScoreFinish(object, objectSize);
			break;
		case ASS_CALL_NOTE:							// 55
			dealCallNote(object, objectSize);
			break;
		case ASS_CALL_NOTE_RESULT:					// 57
			dealCallNoteResult(object, objectSize);
			break;
		case ASS_CALL_SEND_CARD:					// 58
			dealCallSendResult(object, objectSize);
			break;
		case ASS_CALL_SEND_FINISH:					// 59
			dealCallSendFinish(object, objectSize);
			break;
		case ASS_CALL_OPEN:							// 60
			dealCallOpen(object, objectSize);
			break;
		case ASS_CALL_OPEN_RESULT:					// 62
			dealCallOpenResult(object, objectSize);
			break;
		case ASS_CONTINUE_END:						// 65
			dealContinueEnd(object, objectSize);
			break;
		case ASS_CUT_END:							// 67
			HNLOG("ASS_CUT_END");
			dealCutEnd(object, objectSize);
			break;
		case ASS_SALE_END:							// 68
			HNLOG("ASS_SALE_END");
			dealSaleEnd(object, objectSize);
			break;
		case ASS_GAME_RESULT:						// 69
			dealGameResult(object, objectSize);
			break;
		case ASS_SUPER_USER_SET_RESULT:		//³¬¶ËÉèÖÃ½á¹û
			{
				dealSuperSetResult(object,objectSize);
			}break;
		case ASS_CALL_ALLOPEN_FINISH:
			{
				dealOpenCardFinish(object,objectSize);
			}break;
		case ASS_USER_TANPAI:
		{
			dealUserTanpai(object, objectSize);
		}break;
		case S_C_GAME_RECORD_RESULT:
		{
			CHECK_SOCKET_DATA(S_C_GameRecordResult, sizeof(S_C_GameRecordResult), "S_C_GameRecordResult size error!");
			S_C_GameRecordResult* pSuperUser = (S_C_GameRecordResult*)(object);
			_gametableUi->sendRecordData(pSuperUser);
		}break;
		default:
			HNLOG("UNKNOW MESSAGE: %d", messageHead->bAssistantID);
			;

		}
	}
	//ÓÎÏ·×Ü½áËã
	void GameTableLogic::dealGameResult(void* object, INT objectSize)
	{
		HNLOG("ASS_GAME_RESULT");
		CCAssert(sizeof(GameEndInfoStruct) == objectSize, "Broken message ASS_GAME_RESULT.");
		auto call=(GameEndInfoStruct*)object;
		_gametableUi->ShowAllResult((*call),false);
		_isLast=true;


	}

	void GameTableLogic::deal_ASS_CALL_SCORE(void* object, INT objectSize)			// 51
	{
		HNLOG("ASS_CALL_SCORE");
		CCAssert(sizeof(CallScoreStruct) == objectSize, "Broken message ASS_CALL_SCORE.");
		auto call = (CallScoreStruct*)object;

		
		///////////////////////////////////////
		// option button
		if (_iGameType != 0)
		{
			_gametableUi->initParams();
		}

		if ( _iUserStation[0] != -1&&call->bPlayer)
		{
			_gametableUi->render_ASS_CALL_SCORE(true);
			_gametableUi->changTipshow(QIANGZHUANG);
			_gametableUi->jinYongReturn(false);
		}
		else
		{
			_gametableUi->jinYongReturn(true);
		}
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			int viewseat = logicToViewSeatNo(i);
			if (isPlaying(i))
			{
				_gametableUi->showSignState(viewseat, QIANGZHUANG);
			}
		}
		_gametableUi->showTanPai(false);

	}


	
	void GameTableLogic::dealCallScoreResult(void* object, INT objectSize)		// 53
	{
		HNLOG("ASS_CALL_SCORE_RESULT");
		CCAssert(sizeof(CallScoreStruct) == objectSize, "Broken message ASS_CALL_SCORE_RESULT.");
		auto call = (CallScoreStruct*)object;


		///////////////////////////////////////
		_gametableUi->showqiangZhuangTips(call->byUserCallStation);
		_gametableUi->hideSign(logicToViewSeatNo(call->bDeskStation));
		_gametableUi->setZhuangSeat(call->bDeskStation);
		_gametableUi->setAllUserRedayUnVisble();
		//_gametableUi->jinYongReturn(false);
	}

	void GameTableLogic::dealCallScoreFinish(void* object, INT objectSize)		// 54
	{
		HNLOG("ASS_CALL_SCORE_FINISH");
		CCAssert(sizeof(CallScoreStruct) == objectSize, "Broken message ASS_CALL_SCORE_FINISH.");
		auto call = (CallScoreStruct*)object;
		//_gametableUi->jinYongReturn(false);
		int viewSeat = logicToViewSeatNo(call->bDeskStation);

		_gametableUi->setZhuangSeat(call->bDeskStation);
// 		if (_moshi==0)
// 		{
			_gametableUi->playQiangzhuangAciton(call->byUserCallStation,logicToViewSeatNo(call->bDeskStation),true);
// 		}
// 		else
// 		{
// 			_gametableUi->showDealerMark(logicToViewSeatNo(call->bDeskStation),true);
// 			_gametableUi->updateNote(logicToViewSeatNo(call->bDeskStation), 0);
// 
// 		}
		
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			_gametableUi->hideSign(i);
			if (logicToViewSeatNo(i) == 0 && isPlaying(i))
			{
				_gametableUi->jinYongReturn(false);
			}
		}
		
		_gametableUi->setAllUserRedayUnVisble();
		_gametableUi->setGameBeginTipsVisible(false);

		///////////////////////////////////////
		// hide option button
		_gametableUi->render_ASS_CALL_SCORE(false);
		
		
	}

	void GameTableLogic::dealCallNote(void* object, INT objectSize)				// 55
	{
		HNLOG("ASS_CALL_NOTE");
		CCAssert(sizeof(BeginUpgradeStruct) == objectSize, "Broken message ASS_CALL_NOTE.");
		BeginUpgradeStruct *call = (BeginUpgradeStruct *)object;
		// init ui
		
		//_gametableUi->initParams();
	
		//Í¨Öª½Ð·Ö
		_gametableUi->callNote(call);
		_gametableUi->changTipshow(XIAZHU);
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			int viewseat = logicToViewSeatNo(i);
			if (isPlaying(i))
			{
				_gametableUi->showSignState(viewseat, XIAZHU);
			}
			if (logicToViewSeatNo(i) == 0 && isPlaying(i))
			{
				_gametableUi->jinYongReturn(false);
			}

		}
		//_gametableUi->jinYongReturn(false);

	}
	void GameTableLogic::dealCallNoteResult(void* object, INT objectSize)		// 57
	{
		HNLOG("ASS_CALL_NOTE_RESULT");
		CCAssert(sizeof(NoteResult) == objectSize, "Broken message ASS_CALL_NOTE_RESULT.");
		NoteResult *note = (NoteResult *)object;

		//_gametableUi->jinYongReturn(false);
		///////////////////////////////////////
		// show note
		_gametableUi->updateNote(logicToViewSeatNo(note->iOutPeople), note->iCurNote);
		_gametableUi->changTipshow(FAPAI);
		_gametableUi->hideSign(logicToViewSeatNo(note->iOutPeople));
		if (note->iOutPeople == _mySeatNo)
			_gametableUi->callNote(nullptr);
	
	}

	void GameTableLogic::dealCallSendResult(void* object, INT objectSize)		// 58
	{
		HNLOG("ASS_CALL_SEND_CARD");
		CCAssert(sizeof(SendAllCardStruct) == objectSize, "Broken message ASS_CALL_SEND_CARD.");
		memset(&_sendCard,0,sizeof(_sendCard));
		SendAllCardStruct *pSendCard = (SendAllCardStruct *)object;
		memcpy(&_sendCard, pSendCard, objectSize);
		if (_pBetType!=FREEBET)
		{
			//_gametableUi->initParams();
		}
		///////////////////////////////////////
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_gametableUi->hideSign(i);
			if (logicToViewSeatNo(i) == 0 && isPlaying(i))
			{
				_gametableUi->jinYongReturn(false);
			}
		}

		// send card
		_openResultVec.clear();
		_gametableUi->sendCard(_sendCard);
		_gametableUi->showBatBtVisble(false);
		//_gametableUi->jinYongReturn(false);
	}

	void GameTableLogic::dealCallSendFinish(void* object, INT objectSize)		// 59
	{
		HNLOG("ASS_CALL_SEND_FINISH");
		CCAssert(sizeof(SendCardFinishStruct) == objectSize, "Broken message ASS_CALL_SEND_FINISH.");
		SendCardFinishStruct *sendCardFinish = (SendCardFinishStruct *)object;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			LLONG pBetMoney=sendCardFinish->i64PerJuTotalNote[i];
			_gametableUi->updateNote(logicToViewSeatNo(i), pBetMoney);
			if (logicToViewSeatNo(i) == 0 && isPlaying(i) && _iGameType!=0)
			{	
				_gametableUi->showTanPai(true);
				_gametableUi->jinYongReturn(false);
			}

		}
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			BYTE cardValue[5] = {};
			for (int j = 0; j < 5; j++)
			{
				if (logicToViewSeatNo(i)==0)
				{
					cardValue[j] = _sendCard.iUserCard[i][j];
					if (j == 3 && _iGameType == 0)
					{
						cardValue[3] = 0;
					}
				}
				else
				{
					cardValue[j] = 0;
				}
			}
			int viewSeat = logicToViewSeatNo(i);
			//¸üÐÂÊÖÅÆ
			_gametableUi->updateHandcard(cardValue, viewSeat,false);
		}

	}

	void GameTableLogic::dealCallOpen(void* object, INT objectSize)				// 60
	{
		HNLOG("ASS_CALL_OPEN");

		 _gametableUi->changTipshow(XIKAO);


	}


	void GameTableLogic::dealCallOpenResult(void* object, INT objectSize)		// 62
	{
		HNLOG("ASS_CALL_OPEN_RESULT");
		CCAssert(sizeof(UserTanPai) == objectSize, "Broken message ASS_CALL_OPEN_RESULT.");
		UserTanPai *open = (UserTanPai *)object;

		saveOpenCardData(open);
		// showbull
		_gametableUi->updateHandcard(_sendCard.iUserCard[open->byDeskStation], logicToViewSeatNo(open->byDeskStation),true);
		_gametableUi->closeClock();
		//_gametableUi->showBull(*open);
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			_gametableUi->showTanPaiSign(i,false);
		}
		_gametableUi->showTanPai(false);
	}

	
	void GameTableLogic::dealCutEnd(void* object, INT objectSize){}				// 67
	void GameTableLogic::dealSaleEnd(void* object, INT objectSize){}				// 68

	// ASS_CONTINUE_END
	void GameTableLogic::dealContinueEnd(void* object, INT objectSize)			// 90
	{
		HNLOG("ASS_CONTINUE_END");
		CCAssert(sizeof(GameEndStruct) == objectSize, "Broken message ASS_CONTINUE_END.");
		auto buff = (GameEndStruct *)object;
	
		/////////////////////////////////////
		// ui end
		_gametableUi->deal_ASS_CONTINUE_END(buff);
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			_gametableUi->showTanPaiSign(i, false);
		}
		_gametableUi->jinYongReturn(true);

		/////////////////////////////////////
		
		_gametableUi->changTipshow(-1);
		_iUserStation[0] = 5;

	}

	void GameTableLogic::dealGamePointResp(void* object, INT objectSize)
	{
		HNLOG("dealGamePointResp");
		CCAssert(sizeof(MSG_GR_R_UserPoint) == objectSize, "MSG_GR_R_UserPoint is error.");

		MSG_GR_R_UserPoint * buff = (MSG_GR_R_UserPoint*)object;

		/////////////////////////////////////
		// win/fail audio effect
		if (buff->dwUserID == getUserId(_mySeatNo))
		{
			auto endAudio = buff->dwMoney > 0 ? AUDIO_WIN : AUDIO_LOSE;
			//HNAudioEngine::getInstance()->playEffect(endAudio);
		}

	}

	void GameTableLogic::dealGameEndResp(INT bDeskNO)
	{
		HNLOG("dealGameEndResp");
		//ÖØÐÂ¼ÓÔØÍæ¼Ò£¬¸üÐÂ½ð±Ò
		
	}


	


	void GameTableLogic::enterDesk()
	{		
		if (_autoCreate)
		{
			sendGameInfo();
		}
		INT seatNo = INVALID_DESKNO;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* pUser = _deskUserList->getUserByDeskStation(i);
			if (_existPlayer[i] && pUser != nullptr)
			{
				_playerSitted[i] = true;
				seatNo = logicToViewSeatNo(i);
				_gametableUi->loadUser(seatNo, *pUser);

				BYTE seatNo = logicToViewSeatNo(i);

				//if (pUser) _gametableUi->showMaster(seatNo, true);


// 				_uiCallback->addUser(seatNo, i == _mySeatNo);
// 				_uiCallback->setUserName(seatNo, pUser->nickName);
// 				//	_uiCallback->setUserMoney(seatNo, pUser->i64Money);
// 				_uiCallback->showUserMoney(seatNo, true);
// 				_uiCallback->showUserHandCardCount(seatNo, false);

			}
		}
	}

	void GameTableLogic::leaveDesk()
	{
		HNAudioEngine::getInstance()->resumeBackgroundMusic();
		GamePlatform::returnPlatform(LayerType::ROOMLIST);


	}

	void GameTableLogic::dealUserAgreeResp(MSG_GR_R_UserAgree* agree)
	{
		HNLOG("dealUserAgreeResp");
		/////////////////////////////////////
		// start button & timer, stop the timer if the user is ready
		if (agree->bAgreeGame == 1)
		{	
			_gametableUi->userRedayShow(logicToViewSeatNo(agree->bDeskStation),agree->bAgreeGame);

			if (agree->bDeskStation == _mySeatNo)
			{
				_gametableUi->setStartBtnVisible(false);
			}
			   _gametableUi->setGameBeginTipsVisible(true);

		}
	}

	void GameTableLogic::dealGameStartResp(INT deskId)
	{
		HNLOG("dealGameStartResp");
		_gametableUi->showWechat(false);
	}

	void GameTableLogic::sendUserUp()
	{
		RoomLogic()->sendData(MDM_GR_USER_ACTION, ASS_GR_USER_UP);

	}


	// iNoteType 0, 1, 2, 3(smallest)
	void GameTableLogic::sendNote(BYTE notetype)
	{
		HNLOG("sendNote");
		tagUserProcess usernote;
		usernote.iNoteType = notetype;
		usernote.iVrebType = TYPE_NOTE;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_USER_NOTE, &usernote, sizeof(tagUserProcess));
	}

	void GameTableLogic::sendShowhand(UserTanPai show)
	{
		HNLOG("sendShowhand");

		show.byDeskStation = _mySeatNo;
		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_USER_OPEN, &show, sizeof(UserTanPai));
	}

	
	void GameTableLogic::sendGiveUp()
	{
		HNLOG("sendGiveUp");
	}

	void GameTableLogic::sendCallScore(bool call)
	{
		HNLOG("sendCallScore");

		CallScoreStruct callScore;
		callScore.iValue = call ? 1 : 0;
		callScore.bDeskStation = _mySeatNo;
		callScore.bCallScoreflag = false;

		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_USER_SCORE, &callScore, sizeof(CallScoreStruct));
	}


	BYTE GameTableLogic::getGameStatus()
	{
		return _gameStatus;
	}


	INT GameTableLogic::getUserId(BYTE lSeatNo)
	{
		UserInfoStruct* userInfo = _deskUserList->getUserByDeskStation(lSeatNo);
		if(userInfo != nullptr)
		{
			return userInfo->dwUserID;
		}
		return INVALID_USER_ID;
	}

	UserInfoStruct* GameTableLogic::getUserByDeskStation(BYTE lSeatNo)
	{
		return _deskUserList->getUserByDeskStation(lSeatNo);
	}

	bool GameTableLogic::getUserIsBoy(BYTE lSeatNo)
	{
		UserInfoStruct* userInfo = _deskUserList->getUserByDeskStation(lSeatNo);
		if(userInfo != nullptr)
		{
			return userInfo->bBoy;
		}
		return true;
	}

	// ASS_GM_GAME_STATION
	// login from disconnection
	void GameTableLogic::dealGameStationResp(void* object, INT objectSize)
	{
		HNLOG("dealGameStationResp");
		_gameStation = *(GameStation_Base*)object;

		/////////////////////////////////////
		// load desk user
		_gametableUi->setUserScore(_gameStation.scoreArray, _gameStation.iBaseNote);
		for (int i=0;i<PLAY_COUNT;i++)
		{
			if (i==_mySeatNo)
			{
				_gametableUi->setGMButtonVisibel(_gameStation.superUser[i]);
				break;

			}
		}
		
		_gametableUi->initParams();
		//_gametableUi->setBottomScore();
		/////////////////////////////////////
		// game station
		switch (_gameStation.byGameStation)
		{
		case GS_WAIT_SETGAME: //ÓÎÏ·Ã»ÓÐ¿ªÊ¼×´Ì¬
			HNLOG("GAMESTATION: GS_WAIT_SETGAME");
		case GS_WAIT_ARGEE: //µÈ´ýÍæ¼Ò¿ªÊ¼×´Ì¬
			HNLOG("GAMESTATION: GS_WAIT_ARGEE");
		case GS_WAIT_NEXT: //µÈ´ýÏÂÒ»ÅÌ¿ªÊ¼
			HNLOG("GAMESTATION: GS_WAIT_NEXT");
			gameStation_GS_WAIT_ARGEE(object, objectSize);
			break;

		case GS_ROB_NT: //ÇÀ×¯×´Ì¬
			{
				
				HNLOG("GAMESTATION: GS_ROB_NT");
				gameStation_GS_ROB_NT(object, objectSize);
				break;
			}

		case GS_NOTE: //ÏÂ×¢×´Ì¬
			{
				
				HNLOG("GAMESTATION: GS_NOTE");
				gameStation_GS_NOTE(object, objectSize);
				break;
			}

		case GS_SEND_CARD: //·¢ÅÆ×´Ì¬
			{
			
				HNLOG("GAMESTATION: GS_SEND_CARD");
				gameStation_GS_SEND_CARD(object, objectSize);
				break;
			}

		case GS_OPEN_CARD: //ÓÎÏ·½øÐÐÖÐ
			{
				
				HNLOG("GAMESTATION: GS_OPEN_CARD");
				gameStation_GS_OPEN_CARD(object, objectSize);
				break;
			}

		default:
			break;
		}
		
		_gametableUi->setStartBtnVisible(false);
		_gametableUi->showWechat(false);

		_gametableUi->reConnect();
	}

	//设置回放模式
	void GameTableLogic::dealGamePlayBack(bool isback)
	{
		//GameManager::getInstance()->setPlayBack(isback);
	}

	// GS_WAIT_ARGEE
	void GameTableLogic::gameStation_GS_WAIT_ARGEE(void* object, INT objectSize)
	{
		if(objectSize != sizeof(GameStation_WaiteAgree))
		{
			return;
		}

		HNLOG("setGameStation_2: GS_WAIT_ARGEE");
	
		auto wait = (GameStation_WaiteAgree*) object;

		for(int i=0; i<PLAY_COUNT; i++)
		{
			if(_deskUserList->getUserByDeskStation(i)==nullptr)
				continue;

			if(USER_SITTING==_deskUserList->getUserByDeskStation(i)->bUserState)
			{
				if(i == _mySeatNo)
					_gametableUi->setStartBtnVisible(!wait->bUserReady[i]);
			}
		}
		_iUserStation[0] = 5;
	}

	void GameTableLogic::gameStation_GS_ROB_NT(void* object, INT objectSize)
	{
		CCAssert(sizeof(GameStation_RobNt) == objectSize, "Broken message GS_ROB_NT.");
		auto ptr = (GameStation_RobNt*) object;
		SendAllCardStruct tempSendStrut;
		memcpy(tempSendStrut.iUserCard,ptr->iUserCard,sizeof(tempSendStrut.iUserCard));
		memcpy(tempSendStrut.iUserCardCount,ptr->iUserCardCount,sizeof(tempSendStrut.iUserCardCount));
		_sendCard=tempSendStrut;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			int value = logicToViewSeatNo(i);
			_iUserStation[value] = ptr->iUserStation[i];
			if (i==_mySeatNo&&ptr->byUserCallStation[i]==255&&ptr->iUserStation[i]!=0)
			{
				_gametableUi->render_ASS_CALL_SCORE(true);
				_gametableUi->changTipshow(QIANGZHUANG);
			}
			if (ptr->iUserStation[i]!=0 || _iGameType==0)
			{
				 _gametableUi->updateNote(value, ptr->i64PerJuTotalNote[i]);
			}
			
			if (logicToViewSeatNo(i) == 0)
			{
				if (isPlaying(i))
				{
					_gametableUi->jinYongReturn(false);
				}
				else
				{
					_gametableUi->jinYongReturn(true);
				}
			}

			UserInfoStruct* userinfo=getUserByDeskStation(i);
			if (userinfo==nullptr)
			{
				continue;
			}

			if (i!=_mySeatNo)
			{
				BYTE cards[HANDCARD_COUNT] ={0};
				//_gametableUi->sendCardState(value,cards);


			}
			else
			{	
				BYTE cards[HANDCARD_COUNT] ={0};
				memcpy(cards,ptr->iUserCard[i],sizeof(cards));
				//cards[4]=0;
				//_gametableUi->sendCardState(value,cards);

			}


		}
		if (ptr->byGameModel == 0)
		{
			for (int i = 0; i < PLAY_COUNT;i++)
			{
				int value = logicToViewSeatNo(i);
				if (value != 0)
				{
					BYTE cards[HANDCARD_COUNT] = { 0 };
					_gametableUi->updateHandcard(cards, value, false);
				}
				else
				{
					BYTE cards[HANDCARD_COUNT] = { 0 };
					memcpy(cards, ptr->iUserCard[i], sizeof(cards));
					cards[3]=0;
					_gametableUi->updateHandcard(cards, value, false);
				}
				
			}
		}
		
	
		_gametableUi->showqiangZhuangTips(ptr->byUserCallStation );
		_gametableUi->setGameBeginTipsVisible(false);
		
		
	}

	void GameTableLogic::gameStation_GS_NOTE(void* object, INT objectSize)
	{
		CCAssert(sizeof(GameStation_Note) == objectSize, "Broken message GS_NOTE.");
		auto ptr = (GameStation_Note*) object;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (logicToViewSeatNo(i) == 0)
			{
				if (isPlaying(i))
				{
					_gametableUi->jinYongReturn(false);
				}
				else
				{
					_gametableUi->jinYongReturn(true);
				}
			}
			int value = logicToViewSeatNo(i);
			_iUserStation[value] = ptr->iUserStation[i];
			UserInfoStruct* userinfo=getUserByDeskStation(i);
			_gametableUi->setZhuangSeat(ptr->byNtStation);
			CCLOG("the bank is %d", ptr->byNtStation);
			if (userinfo==nullptr)
			{
				continue;
			}
			if (ptr->iUserStation[i] ==2&&ptr->byNtStation!=i)
			{
				_gametableUi->updateNote(value, ptr->i64PerJuTotalNote[i]);
			}
		
		}
		_gametableUi->showDealerMark(logicToViewSeatNo(ptr->byNtStation),true);
		// note option
		if (ptr->byNtStation == _mySeatNo)
		{
			_gametableUi->changTipshow(-1);
			
		}
		else
		{
			if (_iUserStation[0]!=2&&_iUserStation[0])
			{
				_gametableUi->showCallNode();
				_gametableUi->changTipshow(XIAZHU);
			}	

		}
		_gametableUi->setGameBeginTipsVisible(false);
		_gametableUi->hideqiangZhuangTips();
	}

	void GameTableLogic::gameStation_GS_SEND_CARD(void* object, INT objectSize)
	{
		CCAssert(sizeof(GameStation_SendCard) == objectSize, "Broken message GS_SEND_CARD.");
		auto ptr = (GameStation_SendCard*) object;
		SendAllCardStruct tempSendStrut;
		memcpy(tempSendStrut.iUserCard,ptr->iUserCard,sizeof(tempSendStrut.iUserCard));
		memcpy(tempSendStrut.iUserCardCount,ptr->iUserCardCount,sizeof(tempSendStrut.iUserCardCount));
		_sendCard=tempSendStrut;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (logicToViewSeatNo(i) == 0)
			{
				if (isPlaying(i))
				{
					_gametableUi->jinYongReturn(false);
				}
				else
				{
					_gametableUi->jinYongReturn(true);
				}
			}
			int value = logicToViewSeatNo(i);
			_iUserStation[value] = ptr->iUserStation[i];
			UserInfoStruct* userinfo=getUserByDeskStation(i);
			if (userinfo==nullptr)
			{
				continue;
			}

			if (i!=_mySeatNo)
			{
				BYTE cards[HANDCARD_COUNT] ={0};
				_gametableUi->sendCardState(value,cards);


			}
			else
			{	
				BYTE cards[HANDCARD_COUNT] ={0};
				memcpy(cards,ptr->iUserCard[i],sizeof(cards));
				//cards[4]=0;
				_gametableUi->sendCardState(value,cards);
				
			}

		}
	
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			UserInfoStruct* userinfo = getUserByDeskStation(i);
			if (userinfo == nullptr)
			{
				continue;
			}

			if (!ptr->bReadyOpenCard[i] && isPlaying(i))
			{
				if (logicToViewSeatNo(i) == 0)
				{
					_gametableUi->showTanPai(true);
				}
			}

			if (ptr->bReadyOpenCard[i] )
			{
				_gametableUi->showTanPaiSign(logicToViewSeatNo(i), true);
			}
		}

		_gametableUi->showDealerMark(logicToViewSeatNo(ptr->byNtStation),true);
		_gametableUi->setGameBeginTipsVisible(false);
	}

	void GameTableLogic::gameStation_GS_OPEN_CARD(void* object, INT objectSize)
	{
		CCAssert(sizeof(GameStation_OpenCard) == objectSize, "Broken message GS_OPEN_CARD.");

		auto ptr = (GameStation_OpenCard*) object;
		SendAllCardStruct tempSendStrut;
		memcpy(tempSendStrut.iUserCard,ptr->iUserCard,sizeof(tempSendStrut.iUserCard));
		memcpy(tempSendStrut.iUserCardCount,ptr->iUserCardCount,sizeof(tempSendStrut.iUserCardCount));
		_sendCard=tempSendStrut;
		_gametableUi->setGameBeginTipsVisible(false);
		_gametableUi->showDealerMark(logicToViewSeatNo(ptr->byNtStation),true);
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			
			UserInfoStruct* userinfo=getUserByDeskStation(i);
			if (userinfo==nullptr)
			{
				continue;
			}
			int value = logicToViewSeatNo(i);
			if (i!=ptr->byNtStation)
			{
				_gametableUi->updateNote(value, ptr->i64PerJuTotalNote[i]);
			}
			_iUserStation[value] = ptr->iUserStation[i];
			bool hasOpen=false;
			if (ptr->iUserStation[i]==3)
			{
				hasOpen=true;
			}
			if (i!=_mySeatNo)
			{
				if (!hasOpen)
				{
					BYTE cards[HANDCARD_COUNT] ={0};
					_gametableUi->updateHandcard(cards, value,false);
				}
				else
				{
				    _gametableUi->updateHandcard(ptr->iUserCard[i], value,false);
				}
				
			}
			else
			{	BYTE cards[HANDCARD_COUNT] ={0};
			    memcpy(cards,ptr->iUserCard[i],sizeof(cards));
				if(!hasOpen)
				{
				
					//cards[4]=0;
			    }
			  _gametableUi->updateHandcard(cards, value,false);
			
			}
		
			if(!hasOpen)
			{
				continue;
			}
			UserTanPai open;
			open.byDeskStation=i;
			open.iShape=ptr->iOpenShape[i];
			open.byUnderCount=ptr->byOpenUnderCount[i];
			
			memcpy(open.byUpCard,ptr->byOpenUpCard[i],sizeof((open.byUpCard)));
			memcpy(open.byUnderCard,ptr->byOpenUnderCard[i],sizeof((open.byUnderCard)));
			_gametableUi->showBull(open);
			//_gametableUi->showTanPai(true);
			if (logicToViewSeatNo(i) == 0)
			{

				if (isPlaying(i))
				{
					_gametableUi->jinYongReturn(false);
				}
				else
				{
					_gametableUi->jinYongReturn(true);
				}
			}

		}
		

	}

	BYTE GameTableLogic::getCardPoint(BYTE card)
	{
		if (card == 0x00)
			return 0;
		switch (getCardNum(card))
		{
		case 10:
			return 10;
		case 11:
			return 10;
		case 12:
			return 10;
		case 13:
			return 10;
		case 14:
			return 1;
		case 15:
			return 10;
		case 16:
			return 10;
		default:
			return getCardNum(card);
		}
	}

	

	UserTanPai GameTableLogic::getBull()
	{
		return getBull(_sendCard.iUserCard[_mySeatNo]);
	}
	//»ñµÃËùÓÐÈËµÄÅÆ
	UserTanPai GameTableLogic::getAllBull(BYTE seatId)
	{
		BYTE deskNo = viewToLogicSeatNo(seatId);

		return getBull(_sendCard.iUserCard[deskNo]);
	}
	UserTanPai GameTableLogic::getBull(BYTE cardlist[])
	{
		UserTanPai bull;

		BYTE totalpoint = 0;
		for (BYTE i = 0; i < HANDCARD_COUNT; i++)
		{
			totalpoint += getCardPoint(cardlist[i]);
		}

		for (int i = 0; i < 3; i++)
			for (int j = i + 1; j < 4; j++)
				for (int k = j + 1; k < HANDCARD_COUNT; k++)
				{
					BYTE temp = getCardPoint(cardlist[i]) + getCardPoint(cardlist[j]) + getCardPoint(cardlist[k]);
					if (temp % 10 == 0)
					{
						HNLOG("i= %d, j= %d, k= %d", i, j, k);
						bull.byUnderCard[0] = cardlist[i];
						bull.byUnderCard[1] = cardlist[j];
						bull.byUnderCard[2] = cardlist[k];

						for (int x = 0, y = 0; x < HANDCARD_COUNT; x++)
							if (x == i || x == j || x == k) continue;
							else
								bull.byUpCard[y++] = cardlist[x];

						bull.byDeskStation = _mySeatNo;
						bull.iShape = totalpoint % 10 == 0 ? 10 : totalpoint % 10;
						bull.byUnderCount = 3;
					}
				}

				return bull;
	}



	void GameTableLogic::dealUserCutMessageResp(INT userId, BYTE seatNo)
	{
		_gametableUi->showOffLine(logicToViewSeatNo(seatNo));

	}

	
	void GameTableLogic::sendSuperControl(BYTE bWinSaat,BYTE bLoseSeat)
	{
		SuperUserSetData pSuperControl;
		pSuperControl.byMaxDesk=bWinSaat;
		pSuperControl.byMinDesk=bLoseSeat;

		RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_SUPER_USER_SET, &pSuperControl, sizeof(pSuperControl));

	}

	void GameTableLogic::dealSuperSetResult(void* object, INT objectSize)
	{

		HNLOG("dealSuperSetResult");
		CCAssert(sizeof(SuperUserSetData) == objectSize, "Broken message dealSuperSetResult.");
		auto pSuperReult=(SuperUserSetData*)object;
		_gametableUi->showContolSuccese(pSuperReult->bSetSuccese);
	}

	void GameTableLogic::dealOpenCardFinish(void* object, INT objectSize)
	{
		int pResultCount=_openResultVec.size();
		float pDelayTime=0.3f;
		for (int i=_mySeatNo;i<PLAY_COUNT+_mySeatNo;i++)
		{
			int pSeanNo=i%PLAY_COUNT;
			if (!getUserByDeskStation(pSeanNo))
			{
				continue;
			}
			for (int j=0;j<pResultCount;j++)
			{
				UserTanPai openResnlt=_openResultVec.at(j);
				if (openResnlt.byDeskStation==pSeanNo)
				{
					
					BYTE pViewSeat=logicToViewSeatNo(openResnlt.byDeskStation);
					_gametableUi->updateHandCardWithAction(_sendCard.iUserCard[openResnlt.byDeskStation],pViewSeat,openResnlt,pDelayTime);
					pDelayTime+=0.6f;
				}
			}

		}
	}

	void GameTableLogic::saveOpenCardData(UserTanPai* pTanPai)
	{
		UserTanPai pOpenCard;
		memcpy(pOpenCard.bReadyOpenCard,pTanPai->bReadyOpenCard,sizeof(pOpenCard.bReadyOpenCard));
		memcpy(pOpenCard.byUnderCard,pTanPai->byUnderCard,sizeof(pOpenCard.byUnderCard));
		memcpy(pOpenCard.byUpCard,pTanPai->byUpCard,sizeof(pOpenCard.byUpCard));
		pOpenCard.byDeskStation=pTanPai->byDeskStation;
		pOpenCard.byUnderCount=pTanPai->byUnderCount;
		pOpenCard.iShape=pTanPai->iShape;

		_openResultVec.push_back(pOpenCard);
	}

	void GameTableLogic::clearDesk()
	{

	}

	void GameTableLogic::dealUserSitResp(MSG_GR_R_UserSit * userSit, const UserInfoStruct* user)
	{
		auto deskUser = getUserByUserID(user->dwUserID);
		bool isMe = (user->dwUserID == RoomLogic()->loginResult.pUserInfoStruct.dwUserID);

		auto info = getUserByUserID(userSit->dwUserID);

		_gametableUi->loadUser(logicToViewSeatNo(userSit->bDeskStation), *info);
		_gametableUi->showVipTip(logicToViewSeatNo(userSit->bDeskStation), *info);
		
		if (isMe)
		{
			LoadingLayer::removeLoading(Director::getInstance()->getRunningScene());

			BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				_gametableUi->removeUser(i);
			}
			
			//if (userSit->bDeskMaster) _gametableUi->showMaster(seatNo, true);
		}
		else
		{
			BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
			//_uiCallback->addUser(seatNo);

			// ÏÔÊ¾·¿Ö÷±êÖ¾
			if (userSit->bDeskMaster) _gametableUi->showMaster(seatNo, true);
		}

	}


	//
	void GameTableLogic::dealUserInfoChangeResp(const UserInfoStruct* user)
	{


		auto deskUser = getUserByUserID(user->dwUserID);
		if (deskUser)
		{
			float fMoney = (float)user->i64Money;
			BYTE seatNo = logicToViewSeatNo(deskUser->bDeskStation);
			//_gametableUi->showUserProfit(seatNo, fMoney);
		}
	}

	bool GameTableLogic::isPlaying(BYTE seat)
	{
		auto info = getUserByDeskStation(seat);
		if (info != nullptr && (info->bUserState == USER_CUT_GAME || info->bUserState == USER_PLAY_GAME))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void GameTableLogic::dealUserUpResp(MSG_GR_R_UserUp * userSit, const UserInfoStruct* user)
	{

		if (nullptr == user) return;
		HNGameLogicBase::dealUserUpResp(userSit, user);

		//TODO
		BYTE seatNo = logicToViewSeatNo(userSit->bDeskStation);
		_gametableUi->removeUser(seatNo);

		if (userSit->bDeskMaster) _gametableUi->showMaster(seatNo, false);

		if (INVALID_DESKSTATION == _mySeatNo)
		{
			stop();
// 			if (RoomLogic()->getRoomRule() & GRR_GAME_BUY
// 				|| RoomLogic()->getRoomRule() & GRR_QUEUE_GAME
// 				|| RoomLogic()->getRoomRule() & GRR_CONTEST
// 				|| RoomLogic()->getRoomRule() & GRR_TIMINGCONTEST)
			{
				ArmatureDataManager::getInstance()->removeArmatureFileInfo("platform/biyingqipai/biyingqipai.ExportJson");
				RoomLogic()->close();
				GamePlatform::createPlatform();
			}
// 			else
// 				_gametableUi->leaveDesk(false);
		}

	}

	void GameTableLogic::dealUserTanpai(void* object, INT objectSize)
	{
		HNLOG("ASS_USER_TANPAI");
		CCAssert(sizeof(UserTanPai) == objectSize, "Broken message ASS_USER_TANPAI.");
		
		UserTanPai *pUser = (UserTanPai *)object;


		int viewSeat = logicToViewSeatNo(pUser->byDeskStation);
		if (isPlaying(pUser->byDeskStation))
		{
			_gametableUi->showTanPaiSign(viewSeat, true);
		}
		


	}

	void GameTableLogic::dealUserWait(void* object, INT objectSize)
	{

		HNLOG("ASS_CALL_WAIT");
		CCAssert(sizeof(gameModel) == objectSize, "Broken message ASS_CALL_WAIT.");

		gameModel *pUser = (gameModel *)object;
		_iGameType = pUser->gameModelData;
		_gametableUi->initParams();
		_gametableUi->jinYongReturn(false);
		_gametableUi->playGameStartAni();

	}

	void GameTableLogic::sendStandUp()
	{
		do
		{
// 			if (!RoomLogic()->isConnect())
// 			{
// 				_gametableUi->leaveDesk(false);
// 				break;
// 			}
// 
// 			if (INVALID_DESKNO == _mySeatNo)
// 			{
// 				_gametableUi->leaveDesk(false);
// 				break;
// 			}
// 
// 			UserInfoStruct* myInfo = getUserBySeatNo(_mySeatNo);
// 			if (myInfo != nullptr && (_gameStation == GS_PLAY_GAME || _gameStation == GS_SEND_CARD) && (_UserState[_mySeatNo] != STATE_ERR))
// 			{
// 				GamePromptLayer::create()->showPrompt(GBKToUtf8("ÓÎÏ·ÖÐ£¬²»ÄÜÀë¿ª"));
// 				break;
// 			}
			HNGameLogicBase::sendUserUp();
		} while (0);
	}

	void GameTableLogic::dealVoiceMessage(NetMessageHead* messageHead, void* object, INT objectSize)
	{
		switch (messageHead->bAssistantID)
		{
		case ASS_GR_VOIEC:
		{
			//Ð§ÑéÊý¾Ý
			CCAssert(sizeof(VoiceInfo) == objectSize, "VoiceInfo size is error.");
			VoiceInfo* voiceInfo = (VoiceInfo*)object;
			auto userInfo = getUserByUserID(voiceInfo->uUserID);
			if (!userInfo) return;
			if (userInfo->bDeskNO == _deskNo)
			{
				_gametableUi->onChatVoiceMsg(voiceInfo->uUserID, voiceInfo->uVoiceID, voiceInfo->iVoiceTime);
			}
		}
			break;
		default:
			break;
		}
	}

	void GameTableLogic::dealUserChatMessage(MSG_GR_RS_NormalTalk* normalTalk)
	{
		auto user = UserInfoModule()->findUser(normalTalk->dwSendID);
		_gametableUi->onChatTextMsg(user->bDeskStation, normalTalk->szMessage);
	}

	BYTE GameTableLogic::getGameType()
	{
		return _iGameType;
	}

	UserInfoStruct* GameTableLogic::getUserInfo(int seat)
	{
		UserInfoStruct* info = getUserByDeskStation(seat);
		return info;
	}

	void GameTableLogic::dealGameClean()
	{

	}
	void GameTableLogic::dealMatchRoundNum(MSG_GR_ConTestRoundNum * contestRoundNum)
	{

	}
	void GameTableLogic::dealMatchInnings(MSG_GR_ConTestRoundNumInOneRound * contestRoundNum)
	{

	}
	void GameTableLogic::dealMatchInfoupdate(MSG_GM_S_ConTestGameInfo * contestRoundNum)
	{

	}

}
