#include "json/rapidjson.h"
#include "json/document.h"
#include "HNNetExport.h"
#include "NN8GameTableUI.h"
#include "NN8GameTableLogic.h"
#include "NN8GameUserMessageBox.h"
#include "NN8PokerList.h"
#include "NN8CardSuite.h"
#include "NN8NodeList.h"
#include "NN8PlayerUI.h"
#include "NN8SuperSet.h"
#include "HNUIExport.h"
#include "HNLobbyExport.h"
#include "NN8CalculateNode.h"
#include "NN8SetLayer.h"



namespace NN8
{
	const static int MEMBAN_ZORDER = 300;
	const static float OFFX = 35.0;
	const static float OFFY = 20.0;
	const static float OFFX_FirstView = 133.0;
	const static float OFFY_FirstView = 60.0;
	const static int Multiple[5] = {1 ,5 ,10 ,15 ,20 };
	string LongToString(LLONG number)
	{
		char s[30];
		sprintf(s, "%lld", number);
		string str(s);

		return str;
	}

	GameTableUI::GameTableUI() :
		_suite(nullptr),
		_roomController(nullptr),
		_tableLogic(nullptr),
		_startflag(false),
		_customize(false),
		_zhuangSeat(-1),
		_chatCommon(nullptr),
		_calculate(nullptr),
		_playDengActCount(0),
		_maxGameCount(0),
		_gameFinish(false),
		_pIsGudingYaFen(false),
		_clockTime(0),
		_calCount(0)
	{
		memset(&_canvas, 0, sizeof(_canvas));
		memset(_players, 0, sizeof(_players));
		memset(_handcard, 0, sizeof(_handcard));
		memset(_userScore, 0, sizeof(_userScore));
		memset(_resultMoney, 0, sizeof(_resultMoney));
		SpriteFrameCache* cache = SpriteFrameCache::getInstance();
		cache->addSpriteFramesWithFile("Games/NN8/poker/cards.plist");

	}

	GameTableUI::~GameTableUI()
	{
		HN_SAFE_DELETE(_tableLogic);
		SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("Games/NN8/poker/cards.plist");
	}

	HNGameUIBase* GameTableUI::create(INT deskId, bool autoCreate)
	{
		GameTableUI* tableUI = new GameTableUI();
		if (tableUI->init(deskId, autoCreate))
		{
			tableUI->autorelease();
			return tableUI;
		}
		else
		{
			CC_SAFE_DELETE(tableUI);
			return nullptr;
		}
	}

	bool GameTableUI::init(INT deskId, bool autoCreate)

	{
		if (!HNLayer::init())
		{
			return false;
		}

		HNAudioEngine::getInstance()->playBackgroundMusic("NN8/audio/nn/nn_bg_music.mp3",true);
		_deskNo = deskId;
		Size winSize = Director::getInstance()->getWinSize();

		
// 		_calculate = CalculateNode::create();
// 		addChild(_calculate, 200);
// 		_calculate->setPosition(_winSize / 2 + Size(0, -115));
// 		_calculate->setVisible(false);

		if (PlatformLogic()->loginResult.iVipLevel > 0)
		{
			std::string str = "欢迎尊贵的Vip";
			str.append(StringUtils::toString(PlatformLogic()->loginResult.iVipLevel));
			str.append("玩家：");
			str.append(PlatformLogic()->loginResult.nickName);
			str.append(",进入房间！");
			GameNotice::getInstance()->postMessage(GBKToUtf8(str.c_str()),0);
		}

		// initial table tableBg
		auto tableNode = CSLoader::createNode("NN8/table/TableNode2.csb");
		_canvas.tableBg = (ImageView*) tableNode->getChildByName("table_img");
		_canvas.renshu = static_cast<Text*>(_canvas.tableBg->getChildByName("Text_renshu"));
//		_canvas.renshu->setVisible(true);
		_canvas.renshu->setPosition(Vec2(100,720/2+330));
		
// 		_BottonScore = Sprite::create("platform/common/df.png");
// 		_BottonScore->setPosition(Vec2(50, 720 / 2 + 330 ));
// 		_canvas.tableBg->addChild(_BottonScore);


		auto tipTime = Sprite::create("platform/common/tip.png");
		tipTime->setPosition(Vec2(1280 / 2, 720 / 2));
		_canvas.tableBg->addChild(tipTime);

		auto Seque = Sequence::create(ScaleTo::create(1.0f, 1.1f), ScaleTo::create(1.0f, 0.9f), NULL);
		auto ani = RepeatForever::create(Seque);
		tipTime->runAction(ani);
			
		this->runAction(Sequence::create(DelayTime::create(4), CallFunc::create([=](){
			tipTime->setVisible(false);
			tipTime->stopAction(ani);
		}),nullptr));


		VipRoomController* roomController = nullptr;
		//初始化VIP房间控制器
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			roomController = VipRoomController::create(deskId);
			_roomController = roomController;
			roomController->onSetVipInfoCallBack = [this](){
				// 更新房主标志显示
				setDeskInfo();
			};
			roomController->onDissmissCallBack = [this]() {

				if (!_allRoundsEnd)
				{		
					auto prompt = GamePromptLayer::create();
			
					prompt->showPrompt(GBKToUtf8("桌子已解散"));
					prompt->setCallBack([=]() {
						RoomLogic()->close();
						GamePlatform::createPlatform();
					});
				}
			};

			addChild(roomController, Max_Zorder);

			// 修改房间号位置
			roomController->setRoomNumPos(Vec2(15.0f, _winSize.height - 25.0f));
			// 修改局数位置
			roomController->setPlayCountPos(Vec2(15.0f, _winSize.height - 50.0f));
 			roomController->setInvitationBtnTexture("NN8/table/yaoqinghaoyou.png");
// 			// 修改邀请好友位置
 			//roomController->setInvitationBtnPos(_canvas.reday->getPosition() - Vec2(190, 0));
 			// 修改返回位置 影藏
 			roomController->setReturnBtnPos(Vec2(_winSize.width + 500.0f, 0));
 			// 修改解散位置 影藏
 			roomController->setDismissBtnPos(Vec2(_winSize.width + 500.0f, 0));

			roomController->setRoomNumPos(Vec2(10, 700));

			roomController->setPlayCountPos(Vec2(10, 670));

			roomController->setDifenPos(Vec2(10, 640));
		}

		gameChatLayer();

		_canvas.clock = (ImageView*)_canvas.tableBg->getChildByName("clock");
		_canvas.second = dynamic_cast<TextAtlas*>(ui::Helper::seekWidgetByName(_canvas.clock, "AtlasLabel_second"));

		_canvas.fangjianHao = dynamic_cast<Text*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "Text_fh"));
		_canvas.fangjianHao->setString(StringUtils::format("%d",deskId));
		_canvas.jushu = dynamic_cast<Text*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "Text_jushu"));
		_canvas.moshi = dynamic_cast<Text*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "Text_ms"));
		//_canvas.renshu = dynamic_cast<Text*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "Text_renshu"));
		_canvas.yafen = dynamic_cast<Text*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "yfms"));
		_canvas.kejin = dynamic_cast<Text*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "ztkj"));
		_canvas.dealer = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "dealer_card_btn"));

		_canvas.menu=dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "menu_btn"));
		_canvas.setting=dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "setting_btn"));
		_canvas.exitBt=dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "exit_btn"));

		_canvas.gameBegintips=dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "Image_gamebegin"));
		_canvas.gameTips=dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "Image_tips"));
		_canvas.btnreturn = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "btn_return"));
		_canvas.btnTanPai = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "btn_tanpai"));		_canvas.btnreturn->setZOrder(9999);
		_canvas.btnreturn->addClickEventListener([&](Ref*) {
			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
			_tableLogic->stop();
			RoomLogic()->close();
			GamePlatform::returnPlatform(ROOMLIST);
			});

		_canvas.btnTanPai->addClickEventListener([&](Ref*) {
			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
			_canvas.btnTanPai->setVisible(false);
			_canvas.clock->setVisible(false);
			_gameState = CLOCK_NONE;
			unschedule(schedule_selector(GameTableUI::callScoreClock));
			_handcard[0]->setTouchable(false);
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_USER_TANPAI,0,0);

		});

		_canvas.chat = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "btn_chat"));
		_canvas.voice = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "btn_voice"));
		_canvas.set = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "btn_setting"));
		_canvas.set->setPosition(Vec2(_canvas.set->getPosition().x-90, _canvas.set->getPosition().y + 7));
		_canvas.setting->setVisible(false);

		_canvas.set->addClickEventListener(CC_CALLBACK_1(GameTableUI::clickSettingCallBack, this));


		_canvas.chat->addTouchEventListener(CC_CALLBACK_2(GameTableUI::chatBtnClickCallBack, this));
		_canvas.voice->addTouchEventListener([this](Ref* pSender, Widget::TouchEventType type){
			if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
			{
				auto prompt = GamePromptLayer::create();
				prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
				return;
			}
			if (_chatLayer == nullptr)
			{
				//显示聊天界面
				gameChatLayer();
			}
			_chatLayer->voiceChatUiButtonCallBack(pSender, type);
		});
		//btn_voice->setVisible(RoomLogic()->getRoomRule()&GRR_GAME_BUY);


		auto btn_rule = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "btn_rule"));
		btn_rule->setPosition(Vec2(btn_rule->getPosition().x, btn_rule->getPosition().y + 7));
		btn_rule->setVisible(false);
		auto tableRule = CSLoader::createNode("NN8/table/GameRule.csb");
		tableRule->setZOrder(10000);
		tableRule->setPosition(Vec2(640,360));
		this->addChild(tableRule);
		auto panel_rule = (Layout*)tableRule->getChildByName("Panel_rule");
		panel_rule->setVisible(false);
		auto img_rulebg = (ImageView*)panel_rule->getChildByName("Image_rule");
		auto btn_close = (Button*)img_rulebg->getChildByName("Button_close");
		btn_close->addClickEventListener([=](Ref*){
			panel_rule->setVisible(false);
		});
		btn_rule->addClickEventListener([=](Ref*){
			panel_rule->setVisible(false);
		});

// 		auto rule = dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(panel_rule, "Image_rule"));
// 		rule->setZOrder(30000);
// 		auto close_rule = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(rule, "Button_close"));
// 		close_rule->addClickEventListener([&](Ref*){
// 			auto rule = dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(panel_rule, "Image_rule"));
// 			rule->setVisible(false);
// 			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
// 			Node* node = getChildByName("ResultShow");
// 			if (node)
// 			{
// 				node->setVisible(true);
// 			}
// 		});


		
		auto btn_set = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "btn_set"));
		btn_set->addClickEventListener([&](Ref*){
			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
			auto setLayer = GameSetLayer::create();
			setLayer->setName("setLayer");
			setLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 30000);
		});

		_canvas.menu->addTouchEventListener(CC_CALLBACK_2(GameTableUI::tableMenuCallback, this));
		_canvas.setting->addTouchEventListener(CC_CALLBACK_2(GameTableUI::tableMenuCallback, this));
		_canvas.exitBt->addTouchEventListener(CC_CALLBACK_2(GameTableUI::tableMenuCallback, this));

		_canvas.reday=dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "reday_btn"));
		_canvas.reday->addTouchEventListener(CC_CALLBACK_2(GameTableUI::startClickCallback, this));

		_canvas.btnSuperSet=dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "super_btn"));
		_canvas.btnSuperSet->addTouchEventListener(CC_CALLBACK_2(GameTableUI::gmBtClickCallback, this));

		_canvas.invite=dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "yaoqing_btn"));
		_canvas.invite->addTouchEventListener(CC_CALLBACK_2(GameTableUI::clickWebchatEventCallback, this));
		

		_canvas.qiangzhuangBt = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "request_btn"));
		_canvas.buqiangBt = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, "abandon_btn"));
		_canvas.qiangzhuangBt->addTouchEventListener(CC_CALLBACK_2(GameTableUI::requestDealerCallback, this));
		_canvas.buqiangBt->addTouchEventListener(CC_CALLBACK_2(GameTableUI::buxiangCallback, this));

		_canvas.qiangzhuangBt->setVisible(false);
		_canvas.buqiangBt->setVisible(false);

		for (int i=0;i<PLAY_COUNT;i++)
		{
			_canvas.seats[i]   = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, StringUtils::format("player%d_btn",i+1)));
			_canvas.hand[i]    = dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(_canvas.tableBg, StringUtils::format("handcard%d",i+1)));
			_state[i] = 0;
		}

		for (int i=0;i<5;i++)
		{
			_canvas.beiBt[i]   = dynamic_cast<Button*>(ui::Helper::seekWidgetByName(_canvas.tableBg, StringUtils::format("bei_btn%d",i+1)));
			_canvas.beiBt[i]->setTag(i);
	

			_canvas.beiBt[i]->addTouchEventListener(CC_CALLBACK_2(GameTableUI::noteMenuCallback, this));
			
		}


		tableNode->setPosition(winSize/2);
		this->addChild(tableNode, 1);
		//战绩按钮
		auto btn_Record = dynamic_cast<Button*>(_canvas.tableBg->getChildByName("btn_record"));
		btn_Record->setPositionY(btn_set->getPositionY()+7);
		btn_Record->addClickEventListener([=](Ref* ref) {
			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, S_C_GAME_RECORD, 0, NULL);

		});
		

		/////////////////////////////////////
		// seats
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_canvas.seats[i]->setEnabled(false);
			_canvas.hand[i]->setVisible(false);
			_canvas.kuangBtn[i] = (Button*)_canvas.seats[i]->getChildByName("Button_kuang");
			_canvas.kuangBtn[i]->setZOrder(1997);
			_canvas.zhuangjia[i] = (ImageView*)_canvas.seats[i]->getChildByName("userzhuang");
			_canvas.zhuangjia[i]->setZOrder(1998);
			_canvas.qiangzhuangTip[i] = (ImageView*)_canvas.seats[i]->getChildByName("qzTips");
		}

		/////////////////////////////////////
		// handcard
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			float pTempOffX=0.0;
			float pTempOffY=0.0;
			float pScle=0.8;
			if (i==0)
			{
				pTempOffX=OFFX_FirstView;
				pTempOffY=OFFY_FirstView;
				pScle=1.3;
			}
			if (i>0&&i<=5)
			{
				pTempOffX=OFFX;
				pTempOffY=OFFY;
			}
			if (i>=6)
			{
				pTempOffX=-OFFX;
				pTempOffY=-OFFY;
			}
	

			if (i!=0)
			{
				_handcard[i] = NNList::create(Vec2(pTempOffX, pTempOffY));
				_handcard[i]->setCardScle(0.55f);
				Vec2 handpos = _canvas.hand[i]->getParent()->convertToWorldSpace(_canvas.hand[i]->getPosition());
				_handcard[i]->setPosition(this->convertToNodeSpace(handpos).x-200, this->convertToNodeSpace(handpos).y+40);
				_handcard[i]->setTabelUI(this);
				_canvas.tableBg->addChild(_handcard[i], 1);
			}
			else
			{
				_handcard[i] = NNList::create(Vec2(35, 20));
				_handcard[i]->setCardScle(0.55f);
				Vec2 handpos = _canvas.hand[i]->getParent()->convertToWorldSpace(_canvas.hand[i]->getPosition());
				_handcard[i]->setPosition(this->convertToNodeSpace(handpos).x - 200, this->convertToNodeSpace(handpos).y + 40);
				_handcard[i]->setTabelUI(this);
				_canvas.tableBg->addChild(_handcard[i], 1);
				
			}

		}
	

		

	

		/////////////////////////////////////
		// init card suite
		_suite = CardSuite::create(0.5f);
		_suite->setPosition(_canvas.dealer->getPosition());
		_canvas.dealer->getParent()->addChild(_suite, 100);


		/////////////////////////////////////
		// init game table logic
		int roomID = 0;
		ComRoomInfo* pInfo = RoomLogic()->getInstance()->getSelectedRoom();
		if (pInfo)
		{
			roomID =  pInfo->uRoomID;
		}

		_tableLogic = new GameTableLogic(this, deskId,autoCreate);
		_tableLogic->enterDesk();

		initDissolveRoomOpervation();
// 		//手牌触摸事件
// 		auto MyListener = EventListenerTouchOneByOne::create();
// 		MyListener->setSwallowTouches(true);//阻止触摸向下传递
// 		MyListener->onTouchBegan = [this](Touch* touch, Event* event)
// 		{
// 			if (_handcard[0]->getBoundingBox().containsPoint(touch->getLocation()))
// 			{
// 				int Txt_Num=GetPoint(GetCardNum(_cardMySeat[0]));
// 				_calculate->getTextVec().at(0);
// 				
// 			}
// 			return true;
// 		};
// 		MyListener->onTouchMoved = [this](Touch* touch, Event* event)
// 		{
// 		
// 		};
// 		MyListener->onTouchEnded = [this](Touch* touch, Event* event)
// 		{
// 			
// 		};
// 		_eventDispatcher->addEventListenerWithSceneGraphPriority(MyListener);
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			UserInfoStruct* userinfo = _tableLogic->getUserBySeatNo(i);
			if (userinfo)
			{
				_resultMoney[i] = userinfo->i64Money;
			}
		}
		closeClock();
		showTanPai(false);
		return true;
	}

	//显示总结算界面
	void GameTableUI::ShowAllResult(GameEndInfoStruct & cal, bool sh)
	{
		_allRoundsEnd = true;
		Node* pRootNode=Director::getInstance()->getRunningScene();
		Size winSize=Director::getInstance()->getWinSize();
		AllResultShow * allResult=AllResultShow::create(winSize/2);
		allResult->setName("AllResultShow");
		pRootNode->addChild(allResult,10000000);
		allResult->showAllResult(&cal, _tableLogic);
		if (_gameFinish)
		{
			allResult->setVisible(false);
		}
	}
	void GameTableUI::showWechat(bool visible)
	{
		_canvas.invite->setVisible(visible);
		
#if CC_TARGET_PLATFORM  != CC_PLATFORM_WIN32

#endif

		checkInviAndReadyPos();
	}

	void GameTableUI::startClickCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED == touchtype)
		{
			closeClock();
			showTanPai(false);
			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");

			do
			{
				if (pSender == _canvas.reday)
				{
					_tableLogic->sendAgreeGame();
					_canvas.reday->setVisible(false);
					_handcard[0]->setTouchable(true);
					checkInviAndReadyPos();
					break;
				}
			} while (false);
		}
	}


	void GameTableUI::render_ASS_CALL_SCORE(bool show)
	{
		_canvas.clock->setVisible(show);
		if (show)
		{
			showRunTiom(12,CLOCK_QIANGZHUANG);
		}
		else
		{
			_gameState = CLOCK_NONE;
		}
		_canvas.qiangzhuangBt->setVisible(show);
		_canvas.buqiangBt->setVisible(show);
		if (show)
		{
			_canvas.reday->setVisible(false);
			_canvas.invite->setVisible(false);

		}
		setAllUserRedayUnVisble();
	}

	void GameTableUI::requestDealerCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED == touchtype)
		{

			HNAudioEngine::getInstance()->playEffect("Games/NN8/audio/nn/qiangzhuang.mp3");

			_tableLogic->sendCallScore(pSender == _canvas.qiangzhuangBt);

			_canvas.qiangzhuangBt->setVisible(false);
			_canvas.buqiangBt->setVisible(false);
			_canvas.clock->setVisible(false);
			unschedule(schedule_selector(GameTableUI::callScoreClock));
		}
	}

	void GameTableUI::showhandCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED != touchtype) return ;
		HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
		
		
	}

	void GameTableUI::tableMenuCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED == touchtype)
		{

			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
			do
			{
				if (pSender == _canvas.exitBt)
				{
					
					

					break;
				}

				if (pSender ==_canvas.menu)
				{
					if (_canvas.exitBt->isVisible()&& _canvas.setting->isVisible())
					{
						_canvas.exitBt->setVisible(false);
						_canvas.setting->setVisible(false);
					}
					else
					{
						_canvas.exitBt->setVisible(true);
						_canvas.setting->setVisible(true);
					}
					break;
				}

				if (pSender == _canvas.setting)
				{

					break;
				}

			} while (false);
		}
	}


	void GameTableUI::noteMenuCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED == touchtype)
		{
			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");

			Button* bt=(Button*)pSender;
		
			int notetype =bt->getTag() ;
			
			_tableLogic->sendNote(notetype);
			
			for (auto n : _canvas.beiBt)
				n->setVisible(false);
		}
	}


	void GameTableUI::showVipTip(BYTE seat, UserInfoStruct &info)
	{
		if (info.iVipLevel > 0)
		{
			std::string str = "欢迎尊贵的Vip";
			str.append(StringUtils::toString(info.iVipLevel));
			str.append("玩家：");
			str.append(info.nickName);
			str.append(",进入房间！");
			GameNotice::getInstance()->postMessage(GBKToUtf8(str.c_str()),0);
		}
		
	}
	void GameTableUI::loadUser(BYTE seat, UserInfoStruct &info)
	{
		memset(_resultMoney, 0, sizeof(_resultMoney));
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* userinfo = _tableLogic->getUserBySeatNo(i);
			if (userinfo)
			{
				_resultMoney[i] = userinfo->i64Money;
			}
		}

		BYTE logicSeat=_tableLogic->viewToLogicSeatNo(seat);
		if (_players[seat] != nullptr)
		{
			_players[seat]->loadUser(info);	

			UserTanPai bull;
		    bull = _tableLogic->getAllBull( seat);
			_players[seat]->setUIVisble(true);
		}
		else
		{
			
			_players[seat] = PlayerUi::create(info, _canvas.seats[seat], seat);
			// add the player to the seat
			Size size = _canvas.seats[seat]->getContentSize();
			Vec2 pos = _canvas.seats[seat]->getPosition();
			

			// hide the seat button
			_canvas.seats[seat]->setVisible(true);
			if (info.bUserState==USER_ARGEE)
			{
				userRedayShow(seat,true);
			}
			if (info.bUserState==USER_CUT_GAME)
			{
				showOffLine(seat);
			}
			_canvas.tableBg->addChild(_players[seat]);
		


		}

		//_players[seat]->upDateUserMoney(_userScore[logicSeat]);
		//_players[seat]->setOwner(info.dwUserID == HNPlatformConfig()->getMasterID());
	}


	void GameTableUI::showUserUp(BYTE seatId)
	{
		if (_players[seatId] != nullptr)
		{
			

			if (_tableLogic->isPlaying(_tableLogic->viewToLogicSeatNo(seatId)))
			{
				_players[seatId]->showOffLine();
			}
			else
			{
				_players[seatId]->removeListener();
				_players[seatId]->removeFromParent();
				_players[seatId] = nullptr;
				_handcard[seatId]->clear();
				_canvas.seats[seatId]->setVisible(false);
			}

		}

	}









	void GameTableUI::callNote(BeginUpgradeStruct* noteoption)
	{
		if (_pIsGudingYaFen)
		{
			return;
		}

		if (noteoption == nullptr)
		{
			for (auto n : _canvas.beiBt)
				n->setVisible(false);
			_canvas.clock->setVisible(false);
			unschedule(schedule_selector(GameTableUI::callScoreClock));
			_gameState = CLOCK_NONE;
		}
		else
		{
			
			if (_tableLogic->logicToViewSeatNo(_zhuangSeat) != 0 || _tableLogic->getGameType() == 0)
			{
				_canvas.clock->setVisible(true);

				for (auto n : _canvas.beiBt)
					n->setEnabled(true);

				for (int i = 0; i < PLAY_COUNT;i++)
				{
					if (_tableLogic->logicToViewSeatNo(i) == 0)
					{
						for (int j = 4; j >= 0;j--)
						{
							if (_resultMoney[i] < (noteoption->i64UserNoteLimite[i][j]*300))
							{
								//取消金额不足无法下注
								//_canvas.beiBt[j]->setEnabled(false);
							}

							if (_resultMoney[_zhuangSeat] < (noteoption->i64UserNoteLimite[i][j] * 300))
							{
								//取消金额不足无法下注
								//_canvas.beiBt[j]->setEnabled(false);
							}
						}
					}
				}
				_canvas.beiBt[0]->setEnabled(true);
				int count = 0;
				int Multiplee = 0;
				for (auto n : _canvas.beiBt)
				{
					
					if (_dBottonScore != 0)
					{
						Multiplee = _MyselfScore / (3 * _dBottonScore);
						if (Multiplee >= Multiple[count])
							n->setVisible(true);
					}
					else
						n->setVisible(true);

					count++;
				}
					

				showRunTiom(12, CLOCK_XIAZHU);
			}
		}
	}

	void GameTableUI::updateNote(BYTE seatId, LLONG money)
	{
		if (  _players[seatId])
		{
			  _players[seatId]->showCallMark(money);
		}


	}


	void GameTableUI::playNoteAudio(BYTE seatId, BYTE audioType)
	{
		//_players[seatId]->playNoteAudio(audioType);
	}

	void GameTableUI::updateHandcard(BYTE cards[], BYTE seat, bool bOpenCard)
	{
		if (_suite)
		{
			_suite->stopAllActions();
			_suite->setEmptyState(true);
			_suite->recycle(0.1f);
			
		}
		BYTE logicSeat = _tableLogic->viewToLogicSeatNo(seat);
// 		if (_handcard[seat] != nullptr && _tableLogic->isPlaying(logicSeat) && seat == 0 &&_tableLogic->getGameStatus()== 0 &&bOpenCard == false)
// 		{
// 			cards[3] = 0;
// 		}
		if (_handcard[seat] != nullptr && _tableLogic->isPlaying(logicSeat))
		{
			_handcard[seat]->setValueList(cards, HANDCARD_COUNT);
		}
	}

	void GameTableUI::setDissloveBtState()
	{	
		
	}


	void GameTableUI::setGamecount(int counts,int maxCount)
	{

		if (_canvas.jushu)
		{
			if (counts+1<=maxCount)
			{
				
				_canvas.jushu->setString(StringUtils::format("%d/%d",counts+1,maxCount));
				_canvas.jushu->setVisible(true);
			}
			
		}

		if (maxCount-counts<=0)
		{
			_gameFinish=true;
			;
			

		}
		_maxGameCount=maxCount;
	}

	void GameTableUI::sendCard(SendAllCardStruct send)
	{
		///////////////////////////////////////
		// animate & data
		_canvas.clock->setVisible(false);
		unschedule(schedule_selector(GameTableUI::callScoreClock));
		_suite->shuffle(0.1f);
		Sequence* s = nullptr;
		for (BYTE handindex = 0; handindex < 5; handindex++)
		{		
			for (BYTE seatindex = 0; seatindex < PLAY_COUNT; seatindex++)
			{
				BYTE logiSeat = (seatindex + send.iStartPos) % PLAY_COUNT ;
				BYTE viewSeat = _tableLogic->logicToViewSeatNo(logiSeat);
				if (viewSeat == 0)
				{
					_cardMySeat[handindex] = send.iUserCard[logiSeat][handindex];
				}


				if (_players[viewSeat] != nullptr && _tableLogic->isPlaying(logiSeat))
				{
					std::string pHandCardName=StringUtils::format("handcard%d",viewSeat+1);
					auto receiver = _canvas.seats[viewSeat]->getChildByName(pHandCardName);
					auto tarpos = receiver->getParent()->convertToWorldSpace(Vec2(receiver->getPosition().x - 200, receiver->getPosition().y + 40));
					BYTE value = logiSeat == _tableLogic->getMySeatNo() ? send.iUserCard[logiSeat][handindex] : 0;
 					if ( logiSeat == _tableLogic->getMySeatNo()&&handindex==3&&_tableLogic->getGameType() == 0)
 					{
 						value=0;
 					}
				
					auto func = CallFunc::create([=]()
					{			
						HNAudioEngine::getInstance()->playEffect(AUDIO_RUNCRAD);
						_handcard[viewSeat]->add(value);
						
					});
					auto handaction = TargetedAction::create(receiver, func);
					auto suiteaction = _suite->dealCard(0.2f, tarpos);
					s = Sequence::create(s, suiteaction, nullptr);
					if (s == nullptr)
					{
						s = Sequence::create(suiteaction, handaction, nullptr);
					}
					else
					{
						s = Sequence::create(s, suiteaction, handaction, nullptr);
					}
				}
			}
		}

		_suite->runActionPair(_suite, s);
		_suite->recycle(0.1f);

	}





	void GameTableUI::showBull(UserTanPai bull)
	{
		auto seat = _tableLogic->logicToViewSeatNo(bull.byDeskStation);
		float pScle=0.95;
		if (seat==0)
		{
			pScle=0.95;
		}
		_handcard[seat]->showBull(bull,pScle);
		UserInfoStruct* info = _tableLogic->getUserByDeskStation(bull.byDeskStation);
		if (info)
		{
			_handcard[seat]->playBullAudio(bull.iShape, info->bBoy);

		}
		
	}

	void GameTableUI::deal_ASS_CONTINUE_END(GameEndStruct* end)
	{
		setStartBtnVisible(true);
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* userinfo=_tableLogic->getUserBySeatNo(i);
			if (userinfo==nullptr)
			{
				continue;;
			}
			
			int viewSeat=_tableLogic->logicToViewSeatNo(i);
			
			INT money=_userScore[i] + end->iTurePoint[i];
		//	_userScore[i] = money;
 			//_resultMoney[i] += money;

			_players[viewSeat]->upDateUserCoin(end->iChangeMoney[i]);

			if (viewSeat == 0 )
				_MyselfScore = end->iChangeMoney[i];


			if (viewSeat == 0)
			{
				if (end->iTurePoint[i] > 0)
				{
					HNAudioEngine::getInstance()->playEffect("NN8/audio/table/game_win.mp3");
				}
				else
				{
					HNAudioEngine::getInstance()->playEffect("NN8/audio/table/game_over.mp3");
				}
			}

			if (_tableLogic->isPlaying(i))
			{
				_players[viewSeat]->upDateUserMoney(end->iTurePoint[i]);
			}
	
	

		}

		
		auto pRuningScene =  Director::getInstance()->getRunningScene();

		//Size winSize=Director::getInstance()->getWinSize();
		
		//最后一局不显示
		if (!_allRoundsEnd)
		{
			ResultShow*	result = ResultShow::createResultShow(_winSize + Size(0, 100));
			result->setName("ResultShow");
			for (int i = 0; i < PLAY_COUNT; i++)
			{
				int viewSeat = _tableLogic->logicToViewSeatNo(i);
				if (_players[viewSeat])
				{
					_players[viewSeat]->setBetMoneyVisibel(false);
				}

				if (viewSeat == 0 && _tableLogic->isPlaying(i))
				{
					this->addChild(result, 100);
				}
				if (viewSeat == 0 && !_tableLogic->isPlaying(i))
				{
					_canvas.reday->setVisible(true);
				}

			}

			if (_tableLogic)
			{
				result->showResult(end, _zhuangSeat, _tableLogic);
			}

		}
    	
		//this->schedule(schedule_selector(GameTableUI::leaveDesk), 20.0f);

		

		

	}

	void GameTableUI::initParams()
	{
		
		_customize = false;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			_handcard[i]->clear();
			_handcard[0]->setTouchable(true);
			_canvas.zhuangjia[i]->setVisible(false);
			showTanPaiSign(i, false);
		}

		if (_suite)
		{
			_suite->stopAllActions();
			_suite->setEmptyState(true);
			_suite->recycle(0.1f);

		}
		//setAllUserRedayUnVisble();
		setGameBeginTipsVisible(false);
		hideqiangZhuangTips();
		hideBankerTipsAndBetMoneyTips();
	}

	void GameTableUI::showDealerMark(BYTE seat, bool visible)
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if (_canvas.seats[i]->isVisible())
			{
				showKuangBtnAction(i, (i == seat ? true : false) & visible);
				auto func = CallFunc::create([=](){
					_canvas.zhuangjia[i]->setVisible((i == seat ? true : false) & visible);
					_canvas.qiangzhuangTip[i]->setVisible(false);
					_canvas.kuangBtn[i]->setVisible(false);
				});
				_canvas.zhuangjia[i]->runAction(Sequence::createWithTwoActions(DelayTime::create(0.5f), func));
			}
		}
	}


	void GameTableUI::initDissolveRoomOpervation()
	{
		

	}
	void GameTableUI::setStartBtnVisible(bool visible)
	{
		if (!_startflag)
		{
			_canvas.reday->setVisible(visible);
			_startflag = true;
			checkInviAndReadyPos();
		}
		
	}

	void GameTableUI::openChatDialog()
	{
// 		Size winSize = Director::getInstance()->getWinSize();
// 		if (_chatCommon == nullptr)
// 		{
// 			_chatCommon = NN8ChatCommon::create();
// 			_chatCommon->setPosition(winSize.width/2-320,winSize.height/2-300);
// 			this->addChild(_chatCommon,10000);
// 		}
// 		_chatCommon->openMe();
// 		_chatCommon->onSpeakCallBack = [this](const std::string& text)
// 		{
// 			
// 		};
	}

	void GameTableUI::onSocketMessage(UINT MainID, UINT AssistantID,const rapidjson::Document& doc)
	{
		


	}

	void GameTableUI::clickWebchatEventCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{

	}

	void GameTableUI::setUserDefine(std::vector<int> define)
	{
		//
	}

	
	
	

	void GameTableUI::clickShouEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED!= touchtype)
		{
			return;
		}
	
		
	}

	void GameTableUI::clickZhanEventCallBack(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED!= touchtype)
		{
			return;
		}
		
	}

	void GameTableUI::setGameBeginTipsVisible(bool isvisible)
	{
		_canvas.gameBegintips->setVisible(false);
		
	}

	void GameTableUI::changTipshow(int tag)
	{
	
		if (tag>0)
		{
			//_canvas.gameTips->setVisible(true);
			std::string path=StringUtils::format("NN8/table/nn_satae%d.png",tag);
			//_canvas.gameTips->loadTexture(path);
			if (tag==XIKAO)
			{
			
				_canvas.gameTips->setVisible(false);
			}
			else
			{
				
			}
			

		}
		else
		{
			_canvas.gameTips->setVisible(false);
		}
		
	}

	void GameTableUI::userRedayShow(int seat, bool isReday)
	{
		if (_canvas.seats[seat])
		{
			ImageView* reday =(ImageView*)_canvas.seats[seat]->getChildByName("reday");
			reday->setVisible(isReday);

		}

	}

	void GameTableUI::setAllUserRedayUnVisble()
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
		
			if (_canvas.seats[i])
			{
				ImageView* reday =(ImageView*)_canvas.seats[i]->getChildByName("reday");
				reday->setVisible(false);
			}
			
		}
	}


	


	
	void GameTableUI::setBeiBtVisible(bool isvisble)
	{
		if (!isvisble)
		{
			for (auto n : _canvas.beiBt)
				n->setVisible(isvisble);
		}
		else
		{
			int count = 0;
			int Multiplee = 0;
			for (auto n : _canvas.beiBt)
			{
				
				if (_dBottonScore != 0)
				{
					Multiplee = _MyselfScore / (3 * _dBottonScore);
					if (Multiplee >= Multiple[count])
						n->setVisible(true);
				}
				else
					n->setVisible(true);

				count++;
			}
		}
	}





	void GameTableUI::checkInviAndReadyPos()
	{
		Button* toButton = nullptr;
		Button* pBtnArray[10] = {};
		char numBtn = 0;
		if (_canvas.invite->isVisible()) 
		{
			toButton = _canvas.invite;
			numBtn++;
		}
		if (_canvas.reday->isVisible()) 
		{
			toButton = _canvas.reday;
			numBtn++;
		}
		if(!toButton) return ;
	
		auto winSize = Director::getInstance()->getWinSize();
		cocos2d::Vec2 centerPos = Vec2(winSize.width/2.0f, winSize.height/2.0f);
		cocos2d::Vec2 toPos = _canvas.invite->getParent()->convertToNodeSpace(centerPos);

		if (numBtn>1)
		{
			const int MAX_DIST = 120;
// 			_canvas.invite->setPositionX(toPos.x - MAX_DIST);
// 			_canvas.reday->setPositionX(toPos.x + MAX_DIST);
		}
		else
		{
			toButton->setPositionX(toPos.x);
		}
	}

	void GameTableUI::showOffLine(BYTE seat)
	{
		if (_players[seat])
		{
			_players[seat]->showOffLine();
		}
	}

	void GameTableUI::showBatBtVisble(bool isVisble)
	{
	
		setBeiBtVisible(isVisble);	
	}

	void GameTableUI::showqiangZhuangTips(BYTE qzStates[])
	{
		BYTE zhuangState[PLAY_COUNT];
		memcpy(zhuangState,qzStates,sizeof(zhuangState));

		for (int i = 0; i < PLAY_COUNT; i++)
		{
			int viewSat=_tableLogic->logicToViewSeatNo(i);
			if ((_canvas.seats[viewSat]==nullptr)) 
			{
				continue;
			}
			ImageView* qzTips=(ImageView*)_canvas.seats[viewSat]->getChildByName("qzTips");
			Button* qzdengs=(Button*)_canvas.seats[viewSat]->getChildByName("qiangDeng");
			qzTips->setVisible(true);
			//qzdengs->setVisible(true);
			if (zhuangState[i]==1)
			{

				qzTips->setVisible(true);
				//qzTips->setSize(Size(31, 30));
				qzTips->loadTexture("NN8/table/grab.png");
				/*if (viewSat ==0)
				{
					HNAudioEngine::getInstance()->playEffect("Games/NN8/audio/nn/qiangzhuang.mp3");
				}*/
				
				//qzdengs->setVisible(true);
			}
			else if (zhuangState[i]==0)
			{
				//qzdengs->setVisible(false);
				qzTips->setVisible(true);
				//qzTips->setSize(Size(73, 74));
				qzTips->loadTexture("NN8/table/grab_multi_0.png");
				/*if (viewSat == 0)
				{
					HNAudioEngine::getInstance()->playEffect("Games/NN8/audio/nn/buqiang.mp3");
				}*/

			}
			else
			{
				qzTips->setVisible(false);
				qzdengs->setVisible(false);
			}
	
		}
	}

	void GameTableUI::hideqiangZhuangTips()
	{
		this->unschedule(schedule_selector(GameTableUI::dengBtPlayAction));
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			if ((_canvas.seats[i]==nullptr)) 
			{
				continue;
			}
			ImageView* qzTips=(ImageView*)_canvas.seats[i]->getChildByName("qzTips");
			Button* qzdengs=(Button*)_canvas.seats[i]->getChildByName("qiangDeng");
			qzTips->setVisible(false);
			qzdengs->setBrightStyle(Widget::BrightStyle::NORMAL);
			qzdengs->setVisible(false);

		}
		resetDengTipsData();
	}

	void GameTableUI::playQiangzhuangAciton(BYTE qzResult[],BYTE seat,bool visble)
	{
		BYTE zhuangState[PLAY_COUNT];
		memset(_state,0,sizeof(_state));
		memcpy(zhuangState, qzResult, sizeof(zhuangState));
		_canvas.clock->setVisible(false);
		unschedule(schedule_selector(GameTableUI::callScoreClock));
		_vecBtList.clear();
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			int viewSat = _tableLogic->logicToViewSeatNo(i);
			if ((_canvas.seats[viewSat] == nullptr))
			{
				continue;
			}

			if (zhuangState[i] == 1)
			{
				_vecBtList.push_back(_canvas.kuangBtn[viewSat]);
			}
		}
		int count = 0;
		for (int i = 0; i < PLAY_COUNT;i++)
		{
			if (zhuangState[i] == 1)
			{
				_state[count++] = i;//存储抢庄的玩家位置
			}
		}

		if (_vecBtList.size() <= 1)
		{

			int viewSeat = _tableLogic->logicToViewSeatNo(_zhuangSeat);

			if (_players[viewSeat])
			{
				HNAudioEngine::getInstance()->playEffect("NN8/audio/table/banker.mp3");
				_players[viewSeat]->playPartFrame();
			}
			showDealerMark(viewSeat, true);

		}
		else
		{
			this->schedule(schedule_selector(GameTableUI::dengBtPlayAction), 0.2f);
		}
	}

	void GameTableUI::dengBtPlayAction(float t)
	{
		int count = _playDengActCount / _vecBtList.size();
		if (_vecBtList.size() <= 0 || count == 7)
		{
			this->unschedule(schedule_selector(GameTableUI::dengBtPlayAction));
			int viewSeat = _tableLogic->logicToViewSeatNo(_zhuangSeat);
			showDealerMark(viewSeat, true);

			if (_players[viewSeat])
			{
				HNAudioEngine::getInstance()->playEffect("NN8/audio/table/banker.mp3");
				_players[viewSeat]->playPartFrame();
			}
			hideqiangZhuangTips();
			return;
		}


	
		int index = _state[_playDengActCount%_vecBtList.size()];

		int viewseat = _tableLogic->logicToViewSeatNo(index);
		Sprite* sp = Sprite::create("NN8/table/choice_box.png");
		sp->setAnchorPoint(Vec2(0.5, 0.5));
		
		_canvas.seats[viewseat]->addChild(sp);

		sp->setPosition(_canvas.kuangBtn[viewseat]->getPosition());
		sp->setScale(1.1f);
		sp->setZOrder(20000);
		Sequence* seqAct = Sequence::create(FadeIn::create(0.1f), FadeOut::create(0.1f), RemoveSelf::create(true), nullptr);
		sp->runAction(seqAct);
		HNAudioEngine::getInstance()->playEffect("NN8/audio/table/choosing.mp3");
		_playDengActCount++;


	}

	void GameTableUI::resetDengTipsData()
	{
		_playDengActCount=0;
		_vecBtList.clear();
	}

	void GameTableUI::setZhuangSeat(BYTE seat)
	{
		_zhuangSeat=seat;
	}

	void GameTableUI::reConnect()
	{
		//LoadingLayer::removeLoading();
	}

	void GameTableUI::showCallNode()
	{
		if (_pIsGudingYaFen)
		{
			return;
		}
		int count = 0;
		int Multiplee = 0;
		for (auto n : _canvas.beiBt)
		{
			if (_dBottonScore != 0)
			{
				Multiplee = _MyselfScore / (3 * _dBottonScore);
				if (Multiplee >= Multiple[count])
					n->setVisible(true);
			}
			else
				n->setVisible(true);

			count++;
		}
	}

	void GameTableUI::sendCardState(BYTE viewSeat,BYTE cards[])
	{
		BYTE value[5]={0};
		memcpy(value,cards,sizeof(value));
		
		if (_suite)
		{
			_suite->stopAllActions();
			_suite->setEmptyState(true);
			_suite->recycle(0.1f);

		}

		BYTE logicSeat = _tableLogic->viewToLogicSeatNo(viewSeat);
		if (_handcard[viewSeat] != nullptr && _tableLogic->isPlaying(logicSeat))
		{
			_handcard[viewSeat]->setValueList(value, HANDCARD_COUNT);
		}


	}


	void GameTableUI::setUserScore(int score[PLAY_COUNT], int a)
	{
		memcpy(_userScore,score,sizeof(_userScore));
		upDateUserMoney();
	    
		_canvas.renshu->setString(StringUtils::toString(a));
	}

	void GameTableUI::upDateUserMoney()
	{
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* userinfo=_tableLogic->getUserBySeatNo(i);
			if (userinfo==nullptr)
			{
				continue;;
			}
			int viewSeat=_tableLogic->logicToViewSeatNo(i);
			int money=_userScore[i];
			//_players[viewSeat]->upDateUserMoney(money);
			_players[viewSeat]->upDateUserCoin(money);
			if (viewSeat == 0)
				_MyselfScore = money;


			
		}
	}

	void GameTableUI::showGameDeskNotFound()
	{
		std::string text = "";
		auto prompt = GamePromptLayer::create(true);
		prompt->showPrompt(text);
		prompt->setCallBack([this](){
			GamePlatform::returnPlatform(LayerType::ROOMLIST);
		});
	}

	void GameTableUI::hideBankerTipsAndBetMoneyTips()
	{
		for (int i=0;i<PLAY_COUNT;i++)
		{

			if (  _players[i])
			{
				_players[i]->setBetMoneyVisibel(false);
				_players[i]->showCallMark(false);
			}

		}
	}

	bool GameTableUI::isGameFinish()
	{
		return _gameFinish;
	}

	void GameTableUI::showFinalResult()
	{
		Node* pRootNode=Director::getInstance()->getRunningScene();
		Node* pResultNode=pRootNode->getChildByName("AllResultShow");
		if (pResultNode)
		{
			pResultNode->setVisible(true);
		}

	}

	void GameTableUI::setGMButtonVisibel(bool isVisibel)
	{
		if(_canvas.btnSuperSet)
		{
			_canvas.btnSuperSet->setVisible(isVisibel);
		}
	}

	void GameTableUI::showContolSuccese(bool isSuccese)
	{
		Size winSize=Director::getInstance()->getWinSize();
		std::string pShowStr = "";

		
		Label* pShowLeble = Label::createWithTTF(pShowStr,"platform/common/RTWSYueRoudGoG0v1-Regular.ttf",100);
		pShowLeble->setPosition(Vec2(winSize.width/2,winSize.height/3));
		pShowLeble->setColor(Color3B::RED);
		addChild(pShowLeble,1000000);
		Sequence* action= Sequence::create(DelayTime::create(1.0),RemoveSelf::create(true),nullptr);
		pShowLeble->runAction(action);
	}

	void GameTableUI::gmBtClickCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED == touchtype)
		{
			HNAudioEngine::getInstance()->playEffect("platform/sound/sound_button.mp3");
			if (Widget::TouchEventType::ENDED != touchtype)	return;
			GameSuperSet* pSuper=GameSuperSet::create(_tableLogic);
			this->addChild(pSuper,100000000);
		}
	}

	void GameTableUI::updateHandCardWithAction(BYTE cards[], BYTE seat,UserTanPai bull,float delayTime)
	{
	
		Node* pNode =Node::create();
		this->addChild(pNode);
		auto pCallFunc = [&,cards,seat,bull]()
		{
			updateHandcard(cards,seat,true);
			showBull(bull);
		};
		Sequence* pSequnen=Sequence::create(DelayTime::create(delayTime),CallFunc::create(pCallFunc),RemoveSelf::create(true),nullptr);
		pNode->runAction(pSequnen);

	}

	void GameTableUI::showTanPai(bool isVisible)
	{
		_canvas.btnTanPai->setVisible(isVisible);
		if (isVisible)
		{
			//showJisuanNode(true);
			_canvas.clock->setVisible(true);
			showRunTiom(20, CLOCK_TANPAI);
		}
		else
		{
			for (int i = 0; i < PLAY_COUNT;i++)
			{
				if (_players[i])
				{
					showJisuanNode(false);
					_players[i]->showSign(isVisible);
				}
			}
		}
	}

	void GameTableUI::showKuangBtnAction(BYTE seat, bool isPlay)
	{
		if (isPlay == true)
		{
			auto action1 = FadeIn::create(2.0f);
			auto action2 = action1->reverse();
			_canvas.kuangBtn[seat]->runAction(Sequence::create(DelayTime::create(0.5f), action1, action2, NULL));
		}
	}

	int GameTableUI::GetPoint(int Card)
	{
		if (Card == 0x00)
			return 0;
		switch (GetCardNum(Card))
		{
		case 10:
			return 10;
		case 11:
			return 10;
		case 12:
			return 10;
		case 13:
			return 10;
		case 14:
			return 1;
		case 15:
			return 10;
		case 16:
			return 10;
		default:
			return GetCardNum(Card);
		}
	}

	void GameTableUI::jinYongReturn(bool isPlay)
	{
		_canvas.btnreturn->setEnabled(isPlay);
	}

	void GameTableUI::callScoreClock(float dt)
	{
		_clockTime--;
		if (_clockTime > 0 && _clockTime <= 3)
		{
			HNAudioEngine::getInstance()->playEffect("NN8/audio/table/alert.mp3");

		}
		_canvas.second->setString(StringUtils::format("%d", _clockTime));
		if (_clockTime <= 0)
		{
			// 			RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_GM_AGREE_GAME);
			// 			
			_clockTime = 0;
			_canvas.clock->setVisible(false);
			if (_gameState == CLOCK_QIANGZHUANG)
			{
				
				
				_canvas.qiangzhuangBt->setVisible(false);
				_canvas.buqiangBt->setVisible(false);
				_canvas.clock->setVisible(false);
				_tableLogic->sendCallScore(false);
			}
			else if (_gameState == CLOCK_TANPAI)
			{
				_canvas.btnTanPai->setVisible(false);
				_canvas.clock->setVisible(false);
				_handcard[0]->setTouchable(false);
				RoomLogic()->sendData(MDM_GM_GAME_NOTIFY, ASS_USER_TANPAI, 0, 0);
			}
			else if (_gameState == CLOCK_XIAZHU)
			{
				for (int i = 0; i < PLAY_COUNT;i++)
				{
					int viewSeat = _tableLogic->logicToViewSeatNo(i);
					if (_players[viewSeat])
					{
						//_players[viewSeat]->setBetMoneyVisibel(true);
						if (!_players[viewSeat]->isClickBet())
						{
							_players[viewSeat]->showCallMark(1);
						}
					}
				}
			}
			unschedule(schedule_selector(GameTableUI::callScoreClock));
		}

	}

	void GameTableUI::showRunTiom(int time, int nGameState)
	{
		_gameState = nGameState;
		_clockTime = time;
		_canvas.second->setString(StringUtils::format("%d", _clockTime));
		if (time <= 0)
		{
			unschedule(schedule_selector(GameTableUI::callScoreClock));
		}
		else
		{
			schedule(schedule_selector(GameTableUI::callScoreClock), 1.0f);
		}
	}

	void GameTableUI::removeUser(BYTE seatNo)
	{
		if (!seatNoIsOk(seatNo))	return;
		_players[seatNo]->setVisible(false);		_players[seatNo]->setUIVisble(false);
	}
	
	bool GameTableUI::seatNoIsOk(BYTE seatNo)
	{
		return (seatNo < PLAY_COUNT && seatNo >= 0);
	}

	void GameTableUI::showTanPaiSign(int seat, bool isVisable)
	{
		if (_players[seat])
		{
			_players[seat]->showSign(isVisable);
		}
	}

	void GameTableUI::playGameStartAni()
	{
		HNAudioEngine::getInstance()->playEffect("NN8/audio/table/game_start.mp3");
 		Size size = Director::getInstance()->getWinSize();
 		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("NN8/qiangzhuangniuniu/qiangzhuangniuniu0.png", "NN8/qiangzhuangniuniu/qiangzhuangniuniu0.plist"
			,"NN8/qiangzhuangniuniu/qiangzhuangniuniu.ExportJson");
		auto  armature = cocostudio::Armature::create("qiangzhuangniuniu");
		armature->getAnimation()->play("qzniuniuyouxikaishi");
		armature->setScale(1.0f);
		armature->setPosition(size.width / 2, size.height / 2 + 12);
		armature->setVisible(true);
		this->addChild(armature, 100);
		setAllUserRedayUnVisble();
		std::function<void(Armature*, MovementEventType, const std::string&)> armatureFun = [=](Armature* armature, MovementEventType type, const std::string& id)

		{
			if (type == MovementEventType::COMPLETE)
			{
				armature->getAnimation()->stop();
				armature->runAction(Sequence::create(DelayTime::create(.1f), CallFunc::create(CC_CALLBACK_0(Node::removeFromParent, armature)), NULL));
			}
		};
		armature->getAnimation()->setMovementEventCallFunc(armatureFun);
	}

	void GameTableUI::showSignState(int seat,int state)
	{
		if (_players[seat])
		{
			if (seat == 0)
			{
				if (state == QIANGZHUANG)
				{
					_players[seat]->showGameState("NN8/table/pls_grab.png", true);
				}
				else if (state == XIAZHU)
				{
					if (_tableLogic->logicToViewSeatNo(_zhuangSeat) != seat)
					{
						_players[seat]->showGameState("NN8/table/pls_throw.png", true);
					}
				}
			}
			else
			{
				if (state == QIANGZHUANG)
				{
					_players[seat]->showGameState("NN8/table/pls_grabing.png", true);
				}
				else if (state == XIAZHU)
				{
					if (_tableLogic->logicToViewSeatNo(_zhuangSeat) != seat)
					{
						_players[seat]->showGameState("NN8/table/pls_throwing.png", true);
					}
				}
			}
		}
	}

	void GameTableUI::hideSign(int seat)
	{
		if (_players[seat])
		{
			_players[seat]->showGameState("", false);
		}

	}

	void GameTableUI::closeClock()
	{
		_canvas.clock->setVisible(false);
		unschedule(schedule_selector(GameTableUI::callScoreClock));
	}

	void GameTableUI::leaveDesk(float dt)
	{
// 		_tableLogic->stop();
// 		RoomLogic()->close();
// 		GamePlatform::createPlatform();
	}


	void GameTableUI::showJisuanNode(bool isvible)
	{
		if (nullptr == _calculate)
		{
			Vec2 pos = _winSize / 2 + Size(0, -95);
			_calculate = CalculateNode::create();
			_calculate->setAnchorPoint(Vec2(0.5f, 0.5f));
			_calculate->setName("CalculateNode");
			_calculate->setPosition(pos);
			_canvas.tableBg->addChild(_calculate, 100);
		}

		_calculate->setVisible(isvible);
		_calculate->clear();

	}

	bool GameTableUI::calculateNN(BYTE cardValue, bool isAddCardValue)
	{
		if (!_calculate || !_calculate->isVisible()) return true;
		return	_calculate->calculateNN(cardValue, isAddCardValue);
	}

	void GameTableUI::chatBtnClickCallBack(Ref* ref, Widget::TouchEventType type)
	{
		if (RoomLogic()->getRoomRule() & GRR_FORBID_GAME_TALK)
		{
			auto prompt = GamePromptLayer::create();
			prompt->showPrompt(GBKToUtf8("禁止游戏聊天"));
			return;
		}
		if (_chatLayer == nullptr)
		{
			//显示聊天界面
			gameChatLayer();
		}
		_chatLayer->showChatLayer();
	}

	void GameTableUI::gameChatLayer()
	{
		_chatLayer = GameChatLayer::create();
		addChild(_chatLayer, Max_Zorder);
		_chatLayer->setPosition(_winSize / 2);
		_chatLayer->onSendTextCallBack = [=](const std::string msg) {
			if (!msg.empty())
			{
				_tableLogic->sendChatMsg(msg);
			}
		};
	}

	void GameTableUI::clickSettingCallBack(cocos2d::Ref* pSender)
	{
		auto setLayer = GameSetLayer::create();
		setLayer->open(ACTION_TYPE_LAYER::FADE, this, Vec2::ZERO, 50);
		setLayer->setName("setLayer");
		setLayer->onExitCallBack = [=]() {
			if (_roomController)
			{
				_roomController->returnBtnCallBack(pSender);
			}
			else
			{
				_tableLogic->sendStandUp();
			}
			removeSetLayer();
		};

		setLayer->onDisCallBack = [=]() {

			if (_roomController) _roomController->dismissBtnCallBack(pSender);
			removeSetLayer();
		};
		if (RoomLogic()->getRoomRule() & GRR_GAME_BUY)
		{
			//设置游戏状态按钮
			setLayer->setGameStateView();
		}
		else
		{
			//设置金币场退出
			setLayer->setGameGlodExit();
		}
	}

	void GameTableUI::onChatTextMsg(BYTE seatNo, CHAR msg[])
	{
		auto userInfo = _tableLogic->getUserBySeatNo(seatNo);
		auto viewSeatNo = _tableLogic->logicToViewSeatNo(seatNo);

		auto herd = dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(_canvas.seats[viewSeatNo], "userHead"));

		Vec2 bubblePostion = herd->getParent()->convertToWorldSpace(herd->getPosition());

 		Size headSize = _players[viewSeatNo]->getHeadSize();

		bool isFilpped = true;

		if (viewSeatNo > 0 && viewSeatNo < 4)
		{
			isFilpped = false;
		}

		_chatLayer->onHandleTextMessage(_players[viewSeatNo]->getParent(), bubblePostion, msg, userInfo->nickName, userInfo->bBoy, isFilpped, viewSeatNo);
	}

	void GameTableUI::onChatVoiceMsg(int userID, int voiceID, int voiceTime)
	{
		if (nullptr == _chatLayer) return;

		UserInfoStruct* userInfo = _tableLogic->getUserByUserID(userID);

		if (nullptr == userInfo) return;
		auto viewSeatNo = _tableLogic->logicToViewSeatNo(userInfo->bDeskStation);
		auto herd = dynamic_cast<ImageView*>(ui::Helper::seekWidgetByName(_canvas.seats[viewSeatNo], "userHead"));

		Vec2 bubblePostion = herd->getParent()->convertToWorldSpace(herd->getPosition());

		Size headSize = _players[viewSeatNo]->getHeadSize();

		bool isFilpped = true;

		if (viewSeatNo > 0 && viewSeatNo < 4)
		{
			isFilpped = false;
		}
		_chatLayer->onHandleVocieMessage(_players[viewSeatNo]->getParent(), bubblePostion, voiceID, userInfo->nickName, isFilpped, viewSeatNo, voiceTime);
	}

	void GameTableUI::setDeskInfo()
	{
		auto info = _tableLogic->getUserByUserID(HNPlatformConfig()->getMasterID());
		if (info) showMaster(_tableLogic->logicToViewSeatNo(info->bDeskStation), true);

		if (HNPlatformConfig()->getNowCount() > 0)
		{
			//Layout* tableLayout = (Layout*)(_tableWidget->getChildByName("gold_Poker"));
			//Layout* layout_buttons = (Layout*)(tableLayout->getChildByName("layout_middle"));
			//Button* btn_start = (Button*)(_canvas.tableBg->getChildByName("btn_start"));
			//btn_start->setPositionX(_winSize.width / 2);
		}
	}

	void GameTableUI::showMaster(BYTE seatNo, bool bShow)
	{
		if (!seatNoIsOk(seatNo)) return;
		_players[seatNo]->setOwner(bShow);
	}



	void GameTableUI::removeSetLayer()
	{
		auto setLayout = this->getChildByName("setLayer");
		if (setLayout)
		{
			setLayout->removeFromParent();
		}
	}

	void GameTableUI::buxiangCallback(cocos2d::Ref* pSender, Widget::TouchEventType touchtype)
	{
		if (Widget::TouchEventType::ENDED == touchtype)
		{

			HNAudioEngine::getInstance()->playEffect("Games/NN8/audio/nn/buqiang.mp3");

			_tableLogic->sendCallScore(pSender == _canvas.qiangzhuangBt);

			_canvas.qiangzhuangBt->setVisible(false);
			_canvas.buqiangBt->setVisible(false);
			_canvas.clock->setVisible(false);
			unschedule(schedule_selector(GameTableUI::callScoreClock));
		}
	}

	void GameTableUI::sendRecordData(S_C_GameRecordResult* data)
	{
		int PlayerCount = 0;
		CHAR* szName[10];
		for (int j = 0; j < 10; j++)
		{
			szName[j] = "";
		}
		int index = 0;
		for (int i = 0; i < PLAY_COUNT; i++)
		{
			UserInfoStruct* pUser = _tableLogic->getUserBySeatNo(i);
			if (pUser)
			{
				PlayerCount++;
				szName[index] = pUser->nickName;
				index++;
			}
		}

		auto gameRecord = GameRecord::createWithData(szName, data->JuCount, data->GameCount, PlayerCount);
		addChild(gameRecord, 100000000);
	}
	
	void GameTableUI::ShowReadyBtn()
	{
		_canvas.reday->setVisible(true);
	}
}
